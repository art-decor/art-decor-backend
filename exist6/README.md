# Default eXist-db installer config file changes

This directory contains the files and changes needed to the default eXist db installer to be are applied for the ART-DECOR� tools suite.

## Jetty Ports

The following files need to be changed regarding the jetty port definition

* `etc/jetty/jetty-http.xml` - jetty.port shall be 8877
* `etc/jetty/jetty-ssl.xml` - jetty.ssl.port shall be 8843

## Jetty Web Portal

The file `etc/jetty/webapps/portal/index.html` need some tweaks to properly redirect the landing page to the dashboard.

```
old: <br/><a href="/exist">CONTINUE</a>
new: <br/><a href="/exist/apps/dashboard/">CONTINUE</a>

old: window.location.replace("/exist");
new: window.location.replace("/exist/apps/dashboard/");
```

## Jetty MaxFormContentSize

In order to avoid getting a "java.lang.IllegalStateException: Form too large" error when editing large page in eXist-db we need to tell Jetty to allow for large content since by default it only allows for 20K. 
We do this by passing the "org.eclipse.jetty.server.Request.maxFormContentSize" attribute.

Note that setting this value too high can leave your server vulnerable to denial of service attacks.

The file `etc/jetty/jetty.xml` has the added property `org.eclipse.jetty.server.Request.maxFormContentSize`.

```
<Call class="java.lang.System" name="setProperty">
  <Arg>org.eclipse.jetty.server.Request.maxFormContentSize</Arg>
  <Arg>1000000</Arg>
</Call>

```

## Disable Betterform

The following file contains property to switch off betterform

* etc/webapp/WEB-INF/betterform-config.xml
  - initLogging shall be false
  - filter.ignoreResponseBody shall be true

## ART-DECOR paths

The following file contains all necessary ART-DECOR paths, contains services and WebDAV is switched off

* etc/webapp/WEB-INF/controller-config.xml
  - shall comment out <forward pattern="/webdav/" servlet="milton"/>
  - shall have all ART-DECOR paths added, see file the path expressions in the `controller-config.xml` repository file between `<!-- ART-DECOR paths -->` and `<!-- /ART-DECOR paths -->`

## Scheduled jobs

Finally, the file conf.xml in /etc and contains a recommended set of ART-DECOR Release 3 Scheduled Jobs
