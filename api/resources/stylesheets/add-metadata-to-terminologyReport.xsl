<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:json="http://www.json.org" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0" xml:space="default" xpath-default-namespace="">
   <xsl:output method="xml"/>
   <xsl:template match="terminologyReport">
      <terminologyReport>
         <xsl:variable name="messageTypes" select="distinct-values(//message/@type)"/>
         <xsl:variable name="datasetConceptCount" select="count(datasets/dataset/concept[not(parent::conceptList)])"/>
         <xsl:variable name="valueSetConceptCount" select="count(valueSets/valueSet//concept)"/>
         <xsl:copy-of select="@*"/>
         <meta>
            <xsl:for-each select="$messageTypes">
               <xsl:sort select="."/>
               <messageType json:array="true"><xsl:value-of select="."/></messageType>
            </xsl:for-each>
            <datasets concepts="{$datasetConceptCount}">
               <xsl:variable name="messages" select="datasets//message"/>
               <xsl:for-each select="$messageTypes">
                  <xsl:sort select="."/>
                  <xsl:variable name="type" select="."/>
                  <xsl:if test="count($messages[@type=$type]) &gt; 0">
                     <message type="{$type}" count="{count($messages[@type=$type])}"/>
               </xsl:if>
               </xsl:for-each>
            </datasets>
            <transactions>
               <xsl:variable name="messages" select="transactions//message"/>
               <xsl:for-each select="$messageTypes">
                  <xsl:sort select="."/>
                  <xsl:variable name="type" select="."/>
                  <xsl:if test="count($messages[@type=$type]) &gt; 0">
                     <message type="{$type}" count="{count($messages[@type=$type])}"/>
               </xsl:if>
               </xsl:for-each>
            </transactions>
            <valueSets concepts="{$valueSetConceptCount}">
               <xsl:variable name="messages" select="valueSets//message"/>
               <xsl:for-each select="$messageTypes">
                  <xsl:sort select="."/>
                  <xsl:variable name="type" select="."/>
                  <xsl:if test="count($messages[@type=$type]) &gt; 0">
                     <message type="{$type}" count="{count($messages[@type=$type])}"/>
               </xsl:if>
               </xsl:for-each>
           </valueSets>
         </meta>
         <xsl:copy-of select="datasets"/>
         <xsl:copy-of select="transactions"/>
         <xsl:copy-of select="valueSets"/>
         <xsl:copy-of select="ids"/>
      </terminologyReport>
   </xsl:template>
</xsl:stylesheet>