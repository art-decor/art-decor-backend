xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace repo                = "http://exist-db.org/xquery/repo";
import module namespace permissionslib      = "http://art-decor.org/ns/api/permissions" at "modules/library/permissions-lib.xqm";
import module namespace setlib              = "http://art-decor.org/ns/api/settings" at "modules/library/settings-lib.xqm";
import module namespace utillib             = "http://art-decor.org/ns/api/util" at "modules/library/util-lib.xqm";
import module namespace userapi             = "http://art-decor.org/ns/api/user" at "modules/user-api.xqm";
import module namespace serverapi           = "http://art-decor.org/ns/api/server" at "modules/server-api.xqm";

declare namespace f     = "http://hl7.org/fhir";

(: The following external variables are set by the repo:deploy function :)

(:  home="/Applications/eXist-db" 
    dir="/Applications/eXist-db/webapp/WEB-INF/data/expathrepo/ART-1.1.37/." 
    target="/db/apps/art" 
    root="/db/apps/"
:)

(: file path pointing to the exist installation directory :)
declare variable $home external;
(: path to the directory containing the unpacked .xar package :)
declare variable $dir external;
(: the target collection into which the app is deployed :)
declare variable $target external;
(:install path for art (/db, /db/apps), no trailing slash :)
declare variable $root := repo:get-root();

declare %private function local:copy($source as xs:string, $target as xs:string, $base as xs:boolean) {
let $dirName        := tokenize($source,'/')[last()]
let $targetDirName  := if ($base) then $target else (concat($target,'/',$dirName))
let $createDir      := 
    if (xmldb:collection-available($targetDirName)) then () else (
        xmldb:create-collection(string-join(tokenize($targetDirName,'/')[not(position()=last())],'/'),tokenize($targetDirName,'/')[last()])
    )
let $chmodDir       :=
    if (tokenize($targetDirName,'/')[last()] = 'user-info') then (
        sm:chmod($targetDirName, 'rwsrwsr-x'), sm:chown($targetDirName, 'admin:dba')
    ) else ()
let $copyResources  :=
    for $r in xmldb:get-child-resources($source)
    return
        if ($r=xmldb:get-child-resources($targetDirName)) then () else (
            xmldb:copy-resource($source, $r, $targetDirName, $r)
        )
let $copyCollections :=
    for $c in xmldb:get-child-collections($source)
    return
        local:copy(concat($source,'/',$c),$targetDirName, false())

return ()
};

let $installextras          := local:copy(concat($target,'/install-data'),concat($root,'art-data'), true())

(: check if message collection exists, if not then create and set permissions :)
let $fixpermissions         := permissionslib:setApiPermissions()

let $decorTypes             := utillib:getDecorTypes(true())

(: 2017-03-25 patch required because of incorrectly added user info data :)
(:let $patchuserdata          := userapi:patchAllProjectPreferences():)
(: 2017-06-12 patch required because groups with spaces in the name would be concatenated in a way that you could not untangle them :)
(:let $patchuserdata          := userapi:patchAllUserInfo():)

(: make sure we have an external url to point to. If localhost: assume that exist-db runs on port 8877 :)
let $serverapiurl           := replace(serverapi:getServerURLArt(), '/art-decor/', '/exist/apps/api')
let $serverapiurl           := replace($serverapiurl, 'localhost:\d+', 'localhost:8877')
let $patchServerApiUrl      := if (empty(serverapi:getServerURLArtApi())) then serverapi:setServerURLArtApi($serverapiurl) else ()

(: now add that into the api.json definition, if the second url is different from localhost url :)
let $json                   := json-doc($target || '/modules/library/api.json')
let $serverapiurl           := serverapi:getServerURLArtApi()
let $updateApi              :=
    if (array:get($json?servers, 1)?url = serverapi:getServerURLArtApi()) then () else (
        let $newservers     := 
            array:insert-before($json?servers, 1, map { "description": 'ART-DECOR API server (' || substring-before($serverapiurl, ':') || ')', "url": $serverapiurl })
        let $newservers     := 
            if (starts-with($serverapiurl, 'http://localhost')) then () else
            if (starts-with($serverapiurl, 'http:')) then
                array:insert-before($newservers, 1, map { "description": 'ART-DECOR API server (https)', "url": 'https:' || substring-after($serverapiurl, ':') })
            else ($newservers)
        let $update                 := map:remove($json, 'servers')
        let $update                 := map:merge(($update, map:entry('servers', $newservers)))
        return
            xmldb:store($target || '/modules/library', 'api.json', 
                        fn:serialize($update, map {'method':'json', 'indent': true(), 'json-ignore-whitespace-text-nodes':'yes'}), 'application/json'
            )
    )
let $applyServerPatches     := permissionslib:applyPostInstallPatches()
let $applyServerPatches     := userapi:patchAllUserInfo()
    
return ()