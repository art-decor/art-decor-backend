const util = require('./util.js');
const path = require('path');

const chai = require('chai');
const expect = chai.expect;

const chaiResponseValidator = require('chai-openapi-response-validator');
const spec = path.resolve("modules/library/api.json");
chai.use(chaiResponseValidator(spec));

describe('Path /server/user without authentication', function () {
    it('GET user list expect 403', function () {
        return util.axios.get('server/user')
        .catch(function(error) {
            console.log(error.response.data);
            expect(error.response.status).to.equal(403);
        });
    });
});
describe('Path /server/user with authentication', function () {
    it('GET user list expect success and a list of type USER', async function () {
        const res = await util.axios.get('server/user');
        //console.log(res.data);
        expect(res.status).to.equal(200);
        expect(res.data.artifact).to.equal('USER');
    });
});
describe('Path /server/user/{name} without authentication', function () {
   it('GET user admin expect 403', function () {
        return util.axios.get('server/user/admin')
        .catch(function(error) {
            //console.log(error.response.data);
            expect(error.response.status).to.equal(403);
        });
    });
});
describe('Path /server/user/{name} with authentication', function () {
    before(util.login);
    it('GET user admin expect success', async function () {
        const res = await util.axios.get('server/user/admin');
        //console.log(res.data);
        expect(res.status).to.equal(200);
    });
});