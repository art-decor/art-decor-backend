const util = require('./util.js');
const path = require('path');

const chai = require('chai');
const expect = chai.expect;

const chaiResponseValidator = require('chai-openapi-response-validator');
const spec = path.resolve("modules/library/api.json");
chai.use(chaiResponseValidator(spec));

let projectPrefix = 'demo1-';
describe('Path /dataset from project '+projectPrefix, function () {
    let response;
    let datasetId;
    let datasetEffectiveDate;
    let conceptId;
    let conceptEffectiveDate;
    
    before(async function () {
        return util.axios.get('dataset?prefix='+projectPrefix)
            .then (function(res) {
                response = res;
                datasetId = res.data.dataset[0].id;
                datasetEffectiveDate = res.data.dataset[0].effectiveDate;
                //console.log('      Found dataset id: '+datasetId+' effectiveDate: '+datasetEffectiveDate);
            })
            .catch(function(error) {
                console.log(error.response);
            });
        }
    );

    it('Expect successful retrieval status of the dataset list', function () {
        expect(response.status).to.equal(200);
    });
    it('Expect list artifact value', function () {
        expect(response.data.artifact).to.equal('DS');
    });
    it('Expect list to have contents', function () {
        expect(response.data.dataset).to.be.an('array');
    });
    //https://github.com/openapi-library/OpenAPIValidators/issues/216
    /*it('Expect the response to conform to the api specification', function () {
        expect(response).to.satisfyApiSpec;
    });*/

    describe('Path /dataset/{id}', function () {
        it('GET dataset by id, expect success', async function () {
            return util.axios.get('dataset/'+datasetId)
            .then (function(res) {
                //console.log(res.data);
                expect(res.status).to.equal(200);
            })
            .catch(function(error) {
                //console.log(error.response);
            });
        });
    });
    describe('Path /dataset/{id}/{effectiveDate}', function () {
        it('GET dataset by id and effectiveDate, expect success', async function () {
            return util.axios.get('dataset/'+datasetId+'/'+encodeURIComponent(datasetEffectiveDate))
            .then (function(res) {
                //console.log(res.data);
                expect(res.status).to.equal(200);
            })
            .catch(function(error) {
                //console.log(error.response);
            });
        });
    });
    
    describe('Path /concept/{id}', function () {
        before(async function () {
            return util.axios.get('dataset/'+datasetId)
                .then (function(res) {
                    conceptId = res.data.concept[0].id;
                    conceptEffectiveDate = res.data.concept[0].effectiveDate;
                    //console.log('      Found concept id: '+conceptId+' effectiveDate: '+conceptEffectiveDate);
                })
                .catch(function(error) {
                    console.log(error.response);
                });
            }
        );
    
        it('GET concept by id, expect success', async function () {
            return util.axios.get('concept/'+conceptId)
            .then (function(res) {
                //console.log(res.data);
                expect(res.status).to.equal(200);
            })
            .catch(function(error) {
                //console.log(error.response);
            });
        });
        it('GET concept by id and effectiveDate, expect success', async function () {
            return util.axios.get('concept/'+conceptId+'/'+encodeURIComponent(conceptEffectiveDate))
            .then (function(res) {
                //console.log(res.data);
                expect(res.status).to.equal(200);
                //console.log('      Found concept id: '+res.data.id+' effectiveDate: '+res.data.effectiveDate);
            })
            .catch(function(error) {
                //console.log(error.response);
            });
        });
    });
    
});