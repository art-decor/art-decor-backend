const util = require('./util.js');
const path = require('path');

const chai = require('chai');
const expect = chai.expect;

const chaiResponseValidator = require('chai-openapi-response-validator');
const spec = path.resolve("modules/library/api.json");
chai.use(chaiResponseValidator(spec));

describe('Path /token - login', function () {
    let res;
    it('POST login details from .existdb.json, expect 201 + token', async function () {
        try {
            res = await util.login;
        }
        catch (e) {
            console.error(e.message);
        }
        finally {}
    });
    it('POST admin/testing123, expect 401', async function () {
        return util.axios.post( 'token', 
            {
                "username": "admin",
                "password": "testing123"
            }
        )
        .catch(function(error) {
            //console.log(error.response.data);
            expect(error.response.status).to.equal(401);
        });
    });
});