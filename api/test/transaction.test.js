const util = require('./util.js');
const path = require('path');

const chai = require('chai');
const expect = chai.expect;

const chaiResponseValidator = require('chai-openapi-response-validator');
const spec = path.resolve("modules/library/api.json");
chai.use(chaiResponseValidator(spec));

let projectPrefix = 'demo1-';
describe('Path /transaction from project '+projectPrefix, function () {
    let response;
    let transactionId;
    let transactionEffectiveDate;
    
    before(async function () {
        return util.axios.get('scenario?prefix='+projectPrefix)
            .then (function(res) {
                response = res;
                transactionId = res.data.scenario[0].transaction[0].id;
                transactionEffectiveDate = res.data.scenario[0].transaction[0].effectiveDate;
                //console.log('      Found transaction id: '+transactionId+' effectiveDate: '+transactionEffectiveDate);
            })
            .catch(function(error) {
                console.log(error.response);
            });
        }
    );

    it('Expect successful retrieval status of the scenario list', function () {
        expect(response.status).to.equal(200);
    });
    it('Expect list artifact value', function () {
        expect(response.data.artifact).to.equal('SC');
    });
    it('Expect list to have contents', function () {
        expect(response.data.scenario).to.be.an('array');
    });
    //https://github.com/openapi-library/OpenAPIValidators/issues/216
    /*it('Expect the response to conform to the api specification', function () {
        expect(response).to.satisfyApiSpec;
    });*/

    describe('Path /transaction/{id}', function () {
        it('GET transaction by id, expect success', async function () {
            return util.axios.get('transaction/'+transactionId)
            .then (function(res) {
                //console.log(res.data);
                expect(res.status).to.equal(200);
            })
            .catch(function(error) {
                //console.log(error.response);
            });
        });
    });
    describe('Path /transaction/{id}/{effectiveDate}', function () {
        it('GET transaction by id and effectiveDate, expect success', async function () {
            return util.axios.get('transaction/'+transactionId+'/'+encodeURIComponent(transactionEffectiveDate))
            .then (function(res) {
                //console.log(res.data);
                expect(res.status).to.equal(200);
            })
            .catch(function(error) {
                //console.log(error.response);
            });
        });
    });
});
