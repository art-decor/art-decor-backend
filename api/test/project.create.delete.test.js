const util = require('./util.js');
const path = require('path');

const chai = require('chai');
const expect = chai.expect;

const chaiResponseValidator = require('chai-openapi-response-validator');
const spec = path.resolve("modules/library/api.json");
chai.use(chaiResponseValidator(spec));

let projectPrefix = 'demo999999-';
let projectNew = {
  "baseId": "2.16.840.1.113883.3.1937.9999999.62", "prefix": projectPrefix, "experimental": "true", "defaultLanguage": "en-US", "repository": "false", "private": "false",
  "name": [ { "language": "en-US", "#text": "Demo 9999999" }, { "language": "nl-NL", "#text": "Demo 9999999" } ],
  "desc": [ { "language": "en-US", "#text": "New demo project &lt;&gt;&amp;\"" } ],
  "copyright": [ { "years": "2012-", "by": "The ART-DECOR expert group" } ],
  "author": [ { "id": "1", "username": "admin", "notifier": "false", "#text": "Admin" } ],
  "restURI": [ 
      { "for": "DS", "format": "HTML", "#text": "https://art-decor.org/decor/services/RetrieveTransaction?id=__ID__&amp;effectiveDate=__ED__&amp;format=html" },
      { "for": "VS", "format": "XML", "#text": "https://art-decor.org/decor/services/RetrieveValueSet?id=__ID__&amp;effectiveDate=__ED__&amp;language=__LANG__&amp;prefix=__PFX__&amp;format=xml" },
      { "for": "FHIR", "format": "3.0", "#text": "https://art-decor.org/fhir/" },
      { "for": "FHIR", "format": "4.0", "#text": "https://art-decor.org/fhir/" }
  ],
  "reference": [ { "url": "http://demo9999999.art-decor.org/", "logo": "demo9999999.png" } ],
  "defaultElementNamespace": [ { "ns": "hl7:" } ]
};
let projectPatch = {
  "parameter": [
    { "op": "replace", "path": "/private", "value": "true" },
    { "op": "replace", "path": "/defaultLanguage", "value": "nl-NL" },
    { "op": "add", "path": "/name", "value": { "name": [ { "language": "de-DE", "#text": "yadadadad" } ] } },
    { "op": "add", "path": "/ns", "value": { "ns": [ { "uri": "urn:hl7-org:sdtc", "prefix": "sdtc" } ] } }
  ]
};
let projectResult = { 
  "id": "2.16.840.1.113883.3.1937.9999999.62.1", "prefix": projectPrefix, "experimental": "true", "defaultLanguage": "nl-NL", "repository": "false", "private": "true", 
  "lastmodified": "2021-02-06T17:22:11.138+01:00", "collection": "projects/demo999999", 
  "name": [
    { "language": "en-US", "lastTranslated": "2021-02-06T17:22:10", "#text": "Demo 9999999" },
    { "language": "nl-NL", "lastTranslated": "2021-02-06T17:22:10", "#text": "Demo 9999999" },
    { "language": "de-DE", "lastTranslated": "2021-02-06T17:22:11", "#text": "yadadadad" }
  ],
  "desc": [
    { "language": "en-US", "lastTranslated": "2021-02-06T17:22:10", "#text": "New demo project &lt;&gt;&amp;\"" }
  ],
  "copyright": [ { "years": "2012-", "by": "The ART-DECOR expert group" } ],
  "author": [ { "id": "1", "username": "admin", "effectiveDate": "2021-02-06T17:22:10", "notifier": "off", "dbactive": "true", "#text": "Admin" } ],
  "reference": [ { "url": "http://demo9999999.art-decor.org/", "logo": "demo9999999.png" } ],
  "service": [
    { "type": "adram", "status": "500" },
    { "type": "adawib", "status": "unknown" }
  ],
  "restURI": [
    { "for": "DS", "format": "HTML", "#text": "https://art-decor.org/decor/services/RetrieveTransaction?id=__ID__&amp;effectiveDate=__ED__&amp;format=html" }, 
    { "for": "VS", "format": "XML", "#text": "https://art-decor.org/decor/services/RetrieveValueSet?id=__ID__&amp;effectiveDate=__ED__&amp;language=__LANG__&amp;prefix=__PFX__&amp;format=xml" },
    { "for": "FHIR", "format": "3.0", "#text": "https://art-decor.org/fhir/" },
    { "for": "FHIR", "format": "4.0", "#text": "https://art-decor.org/fhir/" }
  ],
  "defaultElementNamespace": [ { "ns": "hl7:" } ],
  "ns": [
    { "uri": "urn:hl7-org:sdtc", "prefix": "sdtc", "default": "false" }, 
    { "uri": "urn:hl7-org:v3", "prefix": "hl7", "default": "true", "readonly": "true" }, 
    { "uri": "urn:hl7-org:v3", "prefix": "cda", "default": "false", "readonly": "true" }
  ],
  "statistics": [
    {
      "datasets": [ { "count": "1", "concepts": "1" } ],
      "scenarios": [ { "count": "0", "transactions": "0" } ],
      "terminologies": [ { "count": "0" } ],
      "profiles": [ { "count": "0" } ],
      "issues": [ { "count": "0", "open": "0" } ], 
      "releases": [ { "count": "0" } ]
    }
  ],
  "ids": [
    {
      "baseId": [
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.1", "type": "DS", "prefix": "demo999999-dataset-", "default": "true" },
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.2.1", "type": "DE", "prefix": "demo999999-dataelement-", "default": "true" },
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.3", "type": "SC", "prefix": "demo999999-scenario-", "default": "true" },
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.4", "type": "TR", "prefix": "demo999999-transaction-", "default": "true" }, 
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.5", "type": "CS", "prefix": "demo999999-codesystem-", "default": "true" },
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.6", "type": "IS", "prefix": "demo999999-issue-", "default": "true" },
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.7", "type": "AC", "prefix": "demo999999-actor-", "default": "true" },
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.8", "type": "CL", "prefix": "demo999999-conceptlist-", "default": "true" },
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.9", "type": "EL", "prefix": "demo999999-template-element-", "default": "true" },
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.10", "type": "TM", "prefix": "demo999999-template-", "default": "true" },
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.11", "type": "VS", "prefix": "demo999999-valueset-", "default": "true" },
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.16", "type": "RL", "prefix": "demo999999-rule-intern-", "default": "true" },
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.17", "type": "TX", "prefix": "demo999999-test-transaction-", "default": "true" },
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.18", "type": "SX", "prefix": "demo999999-test-scenario-", "default": "true" },
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.19", "type": "EX", "prefix": "demo999999-example-instance-", "default": "true" },
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.20", "type": "QX", "prefix": "demo999999-qualification-test-instance-", "default": "true" },
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.21", "type": "CM", "prefix": "demo999999-community-", "default": "true" },
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.24", "type": "MP", "prefix": "demo999999-map-", "default": "true" }
      ],
      "defaultBaseId": [
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.1", "type": "DS" }, 
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.2.1", "type": "DE" }, 
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.3", "type": "SC" }, 
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.4", "type": "TR" }, 
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.5", "type": "CS" }, 
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.6", "type": "IS" }, 
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.7", "type": "AC" }, 
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.8", "type": "CL" }, 
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.9", "type": "EL" }, 
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.10", "type": "TM" }, 
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.11", "type": "VS" }, 
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.16", "type": "RL" }, 
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.17", "type": "TX" }, 
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.18", "type": "SX" }, 
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.19", "type": "EX" }, 
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.20", "type": "QX" },
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.21", "type": "CM" },
        { "id": "2.16.840.1.113883.3.1937.9999999.62.1.24", "type": "MP" }
      ]
    }
  ],
  "issues": [ { "notifier": "on" } ]
};

describe('DELETE /project/'+projectPrefix, function () {
    before(util.login);
    
    it('DELETE project '+projectPrefix+', expect success', async function () {
        return util.axios.delete('project/'+projectPrefix)
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(204);
        })
        .catch(function(error) {
            error.response == null || error.response.data == null ? console.log(error) : console.log(error.response.data);
        });
    });
});
describe('CREATE /project', function () {
    before(util.login);

    it('POST project '+projectPrefix+', expect success', async function () {
        return util.axios.post('project', projectNew)
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(201);
        })
        .catch(function(error) {
            error.response == null || error.response.data == null ? console.log(error) : console.log(error.response.data);
        });
    });
});
describe('PATCH /project/'+projectPrefix, function () {
    before(util.login);

    it('PATCH project '+projectPrefix+', expect success', async function () {
        return util.axios.patch('project/'+projectPrefix, projectPatch)
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(200);
        })
        .catch(function(error) {
            error.response == null || error.response.data == null ? console.log(error) : console.log(error.response.data);
        });
    });
});
describe('CHECK /project', function () {
    before(util.login);

    it('GET project '+projectPrefix+', expect success', async function () {
        return util.axios.get('project/'+projectPrefix)
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(200);
            expect(res.data.prefix).to.equal(projectResult.prefix);
            expect(res.data.repository).to.equal(projectResult.repository);
            expect(res.data.experimental).to.equal(projectResult.experimental);
            expect(res.data.defaultLanguage).to.equal(projectResult.defaultLanguage);
        })
        .catch(function(error) {
            error.response == null || error.response.data == null ? console.log(error) : console.log(error.response.data);
        });
    });
});
describe('DELETE /project/'+projectPrefix, function () {
    before(util.login);
    
    it('DELETE project '+projectPrefix+', expect success', async function () {
        return util.axios.delete('project/'+projectPrefix)
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(204);
        })
        .catch(function(error) {
            error.response == null || error.response.data == null ? console.log(error) : console.log(error.response.data);
        });
    });
});