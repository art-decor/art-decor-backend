const util = require('./util.js');
const path = require('path');

const chai = require('chai');
const expect = chai.expect;
const chaiResponseValidator = require('chai-openapi-response-validator');

const spec = path.resolve("modules/library/api.json");
chai.use(chaiResponseValidator(spec));

describe('Path /news', function () {
    it('GET news, expect success', async function () {
        const res = await util.axios.get('news');
        //console.log(res.data);
        expect(res.status).to.equal(200);
    });
});
