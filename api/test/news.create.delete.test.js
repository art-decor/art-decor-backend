const util = require('./util.js');
const path = require('path');

const chai = require('chai');
const expect = chai.expect;

const chaiResponseValidator = require('chai-openapi-response-validator');
const spec = path.resolve("modules/library/api.json");
chai.use(chaiResponseValidator(spec));

let newsId;
let newsNew = {
  "showuntil": "2021-04-14T16:48:00Z",
  "text": [
    {
      "language": "en-US",
      "#text": "This is a Test Message 1"
    }
  ]
};
let newsUpdated = newsNew;

describe('CREATE /news', function () {
    before(util.login);

    it('POST news item expect success', async function () {
        return util.axios.post('news', newsNew)
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(201);
            newsId = res.data.id;
            newsUpdated.id = newsId;
        })
        .catch(function(error) {
            error.response == null || error.response.data == null ? console.log(error) : console.log(error.response.data);
        });
    });
});
describe('UPDATE /news/{id}', function () {
    before(util.login);

    it('PUT news, expect success', async function () {
        return util.axios.put('news/'+newsId, newsUpdated)
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(200);
        })
        .catch(function(error) {
            error.response == null || error.response.data == null ? console.log(error) : console.log(error.response.data);
        });
    });
});
describe('CHECK /news', function () {
    before(util.login);

    it('GET news, expect success', async function () {
        return util.axios.get('news')
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(200);
            /*expect(res.data.news).to.be.an("array");*/
        })
        .catch(function(error) {
            error.response == null || error.response.data == null ? console.log(error) : console.log(error.response.data);
        });
    });
});
describe('DELETE /news/{id}', function () {
    before(util.login);
    
    it('DELETE news, expect success', async function () {
        return util.axios.delete('news/'+newsId)
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(204);
        })
        .catch(function(error) {
            error.response == null || error.response.data == null ? console.log(error) : console.log(error.response.data);
        });
    });
});