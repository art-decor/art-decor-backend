const util = require('./util.js');
const path = require('path');

const chai = require('chai');
const expect = chai.expect;

const chaiResponseValidator = require('chai-openapi-response-validator');
const spec = path.resolve("modules/library/api.json");
chai.use(chaiResponseValidator(spec));

describe('Path /api', function () {
    it('GET api version, expect success', async function () {
        const res = await util.axios.get('api');
        //console.log('      Found: ' + res.data.version);
        expect(res.status).to.equal(200);
    });
    it('GET api version, expect semver version', async function () {
        const res = await util.axios.get('api');
        expect(res.data.version).to.match(/\d+\.\d+\.\d+/);
    });
});