const util = require('./util.js');
const path = require('path');

const chai = require('chai');
const expect = chai.expect;

const chaiResponseValidator = require('chai-openapi-response-validator');
const spec = path.resolve("modules/library/api.json");
chai.use(chaiResponseValidator(spec));

let groupId = "2.16.840.1.113883.3.1937.9999999.63.1";
let groupNew = {
  "baseId": "2.16.840.1.113883.3.1937.9999999.63", "defaultLanguage": "en-US", 
  "name": [ { "language": "en-US", "#text": "Demo 9999999" } ],
  "desc": [ { "language": "en-US", "#text": "New governance group" } ],
  "copyright": [ { "years": "2012-", "by": "The ART-DECOR expert group" } ]
};
let groupPatch = {
  "parameter": [
    { "op": "replace", "path": "/defaultLanguage", "value": "de-DE" },
    { "op": "add", "path": "/name", "value": { "name": [ { "language": "de-DE", "#text": "yadadadad" } ] } }
  ]
};
let groupResult = { 
  "id": "2.16.840.1.113883.3.1937.9999999.63.1", "defaultLanguage": "de-DE", 
  "name": [
    { "language": "en-US", "lastTranslated": "2021-02-06T17:22:10", "#text": "Demo 9999999" },
    { "language": "de-DE", "lastTranslated": "2021-02-06T17:22:11", "#text": "yadadadad" }
  ],
  "desc": [
    { "language": "en-US", "lastTranslated": "2021-02-06T17:22:10", "#text": "New governance group" }
  ],
  "copyright": [ { "years": "2012-", "by": "The ART-DECOR expert group" } ]
};

describe('DELETE /server/governancegroup/'+groupId, function () {
    before(util.login);
    
    it('DELETE governance group '+groupId+', expect success', async function () {
        return util.axios.delete('server/governancegroup/'+groupId)
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(204);
        })
        .catch(function(error) {
            error.response == null || error.response.data == null ? console.log(error) : console.log(error.response.data);
        });
    });
});
describe('CREATE /server/governancegroup', function () {
    before(util.login);

    it('POST governance group, expect success', async function () {
        return util.axios.post('server/governancegroup', groupNew)
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(201);
        })
        .catch(function(error) {
            error.response == null || error.response.data == null ? console.log(error) : console.log(error.response.data);
        });
    });
});
describe('PATCH /server/governancegroup/'+groupId, function () {
    before(util.login);

    it('PATCH governance group '+groupId+', expect success', async function () {
        return util.axios.patch('server/governancegroup/'+groupId, groupPatch)
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(200);
        })
        .catch(function(error) {
            error.response == null || error.response.data == null ? console.log(error) : console.log(error.response.data);
        });
    });
});
describe('CHECK /server/governancegroup', function () {
    before(util.login);

    it('GET governance group '+groupId+', expect success', async function () {
        return util.axios.get('server/governancegroup/'+groupId)
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(200);
            expect(res.data.defaultLanguage).to.equal(groupResult.defaultLanguage);
        })
        .catch(function(error) {
            error.response == null || error.response == null || error.response.data == null ? console.log(error) : console.log(error.response.data);
        });
    });
});
describe('DELETE /server/governancegroup/'+groupId, function () {
    before(util.login);
    
    it('DELETE governance group '+groupId+', expect success', async function () {
        return util.axios.delete('server/governancegroup/'+groupId)
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(204);
        })
        .catch(function(error) {
            error.response == null || error.response.data == null ? console.log(error) : console.log(error.response.data);
        });
    });
});