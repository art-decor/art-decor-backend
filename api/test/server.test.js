const util = require('./util.js');
const path = require('path');

const chai = require('chai');
const expect = chai.expect;

const chaiResponseValidator = require('chai-openapi-response-validator');
const spec = path.resolve("modules/library/api.json");
chai.use(chaiResponseValidator(spec));

describe('Path /server', function () {
    it('GET server info, expect success', async function () {
        const res = await util.axios.get('server');
        //console.log(res.data);
        expect(res.status).to.equal(200);
    });
    it('GET server decor types, expect success and one or more types', async function () {
        const res = await util.axios.get('server/$decortypes');
        //console.log(res.data);
        expect(res.status).to.equal(200);
        expect(res.data.artifact).to.equal('TYPES');
        expect(res.data.current).to.not.equal('0');
    });
    it('GET server decor type ReleaseStatusCodeLifeCycle, expect success', async function () {
        const res = await util.axios.get('server/$decortypes/ReleaseStatusCodeLifeCycle');
        //console.log(res.data);
        expect(res.status).to.equal(200);
        expect(res.data.enumeration).to.be.an('array');
    });
    it('GET server decor type CannotBeFound, expect success', async function () {
        return util.axios.get('server/$decortypes/CannotBeFound')
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(404);
        })
        .catch(function(error) {
            //console.log(error.response.data);
        });
    });
});