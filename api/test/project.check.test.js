const util = require('./util.js');
const path = require('path');

const chai = require('chai');
const expect = chai.expect;

const chaiResponseValidator = require('chai-openapi-response-validator');
const spec = path.resolve("modules/library/api.json");
chai.use(chaiResponseValidator(spec));

let projectPrefix = 'demo1-';
describe('POST /project/'+projectPrefix+'/$check', function () {
    before(util.login);
    
    it('POST project '+projectPrefix+'/$check without parameters, expect success', async function () {
        return util.axios.post('project/'+projectPrefix+'/$check', {})
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(200);
        })
        .catch(function(error) {
            error.response == null || error.response.data == null ? console.log(error) : console.log(error.response.data);
        });
    });
    
    it('POST project '+projectPrefix+'/$check with parameter compile=true, expect success', async function () {
        return util.axios.post('project/'+projectPrefix+'/$check', { "compile": true })
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(200);
        })
        .catch(function(error) {
            error.response == null || error.response.data == null ? console.log(error) : console.log(error.response.data);
        });
    });
});