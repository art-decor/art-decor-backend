Install npm:
    https://www.npmjs.com/get-npm

Check:
    User name and password in ../.existdb.json/#servers. If this file missing somehow, see below

Run once:
    npm install

Run tests:
    npm test
    
Contents of default .existdb.json:

{
    "servers": {
        "localhost": {
            "server": "http://localhost:8877/exist",
            "user": "admin",
            "password": "",
            "root": "/db/apps/api"
        }
    },
    "sync": {
        "server": "localhost",
        "ignore": [
            ".existdb.json",
            ".git/**",
            "node_modules/**",
            "bower_components/**",
            "package*.json",
            ".vscode/**",
            ".tmpl"
        ]
    },
    "package": {
        "author": "ART-DECOR Expert Group",
        "target": "api",
        "description": "The ART-DECOR API contains RESTful services for DECOR and Terminology objects",
        "namespace": "http://art-decor.org/ns/api",
        "website": "https://art-decor.org",
        "status": "stable",
        "title": "ART-DECOR API",
        "license": "LGPL-3.0-or-later"
    }
}
