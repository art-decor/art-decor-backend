const util = require('./util.js');
const path = require('path');

const chai = require('chai');
const expect = chai.expect;

const chaiResponseValidator = require('chai-openapi-response-validator');
const spec = path.resolve("modules/library/api.json");
chai.use(chaiResponseValidator(spec));

let projectPrefix = 'demo1-';
describe('Path /scenario/actor from project '+projectPrefix, function () {
    let response;
    let actorId;
    
    before(async function () {
        return util.axios.get('scenario/actor?prefix='+projectPrefix)
            .then (function(res) {
                response = res;
                actorId = res.data.actor[0].id;
                //console.log('      Found actor id: '+actorId);
            })
            .catch(function(error) {
                //console.log(error.response);
            });
        }
    );

    it('Expect successful retrieval status of the actor list', function () {
        expect(response.status).to.equal(200);
    });
    it('Expect list to have artifact value AC', function () {
        expect(response.data.artifact).to.equal('AC');
    });
    it('Expect list to have contents', function () {
        expect(response.data.actor).to.be.an('array');
    });
    //https://github.com/openapi-library/OpenAPIValidators/issues/216
    /*it('Expect the response to conform to the api specification', function () {
        expect(response).to.satisfyApiSpec;
    });*/

    describe('Path /scenario/actor/{id}', function () {
        it('GET scenario actor by id, expect success', async function () {
            return util.axios.get('scenario/actor/'+actorId)
            .then (function(res) {
                //console.log(res.data);
                expect(res.status).to.equal(200);
            })
            .catch(function(error) {
                //console.log(error.response);
            });
        });
    });
});
