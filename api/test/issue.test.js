const util = require('./util.js');
const path = require('path');

const chai = require('chai');
const expect = chai.expect;

const chaiResponseValidator = require('chai-openapi-response-validator');
const spec = path.resolve("modules/library/api.json");
chai.use(chaiResponseValidator(spec));

let projectPrefix = 'demo1-';
describe('Path /issue from project '+projectPrefix, function () {
    let response;
    let issueId;
    
    before(async function () {
        return util.axios.get('issue?prefix='+projectPrefix)
            .then (function(res) {
                response = res;
                issueId = res.data.issue[0].id;
                //console.log('      Found issue id: '+issueId);
            })
            .catch(function(error) {
                console.log(error.response);
            });
        }
    );

    it('Expect successful retrieval status of the issue list', function () {
        expect(response.status).to.equal(200);
    });
    it('Expect list artifact value', function () {
        expect(response.data.artifact).to.equal('IS');
    });
    it('Expect list to have contents', function () {
        expect(response.data.issue).to.be.an('array');
    });
    //https://github.com/openapi-library/OpenAPIValidators/issues/216
    /*it('Expect the response to conform to the api specification', function () {
        expect(response).to.satisfyApiSpec;
    });*/
    
    it('GET issue by id, expect success', async function () {
        return util.axios.get('issue/'+issueId)
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(200);
        })
        .catch(function(error) {
            error.response == null || error.response.data == null ? console.log(error) : console.log(error.response.data);
        });
    });
});