const util = require('./util.js');
const path = require('path');

const chai = require('chai');
const expect = chai.expect;

const chaiResponseValidator = require('chai-openapi-response-validator');
const spec = path.resolve("modules/library/api.json");
chai.use(chaiResponseValidator(spec));

describe('Path /project', function () {
    let projectPrefix = 'demo1-';
    
    it('GET project '+projectPrefix+', expect success', async function () {
        const res = await util.axios.get('project/'+projectPrefix);
        //console.log(res.data);
        expect(res.status).to.equal(200);
    });
    it('GET project '+projectPrefix+' with adram status, expect success', async function () {
        const res = await util.axios.get('project/'+projectPrefix+'?checkadram=true');
        //console.log(res.data);
        expect(res.status).to.equal(200);
    });
    
    describe('Path /project '+projectPrefix+' releases', function () {
        let response;
        let releaseDate;
        
        before(async function () {
            return util.axios.get('project/'+projectPrefix+'/$publication')
                .then (function(res) {
                    response = res;
                    releaseDate = res.data.version[0].date
                    //console.log('      Found release/version date: '+releaseDate);
                    //console.log('      '+res.data);
                })
                .catch(function(error) {
                    console.log(error.response);
                });
            }
        );
    
        it('Expect successful retrieval status of the releases/versions', function () {
            expect(response.status).to.equal(200);
        });
        it('Expect to have contents', function () {
            expect(releaseDate).to.be.an('string');
        });
        
        describe('Path /project '+projectPrefix+' release or version with date '+releaseDate, function () {
            it('Expect success', async function () {
                return util.axios.get('project/'+projectPrefix+'/$publication/?effectiveDate='+releaseDate)
                .then (function(res) {
                    //console.log(res.data);
                    expect(res.status).to.equal(200);
                })
                .catch(function(error) {
                    //console.log(error.response.data);
                });
            });
        });
    });
    
    it('GET project demo9999-, expect 404', async function () {
        return util.axios.get('project/demo9999-')
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(404);
        })
        .catch(function(error) {
            //console.log(error.response.data);
        });
    });
    it('GET project demo9999- releases, expect 404', async function () {
        return util.axios.get('project/demo9999-/$publication')
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(404);
        })
        .catch(function(error) {
            //console.log(error.response.data);
        });
    });
    it('GET project '+projectPrefix+' history, expect success', async function () {
        return util.axios.get('project/'+projectPrefix+'/history')
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(200);
        })
        .catch(function(error) {
            //console.log(error.response.data);
        });
    });
    it('GET project demo9999- history, expect 404', async function () {
        return util.axios.get('project/demo9999-/history')
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(404);
        })
        .catch(function(error) {
            //console.log(error.response.data);
        });
    });
});