const chai = require('chai');
const expect = chai.expect;
const axios = require('axios');
// read metadata from .existdb.json
const existJSON = require('../.existdb.json')
let serverInfo = existJSON.servers.localhost

const { origin } = new URL(serverInfo.server)

const app = `${origin}/exist/apps/api`;

const axiosInstance = axios.create({
    baseURL: app,
    headers: {
        "Origin": origin
    },
    withCredentials: true
});

async function login() {
    /*console.log('Logging in user ...');*/
    
    const res = await axiosInstance.post( 'token', 
        {
            "username": serverInfo.user,
            "password": serverInfo.password
        }
    );
    expect(res.status).to.equal(201);
    expect(res.data.user.name).to.equal(serverInfo.user);

    axiosInstance.defaults.headers['X-Auth-Token'] = res.data.token;
    /*console.log('Logged in as %s: %s', res.data.user.name, res.statusText);*/
};

function logout(done) {
    // console.log('Logging out ...');
    axiosInstance.request({
        url: 'login',
        method: 'post',
        params: {
            "logout": "true"
        }
    })
    .catch((error) => {
        expect(error.response.status).to.equal(401);
        done();
    });
};

module.exports = {axios: axiosInstance, login, logout};
