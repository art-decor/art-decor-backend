const util = require('./util.js');
const path = require('path');

const chai = require('chai');
const expect = chai.expect;

const chaiResponseValidator = require('chai-openapi-response-validator');
const spec = path.resolve("modules/library/api.json");
chai.use(chaiResponseValidator(spec));

describe('Path /server/group without authentication', function () {
    it('GET group list expect 403', function () {
        return util.axios.get('server/group')
        .catch(function(error) {
            //console.log(error.response.data);
            expect(error.response.status).to.equal(403);
        });
    });
});
describe('Path /server/group with authentication', function () {
    before(util.login);
    it('GET group list expect success and a list of type GROUP', async function () {
        const res = await util.axios.get('server/group');
        //console.log(res.data);
        expect(res.status).to.equal(200);
        expect(res.data.artifact).to.equal('GROUP');
    });
});
describe('Path /server/group/{name} without authentication', function () {
    it('GET group decor expect 403', function () {
        return util.axios.get('server/group/decor')
        .catch(function(error) {
            console.log(error.response.data);
            expect(error.response.status).to.equal(403);
        });
    });
});
describe('Path /server/group/{name} with authentication', function () {
    before(util.login);
    it('GET group decor expect success', async function () {
        const res = await util.axios.get('server/group/decor');
        //console.log(res.data);
        expect(res.status).to.equal(200);
    });
});