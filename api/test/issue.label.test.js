const util = require('./util.js');
const path = require('path');

const chai = require('chai');
const expect = chai.expect;

const chaiResponseValidator = require('chai-openapi-response-validator');
const spec = path.resolve("modules/library/api.json");
chai.use(chaiResponseValidator(spec));

describe('Path /issue/label', function () {
    before(util.login);
    
    let projectPrefix = 'demo1-'
    let label = {
        "code": "DS",
        "color": "blue",
        "name": "datasets"
    };
    
    it('GET issue label list from '+projectPrefix+', expect success and a list of type ISL', async function () {
        const res = await util.axios.get('issue/label?prefix='+projectPrefix);
        //console.log(res.data);
        expect(res.status).to.equal(200);
        expect(res.data.artifact).to.equal('ISL');
    });
    it('POST issue label '+label.code+' to '+projectPrefix+', expect 201 Created or 403 Forbidden if it already exists', function () {
        return util.axios.post('issue/label?prefix='+projectPrefix, label)
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(201);
            expect(res.data.code).to.equal(label.code);
        })
        .catch(function(error) {
            //console.log(error.response.data);
            expect(error.response.status).to.equal(403);
        });
    });
    it('GET issue label '+label.code+' from '+projectPrefix+', expect success', async function () {
        const res = await util.axios.get('issue/label/'+label.code+'?prefix='+projectPrefix);
        //console.log(res.data);
        expect(res.status).to.equal(200);
        expect(res.data.code).to.equal(label.code);
    });
    it('DELETE issue label '+label.code+' from '+projectPrefix+', expect 204', function () {
        return util.axios.delete('issue/label/'+label.code+'?prefix='+projectPrefix)
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(204);
        })
        .catch(function(error) {
            //console.log(error.response.data);
            expect(error.response.status).to.equal(403);
        });
    });
    it('POST issue label '+label.code+' to '+projectPrefix+', expect 201', function () {
        return util.axios.post('issue/label?prefix='+projectPrefix, label)
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(201);
            expect(res.data.code).to.equal(label.code);
        })
        .catch(function(error) {
            console.log(error.response.data);
        });
    });
});