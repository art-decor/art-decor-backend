const util = require('./util.js');
const path = require('path');

const chai = require('chai');
const expect = chai.expect;

const chaiResponseValidator = require('chai-openapi-response-validator');
const spec = path.resolve("modules/library/api.json");
chai.use(chaiResponseValidator(spec));

let ggid = '2.16.840.1.113883.3.1937';
describe('Path /server/governancegroup', function () {
    it('GET governance group list, expect success', async function () {
        const res = await util.axios.get('server/governancegroup');
        //console.log(res.data);
        expect(res.status).to.equal(200);
        expect(res.data.artifact).to.equal('GOVERNANCEGROUP');
        expect(res.data.current).to.not.equal('0');
    });
    it('GET governance group ART-DECOR, expect success', async function () {
        const res = await util.axios.get('server/governancegroup/'+ggid);
        //console.log(res.data);
    });
    it('GET governance group 1, expect not found', async function () {
        return util.axios.get('server/governancegroup/1')
        .then (function(res) {
            //console.log(res.data);
            expect(res.status).to.equal(404);
        })
        .catch(function(error) {
            //console.log(error.response.data);
        });
    });
});