const util = require('./util.js');
const path = require('path');

const chai = require('chai');
const expect = chai.expect;

const chaiResponseValidator = require('chai-openapi-response-validator');
const spec = path.resolve("modules/library/api.json");
chai.use(chaiResponseValidator(spec));

let projectPrefix = 'demo1-';
describe('Path /scenario from project '+projectPrefix, function () {
    let response;
    let scenarioId;
    let scenarioEffectiveDate;
    
    before(async function () {
        return util.axios.get('scenario?prefix='+projectPrefix)
            .then (function(res) {
                response = res;
                scenarioId = res.data.scenario[0].id;
                scenarioEffectiveDate = res.data.scenario[0].effectiveDate;
                //console.log('      Found scenario id: '+scenarioId+' effectiveDate: '+scenarioEffectiveDate);
            })
            .catch(function(error) {
                console.log(error.response);
            });
        }
    );

    it('Expect successful retrieval status of the scenario list', function () {
        expect(response.status).to.equal(200);
    });
    it('Expect list artifact value', function () {
        expect(response.data.artifact).to.equal('SC');
    });
    it('Expect list to have contents', function () {
        expect(response.data.scenario).to.be.an('array');
    });
    //https://github.com/openapi-library/OpenAPIValidators/issues/216
    /*it('Expect the response to conform to the api specification', function () {
        expect(response).to.satisfyApiSpec;
    });*/

    describe('Path /scenario/{id}', function () {
        it('GET scenario by id, expect success', async function () {
            return util.axios.get('scenario/'+scenarioId)
            .then (function(res) {
                //console.log(res.data);
                expect(res.status).to.equal(200);
            })
            .catch(function(error) {
                //console.log(error.response);
            });
        });
    });
    describe('Path /scenario/{id}/{effectiveDate}', function () {
        it('GET scenario by id and effectiveDate, expect success', async function () {
            return util.axios.get('scenario/'+scenarioId+'/'+encodeURIComponent(scenarioEffectiveDate))
            .then (function(res) {
                //console.log(res.data);
                expect(res.status).to.equal(200);
            })
            .catch(function(error) {
                //console.log(error.response);
            });
        });
    });
});
