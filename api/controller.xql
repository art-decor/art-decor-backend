xquery version "3.1";

declare variable $exist:controller external;
declare variable $exist:path external;
declare variable $exist:resource external;
declare variable $exist:root external;

(:~
 : A list of regular expressions to check which external hosts are
 : allowed to access this ART-DECOR API instance. The check is done
 : against the Origin header sent by the browser.
 :)
declare variable $origin-whitelist  := ("(?:https?://localhost:.*|https?://127.0.0.1:.*)");
declare variable $allowOrigin       := local:allowOriginDynamic(request:get-header("Origin"));

declare function local:allowOriginDynamic($origin as xs:string?) {
    let $origin := replace($origin, "^(\w+://[^/]+).*$", "$1")
    return
        if (local:checkOriginWhitelist($origin-whitelist, $origin)) then
            $origin
        else
            "*"
};

declare function local:checkOriginWhitelist($regexes, $origin) {
    if (empty($regexes)) then
        false()
    else if (matches($origin, head($regexes))) then
        true()
    else
        local:checkOriginWhitelist(tail($regexes), $origin)
};

let $method       := lower-case(request:get-method())

(: some paths are actually handled by a higher up library :)
let $pathstart    := replace($exist:path, '^/([^/]+).*', '$1')
let $pathstart    :=
    switch ($pathstart)
    case 'dataset'               return (
        if (contains($exist:path, '/$concept')) then 'concept' else
        $pathstart
    )
    case 'project'               return if (ends-with($exist:path, '$add-template-ids')) then 'template' else $pathstart
    case 'server'                return if (starts-with($exist:path, '/server/governancegroup')) then 'governancegroup' else $pathstart
    case 'transaction'           return 'scenario'
    default                      return $pathstart

return
if ($exist:path eq "/" or $exist:path eq "/api.html") then
    (: forward root path to index.xql :)
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="api.html"/>
    </dispatch>
else
(: static HTML page for API documentation should be served directly to make sure it is always accessible :)
if ($exist:path eq "/api.html" or ends-with($exist:resource, ".json")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist"/>

(: other images are resolved against the data collection and also returned directly :)
else 
if (not(starts-with($exist:path, '/project')) and matches($exist:resource, "\.(png|jpg|jpeg|gif|tif|tiff|txt|mei)$", "s")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/data/{$exist:path}">
            <set-header name="Cache-Control" value="max-age=31536000"/>
        </forward>
    </dispatch>
else
(: special paths go to special places. api-admin.xql is configured to operate under dba privileges :)
(: Author list requires access to sm:* functions :)
if (matches($exist:path, '^/project/[^/]+/author$') or
    starts-with($exist:path, '/server/user') or 
    starts-with($exist:path, '/server/group') or 
    (starts-with($exist:path, '/server/governancegroup') and $method != 'get') or
    (starts-with($exist:path, '/server/settings') and $method != 'get') or 
    starts-with($exist:path, '/server-mgmt') or 
    starts-with($exist:path, '/news')) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/modules/library/api-admin.xql">
            <set-header name="Access-Control-Allow-Origin" value="{$allowOrigin}"/>
            { if ($allowOrigin = "*") then () else <set-header name="Access-Control-Allow-Credentials" value="true"/> }
            <set-header name="Access-Control-Allow-Methods" value="GET, POST, DELETE, PUT, PATCH, OPTIONS"/>
            <set-header name="Access-Control-Allow-Headers" value="Content-Type, api_key, Authorization"/>
            <set-header name="Access-Control-Expose-Headers" value="pb-start, pb-total"/>
            <set-header name="Cache-Control" value="no-cache"/>
        </forward>
    </dispatch>
(: all other things are sent off to regular user stuff handled by api.xql :)
else (
    (: everything else is passed through :)
    (:<dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <cache-control cache="yes"/>
    </dispatch>:)
    switch (replace($exist:path, '^/([^/]+).*', '$1'))
    case 'api'
    case 'codesystem'
    case 'concept'
    case 'conceptmap'
    case 'dataset'
    case 'implementationguide'
    case 'issue'
    case 'lock'
    case 'mycommunity'
    case 'news'
    case 'oidregistry'
    case 'profile'
    case 'project'
    case 'questionnaire'
    case 'server'
    case 'scenario'
    case 'transaction'
    case 'template'
    case 'terminology'
    case 'token'
    case 'valueset' return (
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/modules/library/api-{$pathstart}.xql">
                <set-header name="Access-Control-Allow-Origin" value="{$allowOrigin}"/>
                { if ($allowOrigin = "*") then () else <set-header name="Access-Control-Allow-Credentials" value="true"/> }
                <set-header name="Access-Control-Allow-Methods" value="GET, POST, DELETE, PUT, PATCH, OPTIONS"/>
                <set-header name="Access-Control-Allow-Headers" value="Content-Type, api_key, Authorization"/>
                <set-header name="Access-Control-Expose-Headers" value="pb-start, pb-total"/>
                <set-header name="Cache-Control" value="no-cache"/>
            </forward>
        </dispatch>
    )
    case 'questionnaireresponse' return (response:set-status-code(404), <result>Please refer to appropriate FHIR server for QuestionnaireResponse</result>)
    default return (
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/modules/library/api.xql">
                <set-header name="Access-Control-Allow-Origin" value="{$allowOrigin}"/>
                { if ($allowOrigin = "*") then () else <set-header name="Access-Control-Allow-Credentials" value="true"/> }
                <set-header name="Access-Control-Allow-Methods" value="GET, POST, DELETE, PUT, PATCH, OPTIONS"/>
                <set-header name="Access-Control-Allow-Headers" value="Content-Type, api_key, Authorization"/>
                <set-header name="Access-Control-Expose-Headers" value="pb-start, pb-total"/>
                <set-header name="Cache-Control" value="no-cache"/>
            </forward>
        </dispatch>
    )
)