xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:~ Value set API allows read, create, update of DECOR codeSystems :)
module namespace csapi              = "http://art-decor.org/ns/api/codesystem";

import module namespace roaster     = "http://e-editiones.org/roaster";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

import module namespace utillib     = "http://art-decor.org/ns/api/util" at "library/util-lib.xqm";
import module namespace utilhtml    = "http://art-decor.org/ns/api/util-html" at "library/util-html-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "library/settings-lib.xqm";
import module namespace serverapi   = "http://art-decor.org/ns/api/server" at "server-api.xqm";
import module namespace decorlib    = "http://art-decor.org/ns/api/decor" at "library/decor-lib.xqm";
import module namespace ggapi       = "http://art-decor.org/ns/api/governancegroups" at "governancegroups-api.xqm";
import module namespace histlib     = "http://art-decor.org/ns/api/history" at "library/history-lib.xqm";

import module namespace adterm      = "http://art-decor.org/ns/terminology" at "../../terminology/api/api-terminology.xqm";

declare namespace json              = "http://www.json.org";
declare namespace http              = "http://expath.org/ns/http-client";
declare namespace output            = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace sch               = "http://purl.oclc.org/dsdl/schematron";
declare namespace sm                = "http://exist-db.org/xquery/securitymanager";
declare namespace svrl              = "http://purl.oclc.org/dsdl/svrl";
declare namespace xs                = "http://www.w3.org/2001/XMLSchema";
declare namespace hl7               = "urn:hl7-org:v3";
declare namespace xsi               = "http://www.w3.org/2001/XMLSchema-instance";

(:~ All functions support their own override, but this is the fallback for the maximum number of results returned on a search :)
declare %private variable $csapi:maxResults             := 50;
declare %private variable $csapi:PROPERTYDEF-TYPE       := utillib:getDecorTypes()/CodeSystemPropertyDefinitionType;
declare %private variable $csapi:CODEDCONCEPT-STATUS    := utillib:getDecorTypes()/CodedConceptStatusCodeLifeCycle;
declare %private variable $csapi:ADDRLINE-TYPE          := utillib:getDecorTypes()/AddressLineType;
declare %private variable $csapi:DECOR2CADTS-XSL        := $setlib:strTerminology || '/resources/stylesheets/DECOR-codeSystem-to-CADTS-codeSystem.xsl';

declare %private variable $csapi:PROGRESS-CSCADTS-10    := 'Starting ...';
declare %private variable $csapi:PROGRESS-CSCADTS-20    := 'Working on ';

(:~ Retrieves all DECOR codeSystem for publication based on project $id (oid) or prefix
    @param $project                 - required. limits scope to this project only
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @param $language                - optional parameter 
    @param $format                  - optional. overrides the accept-header for frontend purposes
    @return as-is or as compiled as JSON
    @since 2024-11-14
:)
declare function csapi:getCodeSystemExtractList($request as map(*)) {
    
    let $project                        := $request?parameters?project[not(. = '')]
    let $projectVersion                 := $request?parameters?release[not(. = '')]
    let $projectLanguage                := $request?parameters?language[not(. = '')]
    let $format                         := $request?parameters?format[not(. = '')]
    
    (: parameter format overrides accept-header as mime-type for using the api in the browser :)
    
    (: 
        this call is used in backend and frontend (browser) - default behaviour mime-types:
        - backend:   default mime-type is json - response payload is json - if no accept type is given and format is not csv or sql  
        - frontend:  default mime-type is xml - response payload is xml - format is not csv or sql 
    :)
    
    let $acceptTypes                    := roaster:accepted-content-types()
    let $acceptedType                   := ($acceptTypes[. = ('application/xml', 'application/json', 'text/csv', 'text/sql')],'application/json')[1]
    
    let $format                         :=
        if ($format) then $format
        else tokenize($acceptedType, '/')[2]

    (: 
       overwrite format in the backend is not always possible because of middleware behaviour
       - if in backend the format is xml and accept is not application/xml, roaster always gives a json payload 
       - if in backend the format is not json and accept is application/json, roaster always gives a json payload
    :) 
    let $check                          :=
        if ($format = 'xml' and not($acceptedType = 'application/xml')) then 
            error($errors:BAD_REQUEST, 'In case of format parameter is xml the accept header should be application/xml')
        else if (not($format = 'json') and $acceptedType = 'application/json') then
            error($errors:BAD_REQUEST, 'In case of format parameter is ' || $format || ' the accept header can be anything but application/json')
        else ()   

    let $check                          :=
        if (empty($project)) then 
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    
    let $projectPrefix                  := utillib:getDecor($project, (), ())/project/@prefix
   
    let $check                  :=
        if (empty($projectPrefix)) then 
            error($errors:BAD_REQUEST, 'Project ' || $project || ' not found.')
        else ()

    (: Return zero or more expanded codesystems wrapped in a &lt;return/&gt; element, and subsequently inside a &lt;repository&gt; element. :)
    
    let $codeSystems                    := 
        for $codeSystem in csapi:getCodeSystemList((),$projectPrefix, $projectVersion, $projectLanguage, (), (), false(), (), (), (), false())/codeSystem/codeSystem
        return  
            csapi:getCodeSystemExtract($projectPrefix, $projectVersion, $projectLanguage, $codeSystem/(@id|@ref), $codeSystem/@effectiveDate, false())

    (: prepare response payload :)
    let $results                        :=
         if (empty($codeSystems/*)) then () 
         else if($format = ('xml', 'json')) then (
            let $xmljson :=
                <codeSystems>
                {
                    $codeSystems/*
                }
                </codeSystems>
            let $xmljson := 
            for $x in $xmljson
                return
                element {name($x)} {
                    $x/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($x/node(), $format)
                }
                                       
             (: in this case the json format overrides header application/xml and works with a text header :)
             let $xmljson               :=
                if ($format = 'json' and not($acceptedType = 'application/json')) then
                    fn:serialize($xmljson, map{"method": $format , "indent": true()})
                    else $xmljson
             
             return $xmljson
            )
             
         else if ($format = 'csv') then csapi:convertCodeSystem2Csv($codeSystems, $projectLanguage)
         else if ($format = 'sql') then transform:transform($codeSystems, doc($setlib:strDecorServices ||'/resources/stylesheets/ToSql4ValueSets.xsl'), ()) 
         else error($errors:BAD_REQUEST, 'Requested mime-type ' || $format || ' is not supported')

    (: prepare response header content-type and content-disposition :)
    let $contentType                    :=
        if ($format = ('csv', 'sql')) then 'text/' || $format
        else 'application/' || $format
            
    let $codeSystemNames                := distinct-values($codeSystems//codeSystem/@name)
    let $fileNamePart                   := if (count($codeSystemNames)>1) then 'project_' || $projectPrefix || '('|| count($codeSystems//codeSystem) || ')' else $codeSystemNames
    let $fileName                       := 'CS_' || $fileNamePart ||'_(download_' || substring(fn:string(current-dateTime()),1,19) || ').' || $format
                
    let $r-header                       := 
        (response:set-header('Content-Type', $contentType || '; charset=utf-8'),
        response:set-header('Content-Disposition', 'filename='|| $fileName))

    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else $results
};



(:~ Retrieves latest DECOR codeSystem for publication based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the codeSystem
    @param $projectPrefix           - optional. limits scope to this project only
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @param $language                - optional
    @param $format                  - optional. overrides the accept-header for frontend purposes
    @return as-is or as compiled as JSON
    @since 2023-11-14
:)
declare function csapi:getLatestCodeSystemExtract($request as map(*)) {
    csapi:getCodeSystemExtract($request)
};

(:~ Retrieves DECOR codeSystem for publication based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the codeSystem
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the codeSystem. If not given assumes latest version for id
    @param $project                 - optional. limits scope to this project only
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @param $language                - optional parameter 
    @param $wrap                    - optional parameter to return a the codesystems wrapped in project - xml only
    @param $format                  - optional. overrides the accept-header for frontend purposes
    @return as-is or as compiled as JSON
    @since 2023-11-17
:)
declare function csapi:getCodeSystemExtract($request as map(*)) {
    
    let $project                        := $request?parameters?project[not(. = '')]
    let $projectVersion                 := $request?parameters?release[not(. = '')]
    let $projectLanguage                := $request?parameters?language[not(. = '')]
    let $withversions                   := $request?parameters?versions = true()
    let $format                         := $request?parameters?format[not(. = '')]
    
    let $idOrName                       := $request?parameters?idOrName[not(. = '')]
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }

    (: parameter format overrides accept-header as mime-type for using the api in the browser :)
    
    (: 
        this call is used in backend and frontend (browser) - default behaviour mime-types:
        - backend:   default mime-type is json - response payload is json - if no accept type is given and format is not csv or sql  
        - frontend:  default mime-type is xml - response payload is xml - format is not csv or sql 
    :)
    
    let $acceptTypes                    := roaster:accepted-content-types()
    let $acceptedType                   := ($acceptTypes[. = ('application/xml', 'application/json', 'text/csv', 'text/sql')],'application/json')[1]
    
    let $format                         :=
        if ($format) then $format
        else tokenize($acceptedType, '/')[2]

    (: 
       overwrite format in the backend is not always possible because of middleware behaviour
       - if in backend the format is xml and accept is not application/xml, roaster always gives a json payload 
       - if in backend the format is not json and accept is application/json, roaster always gives a json payload
    :) 
    let $check                          :=
        if ($format = 'xml' and not($acceptedType = 'application/xml')) then 
            error($errors:BAD_REQUEST, 'In case of format parameter is xml the accept header should be application/xml')
        else if (not($format = 'json') and $acceptedType = 'application/json') then
            error($errors:BAD_REQUEST, 'In case of format parameter is ' || $format || ' the accept header can be anything but application/json')
        else ()   

    let $projectPrefix                  := 
        if (empty($project)) then () else utillib:getDecor($project, $projectVersion, $projectLanguage)/project/@prefix            
    
    (: Return zero or more expanded codesystems wrapped in a &lt;return/&gt; element, and subsequently inside a &lt;repository&gt; element. :)
    let $codeSystems                    := if (empty($projectPrefix)) then csapi:getCodeSystemExtract($idOrName, $effectiveDate) else csapi:getCodeSystemExtract($projectPrefix, $projectVersion, $projectLanguage, $idOrName, $effectiveDate, $withversions)

    (: prepare response payload :)
    let $results                        :=
         if (empty($codeSystems/*)) then () 
         else if($format = ('xml', 'json')) then (
            let $xmljson :=
                <codeSystems>
                {
                    $codeSystems/*
                }
                </codeSystems>
            let $xmljson := 
            for $x in $xmljson
                return
                element {name($x)} {
                    $x/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($x/node(), $format)
                }
                                       
             (: in this case the json format overrides header application/xml and works with a text header :)
             let $xmljson               :=
                if ($format = 'json' and not($acceptedType = 'application/json')) then
                    fn:serialize($xmljson, map{"method": $format , "indent": true()})
                    else $xmljson
             
             return $xmljson
            )
             
         else if ($format = 'csv') then csapi:convertCodeSystem2Csv($codeSystems, $projectLanguage)
         else if ($format = 'sql') then transform:transform($codeSystems, doc($setlib:strDecorServices ||'/resources/stylesheets/ToSql4ValueSets.xsl'), ()) 
         else error($errors:BAD_REQUEST, 'Requested mime-type ' || $format || ' is not supported')

    (: prepare response header content-type and content-disposition :)
    let $contentType                    :=
        if ($format = ('csv', 'sql')) then 'text/' || $format
        else 'application/' || $format
            
    let $fileName                       := 'CS_' || fn:distinct-values($codeSystems//codeSystem/@name) ||'_(download_' || substring(fn:string(current-dateTime()),1,19) || ').' || $format
                
    let $r-header                       := 
        (response:set-header('Content-Type', $contentType || '; charset=utf-8'),
        response:set-header('Content-Disposition', 'filename='|| $fileName))

    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else $results
};

(:~ Retrieves DECOR codeSystem view based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the codeSystem
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the codeSystem. If not given assumes latest version for id
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @param $projectLanguage         - optional parameter to select from a specific compiled language, also ui-language
    @param $inline                  - optional parameter to omit HTML header info. Useful for inclusion of HTML in other pages.
    @param $collapsable             - optional parameter the valueset is collapable.
    @return as-is 
    @since 2024-10-14
:)

declare function csapi:getCodeSystemView($request as map(*)) {

    let $project                        := $request?parameters?project[not(. = '')]
    let $projectVersion                 := $request?parameters?release[not(. = '')]
    let $projectLanguage                := $request?parameters?language[not(. = '')]
    let $inline                         := $request?parameters?inline = true()
    let $collapsable                    := not($request?parameters?collapsable = false())
    
    let $idOrName                       := $request?parameters?idOrName[not(. = '')]
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    
    let $decor                          := if (empty($project)) then () else utillib:getDecor($project, $projectVersion, $projectLanguage)
            
    (: Return zero or more expanded codesystems wrapped in a &lt;return/&gt; element, and subsequently inside a &lt;repository&gt; element :)
    let $results                        := if (empty($decor/project/@prefix)) then csapi:getCodeSystemExtract($idOrName, $effectiveDate) else csapi:getCodeSystemExtract($decor/project/@prefix, $projectVersion, $projectLanguage, $idOrName, $effectiveDate, false())

    let $fileName                       := 'CS_' || fn:distinct-values($results//codeSystem/@name) ||'_(download_' || substring(fn:string(current-dateTime()),1,19) || ').html'
            
    let $r-header                       := 
        (response:set-header('Content-Type', 'text/html; charset=utf-8'),
        response:set-header('Content-Disposition', 'filename='|| $fileName))
    
    return
        if (empty($results/*)) then (
            roaster:response(404, ())
        )

        else 
        
        (: prepare for Html :)
        let $language                       := if (empty($projectLanguage)) then $setlib:strArtLanguage else $projectLanguage
        let $header                         := if ($inline) then false() else true()        
        
        return utilhtml:convertObject2Html($results, $language, $header, $collapsable, $projectVersion, $decor)
       

};

(:~ Retrieves latest DECOR codeSystem based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the codeSystem
    @param $projectPrefix           - optional. limits scope to this project only
    @param $language                - optional
    @return as-is or as compiled as JSON
    @since 2020-05-03
:)

declare function csapi:getLatestCodeSystem($request as map(*)) {
    csapi:getCodeSystem($request)
};

(:~ Retrieves DECOR codeSystem based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the codeSystem
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the codeSystem. If not given assumes latest version for id
    @param $projectPrefix           - optional. limits scope to this project only
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @return as-is or as compiled as JSON
    @since 2020-05-03
:)
declare function csapi:getCodeSystem($request as map(*)) {
    
    let $projectPrefix                  := $request?parameters?prefix[string-length() gt 0]
    let $projectVersion                 := $request?parameters?release[string-length() gt 0]
    let $projectLanguage                := $request?parameters?language[string-length() gt 0]
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $withversions                   := $request?parameters?versions = true()
    
    let $results                        := csapi:getCodeSystem($projectPrefix, $projectVersion, $projectLanguage, $id, $effectiveDate, $withversions)
    let $results                        := 
        if ($withversions) then 
            <list artifact="CS" current="{count($results)}" total="{count($results)}" all="{count($results)}" lastModifiedDate="{current-dateTime()}">{$results}</list> 
        else (
            head($results)
        )
    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple codeSystems for id '", $id, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        )
        else (
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
        )
};

(:~ Retrieves DECOR codeSystem usage based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the codeSystem
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the codeSystem. If not given assumes latest version for id
    @param $projectPrefix           - optional. limits scope to this project only
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @return as-is or as compiled as JSON
    @since 2020-05-03
:)
declare function csapi:getCodeSystemUsage($request as map(*)) {
    
    let $projectPrefix                  := $request?parameters?prefix
    let $projectVersion                 := $request?parameters?release
    let $projectLanguage                := $request?parameters?language
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    
    let $codeSystemsAll                 :=
        if (empty($id)) then () else (
            if (empty($projectPrefix)) then
                csapi:getCodeSystemById($id, ())
            else (
                csapi:getCodeSystemById($id, (), $projectPrefix, ())
            )
        )
    let $mostRecent                     := string(max($codeSystemsAll/descendant-or-self::codeSystem/xs:dateTime(@effectiveDate)))
    let $cs                             := 
        if (empty($effectiveDate)) then 
            $codeSystemsAll/descendant-or-self::codeSystem[@effectiveDate = $mostRecent] 
        else (
            $codeSystemsAll/descendant-or-self::codeSystem[@effectiveDate = $effectiveDate]
        )
    let $isMostRecent                   := $mostRecent = $effectiveDate or empty($effectiveDate)
    
    let $valueSetsAll                   := $setlib:colDecorData//terminology/valueSet//*[@codeSystem = $id]
    let $valueSetsAll                   :=
        if ($isMostRecent) then 
            $valueSetsAll[@codeSystemVersion = $effectiveDate] | $valueSetsAll[not(@codeSystemVersion castable as xs:dateTime)]
        else (
            $valueSetsAll[@codeSystemVersion = $effectiveDate]
        )
    
    let $conceptMapsAllSource           := $setlib:colDecorData//conceptMap//source[@codeSystem = $id]
    let $conceptMapsAllSource           :=
        if ($isMostRecent) then 
            $conceptMapsAllSource[@codeSystemVersion = $effectiveDate] | $conceptMapsAllSource[not(@codeSystemVersion castable as xs:dateTime)]
        else (
            $conceptMapsAllSource[@codeSystemVersion = $effectiveDate]
        )
    
    let $conceptMapsAllTarget           := $setlib:colDecorData//conceptMap//target[@codeSystem = $id]
    let $conceptMapsAllTarget           :=
        if ($isMostRecent) then 
            $conceptMapsAllTarget[@codeSystemVersion = $effectiveDate] | $conceptMapsAllTarget[not(@codeSystemVersion castable as xs:dateTime)]
        else (
            $conceptMapsAllTarget[@codeSystemVersion = $effectiveDate]
        )
    
    let $allTemplAssociations           := $setlib:colDecorData//template//vocabulary[@codeSystem = $id]
    let $allTemplAssociations           :=
        if ($isMostRecent) then 
            $allTemplAssociations[@codeSystemVersion = $effectiveDate] | $allTemplAssociations[not(@codeSystemVersion castable as xs:dateTime)] | $setlib:colDecorData//template//attribute[@value = $id]
        else (
            $allTemplAssociations[@codeSystemVersion = $effectiveDate]
        )
    
    let $url                            := $utillib:strDecorServicesURL
    
    let $results                        := (
        for $item in $valueSetsAll
        let $csid := $item/ancestor::valueSet[1]/@id
        let $csed := $item/ancestor::valueSet[1]/@effectiveDate
        group by $csid, $csed
        return
            utillib:doValueSetAssociation($cs, $item[1])
        ,
        for $item in $conceptMapsAllSource | $conceptMapsAllTarget
        let $type := local-name($item)
        let $vsid := $item/ancestor::conceptMap[1]/@id
        let $vsed := $item/ancestor::conceptMap[1]/@effectiveDate
        (: group by type of association and then by valueSet. This 'may' list a 
        single conceptMap twice if valueSet is on source and target :)
        group by $type, $vsid, $vsed
        return
            utillib:doConceptMapAssociation($cs, $item[1], 
                for $l in distinct-values($item[1]/ancestor::decor/project/name/@language) 
                return 
                    <name language="{$l}">{data($item[1]/ancestor::conceptMap/@displayName)}</name>
            )
        ,
        for $item in $allTemplAssociations
        let $tmid := $item/ancestor::template[1]/@id
        let $tmed := $item/ancestor::template[1]/@effectiveDate
        group by $tmid, $tmed
        return
            utillib:doTemplateAssociation($cs, $item[1])
    )
    
    let $count                          := count($results)
    let $max                            := $count
    let $allcnt                         := $count
    
    return
        <list artifact="USAGE" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements(subsequence($results, 1, $max))
        }
        </list>
};

(:~ Retrieves DECOR code system history based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id
    @param $effectiveDate           - optional parameter denoting the effectiveDate. If not given assumes latest version for id
    @return list
    @since 2022-08-16
:)
declare function csapi:getCodeSystemHistory($request as map(*)) {
    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $projectPrefix                  := ()
    let $results                        := histlib:ListHistory($authmap, $decorlib:OBJECTTYPE-CODESYSTEM, (), $id, $effectiveDate, 0)
    
    return
        <list artifact="{$decorlib:OBJECTTYPE-CODESYSTEM}" current="{count($results)}" total="{count($results)}" all="{count($results)}" lastModifiedDate="{current-dateTime()}">
        {
            utillib:addJsonArrayToElements($results)
        }
        </list>
};

(:~ Returns a list of zero or more codeSystems

@param $governanceGroupId - optional. determines search scope. null is full server, id limits scope to this projects under this governance group only. Does not mix with $projectPrefix
@param $projectPrefix    - optional. determines search scope. null is full server, pfx- limits scope to this project only. Does not mix with $governanceGroupId
@param $projectVersion   - optional. if empty defaults to current version. if valued then the codeSystem will come explicitly from that archived project version which is expected to be a compiled version
@param $projectLanguage  - optional. relevant in combination with $projectVersion if there are more than one languages the project is compiled in
@param $id               - optional. Identifier of the codeSystem to retrieve
@param $name             - optional. Name of the codeSystem to retrieve (codeSystem/@name)
@param $flexibility      - optional. null gets all versions, 'dynamic' gets the newest version based on id or name, yyyy-mm-ddThh:mm:ss gets this specific version
@param $treetype         - optional. Default $cs:TREETYPELIMITEDMARKED
@param $doV2             - optional. Boolean. Default false. 
@param $withversions     - optional. Boolean. Default true. If true and $doV2 is false, returns codeSystemList.codeSystem* where every codeSystem has @id|@ref and a descending by 
effectiveDate list of matching codeSystems. If false and $doV2 is false, returns codeSystemList.codeSystem* containing only the latest version of the codeSystem (or ref if no versions 
exist in $projectPrefix)<br/>If true and $doV2 is true, returns codeSystemList.codeSystem* where every codeSystem has @id|@ref and a descending by effectiveDate list of matching 
codeSystems. If false and $doV2 is false, returns codeSystemList.codeSystem* containing only the latest version of the codeSystem (or ref if no versions exist in $projectPrefix)
@return Zero value sets in case no matches are found, one if only one exists or if a specific version was requested, or more if more versions exist and no specific version was requested
@since 2013-06-14
:)
declare function csapi:getCodeSystemList($request as map(*)) {
    let $governanceGroupId      := $request?parameters?governanceGroupId
    let $projectPrefix          := $request?parameters?prefix
    let $projectVersion         := $request?parameters?release
    let $projectLanguage        := $request?parameters?language
    let $max                    := $request?parameters?max
    let $resolve                :=  if (empty($governanceGroupId)) then not($request?parameters?resolve = false()) else $request?parameters?resolve = true()
    let $searchTerms            := 
        array:flatten(
            for $s in ($request?parameters?search, $request?parameters?id, $request?parameters?name)[string-length() gt 0]
            return
                tokenize(normalize-space(lower-case($s)),'\s')
        )
    let $ed                     := $request?parameters?effectiveDate
    let $includebbr             := $request?parameters?includebbr = true()
    let $sort                   := $request?parameters?sort
    let $sortorder              := $request?parameters?sortorder
    
    let $check                  :=
        if (empty($governanceGroupId) and empty($projectPrefix)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have either parameter governanceGroupId or prefix')
        else 
        if (not(empty($governanceGroupId)) and not(empty($projectPrefix))) then 
            error($errors:BAD_REQUEST, 'Request SHALL NOT have both parameter governanceGroupId or prefix, not both')
        else ()
    let $check                  :=
        if (empty($governanceGroupId)) then () else if ($resolve) then
            error($errors:BAD_REQUEST, 'Request SHALL NOT have governance group scope and resolve=true. This is too expensive to support')
        else ()
    
    let $startT                 := util:system-time()
    
    let $result                 :=
        if (empty($governanceGroupId)) then
            csapi:getCodeSystemList($governanceGroupId, $projectPrefix, $projectVersion, $projectLanguage, $searchTerms, $ed, $includebbr, $sort, $sortorder, $max, $resolve)
        else (
            for $projectId in ggapi:getLinkedProjects($governanceGroupId)/@ref
            return
                csapi:getCodeSystemList($governanceGroupId, $projectId, (), (), $searchTerms, $ed, $includebbr, $sort, $sortorder, $max, $resolve)
                
        )
    
    let $durationT              := (util:system-time() - $startT) div xs:dayTimeDuration("PT0.001S")
    
    return
        <list artifact="{$result[1]/@artifact}" elapsed="{$durationT}" current="{sum($result/xs:integer(@current))}" total="{sum($result/xs:integer(@total))}" all="{sum($result/xs:integer(@all))}" resolve="{$resolve}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            if (empty($governanceGroupId)) then attribute project {$projectPrefix} else attribute governanceGroupId {$governanceGroupId},
            utillib:addJsonArrayToElements($result/*)
        }
        </list>
};

(:~ Deletes a codeSystem reference based on $id (oid) and project id or prefix
    @param $id                      - required parameter denoting the id of the codeSystem
    @param $project                 - required. limits scope to this project only
:)
declare function csapi:deleteCodeSystem($request as map(*)) {
    
    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0][not(. = '*')]
    let $id                     := $request?parameters?id[string-length() gt 0]
    
    let $check                  :=
        if (empty($project) or empty($id)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have both parameter project and id')
        else ()
    
    let $decor                  := 
        if (utillib:isOid($project)) then
            utillib:getDecorById($project)
        else (
            utillib:getDecorByPrefix($project)
        )
    
    let $check                  :=
        if (empty($decor)) then
            error($errors:BAD_REQUEST, 'Project ' || $project || ' not found.')
        else ()
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-TERMINOLOGY)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify codeSystems in project ', $project[1], '. You have to be an active author in the project.'))
        )
    
    let $delete                 := update delete $decor/terminology/codeSystem[@ref = $id]
    
    return
        roaster:response(204, ())
};

(: Create a CodeSystem, either empty or based on another CodeSystem

@param $projectPrefix project to create this codesystem in
@param $targetDate If true invokes effectiveDate of the new codesystem and concepts as [DATE]T00:00:00. If false invokes date + time [DATE]T[TIME] 
@param $sourceId parameter denoting the id of a codesystem to use as a basis for creating the new codesystem
@param $sourceEffectiveDate parameter denoting the effectiveDate of a codesystem to use as a basis for creating the new codesystem",
@param $keepIds Only relevant if source codesystem is specified. If true, the new codesystem will keep the same ids for the new codesystem, and only update the effectiveDate
@param $baseDatasetId Only relevant when a source codesystem is specified and `keepIds` is false. This overrides the default base id for codesystem in the project. The value SHALL match one of the projects base ids for codesystem
@return (empty) codesystem object as xml with json:array set on elements
:)
declare function csapi:postCodeSystem($request as map(*)) {

    let $authmap                := $request?user
    let $projectPrefix          := $request?parameters?prefix[not(. = '')]
    let $targetDate             := $request?parameters?targetDate = true()
    let $sourceId               := $request?parameters?sourceId
    let $data                   := utillib:getBodyAsXml($request?body, 'codeSystem', ())
    let $sourceEffectiveDate    := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?sourceEffectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?sourceEffectiveDate[string-length() gt 0]
        }
    let $refOnly                := $request?parameters?refOnly = true()
    let $keepIds                := $request?parameters?keepIds = true()
    let $baseId                 := $request?parameters?baseId
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($projectPrefix)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter prefix')
        else ()
    
    let $results                := csapi:createCodeSystem($authmap, $projectPrefix, $targetDate, $sourceId, $sourceEffectiveDate, $refOnly, $keepIds, $baseId, $data)
    
    return
        roaster:response(201, 
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
        
        )
};
(: Central logic for creating an empty codesystem

@param $authmap         - required. Map derived from token
@return (empty) codesystem object as xml with json:array set on elements
:)
declare function csapi:createCodeSystem($authmap as map(*), $projectPrefix as xs:string, $targetDate as xs:boolean, $sourceId as xs:string?, $sourceEffectiveDate as xs:string?, $refOnly as xs:boolean?, $keepIds as xs:boolean?, $baseId as xs:string?, $editedCodeSystem as element(codeSystem)?) {

    let $decor                      := utillib:getDecorByPrefix($projectPrefix)
    let $projectLanguage            := $decor/project/@defaultLanguage
    (: if the user sent us a codeSystem with id, we should assume he intends to keep that id :)
    let $keepIds                    := if ($editedCodeSystem/@id) then true() else $keepIds = true()
    (: if the user sent us a codeSystem with effectiveDate, we should assume he intends to keep that effectiveDate :)
    let $now                        := 
        if ($editedCodeSystem/@effectiveDate) then substring($editedCodeSystem/@effectiveDate, 1, 19) else 
        if ($targetDate) then substring(string(current-date()), 1, 10) || 'T00:00:00' else substring(string(current-dateTime()), 1, 19)
    
    let $check                      :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, 'Project with prefix ' || $projectPrefix || ' does not exist')
        )
    let $check                      :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-TERMINOLOGY)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify codeSystem in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                      :=
        if (empty($editedCodeSystem/@id)) then () else (
            if (matches($editedCodeSystem/@id, '^[0-2](\.(0|[1-9][0-9]*)){0,3}$')) then
                error($errors:BAD_REQUEST, 'Code system id ''' || $editedCodeSystem/@id || ''' is reserved. You cannot reuse a reserved identifier. See http://oid-info.com/get/' || $editedCodeSystem/@id)
            else
            if (utillib:isOid($editedCodeSystem/@id)) then () else (
                error($errors:BAD_REQUEST, 'Code system id ''' || $editedCodeSystem/@id || ''' SHALL be a valid OID')
            )
        )
    
    let $sourceCodeSystem           := if (empty($sourceId)) then () else csapi:getCodeSystem($projectPrefix, (), (), $sourceId, $sourceEffectiveDate, false())
    let $storedCodeSystem           := 
        if ($keepIds and $editedCodeSystem[@id]) then csapi:getCodeSystem($projectPrefix, (), (), $editedCodeSystem/@id, $now, false()) else ()
    
    let $cadtsSystems               := 
        if ($refOnly = false() or empty($sourceId)) then () else (
            switch ($sourceId)
            case '2.16.840.1.113883.6.1'  return <codeSystem id="{$sourceId}" name="LOINC" displayName="LOINC"/>
            case '2.16.840.1.113883.6.96' return <codeSystem id="{$sourceId}" name="SNOMED-CT" displayName="SNOMED CT"/>
            default return
                csapi:getBrowsableCodeSystemMeta(collection($setlib:strCodesystemStableData || '/external')/browsableCodeSystem[@oid = $sourceId])
        )
    
    let $check                      :=
        if ($refOnly) then
            if (empty($sourceId)) then
                error($errors:BAD_REQUEST, 'Parameter sourceId SHALL be provided if a codeSystem reference is to be created')
            else
            if ($cadtsSystems) then ()
            else
            if (empty($sourceCodeSystem)) then
                if (empty($sourceEffectiveDate)) then
                    error($errors:BAD_REQUEST, 'Parameter sourceId ' || $sourceId || ' SHALL lead to a codeSystem in scope of this project')
                else (
                    error($errors:BAD_REQUEST, 'Parameters sourceId ' || $sourceId || ' and sourceEffectiveDate ' || $sourceEffectiveDate || ' SHALL lead to a codeSystem in scope of this project')
                )
            else ()
        else
        if (empty($editedCodeSystem) and empty($sourceId)) then 
            error($errors:BAD_REQUEST, 'CodeSystem input data or a sourceId SHALL be provided')
        else
        if ($editedCodeSystem) then
            if ($storedCodeSystem) then
                error($errors:BAD_REQUEST, 'Cannot create new codeSystem. The input codeSystem with id ' || $editedCodeSystem/@id || ' and effectiveDate ' || $now || ' already exists.')
            else ()
        else (
            if (empty($sourceCodeSystem)) then 
                error($errors:BAD_REQUEST, 'Cannot create new codeSystem. Source codeSystem based on id ' || $sourceId || ' and effectiveDate ' || $sourceEffectiveDate || ' does not exist.')
            else  ()
        )
    let $check                      :=
        if ($now castable as xs:dateTime) then () else (
            error($errors:BAD_REQUEST, 'Cannot create new codeSystem. The provided effectiveDate ' || $now || ' is not a valid xs:dateTime. Expected yyyy-mm-ddThh:mm:ss.')
        )
    
    let $editedCodeSystem             := ($editedCodeSystem, $cadtsSystems, $sourceCodeSystem)[1]
    (: decorlib:getNextAvailableIdP() returns <next base="1.2" max="2" next="{$max + 1}" id="1.2.3" type="DS"/> :)
    let $newCodeSystemId              := if ($keepIds or $refOnly) then ($editedCodeSystem/@id)[utillib:isOid(.)][1] else (decorlib:getNextAvailableIdP($decor, $decorlib:OBJECTTYPE-CODESYSTEM, $baseId)/@id)
    
    let $check                        :=
        if (csapi:getCodeSystem($projectPrefix, (), (), $newCodeSystemId, $now, false())[@effectiveDate = $now]) then
            error($errors:BAD_REQUEST, 'Cannot create new Codesystem. The to-be-created codesystem with id ' || $newCodeSystemId || ' and effectiveDate ' || $now || ' already exists.')
        else ()
    
    let $baseCodeSystem               := 
        <codeSystem>
        {
            attribute id {$newCodeSystemId} ,
            $editedCodeSystem/@name[string-length()>0] ,
            $editedCodeSystem/@displayName[string-length()>0] ,
            attribute effectiveDate {$now} ,
            attribute statusCode {"draft"} ,
            $editedCodeSystem/@versionLabel[string-length()>0] ,
            $editedCodeSystem/@expirationDate[string-length()>0] ,
            $editedCodeSystem/@officialReleaseDate[string-length()>0] ,
            $editedCodeSystem/@experimental[string-length()>0] ,
            $editedCodeSystem/@caseSensitive[string-length()>0] ,
            $editedCodeSystem/@canonicalUri[string-length()>0] ,
            $editedCodeSystem/@lastModifiedDate[string-length()>0]
        }
        </codeSystem>
    let $newCodeSystem                   := 
        if ($refOnly) then
            <codeSystem ref="{$baseCodeSystem/@id}" name="{$baseCodeSystem/@name}" displayName="{($baseCodeSystem/@displayName, $baseCodeSystem/@name)[1]}"/>
        else (
            utillib:prepareCodeSystemForUpdate($editedCodeSystem, $baseCodeSystem)
        )
    
    (: prepare sub root elements if not existent :)
    let $codeSystemUpdate                 :=
        if (not($decor/terminology) and $decor/codedConcepts) then
            update insert <terminology/> following $decor/codedConcepts
        else if (not($decor/terminology) and not($decor/codedConcepts)) then
            update insert <terminology/> following $decor/ids
        else ()
        (: now update the codesystem :)
    let $codeSystemUpdate                 :=
        if ($refOnly and $decor//codeSystem[@ref = $newCodeSystem/@ref]) then
            (: this could update the name/displayName if the source thing was updated since adding it previously :)
            update replace $decor//codeSystem[@ref = $newCodeSystem/@ref] with $newCodeSystem
        else 
        if ($decor/terminology/codeSystem) then 
            update insert $newCodeSystem following $decor/terminology/codeSystem[last()]          
        else 
        if ($decor/terminology[valueSet | conceptAssociation]) then
            update insert $newCodeSystem preceding ($decor/terminology/valueSet | $decor/terminology/conceptAssociation)[1]
        else (
            update insert $newCodeSystem into $decor/terminology
        )
   
    (: return the regular codeSystem that was created, or the requested version of the reference that was created, or the latest version of the reference that was created :)
    let $result                         := csapi:getCodeSystem($projectPrefix, (), (), ($newCodeSystem/@id | $newCodeSystem/@ref), ($newCodeSystem/@effectiveDate, $sourceEffectiveDate, 'dynamic')[1], false())
    (:let $result                         := $newCodeSystem:)
    let $doCADTS                        := if ($refOnly) then () else csapi:saveCodeSystemInCADTS($result, $projectPrefix, $projectLanguage)
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

(:~ Update codeSystem statusCode, versionLabel, expirationDate and/or officialReleaseDate

@param $authmap required. Map derived from token
@param $id required. DECOR concept/@id to update
@param $effectiveDate required. DECOR concept/@effectiveDate to update
@param $listOnly optional as xs:boolean. Default: false. Allows to test what will happen before actually applying it
@param $newStatusCode optional as xs:string. Default: empty. If empty, does not update the statusCode
@param $newVersionLabel optional as xs:string. Default: empty. If empty, does not update the versionLabel
@param $newExpirationDate optional as xs:string. Default: empty. If empty, does not update the expirationDate 
@param $newOfficialReleaseDate optional as xs:string. Default: empty. If empty, does not update the officialReleaseDate 
@return list object with success and/or error elements or error
:)
declare function csapi:postCodeSystemStatus($request as map(*)) {

    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $list                           := $request?parameters?list = true()
    let $statusCode                     := $request?parameters?statusCode
    let $versionLabel                   := $request?parameters?versionLabel
    let $expirationDate                 := $request?parameters?expirationDate
    let $officialReleaseDate            := $request?parameters?officialReleaseDate
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    
    return
        csapi:setCodeSystemStatus($authmap, $id, $effectiveDate, $list, $statusCode, $versionLabel, $expirationDate, $officialReleaseDate)
};
(:~ Central logic for patching an existing codeSystem statusCode, versionLabel, expirationDate and/or officialReleaseDate

@param $authmap required. Map derived from token
@param $id required. DECOR concept/@id to update
@param $effectiveDate required. DECOR concept/@effectiveDate to update
@param $listOnly optional as xs:boolean. Default: false. Allows to test what will happen before actually applying it
@param $newStatusCode optional as xs:string. Default: empty. If empty, does not update the statusCode
@param $newVersionLabel optional as xs:string. Default: empty. If empty, does not update the versionLabel
@param $newExpirationDate optional as xs:string. Default: empty. If empty, does not update the expirationDate 
@param $newOfficialReleaseDate optional as xs:string. Default: empty. If empty, does not update the officialReleaseDate 
@return list object with success and/or error elements or error
:)
declare function csapi:setCodeSystemStatus($authmap as map(*), $id as xs:string, $effectiveDate as xs:string?, $listOnly as xs:boolean, $newStatusCode as xs:string?, $newVersionLabel as xs:string?, $newExpirationDate as xs:string?, $newOfficialReleaseDate as xs:string?) as element(list) {
    (:get object for reference:)
    let $object                 := 
        if (empty($effectiveDate)) then (
            let $ttt    := $setlib:colDecorData//codeSystem[@id = $id]
            return
                $ttt[@effectiveDate = max($ttt/xs:dateTime(@effectiveDate))]
        )
        else (
            $setlib:colDecorData//codeSystem[@id = $id][@effectiveDate = $effectiveDate]
        )
    let $decor                  := $object/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix
    let $projectLanguage        := $decor/project/@defaultLanguage
    
    let $check                  :=
        if (count($object) = 1) then () else
        if ($object) then
            error($errors:SERVER_ERROR, 'CodeSystem id ''' || $id || ''' effectiveDate ''' || $effectiveDate || ''' cannot be updated because multiple ' || count($object) || ' were found in: ' || string-join(distinct-values($object/ancestor::decor/project/@prefix), ' / ') || '. Please inform your database administrator.')
        else (
            error($errors:BAD_REQUEST, 'CodeSystem id ''' || $id || ''' effectiveDate ''' || $effectiveDate || ''' cannot be updated because it cannot be found.')
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-TERMINOLOGY)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify codeSystems in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
        
    let $testUpdate             :=
        utillib:applyStatusProperties($authmap, $object, $object/@statusCode, $newStatusCode, $newVersionLabel, $newExpirationDate, $newOfficialReleaseDate, false(), true(), $projectPrefix)
    let $results                :=
        if ($testUpdate[self::error]) then $testUpdate else
        if ($listOnly) then $testUpdate else (
            utillib:applyStatusProperties($authmap, $object, $object/@statusCode, $newStatusCode, $newVersionLabel, $newExpirationDate, $newOfficialReleaseDate, false(), false(), $projectPrefix)
        )
    return
    if ($results[self::error] and not($listOnly)) then
        error($errors:BAD_REQUEST, string-join(
            for $e in $results[self::error]
            return
                $e/@itemname || ' id ''' || $e/@id || ''' effectiveDate ''' || $e/@effectiveDate || ''' cannot be updated: ' || data($e)
            , ' ')
        )
    else (
        let $doCADTS                := if ($listOnly) then () else csapi:saveCodeSystemInCADTS($object, $projectPrefix, $projectLanguage)
        return
        <list artifact="STATUS" current="{count($results)}" total="{count($results)}" all="{count($results)}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            (: max 2 arrays: one with error and one with success :)
            utillib:addJsonArrayToElements($results)
        }
        </list>
    )
};

(:~ Update codeSystem codedConcept statusCode, versionLabel, expirationDate and/or officialReleaseDate

@param $authmap required. Map derived from token
@param $id required. DECOR concept/@id to update
@param $effectiveDate required. DECOR concept/@effectiveDate to update
@param $listOnly optional as xs:boolean. Default: false. Allows to test what will happen before actually applying it
@param $newStatusCode optional as xs:string. Default: empty. If empty, does not update the statusCode
@param $newVersionLabel optional as xs:string. Default: empty. If empty, does not update the versionLabel
@param $newExpirationDate optional as xs:string. Default: empty. If empty, does not update the expirationDate 
@param $newOfficialReleaseDate optional as xs:string. Default: empty. If empty, does not update the officialReleaseDate 
@return list object with success and/or error elements or error
:)
declare function csapi:postCodeSystemCodedConceptStatus($request as map(*)) {

    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $code                           := $request?parameters?code
    let $recurse                        := $request?parameters?recurse = true()
    let $list                           := $request?parameters?list = true()
    let $statusCode                     := $request?parameters?statusCode
    let $versionLabel                   := $request?parameters?versionLabel
    let $expirationDate                 := $request?parameters?expirationDate
    let $officialReleaseDate            := $request?parameters?officialReleaseDate
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    
    return
        csapi:setCodeSystemCodedConceptStatus($authmap, $id, $effectiveDate, $code, $recurse, $list, $statusCode, $versionLabel, $expirationDate, $officialReleaseDate)
};
(:~ Central logic for patching an existing codeSystem codedConcept statusCode, versionLabel, expirationDate and/or officialReleaseDate

@param $authmap required. Map derived from token
@param $id required. DECOR concept/@id to update
@param $effectiveDate required. DECOR concept/@effectiveDate to update
@param $listOnly optional as xs:boolean. Default: false. Allows to test what will happen before actually applying it
@param $newStatusCode optional as xs:string. Default: empty. If empty, does not update the statusCode
@param $newVersionLabel optional as xs:string. Default: empty. If empty, does not update the versionLabel
@param $newExpirationDate optional as xs:string. Default: empty. If empty, does not update the expirationDate 
@param $newOfficialReleaseDate optional as xs:string. Default: empty. If empty, does not update the officialReleaseDate 
@return list object with success and/or error elements or error
:)
declare function csapi:setCodeSystemCodedConceptStatus($authmap as map(*), $id as xs:string, $effectiveDate as xs:string?, $code as xs:string, $recurse as xs:boolean, $listOnly as xs:boolean, $newStatusCode as xs:string?, $newVersionLabel as xs:string?, $newExpirationDate as xs:string?, $newOfficialReleaseDate as xs:string?) as element(list) {
    (:get object for reference:)
    let $object                 := 
        if (empty($effectiveDate)) then (
            let $ttt    := $setlib:colDecorData//codeSystem[@id = $id]
            return
                $ttt[@effectiveDate = max($ttt/xs:dateTime(@effectiveDate))]
        )
        else (
            $setlib:colDecorData//codeSystem[@id = $id][@effectiveDate = $effectiveDate]
        )
    let $codedConcept           := $object/conceptList/codedConcept[@code = $code]
    let $decor                  := $object/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix
    let $projectLanguage        := $decor/project/@defaultLanguage
    
    let $check                  :=
        if (count($object) = 1) then () else
        if ($object) then
            error($errors:SERVER_ERROR, 'CodeSystem id ''' || $id || ''' effectiveDate ''' || $effectiveDate || ''' cannot be updated because multiple ' || count($object) || ' were found in: ' || string-join(distinct-values($object/ancestor::decor/project/@prefix), ' / ') || '. Please inform your database administrator.')
        else (
            error($errors:BAD_REQUEST, 'CodeSystem id ''' || $id || ''' effectiveDate ''' || $effectiveDate || ''' cannot be updated because it cannot be found.')
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-TERMINOLOGY)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify codeSystems in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if ($codedConcept) then () else (
            error($errors:BAD_REQUEST, 'Cannot update status. CodeSystem does not contain code "' || $code || '".')
        )
        
    let $testUpdate             :=
        utillib:applyStatusProperties($authmap, $codedConcept, $codedConcept/@statusCode, $newStatusCode, $newVersionLabel, $newExpirationDate, $newOfficialReleaseDate, $recurse, true(), $projectPrefix)
    let $results                :=
        if ($testUpdate[self::error]) then $testUpdate else
        if ($listOnly) then $testUpdate else (
            utillib:applyStatusProperties($authmap, $codedConcept, $codedConcept/@statusCode, $newStatusCode, $newVersionLabel, $newExpirationDate, $newOfficialReleaseDate, $recurse, false(), $projectPrefix)
        )
    return
    if ($results[self::error] and not($listOnly)) then
        error($errors:BAD_REQUEST, string-join(
            for $e in $results[self::error]
            return
                $e/@itemname || ' code ''' || $e/@code || ''' cannot be updated: ' || data($e)
            , ' ')
        )
    else (
        let $doCADTS                := if ($listOnly) then () else csapi:saveCodeSystemInCADTS($object, $projectPrefix, $projectLanguage)
        return
        <list artifact="STATUS" current="{count($results)}" total="{count($results)}" all="{count($results)}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            (: max 2 arrays: one with error and one with success :)
            utillib:addJsonArrayToElements($results)
        }
        </list>
    )
};

declare function csapi:patchCodeSystem($request as map(*)) {

    let $authmap                := $request?user
    let $id                     := $request?parameters?id
    let $effectiveDate          := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $data                   := utillib:getBodyAsXml($request?body, 'parameters', ())
    
    let $check                  :=
        if ($data) then () else (
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        )
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()

    let $return                 := csapi:patchCodeSystem($authmap, string($id), string($effectiveDate), $data)
    return (
        roaster:response(200, $return)
    )

};
declare function csapi:patchCodeSystem($authmap as map(*), $id as xs:string, $effectiveDate as xs:string, $data as element(parameters)) as element(codeSystem) {

    let $storedCodeSystem       := $setlib:colDecorData//codeSystem[@id = $id][@effectiveDate = $effectiveDate]
    let $decor                  := $storedCodeSystem/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix
    let $projectDefaultLanguage := $decor/project/@defaultLanguage

    let $check                  :=
        if ($storedCodeSystem) then () else (
            error($errors:BAD_REQUEST, 'Codesystem with id ' || $id || ' and effectiveDate ' || $effectiveDate || ' does not exist')
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-TERMINOLOGY)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify codesystem in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    (: Lock? :)
    let $lock                   := decorlib:getLocks($authmap, $id, $effectiveDate, ())
    let $lock                   := if ($lock) then $lock else decorlib:setLock($authmap, $id, $effectiveDate, false())
    
    let $check                  :=
        if ($lock) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have a lock for this codesystem (anymore). Get a lock first.'))
        )
    let $check                  :=
        if ($data[parameter]) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall be an array of ''parameter''.')
        )
    let $unsupportedops         := $data/parameter[not(@op = ('add', 'replace', 'remove'))]
    let $check                  :=
        if ($unsupportedops) then
            error($errors:BAD_REQUEST, 'Submitted parameters shall have supported ''op'' value. Found ''' || string-join(distinct-values($unsupportedops/@op), ''', ''') || ''', expected ''add'', ''replace'', or ''remove''') 
        else ()
    
    (: prepare patches by eliminating all but the last of a particular kind to avoid doing checks on patches we azre not going to apply.
        Example 1: when a codedConcept is added, and then edited a couple of times we only need to check to final state
        Example 2: when a conceptList is edited a couple of times we only need to check the final state
        Example 3: when codeSystem/property is added and chedc a couple of time we only need to check the final state
    :)
    let $preparedPatches        :=
        for $param in $data/parameter
        let $path       := $param/@path
        let $switchPath := if (starts-with($path, '/conceptList/codedConcept/')) then ('/conceptList/codedConcept/') else ($path)
        let $value      := $param/@value
        return
            switch ($switchPath)
            case '/property' return     if ($param/following-sibling::parameter[@path = $path][value/property/@code = $param/property/@code]) then () else $param
            case '/statusCode'
            case '/expirationDate'
            case '/canonicalUri'
            case '/name' 
            case '/displayName'
            case '/experimental' 
            case '/caseSensitive'
            case '/conceptList' return  if ($param/following-sibling::parameter[@path = $path]) then () else $param
            case '/conceptList/codedConcept/' return  (
                let $theCodeString  := substring-after($path, '/conceptList/codedConcept/')
                let $theCode        := tokenize($theCodeString, '/')[1]
                let $searchString   := '/conceptList/codedConcept/' || $theCode
                
                return
                    (: don't want it if a following sibling is /conceptList :)
                    if ($param/following-sibling::parameter[@path = '/conceptList']) then () else
                    (: don't want it if a following sibling has /conceptList/codedConcept/[theCode] :)
                    if ($param/following-sibling::parameter[@path = $searchString]) then () else
                    (: don't want it if a following sibling has /conceptList/codedConcept/[theCode]/... :)
                    if ($param/following-sibling::parameter[starts-with(@path, $searchString || '/')]) then () else (
                        $param
                    )
            )
            default return $param
    
    (: prepare what the conceptList would look like after applying all patches. treat removals separately below in the switch :)
    let $preparedConceptList    := 
        if ($preparedPatches[@path = '/conceptList']) then  
            utillib:prepareCodeSystemConceptListForUpdate($preparedPatches/value/conceptList, $storedCodeSystem/conceptList)
        else
        if ($preparedPatches[starts-with(@path, '/conceptList/codedConcept/')]) then
            <conceptList>
            {
                for $cc in $preparedPatches[@op = ('add', 'replace')]/value/codedConcept
                let $pcc := utillib:prepareCodeSystemCodedConceptForUpdate($cc, $storedCodeSystem/conceptList/codedConcept)
                return
                    <codedConcept>{$pcc/@*, $pcc/(* except (parent|child))}</codedConcept>
            }
            </conceptList>
        else (
            (: not patching conceptList today so no processing necessary :)
        )
    let $newCodedConceptMap     :=
        if ($preparedConceptList) then 
            map:merge(
                for $cc in $preparedConceptList/codedConcept
                return map:entry($cc/@code, $cc)
            )
         else ()
    
    let $check                  :=
        for $param in $preparedPatches
        let $op         := $param/@op
        let $path       := $param/@path
        let $switchPath := if (starts-with($path, '/conceptList/codedConcept/')) then ('/conceptList/codedConcept/') else ($path)
        let $value      := $param/@value
        let $pathpart   := substring-after($path, '/')
        return
            switch ($switchPath)
            case '/statusCode' return (
                if ($op = 'remove') then
                    'Parameter path ''' || $path || ''' does not support ' || $op
                else 
                if (utillib:isStatusChangeAllowable($storedCodeSystem, $value)) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Supported are: ' || string-join(map:get($utillib:itemstatusmap, string($storedCodeSystem/@statusCode)), ', ')
                )
            )
            case '/expirationDate'
            case '/officialReleaseDate'
            return (
                if ($op = 'remove') then () else 
                if ($value castable as xs:dateTime) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL be yyyy-mm-ddThh:mm:ss.'
                )
            )
            case '/canonicalUri'
            case '/versionLabel' return (
                if ($op = 'remove') then () else 
                if ($value[not(. = '')]) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL NOT be empty.'
                )
            )
            case '/name' 
            case '/displayName'
            return (
                if ($op = 'remove') then
                    'Parameter path ''' || $path || ''' does not support ' || $op
                else 
                if ($value[not(. = '')]) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL NOT be empty.'
                )
            )
            case '/experimental' 
            case '/caseSensitive'
            return (
                if ($op = 'remove') then () else 
                if ($value[. = ('true', 'false')]) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL be true or false.'
                )
            )
            case '/desc'
            case '/copyright'
            case '/purpose' return (
                if ($param[count(value/*[name() = $pathpart]) = 1]) then (
                    if ($param/value/*[name() = $pathpart][@inherited]) then 
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Contents SHALL NOT be marked ''inherited''.'
                    else (),
                    if ($param/value/*[name() = $pathpart][matches(@language, '[a-z]{2}-[A-Z]{2}')]) then
                        if ($param[@op = 'remove'] | $param/value/*[name() = $pathpart][.//text()[not(normalize-space() = '')]]) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A ' || $pathpart || ' SHALL have contents.'
                        )
                    else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A ' || $pathpart || ' SHALL have a language with pattern ll-CC.'
                    )
                )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/*[name() = $pathpart]) 
                )
            )
            case '/publishingAuthority' return (
                if ($param[count(value/publishingAuthority) = 1]) then (
                    if ($param/value/publishingAuthority[@inherited]) then 
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Contents SHALL NOT be marked ''inherited''.'
                    else (),
                    if ($param/value/publishingAuthority[@id]) then
                        if ($param/value/publishingAuthority[utillib:isOid(@id)]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || ''' publishingAuthority.id SHALL be an OID is present. Found ' || string-join($param/value/publishingAuthority/@id, ', ')
                        )
                    else (),
                    if ($param/value/publishingAuthority[@name[not(. = '')]]) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || ''' publishingAuthority.name SHALL be a non empty string.'
                    ),
                    let $unsupportedtypes := $param/value/publishingAuthority/addrLine/@type[not(. = $csapi:ADDRLINE-TYPE/enumeration/@value)]
                    return
                        if ($unsupportedtypes) then
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Publishing authority has unsupported addrLine.type value(s) ' || string-join($unsupportedops, ', ') || '. SHALL be one of: ' || string-join($csapi:ADDRLINE-TYPE/enumeration/@value, ', ')
                        else ()
                ) else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' SHALL have a single publishingAuthority under value. Found ' || count($param/value/publishingAuthority)
                )
            )
            case '/property' return (
                if ($param[count(value/property) = 1]) then (
                    if ($op = 'remove') then
                        if ($storedCodeSystem/conceptList/codedConcept/property[@code = $param/value/property/@code]) then
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. CodeSystem.property code=' || string-join($param/value/property/@code, ' ') || ' SHALL NOT be removed while in use in a codedConcept.'
                        else ()
                    else (
                        if ($param/value/property/@code[not(. = '')]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Property.code SHALL be a non empty string.'
                        ),
                        if ($param/value/property/@type[not(. = '')]) then 
                            if ($param/value/property[@type = $csapi:PROPERTYDEF-TYPE/enumeration/@value]) then () else (
                                'Parameter ' || $op || ' not allowed for ''' || $path || ''' with type ''' || string-join($param/value/property/@type, ' ') || '''. Supported are: ' || string-join($csapi:PROPERTYDEF-TYPE/enumeration/@value, ', ')
                            )
                        else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Property.type SHALL be a non empty string.'
                        )
                    )
                )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one property under value. Found ' || count($param/value/property) 
                )
            )
            case '/conceptList' 
            case '/conceptList/codedConcept/' return (
                if ($op = 'remove' and $path = '/conceptList') then
                    'Parameter path ''' || $path || ''' does not support ' || $op
                else
                if ($op = 'remove') then (
                    let $theCodeString  := substring-after($path, '/conceptList/codedConcept/')
                    let $theCode        := tokenize($theCodeString, '/')[1]
                    let $stored         := $storedCodeSystem/conceptList/codedConcept[@code = $theCode]
                    let $storedChildren := $storedCodeSystem/conceptList/codedConcept[parent/@code = $theCode]
                    let $storedChildren :=
                        for $child in $storedChildren
                        return
                            if ($preparedPatches[@op = 'remove']/value/codedConcept[@code = $child/@code]) then () else
                            if ($preparedPatches/value/codedConcept[@code = $child/@code][not(parent/@code = $theCode)]) then () else (
                                $child
                            )
                    
                    return
                        if ($stored[not(@statusCode = 'new')]) then
                            'Parameter path ''' || $path || ''' does not support ' || $op || ' while the codedConcept has status ' || string-join($stored/@statusCode, ', ') || '.'
                        else
                        if ($storedChildren) then
                            'Parameter path ''' || $path || ''' does not support ' || $op || ' while the codeSystem still has children for this concept: ' || string-join($storedChildren/@code, ', ') || '.'
                        else ()
                )
                else
                if ($param[count(value/conceptList | value/codedConcept) = 1]) then (
                    (: Checks on conceptList content
                    - XSD       level/type
                    - SCH       When there are children the leveltype cannot be L
                    - SCH       Level must be 0
                    - SCH       Count from parents is level in a codesystem every coded concept shall be unique.
                    - SCH       parent/child: the relationship between parent and child must be acknowledged.
                    - SCH       properties: is every property on a coded concept declared as codesystem/property?
                    - XQuery    status: When the status is final/active it is not allowed to have changes
                    :)
                    let $theCodeString  := substring-after($path, '/conceptList/codedConcept/')
                    let $theCode        := tokenize($theCodeString, '/')[1]
                    let $thePosition    := tokenize($theCodeString, '/')[2]
                    let $theSiblingCode := tokenize($theCodeString, '/')[3]
                    let $stored         := $storedCodeSystem/conceptList/codedConcept[@code = $theCode]
                    let $sibling        := $storedCodeSystem/conceptList/codedConcept[@code = $theSiblingCode] | $preparedConceptList/codedConcept[@code = $theSiblingCode]
                    
                    let $codeError      :=
                        if ($path = '/conceptList') then () else if ($param/value/codedConcept[@code = $theCode]) then
                            if ($op = 'add' and $stored) then 
                                error($errors:SERVER_ERROR, 'CodedConcept ' || $theCode || ' cannot be added because it already exists.')  
                            else ()
                        else (
                            'CodedConcept ' || $theCode || ' cannot be inserted because the value.codedConcept.code does not match. Found ' || $param/value/codedConcept/@code
                        )
                    let $posError       := 
                        if (string-length($thePosition) = 0) then () else if ($thePosition = ('before', 'after', 'following', 'preceding')) then
                            if (string-length($theSiblingCode) = 0) then 
                                'CodedConcept ' || $theCode || ' cannot be inserted. Position ' || $thePosition || ' stated, but sibling code missing. Expected syntax /conceptList/codedConcept/[theCode]/[before | after]/[theSiblingcode].'
                            else 
                            if ($sibling) then () else (
                                'CodedConcept ' || $theCode || ' cannot be inserted ' || $thePosition || ' ' || $theSiblingCode || ' because that concept does not exist.'
                            )
                        else ( 
                            'CodedConcept ' || $theCode || ' cannot be inserted. Position ' || $thePosition || ' not supported. Expected one of: before or after.'
                        )
                    
                    let $codeSystem     := 
                        <terminology>
                            <codeSystem>
                            {
                                $storedCodeSystem/@*, 
                                $storedCodeSystem/property[not(@code = $data/parameter[@op = 'remove']/value/property/@code)],
                                $data/parameter[not(@op = 'remove')]/value/property,
                                if (($storedCodeSystem/property/@code | $data/parameter[not(@op = 'remove')]/value/property/@code)[. = 'deprecationDate']) then () else (
                                    <property code="deprecationDate" type="dateTime"/>
                                ),
                                if (($storedCodeSystem/property/@code | $data/parameter[not(@op = 'remove')]/value/property/@code)[. = 'retirementDate']) then () else (
                                    <property code="retirementDate" type="dateTime"/>
                                )
                                ,
                                $preparedConceptList
                            }
                            </codeSystem>
                        </terminology>
                    
                    let $xsdValidation  := validation:jaxv-report($codeSystem, $setlib:docDecorSchema)
                    let $xsdError       := 
                        if ($xsdValidation/status = 'valid') then () else (
                            'ConceptList is invalid: ' || string-join(for $msg in $xsdValidation/message return (data($msg) || ' (' || $msg/@line || ',' || $msg/@column || ')'), '. ') || serialize($codeSystem)
                        )
                    let $decorSchFile   := concat($setlib:strDecorCore, '/DECOR.sch')
                    let $decorSch       := if (doc-available($decorSchFile)) then doc($decorSchFile)/sch:schema else ()
                    let $schValidation  := if ($decorSch) then transform:transform($codeSystem, utillib:get-iso-schematron-svrl($decorSch), ()) else ()
                    let $schIssues      := $schValidation//(svrl:failed-assert | svrl:successful-report)[not(@role = ('info','information'))]/svrl:text
                    let $schIssues      :=
                        if ($path = '/conceptList') then $schIssues else (
                            $schIssues,
                            for $parentCode in $param/value/codedConcept/parent/@code
                            let $parent := $storedCodeSystem/conceptList/codedConcept[@code = $parentCode]
                            return
                                if ($parentCode = $theCode) then 
                                    'ERROR Code system concept code ' || $theCode || ' SHALL NOT declare itself as parent. | Location'
                                else if ($parent) then () else (
                                    'ERROR Code system concept code ' || $theCode || ' SHALL declare an existing parent. Parent ' || $parentCode || ' not found in the code system. | Location'
                                )
                        )
                    let $schError       := 
                        if ($schIssues) then 
                            'ConceptList is invalid: ' || string-join(for $msg in $schIssues return replace(data($msg), '\s*\| Location.*', ''), '. ')
                        else ()
                    
                    let $statusError    :=
                        for $codedConcept in $storedCodeSystem/conceptList/codedConcept
                        let $newCodedConcept    := map:get($newCodedConceptMap, $codedConcept/@code)
                        return (
                            if ($newCodedConcept) then
                                if (not(utillib:isStatusChangeAllowable($codedConcept, $newCodedConcept/@statusCode))) then 
                                    'CodedConcept ' || $newCodedConcept/@code || ' with current status ''' || $codedConcept/@statusCode || ''' cannot be updated to ''' || $newCodedConcept/@statusCode || '''. Supported are: ' || string-join(map:get($utillib:codedconceptstatusmap, string($codedConcept/@statusCode)), ', ')
                                else
                                if ($codedConcept[@statusCode = ('new','draft','experimental')] | $codedConcept[empty(@statusCode)]) then () else (
                                    let $canonicalCodedConcept  := csapi:canonicalizeCodedConcept($codedConcept)
                                    let $canonicalNewConcept    := csapi:canonicalizeCodedConcept($newCodedConcept)
                                    
                                    return
                                        if ($canonicalCodedConcept = $canonicalNewConcept) then () else (
                                            'CodedConcept ' || $codedConcept/@code || ' with final statusCode=' || $codedConcept/@statusCode || ' SHALL NOT be updated in the new conceptList.'
                                            (:error($errors:BAD_REQUEST, 'Before: ' || serialize($canonicalCodedConcept) || ' After: ' || serialize($canonicalNewConcept)):)
                                        )
                                )
                            else
                            if ($preparedPatches[@path = '/conceptList']) then
                                'CodedConcept ' || $codedConcept/@code || ' is missing in the new conceptList.' 
                            else ()
                        )
                    return
                        if (empty($posError) and empty($xsdError) and empty($schError) and empty($statusError)) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. ' || string-join(($xsdError, $schError, $statusError), ' ')
                        )
                )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one conceptList under value. Found ' || count($param/value/conceptList) 
                )
            )
            default return (
                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Path value not supported'
            )
     let $check                 :=
        if (empty($check)) then () else (
            error($errors:BAD_REQUEST,  string-join ($check, ' '))
        )
    
    let $intention              := if ($storedCodeSystem[@statusCode = 'final']) then 'patch' else 'version'
    let $update                 := histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-CODESYSTEM, $projectPrefix, $intention, $storedCodeSystem)
    
    let $update                 :=
        for $param in $preparedPatches
        let $path       := $param/@path
        let $switchPath := if (starts-with($path, '/conceptList/codedConcept/')) then ('/conceptList/codedConcept/') else ($path)
        return
            switch ($switchPath)
            case '/statusCode'
            case '/expirationDate'
            case '/officialReleaseDate'
            case '/versionLabel'
            case '/name'
            case '/displayName'
            case '/experimental' 
            case '/caseSensitive'
            case '/canonicalUri'
            return (
                let $attname  := substring-after($path, '/')
                let $new      := attribute {$attname} {$param/@value}
                let $stored   := $storedCodeSystem/@*[name() = $attname]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedCodeSystem
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/desc' return (
                (: only one per language :)
                let $elmname  := substring-after($path, '/')
                let $new      := utillib:prepareFreeFormMarkupWithLanguageForUpdate($param/value/desc)
                let $stored   := $storedCodeSystem/*[name() = $elmname][@language = $param/value/desc/@language]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedCodeSystem
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/copyright' return (
                (: only one per language :)
                let $new      := utillib:prepareFreeFormMarkupWithLanguageForUpdate($param/value/copyright)
                let $stored   := $storedCodeSystem/copyright[@language = $param/value/copyright/@language]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedCodeSystem
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/purpose' return (
                (: only one possible per language :)
                let $elmname  := substring-after($path, '/')
                let $new      := utillib:prepareFreeFormMarkupWithLanguageForUpdate($param/value/purpose)
                let $stored   := $storedCodeSystem/*[name() = $elmname][@language = $param/value/purpose/@language]
                
                return
                switch ($param/@op)
                case 'add' (: fall through :)
                case 'replace' return if ($stored) then update replace $stored with $new else update insert $new into $storedCodeSystem
                case 'remove' return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/publishingAuthority' return (
                (: only one possible :)
                let $new      := utillib:preparePublishingAuthorityForUpdate($param/value/publishingAuthority)
                let $stored   := $storedCodeSystem/publishingAuthority
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedCodeSystem
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/property' return (
                (: one possible per code :)
                let $elmname  := substring-after($path, '/')
                let $new      := utillib:prepareCodeSystemPropertyForUpdate($param/value/property)
                let $stored   := $storedCodeSystem/property[@code = $new/@code]
                
                return
                switch ($param/@op)
                case ('add') return update insert $new into $storedCodeSystem
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedCodeSystem
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/conceptList' return (
                (: one possible :)
                let $elmname  := substring-after($path, '/')
                let $new      := utillib:prepareCodeSystemConceptListForUpdate($param/value/conceptList, $storedCodeSystem/conceptList)
                let $stored   := $storedCodeSystem/conceptList
                
                return
                switch ($param/@op)
                case ('add') return update insert $new into $storedCodeSystem
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedCodeSystem
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/conceptList/codedConcept/' return (
                let $theCodeString  := substring-after($path, '/conceptList/codedConcept/')
                let $theCode        := tokenize($theCodeString, '/')[1]
                let $thePosition    := tokenize($theCodeString, '/')[2]
                let $theSiblingCode := tokenize($theCodeString, '/')[3]
                let $new            := utillib:prepareCodeSystemCodedConceptForUpdate($param/value/codedConcept, $storedCodeSystem/conceptList/codedConcept)
                let $sibling        := $storedCodeSystem/conceptList/codedConcept[@code = $theSiblingCode]
                let $stored         := $storedCodeSystem/conceptList/codedConcept[@code = $theCode]
                
                return
                switch ($param/@op)
                case ('add')
                case ('replace') return (
                    switch ($thePosition)
                    case 'after'
                    case 'following' return (update delete $stored, update insert $new following $sibling)
                    case 'before'
                    case 'preceding' return (update delete $stored, update insert $new preceding $sibling)
                    default return if ($stored) then update replace $stored with $new else update insert $new into $storedCodeSystem/conceptList
                )
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
                
                
            )     
            default return ( (: unknown path :) )
    
    (: after all the updates, the XML Schema order is likely off. Restore order :)
    let $preparedCodeSystem     := utillib:prepareCodeSystemForUpdate($storedCodeSystem, $storedCodeSystem)
    
    let $update                 := update replace $storedCodeSystem with $preparedCodeSystem
    let $update                 := update delete $lock
    
    let $result                 := csapi:getCodeSystem($projectPrefix, (), (), $preparedCodeSystem/@id, $preparedCodeSystem/@effectiveDate, false())
    let $doCADTS                := csapi:saveCodeSystemInCADTS($result, $projectPrefix, $projectDefaultLanguage)
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

declare %private function csapi:canonicalizeCodedConcept($codedConcept as element(codedConcept)) as element(codedConcept) {
    <codedConcept>
    {
        for $att in $codedConcept/@*
        order by name($att)
        return
            data($att),
        for $n in $codedConcept/desc
        order by $n/@language
        return
            data($n),
        for $n in $codedConcept/designation
        order by $n/@language, $n/@type
        return
            data(($n/@language, $n/@type, $n/@displayName, $n)),
        for $n in $codedConcept/property[not(@code = ('deprecationDate', 'retirementDate'))]
        order by $n/@code
        return
            (data($n/@code), for $att in $n/*/@* order by name($att) return data($att)),
        for $n in ($codedConcept/parent, $codedConcept/child)
        order by $n/@code
        return
            data($n/@code)
    }
    </codedConcept>
};

(:~ Returns a codesystem for publication in csv format 

@param $codeSystems required. Is retrieved from request id/effectiveDate
@param $language optional. Or parameter request or derived from artLanguage 
@returns a codesystem in csv format

:)
declare function csapi:convertCodeSystem2Csv($codeSystems as element()*, $language as xs:string?) as xs:string* {

    let $language               := 
        if (empty($language)) then $setlib:strArtLanguage else $language

    let $mapentries             :=
        for $d in distinct-values($codeSystems//designation/string-join((@type,@language),' / '))
        return map:entry($d, max($codeSystems//*/count(designation[$d = string-join((@type,@language),' / ')])))
    let $designationmap     := map:merge($mapentries)
    let $header             := 
        concat('Level;Type;Code;DisplayName;CodeSystem;CodeSystemName;CodeSystemVersion;StatusCode',
            if (count($mapentries)=0) then () else (
                concat(';',string-join(
                    for $d in map:keys($designationmap)
                    for $i in (1 to map:get($designationmap, $d))
                    return concat('&quot;Designation ',$d,'&quot;')
                ,';'))
            )
        ,'&#13;&#10;')
    let $columncount        := count(tokenize($header,';'))
    let $keyCodesystem        :=
        switch($language)
            case "en-US" return "Code System"
            case "de-DE" return "Code System"
            case "nl-NL" return "Codesysteem"
            default return "Code System"
    return (
    (:<concept code="xxxxxx" codeSystem="2.16.840.1.113883.2.4.15.4" displayName="Medicatie" level="0" type="A"/> :)
    (: Replace double quotes with single quotes in the CSV values, except in the code itself, 
    and place in between double quotes if there a white space character in a string
    Note that in the exceptional event that a code contains a double quote, the CSV renders invalid :)
    for $codesystem at $i in $codeSystems//codeSystem[@id]
    return (
        if ($i > 1) then ('&#13;&#10;') else (),
        concat('&quot;',$keyCodesystem,' ',$codesystem/@displayName,' - ',$codesystem/(@id|@ref),' ',$codesystem/@effectiveDate,'&quot;',string-join(for $i in (1 to ($columncount - 1)) return ';',''),'&#13;&#10;'),
        $header,
        for $concept in $codesystem/conceptList/codedConcept
            let $conceptCode := data($concept/@code)
            let $quotedConceptCode := if (matches($conceptCode,'\s+')) then (concat('&quot;',$conceptCode,'&quot;')) else ($conceptCode)
            let $conceptDisplayName := replace(data($concept/designation[@language=$language][1]/@displayName),'"','&apos;')
            let $quotedConceptDisplayName := if (matches($conceptDisplayName,'\s+')) then (concat('&quot;',$conceptDisplayName,'&quot;')) else ($conceptDisplayName)
            let $conceptCodeSystem := replace(data($concept/ancestor::codeSystem/@id),'"','&apos;')
            let $quotedConceptCodeSystem := if (matches($conceptCodeSystem,'\s+')) then (concat('&quot;',$conceptCodeSystem,'&quot;')) else ($conceptCodeSystem)
            let $generatedCodeSystemName := if ($concept/ancestor::codeSystem/@displayName) then (data($concept/ancestor::codeSystem/@displayName)) else (data($concept/ancestor::codeSystem/@name))
            let $conceptCodeSystemName := replace(if ($concept/@codeSystemName) then (data($concept/@codeSystemName)) else if (replace($generatedCodeSystemName,'[0-9\.]','')!='') then ($generatedCodeSystemName) else (''),'"','&apos;')
            let $quotedConceptCodeSystemName := if (matches($conceptCodeSystemName,'\s+')) then (concat('&quot;',$conceptCodeSystemName,'&quot;')) else ($conceptCodeSystemName)
            let $conceptCodeSystemVersion := replace(data($concept/ancestor::codeSystem/@effectiveDate),'"','&apos;')
            let $quotedConceptCodeSystemVersion := if (matches($conceptCodeSystemVersion,'\s+')) then (concat('&quot;',$conceptCodeSystemVersion,'&quot;')) else ($conceptCodeSystemVersion)
            let $conceptStatusCode := replace(data($concept/@statusCode),'"','&apos;')
            let $quotedConceptStatusCode := if (matches($conceptStatusCode,'\s+')) then (concat('&quot;',$conceptStatusCode,'&quot;')) else ($conceptStatusCode)
            let $conceptEffectiveDate := replace(data($concept/@effectiveDate),'"','&apos;')
            let $quotedConceptEffectiveDate := if (matches($conceptEffectiveDate,'\s+')) then (concat('&quot;',$conceptEffectiveDate,'&quot;')) else ($conceptEffectiveDate)
            let $conceptExpirationDate := replace(data($concept/@expirationDate),'"','&apos;')
            let $quotedConceptExpirationDate := if (matches($conceptExpirationDate,'\s+')) then (concat('&quot;',$conceptExpirationDate,'&quot;')) else ($conceptExpirationDate)
        return (
            concat(data($concept/@level),';',data($concept/@type),';',$quotedConceptCode,';',$quotedConceptDisplayName,';',$quotedConceptCodeSystem,';',$quotedConceptCodeSystemName,';',$quotedConceptCodeSystemVersion,';',$quotedConceptStatusCode,';',$quotedConceptEffectiveDate,';',$quotedConceptExpirationDate,
                concat(';',string-join(
                    for $d in map:keys($designationmap)
                    for $i in (1 to map:get($designationmap, $d))
                    let $designation    := $concept/designation[$d=string-join((@type,@language),' / ')][1]
                    return 
                        if ($designation[@displayName]) then
                        if (matches($designation/@displayName,'\s+')) then (
                            concat('&quot;',$designation/@displayName,'&quot;')
                        ) else (
                            $designation/@displayName
                        )
                        else ('')
                ,';'))
            ,'&#13;&#10;')
        )
    ))

};

(: ======================= INTO CADTS ======================== :)
(:~ Convert all project code systems to Centralized ART-DECOR Terminology Services (CADTS), which makes them available for use in value set creation and expansions. Note that project code systems are left as-is. 
    The conversion only happens on the latest version of said code systems. Any code systems that are not the latest are skipped silently. Code systems that happen to match an externally managed system like SNOMED CT are skipped and reported as error
:)
declare function csapi:process-decorprojectcodesystemsincadts-request($threadId as xs:string, $request as element(projectcodesystemconvert-request)) {
    let $projectPrefixes        := tokenize($request/@for, '\s')[not(. = ('', '*'))]
    let $logsignature           := 'csapi:process-decorprojectcodesystemsincadts-request'
    let $progress-initial       := $csapi:PROGRESS-CSCADTS-10
    
    (: =================== PREPARE VARIABLES BEGIN ==================== :)
    let $codeSystems            := 
        if (empty($projectPrefixes)) then
            $setlib:colDecorCache//codeSystem[@id] | $setlib:colDecorData//codeSystem[@id]
        else 
        (: special case when you refresh the cache :)
        if ($projectPrefixes = '*cache*') then
            $setlib:colDecorCache//codeSystem[@id]
        else (
            $setlib:colDecorData//codeSystem[@id][ancestor::decor/project/@prefix = $projectPrefixes] | $setlib:colDecorData//codeSystem[@id][ancestor::decor/project/@id = $projectPrefixes]
        )
    let $codeSystems            :=
        for $csbyid at $i in $codeSystems[@statusCode = ('active', 'final', 'draft', 'pending', 'review')]
        let $csid               := $csbyid/@id
        group by $csid
        return
            head(
                for $cs in $csbyid 
                order by $cs/xs:dateTime(@effectiveDate) descending, util:collection-name($cs) descending 
                return $cs
            )
    let $csCount                := count($codeSystems)
    (: ===================  PREPARE VARIABLES END  ==================== :)
    
    let $mark-busy              := update insert attribute busy {'true'} into $request
    let $progress               := 
        if ($request/@progress) then (
            update value $request/@progress with $progress-initial 
        )
        else (
            update insert attribute progress {$progress-initial} into $request
        )
    let $progress               := 
        if ($request/@progress-percentage) then (
            update value $request/@progress-percentage with round((100 div ($csCount + 1)) * 1) 
        )
        else (
            update insert attribute progress-percentage {round((100 div ($csCount + 1)) * 1)} into $request
        )
    
    (: ========================= CONVERT BEGIN ========================= :)
    let $save               :=
        for $cs at $i in $codeSystems
        let $progress               := update value $request/@progress-percentage with round((100 div ($csCount + 1)) * $i) 
        let $progress               := update value $request/@progress with $csapi:PROGRESS-CSCADTS-20 || $i || ' of ' || $csCount 
        return (
            try { 
                let $r := csapi:saveCodeSystemInCADTS($cs, $cs/ancestor::decor/project/@prefix, $cs/ancestor::decor/project/@defaultLanguage)
                return ()
            }
            catch * {
                let $cselement  := element {local-name($cs)} {$cs/@*, if ($cs[@ident]) then () else attribute ident {$cs/ancestor::decor[1]/project/@prefix}}
                let $save       := update insert $cselement into $request 
                
                return $err:description 
            }
        )
    (: ========================== CONVERT END ========================== :)
    
    return 
        if (empty($save)) then (
            xmldb:remove(util:collection-name($request), util:document-name($request))
        ) 
        else (
            error(xs:QName('csapi:process-decorprojectcodesystemsincadts-request'), 'One or more code systems were not converted successfully: ' || string-join($save, ' '))
        )
};
(: ======================= INTO CADTS ======================== :)

(:~ Stores a DECOR codeSystem into the CADTS space, *if* it is the latest available version of that codeSystem on the server, including what's in cache
    Removes any existing copy of that codeSystem in CADTS before creating. Creation is done in /db/apps/terminology-data/codesystem-stable-data/projects/[prefix]/[codeSystem/@name]-[codeSystem/@id].xml
:)
declare %private function csapi:saveCodeSystemInCADTS($storedCodeSystem as element(codeSystem), $projectPrefix as xs:string, $projectDefaultLanguage as xs:string?) as xs:boolean {
    let $csid               := $storedCodeSystem/@id
    let $csed               := $storedCodeSystem/@effectiveDate
    let $csnm               := $storedCodeSystem/@name
    let $csdn               := $storedCodeSystem/@displayName
    
    let $check              := 
        if (collection($setlib:strCodesystemStableData || '/external')/*[@oid = $csid]) then (
            (: delete any codeSystems in the projects area of CADTS in case those were written before installation of the external version :)
            let $existing   := collection($setlib:strCodesystemStableData)//@oid[. = $csid]
            let $delete     := 
                try { 
                    for $ex in $existing
                    return 
                        if (contains(util:collection-name($ex), $setlib:strCodesystemStableData || '/external')) then () else (
                            xmldb:remove(util:collection-name($ex), util:document-name($ex))
                        )
                } catch * {()}
            
            return
            error($errors:SERVER_ERROR, 'Your code system ' || $csdn || ' (' || $csid || ' version ' || $csed || ') is safe in the project ' || $projectPrefix || 
                    ' but saving to the centralized terminology services (CADTS) failed because you cannot override an external code system from a DECOR project. Please inform your server administrator.')
        )
        else ()
    
    let $latestCodeSystem   := csapi:getCodeSystem((), (), (), $csid, (), false())
    
    let $doSave             := $csed = $latestCodeSystem/@effectiveDate (:and $storedCodeSystem/@statusCode = ('active', 'final', 'draft', 'pending', 'review'):)
    let $save               :=
        try {
            if ($doSave) then (
                
                let $topColl       :=
                    if (contains(util:collection-name($storedCodeSystem), $setlib:strDecorCache)) then (
                        xmldb:create-collection($setlib:strCodesystemStableData, 'cache/' || $projectPrefix)
                    )
                    else (
                        xmldb:create-collection($setlib:strCodesystemStableData, 'projects/' || $projectPrefix)
                    )
                let $coll       :=
                    if (contains(util:collection-name($storedCodeSystem), $setlib:strDecorCache)) then (
                        xmldb:create-collection($setlib:strCodesystemStableData || '/cache/' || $projectPrefix, $csnm || '-' || $csid)
                        )
                    else (
                        xmldb:create-collection($setlib:strCodesystemStableData || '/projects/' || $projectPrefix, $csnm || '-' || $csid)
                    )
                let $params     :=
                    <parameters>
                        <param name="language" value="{($projectDefaultLanguage, $storedCodeSystem/ancestor::decor/project/@defaultLanguage)[not(. = '')][1]}"/>
                        <param name="canonicalUri" value="{utillib:getCanonicalUriForOID('CodeSystem', $csid, $csed, $storedCodeSystem/ancestor::decor/@prefix, ())}"/>
                    </parameters>
                let $data       := transform:transform($storedCodeSystem, doc($csapi:DECOR2CADTS-XSL), $params)
                
                let $existing   := collection($setlib:strCodesystemStableData)//@oid[. = $csid]
                let $delete     := 
                    for $ex in $existing
                    return 
                        if (contains(util:collection-name($ex), $setlib:strCodesystemStableData || '/external')) then () else (
                            xmldb:remove(util:collection-name($ex), util:document-name($ex))
                        )
                
                let $store      := xmldb:store($coll, $csnm || '-' || $csid || '.xml', $data)
                
                return ()
            )
            else ()
        }
        catch * {
            error($errors:SERVER_ERROR, 'Your code system ' || $csdn || ' (' || $csid || ' version ' || $csed || ') is safe in the project ' || $projectPrefix || 
                    ' but saving to the centralized terminology services (CADTS) failed so it is not available for value set usage. Please inform your server administrator. The error was: ' || $err:description)
        }
    return 
        $doSave
};

(:~ Gets the content of a conceptlist by recursively resolving all codedconcepts. Use vsapi:getValueSetExtracted to resolve a codedConcepts. :)
declare function csapi:getCodedConcepts($codedConcepts as element(codedConcept)*, $codeSystemId as xs:string, $level as xs:integer, $conceptList as element(conceptList), $sofar as xs:string*, $checkParentChild as xs:boolean, $language as xs:string) {
    for $codedConcept in $codedConcepts[not(@statusCode = ('cancelled', 'rejected'))]
    let $children       := 
        if ($checkParentChild) then
            $conceptList/codedConcept[parent/@code = $codedConcept/@code][not(@code = $codedConcept/@code)]
        else (
            let $nextFirstOtherHierarchy            := $codedConcept/following-sibling::codedConcept[@level castable as xs:integer][xs:integer(@level) le $level]
            return
                $codedConcept/following-sibling::codedConcept[@level castable as xs:integer][xs:integer(@level) = ($level + 1)][not(preceding-sibling::codedConcept[@code = $nextFirstOtherHierarchy/@code])]
        )
    let $type           := 
        if ($codedConcept[@statusCode = ('deprecated', 'retired', 'cancelled', 'rejected')]) then 'D' else
        if ($codedConcept[@type = 'A'] | $codedConcept[@abstract = 'true'] | $codedConcept/property[@code = 'notSelectable']/valueBoolean[@value = 'true']) then 'A' else
        if ($children) then 'S'
        else ('L')
    return (
        <concept code="{$codedConcept/@code}" 
                 codeSystem="{$codeSystemId}" 
                 codeSystemName="{$codedConcept/ancestor::codeSystem/@displayName}" 
                 codeSystemVersion="{$codedConcept/ancestor::codeSystem/@effectiveDate}" 
                 displayName="{csapi:getDesignationFromCodedConcept($codedConcept, $language)}" 
                 level="{$level}" type="{$type}">
        {
            for $designation in $codedConcept/designation
            let $designationtype    := if ($designation[@type]) then $designation/@type else 'preferred'
            return
                <designation>
                {
                    $designation/@displayName, 
                    attribute type {$designationtype}, 
                    $designation/@language, 
                    $designation/@lastTranslated, 
                    $designation/@mimeType
                }
                </designation>
        }
        {
            $codedConcept/desc[.//text()[not(normalize-space() = '')]]
        }
        </concept>
        ,
        csapi:getCodedConcepts($children[not(@code = $sofar)], $codeSystemId, $level + 1, $conceptList, distinct-values(($sofar, $children/@code)), $checkParentChild, $language)
    )
};

(:~ Return zero or more codesystems as-is

@param $id           - required. Identifier of the codesystem to retrieve
@param $flexibility  - optional. null gets all versions, yyyy-mm-ddThh:mm:ss gets this specific version, anything that doesn't cast to xs:dateTime gets latest version
@return Matching value sets
@since 2024-02-13
:)
declare %private function csapi:getCodeSystemById ($id as xs:string, $flexibility as xs:string?) as element(codeSystem)* {
    if ($flexibility castable as xs:dateTime) then
        $setlib:colDecorData//codeSystem[@id = $id][@effectiveDate = $flexibility] | $setlib:colDecorCache//codeSystem[@id = $id][@effectiveDate = $flexibility]
    else
    if ($flexibility) then (
        let $codeSystems  := $setlib:colDecorData//codeSystem[@id = $id] | $setlib:colDecorCache//codeSystem[@id = $id]
        return
            $codeSystems[@effectiveDate = string(max($codeSystems/xs:dateTime(@effectiveDate)))]
    )
    else (
        $setlib:colDecorData//codeSystem[@id = $id] | $setlib:colDecorCache//codeSystem[@id = $id]
    )
    
};

declare function csapi:getCodeSystemById($id as xs:string, $flexibility as xs:string?, $decorOrPrefix as item(), $decorRelease as xs:string?) as element(codeSystem)* {
    csapi:getCodeSystemById($id, $flexibility, $decorOrPrefix, $decorRelease, ())
};
(:~ Return zero or more codesystems as-is

@param $id            - required. Identifier of the value set to retrieve
@param $flexibility   - optional. null gets all versions, yyyy-mm-ddThh:mm:ss gets this specific version, anything that doesn't cast to xs:dateTime gets latest version
@param $decorOrPrefix - required. determines search scope. pfx- limits scope to this project only
@param $decorRelease  - optional. if empty defaults to current version. if valued then the value set will come explicitly from that archived project version which is expected to be a compiled version
@param $decorLanguage - optional. Language compilation. Defaults to project/@defaultLanguage.
@return Matching value sets
@since 2024-02-13
:)

declare function csapi:getCodeSystemById($id as xs:string, $flexibility as xs:string?, $decorOrPrefix as item(), $decorRelease as xs:string?, $decorLanguage as xs:string?) as element(codeSystem)* {
let $decor          := utillib:getDecor($decorOrPrefix, $decorRelease, $decorLanguage)
let $decors         := if (empty($decorRelease)) then utillib:getBuildingBlockRepositories($decor, (), $utillib:strDecorServicesURL) else $decor

return
    if ($flexibility castable as xs:dateTime) then
        $decors//codeSystem[@id = $id][@effectiveDate = $flexibility]
    else
    if ($flexibility) then (
        let $codeSystems  := $decors//codeSystem[@id = $id]
        return
            $codeSystems[@effectiveDate = string(max($codeSystems/xs:dateTime(@effectiveDate)))]
    )
    else (
        $decors//codeSystem[@id = $id]
    )
};

(:~ Return zero or more codesystems as-is

@param $name         - required. Identifier of the codesystem to retrieve
@param $flexibility  - optional. null gets all versions, yyyy-mm-ddThh:mm:ss gets this specific version, anything that doesn't cast to xs:dateTime gets latest version
@return Matching value sets
@since 2024-11-14
:)
declare %private function csapi:getCodeSystemByName ($name as xs:string, $flexibility as xs:string?) as element(codeSystem)* {
    if ($flexibility castable as xs:dateTime) then
        $setlib:colDecorData//codeSystem[@name = $name][@effectiveDate = $flexibility] | $setlib:colDecorCache//codeSystem[@name = $name][@effectiveDate = $flexibility]
    else
    if ($flexibility) then (
        let $codeSystems  := $setlib:colDecorData//codeSystem[@name = $name] | $setlib:colDecorCache//codeSystem[@name = $name]
        return
            $codeSystems[@effectiveDate = string(max($codeSystems/xs:dateTime(@effectiveDate)))]
    )
    else (
        $setlib:colDecorData//codeSystem[@name = $name] | $setlib:colDecorCache//codeSystem[@name = $name]
    )
    
};

(:~ Return zero or more codesystems as-is

@param $name          - required. Identifier of the value set to retrieve
@param $flexibility   - optional. null gets all versions, yyyy-mm-ddThh:mm:ss gets this specific version, anything that doesn't cast to xs:dateTime gets latest version
@param $decorOrPrefix - required. determines search scope. pfx- limits scope to this project only
@param $decorRelease  - optional. if empty defaults to current version. if valued then the value set will come explicitly from that archived project version which is expected to be a compiled version
@param $decorLanguage - optional. Language compilation. Defaults to project/@defaultLanguage.
@return Matching value sets
@since 2024-11-14
:)

declare function csapi:getCodeSystemByName($name as xs:string, $flexibility as xs:string?, $decorOrPrefix as item(), $decorRelease as xs:string?, $decorLanguage as xs:string?) as element(codeSystem)* {
let $decor          := utillib:getDecor($decorOrPrefix, $decorRelease, $decorLanguage)
let $decors         := if (empty($decorRelease)) then utillib:getBuildingBlockRepositories($decor, (), $utillib:strDecorServicesURL) else $decor

return
    if ($flexibility castable as xs:dateTime) then
        $decors//codeSystem[@name = $name][@effectiveDate = $flexibility]
    else
    if ($flexibility) then (
        let $codeSystems  := $decors//codeSystem[@name = $name]
        return
            $codeSystems[@effectiveDate = string(max($codeSystems/xs:dateTime(@effectiveDate)))]
    )
    else (
        $decors//codeSystem[@name = $name]
    )
};

(:~ Return zero or more codesystems as-is

@param $ref          - required. Identifier of the codesystem to retrieve
@param $flexibility  - optional. null gets all versions, yyyy-mm-ddThh:mm:ss gets this specific version, anything that doesn't cast to xs:dateTime gets latest version
@return Matching value sets
@since 2024-11-14
:)
declare %private function csapi:getCodeSystemByRef ($ref as xs:string, $flexibility as xs:string?) as element(codeSystem)* {
    if (utillib:isOid($ref)) then
        csapi:getCodeSystemById($ref, $flexibility)
    else (
        csapi:getCodeSystemByName($ref, $flexibility)
    )    
    
};

(:~ Return zero or more codesystems as-is

@param $ref           - required. Identifier or name of the value set to retrieve
@param $flexibility   - optional. null gets all versions, yyyy-mm-ddThh:mm:ss gets this specific version, anything that doesn't cast to xs:dateTime gets latest version
@param $decorOrPrefix - required. determines search scope. pfx- limits scope to this project only
@param $decorRelease  - optional. if empty defaults to current version. if valued then the value set will come explicitly from that archived project version which is expected to be a compiled version
@param $decorLanguage - optional. Language compilation. Defaults to project/@defaultLanguage.
@return Matching value sets
@since 2024-11-14
:)

declare function csapi:getCodeSystemByRef($ref as xs:string, $flexibility as xs:string?, $decorOrPrefix as item(), $decorRelease as xs:string?, $decorLanguage as xs:string?) as element(codeSystem)* {
    if (utillib:isOid($ref)) then
        csapi:getCodeSystemById($ref, $flexibility, $decorOrPrefix, $decorRelease, $decorLanguage)
    else (
        csapi:getCodeSystemByName($ref, $flexibility, $decorOrPrefix, $decorRelease, $decorLanguage)
    )  

};

(:~  Get contents of a codeSystem and return like this:
 :   <codeSystem id|ref="oid" ...>
 :       <desc/>                           -- if applicable
 :       <conceptList>
 :           <codedConcept .../>
 :       </conceptList>
 :   </codeSystem>
 :)
declare function csapi:getRawCodeSystem($codeSystem as element(), $includetrail as element()*, $language as xs:string?) as element(codeSystem)* {
 
    <codeSystem>
    {
        $codeSystem/@*,
        if ($codeSystem[@url]) then () else attribute url {($codeSystem/ancestor::cacheme/@bbrurl, $utillib:strDecorServicesURL)[1]},
        if ($codeSystem[@ident]) then () else attribute ident {($codeSystem/ancestor::decor/project/@prefix)[1]},
        (: this helps to establish the default display for a given codedConcept :)
        if ($codeSystem/@language) then () else if (empty($language)) then attribute language {$codeSystem/ancestor::decor/project/@defaultLanguage} else attribute language {$language}
    }
    {
        $codeSystem/desc
    }
    {
        $codeSystem/publishingAuthority
        ,
        if ($codeSystem[@id]) then 
            if ($codeSystem[publishingAuthority]) then () else utillib:inheritPublishingAuthorityFromProject($codeSystem/ancestor::decor)
        else ()
        ,
        $codeSystem/endorsingAuthority
        ,
        $codeSystem/purpose
        ,
        $codeSystem/copyright
        ,
        $codeSystem/property
    }
    {
        for $revisionHistory in $codeSystem/revisionHistory
        return
            <revisionHistory>
            {
                $revisionHistory/@*
                ,
                $revisionHistory/desc
            }
            </revisionHistory>
    }
    {
        if ($codeSystem/conceptList[*]) then (
            <conceptList>
            {
                for $node in $codeSystem/conceptList/codedConcept
                return
                    <codedConcept>
                    {
                        $node/@*
                        ,
                        $node/designation,
                        $node/desc,
                        $node/property,
                        $node/parent,
                        $node/child
                    }
                    </codedConcept>
            }
            </conceptList>
        ) else ()
    }
    </codeSystem>
    };

(:~  Extracts a codeSystem with @id. Use csapi:getCodeSystemExtract to resolve a codeSystem/@ref first. 
:   &lt;codeSystem all-attributes of the input&gt;
:       &lt;desc all /&gt;
:       &lt;conceptList&gt;
:           &lt;codedConcept all-attributes-and-designation-elements /&gt;
:       &lt;/conceptList&gt;
:   &lt;/codeSystem&gt;
:                                
:   @param $codeSystem     - required. The codeSystem element with content to expand
:   @param $prefix       - required. The origin of codeSystem. pfx- limits scope this project only
:   @param $version      - optional. if empty defaults to current version. if valued then the codesystem will come explicitly from that archived project version which is expected to be a compiled version
:   @return The expanded codeSystem
:   @since 2024-02-12
:)
declare function csapi:getCodeSystemExtracted($codeSystem as element(), $projectPrefix as xs:string, $projectVersion as xs:string?, $projectLanguage as xs:string?) as element(codeSystem) {
    let $decor                  := utillib:getDecor($projectPrefix, $projectVersion, $projectLanguage)[1]
    
    let $language               := 
        if (empty($projectLanguage)) then ($decor/@language, $decor/project/@defaultLanguage)[1] else $projectLanguage

    let $language               :=
        if (empty($language)) then ($codeSystem/../@defaultLanguage, serverapi:getServerLanguage()) else ()

    let $rawCodeSystem            := 
        csapi:getRawCodeSystem($codeSystem, <include ref="{$codeSystem/(@id|@ref)}" flexibility="{$codeSystem/@effectiveDate}"/>, $language)
    
    return
        <codeSystem>
        {
            $rawCodeSystem/(@* except (@url|@ident|@referencedFrom))
            ,
            $rawCodeSystem/desc
            ,
            $rawCodeSystem/publishingAuthority
            ,
            $rawCodeSystem/endorsingAuthority
            ,
            $rawCodeSystem/revisionHistory
            ,
            $rawCodeSystem/copyright
            ,
            $rawCodeSystem/property
        }
        {
            if ($rawCodeSystem//conceptList[*]) then (
                <conceptList>
                {
                    $rawCodeSystem//conceptList/codedConcept
                }
                </conceptList>
            ) else ()
        }
        </codeSystem>
};

(:~ Return zero or more extracted codesystems wrapped in a &lt;return/&gt; element, and subsequently inside a &lt;repository&gt; element. This repository element
:   holds at least the attribute @ident with the originating project prefix and optionally the attribute @url with the repository URL in case of an external
:   repository. Id based references can match both codeSystem/@id and codeSystem/@ref. The latter is resolved. Note that duplicate codeSystem matches may be 
:   returned. Example output:
:   &lt;return>
:       &lt;repository url="http://art-decor.org/decor/services/" ident="ad2bbr-">
:           &lt;codeSystem id="2.16.840.1.113883.1.11.10282" name="ParticipationSignature" displayName="ParticipationSignature" effectiveDate="2013-03-11T00:00:00" statusCode="final" versionLabel="DEFN=UV=VO=1206-20130318">
:               ...
:           &lt;/codeSystem>
:       &lt;/repository>
:   &lt;/return>
:   <ul><li> Codesystems get an extra attribute @fromRepository containing the originating project prefix/ident</li> 
:   <li>codeSystem/@ref is treated as if it were the referenced codesystem</li>
:   <li>If parameter prefix is not used then only codeSystem/@id is matched. If that does not give results, then 
:     codeSystem/@ref is matched, potentially expanding into a buildingBlockRepository</li>
:                                
:   @param $id           - required. Identifier of the codesystem to retrieve
:   @param $flexibility  - optional. null gets all versions, 'dynamic' gets the newest version based on id, yyyy-mm-ddThh:mm:ss gets this specific version
:   @return Zero code systems in case no matches are found, one if only one exists or if a specific version was requested, or more if more versions exist and no specific version was requested
:   @since 2024-02-12
:)
declare %private function csapi:getCodeSystemExtract($idOrName as xs:string, $effectiveDate as xs:string?) as element(codeSystem)* {
    let $idOrName               := $idOrName[not(. = '')]
    let $effectiveDate          := ($effectiveDate[not(. = '')], 'dynamic')[1]
    
    let $codeSystems            := csapi:getCodeSystemByRef($idOrName, $effectiveDate)

    return 
    <return>
    {
        for $cs in $codeSystems
        let $prefix    := $cs/ancestor::decor/project/@prefix
        let $urlident  := concat($cs/ancestor::cacheme/@bbrurl,$prefix)
        group by $urlident
        return
            <project ident="{$prefix}">
            {
                if ($cs[ancestor::cacheme]) then
                    attribute url {$cs[1]/ancestor::cacheme/@bbrurl}
                else (),
                $cs[1]/ancestor::decor/project/@defaultLanguage,
                for $codeSystem in $cs[@id]
                return
                    csapi:getCodeSystemExtracted($codeSystem, $prefix, (), ())
            }
            </project>
    }
    </return>
};

(:~ Return zero or more expanded codesystems wrapped in a &lt;return/&gt; element, and subsequently inside a &lt;repository&gt; element. This repository element
:   holds at least the attribute @ident with the originating project prefix and optionally the attribute @url with the repository URL in case of an external
:   repository. Id based references can match both codeSystem/@id and codeSystem/@ref. The latter is resolved. Note that duplicate codeSystem matches may be 
:   returned. Example output:
:   &lt;return>
:       &lt;repository url="http://art-decor.org/decor/services/" ident="ad2bbr-">
:           &lt;codeSystem id="2.16.840.1.113883.1.11.10282" name="ParticipationSignature" displayName="ParticipationSignature" effectiveDate="2013-03-11T00:00:00" statusCode="final" versionLabel="DEFN=UV=VO=1206-20130318">
:               ...
:           &lt;/codeSystem>
:       &lt;/repository>
:   &lt;/return>
:   <ul><li> Codesystems get an extra attribute @fromRepository containing the originating project prefix/ident</li> 
:   <li>codeSystem/@ref is treated as if it were the referenced codesystem</li>
:   <li>If parameter prefix is not used then only codeSystem/@id is matched. If that does not give results, then 
:     codeSystem/@ref is matched, potentially expanding into a buildingBlockRepository</li>
:                                
:   @param $id           - required. Identifier of the codesystem to retrieve
:   @param $flexibility  - optional. null gets all versions, 'dynamic' gets the newest version based on id, yyyy-mm-ddThh:mm:ss gets this specific version
:   @param $prefix       - required. determines search scope. pfx- limits scope to this project only
:   @param $version      - optional. if empty defaults to current version. if valued then the codesystem will come explicitly from that archived project version which is expected to be a compiled version
:   @return Zero code systems in case no matches are found, one if only one exists or if a specific version was requested, or more if more versions exist and no specific version was requested
:   @since 2013-06-14
:)
declare %private function csapi:getCodeSystemExtract($projectPrefix as xs:string*, $projectVersion as xs:string*, $projectLanguage as xs:string?, $idOrName as xs:string, $effectiveDate as xs:string?, $withversions as xs:boolean?) as element(codeSystem)* {
    let $id                     := $idOrName[not(. = '')]
    let $flexibility            := if ($withversions) then () else ($effectiveDate[not(. = '')], 'dynamic')[1]
    let $projectPrefix          := $projectPrefix[not(. = '')][1]
    let $projectVersion         := $projectVersion[not(. = '')][1]
    let $projectLanguage        := $projectLanguage[not(. = '')][1]

    (: retrieve all codesystems for the input $decor project(s) 
        - when compiled version  - as is - 
        - when live version - getting all (cached) decor projects in scope  
    :)
    let $decor                  := utillib:getDecor($projectPrefix, $projectVersion, $projectLanguage)
    let $decors                 := if (empty($projectVersion)) then utillib:getBuildingBlockRepositories($decor, (), $utillib:strDecorServicesURL) else $decor[1]

    let $codeSystems              := 
        if (utillib:isOid($idOrName)) then (
            if ($flexibility castable as xs:dateTime) then
                $decors//codeSystem[@id = $idOrName][@effectiveDate = $flexibility]
            else
            if ($flexibility) then (
                let $codeSystems  := $decors//codeSystem[@id = $idOrName]
                return
                    $codeSystems[@effectiveDate = string(max($codeSystems/xs:dateTime(@effectiveDate)))]
            )
            else (
                $decors//codeSystem[@id = $idOrName]
            )
        )
        else (
            if ($flexibility castable as xs:dateTime) then
                $decors//codeSystem[@name = $idOrName][@effectiveDate = $flexibility]
            else
            if ($flexibility) then (
                let $codeSystems  := $decors//codeSystem[@name = $idOrName]
                return
                    $codeSystems[@effectiveDate = string(max($codeSystems/xs:dateTime(@effectiveDate)))]
            )
            else (
                $decors//codeSystem[@name = $idOrName]
            )
        
        )
    
    let $codeSystems              :=  $codeSystems | $decor//codeSystem[@ref = $idOrName]
    
    return
    <return>
    
    {
        (:from the requested project, return codeSystem/(@id and @ref):)
        (: when retrieving code systems from a compiled project, the @url/@ident they came from are on the codeSystem element
           reinstate that info so downstream logic works as if it really came from the repository again.
        :)
        for $repository in $decor
        for $cs in $codeSystems
        let $url                := if (empty($cs/@url)) then ($cs/ancestor::cacheme/@bbrurl, $utillib:strDecorServicesURL)[1] else $cs/@url
        let $ident              := if (empty($cs/@ident)) then $cs/ancestor::decor/project/@prefix else $cs/@ident
        let $repo               := if ($ident=$projectPrefix) then 'project' else 'repository'
        let $refFrom            := if (empty($cs/@referencedFrom)) then ($decors//project[buildingBlockRepository/@ident = $ident][buildingBlockRepository/@url = $url]/@prefix)[1] else $cs/@referencedFrom
        let $projectType        := if (exists($cs/ancestor::cacheme)) then 'cached' else 'local'
        let $source             := $url || $ident
        group by $source
        order by $repo[1]
        return
        
            element {$repo[1]} {
                
                if ($ident=$projectPrefix) then 
                (
                    attribute ident{$ident[1]}
                ) else
                ( 
                    attribute url {$url[1]},
                    attribute ident{$ident[1]},
                    attribute referencedFrom {$refFrom[1]},
                    attribute projecttype {$projectType[1]}
                ),
                for $codeSystem in $cs
                    order by $codeSystem/@effectiveDate descending
                    return
                        csapi:getCodeSystemExtracted($codeSystem, $projectPrefix, $projectVersion, $projectLanguage)
                }
    }
 
    </return>

};    

declare %private function csapi:getDesignationFromCodedConcept($codedConcept as element(codedConcept), $language as xs:string?) as xs:string? {
    let $designation    := $codedConcept/designation[@language = $language]
    let $designation    := if ($designation) then $designation else $codedConcept/designation
    let $designation    :=
        if ($designation[@type = 'fsn']) then $designation[@type = 'fsn'][1] else
        if ($designation[@type = 'preferred']) then $designation[@type = 'preferred'][1] else
        if ($designation[empty(@type)]) then $designation[empty(@type)][1] else $designation[1]
    
    return
        $designation/@displayName
};

declare %private function csapi:getCodeSystem($projectPrefix as xs:string*, $projectVersion as xs:string?, $projectLanguage as xs:string?, $id as xs:string, $effectiveDate as xs:string?, $withversions as xs:boolean) as element(codeSystem)* {
    let $id                     := $id[not(. = '')]
    let $flexibility            := if ($withversions) then () else ($effectiveDate[not(. = '')], 'dynamic')[1]
    let $effectiveDate          := $effectiveDate[not(. = '')]
    let $projectPrefix          := $projectPrefix[not(. = '')]
    let $projectVersion         := $projectVersion[not(. = '')]
    
    let $results                :=
        if (empty($projectPrefix)) then (
            csapi:getCodeSystemById($id, $flexibility)
        )
        else (
            csapi:getCodeSystemExtract($projectPrefix[1], $projectVersion[1], $projectLanguage, $id, $effectiveDate, $withversions)/(descendant-or-self::codeSystem[@id][@effectiveDate], descendant-or-self::codeSystem[@ref])[1]
        )
    
    (: UPGRADE OLDER SYSTEMS - adds parent based on level. adds statusCode based on currently defined code list :)
    let $results                :=
        for $codeSystem in $results
        let $doAddParents       := exists($codeSystem/conceptList/codedConcept[@level != '0']) and empty($codeSystem/conceptList/codedConcept[parent])
        let $doAddStatus        := exists($codeSystem/conceptList/codedConcept[empty(@statusCode)])
        let $ident              := ($codeSystem/parent::*/@ident, $codeSystem/ancestor::*/@bbrident, $codeSystem/ancestor::decor/project/@prefix)[1]
        return
            <codeSystem>
            {
                $codeSystem/@*,
                if ($codeSystem/@url) then () else if ($ident) then
                    attribute url {($codeSystem/parent::*/@url, $codeSystem/ancestor::*/@bbrurl, $utillib:strDecorServicesURL)[1]}
                else (),    
                if ($codeSystem/@ident) then () else if ($ident) then
                    attribute ident {$ident}
                else ()
                ,
                $codeSystem/desc
                ,
                if ($codeSystem/publishingAuthority) then $codeSystem/publishingAuthority else (
                    let $copyright  := ($codeSystem/ancestor::decor/project/copyright[empty(@type)] | $codeSystem/ancestor::decor/project/copyright[@type = 'author'])[1]
                    return
                    if ($copyright) then
                        <publishingAuthority name="{$copyright/@by}" inherited="project">
                        {
                            (: Currently there is no @id, but if it ever would be there ...:)
                            $copyright/@id,
                            $copyright/addrLine
                        }
                        </publishingAuthority>
                    else ()
                )
                ,
                $codeSystem/endorsingAuthority
                ,
                $codeSystem/revisionHistory
                ,
                $codeSystem/copyright
                ,
                $codeSystem/property
                ,
                if ($doAddParents or $doAddStatus) then
                    <conceptList>
                    {
                        for $codedConcept in $codeSystem/conceptList/codedConcept
                        let $statusCode     := 
                            switch ($codedConcept/@statusCode)
                            case 'final' return 'active'
                            default return (
                                if ($csapi:CODEDCONCEPT-STATUS/enumeration[@value = $codedConcept/@statusCode]) then $codedConcept/@statusCode else
                                if ($codeSystem[@statusCode = 'draft']) then ($codedConcept/@statusCode, 'draft')[1] else ($codedConcept/@statusCode, 'active')[1]
                            )
                        let $parent         :=
                            if ($doAddParents and $codedConcept[@level castable as xs:integer][xs:integer(@level) gt 0]) then (
                                let $theLevel                           := $codedConcept/xs:integer(@level)   
                                return $codedConcept/preceding-sibling::codedConcept[@level castable as xs:integer][xs:integer(@level) lt $theLevel][1]
                            ) else ()
                        return
                            <codedConcept>
                            {
                                $codedConcept/(@* except @statusCode),
                                attribute statusCode {$statusCode},
                                $codedConcept/(node() except (parent | child)),
                                if ($parent) then <parent>{$parent/@code}</parent> else ()
                            }
                            </codedConcept>
                    }
                    </conceptList>
                else (
                    $codeSystem/conceptList
                )
            }
            </codeSystem>
    (: If all else fails, check the oid registries for a name for this thing :)
    let $results                :=
        (:
            <codeSystem oid="2.16.840.1.113883.6.1" id="loinc" url="http://loinc.org" version="2.70" statusCode="active" experimental="false" 
                        effectiveDate="2020-07-02" defaultLanguage="en-US" content="complete" 
                        type="complex" structure="network" count="167424">
                <language complete="true">en-US</language>
                <language complete="">es-AR</language>...
                <logo link="http://loinc.org">LOINC-logo40.png</logo>
                <license><text>This product includes all or a portion of the LOINC(r) table, LOINC panels and ....<text></license>
            </codeSystem>
        :)
        if (empty($results)) then
            for $cs in adterm:getCodeSystemInfo($id)[@oid]
            let $effective    :=
                if ($cs/@effectiveDate castable as xs:dateTime)         then $cs/@effectiveDate else 
                if ($cs/@effectiveDate castable as xs:date)             then $cs/@effectiveDate || 'T00:00:00' else
                if ($cs/@effectiveDate || '-01' castable as xs:date)    then $cs/@effectiveDate || '-01T00:00:00' else
                if ($cs/@effectiveDate || '-01-01' castable as xs:date) then $cs/@effectiveDate || '-01-01T00:00:00' else (
                    substring(string(current-dateTime()), 1, 19)
                )
            let $status       :=
                if ($cs/@statusCode[. = map:keys($utillib:itemstatusmap)]) then $cs/@statusCode else  'final'
            return
                <codeSystem id="{$id}">
                {
                    attribute effectiveDate {$effective},
                    attribute statusCode {$status},
                    if ($cs/@version)               then attribute versionLabel {$cs/@version} else (),
                    if ($cs/@experimental = 'true') then attribute experimental {'true'} else (),
                    if ($cs/@uri)                   then attribute canonicalUri {$cs/@uri} else (),
                    if ($cs/name)                   then (
                        attribute name {utillib:shortName($cs/name[1])}, attribute displayName {$cs/name[1]}
                    ) else (
                        attribute name {'unknownSystem'}, attribute displayName  {'Unknown System'}
                    ),
                    if ($cs/publisher)              then
                        <publishingAuthority name="{data($cs/publisher[1])}">
                        {
                            for $addr in $cs/contact[1]/name
                            return
                                <addrLine>{data($addr)}</addrLine>
                            ,
                            for $addr in $cs/contact[1]/telecom
                            return
                                <addrLine>
                                {
                                    switch ($addr/@system)
                                    case 'phone'
                                    case 'fax'
                                    case 'email' return attribute type {$addr/@system}
                                    case 'url'   return attribute type {'uri'}
                                    (:case 'pager'
                                    case 'sms'
                                    case 'other':)
                                    default return ()
                                    ,
                                    data($addr/@value)
                                }
                                </addrLine>
                        }
                        </publishingAuthority>
                    else ()
                    ,
                    if ($cs/license)                then <copyright language="{($cs/@defaultLanguage, 'en-US')[1]}">{$cs/license[1]/text[1]/node()}</copyright> else ()
                }
                </codeSystem>
        else $results
    
    for $cs in $results
    let $id     := $cs/@id | $cs/@ref
    let $ed     := $cs/@effectiveDate
    let $csinfo := if ($cs[@id]) then () else collection($setlib:strCodesystemStableData || '/external')/*[@oid = $id]
    order by $id, $ed descending
    return
        element {name($cs)} {
            if ($cs[@id]) then (
                $cs/@*,
                $cs/(* except conceptList)
            )
            else (
                $cs/(@* except @external),
                attribute external {exists($csinfo)},
                if ($cs[@versionLabel]) then () else if ($csinfo/@version[not(. = '')]) then
                    attribute versionLabel {$csinfo/@version}
                else (),
                if ($cs[@canonicalUri]) then () else if ($csinfo/@url[not(. = '')]) then
                    attribute canonicalUri {$csinfo/@url}
                else (),
                for $a in $csinfo/(@effectiveDate | @expirationDate | @statusCode | @lastModifiedDate | @officialReleaseDate | @experimental | @caseSensitive)[not(. = '')]
                return
                    if ($cs/@*[name() = name($a)]) then () else ($a)
                ,
                for $d in $csinfo/description[.//text()]
                return
                    <desc>{$d/@*, $d/node()}</desc>
                ,
                $csinfo/publishingAuthority,
                $csinfo/copyright
            )
            ,
            (: we return just a count of issues and leave it up to the calling party to get those if required :)
            if (empty($ed)) then
                <issueAssociation count="{count($setlib:colDecorData//issue/object[@id = $id])}"/>
            else (
                <issueAssociation count="{count($setlib:colDecorData//issue/object[@id = $id][@effectiveDate = $ed])}"/>
            )
            ,
            $cs/conceptList
        }
};

(:~ Returns a list of zero or more codeSystems as listed in the terminology section. This function is useful e.g. to call from a CodeSystemIndex. Parameter id, name or prefix is required.

@param $projectPrefix    - optional. determines search scope. null is full server, pfx- limits scope to this project only
@param $projectVersion   - optional. if empty defaults to current version. if valued then the codeSystem will come explicitly from that archived project version which is expected to be a compiled version
@param $projectLanguage  - optional. defaults to project defaultLanguage or first available if multiple compilation exist 
@param $objectid         - optional. Identifier of the codeSystem to retrieve
@param $objectnm         - optional. Name of the codeSystem to retrieve (codeSystem/@name)
@param $objected         - optional. null gets all versions, 'dynamic' gets the newest version based on id or name, yyyy-mm-ddThh:mm:ss gets this specific version
@param $max              - optional. Maximum number of results with minimum 1. Default is $isapi:maxResults (50)
@param $resolve          - optional. Default = 'true' If true, resolves any references
@return List object with zero or more codeSystem
@since 2013-06-14
:)
declare %private function csapi:getCodeSystemList($governanceGroupId as xs:string?, $projectPrefix as xs:string?, $projectVersion as xs:string?, $projectLanguage as xs:string?, $searchTerms as xs:string*, $objected as xs:string?, $includebbr as xs:boolean, $sort as xs:string?, $sortorder as xs:string?, $max as xs:integer?, $resolve as xs:boolean) as element(list) {
    
    let $objected               := $objected[not(. = '')]
    let $governanceGroupId      := $governanceGroupId[not(. = '')]
    let $projectPrefix          := $projectPrefix[not(. = '')]
    let $projectVersion         := $projectVersion[not(. = '')]
    let $projectLanguage        := ($projectLanguage[not(. = '')], '*')[1]
    let $sort                   := $sort[string-length() gt 0]
    let $sortorder              := $sortorder[. = 'descending']
    
    let $startT                 := util:system-time()
    
    let $decor                  := 
        if ($governanceGroupId) then
            for $projectId in ggapi:getLinkedProjects($governanceGroupId)/@ref
            return
                utillib:getDecorById($projectId)
        else
        if (utillib:isOid($projectPrefix)) then
            utillib:getDecorById($projectPrefix, $projectVersion, $projectLanguage)
        else (
            utillib:getDecorByPrefix($projectPrefix, $projectVersion, $projectLanguage)
        )
    let $projectPrefix          := ($decor/project/@prefix)[1]
    
    let $results                := 
        if (empty($searchTerms)) then (
            $decor//codeSystem
        )
        else (
            let $buildingBlockRepositories  := 
                if ($includebbr) then (
                    utillib:getBuildingBlockRepositories($decor, (), $utillib:strDecorServicesURL)
                )
                else (
                    $decor
                )
            let $luceneQuery                := utillib:getSimpleLuceneQuery($searchTerms, 'wildcard')
            let $luceneOptions              := utillib:getSimpleLuceneOptions()

            (:browsable:      draft, active,   retired, rejected, pending/review, cancelled :)
            (:decor    : new, draft, final, deprecated, rejected, pending,        cancelled :)
            let $cadtsSystems               := csapi:getBrowsableCodeSystemMeta(collection($setlib:strCodesystemStableData || '/external')/browsableCodeSystem[@oid[. = $searchTerms] | ft:query(@id | name, $luceneQuery, $luceneOptions)])
            let $decorSystems               := 
                for $ob in ($buildingBlockRepositories//codeSystem[@id = $searchTerms] | 
                            $buildingBlockRepositories//codeSystem[@id][ft:query(@name | @displayName, $luceneQuery, $luceneOptions)])
                return
                    <codeSystem>
                    {
                        $ob/(@* except (@url | @ident | @cachedProject)),
                        attribute url {($ob/ancestor::cacheme/@bbrurl, $utillib:strDecorServicesURL)[1]},
                        attribute ident {$ob/ancestor::decor/project/@prefix},
                        attribute cachedProject {exists($ob/ancestor::cacheme)},
                        $ob/node()
                    }
                    </codeSystem>
            
            return (
                $cadtsSystems | $decorSystems
            )
        )
    
    let $allcnt                 := count($results)
    
    let $objectsByRef           :=
        if ($resolve and empty($projectVersion)) then (
            let $decors                 := 
                if (empty($projectVersion)) then utillib:getBuildingBlockRepositories($decor, (), $utillib:strDecorServicesURL) else $decor
            
            for $csref in $results/@ref
            group by $csref
            return (
                let $css    := $decors//codeSystem[@id = $csref]
                (:let $css    := 
                    if ($css) then $css else 
                        csapi:getBrowsableCodeSystemMeta(collection($setlib:strCodesystemStableData || '/external')/browsableCodeSystem[@oid = $csref]):)
                let $csbyid := $css[@effectiveDate = max($css[@effectiveDate castable as xs:dateTime]/xs:dateTime(@effectiveDate))]
                (: rewrite name and displayName based on latest target codeSystem. These sometimes run out of sync when the original changes its name :)
                return (
                    element {name($csref[1])} {
                        $csref[1], 
                        attribute name {($csbyid/@name, $csref/../@name)[1]}, 
                        attribute displayName {($csbyid/@displayName, $csref/../@displayName, $csbyid/@name, $csref/../@name)[1]},
                        attribute url {$utillib:strDecorServicesURL},
                        attribute ident {($csref/ancestor::decor/project/@prefix)[1]}
                    }
                    ,
                    for $csid in $css
                    return
                    element {name($csid)} {
                        $csid/@*,
                        if ($csid/ancestor::decor) then (
                            if ($csid/@url) then () else   attribute url {($csid/ancestor::cacheme/@bbrurl, $utillib:strDecorServicesURL)[1]},
                            if ($csid/@ident) then () else attribute ident {($csid/ancestor::decor/project/@prefix)[1]}
                        )
                        else (),
                        (: this element is not supported (yet?) :)
                        $csid/classification
                    }
                )
            )
        )
        else (
            $results[@ref]
        )
    
    let $results            := $results[@id] | $objectsByRef
    
    (: now we can determine $objected :)
    let $results            :=
        if (empty($objected)) then 
            (: all :)
            $results
        else
        if ($objected castable as xs:dateTime) then
            (: match and ref :)
            $results[@ref] | $results[@effectiveDate = $objected]
        else (
            (: newest and ref :)
            $results[@ref] | $results[@effectiveDate = max($results[@effectiveDate]/xs:dateTime(@effectiveDate))]
        )
    
    let $results            :=
        for $res in $results
        let $id             := $res/@id | $res/@ref
        group by $id
        return (
            let $subversions    :=
                for $resv in $res
                order by $resv/@effectiveDate descending
                return
                    <codeSystem uuid="{util:uuid()}">
                    {
                        $resv/(@* except @uuid),
                        (: this element is not supported (yet?) :)
                        $resv/classification
                    }
                    </codeSystem>
            let $latest         := ($subversions[@id], $subversions)[1]
            let $rproject       := $res/ancestor::decor/project/@prefix
            return
            <codeSystem uuid="{util:uuid()}" id="{$id}">
            {
                $latest/(@* except (@uuid | @id | @ref | @project)),
                ($res/@ref)[1],
                (: there is no attribute @project, but better safe than sorry :)
                (: wrong setting for governance group search: if (empty($governanceGroupId)) then $latest/@project else attribute project {$projectPrefix}, :)
                if (empty($governanceGroupId)) then $latest/@project else attribute project { $rproject[1] },
                $subversions
            }
            </codeSystem>
        )
    let $count              := count($results/codeSystem)
    let $max                := if ($max ge 1) then $max else $count
    
    (: handle sorting. somehow reverse() does not do what I expect :)
    let $results            :=
        switch ($sort)
        case 'displayName' return 
            if ($sortorder = 'descending') then 
                for $r in $results order by $r/lower-case(@displayName) descending return $r
            else (
                for $r in $results order by $r/lower-case(@displayName)            return $r
            )
        case 'name'        return 
            if ($sortorder = 'descending') then 
                for $r in $results order by $r/lower-case(@name) descending return $r
            else (
                for $r in $results order by $r/lower-case(@name)            return $r
            )
        default            return 
            if ($sortorder = 'descending') then 
                for $r in $results order by replace(replace($r/@id, '\.', '.0000000000'), '.0*([0-9]{9,})', '.$1') descending return $r
            else (
                for $r in $results order by replace(replace($r/@id, '\.', '.0000000000'), '.0*([0-9]{9,})', '.$1')            return $r
            )
    
    let $durationT := (util:system-time() - $startT) div xs:dayTimeDuration("PT0.001S")
    
    return
        <list artifact="CS" elapsed="{$durationT}" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" resolve="{$resolve}" project="{$projectPrefix}" lastModifiedDate="{current-dateTime()}">
        {
            subsequence($results, 1, $max)
        }
        </list>
};

(:~ Retrieves the logo for a DECOR codeSystem based on $org denoting the Terminology Governance Group identifier string, such as "who" or "rsna" etc.
    @param $org                     - required parameter denoting the id string of the Terminology Governance Group
    @return governance group logo
    @since 2022-03-16
:)
declare function csapi:getTerminologyGoveranceGroupLogo($request as map(*)) {
    
    (: get the Terminology Governance Group identifier :)
    let $group         := $request?parameters?org[string-length() gt 0]
    
    let $check         :=
        if (empty($group[not(. = '')])) then
            error($errors:BAD_REQUEST, 'Missing required parameter org')
        else ()
    
    let $check         :=
        if ($group = ('adot', 'bfarm', 'hl7', 'hpo', 'iso', 'orphanet', 'regenstrief', 'rivm', 'rsna', 'snomed', 'who'))
        then ()
        else error($errors:BAD_REQUEST, 'Illegal value of parameter org')
        
    (: for now only default logos are returned with 40px in height :)
    let $defaultln     := "logo40.png"
    let $logosrc       := concat($setlib:strTerminologyLogo, '/', $group, '/', $defaultln)
    let $logofilename  := concat($group, '-', $defaultln)
    
    return
        if (util:binary-doc-available($logosrc)) then (
            (: instruct client to cache 7 days. which is 7 * 24 * 60 * 60 = 604800 seconds :)
            response:set-header("Cache-Control", "max-age=604800"),
            response:stream-binary(util:binary-doc($logosrc), 'image/png', $logofilename)
        ) else
            roaster:response(404, (), (), map { "Location": xs:anyURI($logosrc) })
};

declare function csapi:getBrowsableCodeSystemMeta($browsableCodeSystems as element(browsableCodeSystem)*) as element(codeSystem)* {
    for $cs in $browsableCodeSystems
    let $id                     := ($cs/@oid | $cs/@ref)[1]
    let $ed                     := $cs/@effectiveDate
    group by $id, $ed
    return (
        let $theCs  := ($cs[@oid], $cs)[1]
        return
        <codeSystem>
        {
            attribute id {$id},
            attribute name {$theCs/@id},
            attribute displayName {$theCs/name[1]},
            attribute effectiveDate {if ($theCs/@effectiveDate[. castable as xs:date]) then $theCs/@effectiveDate[. castable as xs:date][1] || 'T00:00:00' else ($theCs/@effectiveDate)[1]},
            if ($theCs/@expirationDate) then attribute expirationDate {if ($theCs/@expirationDate[. castable as xs:date]) then $theCs/@expirationDate[. castable as xs:date][1] || 'T00:00:00' else $theCs/@expirationDate} else (),
            if ($theCs/@version) then attribute versionLabel {$theCs/@version} else (),
            switch ($theCs/@statusCode)
            case 'active' return attribute statusCode {'final'}
            case 'review' return attribute statusCode {'pending'}
            default return attribute statusCode {$theCs/@statusCode}
            ,
            $theCs/@experimental
        }
        </codeSystem>
    )
};