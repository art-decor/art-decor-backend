xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

(:~ News API allow retrieval, creation, updating of news items. For example about new features, upcoming maintenance windows etc. :)
module namespace newsapi            = "http://art-decor.org/ns/api/news";

import module namespace roaster     = "http://e-editiones.org/roaster";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "library/settings-lib.xqm";
import module namespace utillib     = "http://art-decor.org/ns/api/util" at "library/util-lib.xqm";
import module namespace serverapi   = "http://art-decor.org/ns/api/server" at "server-api.xqm";

declare namespace json      = "http://www.json.org";

declare variable $newsapi:NEWS-TYPE       := ('error', 'info', 'warning');

(:~ Retrieves latest DECOR news (urgent news)

@return news as JSON
@author ART-DECOR® Expert Group
@see https://art-decor.org
@since 2020-07-10
@error The requested URI does not exist
:)
declare function newsapi:getNews($request as map(*)) {
    let $timeStamp          := current-dateTime()
    let $urgentnews         := doc($setlib:strUrgentnews)/*
    let $results            := $urgentnews/news[empty(@showuntil)] | $urgentnews/news[@showuntil castable as xs:dateTime][xs:dateTime(@showuntil) gt $timeStamp]
    
    let $count              := count($results)
    let $max                := $count
    return
        <list artifact="NEWS" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$count}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements(subsequence($results, 1, $max))
        }
        </list>
};

(:~ Create DECOR news (urgent news)

@return news item
@author ART-DECOR® Expert Group
@see https://art-decor.org
@since 2021-02-19
@error The news record cannot be created
:)
declare function newsapi:postNewsItem($request as map(*)) {
    
    let $authmap                := $request?user
    let $data                   := utillib:getBodyAsXml($request?body, 'news', ())
        
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else if ($authmap?groups = 'dba') then ()
        else (
            error($errors:FORBIDDEN, 'You need to be dba for this feature')
        )
    let $check                  :=
        if (empty($data)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $return                 := newsapi:createUpdateNewsItem($authmap, (), $data)
    return (
        roaster:response(201, $return)
    )
};

(:~ Update DECOR news (urgent news)
@return news item
@author ART-DECOR® Expert Group
@see https://art-decor.org
@since 2021-02-19
@error The news record cannot be created
:)
declare function newsapi:putNewsItem($request as map(*)) {
    
    let $authmap                := $request?user
    let $newsid                 := $request?parameters?id
    let $data                   := utillib:getBodyAsXml($request?body, 'news', ())
        
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else if ($authmap?groups = 'dba') then ()
        else (
            error($errors:FORBIDDEN, 'You need to be dba for this feature')
        )
    let $check                  :=
        if (empty($newsid)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have parameter id')
        else
        if (empty($data)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $return                 := newsapi:createUpdateNewsItem($authmap, $newsid, $data)
    return (
        roaster:response(200, $return)
    )
};

(:~ Delete DECOR news (urgent news)
@return news item
@author ART-DECOR® Expert Group
@see https://art-decor.org
@since 2021-02-19
@error The news record cannot be created
:)
declare function newsapi:deleteNewsItem($request as map(*)) {
    
    let $authmap                := $request?user
    let $newsid                 := $request?parameters?id
        
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else if ($authmap?groups = 'dba') then ()
        else (
            error($errors:FORBIDDEN, 'You need to be dba for this feature')
        )
    let $check                  :=
        if (empty($newsid)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have parameter id')
        else ()
    
    let $return                 := newsapi:deleteNewsItem($authmap, $newsid)
    return (
        roaster:response(204, $return)
    )
};

(:~ Central logic for creating news

@param $data         - required. DECOR news object as xml
@return news object as xml with json:array set on elements
:)
declare function newsapi:createUpdateNewsItem($authmap as map(*)?, $id as xs:string?, $data as element(news)) as element(news) {

    let $check                  :=
        if (empty($id)) then () else if ($id = $data/@id) then () else (
            error($errors:BAD_REQUEST, 'Submitted data id ' || $data/@id || ' SHALL NOT conflict with path id ' || $id)
        )
    let $check                  :=
        if (empty($data/@type) or $data[@type = $newsapi:NEWS-TYPE]) then () else (
            error($errors:BAD_REQUEST, 'Submitted data has unsupported type ' || $data/@type ||'. Type SHALL be one of ' || string-join($newsapi:NEWS-TYPE, ', '))
        )
    let $check                  :=
        if ($data/text[.//text()[not(normalize-space() = '')]]) then () else (
            error($errors:BAD_REQUEST, 'Submitted data SHALL have at least one text element containing the news text')
        )
    
    let $urgentnews             := doc($setlib:strUrgentnews)/*
    let $storedNews             := $urgentnews/news[@id = $id]
    
    let $check                  :=
        if (empty($id)) then () else if ($storedNews) then () else (
            error($errors:BAD_REQUEST, concat('News item with id  ''', $id, ''' does not exist so cannot update'))
        )
        
    let $serverLanguage         := serverapi:getServerLanguage()
    let $newNews                :=
        <news>
        {
            if ($data/@showuntil[. castable as xs:dateTime]) then $data/@showuntil else (
                attribute showuntil {current-dateTime() + xs:dayTimeDuration('P30D')}
            ),
            attribute id {($id, util:uuid())[1]},
            attribute type {if ($data/@type) then $data/@type else 'info'}
        }
        {
           for $node in $data/text
           let $language  := ($node/@language[not(. = '')], $serverLanguage)[1]
           return 
                <text language="{$language}">{$node/node()}</text>
        }
        </news>

    let $update                 :=
        if ($storedNews) then 
            update replace $storedNews with $newNews 
        else (
            update insert $newNews into $urgentnews
        )

    return
        element {name($newNews)} {
            $newNews/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($newNews/*)
        }
};

(:~ Central logic for deleting a news items. If the item does not exist, then no error is given.

@param $id                      - required. News item id to delete
@return nothing
:)
declare function newsapi:deleteNewsItem($authmap as map(*)?, $id as xs:string) {
    let $update                 := update delete doc($setlib:strUrgentnews)/*/news[@id = $id]
    
    return
        ()
};