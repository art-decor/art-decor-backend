xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:~ Server API allows read, create, update of ART-DECOR Server properties :)
module namespace servermgmtapi             = "http://art-decor.org/ns/api/server-mgmt";

import module namespace roaster     = "http://e-editiones.org/roaster";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

import module namespace utillib     = "http://art-decor.org/ns/api/util" at "library/util-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "library/settings-lib.xqm";
import module namespace permlib     = "http://art-decor.org/ns/api/permissions" at "library/permissions-lib.xqm";
import module namespace serverapi   = "http://art-decor.org/ns/api/server" at "server-api.xqm";
import module namespace claml       = "http://art-decor.org/ns/terminology/claml" at "../../terminology/claml/api/api-claml.xqm";
import module namespace decor-cache = "http://art-decor.org/ns/api/decor-cache" at "library/decor-cache.xqm";

(: TODO refactor references to art modules? :)
import module namespace adpfix      = "http://art-decor.org/ns/art-decor-permissions" at "../../art/api/api-permissions.xqm";

declare namespace http              = "http://expath.org/ns/http-client";
declare namespace json              = "http://www.json.org";
declare namespace expath            = "http://expath.org/ns/pkg";
declare namespace scheduler         = "http://exist-db.org/xquery/scheduler";

(:~ Update permissions related to ART and DECOR package (e.g. projects). Require dba authentication
@return TODO
@since 2020-05-03
@see https://docs.art-decor.org/administration/#fix-art-permissions
:)
declare function servermgmtapi:postUpdatePermissionsArt($request as map(*)) {
    (:let $dummy := util:log('INFO', 'servermgmtapi:postUpdatePermissionsArt()'):)
    let $authmap                        := $request?user
    
    let $check := servermgmtapi:checkAuthentication($authmap)

    let $fix    := adpfix:setArtPermissions()
    let $fix    := adpfix:setDecorPermissions()
    
    return <result dateTime='{current-dateTime()}'/>
};

(:~ Clean up /db/apps/decor/tmp and /db/apps/decor/releases/*/development so only the latest 3 compilations per project are left. Require dba authentication
@return TODO
@since 2020-05-03
@see https://docs.art-decor.org/administration/#clean-up-decor
:)
declare function servermgmtapi:postCleanUpDecor($request as map(*)) {
    (:let $dummy := util:log('INFO', 'servermgmtapi:postUpdatePermissionsArt()'):)
    let $authmap                        := $request?user
    
    let $check := servermgmtapi:checkAuthentication($authmap)

    let $fix    := servermgmtapi:cleanupDecorTmp()
    let $fix    := servermgmtapi:cleanupDevelopmentCompilations()
    
    return <result dateTime='{current-dateTime()}'/>
};

(:~ Clean up compilations in /db/apps/decor/tmp if they are not the latest compile for the project. Require dba authentication
@return TODO
@since 2023-11-08
@see https://docs.art-decor.org/administration/#clean-up-decor
:)
declare %private function servermgmtapi:cleanupDecorTmp() {
    let $cleanup    :=
        for $decors in collection($setlib:strDecorTemp)/compile[@compileDate castable as xs:dateTime][@compilationFinished castable as xs:dateTime]
        let $projectPrefix  := $decors/decor/project/@prefix
        group by $projectPrefix
        return (
            let $newest := max($decors/xs:dateTime(@compileDate))
            
            return (
                for $decor in $decors[not(@compileDate = string($newest))]
                let $dummy := util:log('INFO', 'Removing out-dated pre-compiled set for "' || $projectPrefix || '"...')
                return (
                    xmldb:remove(util:collection-name($decor), util:document-name($decor))
                )
            )
        )
    return $cleanup
};

(:~ Clean up all but the newest 3 project runtime compilations in /db/apps/decor/releases/*/development. Require dba authentication
@return TODO
@since 2023-11-08
@see https://docs.art-decor.org/administration/#clean-up-decor
:)
declare %private function servermgmtapi:cleanupDevelopmentCompilations() as element(compiled)* {
    for $projectPrefix in xmldb:get-child-collections($setlib:strDecorVersion)
    let $compilations   := 
        for $compilation in $setlib:colDecorVersion//compiled[@for = concat($projectPrefix, '-')][ancestor::compilation]
        order by $compilation/@on descending
        return $compilation
    let $ocount     := count(subsequence($compilations, 4))
    let $dummy := util:log('INFO', 'Removing ' || $ocount || ' out-dated project runtime compilations for "' || $projectPrefix || '"...')
    order by lower-case($projectPrefix)
    return (
        for $ref in subsequence($compilations, 4)/@as
        let $c          := <compiled>{$ref/../@*}</compiled>
        let $del        := 
            try {
                xmldb:remove(concat(util:collection-name($ref), '/', $ref)),
                xmldb:remove(util:collection-name($ref), util:document-name($ref))
            }
            catch * {$c/@as, $err:code, $err:description}
        order by $c/@on
        return
            <compiled>{$c/@*}</compiled>
    )
};

(:~ Update permissions related to FHIR package. Note that FHIR package may not be installed and versions 1.0, 3.0, 4.0 and more may exist. Require dba authentication
@return TODO
@since 2020-05-03
:)
declare function servermgmtapi:postUpdatePermissionsFhir($request as map(*)) {
    (:let $dummy := util:log('INFO', 'servermgmtapi:postUpdatePermissionsFhir()'):)
    let $authmap := $request?user
    let $check := servermgmtapi:checkAuthentication($authmap)
    
    let $version := string($request?parameters?version)
    let $operationType := string($request?parameters?type)
    
    let $allFhirVersions as xs:string* := if ($version eq '') then serverapi:getInstalledFhirServices() else $version
    
    (: don't rewrap errors deeper down. They should be self-explanatory :)
    let $results :=
        for $fhirVersion in $allFhirVersions
        return servermgmtapi:performUpdatePermissionsFhir($fhirVersion, $operationType)
    
    return <result dateTime="{current-dateTime()}"/>
};

(:~ Update permissions related to Terminology package. Note that the package may not be installed. Require dba authentication
@return TODO
@since 2020-05-03
:)
declare function servermgmtapi:postUpdatePermissionsTerminology($request as map(*)) {
    (:let $dummy := util:log('INFO', 'servermgmtapi:postUpdatePermissionsTerminology()'):)
    
    let $authmap                        := $request?user
    
    let $check := servermgmtapi:checkAuthentication($authmap)
    
    let $moduleNS := xs:anyURI('http://art-decor.org/ns/terminology-permissions')
    let $modulePaths := (repo:get-root() || 'terminology/api/api-permissions.xqm')
    
    (:let $dummy := util:log('INFO', 'Dynamically loading module with namespace "' || $moduleNS || '" and paths ' || string-join($modulePaths, ', ')):)
    
    let $module := load-xquery-module($moduleNS, map{'location-hints': $modulePaths})
    
    (:let $dummy := util:log('INFO', 'Module with namespace "' || $moduleNS || '" loaded succesfully'):)
    
    let $function1 := servermgmtapi:function-lookup($module, QName($moduleNS, 'setTerminologyQueryPermissions'), 0)
    let $function2 := servermgmtapi:function-lookup($module, QName($moduleNS, 'setTerminologyAuthoringCollectionPermissions'), 0)
    
    (: Instead of checking if a function is available we catch the server error if it happens and inform the user like we do if the functions exists but throws an error. :)
    let $result := $function1()
    let $result := ($result, $function2())
    
    return <result dateTime="{current-dateTime()}"/>
};

(:~ Update permissions related to Oids package. Note that the package may not be installed. Require dba authentication
@return TODO
@since 2020-05-03
:)
declare function servermgmtapi:postUpdatePermissionsOids($request as map(*)) {
    (:let $dummy := util:log('INFO', 'servermgmtapi:postUpdatePermissionsOids()'):)
    
    let $authmap                        := $request?user
    
    let $check := servermgmtapi:checkAuthentication($authmap)
    
    let $moduleNS := xs:anyURI('http://art-decor.org/ns/oids/permissions')
    let $modulePaths := (repo:get-root() || 'tools/oids/api/api-permissions.xqm')
    
    (:let $dummy := util:log('INFO', 'Dynamically loading module with namespace "' || $moduleNS || '" and paths ' || string-join($modulePaths, ', ')):)
    
    let $module := load-xquery-module($moduleNS, map{'location-hints': $modulePaths})
    
    (:let $dummy := util:log('INFO', 'Module with namespace "' || $moduleNS || '" loaded succesfully'):)
    
    let $function := servermgmtapi:function-lookup($module, QName($moduleNS, 'setOidsPermissions'), 0)
    
    (: Instead of checking if a function is available we catch the server error if it happens and inform the user like we do if the functions exists but throws an error. :)
    let $result := $function()
    
    return <result dateTime="{current-dateTime()}"/>
};

(:~ Retrieve server-functions xml containing CRUD markers for UI-functionality:)
declare function servermgmtapi:getServerFunctions($request as map(*)) {
    
    let $results := doc($setlib:strServerFunctions)/server-functions

    return 
    for $result in $results
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

(:~ Update one or all OID Registries lookup files. Note that the package may not be installed. Note that this function may need to move to a separate set of OID functions. Require dba authentication
@return TODO
@since 2020-05-03
:)
declare function servermgmtapi:postUpdateOidLookups($request as map(*)) {
    let $moduleNS := xs:anyURI('http://art-decor.org/ns/tools/oids')
    let $modulePaths := (repo:get-root() || 'tools/oids/api/api-oids.xqm')
    
    let $authmap                        := $request?user
    let $statusonly                     := $request?parameters?statusonly = true()
    let $force                          := $request?parameters?force = true()
    let $registryname                   := $request?parameters?registryname[string-length() gt 0]
    
    (:let $dummy := util:log('INFO', 'servermgmtapi:postUpdatePermissionsOids(), force="' || $force || '", registryname="' || $registryname || '"'):)
    
    let $check := servermgmtapi:checkAuthentication($authmap)
    
    (:let $dummy := util:log('INFO', 'Dynamically loading module with namespace "' || $moduleNS || '" and paths ' || string-join($modulePaths, ', ')):)
    
    let $module := load-xquery-module($moduleNS, map{'location-hints': $modulePaths})
    
    (:let $dummy := util:log('INFO', 'Module with namespace "' || $moduleNS || '" loaded succesfully'):)
    
    (: Instead of checking if a function is available we catch the server error if it happens and inform the user like we do if the functions exists but throws an error. :)
    let $function := servermgmtapi:function-lookup($module, QName($moduleNS, 'createOidRegistriesLookup'), 2)
    
    let $results := $function($registryname, $force)
    
    (: $result created by this call: <result dateTime="{current-dateTime()}">{$lookupFiles}</result> :)
    for $result in $results
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

(:~ Update classification index. Require dba authentication
@return result object with index, codeSystems count and last execution time
@since 2020-05-03
:)
declare function servermgmtapi:postUpdateClassificationIndex($request as map(*)) {
    (:let $dummy := util:log('INFO', 'servermgmtapi:postUpdateClassicationIndex()'):)
    let $authmap                        := $request?user
    let $statusonly                     := $request?parameters?statusonly = true()
    
    let $check                          := servermgmtapi:checkAuthentication($authmap)
    (: sample result:
      <result index="/db/apps/terminology/claml/classification-index.xml" codeSystems="0" time="2021-06-01T00:15:00+02:00"/>
    :)
    let $results                       := claml:createClaMLIndex($statusonly)
     
    for $result in $results
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

(:~ Update cache of DECOR projects. Require dba authentication
@return cachedBuildingBlockRepositories object
@since 2020-05-03
:)
declare function servermgmtapi:postUpdateDecorCache($request as map(*)) {
    let $authmap                        := $request?user
    
    let $statusonly                     := $request?parameters?statusonly = true()
    let $cacheformat                    := ($request?parameters?cacheformat[string-length() gt 0], 'decor')[1]
    
    (:let $dummy := util:log('INFO', 'servermgmtapi:postUpdateDecorCache(), user="' || $authmap?name || '", statusonly="' || $statusonly || '", cacheformat="' || $cacheformat || '"'):)
    
    let $check                          := servermgmtapi:checkAuthentication($authmap)
    let $results                        := decor-cache:updateDecorCache($authmap, $statusonly, $cacheformat)
    
    (: update the CADTS systems :)
    let $now                    := substring(string(current-dateTime()), 1, 19)
    let $stamp                          := util:uuid()
    let $projectcsconvert-request       :=  
        <projectcodesystemconvert-request uuid="{$stamp}" for="*cache*" on="{current-dateTime()}" as="{$stamp}" by="{$authmap?name}"
                                          progress="Added to process queue ..." progress-percentage="0"/>
    let $write                          := xmldb:store($setlib:strDecorScheduledTasks, 'projectcodesystemconvert--cache--' || replace($now, '\D', '') || '.xml', $projectcsconvert-request)
    let $tt                             := sm:chmod($write, 'rw-rw----')
    (:/ update the CADTS systems :)
    
    for $result in $results
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

(:~ Check for dba authentication.
@param $authmap map with user name, groups and isdba boolean
@return empty sequence if authentication OK, else the error() function is called.
@since 2021-05-19
:)
declare %private function servermgmtapi:checkAuthentication($authmap as map(*)?) {
    if (empty($authmap)) then
        error($errors:UNAUTHORIZED, 'You need to authenticate first')
    else
    if ($authmap?groups = 'dba') then () else (
        error($errors:FORBIDDEN, 'You need to be dba for this function')
    )
};

(:~ A wrapper around utillib:function-lookup() that throws a SERVER_ERROR, if the function is not found. :)
declare %private function servermgmtapi:function-lookup($module as map(*), $qname as xs:QName, $arity as xs:nonNegativeInteger) as function(*)? {
    let $function := utillib:function-lookup($module, $qname, $arity)
    return
       if (exists($function))
       then $function
       else error($errors:SERVER_ERROR, 'Dynamically loaded function not found, function "' || $qname || '", arity ' || $arity)
};

(:~ Auxiliary function that performs the required operation for the public calling function servermgmtapi:postUpdatePermissionsFhir().
    @return the result of the operation corresponding to the required operation
    @since 2021-05-20
:)
declare %private function servermgmtapi:performUpdatePermissionsFhir($fhirVersion as xs:string, $operationType as xs:string) {
    (:let $dummy := util:log('INFO', 'servermgmtapi:performUpdatePermissionsFhir(), version="' || $fhirVersion || '", type="' || $operationType || '"'):)
    
    let $allowedVersions as xs:string*    := serverapi:getInstalledFhirServices()
    let $check :=
        if ($fhirVersion = $allowedVersions) then () else
        if (empty($allowedVersions)) then 
            error($errors:BAD_REQUEST, 'This server currently has no FHIR servers installed, hence there are no permissions to update.')
        else (
            error($errors:BAD_REQUEST, 'This server currently does not have FHIR server version ' || $fhirVersion || ' installed. Installed version(s): ' || string-join($allowedVersions, ', '))
        )
        
    let $moduleNS := xs:anyURI('http://art-decor.org/ns/fhir-permissions')
    let $baseFhirModulePath := repo:get-root() || 'fhir/' || $fhirVersion || '/api/'
    let $modulePaths := ($baseFhirModulePath || 'api-permissions.xqm')
    
    (:let $dummy := util:log('INFO', 'Dynamically loading module with namespace "' || $moduleNS || '" and paths ' || string-join($modulePaths, ', ')):)
    
    let $module := load-xquery-module($moduleNS, map{'location-hints': $modulePaths})
    
    (:let $dummy := util:log('INFO', 'Module with namespace "' || $moduleNS || '" loaded succesfully'):)
    
    let $adfixNS := 'http://art-decor.org/ns/fhir-permissions'
    
    let $fhirFunction :=
        switch ($operationType)
        case ('capabilitystatement') return servermgmtapi:function-lookup($module, QName($adfixNS, 'refreshCapabilityStatement'), 0)
        case ('conformance') return servermgmtapi:function-lookup($module, QName($adfixNS, 'refreshCapabilityStatement'), 0)
        case ('cleanup') return servermgmtapi:function-lookup($module, QName($adfixNS, 'cleanupData'), 0)
        default return servermgmtapi:function-lookup($module, QName($adfixNS, 'setPermissions'), 0)
    
    return $fhirFunction()
};

(:~ Function to retrieve the details of all scheduled jobs of the eXist db. 
    Please note that you must be a dba to call this function. 
    The return structure is very similar to the origin, see 
    https://exist-db.org/exist/apps/fundocs/view.html?uri=http://exist-db.org/xquery/scheduler
    but has no namespace anymore, summarizes data and returns ART-DERCOR related scheduled-jobs only.
    @return scheduler structure as list of jobs focus on ART-DERCOR related scheduled-jobs
    @since 2022-07-04
:)
declare function servermgmtapi:getSchedulers($request as map(*)) {
    let $authmap  := $request?user
    let $check    := servermgmtapi:checkAuthentication($authmap)
    
    let $recognizedartdecortasks :=
        ('scheduled-notifier', 'scheduled-refreshs', 'scheduled-tasks', 'periodic-notifier')
        
    let $sj       := scheduler:get-scheduled-jobs()
    let $pj       := collection($setlib:strDecorScheduledTasks)/*[@for]
    
    let $results  :=
    (
        for $j in $sj/scheduler:jobs//scheduler:group/scheduler:job[@name = $recognizedartdecortasks]
            return
                <artdecorjob name="{$j/@name}" json:array="true">
                {   
                    for $t in $j/scheduler:trigger
                    return
                        <trigger>
                        {
                            attribute expression { $t/expression },
                            attribute state { $t/state },
                            attribute start { $t/start },
                            attribute end { $t/end },
                            attribute previous { $t/previous },
                            attribute next { $t/next },
                            attribute final { $t/final }
                        }
                        </trigger>
                }
                </artdecorjob>,
        for $p in $pj
            return
                <projectjob name="{$p/name()}" json:array="true">
                {
                    $p/(@* except @name)
                }
                </projectjob>
    )

    let $count := $results/*

    return
        <list artifact="ALLSCHEDULEDTASKS" current="{$count}" total="{$count}" all="{$count}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org"> 
        {
            $results
        }
        </list>
};
