xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:~ User API allows read, create, update of ART-DECOR server user :)
module namespace userapi                 = "http://art-decor.org/ns/api/user";

import module namespace roaster         = "http://e-editiones.org/roaster";
import module namespace errors          = "http://e-editiones.org/roaster/errors";

import module namespace setlib          = "http://art-decor.org/ns/api/settings" at "library/settings-lib.xqm";
import module namespace utillib         = "http://art-decor.org/ns/api/util" at "library/util-lib.xqm";
import module namespace decorlib        = "http://art-decor.org/ns/api/decor" at "library/decor-lib.xqm";
import module namespace serverapi       = "http://art-decor.org/ns/api/server" at "server-api.xqm";

declare namespace request     = "http://exist-db.org/xquery/request";
declare namespace xmldb       = "http://exist-db.org/xquery/xmldb";
declare namespace xs          = "http://www.w3.org/2001/XMLSchema";
declare namespace sub         = "http://art-decor.org/ns/art-decor-user-subscriptions";
declare namespace json        = "http://www.json.org";
declare namespace sm          = "http://exist-db.org/xquery/securitymanager";
declare namespace config      = "http://exist-db.org/Configuration";
declare namespace xis         = "http://art-decor.org/ns/xis";

(:~ All functions support their own override, but this is the fallback for the maximum number of results returned on a search :)
declare %private variable $userapi:maxResults     := 50;
(:~ Groups that are allowed to read and/or change properties of users that are not the currently logged in user :)
declare variable $userapi:editGroups              := ('decor-admin','dba','terminology');
(:~ The path to the user info file. Copied here so we can remove it from art-decor-settings.xqm :)
declare variable $userapi:strUserInfo             := $setlib:strUserInfo;
(:~ The collection contents of the user info. Copied here so we can remove it from art-decor-settings.xqm :)
declare variable $userapi:colUserInfo             := collection($userapi:strUserInfo);
(:~ To subscribe to certain issues, the type needs to be from this list. Supported types may be found in DECOR.xsd under simpleType DecorObjectType. 
    Special: #ALL (any issue), #NOOB (issues without objects), #ISAUTHOR (issues the user authored), #ISASSIGNED (issues the user is currently assigned to)
:)
declare variable $userapi:subALL                  := '#ALL';
declare variable $userapi:subNOOB                 := '#NOOB';
declare variable $userapi:subISAUTHOR             := '#ISAUTHOR';
declare variable $userapi:subISASSIGNED           := '#ISASSIGNED';
declare variable $userapi:arrSubscriptionTypes    := ($userapi:subALL,$userapi:subNOOB,$userapi:subISAUTHOR,$userapi:subISASSIGNED,doc($setlib:strDecorTypes)//DecorObjectType/enumeration/@value/string());
(:~ The default type of subscription for issues :)
declare variable $userapi:arrSubscriptionDefault  := ($userapi:subISAUTHOR,$userapi:subISASSIGNED);
(:~ Resource that holds all subscriptions :)
declare variable $userapi:strSubscriptionFile     := 'user-subscriptions.xml';
(:~ uri that the user full name is under in eXist-db :)
declare variable $userapi:uriPropFullName         := xs:anyURI('http://axschema.org/namePerson');
(:~ uri that the user description is under in eXist-db :)
declare variable $userapi:uriPropDescription      := xs:anyURI('http://exist-db.org/security/description');
(:~ list of test account group names :)
declare variable $userapi:xisTestAccounts         := if (xmldb:collection-available($setlib:strXisResources)) then doc($setlib:strTestAccounts)//xis:testAccount/@name else ();
(:~ list of users this api should never touch :)
declare variable $userapi:PROTECTEDUSERS          := ('admin', 'SYSTEM', 'eXide', 'guest', 'monex', 'nobody', 'packageservice');
(:~ list of groups that should never be a user :)
declare variable $userapi:PROTECTEDGROUPS         := ('ada-user', 'dba', 'decor', 'decor-admin', 'debug', 'editor', 'eXide', 'guest', 'issues', 'monex', 'packageservice', 'terminology', 'tools', 'xis');

(:~ Retrieves eXist-db user

@param $bearer-token            - optional. provides authorization for the user. The token, if available, should be on the X-Auth-Token HTTP Header
@param $username                - required. exact username
@return user object
@since 2020-08-11
:)
declare function userapi:getServerUser($request as map(*)) {
    
    let $authmap            := $request?user
    let $username           := ($request?parameters?name[string-length() gt 0], 'guest')[1]
    
    return
        if (sm:user-exists($username)) then (
            if ($username='SYSTEM' or not($authmap?groups = $userapi:editGroups or $authmap?name = $username or string($username) = 'guest')) then (
                error($errors:FORBIDDEN, 'Access to user ' || $username || ' is restricted.')
            )
            else (
                let $userDisplayName    := (userapi:getUserDisplayName($username), $username)[1]
                let $result             := userapi:userInfo($authmap, $username, $userDisplayName, true())
                
                return
                    element {name($result)} {
                        $result/@*,
                        namespace {"json"} {"http://www.json.org"},
                        utillib:addJsonArrayToElements($result/*)
                    }
            )
        )
        else (
            roaster:response(404, ())
        )
};

(:~ Retrieves eXist-db user list

@param $bearer-token            - optional. provides authorization for the user. The token, if available, should be on the X-Auth-Token HTTP Header
@param $searchString            - optional. glob style search for username
@param $searchActive            - optional. boolean to search explicitly for active|inactive users
@param $searchGroups            - optional. comma separated list of groups at least one of which a user should be part of
@param $searchCreation          - optional. by user creation date. yyyy-mm-dd that may be prefixed with gt (greater than), lt (smaller than), ge (greater or equal than), le (smaller than)
@param $searchLastLogin         - optional. by last login. yyyy-mm-dd that may be prefixed with gt (greater than), lt (smaller than), ge (greater or equal than), le (smaller than)
@param $sortorder               - optional. Sort order. Default is ascending. Only other option is descending.
@param $max                     - optional integer, maximum number of results
@return list structure with optional user array
@since 2020-08-11
:)
declare function userapi:getServerUserList($request as map(*)) {

    let $authmap            := $request?user
    let $searchString       := lower-case($request?parameters?search[string-length() gt 0][1])
    let $searchActive       := $request?parameters?active
    let $searchGroups       := $request?parameters?groups[string-length() gt 0]
    let $searchGroups       := if ($searchGroups) then tokenize($searchGroups, '\s') else ()
    let $searchCreation     := $request?parameters?creationdate[string-length() gt 0]
    let $searchLastLogin    := $request?parameters?logindate[string-length() gt 0]
    let $sortorder          := $request?parameters?sortorder[string-length() gt 0]
    let $max                := $request?parameters?max
    
    let $searchCRdate       := replace($searchCreation, '^[a-z]{2}', '')
    let $searchCRcomp       := replace($searchCreation, '^([a-z]{2}).*', '$1')
    let $searchLLdate       := replace($searchLastLogin, '^[a-z]{2}', '')
    let $searchLLcomp       := replace($searchLastLogin, '^([a-z]{2}).*', '$1')
    
    let $check              :=
        if (empty($authmap)) then
            error($errors:FORBIDDEN, 'Access to users is restricted to authenticated users.')
        else ()
    let $check              :=
        if (empty($searchCreation)) then () else if ($searchCRdate castable as xs:date) then () else (
            error($errors:BAD_REQUEST, 'Parameter creationdate SHALL contain/be a valid date yyyy-mm-dd. Found: ' || $searchCreation)
        )
    let $check              :=
        if (empty($searchLastLogin)) then () else if ($searchLLdate castable as xs:date) then () else (
            error($errors:BAD_REQUEST, 'Parameter logindate SHALL contain/be a valid date yyyy-mm-dd. Found: ' || $searchLastLogin)
        )
    
    (: reindex user-info.xml :)
    (:let $reindex            := 
        try {
            xmldb:reindex($userapi:strUserInfo)
        }
        catch * {
            ()
        }:)

    (: list-users() fails for non-dba users, i.e. must be logged in :)
    (:  when you are e.g. an decor-admin user you still need access to the user list to add authors to a project. Fallback onto user-info.xml. 
        Caveat: people who have been added through the eXist-db user manager will NOT show up here...:)
    let $accounts      := 
        try { 
            if ($authmap?groups = $userapi:editGroups) then sm:list-users()[not(. = 'SYSTEM')] else error($errors:UNAUTHORIZED)
        }
        catch * {
            distinct-values(('guest', $authmap?name))
        }

    (: By username :)
    let $results        := 
        if (empty($searchString)) then 
            $accounts 
        else 
        if (ends-with($searchString, "*")) then 
            $accounts[starts-with(lower-case(.), substring-before($searchString, '*'))]
        else (
            $accounts[contains(lower-case(.), $searchString)]
        )
        
     (: By active account :)
     let $results       := 
        if (empty($searchActive)) then $results else (
            for $username in $results
            return
                if (sm:is-account-enabled($username)) then $username else ()
        )
     
     (: By group membership :)
     let $results       :=
        if (empty($searchGroups)) then $results else (
            for $username in $results
            return
                if (sm:get-user-groups($username)[. = $searchGroups]) then $username else ()
        )
     
     (: By last login date :)
     let $results        :=
          if (empty($searchLastLogin)) then $results else (
              for $username in $results
              let $actualDate     := userapi:getUserLastLoginTime($username)
              let $actualDate     := if (empty($actualDate)) then () else xs:date(substring(string($actualDate), 1, 10))
              return
                  if (empty($actualDate)) then $username else (
                      switch ($searchLLcomp)
                      case 'gt' return if (xs:date($actualDate) gt xs:date($searchLLdate)) then $username else ()
                      case 'ge' return if (xs:date($actualDate) ge xs:date($searchLLdate)) then $username else ()
                      case 'lt' return if (xs:date($actualDate) lt xs:date($searchLLdate)) then $username else ()
                      case 'le' return if (xs:date($actualDate) le xs:date($searchLLdate)) then $username else ()
                      default return if (xs:date($actualDate) = xs:date($searchLLdate)) then $username else ()
                  )
          )
      
      (: By account creation date :)
      let $results        :=
          if (empty($searchCreation)) then $results else (
              for $username in $results
              let $actualDate     := userapi:getUserCreationDate($username)
              let $actualDate     := if (empty($actualDate)) then () else xs:date(substring(string($actualDate), 1, 10))
              return
                  if (empty($actualDate)) then $username else (
                      switch ($searchCRcomp)
                      case 'gt' return if (xs:date($actualDate) gt xs:date($searchCRdate)) then $username else ()
                      case 'ge' return if (xs:date($actualDate) ge xs:date($searchCRdate)) then $username else ()
                      case 'lt' return if (xs:date($actualDate) lt xs:date($searchCRdate)) then $username else ()
                      case 'le' return if (xs:date($actualDate) le xs:date($searchCRdate)) then $username else ()
                      default return if (xs:date($actualDate) = xs:date($searchCRdate)) then $username else ()
                  )
          )
    
    (: Do sorting and add details :)
    let $results        := 
        if (empty($sortorder)) then
            for $username in $results
            let $userDisplayName    := try { (userapi:getUserDisplayName($username), $username)[1] } catch * {$username}
            return
                userapi:userInfo($authmap, $username, $userDisplayName, false())
        else
        if ($sortorder = 'descending') then 
            for $username in $results
            let $userDisplayName    := try { (userapi:getUserDisplayName($username), $username)[1] } catch * {$username}
            order by lower-case($userDisplayName) descending
            return
                userapi:userInfo($authmap, $username, $userDisplayName, false())
        else
            for $username in $results
            let $userDisplayName    := try { (userapi:getUserDisplayName($username), $username)[1] } catch * {$username}
            order by lower-case($userDisplayName)
            return
                userapi:userInfo($authmap, $username, $userDisplayName, false())
    
    let $count              := count($results)
    let $allcnt             := count($accounts)
    (: Return user details for all users except SYSTEM :)
    let $max            := if ($max ge 1) then $max else $allcnt
    
    return
        <list artifact="USER" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements(subsequence($results, 1, $max))
        }
        </list>
};

(:~ Retrieves eXist-db group

@param $bearer-token            - optional. provides authorization for the user. The token, if available, should be on the X-Auth-Token HTTP Header
@param $name                    - required. exact group name
@return group object
@since 2020-08-11
:)
declare function userapi:getServerGroup($request as map(*)) {
    
    let $authmap            := $request?user
    let $groupname          := $request?parameters?name
    
    return
        if (empty($authmap)) then
            error($errors:FORBIDDEN, 'Access to groups is restricted to authenticated users.')
        else
        if (sm:group-exists($groupname)) then
            <group xmlns:json="http://www.json.org" userGroup="{sm:user-exists($groupname)}" testGroup="{exists($userapi:xisTestAccounts[. = $groupname])}">{$groupname}</group>
        else (
            roaster:response(404, ())
        )
};

(:~ Retrieves eXist-db group list

@param $bearer-token            - optional. provides authorization for the user. The token, if available, should be on the X-Auth-Token HTTP Header
@param $searchUser              - optional. username to retrieve groups for
@param $searchTest              - optional. boolean retrieve only groups in use as XIS test group
@param $sortorder               - optional. Sort order. Default is ascending. Only other option is descending.
@param $max                     - optional integer, maximum number of results
@return list structure with optional group array
@since 2020-08-11
:)
declare function userapi:getServerGroupList($request as map(*)) {

    let $authmap            := $request?user
    let $searchUser         := $request?parameters?username[string-length() gt 0][1]
    let $searchTest         := $request?parameters?testgroup
    let $sortorder          := $request?parameters?sortorder[string-length() gt 0]
    let $max                := $request?parameters?max
    
    let $groups             := 
        if (empty($authmap)) then
            error($errors:FORBIDDEN, 'Access to groups is restricted to authenticated users.')
        else
        if (empty($searchUser)) then sm:list-groups() else if (sm:user-exists($searchUser)) then sm:get-user-groups($searchUser) else ()

    (: Do sorting and add details :)
    let $results        := 
        if (empty($sortorder)) then
            for $group at $i in $groups
            let $userExists         := try { sm:user-exists($group) } catch * {false()}
            let $testAccountExists  := try { exists($userapi:xisTestAccounts[. = $group]) } catch * {false()}
            return
                if (empty($searchTest) or ($searchTest = $testAccountExists)) then  
                    <group userGroup="{$userExists}" testGroup="{$testAccountExists}">{$group}</group>
                else ()
        else
        if ($sortorder = 'descending') then 
            for $group at $i in $groups
            let $userExists         := try { sm:user-exists($group) } catch * {false()}
            let $testAccountExists  := try { exists($userapi:xisTestAccounts[. = $group]) } catch * {false()}
            order by lower-case($group) descending
            return
                if (empty($searchTest) or ($searchTest = $testAccountExists)) then  
                    <group userGroup="{$userExists}" testGroup="{$testAccountExists}">{$group}</group>
                else ()
        else
            for $group at $i in $groups
            let $userExists         := try { sm:user-exists($group) } catch * {false()}
            let $testAccountExists  := try { exists($userapi:xisTestAccounts[. = $group]) } catch * {false()}
            order by lower-case($group)
            return
                if (empty($searchTest) or ($searchTest = $testAccountExists)) then  
                    <group userGroup="{$userExists}" testGroup="{$testAccountExists}">{$group}</group>
                else ()
    
    let $count              := count($results)
    let $allcnt             := count($groups)
    (: Return user details for all users except SYSTEM :)
    let $max                := if ($max ge 1) then $max else $allcnt
    
    return
        <list artifact="GROUP" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements(subsequence($results, 1, $max))
        }
        </list>
};

(:~ Allows creation of a new user

@param $bearer-token            - required. provides authorization for the user. The token, if available, should be on the X-Auth-Token HTTP Header
@param $data                    - required. shall contain password and confirmPassword, shall contain username, which will be taken from the token otherwise
@return nothing
@since 2020-08-11
:)
declare function userapi:postServerUser($request as map(*)) {

    let $authmap                := $request?user
    let $data                   := utillib:getBodyAsXml($request?body)
    
    let $username               := $data/@name
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($data)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $update             := userapi:createUpdateServerUser($authmap, $data, true())
    let $result             := userapi:userInfo($authmap, $username, (), true())
    
    return
        roaster:response(201, 
            element {name($result)} {
                $result/@*,
                namespace {"json"} {"http://www.json.org"},
                utillib:addJsonArrayToElements($result/*)
            }
        )
};

(:~ Allows update of an existing user

@param $bearer-token            - required. provides authorization for the user. The token, if available, should be on the X-Auth-Token HTTP Header
@param $data                    - required. shall contain password and confirmPassword, may contain username, which will be taken from the token otherwise
@return nothing
@since 2020-08-11
:)
declare function userapi:putServerUser($request as map(*)) {

    let $authmap                := $request?user
    let $data                   := utillib:getBodyAsXml($request?body)
    
    let $username               := $data/@name
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($data)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $update             := userapi:createUpdateServerUser($authmap, $data, false())
    let $result             := userapi:userInfo($authmap, $username, (), true())
    
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

(:~ Allows a user password update

@param $bearer-token            - required. provides authorization for the user. The token, if available, should be on the X-Auth-Token HTTP Header
@param $data                    - required. shall contain password and confirmPassword, may contain username, which will be taken from the token otherwise
@return nothing
@since 2020-08-11
:)
declare function userapi:postServerUserPassword($request as map(*)) {

    let $authmap                := $request?user
    let $data                   := utillib:getBodyAsXml($request?body)
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($data)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $username               := if ($data/@username) then $data/@username else $authmap?name
    let $password               := if ($data[@password = @confirmPassword]) then $data/@password else ()
    
    let $ttl                    := '15'
    let $check                  :=
        if ($authmap?issued castable as xs:dateTime and current-dateTime() - xs:dateTime($authmap?issued)  lt xs:dayTimeDuration('PT' || $ttl || 'M')) then () else (
            error($errors:FORBIDDEN, 'Password update not accessible unless your token is issued in the last ' || $ttl || ' minutes. Please request a new token.')
        )
    let $check                  :=
        if (sm:is-dba($authmap?name)) then () else if ($authmap?name = $username) then () else (
            error($errors:FORBIDDEN, 'Password update not accessible unless you are dba or if you are changing your own password.')
        )
    let $check                  :=
        if (sm:user-exists($username)) then () else (
            error($errors:BAD_REQUEST, 'User does not exist.')
        )
    let $check                  :=
        if ($data[@password = @confirmPassword]) then () else (
            error($errors:BAD_REQUEST, 'Password does not match confirmation password.')
        )
    let $check                  := 
        if (userapi:isStrongEnoughPassword($password, $username)) then () else (  
            error($errors:BAD_REQUEST, 'Password SHALL be minimum of 10 characters. Other restrictions may apply, but are not enforced here.')
        )
    
    let $update                 := sm:passwd($username, $password)
    
    return 
        roaster:response(200, ())
};

(:~
:   Return full userInfo for the given username. If this username is not equal to the currently logged in user, the 
:   currently logged in user needs to be part of a group with permissions. If he is not, an error is returned
:   
:   @param $username The username to get the info for
:   @return The configured user info e.g. 
:       <user name="john">
:           <defaultLanguage/>
:           <displayName/>
:           <description/>
:           <email/>
:           <organization/>
:           <logins/>
:           <lastissuenotify/>
:       </user>, null or error()
:   @since 2013-11-07
:)
declare function userapi:getUserInfo($username as xs:string) as element(user)? {
    let $userInfo   := collection($userapi:strUserInfo)//user[@name = $username]
    
    return
        if (count($userInfo) gt 1) then
            error($errors:SERVER_ERROR, 'User info file contains multiple mentions of user ' || $username || '. Alert your administrator as this should not be possible.')
        else (
            $userInfo
        )
};

(:~
:   Return sequence of user names that have an entry in user-info.xml
:   
:   @return The list of user names, or null
:   @since 2013-11-07
:)
declare function userapi:getUserList() as xs:string* {
    userapi:searchUserByName(())/@name
};
declare function userapi:getUserList($searchString as xs:string?) as xs:string* {
    userapi:searchUserByName($searchString)/@name
};

(:~
:   Search user by (user) name
:
:   @param $searchString - required. The username to set the info for
:   @return list of elements containing username and displayName <user username="...">DisplayName</user>
:   @since 2016-11-20
:)
declare function userapi:searchUserByName($searchString as xs:string?) as element()* {
    if (empty($searchString)) then (
        $userapi:colUserInfo//user
    )
    else (
        let $luceneQuery    := utillib:getSimpleLuceneQuery($searchString)
        let $luceneOptions  := utillib:getSimpleLuceneOptions()
        return
            $userapi:colUserInfo//user[ft:query(@name | displayName, $luceneQuery)]
    )
};

(:~
:   Return active status for the current user (may be guest).
:   
:   @param $username - required. The username to set the info for
:   @return The boolean. true = active, or false = inactive
:   @since 2018-09-03
:)
declare function userapi:isUserActive($username as xs:string) as xs:boolean {
    (: you could not be logged in if you were inactive :)
    if (setlib:strCurrentUserName() = $username) then true() else 
    (: sm:is-account-enabled fails if account does not exist :)
    if (sm:user-exists($username)) then sm:is-account-enabled($username) else false()
};

(:~
:   Return language for the given username or the server language as fallback.
:   
:   @param $username The username to get the info for
:   @return The configured user language e.g. 'en-US', the server language (if no user setting exists) or error()
:   @since 2013-11-07
:   @see userapi:getUserLanguage($username as xs:string, $defaultwhenempty as xs:boolean)
:)
declare function userapi:getUserLanguage($username as xs:string) as xs:string {
    userapi:getUserLanguage($username, true())
};

(:~
:   Return language for the given username. If the user does not have a preference and parameter $defaultwhenempty is true, 
:   the browser language setting is returned, if that fails to get a language, the server language is returned as final 
:   fallback. If the user does not have a preference and parameter $defaultwhenempty is false, the result is empty()
:   If this username is not equal to the currently logged in user, the currently logged in user needs to be part of a group 
:   with permissions. If he is not, an error is returned.
:   
:   @param $username The username to get the info for
:   @return The configured user language e.g. 'en-US', the server language (if no user setting exists) or error()
:   @since 2013-11-07
:)
declare function userapi:getUserLanguage($username as xs:string, $defaultwhenempty as xs:boolean) as xs:string? {
    let $language := try { userapi:getUserInfo($username)/defaultLanguage[string-length() gt 0] } catch * {()}
    return
    if (empty($language) and $defaultwhenempty) then 
        (userapi:getSupportedBrowserLanguage(), $setlib:strArtLanguage)[1]
    else (
        $language
    )
};

(:~
:   Return language based on browser language. The returned value comes from the list of languages found in 
:   form-resources.xml to make sure it is supported in the interface
:   See also: http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
:   Accept-Language: nl,en-us;q=0.7,en;q=0.3
:   Accept-Language: nl-nl
:   Note: Safari sends only 1 language, FireFox a list, mileage may vary per browser (version)
:   
:   @return The first accepted browser language that is also supported by ART e.g. 'en-US', or ()
:   @since 2013-11-07
:)
declare %private function userapi:getSupportedBrowserLanguage() as xs:string? {
    let $supported-languages := doc(concat($setlib:strArtResources,'/form-resources.xml'))//@xml:lang
    let $accept-language     := if (request:exists()) then request:get-header('accept-language') else ()
    
    let $accepted-and-supported-languages :=
        for $lang-range in tokenize($accept-language,',')
        let $lang               := tokenize($lang-range,';')[1]
        let $supported-language := $supported-languages[lower-case(.)=lower-case($lang)]
        return
            if (empty($supported-language)) then
                (: e.g. $lang=nl :)
                if (string-length($lang)=2) then
                    $supported-languages[starts-with(lower-case(.),lower-case($lang))]
                else ()
            else (
                $supported-language
            )
    return
        $accepted-and-supported-languages[1]
};

(:~ Return groups for the given username. If this username is not equal to the currently logged in user, the 
:   currently logged in user needs to be part of a group with permissions. If he is not, an error is returned
:   
:   @param $username The username to get the info for
:   @return The configured groups e.g. 'decor decor-admin', or null
:   @since 2015-10-14
:)
declare function userapi:getUserGroups($username as xs:string) as xs:string? {
    if (sm:user-exists($username)) then sm:get-user-groups($username) else ()
};

(:~
:   Return organization for the given username. If this username is not equal to the currently logged in user, the 
:   currently logged in user needs to be part of a group with permissions. If he is not, an error is returned
:   
:   @param $username The username to get the info for
:   @return The configured organization e.g. 'St. Joseph Hospital', null or error()
:   @since 2013-11-07
:)
declare function userapi:getUserOrganization($username as xs:string) as xs:string? {
    userapi:getUserInfo($username)/organization
};

(:~
:   Return display name for the given username, usually his full name. If this username is not equal to the currently logged in user, the 
:   currently logged in user needs to be part of a group with permissions. If he is not, an error is returned
:   
:   @param $username The username to get the info for
:   @return The configured display name e.g. 'John Doe', null or error()
:   @since 2013-11-07
:)
declare function userapi:getUserDisplayName($username as xs:string) as xs:string? {
    try {
        let $displayName    := if (sm:user-exists($username)) then sm:get-account-metadata($username, $userapi:uriPropFullName) else ()
        
        return
            if (empty($displayName)) then error() else ($displayName)
    }    
    catch * {
        (userapi:getUserInfo($username)/displayName[string-length() gt 0], $username)[1]
    }
};

(:~
:   Return email for the given username. If this username is not equal to the currently logged in user, the 
:   currently logged in user needs to be part of a group with permissions. If he is not, an error is returned
:   
:   @param $username The username to get the info for
:   @return The configured email e.g. 'mailto:johndoe@stjosephhosptial.org', null or error()
:   @since 2013-11-07
:)
declare function userapi:getUserEmail($username as xs:string) as xs:string? {
    (: some resistence against accidently existing empty <email/> elements in user files :)
    let $uems := userapi:getUserInfo($username)/email[string-length() gt 0]
    let $check := if (count($uems) > 1) then util:log("ERROR", '+++ Error: user ' || $username || ' has multiple emails ' || string-join($uems, ', ')) else ()
    return $uems[1]
};

(:~
:   Return description for the given username. If this username is not equal to the currently logged in user, the 
:   currently logged in user needs to be part of a group with permissions. If he is not, an error is returned
:   
:   @param $username The username to get the info for
:   @return The configured description e.g. 'Added at request of XXX', null or error()
:   @since 2013-11-07
:)
declare function userapi:getUserDescription($username as xs:string) as xs:string? {
    if (sm:user-exists($username)) then sm:get-account-metadata($username, $userapi:uriPropDescription) else ()
};

(:~
:   Return the date the account was added. If this username is not equal to the currently logged in user, the 
:   currently logged in user needs to be part of a group with permissions. If he is not, an error is returned
:   
:   @param $username The username to get the info for
:   @return The configured user info e.g. 2013-01-01T12:34:23, null or error()
:   @since 2013-11-07
:)
declare function userapi:getUserCreationDate($username as xs:string) as xs:dateTime? {
    let $creationDate     :=
        try {
            let $accountfile      := collection('/db/system/security/exist/accounts')//config:account/config:name[. = $username]
            return 
                if (empty($accountfile)) then () else xmldb:created(util:collection-name($accountfile), util:document-name($accountfile)) 
        }
        catch * {()}
    
    return
        if (empty($creationDate)) then userapi:getUserInfo($username)/xs:dateTime(@effectiveDate) else $creationDate
};

(:~
:   Return last time the currently logged in user was logged in. If this username is not equal to the 
:   currently logged in user, the currently logged in user needs to be part of a group with permissions. If he 
:   is not, an error is returned
:   
:   @param $username The username to get the info for
:   @return The configured dateTime as xs:dateTime e.g. '2013-11-11T13:24:00', null or error()
:   @since 2013-11-07
:)
declare function userapi:getUserLastLoginTime($username as xs:string) as xs:dateTime? {
    max(userapi:getUserInfo($username)/logins/login/xs:dateTime(@at))
};

(:~
:   Return last time the currently logged in user was notified for issues. If this username is not equal to the 
:   currently logged in user, the currently logged in user needs to be part of a group with permissions. If he 
:   is not, an error is returned
:   
:   @param $username The username to get the info for
:   @return The configured dateTime as xs:dateTime e.g. '2013-11-11T13:24:00', null or error()
:   @since 2013-11-07
:)
declare function userapi:getUserLastIssueNotify($username as xs:string) as xs:dateTime? {
    let $lastNotify   := userapi:getUserInfo($username)/lastissuenotify
    return 
        if ($lastNotify/@at castable as xs:dateTime) then xs:dateTime($lastNotify/@at) else ()
};

(:~
:   Returns entry for a project for the given username. If this username is not equal to the currently logged in user, the 
:   currently logged in user needs to be part of a group with permissions. If he is not, an error is returned
:   
:   @param $username The username to set the info for
:   @param $projectId The project/@id to retieve info for. If empty returns all
:   @return project element that was created or updated or error()
:   @since 2016-11-20
:)
declare function userapi:getProjectPreference($username as xs:string, $projectId as xs:string?) as element(project)* {
    if (empty($projectId)) then (
        userapi:getUserInfo($username)/preferences/project
    ) else (
        userapi:getUserInfo($username)/preferences/project[@id = $projectId]
    )
};

(:~
:   Return the DECOR project settings for a specific user and project.
:   
:   @param $username - required. The username to get the info for
:   @param $prefix - required. project prefix to get the info for
:   @return one or more issue types for the project, or $userapi:arrSubscriptionDefault
:   @since 2014-06-23
:)
declare function userapi:getUserDecorSubscriptionSettings($username as xs:string, $prefix as xs:string) as xs:string+ {
    (:  NOTE: this purposely bypasses the security check implemented in userapi:getUserInfo() because in 
        saving issues the subscriptions need to be updated for all users based on this info, regardless of 
        who was logged in at the time :)
    let $return := 
        if ($setlib:colDecorData/decor/project[@prefix = $prefix]/author[@username = $username][@notifier='on']) then
            $userapi:subALL
        else 
        if ($username = 'guest') then () 
        else (
            $userapi:colUserInfo//project[@prefix = $prefix][ancestor::user/@name = $username][1]/@subscribeIssues
        )
        
    return
        if ($return) then (tokenize($return,' ')) else ($userapi:arrSubscriptionDefault)
};

(:~
:   Return boolean value to indicate if this username has a subscription for requested issueId. 
:   
:   @param $username - required. The username to get the info for
:   @param $issueId - optional. The issue id to get the info for
:   @return if subscription exists 'true()', else 'false()'
:   @since 2014-06-23
:)
declare function userapi:userHasIssueSubscription($username as xs:string, $issueId as xs:string) as xs:boolean {
    let $step1 := collection($setlib:strArtData)//sub:issue[@id = $issueId]
    let $step2 := $step1[@user = $username]
    return $step2/@notify = 'true'
};

(:~
:   Return potential empty string value to indicate if this username has a subscription for requested issueId. This
:   allows you to distinguish between 'has subscription', 'explicitly does not have a subscription', 'subscription not set'
:   
:   @param $username - required. The username to get the info for
:   @param $issueId - optional. The issue id to get the info for
:   @return if subscription exists 'true()', if explicitly no subscription 'false()', else empty
:   @since 2014-06-23
:)
declare %private function userapi:userCheckIssueSubscription($username as xs:string, $issueId as xs:string?) as xs:boolean? {
    let $step1 := collection($setlib:strArtData)//sub:issue[@id = $issueId]
    let $step2 := $step1[@user = $username]/@notify
    return
        if ($step2 castable as xs:boolean) then xs:boolean($step2) else ()
};

(:~
:   Return boolean value to indicate if an issue with certain characteristics is within the settings for automatic subscription
:   for the current logged in user. Logic:
:   - If the user has explicitly set or unset a subscription, return that value as the auto setting is then irrelevant
:   - If the user has an auto subscription to #ALL issues, return true()
:   - If the user is the current issue author and has an auto subscription to #ISAUTHOR issues, return true()
:   - If the user is the currently assigned to the issue and has an auto subscription to #ISASSIGNED issues, return true()
:   - If the issue has no objects and the user has an auto subscription to #NOOB issues, return true()
:   - If the issue has at least 1 object that the user has an auto subscription for , return true()
:   - Else false()
:   
:   @param $username - required. The username to get the info for
:   @param $prefix - required. The project prefix to get the info for
:   @param $issueId - optional. The issue id to get the info for
:   @param $objectTypes - optional. The issue object types to get the info for (issue/object/@type)
:   @param $originalAuthorUserName - optional. The username of the original issue author (project/author[@id=issue/tracking[first]/author/@id]/@username)
:   @param $currentAssignedAuthorName - optional. The username of the currently assigned person (project/author[@id=issue/assignment[last]/author/@id]/@username)
:   @return if subscription exists 'true()', if explicitly no subscription 'false()', else empty
:   @since 2014-06-23
:)
declare function userapi:userHasIssueAutoSubscription($username as xs:string, $prefix as xs:string, $issueId as xs:string?, $objectTypes as xs:string*, $originalAuthorUserName as xs:string?, $currentAssignedAuthorName as xs:string?) as xs:boolean {
    userapi:userHasIssueAutoSubscription($username, $prefix, $issueId, $objectTypes, $originalAuthorUserName, $currentAssignedAuthorName, ())
};
declare function userapi:userHasIssueAutoSubscription($username as xs:string, $prefix as xs:string, $issueId as xs:string?, $objectTypes as xs:string*, $originalAuthorUserName as xs:string?, $currentAssignedAuthorName as xs:string?, $userSubscriptionSettings as xs:string*) as xs:boolean {
    let $userSubscriptionSettings   := if (empty($userSubscriptionSettings)) then userapi:getUserDecorSubscriptionSettings($username, $prefix) else $userSubscriptionSettings
    let $userCurrentSubscription    := userapi:userCheckIssueSubscription($username, $issueId)
    return
        if (not(empty($userCurrentSubscription))) then
            (:subscription explicitly set or unset for this particular issue:)
            $userCurrentSubscription
        else if ($userSubscriptionSettings=$userapi:subALL) then
            (:user subscribes to any issue type:)
            true()
        else if ($username = $originalAuthorUserName and $userSubscriptionSettings = $userapi:subISAUTHOR) then
            (:user subscribes to any issue where he is author:)
            true()
        else if ($username = $currentAssignedAuthorName and $userSubscriptionSettings = $userapi:subISASSIGNED) then
            (:user subscribes to any issue where he is the assigned person:)
            true()
        else if (empty($objectTypes) and $userSubscriptionSettings = $userapi:subNOOB) then
            (:user subscribes to any issue where there's no object:)
            true()
        else if (empty($objectTypes) = false() and $objectTypes = $userSubscriptionSettings) then
            (:user subscribes to any issue where there's at least one object of a certain type:)
            true()
        else
            false()
};

(: ----------------  Write functions ---------------- :)

(:~
:   Create basic userInfo if there is no user info yet for the given username. If this username is not equal to the currently 
:   logged in user, the currently logged in user needs to be part of a group with permissions. If he is not, an error is returned
:   If the given username does not have a matching account in eXist, an error is returned.
:   
:   @param $authmap The credentials of the logged in user
:   @param $username The username to set the info for
:   @return The existing user info, or the basic info we just added e.g. 
:       <user name="john">
:           <defaultLanguage/>
:           <displayName/>
:           <description/>
:           <email/>
:           <organization/>
:           <logins/>
:           <lastissuenotify/>
:       </user>, null or error()
:   @since 2013-11-07
:)
declare function userapi:createUserInfo($authmap as map(*), $username as xs:string) as element(user) {
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check          := 
        if (sm:is-dba($authmap?name)) then () else if ($authmap?name = $username) then () else (
            error($errors:FORBIDDEN, concat('Guest user cannot add user settings for a different user. Cannot add user settings.'))
        )
    let $check          :=
        if (sm:user-exists($username)) then () else (
            error($errors:BAD_REQUEST, concat('User ', $username,' does not have an account yet. Cannot add user settings.'))
        )
    
    (: FIXME ... eXist-db 5 and 6 keep ff-ing up the index on this file :)
    (:let $reindex       := try { xmldb:reindex($userapi:colUserInfo} catch * {()}:)
    
    let $userInfo       := userapi:getUserInfo($username)
    let $update         := 
        if (empty($userInfo/@effectiveDate[. castable as xs:dateTime])) then
            let $creationDate := format-dateTime((userapi:getUserCreationDate($username), current-dateTime())[1], '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')
            return 
                if ($userInfo/@effectiveDate) then
                    update value $userInfo/@effectiveDate with $creationDate
                else (
                    update insert attribute effectiveDate {$creationDate} into $userInfo
                )
        else ()
    
    let $userInfo       := 
        if ($userInfo) then ($userInfo) else (
            let $newUserInfo    :=
                <user name="{$username}" active="{userapi:isUserActive($username)}" effectiveDate="{format-dateTime(current-dateTime(), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')}">
                    <defaultLanguage>{$setlib:strArtLanguage}</defaultLanguage>
                    <displayName>{$username}</displayName>
                    <email/>
                    <groups/>
                    <organization/>
                    <logins/>
                    <lastissuenotify/>
                    <decor-settings/>
                </user>
            let $userFile       := xmldb:store($userapi:strUserInfo, $username || '.xml', $newUserInfo)
            let $fixPermissions := 
                if ($username = 'guest.xml') then 
                    sm:chmod(xs:anyURI($userFile), 'rw-rw-rw-') 
                else (
                    sm:chmod(xs:anyURI($userFile), 'rw-rw----'),
                    sm:chown(xs:anyURI($userFile), $username || ':dba')
                )
            let $update         := userapi:setUserGroups($authmap, $username)
            
            return doc($userFile)/user
        )
        
    return
        if ($userInfo) then $userInfo else (
            error($errors:BAD_REQUEST, concat('User ',$username,' does not have a user-info.xml account. Cannot get user settings.'))
        )
};

(:~
:   Set basic userInfo overwriting any existing info for the given properties. If this username is not equal to the currently 
:   logged in user, the currently logged in user needs to be part of a group with permissions. If he is not, an error is returned
:   
:   @param $authmap The credentials of the logged in user
:   @param $username The username to set the info for
:   @param $language The language to set, format 'll-CC' (language-country)
:   @param $displayName The display name to set
:   @param $email The email address to set format user@host.realm
:   @param $organization The organization name to set
:   @return The existing user info after applying the updates e.g. 
:       <user name="john" effectiveDate="2013-01-01T00:00:00">
:           <defaultLanguage>en-US</defaultLanguage>
:           <groups>decor decor-admin</groups>
:           <displayName>John Doe</displayName>
:           <description/>
:           <email/>
:           <organization>St. Johns Hospital</organization>
:       </user>, null or error()
:   @since 2013-11-07
:)
declare function userapi:setUserInfo($authmap as map(*), $username as xs:string, $language as xs:string?, $displayName as xs:string?, $email as xs:string?, $organization as xs:string?, $description as xs:string?) as element() {
    let $update   := userapi:setUserActive($authmap, $username)
    let $update   := userapi:setUserGroups($authmap, $username)
    let $update   := userapi:setUserDisplayName($authmap, $username, $displayName)
    let $update   := userapi:setUserLanguage($authmap, $username, $language)
    let $update   := userapi:setUserEmail($authmap, $username, $email)
    let $update   := userapi:setUserOrganization($authmap, $username, $organization)
    let $update   := userapi:setUserDescription($authmap, $username, $description)
    
    return
        userapi:getUserInfo($username)
};

(:~
:   Sets the active status for the given username based on system value. If this username is not equal to the currently 
:   logged in user, the currently logged in user needs to be part of a group with permissions. If he is not, an error is returned
:   
:   @param $authmap The username to set the info for
:   @return nothing or error()
:   @since 2018-09-03
:)
declare function userapi:setUserActive($authmap as map(*), $username as xs:string) {
    let $currentUserInfo := userapi:createUserInfo($authmap, $username)
    return
        if (exists($currentUserInfo/@active)) then
            update value $currentUserInfo/@active with userapi:isUserActive($username)
        else (
            update insert attribute active {userapi:isUserActive($username)} into $currentUserInfo
        )
};

(:~
:   Sets the default language for the given username. If this username is not equal to the currently 
:   logged in user, the currently logged in user needs to be part of a group with permissions. If he is not, an error is returned
:   
:   @param $authmap The username to set the info for
:   @param $language The language to set, format 'll-CC' (language-country) or empty
:   @return nothing or error()
:   @error Parameter language SHALL be empty or have the case sensitive format ll-CC, e.g. en-US or de-DE
:   @since 2013-11-07
:   @since 2014-04-07 Made $language optional so you can 'unset' the language. Applicable to guest user mostly, an empty language will trigger browser language based behavior
:)
declare function userapi:setUserLanguage($authmap as map(*), $username as xs:string, $language as xs:string?) {
    let $language        := 
        if (string-length($language) = 0) then '' else 
        if (matches($language,'^[a-z]{2}-[A-Z]{2}$')) then $language 
        else (
            error($errors:BAD_REQUEST, 'Cannot set user language ''' || $language || '''. Language SHALL be empty or match pattern ll-CC where ll is lower-case language and CC is uppercase country/region')
        )
    let $currentUserInfo := userapi:createUserInfo($authmap, $username)
    return
        if (exists($currentUserInfo/defaultLanguage)) then
            update value $currentUserInfo/defaultLanguage with $language
        else (
            update insert <defaultLanguage>{$language}</defaultLanguage> into $currentUserInfo
        )
};

(:~
:   Sets the groups string for the given username. If this username is not equal to the currently 
:   logged in user, the currently logged in user needs to be part of a group with permissions. If he is not, an error is returned
:   
:   @param $authmap The username to set the info for
:   @return nothing or error()
:   @since 2015-10-14
:)
declare function userapi:setUserGroups($authmap as map(*), $username as xs:string) {
    if (sm:user-exists($username)) then (
        let $groups           := <groups>{for $group in sm:get-user-groups($username) return <group>{$group}</group>}</groups>
        let $currentUserInfo  := userapi:createUserInfo($authmap, $username)
        let $update           := 
            if (exists($currentUserInfo/groups)) then
                update replace $currentUserInfo/groups with $groups
            else (
                update insert $groups into $currentUserInfo
            )
        return ()
    )
    else ()
};

(:~
:   Sets the organization name for the given username. If this username is not equal to the currently 
:   logged in user, the currently logged in user needs to be part of a group with permissions. If he is not, an error is returned
:   
:   @param $authmap The username to set the info for
:   @param $organization The organization name to set
:   @return nothing or error()
:   @since 2013-11-07
:)
declare function userapi:setUserOrganization($authmap as map(*), $username as xs:string, $organization as xs:string?) {
    let $organization    := if (exists($organization)) then $organization else ('')
    let $currentUserInfo := userapi:createUserInfo($authmap, $username)
    return
        if (exists($currentUserInfo/organization)) then
            update value $currentUserInfo/organization with $organization
        else (
            update insert <organization>{$organization}</organization> into $currentUserInfo
        )
};

(:~
:   Sets the display name for the given username. If this username is not equal to the currently 
:   logged in user, the currently logged in user needs to be part of a group with permissions. If he is not, an error is returned
:   
:   @param $authmap The username to set the info for
:   @param $displayName The display name to set
:   @return nothing or error()
:   @since 2013-11-07
:)
declare function userapi:setUserDisplayName($authmap as map(*), $username as xs:string, $displayName as xs:string?) {
    if (sm:user-exists($username)) then (
        let $displayName      := string(($displayName, $username)[1])
        let $update           := sm:set-account-metadata($username, $userapi:uriPropFullName, $displayName)
        let $currentUserInfo  := userapi:getUserInfo($username)
        let $update           := 
            if (exists($currentUserInfo/displayName)) then
                update value $currentUserInfo/displayName with $displayName
            else (
                update insert <displayName>{$displayName}</displayName> into $currentUserInfo
            )
        return ()
    )
    else ()
};

(:~
:   Sets the email address name for the given username. If this username is not equal to the currently 
:   logged in user, the currently logged in user needs to be part of a group with permissions. If he is not, an error is returned
:   
:   @param $authmap The username to set the info for
:   @param $email The email address to set format user@host.realm
:   @return nothing or error()
:   @since 2013-11-07
:)
declare function userapi:setUserEmail($authmap as map(*), $username as xs:string, $email as xs:string?) {
    let $email           := if (exists($email)) then $email else ('')
    let $currentUserInfo := userapi:getUserInfo($username)
    return
        if (exists($currentUserInfo/email)) then
            update value $currentUserInfo/email with $email
        else (
            update insert <email>{$email}</email> into $currentUserInfo
        )
};

(:~
:   Sets the description for the given username. If this username is not equal to the currently 
:   logged in user, the currently logged in user needs to be part of a group with permissions. If he is not, an error is returned
:   
:   @param $authmap The username to set the info for
:   @param $description The description to set
:   @return nothing or error()
:   @since 2013-11-07
:)
declare function userapi:setUserDescription($authmap as map(*), $username as xs:string, $description as xs:string?) {
    if (sm:user-exists($username)) then (
        let $description      := string($description)
        let $update           := sm:set-account-metadata($username, $userapi:uriPropDescription, $description)
        let $currentUserInfo  := userapi:getUserInfo($username)
        return
            if (exists($currentUserInfo/description)) then
                update value $currentUserInfo/description with $description
            else (
                update insert <description>{$description}</description> into $currentUserInfo
            )
    ) else ()
};

(:~
:   Sets the last login time for the given username, keeping no more than the 5 latest login elements. If this username is not equal 
:   to the currently logged in user, the currently logged in user needs to be part of a group with permissions. If he is not, an 
:   error is returned
:   
:   @param $authmap The username to set the info for
:   @param $datetime The dateTime to set, format yyyy-MM-ddTHH:mm:ss(.sss+/-ZZ:zz)
:   @return nothing or error()
:   @since 2013-11-07
:)
declare function userapi:setUserLastLoginTime($authmap as map(*), $username as xs:string, $datetime as xs:dateTime?) {
    userapi:setUserLastLoginTime($authmap, $username, $datetime, -1)
};
declare function userapi:setUserLastLoginTime($authmap as map(*), $username as xs:string, $datetime as xs:dateTime?, $tokenlifetime as xs:integer) {
    let $ends            := if ($tokenlifetime < 0) then '' else current-dateTime() + xs:dayTimeDuration(concat('PT' , $tokenlifetime, 'S'))
    let $newlastlogin    := <login at="{substring($datetime,1,19)}" ends="{$ends}"/>
    let $currentUserInfo := userapi:getUserInfo($username)
    return
        if (exists($currentUserInfo/logins)) then
            let $logins := 
                for $login in $currentUserInfo/logins/login[@at]
                order by xs:dateTime($login/@at) descending
                return $login
            let $logins :=
                for $login in ($newlastlogin|subsequence($logins,1,4))
                order by xs:dateTime($login/@at) ascending
                return $login
            return (
                (: add last login record + last 4 records to avoid too long lists:)
                update replace $currentUserInfo/logins with <logins>{$logins}</logins>
            )
        else (
            (: non-existent logins element, add into user element :)
            update insert <logins>{$newlastlogin}</logins> into $currentUserInfo
        )
};

(:~
:   Sets the last issue notify time for the given username. If this username is not equal to the currently logged in user, the 
:   currently logged in user needs to be part of a group with permissions. If he is not, an error is returned
:   
:   @param $authmap The username to set the info for
:   @param $datetime The dateTime to set, format yyyy-MM-ddTHH:mm:ss(.sss+/-ZZ:zz)
:   @return nothing or error()
:   @since 2013-11-07
:)
declare function userapi:setUserLastIssueNotify($authmap as map(*), $username as xs:string, $datetime as xs:dateTime?) {
    let $newlastissuenotify := <lastissuenotify at="{substring($datetime,1,19)}"/>
    let $currentUserInfo    := userapi:getUserInfo($username)
    return
        if (exists($currentUserInfo/lastissuenotify)) then
            update replace $currentUserInfo/lastissuenotify with $newlastissuenotify
        else (
            update insert $newlastissuenotify into $currentUserInfo
        )
};

(:~
:   Adds entry for a project for the given username. If this username is not equal to the currently logged in user, the 
:   currently logged in user needs to be part of a group with permissions. If he is not, an error is returned
:   
:   @param $authmap The username to set the info for
:   @param $projectId The project/@id to create the entry for
:   @return project element that was created or updated or error()
:   @since 2016-11-20
:)
declare function userapi:updateProjectPreference($authmap as map(*), $username as xs:string, $projectId as xs:string?) as element(project)? {
    if (empty($projectId)) then () else (
        let $now                    := format-dateTime(current-dateTime(), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')
        let $decor                  := utillib:getDecorById($projectId)
        let $projectPrefix          := $decor/project/@prefix
        let $newprojectentry        := <project id="{$projectId}" prefix="{$projectPrefix}" count="1" at="{$now}"/>
        
        let $currentUserInfo        := userapi:getUserInfo($username)
        let $existingprojectentry   := $currentUserInfo/preferences/project[@id = $projectId]
        
        let $update                 := 
            if ($currentUserInfo[preferences]) then
                if ($existingprojectentry) then (
                    let $u  := update replace $existingprojectentry/@prefix with $projectPrefix
                    let $u  := update replace $existingprojectentry/@count with ($existingprojectentry/xs:integer(@count) + 1)
                    let $u  := update replace $existingprojectentry/@at with $now
                    
                    return ()
                )
                else (
                    update insert $newprojectentry into $currentUserInfo/preferences
                )
            else (
                update insert <preferences>{$newprojectentry}</preferences> into $currentUserInfo
            )
        
        return userapi:getProjectPreference($username, $projectId)
    )
};

(:~
:   Adds a hide-statuses entry for a section in a project for the given username. If this username is not equal to the currently logged in user, the 
:   currently logged in user needs to be part of a group with permissions. If he is not, an error is returned
:   
:   @param $authmap The username to set the info for
:   @param $projectId The project/@id to create the entry for
:   @param $section The section to create the entry for. See $decorlib:SECTIONS-ALL for supported values, e.g. datasets, rules, terminology
:   @return project element that was created or updated or error()
:   @since 2018-01-11
:)
declare function userapi:updateProjectPreferenceStatuses($authmap as map(*), $username as xs:string, $projectId as xs:string?, $section as xs:string, $hide-statuses as xs:string*) as element(project)? {
    if (empty($projectId)) then () else (
        let $now                    := format-dateTime(current-dateTime(), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')
        let $decor                  := utillib:getDecorById($projectId)
        let $projectPrefix          := $decor/project/@prefix
        let $newprojectentry        := <project id="{$projectId}" prefix="{$projectPrefix}" count="1" at="{$now}"/>
        
        let $currentUserInfo        := userapi:getUserInfo($username)
        let $existingprojectentry   := $currentUserInfo/preferences/project[@id = $projectId]
        
        let $update                 := 
            if ($currentUserInfo[preferences]) then
                if ($existingprojectentry) then () else (
                    update insert $newprojectentry into $currentUserInfo/preferences
                )
            else (
                update insert <preferences>{$newprojectentry}</preferences> into $currentUserInfo
            )
        
        let $existingprojectentry   := $currentUserInfo/preferences/project[@id = $projectId]
        let $newsectionentry        := 
            if (empty($hide-statuses)) then ()
            else
            if ($section = $decorlib:SECTIONS-ALL) then
                <section name="{$section}" hide-statuses="{$hide-statuses}"/>
            else ()
        let $update                 :=
            if ($newsectionentry) then (
                if ($existingprojectentry/section[@name = $section][@hide-statuses]) then
                    update value $existingprojectentry/section[@name = $section]/@hide-statuses with $hide-statuses
                else
                if ($existingprojectentry/section[@name = $section]) then
                    update insert $newsectionentry/@hide-statuses into $existingprojectentry/section[@name = $section]
                else
                    update insert $newsectionentry into $existingprojectentry
            )
            else ()
        
        return $currentUserInfo/preferences/project[@id = $projectId]
    )
};

(:~
:   Patches data for all users incorrectly added by userapi:updateProjectPreference leading to nested user/preferences. Before:
:   <preferences>
:       <project id="2.16.840.1.113883.3.1937.99.62.3" prefix="demo1-" count="44" at="2017-01-16T05:13:54"/>
:       <preferences>
:           <project id="2.16.840.1.113883.2.4.3.11.60.90" prefix="peri20-" count="1" at="2017-01-19T14:03:05"/>
:       </preferences>
:       <preferences>
:           <project id="2.16.840.1.113883.2.4.3.11.60.90" prefix="peri20-" count="1" at="2017-02-02T20:20:00"/>
:       </preferences>
:   </preferences>
:   after:
:       <preferences>
:           <project id="2.16.840.1.113883.3.1937.99.62.3" prefix="demo1-" count="44" at="2017-01-16T05:13:54"/>
:       </preferences>
:   
:   @return nothing
:   @since 2017-03-25
:)
declare function userapi:patchAllProjectPreferences() {
    let $delete := update delete $userapi:colUserInfo//user/preferences/preferences
    return ()
};

(:~ Patches groups section for all users so it is no longer a space separated list but a list of child element call group. Before:
:   <groups>group1 group2 group 3</groups>
:
:   after:
:   <groups>
:       <group>group1</group>
:       <group>group2</group>
:       <group>group 3</group>
:   </groups>
:   
:   @return nothing
:   @since 2017-06-12
:)
declare function userapi:patchAllUserGroups() {
    for $userInfo in $userapi:colUserInfo//user
    let $username       := $userInfo/@name
    return
        if (sm:user-exists($username)) then (
            userapi:setUserGroups(map { "name": "admin", "groups": [ "dba" ] }, $username)
        )
        else (
            xmldb:remove(util:collection-name($userInfo), util:document-name($userInfo))
        )
};

(:~ Update art-data user-info.xml in case people have been using the eXist-db dashboard for user management. Additional entries in user-info.xml 
:   are removed. Missing users are added to user-info.xml. Matching entries in user-info.xml are updated for active status and groups
:
:   @return nothing
:   @since 2019-08-17
:)
declare function userapi:patchAllUserInfo() {
    (: rewrite any art-data/user-info.xml users as art-data/user-info/{user/@name}.xml and then remove user-info.xml :)
    let $docName        := 'user-info.xml'
    let $rewrite        :=
        if (doc-available($setlib:strArtData || '/' || $docName)) then (
            let $r      :=
                for $user in doc($setlib:strArtData || '/' || $docName)/*/user[@name[not(. = '')]]
                return
                    xmldb:store($userapi:strUserInfo, $user/@name || '.xml', $user)
            let $u      := xmldb:remove($setlib:strArtData, $docName)
            
            return ()
        )
        else ()
    
    (: remove users in user-info that have no corresponding eXist-db user :)
    let $removeUsers    :=
        for $user in userapi:getUserList(())
        let $userInfo       := userapi:getUserInfo($user)
        return
            if (sm:user-exists($user)) then () else if (empty($userInfo)) then () else (
                xmldb:remove(util:collection-name($userInfo), util:document-name($userInfo))
            )
    
    (: add users in user-info that have a corresponding eXist-db user, and/or set active status :)
    let $authmap        := map { "name": "admin", "groups": [ "dba" ], "dba": true() }
    let $addUsers       :=
        for $username in sm:list-users()[not(. = 'SYSTEM')]
        (: some server use eXist-db user manager which does not use user-info.xml in which case the two are out of sync :)
        let $displayName    := (sm:get-account-metadata($username, $userapi:uriPropFullName), userapi:getUserInfo($username)/displayName[not(. = '')])[not(. = $username)]
        return (
            userapi:setUserActive($authmap, $username), 
            userapi:setUserGroups($authmap, $username),
            userapi:setUserDisplayName($authmap, $username, ($displayName, $username)[1]),
            userapi:setUserDescription($authmap, $username, userapi:getUserDescription($username))
        )
    let $fixPermissions := sm:chmod(xs:anyURI($userapi:strUserInfo), 'rwsrwsr-x')
    let $fixPermissions := sm:chown(xs:anyURI($userapi:strUserInfo), 'admin:dba')
    let $fixPermissions :=
        for $r in xmldb:get-child-resources($userapi:strUserInfo)
        let $docname    := $userapi:strUserInfo || '/' || $r
        let $username   := if (doc-available($docname)) then doc($docname)/*/@name else ()
        return
            if ($r = 'guest.xml') then 
                sm:chmod(xs:anyURI($docname), 'rw-rw-rw-') 
            else (
                sm:chmod(xs:anyURI($docname), 'rw-rw----'),
                if (empty($username)) then () else sm:chown(xs:anyURI($docname), $username || ':dba')
            )
    
    return ()
};

(:~
:   Sets the settings for the given project. If this username is not equal to the currently logged in user, the 
:   currently logged in user needs to be part of a group with permissions. If he is not, an error is returned.
:   
:   @param $authmap - required. The username to set the info for
:   @param $prefix - required. The project prefix to set subscriptions for
:   @param $issueTypes - required.  Array of space separated issue object types. Supported types may be found in DECOR.xsd under simpleType DecorObjectType. 
:                       Special: #ALL (any issue), #NOOB (issues without objects), #ISAUTHOR (issues the user authored), #ISASSIGNED (issues the user is currently assigned to)
:   @return nothing or error()
:   @since 2014-06-23
:)
declare function userapi:setUserDecorSubscriptionSettings($authmap as map(*), $username as xs:string, $prefix as xs:string, $issueTypes as xs:string+) {
    let $inputCheck             :=
        if ($issueTypes[not(.=$userapi:arrSubscriptionTypes)]) then
            error($errors:BAD_REQUEST, concat('Cannot set user decor subscription settings. Issue type "',string-join($issueTypes[not(.=$userapi:arrSubscriptionTypes)],' '),'" is not supported. Supported types are "',string-join($userapi:arrSubscriptionTypes,' '),'"'))
        else ()
    let $newProjectSettings     := <project prefix="{$prefix}" subscribeIssues="{string-join($issueTypes,' ')}"/>
    let $currentUserInfo        := userapi:getUserInfo($username)
    let $currentDecorSettings   := $currentUserInfo/decor-settings
    let $currentProjectSettings := $currentDecorSettings/project[@prefix=$prefix]
    
    return
        if ($currentProjectSettings) then
            update replace $currentProjectSettings with $newProjectSettings
        else if ($currentDecorSettings) then
            update insert $newProjectSettings into $currentUserInfo
        else (
            update insert <decor-settings>{$newProjectSettings}</decor-settings> into $currentUserInfo
        )
};

(:~
:   Deletes all active subscriptions for the given user and the given project or all projects if empty. 
:   Keeps deactivated subscriptions (@notify='false') if $fullreset=false() and deletes those too if $fullreset=true()
:
:   Example: if you want to delete every subscription for user john in all projects 
:       userapi:deleteUserIssueSubscriptions('john', (), true())
:   Example: if you want to delete active subscriptions for user john in project demo1- 
:       userapi:deleteUserIssueSubscriptions('john', 'demo1-', false())
:   
:   @param $authmap - required. The username to set the info for
:   @param $prefix - optional. The project prefix to set subscriptions for
:   @param $fullreset - required. If false will keep subscriptions that were deactivated. If false will delete any subscription
:   @return nothing
:   @since 2014-06-23
:)
declare function userapi:deleteUserIssueSubscriptions($authmap as map(*), $username as xs:string, $prefix as xs:string?, $fullreset as xs:boolean) {
    let $delete :=
        if (empty($prefix)) then
            if ($fullreset) then
                update delete collection($setlib:strArtData)/sub:decor-subscriptions/sub:issue[@user = $username]
            else (
                update delete collection($setlib:strArtData)/sub:decor-subscriptions/sub:issue[@user = $username][@notify = 'true']
            )
        else (
            if ($fullreset) then
                update delete collection($setlib:strArtData)/sub:decor-subscriptions/sub:issue[@user = $username][@prefix = $prefix]
            else (
                update delete collection($setlib:strArtData)/sub:decor-subscriptions/sub:issue[@user = $username][@prefix = $prefix][@notify = 'true']
            )
        )
    return ()
};

(:~
:   Sets a subscription for the given issueId and the given user.
:   
:   @param $authmap - required. The username to set the info for
:   @param $issueOrIssueId - required. The issue (id) to set the info for
:   @return nothing
:   @since 2014-06-23
:)
declare function userapi:setUserIssueSubscription($authmap as map(*), $issueOrIssueId as item()) {
    userapi:updateUserIssueSubscription($authmap, $issueOrIssueId, true())
};

(:~
:   Explicitly deactivates a subscription for the given issueId and the given user.
:   
:   @param $authmap - required. The username to set the info for
:   @param $issueOrIssueId - required. The issue (id) to set the info for
:   @return nothing
:   @since 2014-06-23
:)
declare function userapi:unsetUserIssueSubscription($authmap as map(*), $issueOrIssueId as item()) {
    userapi:updateUserIssueSubscription($authmap, $issueOrIssueId, false())
};

(:~
:   Handles the actual logic for userapi:setUserIssueSubscription and userapi:unsetUserIssueSubscription 
:   based on parameter $activate
:   
:   @param $authmap - required. The username to set the info for
:   @param $issueOrIssueId - required. The issue (id) to set the info for
:   @param $activate - required. true() will activate, false() will explicitly deactivate
:   @return nothing
:   @since 2014-06-23
:)
declare function userapi:updateUserIssueSubscription($authmap as map(*), $issueOrIssueId as item(), $activate as xs:boolean) {
    let $issue                      :=
        if ($issueOrIssueId instance of element(issue)) then $issueOrIssueId else (
            $setlib:colDecorData//issue[@id=$issueOrIssueId]
        )
    let $prefix                     := $issue/ancestor::decor/project/@prefix
    let $issueId                    := $issue/@id
    let $username                   := $authmap?name
    let $checkParam                 :=
        if (empty($prefix) and $activate) then
            error($errors:BAD_REQUEST, concat('Unable to add subscription for issue with id ''',string($issueOrIssueId),''' as it does not exist.'))
        else ()
    
    let $newSubscription            := 
        <decor-subscriptions xmlns="http://art-decor.org/ns/art-decor-user-subscriptions">
            <issue id="{$issueId}" user="{$username}" prefix="{$prefix}" notify="{$activate}"/>
        </decor-subscriptions>
    let $currentDecorSubscriptions  := collection($setlib:strArtData)/sub:decor-subscriptions
    let $currentSubscription        := $currentDecorSubscriptions/sub:issue[@id = $issueId][@user = $username]
    
    let $update                     :=
        if ($currentSubscription[@notify]) then
            update value $currentSubscription/@notify with $activate
        else if ($currentDecorSubscriptions) then
            update insert $newSubscription/sub:issue into $currentDecorSubscriptions
        else (
            let $subscrFile := xmldb:store($setlib:strArtData, $userapi:strSubscriptionFile, $newSubscription)
            return (
                sm:chgrp(xs:anyURI(concat('xmldb:exist://',$subscrFile)),'decor'),
                sm:chmod(xs:anyURI(concat('xmldb:exist://',$subscrFile)),sm:octal-to-mode('0775')),
                sm:clear-acl(xs:anyURI(concat('xmldb:exist://',$subscrFile)))
            )
        )
    return ()
};

declare %private function userapi:userInfo($authmap as map(*)?, $username as xs:string, $userDisplayName as xs:string?, $doDetails as xs:boolean) as element(user) {
    let $userDisplayName      := if (empty($userDisplayName)) then (userapi:getUserDisplayName($username), $username)[1] else $userDisplayName
    let $userActive           := if (sm:user-exists($username)) then sm:is-account-enabled($username) else false()
    let $userEmail            := userapi:getUserEmail($username)
    let $userLanguage         := if ($doDetails) then userapi:getUserLanguage($username, false()) else ()
    let $userOrganization     := if ($doDetails) then userapi:getUserOrganization($username) else ()
    let $userCreationDate     := if ($doDetails) then userapi:getUserCreationDate($username) else ()
    let $userLastLogin        := if ($doDetails) then userapi:getUserLastLoginTime($username) else ()
    let $userLastIssueNotify  := if ($doDetails) then userapi:getUserLastIssueNotify($username) else ()
    let $userDescription      := if ($doDetails) then userapi:getUserDescription($username) else ()
    let $userPrimaryGroup     := if ($doDetails) then sm:get-user-primary-group($username) else ()
    let $userGroups           := if (sm:user-exists($username)) then sm:get-user-groups($username) else ()
    return
    <user name="{$username}" active="{$userActive}" displayName="{$userDisplayName}">
    {
        if (empty($authmap) or $authmap?name = 'guest') then () else (
            if (empty($userEmail)) then () else (
                attribute email {$userEmail}
            ),
            if ($doDetails) then (
                if (empty($userOrganization)) then () else attribute organization {$userOrganization},
                if (empty($userLanguage)) then () else attribute defaultLanguage {$userLanguage},
                if (empty($userCreationDate)) then () else attribute creationdate {$userCreationDate},
                if (empty($userLastLogin)) then () else attribute lastlogin {$userLastLogin},
                if (empty($userLastIssueNotify)) then () else attribute lastissuenotify {$userLastIssueNotify},
                if (empty($userDescription)) then () else attribute description {$userDescription}
            ) else ()
            ,
            if ($authmap?groups = $userapi:editGroups or $username = $authmap?name) then (
                if ($doDetails) then (if (empty($userPrimaryGroup)) then () else attribute primarygroup {$userPrimaryGroup}) else ()
                ,
                for $group in $userGroups
                return
                    <groups>{$group}</groups>
            ) else ()
            ,
            if ($doDetails) then (
                for $project in $setlib:colDecorData//decor/project[author/@username=$username]
                let $projectName      := 
                    if (exists($project/name[@language=$userLanguage])) then (
                        $project/name[@language=$userLanguage][1]
                    ) else (
                        $project/name[@language=$project/@defaultLanguage][1]
                    )
                let $author           := $project/author[@username=$username][1]
                let $authorEmail      := if ($author/@email[not(. = '')]) then replace($author/@email, 'mailto:', '') else ()
                let $authorNotifier   := ($author/@notifier[not(. = '')], 'off')[1]
                let $authorActive     := not($author/@active = 'false') 
                order by lower-case($projectName)
                return
                    <project prefix="{$project/@prefix}">
                    {
                        if (empty($authorEmail)) then () else attribute email {$authorEmail},
                        attribute notifier {$authorNotifier},
                        attribute active {$authorActive},
                        $project/name
                    }
                    </project>
            ) else ()
        )
    }
    </user>
};

declare function userapi:createUpdateServerUser($authmap as map(*), $data as element(), $create as xs:boolean) {
(:let $userInfo :=
    <user name="test" active="true" newpwd="" newpwd-confirm="">
        <!-- account-info -->
        <groups><group>guest</group></groups>
        <primarygroup>guest</primarygroup>
        <description>test</description>
        
        <!-- user-info -->
        <defaultLanguage>{$setlib:strArtLanguage}</defaultLanguage>
        <displayName>Test user</displayName>
        <email></email>
        <organization>Test organization</organization>
    </user>:)
    
    let $username               := if ($create) then $data/@name else ($data/@name, $authmap?name)[1]
    let $password               := if ($data[@password = @confirmPassword]) then $data/@password else ()
    let $check                  := 
        if ($create) then (
            if ($authmap?dba) then () else (
                error($errors:FORBIDDEN, 'User ' || $authmap?name || ' cannot create new user. Only dba users can create new users')
            ),
            if (sm:user-exists($username)) then 
                error($errors:BAD_REQUEST, 'User ' || $authmap?name || ' cannot create new user. User ' || $username || ' already exists')
            else (),
            if (empty($password)) then
                error($errors:BAD_REQUEST, 'User ' || $authmap?name || ' cannot create new user. Password is required on new users.')
            else (),
            if ($data[@password = @confirmPassword]) then () else (
                error($errors:BAD_REQUEST, 'User ' || $authmap?name || ' cannot create new user. Password does not match confirmation password.')
            ),
            if ($username = $userapi:PROTECTEDUSERS) then
                error($errors:BAD_REQUEST, 'User ' || $authmap?name || ' cannot create new user ' || $username || '. User matches one of the protected system users.')
            else (),
            if ($username = $userapi:PROTECTEDGROUPS) then
                error($errors:BAD_REQUEST, 'User ' || $authmap?name || ' cannot create new user ' || $username || '. User matches one of the protected groups. Protected groups: ', string-join($userapi:PROTECTEDGROUPS, ', '))
            else ()
        )
        else (
            if ($authmap?dba) then () else if ($authmap?name = $username) then () else (
                error($errors:FORBIDDEN, 'User ' || $authmap?name || ' cannot update user ' || $username || '. Only dba users can update other users')
            ),
            if (sm:user-exists($username)) then () else (
                error($errors:BAD_REQUEST, 'User ' || $authmap?name || ' cannot update user ' || $username || '. User does not exist')
            ),
            if ($data[string-length(@password) = 0] | $data[@password = @confirmPassword]) then () else (
                error($errors:BAD_REQUEST, 'User ' || $authmap?name || ' cannot update user ' || $username || '. Password does not match confirmation password.')
            )
        )
        
    (: there is no need to manage admin, SYSTEM and other special purpose system users through here. In fact it might pose a security risk if you allow this. :)
    let $check                  :=
        if ($username = $userapi:PROTECTEDUSERS) then
            error($errors:BAD_REQUEST, 'User ' || $username || ' cannot be managed through this api. Please use the eXist-db User Manager or eXide.')
        else ()
    let $check                  := 
        if (empty($password) or userapi:isStrongEnoughPassword($password, $username)) then () else (  
            error($errors:BAD_REQUEST, 'Password SHALL be minimum of 10 characters. Other restrictions may apply, but are not enforced here.')
        )
    
    let $userIsActive           := if ($data/@active castable as xs:boolean) then (xs:boolean($data/@active)) else true()
    let $userGroups             := $data/groups
    (: Note the primary group you set should exist. If it does not exist, the primary group will default
       to the first group that the user happens to be in based on the request :)
    let $userPrimaryGroup       := 
        if ($userGroups[. = $data/@primarygroup]) then $data/@primarygroup else
        if ($userGroups[. = 'decor'])             then 'decor' else
        if ($userGroups[. = 'terminology'])       then 'terminology' else
        if ($userGroups[. = 'xis'])               then 'xis' else (
            head($userGroups)
        )
    let $userDescription        := string($data/@description)
    
    (: user-info Initially set by the admin that creates the account, but also user editable from there on :)
    let $userLanguage           := $data/@defaultLanguage[string-length() gt 0]
    let $userDisplayName        := $data/@displayName[string-length() gt 0]
    let $userEmail              := $data/@email[string-length() gt 0]
    let $userOrganization       := $data/@organization[string-length() gt 0]
    
    (: Save user details for all users except SYSTEM :)
    let $createUpdateUser       := 
        if ($create) then (
            (:  This may happen if we have ever created this user before, and deleted him through the eXist-db user manager. 
                The group then lives on and sm:create-account() will fail on that group
                We may delete this group if it is empty and not one of the protected groups
                The latter is already checked earlier in this routine but doesn't hurt to recheck
            :)
            if (sm:group-exists($username) and empty(sm:get-group-members($username)) and not($username = $userapi:PROTECTEDGROUPS)) then
                sm:remove-group($username)
            else (
                (: cannot be helped. this group already has members. we cannot delete a group with members in it: next statement will fail :)
            )
            ,
            (:sm:create-account($username as xs:string, $password as xs:string, $groups as xs:string*, $full-name as xs:string, $description as xs:string) as empty-sequence():)
            (:sm:create-account($username as xs:string, $password as xs:string, $primary-group as xs:string, $groups as xs:string*, $full-name as xs:string, $description as xs:string) as empty-sequence():)
            if ($userGroups[. = $userPrimaryGroup]) then
                sm:create-account($username, $password, $userPrimaryGroup, $userGroups, string($userDisplayName), string($userDescription))
            else (
                sm:create-account($username, $password, $userGroups, string($userDisplayName), string($userDescription))
            )
        )
        else (
            (:updated user:)
            let $currentGroups       := sm:get-user-groups($username)
            let $actionAddGroups     := 
                if (sm:is-dba($authmap?name)) then 
                    for $group in $userGroups[not(. = $currentGroups)] 
                    return if (sm:group-exists($group)) then sm:add-group-member($group, $username) else ()
                else ()
            let $actionRemoveGroups  := 
                if (sm:is-dba($authmap?name)) then 
                    for $group in $currentGroups[not(. = $userGroups)] 
                    return if (sm:group-exists($group)) then sm:remove-group-member($group, $username) else ()
                else ()
            
            (: Update user and server-info :)
            let $update              := if (empty($password)) then () else sm:passwd($username, $password)
            
            return ()
        )
    
    (: dba only stuff ... :)
    let $update               := 
        if (sm:is-dba($authmap?name)) then (
            sm:set-account-enabled($username, $userIsActive),
            if (empty($userPrimaryGroup)) then () else if (sm:group-exists($userPrimaryGroup)) then sm:set-user-primary-group($username, $userPrimaryGroup) else (),
            if (empty($password)) then () else if ($username = $setlib:arrSystemLevelUsers) then serverapi:setPassword($username, $password) else ()
        )
        else ()
    
    (: Cannot add user-info before the user exists, so make this last step :)
    let $update               := userapi:setUserInfo($authmap, $username, $userLanguage, $userDisplayName, $userEmail, $userOrganization, $userDescription)
    
    return ()
};

declare function userapi:isStrongEnoughPassword($password as xs:string?, $username as xs:string) as xs:boolean {
let $normalizedPass         := replace(lower-case(translate($password, '1340', 'ieao')), '[^a-z]', '')

return
    (: minimum 10 chars :)
    if (string-length($password) lt 10) then false() else
    (:(\: minimum 1 digit :\)
    if (not(matches($password, '\d'))) then false() else
    (\: minimum 1 non alpha/digit character :\)
    if (not(matches($password, '[^A-Za-z\d]'))) then false() else
    (\: don't base passwords on 'test' :\)
    if (contains($normalizedPass, 'test')) then false() else
    (\: don't base passwords on 'password' :\)
    if (contains($normalizedPass, 'password')) then false() else
    (\: don't base passwords on 'wachtwoord' :\)
    if (contains($normalizedPass, 'wachtwoord')) then false() else
    (\: don't base passwords on 'welkom' :\)
    if (contains($normalizedPass, 'welkom')) then false() else
    (\: don't base passwords on 'welcome' :\)
    if (contains($normalizedPass, 'welcome')) then false() else:)
        true()
};

(: =================  USER NOTIFICATION  ================== :)

(:~ Creates a request to inform a (new) user about his username,
    his (newly assigned) password and his associated projects.
    Here only the user along with a few additional information
    is recorded as the request, the process that takes care
    of the request (scheduled-tasks) will assign a new
    password, evaluate all projects where the user
    is involved in as author and send that information as
    an email to him.
    @param parameter/user to be notified
    @return user-notify-request
    @since 2021-11-26
:)
declare function userapi:postServerUserNotify($request as map(*)) {
    
    let $authmap                := $request?user
    let $user2notify            := $request?parameters?user
   
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (sm:is-dba($authmap?name)) then () else (
            error($errors:FORBIDDEN, 'Cannot create new notify for user. Only dba users can trigger a notify of other users')
        )
    let $check                  :=
        if (empty($user2notify)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter user')
        else ()
    let $userinfo               := userapi:getUserInfo($user2notify)
    let $check                  :=
        if (empty($userinfo)) then
            error($errors:BAD_REQUEST, 'The specificed user cannot be found or has no detail data.')
        else ()
    let $email                  := $userinfo/email
    let $check                  :=
        if (empty($email)) then
            error($errors:BAD_REQUEST, 'The specificed user has no email yet.')
        else ()
    let $check                  :=
        if (matches($email, '\S+@[^\s\.]+\.\S+')) then ()
            else error($errors:BAD_REQUEST, 'The specificed email is not valid.')

    let $displayName            := $userinfo/displayName
    let $language               := $userinfo/defaultLanguage
    let $language               := 
        if (empty($language))
        then 'en-US'
        else $language

    let $now                    := substring(string(current-dateTime()), 1, 19)
    let $stamp                  := util:uuid()
      
    let $user-notify-request   :=  
        <user-notify-request uuid="{$stamp}"
            for="{$user2notify}" email="{$email}" name="{$displayName}" on="{current-dateTime()}" as="{$stamp}" by="{$authmap?name}" 
            language="{$language}" setpassword="true" progress="Added to notification queue ..." progress-percentage="0"/>
        
    let $write                  := xmldb:store($setlib:strDecorScheduledTasks, $stamp || '.xml', $user-notify-request)
    let $tt                     := sm:chmod($write, 'rw-rw----')
    
    return
        $user-notify-request

};

(: =================  USER NOTIFICATION  ================== :)