xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:~ Issue API allows read, create, update of DECOR issues and issue labels :)
module namespace isapi              = "http://art-decor.org/ns/api/issue";

import module namespace roaster     = "http://e-editiones.org/roaster";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

import module namespace utillib     = "http://art-decor.org/ns/api/util" at "library/util-lib.xqm";
import module namespace decorlib    = "http://art-decor.org/ns/api/decor" at "library/decor-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "library/settings-lib.xqm";
import module namespace userapi     = "http://art-decor.org/ns/api/user" at "user-api.xqm";
import module namespace cs          = "http://art-decor.org/ns/decor/codesystem" at "../../art/api/api-decor-codesystem.xqm";
import module namespace vs          = "http://art-decor.org/ns/decor/valueset" at "../../art/api/api-decor-valueset.xqm";
import module namespace tmapi       = "http://art-decor.org/ns/api/template" at "template-api.xqm";

declare namespace json          = "http://www.json.org";
declare namespace http          = "http://expath.org/ns/http-client";

(:~ All functions support their own override, but this is the fallback for the maximum number of results returned on a search :)
declare %private variable $isapi:maxResults             := 50;

declare %private variable $isapi:ISSUE-PRIORITY         := utillib:getDecorTypes()/IssuePriority;
declare %private variable $isapi:ISSUE-TYPE             := utillib:getDecorTypes()/IssueType;
declare %private variable $isapi:ISSUE-OBJECT-TYPE      := utillib:getDecorTypes()/IssueObjectType;

(:~ Retrieves DECOR issue based on $id (oid), including meta data, trackings and assignments.
 
To create one use `POST /api/issue/`"

@param $id required parameter denoting the id of the issue
@return issue structure including generated meta data
@since 2020-05-03
:)
declare function isapi:getIssue($request as map(*)) {

    let $authmap        := $request?user
    let $id             := $request?parameters?id
    let $projectPrefix  := $request?parameters?prefix[string-length() gt 0]
    let $issues         := isapi:getIssueById($id, $projectPrefix)

    return
        if (empty($issues)) then (
            roaster:response(404, ())
        )
        else
        if (count($issues) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple issues for id '", $id, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        )
        else (
            for $issue in $issues
            let $includeObjects     := true()
            let $expandIssues       := true()
            let $includeEvents      := true()
            let $result             := isapi:getExpandedIssues($authmap, $issues, $includeObjects, $expandIssues, $includeEvents)
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    (: we return just a count of issues and leave it up to the calling party to get those if required :)
                    <issueAssociation json:array="true" count="{count($setlib:colDecorData//issue/object[@id = $result/@id])}"/>
                    ,
                    utillib:addJsonArrayToElements($result/*)
                }
        )

};

(:~ Create DECOR issue

@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $projectPrefix            - required. the project prefix to upload to 
@param $request-body             - required. json body containing the issue object
@return issue structure including generated meta data
@since 2020-05-03
:)
declare function isapi:postIssue($request as map(*)) {

    let $authmap                := $request?user
    let $projectPrefix          := $request?parameters?prefix[string-length() gt 0]
    let $data                   := utillib:getBodyAsXml($request?body)
    
    (:let $s                      := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data):) 
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($data)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $return                 := isapi:postIssue($authmap, string($projectPrefix), $data)
    return (
        roaster:response(201, $return)
    )

};

(:~ Update DECOR issue

@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $id                       - required. the id for the issue to update 
@param $request-body             - required. json body containing new issue structure
@return issue structure including generated meta data
@since 2020-05-03
:)
declare function isapi:putIssue($request as map(*)) {

    let $authmap                := $request?user
    let $issueId                := $request?parameters?id
    let $data                   := utillib:getBodyAsXml($request?body)
    
    (:let $s                      := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data):) 
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($data)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $return                 := isapi:putIssue($authmap, string($issueId), $data)
    return (
        roaster:response(200, $return)
    )

};

(:~ Update DECOR issue parts. Expect array of parameter objects, each containing RFC 6902 compliant contents. Note: RestXQ does not do PATCH (yet), but that would be the preferred option.

{ "op": "[add|remove|replace]", "path": "[/|/displayName|/priority|/type]", "value": "[string|object]" }

where

* op - add (object associations, tracking, event) or remove (object associations only) or replace (displayName, priority, type, tracking, assignment)

* path - / (object, tracking, assignment) or /displayName or /priority or /type

* value - string when the path is not /. object when path is /
@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $id                       - required. the id for the issue to update 
@param $request-body             - required. json body containing array of parameter objects each containing RFC 6902 compliant contents
@return issue structure including generated meta data
@since 2020-05-03
@see http://tools.ietf.org/html/rfc6902
:)
declare function isapi:patchIssue($request as map(*)) {

    let $authmap                := $request?user
    let $issueId                := $request?parameters?id
    let $data                   := utillib:getBodyAsXml($request?body, 'parameters', ())
    
    (:let $s                      := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data) :)
    
    let $check                  :=
        if ($data) then () else (
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        )
    
    let $return                 := isapi:patchIssue($authmap, string($issueId), $data)
    return (
        roaster:response(200, $return)
    )

};

(:~ Retrieves DECOR issue list

@param $bearer-token            - optional. provides authorization for the user. The token, if available, should be on the X-Auth-Token HTTP Header
@param $projectPrefix           - required. limits scope to this project only
@param $searchTerms             - optional. Comma separated list of terms that results should match in issue/@displayName or any desc element. Results all if empty
@param $types                   - optional. Comma separated list of issue/@type values that results should have. Returns all if empty. Supported types are in DECOR.xsd, but include INC (incident), RFC (request for change), FUT (future consideration), CLF (clarification)
@param $priorities              - optional. Comma separated list of issue/@priority values that results should have. Returns all if empty. Supported types are in DECOR.xsd, but include HH, H, N, L, LL
@param $statuscodes             - optional. Comma separated list of issue most recent status code values that results should have. Returns all if empty. Supported types are in DECOR.xsd, but include new, open, inprogress, feedback, closed, rejected, deferred, cancelled
@param $lastassignedids         - optional. Comma separated list of project/author/@id values that the latest assigned person of the issue should match. Returns all if empty. Expected are integers, but should really match ids as listed in project/author/@id
@param $labels                  - optional. Comma separated list of issue most recent label code values that results should have. Returns all if empty. Supported label codes are defined in the project under issues/labels
@param $changedafter            - optional. List issues that where changed on or after this value. Acceptable values: YYYY(-MM(-SST(hh(\:mm(\:ss([+/-]ZZ:((zz))))))) and T(([+/-]\d+(\.\d+)?[YMDhms])?{{hh:mm:ss}}?)?
@param $sort                    - optional. Value to sort on. Supported are 'issue' (display name),'priority','type','status','date','assigned-to','label'. There's no default.
@param $sortorder               - optional. Sort order. Default is ascending. Only other option is descending.
@param $max                     - optional integer, maximum number of results
@return issue structure including generated meta data
@since 2020-08-11
:)
declare function isapi:getIssueList($request as map(*)) {

    let $projectPrefix        := $request?parameters?prefix
    let $searchTerms          := $request?parameters?search
    let $types                := $request?parameters?type
    let $priorities           := $request?parameters?priority
    let $statuscodes          := $request?parameters?status
    let $lastassignedids      := $request?parameters?assignee
    let $labels               := $request?parameters?label
    let $changedafter         := $request?parameters?changedafter
    
    let $objectId             := $request?parameters?objectId
    let $objectEffectiveDate  := $request?parameters?objectEffectiveDate
    
    let $sort                 := $request?parameters?sort
    let $sortorder            := $request?parameters?sortorder
    let $max                  := $request?parameters?max
    
    return
        isapi:getIssueList($request?user, $projectPrefix, $searchTerms, $types, $priorities, $statuscodes, $lastassignedids, $labels, $changedafter, $objectId, $objectEffectiveDate, $sort, $sortorder, $max)
        
};

(:~ Retrieves DECOR issue label list

@param $projectPrefix            - required. limits scope to this project only
@param $code                     - optional. Label code if you want a specific code
@param $sort                     - optional. Value to sort on. Supported are 'issue' (display name),'priority','type','status','date','assigned-to','label'. There's no default.
@param $sortorder                - optional. Sort order. Default is ascending. Only other option is descending.
@return list structure where artifact is ISL containing zero to many labels
@since 2020-08-11
:)
declare function isapi:getIssueLabelList($request as map(*)) {

    let $projectPrefix      := $request?parameters?prefix[string-length() gt 0]
    let $code               := $request?parameters?code
    let $sort               := $request?parameters?sort
    let $sortorder          := $request?parameters?sortorder
    let $max                := $request?parameters?max
    
    let $check              :=
        if (empty($projectPrefix)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter prefix')
        else ()
    
    return
        isapi:getIssueLabelList($projectPrefix, $code, $sort, $sortorder, $max)

};

(:~ Retrieves DECOR issue label

@param $projectPrefix            - required. limits scope to this project only
@param $code                     - required. code for the label to retrieve
@return one or zero label object
@since 2020-08-11
:)
declare function isapi:getIssueLabel($request as map(*)) {

    let $projectPrefix      := $request?parameters?prefix[string-length() gt 0]
    let $code               := $request?parameters?code
    (:let $sort               := $request?parameters?sort
    let $max                := $request?parameters?max:)
    
    let $labels             := if (empty($projectPrefix)) then () else isapi:getIssueLabel($projectPrefix, $code)
    
    let $check              :=
        if (empty($projectPrefix)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter prefix')
        else ()
    let $check              :=
        if (empty($code)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter code')
        else ()
    
    return
        if (empty($labels)) then 
            roaster:response(404, ())
        else
        if (count($labels) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple labels in '", $projectPrefix, "' for code '", $code, "'. Expected 0..1. You should delete and recreate this label. Consider alerting your administrator as this should not be possible."))
        )
        else (
            for $label in $labels
            return
                element {name($label)} {
                    $label/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($label/*)
                }
        )

};

(:~ Create DECOR issue label

@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $projectPrefix            - required. the project prefix to upload to 
@param $request-body             - required. json body containing the new label
@return label structure
@since 2020-05-03
:)
declare function isapi:postIssueLabel($request as map(*)) {

    let $authmap                := $request?user
    let $projectPrefix          := $request?parameters?prefix[string-length() gt 0]
    let $data                   := utillib:getBodyAsXml($request?body)
    
    (:let $s                      := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data):) 
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($data)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $return                 := isapi:createUpdateIssueLabel($authmap, string($projectPrefix), (), $data)
    return (
        roaster:response(201, $return)
    )

};

(:~ Update DECOR issue label

@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $projectPrefix            - required. the project prefix to upload to
@param $code                     - required. the code for the issue label to update 
@param $request-body             - required. json body containing the new label
@return label structure
@since 2020-05-03
:)
declare function isapi:putIssueLabel($request as map(*)) {

    let $authmap                := $request?user
    let $projectPrefix          := $request?parameters?prefix[string-length() gt 0]
    let $code                   := $request?parameters?code
    let $data                   := utillib:getBodyAsXml($request?body)
    
    (:let $s                      := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data):) 
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($data)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $return                 := isapi:createUpdateIssueLabel($authmap, string($projectPrefix), $code, $data)
    return (
        roaster:response(200, $return)
    )

};

(:~ Delete DECOR issue label if it exists. If it does not: no error is returned. No checking is performed on whether or not the label is still in use.

@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $code                     - required. the code for the issue label to update 
@param $projectPrefix            - required. the project prefix to delete from
@return nothing
@since 2020-05-03
:)
declare function isapi:deleteIssueLabel($request as map(*)) {

    let $authmap            := $request?user
    let $projectPrefix      := $request?parameters?prefix[string-length() gt 0]
    let $code               := $request?parameters?code
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    
    let $return                 := isapi:deleteIssueLabel($authmap, $projectPrefix, $code)
    return (
        roaster:response(204, $return)
    )

};

(:~ Subscribe or unsubscribe authenticated user to an issue. Authorship in the issues project is irrelevant

@param $bearer-token required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $projectPrefix optional. the project prefix for the issue
@param $id required parameter denoting the id of the issue
@return issue structure including generated meta data
@since 2021-09-21
:)
declare function isapi:postIssueSubscription($request as map(*)) {

    let $authmap                := $request?user
    let $id                     := $request?parameters?id
    let $projectPrefix          := $request?parameters?prefix[string-length() gt 0]
    let $subscribe              := not($request?parameters?value = false())
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    
    let $issues                 := isapi:getIssueById($id, $projectPrefix)
    let $return                 := if (count($issues) le 1) then userapi:updateUserIssueSubscription($authmap, $issues, $subscribe) else ()

    return
        isapi:getIssue($request)
};

(:~ Return zero or more issues as-is

@param $id           - required. Identifier of the issue to retrieve
@return zero or more issues matching id (more than one constitutes a functional issue)
@since 2014-07-09
:)
declare function isapi:getIssueById($id as xs:string, $projectPrefix as xs:string?) as element(issue)* {
    if (matches($id, '^\d+$')) then 
        if (string-length($projectPrefix) gt 0) then 
            utillib:getDecorByPrefix($projectPrefix)//issue[ends-with(@id, concat('.', $id))]
        else ()
    else  $setlib:colDecorData//issue[@id = $id]
};

(:~ Return zero or more issues as-is

@param $id            - required. Identifier of the object to retrieve the issue for
@param $effectiveDate - optional. Effective date of the object to retrieve the issue for
@return zero or more issues matching an object based on id and optional effectiveDate
@since 2014-07-09
:)
declare function isapi:getIssueByObject($id as xs:string, $effectiveDate as xs:string?) as element(issue)* {
    if (string-length($effectiveDate)=0) then
        $setlib:colDecorData//issue[object[@id = $id]]
    else (
        $setlib:colDecorData//issue[object[@id = $id][@effectiveDate = $effectiveDate]]
    )
};

(:~ Return zero or more expanded issue metadata elements wrapped in a list element.

@param $authmap                 - optional. Map derived from token
@param $projectPrefix           - required. DECOR project/@prefix to get issues from
@param $searchTerms             - optional. List of terms that results should match in issue/@displayName or any desc element. Results all if empty
@param $types                   - optional. List of issue/@type values that results should have. Returns all if empty
@param $priorities              - optional. List of issue/@priority values that results should have. Returns all if empty
@param $statuscodes             - optional. List of issue most recent status code values that results should have. Returns all if empty
@param $lastassignedids         - optional. List of project/author/@id values that the latest assigned person of the issue should match. Returns all if empty
@param $labels                  - optional. List of issue most recent label code values that results should have. Returns all if empty
@param $changedafter            - optional. List issues that where changed on or after this value. Acceptable values: YYYY(-MM(-SST(hh(\:mm(\:ss([+/-]ZZ:((zz))))))) and T(([+/-]\d+(\.\d+)?[YMDhms])?{{hh:mm:ss}}?)?
@param $sort                    - optional. Value to sort on. Supported are 'issue' (display name),'priority','type','status','date','assigned-to','label'. There's no default.
@param $sortorder               - optional. Sort order. Default is ascending. Only other option is descending.
@param $max                     - optional. Maximum number of results with minimum 1. Default is $isapi:maxResults (50)
@return list structure containing zero or more issue objects with expanded meta data, object associations but no events.
@since 2014-07-09
:)
declare function isapi:getIssueList($authmap as map(*)?, $projectPrefix as xs:string, $searchTerms as xs:string*, $types as xs:string*, $priorities as xs:string*, $statuscodes as xs:string*, $lastassignedids as xs:string*, $labels as xs:string*, $changedafter as xs:string?, $objectId as xs:string?, $objectEffectiveDate as xs:string?, $sort as xs:string?, $sortorder as xs:string?, $max as xs:integer?) as element(list) {
    let $projectPrefix        := $projectPrefix[string-length() gt 0]
    let $searchTerms          := for $term in $searchTerms     return tokenize(lower-case($term[string-length() gt 0]),'[\s,]')
    let $types                := for $term in $types           return tokenize($term[string-length() gt 0],'[\s,]')
    let $priorities           := for $term in $priorities      return tokenize($term[string-length() gt 0],'[\s,]')
    let $statuscodes          := for $term in $statuscodes     return tokenize($term[string-length() gt 0],'[\s,]')
    let $lastassignedids      := for $term in $lastassignedids return tokenize($term[string-length() gt 0],'[\s,]')
    let $labels               := for $term in $labels          return tokenize($term[string-length() gt 0],'[\s,]')
    let $changedafterv        := $changedafter[string-length() gt 0]
    let $objectId             := $objectId[string-length() gt 0]
    let $objectEffectiveDate  := $objectEffectiveDate[string-length() gt 0]
    let $sort                 := $sort[string-length() gt 0]
    let $sortorder            := $sortorder[. = 'descending']
    
    let $decor                := if (empty($projectPrefix)) then () else utillib:getDecorByPrefix($projectPrefix)
       
    let $projectAuthors       := $decor/project/author
    let $issues               := $decor/issues/issue
    let $allcnt               := count($issues)
    let $termIsId             := count($searchTerms)=1 and $searchTerms[1] castable as xs:integer
    let $changedafter         := utillib:parseAsDateTime($changedafterv)
    
    let $check                :=
        if (empty($changedafterv)) then () else if (empty($changedafter)) then
            error($errors:BAD_REQUEST, concat('Parameter changedafter ''', $changedafterv, ''' shall be a (partial) date-time yyyy[-mm[-dd[Thh[:mm[:ss[Z | +/-hh:mm]]]]]]'))
        else ()
        
    let $issues             := 
        if ($termIsId) then
            $issues[ends-with(@id,concat('.',$searchTerms[1]))]
        else (
            let $r  := 
                if (empty($searchTerms)) then $issues else (
                    let $luceneQuery    := utillib:getSimpleLuceneQuery($searchTerms)
                    let $luceneOptions  := utillib:getSimpleLuceneOptions()
                    return 
                        $issues[ft:query(@displayName|*/desc,$luceneQuery,$luceneOptions)]
                )
            let $r  := 
                if (empty($objectId))               then $r else 
                if (empty($objectEffectiveDate))    then $r[object[@id = $objectId]] else (
                    $r[object[@id = $objectId][@effectiveDate = $objectEffectiveDate]]
                )
            let $r  := if (empty($types))           then $r else ($r[@type=$types])
            let $r  := if (empty($priorities))      then $r else ($r[@priority=$priorities])
            let $r  := if (empty($changedafter))    then $r else ($r[tracking[xs:dateTime(@effectiveDate) ge $changedafter] | assignment[xs:dateTime(@effectiveDate) ge $changedafter]])
            
            return $r
        )

    (:can sort on indexed db content -- faster:)
    let $results            :=
        switch ($sort) 
        case 'issue'        return 
            if (empty($sortorder)) then 
                for $issue in $issues order by $issue/@displayName return $issue
            else
                for $issue in $issues order by $issue/@displayName descending return $issue
        case 'priority'     return
            if (empty($sortorder)) then 
                for $issue in $issues order by $issue/@priority return $issue
            else
                for $issue in $issues order by $issue/@priority descending return $issue
        case 'type'         return
            if (empty($sortorder)) then 
                for $issue in $issues order by $issue/@type return $issue
            else
                for $issue in $issues order by $issue/@type descending return $issue
        default             return $issues
    
    (:calculate missing meta data:)
    let $results            :=
        for $issue in $results
        let $issuemeta  := isapi:getIssueMeta($authmap, $issue, false(), false(), false(), $projectAuthors, $projectPrefix)
        let $return     := if (empty($statuscodes) or $termIsId)       then $issuemeta else $issuemeta[@currentStatusCode=$statuscodes]
        let $return     := if (empty($lastassignedids) or $termIsId)   then $return    else if ($lastassignedids='#UNASSIGNED#') then $return[@lastAssignmentId=''] else $return[@lastAssignmentId=$lastassignedids]
        let $return     := if (empty($labels) or $termIsId)            then $return    else $return[tokenize(@currentLabels,'\s')=$labels]
        return
            $return
    
    (:can sort on calculated content -- slower:)
    let $results            :=
        switch ($sort) 
        case 'status'       return
            if (empty($sortorder)) then 
                for $issue in $issues order by $issue/@currentStatusCode return $issue
            else
                for $issue in $issues order by $issue/@currentStatusCode descending return $issue
        case 'date'         return
            if (empty($sortorder)) then 
                for $issue in $issues order by $issue/@lastDate return $issue
            else
                for $issue in $issues order by $issue/@lastDate descending return $issue
        case 'assigned-to'  return
            if (empty($sortorder)) then 
                for $issue in $issues order by $issue/@lastAssignment return $issue
            else
                for $issue in $issues order by $issue/@lastAssignment descending return $issue
        case 'label'        return
            if (empty($sortorder)) then 
                for $issue in $issues order by $issue/@currentLabels return $issue
            else
                for $issue in $issues order by $issue/@currentLabels descending return $issue
        (:default would mean id or something we don't support, but that would be impossible at this point:)
        default             return $results
    
    let $count              := count($results)
    let $max                := if ($max ge 1) then $max else $isapi:maxResults
    
    return
        <list artifact="IS" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements(subsequence($results, 1, $max))
        }
        </list>
        
};

(:~ Return zero or one issue label as-is

@param $projectPrefix            - required. the project prefix to get the labels from
@param $code                     - required. the code for the issue label to retrieve
@return label or nothing
@since 2014-07-09
:)
declare function isapi:getIssueLabel($projectPrefix as xs:string, $code as xs:string) as element(label)* {
(:
        <label code="" name="" color="">
            <desc language="en-US">....</desc>
        </label>
:)
    utillib:getDecorByPrefix($projectPrefix)/issues/labels/label[@code = $code]
        
};

(:~ Return zero or one issue label as-is

@param $projectPrefix            - required. the project prefix to get the labels from
@param $code                     - optional. the code for the issue label to retrieve. gets all labels if empty
@return list structure where artifact is ISL containing zero or one label
@since 2014-07-09
:)
declare function isapi:getIssueLabelList($projectPrefix as xs:string, $code as xs:string?, $sort as xs:string?, $sortorder as xs:string?, $max as xs:integer?) as element(list) {
(:
    <labels>
        <label code="" name="" color="">
            <desc language="en-US">....</desc>
        </label>
    </labels>
:)
    let $sort               := $sort[string-length() gt 0]
    let $sortorder          := $sortorder[. = 'descending']
    
    let $labels             := 
        if (empty($projectPrefix)) then () else 
        if (empty($code)) then 
            utillib:getDecorByPrefix($projectPrefix)/issues/labels/label
        else (
            utillib:getDecorByPrefix($projectPrefix)/issues/labels/label[@code = $code]
        )
    let $allcnt             := count($labels)
    
    (:can sort on indexed db content -- faster:)
    let $results            :=
        switch ($sort) 
        case 'code'         return 
            if (empty($sortorder)) then 
                for $label in $labels order by $label/@code return $label
            else
                for $label in $labels order by $label/@code descending return $label
        default             return $labels
    
    let $count              := count($results)
    let $max                := if ($max ge 1) then $max else $isapi:maxResults
    
    return
        <list artifact="ISL" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements(subsequence($results, 1, $max))
        }
        </list>
};

(:~ Central logic for creating a new issue

@param $authmap      - required. Map derived from token
@param $prefix       - required. DECOR project/@prefix to get issues from
@param $data         - required. DECOR issue object as xml
@return issue object as xml with json:array set on elements
:)
declare function isapi:postIssue($authmap as map(*), $projectPrefix as xs:string, $data as element()) as element(issue) {

    let $decor                  := 
        if (count($projectPrefix[not(. = '')]) = 1) then utillib:getDecorByPrefix($projectPrefix[not(. = '')]) else (
            error($errors:BAD_REQUEST, 'You are missing required parameter prefix')
        ) 
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix ''', $projectPrefix, ''', does not exist. Cannot create in non-existent project.'))
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-ISSUES)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to add issues to project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                  := 
        if ($data/@id) then 
            if ($decor//issue[@id = $data/@id]) then () else (
                error($errors:FORBIDDEN, concat('Id ''', $data/@id, ''' already exists.'))
            )
        else ()
    let $check                  := 
        if ($data/@displayName) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall have a displayName')
        )
    let $check                  :=
        if (empty($data/object[string(@id) = ''])) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall not have ''object'' without an id')
        )
    let $check                  :=
        if (empty($data/tracking[string(@statusCode) = ''])) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall not have a ''tracking'' without a statusCode or where value is empty')
        )
    let $check                  :=
        if (empty($data/assignment[string(@to) = ''])) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall not have an ''assignment'' without a to or where value is empty')
        )
    let $check                  :=
        if (empty($data/*/author[string(@id) = ''])) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall not have an ''assignment'' or ''tracking'' author without an id or where value is empty')
        )
    let $check                  :=
        if (empty($data/*/desc[string(@language) = ''])) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall not have an ''assignment'' or ''tracking'' desc without a language or where value is empty')
        )
        
    let $prefix                 := $decor/project/@prefix
    let $projectAuthor          := $decor/project/author[@username = $authmap?name]
    let $newId                  := (decorlib:getNextAvailableIdP($decor, $decorlib:OBJECTTYPE-ISSUE, decorlib:getDefaultBaseIdsP($decor, $decorlib:OBJECTTYPE-ISSUE)/@id)/@id)[1]
    
    let $check                  :=
        if (utillib:isOid($newId[1])) then () else (
            error($errors:SERVER_ERROR, 'Cannot create an id for the issue. Please check default base id for type IS (issue) in project ' || $projectPrefix)
        )
    
    let $issueAuthorId          := $data/tracking/author/@id
    let $issueAuthorUserName    := $decor/project/author[@id = $issueAuthorId]/@username
    let $issueAssignedId        := ($data/assignment/@to)[not(. = '')][last()]
    let $issueAssignedUserName  := $decor/project/author[@id = $issueAssignedId]/@username
    
    let $newIssue               :=
        <issue>
        {
            attribute id {$newId},
            attribute displayName {$data/@displayName},
            attribute priority {($data/@priority, 'N')[1]},
            attribute type {($data/@type, 'RFC')[1]}
        }
        {
            for $object in $data/object
            let $objectid   := $object/@id
            let $objected   := $object/@effectiveDate
            group by $objectid, $objected
            return (
                <object>
                {
                    $object[1]/@id, 
                    $object[1]/@effectiveDate[not(. = '')], 
                    (: unavoidable performance hit. can't trust the incoming type ... :)
                    if ($setlib:colDecorData//dataset[(@id | @ref) = $objectid]) then attribute type {'DS'} else
                    if ($setlib:colDecorData//concept[(@id | @ref) = $objectid]) then attribute type {'DE'} else
                    if ($setlib:colDecorData//scenario[(@id | @ref) = $objectid]) then attribute type {'SC'} else
                    if ($setlib:colDecorData//transaction[(@id | @ref) = $objectid]) then attribute type {'TR'} else
                    if ($setlib:colDecorData//valueSet[(@id | @ref) = $objectid]) then attribute type {'VS'} else
                    if ($setlib:colDecorData//codeSystem[(@id | @ref) = $objectid]) then attribute type {'CS'} else
                    if ($setlib:colDecorData//template[(@id | @ref) = $objectid]) then attribute type {'TM'} else
                    if ($setlib:colDecorData//issue[(@id | @ref) = $objectid]) then attribute type {'IS'} else
                    if ($setlib:colDecorData//mapping[(@id | @ref) = $objectid]) then attribute type {'MP'} else 
                    if ($setlib:colDecorData//questionnaire[(@id | @ref) = $objectid]) then attribute type {'QQ'} else
                    if ($setlib:colDecorData//questionnaireresponse[(@id | @ref) = $objectid]) then attribute type {'QR'} else
                    if ($setlib:colDecorData//structuredefinition[(@id | @ref) = $objectid]) then attribute type {'SD'} else 
                    if ($setlib:colDecorDataIG//implementationGuide[(@id | @ref) = $objectid]) then attribute type {'IG'} else (
                        error($errors:BAD_REQUEST, 'Submitted data contains an object with id ''' || $objectid ||
                         ''' that cannot be associated to any dataset, concept, scenario, transaction, valueSet, codeSystem, template, issue, mapping, questionnaire/response, structuredefinition or implementationguide')
                    )
                    , 
                    $object[1]/@name[not(. = '')]
                }
                </object>
            )
        }
        {
            for $event at $i in $data/tracking | $data/assignment
            let $timestamp  := format-dateTime(current-dateTime()+xs:dayTimeDuration(concat('PT', $i, 'S')), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')
            order by $timestamp
            return
                element {local-name($event)} {
                    if ($event[self::tracking]) then
                        for $att in $event/@statusCode | $event/@labels | attribute effectiveDate {$timestamp}
                        order by local-name($att)
                        return $att
                    else (
                        for $att in $event/@to | $event/@name | $event/@labels | attribute effectiveDate {$timestamp}
                        order by local-name($att)
                        return $att
                    )
                    ,
                    <author id="{$projectAuthor/@id}">{data(($projectAuthor, $authmap?name)[1])}</author>
                    ,
                    if ($event[self::tracking][empty(desc)]) then <desc language="{$decor/project/@defaultLanguage}"/> else ()
                    ,
                    utillib:prepareFreeFormMarkupWithLanguageForUpdate($event/desc)
                }
        }
        </issue>

    let $insert                 :=
        if ($decor[issues]) then
            update insert $newIssue into $decor/issues
        else (
            update insert <issues>{$newIssue}</issues> into $decor
        )
    let $delete                 := update delete $decor//issue[@id = $newId]//@json:array

    let $subsribe               :=
        for $authorUserName in $decor/project/author/@username
        let $userAutoSubscribes := 
            if (empty($authmap)) then false() else (
                userapi:userHasIssueAutoSubscription($authorUserName, $prefix, $newId, $newIssue/object/@type, $issueAuthorUserName[1], $issueAssignedUserName[1])
            )
        return
            if ($userAutoSubscribes) then
                userapi:updateUserIssueSubscription($authmap, $newId, true())
            else ()
    
    let $includeSubscriptionInfo    := true()
    let $includeObjects             := true()
    let $expandIssues               := true()
    let $includeEvents              := true()
    
    let $result                     := isapi:getExpandedIssues($authmap, $newId, $includeObjects, $expandIssues, $includeEvents)
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

(:~ Central logic for updating an existing issue displayName|priority|type, add|remove object associations, add|replace tracking|assignment

@param $authmap         - required. Map derived from token
@param $id              - required. DECOR issue/@id to update
@param $request-body    - required. DECOR xml element parameter elements each containing RFC 6902 compliant contents
@return issue object as xml with json:array set on elements
:)
declare function isapi:patchIssue($authmap as map(*), $id as xs:string, $request-body as element(parameters)) as element(issue) {
(:
<issue>
    <parameter op="replace" path="/displayName" value="new value"/>
    <parameter op="replace" path="/priority" value="new value"/>
    <parameter op="add" path="/">
        <value>
            <tracking statusCode="closed">
                <desc language="de-DE">yadadadad</desc>
            </tracking>
        </value>
    </parameter>
</issue>
:)
    let $issue                  := isapi:getIssueById($id, ())
    let $decor                  := $issue/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix
    let $projectAuthor          := $decor/project/author[@username = $authmap?name]
    
    let $check                  :=
        if ($issue) then () else (
            error($errors:BAD_REQUEST, concat('Issue with id ''', $id, ''' does not exist'))
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $issue/ancestor::decor, $decorlib:SECTION-ISSUES)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify issues in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if ($request-body[parameter]) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall be an array of ''parameter''.')
        )
    let $unsupportedops         := $request-body/parameter[not(@op = ('add', 'replace', 'remove'))]
    let $check                  :=
        if ($unsupportedops) then
            error($errors:BAD_REQUEST, 'Submitted parameters shall have supported ''op'' value. Found ''' || string-join(distinct-values($unsupportedops/@op), ''', ''') || ''', expected ''add'', ''replace'', or ''remove''') 
        else ()
    let $check                  :=
        for $param in $request-body/parameter
        return
            if ($param[@op = 'add'][@path = '/']/value[object | tracking | assignment]) then ()
            else
            if ($param[@op = 'remove'][@path = '/']/value[object]) then ()
            else
            if ($param[@op = 'replace'][@path = '/']/value[tracking | assginment][@effectiveDate]) then ()
            else
            if ($param[@op = 'replace'][@path = '/displayName'][@value[not(. = '')]]) then ()
            else
            if ($param[@op = 'replace'][@path = '/priority']) then (
                if ($param[@value = $isapi:ISSUE-PRIORITY/enumeration/@value]) then () else (
                    concat('Parameter value ''', $param/@value, ''' for priority not supported. Supported are: ', string-join($isapi:ISSUE-PRIORITY/enumeration/@value, ' '))
                )
            )
            else
            if ($param[@op = 'replace'][@path = '/type']) then (
                if ($param[@value = $isapi:ISSUE-TYPE/enumeration/@value]) then () else (
                    concat('Parameter value ''', $param/@value, ''' for type not supported. Supported are: ', string-join($isapi:ISSUE-TYPE/enumeration/@value, ' '))
                )
            )
            else
            if ($param/object[empty(@id)] | $param/object[empty(@type)]) then 
                concat('Parameter object is missing ''id'' and/or ''type'': op=''', $param/@op, ''' path=''', $param/@path, '''', if ($param[@value]) then concat(' value=''', $param/@value, '''') else string-join($param/value/*/name(), ' '), '.')
            else
            if ($param[tracking[empty(@statusCode)]]) then 
                concat('Parameter tracking is missing ''statusCode'': op=''', $param/@op, ''' path=''', $param/@path, '''', if ($param[@value]) then concat(' value=''', $param/@value, '''') else string-join($param/value/*/name(), ' '), '.')
            else
            if ($param[assignment[empty(@to)]]) then 
                concat('Parameter tracking is missing ''to'': op=''', $param/@op, ''' path=''', $param/@path, '''', if ($param[@value]) then concat(' value=''', $param/@value, '''') else string-join($param/value/*/name(), ' '), '.')
            else (
                concat('Parameter combination not supported: op=''', $param/@op, ''' path=''', $param/@path, '''', if ($param[@value]) then concat(' value=''', $param/@value, '''') else string-join($param/value/*/name(), ' '), '.')
            )
     let $check                 :=
        if (empty($check)) then () else (
            error($errors:BAD_REQUEST,  string-join ($check, ' '))
        )
    
    let $update                 :=
        for $param in $request-body/parameter
        return
            switch ($param/@path)
            case '/displayName'
            case '/priority'
            case '/type' return (
                let $attname  := substring-after($param/@path, '/')
                let $new      := attribute {$attname} {$param/@value}
                let $stored   := $issue/@*[name() = $attname]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $issue
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            default return ()
    
    let $update                 :=
        for $param in $request-body/parameter
        return
            if ($param[@op = 'add' or @op = 'replace'][@path = '/']/value[object | tracking | assignment]) then (
                for $object in $param/value/object
                let $updatedObject  := <object>{$object/@id[not(. = '')], $object/@effectiveDate[not(. = '')], $object/@type[not(. = '')], $object/@name[not(. = '')]}</object>
                return
                    if ($object[@id][@effectiveDate]) then (
                        update delete $issue/object[@id = $object/@id][@effectiveDate = $object/@effectiveDate],
                        update insert $updatedObject preceding ($issue/tracking | $issue/assignment)[1]
                    )
                    else (
                        update delete $issue/object[@id = $object/@id],
                        update insert $updatedObject preceding ($issue/tracking | $issue/assignment)[1]
                    )
                ,
                (:
                    <tracking effectiveDate="2012-09-09T15:54:28" statusCode="open" labels="">
                        <author id="3">Full Name</author>
                        <desc language="nl-NL">...</desc>
                    </tracking>
                    <assignment to="1" name="Full Name" effectiveDate="2012-09-09T15:54:28" labels="">
                        <author id="1">Full Name</author>
                        <desc language="de-DE">...</desc>
                    </assignment>
                :)
                for $object in $param/value/tracking | $param/value/assignment
                let $event          := $issue/*[name() = name($object)][@effectiveDate = $object/@effectiveDate]
                let $assignAuthor   := $decor/project/author[@id = $object/@to]
                let $updatedEvent   :=
                    element {name($object)} {
                        if ($event/@effectiveDate[not(. = '')]) then 
                            $event/@effectiveDate
                        else 
                            attribute effectiveDate {substring(string(current-dateTime()), 1, 19)}
                        ,
                        if ($object/@labels[not(. = '')]) then 
                            $object/@labels 
                        else ()
                        ,
                        (: tracking requires statusCode. This is checed above. No need to recheck here :)
                        if ($object[self::tracking]/@statusCode) then 
                            $object/@statusCode 
                        else (),
                        (: tracking requires statusCode. This is checed above. No need to recheck here :)
                        if ($object[self::assignment]/@to) then (
                            $object/@to, attribute name {($assignAuthor[not(. = '')], $object/@name[not(. = '')])[1]}
                        ) else ()
                        ,
                        $event/author
                        ,
                        <author id="{$projectAuthor/@id}">{if ($event) then attribute effectiveDate {substring(string(current-dateTime()), 1, 19)} else (), data(($projectAuthor, $authmap?name)[1])}</author>
                        ,
                        utillib:prepareFreeFormMarkupWithLanguageForUpdate($object/desc)
                    }
                return
                    if ($event) then
                        update replace $event with $updatedEvent
                    else (
                        update insert $updatedEvent into $issue 
                    )
            )
            else
            if ($param[@op = 'remove'][@path = '/']/value[object]) then (
                for $object in $param/value/object
                return
                    if ($object[@id][@effectiveDate]) then
                        update delete $issue/object[@id = $object/@id][@effectiveDate = $object/@effectiveDate]
                    else (
                        update delete $issue/object[@id = $object/@id]
                    )
            )
            else ()
    
    let $includeSubscriptionInfo    := true()
    let $includeObjects             := true()
    let $expandIssues               := true()
    let $includeEvents              := true()
    
    let $result                     := isapi:getExpandedIssues($authmap, $issue, $includeObjects, $expandIssues, $includeEvents)
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

(:~ Central logic for updating an existing issue

@param $authmap         - required. Map derived from token
@param $id              - required. DECOR issue/@id to update
@param $request-body    - required. DECOR issue xml element containing everything that should be in the updated issue
@return issue object as xml with json:array set on elements
:)
declare function isapi:putIssue($authmap as map(*), $id as xs:string, $data as element()) as element(issue) {

    let $issue                  := isapi:getIssueById($id, ())
    let $decor                  := $issue/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if ($issue) then () else (
            error($errors:BAD_REQUEST, concat('Issue with id ''', $id, ''' does not exist'))
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $issue/ancestor::decor, $decorlib:SECTION-ISSUES)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify issues in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if ($data[@id = $id] | $data[empty(@id)]) then () else (
            error($errors:BAD_REQUEST, concat('Submitted data shall have no issue id or the same issue id as the issue id ''', $id, ''' used for updating. Found in request body: ', ($data/@id, 'null')[1]))
        )
    let $check                  :=
        if (empty($data/object[string(@id) = '' or string(@type) = ''])) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall not have ''object'' without an id and/or type')
        )
    let $check                  :=
        if (empty($data/tracking[string(@statusCode) = ''])) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall not have a ''tracking'' without a statusCode or where value is empty')
        )
    let $check                  :=
        if (empty($data/assignment[string(@to) = ''])) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall not have an ''assignment'' without a to or where value is empty')
        )
    let $check                  :=
        if (empty($data/*/author[string(@id) = ''])) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall not have an ''assignment'' or ''tracking'' author without an id or where value is empty')
        )
    let $check                  :=
        if (empty($data/*/desc[string(@language) = ''])) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall not have an ''assignment'' or ''tracking'' desc without a language or where value is empty')
        )
        
    let $newIssue               :=
        <issue>
        {
            attribute id {$id},
            attribute displayName {$data/@displayName},
            attribute priority {($data/@priority, 'N')[1]},
            attribute type {($data/@type, 'RFC')[1]}
        }
        {
            for $object in $data/object
            let $objectid   := $object/@id
            let $objecttype := $object/@type
            let $objected   := $object/@effectiveDate
            group by $objectid, $objecttype, $objected
            return
                <object>
                {
                    $object[1]/@id, $object[1]/@effectiveDate[not(. = '')], $object[1]/@type, $object[1]/@name[not(. = '')]
                }
                </object>
        }
        {
            for $event at $i in $data/tracking | $data/assignment
            let $timestamp  := format-dateTime(current-dateTime()+xs:dayTimeDuration(concat('PT', $i, 'S')), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')
            order by $timestamp
            return
                element {local-name($event)} {
                    if ($event[self::tracking]) then
                        for $att in $event/@statusCode | $event/@labels | attribute effectiveDate {$timestamp}
                        order by local-name($att)
                        return $att
                    else (
                        for $att in $event/@to | $event/@name | $event/@labels | attribute effectiveDate {$timestamp}
                        order by local-name($att)
                        return $att
                    )
                    ,
                    for $author in $event/author
                    let $projectAuthor  := $decor/project/author[@id = $author/@id]
                    return
                        <author>
                        {
                            $author/@id, $author/@effectiveDate[not(. = '')]
                            ,
                            data(($projectAuthor[not(. = '')], $projectAuthor/@username, $author)[1])
                        }
                        </author>
                    ,
                    if ($event[self::tracking][empty(desc)]) then <desc language="{$decor/project/@defaultLanguage}"/> else ()
                    ,
                    utillib:prepareFreeFormMarkupWithLanguageForUpdate($event/desc)
                }
        }
        </issue>
    
    let $update                 := update replace $issue with $newIssue
    let $delete                 := update delete $issue//@json:array
    
    let $issueAuthorId          := ($issue/*/author/@id)[1]
    let $issueAuthorUserName    := $decor/project/author[@id = $issueAuthorId]/@username
    let $issueAssignedId        := ($issue/assignment/@to)[not(. = '')][last()]
    let $issueAssignedUserName  := $decor/project/author[@id = $issueAssignedId]/@username
    
    let $subsribe               :=
        for $authorUserName in $decor/project/author/@username
        let $userAutoSubscribes := 
            if (empty($authmap)) then false() else (
                userapi:userHasIssueAutoSubscription($authorUserName, $projectPrefix, $newIssue/@id, $newIssue/object/@type, $issueAuthorUserName[1], $issueAssignedUserName[1])
            )
        return
            if ($userAutoSubscribes) then
                userapi:updateUserIssueSubscription($authmap, $newIssue/@id, true())
            else ()
    
    let $includeSubscriptionInfo    := true()
    let $includeObjects             := true()
    let $expandIssues               := true()
    let $includeEvents              := true()
    
    let $result                     := isapi:getExpandedIssues($authmap, $issue, $includeObjects, $expandIssues, $includeEvents)
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

(:~ Returns all issues optionally filtered on project/id/name. Issues carry only attributes (whatever was available in the db). 

@param $projectPrefix optional DECOR project prefix to search in. Special value '*' searches every non-private DECOR project. Empty searches every non-private Building Block Repository DECOR project. Any other value is assumed to be project/@prefix.
@param $searchTerms optional sequence of terms to look for. Returns every object if empty
@param $maxResults optional maximum number of results to return, defaults to $adsearch:maxResults
@param $version optional. Go to some archived release. Defaults to active data
@return resultset with max $maxResults results
@since 2014-06-06
:)
declare function isapi:searchIssue($projectPrefix as xs:string?, $searchTerms as xs:string*, $maxResults as xs:integer?, $version as xs:string?, $language as xs:string?) as element(issue)* {
    let $maxResults     := if ($maxResults) then $maxResults else $isapi:maxResults
    
    let $decorObjects   := 
        if (empty($projectPrefix)) then (
            if (empty($version)) then (
                $setlib:colDecorData//issue[ancestor::decor[@repository='true']][not(ancestor::decor[@private='true'])]
             ) else (
                $setlib:colDecorVersion//issue[ancestor::decor[@repository='true']][not(ancestor::decor[@private='true'])][ancestor::decor/@versionDate=$version]
             )
        ) else
        if ($projectPrefix='*') then (
            if (empty($version)) then (
                $setlib:colDecorData//issue[not(ancestor::decor[@private='true'])]
            ) else (
                $setlib:colDecorVersion//issue[not(ancestor::decor[@private='true'])][ancestor::decor/@versionDate=$version]
            )
        ) else (
            if (empty($version)) then (
                $setlib:colDecorData//issue[ancestor::decor/project[@prefix=$projectPrefix]]
            ) else (
                $setlib:colDecorVersion//issue[ancestor::decor/project[@prefix=$projectPrefix]][ancestor::decor/@versionDate=$version]
            )
        )
    
    let $results        := 
        if (count($searchTerms)=0) then 
            $decorObjects
        else (
            let $luceneQuery    := utillib:getSimpleLuceneQuery($searchTerms)
            let $luceneOptions  := utillib:getSimpleLuceneOptions()
            return
            if (count($searchTerms)=1 and matches($searchTerms[1],'^\d+(\.\d+)*$')) then
                $decorObjects[ends-with(@id, $searchTerms[1])]
            else if (count($searchTerms)=1 and matches($searchTerms[1],'^[0-2](\.(0|[1-9][0-9]*))*$')) then
                $decorObjects[@id=$searchTerms[1]]
            else (
                $decorObjects[ft:query(@displayName,$luceneQuery,$luceneOptions)] | $decorObjects[ft:query(*/desc,$luceneQuery,$luceneOptions)]
            )
        )
    
    (:shortest match first:)
    let $results        :=
        for $r in $results
        order by string-length($r/@displayName)
        return $r
    
    (:let $count          := count($results):)

    return
        subsequence($results, 1, $maxResults)
};

(:~ Central logic for creating a new issue label or updating one

@param $authmap                 - required. Map derived from token
@param $projectPrefix           - required. DECOR project/@prefix to get issues from
@param $code                    - optional. the code for the issue label to update, empty if to be created
@param $data                    - required. DECOR issue object as xml
@return issue label object as xml with json:array set on elements
:)
declare function isapi:createUpdateIssueLabel($authmap as map(*), $projectPrefix as xs:string, $code as xs:string?, $data as element()) as element(label) {
(:
<label code="DS" name="datasets" color="green">
    <desc language="en-US">My great label</desc>
</label>
:)    
    let $decor                  := utillib:getDecorByPrefix($projectPrefix)
    let $label                  := $decor/issues/labels/label[@code = $code]
    
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix ''', $projectPrefix, ''', does not exist. Cannot create in non-existent project.'))
        )
    
    let $projectPrefix          := $decor/project/@prefix
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-ISSUES)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify issues in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    (: relevant for updates only :)
    let $check                  :=
        if (empty($code)) then
            if ($decor/issues/labels/label[@code = $data/@code]) then 
                error($errors:FORBIDDEN, concat('Issue label with code ''', $data/@code, ''' already exists.'))
            else ()
        else ()
    let $check                  :=
        if (empty($code)) then () else if ($label) then () else (
            error($errors:BAD_REQUEST, concat('Issue label with code ''', $code, ''' does not exist so cannot update'))
        )
    let $check                  := (
        if (empty($code)) then () else if ($data[empty(@code)]) then () else if ($data[@code = $code]) then () else (
            concat('Label code in data (',$data/@code,') SHALL NOT be different from label to be updated (', $code,')')
        ),
        if (empty($code)) then 
            if ($data[@code[not(. = '')][string-length() le 128]]) then () else (
                'Label code SHALL be between 1 and 128 characters. Shorter is better'
            )
        else (),
        if ($data[@name[not(. = '')][string-length() le 80]]) then () else (
            'Label name SHALL be between 1 and 80 characters. Shorter is better'
        ),
        if ($data[@color[not(. = '')]]) then () else (
            'Label color SHALL be some allowable web color like red, #ff0000, or rgb(255, 0, 0)'
        )
    )
    let $check                  :=
        if (empty($check)) then () else (
            error($errors:BAD_REQUEST, string-join ($check, ' '))
        )
    
    let $newLabel               := 
        <label>
        {
            attribute code {($code, $data/@code)[1]},
            $data/@name, 
            $data/@color, 
            for $desc in $data/desc 
            return 
                <desc language="{($desc/@language[not(. = '')] , $decor/project/@defaultLanguage)[1]}">{$desc/node()}</desc>
        }
        </label>
    
    let $update                 := 
        if ($decor/issues/labels) then () else 
        if ($decor/issues/issue) then 
            update insert <labels/> preceding $decor/issues/issue[1] 
        else (
            update insert <labels/> into $decor/issues
        )
    let $update                 :=
        if ($label) then 
            update replace $label with $newLabel 
        else (
            update insert $newLabel into $decor/issues/labels
        )
    
    return
        <label xmlns:json="http://www.json.org">
        {
            $newLabel/@*,
            utillib:addJsonArrayToElements($newLabel/*)
        }
        </label>
};

(:~ Central logic for deleting an issue label. If the label does not exist, then no error is given. Deletion does not check if the label is still in use somewhere (!)

@param $authmap                 - required. Map derived from token
@param $projectPrefix           - required. DECOR project/@prefix to get issues from
@param $code                    - required. the code for the issue label to delete
@return nothing
:)
declare function isapi:deleteIssueLabel($authmap as map(*), $projectPrefix as xs:string, $code as xs:string) {
    let $decor                  := 
        if (count($projectPrefix[not(. = '')]) = 1) then utillib:getDecorByPrefix($projectPrefix[not(. = '')]) else (
            error($errors:BAD_REQUEST, 'You are missing required parameter prefix')
        )
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix ''', $projectPrefix, ''', does not exist.'))
        )
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-ISSUES)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify issues in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $labelsInUse            := distinct-values(tokenize(string-join($decor/issues//tracking/@labels | $decor/issues//assignment/@labels, ' '), ' '))
    let $check                  :=
        if ($labelsInUse[. = $code]) then 
            error($errors:FORBIDDEN, 'Label ' || $code || ' SHALL NOT be deleted while in use.')
        else ()

    let $label                  := $decor/issues/labels/label[@code = $code]
    let $update                 := update delete $label
    
    return
        ()
};

(:~ Return expanded issue metadata on the issue element. The expansion adds the following attributes:

* currentStatusCode - the current issue status based on the newest available tracking
* currentLabels - the current issue label codes based on the newest available event (tracking|assignment) 
* lastDate - the latest change on the event based on the newest available event (tracking|assignment), not the last edit that may be later
* lastAuthorId - id of the user this issue was last authored by
* lastAuthor - name of the user this issue was last authored by
* lastAssignmentId - id of the user this issue is assigned to
* lastAssignment - name of the user this issue is assigned to
* iddisplay - a more readable version of the issue id based on the project baseId prefixes
* ident - the project prefix of the project this issue lives in
* currentUserIsSubscribed - boolean based on if currently logged in user has a subscription for this issue
* currentUserAutoSubscribes - boolean based on if currently logged in user has an automatic subscription for this issue

@param $authmap                 - optional. Map derived from token
@param $idOrIssue               - optional. Identifier of the issue or the issue itself
@param $includeSubscriptionInfo - required. Whether or not to include subscription info the current user
@param $includeObjects          - required. Boolean to decide whether or not to include objects in the output
@param $includeEvents           - required. Boolean to decide whether or not to include events in the output
@return issue with expanded metadata
@since 2014-07-09
:)
declare function isapi:getIssueMeta($authmap as map(*)?, $idOrIssue as item(), $includeSubscriptionInfo as xs:boolean, $includeObjects as xs:boolean, $includeEvents as xs:boolean) as element(issue) {
let $issue                  := if ($idOrIssue instance of element(issue)) then $idOrIssue else isapi:getIssueById($idOrIssue, ())
let $decorAuthors           := $issue/ancestor::decor/project/author
let $projectPrefix          := $issue/ancestor::decor/project/@prefix

return isapi:getIssueMeta($authmap, $issue, $includeSubscriptionInfo, $includeObjects, $includeEvents, $decorAuthors, $projectPrefix)
};

(:~ Return expanded issue metadata on the issue element

@param $authmap                 - required. Map derived from token
@param $idOrIssue               - optional. Identifier of the issue or the issue itself
@param $includeSubscriptionInfo - required. Whether or not to include subscription info the current user
@param $includeObjects          - required. Boolean to decide whether or not to include objects in the output
@param $includeEvents           - required. Boolean to decide whether or not to include events in the output
@param $decorAuthors            - required. zero to many /decor/project/author elements to get names from
@param $projectPrefix           - required. /decor/project/@prefix for this issue
@return issue with expanded metadata
@see isapi:getIssueMeta($authmap as map(*)?, $idOrIssue as item(), $includeSubscriptionInfo as xs:boolean, $includeObjects as xs:boolean, $includeEvents as xs:boolean) for more documentation
:)
declare function isapi:getIssueMeta($authmap as map(*)?, $idOrIssue as item(), $includeSubscriptionInfo as xs:boolean, $includeObjects as xs:boolean, $includeEvents as xs:boolean, $decorAuthors as element(author)*, $projectPrefix as xs:string) as element(issue) {
    let $issue                  := if ($idOrIssue instance of element(issue)) then $idOrIssue else isapi:getIssueById($idOrIssue, ())
    let $minEffTracking         := min($issue/tracking/xs:dateTime(@effectiveDate))
    let $maxEffTracking         := max($issue/tracking/xs:dateTime(@effectiveDate))
    let $maxEffAssignment       := max($issue/assignment/xs:dateTime(@effectiveDate))
    
    let $firstTracking          := ($issue/tracking[@effectiveDate = $minEffTracking])[1]
    let $lastAssignment         := ($issue/assignment[@effectiveDate = $maxEffAssignment])[1]
    let $lastTracking           := ($issue/tracking[@effectiveDate = $maxEffTracking])[1]
    
    let $maxEffAll              := max(($lastTracking|$lastAssignment)/xs:dateTime(@effectiveDate))
    let $lastEvent              := $lastTracking[@effectiveDate=$maxEffAll] | $lastAssignment[@effectiveDate=$maxEffAll]
    let $lastEvent              := $lastEvent[last()]
    
    let $lastEventAuthorId      := ($lastEvent/author/@id)[last()]
    let $lastEventAuthorName    := ($decorAuthors[@id = $lastEventAuthorId], $lastEvent/author)[1]
    let $lastEventAuthorName    := 
        if ($lastEventAuthorName) then
            $lastEventAuthorName
        else
            ($lastEvent/author)[last()]/text()
    
    let $currentType            := $issue/@type
    let $currentPriority        := if ($issue/@priority[not(.='')]) then ($issue/@priority) else ('N')
    let $currentStatus          := $lastTracking/@statusCode
    let $currentLabels          := $lastEvent/@labels[not(.='')]
    
    let $issueAuthorId          := ($firstTracking/author/@id)[1]
    let $issueAuthorUserName    := ($decorAuthors[@id = $issueAuthorId]/@username, $firstTracking/author)[1]
    let $issueAssignedId        := $lastAssignment/@to[not(. = '')]
    let $issueAssignedAuthor    := ($decorAuthors[@id = $issueAssignedId], $lastAssignment/@name)[1] 
    let $issueAssignedUserName  := $issueAssignedAuthor/@username
    let $issueAssignedName      := ($issueAssignedAuthor, $lastAssignment/@name)[1]
    
    return
        <issue  xmlns:json="http://www.json.org"
                id="{$issue/@id}" 
                displayName="{$issue/@displayName}" 
                priority="{$currentPriority}" 
                type="{$currentType}" 
                currentStatusCode="{$currentStatus}" 
                lastDate="{$lastEvent/@effectiveDate}" 
                lastAuthorId="{$lastEventAuthorId}"
                lastAuthor="{$lastEventAuthorName}" 
                iddisplay="{utillib:getNameForOID($issue/@id, (), $issue/ancestor::decor)}"
                ident="{$issue/ancestor::decor/project/@prefix}">
        {
            if (empty($currentLabels)) then () else (
                attribute currentLabels {$currentLabels}
            ),
            if (empty($issueAssignedId)) then () else (
                attribute lastAssignmentId {$issueAssignedId},
                attribute lastAssignment {$issueAssignedName}
            ),
            if ($includeSubscriptionInfo) then (
                attribute currentUserIsSubscribed {if (empty($authmap)) then false() else userapi:userHasIssueSubscription($authmap?name, $issue/@id)},
                attribute currentUserAutoSubscribes {if (empty($authmap)) then false() else userapi:userHasIssueAutoSubscription($authmap?name, $projectPrefix, $issue/@id, $issue/object/@type, $issueAuthorUserName[1], $issueAssignedUserName[1])}
            ) else ()
        }
        {
            for $n in $issue/object return utillib:addJsonArrayToElements($n)
            ,
            if ($includeEvents) then 
                for $n in $issue/tracking | $issue/assignment return utillib:addJsonArrayToElements($n)
            else ()
        }
        </issue>
};

(:~ Return zero or more expanded issues

@param $authmap                 - optional. relevant for tying subscription data to the issue for a given user. Expects user name to be under key "name" 
@param $idsOrIssues             - optional. Identifier of the issues or the issues themselves
@param $includeObjects          - required. Boolean to decide whether or not to include objects in the output
@param $expandObjects           - required. Boolean to decide whether or not objects, is $includeObjects = true(), should be resolved/expanded 
@param $includeEvents           - required. Boolean to decide whether or not to include events in the output 
@return issue element(s) with expanded meta data and optionally objects/events
@since 2014-07-09
:)
declare function isapi:getExpandedIssues($authmap as map(*)?, $idsOrIssues as item()*, $includeObjects as xs:boolean, $expandObjects as xs:boolean, $includeEvents as xs:boolean) as element(issue)* {
for $idOrIssue in $idsOrIssues
let $issue                  := if ($idOrIssue instance of element(issue)) then $idOrIssue else isapi:getIssueById($idOrIssue, ())
let $decor                  := $issue/ancestor::decor
let $projectPrefix          := $decor/project/@prefix
let $projectVersion         := $decor/@versionDate
let $language               := $decor/project/@defaultLanguage
let $meta                   := isapi:getIssueMeta($authmap, $issue, true(), false(), false())
let $objects                := 
    if ($includeObjects) then (
        for $o in $issue/object
        let $obid    := $o/@id
        let $obed    := $o/@effectiveDate[. castable as xs:dateTime]
        group by $obid, $obed
        return $o[1]
    ) else ()
return
    <issue>
    {
        $meta/@*
    }
    {
        if ($expandObjects) then (
            for $object in $objects
            let $objectId       := $object/@id
            let $objectEffDate  := $object/@effectiveDate[. castable as xs:dateTime]
            let $objectType     := $object/@type
            let $objectContent  := 
                (: optimization. if we let it search without being specific, then performance decreases drastically :)
                if ($objectType = 'VS') then (
                    (:get valueset:)
                    let $valueSet   := vs:getValueSetById($objectId, $objectEffDate, $projectPrefix, $projectVersion, true())/*/valueSet
                    return 
                        if ($valueSet[@id]) then (<valueSet>{$valueSet[@id][1]/(@* except (@url|@ident)), $valueSet[@id][1]/parent::*/(@url|@ident), $valueSet[@id][1]/node()}</valueSet>) else ($valueSet[1])
                ) else
                if ($objectType = 'DE') then (
                    (:get data element (dataset concept):)
                    utillib:getConcept($objectId, $objectEffDate, $projectVersion, $language)
                ) else 
                if ($objectType = 'TM') then (
                    (:get template:)
                    let $templates  := tmapi:getTemplateById($objectId, $objectEffDate, $projectPrefix, $projectVersion, $language)[@id][@effectiveDate]
                    return 
                        if ($templates) then (
                            <template>
                            {
                                $templates[1]/@*, 
                                if ($templates[1]/@url) then () else attribute url {$templates[1]/ancestor::cacheme/@bbrurl, $utillib:strDecorServicesURL},
                                if ($templates[1]/@ident) then () else attribute ident {$templates[1]/ancestor::decor/project/@prefix}
                            }
                            </template>
                        ) else ()
                ) else 
                if ($objectType = 'EL') then (
                    (:get template element:)
                    let $o  := $decor//element[@id = $objectId]
                    return $o[ancestor::template]
                ) else 
                if ($objectType = 'TR') then (
                    (:get transaction:)
                    utillib:getTransaction($objectId, $objectEffDate)
                ) else 
                if ($objectType = 'DS') then (
                    (:get dataset:)
                    utillib:getDataset($objectId, $objectEffDate)
                ) else 
                if ($objectType = 'SC') then (
                    (:get scenario:)
                    utillib:getScenario($objectId, $objectEffDate)
                ) else 
                if ($objectType = 'CS') then (
                    (:get codeSystem:)
                    cs:getCodeSystemById($objectId, $objectEffDate)//codeSystem
                ) else 
                if ($objectType = 'IS') then (
                    (:get issue:)
                    isapi:getIssueById($objectId, ())
                ) else 
                if ($objectType = 'QQ') then (
                    (:get issue:)
                    utillib:getQuestionnaire($objectId, $objectEffDate)
                ) else 
                if ($objectType = 'QR') then (
                    (:get issue:)
                    utillib:getQuestionnaireResponse($objectId, $objectEffDate)
                ) else 
                if (empty($objectEffDate)) then (
                    (:get any -- performance hit!:)
                    $decor//*[@id = $objectId][not(ancestor::history | self::object)]
                ) 
                else (
                    (:get any -- performance hit!:)
                    let $o  := $decor//*[@id = $objectId]
                    return $o[@effectiveDate = $objectEffDate][not(ancestor::history | self::object)]
                )
            let $objectContent := $objectContent[1]
            order by $objectType, $objectId, $objectEffDate
            return
                <object id="{$objectId}">
                {
                    if (empty($objectEffDate)) then () else attribute effectiveDate {$objectEffDate},
                    if (empty($objectType))    then () else attribute type {$objectType},
                    attribute iddisplay {utillib:getNameForOID($objectId, $language, $objectContent/ancestor::decor)}
                    ,
                    if (empty($objectContent)) then () else (
                        attribute ident {($objectContent/ancestor-or-self::*/@ident, $objectContent/ancestor::decor/project/@prefix)[1]},
                        $objectContent/ancestor-or-self::*[@ident][1]/@url
                    )
                    ,
                    attribute linkedartefactmissing { empty($objectContent) },
                    $objectContent/(@* except (@id|@type|@effectiveDate|@iddisplay|@url|@ident|@linkedartefactmissing|@name|@displayName|@statusCode))
                }
                {
                    if (empty($objectContent)) then () else
                    if ($objectType = 'DE') then (
                        let $datasetContent     := $objectContent/ancestor::dataset
                        let $originalConcept    := utillib:getOriginalForConcept($objectContent)
                        return (
                            attribute displayName {($originalConcept/name[@language = $language], $originalConcept/name, $object/@displayName, $object/@name)[1]},
                            $objectContent/@statusCode,
                            $originalConcept/name[.//text()[not(normalize-space(.) = '')]],
                            $originalConcept/desc[.//text()[not(normalize-space(.) = '')]]
                            ,
                            <dataset>
                            {
                                $datasetContent/@*,
                                attribute iddisplay {utillib:getNameForOID($datasetContent/@id, $language, $objectContent/ancestor::decor)},
                                $datasetContent/name[.//text()[not(normalize-space(.) = '')]],
                                <path>
                                {
                                    for $ancestor in $objectContent/ancestor::concept
                                    let $c  := utillib:getOriginalForConcept($ancestor)
                                    return
                                        concat(($c/name[@language = $language], $c/name)[1], ' / ')
                                }
                                </path>
                            }
                            </dataset>
                        )
                    ) else 
                    if ($objectType = 'TR') then (
                        let $datasetId  := $objectContent/representingTemplate/@sourceDataset[not(. = '')]
                        let $datasetEd  := $objectContent/representingTemplate/@sourceDatasetFlexibility[. castable as xs:dateTime]
                        let $datasets   := 
                            if ($datasetId) then 
                                utillib:getDataset($datasetId, $datasetEd, $projectVersion, $language)
                            else ()
                        let $templateId := $objectContent/representingTemplate/@ref[not(. = '')]
                        let $templateEd := $objectContent/representingTemplate/@flexibility[. castable as xs:dateTime]
                        let $templates  := 
                            if ($templateId) then 
                                tmapi:getTemplateById($templateId, $templateEd, $projectPrefix, $projectVersion, $language)[@id][@effectiveDate]
                            else ()
                        let $questionnaireId  := $objectContent/representingTemplate/@representingQuestionnaire[not(. = '')]
                        let $questionnaireEd  := $objectContent/representingTemplate/@representingQuestionnaireFlexibility[. castable as xs:dateTime]
                        let $questionnaires   := 
                            if ($questionnaireId) then 
                                utillib:getQuestionnaire($questionnaireId, $questionnaireId, $projectVersion, $language)
                            else ()
                        return (
                            attribute displayName {($objectContent/name[@language = $language], $objectContent/name)[1]},
                            ($objectContent/ancestor-or-self::*/@statusCode)[1],
                            $objectContent/name[.//text()[not(normalize-space(.) = '')]],
                            $objectContent/desc[.//text()[not(normalize-space(.) = '')]],
                            if ($objectContent/representingTemplate) then (
                                <representingTemplate>
                                {
                                    $objectContent/representingTemplate/@*,
                                    if ($templates) then (
                                        attribute {'templateName'} {($templates[1]/@displayName, $templates[1]/@name)[1]}
                                    ) else (),
                                    if ($datasets) then (
                                        attribute {'datasetName'} {($datasets/name[@language = $language], $datasets/name)[1]}
                                    ) else (),
                                    if ($questionnaires) then (
                                        attribute {'questionnaireName'} {($questionnaires/name[@language = $language], $questionnaires/name)[1]}
                                    ) else ()
                                }
                                </representingTemplate>
                            ) else ()
                        )
                     ) else (
                        attribute displayName {($objectContent/name[@language = $language], $objectContent/name, $objectContent/@displayName, $objectContent/@name)[1]},
                        attribute statusCode {$objectContent/@statusCode | $objectContent/tracking[@effectiveDate = max($objectContent/tracking/xs:dateTime(@effectiveDate))][1]/@statusCode},
                        if ($objectContent/name) then (
                            $objectContent/name[.//text()[not(normalize-space(.) = '')]]
                        ) else 
                        if ($objectContent[@name | @displayName]) then 
                            <name language="{$language}">{data(($objectContent/@displayName, $objectContent/@name)[1])}</name> 
                        else (),
                        $objectContent/desc[.//text()[not(normalize-space(.) = '')]]
                    )
                }
                </object>
        )
        else (
            $objects
        )
    }
    {
        if ($includeEvents) then
            for $n in $issue/tracking | $issue/assignment order by $n/@effectiveDate return $n
        else ()
    }
    </issue>

};

(:~ Return zero or more expanded issues based on their reference to a given object id/effectiveDate

@param $objectId                - required. Identifier of the object to retrieve the issue for
@param $objectEffectiveDate     - optional. Effective date of the object to retrieve the issue for
@param $includeObjects          - required. Boolean to decide whether or not to include objects in the output
@param $expandObjects           - required. Boolean to decide whether or not objects, is $includeObjects = true(), should be resolved/expanded 
@param $includeEvents           - required. Boolean to decide whether or not to include events in the output
@return Matching issues
@since 2014-07-09
:)
declare function isapi:getExpandedIssuesByObject ($authmap as map(*), $objectId as xs:string, $objectEffectiveDate as xs:string?, $includeObjects as xs:boolean, $expandObjects as xs:boolean, $includeEvents as xs:boolean) as element(issue) {
    isapi:getExpandedIssues($authmap, isapi:getIssueByObject($objectId, $objectEffectiveDate), $includeObjects, $expandObjects, $includeEvents)
};
