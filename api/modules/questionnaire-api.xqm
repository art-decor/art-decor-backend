xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:~ Questionnaire API allows read, create, update on DECOR links into HL7 FHIR Questionnaire :)
module namespace qapi              = "http://art-decor.org/ns/api/questionnaire";

import module namespace roaster     = "http://e-editiones.org/roaster";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

import module namespace utillib     = "http://art-decor.org/ns/api/util" at "/db/apps/api/modules/library/util-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "/db/apps/api/modules/library/settings-lib.xqm";
import module namespace decorlib    = "http://art-decor.org/ns/api/decor" at "library/decor-lib.xqm";
import module namespace histlib     = "http://art-decor.org/ns/api/history" at "library/history-lib.xqm";
import module namespace jx          = "http://joewiz.org/ns/xquery/json-xml" at "library/json-xml.xqm";
import module namespace serverapi   = "http://art-decor.org/ns/art-decor-server" at "server-api.xqm";
import module namespace ggapi       = "http://art-decor.org/ns/api/governancegroups" at "governancegroups-api.xqm";
import module namespace router      = "http://e-editiones.org/roaster/router";

declare namespace json      = "http://www.json.org";
declare namespace f         = "http://hl7.org/fhir";

(:~ All functions support their own override, but this is the fallback for the maximum number of results returned on a search :)
declare %private variable $qapi:maxResults              := 50;
declare %private variable $qapi:ITEM-STATUS             := utillib:getDecorTypes()/ItemStatusCodeLifeCycle;
declare %private variable $qapi:STATUSCODES-FINAL       := ('final', 'pending', 'rejected', 'cancelled', 'deprecated');
declare %private variable $qapi:RELATIONSHIP-TYPES      := utillib:getDecorTypes()/QuestionnaireRelationshipTypes;
declare %private variable $qapi:FHIROBJECT-FORMAT       := utillib:getDecorTypes()/FhirObjectFormat;
declare %private variable $qapi:FHIRRESOURCE-TYPE       := utillib:getDecorTypes()/FHIRResourceType;
declare %private variable $qapi:ADDRLINE-TYPE           := utillib:getDecorTypes()/AddressLineType;

(:~ Returns a list of zero or more questionnaires
    
@param $projectPrefix           - required. limits scope to this project only
@param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
@param $projectLanguage         - optional parameter to select from a specific compiled language
@param $objectId                - optional. Filter output to just those based on this questionnaire/relationship/@ref. For each questionnaire
@param $objectEffectiveDate     - optional. Filter output to just those based on this questionnaire/relationship/@flexibility. relevant when objectId has a value.
@param $sort                    - optional parameter to indicate sorting. Only option 'name'
@param $sortorder               - optional parameter to indicate sort order. Only option 'descending'
@return all live repository/non-private questionnaire as JSON
@since 2020-05-03
:)
declare function qapi:getQuestionnaireList($request as map(*)) {
    let $governanceGroupId      := $request?parameters?governanceGroupId[not(. = '')]
    let $projectPrefix          := $request?parameters?prefix[not(. = '')]
    let $projectVersion         := $request?parameters?release[not(. = '')]
    let $projectLanguage        := $request?parameters?language[not(. = '')]
    let $objectId               := $request?parameters?objectId[not(. = '')]
    let $objectEffectiveDate    := $request?parameters?objectEffectiveDate[not(. = '')]
    let $sort                   := $request?parameters?sort[not(. = '')]
    let $sortorder              := $request?parameters?sortorder[not(. = '')]
    let $max                    := $request?parameters?max[not(. = '')]
    let $resolve                := if (empty($governanceGroupId)) then not($request?parameters?resolve = false()) else $request?parameters?resolve = true()
    
    let $check                  :=
        if (empty($governanceGroupId) and empty($projectPrefix)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have either parameter governanceGroupId or prefix')
        else 
        if (not(empty($governanceGroupId)) and not(empty($projectPrefix))) then 
            error($errors:BAD_REQUEST, 'Request SHALL have parameter governanceGroupId or prefix, not both')
        else ()
        
    let $startT                 := util:system-time()
    
    let $result                 :=
        if (empty($governanceGroupId)) then
            qapi:getQuestionnaireList($projectPrefix, $projectVersion, $projectLanguage, $objectId, $objectEffectiveDate, $sort, $sortorder, $max)
        else (
            for $projectId in ggapi:getLinkedProjects($governanceGroupId)/@ref
            return
                qapi:getQuestionnaireList($projectId, $projectVersion, $projectLanguage, $objectId, $objectEffectiveDate, $sort, $sortorder, $max)     
        )
        
    let $durationT              := (util:system-time() - $startT) div xs:dayTimeDuration("PT0.001S")
    
    return
        <list artifact="{$result[1]/@artifact}" elapsed="{$durationT}" current="{sum($result/xs:integer(@current))}" total="{sum($result/xs:integer(@total))}" all="{sum($result/xs:integer(@allcnt))}" resolve="{$resolve}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            if (empty($governanceGroupId)) then attribute project {$projectPrefix} else attribute governanceGroupId {$governanceGroupId},
            utillib:addJsonArrayToElements($result/*)
        }
        </list>
};

declare function qapi:getQuestionnaireList($projectPrefix as xs:string, $projectVersion as xs:string?, $projectLanguage as xs:string?, $objectId as xs:string?, $objectEffectiveDate as xs:string?, $sort as xs:string?, $sortorder as xs:string?, $max as xs:string?) {
    let $sort                       := $sort[string-length() gt 0]
    let $sortorder                  := $sortorder[. = 'descending']
    
    let $projectPrefix              := $projectPrefix[not(. = '')]
    let $projectVersion             := $projectVersion[not(. = '')]
    let $decor                      := if (empty($projectPrefix)) then () else utillib:getDecor($projectPrefix, $projectVersion, $projectLanguage)
    
    let $allcnt                     := count($decor//questionnaire)
    
    let $objectId                   := $objectId[not(. = '')]
    let $objectEffectiveDate        := $objectEffectiveDate[not(. = '')]
    
    let $allQuestionnaireResponses  := collection(util:collection-name($decor))//f:QuestionnaireResponse
    let $filteredQuestionnaires     := 
        if (empty($objectId)) then $decor//questionnaire else (
            if (empty($objectEffectiveDate)) then (
                let $object                     :=
                    if (empty($objectId)) then () else (
                        utillib:getScenario($objectId, $objectEffectiveDate, $projectVersion, $projectLanguage) |
                        utillib:getTransaction($objectId, $objectEffectiveDate, $projectVersion, $projectLanguage) |
                        utillib:getDataset($objectId, $objectEffectiveDate, $projectVersion, $projectLanguage)
                    )
                (: get questionnaires related to object :)
                return
                    if ($object) then (
                        $decor//questionnaire[relationship[@ref = $object/@id][@flexibility = $object/@effectiveDate]]
                    )
                    else ( 
                        $decor//questionnaire[relationship[@ref = $objectId]]
                    )
            )
            else (
                (: get questionnaires and questionnaireresponses related to object :)
                $decor//questionnaire[relationship[@ref = $objectId][@flexibility = $objectEffectiveDate]]
            )
        )
    
    let $projectLanguage      := 
        if (empty($projectLanguage)) 
        then 
            if (empty($projectPrefix)) then
                $filteredQuestionnaires/ancestor::decor/project/@defaultLanguage
            else
                $decor/project/@defaultLanguage
        else $projectLanguage
    
    let $filteredQuestionnaires         := $filteredQuestionnaires[self::questionnaire]
    
    (:can sort on indexed db content -- faster:)
    let $results                :=
        switch ($sort) 
        case 'effectiveDate'    return 
            if (empty($sortorder)) then 
                for $q in $filteredQuestionnaires order by $q/@effectiveDate return $q
            else
                for $q in $filteredQuestionnaires order by $q/@effectiveDate descending return $q
        case 'name'    return 
            if (empty($sortorder)) then 
                for $q in $filteredQuestionnaires order by lower-case($q/@name) return $q
            else
                for $q in $filteredQuestionnaires order by lower-case($q/@name) descending return $q
        default                 return $filteredQuestionnaires
    
    let $count              := count($results)
    let $max                := if ($max ge 1) then $max else $qapi:maxResults
    
    let $forOutput          := true()
    (: build map of oid names. is more costly when you do that deep down :)
    let $oidnamemap         := qapi:buildOidNameMap($results, $projectLanguage[1])
    let $questionnaires     :=
        for $q in $results
        return
            qapi:handleQuestionnaire($q, $projectPrefix, $projectLanguage[1], $oidnamemap, $allQuestionnaireResponses, true(), false(), $forOutput)
    let $unmatchedqr        :=
        if (empty($objectId)) then 
            for $qr in $allQuestionnaireResponses[not(f:questionnaire/@value = $questionnaires/@canonicalUri)]
            return
                qapi:handleQuestionnaire($qr, $projectPrefix, $projectLanguage[1], $oidnamemap, (), true(), false(), $forOutput)
        else ()
    let $pendingRequests    := ()
        (:for $pendingRequest in collection($setlib:strDecorScheduledTasks)/questionnaire-transform-request
        order by $pendingRequest/@on
        return
            $pendingRequest:)
        
    
    return
    <list artifact="QQQR" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
    {
        utillib:addJsonArrayToElements( ($questionnaires, (), ()) )
    }
    </list>
};

(:~  Return questionnaire based on id and optionally effectiveDate. If effectiveDate is empty, the latest version is returned
:
:   @param $q-id   - required questionnaire/@id
:   @param $q-ed   - optional questionnaire/@effectiveDate
:   @return exactly 1 questionnaire or nothing if not found
:   @since 2015-04-27
:)
declare function qapi:getLatestQuestionnaire($request as map(*)) {
    qapi:getQuestionnaire($request)
};

declare function qapi:getQuestionnaire($request as map(*)) {

    let $q-id                           := $request?parameters?id
    let $q-ed                           := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    
    let $projectPrefix                := ()
    let $decorVersion                 := $request?parameters?release
    let $language                     := $request?parameters?language

    let $qs                           := utillib:getQuestionnaire($q-id, $q-ed, $projectPrefix, $decorVersion, $language)
    let $acceptTypes                  := router:accepted-content-types()
    let $acceptedType                 := ($acceptTypes[. = ('application/xml', 'application/json')], 'application/json')[1]

    let $forOutput                    := true()
    let $oidnamemap                   := qapi:buildOidNameMap($qs, $language[1])
    let $results                      := 
        for $q in $qs
        return
            qapi:handleQuestionnaire($q, $projectPrefix, $language, $oidnamemap, collection(util:collection-name($q))//f:QuestionnaireResponse, false(), false(), $forOutput)
    
    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple questionnaires for id '", $q-id, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        )
        else (
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*, tokenize($acceptedType, '/')[2])
                }
        )
};

(:~ Retrieves DECOR questionnaire history based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id
    @param $effectiveDate           - optional parameter denoting the effectiveDate. If not given assumes latest version for id
    @return list
    @since 2022-08-16
:)
declare function qapi:getQuestionnaireHistory($request as map(*)) {
    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $projectPrefix                  := ()
    let $results                        := histlib:ListHistory($authmap, $decorlib:OBJECTTYPE-QUESTIONNAIRE, (), $id, $effectiveDate, 0)
    
    return
        <list artifact="{$decorlib:OBJECTTYPE-QUESTIONNAIRE}" current="{count($results)}" total="{count($results)}" all="{count($results)}" lastModifiedDate="{current-dateTime()}">
        {
            utillib:addJsonArrayToElements($results)
        }
        </list>
};

(:declare function qapi:getQuestionnaire($q-id as xs:string, $q-ed as xs:string?) as element()* {
    qapi:getQuestionnaire($q-id, $q-ed,(),())
};:)

(:~  Return the live questionnaire if param decorVersion is not a dateTime OR 
:   compiled, archived/released DECOR questionnaire based on @id|@effectiveDate and decor/@versionDate.
:   Released/compiled matches could be present in multiple languages. Use the parameters @language to select the right one.
:
:   @param $q-id           - required questionnaire/@id
:   @param $q-ed           - optional questionnaire/@effectiveDate
:   @param $decorVersion    - optional decor/@versionDate
:   @param $language        - optional decor/@language. Only relevant with parameter $decorVersion. Empty uses decor/project/@defaultLanguage, '*' selects all versions, e.g. 'en-US' selects the US English version (if available)
:   @return exactly 1 live questionnaire, or 1 or more archived DECOR version instances or nothing if not found
:   @since 2015-04-29
:)
(:declare function qapi:getQuestionnaire($q-id as xs:string, $q-ed as xs:string?, $decorVersion as xs:string?) as element()* {
    qapi:getQuestionnaire($q-id, $q-ed,$decorVersion,())
};
:)
(:declare function qapi:getQuestionnaire($q-id as xs:string, $q-ed as xs:string?, $decorVersion as xs:string?, $language as xs:string?) as element()* {
    let $decor          :=
        if ($decorVersion[string-length()>0]) then
            let $d      := $setlib:colDecorVersion/decor[@versionDate=$decorVersion]
            return
            if (empty($language)) then
                $d[@language=$d/project/@defaultLanguage]
            else if ($language='*') then
                $d
            else
                $d[@language=$language]
        else (
            $setlib:colDecorData/decor
        )
    let $quest          := $decor//questionnaire[@id = $q-id][ancestor::scenarios] | $decor//questionnaireresponse[@id = $q-id][ancestor::scenarios]
    
    return
        if ($q-ed castable as xs:dateTime) then 
            $quest[@effectiveDate = $q-ed] 
        else 
        if ($quest[2]) then 
            $quest[@effectiveDate = max($quest/xs:dateTime(@effectiveDate))]
        else (
            $quest
        )
};
:)
declare function qapi:patchQuestionnaire($request as map(*)) {

    let $authmap                       := $request?user
    let $qid                           := $request?parameters?id
    let $qed                           := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $projectPrefix                  := ()
    let $projectVersion                 := ()
    let $projectLanguage                := ()
    let $data                           := utillib:getBodyAsXml($request?body, 'parameters', ())
    
    let $check                  :=
        if ($data) then () else (
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        )
    
    let $return                         := qapi:patchQuestionnaire($authmap, true(), string($qid), $qed, $data)
   
    return 
      
        $return

};

declare function qapi:patchQuestionnaire($authmap as map(*), $doQuestionnaire as xs:boolean, $id as xs:string, $effectiveDate as xs:string, $data as element(parameters)) {

    let $storedQuestionnaire    := utillib:getQuestionnaire($id, $effectiveDate)
    
    let $decor                  := $storedQuestionnaire/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix
    let $projectLanguage        := $decor/project/@defaultLanguage

    let $check                  :=
        if (empty($storedQuestionnaire)) then (
            error($errors:BAD_REQUEST, 'Questionnaire with id ' || $id || ' and effectiveDate ' || $effectiveDate || ' does not exist')
        )
        else
        if (count($storedQuestionnaire) gt 1) then (
            error($errors:SERVER_ERROR, 'Found multiple results for id ' || $id || ' effectiveDate ' || $effectiveDate || '. Expected 1..1. Alert your administrator as this should not be possible.')
        )
        else ()
    
    let $lock                   := decorlib:getLocks($authmap, $id, $effectiveDate, ())
    let $lock                   := if ($lock) then $lock else decorlib:setLock($authmap, $id, $effectiveDate, false())
    
    let $check                  :=
        if ($storedQuestionnaire[@statusCode = $qapi:STATUSCODES-FINAL]) then
            if ($data/parameter[@path = '/statusCode'][@op = 'replace'][not(@value = $qapi:STATUSCODES-FINAL)]) then () else 
            if ($data[count(parameter) = 1]/parameter[@path = '/statusCode']) then () else (
                error($errors:BAD_REQUEST, concat('The ', name($storedQuestionnaire), ' cannot be patched while it has one of status: ', string-join($qapi:STATUSCODES-FINAL, ', '), if ($storedQuestionnaire/@statusCode = 'pending') then 'You should switch back to status draft to edit.' else ()))
            )
        else ()
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-RULES)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify questionnaires in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if ($lock) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have a lock for this ' || name($storedQuestionnaire) || ' (anymore). Get a lock first.'))
        )
    let $check                  :=
        if ($data[parameter]) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall be an array of ''parameter''.')
        )
    let $unsupportedops         := $data/parameter[not(@op = ('add', 'replace', 'remove'))]
    let $check                  :=
        if ($unsupportedops) then
            error($errors:BAD_REQUEST, 'Submitted parameters shall have supported ''op'' value. Found ''' || string-join(distinct-values($unsupportedops/@op), ''', ''') || ''', expected ''add'', ''replace'', or ''remove''') 
        else ()
    let $checkn                 := 
        if ($data/parameter[@path = '/name']) then
            if (count($storedQuestionnaire/name) - count($data/parameter[@op = 'remove'][@path = '/name']) + count($data/parameter[@op = ('add', 'replace')][@path = '/name']) ge 1) then () else (
                'A ' || name($storedQuestionnaire) || ' SHALL have at least one name. You cannot remove every name.'
            )
        else ()
    
    let $check                  :=
        for $param in $data/parameter
        let $op         := $param/@op
        let $path       := $param/@path
        let $value      := $param/@value
        let $pathpart   := substring-after($path, '/')
        return
            switch ($path)
            case '/statusCode' return (
                if ($op = 'remove') then
                    'Parameter path ''' || $path || ''' does not support ' || $op
                else 
                if (utillib:isStatusChangeAllowable($storedQuestionnaire, $value)) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Supported are: ' || string-join(map:get($utillib:itemstatusmap, string($storedQuestionnaire/@statusCode)), ', ')
                )
            )
            case '/expirationDate'
            case '/officialReleaseDate' return (
                if ($op = 'remove') then () else 
                if ($value castable as xs:dateTime) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL be yyyy-mm-ddThh:mm:ss'
                )
            )
            case '/canonicalUri' return (
                if ($op = 'remove') then () else 
                if ($value castable as xs:anyURI) then (
                    let $otherQuestionnnaires := $setlib:colDecorData//questionnaire[@canonicalUri = $value]
                    return 
                    if ($otherQuestionnnaires except $storedQuestionnaire) then
                        'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL be unique on the server, otherwise you cannot reference from a QuestionnaireReponse. Found in ' || string-join(distinct-values($otherQuestionnnaires/ancestor::decor/project/@prefix), ', ')
                    else ()
                ) else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL be a URI'
                )
            )
            case '/versionLabel' return (
                if ($op = 'remove') then () else 
                if ($value[not(. = '')]) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL NOT be empty.'
                )
            )
            case '/fhirVersion'
            return (
                if ($param[@value = $qapi:FHIROBJECT-FORMAT/enumeration/@value]) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || '''. fhirVersion has unsupported value ' || string-join($param/@value, ', ') || '. SHALL be one of: ' || string-join($qapi:FHIROBJECT-FORMAT/enumeration/@value, ', ')
                )
            )
            case '/enhanced'
            return (
                if ($op = 'remove') then () else 
                if ($value[. = ('true', 'false')]) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL be true or false'
                )
            )
            case '/name'
            case '/desc'
            case '/copyright'
            return (
                if ($param[count(value/*[name() = $pathpart]) = 1]) then
                    if ($param/value/*[name() = $pathpart][matches(@language, '[a-z]{2}-[A-Z]{2}')]) then ( 
                        if ($param/value/*[name() = $pathpart][@inherited]) then 
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Contents SHALL NOT be marked ''inherited''.'
                        else (),
                        if ($param[@op = 'remove'] | $param/value/*[name() = $pathpart][.//text()[not(normalize-space() = '')]]) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A ' || $pathpart || ' SHALL have contents.'
                        )
                    )
                    else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A ' || $pathpart || ' SHALL have a language with pattern ll-CC'
                    )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/*[name() = $pathpart]) 
                )
            )
            case '/classification' return (
                if ($param[count(value/classification) = 1]) then 
                    if ($op = 'remove') then () else (
                        if ($param/value/classification/tag[not(string(string-join(., '')) = '')]) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Classification SHALL have at least one non-empty tag'
                        )
                    )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/*[name() = $pathpart]) 
                )
            )
            case '/relationship' return (
                if ($param[count(value/relationship) = 1]) then 
                    if ($op = 'remove') then () else (
                        if ($param/value/relationship[@type = $qapi:RELATIONSHIP-TYPES/enumeration/@value]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Relationship has unsupported .type value ' || string-join($param/value/relationship/@type, ', ') || '. SHALL be one of: ' || string-join($qapi:RELATIONSHIP-TYPES/enumeration/@value, ', ')
                        ),
                        if ($param/value/relationship[@ref]) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Relationship SHALL have ref property'
                        ),
                       
                        if ($param/value/relationship[@type = 'ANSW'] and name($storedQuestionnaire) = 'questionnaire') then
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Relationship type ' || ($param/value/relationship/@type)[1] || ' is not compatible with a ' || name($storedQuestionnaire)
                        else ()
                   )
                else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || '''. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/*[name() = $pathpart]) 
                )
            )
            case '/experimental' return (
                if ($op = 'remove') then () else 
                if (not($doQuestionnaire)) then
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported on ' || name($storedQuestionnaire)
                else
                if ($value[. = ('true', 'false')]) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL be true or false'
                )
            )
            case '/subjectType' return (
                if (not($doQuestionnaire)) then
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported on ' || name($storedQuestionnaire)
                else
                if ($param[count(value/subjectType) = 1]) then
                    if ($param[value/subjectType/@code = $qapi:FHIRRESOURCE-TYPE/enumeration/@value]) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || '''. ' || $pathpart || ' has unsupported value ' || string-join($param/@value, ', ') || '. SHALL be one of: ' || string-join($qapi:FHIRRESOURCE-TYPE/enumeration/@value, ', ')
                    )
                else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || '''. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/*[name() = $pathpart]) 
                )
            )
            case '/endorsingAuthority'
            case '/publishingAuthority' return (
                if (not($doQuestionnaire)) then
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported on ' || name($storedQuestionnaire)
                else
                if ($param[count(value/*[name() = $pathpart]) = 1]) then (
                    if ($param/value/*[name() = $pathpart][@inherited]) then 
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Contents SHALL NOT be marked ''inherited''.'
                    else (),
                    if ($param/value/*[name() = $pathpart][@id]) then
                        if ($param/value/*[name() = $pathpart][utillib:isOid(@id)]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || ''' ' || $pathpart || '.id SHALL be an OID is present. Found ' || string-join($param/value/*[name() = $pathpart]/@id, ', ')
                        )
                    else (),
                    if ($param/value/*[name() = $pathpart][@name[not(. = '')]]) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || ''' ' || $pathpart || '.name SHALL be a non empty string.'
                    ),
                    let $unsupportedtypes := $param/value/*[name() = $pathpart]/addrLine/@type[not(. = $qapi:ADDRLINE-TYPE/enumeration/@value)]
                    return
                        if ($unsupportedtypes) then
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. ' || $pathpart || ' has unsupported addrLine.type value(s) ' || string-join($unsupportedops, ', ') || '. SHALL be one of: ' || string-join($qapi:ADDRLINE-TYPE/enumeration/@value, ', ')
                        else ()
                ) else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || '''. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/*[name() = $pathpart])
                )
            )
            case '/code'
            case '/jurisdiction'
            return (
                if (not($doQuestionnaire)) then
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported on ' || name($storedQuestionnaire)
                else
                if ($param[count(value/*[name() = $pathpart]) = 1]) then
                    if ($op = 'remove') then () else (
                        if ($param/value/*[name() = $pathpart][@code[matches(., '^\S+$')]][utillib:isOid(@codeSystem)][empty(@canonicalUri) or @canonicalUri castable as xs:anyURI]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Input SHALL have code without whitespace, a codeSystem as valid OID, and optionally a canonicalUri as URI. Found: ' || string-join(for $att in $param/value/code/@* return name($att) || ': "' || $att || '" ', ' ')
                        )
                    )
                else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || '''. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/*[name() = $pathpart])
                )
            )
            case '/item'
            return (
                if (not($doQuestionnaire)) then
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported on ' || name($storedQuestionnaire)
                else
                if ($param[count(value/item) ge 1]) then (
                    let $data     := qapi:handleQuestionnaireItem($param/value/item, (), $storedQuestionnaire/item)
                    let $data     := 
                        element {name($storedQuestionnaire)} {
                            $storedQuestionnaire/@*,
                            $storedQuestionnaire/(node() except item),
                            $data
                        }
                    let $r        := validation:jaxv-report($data, $setlib:docDecorSchema)
                    return
                    if ($r/status='invalid') then
                        error($errors:BAD_REQUEST, 'Questionnaire contents are not schema compliant: ' || serialize($r))
                        (:error($errors:BAD_REQUEST, 'Questionnaire contents are not schema compliant: ' || serialize($param/value/item) || ' ----- ' || serialize($data) || ' ----- ' || serialize($r)):)
                    else ()
                )
                else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || '''. Input SHALL have one or more ' || $pathpart || ' under value containing exactly one object. Found ' || count($param/value/*[name() = $pathpart]/*)
                )
            )
            (:case '/contained'
            return (
                if (not($doQuestionnaire)) then
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported on ' || name($storedQuestionnaire)
                else
                if ($param[count(value/*[name() = $pathpart]/*) = 1]) then (
                    (\: object shall have id that is referred to :\)
                    let $reference    := '#' || value/*[name() = $pathpart]/*/id/@value
                    let $referrers    := $storedQuestionnaire/item//@ref | $data/parameter[not(@op = 'remove')][@path = '/item']/value/item//@ref
                    return
                    if ($op = 'remove') then
                        if ($referrers[. = $ref]) then
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Contained object SHALL NOT still be referred to in the questionnaire at removal time. Found references ' || $reference
                        else ()
                    else
                    if ($referrers[. = $ref]) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || '''. Contained object SHALL have a reference to it somewhere in the questionnaire. Expected reference ' || $reference
                    )
                )
                else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || '''. Input SHALL have exactly one ' || $pathpart || ' under value containing exactly one object. Found ' || count($param/value/*[name() = $pathpart]/*) 
                )
            ):)
            default return (
                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Path value not supported.'
            )
     let $check                 :=
        if (empty($checkn) and empty($check)) then () else (
            error($errors:BAD_REQUEST,  string-join (($checkn, $check), '. '))
        )
    
    let $intention              := if ($storedQuestionnaire[@statusCode = 'final']) then 'patch' else 'version'
    let $objectType             := $decorlib:OBJECTTYPE-QUESTIONNAIRE
    let $update                 := histlib:AddHistory($authmap?name, $objectType, $projectPrefix, $intention, $storedQuestionnaire)
    
    let $update                 :=
        for $param in $data/parameter
        return
            switch ($param/@path)
            case '/statusCode'
            case '/expirationDate'
            case '/officialReleaseDate'
            case '/versionLabel'
            case '/experimental'
            case '/fhirVersion'
            case '/enhanced' return (
                let $attname  := substring-after($param/@path, '/')
                let $new      := attribute {$attname} {$param/@value}
                let $stored   := $storedQuestionnaire/@*[name() = $attname]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedQuestionnaire
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/canonicalUri' return (
                let $attname    := substring-after($param/@path, '/')
                let $new        := attribute {$attname} {$param/@value}
                let $stored     := $storedQuestionnaire/@canonicalUri
                let $storedUri  := if ($stored) then string($stored) else utillib:getCanonicalUri('Questionnaire', $storedQuestionnaire)
                let $storedqr   := $setlib:colDecorData//f:QuestionnaireResponse/f:questionnaire/@value[. = $storedUri]
                
                let $updateQuestionnaire          :=
                    switch ($param/@op)
                    case ('add') (: fall through :)
                    case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedQuestionnaire
                    case ('remove') return update delete $stored
                    default return ( (: unknown op :) )
                let $updateQuestionnaireResponses :=
                    if ($storedqr) then update value $storedqr with $new else ()
                
                return
                    ()
            )
            case '/name'
            case '/copyright'
            case '/desc'
            return (
                (: only one per language :)
                let $elmname  := substring-after($param/@path, '/')
                let $new      := utillib:prepareFreeFormMarkupWithLanguageForUpdate($param/value/*[name() = $elmname])
                let $stored   := $storedQuestionnaire/*[name() = $elmname][@language = $param/value/*[name() = $elmname]/@language]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedQuestionnaire
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/classification' 
            return (
                (: only one :)
                let $elmname  := substring-after($param/@path, '/')
                let $new      := 
                    if ($param/value/*[name() = $elmname]) then
                        <classification>
                        {
                            $param/value/*[name() = $elmname]/tag[not(string(string-join(., '')) = '')]
                        }
                        </classification>
                    else ()
                let $stored   := $storedQuestionnaire/*[name() = $elmname]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedQuestionnaire
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/relationship' return (
                (: only one per template or model, insert before others :)
                let $new      := utillib:prepareDatasetRelationshipForUpdate($param/value/relationship)
                let $stored   := 
                    if ($new[@type = 'ANSW'] | $new[@type = 'DRIV']) then 
                        $storedQuestionnaire/relationship[@type = $new/@type]
                    else (
                        $storedQuestionnaire/relationship[@ref = $new/@ref]
                    )
                
                return
                switch ($param/@op)
                case 'add' (: fall through :)
                case 'replace' return if ($stored) then (update insert $new preceding $stored[1], update delete $stored) else (update insert $new into $storedQuestionnaire)
                case 'remove' return update delete $stored
                default return ( (: unknown op :) )
            )
            (: datatype code :)
            case '/subjectType' return (
                let $new      := <subjectType code="{$param/value/subjectType/@code}"/>
                let $stored   := $storedQuestionnaire/subjectType[@code = $new/@code]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedQuestionnaire
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/endorsingAuthority' return (
                (: only one possible :)
                let $new      := utillib:prepareEndorsingAuthorityForUpdate($param/value/endorsingAuthority)
                let $stored   := $storedQuestionnaire/endorsingAuthority
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedQuestionnaire
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/publishingAuthority' return (
                (: only one possible :)
                let $new      := utillib:preparePublishingAuthorityForUpdate($param/value/publishingAuthority)
                let $stored   := $storedQuestionnaire/publishingAuthority
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedQuestionnaire
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            (: datatype Coding :)
            case '/code'
            case '/jurisdiction'
            return (
                let $attname  := substring-after($param/@path, '/')
                let $new      := utillib:prepareValueCodingTypeForUpdate($param/value/*[name() = $attname])
                let $stored   := $storedQuestionnaire/*[name() = $attname][@code = $new/@code][@codeSystem = $new/@codeSystem]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedQuestionnaire
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/item'
            return (
                let $new      := qapi:handleQuestionnaireItem($param/value/item, (), $storedQuestionnaire/item)
                let $stored   := update delete $storedQuestionnaire/item
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedQuestionnaire
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            (:case '/contained'
            return (
                let $attname  := substring-after($param/@path, '/')
                let $new      := $param/value/*[name() = $attname]
                (\: Assumption: should be unique regardless of resource type
                    https://chat.fhir.org/#narrow/stream/179166-implementers/topic/Contained.20Resource.2Eid.3A.20unique.20or.20unique.20per.20type.3F
                :\)
                let $objid    := $new/id/@value
                let $stored   := $storedQuestionnaire/*[name() = $attname]/*/id[@value = $objid]
                
                return
                switch ($param/@op)
                case ('add') (\: fall through :\)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedQuestionnaire
                case ('remove') return update delete $stored
                default return ( (\: unknown op :\) )
            ):)
            default return ( (: unknown path :) )
    
    let $update                 :=
        if ($storedQuestionnaire[@lastModifiedDate]) then 
            update value $storedQuestionnaire/@lastModifiedDate with substring(string(current-dateTime()), 1, 19)
        else (
            update insert attribute lastModifiedDate {substring(string(current-dateTime()), 1, 19)} into $storedQuestionnaire
        )
    
    (: after all the updates, the XML Schema order is likely off. Restore order - TODO: externalize into separate function based on FHIR version :)
    
    (: build map of oid names. is more costly when you do that deep down :)
    let $forOutput              := false()
    let $oidnamemap             := map {}
    let $return                 := qapi:handleQuestionnaire($storedQuestionnaire, $projectPrefix, $projectLanguage, $oidnamemap, (), false(), false(), $forOutput)
    
    let $update                 := update replace $storedQuestionnaire with $return
    let $update                 := update value $storedQuestionnaire/@lastModifiedDate with substring(string(current-dateTime()), 1, 19)
    let $update                 := update delete $storedQuestionnaire/@uuid
    
    let $update                 := update delete $lock
    
    let $forOutput              := true()
    let $oidnamemap             := qapi:buildOidNameMap($storedQuestionnaire, $projectLanguage[1])
    let $return                 := qapi:handleQuestionnaire($storedQuestionnaire, $projectPrefix, $projectLanguage, $oidnamemap, (), false(), false(), $forOutput)
    
    return
        element {name($return)} {
            $return/@*,
            attribute keys {string-join(for $k in map:keys($oidnamemap) return $k || '=' || map:get($oidnamemap, $k), ' ')},
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($return/*)
        }
};

(:~ Build a map that has oid as key and human readable version as value :)
declare %private function qapi:buildOidNameMap($questionnaires as element()*, $language as xs:string?) as map(*)? {
    map:merge(
        for $oid in $questionnaires/@id | $questionnaires//@codeSystem[utillib:isOid(.)][empty(../@codeSystemName)]
        let $grp := $oid
        group by $grp
        return
            map:entry($oid[1], utillib:getNameForOID($oid[1], $language, $oid[1]/ancestor::decor))
    )
};

(: Create a questionnaire, either empty or with input data or based on another questionnaire

@param $projectPrefix project to create this questionnaire in
@param $targetDate If true invokes effectiveDate of the new questionnaire as [DATE]T00:00:00. If false invokes date + time [DATE]T[TIME] 
@param $sourceId parameter denoting the id of a questionnaire to use as a basis for creating the new questionnaire
@param $sourceEffectiveDate parameter denoting the effectiveDate of a questionnaire to use as a basis for creating the new questionnaire,
@param $keepIds Only relevant if source questionnaire is specified. If true, the new questionnaire will keep the same ids for the new questionnaire, and only update the effectiveDate
@param $baseId Only relevant when a source questionnaire is specified and `keepIds` is false. This overrides the default base id for questionnaire in the project. The value SHALL match one of the projects base ids for questionnaire
@return questionnaire as xml with json:array set on elements
:)
declare function qapi:postQuestionnaire($request as map(*)) {

    let $authmap                := $request?user
    let $projectPrefix          := $request?parameters?project[not(. = '')]
    let $data                   := utillib:getBodyAsXml($request?body, 'questionnaire', ())
    let $targetDate             := $request?parameters?targetDate = true()
    let $sourceId               := $request?parameters?sourceId
    let $sourceEffectiveDate    := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?sourceEffectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?sourceEffectiveDate[string-length() gt 0]
        }
    let $keepIds                := $request?parameters?keepIds = true()
    let $baseId                 := $request?parameters?baseId
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($projectPrefix)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    
    let $results                := qapi:createQuestionnaire($authmap, $projectPrefix, $data, $targetDate, $sourceId, $sourceEffectiveDate, $keepIds, $baseId)
    
    return
        roaster:response(201, 
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
        )
};

declare function qapi:putQuestionnaire($request as map(*)) {

    let $authmap                        := $request?user
    let $qid                            := $request?parameters?id
    let $qed                            := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $fhirVersion                    := $request?parameters?fhirVersion[string-length() gt 0]
    let $data                           := utillib:getBodyAsXml($request?body, 'questionnaire', ())
    let $deletelock                     := $request?parameters?deletelock = true()
    
    let $check                          :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                          :=
        if (empty($data)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $result                         := qapi:putQuestionnaire($authmap, string($qid), $qed, $data, $fhirVersion, $deletelock, true())
    return 
        if (empty($result)) then () else (
            element {name($result)} {
                $result/@*,
                namespace {"json"} {"http://www.json.org"},
                utillib:addJsonArrayToElements($result/*)
            }
        )
        
    (:let $accept-type                    := head(router:accepted-content-types()[contains(., 'json') or contains(., 'xml')])
    
    return
        if (empty($result)) then roaster:response(404, ()) else (
            response:redirect-to(xs:anyURI(serverapi:getServerURLFhirServices() || $result/@fhirVersion || '/public/Questionnaire/' || $result/@id || '--' || replace($result/@effectiveDate, '\D', '') || '/?_format=' || $accept-type))
        ):)
};

(: Central logic for creating an empty questionnaire

@param $authmap         - required. Map derived from token
@return (empty) questionnaire object as xml with json:array set on elements
:)
declare function qapi:createQuestionnaire($authmap as map(*), $projectPrefix as xs:string, $data as element(questionnaire)?, $targetDate as xs:boolean, $sourceId as xs:string?, $sourceEffectiveDate as xs:string?, $keepIds as xs:boolean?, $baseId as xs:string?) {

    let $decor                  := if (utillib:isOid($projectPrefix)) then utillib:getDecorById($projectPrefix) else utillib:getDecorByPrefix($projectPrefix)
    let $projectLanguage        := $decor/project/@defaultLanguage
    let $now                    := if ($targetDate) then substring(string(current-date()), 1, 10) || 'T00:00:00' else substring(string(current-dateTime()), 1, 19)
    
    let $sourceQuestionnaire    := if (empty($data)) then if (empty($sourceId)) then () else utillib:getQuestionnaire($sourceId, $sourceEffectiveDate) else $data
    
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, 'Project with prefix ' || $projectPrefix || ' does not exist')
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-RULES)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify questionnaires in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $baseId                 :=
        if (empty($baseId)) then decorlib:getDefaultBaseIdsP($decor, $decorlib:OBJECTTYPE-QUESTIONNAIRE)[1]/@id else
        if (decorlib:getBaseIdsP($decor, $decorlib:OBJECTTYPE-QUESTIONNAIRE)/@id = $baseId) then $baseId else (
            error($errors:BAD_REQUEST, 'Questionnaire base id ' || $baseId || ' is not configured as baseId for questionnaires in this project. Expected one of: ' || string-join(decorlib:getBaseIdsP($decor, $decorlib:OBJECTTYPE-QUESTIONNAIRE)/@id, ' '))
        )
    let $check                  :=
        if (empty($sourceId)) then () else if ($sourceQuestionnaire) then () else (
            error($errors:BAD_REQUEST, 'Source questionnaire based on id ' || $sourceId || ' and effectiveDate ' || $sourceEffectiveDate || ' does not exist. Cannot use this as source')
        )
    let $keepIds                := $keepIds = true()
    
    (: decorlib:getNextAvailableIdP() returns <next base="1.2" max="2" next="{$max + 1}" id="1.2.3" type="DS"/> :)
    let $newQuestionnaireId     := if ($keepIds) then $sourceQuestionnaire/@id else (decorlib:getNextAvailableIdP($decor, $decorlib:OBJECTTYPE-QUESTIONNAIRE, $baseId)/@id)
    
    let $check                  :=
        if (string-length($newQuestionnaireId) > 0 and string-length($projectLanguage) > 0) then () else (
            error($errors:BAD_REQUEST, 'Default base ID for questionnaires not set or project default language not set')
        )
        
    let $result                 :=
        if ($sourceQuestionnaire) then 
            <questionnaire id="{$newQuestionnaireId}" effectiveDate="{$now}" statusCode="new" lastModifiedDate="{$now}">
            {
                $sourceQuestionnaire/name,
                $sourceQuestionnaire/desc,
                $sourceQuestionnaire/classification,
                (:if ($data) then () else (
                    <relationship type="VERSION" ref="{$sourceQuestionnaire/@id}" flexibility="{$sourceQuestionnaire/@effectiveDate}"/>
                ),:)
                $sourceQuestionnaire/(node() except (name | desc | classification))
            }
            </questionnaire>
        else (
            <questionnaire id="{$newQuestionnaireId}" effectiveDate="{$now}" statusCode="draft" lastModifiedDate="{$now}">
                <name language="{$projectLanguage}">Questionnaire</name>
                <desc language="{$projectLanguage}">Questionnaire</desc>
            </questionnaire>
        )
    let $forOutput              := false()
    let $result                 := qapi:handleQuestionnaire($result, $decor/project/@prefix, $projectLanguage, map {}, (), false(), true(), $forOutput)
    
    let $check                  :=
        if ($result) then (
            let $r  := validation:jaxv-report($result, $setlib:docDecorSchema)
            return
                if ($r/status='invalid') then
                    error($errors:BAD_REQUEST, 'Questionnaire is not schema compliant: ' || serialize($r))
                    (:error($errors:BAD_REQUEST, 'Questionnaire is not schema compliant: ' || serialize($r) || ' ------- ' || serialize($result)):)
                else ()
        )
        else ()
    
    let $result                 := (
        comment { ' ' || replace($result/name[1], '--', '-') || ' ' },
        <questionnaireAssociation questionnaireId="{$result/@id}" questionnaireEffectiveDate="{$result/@effectiveDate}"/>,
        $result
    )
    
    let $updateQuestionnaire    := 
        if ($decor/rules) then
            update insert $result into $decor/rules
        else
        if ($decor/issues) then
            update insert <rules>{$result}</rules> preceding $decor/issues
        else (
            update insert <rules>{$result}</rules> into $decor
        )
    
    let $forOutput              := true()
    let $result                 := qapi:handleQuestionnaire(utillib:getQuestionnaire($result/@id, $result/@effectiveDate), $decor/project/@prefix, $projectLanguage, map {}, (), false(), true(), $forOutput)
    
    return
        $result
};

(:~ Central logic for updating an existing questionnaire

@param $authmap         - required. Map derived from token
@param $id              - required. DECOR questionnaire/@id to update
@param $effectiveDate   - required. DECOR questionnaire/@effectiveDate to update
@param $data            - required. DECOR questionnaire xml element containing everything that should be in the updated questionnaire or quesrtionnaire response
@return concept object as xml with json:array set on elements
:)
declare function qapi:putQuestionnaire($authmap as map(*), $id as xs:string, $effectiveDate as xs:string, $data as item(), $fhirVersion as xs:string?, $deletelock as xs:boolean, $doQuestionnaire as xs:boolean) {

    let $storedQuestionnaire    := utillib:getQuestionnaire($id, $effectiveDate)
    let $decor                  := $storedQuestionnaire/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix
    let $projectLanguage        := $decor/project/@defaultLanguage
    let $objectName             := 
        switch (lower-case(local-name($storedQuestionnaire)))
        case 'questionnaire' return 'Questionnaire'
        default return upper-case(substring(name($storedQuestionnaire), 1, 1)) || substring(name($storedQuestionnaire), 2)
    
    let $objectType             := $decorlib:OBJECTTYPE-QUESTIONNAIRE
     
    let $check                  :=
        if (empty($storedQuestionnaire)) then (
            error($errors:BAD_REQUEST, $objectName || ' with id ' || $id || ' and effectiveDate ' || $effectiveDate || ' does not exist')
        )
        else
        if (count($storedQuestionnaire) gt 1) then (
            error($errors:SERVER_ERROR, 'Found multiple results for id ' || $id || ' effectiveDate ' || $effectiveDate || '. Expected 1..1. Alert your administrator as this should not be possible.')
        )
        else
        if ($storedQuestionnaire[@statusCode = $qapi:STATUSCODES-FINAL]) then
            error($errors:BAD_REQUEST, $objectName || ' with id ''' || $id || ''' and effectiveDate ''' || $effectiveDate || ''' cannot be updated while it one of status: ' || string-join($qapi:STATUSCODES-FINAL, ', '))
        else ()
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-RULES)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify scenarios in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    
    let $fhirVersion            := ($fhirVersion, $storedQuestionnaire/@fhirVersion)[1]
    
    let $check                  :=
        if ($fhirVersion = $qapi:FHIROBJECT-FORMAT/enumeration/@value) then () else (
            $objectName || '.fhirVersion has unsupported value ' || string-join($fhirVersion, ', ') || '. SHALL be one of: ' || string-join($qapi:FHIROBJECT-FORMAT/enumeration/@value, ', ')
        )
    
    let $forOutput              := false()
    (: build map of oid names. is more costly when you do that deep down :)
    let $oidnamemap             := map {}
    let $preparedData           := qapi:handleQuestionnaire($data, $projectPrefix, $projectLanguage, $oidnamemap, (), false(), true(), $forOutput)
    let $check                  := (
            let $r  := validation:jaxv-report($preparedData, $setlib:docDecorSchema)
            return
                if ($r/status='invalid') then
                    error($errors:BAD_REQUEST, 'Contents are not schema compliant: ' || serialize($r))
                else ()
        )
    let $check                  :=
        if (lower-case(local-name($storedQuestionnaire)) = lower-case(local-name($data))) then () else (
            error($errors:BAD_REQUEST, 'You cannot update a ' || $objectName || ' with a ' || local-name($data))
        )
    
    let $lock                   := decorlib:getLocks($authmap, $id, $effectiveDate, ())
    let $lock                   := if ($lock) then $lock else decorlib:setLock($authmap, $id, $effectiveDate, false())

    let $check                  :=
        if ($lock) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have a lock for this ' || name($storedQuestionnaire) || ' (anymore). Get a lock first.'))
        )                   
    
    let $intention              :=
        if ($storedQuestionnaire[@statusCode='final']) then 'patch' else 'version'
    let $history                := histlib:AddHistory($authmap?name, $objectType, $projectPrefix, $intention, $storedQuestionnaire)
    
    let $instant                := current-dateTime()
    let $now                    := substring(string($instant), 1, 19)
    let $locks                  := if ($deletelock) then decorlib:getLocks($authmap, $storedQuestionnaire, true()) else ()
    let $update                 := update replace $storedQuestionnaire with $preparedData
    let $update                 := update value $storedQuestionnaire/@lastModifiedDate with substring(string(current-dateTime()), 1, 19)
    
    let $delete                 := if ($locks) then update delete $locks else ()
    
    let $forOutput              := true()
    (: build map of oid names. is more costly when you do that deep down :)
    let $oidnamemap             := qapi:buildOidNameMap($data, $projectLanguage[1])
    let $preparedData           := qapi:handleQuestionnaire($data, $projectPrefix, $projectLanguage, $oidnamemap, (), false(), true(), $forOutput)
    
    return
        element {name($preparedData)} {
            if ($data/@uuid) then $data/@uuid else attribute uuid {util:uuid()},
            $preparedData/(@* except @uuid),
            $preparedData/*
        }

};

(:~ Input is questionnaire. Returns just the DECOR wrapper info, not the actual Q. 
 
  @param $listMode as boolean, skips any parts irrelevant to a tree of questionnaires hence adds just meta data
  @param $decorMode as boolean, skips uuid attribute
:)
declare function qapi:handleQuestionnaire($qObjects as element()*, $projectPrefix as xs:string?, $projectLanguage as xs:string?, $oidnamemap as map(*)?, $questionnaireResponses as element(f:QuestionnaireResponse)*, $listMode as xs:boolean, $decorMode as xs:boolean, $forOutput as xs:boolean) as element()* {
    for $q in $qObjects 
    let $isQQ                   := lower-case(local-name($q)) = 'questionnaire'
    let $isQR                   := lower-case(local-name($q)) = 'questionnaireresponse'
    let $iddisplay              := if ($q/@id) then map:get($oidnamemap, $q/@id) else ()
    let $projectLanguage        := if (empty($projectLanguage)) then ($q/ancestor::decor/project/@defaultLanguage, 'en-US')[1] else $projectLanguage
    let $canonicalUri           :=
        if ($isQQ) then attribute canonicalUri {($q/@canonicalUri[. castable as xs:anyURI], serverapi:getServerURLFhirCanonicalBase() || 'Questionnaire/'|| $q/@id || '--' || replace($q/@effectiveDate, '\D', ''))[1]} else ()
    let $fhirVersion            := 
        (: 2022-12-31 AH Disabled for questionnaire after leaving the FHIR format for DECOR :)
        if (local-name($q) = 'QuestionnaireResponse') then attribute fhirVersion {tokenize(util:document-name($q), '-')[2]} else ()
    (: empty $oidnamemap unless we are working on output, otherwise we add codeSystemName attributes to the db that the user did not anticipate :)
    let $oidnamemap             :=
        if ($forOutput) then $oidnamemap else map {}
    return
        element {lower-case(local-name($q))}
        {
            (: =========== attributes =========== :)
            if ($decorMode) then () else (
                if ($q/@uuid) then $q/@uuid else attribute uuid {util:uuid()}
            ),
            (:Not in use yet: Reference to a Questionnaire Wrapper @id. Questionnaire Wrappers SHALL carry either @id or @ref:)
            if ($q/@ref) then $q/@ref else attribute id {($q/@id, $q/f:id/@value)[1]},
            if ($q/@effectiveDate) then $q/@effectiveDate else (
                let $d  := ($q/f:authored/@value, $q/f:meta/f:lastUpdated/@value, string(current-dateTime()))[1]
                return
                    attribute effectiveDate {
                        if ($d castable as xs:date) then
                            substring($d, 1, 10) || 'T00:00:00'
                        else (
                            substring($d, 1, 19)
                        )
                    }
            ),
            if ($q/@statusCode) then $q/@statusCode else if ($q/f:status/@value) then attribute statusCode {$q/f:status/@value} else (),
            $q/@versionLabel,
            $q/@expirationDate[. castable as xs:dateTime],
            $q/@officialReleaseDate[. castable as xs:dateTime],
            $canonicalUri,
            if ($isQR and $q/f:questionnaire/@value) then attribute questionnaire {$q/f:questionnaire/@value} else (), 
            if ($q/@lastModifiedDate) then $q/@lastModifiedDate else attribute lastModifiedDate {substring(($q/f:meta/f:lastUpdated/@value, string(current-dateTime()))[1], 1, 19)},
            if (string-length($iddisplay) = 0) then () else attribute iddisplay {$iddisplay},
            if ($q/ancestor::decor/project[@prefix = $projectPrefix]) then () else
            if (empty($q/ancestor::decor/project/@prefix)) then () else (
                attribute ident {$q/ancestor::decor/project/@prefix}
            )
            ,
            $fhirVersion,
            if ($isQR) then attribute link {xs:anyURI(serverapi:getServerURLFhirServices() || $fhirVersion || '/public/QuestionnaireResponse/' || $q/f:id/@value[1])} else (), 
            $q/ancestor::decor/@versionDate,
            $q/ancestor::decor/@language,
            
            (:Identifying the Questionnaire as Enhanced (type is QE), thus edited, modified, enriched.:)
            if ($isQQ) then attribute enhanced {$q/@enhanced = 'true'} else (),
            (: =========== elements =========== :)
            if ($q/name) then $q/name else (
                <name language="{($projectLanguage, 'en-US')[1]}"> 
                {
                    'QR authored at ' || substring(($q/f:authored/@value, $q/f:meta/f:lastUpdated/@value, string(current-dateTime()))[1], 1, 10)
                }
                </name>
            ),
            if ($listMode) then () else (
                $q/desc
            ),
            $q/classification,
            if ($listMode) then $q/relationship else (
                for $relationship in $q/relationship
                let $object   :=
                    if ($forOutput) then 
                        let $rid                    := $relationship/@ref
                        let $red                    := $relationship/@flexibility
                        let $rtp                    := $relationship/@type
                        return
                            switch ($rtp)
                            case 'ANSW' return utillib:getQuestionnaire($rid, $red)
                            case 'DRIV' return utillib:getTransaction($rid, $red)
                            default return ( (: we don't know this type (yet?). 
                                Assume that questionnaires could be a specialization/version etc of another questionnaire, and questionnairereponse could be that of another questionnairerepsonse :)
                                if ($isQQ) then utillib:getQuestionnaire($rid, $red) else utillib:getQuestionnaireResponse($rid, $red)
                            )
                     else ()       
               
                let $artifact := decorlib:getObjectDecorType($object[1])
                return
                    <relationship>
                    {
                        $relationship/@type[not(. = '')],
                        $relationship/@ref[not(. = '')],
                        $relationship/@flexibility[not(. = '')]
                    }
                    {
                        if ($object) then (
                            $object/ancestor::decor/project/@prefix,
                            attribute iStatusCode {$object/@statusCode}, 
                            if ($object/@expirationDate) then attribute iExpirationDate {$object/@expirationDate} else (),
                            if ($object/@versionLabel) then attribute iVersionLabel {$object/@versionLabel} else (),
                            let $iddisplay  := 
                                if (map:contains($oidnamemap, $relationship/@ref)) then map:get($oidnamemap, $relationship/@ref) else (
                                    utillib:getNameForOID($relationship/@ref, $projectLanguage, $object/ancestor::decor)
                                )
                            return attribute refdisplay {$iddisplay},
                            attribute artifact {$artifact},
                            (:attribute localInherit {$object/ancestor::decor/project/@prefix = $projectPrefix},:)
                            if ($projectLanguage = '*') then ($object/name) else (
                                <name language="{$projectLanguage}">{data(($object/name[@language = $projectLanguage], $object/name)[1])}</name>
                            )
                        ) else ()
                    }
                    </relationship>
            )
            ,
            if ($listMode) then () else 
            if ($isQQ) then (
                $q/subjectType,
                if ($q/publishingAuthority) then 
                    (for $childnode in $q/publishingAuthority return utillib:preparePublishingAuthorityForUpdate($childnode))
                    else if ($forOutput) then utillib:inheritPublishingAuthority($q) else (),
                for $childnode in $q/code return utillib:prepareValueCodingTypeForUpdate($childnode, $oidnamemap),
                if ($q/copyright) then $q/copyright else if ($forOutput) then utillib:inheritCopyright($q) else (),
                for $childnode in $q/jurisdiction return utillib:prepareValueCodingTypeForUpdate($childnode, $oidnamemap),
                $q/contained,
                qapi:handleQuestionnaireItem($q/item, $oidnamemap, ())
            )
            else ()
            ,
            if ($decorMode) then () else (
                (: connect questionnaireresponse. Give empty parameter $filteredQuestionnaireResponses to avoid processing any further :)
                for $qr in $questionnaireResponses[f:questionnaire/@value = $canonicalUri]
                return
                    qapi:handleQuestionnaire($qr, $projectPrefix, $projectLanguage, $oidnamemap, (), $listMode, $decorMode, $forOutput)
            )
        }
};

declare function qapi:handleQuestionnaireItem($items as element(item)*, $oidnamemap as map(*)?, $storedItems as element(item)*) as element(item)* {
    qapi:handleQuestionnaireItem($items, $oidnamemap, $storedItems, (), ())
};

declare function qapi:handleQuestionnaireItem($items as element(item)*, $oidnamemap as map(*)?, $storedItems as element(item)*, $readOnlyInherit as xs:string?, $hiddenItemInherit as xs:string?) as element(item)* {
    for $item in $items
    let $storedItem         := $storedItems[@linkId = $item/@linkId]
        
    (: AD30-1697 Switch on readOnly: if group item readOnly is true all children should inherit this value :)
    let $readOnlyInherit    :=
        if (not(empty($readOnlyInherit)) or empty($storedItem)) then $readOnlyInherit 
        (:else if ($item/@type = "group" and $item/@readOnly != $storedItem/@readOnly) then $item/@readOnly:)
        else if ($item/@type = "group" and $item/@readOnly = true()) then true() else ()

    let $readOnly           :=
        if (not(empty($readOnlyInherit))) then $readOnlyInherit else $item/@readOnly
    
    (: AD30-1697 Switch on hiddenItem: if group item hiddenItem is true all children should inherit this value :)    
    let $hiddenItemInherit    :=
        if (not(empty($hiddenItemInherit)) or empty($storedItem)) then $hiddenItemInherit 
            (:else if ($item/@type = "group" and $item/@hiddenItem != $storedItem/@hiddenItem) then $item/@hiddenItem :)
            else if ($item/@type = "group" and $item/@hiddenItem = true()) then true() else ()
    let $hiddenItem           :=
        if (not(empty($hiddenItemInherit))) then $hiddenItemInherit else $item/@hiddenItem    
    
    return 
    <item>
    {
        for $att in $item/(@id | @linkId | @definition | @prefix | @type | @enableBehavior | @required | @repeats | @minLength | @maxLength | @minValue | @maxValue | @maxDecimalPlaces | @renderingStyle)
        order by name($att)
        return
            if (normalize-space($att) = '') then () else (
                (:switch (name($att))
                case 'readOnly' return attribute {name($att)} {replace($readonly, '(^\s+)|(\s+$)', '')}
                default return attribute {name($att)} {replace($att, '(^\s+)|(\s+$)', '')}:)
                attribute {name($att)} {replace($att, '(^\s+)|(\s+$)', '')}
                
            )
    }
    { 
        if (not(empty($readOnly))) then attribute readOnly {$readOnly} else attribute readOnly {false()},
        if (not(empty($hiddenItem))) then attribute hiddenItem {$hiddenItem} else attribute hiddenItem {false()}
    }
    {
        $item/text,
        $item/synonym,
        $item/operationalization,
        for $childnode in $item/code return utillib:prepareValueCodingTypeForUpdate($childnode, $oidnamemap),
        $item/designNote,
        for $childnode in $item/itemControl return utillib:prepareValueCodingTypeForUpdate($childnode, $oidnamemap),
        for $childnode in $item/unitOption return utillib:prepareValueCodingTypeForUpdate($childnode, $oidnamemap),
        for $childnode in $item/enableWhen return qapi:prepareQuestionnaireEnableWhenConditionTypeForUpdate($childnode, $oidnamemap),
        for $childnode in $item/answerValueSet return qapi:prepareQuestionnaireAnswerValueSetForUpdate($childnode, $oidnamemap),
        (:$item/answerIntensionalInclude[@*],:)
        for $childnode in $item/answerOption return qapi:handleQuestionnaireItemAnswerOptionType($childnode, $oidnamemap),
        for $childnode in $item/initial return qapi:handleQuestionnaireItemInitialOptionType($childnode, $oidnamemap),
        $item/terminologyServer[@ref[not(. = '')]],
        for $childnode in $item/itemExtractionContext return qapi:handleQuestionnaireItemExtractionContext($childnode),
        qapi:handleQuestionnaireItem($item/item, $oidnamemap, (if (empty($storedItem)) then () else $storedItem/item), $readOnlyInherit, $hiddenItemInherit)
    }
    </item>
    
};
declare function qapi:prepareQuestionnaireEnableWhenConditionTypeForUpdate($item as element(enableWhen), $oidnamemap as map(*)?) as element(enableWhen)? {
    for $node in $item[exists(*[starts-with(name(), 'answer')])]
    return
    <enableWhen>
    {
        for $att in $item/(@id | @question | @operator)
        order by name($att)
        return
            if (normalize-space($att) = '') then () else (
                attribute {name($att)} {replace($att, '(^\s+)|(\s+$)', '')}
            )
    }
    {
        if ($node/answerBoolean) then 
            $node/answerBoolean[1]
        else 
        if ($node/answerDecimal) then 
            $node/answerDecimal[1]
        else
        if ($node/answerInteger) then 
            $node/answerInteger[1]
        else
        if ($node/answerDate) then 
            $node/answerDate[1]
        else
        if ($node/answerDateTime) then 
            $node/answerDateTime[1]
        else
        if ($node/answerTime) then 
            $node/answerTime[1]
        else
        if ($node/answerString) then 
            $node/answerString[1]
        else
        if ($node/answerCoding) then 
            utillib:prepareValueCodingTypeForUpdate($node/answerCoding[1], $oidnamemap)
        else
        if ($node/answerQuantity) then 
            utillib:prepareValueQuantityTypeForUpdate($node/answerQuantity[1], $oidnamemap)
        else ()
    }
    </enableWhen>
};

declare function qapi:prepareQuestionnaireAnswerValueSetForUpdate($item as element(answerValueSet), $oidnamemap as map(*)?) as element(answerValueSet)? {
    for $node in $item
    return
    <answerValueSet>
    {
        for $att in $item/@*
        return
            if (normalize-space($att) = '') then () else (
                attribute {name($att)} {replace($att, '(^\s+)|(\s+$)', '')}
            )
    }
    {
        if ($node/name) then 
            $node/name
        else ()

    }
    </answerValueSet>
};

declare function qapi:handleQuestionnaireItemAnswerOptionType($item as element(answerOption), $oidnamemap as map(*)?) as element(answerOption)? {
    for $node in $item[valueInteger | valueDate | valueTime | valueString | valueCoding | valueReference]
    return
    <answerOption>
    {
        if ($node/valueInteger) then 
            $node/valueInteger[1]
        else 
        if ($node/valueDate) then 
            $node/valueDate[1]
        else
        if ($node/valueTime) then 
            $node/valueTime[1]
        else
        if ($node/valueString) then 
            $node/valueString (: string can have mulitple mulit-lingual instances :)
        else
        if ($node/valueCoding) then
            (
            $node/@ordinal[not(. = '')],
            utillib:prepareValueCodingTypeForUpdate($node/valueCoding[1], $oidnamemap)
            )
        else
        if ($node/valueReference) then 
            $node/valueReference[1]
        else ()
    }
    </answerOption>
};
declare function qapi:handleQuestionnaireItemInitialOptionType($item as element(initial), $oidnamemap as map(*)?) as element(initial)? {
    for $node in $item[valueBoolean | valueDecimal | valueInteger | valueDate | valueDateTime | valueTime | valueString | valueUri | valueCoding | valueQuantity]
    return
    <initial>
    {
        if ($node/valueBoolean) then 
            $node/valueBoolean[1]
        else
        if ($node/valueDecimal) then 
            $node/valueDecimal[1]
        else
        if ($node/valueInteger) then 
            $node/valueInteger[1]
        else 
        if ($node/valueDate) then 
            $node/valueDate[1]
        else
        if ($node/valueDateTime) then 
            $node/valueDateTime[1]
        else
        if ($node/valueTime) then 
            $node/valueTime[1]
        else
        if ($node/valueString) then 
            $node/valueString[1]
        else
        if ($node/valueUri) then 
            $node/valueUri[1]
        else
        if ($node/valueCoding) then 
            utillib:prepareValueCodingTypeForUpdate($node/valueCoding[1], $oidnamemap)
        else
        if ($node/valueQuantity) then 
            utillib:prepareValueQuantityTypeForUpdate($node/valueQuantity[1], $oidnamemap)
        else ()
    }
    </initial>
};
(: Establishes mapping context for a Questionnaire item :)
declare function qapi:handleQuestionnaireItemExtractionContext($item as element(itemExtractionContext)) as element(itemExtractionContext)? {
    for $node in $item[(@code | expression/@*)[not(normalize-space() = '')]]
    return
    <itemExtractionContext>
    {
        for $att in $node/@code
        return
            if (normalize-space($att) = '') then () else (
                attribute {name($att)} {replace($att, '(^\s+)|(\s+$)', '')}
            )
        ,
        for $childnode in $node/expression
        return
            <expression>
            {
                for $att in $childnode/(@description | @name | @language | @expression | @reference)
                order by name($att)
                return
                    if (normalize-space($att) = '') then () else (
                        attribute {name($att)} {replace($att, '(^\s+)|(\s+$)', '')}
                    )
            }
            </expression>
    }
    </itemExtractionContext>
};

