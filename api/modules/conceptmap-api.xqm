xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:~ Dataset concept API allows read, create, update on DECOR concepts in DECOR datasets :)
module namespace mpapi                 = "http://art-decor.org/ns/api/conceptmap";

import module namespace roaster     = "http://e-editiones.org/roaster";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

import module namespace utillib     = "http://art-decor.org/ns/api/util" at "/db/apps/api/modules/library/util-lib.xqm";
import module namespace utilhtml    = "http://art-decor.org/ns/api/util-html" at "library/util-html-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "/db/apps/api/modules/library/settings-lib.xqm";
import module namespace decorlib    = "http://art-decor.org/ns/api/decor" at "/db/apps/api/modules/library/decor-lib.xqm";
import module namespace ggapi       = "http://art-decor.org/ns/api/governancegroups" at "governancegroups-api.xqm";
import module namespace histlib     = "http://art-decor.org/ns/api/history" at "library/history-lib.xqm";

declare namespace json      = "http://www.json.org";
declare namespace http      = "http://expath.org/ns/http-client";

declare %private variable $mpapi:EQUIVALENCY-TYPE       := utillib:getDecorTypes()/EquivalencyType;
declare %private variable $mpapi:CODINGSTRENGTH-TYPE    := utillib:getDecorTypes()/CodingStrengthType;
declare %private variable $mpapi:STATUSCODES-FINAL      := ('final', 'pending', 'rejected', 'cancelled', 'deprecated');
declare %private variable $mpapi:ADDRLINE-TYPE          := utillib:getDecorTypes()/AddressLineType;

(:~ Retrieves latest DECOR conceptMap based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the conceptMap
    @param $projectPrefix           - optional. limits scope to this project only
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @return as-is or as compiled as JSON
    @since 2020-05-03
:)
declare function mpapi:getLatestConceptMap($request as map(*)) {
    mpapi:getConceptMap($request)
};

(:~ Retrieves DECOR conceptMap based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the conceptMap
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the conceptMap. If not given assumes latest version for id
    @param $projectPrefix           - optional. limits scope to this project only
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @return as-is or as compiled as JSON
    @since 2020-05-03
:)
declare function mpapi:getConceptMap($request as map(*)) {
    
    let $projectPrefix                  := $request?parameters?prefix
    let $projectVersion                 := $request?parameters?release
    let $projectLanguage                := $request?parameters?language
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $withversions                   := $request?parameters?versions = true()
    
    let $results                        := mpapi:getConceptMap($projectPrefix, $projectVersion, $projectLanguage, $id, $effectiveDate, $withversions, true())
    let $results                        := 
        if ($withversions) then 
            <list artifact="VS" current="{count($results)}" total="{count($results)}" all="{count($results)}" lastModifiedDate="{current-dateTime()}">{$results}</list> 
        else (
            head($results)
        )
    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple conceptMaps for id '", $id, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        )
        else (
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
        )
};

(:~ Retrieves DECOR conceptmap for publication based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the conceptmap
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the conceptmap. If not given assumes latest version for id
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @param $projectLanguage         - optional parameter to select from a specific compiled language
    @param $format                  - optional. overrides the accept-header for frontend purposes
    @param $download                - optional as xs:boolean. Default: false. 
    @return as-is or as compiled as JSON
    @since 2024-12-12
:)

declare function mpapi:getConceptMapExtract($request as map(*)) {

    let $project                        := $request?parameters?project[not(. = '')]
    let $projectVersion                 := $request?parameters?release[not(. = '')]
    let $projectLanguage                := $request?parameters?language[not(. = '')]
    let $format                         := $request?parameters?format[not(. = '')]
    let $download                       := $request?parameters?download=true()
    
    let $id                             := $request?parameters?id[not(. = '')]
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }

    (: parameter format overrides accept-header as mime-type for using the api in the browser :)
    
    (: 
        this call is used in backend and frontend (browser) - default behaviour mime-types:
        - backend:   default mime-type is json - response payload is json - if no accept type is given
        - frontend:  default mime-type is xml - response payload is xml - if no format is given
    :)
    
    let $acceptTypes                    := roaster:accepted-content-types()
    let $acceptedType                   := ($acceptTypes[. = ('application/xml', 'application/json')],'application/json')[1]
    
    let $format                         := if ($format) then $format else tokenize($acceptedType, '/')[2]

    (: 
       overwrite format in the backend is not always possible because of middleware behaviour
       - if in backend the format is xml and accept is not application/xml, roaster always gives a json payload 
    :) 
    let $check                          :=
        if ($format = 'xml' and not($acceptedType = 'application/xml')) then 
            error($errors:BAD_REQUEST, 'In case of format parameter is xml the accept header should be application/xml')
        else ()   
    
    let $projectPrefix                  := 
        if (empty($project)) then () else utillib:getDecor($project, $projectVersion, $projectLanguage)/project/@prefix      
    
    let $conceptMaps                    := 
        if (empty($projectPrefix)) then mpapi:getConceptMap('', '', $projectLanguage, $id, $effectiveDate, true(), true()) else mpapi:getConceptMap($projectPrefix, $projectVersion, $projectLanguage, $id, $effectiveDate, true(), true())

    let $results                        :=
        if (empty($conceptMaps/*)) then () 
        else if ($format = 'xml') then
            <conceptMaps>
            {
                $conceptMaps
            }
            </conceptMaps>
        else $conceptMaps

    let $filename                       := 'MP_' || $conceptMaps/descendant-or-self::conceptMap/@displayName[1] || '_(download_' || substring(fn:string(current-dateTime()),1,19) || ').' || $format
    
    let $results                        :=
        for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/node(), $format)
                }
    
    (: in this case the json format overrides header application/xml and works with a text header :)
    let $results                        :=
        if ($results and $format = 'json' and not($acceptedType = 'application/json')) then
            fn:serialize($results, map{"method": $format , "indent": true()})
            else $results     
           
    let $r-header                       := 
        (response:set-header('Content-Type', 'application/' || $format || '; charset=utf-8'),
        if ($download) then response:set-header('Content-Disposition', 'attachment; filename='|| $filename) else ())
    
    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple conceptmaps for id '", $id, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        )
        else $results        

};

(:~ Retrieves DECOR conceptmap view based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the conceptmap
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the dataset. If not given assumes latest version for id
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @param $projectLanguage         - optional parameter to select from a specific compiled language
    @param $inline                  - optional parameter to omit HTML header info. Useful for inclusion of HTML in other pages.
    @param $collapsable             - optional parameter the valueset is collapable.
    @return as-is 
    @since 2024-12-12
:)

declare function mpapi:getConceptMapView($request as map(*)) {

    let $project                        := $request?parameters?project[not(. = '')]
    let $projectVersion                 := $request?parameters?release[not(. = '')]
    let $projectLanguage                := $request?parameters?language[not(. = '')]
    let $inline                         := $request?parameters?inline = true()
    let $collapsable                    := not($request?parameters?collapsable = false())
    
    let $id                             := $request?parameters?id[not(. = '')]
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }

    let $decor                          := 
        if (empty($project)) then () else utillib:getDecor($project, $projectVersion, $projectLanguage)
    
    let $results                        := 
        if (empty($decor/project/@prefix)) then mpapi:getConceptMap('', '', $projectLanguage, $id, $effectiveDate, true(), true()) else mpapi:getConceptMap($decor/project/@prefix, $projectVersion, $projectLanguage, $id, $effectiveDate, true(), true())

    let $filename                       := 'MP_' || $results/descendant-or-self::conceptMap/@displayName[1] || '_(download_' || substring(fn:string(current-dateTime()),1,19) || ').html'
            
    let $r-header                       := 
        (response:set-header('Content-Type', 'text/html; charset=utf-8'),
        response:set-header('Content-Disposition', 'filename='|| $filename))
    
    return
        if (empty($results/*)) then (
            roaster:response(404, ())
        )

        else 
        
        (: prepare for Html :)
        let $language                       := if (empty($projectLanguage)) then $setlib:strArtLanguage else $projectLanguage
        let $header                         := if ($inline) then false() else true()        
        
        return utilhtml:convertObject2Html($results, $language, $header, $collapsable, $projectVersion, $decor)
       
};

(:~ Retrieves DECOR conceptMap history based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id
    @param $effectiveDate           - optional parameter denoting the effectiveDate. If not given assumes latest version for id
    @return as-is or as compiled as JSON
    @since 2020-05-03
:)
declare function mpapi:getConceptMapHistory($request as map(*)) {
    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $projectPrefix                  := ()
    let $results                        := histlib:ListHistory($authmap, $decorlib:OBJECTTYPE-MAPPING, (), $id, $effectiveDate, 0)
    
    return
        <list artifact="{$decorlib:OBJECTTYPE-MAPPING}" current="{count($results)}" total="{count($results)}" all="{count($results)}" lastModifiedDate="{current-dateTime()}">
        {
            utillib:addJsonArrayToElements($results)
        }
        </list>
};

(:~ Retrieves DECOR conceptMap usage based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the conceptMap
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the conceptMap. If not given assumes latest version for id
    @param $projectPrefix           - optional. limits scope to this project only
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @return as-is or as compiled as JSON
    @since 2020-05-03
:)
declare function mpapi:getConceptMapUsage($request as map(*)) {
    
    let $projectPrefix                  := $request?parameters?prefix
    let $projectVersion                 := $request?parameters?release
    let $projectLanguage                := $request?parameters?language
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    
    let $conceptMapsAll                   :=
        if (empty($id)) then () else (
            if (empty($projectPrefix)) then
                mpapi:getConceptMapById($id, (), false())
            else (
                mpapi:getConceptMapById($id, (), $projectPrefix, (), false())
            )
        )
    let $mostRecent                     := string(max($conceptMapsAll/descendant-or-self::conceptMap/xs:dateTime(@effectiveDate)))
    let $cm                             := 
        if (empty($effectiveDate)) then 
            $conceptMapsAll/descendant-or-self::conceptMap[@effectiveDate = $mostRecent] 
        else (
            $conceptMapsAll/descendant-or-self::conceptMap[@effectiveDate = $effectiveDate]
        )
    let $cm                             := $cm[1]
    
    (:let $check                          :=
        if (count($cm) le 1) then $cm else (
            error(xs:QName('mpapi:getConceptMapUsage'), 'ConceptMap ' || $id || ' effectiveDate ''' || $effectiveDate || ''' yields ' || count($cm) || ' results. Projects involved: ' || string-join(distinct-values($cm/ancestor-or-self::*/@ident), ', '))
        ):)
    
    let $isMostRecent                   := $mostRecent = $effectiveDate or empty($effectiveDate)
    
    (:let $allAssociations                := $setlib:colDecorData//terminologyAssociation[@conceptMap = $id]
    let $allAssociations                :=
        if ($isMostRecent) then 
            $allAssociations[@flexibility = $effectiveDate] | $allAssociations[not(@flexibility castable as xs:dateTime)]
        else (
            $allAssociations[@flexibility = $effectiveDate]
        )
    let $allAssociations                :=
        if (empty($projectPrefix)) then $allAssociations else (
            for $ta in $allAssociations
            return
                if ($ta/ancestor::decor/project/@prefix = $projectPrefix) then $ta else ()
        )
        
    let $conceptMapsAll                   := $setlib:colDecorData//terminology/conceptMap//*[@ref = $id]
    let $conceptMapsAll                   :=
        if ($isMostRecent) then 
            $conceptMapsAll[@flexibility = $effectiveDate] | $conceptMapsAll[not(@flexibility castable as xs:dateTime)]
        else (
            $conceptMapsAll[@flexibility = $effectiveDate]
        )
    
    let $allTemplAssociations           := $setlib:colDecorData//template//vocabulary[@conceptMap = $id]
    let $allTemplAssociations           :=
        if ($isMostRecent) then 
            $allTemplAssociations[@flexibility = $effectiveDate] | $allTemplAssociations[not(@flexibility castable as xs:dateTime)]
        else (
            $allTemplAssociations[@flexibility = $effectiveDate]
        )
    
    let $url                            := $utillib:strDecorServicesURL
    
    let $results                        := (
        for $item in $allAssociations
        let $clpfx  := $item/ancestor::decor/project/@prefix
        let $clid   := $item/@conceptId
        group by $clpfx, $clid
        return (
            let $originalConcept            := utillib:getConceptList($item[1]/@conceptId, ())/ancestor::concept[1]
            return
                if ($originalConcept) then utillib:doConceptAssociation($cm[1], $originalConcept, $originalConcept/name) else ()
        )
        ,
        for $item in $conceptMapsAll
        let $cmid := $item/ancestor::conceptMap[1]/@id
        let $cmed := $item/ancestor::conceptMap[1]/@effectiveDate
        group by $cmid, $cmed
        return
            utillib:doConceptMapAssociation($cm, $item[1])
        ,
        for $item in $allTemplAssociations
        let $tmid := $item/ancestor::template[1]/@id
        let $tmed := $item/ancestor::template[1]/@effectiveDate
        group by $tmid, $tmed
        return
            utillib:doTemplateAssociation($cm, $item[1])
    ):)
    
    let $results                        := ()
    let $count                          := count($results)
    let $max                            := $count
    let $allcnt                         := $count
    
    return
        <list artifact="USAGE" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements(subsequence($results, 1, $max))
        }
        </list>
};

(:~ Retrieves DECOR concept maps based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
@param $id                          - required parameter denoting the id of the concept
@param $effectiveDate               - required parameter denoting the effectiveDate of the concept. If not given assumes latest version for id
@param $transactionId               - required parameter denoting the id of the transaction that the concept is in
@param $transactionEffectiveDate    - optional parameter denoting the effectiveDate of the transaction. If not given assumes latest version for id
@param $projectVersion              - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
@param $projectLanguage             - optional parameter to select from a specific compiled language
@param $associations                - optional boolean parameter relevant if $treeonly = 'false' to include associations: terminologyAssociation, identifierAssociation
@return as-is or as compiled as JSON
@since 2020-05-03
:)
declare function mpapi:getConceptMapList($request as map(*)) {
    
    let $governanceGroupId      := $request?parameters?governanceGroupId
    let $projectPrefix          := $request?parameters?prefix
    let $projectVersion         := $request?parameters?release
    let $projectLanguage        := $request?parameters?language
    let $max                    := $request?parameters?max
    let $resolve                :=  if (empty($governanceGroupId)) then not($request?parameters?resolve = false()) else $request?parameters?resolve = true()
    let $searchTerms            := 
        array:flatten(
            for $s in ($request?parameters?search, $request?parameters?id, $request?parameters?name)[string-length() gt 0]
            return
                tokenize(normalize-space(lower-case($s)),'\s')
        )
    let $includebbr             := $request?parameters?includebbr = true()
    let $sort                   := $request?parameters?sort
    let $sortorder              := $request?parameters?sortorder
    
    let $check                  :=
        if (empty($governanceGroupId) and empty($projectPrefix)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have either parameter governanceGroupId or prefix')
        else 
        if (not(empty($governanceGroupId)) and not(empty($projectPrefix))) then 
            error($errors:BAD_REQUEST, 'Request SHALL NOT have both parameter governanceGroupId or prefix, not both')
        else ()
    let $check                  :=
        if (empty($governanceGroupId)) then () else if ($resolve) then
            error($errors:BAD_REQUEST, 'Request SHALL NOT have governance group scope and resolve=true. This is too expensive to support')
        else ()
    
    let $result                 :=
        if (empty($governanceGroupId)) then
            mpapi:getConceptMapList($governanceGroupId, $projectPrefix, $projectVersion, $projectLanguage, $searchTerms, $includebbr, $sort, $sortorder, $max, $resolve, $request?parameters)
        else (
            for $projectId in ggapi:getLinkedProjects($governanceGroupId)/@ref
            return
                mpapi:getConceptMapList($governanceGroupId, $projectId, (), (), $searchTerms, $includebbr, $sort, $sortorder, $max, $resolve, $request?parameters)    
        )

    return
        <list artifact="MP" current="{count($result/*)}" total="{count($result/*)}" all="{count($result/*)}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements($result/*)
        }
        </list>
};

(:~ Update DECOR conceptMap. Expect array of parameter objects, each containing RFC 6902 compliant contents. Note: RestXQ does not do PATCH (yet), but that would be the preferred option.

{ "op": "[add|remove|replace]", "path": "/", "value": "[terminologyAssociation|identifierAssociation]" }

where
* op - add (object associations, tracking, event) or remove (object associations only) or replace (displayName, priority, type, tracking, assignment)
* path - / 
* value - terminologyAssociation|identifierAssociation object
@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $id                       - required. the id for the issue to update 
@param $request-body             - required. json body containing array of parameter objects each containing RFC 6902 compliant contents
@return issue structure including generated meta data
@since 2020-05-03
@see http://tools.ietf.org/html/rfc6902
:)
declare function mpapi:patchConceptMap($request as map(*)) {

    let $authmap                        := $request?user
    let $cmid                           := $request?parameters?id
    let $cmed                           := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $data                           := utillib:getBodyAsXml($request?body, 'parameters', ())
    
    (:let $s                      := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data) :)
    
    let $check                  :=
        if ($data) then () else (
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        )
    
    let $results                        := mpapi:patchConceptMap($authmap, string($cmid), $cmed, $data)
    return (
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple conceptMaps for id '", $cmid, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        )
        else (
            $results
        )
    )

};

(: Create a conceptMap, either empty or based on another conceptMap

@param $projectPrefix project to create this scenario in
@param $targetDate If true invokes effectiveDate of the new conceptMap and concepts as [DATE]T00:00:00. If false invokes date + time [DATE]T[TIME] 
@param $sourceId parameter denoting the id of a dataset to use as a basis for creating the new conceptMap
@param $sourceEffectiveDate parameter denoting the effectiveDate of a dataset to use as a basis for creating the new conceptMap",
@param $keepIds Only relevant if source dataset is specified. If true, the new conceptMap will keep the same ids for the new conceptMap, and only update the effectiveDate
@param $baseDatasetId Only relevant when a source dataset is specified and `keepIds` is false. This overrides the default base id for datasets in the project. The value SHALL match one of the projects base ids for datasets
@return (empty) dataset object as xml with json:array set on elements
:)
declare function mpapi:postConceptMap($request as map(*)) {

    let $authmap                := $request?user
    let $projectPrefix          := $request?parameters?prefix[not(. = '')]
    let $targetDate             := $request?parameters?targetDate = true()
    let $sourceId               := $request?parameters?sourceId
    let $data                   := utillib:getBodyAsXml($request?body, 'conceptMap', ())
    let $sourceEffectiveDate    := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?sourceEffectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?sourceEffectiveDate[string-length() gt 0]
        }
    let $refOnly                := $request?parameters?refOnly = true()
    let $keepIds                := $request?parameters?keepIds = true()
    let $baseId                 := $request?parameters?baseId
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($projectPrefix)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter prefix')
        else ()
    
    let $results                := mpapi:createConceptMap($authmap, $projectPrefix, $targetDate, $sourceId, $sourceEffectiveDate, $refOnly, $keepIds, $baseId, $data)
    
    return
        roaster:response(201, 
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
        
        )
};

(:~ Update DECOR conceptMap

@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $id                       - required. the id for the conceptMap to update 
@param $request-body             - required. json body containing new conceptMap structure
@return conceptMap structure including generated meta data
@since 2020-05-03
:)
declare function mpapi:putConceptMap($request as map(*)) {

    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $data                           := utillib:getBodyAsXml($request?body, 'conceptMap', ())
    let $deletelock                     := $request?parameters?deletelock = true()
    
    (:let $s                              := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data):) 
    
    let $check                          :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                          :=
        if (empty($data)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $return                         := mpapi:putConceptMap($authmap, $id, $effectiveDate, $data, $deletelock)
    return (
        roaster:response(200, $return)
    )

};

(:~ Returns a list of zero or more valuesets as listed in the terminology section. This function is useful e.g. to call from a ConceptMapIndex. Parameter id, name or prefix is required.

@param $projectPrefix    - optional. determines search scope. null is full server, pfx- limits scope to this project only
@param $projectVersion   - optional. if empty defaults to current version. if valued then the valueset will come explicitly from that archived project version which is expected to be a compiled version
@param $projectLanguage  - optional. defaults to project defaultLanguage or first available if multiple compilation exist 
@param $objectid         - optional. Identifier of the valueset to retrieve
@param $objected         - optional. null gets all versions, 'dynamic' gets the newest version based on id or name, yyyy-mm-ddThh:mm:ss gets this specific version
@param $max              - optional. Maximum number of results with minimum 1. Default is $cmapi:maxResults (50)
@param $resolve          - optional. Default = 'true' If true, resolves any references
@return List object with zero or more conceptMap
@since 2013-06-14
:)
declare function mpapi:getConceptMapList($governanceGroupId as xs:string?, $projectPrefix as xs:string, $projectVersion as xs:string?, $projectLanguage as xs:string?, $searchTerms as xs:string*, $includebbr as xs:boolean, $sort as xs:string?, $sortorder as xs:string?, $max as xs:integer?, $resolve as xs:boolean, $otherparams as map(*)?) as element(list) {
    
    let $governanceGroupId      := $governanceGroupId[not(. = '')]
    let $projectPrefix          := $projectPrefix[not(. = '')]
    let $projectVersion         := $projectVersion[not(. = '')]
    let $projectLanguage        := ($projectLanguage[not(. = '')], '*')[1]
    let $sort                   := $sort[string-length() gt 0]
    let $sortorder              := $sortorder[. = 'descending']
    
    let $cmid                   := $otherparams?id[not(. = '')]
    let $cmed                   := $otherparams?effectiveDate[not(. = '')]
    let $status                 := $otherparams?status[not(. = '')]
    let $vsid                   := $otherparams?valueSetId[not(. = '')]
    let $vsed                   := $otherparams?valueSetEffectiveDate[not(. = '')]
    let $k                      := 'valueSetId:source'
    let $vssourceid             := $otherparams($k)[not(. = '')]
    let $k                      := 'valueSetEffectiveDate:source'
    let $vssourceed             := $otherparams($k)[not(. = '')]
    let $k                      := 'valueSetId:target'
    let $vstargetid             := $otherparams($k)[not(. = '')]
    let $k                      := 'valueSetEffectiveDate:target'
    let $vstargeted             := $otherparams($k)[not(. = '')]
    
    let $csid                   := $otherparams?codeSystemId[not(. = '')]
    let $csed                   := $otherparams?codeSystemEffectiveDate[not(. = '')]
    let $k                      := 'codeSystemId:source'
    let $cssourceid             := $otherparams($k)[not(. = '')]
    let $k                      := 'codeSystemEffectiveDate:source'
    let $cssourceed             := $otherparams($k)[not(. = '')]
    let $k                      := 'codeSystemId:target'
    let $cstargetid             := $otherparams($k)[not(. = '')]
    let $k                      := 'codeSystemEffectiveDate:target'
    let $cstargeted             := $otherparams($k)[not(. = '')]
    
    let $startT                 := util:system-time()
    
    let $check                  :=
        if (empty($projectPrefix[not(. = '')])) then
            error($errors:BAD_REQUEST, 'Missing required parameter prefix')
        else ()
    
    let $decor                  := 
        if (utillib:isOid($projectPrefix)) then
            utillib:getDecorById($projectPrefix, $projectVersion, $projectLanguage)
        else (
            utillib:getDecorByPrefix($projectPrefix, $projectVersion, $projectLanguage)
        )
    let $projectPrefix          := ($decor/project/@prefix)[1]
    
    let $results                :=
        if (empty($cmid)) then 
            $decor//conceptMap 
        else 
        if ($cmed castable as xs:dateTime) then 
            $decor//conceptMap[@id = $cmid][@effectiveDate = $cmed]
        else (
            $decor//conceptMap[@id = $cmid]
        )
    let $results                := 
        if (empty($searchTerms)) then (
            $results
        )
        else (
            let $buildingBlockRepositories  := 
                if ($includebbr) then (
                    utillib:getBuildingBlockRepositories($decor, (), $utillib:strDecorServicesURL)
                )
                else (
                    $decor
                )
            let $luceneQuery                := utillib:getSimpleLuceneQuery($searchTerms, 'wildcard')
            let $luceneOptions              := utillib:getSimpleLuceneOptions() 

            for $ob in ($buildingBlockRepositories//conceptMap[@id = $searchTerms] | 
                        $buildingBlockRepositories//conceptMap[@id][ft:query(@name, $luceneQuery, $luceneOptions)] |
                        $buildingBlockRepositories//conceptMap[@id][ft:query(@displayName, $luceneQuery, $luceneOptions)])
            return
                <conceptMap>
                {
                    $ob/(@* except (@url|@ident)),
                    attribute url {($ob/ancestor::cacheme/@bbrurl, $utillib:strDecorServicesURL)[1]},
                    attribute ident {$ob/ancestor::decor/project/@prefix},
                    attribute cachedProject {exists($ob/ancestor::cacheme)},
                    $ob/node()
                }
                </conceptMap>
        )
    
    let $allcnt                 := count($results)
    
    let $conceptMapsByRef           :=
        if ($resolve and empty($projectVersion)) then
            for $cm in $results[@ref]
            let $id     := $cm/@ref
            let $cms    := mpapi:getConceptMap($projectPrefix, $projectVersion, $projectLanguage, $cm/@ref, (), true(), false())
            let $cmbyid := $cms[@effectiveDate = max($cms/xs:dateTime(@effectiveDate))]
            return (
                (: rewrite name and displayName based on latest target codeSystem. These sometimes run out of sync when the original changes its name :)
                element {name($cm)} {
                    $cm/@ref, 
                    attribute name {utillib:shortName(($cmbyid/@displayName, $cm/@displayName)[1])}, 
                    attribute displayName {($cmbyid/@displayName, $cm/@displayName, $cmbyid/@name, $cm/@name)[1]}
                } | $cms
            )
        else (
            $results[@ref]
        )
    
    let $results            := $results[@id] | $conceptMapsByRef
    
    let $results            :=
        if (empty($status)) then $results else (
            $results[@statusCode = $status]
        )
    let $results            :=
        if (empty($vsid)) then $results else (
            if (empty($vsed)) then 
                $results[sourceScope[@ref = $vsid] | targetScope[@ref = $vsed]]
            else (
                $results[sourceScope[@ref = $vsid][@flexibility = $vsed] | targetScope[@ref = $vsed][@flexibility = $vsed]]
            )
        )
    let $results            :=
        if (empty($vssourceid)) then $results else (
            if (empty($vssourceed)) then 
                $results[sourceScope[@ref = $vssourceid]]
            else (
                $results[sourceScope[@ref = $vssourceid][@flexibility = $vssourceed]]
            )
        )
    let $results            :=
        if (empty($vstargetid)) then $results else (
            if (empty($vstargeted)) then 
                $results[targetScope[@ref = $vstargetid]]
            else (
                $results[tagretScope[@ref = $vstargetid][@flexibility = $vstargeted]]
            )
        )
    let $results            :=
        if (empty($csid)) then $results else (
            if (empty($csed)) then 
                $results[group[source[@codeSystem = $csid] | target[@codeSystem = $csed]]]
            else (
                $results[group[source[@codeSystem = $csid][@codeSystemVersion = $csed] | target[@codeSystem = $csed][@codeSystemVersion = $csed]]]
            )
        )
    let $results            :=
        if (empty($cssourceid)) then $results else (
            if (empty($cssourceed)) then 
                $results[group[source[@codeSystem = $cssourceid]]]
            else (
                $results[group[source[@codeSystem = $cssourceid][@codeSystemVersion = $cssourceed]]]
            )
        )
    let $results            :=
        if (empty($cstargetid)) then $results else (
            if (empty($cstargeted)) then 
                $results[group[target[@codeSystem = $cstargetid]]]
            else (
                $results[group[target[@codeSystem = $cstargetid][@codeSystemVersion = $cstargeted]]]
            )
        )
        
    let $results            :=
        for $cm in $results
        let $id             := $cm/@id | $cm/@ref
        group by $id
        return (
            let $subversions    :=
                for $cmv in $cm
                order by $cmv/@effectiveDate descending
                return
                    <conceptMap uuid="{util:uuid()}">
                    {
                        $cmv/(@* except @uuid),
                        (: this element is not supported (yet?) :)
                        $cmv/classification
                    }
                    </conceptMap>
            let $latest         := ($subversions[@id], $subversions)[1]
            let $rproject       := $cm/ancestor::decor/project/@prefix
            return
            <conceptMap uuid="{util:uuid()}" id="{$id}">
            {
                $latest/(@* except (@uuid | @id | @ref | @project)),
                ($cm/@ref)[1],
                (: there is no attribute @project, but better safe than sorry :)
                (: wrong setting for governance group search: if (empty($governanceGroupId)) then $latest/@project else attribute project {$projectPrefix}, :)
                if (empty($governanceGroupId)) then $latest/@project else attribute project { $rproject[1] },
                $subversions
            }
            </conceptMap>
        )
    let $count              := count($results/conceptMap)
    let $max                := if ($max ge 1) then $max else $count
    
    (: handle sorting. somehow reverse() does not do what I expect :)
    let $results            :=
        switch ($sort)
        case 'displayName' return 
            if ($sortorder = 'descending') then 
                for $r in $results order by $r/lower-case(@displayName) descending return $r
            else (
                for $r in $results order by $r/lower-case(@displayName)            return $r
            )
        case 'name'        return 
            if ($sortorder = 'descending') then 
                for $r in $results order by $r/lower-case(@name) descending return $r
            else (
                for $r in $results order by $r/lower-case(@name)            return $r
            )
        default            return 
            if ($sortorder = 'descending') then 
                for $r in $results order by replace(replace($r/@id, '\.', '.0000000000'), '.0*([0-9]{9,})', '.$1') descending return $r
            else (
                for $r in $results order by replace(replace($r/@id, '\.', '.0000000000'), '.0*([0-9]{9,})', '.$1')            return $r
            )
    
    let $durationT := (util:system-time() - $startT) div xs:dayTimeDuration("PT0.001S")
    
    return
        <list artifact="MP" elapsed="{$durationT}" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" resolve="{$resolve}" project="{$projectPrefix}" lastModifiedDate="{current-dateTime()}">
        {
            subsequence($results, 1, $max)
        }
        </list>
};

(:~ Central logic for patching an existing dataset or transaction concept map

@param $authmap         - required. Map derived from token
@param $id              - required. DECOR concept/@id to update
@param $effectiveDate   - required. DECOR concept/@effectiveDate to update
@param $data            - required. DECOR concept xml element containing everything that should be in the updated concept
@return concept object as xml with json:array set on elements
:)
declare function mpapi:patchConceptMap($authmap as map(*), $id as xs:string, $effectiveDate as xs:string, $data as element(parameters)) {

    let $projectVersion             := ()
    let $projectLanguage            := ()
    let $id                         := $id[not(. = '')]
    let $effectiveDate              := $effectiveDate[not(. = '')]
    
    let $storedConceptMap           := $setlib:colDecorData//conceptMap[@id = $id][@effectiveDate = $effectiveDate]
    
    let $check                      :=
        if (count($storedConceptMap) = 1) then () else 
        if (count($storedConceptMap) gt 1) then
            error($errors:SERVER_ERROR, 'ConceptMap with id ' || $id || ' and effectiveDate ' || $effectiveDate || ' occurs multiple times. Please inform your server administrator.')
        else (
            error($errors:BAD_REQUEST, 'ConceptMap with id ' || $id || ' and effectiveDate ' || $effectiveDate || ' does not exist')
        )
    
    let $decor                      := $storedConceptMap/ancestor::decor
    let $projectPrefix              := $decor/project/@prefix
    
    let $lock                       := decorlib:getLocks($authmap, $id, $effectiveDate, ())
    let $lock                       := if ($lock) then $lock else decorlib:setLock($authmap, $id, $effectiveDate, false())
    
    let $check                      :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-TERMINOLOGY)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify terminology in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if ($storedConceptMap[@statusCode = $mpapi:STATUSCODES-FINAL]) then
            if ($data/parameter[@path = '/statusCode'][@op = 'replace'][not(@value = $mpapi:STATUSCODES-FINAL)]) then () else
            if ($data[count(parameter) = 1]/parameter[@path = '/statusCode']) then () else (
                error($errors:BAD_REQUEST, concat('ConceptMap cannot be patched while it has one of status: ', string-join($mpapi:STATUSCODES-FINAL, ', '), if ($storedConceptMap/@statusCode = 'pending') then 'You should switch back to status draft to edit.' else ()))
            )
        else ()
    let $check                  :=
        if ($lock) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have a lock for this conceptMap (anymore). Get a lock first.'))
        )
    
    let $check                  :=
        if ($data[parameter]) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall be an array of ''parameter''.')
        )
    let $unsupportedops         := $data/parameter[not(@op = ('add', 'replace', 'remove'))]
    let $check                  :=
        if ($unsupportedops) then
            error($errors:BAD_REQUEST, 'Submitted parameters shall have supported ''op'' value. Found ''' || string-join(distinct-values($unsupportedops/@op), ''', ''') || ''', expected ''add'', ''replace'', or ''remove''') 
        else ()
        
    let $check                  :=
        for $param in $data/parameter
        let $op         := $param/@op
        let $path       := $param/@path
        let $elmname    := substring-after($param/@path, '/')
        let $value      := ($param/@value, $param/value/*[name() = $elmname])[1]
        return
            switch ($path)
            case '/statusCode' return (
                if ($op = 'remove') then
                    'Parameter path ''' || $path || ''' does not support ' || $op
                else 
                if (utillib:isStatusChangeAllowable($storedConceptMap, $value)) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Supported are: ' || string-join(map:get($utillib:itemstatusmap, string($storedConceptMap/@statusCode)), ', ')
                )
            )
            case '/expirationDate'
            case '/officialReleaseDate' return (
                if ($op = 'remove') then () else 
                if ($value castable as xs:dateTime) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL be yyyy-mm-ddThh:mm:ss.'
                )
            )
            case '/displayName' return (
                if ($op = 'remove') then
                    'Parameter path ''' || $path || ''' does not support ' || $op
                else 
                if ($value[not(. = '')]) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL NOT be empty.'
                )
            )
            case '/canonicalUri'
            case '/versionLabel' return (
                if ($op = 'remove') then () else 
                if ($param/@value[not(. = '')]) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL NOT be empty.'
                )
            )
            case '/desc' 
            case '/purpose'
            case '/copyright' return (
                if ($param[count(value/*[name() = $elmname]) = 1]) then (
                    if ($param/value/*[name() = $elmname][@inherited]) then 
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Contents SHALL NOT be marked ''inherited''.'
                    else (),
                    if ($param/value/*[name() = $elmname][matches(@language, '[a-z]{2}-[A-Z]{2}')]) then 
                        if ($param[@op = 'remove'] | $param/value/*[name() = $elmname][.//text()[not(normalize-space() = '')]]) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A ' || $elmname || ' SHALL have contents.'
                        )
                    else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A ' || $elmname || ' SHALL have a language with pattern ll-CC.'
                    )
                )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one ' || $elmname || ' under value. Found ' || count($param/value/*[name() = $elmname]) 
                )
            )
            case '/publishingAuthority' return (
                if ($param[count(value/publishingAuthority) = 1]) then (
                    if ($param/value/publishingAuthority[@inherited]) then 
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Contents SHALL NOT be marked ''inherited''.'
                    else (),
                    if ($param/value/publishingAuthority[@id]) then
                        if ($param/value/publishingAuthority[utillib:isOid(@id)]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || ''' publishingAuthority.id SHALL be an OID is present. Found ' || string-join($param/value/publishingAuthority/@id, ', ')
                        )
                    else (),
                    if ($param/value/publishingAuthority[@name[not(. = '')]]) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || ''' publishingAuthority.name SHALL be a non empty string.'
                    ),
                    let $unsupportedtypes := $param/value/publishingAuthority/addrLine/@type[not(. = $mpapi:ADDRLINE-TYPE/enumeration/@value)]
                    return
                        if ($unsupportedtypes) then
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Publishing authority has unsupported addrLine.type value(s) ' || string-join($unsupportedops, ', ') || '. SHALL be one of: ' || string-join($mpapi:ADDRLINE-TYPE/enumeration/@value, ', ')
                        else ()
                ) else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' SHALL have a single publishingAuthority under value. Found ' || count($param/value/publishingAuthority)
                )
            )
            case '/jurisdiction' return (
                if ($param[count(value/*[name() = $elmname]) = 1]) then
                    if ($op = 'remove') then () else (
                        if ($param/value/*[name() = $elmname][@code[matches(., '^\S+$')]][utillib:isOid(@codeSystem)][empty(@canonicalUri) or @canonicalUri castable as xs:anyURI]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Input SHALL have code without whitespace, a codeSystem as valid OID, and optionally a canonicalUri as URI. Found: ' || string-join(for $att in $param/value/code/@* return name($att) || ': "' || $att || '" ', ' ')
                        )
                    )
                else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || '''. Input SHALL have exactly one ' || $elmname || ' under value. Found ' || count($param/value/*[name() = $elmname])
                )
            )
            case '/sourceScope'
            case '/targetScope' return (
                if ($param[count(value/*[name() = $elmname]) = 1]) then (
                    if ($op = 'remove') then
                        'Parameter ' || $op || ' not allowed for ''' || $path || ''''
                    else (
                        let $vsid       := $param/value/*/@ref
                        let $vsed       := $param/value/*/@flexibility
                        let $vsuri      := $param/value/*/@canonicalUri
                        let $valueSet   := 
                            if ($vsed castable as xs:dateTime) then (
                                $setlib:colDecorData//valueSet[@id = $vsid][@effectiveDate = $vsed] | 
                                $setlib:colDecorCache//valueSet[@id = $vsid][@effectiveDate = $vsed]
                            )[1]
                            else (
                                for $vs in  $setlib:colDecorData//valueSet[@id = $vsid] | 
                                            $setlib:colDecorCache//valueSet[@id = $vsid]
                                order by $vs/@effectiveDate descending
                                return $vs
                            )[1]
                        return
                            if (not(utillib:isOid($vsid))) then
                                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Input SHALL have ref as an OID on ' || $elmname || ' under value. Found: ' || $param/value/*[name() = $elmname]/@ref
                            else
                            if (empty($valueSet)) then
                                'Parameter ' || $op || ' not allowed for ''' || $path || '''. ' || $elmname || ' does not resolve to a value set. Found: ' || string-join(for $att in $param/value/*[name() = $elmname]/@* return name($att) || ': "' || $att || '" ', ' ')
                            else 
                            if (string-length($vsuri) = 0 or $valueSet[@canonicalUri = $vsuri]) then () else (
                                'Parameter ' || $op || ' not allowed for ''' || $path || '''. ' || $elmname || ' points to a valueSet that has a different canonicalUri than stated in the patch. Stated: ' || $vsuri || ' / found on valueSet: ''' || $valueSet/@canonicalUri || ''''
                            )
                    )
                )
                else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || '''. Input SHALL have exactly one ' || $elmname || ' under value. Found: ' || count($param/value/*[name() = $elmname])
                )
            )
            case '/group' return (
                if ($param[count(value/*[name() = $elmname]) = 1]) then (
                    if ($op = 'remove') then () else (
                        for $cs in $param/value/*[name() = $elmname]/source | $param/value/*[name() = $elmname]/target
                        let $csid       := $cs/@codeSystem
                        let $csed       := $cs/@codeSystemVersion
                        let $csuri      := $cs/@canonicalUri
                        let $canonical  := utillib:getCanonicalUriForOID('CodeSystem', $csid, $csed[. castable as xs:dateTime], $decor, ())
                        return
                            if (not(utillib:isOid($csid))) then
                                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Input SHALL have ' || name($cs) || '.codeSystem as an OID on ' || $elmname || ' under value. Found: ' || $csid
                            else
                            if (string-length($csuri) = 0 or $csuri = $canonical) then () else (
                                'Parameter ' || $op || ' not allowed for ''' || $path || '''. ' || $elmname || ' points to a codeSystem that has a different canonicalUri than stated in the patch. Stated: ' || $csuri || ' / found: ''' || $canonical || ''''
                            )
                        ,
                        let $preparedData   :=
                            element {name($storedConceptMap)}
                            {
                                $storedConceptMap/@*,
                                $storedConceptMap/(* except group),
                                utillib:prepareConceptMapGroupForUpdate($param/value/*[name() = $elmname])
                            }
                        let $r              := validation:jaxv-report($preparedData, $setlib:docDecorSchema)
                        return
                            if ($r/status='invalid') then
                                error($errors:BAD_REQUEST, 'Parameter ' || $op || ' not allowed for ''' || $path || '''. Contents are not schema compliant: ' || serialize($r))
                            else ()
                    )
                )
                else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || '''. Input SHALL have exactly one ' || $elmname || ' under value. Found: ' || count($param/value/*[name() = $elmname])
                )
            )
            default return (
                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Path value not supported'
            )
     let $check                 :=
        if (empty($check)) then () else (
            error($errors:BAD_REQUEST,  string-join ($check, ' '))
        )
    
    let $intention              := if ($storedConceptMap[@statusCode = 'final']) then 'patch' else 'version'
    let $update                 := histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-MAPPING, $projectPrefix, $intention, $storedConceptMap)
    
    let $update                 :=
        for $param in $data/parameter
        let $elmname  := substring-after($param/@path, '/')
        return
            switch ($param/@path)
            case '/statusCode'
            case '/displayName'
            case '/expirationDate'
            case '/officialReleaseDate'
            case '/canonicalUri'
            case '/versionLabel' return (
                let $attname  := substring-after($param/@path, '/')
                let $new      := attribute {$attname} {$param/@value}
                let $stored   := $storedConceptMap/@*[name() = $attname]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedConceptMap
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/desc'
            case '/purpose' return (
                (: only one per language :)
                let $new      := utillib:prepareFreeFormMarkupWithLanguageForUpdate($param/value/*[name() = $elmname])
                let $stored   := $storedConceptMap/*[name() = $elmname][@language = $param/value/*[name() = $elmname]/@language]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedConceptMap
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/publishingAuthority' return (
                (: only one possible :)
                let $new      := utillib:preparePublishingAuthorityForUpdate($param/value/publishingAuthority)
                let $stored   := $storedConceptMap/publishingAuthority
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedConceptMap
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/jurisdiction'
            return (
                let $attname  := substring-after($param/@path, '/')
                let $new      := utillib:prepareValueCodingTypeForUpdate($param/value/*[name() = $attname])
                let $stored   := $storedConceptMap/*[name() = $attname][@code = $new/@code][@codeSystem = $new/@codeSystem]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedConceptMap
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/copyright' return (
                (: only one per language :)
                let $new      := utillib:prepareFreeFormMarkupWithLanguageForUpdate($param/value/copyright)
                let $stored   := $storedConceptMap/copyright[@language = $param/value/copyright/@language]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedConceptMap
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/sourceScope'
            case '/targetScope' return (
                (: only one per language :)
                let $new      := utillib:prepareConceptMapScopeForUpdate($param/value/*[name() = $elmname])
                let $stored   := $storedConceptMap/*[name() = $elmname]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedConceptMap
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/group' return (
                (: only one per language :)
                let $new      := mpapi:getRawConceptMapGroup($param/value/group)
                let $stored   := $storedConceptMap/group[source/@codeSystem = $new/source/@codeSystem][target/@codeSystem = $new/target/@codeSystem]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedConceptMap
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            default return ( (: unknown path :) )
    
    (: after all the updates, the XML Schema order is likely off. Restore order :)
    let $preparedConceptMap     := utillib:prepareConceptMapForUpdate($storedConceptMap, $storedConceptMap)
    
    let $update                 := update replace $storedConceptMap with $preparedConceptMap
    let $update                 := update delete $lock
    
    let $result                 := mpapi:getConceptMap($projectPrefix, $projectVersion, $projectLanguage, $id, $effectiveDate, false(), true())
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

(: Central logic for creating a new conceptMap

@param $authmap         - required. Map derived from token
@return (empty) conceptMap object as xml with json:array set on elements
:)
declare function mpapi:createConceptMap($authmap as map(*), $projectPrefix as xs:string, $targetDate as xs:boolean, $sourceId as xs:string?, $sourceEffectiveDate as xs:string?, $refOnly as xs:boolean?, $keepIds as xs:boolean?, $baseId as xs:string?, $editedConceptMap as element(conceptMap)?) {

    let $decor                      := utillib:getDecorByPrefix($projectPrefix)
    let $projectLanguage            := $decor/project/@defaultLanguage
    (: if the user sent us a conceptMap with id, we should assume he intends to keep that id :)
    let $keepIds                    := if ($editedConceptMap/@id) then true() else $keepIds = true()
    (: if the user sent us a conceptMap with effectiveDate, we should assume he intends to keep that effectiveDate :)
    let $now                        := 
        if ($editedConceptMap/@effectiveDate) then substring($editedConceptMap/@effectiveDate, 1, 19) else 
        if ($targetDate) then substring(string(current-date()), 1, 10) || 'T00:00:00' else substring(string(current-dateTime()), 1, 19)
    
    let $check                      :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, 'Project with prefix ' || $projectPrefix || ' does not exist')
        )
    let $check                      :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-TERMINOLOGY)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify terminology in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                      :=
        if (empty($editedConceptMap/@id)) then () else (
            if (matches($editedConceptMap/@id, '^[0-2](\.(0|[1-9][0-9]*)){0,3}$')) then
                error($errors:BAD_REQUEST, 'Concept map id ''' || $editedConceptMap/@id || ''' is reserved. You cannot reuse a reserved identifier. See http://oid-info.com/get/' || $editedConceptMap/@id)
            else
            if (utillib:isOid($editedConceptMap/@id)) then () else (
                error($errors:BAD_REQUEST, 'Concept map id ''' || $editedConceptMap/@id || ''' SHALL be a valid OID')
            )
        )
    
    let $sourceConceptMap           := if (empty($sourceId)) then () else mpapi:getConceptMap($projectPrefix, (), (), $sourceId, $sourceEffectiveDate, false(), true())
    let $storedConceptMap           := 
        if ($keepIds and $editedConceptMap[@id]) then mpapi:getConceptMap((), (), (), $editedConceptMap/@id, $now, false(), true()) else ()
    
    let $check                      :=
        if ($refOnly) then
            if (empty($sourceId)) then
                error($errors:BAD_REQUEST, 'Parameter sourceId SHALL be provided if a value set reference is to be created')
            else
            if (empty($sourceConceptMap)) then
                if (empty($sourceEffectiveDate)) then
                    error($errors:BAD_REQUEST, 'Parameter sourceId ' || $sourceId || ' SHALL lead to a conceptMap in scope of this project')
                else (
                    error($errors:BAD_REQUEST, 'Parameters sourceId ' || $sourceId || ' and sourceEffectiveDate ' || $sourceEffectiveDate || ' SHALL lead to a conceptMap in scope of this project')
                )
            else ()
        else
        if (empty($editedConceptMap) and empty($sourceId)) then 
            error($errors:BAD_REQUEST, 'ConceptMap input data or a sourceId SHALL be provided')
        else
        if ($editedConceptMap) then
            if ($storedConceptMap) then
                error($errors:BAD_REQUEST, 'Cannot create new conceptMap. The input conceptMap with id ' || $editedConceptMap/@id || ' and effectiveDate ' || $now || ' already exists.')
            else ()
        else (
            if (empty($sourceConceptMap)) then 
                error($errors:BAD_REQUEST, 'Cannot create new conceptMap. Source valueset based on id ' || $sourceId || ' and effectiveDate ' || $sourceEffectiveDate || ' does not exist.')
            else  ()
        )
    let $check                      :=
        if ($now castable as xs:dateTime) then () else (
            error($errors:BAD_REQUEST, 'Cannot create new conceptMap. The provided effectiveDate ' || $now || ' is not a valid xs:dateTime. Expected yyyy-mm-ddThh:mm:ss.')
        )
    
    let $editedConceptMap           := ($editedConceptMap, $sourceConceptMap)[1]
    (: decorlib:getNextAvailableIdP() returns <next base="1.2" max="2" next="{$max + 1}" id="1.2.3" type="DS"/> :)
    let $newConceptMapId            := if ($keepIds) then $editedConceptMap/@id else (decorlib:getNextAvailableIdP($decor, $decorlib:OBJECTTYPE-MAPPING, $baseId)/@id)
    
    let $check                      :=
        if (mpapi:getConceptMap($projectPrefix, (), (), $newConceptMapId, $now, false(), true())) then
            error($errors:BAD_REQUEST, 'Cannot create new conceptMap. The to-be-created conceptMap with id ' || $newConceptMapId || ' and effectiveDate ' || $now || ' already exists.')
        else ()
    
    let $baseConceptMap             := 
        <conceptMap>
        {
            attribute id {$newConceptMapId} ,
            $editedConceptMap/@name[string-length()>0] ,
            $editedConceptMap/@displayName[string-length()>0] ,
            attribute effectiveDate {$now} ,
            attribute statusCode {"draft"} ,
            $editedConceptMap/@versionLabel[string-length()>0] ,
            $editedConceptMap/@expirationDate[string-length()>0] ,
            $editedConceptMap/@officialReleaseDate[string-length()>0] ,
            $editedConceptMap/@experimental[string-length()>0] ,
            $editedConceptMap/@canonicalUri[string-length()>0] ,
            $editedConceptMap/@lastModifiedDate[string-length()>0]
        }
        </conceptMap>
    let $newConceptMap                  := 
        if ($refOnly) then
            <conceptMap ref="{$sourceConceptMap/@id}" name="{utillib:shortName($sourceConceptMap/@displayName)}" displayName="{$sourceConceptMap/@displayName}"/>
        else (
            let $n            := utillib:prepareConceptMapForUpdate($editedConceptMap, $baseConceptMap)
            let $consistency  := $n//(source | target)[@codeSystem = ($n/sourceScope/@ref | $n/targetScope/@ref)]   
            let $report       := validation:jaxv(<terminology>{$n}</terminology>, $setlib:docDecorSchema)
            return
                if ($consistency) then
                    error($errors:BAD_REQUEST, 'Cannot create new conceptMap. The to-be-created conceptMap contains the same identifiers at value set and code system level. This is logically impossible: ' || string-join(distinct-values($consistency/@codeSystem), ', '))
                else
                if ($report) then $n else (
                    error($errors:BAD_REQUEST, 'Cannot create new conceptMap. The to-be-created conceptMap is not valid against ' || substring-after(util:collection-name($setlib:docDecorSchema), 'db/apps') || '/' || util:document-name($setlib:docDecorSchema) || ' on this server.')
                )
        )
    
    (: prepare sub root elements if not existent :)
    let $conceptMapUpdate               :=
        if (not($decor/terminology) and $decor/codedConcepts) then
            update insert <terminology/> following $decor/codedConcepts
        else if (not($decor/terminology) and not($decor/codedConcepts)) then
            update insert <terminology/> following $decor/ids
        else ()
        (: now update the value set :)
    let $conceptMapUpdate               :=
        if ($refOnly and $decor//conceptMap[@ref = $newConceptMap/@ref]) then
            (: this could update the name/displayName if the source thing was updated since adding it previously :)
            update replace $decor//conceptMap[@ref = $newConceptMap/@ref] with $newConceptMap
        else (
            update insert $newConceptMap into $decor/terminology
        )
    
    (: return the regular conceptMap that was created, or the requested version of the reference that was created, or the latest version of the reference that was created :)
    let $result                         := mpapi:getConceptMap($projectPrefix, (), (), ($newConceptMap/@id | $newConceptMap/@ref), ($newConceptMap/@effectiveDate, $sourceEffectiveDate, 'dynamic')[1], false(), true())
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

(:~ Central logic for updating an existing conceptMap

@param $authmap         - required. Map derived from token
@param $id              - required. DECOR conceptMap/@id to update
@param $request-body    - required. DECOR conceptMap xml element containing everything that should be in the updated conceptMap
@return conceptMap object as xml with json:array set on elements
:)
declare function mpapi:putConceptMap($authmap as map(*), $id as xs:string, $effectiveDate as xs:string, $data as element(), $deletelock as xs:boolean) as element(conceptMap) {

    let $storedConceptMap       := $setlib:colDecorData//conceptMap[@id = $id][@effectiveDate = $effectiveDate] 
    let $decor                  := $storedConceptMap/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix
    let $projectVersion         := ()
    let $projectLanguage        := $decor/project/@defaultLanguage
    
    let $lock                   := decorlib:getLocks($authmap, $id, $effectiveDate, ())
    let $lock                   := if ($lock) then $lock else decorlib:setLock($authmap, $id, $effectiveDate, false())
    
    let $check                  :=
        if ($storedConceptMap) then () else (
            error($errors:BAD_REQUEST, 'ConceptMap with id ''' || $id || ''' and effectiveDate ''' || $effectiveDate || ' does not exist')
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-TERMINOLOGY)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify conceptMaps in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if ($lock) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have a lock for this conceptMap (anymore). Get a lock first.'))
        )    
    let $check                  :=
        if ($storedConceptMap[@statusCode = $mpapi:STATUSCODES-FINAL]) then
            error($errors:BAD_REQUEST, concat('ConceptMap cannot be updated while it has one of status: ', string-join($mpapi:STATUSCODES-FINAL, ', '), if ($storedConceptMap/@statusCode = 'pending') then 'You should switch back to status draft to edit.' else ()))
        else ()
    let $check                  :=
        if ($data[@id = $id] | $data[empty(@id)]) then () else (
            error($errors:BAD_REQUEST, concat('Submitted data shall have no conceptMap id or the same conceptMap id as the conceptMap id ''', $id, ''' used for updating. Found in request body: ', ($data/@id, 'null')[1]))
        )
    let $check                  :=
        if ($data[@effectiveDate = $effectiveDate] | $data[empty(@effectiveDate)]) then () else (
            error($errors:BAD_REQUEST, concat('Submitted data shall have no conceptMap effectiveDate or the same conceptMap effectiveDate as the conceptMap effectiveDate ''', $effectiveDate, ''' used for updating. Found in request body: ', ($data/@effectiveDate, 'null')[1]))
        )
    
    let $baseConceptMap         := 
        <conceptMap>
        {
            attribute id {$id} ,
            $data/@name[string-length()>0] ,
            $data/@displayName[string-length()>0] ,
            attribute effectiveDate {$effectiveDate} ,
            attribute statusCode {"draft"} ,
            $data/@versionLabel[string-length()>0] ,
            $data/@expirationDate[. castable as xs:dateTime] ,
            $data/@officialReleaseDate[. castable as xs:dateTime] ,
            $data/@experimental[. = ('true', 'false')] ,
            $data/@canonicalUri[string-length()>0],
            $data/@lastModifiedDate[string-length()>0]
        }
        </conceptMap>
    let $newConceptMap          := utillib:prepareConceptMapForUpdate($data, $storedConceptMap)
    
    let $r                      := validation:jaxv-report($newConceptMap, $setlib:docDecorSchema)
    let $check                  :=
        if ($r/status='invalid') then
            error($errors:BAD_REQUEST, 'Contents are not schema compliant: ' || serialize($r))
        else ()
    let $check                  :=
        for $scope in $newConceptMap/sourceScope | $newConceptMap/targetScope
        let $vsid       := $scope/@ref
        let $vsed       := $scope/@flexibility
        let $vsuri      := $scope/@canonicalUri
        let $valueSet   := 
            if ($vsed castable as xs:dateTime) then (
                    $setlib:colDecorData//valueSet[@id = $vsid][@effectiveDate = $vsed] | 
                    $setlib:colDecorCache//valueSet[@id = $vsid][@effectiveDate = $vsed]
            )[1]
            else (
                for $vs in  $setlib:colDecorData//valueSet[@id = $vsid] | 
                            $setlib:colDecorCache//valueSet[@id = $vsid]
                order by $vs/@effectiveDate descending
                return $vs
            )[1]
        return
            if (not(utillib:isOid($vsid))) then
                error($errors:BAD_REQUEST, name($scope) || ' SHALL have ref as an OID. Found: ' || $vsid)
            else
            if (empty($valueSet)) then
                error($errors:BAD_REQUEST, name($scope) || ' SHALL resolve to a value set. Found: ' || string-join(for $att in $scope/@* return name($att) || ': "' || $att || '" ', ' '))
            else 
            if (string-length($vsuri) = 0 or $valueSet[@canonicalUri = $vsuri]) then () else (
                error($errors:BAD_REQUEST, name($scope) || ' SHALL leave canonicalUri empty or it SHALL be the same as on the valueSet. Stated: ' || $vsuri || ' / found on valueSet: ''' || $valueSet/@canonicalUri || '''')
            )
    let $check                  :=
        for $scope in $newConceptMap/group/source | $newConceptMap/group/target
        let $csid       := $scope/@codeSystem
        let $csed       := $scope/@codeSystemVersion
        let $csuri      := $scope/@canonicalUri
        let $canonical  := utillib:getCanonicalUriForOID('CodeSystem', $csid, $csed[. castable as xs:dateTime], $decor, ())
        return
            if (not(utillib:isOid($csid))) then
                error($errors:BAD_REQUEST, 'group.' || name($scope) || ' SHALL have ref as an OID. Found: ' || $csid)
            else
            if (string-length($csuri) = 0 or $csuri = $canonical) then () else (
                error($errors:BAD_REQUEST, 'group.' || name($scope) || ' SHALL leave canonicalUri empty or it SHALL be the same as on the codeSystem (' || $csid || '). Stated: ' || $csuri || ' / found on codeSystem: ''' || $canonical || '''')
            )
    
    (: save history:)
    let $intention                      := if ($storedConceptMap[@statusCode = 'final']) then 'patch' else 'version'
    let $history                        := histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-MAPPING, $projectPrefix, $intention, $storedConceptMap)
    
    (: now update the value set :)
    let $conceptMapUpdate       := update replace $storedConceptMap with $newConceptMap
    let $delete                 := update delete $storedConceptMap//@json:array
    let $deleteLock             := if ($deletelock) then update delete $lock else ()
    
    let $result                 := mpapi:getRawConceptMap($newConceptMap, $projectLanguage, $projectPrefix, $projectVersion, false())
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

declare %private function mpapi:addConceptMapRef($decor as element(), $repoPrefix as xs:string, $repoUrl as xs:string, $conceptMapId as xs:string, $conceptMapName as xs:string, $conceptMapDisplayName as xs:string) as item()* {
    
    let $conceptMapRefElm     := <conceptMap ref="{$conceptMapId}" name="{$conceptMapName}" displayName="{$conceptMapDisplayName}"/>
    let $buildingBlockElm   := <buildingBlockRepository url="{$repoUrl}" ident="{$repoPrefix}"/>
    
    let $addConceptMapRef      :=
        if ($decor//conceptMap[@id = $conceptMapId] | $decor//conceptMap[@ref = $conceptMapId]) then () else (
            let $dummy1 := update insert $conceptMapRefElm following $decor/terminology/*[last()]
            let $dummy2 := 
                if ($decor/project/buildingBlockRepository[@url=$buildingBlockElm/@url][@ident=$buildingBlockElm/@ident][empty(@format)] |
                    $decor/project/buildingBlockRepository[@url=$buildingBlockElm/@url][@ident=$buildingBlockElm/@ident][@format='decor']) then ('false') else (
                    update insert $buildingBlockElm following $decor/project/(author|reference|restURI|defaultElementNamespace|contact)[last()]
                )
            
            return 
                if ($dummy2='false') then 'ref' else 'ref-and-bbr'
        )
    
    return ()
};

(:~ Get conceptMap
@param $doContents If we only need the conceptMap for the list we can forego the contents
:)
declare function mpapi:getConceptMap($projectPrefix as xs:string*, $projectVersion as xs:string*, $projectLanguage as xs:string?, $id as xs:string, $effectiveDate as xs:string?, $withversions as xs:boolean, $doContents as xs:boolean) as element(conceptMap)* {
    let $id                     := $id[not(. = '')]
    let $effectiveDate          := $effectiveDate[not(. = '')]
    let $projectPrefix          := $projectPrefix[not(. = '')]
    let $projectVersion         := $projectVersion[not(. = '')]
    let $serialize              := false()
    
    let $results                :=
        if (empty($projectPrefix)) then (
            (: mpapi:getConceptMapById($id, $flexibility, false()) will not return all versions, only latest. Don't want to break that and cause issues so we do it here :)
            let $conceptMaps      := $setlib:colDecorData//conceptMap[@id = $id] | $setlib:colDecorCache//conceptMap[@id = $id]
            let $conceptMaps      :=
                if (empty($conceptMaps)) then
                    $setlib:colDecorData//conceptMap[@ref = $id] | $setlib:colDecorCache//conceptMap[@ref = $id]
                else
                if ($withversions) then $conceptMaps else
                if ($effectiveDate castable as xs:dateTime) then (
                    $conceptMaps[@effectiveDate = $effectiveDate]
                ) else (
                    $conceptMaps[@effectiveDate = max($conceptMaps/xs:dateTime(@effectiveDate))][1]
                )
            let $projectPrefix      := $conceptMaps[1]/ancestor::decor/project/@prefix
            let $projectLanguage    := $conceptMaps[1]/ancestor::decor/project/@defaultLanguage
            let $conceptMaps      :=
                if ($conceptMaps) then 
                    <repository>
                    {
                        $conceptMaps[1]/ancestor::decor/project/(@* except (@url|@ident)),
                        attribute url {$utillib:strDecorServicesURL},
                        attribute ident {$projectPrefix},
                        mpapi:getRawConceptMap($conceptMaps[1], $projectLanguage, $projectPrefix, (), $serialize)
                    }
                    </repository>
                else ()
            return
                $conceptMaps/conceptMap
        )
        else (
            mpapi:getConceptMapById($id, $effectiveDate, $projectPrefix[1], $projectVersion[1], $serialize)/(descendant-or-self::conceptMap[@id][@effectiveDate], descendant-or-self::conceptMap[@ref])[1] 
        )
    let $results                        :=
        if (empty($results) and not(empty($projectPrefix))) then
            if (utillib:isOid($projectPrefix)) then (
                let $decor              := utillib:getDecorById($projectPrefix, $projectVersion, $projectLanguage)
                return
                    ($decor//conceptMap[@ref = $id])[last()]
            ) 
            else (
                let $decor              := utillib:getDecorByPrefix($projectPrefix, $projectVersion, $projectLanguage)
                return
                    ($decor//conceptMap[@ref = $id])[last()]
            )
        else (
            $results
        )
    
    
    for $cm in $results
    let $id             := $cm/@id | $cm/@ref
    let $ed             := $cm/@effectiveDate
    let $projectPrefix  := ($cm/@ident, $cm/parent::*/@ident, $cm/ancestor::*/@bbrident, $cm/ancestor::decor/project/@prefix)[1]
    order by $id, $ed descending
    return
        element {name($cm)} {
            $cm/@*,
            if ($cm/@url) then () else attribute url {($cm/parent::*/@url, $cm/ancestor::*/@bbrurl, $utillib:strDecorServicesURL)[1]},
            if ($cm/@ident) then () else attribute ident {$projectPrefix}
            ,
            if ($doContents) then (
                $cm/(* except group)
                ,
                (: we return just a count of issues and leave it up to the calling party to get those if required :)
                <issueAssociation count="{count($setlib:colDecorData//issue/object[@id = $cm/@id][@effectiveDate = $cm/@effectiveDate] | $setlib:colDecorData//issue/object[@id = $cm/@ref])}"/>
                ,
                $cm/group
            )
            else (
                (:$cm/classification:)
            )
        }
};

(:~ Return zero or more conceptMaps as-is wrapped in a &lt;return/&gt; element, and subsequently inside a &lt;repository&gt; element. This repository element
holds at least the attribute @ident with the originating project prefix and optionally the attribute @url with the repository URL in case of an external
repository. Id based references can match both conceptMap/@id and conceptMap/@ref. The latter is resolved. Note that duplicate conceptMap matches may be 
returned. Example output:
<return>
  <repository url="http://art-decor.org/decor/services/" ident="ad2bbr-">
    <conceptMap id="2.16.840.1.113883.1.11.10282" displayName="ParticipationSignature" effectiveDate="2013-03-11T00:00:00" statusCode="final" versionLabel="DEFN=UV=VO=1206-20130318">
    ...
    </conceptMap>
  </repository>
</return>

@param $id           - required. Identifier of the conceptMap to retrieve
@param $flexibility  - optional. null gets all versions, 'dynamic' gets the newest version based on id, yyyy-mm-ddThh:mm:ss gets this specific version
@return Zero concept maps in case no matches are found, one if only one exists or if a specific version was requested, or more if more versions exist and no specific version was requested
@since 2013-06-14
:)
declare function mpapi:getConceptMapById($id as xs:string, $flexibility as xs:string?, $serialize as xs:boolean) as element(return) {
    let $conceptMaps  := $setlib:colDecorData//conceptMap[@id = $id] | $setlib:colDecorCache//conceptMap[@id = $id]
    let $conceptMaps  :=
        if ($flexibility castable as xs:dateTime) then (
            $conceptMaps[@effectiveDate = $flexibility]
        ) else (
            $conceptMaps[@effectiveDate = max($conceptMaps/xs:dateTime(@effectiveDate))][1]
        )
    return
    <return>
    {
        for $repository in $conceptMaps
        let $prefix    := $repository/ancestor::decor/project/@prefix
        let $urlident  := concat($repository/ancestor::cacheme/@bbrurl, $prefix)
        group by $urlident
        return
            <project ident="{$prefix}">
            {
                if ($repository[ancestor::cacheme]) then
                    attribute url {$repository[1]/ancestor::cacheme/@bbrurl}
                else (),
                $repository[1]/ancestor::decor/project/@defaultLanguage
                ,
                for $conceptMap in $repository
                let $ideff  := concat($conceptMap/@id, $conceptMap/@ref, $conceptMap/@effectiveDate)
                group by $ideff
                order by $conceptMap[1]/@effectiveDate descending
                return 
                    mpapi:getRawConceptMap($conceptMap[1], $repository[1]/ancestor::decor/project/@defaultLanguage, $prefix, (), $serialize)
            }
            </project>
    }
    </return>
};

(:~ See mpapi:getConceptMapById ($id as xs:string, $flexibility as xs:string?, $prefix as xs:string, $version as xs:string?, $serialize as xs:boolean) for documentation

@param $id           - required. Identifier of the conceptMap to retrieve
@param $flexibility  - optional. null gets all versions, 'dynamic' gets the newest version based on id, yyyy-mm-ddThh:mm:ss gets this specific version
@param $prefix       - required. determines search scope. pfx- limits scope to this project only
@return Zero value sets in case no matches are found, one if only one exists or if a specific version was requested, or more if more versions exist and no specific version was requested
@since 2013-06-14
:)
declare function mpapi:getConceptMapById($id as xs:string, $flexibility as xs:string?, $prefix as xs:string, $serialize as xs:boolean) as element(return) {
    mpapi:getConceptMapById($id, $flexibility, $prefix, (), $serialize)
};

(:~ Return zero or more conceptMaps as-is wrapped in a &lt;return/&gt; element, and subsequently inside a &lt;repository&gt; element. This repository element
holds at least the attribute @ident with the originating project prefix and optionally the attribute @url with the repository URL in case of an external
repository. Id based references can match both conceptMap/@id and conceptMap/@ref. The latter is resolved. Note that duplicate conceptMap matches may be 
returned. Example output for conceptMap/@ref:<br/>

<return>
  <project ident="epsos-">
    <conceptMap ref="2.16.840.1.113883.1.11.10282" displayName="ParticipationSignature">
    ...
    </conceptMap>
  </project>
  <repository url="http://art-decor.org/decor/services/" ident="ad2bbr-" referencedFrom="epsos-">
    <conceptMap id="2.16.840.1.113883.1.11.10282" displayName="ParticipationSignature" effectiveDate="2013-03-11T00:00:00" statusCode="final" versionLabel="DEFN=UV=VO=1206-20130318">
    ...
    </conceptMap>
  </repository>
</return>
   
@param $id           - required. Identifier of the conceptMap to retrieve
@param $flexibility  - optional. null gets all versions, 'dynamic' gets the newest version based on id (regardless of name), yyyy-mm-ddThh:mm:ss gets this specific version
@param $prefix       - required. determines search scope. pfx- limits scope to this project only
@param $version      - optional. if empty defaults to current version. if valued then the conceptMap will come explicitly from that archived project version which is expected to be a compiled version
@return Zero value sets in case no matches are found, one if only one exists or if a specific version was requested, or more if more versions exist and no specific version was requested
@since 2013-06-14
:)
declare function mpapi:getConceptMapById($id as xs:string, $flexibility as xs:string?, $prefix as xs:string, $version as xs:string?, $serialize as xs:boolean) as element(return) {
let $argumentCheck              :=
    if (string-length($id)=0) then
        error(xs:QName('error:NotEnoughArguments'),'Argument id is required')
    else if (string-length($prefix)=0) then 
        error(xs:QName('error:NotEnoughArguments'),'Argument prefix is required')
    else ()

let $internalrepositories       := utillib:getDecorByPrefix($prefix, $version, ())[1]

(:let $internalconceptMaps          := $internalrepositories//conceptMap[@id = $id] | $internalrepositories//conceptMap[@ref = $id]:)
let $internalconceptMaps          :=
    if (empty($version)) then
        $setlib:colDecorData//conceptMap[@id = $id][ancestor::decor/project/@prefix = $prefix] | $setlib:colDecorData//conceptMap[@ref = $id][ancestor::decor/project/@prefix = $prefix]
    else (
        $setlib:colDecorVersion//decor[@versionDate = $version][project/@prefix = $prefix][1]/terminology/conceptMap[@id = $id] | $setlib:colDecorVersion//decor[@versionDate = $version][project/@prefix = $prefix][1]/terminology/conceptMap[@ref = $id]
        (:
        (\: get all conceptMaps matching by id or ref from the terminology section of the first compilation hit because they are also compiled into datasets :\)
        let $cmset  := 
            for $cm in $setlib:colDecorVersion//conceptMap[@id = $id] | $setlib:colDecorVersion//conceptMap[@ref = $id]
            let $cmv := $cm/ancestor::decor/@versionDate
            let $cmp := $cm/ancestor::decor/project/@prefix
            group by $cmv, $cmp
            return
                if ($cmv = $version and $cmp = $prefix) then $cm else ()
        let $cmfl   := string(($cmset/ancestor::decor/@language)[1])
        return
            for $cm in $cmset
            return
                if ($cm[ancestor::terminology][ancestor::decor/@language = $cmfl]) then $cm else ()
        :)
    )

let $repositoryConceptMapLists    :=
    <repositoryConceptMapLists>
    {
        (:  don't go looking in repositories when this is an archived project version. the project should be compiled already and 
            be self contained. Repositories in their current state would give a false picture of the status quo when the project 
            was archived.
        :)
        if (empty($version)) then
            let $buildingBlockRepositories  := $internalrepositories/project/buildingBlockRepository[empty(@format)] | 
                                               $internalrepositories/project/buildingBlockRepository[@format='decor']
            (:this is the starting point for the list of servers we already visited to avoid circular reference problems:)
            (:let $bbrList                    := <buildingBlockRepository url="{$utillib:strDecorServicesURL}" ident="{$prefix}"/>:)
            return
                mpapi:getConceptMapByIdFromBBR($prefix, $id, $prefix, $buildingBlockRepositories, (), (), ())/descendant-or-self::*[conceptMap[@id]]
        else ()
    }
    {
        (:from the requested project, return conceptMap/(@id and @ref):)
        (: when retrieving value sets from a compiled project, the @url/@ident they came from are on the conceptMap element
           reinstate that info on the repositoryConceptMapList element so downstream logic works as if it really came from 
           the repository again.
        :)
        for $repository in $internalrepositories
        for $conceptMaps in $internalconceptMaps
        let $source := concat($conceptMaps/@url,$conceptMaps/@ident)
        group by $source
        return
            if (string-length($conceptMaps[1]/@url)=0) then
                <repositoryConceptMapList ident="{$repository/project/@prefix}">
                {
                    $conceptMaps
                }
                </repositoryConceptMapList>
            else (
                <repositoryConceptMapList url="{$conceptMaps[1]/@url}" ident="{$conceptMaps[1]/@ident}" referencedFrom="{$prefix}">
                {
                    for $conceptMap in $conceptMaps
                    return
                        <conceptMap>{$conceptMap/(@* except (@url|@ident|@referencedFrom)), $conceptMap/node()}</conceptMap>
                }
                </repositoryConceptMapList>
            )
    }
    </repositoryConceptMapLists>

let $max                        :=
    if ($flexibility castable as xs:dateTime) then $flexibility else 
    if (empty($flexibility)) then () else (
        max($repositoryConceptMapLists//conceptMap/xs:dateTime(@effectiveDate))
    )

return
    <return>
    {
        if (empty($max)) then
            for $segment in $repositoryConceptMapLists/*
            let $elmname := if ($segment[empty(@url)][@ident=$prefix]) then 'project' else 'repository'
            return
                element {$elmname} {
                    $segment/@*,
                    for $conceptMap in $segment/conceptMap
                    let $ideff  := concat($conceptMap/@id, $conceptMap/@ref, $conceptMap/@effectiveDate)
                    group by $ideff
                    order by $conceptMap[1]/@effectiveDate descending
                    return (
                        mpapi:getRawConceptMap($conceptMap[1], (), $prefix, $version, $serialize)
                    )
                }
        else (
            for $segment in $repositoryConceptMapLists/*[conceptMap[@effectiveDate = $max]]
            let $elmname := if ($segment[empty(@url)][@ident=$prefix]) then 'project' else 'repository'
            return
                element {$elmname} {
                    $segment/@*, 
                    for $conceptMap in $segment/conceptMap[@ref] | $segment/conceptMap[@effectiveDate = $max]
                    let $ideff  := concat($conceptMap/@id, $conceptMap/@ref, $conceptMap/@effectiveDate)
                    group by $ideff
                    order by $conceptMap[1]/@effectiveDate descending
                    return (
                        mpapi:getRawConceptMap($conceptMap[1], (), $prefix, $version, $serialize)
                    )
                }
        )
    }
    </return>
};

(:~ Look for conceptMap[@id] and recurse if conceptMap[@ref] is returned based on the buildingBlockRepositories in the project that returned it.
If we get a conceptMap[@ref] from an external repository (through RetrieveConceptMap), then tough luck, nothing can help us. The returned 
data is a nested repositoryConceptMapList element allowing you to see the full trail should you need that. Includes duplicate protection 
so every project is checked once only.

Example below reads:
- We checked hwg- and found BBR hg-
- We checked hg- and found BBR nictz2bbr-
- We checked nictiz2bbr- and found the requested conceptMap

<repositoryConceptMapList url="http://decor.nictiz.nl/decor/services/" ident="hg-" referencedFrom="hwg-">
  <repositoryConceptMapList url="http://decor.nictiz.nl/decor/services/" ident="nictiz2bbr-" referencedFrom="hg-">
    <conceptMap id="2.16.840.1.113883.2.4.3.11.60.1.11.2" name="RoleCodeNLZorgverlenertypen" displayName="RoleCodeNL - zorgverlenertype (personen)" effectiveDate="2011-10-01T00:00:00" statusCode="final">
    ...
    </conceptMap>
  </repositoryConceptMapList>
</repositoryConceptMapList>
:)
declare %private function mpapi:getConceptMapByIdFromBBR($basePrefix as xs:string, $id as xs:string, $prefix as xs:string, $externalrepositorylist as element()*, $bbrmap as map(*)?, $localbyid as element(conceptMap)*, $localbyref as element(conceptMap)*) as element()* {

let $newmap                 := 
    map:merge(
        for $bbr in $externalrepositorylist return map:entry(concat($bbr/@ident, $bbr/@url), '')
    )

let $conceptMapsById          := if ($localbyid) then $localbyid else $setlib:colDecorData//conceptMap[@id = $id]
let $conceptMapsByRef         := if ($localbyref) then $localbyref else $setlib:colDecorData//conceptMap[@ref = $id]

let $return                 := 
    for $repository in $externalrepositorylist
    let $repourl                := $repository/@url
    let $repoident              := $repository/@ident
    let $hasBeenProcessedBefore := 
        if (empty($bbrmap)) then false() else (
            map:contains($bbrmap, concat($repoident, $repourl)) or map:contains($bbrmap, concat($basePrefix, $utillib:strDecorServicesURL))
        )
    (:let $newmap                 :=
        if (empty($bbrmap)) then $newmap else map:merge(($bbrmap, $newmap)):)
    (: map:merge does not exist in eXist 2.2 :)
    let $newmap                 :=
        if (empty($bbrmap)) then $newmap else map:merge((
            for $k in map:keys($bbrmap) return map:entry($k, ''),
            for $k in map:keys($newmap) return if (map:contains($bbrmap, $k)) then () else map:entry($k, '')
        ))
    return
        if ($hasBeenProcessedBefore) then () else (
            <repositoryConceptMapList url="{$repourl}" ident="{$repoident}" referencedFrom="{$prefix}">
            {
                (: if this buildingBlockRepository resolves to our own server, then get it directly from the db. :)
                if ($repository[@url = $utillib:strDecorServicesURL]) then (
                    let $conceptMaps          := $conceptMapsById[ancestor::decor/project[@prefix = $repoident]]
                    let $conceptMapsRef       := $conceptMapsByRef[ancestor::decor/project[@prefix = $repoident]]
                    
                    let $project            := ($conceptMaps/ancestor::decor | $conceptMapsRef/ancestor::decor)[1]
                    let $bbrs               :=
                        $project//buildingBlockRepository[empty(@format)] |
                        $project//buildingBlockRepository[@format='decor']
                    return 
                    if ($project) then (
                        attribute projecttype {'local'},
                        $conceptMaps,
                        if (not($conceptMaps) or $conceptMapsRef) then (
                            mpapi:getConceptMapByIdFromBBR($basePrefix, $id, $repoident, $bbrs, $newmap, $conceptMapsById, $conceptMapsByRef)
                        ) else ()
                    ) else ()
                )
                (: check cache first and do a server call as last resort. :)
                else (
                    let $cachedProject          := $setlib:colDecorCache//cacheme[@bbrurl = $repourl][@bbrident = $repoident]
                    let $cachedConceptMaps        := $cachedProject//conceptMap[@id = $id] | $cachedProject//conceptMap[@ref = $id]
                    let $bbrs               :=
                        $cachedProject//buildingBlockRepository[empty(@format)] |
                        $cachedProject//buildingBlockRepository[@format='decor']
                    return
                    if ($cachedProject) then (
                        attribute projecttype {'cached'},
                        $cachedConceptMaps[@id],
                        if (not($cachedConceptMaps) or $cachedConceptMaps[@ref]) then (
                            mpapi:getConceptMapByIdFromBBR($basePrefix, $id, $repoident, $bbrs, $newmap, $conceptMapsById, $conceptMapsByRef)
                        ) else ()
                    )
                    (: http call as last resort. :)
                    else (
                        try {
                            let $service-uri    := concat($repourl,'RetrieveConceptMap?format=xml&amp;prefix=',$repoident, '&amp;id=', $id)
                            let $requestHeaders := 
                                <http:request method="GET" href="{$service-uri}">
                                    <http:header name="Content-Type" value="text/xml"/>
                                    <http:header name="Cache-Control" value="no-cache"/>
                                    <http:header name="Max-Forwards" value="1"/>
                                </http:request>
                            let $server-response := http:send-request($requestHeaders)
                            let $server-check    :=
                                if ($server-response[1]/@status='200') then () else (
                                    error(QName('http://art-decor.org/ns/error', 'RetrieveError'), concat('Server returned HTTP status: ',$server-response[1]/@status, ' for URI ''',$service-uri,''' with body ''',string-join($server-response[2], ' '),''''))
                                )
                            let $conceptMaps      := $server-response//conceptMap[@id]
                            return (
                                attribute projecttype {'remote'},
                                $conceptMaps
                            )
                        }
                        catch * {()}
                    )
                )
            }
            </repositoryConceptMapList>
        )

return 
    $return
};

(:~ Get contents of a conceptMap :)
declare function mpapi:getRawConceptMap($conceptMap as element(conceptMap), $language as xs:string?, $prefix as xs:string, $version as xs:string?, $serialize as xs:boolean) as element(conceptMap) {
<conceptMap>
{
    $conceptMap/@*
}
{
    if ($serialize) then 
        for $node in $conceptMap/desc
        return
            utillib:serializeNode($node)
    else ($conceptMap/desc)
}
{
    $conceptMap/publishingAuthority
    ,
    if ($conceptMap[@id]) then 
        if ($conceptMap[publishingAuthority]) then () else (
            let $decor      := utillib:getDecorByPrefix($prefix, $version, $language)
            let $copyright  := ($decor/project/copyright[empty(@type)] | $decor/project/copyright[@type = 'author'])[1]
            return
            if ($copyright) then
                <publishingAuthority name="{$copyright/@by}" inherited="project">
                {
                    (: Currently there is no @id, but if it ever would be there ...:)
                    $copyright/@id,
                    $copyright/addrLine
                }
                </publishingAuthority>
            else ()
        )
    else ()
    ,
    $conceptMap/endorsingAuthority
    ,
    if ($serialize) then 
        for $node in $conceptMap/purpose
        return
            utillib:serializeNode($node)
    else ($conceptMap/purpose)
    ,
    let $copyrights     :=    
        if ($conceptMap[@id]) then
            utillib:handleCodeSystemCopyrights($conceptMap/copyright, distinct-values($conceptMap//@codeSystem))
        else (
            $conceptMap/copyright
        )
    return
        if ($serialize) then 
            for $node in $copyrights
            return
                utillib:serializeNode($node)
        else ($copyrights)
    ,
    $conceptMap/jurisdiction,
    $conceptMap/sourceScope,
    $conceptMap/targetScope,
    mpapi:getRawConceptMapGroup($conceptMap/group)
}
</conceptMap>
};

(:~ Get content of ConceptMap group :)
declare function mpapi:getRawConceptMapGroup($groups as element(group)*) as element(group)* {
    for $group in $groups
    return
        <group>
        {
            $group/source,
            $group/target,
            $group/element
        }
        </group>
};