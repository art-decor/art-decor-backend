xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

module namespace ggapi              = "http://art-decor.org/ns/api/governancegroups";

import module namespace roaster     = "http://e-editiones.org/roaster";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

import module namespace utillib     = "http://art-decor.org/ns/api/util" at "library/util-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "library/settings-lib.xqm";

import module namespace templ       = "http://art-decor.org/ns/decor/template"     at "/db/apps/art/api/api-decor-template.xqm";
import module namespace vs          = "http://art-decor.org/ns/decor/valueset"     at "/db/apps/art/api/api-decor-valueset.xqm";
import module namespace adpfix      = "http://art-decor.org/ns/art-decor-permissions" at "/db/apps/art/api/api-permissions.xqm";
import module namespace serverapi   = "http://art-decor.org/ns/api/server" at "server-api.xqm";

declare namespace http              = "http://expath.org/ns/http-client";
declare namespace sm                = "http://exist-db.org/xquery/securitymanager";
declare namespace xs                = "http://www.w3.org/2001/XMLSchema";
declare namespace output            = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace json              = "http://www.json.org";

declare %private variable $ggapi:GG-COPYRIGHT-TYPE                      := utillib:getDecorTypes()/CopyrightType;

(:  normally we return contents for ART XForms. These require serialize desc nodes. When we return for other purposes like 
:   XSL there is no need for serialization or worse it hurts :)
(:declare variable $ggapi:boolSerialize as xs:boolean    := true();:)
declare variable $ggapi:strDecorHostedGovernanceGroups := 'hosted-governance-groups.xml';
declare variable $ggapi:strDecorLinkedGovernanceGroups := 'governance-group-links.xml';

(:~ All functions support their own override, but this is the fallback for the maximum number of results returned on a search :)
declare %private variable $ggapi:maxResults                             := 50;

(:~ Retrieves DECOR project list

@param $bearer-token            - optional. provides authorization for the user. The token, if available, should be on the X-Auth-Token HTTP Header
@param $searchTerms             - optional. Comma separated list of terms that results should match in project/name element. Results all if empty
@param $id                      - optional. limits scope to this project only
@param $repository              - optional. boolean. limits scope to projects marked as repository, or not
@param $experimental            - optional. boolean. limits scope to projects marked as experimental, or not
@param $author                  - optional. boolean. limits scope to projects where user is marked as author, or not
@param $changeddate             - optional. optional. by project changed date. yyyy-mm-dd that may be prefixed with gt (greater than), lt (smaller than), ge (greater or equal than), le (smaller than)
@param $sortorder               - optional. Sort order. Default is ascending. Only other option is descending.
@param $max                     - optional integer, maximum number of results
@return list with zero to many projects
@since 2020-08-11
:)
declare function ggapi:getGovernanceGroupList($request as map(*)) {

    let $ggid               := $request?parameters?id[string-length() gt 0]
    let $searchTerms        := $request?parameters?search[string-length() gt 0]
    
    let $results            := ggapi:getGovernanceGroupById($ggid)
    let $results            := 
        if (empty($searchTerms)) then $results else (
            let $searchTerms    := tokenize(lower-case($searchTerms[string-length() gt 0]),'\s')
            let $luceneQuery    := utillib:getSimpleLuceneQuery($searchTerms)
            let $luceneOptions  := utillib:getSimpleLuceneOptions()
            return 
                $results[ft:query(name,$luceneQuery,$luceneOptions)]
        )
    let $count              := count($results)
    let $allcnt             := count($results)
    let $results            :=
        for $group in $results
        return
            ggapi:getGovernanceGroupInfo($group, false())
    return
        <list artifact="GOVERNANCEGROUP" current="{$count}" total="{$count}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements($results)
        }
        </list>
        
};

(:~ Retrieves governance group based on $id. Optionally includes related project information
    @param $id required path part which may be its prefix or its oid
    @param $language optional string parameter to return certain information with. Defaults for project default language
    @param $details optional boolean parameter to include project information or not. Default is false
    @return governance group as object
    @since 2020-05-03
:)
declare function ggapi:getGovernanceGroup($request as map(*)) {

    let $ggid               := $request?parameters?id[string-length() gt 0]
    let $doDetails          := $request?parameters?details = true()
    
    let $check              :=
        if (empty($ggid)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter id')
        else ()
    
    let $results            := ggapi:getGovernanceGroupInfo($ggid, $doDetails)
    
    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple governance groups id '", $ggid, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        )
        else (
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
        )

};

declare function ggapi:getHostedGovernanceGroups() as element(governance)? {
    let $createHostedGovernanceGroups   := 
        if (doc-available(concat($setlib:strArtData,'/',$ggapi:strDecorHostedGovernanceGroups))) then () else (
            xmldb:store($setlib:strArtData, $ggapi:strDecorHostedGovernanceGroups, 
                <governance xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="https://assets.art-decor.org/ADAR/rv/DECOR-governance-groups.xsd"/>)
        )
    
    return
        doc(concat($setlib:strArtData,'/',$ggapi:strDecorHostedGovernanceGroups))/governance
};

(:~ Create governance group.
    
    @param $checkadram optional boolean parameter to include ADRAM information or not. Default is false
    @return project as JSON object
    @since 2020-05-03
:)
declare function ggapi:postGovernanceGroup($request as map(*)) {

    let $authmap                := $request?user
    let $doDetails              := $request?parameters?details = true()
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($request?body)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $data                   := utillib:getBodyAsXml($request?body, 'group', ())
    let $results                := ggapi:postGovernanceGroup($authmap, $data, $doDetails)

    return
        if (empty($results)) then (
            error($errors:SERVER_ERROR, 'Governance group not returned after successful creation. Please inform your server administrator.')
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple governance groups with id '", string-join(distinct-values($results/@id), ' '), "'. Expected 1..1. Alert your administrator as this should not be possible."))
        )
        else (
            roaster:response(201, 
              for $result in $results
              return
                  element {name($result)} {
                      $result/@*,
                      namespace {"json"} {"http://www.json.org"},
                      utillib:addJsonArrayToElements($result/*)
                  }
            )
        )

};

declare function ggapi:postGovernanceGroup($authmap as map(*)?, $data as element(group), $doDetails as xs:boolean) {
    let $groupBaseId            := if ($data/@baseId[utillib:isOid(.)]) then $data/@baseId else serverapi:getServerDefaultId('decor-govgroup')[utillib:isOid(.)] 
    let $groupId                := $data/@id[utillib:isOid(.)]
    let $groupLanguage          := $data/@defaultLanguage[matches(., '^[a-z]{2}-[A-Z]{2}$')]
    let $allGroups              := ggapi:getHostedGovernanceGroups()/group
    
    let $check                  := 
        if (empty($groupId)) then 
            if (empty($groupBaseId)) then
                error($errors:BAD_REQUEST, 'Your governance group does not have an id, nor a baseId. Since your server also does not have a default baseId, it is not possible to generate an id for your governance group')
            else ()
        else if (empty($allGroups[@id = $groupId])) then () else (
            error($errors:BAD_REQUEST, 'Governance group with id ' || $groupId || ' already exists. Cannot create.')
        )
    let $check                  := 
        if (empty($groupLanguage)) then 
            error($errors:BAD_REQUEST, 'Governance group SHALL have a defaultLanguage as ll-CC. Found: ' || $data/@defaultLanguage)
        else ()
    let $check                  := 
        if (empty($data/name[@language = $groupLanguage])) then 
            error($errors:BAD_REQUEST, 'Governance group SHALL have at least a name matching its defaultLanguage ' || $groupLanguage || '. Cannot create.')
        else ()
    
    let $groupId                :=
        if (empty($groupId)) then $groupBaseId || '.' || (max($allGroups[matches(@id, concat('^', $groupBaseId, '\.\d$'))]/xs:integer(tokenize(@id, '\.')[last()])), 0)[1] + 1 else $groupId
    
    let $check                  :=
        if (matches($groupId, '^[0-2](\.(0|[1-9][0-9]*)){0,3}$')) then
            error($errors:BAD_REQUEST, 'Governance group with id ''' || $groupId || ''' is reserved. You cannot reuse a reserved identifier. See http://oid-info.com/get/' || $groupId)
        else ()

    let $newGroup               := ggapi:handleGroup(<group id="{$groupId}">{$data/@defaultLanguage, $data/*}</group>)
    let $store                  := ggapi:updateGroups($newGroup)
    
    return ggapi:getGovernanceGroupInfo($groupId, $doDetails)
};

(:~ Update governance group parts. Expect array of parameter objects, each containing RFC 6902 compliant contents. Note: RestXQ does not do PATCH (yet), but that would be the preferred option.

{ "op": "[add|remove|replace]", "path": "[e.g. /name|/desc|/copyright]", "value": "[string|object]" }

where

* op - add or remove or replace
* path - paths to the object: e.g. /repository|/name
* value - as relevant for the path

@param $bearer-token            - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $id                      - required. the id or prefix for the group to update 
@param $request-body            - required. json body containing array of parameter objects each containing RFC 6902 compliant contents
@return governance group
@since 2020-05-03
@see http://tools.ietf.org/html/rfc6902
:)
declare function ggapi:patchGovernanceGroup($request as map(*)) {

    let $authmap                := $request?user
    let $ggid                   := $request?parameters?id[string-length() gt 0]
    let $doDetails              := $request?parameters?details = true()

    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($request?body)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    let $check                  :=
        if (empty($ggid)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter id')
        else ()
    
    let $data                   := utillib:getBodyAsXml($request?body, 'parameters', ())
    let $return                 := ggapi:patchGovernanceGroup($authmap, $ggid, $data, $doDetails)
    return (
        roaster:response(200, $return)
    )

};

(:~ Deletes governance group based on $id. **dba only**
    @param $id required path matching the governance group id
    @return nothing
    @since 2020-05-03
:)
declare function ggapi:deleteGovernanceGroup($request as map(*)) {

    let $authmap                := $request?user
    let $ggid                   := $request?parameters?id[string-length() gt 0]
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else if ($authmap?groups = 'dba') then ()
        else (
            error($errors:FORBIDDEN, 'You need to be dba for this feature')
        )
    let $check                  :=
        if (empty($ggid)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter id')
        else ()
    
    let $results                :=  ggapi:getGovernanceGroupById($ggid)
    
    let $check                  :=
        if (count($results) gt 1) then
            error($errors:SERVER_ERROR, concat("Found multiple governance groups '", $ggid, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        else ()
    
    let $delete                 := update delete $results
    
    return
        roaster:response(204, ())

};

(: Returns any governance-group-links elements for a given projectId so we know what governance groups a project claims to be partOf 
<governance-group-links ref="2.16.840.1.113883.2.4.3.111.3.7">
    <partOf ref="2.16.840.1.113883.2.4.3.111"/>
</governance-group-links>:)
declare function ggapi:getLinkedGovernanceGroups($projectId as xs:string?) as element(governance-group-links)* {
    if (empty($projectId)) then
        $setlib:colDecorData/governance-group-links
    else
        $setlib:colDecorData/governance-group-links[@ref = $projectId]
};
(: Returns any governance-group-links elements for a given groupId so we know what project claims to be partOf that governance group
<governance-group-links ref="2.16.840.1.113883.2.4.3.111.3.7">
    <partOf ref="2.16.840.1.113883.2.4.3.111"/>
</governance-group-links>:)
declare function ggapi:getLinkedProjects($groupId as xs:string) as element(governance-group-links)* {
    $setlib:colDecorData/governance-group-links[partOf/@ref = $groupId]
};

(:~
:   Adds, Updates, Deletes governance groups on the input.
:   Expected input: 
:   <group id="groupid" defaultLanguage="en-US">
:       <edit id="previousgroupid"/> | <delete/>
:       <name .../>
:       <desc .../>
:       <copyright .../>
:   </group>
:   
:   @param $groups - optional. Groups to add/upate/delete
:   @return null or error() in case of an unsupported action
:   @since 2015-03-09
:)
declare function ggapi:saveGroups($groups as element(group)*) {
    let $results        := ggapi:deleteGroups($groups[*/name()='delete'])
    let $results        := ggapi:updateGroups($groups[edit])
    
    return ()
};
(:~
:   Adds, Updates governance groups on the input.
:   Expected input: 
:   <group id="groupid" defaultLanguage="en-US">
:       <edit id="previousgroupid"/>
:       <name .../>
:       <desc .../>
:       <copyright .../>
:   </group>
:   
:   @param $groups - optional. Groups to add/upate
:   @return null or error() in case of an unsupported action
:   @since 2015-08-07
:)
declare function ggapi:updateGroups($groups as element(group)*) {
    let $currentGroups  := ggapi:getHostedGovernanceGroups()
    
    for $group in $groups
    let $groupid                := $group/@id
    let $previousgroupid        := $group/edit/@id
    (: content of the whole new hosted-governance-group.xml file :)
    let $ggallcontent           := ggapi:prepareGovernanceGroupFile($group)
    let $resultupdategroup      := 
        if (not($currentGroups)) then (
            xmldb:store($setlib:strArtData, $ggapi:strDecorHostedGovernanceGroups, $ggallcontent)
        )
        else if ($currentGroups/group[@id=$groupid]) then (
            update replace $currentGroups/group[@id=$groupid] with $ggallcontent/group
        )
        else (
            update insert $ggallcontent/group into $currentGroups
        )
    (: delete group under old id and update governance group links if necessary :)
    let $resultupdatelink       :=
        if ($previousgroupid and not($groupid=$previousgroupid)) then (
            update delete $currentGroups/group[@id=$previousgroupid]
            ,
            ggapi:updateGroupLinks($previousgroupid, $groupid)
        ) else ()

    return ()
};
(:~
:   Deletes governance groups on the input.
:   Expected input: 
:   <group id="groupid" defaultLanguage="en-US">
:       <delete/>
:       <name .../>
:       <desc .../>
:       <copyright .../>
:   </group>
:   
:   @param $groups - optional. Groups to delete
:   @return null or error() in case of an unsupported action
:   @since 2015-03-09
:)
declare function ggapi:deleteGroups($groups as element(group)*) {
    let $currentGroups  := ggapi:getHostedGovernanceGroups()
    
    for $group in $groups
    let $groupid                := $group/@id
    let $previousgroupid        := $group/edit/@id
    (: content of the whole new hosted-governance-group.xml file :)
    let $ggallcontent           := ggapi:prepareGovernanceGroupFile($group)
    let $resultdelete           := update delete $currentGroups/group[@id=$groupid]
    let $resultunlink           := ggapi:unlinkGroup($groupid, ())
    
    return ()
};

declare function ggapi:getGovernanceGroupInfo($ggid as item(), $doDetails as xs:boolean) as element(group)* {
    
    let $results as element(group)*     := 
        typeswitch ($ggid)
        case document-node() return $ggid/*
        case element() return $ggid
        default return ggapi:getGovernanceGroupById($ggid)
    
    for $group in $results
    let $links          := if ($doDetails) then ggapi:getLinkedProjects($results/@id)/@ref else ()
    let $linkedprojects := 
        if ($doDetails) then 
            for $project in $setlib:colDecorData/decor/project[@id = $links]
            return ggapi:projectDetails($project, $doDetails)
        else ()
    (: consolidate across all projects :)
    let $allvs          :=
        if ($doDetails) then
            for $a in $linkedprojects//valueSet/valueSet
            let $obid   := ($a/@id | $a/@ref)[1]
            let $obed   := $a/@effectiveDate
            group by $obid, $obed
            return
                element {($a/name())[1]} {$obid, $obed}
        else ()
    let $alltm        :=
        if ($doDetails) then
            for $a in $linkedprojects//template/template
            let $obid   := ($a/@id | $a/@ref)[1]
            let $obed   := $a/@effectiveDate
            group by $obid, $obed
            return
                element {($a/name())[1]} {$obid, $obed}
        else ()
    return
        <group>
        {
            $results/@id, 
            $results/@defaultLanguage
        }
        {
            if ($doDetails) then (
                attribute valuesetcount {count($allvs)},
                attribute templatecount {count($alltm)}
            ) else ()
        }
        {
            $results/name, 
            $results/desc, 
            for $copyright in $results/copyright
            return
                <copyright>
                {
                    attribute years {$copyright/@years},
                    $copyright/@by[not(. = '')],
                    $copyright/@logo[not(. = '')],
                    $copyright/@type[not(. = '')],
                    for $addr in $copyright/addrLine
                    return
                        <addrLine>
                        {
                            attribute type {
                                if ($addr[matches(., 'https?:/')]) then 'uri' else
                                if ($addr[matches(., 'ftps?:/')]) then 'uri' else
                                if ($addr[matches(., 'tel:')]) then 'phone' else
                                if ($addr[matches(., 'fax:')]) then 'fax' else
                                if ($addr[matches(., 'mailto:')]) then 'email' else
                                if ($addr[string-length(replace(., '[^\d]', '')) ge 10]) then 'phone' else
                                if ($addr[matches(., '\S+@[^\s\.]+\.\S+')]) then 'email' else ()
                            },
                            $addr/(@* except @type),
                            $addr/node()
                        }
                        </addrLine>
                }
                </copyright>
        }
        {
            if ($doDetails) then (
                (: project order mimics the art menu order :)
                (:normal projects:)
                (:experimental projects:)
                (:repositories:)
                for $project in $linkedprojects
                let $defaultlang := $project/@defaultLanguage
                order by $project/@experimental ='true', $project/@repository ='true', lower-case($project/name[@language=$defaultlang][1])
                return
                    $project
            ) else ()
        }
        </group>
};
(:~
:   Retrieves the list of currently available hosted governance groups. This could be 0..*. 
:   Each governance group has @id, @defaultLanguage and name. 
:   Output: 
:   <result>
:       {[
:       <group id="groupid" defaultLanguage="en-US">
:           <name .../>
:       </group>
:       ]}
:   </result>
:   
:   @return 0..* group elements wrapped in a <result/> element.
:   @since 2015-03-09
:)
declare function ggapi:getGovernanceGroupById($groupid as xs:string?) as element(group)* {
  let $govgrps        := ggapi:getHostedGovernanceGroups()/group
  return 
      if (empty($groupid)) then $govgrps else $govgrps[@id=$groupid]
};
(:~
:   Links projects or unlinks projects to a governance group based on the @action attribute of the input.
:   Expected input: <group action="link|unlink" id="groupid" projectId="projectid"/>
:   
:   @param $groups - optional. The groups to link/unlink
:   @return null or error() in case of an unsupported action
:   @since 2015-03-09
:)
declare function ggapi:saveGroupLinks ($groups as element(group)*) {
    for $group in $groups
    let $action                 := $group/@action
    let $groupid                := $group/@id
    let $projectid              := $group/@projectId
    (: 
        if   :: no file exist => make one with the whole content, and done
        else :: if partOf element with this governance group already exist and unlink=true => delete this partOf element
    :)
    let $result :=
        if ($action='unlink') then 
            ggapi:unlinkGroup($groupid, $projectid)
        else if ($action='link') then (
            ggapi:linkGroup($groupid, $projectid)
        )
        else (
            error($errors:BAD_REQUEST, 'Found unsupported action ''' || $action || '''. Expected @action=link or @action=unlink')
        )
    return ()
};
(:~ Central logic for updating an existing project properties

@param $authmap         - required. Map derived from token
@param $id              - required. DECOR issue/@id to update
@param $request-body    - required. DECOR xml element parameter elements each containing RFC 6902 compliant contents
@return project object as xml with json:array set on elements
:)
declare function ggapi:patchGovernanceGroup($authmap as map(*), $ggid as xs:string, $request-body as element(parameters), $doDetails as xs:boolean) as element(group)? {
    let $group                  := 
        if (count($ggid[not(. = '')]) = 1) then
            ggapi:getGovernanceGroupById($ggid)
        else (
            error($errors:BAD_REQUEST, 'You are missing required parameter ggid')
        )
    let $check                  :=
        if ($group) then () else (
            error($errors:BAD_REQUEST, 'Governance group with id ''' || $ggid || ''', does not exist.')
        )
    let $now                    := substring(string(current-dateTime()), 1, 19)
    
    let $check                  :=
        if ($authmap?groups = 'dba') then () else (
            error($errors:FORBIDDEN, 'User ' || $authmap?name || ' does not have sufficient permissions to modify the governance group with id ' || $ggid || '. You have to be an active dba.')
        )
    let $check                  :=
        if ($request-body[parameter]) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall be an array of ''parameter''.')
        )
    let $unsupportedops         := $request-body/parameter[not(@op = ('add', 'replace', 'remove'))]
    let $check                  :=
        if ($unsupportedops) then
            error($errors:BAD_REQUEST, 'Submitted parameters shall have supported ''op'' value. Found ''' || string-join(distinct-values($unsupportedops/@op), ''', ''') || ''', expected ''add'', ''replace'', or ''remove''') 
        else ()
    let $checkn                 :=
        if (count($group/name) - count($request-body/parameter[@op = 'remove'][@path = '/name']) + count($request-body/parameter[@op = 'add'][@path = '/name']) ge 1) then () else (
            'A governance group SHALL have at least one name. You cannot remove every name.'
        )
    let $check                  :=
        for $param in $request-body/parameter
        return
            if ($param[@path = '/defaultLanguage']) then
                if ($param[@op = 'remove']) then
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported.'
                else
                if ($param[@op = 'add'] and $group/@defaultLanguage) then
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' already exists and there can only be one.'
                else
                if ($param[matches(@value, '[a-z]{2}-[A-Z]{2}')]) then () else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A governance group defaultLanguage SHALL have pattern ll-CC. Found: ''' || $param/@value || ''''
                )
            else
            if ($param[@path = '/name']) then (
                if ($param[count(value/name) = 1]) then
                    if ($param/value/name[matches(@language, '[a-z]{2}-[A-Z]{2}')]) then 
                        if ($param[@op = 'remove'] | $param/value/name[.//text()[not(normalize-space() = '')]]) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A governance group name SHALL have contents.'
                        )
                    else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A governance group name SHALL have a language with pattern ll-CC.'
                    )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one name under value. Found ' || count($param/value/name) 
                )
            )
            else
            if ($param[@path = '/desc']) then (
                if ($param[count(value/desc) = 1]) then
                    if ($param/value/desc[matches(@language, '[a-z]{2}-[A-Z]{2}')]) then 
                        if ($param[@op = 'remove'] | $param/value/desc[.//text()[not(normalize-space() = '')]]) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A governance group desc SHALL have contents.'
                        )
                    else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A governance group desc SHALL have a language with pattern ll-CC.'
                    )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one desc under value. Found ' || count($param/value/desc) 
                )
            )
            else
            if ($param[@path = '/copyright']) then (
                if ($param[count(value/copyright) = 1]) then (
                    if ($param/value/copyright[@years]) then () else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A project copyright SHALL have at least years.'
                    ),
                    (:if ($param/value/copyright[@by]) then () else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A project copyright SHALL have at least by.'
                    ),:)
                    if ($param/value/copyright[empty(@type)] | $param/value/copyright[@type = $ggapi:GG-COPYRIGHT-TYPE/enumeration/@value]) then () else (
                        concat('Parameter value ''', $param/value/copyright/@type, ''' for copyright/type not supported. Supported are: ', string-join($ggapi:GG-COPYRIGHT-TYPE/enumeration/@value, ' '))
                    )
                )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one copyright under value. Found ' || count($param/value/copyright) 
                )
            )
            else (
                'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported.'
            )
     let $check                 :=
        if (empty($check) and empty($checkn)) then () else (
            error($errors:BAD_REQUEST,  string-join(($check, $checkn), ' '))
        )

    let $update                 :=
        for $param in $request-body/parameter
        return
            switch ($param/@path)
            (:<xs:element name="name" type="BusinessNameWithLanguage" minOccurs="1" maxOccurs="unbounded"/>:)
            case '/defaultLanguage' return 
                if ($group[@defaultLanguage]) then
                    update value $group/@defaultLanguage with $param/@value
                else (
                    update insert attribute defaultLanguage {$param/@value} into $group
                )
            case '/name' (: fall through :)
            (:<xs:element name="desc" type="FreeFormMarkupWithLanguage" minOccurs="0" maxOccurs="unbounded"/>:)
            case '/desc' return (
                let $elmname  := substring-after($param/@path, '/')
                let $new      := utillib:prepareFreeFormMarkupWithLanguageForUpdate($param/value/*)
                let $stored   := $group/*[name() = $elmname][@language = $param/value/*/@language]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $group
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            (:<xs:element ref="copyright" minOccurs="1" maxOccurs="unbounded"/>:)
            case '/copyright' return (
                (: there can be only one :)
                let $elmname  := substring-after($param/@path, '/')
                let $new      := ggapi:handleGroupCopyright($param/value/copyright)
                let $stored   := $group/copyright
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return (update delete $stored, update insert $new into $group)
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            default return ( (: unknown path :) )
    
    (: after all the updates, the XML Schema order is likely off. Restore order :)
    let $update                 := update replace $group with ggapi:handleGroup($group)
       
    return
        ggapi:getGovernanceGroupInfo($ggid, $doDetails)
};
(:~
:   Links project to a governance group
:   
:   @param $groupid - required. The governance group id
:   @param $projectid - required. The decor project id
:   @return null
:   @since 2015-03-09
:)
declare function ggapi:linkGroup ($groupid as xs:string, $project as item()) {
    let $decor                  :=
        typeswitch ($project)
        case element() return $project/ancestor-or-self::decor
        case document-node() return $project/decor
        default return (
            if (utillib:isOid($project)) then utillib:getDecorById($project) else utillib:getDecorByPrefix($project)
        )
    let $projectId              := $decor/project/@id
    
    let $currentLinks           := ggapi:getLinkedGovernanceGroups($projectId)
    let $newLinks               := ggapi:prepareGovernanceGroupLink($groupid, $projectId)
    (: get project parent collection of project :)
    let $update := 
        if (empty($currentLinks)) then (
            let $f  := xmldb:store(util:collection-name($decor), $ggapi:strDecorLinkedGovernanceGroups, $newLinks)
            let $u  := try { adpfix:setDecorPermissions() } catch * {sm:chmod($f, 'rw-rw-r--')}
            return ()
        )
        else if ($currentLinks/partOf[@ref = $groupid]) then ( (: already exists :) )
        else (
            update insert $newLinks/partOf into $currentLinks
        )
    
    return ()
};

(:~
:   Updates project links for a governance group when the governance group id was updated.
:   
:   @param $previousgroupid - required. The previous governance group id
:   @param $newgroupid - required. The new governance group id
:   @return null
:   @since 2015-03-09
:)
declare function ggapi:updateGroupLinks ($previousgroupid as xs:string, $newgroupid as xs:string) {
    let $currentLinks           := ggapi:getLinkedGovernanceGroups(())
    let $update                 := update value $currentLinks/partOf[@ref=$previousgroupid]/@ref with $newgroupid
    
    return ()
};

(:~
:   Unlinks projects from a governance group
:   
:   @param $groupid - required. The governance group id
:   @param $projectid - optional. The decor project id. Unlinks from every project if not given.
:   @return null
:   @since 2015-03-09
:)
declare function ggapi:unlinkGroup ($groupid as xs:string, $projectid as xs:string?) {
    let $currentLinks           := ggapi:getLinkedGovernanceGroups($projectid)
    let $update                 := update delete $currentLinks/partOf[@ref=$groupid]
    
    return ()
};

declare %private function ggapi:projectDetails($project as element(project),$addArtefactList as xs:boolean) as element() {
    <project>
    {
        $project/@id,
        $project/@prefix,
        $project/@defaultLanguage,
        attribute experimental {$project/@experimental = 'true'},
        attribute repository {$project/ancestor::decor/@repository = 'true'},
        attribute private {$project/ancestor::decor/@private = 'true'},
        if ($project/name[@language = $project/@defaultLanguage]) then () else (
            <name language="{$project/@defaultLanguage}">{$project/name/node()}</name>
        ),
        $project/name,
        (: 
            get governance group project template and value set list, 
            but only if this get call is for one single governance group only
            AND
            the parameter getall=true. This parameter is false in the first call to 
            just get the most important information about the governance gropup.
            A subsequent but typically asynchronous call will get the detailed
            list of templates and value sets then
        :)
        if ($addArtefactList=true()) then (
            (: 
                following is copy from get-valueset-list.xquery, 
                should use vs:getValueSetList ($id as xs:string?, $name as xs:string?, $flexibility as xs:string?, $prefix as xs:string?, $version as xs:string?)
                as vs:getValueSetList('','','',$project/@prefix) from valueset api but this does not work !?
                
                KH: 20151205: sinterklaas zei, nu werkt het ;-)
            :)
            (:
            for $valueSet in $project/../terminology/valueSet
            let $latestVersion  := max($valueSet/xs:dateTime(@effectiveDate))
            let $latestValueSet := if (empty($latestVersion)) then $valueSet[1] else ($valueSet[@effectiveDate=$latestVersion][1])
            let $name := $valueSet/@name
            group by $name
            order by if ($valueSet[1]/@displayName[string-length()>0]) then lower-case($valueSet[1]/@displayName) else (lower-case($valueSet[1]/@name))
            return
            <valueSet>
            {
                $latestValueSet/@name,
                $latestValueSet/@displayName,
                $latestValueSet/@effectiveDate,
                if ($latestValueSet/@id[string-length()>0]) then (
                    $latestValueSet/@id
                ) else (
                    $latestValueSet/@ref
                ),
                $latestValueSet/@statusCode
            }
            </valueSet>,
            :)
            
            vs:getValueSetList-v2((),(),(),$project/@prefix,(),true(),())/valueSet
            ,
            templ:getTemplateList((),(),(),$project/@prefix,(),false())/*
            
        ) else ()
    }
    </project>
};

declare %private function ggapi:prepareGovernanceGroupFile ($groups as element(group)*) as element(governance) {
    <governance xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="https://assets.art-decor.org/ADAR/rv/DECOR-governance-groups.xsd">
    {
        for $group in $groups
        return ggapi:prepareGovernanceGroup($group)
    }
    </governance>
};

declare %private function ggapi:prepareGovernanceGroup ($group as element()) as element(group) {
    <group>
    {
        $group/(@id|@defaultLanguage)
        ,
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($group/name)
        ,
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($group/desc)
        ,
        $group/(* except (edit|delete|name|desc|copyright|project)),
        <copyright>
        {
            $group/copyright/@*,
            for $addr in $group/copyright/addrLine[not(.='')]
            return
                <addrLine>{$addr/@*[not(.='')],$addr/node()}</addrLine>
        }
        </copyright>
    }
    </group>
};

declare %private function ggapi:prepareGovernanceGroupLink ($groupid as xs:string, $projectid as xs:string) as element(governance-group-links) {
    <governance-group-links ref="{$projectid}" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="https://assets.art-decor.org/ADAR/rv/DECOR-governance-groups.xsd">
        <partOf ref="{$groupid}"/>
    </governance-group-links>
};

declare function ggapi:handleGroup($in as element(group)) {
    <group>
    {
        attribute id {$in/@id},
        attribute defaultLanguage {$in/@defaultLanguage},
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($in/name),
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($in/desc),
        if ($in[copyright]) then 
            for $n in $in/copyright return ggapi:handleGroupCopyright($n)
        else (
            ggapi:handleGroupCopyright(<copyright years="{year-from-date(current-date())}" by="ART-DECOR"/>)
        )
    }
    </group>
};
declare function ggapi:handleGroupCopyright($in as element(copyright)) {
    if ($in[@years]) then
        element {name($in)} {
            $in/@years,
            $in/@by,
            $in/@logo[not(normalize-space() = '')],
            $in/@type[not(normalize-space() = '')],
            for $node in $in/addrLine[exists(node())][.//text()[not(normalize-space() = '')]]
            return
                <addrLine>{$node/@type[not(. = '')], $node/node()}</addrLine>
        }
    else ()
};