xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

(:~ MyCommunity API allows read, create, update of community properties for a DECOR project :)
module namespace mcapi              = "http://art-decor.org/ns/api/mycommunity";

import module namespace roaster     = "http://e-editiones.org/roaster";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "library/settings-lib.xqm";
import module namespace utillib     = "http://art-decor.org/ns/api/util" at "library/util-lib.xqm";
import module namespace decorlib    = "http://art-decor.org/ns/api/decor" at "library/decor-lib.xqm";

declare namespace json      = "http://www.json.org";

(:~ Retrieves myCommunity environment list for a DECOR project

@param $project                  - required. the project prefix or id for community to create
@return as-is or as compiled as JSON
@since 2024-03-20
:)
declare function mcapi:getMyCommunityList($request as map(*)) {
    
    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]

    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()

    let $decor                  := utillib:getDecor($project, (), ())

    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $project, ''', does not exist.'))
        )

    let $format                 := tokenize((roaster:accepted-content-types()[. = ('application/xml', 'application/json')],'application/json')[1], '/')[2]
    
    let $results                :=
        for $community in decorlib:getDecorCommunity((), $decor/project/@id, ())
        order by lower-case(($community/@displayName, $community/@name)[1])
        let $private            := decorlib:isPrivateCommunity($community)
        let $read               := if (not(empty($authmap))) then decorlib:authorCanReadCommunityP($authmap, $community)
            else if($private) then false() else true()
        let $write              := if (not(empty($authmap))) then decorlib:authorCanEditCommunityP($authmap,$community) else false()   
        return
            <community>
            {
                $community/(@* except (@xsi:noNamespaceSchemaLocation)),
                attribute private {$private},
                attribute read {$read},
                attribute write {$write},
                $community/desc
            }
            </community>
    
    let $count                  := count($results)
    let $max                    := $count
    let $allcnt                 := count($results)
    return
        <list artifact="MYCOMMUNITY" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements(subsequence($results, 1, $max), $format)
        }
        </list>
};

(:~ Retrieves myCommunity environment for a DECOR project

@param $project                  - required. the project prefix or id for community to create
@param $name                     - required. the community name for community to create
@return as-is or as compiled as JSON
@since 2024-03-20
:)
declare function mcapi:getMyCommunity($request as map(*)) {
    
    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    let $name                   := $request?parameters?name[string-length() gt 0]

    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()

    let $check                  :=
        if (empty($name)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter name (community name)')
        else ()    

    let $decor                  := utillib:getDecor($project, (), ())

    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $project, ''', does not exist.'))
        )

    let $communities            := decorlib:getDecorCommunity($name, $decor/project/@id)
    
    let $results                  :=
    for $community in $communities
        let $read               := 
            if (not(empty($authmap))) then decorlib:authorCanReadCommunityP($authmap, $community)
            else if(decorlib:isPrivateCommunity($community)) then false() else true()   
        let $check              :=
            if ($read) then () else 
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to read community  ''', $name, ''' in project ''', $project, '''. You have to be an author with read or write permissions in the the community.'))       

            return mcapi:getMyCommunityResponse($community)
    
    let $format                 := tokenize((roaster:accepted-content-types()[. = ('application/xml', 'application/json')],'application/json')[1], '/')[2]
        
    return
    if (empty($results)) then (
        roaster:response(404, ())
    )
    else
    if (count($results) gt 1) then (
        error($errors:SERVER_ERROR, concat("Found multiple communities for project '", $project, "' and name '", $name, "'. Expected 0..1. Alert your administrator as this should not be possible."))
    )
    else (
        for $result in $results
        return
            element {name($result)} {
                $result/@*,
                namespace {"json"} {"http://www.json.org"},
                utillib:addJsonArrayToElements($result/*, $format)
            }
    )
};

(:~ Creates new myCommunity environment for a DECOR project

@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $project                  - required. the project prefix or id for community to create
@param $name                     - required. the community name for community to create
@param $request-body             - required. json body containing new community structure
@return created community object
@since 2024-03-20
:)
declare function mcapi:postMyCommunity($request as map(*)) {
    
    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    let $name                   := $request?parameters?name[string-length() gt 0]

    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()

    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    
    let $check                  :=
        if (empty($name)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter name (community name)')
        else ()    

    let $check                  :=
        if (empty($request?body)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
   
    let $decor                  := utillib:getDecor($project, (), ())
    let $check                  :=
        if ($decor) then () else 
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $project, ''', does not exist.'))
        
    let $projectId              := $decor/project/@id
    
    let $community              := decorlib:getDecorCommunity($name, $projectId)
    let $check                  :=
        if ($community) then 
            error($errors:BAD_REQUEST, concat('MyCommunity with id ''', $projectId, ''' and name ''', $name, ''', already exists.'))
        else ()

    (: current user should have permission to edit a project for creating a community environment :)
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-PROJECT)) then () else 
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to create a community environment in this project ', $project, '. You have to be an active author in the project.'))
   
    let $body                   := utillib:getBodyAsXml($request?body, 'community', ()) 
    let $results                := mcapi:handleMyCommunity($decor, $name, $body, ())

    let $format                 := tokenize((roaster:accepted-content-types()[. = ('application/xml', 'application/json')],'application/json')[1], '/')[2]

    return
        roaster:response(201, 
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*, $format)
                }
        )
};

(:~ Updates myCommunity environment for a DECOR project

@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $project                  - required. the project prefix or id for community to update
@param $name                     - required. the community name for community to update
@param $request-body             - required. json body containing new community structure
@return updated community object
@since 2024-03-20
:)
declare function mcapi:putMyCommunity($request as map(*)) {
    
    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    let $name                   := $request?parameters?name[string-length() gt 0]

    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()

    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    
    let $check                  :=
        if (empty($name)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter name (community name)')
        else ()    

    let $check                  :=
        if (empty($request?body)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()

    let $decor                  := utillib:getDecor($project, (), ())
    let $check                  :=
        if ($decor) then () else 
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $project, ''', does not exist.'))
        
    let $projectId              := $decor/project/@id
    
    let $storedCommunity        := decorlib:getDecorCommunity($name, $projectId)
    let $check                  :=
        if ($storedCommunity) then ()
            else error($errors:BAD_REQUEST, concat('MyCommunity with id ''', $projectId, ''' and name ''', $name, ''', does not exist.'))
    
    
    (: current user should have permission to edit the community :)
    let $check                  :=
        if (decorlib:authorCanEditCommunity($authmap, $name, $projectId)) then () else 
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to update a community environment in this project ', $project, '. You have to be an active author in the community.'))
        
    let $lock                   := decorlib:getLocks($authmap, $storedCommunity, false())
    let $lock                   := if ($lock) then $lock else decorlib:setLock($authmap, $storedCommunity, false())
    let $check                  :=
        if ($lock) then ($lock) else (
            error($errors:BAD_REQUEST, concat('MyCommunity with id ''', $projectId, ''' and name ''', $name,''' does not have a lock for current user (anymore) and it cannot be obtained.'))
        )

    let $body                   := utillib:getBodyAsXml($request?body, 'community', ()) 
    let $results                := mcapi:handleMyCommunity($decor, $name, $body, $storedCommunity)

    let $deleteLock             := if ($lock) then update delete $lock else ()

    let $format                 := tokenize((roaster:accepted-content-types()[. = ('application/xml', 'application/json')],'application/json')[1], '/')[2]

    return
        for $result in $results
        return
            element {name($result)} {
                $result/@*,
                namespace {"json"} {"http://www.json.org"},
                utillib:addJsonArrayToElements($result/*, $format)
            }
};

declare %private function mcapi:getMyCommunityResponse($community as element(community)) as element(community) {

    <community>
    {
        $community/(@* except (@xsi:noNamespaceSchemaLocation)),
        $community/desc,
        $community/access
    }
    {
    if ($community/prototype/@ref) then 
        <prototype>
        {
            let $external   := doc(xs:anyURI($community/prototype/@ref))/prototype
            return (
                $community/prototype/@ref,
                $external/node()
            )
        }
        </prototype>
    else
        <prototype>
        {
            for $data in $community/prototype/data
            return
                <data>
                {
                    $data/@*,
                    for $desc in $data/desc return utillib:serializeNode($desc),
                    $data/enumValue
                }
                </data>
        }
        </prototype>
    }
    {
        <associations>
        {
            for $association in $community/associations/association
            return
                <association>
                {
                    $association/object,
                    for $data in $association/data return utillib:serializeNode($data)
                }
                </association>
        }
        </associations>
    }
    </community>

};

declare %private function mcapi:handleMyCommunity($decor as element(decor), $name as xs:string, $body as element(community), $storedCommunity as element(community)?) as element(community) { 

    let $projectId              := $decor/project/@id
    
    (: check request body :)
    let $check               :=
        if ($body/@projectId = $projectId) then () else
            error($errors:BAD_REQUEST, concat('The myCommunity request project url ''', $decor/project/@prefix, ''', id ''', $projectId, ''' shall match the request body projectId ''', $body/@projectId, '''.'))
    let $check               :=
        if ($body/@name = $name) then () else
            error($errors:BAD_REQUEST, concat('The myCommunity request name url ''', $name, ''' shall match the request body name ''', $body/@name, '''.'))         
    let $check               :=
        if ($body[desc]) then () else 
            error($errors:BAD_REQUEST, concat('MyCommunity with id ''', $projectId, ''' and name ''', $name, ''', must have a descripion.'))
    let $check               :=
        if ($body[prototype]) then () else 
            error($errors:BAD_REQUEST, concat('MyCommunity with id ''', $projectId, ''' and name ''', $name, ''', must have a prototype.'))
    let $check               :=
        if ($body//author[matches(@rights,'w')]) then () else 
            error($errors:BAD_REQUEST, concat('MyCommunity with id ''', $projectId, ''' and name ''', $name, ''', must have a at least one author with editing rights.'))     
        
    (: every author in the community - except gues - should be an author in the project :)
    let $check               :=
        for $author in $body/access/author
        where not($author/@username = 'guest')
        let $authcommap     := map {"name": $author/@username }
        let $check                  :=
            if (decorlib:isActiveAuthorP($authcommap, $decor)) then () else 
                error($errors:FORBIDDEN, concat('User ', $author/@username, ' does not have sufficient permissions to be a member of the community environment in this project ''', $decor/project/@prefix, ''', id ''', $projectId, '''. They should to be an active author in the project.'))
        return ()
    
    (: prototype ref must exist as a prototype :) 
    let $check                   :=
        if ($body/prototype/@ref and not(doc(xs:anyURI($body/prototype/@ref))/prototype)) then 
            error($errors:BAD_REQUEST, concat('The referenced prototype ''', $body/prototype/@ref, ''' does not exist as a prototype element.'))
        else ()
            
    
    (: prepare the community to be saved :)
    let $community :=
        <community>
        {
            attribute name {$name},
            attribute projectId {$projectId},
            attribute displayName {if (empty($body/@displayName)) then $name else $body/@displayName},
            $body/desc ! utillib:parseNode(.),
            $body/access,
            if ($body/prototype/@ref) then (
                <prototype>
                    {$body/prototype/@ref}
                </prototype>
            )
            else (
                <prototype>
                {
                    for $data in $body/prototype/data
                    return
                        <data>
                        {
                            $data/@*,
                            for $desc in $data/desc return utillib:parseNode($desc),
                            (:$data/desc ! utillib:parseNode(.),:)
                            $data/enumValue
                        }
                        </data>
                }
                </prototype>
             ),
                <associations>
                {
                    for $association in $body/associations/association
                    return
                        <association>
                        {
                            $association/object,
                            for $data in $association/data  return utillib:parseNode($data)
                        }
                        </association>
                }
                </associations> 
             }
        </community>
    
    (: check community to be saved is scheme compliant :)
    let $check                  :=
        if ($community) then (
            let $r  := validation:jaxv-report($community, (doc($setlib:strDecorCore || '/DECORmycommunity.xsd')))
            return
                if ($r/status='invalid') then
                    error($errors:BAD_REQUEST, 'MyCommunity is not schema compliant: ' || serialize($r))
                else ()
        )
        else ()
    
    (: now create or update the community :)
    (:let $storedResource          := 
        if ($storedCommunity) then util:collection-name($decor) || '/' || util:document-name($storedCommunity) else ():)
        
    let $saveCommunity          := 
        if ($storedCommunity) then
        (
            (: update the community :)
            update value $storedCommunity/@displayName with $community/@displayName,
            update delete $storedCommunity/desc,
            for $desc in $community/desc return update insert $desc preceding $storedCommunity/access,
            update replace $storedCommunity/access with $community/access, 
            update replace $storedCommunity/prototype with $community/prototype, 
            if ($storedCommunity/associations) then ()
               else update insert <associations/> following $storedCommunity/prototype,
            update replace $storedCommunity/associations with $community/associations
               
        )
        else (
            (: store the new community :)
            let $newResource            := xmldb:store(util:collection-name($decor), 'community-' || $name || '.xml', $community)
            (:let $chown                  := sm:chown($storedCommunity, 'admin:decor'):)
            let $chmodCommunity         := sm:chmod($newResource, 'rw-rw-r--')
            return ()
        )    
    
    return mcapi:getMyCommunityResponse($community)
};
