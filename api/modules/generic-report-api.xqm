xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:  
    Create List and Retrieve Generic and Sanity Reports
    
    Generic and Sanity Report Types:
      PSD   List of primary and secondary datasets used in scenarios of a project
      
:)
(:~ generic report API creates generic reports for a Project (CADTS-aware) :)
module namespace grepapi            = "http://art-decor.org/ns/api/generic-report";


import module namespace roaster     = "http://e-editiones.org/roaster";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

import module namespace utillib     = "http://art-decor.org/ns/api/util" at "library/util-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "library/settings-lib.xqm";
import module namespace decorlib    = "http://art-decor.org/ns/api/decor" at "library/decor-lib.xqm";

declare namespace json      = "http://www.json.org";
declare namespace rest      = "http://exquery.org/ns/restxq";
declare namespace resterr   = "http://exquery.org/ns/restxq/error";
declare namespace http      = "http://expath.org/ns/http-client";
declare namespace output    = "http://www.w3.org/2010/xslt-xquery-serialization";


declare %private variable $grepapi:PROGRESS-REPORT-10                := 'Checking request parameters ...';
declare %private variable $grepapi:PROGRESS-DATASET-20               := 'Processing datasets ...';
declare %private variable $grepapi:PROGRESS-SCENARIO-30              := 'Processing scenarios ...';
declare %private variable $grepapi:PROGRESS-SUMMARY-90               := 'Summarization ...';

declare %private variable $grepapi:supportedLanguages   := ('en-US','en-GB','nl-NL','de-DE','fr-FR','it-IT','pl-PL');


(:~ Get a generic report (list) for a given project
    @param $projectPrefix     - required path part which may be its prefix or its oid
    @return as JSON
    @since 2024-04-30
:)
declare function grepapi:getGenericReport($request as map(*)) {
    
    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    let $specificref            := $request?parameters?reportId[string-length() gt 0]
    let $retrieveReport       := $request?parameters?retrieve = true()
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
        
    let $decor                  := if (utillib:isOid($project)) then utillib:getDecorById($project) else utillib:getDecorByPrefix($project)
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $project, ''', does not exist.'))
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-PROJECT)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to create a runtime version of project ', $project, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if (empty($specificref)) then () else if (matches($specificref, '^([1-9][0-9]*(\.[0-9]+)*)|([0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12})$', 'i')) then () else (
            error($errors:BAD_REQUEST, 'Parameter reportId SHALL be an oid or uuid. Found: ' || $specificref)
        )
    
    let $results                := grepapi:getGenericReport($projectPrefix, $specificref, $retrieveReport)
     
    return
        if (empty($results)) then 
            roaster:response(404, ())
        else (
            $results
        )
};

(:~ 
   Get list of generic reports or specific report

   @param $project - required. project prefix or id of the project
   @return list of generic reports or specific report
:)
declare function grepapi:getGenericReport($projectPrefix as xs:string*, $specificref as xs:string?, $retrieve as xs:boolean?) {
    if ($retrieve) then (
        let $fullReport := $setlib:colDecorVersion//genericReport[@for = $projectPrefix][@as = $specificref]
        return
            for $result in $fullReport
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result)
                }
    ) else (
        let $reportrequestF        :=
            if (string-length($specificref) > 0)
            then
                collection($setlib:strDecorScheduledTasks)/generic-report-request[@for = $projectPrefix][@as = $specificref]
            else
                collection($setlib:strDecorScheduledTasks)/generic-report-request[@for = $projectPrefix]
        let $genericReportF              :=
            if (string-length($specificref) > 0)
            then
                $setlib:colDecorVersion//genericReport[@for = $projectPrefix][@as = $specificref]
            else
                $setlib:colDecorVersion//genericReport[@for = $projectPrefix]
    
        let $count                  := count($genericReportF | $reportrequestF)
        
        let $genericReportF         :=
            for $c in $genericReportF
            let $as   := $c/@as
            group by $as
            order by $c/@on descending
            return $c[1]
        
        let $results                :=
            <list artifact="GENERICREPORT" current="{$count}" total="{$count}" all="{$count}" lastModifiedDate="{current-dateTime()}">
            {
                for $c in $reportrequestF
                order by $c/@on descending
                return
                    element {name($c)}
                    {
                        $c/@*
                    }
                ,
                for $c at $count in $genericReportF
                return
                    <genericReport>
                    {
                        $c/@* except $c/@status,
                        if ($count < 3) then $c/@status else attribute {'status'} {'retired'},
                        $c/dir
                    }
                    </genericReport>
            }
            </list>
    
        for $result in $results
        return
            element {name($result)} {
                $result/@*,
                namespace {"json"} {"http://www.json.org"},
                utillib:addJsonArrayToElements($result/*)
            }
    )
};


(:~ Create new generic report and return the listing of generic reports

@param $project - required. project prefix or id of the project to create the environment for
@return xml listing of generic reports
:)
declare function grepapi:createGenericReportRequest($request as map(*)) {
    
    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    let $check                  :=
        if (empty($request?body)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
        
    let $data                   := utillib:getBodyAsXml($request?body, 'report-parameters', ())
        
    let $decor                  := if (utillib:isOid($project)) then utillib:getDecorById($project) else utillib:getDecorByPrefix($project)
    
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $project, ''', does not exist.'))
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-PROJECT)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to create a runtime version of project ', $project, '. You have to be an active author in the project.'))
        )
    
    let $projectPrefix          := $decor/project/@prefix

    let $now                    := substring(xs:string(current-dateTime()), 1, 19)
    let $stamp                  := util:uuid()
    let $author                 := $decor/project/author[@username = $authmap?name]
    let $author                 := if (empty($author)) then $authmap?name else $author
    let $language               := $decor/project/@defaultLanguage
    let $timeStamp              := 'development'
    let $filters                := $data/filters
    
    let $check                  :=
        if (count($projectPrefix) = 1) then () else (
            error($errors:BAD_REQUEST, 'Project parameter ' || $project || ' SHALL refer to exactly one project. Found: ' || count($decor))
        )
        
    let $reporttype             := $data/@type[string-length() gt 0]
    (:let $reporttype             := 'PSD':)
    let $check                  :=
        if ($reporttype = 'PSD') then () else (
            error($errors:BAD_REQUEST, 'Report type parameter ' || $reporttype || ' SHALL ba a valid report type.')
        )

    (:
        create a request file named like demo10-20240430164837.xml in scheduled tasks folder containing:
        <generic-report-request
           uuid="b140184f-c384-41fa-8018-4fc384c1faa5"
           type="PSD"
           for="demo10-"
           on="2024-04-30T16:48:34"
           as="b140184f-c384-41fa-8018-4fc384c1faa5"
           by="dr Kai Heitmann"
           language="en-US"
           progress="Added to process queue ..."
           progress-percentage="0"
         />
    :)
    let $generic-report-request  := 
        <generic-report-request
            uuid="{$stamp}"
            type="{$reporttype}"
            for="{$projectPrefix}"
            on="{$now}"
            as="{$stamp}"
            by="{$author}"
            language="{$language}"
            progress="Added to process queue ..."
            progress-percentage="0">
        {
            if ($filters) then (
                attribute filterlabel {($filters/@label, $filters/@id)[1]} |
                attribute transactions {$filters/transaction/concat(@ref, '--', @flexibility)}
            ) else (),
            $filters
        }
        </generic-report-request>

    let $write                  := xmldb:store($setlib:strDecorScheduledTasks, $projectPrefix || replace($now, '\D', '') || '.xml', $generic-report-request)
    let $tt                     := sm:chmod($write, 'rw-rw----')
    
    return
        grepapi:getGenericReport($projectPrefix, (), ())
};


(:~ Process generic report requests :)
declare function grepapi:process-generic-report-request($threadId as xs:string, $request as element(generic-report-request)) {   
    let $xsltParameters     :=  <parameters></parameters>
    let $logsignature           := 'grepapi:process-generic-report-request'
    let $mark-busy              := update insert attribute busy {'true'} into $request
    let $progress               := 
        if ($request/@progress) then (
            update value $request/@progress with $grepapi:PROGRESS-REPORT-10 
        )
        else (
            update insert attribute progress {$grepapi:PROGRESS-REPORT-10} into $request
        )
    let $progress               := 
        if ($request/@progress-percentage) then (
            update value $request/@progress-percentage with round(10)
        )
        else (
            update insert attribute progress-percentage {round(10)} into $request
        )
    let $timeStamp              := current-dateTime()
    let $projectPrefix          := $request/@for[not(. = '*')]
    let $decor                  := if (empty($projectPrefix)) then () else utillib:getDecorByPrefix($projectPrefix)
    let $language               := $request/@language[not(. = '')]
    let $reportCollection       := concat($setlib:strDecorVersion,'/',substring($projectPrefix, 1, string-length($projectPrefix) - 1),'/development')

    return
        if (count($decor) = 1) then (
        
            let $datasets2analyze       := $decor/datasets/dataset[@statusCode=('draft','final','pending')]
            let $allTransactions        := $decor/scenarios/scenario/transaction/transaction[representingTemplate][@statusCode=('draft','final','pending')]
            let $filters                := $request/filters
            let $transactions2analyze   := if (empty($filters/*)) then $allTransactions else (
                 for $t in $filters/transaction
                 return
                    if ($allTransactions[@id=$t/@ref][@effectiveDate=$t/@flexibility]) then $allTransactions[@id=$t/@ref][@effectiveDate=$t/@flexibility] else ()
            )

            
            let $report1 := 
                <genericReport type="{$request/@type}" for="{$request/@for}" on="{$request/@on}" as="{$request/@as}" by="{$request/@by}" status="active">
                    {
                        update value $request/@progress with $grepapi:PROGRESS-DATASET-20,
                        update value $request/@progress-percentage with round(20)
                    }
                    <datasets count="{count($datasets2analyze)}" total="{count($datasets2analyze)}">
                    {
                        (: do datasets :)
                        for $d in $datasets2analyze
                        return
                            <dataset>
                            {
                                $d/@*,
                                $d/name,
                                $d/desc
                            }
                            </dataset>
                    }
                    </datasets>
                    {
                        update value $request/@progress with $grepapi:PROGRESS-SCENARIO-30,
                        update value $request/@progress-percentage with round(30)
                    }
                    <transactions count="{count($transactions2analyze)}" total="{count($allTransactions)}">
                    {
                         (: do transactions :)
                         for $t at $pos in $transactions2analyze
                         let $update := update value $request/@progress-percentage with round(30 + (($pos * 57) div count($transactions2analyze)))
                         return
                            if (empty($t/representingTemplate)) then ()
                            else
                                <transaction>
                                {
                                    $t/@*,
                                    $t/name,
                                    $t/desc,
                                    grepapi:processRepresentingTemplate($decor, $t, $language)/*
                                }
                                </transaction>
                    }
                    </transactions>
                </genericReport>
                
            let $report2 :=
                    <summary>
                    {
                        update value $request/@progress with $grepapi:PROGRESS-SUMMARY-90,
                        update value $request/@progress-percentage with round(90)
                    }
                    {
                        attribute primarySourceDatasets { count($report1/transactions/transaction/primarySourceDataset) },
                        attribute secondarySourceDatasets { count($report1/transactions/transaction//secondarySourceDataset) }   
                    }
                    </summary>
             
            let $theGenericReport :=
                <genericReport>
                {
                    $report2/@*,
                    $report1/@*,
                    $report1/*
                }
                </genericReport>
                
            return (
            
                if (not(xmldb:collection-available($reportCollection))) then
                    try {
                        let $ttt    := xmldb:create-collection($setlib:strDecorVersion, concat(substring($projectPrefix, 1, string-length($projectPrefix) - 1),'/development'))
                        let $tt     := try { sm:chmod($ttt, 'rwxrwsr-x') } catch * {()}
                        return $ttt
                    } 
                    catch * {
                        <error>+++ Create collection failed '{concat(substring($projectPrefix, 1, string-length($projectPrefix) - 1),'/development')}': {$err:code}: {$err:description}</error>
                    }
                else()
                ,
                xmldb:store($reportCollection,concat($projectPrefix,'generic-report-',$request/@as,'.xml'), $theGenericReport)
                ,
                xmldb:remove(util:collection-name($request), util:document-name($request))
                
            )                
        )
        else (
            let $message                := 'ERROR. Found ' || count($decor) || ' projects for prefix ' || $projectPrefix || '. Expected: 1. Compile id: ' || $request/@as
            let $log                    := utillib:log-scheduler-event('error', $threadId, $logsignature, $message)
            let $progress               := update value $request/@progress with $message
            
            return ()
        )

};


(:~ Deletes generic report

@param $project - required. project prefix or id of the project
@param $specificref - required. reference to the runtime environmnent
@return xml listing of environments
:)
declare function grepapi:deleteGenericReport($request as map(*)) {
    
    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    let $specificref            := $request?parameters?reportId[string-length() gt 0]
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    let $check                  :=
        if (matches($specificref, '^([1-9][0-9]*(\.[0-9]+)*)|([0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12})$', 'i')) then () else (
            error($errors:BAD_REQUEST, 'Parameter compilationId SHALL be an oid or uuid. Found: ' || $specificref)
        )
    
    let $decor                  := if (utillib:isOid($project)) then utillib:getDecorById($project) else utillib:getDecorByPrefix($project)
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if (count($projectPrefix) = 1) then () else (
            error($errors:BAD_REQUEST, 'Project parameter ' || $project || ' SHALL refer to exactly one project. Found: ' || count($decor))
        )
    
    let $part1                  := concat('xmldb:exist://', $setlib:strDecorVersion, '/', substring($projectPrefix, 1, string-length($projectPrefix) - 1), '/development/')
    let $tmp1                   := 
        if (xmldb:collection-available($part1)) then (
            if (doc-available(concat($part1, $projectPrefix,'generic-report-', $specificref, '.xml'))) then xmldb:remove($part1, concat($projectPrefix,'generic-report-', $specificref, '.xml')) else ()
        )
        else () 
    
    let $part1                  := collection($setlib:strDecorScheduledTasks)/generic-report-request[@for = $projectPrefix][@as = $specificref]
    let $tmp3                   := if ($part1) then xmldb:remove(util:collection-name($part1), util:document-name($part1)) else ()
    
    return
        roaster:response(204, ())
};


(:
    private functions
    =================
:)

declare %private function grepapi:doReport($decor as element()) as element() {
    (: do the generic report :)
    let $projectId              := ($decor/project/@id)[1]
    let $projectPrefix          := ($decor/project/@prefix)[1]
    return
        <genericReport project="{$projectId}" prefix="{$projectPrefix}"/>
};

declare %private function grepapi:processRepresentingTemplate($decor as element(), $rt as element(), $language as xs:string) as element() {
    
    let $fulltransactiondataset := utillib:getFullDataset($rt/@id, $rt/@effectiveDate, (), (), $language, (), true(), ())
    
    let $result :=
        for $c in $rt/representingTemplate/concept       
        let $cinds := $fulltransactiondataset//concept[@id=$c/@ref][@effectiveDate=$c/@flexibility]
        return
            <concept>
            {
                $c/@*,
                if (empty($cinds)) then <error type="conceptNotFound" id="{$c/@ref}" effectiveDate="{$c/@flexibility}"/>
                else (
                    if ($cinds/inherit) then (
                        (: <inherit ref="2.16.840.1.113883.3.1937.777.28.2.508" effectiveDate="2024-01-22T11:00:00" 
                             prefix="prsb03-" datasetId="2.16.840.1.113883.3.1937.777.28.1.189" datasetEffectiveDate="2024-01-22T11:00:00"
                             datasetStatusCode="final" datasetVersionLabel="v2" iType="group" iStatusCode="final"
                             iEffectiveDate="2024-01-22T11:00:00" refdisplay="prsb03-dataelement-508"/> :)
                        $cinds/inherit,
                        if ($cinds/inherit[@datasetId][@datasetEffectiveDate]) then (
                            <tie type="inherited">
                            {
                                attribute id {$cinds/inherit/@datasetId},
                                attribute effectiveDate {$cinds/inherit/@datasetEffectiveDate},
                                attribute statusCode {$cinds/inherit/@datasetStatusCode},
                                attribute versionLabel {$cinds/inherit/@datasetVersionLabel},
                                $decor//dataset[@id=$cinds/inherit/@datasetId][@effectiveDate=$cinds/inherit/@datasetEffectiveDate]/(name, desc)
                            }
                            </tie>
                        ) else <error type="inheritNoDataSetInfo" id="{$cinds/inherit/@ref}" effectiveDate="{$cinds/inherit/@effectiveDate}"/>
                    ) 
                    else (),
                    if ($cinds/relationship) then (
                        (:  <relationship type="SPEC" ref="2.16.840.1.113883.2.6.60.3.2.6.20306" flexibility="2019-02-01T17:12:50" 
                             prefix="aktin-" datasetId="2.16.840.1.113883.2.6.60.3.1.6" datasetEffectiveDate="2018-08-17T00:00:00" 
                             datasetStatusCode="final" datasetVersionLabel="v2020.1" iType="item" iStatusCode="final" 
                             iEffectiveDate="2019-02-01T17:12:50" iVersionLabel="v2020.1" refdisplay="nap-dataelement2020(1)-20306">
                                 <name language="de-DE">ASA-Score</name>
                             </relationship>  :)
                        $cinds/relationship,
                        for $tir in $cinds/relationship
                        return
                            if ($tir[@datasetId][@datasetEffectiveDate]) then (
                                <tie type="relationship" as="{$tir/@type}">
                                {
                                    attribute id {$tir/@datasetId},
                                    attribute effectiveDate {$tir/@datasetEffectiveDate},
                                    attribute statusCode {$tir/@datasetStatusCode},
                                    attribute versionLabel {$tir/@datasetVersionLabel},
                                    $decor//dataset[@id=$tir/@datasetId][@effectiveDate=$tir/@datasetEffectiveDate]/(name, desc)
                                }
                                </tie>
                             ) else <error type="relationshipNoDataSetInfo" id="{$tir/@ref}" effectiveDate="{$tir/@flexibility}"/>
                    )
                    else (),
                    <sourceDatasetConcept>
                    {
                        $cinds/(* except concept)
                    }
                    </sourceDatasetConcept>
                )
            }
            </concept>
    
    let $total         := count($rt/representingTemplate/concept)
    let $ties          := count($result//tie)
    let $inherited     := count($result//tie[@type='inherited'])
    let $relationships := count($result//tie[@type='relationship'])
    
    let $fullprimarysourcedataset := $decor//dataset[@id=$rt/representingTemplate/@sourceDataset][@effectiveDate=$rt/representingTemplate/@sourceDatasetFlexibility]
    (: utillib:getFullDataset($rt/representingTemplate/@sourceDataset, $rt/representingTemplate/@sourceDatasetFlexibility, (), (), $language, (), true(), ()) :)

    return
        <result>
        {
            <primarySourceDataset total="{$total}" inherited="{$inherited}" relationships="{$relationships}">
            {
                    attribute id { $rt/representingTemplate/@sourceDataset },
                    attribute effectiveDate { $rt/representingTemplate/@sourceDatasetFlexibility },
                    $fullprimarysourcedataset/@statusCode,
                    $fullprimarysourcedataset/@versionLabel,
                    $fullprimarysourcedataset/name,
                    $fullprimarysourcedataset/desc,
                    <ties>{$result//tie}</ties>,
                    for $s in $result//tie
                    let $cid := concat(($s/@id)[1], ($s/@effectiveDate)[1])
                    group by $cid
                    return
                       <secondarySourceDataset
                        inherited="{count($result//tie[@type='inherited'][concat(@id,@effectiveDate) = $cid])}"
                        relationships="{count($result//tie[@type='relationship'][concat(@id,@effectiveDate) = $cid])}">
                       {
                            attribute id { ($s/@id)[1] },
                            attribute effectiveDate { ($s/@effectiveDate)[1] },
                            attribute statusCode { ($s/@statusCode)[1] },
                            ($s/name)[1],
                            ($s/desc)[1]
                       }
                       </secondarySourceDataset>
            }
            </primarySourceDataset>,
            <concepts>
            {
                $result
            }
            </concepts>
       }
       </result>

};