xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:~ Server API allows read, create, update of ART-DECOR Server properties :)
module namespace serverapi          = "http://art-decor.org/ns/api/server";

import module namespace roaster     = "http://e-editiones.org/roaster";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "library/settings-lib.xqm";
import module namespace utillib     = "http://art-decor.org/ns/api/util" at "library/util-lib.xqm";
import module namespace permlib     = "http://art-decor.org/ns/api/permissions" at "library/permissions-lib.xqm";
import module namespace decorlib    = "http://art-decor.org/ns/api/decor" at "library/decor-lib.xqm";

declare namespace http              = "http://expath.org/ns/http-client";
declare namespace json              = "http://www.json.org";
declare namespace expath            = "http://expath.org/ns/pkg";

(:~ The collection holding valid menu-structures for ART :)
declare variable $serverapi:strServerMenuPath            := $setlib:strServerMenuPath;
(:~ The default art menu template an ART-DECOR instance unless configured otherwise :)
declare variable $serverapi:strDefaultServerMenu         := $setlib:strDefaultServerMenu;
(:~ The default logo name for an ART-DECOR instance unless configured otherwise :)
declare variable $serverapi:strDefaultServerLogo         := $setlib:strDefaultServerLogo;
(:~ The default logo url for an ART-DECOR instance unless configured otherwise :)
declare variable $serverapi:strDefaultServerLogoUrl      := $setlib:strDefaultServerLogoUrl;
(:~ The list of users that may be regarded as 'system-level' users :)
declare variable $serverapi:arrSystemLevelUsers          := $setlib:arrSystemLevelUsers;

declare %private variable $serverapi:LOG-COLL            := $setlib:strDecorScheduledTasks;
declare %private variable $serverapi:SCAN-COLL           := collection($serverapi:LOG-COLL);
declare %private variable $serverapi:LOCALES             := $setlib:strApiLocales;

(:~ Retrieves server info like defaultLanguage, statistics, installed packages and database version
@return serverinfo object
@since 2020-05-03
:)
declare function serverapi:getServerInfo($request as map(*)) {
let $projects       := $setlib:colDecorData/decor
let $authmap        := $request?user
let $cvs            := count($projects/terminology/valueSet)
let $ccs            := count($projects/terminology/codeSystem)
let $ccda           := count($projects/rules/template)
let $cfhir          := count($projects/rules/profile)
let $allPackages    := collection($setlib:root)/expath:package
(: user info check, no authentication needed :)
let $userInfos      := collection($setlib:strUserInfo)//user
let $docServerInfo  := doc($setlib:strServerInfo)

let $now            := current-dateTime()

return
  <serverinfo defaultLanguage="{serverapi:getServerLanguage()}" xmlns:json="http://www.json.org">
  {
      utillib:addJsonArrayToElements(
          <statistics>
              <projects count="{count($projects/project)}"/>
              <datasets count="{count($projects/datasets/dataset)}" concepts="{count($projects/datasets/dataset//concept)}"/>
              <scenarios count="{count($projects/scenarios/scenario)}" transactions="{count($projects/scenarios//transaction)}"/>
              <terminologies count="{$cvs + $ccs}" valueSets="{$cvs}" codeSystems="{$ccs}"/>
              <rules count="{$ccda + $cfhir}" template="{$ccda}" profile="{$cfhir}"/>
              <issues count="{count($projects/issues/issue  )}" open="{count($projects/issues/issue[@statusCode = ('open')])}"/>
              <releases count="{count($projects/project/release)}"/>
          </statistics>
          |
          <cache>
          {
            if ($setlib:colDecorCache//cachedBuildingBlockRepositories) then (
                (:
                    tell that cache is true and report number of cached projects in @cached,
                    number of errorneous cached projects in @unreachable,
                    the actual time of the last caching in time @time and
                    how fresh it is in @fresh expressed in minutes so that receivers can 
                    construct a traffic light on how fresh the cache is
                :)
                attribute {'cache'}        { 'true' },
                attribute {'repos'}        { $setlib:colDecorCache//cachedBuildingBlockRepositories/@count },
                attribute {'objects'}      { count($setlib:colDecorCache//cacheme//(template[@id]|valueSet[@id])) },
                attribute {'unreachable'}  { $setlib:colDecorCache//cachedBuildingBlockRepositories/@errors },
                attribute {'time'}         { $setlib:colDecorCache//cachedBuildingBlockRepositories/@time },
                if ($setlib:colDecorCache//cachedBuildingBlockRepositories/@time castable as xs:dateTime) then (
                    attribute {'fresh'}    { 
                        format-number(
                            ($now - xs:dateTime($setlib:colDecorCache//cachedBuildingBlockRepositories/@time)) 
                                div xs:dayTimeDuration("PT1M"),
                            "0"
                        )
                    }
                ) else (
                    attribute {'fresh'}    { '-1' }
                )
            ) else (
                attribute {'cache'}        { 'false' }
            )
          }
          </cache>
          |
          <projects>
          {
             for $p in $projects/project
             let $private   := $p/../@private = 'true'
             let $include   := 
                if (empty($authmap)) then not($private) else 
                if (not($private)) then true() else decorlib:isAuthorP($authmap, $p/..)
             let $name      := $p/name[@language=$p/@defaultLanguage]
             order by lower-case($name)
             return
                if ($include) then 
                    <project
                        id="{$p/@id}" 
                        prefix="{$p/@prefix}" 
                        defaultLanguage="{$p/@defaultLanguage}" 
                        lastmodified="{xmldb:last-modified(util:collection-name($p), util:document-name($p))}" 
                        experimental="{$p/@experimental = 'true'}" 
                        repository="{$p/../@repository = 'true'}" 
                        private="{$private}"
                        author="{if (empty($authmap)) then false() else exists($p/author[@username = $authmap?name][not(@active = 'false')])}">
                    {
                        $p/name
                    }
                    </project>
                else ()
          }
          </projects>
          |
          <packages>
          {
                for $package in $allPackages
                let $parentCollection := substring-after(util:collection-name($package),$setlib:root)
                order by lower-case($parentCollection)
                return
                <package title="{normalize-space($package//expath:title)}" collection="{$parentCollection}">
                {
                    $package/@version | $package/@abbrev | $package/@short
                }
                </package>
          }
          </packages>
          |
          <database version="{system:get-version()}" build="{system:get-build()}" revision="{system:get-revision()}" uptime="{system:get-uptime()}"/>
          |
          <serviceservers>
          {
             $docServerInfo/server-info/service-servers/*
          }
          </serviceservers>
          |
          <terminologybrowserlanguages>
          {
             if ($docServerInfo/server-info/terminology-browser-languages)
             then $docServerInfo/server-info/terminology-browser-languages/@*
            else (
                attribute preferred { 'en-US' },
                attribute selected  { 'en-US de-DE nl-NL' }
            )
          }
          </terminologybrowserlanguages>
         |
         <usersinfo>
         {
            if (count($userInfos) > 0)
            then (
                if (count($userInfos//displayName[string-length() gt 0]))
                then attribute artdatavalid { 'true' }
                else attribute artdatavalid { 'false' },
                attribute activelogins { count($userInfos//logins[login[@ends > $now]]) }
            ) else (
                attribute artdatavalid { 'unknown' },
                attribute activelogins { -1 }
            )
          }
          </usersinfo>
         
      )
  }
  </serverinfo>
};

(:~ Retrieves server DECOR info derived from the DECOR.xsd

@return decorTypes object as JSON
@since 2020-05-03
:)
declare function serverapi:getServerDecorTypes($request as map(*)) {
    let $recreate       := $request?parameters?recreate = true()
    
    return
    utillib:getDecorTypes($recreate)
};

(:~ Retrieves server DECOR info derived from the DECOR.xsd

@return decorType object as JSON
@since 2020-05-03
:)
declare function serverapi:getServerDecorType($request as map(*)) {
    let $recreate       := $request?parameters?recreate = true()
    let $results        := utillib:getDecorTypes($recreate)/*[name() = $request?parameters?name]
    
    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple hits for type name '", string-join($request?parameters?name, ' '), "'. Expected 0..1. Alert your administrator as this should not be possible."))
        )
        else (
            for $result in $results
            return
                element {name($result)} {
                    $result/(@* except @json:array),
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
        )
};

(:~ Retrieves DECOR status code transitions map

@return statusMap object as JSON
@since 2021-03-19
:)
declare function serverapi:getDecorStatusMaps($request as map(*)) {
    <statusMaps xmlns:json="http://www.json.org">
        <ItemStatusCodeLifeCycle json:array="true">
        {
            for $k in map:keys($utillib:itemstatusmap)[not(. = '')]
            for $e in map:get($utillib:itemstatusmap, $k)
            return
                element {$k} {
                    attribute json:array {'true'},
                    $e
                }
        }
        </ItemStatusCodeLifeCycle>
        <TemplateStatusCodeLifeCycle json:array="true">
        {
            for $k in map:keys($utillib:templatestatusmap)[not(. = '')]
            for $e in map:get($utillib:templatestatusmap, $k)
            return
                element {$k} {
                    attribute json:array {'true'},
                    $e
                }
        }
        </TemplateStatusCodeLifeCycle>
        <ReleaseStatusCodeLifeCycle json:array="true">
        {
            for $k in map:keys($utillib:releasestatusmap)[not(. = '')]
            for $e in map:get($utillib:releasestatusmap, $k)
            return
                element {$k} {
                    attribute json:array {'true'},
                    $e
                }
        }
        </ReleaseStatusCodeLifeCycle>
        <IssueStatusCodeLifeCycle json:array="true">
        {
            for $k in map:keys($utillib:issuestatusmap)[not(. = '')]
            for $e in map:get($utillib:issuestatusmap, $k)
            return
                element {$k} {
                    attribute json:array {'true'},
                    $e
                }
        }
        </IssueStatusCodeLifeCycle>
        <CodedConceptStatusCodeLifeCycle json:array="true">
        {
            for $k in map:keys($utillib:codedconceptstatusmap)[not(. = '')]
            for $e in map:get($utillib:codedconceptstatusmap, $k)
            return
                element {$k} {
                    attribute json:array {'true'},
                    $e
                }
        }
        </CodedConceptStatusCodeLifeCycle>
    </statusMaps>
};

declare function serverapi:getServerSettings($request as map(*)) {

    let $result     := doc($setlib:strServerInfo)/server-info
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/(* except (system-users|service-servers)))
        }
};

declare function serverapi:patchServerSettings($request as map(*)) {

    let $authmap                := $request?user
    let $data                   := utillib:getBodyAsXml($request?body, 'parameters', ())
    
    (:let $s                      := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data) :)
    
    let $check                  :=
        if ($data) then () else (
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        )
    
    let $return                 := serverapi:patchServerSettings($authmap, $data)
    return (
        roaster:response(200, $return)
    )
};

(:~ Retrieves the list of server building block repositories local and external (from the listed of trusted BBRs). 
No authentication, parameters or request-body required or possible

@return buildingBlockRepositories object with an array of buildingBlockRepository objects :)
declare function serverapi:getServerRepositories($request as map(*)) {

    let $authmap                := $request?user
    let $uri                    := $request?parameters?uri[string-length() gt 0]
    let $type                   := $request?parameters?type[string-length() gt 0]
    
    let $check                  :=
        if (empty($uri)) then () else if (empty($authmap)) then
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else
        if ($authmap?groups = 'dba') then () else (
            error($errors:FORBIDDEN, 'You need to be dba for this function with parameter uri')
        )
    
    let $results                :=    
        if ($uri) then (
            serverapi:getRepositoriesFromServer($uri)
        )
        else if ($type='internal') then (
            serverapi:getServerInternalRepositories()
        )
        else if ($type='external') then (
            serverapi:getServerExternalRepositories()
        )
        else (
            serverapi:getServerAllRepositories()
        )

    let $count                  := count($results/*)
    let $max                    := $count
    let $allcnt                 := $count
    
    return
        <list artifact="BBR" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            $results/@used-url
            ,
            utillib:addJsonArrayToElements($results/*)
        }
        </list>

};

(:~ Central logic for updating an existing issue displayName|priority|type, add|remove object associations, add|replace tracking|assignment

@param $authmap         - required. Map derived from token
@param $request-body    - required. DECOR xml element parameter elements each containing RFC 6902 compliant contents
@return issue object as xml with json:array set on elements
:)
declare function serverapi:patchServerSettings($authmap as map(*), $request-body as element(parameters)) as element(server-info) {
    let $singleValuePaths       := (
        '/url-art-decor-deeplinkprefix',
        '/url-art-decor3-deeplinkprefix',
        '/url-art-decor-services', 
        '/url-fhir-services', 
        '/url-fhir-canonical-base',
        '/url-art-decor-api', 
        '/default-fhir-version', 
        '/defaultLanguage', 
        '/xformStylesheet', 
        '/art-menu-template', 
        '/orbeon-version', 
        '/buildingBlockServer'
    )
    let $validPaths             := (
        $singleValuePaths, 
        '/art-server-logo', 
        '/ids', 
        '/buildingBlockRepository', 
        '/service-server'
    )
(:
<server-info>
    <parameter op="replace" path="/url-art-decor-deeplinkprefix" value="new value"/>
    <parameter op="replace" path="/url-art-decor3-deeplinkprefix" value="new value"/>
    <parameter op="replace" path="/url-art-decor-services" value="new value"/>
    <parameter op="replace" path="/url-fhir-services" value="new value"/>
    <parameter op="replace" path="/url-fhir-caonical-base" value="new value"/>
    <parameter op="replace" path="/url-art-decor-api" value="new value"/>
    <parameter op="replace" path="/default-fhir-version" value="new value"/>
    <parameter op="replace" path="/defaultLanguage" value="new value"/>
    <parameter op="replace" path="/xformStylesheet" value="new value"/>
    <parameter op="replace" path="/art-menu-template" value="new value"/>
    <parameter op="replace" path="/orbeon-version" value="new value"/>
    <parameter op="replace" path="/art-server-logo">
      <value>
        <art-server-logo href="https://art-decor.org">art-decor-logo40.png</art-server-logo>
      </value>
    </parameter>
    <parameter op="replace" path="/ids">
        <value>
            <type code="server" displayName="Base Server ID"/>
            <baseId id="" type="server" default="true"/>
        </value>
    </parameter>
    <parameter op="replace" path="/buildingBlockServer" value="http://art-decor.org/decor/services/"/>
    <parameter op="replace" path="/buildingBlockRepository">
        <value>
            <buildingBlockRepository url="http://art-decor.org/decor/services/" ident="ad1bbr-" format="decor">
                <name language="en-US">CDA Release 2</name>
                <name language="nl-NL">CDA Release 2</name>
                <name language="de-DE">CDA Release 2</name>
            </buildingBlockRepository>
        </value>
    </parameter>
    <parameter op="replace" path="/service-server">
        <value>
            <server name="build.art-decor.org" apiurl="https://serviceapi.art-decor.org" apiversion="1">
                <has uptimes="true" bbrs="true" adawib="true"/>
            </server>
        </value>
    </parameter>
</server-info>
:)
    let $check                  :=
        if ($authmap?groups = 'dba') then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify server settings. You have to be in the group dba.'))
        )
    let $check                  :=
        if ($request-body[parameter]) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall be an array of ''parameter''.')
        )
    let $unsupportedops         := $request-body/parameter[not(@op = ('add', 'replace', 'remove'))]
    let $check                  :=
        if ($unsupportedops) then
            error($errors:BAD_REQUEST, 'Submitted parameters shall have supported ''op'' value. Found ''' || string-join(distinct-values($unsupportedops/@op), ''', ''') || ''', expected ''add'', ''replace'', or ''remove''') 
        else ()
    let $check                  :=
        for $param in $request-body/parameter
        return
            if ($param[@path = $singleValuePaths]) then (
                if ($param[@value]) then () else (
                    concat('Parameter path ''', $param/@path, ''' SHALL have a value attribute.')
                ),
                if ($param[@path = '/default-fhir-version']) then 
                    if ($param[@value = serverapi:getInstalledFhirServices()] | $param[@value = '']) then () else (
                        concat('Parameter path ''', $param/@path, ''' SHALL have a supported value attribute of an installed FHIR server package. Supported are: ''', string-join(serverapi:getInstalledFhirServices(), ', '), '''')
                    )
                else
                if ($param[@path = '/xformStylesheet']) then 
                    if ($param[@value = serverapi:getServerXSLsArt()]) then () else (
                        concat('Parameter path ''', $param/@path, ''' SHALL have a supported value attribute of an XSL. Supported are: ''', string-join(serverapi:getServerXSLsArt(), ', '), '''')
                    )
                else (),
                if ($param[@path = '/art-menu-template']) then 
                    if ($param[@value = serverapi:getServerMenuTemplates()]) then () else (
                        concat('Parameter path ''', $param/@path, ''' SHALL have a supported value attribute of a menu template. Supported are: ''', string-join(serverapi:getServerMenuTemplates(), ', '), '''')
                    )
                else (),
                if ($param[@path = '/orbeon-version']) then 
                    if ($param[@value = serverapi:getServerOrbeonVersions()/@version]) then () else (
                        concat('Parameter path ''', $param/@path, ''' SHALL have a supported value attribute of an Orbeon version. Supported are: ''', string-join(serverapi:getServerOrbeonVersions()/@version, ', '), '''')
                    )
                else ()
            )
            else
            if ($param[@path = '/art-server-logo']/value[*]) then (
            )
            else
            if ($param[@path = '/ids']) then (
                if ($param/value[type | baseId]) then (
                    for $paramValue in $param/value/type
                    return
                        if ($paramValue[@code[not(. = '')]][@displayName[not(. = '')]]) then () else (
                            concat('Parameter path ''', $param/@path, ''' that targets a type SHALL, on each type, have have a non-empty code and non-empty displayName on each type.')
                        )
                    ,
                    for $paramValue in $param/value/baseId
                    return
                        if ($paramValue[@id = '' or utillib:isOid(@id)][@type = ($param/value/type/@code, serverapi:getServerAllIds()/type/@code)][@default castable as xs:boolean]) then () else (
                            concat('Parameter path ''', $param/@path, ''' that targets a baseId SHALL, on each baseId, have an id that is empty or an OID, and SHALL have a marker ''default'' that is either true or false, and SHALL have a type that matches a defined type.code')
                        )
                    ,
                    for $paramValue in $param/value/*
                    return
                        if ($paramValue[self::type | self::baseId]) then () else (
                            concat('Parameter path ''', $param/@path, ''' SHALL have a value object containing an array of type and/or baseId. Found: ''', name($paramValue), '''')
                        )
                )
                else (
                    concat('Parameter path ''', $param/@path, ''' SHALL have a value object containing an array of type and/or baseId.')
                )
            )
            else
            if ($param[@path = '/buildingBlockRepository']) then (
                if ($param/value[count(buildingBlockRepository) = 1]) then (
                    if ($param/value/buildingBlockRepository[@url[not(. = '' or . = serverapi:getServerURLServices())]]) then () else (
                        concat('Parameter path ''', $param/@path, ''' SHALL have a value object containing exactly one buildingBlockRepository object. This object SHALL have a non-empty url that does not match the current server url for services.')
                    ),
                    if ($param/value/buildingBlockRepository[@ident[not(. = '')] | @format[not(. = '')]]) then () else (
                        concat('Parameter path ''', $param/@path, ''' SHALL have a value object containing exactly one buildingBlockRepository object. This object SHALL have either ident (DECOR project prefix) or format.')
                    ),
                    if ($param/value/buildingBlockRepository[@type = 'external']) then () else (
                        concat('Parameter path ''', $param/@path, ''' SHALL have a value object containing exactly one buildingBlockRepository object. This object SHALL have type=external.')
                    ),
                    if ($param/value/buildingBlockRepository[name[matches(@language, '^[a-z]{2}-[A-Z]{2}$')]]) then () else (
                        concat('Parameter path ''', $param/@path, ''' SHALL have a value object containing exactly one buildingBlockRepository object. This object SHALL have at least one name with language formatted as cc-LL (country-language).')
                    )
                )
                else (
                    concat('Parameter path ''', $param/@path, ''' SHALL have a value object containing exactly one buildingBlockRepository object.')
                )
            )
            else (
                concat('Parameter combination not supported: op=''', $param/@op, ''' path=''', $param/@path, '''', if ($param[@value]) then concat(' value=''', $param/@value, '''') else string-join($param/value/*/name(), ' '), '.')
            )
     let $check                 :=
        if (empty($check)) then () else (
            error($errors:BAD_REQUEST,  string-join ($check, ' '))
        )
    
    let $update                 :=
        for $param in $request-body/parameter
        return
            if ($param[@path = $singleValuePaths][@value]) then (
                switch ($param/@path)
                case '/url-art-decor-deeplinkprefix' return serverapi:setServerURLArt($param/@value)
                case '/url-art-decor3-deeplinkprefix' return serverapi:setServerURLArt3($param/@value)
                case '/url-art-decor-services' return serverapi:setServerURLServices($param/@value)
                case '/url-fhir-services' return serverapi:setServerURLFhirServices($param/@value)
                case '/url-fhir-canonical-base' return serverapi:setServerURLFhirCanonicalBase($param/@value)
                case '/url-art-decor-api' return serverapi:setServerURLArtApi($param/@value)
                case '/default-fhir-version' return serverapi:setServerFhirDefaultVersion($param/@value)
                case '/defaultLanguage' return serverapi:setServerLanguage($param/@value)
                case '/xformStylesheet' return serverapi:setServerXSLArt($param/@value)
                case '/art-menu-template' return serverapi:setServerMenuTemplate($param/@value)
                case '/orbeon-version' return serverapi:setServerOrbeonVersion($param/@value)
                case '/buildingBlockServer' return
                    switch ($param/@op)
                    case 'add' 
                    case 'replace' return serverapi:setServerRepositoryServer(<buildingBlockServer url="{$param/@value}"/>)
                    case 'remove' return serverapi:deleteServerRepositoryServer($param/@value)
                    default return ()
                default return ()
            )
            else
            if ($param[@path = '/art-server-logo']/value) then (
                serverapi:setServerLogoAndUrl(string($param/value/*), string($param/value/*/@href))
            )
            else
            if ($param[@op = ('add', 'replace')][@path = '/ids']/value) then (
                for $type in $param/value/type
                return serverapi:setServerIdType($type/@code, $type/@displayName)
                ,
                for $id in $param/value/baseId
                return serverapi:setServerId($id/@type, $id/@id, xs:boolean($id/@default))
            )
            else
            if ($param[@op = 'remove'][@path = '/ids']/value) then (
                for $id in $param/value/baseId
                return serverapi:deleteServerId($id/@type, $id/@id)
                ,
                for $type in $param/value/type/@code
                return serverapi:deleteServerIdType($type)
            )
            else
            if ($param[@op = ('add', 'replace')][@path = '/buildingBlockRepository']/value/buildingBlockRepository) then (
                for $bbr in $param/value/buildingBlockRepository
                return serverapi:setServerExternalRepository($bbr)
            )
            else
            if ($param[@op = 'remove'][@path = '/buildingBlockRepository']/value/buildingBlockRepository) then (
                for $bbr in $param/value/buildingBlockRepository
                return serverapi:deleteServerExternalRepository($bbr/@url, $bbr/@ident)
            )
            else ()
    
    let $result     := doc($setlib:strServerInfo)/server-info
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/(* except (system-users|service-servers)))
        }
};

(:~ Return all settings
@return server-info element with contents
@since 2014-03-27
:)
declare function serverapi:getServerSettings() as element()? {
    if (serverapi:checkPermissions()) then (
        doc($setlib:strServerInfo)/server-info
    ) else ()
};

(:~ Return all settings from default settings copy

@return server-info element with contents
@since 2014-09-11
:)
declare function serverapi:getServerSettingsDefault() as element() {
    doc($setlib:strServerInfoDefault)/server-info
};

(:~ Save setting

@return nothing or error
@since 2014-03-27
:)
declare function serverapi:saveServerSetting($settings as element()) {
    if (serverapi:checkPermissions()) then (
        let $action := $settings/@action
        return 
        switch ($action)
        case 'save-server-ids' return (
            let $currentids     := serverapi:getServerAllIds()
            let $delete         :=
                for $baseId in $currentids/baseId
                return serverapi:deleteServerId($baseId/@type, $baseId/@id)
            let $delete         :=
                for $type in $currentids/type
                return serverapi:deleteServerIdType($type/@code)
            let $add            :=
                for $type in $settings/ids/type
                return serverapi:setServerIdType($type/@code, $type/@displayName)
            let $add            :=
                for $baseId in $settings/ids/baseId
                return serverapi:setServerId($baseId/@type, $baseId/@id, $baseId/@default='true')
            
            return ()
        )
        case 'save-language' return
            serverapi:setServerLanguage($settings/defaultLanguage)
        case 'save-server-api-url' return
            serverapi:setServerURLArtApi($settings/url-art-decor-api)
        case 'save-server-url' return
            serverapi:setServerURLArt($settings/url-art-decor-deeplinkprefix)
        case 'save-server-url3' return
            serverapi:setServerURLArt3($settings/url-art-decor3-deeplinkprefix)
        case 'save-services-url' return
            serverapi:setServerURLServices($settings/url-art-decor-services)
        case 'save-fhir-url' return
            serverapi:setServerURLFhirServices($settings/url-fhir-services)
        case 'save-fhir-canonical-base' return
            serverapi:setServerURLFhirCanonicalBase($settings/url-fhir-canonical-base)
        case 'save-fhir-default-version' return
            serverapi:setServerFhirDefaultVersion($settings/default-fhir-version)
        case 'save-server-xsl' return
            serverapi:setServerXSLArt($settings/xformStylesheet)
        case 'save-server-menu-template' return
            serverapi:setServerMenuTemplate($settings/art-menu-template)
        case 'save-server-logo-and-url' return
            serverapi:setServerLogoAndUrl($settings/art-server-logo, $settings/art-server-logo/@href)
        case 'save-repository-servers' return (
            let $currentsvrs := serverapi:getServerRepositoryServers()
            let $delete      := 
                for $svr in $currentsvrs/buildingBlockServer 
                return serverapi:deleteServerRepositoryServer($svr/@url)
            return
                try {
                    for $svr in $settings/externalBuildingBlockRepositoryServers/buildingBlockServer 
                    return serverapi:setServerRepositoryServer($svr)
                }
                catch * {
                    (: one of the new servers returned an error. restore what we had and rethrow our error :)
                    let $delete     :=
                        for $svr in serverapi:getServerRepositoryServers()/buildingBlockServer
                        return serverapi:deleteServerRepositoryServer($svr/@url)
                    let $add        :=
                        for $svr in $currentsvrs/buildingBlockServer
                        return serverapi:setServerRepositoryServer($svr)
                    return
                    error(QName($err:module,$err:code),$err:description)
                }
        )
        case 'save-repositories' return (
            let $currentbbrs := serverapi:getServerExternalRepositories()
            let $delete      := 
                for $bbr in $currentbbrs/buildingBlockRepository 
                return serverapi:deleteServerExternalRepository($bbr/@url, $bbr/@ident)
            return
                try {
                    for $bbr in $settings/externalBuildingBlockRepositories/buildingBlockRepository 
                    return serverapi:setServerExternalRepository($bbr)
                }
                catch * {
                    (: one of the new bbrs returned an error. restore what we had and rethrow our error :)
                    let $delete     :=
                        for $bbr in serverapi:getServerExternalRepositories()/buildingBlockRepository
                        return serverapi:deleteServerExternalRepository($bbr/@url, $bbr/@ident)
                    let $add        :=
                        for $bbr in $currentbbrs/buildingBlockRepository
                        return serverapi:setServerExternalRepository($bbr)
                    return
                    error(QName($err:module,$err:code),$err:description)
                }
        )
        (:case 'save-server-orbeon-version' return (
            serverapi:setServerOrbeonVersion($settings/orbeon-version), permlib:updateXForms('update')
        ):)
        default return
            error($errors:BAD_REQUEST, concat('Don''t know what to save. Unsupported action in @action: ',$action,' Supported actions are ''save-language'',''save-server-api-url'', ''save-server-url'',''save-services-url'',''save-server-xsl'',''save-repository-servers'',''save-repositories'''))
    ) else ()
};

(:~ Return the configured server-baseIds or empty
@return ids as ids
@since 2015-01-14
:)
declare function serverapi:getServerAllIds() as element(ids)? {
    doc($setlib:strServerInfo)/server-info/ids
};

(:~ Return the configured server-baseId types or empty

@return types as xs:string*
@since 2015-01-14
:)
declare function serverapi:getServerIdTypes() as xs:string* {
    doc($setlib:strServerInfo)/server-info/ids/baseId/@type[string-length()>0]
};

(:~ Return the configured server-baseId for a type. Get types through serverapi:getServerIdTypes()

@param $type server base id type
@return types as xs:string*
@since 2015-01-14
:)
declare function serverapi:getServerId($type as xs:string) as xs:string* {
    doc($setlib:strServerInfo)/server-info/ids/baseId[@type=$type]/@id[string-length()>0]
};

(:~ Return the default configured server-baseId for a type. Get types through serverapi:getServerIdTypes()
@return types as xs:string?
@since 2015-01-14
:)
declare function serverapi:getServerDefaultId($type as xs:string) as xs:string? {
    doc($setlib:strServerInfo)/server-info/ids/baseId[@type=$type][@default='true']/@id[string-length()>0]
};

(:~ Delete an existing server baseId. The match is done based on @id + @type, the rest is irrelevant.

@param $type server base id type
@param $id server base id
@return nothing or error you are not dba
@since 2014-03-27
:)
declare function serverapi:deleteServerId($type as xs:string, $id as xs:string) {
    if (serverapi:checkPermissions()) then (
        let $currentids := serverapi:getServerAllIds()
        return
            update delete $currentids/baseId[@id=$id][@type=$type]
    ) else ()
};

(:~ Delete an existing server id type if not in use. The match is done based on attributes id + type, the rest is irrelevant.

@param $type server base id type
@return nothing or error you are not dba
@since 2014-03-27
:)
declare function serverapi:deleteServerIdType($type as xs:string) {
    if (serverapi:checkPermissions()) then (
        if (empty(serverapi:getServerId($type))) then (
            let $currentids := serverapi:getServerAllIds()
            return
                update delete $currentids/type[@code=$type]
        ) else (
            error($errors:BAD_REQUEST, 'Cannot delete type. This type ' || $type || ' is still in use')
        )
    ) else ()
};

(:~ Set the configured server-baseId for a type. Get types through serverapi:getServerIdTypes().

Updates attribute default if both attributes id + type already exist, or adds it if it does not.

Updates attribute default as 'false' for other ids of the same type if parameter $default=true().

@param $type server base id type
@param $id server base id
@return nothing or error if you are not dba, if the type does not exist yet or if the id was already assigned to a different type.
@since 2015-01-14
:)
declare function serverapi:setServerId($type as xs:string, $id as xs:string, $default as xs:boolean) {
    if (serverapi:checkPermissions()) then (
        let $currentids := serverapi:getServerAllIds()
        
        return
        if (not($currentids/type[@code=$type])) then (
            error($errors:BAD_REQUEST, 'Cannot set server id. Type ' || $type || ' must exist before an id can be assigned to it')
        ) else if ($currentids/baseId[@id=$id][not(@type=$type)][string-length($id)>0]) then (
            error($errors:BAD_REQUEST, 'Cannot set server id. This id ' || $id || ' has already been assigned to a different type')
        ) else (
            if ($currentids/baseId[@type=$type][@id=$id]) then
                update value $currentids/baseId[@type=$type][@id=$id]/@default with $default
            else (
                update insert <baseId id="{$id}" type="{$type}" default="{$default}"/> into $currentids
            )
            ,
            if ($default) then (
                update value $currentids/baseId[@type=$type][not(@id=$id)]/@default with 'false'
            ) else ()
        )
    ) else ()
};

(:~ Set the configured server-id type. Get existing types through serverapi:getServerIdTypes().

Updates if attribute code on type already exists, or adds it if it does not

@param $type server base id type
@param $displayName server base id display name
@return nothing or error if you are not dba
@since 2015-01-14
:)
declare function serverapi:setServerIdType($type as xs:string, $displayName as xs:string) {
    if (serverapi:checkPermissions()) then (
        let $currentids := serverapi:getServerAllIds()
        
        return
        if ($currentids/type[@code=$type]) then
            update value $currentids/type[@code=$type]/@displayName with $displayName
        else if ($currentids/id) then
            update insert <type code="{$type}" displayName="{$displayName}"/> preceding $currentids/id[1]
        else (
            update insert <type code="{$type}" displayName="{$displayName}"/> into $currentids
        )
    ) else ()
};

(:~ Return the configured server-language or default value 'en-US'

@return server-language as xs:string('ll-CC')
@since 2014-03-27
:)
declare function serverapi:getServerLanguage() as xs:string {
    let $tmp    := doc($setlib:strServerInfo)/server-info/defaultLanguage
    return
        if ($tmp) then $tmp else ($setlib:strDefaultServerLanguage)
};

(:~ Set the server-language. Example: en-US

@param $language string value. Must have format ll-CC where ll is lower-case language and CC is uppercase country/region
@return nothing or error if you are not dba or if the supplied $language does not match the pattern
@since 2014-03-27
:)
declare function serverapi:setServerLanguage($language as xs:string) {
    if (serverapi:checkPermissions()) then (
        let $docServerInfo  := doc($setlib:strServerInfo)
        return
        if (matches($language,'[a-z]{2}-[A-Z]{2}')) then (
            if ($docServerInfo/server-info/defaultLanguage) then
                update value $docServerInfo/server-info/defaultLanguage with $language
            else (
                update insert <defaultLanguage>{$language}</defaultLanguage> into $docServerInfo/server-info
            )
        ) else (
            error($errors:BAD_REQUEST, 'Cannot set server language ''' || $language || '''. Language SHALL match pattern ll-CC where ll is lower-case language and CC is uppercase country/region')
        )
    ) else ()
};

(:~ Return the configured server-url http or https for ART-DECOR API or empty string. Example: https://art-decor.org/art-decor/

@return xs:anyURI('https://.../exist/apps/api')
@since 2021-03-10
:)
declare function serverapi:getServerURLArtApi() as xs:string? {
    doc($setlib:strServerInfo)/server-info/url-art-decor-api/string()
};

(:~ Set the server-url http or https for ART-DECOR API server. Example: https://art-decor.org/exist/apps/api

@param $url string value. Must have format ^https?://host:port(/path)?.*/api
@return nothing or error if you are not dba or if the supplied $url does not match the pattern
@since 2014-03-27
:)
declare function serverapi:setServerURLArtApi($url as xs:string) {
    if (serverapi:checkPermissions()) then (
        let $docServerInfo  := doc($setlib:strServerInfo)
        return
        if ($url castable as xs:anyURI and matches($url,'^https?://.*/api$')) then (
            if ($docServerInfo/server-info/url-art-decor-api) then
                update value $docServerInfo/server-info/url-art-decor-api with $url
            else (
                update insert <url-art-decor-api>{$url}</url-art-decor-api> into $docServerInfo/server-info
            )
        ) else (
            error($errors:BAD_REQUEST, 'Cannot set ART-DECOR API server url ''' || $url || '''. Url must be castable as xs:anyURI and match pattern ''https?://host:port(/path)?/api''.')
        )
    ) else ()
};

(:~ Return the configured server-url http or https for ART-DECOR or empty string. Example: https://art-decor.org/art-decor/

@return xs:anyURI('http://.../art-decor/')
@since 2014-03-27
:)
declare function serverapi:getServerURLArt() as xs:string? {
    utillib:getServerURLArt()
};

(:~ Set the server-url http or https for ART-DECOR server. Example: https://art-decor.org/art-decor/

@param $url string value. Must have format ^https?://host:port(/path)?/art-decor/
@return nothing or error if you are not dba or if the supplied $url does not match the pattern
@since 2014-03-27
:)
declare function serverapi:setServerURLArt($url as xs:string) {
    if (serverapi:checkPermissions()) then (
        let $docServerInfo  := doc($setlib:strServerInfo)
        return
        if ($url castable as xs:anyURI and matches($url,'^https?://.*/art-decor/$')) then (
            if ($docServerInfo/server-info/url-art-decor-deeplinkprefix) then
                update value $docServerInfo/server-info/url-art-decor-deeplinkprefix with $url
            else (
                update insert <url-art-decor-deeplinkprefix>{$url}</url-art-decor-deeplinkprefix> into $docServerInfo/server-info
            )
        ) else (
            error($errors:BAD_REQUEST, 'Cannot set ART server url ''' || $url || '''. URL must be castable as xs:anyURI and match pattern ''https?://host:port(/path)?/art-decor/''.')
        )
    ) else ()
};

(:~ Return the configured server-url http or https for ART-DECOR 3 Vue or empty string. Example: https://art-decor.org/ad/

@return xs:anyURI('http://.../ad/')
@since 2014-03-27
:)
declare function serverapi:getServerURLArt3() as xs:string? {
    utillib:getServerURLArt3()
};

(:~ Set the server-url http or https for ART-DECOR 3 Vue server. Example: https://art-decor.org/ad/

@param $url string value. Must have format ^https?://host:port(/path)?/ad/
@return nothing or error if you are not dba or if the supplied $url does not match the pattern
@since 2014-03-27
:)
declare function serverapi:setServerURLArt3($url as xs:string) {
    if (serverapi:checkPermissions()) then (
        let $docServerInfo  := doc($setlib:strServerInfo)
        return
        if ($url castable as xs:anyURI and matches($url,'^https?://.*/ad/$')) then (
            if ($docServerInfo/server-info/url-art-decor3-deeplinkprefix) then
                update value $docServerInfo/server-info/url-art-decor3-deeplinkprefix with $url
            else (
                update insert <url-art-decor3-deeplinkprefix>{$url}</url-art-decor3-deeplinkprefix> into $docServerInfo/server-info
            )
        ) else (
            error($errors:BAD_REQUEST, 'Cannot set ART server url ''' || $url || '''. URL must be castable as xs:anyURI and match pattern ''https?://host:port(/path)?/ad/''.')
        )
    ) else ()
};

(:~ Return the configured server-url http or https for ART-DECOR services or empty string. Example: http://art-decor.org/decor/services/

@return xs:anyURI('http://.../services/')
@since 2014-03-27
:)
declare function serverapi:getServerURLServices() as xs:string? {
    utillib:getServerURLServices()
};

(:~ Set the server-url http or https for ART-DECOR services. Example: http://art-decor.org/decor/services/

@param $url string value. Must have format ^https?://host:port(/path)?/services/
@return nothing or error if you are not dba or if the supplied $url does not match the pattern
@since 2014-03-27
:)
declare function serverapi:setServerURLServices($url as xs:string) {
    if (serverapi:checkPermissions()) then (
        let $docServerInfo  := doc($setlib:strServerInfo)
        return
        if ($url castable as xs:anyURI and matches($url,'^https?://.*/services/$')) then (
            if ($docServerInfo/server-info/url-art-decor-services) then
                update value $docServerInfo/server-info/url-art-decor-services with $url
            else (
                update insert <url-art-decor-services>{$url}</url-art-decor-services> into $docServerInfo/server-info
            )
        ) else (
            error($errors:BAD_REQUEST, 'Cannot set services server url ''' || $url || '''. URL must be castable as xs:anyURI and match pattern ''^https?://host:port(/path)?/services/''.')
        )
    ) else ()
};

(:~ Return the configured server-url http or https for ART-DECOR FHIR services or empty string. Example: https://art-decor.org/fhir/

@return xs:anyURI('http://.../services/')
@since 2015-02-27
:)
declare function serverapi:getServerURLFhirServices() as xs:string? {
    utillib:getServerURLFhirServices()
};

(:~ Return the configured server-url http or https for ART-DECOR FHIR services or empty string.
    Example: http://art-decor.org/fhir/
    
    @return 'http://.../services/' as xs:string
    @since 2015-02-27
:)
declare function serverapi:getServerURLFhirCanonicalBase() as xs:string? {
    utillib:getServerURLFhirCanonicalBase()
};

(:~ Return the installed FHIR server versions. The result of serverapi:getServerURLFhirServices() concatenated with each of the installed FHIR server versions is expected as endpoint. E.g.

let $endpoints  :=
   for $endpoint in serverapi:getInstalledFhirServices() 
   return concat(serverapi:getServerURLFhirServices(), $endpoint, '/')

Example endpoints: dstu2, stu3, release4.
Any collection under $setlib:strFhir that holds an expath-pkg.xml as a sign of an installed package is returned.

@return sequence of strings
@since 2015-03-27
:)
declare function serverapi:getInstalledFhirServices() as xs:string* {
    utillib:getInstalledFhirServices()
};

(:~ Set the server-url http or https for ART-DECOR FHIR services. Example: https://art-decor.org/fhir/

@param $url string value. Must have format ^https?://host:port(/path)?/services/
@return nothing or error if you are not dba or if the supplied $url does not match the pattern
@since 2015-02-27
:)
declare function serverapi:setServerURLFhirServices($url as xs:string) {
    if (serverapi:checkPermissions()) then (
        let $docServerInfo  := doc($setlib:strServerInfo)
        return
        if ($url castable as xs:anyURI and matches($url,'^https?://.*/fhir/$')) then (
            let $canonicalBase  := $docServerInfo/server-info/url-fhir-canonical-base
            let $currentService := string(serverapi:getServerURLFhirServices())
            return (
                if ($docServerInfo/server-info/url-fhir-services) then
                    update value $docServerInfo/server-info/url-fhir-services with $url
                else (
                    update insert <url-fhir-services>{$url}</url-fhir-services> into $docServerInfo/server-info
                ),
                (: The FHIR canonicalUris are being built from the ServerURLFhirServices, if ServerURLFhirCanonicalBase is empty
                  So, when we update the ServerURLFhirServices, we should retain the previous value of ServerURLFhirServices
                  as ServerURLFhirCanonicalBase to keep those stable.
                :)
                if (string-length($canonicalBase) = 0) then
                    if (string-length($currentService) gt 0) then
                        serverapi:setServerURLFhirCanonicalBase($currentService)
                    else (
                        serverapi:setServerURLFhirCanonicalBase($url)
                    )
                else ()
            )
        ) else (
            error($errors:BAD_REQUEST, 'Cannot set FHIR server url ''' || $url || '''. URL must be castable as xs:anyURI and match pattern ''^https?://host:port(/path)?/fhir/''.')
        )
    ) else ()
};

(:~ Set the server-url http or https for ART-DECOR FHIR canonical urls
    Example: http://art-decor.org/fhir/
    
    @param $url string value. Must have format ^https?://host:port(/path)?/services/
    @return nothing or error if you are not dba or if the supplied $url does not match the pattern
    @since 2015-02-27
:)
declare function serverapi:setServerURLFhirCanonicalBase($url as xs:string) {
    if (serverapi:checkPermissions()) then (
        let $docServerInfo  := doc($setlib:strServerInfo)
        return
        if ($url castable as xs:anyURI and matches($url,'^https?://.*/fhir/$')) then (
            if ($docServerInfo/server-info/url-fhir-canonical-base) then
                update value $docServerInfo/server-info/url-fhir-canonical-base with $url
            else (
                update insert <url-fhir-canonical-base>{$url}</url-fhir-canonical-base> into $docServerInfo/server-info
            )
        ) else (
            error(QName('http://art-decor.org/ns/error', 'InvalidFormat'), 'URL must be castable as xs:anyURI and match pattern ''^https?://host:port(/path)?/fhir/''.')
        )
    ) else ()
};

(:~ Get default-fhir-version for ART-DECOR FHIR services. Example: 1.0 (dstu2) or 3.0 (stu3)

@return nothing or string
@since 2017-09-25
:)
declare function serverapi:getServerFhirDefaultVersion() as xs:string? {
    let $fhirVersion    := doc($setlib:strServerInfo)/server-info/default-fhir-version

    return (
        switch ($fhirVersion) 
        case 'dstu2' return '1.0'
        case 'stu3' return '3.0'
        default return $fhirVersion
    )
};

(:~ Set the default-fhir-version for ART-DECOR FHIR services. Example: dstu2 or stu3

@param $version string value. Must be one of serverapi:getInstalledFhirServices()
@return nothing or error if you are not dba or if the supplied $version is not in the installed versions
@since 2017-09-25
:)
declare function serverapi:setServerFhirDefaultVersion($version as xs:string) {
    if (serverapi:checkPermissions()) then (
        let $docServerInfo  := doc($setlib:strServerInfo)
        return
        if ($version = '' or $version = serverapi:getInstalledFhirServices()) then (
            if ($docServerInfo/server-info/default-fhir-version) then
                update value $docServerInfo/server-info/default-fhir-version with $version
            else (
                update insert <default-fhir-version>{$version}</default-fhir-version> into $docServerInfo/server-info
            )
        ) else (
            error($errors:BAD_REQUEST, 'Cannot set FHIR default version ''' || $version || '''. This version is not installed. Please select one of: ', string-join(serverapi:getInstalledFhirServices(), ' '))
        )
    ) else ()
};

(:~ Return the configured server-xsl that constitutes the interface for ART or default value apply-rules.xsl. Example: apply-rules.xsl

@return xs:string('apply.rules.xsl')
@since 2014-03-27
:)
declare function serverapi:getServerXSLArt() as xs:string {
let $xsl    :=  doc($setlib:strServerInfo)/server-info/xformStylesheet/string()
return
    if ($xsl=serverapi:getServerXSLsArt()) then 
        $xsl
    else (
        $setlib:strDefaultServerXSL
    )
};

(:~ Return the available server-xsls that constitutes the interface for ART. Example: apply-rules-artdecororg.xsl apply-rules.xsl

@return list of available xsls
@since 2014-03-27
:)
declare function serverapi:getServerXSLsArt() as xs:string* {
    for $xsl in xmldb:get-child-resources($setlib:strServerXSLPath)[ends-with(.,'.xsl')]
    return $xsl
};

(:~ Set the server-xsl that constitutes the interface for ART. Example: apply-rules.xsl

@param $xsl-resource-name string value of the xsl. Name only!
@return nothing or error if you are not dba or if the supplied $xsl-resource-name does not exist
@since 2014-03-27
:)
declare function serverapi:setServerXSLArt($xsl-resource-name as xs:string) {
    if (serverapi:checkPermissions()) then (
        let $docServerInfo          := doc($setlib:strServerInfo)
        let $xsl-resource-name-full := concat($setlib:strServerXSLPath,'/',tokenize($xsl-resource-name,'/')[last()])
        return
        if (doc-available($xsl-resource-name-full)) then (
            if ($docServerInfo/server-info/xformStylesheet) then
                update value $docServerInfo/server-info/xformStylesheet with $xsl-resource-name
            else (
                update insert <xformStylesheet>{$xsl-resource-name}</xformStylesheet> into $docServerInfo/server-info
            )
        ) else (
            error($errors:BAD_REQUEST, 'Cannot set XSL ''' || $xsl-resource-name-full || '''. This XSL does not exist. Call serverapi:getServerXSLs for valid values.')
        )
    ) else ()
};

(:~ Return the configured server-menu-template. Example: art-menu-template.xml

@return string
@since 2014-09-10
:)
declare function serverapi:getServerMenuTemplate() as xs:string {
    let $tmp    := doc($setlib:strServerInfo)/server-info/art-menu-template
    return
    if ($tmp) then $tmp else ($serverapi:strDefaultServerMenu)
};

(:~ Set the server-menu-template that constitutes the interface for ART. Example: art-menu-template.xml

@param string name of the logo. Name only!
@return nothing or error if you are not dba
@since 2014-09-10
:)
declare function serverapi:setServerMenuTemplate($menu-template as xs:string) {
    let $docServerInfo  := doc($setlib:strServerInfo)
    let $newdata        := <art-menu-template>{$menu-template}</art-menu-template>
    return
    if (serverapi:checkPermissions()) then (
        if ($docServerInfo/server-info/art-menu-template) then (
            update replace $docServerInfo/server-info/art-menu-template with $newdata
        )
        else (
            update insert $newdata into $docServerInfo/server-info
        )
    ) else ()
};

(:~ Return the available server-menu-templates that constitutes the interface for ART. Example: art-menu-template.xml art-menu-template-nictiz.xml

@return list of available xsls
@since 2014-09-11
:)
declare function serverapi:getServerMenuTemplates() as xs:string* {
    for $xml in collection($serverapi:strServerMenuPath)/menu[section]
    return util:document-name($xml)
};

(:~ Return the available server-logo for display top right in ART. Example: art-decor-logo40.png

@return string
@since 2014-09-10
:)
declare function serverapi:getServerLogo() as xs:string {
    let $tmp    := doc($setlib:strServerInfo)/server-info/art-server-logo
    return
        if ($tmp) then $tmp else ($serverapi:strDefaultServerLogo)
};

(:~ Return the available server-logo-url for linking the logo top right in ART. Example: https://art-decor.org

@return string
@since 2014-09-10
:)
declare function serverapi:getServerLogoUrl() as xs:string {
    let $tmp    := doc($setlib:strServerInfo)/server-info/art-server-logo/@href
    return
        if ($tmp) then $tmp else ($serverapi:strDefaultServerLogoUrl)
};

(:~ Set the server-logo that constitutes the interface for ART. Example: art-decor-logo40.png

@param string name of the logo. Name only!
@return nothing or error if you are not dba
@since 2014-09-10
:)
declare function serverapi:setServerLogoAndUrl($logo-name as xs:string, $logo-url as xs:string?) {
    if (serverapi:checkPermissions()) then (
        let $docServerInfo  := doc($setlib:strServerInfo)
        return
        if ($docServerInfo/server-info/art-server-logo) then (
            update replace $docServerInfo/server-info/art-server-logo with <art-server-logo href="{$logo-url}">{$logo-name}</art-server-logo>
        )
        else (
            update insert <art-server-logo href="{$logo-url}">{$logo-name}</art-server-logo> into $docServerInfo/server-info
        )
    ) else ()
};

(:~ Return the configured external building block repository servers as XML element. Example: 

<externalBuildingBlockRepositoryServers>
    <buildingBlockServer url="https://art-decor.org/decor/services/"/>
</externalBuildingBlockRepositoryServers>

@return list of configured external building block repository servers
@since 2014-03-27
:)
declare function serverapi:getServerRepositoryServers() as element()? {
    doc($setlib:strServerInfo)/server-info/externalBuildingBlockRepositoryServers
};

(:~ Save/update the provided external building block repository server. Example input: 

<externalBuildingBlockRepositoryServers>
    <buildingBlockServer url="https://art-decor.org/decor/services/"/>
</externalBuildingBlockRepositoryServers>

@param $buildingBlockServer MUST contain the new buildingBlockServer info
@return nothing or error you are not dba, or if the buildingBlockServer element does not have attributes @url
@since 2014-03-27
:)
declare function serverapi:setServerRepositoryServer($buildingBlockServer as element()) as element()? {
    if (serverapi:checkPermissions()) then (
        if ($buildingBlockServer[name()='buildingBlockServer'][@url] and $buildingBlockServer/@url castable as xs:anyURI and matches($buildingBlockServer/@url,'^https?://.*/services/$')) then (
            let $docServerInfo              := doc($setlib:strServerInfo)
            let $existingRepositoryServers  := $docServerInfo/server-info/externalBuildingBlockRepositoryServers
            let $existingRepositoryServer   := $existingRepositoryServers/buildingBlockServer[@url=$buildingBlockServer/@url]
            return
            if ($existingRepositoryServer) then (
                (: BBR already exist. Delete and write new :)
                update replace $existingRepositoryServer with $buildingBlockServer
            )
            else if ($existingRepositoryServers) then
                update insert $buildingBlockServer into $existingRepositoryServers
            else (
                update insert <externalBuildingBlockRepositoryServers>{$buildingBlockServer}</externalBuildingBlockRepositoryServers> into $docServerInfo/server-info
            )
        ) else (
            error($errors:BAD_REQUEST, 'Element <buildingBlockServer> must have attribute @url and @url must be castable as xs:anyURI and match pattern ''^https?://host:port(/path)?/services/''.')
        )
    ) else ()
};

(:~ Delete an existing external building block repository server. The match is done based on attribute url, the rest is irrelevant.

@param $url url of the repository server to-be-deleted
@param $ident ident of the repository server to-be-deleted
@return nothing or error you are not dba
@since 2014-03-27
:)
declare function serverapi:deleteServerRepositoryServer($url as xs:string) {
    if (serverapi:checkPermissions()) then (
        let $existingRepositoryServers  := serverapi:getServerRepositoryServers()
        return
            update delete $existingRepositoryServers/buildingBlockServer[@url=$url]
    ) else ()
};

(:~ Return the configured internal building block repositories as XML element. Example: 
<internalBuildingBlockRepositories>
    <buildingBlockRepository url="https://art-decor.org/decor/services/" ident="ad1bbr-" type="local">
        <name language="en-US">CDA Release 2</name>
        <name language="nl-NL">CDA Release 2</name>
        <name language="de-DE">CDA Release 2</name>
    </buildingBlockRepository>
    <buildingBlockRepository url="https://art-decor.org/decor/services/" ident="ad2bbr-" type="local">
        <name language="en-US">HL7 V3 Value Sets</name>
        <name language="nl-NL">HL7v3-waardenlijsten</name>
        <name language="de-DE">HL7 V3 Value Sets</name>
    </buildingBlockRepository>
    <buildingBlockRepository url="https://art-decor.org/decor/services/" ident="ccda-" type="local">
        <name language="en-US">Consolidated CDA 1.1</name>
        <name language="nl-NL">Consolidated CDA 1.1</name>
        <name language="de-DE">Consolidated CDA 1.1</name>
    </buildingBlockRepository>
</internalBuildingBlockRepositories>

@return list of configured external building block repositories
@since 2014-03-27
:)
declare function serverapi:getServerInternalRepositories() as element() {
    let $thisServer := serverapi:getServerURLServices()
    
    return
    <internalBuildingBlockRepositories>
    {
        for $bbr in $setlib:colDecorData//decor[@repository='true'][not(@private='true')]
        let $ident      := $bbr/project/@prefix
        return
            <buildingBlockRepository url="{$thisServer}" ident="{$ident}" type="local" format="decor">
            {
                for $lang in utillib:getArtLanguages()
                return
                    if ($bbr/project/name[@language=$lang]) then
                        $bbr/project/name[@language=$lang]
                    else if ($bbr/project/name[@language='en-US']) then
                        <name language="{$lang}">{$bbr/project/name[@language='en-US']/node()}</name>
                    else (
                        <name language="{$lang}">{$bbr/project/name[@language=$bbr/project/@defaultLanguage]/node()}</name>
                    )
            }
            </buildingBlockRepository>
    }
    </internalBuildingBlockRepositories>
};

(:~ Return the configured external building block repositories as XML element. Example: 
<externalBuildingBlockRepositories>
    <buildingBlockRepository url="https://art-decor.org/decor/services/" ident="ad1bbr-" type="external" format="decor">
        <name language="en-US">CDA Release 2</name>
        <name language="nl-NL">CDA Release 2</name>
        <name language="de-DE">CDA Release 2</name>
    </buildingBlockRepository>
    <buildingBlockRepository url="https://art-decor.org/decor/services/" ident="ad2bbr-" type="external" format="decor">
        <name language="en-US">HL7 V3 Value Sets</name>
        <name language="nl-NL">HL7v3-waardenlijsten</name>
        <name language="de-DE">HL7 V3 Value Sets</name>
    </buildingBlockRepository>
    <buildingBlockRepository url="https://art-decor.org/decor/services/" ident="ccda-" type="external" format="decor">
        <name language="en-US">Consolidated CDA 1.1</name>
        <name language="nl-NL">Consolidated CDA 1.1</name>
        <name language="de-DE">Consolidated CDA 1.1</name>
    </buildingBlockRepository>
</externalBuildingBlockRepositories>

@return list of configured external building block repositories
@since 2014-03-27
:)
declare function serverapi:getServerExternalRepositories() as element()? {
let $config         := doc($setlib:strServerInfo)/server-info/externalBuildingBlockRepositories
let $art-language   := utillib:getArtLanguages()
return
    <externalBuildingBlockRepositories>
    {
        for $bbr in $config/buildingBlockRepository
        return
        <buildingBlockRepository>
        {
            $bbr/@*,
            if ($bbr/@format) then () else (attribute format {'decor'}),
            $bbr/name,
            for $lang in $art-language[not(.=$bbr/name/@language)]
            return
                <name language="{$lang}">{$bbr/name[@language='en-US']/node()}</name>
        }
        </buildingBlockRepository>
    }
    </externalBuildingBlockRepositories>
};

(:~ Return the configured internal and external building block repositories as XML element. Example: 
<buildingBlockRepositories>
    <buildingBlockRepository url="https://art-decor.org/decor/services/" ident="ad1bbr-" type="external" format="decor">
        <name language="en-US">CDA Release 2</name>
        <name language="nl-NL">CDA Release 2</name>
        <name language="de-DE">CDA Release 2</name>
    </buildingBlockRepository>
    <buildingBlockRepository url="https://art-decor.org/decor/services/" ident="ad2bbr-" type="external" format="decor">
        <name language="en-US">HL7 V3 Value Sets</name>
        <name language="nl-NL">HL7v3-waardenlijsten</name>
        <name language="de-DE">HL7 V3 Value Sets</name>
    </buildingBlockRepository>
    <buildingBlockRepository url="https://art-decor.org/decor/services/" ident="ccda-" type="external" format="decor">
        <name language="en-US">Consolidated CDA 1.1</name>
        <name language="nl-NL">Consolidated CDA 1.1</name>
        <name language="de-DE">Consolidated CDA 1.1</name>
    </buildingBlockRepository>
    <buildingBlockRepository url="http://myserver.org/decor/services/" ident="bbr1-" type="local" format="decor">
        <name language="en-US">My BBR 1</name>
        <name language="nl-NL">Mijn BBR 1</name>
        <name language="de-DE">Mein BBR 1</name>
    </buildingBlockRepository>
    <buildingBlockRepository url="http://myserver.org/decor/services/" ident="bbr2-" type="local" format="decor">
        <name language="en-US">My BBR 2</name>
        <name language="nl-NL">Mijn BBR 2</name>
        <name language="de-DE">Mein BBR 2</name>
    </buildingBlockRepository>
</buildingBlockRepositories>

@return list of configured external building block repositories
@since 2014-03-27
:)
declare function serverapi:getServerAllRepositories() as element() {
    <buildingBlockRepositories>
    {
        serverapi:getServerExternalRepositories()/buildingBlockRepository
        ,
        serverapi:getServerInternalRepositories()/buildingBlockRepository
    }
    </buildingBlockRepositories>
};

(:~ Return the repositories as available at the given server URL as external building block repositories as XML element
Example input:
    https://art-decor.org/decor/services/
Example output: 
<externalBuildingBlockRepositories used-url="uri-that-was-built-and-used">
    <buildingBlockRepository url="$external-server-services-url" ident="ad1bbr-" type="external">
        <name language="en-US">CDA Release 2</name>
        <name language="nl-NL">CDA Release 2</name>
        <name language="de-DE">CDA Release 2</name>
    </buildingBlockRepository>
    <buildingBlockRepository url="$external-server-services-url" ident="ad2bbr-" type="external">
        <name language="en-US">HL7 V3 Value Sets</name>
        <name language="nl-NL">HL7v3-waardenlijsten</name>
        <name language="de-DE">HL7 V3 Value Sets</name>
    </buildingBlockRepository>
    <buildingBlockRepository url="$external-server-services-url" ident="ccda-" type="external">
        <name language="en-US">Consolidated CDA 1.1</name>
        <name language="nl-NL">Consolidated CDA 1.1</name>
        <name language="de-DE">Consolidated CDA 1.1</name>
    </buildingBlockRepository>
</externalBuildingBlockRepositories>

@param $external-server-services-url the full url to the services including the trailing slash, e.g. https://art-decor.org/decor/services/
@return list of available external building block repositories
@since 2014-03-27
:)
declare function serverapi:getRepositoriesFromServer($external-server-services-url as xs:string) as element() {
    let $external-server-services-url   := 
        if (ends-with($external-server-services-url, '/')) then 
            $external-server-services-url 
        else (
            $external-server-services-url || '/'
        )
    let $service-uri     := $external-server-services-url || 'ProjectIndex?format=xml'
    return
    try {
        let $requestHeaders := 
            <http:request method="GET" href="{$service-uri}">
                <http:header name="Content-Type" value="text/xml"/>
                <http:header name="Cache-Control" value="no-cache"/>
                <http:header name="Max-Forwards" value="1"/>
            </http:request>
        let $server-response := http:send-request($requestHeaders)
        let $server-check    :=
            if ($server-response[1]/@status='200') then () else (
                error($errors:SERVER_ERROR, concat('Server returned HTTP status: ',$server-response[1]/@status, ' for URI ''',$service-uri,''' with body ''',string-join($server-response[2], ' '),''''))
            )
        return
        <externalBuildingBlockRepositories used-url="{$service-uri}">
        {
            for $repository in $server-response[2]/return/project[@repository='true']
            return
                <buildingBlockRepository url="{$external-server-services-url}" ident="{$repository/@prefix}" type="external" format="decor">
                {
                    for $lang in utillib:getArtLanguages()
                    return
                    <name language="{$lang}">{$repository/@name/string()}</name>
                }
                </buildingBlockRepository>
        }
        </externalBuildingBlockRepositories>
    }
    catch * {
        error($errors:SERVER_ERROR, concat('ERROR ',$err:code,'. Could not retrieve building block repositories from ''',$service-uri,'''. ',$err:description,' module: ',$err:module,' (',$err:line-number,' ',$err:column-number,')'))
    }
};

(:~ Set a new or update an existing external building block repositories
Example input: 
    <buildingBlockRepository url="https://art-decor.org/decor/services/" ident="ad1bbr-" type="external">
        <name language="en-US">CDA Release 2</name>
        <name language="nl-NL">CDA Release 2</name>
        <name language="de-DE">CDA Release 2</name>
    </buildingBlockRepository>

@param $buildingBlockRepository MUST contain the new buildingBlockRepository info
@return nothing or error you are not dba, or if the buildingBlockRepository element does not have attributes @url, @ident or @type=''external'' and at least one element <name language="ll-CC">
@since 2014-03-27
:)
declare function serverapi:setServerExternalRepository($buildingBlockRepository as element()) {
    if (serverapi:checkPermissions()) then (
        let $docServerInfo              := doc($setlib:strServerInfo)
        let $buildingBlockRepository    :=
            element {$buildingBlockRepository/name()} {
                $buildingBlockRepository/@url[. castable as xs:anyURI],
                $buildingBlockRepository/@ident[not(.='')],
                $buildingBlockRepository/@type,
                $buildingBlockRepository/@format[not(.=('','decor'))],
                $buildingBlockRepository/node()
            }
            
        return
        if ($buildingBlockRepository[name()='buildingBlockRepository'][@url][@ident][empty(@format)][@type='external'][name/@language] |
            $buildingBlockRepository[name()='buildingBlockRepository'][@url][@format[not(.='decor')]][@type='external'][name/@language]) then (
            let $existingBuildingBlockRepositories  := $docServerInfo/server-info/externalBuildingBlockRepositories
            let $existingBuildingBlockRepository    := 
                if ($buildingBlockRepository[@ident]) then
                    $existingBuildingBlockRepositories/buildingBlockRepository[@url=$buildingBlockRepository/@url][@ident=$buildingBlockRepository/@ident]
                else (
                    $existingBuildingBlockRepositories/buildingBlockRepository[@url=$buildingBlockRepository/@url]
                )
            return
            if ($existingBuildingBlockRepository) then (
                (: BBR already exists. Delete and write new :)
                update replace $existingBuildingBlockRepository with $buildingBlockRepository
            )
            else if ($existingBuildingBlockRepositories) then
                update insert $buildingBlockRepository into $existingBuildingBlockRepositories
            else (
                update insert <externalBuildingBlockRepositories>{$buildingBlockRepository}</externalBuildingBlockRepositories> into $docServerInfo/server-info
            )
        ) else (
            error($errors:BAD_REQUEST, 'Element <buildingBlockRepository> must have attributes @url, @ident (when @format=''decor'') and @type=''external'' and at least one element <name language="ll-CC">')
        )
    ) else ()
};

(:~ Get the orbeon version so we know what to load in some cases like CSS

@return orbeonVersion, defaults to 3.9
@since 2018-09-04
:)
declare function serverapi:getServerOrbeonVersion() as xs:string {
    let $orbeon-version    := doc($setlib:strServerInfo)/server-info/orbeon-version
    
    return
        if (empty($orbeon-version)) then '3.9' else $orbeon-version
};

(:~ Get the orbeon version so we know what to load in some cases like CSS

@return orbeonVersion, defaults to 3.9
@since 2018-09-04
:)
declare function serverapi:getServerOrbeonVersions() as element()* {
    <orbeon version="3.9" displayName="Orbeon 3.9"/> |
    <orbeon version="2018" displayName="Orbeon 2018.x"/> |
    <orbeon version="2019" displayName="Orbeon 2019.x"/>
};

(:~ Set the orbeon version so we know what to load in some cases like CSS

@param $orbeonVersion required string, e.g. 3.9 or 2017 or 2018. Check 
@return nothing or error if you are not dba
@since 2018-09-04
:)
declare function serverapi:setServerOrbeonVersion($orbeonVersion as xs:string) {
    if (serverapi:checkPermissions()) then (
        let $server-settings    := doc($setlib:strServerInfo)/server-info
        
        return
        if ($server-settings/orbeon-version) then
            update value $server-settings/orbeon-version with $orbeonVersion
        else
            update insert <orbeon-version>{$orbeonVersion}</orbeon-version> into $server-settings
    ) else ()
};

(:~ Delete an existing external building block repository. The match is done based on @url and optionally on @ident, the rest is irrelevant.
If you omit $ident then all BBRs for the given $url are deleted

@param $url url of the bbr to-be-deleted
@param $ident ident of the bbr to-be-deleted
@return nothing or error you are not dba
@since 2014-03-27
:)
declare function serverapi:deleteServerExternalRepository($url as xs:string, $ident as xs:string?) {
    if (serverapi:checkPermissions()) then (
        let $existingBuildingBlockRepositories  := doc($setlib:strServerInfo)/server-info/externalBuildingBlockRepositories
        return
            update delete $existingBuildingBlockRepositories/buildingBlockRepository[@url=$url][empty($ident) or @ident=$ident]
    ) else ()
};

(:~ Return the configured password for the requested user

@param $user username
@return password as xs:string?
@since 2015-05-06
:)
declare %private function serverapi:getPassword($user as xs:string) as xs:string? {
    doc($setlib:strServerInfo)//user[@user=$user][ancestor::system-users]/@pass
};

(:~ Return usernames that we have saved passwords for

@return $username as xs:string*
@since 2015-05-06
:)
declare function serverapi:getSavedUsernames() as xs:string* {
    doc($setlib:strServerInfo)/server-info/system-users/user/@user
};

(:~ Set the password for the requested system-user.

@param $user username
@param $pass password
@return nothing or error if you are not dba
@since 2015-05-06
:)
declare function serverapi:setPassword($user as xs:string, $pass as xs:string) {
    if (serverapi:checkPermissions()) then (
        let $server-settings    := doc($setlib:strServerInfo)/server-info
        
        return
        if ($server-settings/system-users/user[@user=$user]) then
            update value $server-settings/system-users/user[@user=$user]/@pass with $pass
        else if ($server-settings/system-users) then
            update insert <user user="{$user}" pass="{$pass}"/> into $server-settings/system-users
        else (
            update insert <system-users><user user="{$user}" pass="{$pass}"/></system-users> into $server-settings
        )
    ) else ()
};

(:~ Logs in for the requested user at the requested path. Only possible for server/system level users like "xis-webservice"

@param $user username
@param $path database path
@return true|false or error
@since 2015-05-06
:)
declare function serverapi:login($user as xs:string, $path as xs:string?) as xs:boolean {
    let $path   := if (string-length($path)=0) then '/db' else $path
    return xmldb:login($path, $user, serverapi:getPassword($user))
};

(:~ Authenticates the requested user at the requested path. Only possible for server/system level users like "xis-webservice".
Check if the user, $user-id, can authenticate against the database collection $collection-uri. The function simply tries to 
read the collection $collection-uri, using the credentials $user-id and $password. Collection URIs can be specified either 
as a simple collection path or an XMLDB URI. It returns true if the authentication succeeds, false otherwise.

@param $user username
@param $path database path
@return true|false or error
@since 2015-05-06
:)
declare function serverapi:authenticate($user as xs:string, $path as xs:string?) as xs:boolean {
    let $path   := if (string-length($path)=0) then '/db' else $path
    return xmldb:authenticate($path, $user, serverapi:getPassword($user))
};

(:~ Facilitates getting new properties in art/install-data/server-info.xml into the live art-data/server-info.xml copy

@return nothing or error you are not dba or if the install-data/server-info.xml file is missing
@since 2014-03-27
:)
declare function serverapi:mergeServerSettings() {
    if (serverapi:checkPermissions()) then (
        let $docServerInfo  := doc($setlib:strServerInfo)
        return
        if (doc-available($strServerInfoName)) then ( 
            for $setting in doc($setlib:strServerInfoDefault)/server-info/*
            let $ns := $setting/namespace-uri()
            let $nm := $setting/local-name()
            return
                if ($docServerInfo/server-info/*[local-name()=$nm][namespace-uri()=$ns]) then (
                    (:already exists:)
                )
                else if ($nm='defaultLanguage') then (
                    serverapi:setServerLanguage($setting)
                )
                else if ($nm='url-art-decor-api') then (
                    serverapi:setServerURLArtApi($setting)
                )
                else if ($nm='url-art-decor-deeplinkprefix') then (
                    serverapi:setServerURLArt($setting)
                )
                else if ($nm='url-art-decor3-deeplinkprefix') then (
                    serverapi:setServerURLArt3($setting)
                )
                else if ($nm='url-art-decor-services') then (
                    serverapi:setServerURLServices($setting)
                )
                else if ($nm='url-fhir-services') then (
                    serverapi:setServerURLFhirServices($setting)
                )
                else if ($nm='xformStylesheet') then (
                    serverapi:setServerXSLArt($setting)
                )
                else if ($nm='orbeon-version') then (
                    serverapi:setServerOrbeonVersion($setting)
                )
                else (
                    update insert $setting into $docServerInfo/server-info
                )
        ) else (
            error($errors:SERVER_ERROR, concat('Cannot merge when server-info.xml is missing: ',$strServerInfoName))
        )
    ) else ()
};

(:~ Replaces a building block repository url with a new value in all active places :)
declare function serverapi:replaceBuildingBlockRepositoryUrl($old as xs:string, $new as xs:string) {
    if ($new castable as xs:anyURI and matches($new, '^https?://.*/decor/services/$')) then (
        let $docServerInfo  := doc($setlib:strServerInfo)
        let $stored         := 
            $docServerInfo/server-info/externalBuildingBlockRepositories/buildingBlockServer/@url[. = $old] |
            $docServerInfo/server-info/externalBuildingBlockRepositories/buildingBlockRepository/@url[. = $old] |
            $docServerInfo/server-info/url-art-decor-services[. = $old] |
            $setlib:colDecorData//buildingBlockRepository/@url[. = $old]
            
        return
            update value $stored with $new
    ) else (
        error($errors:BAD_REQUEST, 'Cannot set services server url ''' || $url || '''. URL must be castable as xs:anyURI and match pattern ''^https?://host:port(/path)?/services/''.')
    )
};

(:~ Consolidated local function for checking if you are dba when you are writing info.

@return nothing or error you are not dba
@since 2014-03-27
:)
declare %private function serverapi:checkPermissions() as xs:boolean {
    if (sm:is-dba(setlib:strCurrentUserName())) then (true()) else (
        error($errors:FORBIDDEN, 'Must be dba for full access')
    )
};