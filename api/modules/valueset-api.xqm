xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:~ Value set API allows read, create, update of DECOR valueSets :)
module namespace vsapi              = "http://art-decor.org/ns/api/valueset";

import module namespace roaster     = "http://e-editiones.org/roaster";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

import module namespace utillib     = "http://art-decor.org/ns/api/util" at "library/util-lib.xqm";
import module namespace utilhtml    = "http://art-decor.org/ns/api/util-html" at "library/util-html-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "library/settings-lib.xqm";
import module namespace decorlib    = "http://art-decor.org/ns/api/decor" at "library/decor-lib.xqm";
import module namespace ggapi       = "http://art-decor.org/ns/api/governancegroups" at "governancegroups-api.xqm";
import module namespace csapi       = "http://art-decor.org/ns/api/codesystem" at "codesystem-api.xqm";
import module namespace serverapi   = "http://art-decor.org/ns/api/server" at "server-api.xqm";
import module namespace histlib     = "http://art-decor.org/ns/api/history" at "library/history-lib.xqm";

declare namespace json      = "http://www.json.org";
declare namespace rest      = "http://exquery.org/ns/restxq";
declare namespace resterr   = "http://exquery.org/ns/restxq/error";
declare namespace http      = "http://expath.org/ns/http-client";
declare namespace output    = "http://www.w3.org/2010/xslt-xquery-serialization";

(:~ All functions support their own override, but this is the fallback for the maximum number of results returned on a search :)
declare %private variable $vsapi:maxResults                 := 50;
declare %private variable $vsapi:STATUSCODES-FINAL          := ('final', 'pending', 'rejected', 'cancelled', 'deprecated');
declare %private variable $vsapi:ADDRLINE-TYPE              := utillib:getDecorTypes()/AddressLineType;
declare %private variable $vsapi:VOCAB-TYPE                 := utillib:getDecorTypes()/VocabType;
declare %private variable $vsapi:DESIGNATION-TYPE           := utillib:getDecorTypes()/DesignationType;
declare %private variable $vsapi:INTENSIONALOPERATORS-TYPE  := utillib:getDecorTypes()/IntensionalOperators;


(:~ Retrieves list of all DECOR valueSets of a project denoted by project prefix or oid for publication
    @param $project                 - required. limits scope to this project only
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @param $language                - optional
    @param $format                  - optional. overrides the accept-header for frontend purposes
    @return as-is or as compiled as JSON
    @since 2024-11-15
:)
declare function vsapi:getValueSetExtractList($request as map(*)) { 

    let $project                        := $request?parameters?project[not(. = '')]
    let $projectVersion                 := $request?parameters?release[not(. = '')]
    let $projectLanguage                := $request?parameters?language[not(. = '')]
    let $format                         := $request?parameters?format[not(. = '')]
           
    (: parameter format overrides accept-header as mime-type for using the api in the browser :)
    
    (: 
        this call is used in backend and frontend (browser) - default behaviour mime-types:
        - backend:   default mime-type is json - response payload is json - if no accept type is given and format is not csv or sql  
        - frontend:  default mime-type is xml - response payload is xml - format is not csv or sql 
    :)
    
    let $acceptTypes                    := roaster:accepted-content-types()
    let $acceptedType                   := ($acceptTypes[. = ('application/xml', 'application/json', 'text/csv', 'text/sql')],'application/json')[1]
    
    let $format                         := if ($format) then $format else tokenize($acceptedType, '/')[2]

    (: 
       overwrite format in the backend is not always possible because of middleware behaviour
       - if in backend the format is xml and accept is not application/xml, roaster always gives a json payload 
       - if in backend the format is not json and accept is application/json, roaster always gives a json payload
    :) 
    let $check                          :=
        if ($format = ('xml', 'svs') and not($acceptedType = 'application/xml')) then 
            error($errors:BAD_REQUEST, 'In case of format parameter is xml or svs the accept header should be application/xml')
        else if (not($format = 'json') and $acceptedType = 'application/json') then
            error($errors:BAD_REQUEST, 'In case of format parameter is ' || $format || ' the accept header can be anything but application/json')
        else ()   

    let $check                          :=
        if (empty($project)) then 
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    
    let $projectPrefix                  := utillib:getDecor($project, $projectVersion, $projectLanguage)/project/@prefix
   
    let $check                  :=
        if (empty($projectPrefix)) then 
            error($errors:BAD_REQUEST, 'Project ' || $project || ' not found.')
        else ()
    
    (: Return zero or more expanded valuesets wrapped in a &lt;return/&gt; element, and subsequently inside a &lt;repository&gt; element :)
    let $valueSets                      := 
    for $valueSet in vsapi:getValueSetList((), $projectPrefix, $projectVersion, $projectLanguage, (), (), false(), (), (), (), false())/valueSet/valueSet
    return
        vsapi:getValueSetExtract($projectPrefix, $projectVersion, $projectLanguage, $valueSet/(@id|@ref), $valueSet/@effectiveDate, false())

    (: prepare response payload :)
    let $results                        :=
         if (empty($valueSets/*)) then () 
         else if($format = ('xml', 'json')) then (
            let $xmljson :=
                <valueSets>
                    {$valueSets/*}
                </valueSets>                   
            let $xmljson := 
            for $x in $xmljson
                return
                element {name($x)} {
                    $x/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($x/node(), $format)
                }
             (: in this case the json format overrides header application/xml and works with a text header :)
             let $xmljson               :=
                if ($format = 'json' and not($acceptedType = 'application/json')) then
                    fn:serialize($xmljson, map{"method": $format , "indent": true()})
                    else $xmljson
             return $xmljson
         )   
         else if ($format = 'svs') then vsapi:convertValueSet2Svs($valueSets, $projectLanguage, $projectVersion, 'svs') 
         else if ($format = 'csv') then vsapi:convertValueSet2Csv($valueSets, $projectLanguage, $projectPrefix)
         else if ($format = 'sql') then fn:string(transform:transform($valueSets, doc($setlib:strDecorServices ||'/resources/stylesheets/ToSql4ValueSets.xsl'), ()))
         else error($errors:BAD_REQUEST, 'Requested mime-type ' || $format || ' is not supported')
    
    (: prepare response header content-type and content-disposition :)
        let $contentType                    :=
            if ($format = 'svs') then 'application/xml'
            else if ($format = ('csv', 'sql')) then 'text/' || $format
            else 'application/' || $format
                
        let $fileExt                    := if ($format = 'svs') then 'xml' else $format
        let $valueSetNames              := distinct-values($valueSets//valueSet/@name)
        let $fileNamePart               := if (count($valueSetNames)>1) then 'project_' || $projectPrefix || '('|| count($valueSets//valueSet) || ')' else $valueSetNames
        let $fileName                   := 'VS_' || $fileNamePart ||'_(download_' || substring(fn:string(current-dateTime()),1,19) || ').' || $fileExt
            
        let $r-header                   := 
            (response:set-header('Content-Type', $contentType || '; charset=utf-8'),
            response:set-header('Content-Disposition', 'filename='|| $fileName))
    
    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else $results

};

(:~ Retrieves latest DECOR valueSet for publication based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the valueSet
    @param $projectPrefix           - optional limits scope to this project only
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @param $language                - optional
    @param $format                  - optional overrides the accept-header for frontend purposes
    @return as-is or as compiled as JSON
    @since 2023-11-14
:)
declare function vsapi:getLatestValueSetExtract($request as map(*)) {
    vsapi:getValueSetExtract($request)
};

(:~ Retrieves one or more DECOR valueSet for publication based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the valueSet
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the valueSet. If not given assumes latest version for id
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @param $project                 - optional. limits scope to this project only
    @param $language                - optional
    @param $wrap                    - optional parameter to return the valuesets wrapped in repository and with project data - xml only
    @param $format                  - optional. overrides the accept-header for frontend purposes
    @return as-is or as compiled as JSON
    @since 2023-11-14
:)
declare function vsapi:getValueSetExtract($request as map(*)) { 

    let $project                        := $request?parameters?project[not(. = '')]
    let $projectVersion                 := $request?parameters?release[not(. = '')]
    let $projectLanguage                := $request?parameters?language[not(. = '')]
    let $withversions                   := $request?parameters?versions = true()
    let $format                         := $request?parameters?format[not(. = '')]
    
    let $idOrName                       := $request?parameters?idOrName[not(. = '')]
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
           
    (: parameter format overrides accept-header as mime-type for using the api in the browser :)
    
    (: 
        this call is used in backend and frontend (browser) - default behaviour mime-types:
        - backend:   default mime-type is json - response payload is json - if no accept type is given and format is not csv or sql  
        - frontend:  default mime-type is xml - response payload is xml - format is not csv or sql 
    :)
    
    let $acceptTypes                    := roaster:accepted-content-types()
    let $acceptedType                   := ($acceptTypes[. = ('application/xml', 'application/json', 'text/csv', 'text/sql')],'application/json')[1]
    
    let $format                         := if ($format) then $format else tokenize($acceptedType, '/')[2]

    (: 
       overwrite format in the backend is not always possible because of middleware behaviour
       - if in backend the format is xml and accept is not application/xml, roaster always gives a json payload 
       - if in backend the format is not json and accept is application/json, roaster always gives a json payload
    :) 
    let $check                          :=
        if ($format = ('xml', 'svs') and not($acceptedType = 'application/xml')) then 
            error($errors:BAD_REQUEST, 'In case of format parameter is xml or svs the accept header should be application/xml')
        else if (not($format = 'json') and $acceptedType = 'application/json') then
            error($errors:BAD_REQUEST, 'In case of format parameter is ' || $format || ' the accept header can be anything but application/json')
        else ()   

    let $projectPrefix                  := if (empty($project)) then () else utillib:getDecor($project, $projectVersion, $projectLanguage)/project/@prefix 
    
    (: Return zero or more expanded valuesets wrapped in a &lt;return/&gt; element, and subsequently inside a &lt;repository&gt; element :)
    let $valueSets                      := if (empty($projectPrefix)) then vsapi:getValueSetExtract($idOrName, $effectiveDate) else vsapi:getValueSetExtract($projectPrefix, $projectVersion, $projectLanguage, $idOrName, $effectiveDate, $withversions)

    (: prepare response payload :)
    let $results                        :=
         if (empty($valueSets/*)) then () 
         else if($format = ('xml', 'json')) then (
            let $xmljson :=
                <valueSets>
                    {$valueSets/*}
                </valueSets>                   
            let $xmljson := 
            for $x in $xmljson
                return
                element {name($x)} {
                    $x/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($x/node(), $format)
                }
             (: in this case the json format overrides header application/xml and works with a text header :)
             let $xmljson               :=
                if ($format = 'json' and not($acceptedType = 'application/json')) then
                    fn:serialize($xmljson, map{"method": $format , "indent": true()})
                    else $xmljson
             return $xmljson
         )   
         else if ($format = 'svs') then vsapi:convertValueSet2Svs($valueSets, $projectLanguage, $projectVersion, 'svs') 
         else if ($format = 'csv') then vsapi:convertValueSet2Csv($valueSets, $projectLanguage, $projectPrefix)
         else if ($format = 'sql') then fn:string(transform:transform($valueSets, doc($setlib:strDecorServices ||'/resources/stylesheets/ToSql4ValueSets.xsl'), ()))
         else error($errors:BAD_REQUEST, 'Requested mime-type ' || $format || ' is not supported')
    
    (: prepare response header content-type and content-disposition :)
        let $contentType                    :=
            if ($format = 'svs') then 'application/xml'
            else if ($format = ('csv', 'sql')) then 'text/' || $format
            else 'application/' || $format
                
        let $fileExt                    := if ($format = 'svs') then 'xml' else $format
        let $fileName                   := 'VS_' || fn:distinct-values($valueSets//valueSet/@name) ||'_(download_' || substring(fn:string(current-dateTime()),1,19) || ').' || $fileExt
            
        let $r-header                   := 
            (response:set-header('Content-Type', $contentType || '; charset=utf-8'),
            response:set-header('Content-Disposition', 'filename='|| $fileName))
    
    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else $results

};

(:~ Retrieves DECOR valueSet view based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the valueSet
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the valueSet. If not given assumes latest version for id
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @param $projectLanguage         - optional parameter to select from a specific compiled language, also ui-language
    @param $inline                  - optional parameter to omit HTML header info. Useful for inclusion of HTML in other pages.
    @param $collapsable             - optional parameter the valueset is collapable.
    @return as-is 
    @since 2024-10-14
:)

declare function vsapi:getValueSetViewList($request as map(*)) {

    let $project                        := $request?parameters?project[not(. = '')]
    let $projectVersion                 := $request?parameters?release[not(. = '')]
    let $projectLanguage                := $request?parameters?language[not(. = '')]
    let $inline                         := $request?parameters?inline = true()
    let $collapsable                    := not($request?parameters?collapsable = false())
   
    let $check                          :=
        if (empty($project)) then 
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    
    let $decor                          := utillib:getDecor($project, $projectVersion, $projectLanguage)
    let $projectPrefix                  := $decor/project/@prefix
    
    let $check                  :=
        if (empty($projectPrefix)) then 
            error($errors:BAD_REQUEST, 'Project ' || $project || ' not found.')
        else ()
    
    (: Return zero or more expanded valuesets wrapped in a &lt;return/&gt; element, and subsequently inside a &lt;repository&gt; element :)
    let $results                      := 
        for $valueSet in vsapi:getValueSetList((), $projectPrefix, $projectVersion, $projectLanguage, (), (), false(), (), (), (), false())/valueSet/valueSet
        return
            vsapi:getValueSetExtract($projectPrefix, $projectVersion, $projectLanguage, $valueSet/(@id|@ref), $valueSet/@effectiveDate, false())

    let $valueSetNames              := distinct-values($results//valueSet/@name)
    let $fileNamePart               := if (count($valueSetNames)>1) then 'project_' || $projectPrefix || '('|| count($results//valueSet) || ')' else $valueSetNames
    let $fileName                   := 'VS_' || $fileNamePart ||'_(download_' || substring(fn:string(current-dateTime()),1,19) || ').html'
    
            
    let $r-header                       := 
        (response:set-header('Content-Type', 'text/html; charset=utf-8'),
        response:set-header('Content-Disposition', 'filename='|| $fileName))
    
    return
        if (empty($results/*)) then (
            roaster:response(404, ())
        )

        else 
        
        (: prepare for Html :)
        let $language                       := if (empty($projectLanguage)) then $setlib:strArtLanguage else $projectLanguage
        let $header                         := if ($inline) then false() else true()        
        
        return utilhtml:convertObject2Html($results, $language, $header, $collapsable, $projectVersion, $decor)

};


(:~ Retrieves DECOR valueSet view based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the valueSet
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the valueSet. If not given assumes latest version for id
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @param $projectLanguage         - optional parameter to select from a specific compiled language, also ui-language
    @param $inline                  - optional parameter to omit HTML header info. Useful for inclusion of HTML in other pages.
    @param $collapsable             - optional parameter the valueset is collapable.
    @return as-is 
    @since 2024-10-14
:)

declare function vsapi:getValueSetView($request as map(*)) {

    let $project                        := $request?parameters?project[not(. = '')]
    let $projectVersion                 := $request?parameters?release[not(. = '')]
    let $projectLanguage                := $request?parameters?language[not(. = '')]
    let $inline                         := $request?parameters?inline = true()
    let $collapsable                    := not($request?parameters?collapsable = false())
    
    let $idOrName                       := $request?parameters?idOrName[not(. = '')]
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    
    let $decor                          := if (empty($project)) then () else utillib:getDecor($project, $projectVersion, $projectLanguage)
            
    (: Return zero or more expanded valuesets wrapped in a &lt;return/&gt; element, and subsequently inside a &lt;repository&gt; element :)
    let $results                        := if (empty($decor/project/@prefix)) then vsapi:getValueSetExtract($idOrName, $effectiveDate) else vsapi:getValueSetExtract($decor/project/@prefix, $projectVersion, $projectLanguage, $idOrName, $effectiveDate, false()) 

    let $fileName                       := 'VS_' || fn:distinct-values($results//valueSet/@name) ||'_(download_' || substring(fn:string(current-dateTime()),1,19) || ').html'
            
    let $r-header                       := 
        (response:set-header('Content-Type', 'text/html; charset=utf-8'),
        response:set-header('Content-Disposition', 'filename='|| $fileName))
    
    return
        if (empty($results/*)) then (
            roaster:response(404, ())
        )

        else 
        
        (: prepare for Html :)
        let $language                       := if (empty($projectLanguage)) then $setlib:strArtLanguage else $projectLanguage
        let $header                         := if ($inline) then false() else true()        
        
        return utilhtml:convertObject2Html($results, $language, $header, $collapsable, $projectVersion, $decor)
       

};

(:~ Retrieves latest DECOR valueSet based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the valueSet
    @param $projectPrefix           - optional. limits scope to this project only
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @return as-is or as compiled as JSON
    @since 2020-05-03
:)
declare function vsapi:getLatestValueSet($request as map(*)) {
    vsapi:getValueSet($request)
};


(:~ Retrieves DECOR valueSet based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the valueSet
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the valueSet. If not given assumes latest version for id
    @param $projectPrefix           - optional. limits scope to this project only
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @return as-is or as compiled as JSON
    @since 2020-05-03
:)
declare function vsapi:getValueSet($request as map(*)) {
    
    let $projectPrefix                  := $request?parameters?prefix
    let $projectVersion                 := $request?parameters?release
    let $projectLanguage                := $request?parameters?language
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    (: only applicable when no effective date is given, if true all versions are given :)
    let $withversions                   := $request?parameters?versions = true()
    
    let $results                        := vsapi:getValueSet($projectPrefix, $projectVersion, $projectLanguage, $id, $effectiveDate, $withversions, true())
    let $results                        := 
        if ($withversions) then 
            <list artifact="VS" current="{count($results)}" total="{count($results)}" all="{count($results)}" lastModifiedDate="{current-dateTime()}">{$results}</list> 
        else (
            head($results)
        )
    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple valueSets for id '", $id, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        )
        else (
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
        )
};

(:~ Retrieves DECOR valueSet history based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id
    @param $effectiveDate           - optional parameter denoting the effectiveDate. If not given assumes latest version for id
    @return as-is or as compiled as JSON
    @since 2020-05-03
:)
declare function vsapi:getValueSetHistory($request as map(*)) {
    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $projectPrefix                  := ()
    let $results                        := histlib:ListHistory($authmap, $decorlib:OBJECTTYPE-VALUESET, (), $id, $effectiveDate, 0)
    
    return
        <list artifact="{$decorlib:OBJECTTYPE-VALUESET}" current="{count($results)}" total="{count($results)}" all="{count($results)}" lastModifiedDate="{current-dateTime()}">
        {
            utillib:addJsonArrayToElements($results)
        }
        </list>
};

(:~ Retrieves DECOR valueSet usage based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the valueSet
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the valueSet. If not given assumes latest version for id
    @param $projectPrefix           - optional. limits scope to this project only
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @return as-is or as compiled as JSON
    @since 2020-05-03
:)
declare function vsapi:getValueSetUsage($request as map(*)) {
    
    let $projectPrefix                  := $request?parameters?prefix
    let $projectVersion                 := $request?parameters?release
    let $projectLanguage                := $request?parameters?language
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    
    let $valueSetsAll                   :=
        if (empty($id)) then () else (
            if (empty($projectPrefix)) then
                vsapi:getValueSetById($id, ())
            else (
                vsapi:getValueSetById($id, (), $projectPrefix, $projectVersion, $projectLanguage)
            )
        )
    let $mostRecent                     := string(max($valueSetsAll/descendant-or-self::valueSet/xs:dateTime(@effectiveDate)))
    let $vs                             := 
        if (empty($effectiveDate)) then 
            $valueSetsAll/descendant-or-self::valueSet[@effectiveDate = $mostRecent] 
        else (
            $valueSetsAll/descendant-or-self::valueSet[@effectiveDate = $effectiveDate]
        )
    let $vs                             := $vs[1]
    
    (:let $check                          :=
        if (count($vs) le 1) then $vs else (
            error(xs:QName('vsapi:getValueSetUsage'), 'ValueSet ' || $id || ' effectiveDate ''' || $effectiveDate || ''' yields ' || count($vs) || ' results. Projects involved: ' || string-join(distinct-values($vs/ancestor-or-self::*/@ident), ', '))
        ):)
    
    let $isMostRecent                   := $mostRecent = $effectiveDate or empty($effectiveDate)
    let $effectiveDate                  := ($effectiveDate, $mostRecent)[. castable as xs:dateTime][1]
    
    let $allAssociations                := $setlib:colDecorData//terminologyAssociation[@valueSet = $id]
    let $allAssociations                :=
        if ($isMostRecent) then 
            $allAssociations[@flexibility = $effectiveDate] | $allAssociations[not(@flexibility castable as xs:dateTime)]
        else (
            $allAssociations[@flexibility = $effectiveDate]
        )
    let $allAssociations                :=
        if (empty($projectPrefix)) then $allAssociations else (
            for $ta in $allAssociations
            return
                if ($ta/ancestor::decor/project/@prefix = $projectPrefix) then $ta else ()
        )
        
    let $valueSetsAll                   := $setlib:colDecorData//terminology/valueSet//*[@ref = $id]
    let $valueSetsAll                   :=
        if ($isMostRecent) then 
            $valueSetsAll[@flexibility = $effectiveDate] | $valueSetsAll[not(@flexibility castable as xs:dateTime)]
        else (
            $valueSetsAll[@flexibility = $effectiveDate]
        )
    
    (: <valueSetExpansionSet id="5e09966c-e0bb-482b-8996-6ce0bb182bfd" effectiveDate="2023-08-08T14:25:32" statusCode="final">
          <valueSet ....>
       </valueSetExpansionSet>
    :)
    let $valueSetExpansionsAll          := collection($setlib:strValuesetExpansionData)/valueSetExpansionSet[valueSet[@id = $id][@effectiveDate = $effectiveDate]]
    
    let $conceptMapsAllSource           := $setlib:colDecorData//conceptMap/sourceScope[@ref = $id]
    let $conceptMapsAllSource           :=
        if ($isMostRecent) then 
            $conceptMapsAllSource[@flexibility = $effectiveDate] | $conceptMapsAllSource[not(@flexibility castable as xs:dateTime)]
        else (
            $conceptMapsAllSource[@flexibility = $effectiveDate]
        )
    
    let $conceptMapsAllTarget           := $setlib:colDecorData//conceptMap/targetScope[@ref = $id]
    let $conceptMapsAllTarget           :=
        if ($isMostRecent) then 
            $conceptMapsAllTarget[@flexibility = $effectiveDate] | $conceptMapsAllTarget[not(@flexibility castable as xs:dateTime)]
        else (
            $conceptMapsAllTarget[@flexibility = $effectiveDate]
        )
        
    let $allTemplAssociations           := $setlib:colDecorData//template//vocabulary[@valueSet = $id]
    let $allTemplAssociations           :=
        if ($isMostRecent) then 
            $allTemplAssociations[@flexibility = $effectiveDate] | $allTemplAssociations[not(@flexibility castable as xs:dateTime)]
        else (
            $allTemplAssociations[@flexibility = $effectiveDate]
        )
    
    let $url                            := serverapi:getServerURLServices()
    
    let $results                        := (
        for $item in $allAssociations
        let $clpfx  := $item/ancestor::decor/project/@prefix
        let $clid   := $item/@conceptId
        group by $clpfx, $clid
        return (
            let $originalConcept            := utillib:getConceptList($item[1]/@conceptId, ())/ancestor::concept[1]
            return
                if ($originalConcept) then utillib:doConceptAssociation($vs[1], $originalConcept, $originalConcept/name) else ()
        )
        ,
        for $item in $valueSetsAll
        let $vsid := $item/ancestor::valueSet[1]/@id
        let $vsed := $item/ancestor::valueSet[1]/@effectiveDate
        group by $vsid, $vsed
        return
            utillib:doValueSetAssociation($vs, $item[1])
        ,
        let $decorMap := map:merge(for $p in distinct-values($vs/ancestor-or-self::*/@ident) return map:entry($p, utillib:getDecorByPrefix($p)))
        for $item in $valueSetExpansionsAll
        return
            utillib:doValueSetExpansionAssociation($vs, $item[1], try { map:get($decorMap, ($vs/ancestor-or-self::*/@ident)[1]) } catch * {()})
        ,
        for $item in $conceptMapsAllSource | $conceptMapsAllTarget
        let $type := local-name($item)
        let $vsid := $item/ancestor::conceptMap[1]/@id
        let $vsed := $item/ancestor::conceptMap[1]/@effectiveDate
        (: group by type of association and then by valueSet. This 'may' list a 
        single conceptMap twice if valueSet is on source and target :)
        group by $type, $vsid, $vsed
        return
            utillib:doConceptMapAssociation($vs, $item[1], 
                for $l in distinct-values($item[1]/ancestor::decor/project/name/@language) 
                return 
                    <name language="{$l}">{data($item[1]/ancestor::conceptMap/@displayName)}</name>
            )
        ,
        for $item in $allTemplAssociations
        let $tmid := $item/ancestor::template[1]/@id
        let $tmed := $item/ancestor::template[1]/@effectiveDate
        group by $tmid, $tmed
        return
            utillib:doTemplateAssociation($vs, $item[1])
    )
    
    let $count                          := count($results)
    let $max                            := $count
    let $allcnt                         := $count
    
    return
        <list artifact="USAGE" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements(subsequence($results, 1, $max))
        }
        </list>
};

(:~ Returns a list of zero or more valuesets

@param $governanceGroupId - optional. determines search scope. null is full server, id limits scope to this projects under this governance group only. Does not mix with $projectPrefix
@param $projectPrefix    - optional. determines search scope. null is full server, pfx- limits scope to this project only. Does not mix with $governanceGroupId
@param $projectVersion   - optional. if empty defaults to current version. if valued then the valueset will come explicitly from that archived project version which is expected to be a compiled version
@param $projectLanguage  - optional. relevant in combination with $projectVersion if there are more than one languages the project is compiled in
@param $id               - optional. Identifier of the valueset to retrieve
@param $name             - optional. Name of the valueset to retrieve (valueSet/@name)
@param $flexibility      - optional. null gets all versions, 'dynamic' gets the newest version based on id or name, yyyy-mm-ddThh:mm:ss gets this specific version
@return Zero value sets in case no matches are found, one if only one exists or if a specific version was requested, or more if more versions exist and no specific version was requested
@since 2013-06-14
:)
declare function vsapi:getValueSetList($request as map(*)) {
    let $governanceGroupId      := $request?parameters?governanceGroupId
    let $projectPrefix          := $request?parameters?prefix
    let $projectVersion         := $request?parameters?release
    let $projectLanguage        := $request?parameters?language
    let $max                    := $request?parameters?max
    let $resolve                :=  if (empty($governanceGroupId)) then not($request?parameters?resolve = false()) else $request?parameters?resolve = true()
    let $searchTerms            := 
        array:flatten(
            for $s in ($request?parameters?search, $request?parameters?id, $request?parameters?name)[string-length() gt 0]
            return
                tokenize(normalize-space(lower-case($s)),'\s')
        )
    let $ed                     := $request?parameters?effectiveDate
    let $includebbr             := $request?parameters?includebbr = true()
    let $sort                   := $request?parameters?sort
    let $sortorder              := $request?parameters?sortorder
    
    let $check                  :=
        if (empty($governanceGroupId) and empty($projectPrefix)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have either parameter governanceGroupId or prefix')
        else 
        if (not(empty($governanceGroupId)) and not(empty($projectPrefix))) then 
            error($errors:BAD_REQUEST, 'Request SHALL have parameter governanceGroupId or prefix, not both')
        else ()
    let $check                  :=
        if (empty($governanceGroupId)) then () else if ($resolve) then
            error($errors:BAD_REQUEST, 'Request SHALL NOT have governance group scope and resolve=true. This is too expensive to support')
        else ()
    
    let $startT                 := util:system-time()
    
    let $result                 :=
        if (empty($governanceGroupId)) then
            vsapi:getValueSetList($governanceGroupId, $projectPrefix, $projectVersion, $projectLanguage, $searchTerms, $ed, $includebbr, $sort, $sortorder, $max, $resolve)
        else (
            for $projectId in ggapi:getLinkedProjects($governanceGroupId)/@ref
            return
                vsapi:getValueSetList($governanceGroupId, $projectId, (), (), $searchTerms, $ed, $includebbr, $sort, $sortorder, $max, $resolve)    
        )
    
    let $durationT              := (util:system-time() - $startT) div xs:dayTimeDuration("PT0.001S")
    
    return
        <list artifact="{$result[1]/@artifact}" elapsed="{$durationT}" current="{sum($result/xs:integer(@current))}" total="{sum($result/xs:integer(@total))}" all="{sum($result/xs:integer(@allcnt))}" resolve="{$resolve}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            if (empty($governanceGroupId)) then attribute project {$projectPrefix} else attribute governanceGroupId {$governanceGroupId},
            utillib:addJsonArrayToElements($result/*)
        }
        </list>
};

(:~ Deletes a valueSet reference based on $id (oid) and project id or prefix
    @param $id                      - required parameter denoting the id of the valueSet
    @param $project                 - required. limits scope to this project only
:)
declare function vsapi:deleteValueSet($request as map(*)) {
    
    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0][not(. = '*')]
    let $id                     := $request?parameters?id[string-length() gt 0]
    
    let $check                  :=
        if (empty($project) or empty($id)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have both parameter project and id')
        else ()
    
    let $decor                  := 
        if (utillib:isOid($project)) then
            utillib:getDecorById($project)
        else (
            utillib:getDecorByPrefix($project)
        )
    
    let $check                  :=
        if (empty($decor)) then
            error($errors:BAD_REQUEST, 'Project ' || $project || ' not found.')
        else ()
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-TERMINOLOGY)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify valueSets in project ', $project[1], '. You have to be an active author in the project.'))
        )
    
    let $delete                 := update delete $decor/terminology/valueSet[@ref = $id]
    
    return
        roaster:response(204, ())
};

(: Create a valueset, either empty or based on another valueSet

@param $projectPrefix project to create this scenario in
@param $targetDate If true invokes effectiveDate of the new valueSet and concepts as [DATE]T00:00:00. If false invokes date + time [DATE]T[TIME] 
@param $sourceId parameter denoting the id of a dataset to use as a basis for creating the new valueSet
@param $sourceEffectiveDate parameter denoting the effectiveDate of a dataset to use as a basis for creating the new valueSet",
@param $keepIds Only relevant if source dataset is specified. If true, the new valueSet will keep the same ids for the new valueSet, and only update the effectiveDate
@param $baseDatasetId Only relevant when a source dataset is specified and `keepIds` is false. This overrides the default base id for datasets in the project. The value SHALL match one of the projects base ids for datasets
@return (empty) dataset object as xml with json:array set on elements
:)
declare function vsapi:postValueSet($request as map(*)) {

    let $authmap                := $request?user
    let $projectPrefix          := $request?parameters?prefix[not(. = '')]
    let $targetDate             := $request?parameters?targetDate = true()
    let $sourceId               := $request?parameters?sourceId
    let $data                   := utillib:getBodyAsXml($request?body, 'valueSet', ())
    let $sourceEffectiveDate    := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?sourceEffectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?sourceEffectiveDate[string-length() gt 0]
        }
    let $refOnly                := $request?parameters?refOnly = true()
    let $keepIds                := $request?parameters?keepIds = true()
    let $baseId                 := $request?parameters?baseId
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($projectPrefix)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter prefix')
        else ()
    
    let $results                := vsapi:createValueSet($authmap, $projectPrefix, $targetDate, $sourceId, $sourceEffectiveDate, $refOnly, $keepIds, $baseId, $data)
    
    return
        roaster:response(201, 
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
        
        )
};

(:~ Update valueSet statusCode, versionLabel, expirationDate and/or officialReleaseDate

@param $authmap required. Map derived from token
@param $id required. DECOR concept/@id to update
@param $effectiveDate required. DECOR concept/@effectiveDate to update
@param $recurse optional as xs:boolean. Default: false. Allows recursion into child particles to apply the same updates. Not implemented (yet)
@param $listOnly optional as xs:boolean. Default: false. Allows to test what will happen before actually applying it
@param $newStatusCode optional as xs:string. Default: empty. If empty, does not update the statusCode
@param $newVersionLabel optional as xs:string. Default: empty. If empty, does not update the versionLabel
@param $newExpirationDate optional as xs:string. Default: empty. If empty, does not update the expirationDate 
@param $newOfficialReleaseDate optional as xs:string. Default: empty. If empty, does not update the officialReleaseDate 
@return list object with success and/or error elements or error
:)
declare function vsapi:postValueSetStatus($request as map(*)) {

    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    (: Note: we _could_ implement traversing into value set inclusions or even attached codesystems through this parameter. In AD2 however we never implemented this and we did not plan for introducing this in AD3 nor was it ever asked of us.
             Hence the provision is made should it ever be needed, but other than that, it is not implemented.
    :)
    let $recurse                        := $request?parameters?recurse = true()
    let $list                           := $request?parameters?list = true()
    let $statusCode                     := $request?parameters?statusCode
    let $versionLabel                   := $request?parameters?versionLabel
    let $expirationDate                 := $request?parameters?expirationDate
    let $officialReleaseDate            := $request?parameters?officialReleaseDate
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    
    return
        vsapi:setValueSetStatus($authmap, $id, $effectiveDate, $recurse, $list, $statusCode, $versionLabel, $expirationDate, $officialReleaseDate)
};

(:~ Update DECOR valueSet

@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $id                       - required. the id for the valueSet to update 
@param $request-body             - required. json body containing new valueSet structure
@return valueSet structure including generated meta data
@since 2020-05-03
:)
declare function vsapi:putValueSet($request as map(*)) {

    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $data                           := utillib:getBodyAsXml($request?body, 'valueSet', ())
    let $deletelock                     := $request?parameters?deletelock = true()
    
    (:let $s                              := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data):) 
    
    let $check                          :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                          :=
        if (empty($data)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $return                         := vsapi:putValueSet($authmap, $id, $effectiveDate, $data, $deletelock)
    return (
        roaster:response(200, $return)
    )

};

(:~ Update DECOR valueSet parts. Expect array of parameter objects, each containing RFC 6902 compliant contents. Note: RestXQ does not do PATCH (yet), but that would be the preferred option.

{ "op": "[add|remove|replace]", "path": "e.g. [/statusCode|/expirationDate|/officialReleaseDate|/canonicalUri|/versionLabel|/name|/displayName|/experimental|/desc|/publishingAuthority|/copyright|/completeCodeSystem|/conceptList]", "value": "[string|object]" }

where

* op - add & replace (statusCode, expirationDate, officialReleaseDate, canonicalUri, versionLabel, name, displayName, experimental, desc, publishingAuthority, copyright, completeCodeSystem, conceptList) or remove (desc, publishingAuthority, copyright, completeCodeSystem, conceptList)

* path - see above

* value - string when the path is not /. object when path is /
@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $id                       - required. the id for the valueSet to update 
@param $request-body             - required. json body containing array of parameter objects each containing RFC 6902 compliant contents
@return valueSet structure
@since 2020-05-03
@see http://tools.ietf.org/html/rfc6902
:)
declare function vsapi:patchValueSet($request as map(*)) {

    let $authmap                := $request?user
    let $id                     := $request?parameters?id
    let $effectiveDate          := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $data                   := utillib:getBodyAsXml($request?body, 'parameters', ())
    
    (:let $s                      := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data) :)
    
    let $check                  :=
        if ($data) then () else (
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        )
    
    let $return                 := vsapi:patchValueSet($authmap, string($id), string($effectiveDate), $data)
    return (
        roaster:response(200, $return)
    )

};

(:~ Central logic for patching an existing valueSet statusCode, versionLabel, expirationDate and/or officialReleaseDate

@param $authmap required. Map derived from token
@param $id required. DECOR concept/@id to update
@param $effectiveDate required. DECOR concept/@effectiveDate to update
@param $recurse optional as xs:boolean. Default: false. Allows recursion into child particles to apply the same updates
@param $listOnly optional as xs:boolean. Default: false. Allows to test what will happen before actually applying it
@param $newStatusCode optional as xs:string. Default: empty. If empty, does not update the statusCode
@param $newVersionLabel optional as xs:string. Default: empty. If empty, does not update the versionLabel
@param $newExpirationDate optional as xs:string. Default: empty. If empty, does not update the expirationDate 
@param $newOfficialReleaseDate optional as xs:string. Default: empty. If empty, does not update the officialReleaseDate 
@return list object with success and/or error elements or error
:)
declare function vsapi:setValueSetStatus($authmap as map(*), $id as xs:string, $effectiveDate as xs:string?, $recurse as xs:boolean, $listOnly as xs:boolean, $newStatusCode as xs:string?, $newVersionLabel as xs:string?, $newExpirationDate as xs:string?, $newOfficialReleaseDate as xs:string?) as element(list) {
    (:get object for reference:)
    let $object                 := 
        if (empty($effectiveDate)) then (
            let $ttt    := $setlib:colDecorData//valueSet[@id = $id]
            return
                $ttt[@effectiveDate = max($ttt/xs:dateTime(@effectiveDate))]
        )
        else (
            $setlib:colDecorData//valueSet[@id = $id][@effectiveDate = $effectiveDate]
        )
    let $decor                  := $object/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if (count($object) = 1) then () else
        if ($object) then
            error($errors:SERVER_ERROR, 'ValueSet id ''' || $id || ''' effectiveDate ''' || $effectiveDate || ''' cannot be updated because multiple ' || count($object) || ' were found in: ' || string-join(distinct-values($object/ancestor::decor/project/@prefix), ' / ') || '. Please inform your database administrator.')
        else (
            error($errors:BAD_REQUEST, 'ValueSet id ''' || $id || ''' effectiveDate ''' || $effectiveDate || ''' cannot be updated because it cannot be found.')
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-TERMINOLOGY)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify valueSets in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
        
    let $testUpdate             :=
        utillib:applyStatusProperties($authmap, $object, $object/@statusCode, $newStatusCode, $newVersionLabel, $newExpirationDate, $newOfficialReleaseDate, $recurse, true(), $projectPrefix)
    let $results                :=
        if ($testUpdate[self::error]) then $testUpdate else
        if ($listOnly) then $testUpdate else (
            utillib:applyStatusProperties($authmap, $object, $object/@statusCode, $newStatusCode, $newVersionLabel, $newExpirationDate, $newOfficialReleaseDate, $recurse, false(), $projectPrefix)
        )
    return
    if ($results[self::error] and not($listOnly)) then
        error($errors:BAD_REQUEST, string-join(
            for $e in $results[self::error]
            return
                $e/@itemname || ' id ''' || $e/@id || ''' effectiveDate ''' || $e/@effectiveDate || ''' cannot be updated: ' || data($e)
            , ' ')
        )
    else (
        <list artifact="STATUS" current="{count($results)}" total="{count($results)}" all="{count($results)}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            (: max 2 arrays: one with error and one with success :)
            utillib:addJsonArrayToElements($results)
        }
        </list>
    )
};

(:~ Central logic for updating an existing valueSet

@param $authmap         - required. Map derived from token
@param $id              - required. DECOR valueSet/@id to update
@param $request-body    - required. DECOR valueSet xml element containing everything that should be in the updated valueSet
@return valueSet object as xml with json:array set on elements
:)
declare function vsapi:putValueSet($authmap as map(*), $id as xs:string, $effectiveDate as xs:string, $data as element(), $deletelock as xs:boolean) as element(valueSet) {

    let $storedValueSet         := $setlib:colDecorData//valueSet[@id = $id][@effectiveDate = $effectiveDate] 
    let $decor                  := $storedValueSet/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix
    
    let $lock                   := decorlib:getLocks($authmap, $id, $effectiveDate, ())
    let $lock                   := if ($lock) then $lock else decorlib:setLock($authmap, $id, $effectiveDate, false())
    
    let $check                  :=
        if ($storedValueSet) then () else (
            error($errors:BAD_REQUEST, 'ValueSet with id ''' || $id || ''' and effectiveDate ''' || $effectiveDate || ' does not exist')
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-TERMINOLOGY)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify valueSets in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if ($lock) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have a lock for this valueSet (anymore). Get a lock first.'))
        )    
    let $check                  :=
        if ($storedValueSet[@statusCode = $vsapi:STATUSCODES-FINAL]) then
            error($errors:BAD_REQUEST, concat('ValueSet cannot be updated while it has one of status: ', string-join($vsapi:STATUSCODES-FINAL, ', '), if ($storedValueSet/@statusCode = 'pending') then 'You should switch back to status draft to edit.' else ()))
        else ()
    let $check                  :=
        if ($data[@id = $id] | $data[empty(@id)]) then () else (
            error($errors:BAD_REQUEST, concat('Submitted data shall have no valueSet id or the same valueSet id as the valueSet id ''', $id, ''' used for updating. Found in request body: ', ($data/@id, 'null')[1]))
        )
    let $check                  :=
        if ($data[@effectiveDate = $effectiveDate] | $data[empty(@effectiveDate)]) then () else (
            error($errors:BAD_REQUEST, concat('Submitted data shall have no valueSet effectiveDate or the same valueSet effectiveDate as the valueSet effectiveDate ''', $effectiveDate, ''' used for updating. Found in request body: ', ($data/@effectiveDate, 'null')[1]))
        )
    let $check                  := 
        if ($data//exclude[@codeSystem][empty(@code)] | $data//items[@is = 'exclude'][@codeSystem][empty(@code)]) then (
            error($errors:BAD_REQUEST, 'Value set with exclude on complete code system not supported. Exclude SHALL have both @code and @codeSystem')
        ) else ()
    let $css                    := $data//(include | exclude |items[@is=('include', 'exclude')])[@codeSystem][empty(@code)]/@codeSystem
    let $check                  := 
        if (count($css) = count(distinct-values($css))) then () else (
             error($errors:BAD_REQUEST, 'Value set SHALL contain max 1 include per complete code system.')
        )
    let $css                    := $data//(include | exclude |items[@is=('include', 'exclude')])/@ref
    let $check                  := 
        if (count($css) = count(distinct-values($css))) then () else (
            error($errors:BAD_REQUEST, 'Value set SHALL contain max 1 include/exclude per value set regardless of its version.')
        )
    let $check                  := 
        if ($css = $id) then 
            error($errors:BAD_REQUEST, 'Value set SHALL NOT contain itself as inclusion.')
        else ()
    let $baseValueset           := 
        <valueSet>
        {
            attribute id {$id} ,
            $data/@name[string-length()>0] ,
            $data/@displayName[string-length()>0] ,
            attribute effectiveDate {$effectiveDate} ,
            attribute statusCode {"draft"} ,
            $data/@versionLabel[string-length()>0] ,
            $data/@expirationDate[. castable as xs:dateTime] ,
            $data/@officialReleaseDate[. castable as xs:dateTime] ,
            $data/@experimental[. = ('true', 'false')] ,
            $data/@canonicalUri[string-length()>0],
            $data/@lastModifiedDate[string-length()>0]
        }
        </valueSet>
    let $newValueSet            := utillib:prepareValueSetForUpdate($data, $storedValueSet)
    
    (: save history:)
    let $intention              := if ($storedValueSet[@statusCode = 'final']) then 'patch' else 'version'
    let $history                := histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-VALUESET, $projectPrefix, $intention, $storedValueSet)
    
    (: now update the value set :)
    let $valueSetUpdate         := update replace $storedValueSet with $newValueSet
    let $delete                 := update delete $storedValueSet//@json:array
    let $deleteLock             := if ($deletelock) then update delete $lock else ()
    
    let $result                 := vsapi:getValueSet($projectPrefix, (), (), $id, $effectiveDate, false(), true())
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

(:~ Central logic for patching an existing valueSet

@param $authmap         - required. Map derived from token
@param $id              - required. DECOR valueSet/@id to update
@param $effectiveDate   - required. DECOR valueSet/@effectiveDate to update
@param $data            - required. DECOR xml element parameter elements each containing RFC 6902 compliant contents
@return dataset object as xml with json:array set on elements
:)
declare function vsapi:patchValueSet($authmap as map(*), $id as xs:string, $effectiveDate as xs:string, $data as element(parameters)) as element(valueSet) {

    let $storedValueSet         := $setlib:colDecorData//valueSet[@id = $id][@effectiveDate = $effectiveDate]
    let $decor                  := $storedValueSet/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix

    let $lock                   := decorlib:getLocks($authmap, $id, $effectiveDate, ())
    let $lock                   := if ($lock) then $lock else decorlib:setLock($authmap, $id, $effectiveDate, false())
    
    let $check                  :=
        if ($storedValueSet) then () else (
            error($errors:BAD_REQUEST, 'ValueSet with id ' || $id || ' and effectiveDate ' || $effectiveDate || ' does not exist')
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-TERMINOLOGY)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify valueSets in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if ($storedValueSet[@statusCode = $vsapi:STATUSCODES-FINAL]) then
            if ($data/parameter[@path = '/statusCode'][@op = 'replace'][not(@value = $vsapi:STATUSCODES-FINAL)]) then () else
            if ($data[count(parameter) = 1]/parameter[@path = '/statusCode']) then () else (
                error($errors:BAD_REQUEST, concat('ValueSet cannot be patched while it has one of status: ', string-join($vsapi:STATUSCODES-FINAL, ', '), if ($storedValueSet/@statusCode = 'pending') then 'You should switch back to status draft to edit.' else ()))
            )
        else ()
    let $check                  :=
        if ($lock) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have a lock for this valueSet (anymore). Get a lock first.'))
        )
    let $check                  :=
        if ($data[parameter]) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall be an array of ''parameter''.')
        )
    let $unsupportedops         := $data/parameter[not(@op = ('add', 'replace', 'remove'))]
    let $check                  :=
        if ($unsupportedops) then
            error($errors:BAD_REQUEST, 'Submitted parameters shall have supported ''op'' value. Found ''' || string-join(distinct-values($unsupportedops/@op), ''', ''') || ''', expected ''add'', ''replace'', or ''remove''') 
        else ()
    
    let $check                  :=
        for $param in $data/parameter
        let $op         := $param/@op
        let $path       := $param/@path
        let $value      := $param/@value
        let $pathpart   := substring-after($path, '/')
        return
            switch ($path)
            case '/statusCode' return (
                if ($op = 'remove') then
                    'Parameter path ''' || $path || ''' does not support ' || $op
                else 
                if (utillib:isStatusChangeAllowable($storedValueSet, $value)) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Supported are: ' || string-join(map:get($utillib:itemstatusmap, string($storedValueSet/@statusCode)), ', ')
                )
            )
            case '/expirationDate'
            case '/officialReleaseDate' return (
                if ($op = 'remove') then () else 
                if ($value castable as xs:dateTime) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL be yyyy-mm-ddThh:mm:ss.'
                )
            )
            case '/canonicalUri'
            case '/versionLabel' return (
                if ($op = 'remove') then () else 
                if ($value[not(. = '')]) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL NOT be empty.'
                )
            )
            case '/name' 
            case '/displayName' return (
                if ($op = 'remove') then
                    'Parameter path ''' || $path || ''' does not support ' || $op
                else 
                if ($value[not(. = '')]) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL NOT be empty.'
                )
            )
            case '/experimental' return (
                if ($op = 'remove') then () else 
                if ($value[. = ('true', 'false')]) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL be true or false.'
                )
            )
            case '/desc' 
            case '/copyright' return (
                if ($param[count(value/*[name() = $pathpart]) = 1]) then (
                    if ($param/value/*[name() = $pathpart][@inherited]) then 
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Contents SHALL NOT be marked ''inherited''.'
                    else (),
                    if ($param/value/*[name() = $pathpart][matches(@language, '[a-z]{2}-[A-Z]{2}')]) then
                        if ($param[@op = 'remove'] | $param/value/*[name() = $pathpart][.//text()[not(normalize-space() = '')]]) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A ' || $pathpart || ' SHALL have contents.'
                        )
                    else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A ' || $pathpart || ' SHALL have a language with pattern ll-CC.'
                    )
                )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/*[name() = $pathpart]) 
                )
            )
            case '/purpose' return (
                if ($param[count(value/*[name() = $pathpart]) = 1]) then
                    if ($param/value/*[name() = $pathpart][matches(@language, '[a-z]{2}-[A-Z]{2}')]) then
                        if ($param[@op = 'remove'] | $param/value/*[name() = $pathpart][.//text()[not(normalize-space() = '')]]) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A ' || $pathpart || ' SHALL have contents.'
                        )
                    else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A ' || $pathpart || ' SHALL have a language with pattern ll-CC.'
                    )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/*[name() = $pathpart]) 
                )
            )
            case '/publishingAuthority' return (
                if ($param[count(value/publishingAuthority) = 1]) then (
                    if ($param/value/publishingAuthority[@inherited]) then 
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Contents SHALL NOT be marked ''inherited''.'
                    else (),
                    if ($param/value/publishingAuthority[@id]) then
                        if ($param/value/publishingAuthority[utillib:isOid(@id)]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || ''' publishingAuthority.id SHALL be an OID is present. Found ' || string-join($param/value/publishingAuthority/@id, ', ')
                        )
                    else (),
                    if ($param/value/publishingAuthority[@name[not(. = '')]]) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || ''' publishingAuthority.name SHALL be a non empty string.'
                    ),
                    let $unsupportedtypes := $param/value/publishingAuthority/addrLine/@type[not(. = $vsapi:ADDRLINE-TYPE/enumeration/@value)]
                    return
                        if ($unsupportedtypes) then
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Publishing authority has unsupported addrLine.type value(s) ' || string-join($unsupportedops, ', ') || '. SHALL be one of: ' || string-join($vsapi:ADDRLINE-TYPE/enumeration/@value, ', ')
                        else ()
                ) else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' SHALL have a single publishingAuthority under value. Found ' || count($param/value/publishingAuthority)
                )
            )
            (:case '/completeCodeSystem' return (
                if ($param[count(value/completeCodeSystem) = 1]) then (
                    if ($param/value/completeCodeSystem[utillib:isOid(@codeSystem)]) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || '''. CompleteCodeSystem has unsupported value for .codeSystem ' || string-join($param/value/completeCodeSystem/@codeSystem, ', ') || '. SHALL a valid oid.'
                    )(\:,
                    if ($param/value/completeCodeSystem/@type[not(. = 'D')]) then
                        'Parameter ' || $op || ' not allowed for ''' || $path || '''. CompleteCodeSystem has unsupported value for .type ' || string-join($param/value/completeCodeSystem/@type, ', ') || '. SHALL ''D'' if present.'
                    else ():\)
                    ,
                    if ($param/value/completeCodeSystem/filter) then (
                        if ($param/value/completeCodeSystem/filter[.[empty(@property)] | .[empty(@op)] | .[empty(@value)]]) then
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. CompleteCodeSystem ' || string-join($param/value/completeCodeSystem/@codeSystem, ', ') || ' has at least one filter that is missing property, op or value.'
                        else (),
                        let $unsupops   := $param/value/completeCodeSystem/filter[not(@op = $vsapi:INTENSIONALOPERATORS-TYPE/enumeration/@value)]
                        return
                        if ($unsupops) then
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''.  CompleteCodeSystem ' || string-join($param/value/completeCodeSystem/@codeSystem, ', ') || ' has at least one filter with an unsupported value for ''op''. Found: ''' || string-join($unsupops/@op, ', ') || '''. SHALL be one of: ' || string-join($vsapi:INTENSIONALOPERATORS-TYPE/enumeration/@value, ', ')
                        else ()
                    )
                    else ()
                ) else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' SHALL have a single completeCodeSystem under value. Found ' || count($param/value/completeCodeSystem)
                )
            ):)
            case '/items' return (
                if ($op = 'remove') then () else
                if ($param[count(value/items) ge 1]) then (
                    (:for $concept in (   $param/value/items[@is='completeCodeSystem'])
                    let $is     := ($concept/@is, name($concept))[1]
                    return (
                        (\:if ($concept/@type[not(. = 'D')]) then
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. ' || $is || ' has unsupported .type value ''' || $concept/@type || '''. SHALL be D'
                        else (),:\)
                        if ($concept/@codeSystem[utillib:isOid(.)]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. ' || $is || ' has unsupported .codeSystem value ''' || $concept/@codeSystem || '''. SHALL be a valid oid'
                        ),
                        if ($concept[empty(@flexibility)] | $concept[@flexibility = 'dynamic'] | $concept[@flexibility castable as xs:dateTime]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. ' || $is || ' has unsupported .codeSystem value ''' || $concept/@codeSystem || '''. SHALL be a valid dateTime or ''dynamic'''
                        ),
                        if ($concept/filter) then (
                            if ($concept/filter[.[empty(@property)] | .[empty(@op)] | .[empty(@value)]]) then
                                'Parameter ' || $op || ' not allowed for ''' || $path || '''. CompleteCodeSystem ' || string-join($concept/@codeSystem, ', ') || ' has at least one filter that is missing property, op or value.'
                            else (),
                            let $unsupops   := $concept/filter[not(@op = $vsapi:INTENSIONALOPERATORS-TYPE/enumeration/@value)]
                            return
                            if ($unsupops) then
                                'Parameter ' || $op || ' not allowed for ''' || $path || '''.  CompleteCodeSystem ' || string-join($concept/@codeSystem, ', ') || ' has at least one filter with an unsupported value for ''op''. Found: ''' || string-join($unsupops/@op, ', ') || '''. SHALL be one of: ' || string-join($vsapi:INTENSIONALOPERATORS-TYPE/enumeration/@value, ', ')
                            else ()
                        )
                        else ()
                    ),:)
                    for $concept in (   $param/value/items[@is='concept'] | 
                                        $param/value/items[@is='exception'])
                    let $is     := ($concept/@is, name($concept))[1]
                    return (
                        if ($concept/@type[. = $vsapi:VOCAB-TYPE/enumeration/@value]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. ' || $is || ' has unsupported .type value ''' || $concept/@type || '''. SHALL be one of: ' || string-join($vsapi:VOCAB-TYPE/enumeration/@value, ', ')
                        ),
                        if ($concept/@level[. castable as xs:nonNegativeInteger]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. ' || $is || ' has unsupported .level value ''' || $concept/@level || '''. SHALL be 0 or higher'
                        ),
                        if ($concept/@codeSystem[utillib:isOid(.)]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. ' || $is || ' has unsupported .codeSystem value ''' || $concept/@codeSystem || '''. SHALL be a valid oid'
                        ),
                        if ($concept/@code[matches(., '^\S+$')]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. ' || $is || ' has unsupported .code value ''' || $concept/@code || '''. SHALL be populated and SHALL NOT have whitespace'
                        ),
                        if ($concept/@displayName[not(. = '')]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. ' || $is || ' has unsupported .displayName value ''' || $concept/@displayName || '''. SHALL be populated'
                        )
                    ),
                    for $concept in (   $param/value/items[@is='include'] | 
                                        $param/value/items[@is='exclude'])
                    let $is     := ($concept/@is, name($concept))[1]
                    return (
                        if ($concept[empty(@op)] | $concept[@op = $vsapi:INTENSIONALOPERATORS-TYPE/enumeration/@value]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. ' || $is || ' has unsupported .op value ''' || $concept/@op || '''. SHALL be one of: ' || string-join($vsapi:INTENSIONALOPERATORS-TYPE/enumeration/@value, ', ')
                        ),
                        if ($concept[@code][@flexibility]) then
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. ' || $is || ' has unsupported combination of attributes. SHALL NOT have .code and .flexibility'
                        else
                        if ($concept[empty(@ref | @codeSystem)][@flexibility]) then
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. ' || $is || ' has unsupported combination of attributes. SHALL NOT have .flexibility without .codeSystem or .ref'
                        else
                        if ($concept[empty(@code)][@op]) then
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. ' || $is || ' has unsupported combination of attributes. SHALL NOT have .op without .code'
                        else
                        if ($concept[@ref][@op]) then
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. ' || $is || ' has unsupported combination of attributes. SHALL NOT have .op and .ref'
                        else
                        if ($concept[empty(@codeSystem)] | $concept[utillib:isOid(@codeSystem)]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. ' || $is || ' has unsupported .codeSystem value ''' || $concept/@codeSystem || '''. SHALL be a valid oid'
                        ),
                        if ($concept[empty(@code)] | $concept[matches(@code, '^\S+$')]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. ' || $is || ' has unsupported .codeSystem value ''' || $concept/@code || '''. SHALL NOT have whitespace'
                        ),
                        if ($concept/@ref = $storedValueSet/(@id | @ref)) then 
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. ValueSet ' || $is || ' SHALL NOT include itself'
                        else (),
                        if ($concept/filter) then (
                            if ($concept/filter[.[empty(@property)] | .[empty(@op)] | .[empty(@value)]]) then
                                'Parameter ' || $op || ' not allowed for ''' || $path || '''. ' || $is || string-join($concept/@codeSystem, ', ') || ' has at least one filter that is missing property, op or value.'
                            else (),
                            let $unsupops   := $concept/filter[not(@op = $vsapi:INTENSIONALOPERATORS-TYPE/enumeration/@value)]
                            return
                            if ($unsupops) then
                                'Parameter ' || $op || ' not allowed for ''' || $path || '''. ' || $is || string-join($concept/@codeSystem, ', ') || ' has at least one filter with an unsupported value for ''op''. Found: ''' || string-join($unsupops/@op, ', ') || '''. SHALL be one of: ' || string-join($vsapi:INTENSIONALOPERATORS-TYPE/enumeration/@value, ', ')
                            else ()
                        )
                        else (),
                        if ($is = 'exclude') then 
                            if ($concept[@codeSystem][empty(@code)]) then
                                'Parameter ' || $op || ' not allowed for ''' || $path || '''. ' || $is || ' SHALL have both .code and .codeSystem. Exclusion of complete code system not supported'
                            else ()
                        else ()
                    ),
                    let $css                := $param/value/items[@is=('include', 'exclude')][@codeSystem][empty(@code)]/@codeSystem
                    return
                        if (count($css) = count(distinct-values($css))) then () else (
                             'Parameter ' || $op || ' not allowed for ''' || $path || '''. Value set SHALL contain max 1 include per complete code system.'
                        )
                    ,
                    let $css                := $param/value/items[@is=('include', 'exclude')]/@ref
                    return
                        if (count($css) = count(distinct-values($css))) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Value set SHALL contain max 1 include/exclude per value set regardless of its version.'
                        )
                ) else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' SHALL have one or more items under value. Found ' || count($param/value/items)
                )
            )
            default return (
                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Path value not supported'
            )
     let $check                 :=
        if (empty($check)) then () else (
            error($errors:BAD_REQUEST,  string-join ($check, ' '))
        )
    
    let $intention              := if ($storedValueSet[@statusCode = 'final']) then 'patch' else 'version'
    let $update                 := histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-VALUESET, $projectPrefix, $intention, $storedValueSet)
    
    let $update                 :=
        for $param in $data/parameter
        return
            switch ($param/@path)
            case '/statusCode'
            case '/expirationDate'
            case '/officialReleaseDate'
            case '/canonicalUri'
            case '/versionLabel'
            case '/name'
            case '/displayName'
            case '/experimental' return (
                let $attname  := substring-after($param/@path, '/')
                let $new      := attribute {$attname} {$param/@value}
                let $stored   := $storedValueSet/@*[name() = $attname]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedValueSet
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/desc' return (
                (: only one per language :)
                let $elmname  := substring-after($param/@path, '/')
                let $new      := utillib:prepareFreeFormMarkupWithLanguageForUpdate($param/value/desc)
                let $stored   := $storedValueSet/*[name() = $elmname][@language = $param/value/desc/@language]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedValueSet
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/purpose' return (
                (: only one possible per language :)
                let $elmname  := substring-after($param/@path, '/')
                let $new      := utillib:prepareFreeFormMarkupWithLanguageForUpdate($param/value/purpose)
                let $stored   := $storedValueSet/*[name() = $elmname][@language = $param/value/purpose/@language]
                
                return
                switch ($param/@op)
                case 'add' (: fall through :)
                case 'replace' return if ($stored) then update replace $stored with $new else update insert $new into $storedValueSet
                case 'remove' return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/publishingAuthority' return (
                (: only one possible :)
                let $new      := utillib:preparePublishingAuthorityForUpdate($param/value/publishingAuthority)
                let $stored   := $storedValueSet/publishingAuthority
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedValueSet
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/copyright' return (
                (: only one per language :)
                let $new      := utillib:prepareFreeFormMarkupWithLanguageForUpdate($param/value/copyright)
                let $stored   := $storedValueSet/copyright[@language = $param/value/copyright/@language]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedValueSet
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            (:case '/completeCodeSystem' return (
                let $new      := 
                    <completeCodeSystem>
                    {
                        $param/value/completeCodeSystem/@codeSystem[not(. = '')],
                        $param/value/completeCodeSystem/@codeSystemName[not(. = '')],
                        $param/value/completeCodeSystem/@codeSystemVersion[not(. = '')],
                        $param/value/completeCodeSystem/@flexibility[not(. = '')],
                        $param/value/completeCodeSystem/@type[. = 'D'],
                        for $f in $param/value/completeCodeSystem/filter
                        return
                            <filter>{$f/@property, $f/@op, $f/@value}</filter>
                    }
                    </completeCodeSystem>
                let $stored   := $storedValueSet/completeCodeSystem[@codeSystem = $new/@codeSystem]
                
                return
                switch ($param/@op)
                case 'add'
                case 'replace' return if ($stored) then update replace $stored with $new else update insert $new into $storedValueSet
                case 'remove' return update delete $stored
                default return ( (\: unknown op :\) )
            ):)
            case '/items' return (
                (:let $newcs    := 
                    for $completeCodeSystem in $param/value/items[@is = 'completeCodeSystem']
                    return
                        <completeCodeSystem>
                        {
                            $completeCodeSystem/@codeSystem, 
                            $completeCodeSystem/@codeSystemName[not(. = '')], 
                            $completeCodeSystem/@codeSystemVersion[not(. = '')], 
                            $completeCodeSystem/@flexibility[not(. = '')],
                            $completeCodeSystem/@type[. = 'D'],
                            for $f in $completeCodeSystem/filter
                            return
                                <filter>{$f/@property, $f/@op, $f/@value}</filter>
                        }
                        </completeCodeSystem>:)
                let $newcl    := utillib:prepareValueSetConceptListForUpdate($param/value/items)
                let $stored   := $storedValueSet/conceptList | $storedValueSet/completeCodeSystem
                
                let $delete   := update delete $stored
                
                return
                    switch ($param/@op)
                    case 'add'
                    case 'replace' return update insert $newcl into $storedValueSet
                    case 'remove' return ()
                    default return ( (: unknown op :) )
            )
            default return ( (: unknown path :) )
    
    (: after all the updates, the XML Schema order is likely off. Restore order :)
    let $preparedValueSet       := utillib:prepareValueSetForUpdate($storedValueSet, $storedValueSet)
    
    let $update                 := update replace $storedValueSet with $preparedValueSet
    let $update                 := update delete $lock
    
    let $result                 := vsapi:getValueSet($projectPrefix, (), (), $preparedValueSet/@id, $preparedValueSet/@effectiveDate, false(), true())
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

(:~ 
@param $doContents If we only need the valueSet for the list we can forego the contents
:)
declare %private function vsapi:getValueSet($projectPrefix as xs:string*, $projectVersion as xs:string*, $projectLanguage as xs:string?, $id as xs:string, $effectiveDate as xs:string?, $withversions as xs:boolean, $doContents as xs:boolean) as element(valueSet)* {
    let $id                     := $id[not(. = '')]
    let $effectiveDate          := if ($withversions) then () else ($effectiveDate[not(. = '')], 'dynamic')[1]
    let $projectPrefix          := $projectPrefix[not(. = '')][1]
    let $projectVersion         := $projectVersion[not(. = '')][1]
    let $projectLanguage        := $projectLanguage[not(. = '')][1]
    
    let $results                :=
        if (empty($projectPrefix)) then (
            vsapi:getValueSetById($id, $effectiveDate)
        )
        else (
            vsapi:getValueSetById($id, $effectiveDate, $projectPrefix, $projectVersion, $projectLanguage)
        )
    let $results                :=
        if ($results) then $results else ( 
            if (empty($projectPrefix)) then 
                $setlib:colDecorData//valueSet[@ref = $id] | $setlib:colDecorCache//valueSet[@ref = $id]
            else (
                utillib:getDecor($projectPrefix, $projectVersion, $projectLanguage)//valueSet[@ref = $id]
            )
        )[last()]
    
    for $vs in $results
    let $id             := $vs/@id | $vs/@ref
    let $ed             := $vs/@effectiveDate
    let $projectPrefix  := ($vs/ancestor::*/@bbrident, $vs/ancestor::decor/project/@prefix)[1]
    order by $id, $ed descending
    return
        element {name($vs)}
        {
            $vs/@*,
            if ($vs[@id]) then (
                if ($vs/@url) then () else attribute url {($vs/ancestor::*/@bbrurl, serverapi:getServerURLServices())[1]},
                if ($vs/@ident) then () else attribute ident {$projectPrefix}
            ) else ()
            ,
            if ($doContents) then (
                $vs/desc,
                if ($vs/sourceCodeSystem) then $vs/sourceCodeSystem else (
                    vsapi:createSourceCodeSystems($vs, ($vs/ancestor::decor, $projectPrefix)[1], ($projectLanguage, $vs/ancestor::decor/project/@defaultLanguage)[1])
                ),
                if ($vs[@id]) then 
                    if ($vs[publishingAuthority]) then () else utillib:inheritPublishingAuthorityFromProject($vs/ancestor::decor)
                else ()
                ,
                $vs/(* except (desc | sourceCodeSystem | conceptList | completeCodeSystem))
                ,
                (: we return just a count of issues and leave it up to the calling party to get those if required :)
                <issueAssociation count="{count($setlib:colDecorData//issue/object[@id = $vs/@id][@effectiveDate = $vs/@effectiveDate] | $setlib:colDecorData//issue/object[@id = $vs/@ref])}"/>
                ,
                <valueSetExpansionAssociation count="{
                    count(
                        if (empty($ed)) then 
                            collection($setlib:strValuesetExpansionData)/valueSetExpansionSet[valueSet[@id = $id]]
                        else (
                            collection($setlib:strValuesetExpansionData)/valueSetExpansionSet[valueSet[@id = $id][@effectiveDate = $ed]]
                        )
                    )
                }"/>
                ,
                let $ccs    := for $c in $vs/completeCodeSystem return <include>{$c/@*, $c/*}</include>
                return
                    vsapi:copyConceptList($ccs | $vs/conceptList/concept | $vs/conceptList/include | $vs/conceptList/exclude | $vs/conceptList/exception)
            )
            else (
                (:$vs/classification:)
            )
        }
};

(:~ Return zero or more value sets as-is

@param $ref          - required. Identifier or name of the value set to retrieve
@param $flexibility  - optional. null gets all versions, yyyy-mm-ddThh:mm:ss gets this specific version, anything that doesn't cast to xs:dateTime gets latest version
@return Matching value sets
@since 2023-09-10
:)
declare function vsapi:getValueSetByRef($ref as xs:string, $flexibility as xs:string?) as element(valueSet)* {
    if (utillib:isOid($ref)) then
        vsapi:getValueSetById($ref, $flexibility)
    else (
        vsapi:getValueSetByName($ref, $flexibility)
    )
};

(:~ Return zero or more value sets as-is

@param $ref           - required. Identifier or nameof the value set to retrieve
@param $flexibility   - optional. null gets all versions, yyyy-mm-ddThh:mm:ss gets this specific version, anything that doesn't cast to xs:dateTime gets latest version
@param $decorOrPrefix - required. determines search scope. pfx- limits scope to this project only
@param $decorRelease  - optional. if empty defaults to current version. if valued then the value set will come explicitly from that archived project version which is expected to be a compiled version
@param $decorLanguage - optional. Language compilation. Defaults to project/@defaultLanguage.
@return Matching value sets
@since 2023-09-10
:)
declare function vsapi:getValueSetByRef($ref as xs:string, $flexibility as xs:string?, $decorOrPrefix as item(), $decorRelease as xs:string?, $decorLanguage as xs:string?) as element(valueSet)* {
    if (utillib:isOid($ref)) then
        vsapi:getValueSetById($ref, $flexibility, $decorOrPrefix, $decorRelease, $decorLanguage)
    else (
        vsapi:getValueSetByName($ref, $flexibility, $decorOrPrefix, $decorRelease, $decorLanguage)
    )
};

(:~ Return zero or more value sets as-is

@param $id           - required. Identifier of the value set to retrieve
@param $flexibility  - optional. null gets all versions, yyyy-mm-ddThh:mm:ss gets this specific version, anything that doesn't cast to xs:dateTime gets latest version
@return Matching value sets
@since 2023-09-10
:)
declare function vsapi:getValueSetById($id as xs:string, $flexibility as xs:string?) as element(valueSet)* {
    if ($flexibility castable as xs:dateTime) then
        $setlib:colDecorData//valueSet[@id = $id][@effectiveDate = $flexibility] | $setlib:colDecorCache//valueSet[@id = $id][@effectiveDate = $flexibility]
    else
    if ($flexibility) then (
        let $valuesets  := $setlib:colDecorData//valueSet[@id = $id] | $setlib:colDecorCache//valueSet[@id = $id]
        return
            $valuesets[@effectiveDate = string(max($valuesets/xs:dateTime(@effectiveDate)))]
    )
    else (
        $setlib:colDecorData//valueSet[@id = $id] | $setlib:colDecorCache//valueSet[@id = $id]
    )
};

(:~ Return zero or more value sets as-is

@param $id            - required. Identifier of the value set to retrieve
@param $flexibility   - optional. null gets all versions, yyyy-mm-ddThh:mm:ss gets this specific version, anything that doesn't cast to xs:dateTime gets latest version
@param $decorOrPrefix - required. determines search scope. pfx- limits scope to this project only
@param $decorRelease  - optional. if empty defaults to current version. if valued then the value set will come explicitly from that archived project version which is expected to be a compiled version
@param $decorLanguage - optional. Language compilation. Defaults to project/@defaultLanguage.
@return Matching value sets
@since 2023-09-10
:)
declare function vsapi:getValueSetById($id as xs:string, $flexibility as xs:string?, $decorOrPrefix as item(), $decorRelease as xs:string?, $decorLanguage as xs:string?) as element(valueSet)* {
let $decor          := utillib:getDecor($decorOrPrefix, $decorRelease, $decorLanguage)
let $decors         := if (empty($decorRelease)) then utillib:getBuildingBlockRepositories($decor, (), $utillib:strDecorServicesURL) else $decor

return
    if ($flexibility castable as xs:dateTime) then
        $decors//terminology/valueSet[@id = $id][@effectiveDate = $flexibility]
    else
    if ($flexibility) then (
        let $valuesets  := $decors//terminology/valueSet[@id = $id]
        return
            $valuesets[@effectiveDate = string(max($valuesets/xs:dateTime(@effectiveDate)))]
    )
    else (
        $decors//terminology/valueSet[@id = $id]
    )
};

(:~ Return zero or more value sets as-is

@param $name         - required. Identifier of the value set to retrieve
@param $flexibility  - optional. null gets all versions, yyyy-mm-ddThh:mm:ss gets this specific version, anything that doesn't cast to xs:dateTime gets latest version
@return Matching value sets
@since 2023-09-10
:)
declare function vsapi:getValueSetByName($name as xs:string, $flexibility as xs:string?) as element(valueSet)* {
    if ($flexibility castable as xs:dateTime) then
        $setlib:colDecorData//valueSet[@name = $name][@effectiveDate = $flexibility] | $setlib:colDecorCache//valueSet[@name = $name][@effectiveDate = $flexibility]
    else
    if ($flexibility) then (
        let $valuesets := $setlib:colDecorData//valueSet[@name = $name] | $setlib:colDecorCache//valueSet[@name = $name]
        return
            $valuesets[@effectiveDate = string(max($valuesets/xs:dateTime(@effectiveDate)))]
    )
    else (
        $setlib:colDecorData//valueSet[@name = $name] | $setlib:colDecorCache//valueSet[@name = $name]
    )
};

(:~ Return zero or more value sets as-is

@param $name          - required. Identifier of the value set to retrieve
@param $flexibility   - optional. null gets all versions, yyyy-mm-ddThh:mm:ss gets this specific version, anything that doesn't cast to xs:dateTime gets latest version
@param $decorOrPrefix - required. determines search scope. pfx- limits scope to this project only
@param $decorRelease  - optional. if empty defaults to current version. if valued then the value set will come explicitly from that archived project version which is expected to be a compiled version
@param $decorLanguage - optional. Language compilation. Defaults to project/@defaultLanguage.
@return Matching value sets
@since 2023-09-10
:)
declare function vsapi:getValueSetByName($name as xs:string, $flexibility as xs:string?, $decorOrPrefix as item(), $decorRelease as xs:string?, $decorLanguage as xs:string?) as element(valueSet)* {
let $decor          := utillib:getDecor($decorOrPrefix, $decorRelease, $decorLanguage)
let $decors         := if (empty($decorRelease)) then utillib:getBuildingBlockRepositories($decor, (), $utillib:strDecorServicesURL) else $decor

return
    if ($flexibility castable as xs:dateTime) then
        $decors//valueSet[@name = $name][@effectiveDate = $flexibility]
    else
    if ($flexibility) then (
        let $valuesets := $decors//valueSet[@name = $name]
        return
            $valuesets[@effectiveDate = string(max($valuesets/xs:dateTime(@effectiveDate)))]
    )
    else (
        $decors//valueSet[@name = $name]
    )
};

(:~ Get contents of a valueSet and return like this:
 :   <valueSet id|ref="oid" ...>
 :       <completeCodeSystem .../>         -- if applicable
 :       <conceptList>
 :           <concept .../>
 :           <include ...>                 -- handled by vsapi:getValueSetInclude() 
 :               <valueSet ...>
 :                   ...
 :               </valueSet>
 :           </include>
 :           <exception .../>
 :       </conceptList>
 :   </valueSet>
 :)
declare function vsapi:getRawValueSet($valueSet as element(valueSet), $includetrail as element()*, $language as xs:string?, $getincludes as xs:boolean) as element(valueSet) {
<valueSet>
{
    $valueSet/@*,
    if ($valueSet[@url])   then () else attribute url {($valueSet/ancestor::cacheme/@bbrurl, $utillib:strDecorServicesURL)[1]},
    if ($valueSet[@ident]) then () else attribute ident {($valueSet/ancestor::decor/project/@prefix)[1]}
}
{
    $valueSet/desc
}
{
    $valueSet/publishingAuthority
    ,
    if ($valueSet[@id]) then 
        if ($valueSet[publishingAuthority]) then () else (
            let $decor      := $valueSet/ancestor::decor
            let $copyright  := ($decor/project/copyright[empty(@type)] | $decor/project/copyright[@type = 'author'])[1]
            return
            if ($copyright) then
                <publishingAuthority name="{$copyright/@by}" inherited="project">
                {
                    (: Currently there is no @id, but if it ever would be there ...:)
                    $copyright/@id,
                    $copyright/addrLine
                }
                </publishingAuthority>
            else ()
        )
    else ()
    ,
    $valueSet/endorsingAuthority
    ,
    $valueSet/purpose
    ,
    if ($valueSet[@id]) then
        vsapi:handleValueSetCopyrights($valueSet/copyright, distinct-values($valueSet//@codeSystem))
    else (
        $valueSet/copyright
    )
}
{
    for $revisionHistory in $valueSet/revisionHistory
    return
        <revisionHistory>
        {
            $revisionHistory/@*
            ,
            $revisionHistory/desc
        }
        </revisionHistory>
}
{
    if ($valueSet/completeCodeSystem | $valueSet/conceptList[*]) then (
        <conceptList>
        {
            for $codeSystem in $valueSet/completeCodeSystem
            return 
                <include>
                {
                    $codeSystem/@*, 
                    $codeSystem/filter
                }
                </include>
            ,
            for $node in $valueSet/conceptList/concept | 
                         $valueSet/conceptList/include | 
                         $valueSet/conceptList/exclude | 
                         $valueSet/conceptList/exception
            return (
                if ($node[self::include[@ref]]) then (
                    if ($getincludes) then vsapi:getValueSetInclude($node, $includetrail, $language) else $node
                ) else 
                if ($node[self::include[@op]] | $node[self::exclude[@op]]) then (
                    $node
                ) 
                else (
                    element {$node/name()} 
                    {
                        $node/@*
                        ,
                        $node/designation,
                        $node/desc,
                        $node/filter
                    }
                )
            )
        }
        </conceptList>
    ) else ()
}
</valueSet>
};

(:~ Extract a valueSet with @id by recursively resolving all includes. Use vsapi:getValueSetExtract to resolve a valueSet/@ref first. 
<valueSet all-attributes of the input>
  <desc all />
  <sourceCodeSystem id="any-concept-or-exception-codesystem-in-valueSet" identifierName="codeSystemName" canonicalUri="xs:anyURI" canonicalUriDSTU2="xs:anyURI" canonicalUriSTU3="xs:anyURI" canonicalUriR4="xs:anyURI" />
  <completeCodeSystem all-attributes of the input>
  <conceptList>
    <concept all-attributes-and-desc-elements />
    <exception all-attributes-and-desc-elements exceptions-contain-no-duplicates />
  </conceptList>
</valueSet>
                                
@param $valueSet     - required. The valueSet element with content to expand
@param $prefix       - required. The origin of valueSet. pfx- limits scope this project only
@param $version      - optional. if empty defaults to current version. if valued then the valueset will come explicitly from that archived project version which is expected to be a compiled version
@return The expanded valueSet
@since 2024-05-02
:)
declare function vsapi:getValueSetExtracted($valueSet as element(), $projectPrefix as xs:string, $projectVersion as xs:string?, $projectLanguage as xs:string?) as element(valueSet) {
    let $decor                  := utillib:getDecorByPrefix($projectPrefix, $projectVersion, $projectLanguage)[1]
    
    let $language               := 
        if (empty($projectLanguage)) then ($decor/@language, $decor/project/@defaultLanguage)[1] else $projectLanguage

    let $language               :=
        if (empty($language)) then ($valueSet/../@defaultLanguage, serverapi:getServerLanguage()) else ()

    let $rawValueSet            := 
        vsapi:getRawValueSet($valueSet, <include ref="{$valueSet/(@id|@ref)}" flexibility="{$valueSet/@effectiveDate}"/>, $language, true())
    let $orgPrefix              := if ($valueSet/../@ident) then $valueSet/../@ident else $projectPrefix

    (: build map of oid names. is more costly when you do that deep down :)
    let $oidnamemap             :=
        map:merge(
            for $oid in distinct-values($rawValueSet//@codeSystem)
            let $oidName    := utillib:getNameForOID($oid, $language, $orgPrefix)
            return
                if (string-length($oidName) = 0) then () else map:entry($oid, $oidName)
        )

    let $codesystemmap   :=
        map:merge(
            for $sourceCodeSystem in $rawValueSet/completeCodeSystem[empty(filter)] | $rawValueSet/conceptList/include[@codeSystem][empty(@op | @code | @ref | filter)]
            let $csid               := $sourceCodeSystem/@codeSystem
            let $csed               := if ($sourceCodeSystem[@flexibility castable as xs:dateTime]) then $sourceCodeSystem/@flexibility else 'dynamic'
            let $csided             := $csid || $csed
            group by $csided
            return (
                let $codeSystemContents := (csapi:getCodeSystemById($csid[1], $csed[1], $orgPrefix, $projectVersion)//codeSystem[@id])[1]
                return
                    if ($codeSystemContents) then map:entry($csided, $codeSystemContents) else ()
            )
        )
        
     let $completeCodeSystems    := $rawValueSet//completeCodeSystem | $rawValueSet/conceptList/include[@codeSystem][empty(@op | @code | @ref | filter | @exception[. = 'true'])]
     
     return
        <valueSet>
        {
            $rawValueSet/(@* except (@url|@ident|@referencedFrom))
            ,
            $rawValueSet/desc
        }
        {
            (: when a value set is pulled from a compiled project version, the sourceCodeSystem should already be present :)
            if ($rawValueSet/sourceCodeSystem) then $rawValueSet/sourceCodeSystem else (
                vsapi:createSourceCodeSystems($rawValueSet, ($rawValueSet/ancestor::decor, $projectPrefix)[1], ($language, $rawValueSet/ancestor::decor/project/@defaultLanguage)[1])
            )
        }
        {
            $rawValueSet/publishingAuthority
            ,
            $rawValueSet/endorsingAuthority
            ,
            $rawValueSet/revisionHistory
            ,
            $rawValueSet/purpose
            ,
            $rawValueSet/copyright
        }
        {
            if ($rawValueSet/conceptList[include | exclude | duplicate] or count(map:keys($codesystemmap)) gt 0) then (
                <conceptList>
                {
                    (:get defined and included codeSystems:)
                    for $sourceCodeSystem in $completeCodeSystems
                        let $csid               := $sourceCodeSystem/@codeSystem
                        let $csed               := if ($sourceCodeSystem[@flexibility castable as xs:dateTime]) then $sourceCodeSystem/@flexibility else 'dynamic'
                        let $csided             := $csid || $csed
                        let $codeSystemName     := map:get($codesystemmap, $csid)
                        return
                            if (map:contains($codesystemmap, $csided)) then (
                                let $conceptList        := map:get($codesystemmap, $csided)//conceptList
                                let $checkParentChild   := exists($conceptList/codedConcept[parent | child])
                                let $topLevelConcepts   := if ($checkParentChild) then $conceptList/codedConcept[empty(parent)] else $conceptList/codedConcept[@level = '0']
                                
                                return
                                    csapi:getCodedConcepts($topLevelConcepts, $csid, 0, $conceptList, $topLevelConcepts/@code, $checkParentChild, $language)
                            ) else (
                                <include codeSystem="{$csid}">
                                {
                                    if ($codeSystemName = '') then () else attribute codeSystemName {$codeSystemName}, 
                                    $sourceCodeSystem/@codeSystemVersion, 
                                    $sourceCodeSystem/@flexibility,
                                    (: 2020-12 Added for deprecation of a codeSystem in the context of a valueSet :)
                                    $sourceCodeSystem/@type[. = 'D'],
                                    for $f in $sourceCodeSystem/filter
                                    return
                                        <filter>{$f/@property, $f/@op, $f/@value}</filter>
                                }
                                </include>
                            )
                }
                {
                    for $source in $rawValueSet/conceptList/include[@ref][valueSet]
                    return
                        comment { ' include ref=' || $source/@ref || ' flexibility=' || ($source/@flexibility[not(. = '')], 'dynamic')[1] || ' displayName=' || replace(($source/valueSet/@displayName)[1], '--', '-\\-') || ' effectiveDate=' || ($source/valueSet/@effectiveDate)[1] || ' '}
                }
                {
                    for $source in $rawValueSet//conceptList/concept[not(ancestor::include[@exception = 'true'])] | 
                                   $rawValueSet//conceptList/include[not(.[@ref][valueSet] | .[@exception = 'true'])] except $completeCodeSystems |
                                   $rawValueSet//conceptList/exclude
                    let $csid               := $source/@codeSystem
                    let $csed               := if ($source[@flexibility castable as xs:dateTime]) then $source/@flexibility else 'dynamic'
                    let $csided             := concat($csid, $csed)
                    return
                        if ($source[self::concept | self::exclude]) then $source else
                        if (map:contains($codesystemmap, $csided)) then () else (
                            $source
                        )
                }
                {
                    for $node in $rawValueSet//conceptList/exception |
                                 $rawValueSet//conceptList/concept[ancestor::include[@exception='true']]
                    let $code       := $node/@code
                    let $codeSystem := $node/@codeSystem
                    group by $code, $codeSystem
                    return
                        (:<exception>{$exceptions/@*, $exceptions/*}</exception>:)
                        <exception>{$node[1]/@*, $node[1]/*}</exception>
                }
                {
                    $rawValueSet//conceptList/duplicate
                }
                </conceptList>
                )
                else (
                    $rawValueSet/conceptList
            )
        }    
        </valueSet>
};

(:~ Return zero or more extracted valuesets wrapped in a &lt;return/&gt; element, and subsequently inside a &lt;repository&gt; element. This repository element
:   holds at least the attribute @ident with the originating project prefix and optionally the attribute @url with the repository URL in case of an external
:   repository. Id based references can match both valueSet/@id and valueSet/@ref. The latter is resolved. Note that duplicate valueSet matches may be 
:   returned. Example output:
:   &lt;return>
:       &lt;repository url="http://art-decor.org/decor/services/" ident="ad2bbr-">
:           &lt;valueSet id="2.16.840.1.113883.1.11.10282" name="ParticipationSignature" displayName="ParticipationSignature" effectiveDate="2013-03-11T00:00:00" statusCode="final" versionLabel="DEFN=UV=VO=1206-20130318">
:               ...
:           &lt;/valueSet>
:       &lt;/repository>
:   &lt;/return>
:   <ul><li> Valuesets get an extra attribute @fromRepository containing the originating project prefix/ident</li> 
:   <li>valueSet/@ref is treated as if it were the referenced valueset</li>
:   <li>If parameter prefix is not used then only valueSet/@id is matched. If that does not give results, then 
:     valueSet/@ref is matched, potentially expanding into a buildingBlockRepository</li>
:   <li>Any (nested) include @refs in the valueset are resolved, if applicable using the buildingBlockRepository 
:     configuration in the project that the valueSet/@id was found in</li>
:                                
:   @param $id           - required. Identifier of the valueset to retrieve
:   @param $flexibility  - optional. null gets all versions, 'dynamic' gets the newest version based on id, yyyy-mm-ddThh:mm:ss gets this specific version
:   @return Zero value sets in case no matches are found, one if only one exists or if a specific version was requested, or more if more versions exist and no specific version was requested
:   @since 2024-02-06
:)
declare %private function vsapi:getValueSetExtract($idOrName as xs:string, $effectiveDate as xs:string?) as element(valueSet)* {
    let $idOrName               := $idOrName[not(. = '')]
    let $effectiveDate          := ($effectiveDate[not(. = '')], 'dynamic')[1]
    
    let $valueSets              := vsapi:getValueSetByRef($idOrName, $effectiveDate)
    
    return
        <return>
        {
            for $vs in $valueSets
            let $prefix    := $vs/ancestor::decor/project/@prefix
            let $urlident  := $vs/ancestor::cacheme/@bbrurl || $prefix
            group by $urlident
            return
                <project ident="{$prefix}">
                {
                    if ($vs[ancestor::cacheme]) then
                        attribute url {$vs[1]/ancestor::cacheme/@bbrurl}
                    else (),
                    $vs[1]/ancestor::decor/project/@defaultLanguage,
                    for $valueSet in $vs[@id]
                    return
                        vsapi:getValueSetExtracted($valueSet, $prefix, (), ())
                }
                </project>
        }
        </return>

};

(:~ Return zero or more expanded valuesets wrapped in a &lt;return/&gt; element, and subsequently inside a &lt;repository&gt; element. This repository element
:   holds at least the attribute @ident with the originating project prefix and optionally the attribute @url with the repository URL in case of an external
:   repository. Id based references can match both valueSet/@id and valueSet/@ref. The latter is resolved. Note that duplicate valueSet matches may be 
:   returned. Example output:
:   &lt;return>
:       &lt;repository url="http://art-decor.org/decor/services/" ident="ad2bbr-">
:           &lt;valueSet id="2.16.840.1.113883.1.11.10282" name="ParticipationSignature" displayName="ParticipationSignature" effectiveDate="2013-03-11T00:00:00" statusCode="final" versionLabel="DEFN=UV=VO=1206-20130318">
:               ...
:           &lt;/valueSet>
:       &lt;/repository>
:   &lt;/return>
:   <ul><li> Valuesets get an extra attribute @fromRepository containing the originating project prefix/ident</li> 
:   <li>valueSet/@ref is treated as if it were the referenced valueset</li>
:   <li>If parameter prefix is not used then only valueSet/@id is matched. If that does not give results, then 
:     valueSet/@ref is matched, potentially expanding into a buildingBlockRepository</li>
:   <li>Any (nested) include @refs in the valueset are resolved, if applicable using the buildingBlockRepository 
:     configuration in the project that the valueSet/@id was found in</li>
:                                
:   @param $id           - required. Identifier of the valueset to retrieve
:   @param $flexibility  - optional. null gets all versions, 'dynamic' gets the newest version based on id, yyyy-mm-ddThh:mm:ss gets this specific version
:   @param $prefix       - required. determines search scope. pfx- limits scope to this project only
:   @param $version      - optional. if empty defaults to current version. if valued then the valueset will come explicitly from that archived project version which is expected to be a compiled version
:   @return Zero value sets in case no matches are found, one if only one exists or if a specific version was requested, or more if more versions exist and no specific version was requested
:   @since 2024-02-06
:)
declare %private function vsapi:getValueSetExtract($projectPrefix as xs:string*, $projectVersion as xs:string*, $projectLanguage as xs:string?, $idOrName as xs:string, $effectiveDate as xs:string?, $withversions as xs:boolean?) as element(valueSet)* {
    let $idOrName               := $idOrName[not(. = '')]
    let $flexibility            := if ($withversions) then () else ($effectiveDate[not(. = '')], 'dynamic')[1]
    let $projectPrefix          := $projectPrefix[not(. = '')][1]
    let $projectVersion         := $projectVersion[not(. = '')][1]
    let $projectLanguage        := $projectLanguage[not(. = '')][1]

    (: retrieve all valuesets for the input $decor project(s) 
        - when compiled version  - as is - 
        - when live version - getting all (cached) decor projects in scope  
    :)
    let $decor                  := utillib:getDecor($projectPrefix, $projectVersion, $projectLanguage)
    let $decors                 := if (empty($projectVersion)) then utillib:getBuildingBlockRepositories($decor, (), $utillib:strDecorServicesURL) else $decor[1]
    
    let $valueSets              := 
        if (utillib:isOid($idOrName)) then (
        
            if ($flexibility castable as xs:dateTime) then
                $decors//terminology/valueSet[@id = $idOrName][@effectiveDate = $flexibility]
            else
            if ($flexibility) then (
                let $valuesets  := $decors//terminology/valueSet[@id = $idOrName]
                return
                    $valuesets[@effectiveDate = string(max($valuesets/xs:dateTime(@effectiveDate)))]
            )
            else (
                $decors//terminology/valueSet[@id = $idOrName]
            )
        )
        else (
        
            if ($flexibility castable as xs:dateTime) then
                $decors//valueSet[@name = $idOrName][@effectiveDate = $flexibility]
            else
            if ($flexibility) then (
                let $valuesets := $decors//valueSet[@name = $idOrName]
                return
                    $valuesets[@effectiveDate = string(max($valuesets/xs:dateTime(@effectiveDate)))]
            )
            else (
                $decors//valueSet[@name = $idOrName]
            )
       )    
    
    let $valueSets              :=  $valueSets | $decor//terminology/valueSet[@ref = $idOrName]
    
    return
    <return>
    
    {
        (:from the requested project, return valueSet/(@id and @ref):)
        (: when retrieving value sets from a compiled project, the @url/@ident they came from are on the valueSet element
           reinstate that info so downstream logic works as if it really came from the repository again.
        :)
        for $repository in $decor
        for $vs in $valueSets
        let $url                := if (empty($vs/@url)) then ($vs/ancestor::cacheme/@bbrurl, $utillib:strDecorServicesURL)[1] else $vs/@url
        let $ident              := if (empty($vs/@ident)) then $vs/ancestor::decor/project/@prefix else $vs/@ident
        let $repo               := if ($ident=$projectPrefix) then 'project' else 'repository'
        let $refFrom            := if (empty($vs/@referencedFrom)) then ($decors//project[buildingBlockRepository/@ident = $ident][buildingBlockRepository/@url = $url]/@prefix)[1] else $vs/@referencedFrom
        let $projectType        := if (exists($vs/ancestor::cacheme)) then 'cached' else 'local'
        let $source             := $url || $ident
        group by $source
        order by $repo[1]
        return
        
            element {$repo[1]} {
                
                if ($ident=$projectPrefix) then 
                (
                    attribute ident{$ident[1]}
                ) else
                ( 
                    attribute url {$url[1]},
                    attribute ident{$ident[1]},
                    attribute referencedFrom {$refFrom[1]},
                    attribute projecttype {$projectType[1]}
                ),
                for $valueSet in $vs
                    order by $valueSet/@effectiveDate descending
                    return
                        vsapi:getValueSetExtracted($valueSet, $projectPrefix, $projectVersion, $projectLanguage)
                }
    }
 
    </return>

};    

declare %private function vsapi:getValueSetInclude($include as element(), $includetrail as element()*, $language as xs:string?) as element()* {
let $valuesetId               := $include/@ref
let $valuesetFlex             := $include/@flexibility
let $decor                    := $include/ancestor::decor
let $valueSet                 := vsapi:getValueSetById($valuesetId, $valuesetFlex, $decor, (), ())

return
    if ($includetrail[@ref = $include/@ref][@flexibility = $valueSet/@effectiveDate]) then (
        <duplicate>{$include/@*}</duplicate>
    )
    else (
        let $includetrail := $includetrail | <include ref="{$include/@ref}" flexibility="{$valueSet/@effectiveDate}"/>
        return
        <include>
        {
            $include/@ref,
            $include/@flexibility,
            $include/@exception
        }
        {
            if (exists($valueSet)) then 
                vsapi:getRawValueSet($valueSet[1], $includetrail, $language, true())
            else ()
        }
        </include>
    )
};

(: Returns appropriate copyright texts based on $usedCodeSystems
   <copyright language="en-US">Licensing note: .......</copyright>
:)
declare %private function vsapi:handleValueSetCopyrights($copyrightsource as element(copyright)*, $usedCodeSystems as item()*) as element(copyright)* {

    let $extracopyrights :=
        <extracopyright oid="2.16.840.1.113883.6.96" urn="http://snomed.info/sct">This artefact includes content from SNOMED Clinical Terms® (SNOMED CT®) which is copyright of the International Health Terminology Standards Development Organisation (IHTSDO). Implementers of these artefacts must have the appropriate SNOMED CT Affiliate license - for more information contact http://www.snomed.org/snomed-ct/getsnomed-ct or info@snomed.org.</extracopyright>
        
    let $extra :=
        for $t in $extracopyrights
        return
            (: if codesystem is used :) 
            if ($t/@oid = $usedCodeSystems)
            then (
                (: if text is not yet in the copyright texts :)
                if ($copyrightsource[@language = 'en-US'][empty(@inherited)]) then 
                    <copyright language="en-US">{$copyrightsource[@language='en-US']/(node() except *:div[@data-source='inherited'])}<div data-source="inherited" style="border-top: 1px solid black">{$t/text()}</div></copyright>
                else (
                    <copyright language="en-US" inherited="true">{$t/text()}</copyright>
                )
            )
            else ()
    
    return
        if (empty($extra)) then $copyrightsource else ($copyrightsource[not(@language='en-US')], $extra)
    
};

declare %private function vsapi:copyConceptList($nodes as element()*) {
    for $node in $nodes
    let $elname := name($node)
    return
        <items>
        {
             attribute { 'is' } { $elname },
             $node/(@* except @is),
             $node/node()
        }
        </items>
};

(: Central logic for creating an empty dataset

@param $authmap         - required. Map derived from token
@return (empty) dataset object as xml with json:array set on elements
:)
declare function vsapi:createValueSet($authmap as map(*), $projectPrefix as xs:string, $targetDate as xs:boolean, $sourceId as xs:string?, $sourceEffectiveDate as xs:string?, $refOnly as xs:boolean?, $keepIds as xs:boolean?, $baseId as xs:string?, $editedValueset as element(valueSet)?) {

    let $decor                      := utillib:getDecorByPrefix($projectPrefix)
    let $projectLanguage            := $decor/project/@defaultLanguage
    
    let $check                      :=
        if (empty($editedValueset/@id)) then () else (
            if (matches($editedValueset/@id, '^[0-2](\.(0|[1-9][0-9]*)){0,3}$')) then
                error($errors:BAD_REQUEST, 'Value set id ''' || $editedValueset/@id || ''' is reserved. You cannot reuse a reserved identifier. See http://oid-info.com/get/' || $editedValueset/@id)
            else
            if (utillib:isOid($editedValueset/@id)) then () else (
                error($errors:BAD_REQUEST, 'Value set id ''' || $editedValueset/@id || ''' SHALL be a valid OID')
            )
        )
    
    (: if the user sent us a valueSet with id, we should assume he intends to keep that id :)
    let $keepIds                    := if ($editedValueset/@id) then true() else $keepIds = true()
    (: if the user sent us a valueSet with effectiveDate, we should assume he intends to keep that effectiveDate :)
    let $now                        := 
        if ($editedValueset/@effectiveDate) then substring($editedValueset/@effectiveDate, 1, 19) else 
        if ($targetDate) then substring(string(current-date()), 1, 10) || 'T00:00:00' else substring(string(current-dateTime()), 1, 19)
    
    let $check                      :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, 'Project with prefix ' || $projectPrefix || ' does not exist')
        )
    let $check                      :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-TERMINOLOGY)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify terminology in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    
    let $sourceValueset             := if (empty($sourceId)) then () else vsapi:getValueSet($projectPrefix, (), (), $sourceId, $sourceEffectiveDate, false(), true())
    let $storedValueset             := 
        if ($keepIds and $editedValueset[@id]) then vsapi:getValueSet($projectPrefix, (), (), $editedValueset/@id, $now, false(), true()) else ()
    
    let $check                      :=
        if ($refOnly) then
            if (empty($sourceId)) then
                error($errors:BAD_REQUEST, 'Parameter sourceId SHALL be provided if a value set reference is to be created')
            else
            if (empty($sourceValueset)) then
                if (empty($sourceEffectiveDate)) then
                    error($errors:BAD_REQUEST, 'Parameter sourceId ' || $sourceId || ' SHALL lead to a value set in scope of this project')
                else (
                    error($errors:BAD_REQUEST, 'Parameters sourceId ' || $sourceId || ' and sourceEffectiveDate ' || $sourceEffectiveDate || ' SHALL lead to a value set in scope of this project')
                )
            else ()
        else
        if (empty($editedValueset) and empty($sourceId)) then 
            error($errors:BAD_REQUEST, 'ValueSet input data or a sourceId SHALL be provided')
        else
        if ($editedValueset) then
            if ($storedValueset) then
                error($errors:BAD_REQUEST, 'Cannot create new valueSet. The input valueSet with id ' || $editedValueset/@id || ' and effectiveDate ' || $now || ' already exists.')
            else ()
        else (
            if (empty($sourceValueset)) then 
                error($errors:BAD_REQUEST, 'Cannot create new valueSet. Source valueset based on id ' || $sourceId || ' and effectiveDate ' || $sourceEffectiveDate || ' does not exist.')
            else  ()
        )
    let $check                      :=
        if ($now castable as xs:dateTime) then () else (
            error($errors:BAD_REQUEST, 'Cannot create new valueSet. The provided effectiveDate ' || $now || ' is not a valid xs:dateTime. Expected yyyy-mm-ddThh:mm:ss.')
        )
    
    let $editedValueset             := ($editedValueset, $sourceValueset)[1]
    (: decorlib:getNextAvailableIdP() returns <next base="1.2" max="2" next="{$max + 1}" id="1.2.3" type="DS"/> :)
    let $newValuesetId              := if ($keepIds) then $editedValueset/@id else (decorlib:getNextAvailableIdP($decor, $decorlib:OBJECTTYPE-VALUESET, $baseId)/@id)
    
    let $check                      :=
        if (vsapi:getValueSet($projectPrefix, (), (), $newValuesetId, $now, false(), false())[@id]) then
            error($errors:BAD_REQUEST, 'Cannot create new valueSet. The to-be-created valueSet with id ' || $newValuesetId || ' and effectiveDate ' || $now || ' already exists.')
        else ()
    
    let $baseValueset               := 
        <valueSet>
        {
            attribute id {$newValuesetId} ,
            $editedValueset/@name[string-length()>0] ,
            $editedValueset/@displayName[string-length()>0] ,
            attribute effectiveDate {$now} ,
            attribute statusCode {"draft"} ,
            $editedValueset/@versionLabel[string-length()>0] ,
            $editedValueset/@expirationDate[string-length()>0] ,
            $editedValueset/@officialReleaseDate[string-length()>0] ,
            $editedValueset/@experimental[string-length()>0] ,
            $editedValueset/@canonicalUri[string-length()>0] ,
            $editedValueset/@lastModifiedDate[string-length()>0]
        }
        </valueSet>
    let $newValueSet                    := 
        if ($refOnly) then
            <valueSet ref="{$sourceValueset/@id}" name="{$sourceValueset/@name}" displayName="{($sourceValueset/@displayName, $sourceValueset/@name)[1]}"/>
        else (
            utillib:prepareValueSetForUpdate($editedValueset, $baseValueset)
        )
    
    let $valueSetAssociation            := 
        if ($editedValueset[string-length(@conceptId)>0]) then 
            <terminologyAssociation>
            {
                $editedValueset/@conceptId,
                attribute valueSet {$newValueSet/@id},
                if ($editedValueset[string-length(@flexibility)=0]) then () else (
                    attribute flexibility {$newValueSet/@effectiveDate}
                ),
                attribute effectiveDate {$now}
            }
            </terminologyAssociation>
        else ()
    let $terminologyAssociations        :=
        for $concept in $editedValueset//*[string-length(@conceptId)>0][string-length(@code)>0][string-length(@codeSystem)>0]
        return
            <terminologyAssociation>
            {
                $concept/@conceptId, $concept/@code, $concept/@codeSystem, $concept/@displayName, 
                attribute effectiveDate {$now}
            }
            </terminologyAssociation>
    
    (: prepare sub root elements if not existent :)
    let $valueSetUpdate                 :=
        if (not($decor/terminology) and $decor/codedConcepts) then
            update insert <terminology/> following $decor/codedConcepts
        else if (not($decor/terminology) and not($decor/codedConcepts)) then
            update insert <terminology/> following $decor/ids
        else ()
        (: now update the value set :)
    let $valueSetUpdate                 :=
        if ($refOnly and $decor//valueSet[@ref = $newValueSet/@ref]) then
            (: this could update the name/displayName if the source thing was updated since adding it previously :)
            update replace $decor//valueSet[@ref = $newValueSet/@ref] with $newValueSet
        else
        if ($decor/terminology/conceptMap) then
            update insert $newValueSet preceding $decor/terminology/conceptMap[1] 
        else (
            update insert $newValueSet into $decor/terminology
        )
    
    let $terminologyAssociationUpdates  :=
        utillib:addTerminologyAssociations($valueSetAssociation | $terminologyAssociations, $decor, false(), false())
   
    (: return the regular valueSet that was created, or the requested version of the reference that was created, or the latest version of the reference that was created :)
    let $result                         := vsapi:getValueSet($projectPrefix, (), (), ($newValueSet/@id | $newValueSet/@ref), ($newValueSet/@effectiveDate, $sourceEffectiveDate, 'dynamic')[1], false(), true())
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

(:<sourceCodeSystem id="any-concept-or-exception-codesystem-in-valueSet" identifierName="codeSystemName" canonicalUri="xs:anyURI" canonicalUriDSTU2="xs:anyURI" canonicalUriSTU3="xs:anyURI" canonicalUriR4="xs:anyURI" />:)
declare %private function vsapi:createSourceCodeSystems($valueSet as element(valueSet), $decorOrPrefix as item(), $language as xs:string?) as element(sourceCodeSystem)* {
    for $sourceCodeSystem in $valueSet//@codeSystem
    let $csid := $sourceCodeSystem
    let $csed := $sourceCodeSystem/../@codeSystemVersion
    group by $csid, $csed
    order by $csid[1], $csed[1]
    return
        <sourceCodeSystem id="{$csid}" identifierName="{replace(normalize-space(utillib:getNameForOID($csid, $language, $decorOrPrefix)),'\s','&#160;')}" 
                                                   canonicalUri="{utillib:getCanonicalUriForOID('CodeSystem', $csid, $csed, $decorOrPrefix, ())}">
        {
            for $fhirVersion in $setlib:arrKeyFHIRVersions
            return
                attribute {concat('canonicalUri', $fhirVersion)} {utillib:getCanonicalUriForOID('CodeSystem', $csid, $csed, $decorOrPrefix, $fhirVersion)}
            ,
            (: ART-DECOR 3: get for example external/hl7 :)
            let $coll   := collection($setlib:strCodesystemStableData)//identifier[@id = 'urn:oid:' || $csid]
            
            return
                if (empty($coll)) then () else attribute context {substring-after(util:collection-name($coll[1]), $setlib:strCodesystemStableData || '/')}
        }
        </sourceCodeSystem>
};

(:~ Returns a list of zero or more valuesets as listed in the terminology section. This function is useful e.g. to call from a ValueSetIndex. Parameter id, name or prefix is required.

@param $projectPrefix    - optional. determines search scope. null is full server, pfx- limits scope to this project only
@param $projectVersion   - optional. if empty defaults to current version. if valued then the valueset will come explicitly from that archived project version which is expected to be a compiled version
@param $projectLanguage  - optional. defaults to project defaultLanguage or first available if multiple compilation exist 
@param $objectid         - optional. Identifier of the valueset to retrieve
@param $objectnm         - optional. Name of the valueset to retrieve (valueSet/@name)
@param $objected         - optional. null gets all versions, 'dynamic' gets the newest version based on id or name, yyyy-mm-ddThh:mm:ss gets this specific version
@param $max              - optional. Maximum number of results with minimum 1. Default is $vsapi:maxResults (50)
@param $resolve          - optional. Default = 'true' If true, resolves any references
@return List object with zero or more valueSet
@since 2013-06-14
:)
declare %private function vsapi:getValueSetList($governanceGroupId as xs:string?, $projectPrefix as xs:string?, $projectVersion as xs:string?, $projectLanguage as xs:string?, $searchTerms as xs:string*, $objected as xs:string?, $includebbr as xs:boolean, $sort as xs:string?, $sortorder as xs:string?, $max as xs:integer?, $resolve as xs:boolean) as element(list) {
    
    let $objected               := $objected[not(. = '')]
    let $governanceGroupId      := $governanceGroupId[not(. = '')]
    let $projectPrefix          := $projectPrefix[not(. = '')]
    let $projectVersion         := $projectVersion[not(. = '')]
    let $projectLanguage        := ($projectLanguage[not(. = '')], '*')[1]
    let $sort                   := $sort[string-length() gt 0]
    let $sortorder              := $sortorder[. = 'descending']
    
    let $startT                 := util:system-time()
    
    let $decor                  := 
        if ($governanceGroupId) then
            for $projectId in ggapi:getLinkedProjects($governanceGroupId)/@ref
            return
                utillib:getDecorById($projectId)
        else
        if (utillib:isOid($projectPrefix)) then
            utillib:getDecorById($projectPrefix, $projectVersion, $projectLanguage)
        else (
            utillib:getDecorByPrefix($projectPrefix, $projectVersion, $projectLanguage)
        )
    let $projectPrefix          := ($decor/project/@prefix)[1]
    
    let $results                := 
        if (empty($searchTerms)) then (
            $decor//valueSet
        )
        else (
            let $buildingBlockRepositories  := 
                if ($includebbr) then (
                    utillib:getBuildingBlockRepositories($decor, (), $utillib:strDecorServicesURL)
                )
                else (
                    $decor
                )
            let $luceneQuery                := utillib:getSimpleLuceneQuery($searchTerms, 'wildcard')
            let $luceneOptions              := utillib:getSimpleLuceneOptions() 

            for $ob in ($buildingBlockRepositories//valueSet[@id = $searchTerms] | 
                        $buildingBlockRepositories//valueSet[@id][ft:query(@name, $luceneQuery, $luceneOptions)] |
                        $buildingBlockRepositories//valueSet[@id][ft:query(@displayName, $luceneQuery, $luceneOptions)])
            return
                <valueSet>
                {
                    $ob/(@* except (@url|@ident)),
                    attribute url {($ob/ancestor::cacheme/@bbrurl, $utillib:strDecorServicesURL)[1]},
                    attribute ident {$ob/ancestor::decor/project/@prefix},
                    attribute cachedProject {exists($ob/ancestor::cacheme)},
                    $ob/node()
                }
                </valueSet>
        )
    
    let $allcnt                 := count($results)
    
    let $objectsByRef           :=
        if ($resolve and empty($projectVersion)) then (
            let $decors                 := 
                if (empty($projectVersion)) then utillib:getBuildingBlockRepositories($decor, map {}, $utillib:strDecorServicesURL) else $decor
            
            for $csref in $results/@ref
            group by $csref
            return (
                let $css    := $decors//valueSet[@id = $csref]
                (:let $css    := 
                    if ($css) then $css else 
                        csapi:getBrowsableCodeSystemMeta(collection($setlib:strCodesystemStableData || '/external')/browsableCodeSystem[@oid = $csref]):)
                let $csbyid := $css[@effectiveDate = max($css[@effectiveDate castable as xs:dateTime]/xs:dateTime(@effectiveDate))]
                (: rewrite name and displayName based on latest target codeSystem. These sometimes run out of sync when the original changes its name :)
                return (
                    element {name($csref[1])} {
                        $csref[1], 
                        attribute name {($csbyid/@name, $csref/../@name)[1]}, 
                        attribute displayName {($csbyid/@displayName, $csref/../@displayName, $csbyid/@name, $csref/../@name)[1]}
                        (:,
                        attribute url {$utillib:strDecorServicesURL},
                        attribute ident {($csref/ancestor::decor/project/@prefix)[1]}:)
                    }
                    ,
                    for $csid in $css
                    return
                    element {name($csid)} {
                        $csid/@*,
                        if ($csid/ancestor::decor) then (
                            if ($csid/@url) then () else   attribute url {($csid/ancestor::cacheme/@bbrurl, $utillib:strDecorServicesURL)[1]},
                            if ($csid/@ident) then () else attribute ident {($csid/ancestor::decor/project/@prefix)[1]}
                        )
                        else (),
                        (: this element is not supported (yet?) :)
                        $csid/classification
                    }
                )
            )
        )
        else (
            $results[@ref]
        )
    
    let $results            := $results[@id] | $objectsByRef
    
    (: now we can determine $objected :)
    let $results            :=
        if (empty($objected)) then 
            (: all :)
            $results
        else
        if ($objected castable as xs:dateTime) then
            (: match and ref :)
            $results[@ref] | $results[@effectiveDate = $objected]
        else (
            (: newest and ref :)
            $results[@ref] | $results[@effectiveDate = max($results[@effectiveDate]/xs:dateTime(@effectiveDate))]
        )
    
    let $results            :=
        for $res in $results
        let $id             := $res/@id | $res/@ref
        group by $id
        return (
            let $subversions    :=
                for $resv in $res
                order by $resv/@effectiveDate descending
                return
                    <valueSet uuid="{util:uuid()}">
                    {
                        $resv/(@* except @uuid),
                        (: this element is not supported (yet?) :)
                        $resv/classification
                    }
                    </valueSet>
            let $latest         := ($subversions[@id], $subversions)[1]
            let $rproject       := $res/ancestor::decor/project/@prefix
            return
            <valueSet uuid="{util:uuid()}" id="{$id}">
            {
                $latest/(@* except (@uuid | @id | @ref | @project)),
                ($res/@ref)[1],
                (: there is no attribute @project, but better safe than sorry :)
                (: wrong setting for governance group search: if (empty($governanceGroupId)) then $latest/@project else attribute project {$projectPrefix}, :)
                if (empty($governanceGroupId)) then $latest/@project else attribute project { $rproject[1] },
                $subversions
            }
            </valueSet>
        )
    let $count              := count($results/valueSet)
    let $max                := if ($max ge 1) then $max else $count
    
    (: handle sorting. somehow reverse() does not do what I expect :)
    let $results            :=
        switch ($sort)
        case 'displayName' return 
            if ($sortorder = 'descending') then 
                for $r in $results order by $r/lower-case(@displayName) descending return $r
            else (
                for $r in $results order by $r/lower-case(@displayName)            return $r
            )
        case 'name'        return 
            if ($sortorder = 'descending') then 
                for $r in $results order by $r/lower-case(@name) descending return $r
            else (
                for $r in $results order by $r/lower-case(@name)            return $r
            )
        default            return 
            if ($sortorder = 'descending') then 
                for $r in $results order by replace(replace($r/@id, '\.', '.0000000000'), '.0*([0-9]{9,})', '.$1') descending return $r
            else (
                for $r in $results order by replace(replace($r/@id, '\.', '.0000000000'), '.0*([0-9]{9,})', '.$1')            return $r
            )
    
    let $durationT := (util:system-time() - $startT) div xs:dayTimeDuration("PT0.001S")
    
    return
        <list artifact="VS" elapsed="{$durationT}" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" resolve="{$resolve}" project="{$projectPrefix}" lastModifiedDate="{current-dateTime()}">
        {
            subsequence($results, 1, $max)
        }
        </list>
};

(:~ Returns a valueset for publication in csv format 

@param $cvalueSetss required, is retrieved from request id/effectiveDate
@param $language optional from request parameter or derived from artLanguage 
@param $projectPrefix optional from parameter request 
@returns a valueSet in csv format

:)
declare function vsapi:convertValueSet2Csv($valueSets as element()*, $language as xs:string?, $projectPrefix as xs:string?) as xs:string* {

    let $language           := if (empty($language)) then $setlib:strArtLanguage else $language
    
    let $mapentries :=
        for $d in distinct-values($valueSets//designation/string-join((@type,@language),' / '))
        return map:entry($d, max($valueSets//*/count(designation[$d = string-join((@type,@language),' / ')])))
    let $designationmap     := map:merge($mapentries)
    let $header             := 
        concat('Level;Type;Code;DisplayName;CodeSystem;CodeSystemName;CodeSystemVersion;Flexibility;Exception',
            if (count($mapentries)=0) then () else (
                concat(';',string-join(
                    for $d in map:keys($designationmap)
                    for $i in (1 to map:get($designationmap, $d))
                    return concat('&quot;Designation ',$d,'&quot;')
                ,';'))
            )
        ,'&#13;&#10;')
    let $columncount        := count(tokenize($header,';'))
    let $keyValueSet        :=
        switch($language)
            case "en-US" return "Value Set"
            case "de-DE" return "Value Set"
            case "nl-NL" return "Waardelijst"
            default return "Value Set"
    return (
    (:<concept code="xxxxxx" codeSystem="2.16.840.1.113883.2.4.15.4" displayName="Medicatie" level="0" type="A"/> :)
    (: Replace double quotes with single quotes in the CSV values, except in the code itself, 
    and place in between double quotes if there a white space character in a string
    Note that in the exceptional event that a code contains a double quote, the CSV renders invalid :)
    for $valueset at $i in $valueSets//valueSet[@id]
    return (
        if ($i > 1) then ('&#13;&#10;') else (),
        concat('&quot;',$keyValueSet,' ',$valueset/@displayName,' - ',$valueset/(@id|@ref),' ',$valueset/@effectiveDate,'&quot;',string-join(for $i in (1 to ($columncount - 1)) return ';',''),'&#13;&#10;'),
        $header,
        for $concept in ($valueset/completeCodeSystem, $valueset/conceptList/include[@codeSystem][not(@code)], $valueset/conceptList/concept, $valueset/conceptList/exception)
            let $conceptCode                    := data($concept/@code)
            let $quotedConceptCode              := if (matches($conceptCode,'\s+')) then (concat('&quot;',$conceptCode,'&quot;')) else ($conceptCode)
            let $conceptDisplayName             := replace(data($concept/@displayName),'"','&apos;')
            let $quotedConceptDisplayName       := if (matches($conceptDisplayName,'\s+')) then (concat('&quot;',$conceptDisplayName,'&quot;')) else ($conceptDisplayName)
            let $conceptCodeSystem              := replace(data($concept/@codeSystem),'"','&apos;')
            let $quotedConceptCodeSystem        := if (matches($conceptCodeSystem,'\s+')) then (concat('&quot;',$conceptCodeSystem,'&quot;')) else ($conceptCodeSystem)
            let $generatedCodeSystemName        := utillib:getNameForOID($concept/@codeSystem,$language,$projectPrefix)
            let $conceptCodeSystemName          := replace(if ($concept/@codeSystemName) then (data($concept/@codeSystemName)) else if (replace($generatedCodeSystemName,'[0-9\.]','')!='') then ($generatedCodeSystemName) else (''),'"','&apos;')
            let $quotedConceptCodeSystemName    := if (matches($conceptCodeSystemName,'\s+')) then (concat('&quot;',$conceptCodeSystemName,'&quot;')) else ($conceptCodeSystemName)
            let $conceptCodeSystemVersion       := replace(data($concept/@codeSystemVersion),'"','&apos;')
            let $quotedConceptCodeSystemVersion := if (matches($conceptCodeSystemVersion,'\s+')) then (concat('&quot;',$conceptCodeSystemVersion,'&quot;')) else ($conceptCodeSystemVersion)
            let $conceptFlexibility             := if ($concept[self::completeCodeSystem | self::include]) then (replace(if ($concept/@flexibility) then (data($concept/@flexibility)) else ('dynamic'),'"','&apos;')) else ('')
            let $quotedconceptFlexibility       := if (matches($conceptFlexibility,'\s+')) then (concat('&quot;',$conceptFlexibility,'&quot;')) else ($conceptFlexibility)
            let $quotedException                := $concept/name()='exception'
        return (
            concat(data($concept/@level),';',data($concept/@type),';',$quotedConceptCode,';',$quotedConceptDisplayName,';',$quotedConceptCodeSystem,';',$quotedConceptCodeSystemName,';',$quotedConceptCodeSystemVersion,';',$quotedconceptFlexibility,';',$quotedException,
                concat(';',string-join(
                    for $d in map:keys($designationmap)
                    for $i in (1 to map:get($designationmap, $d))
                    let $designation    := $concept/designation[$d=string-join((@type,@language),' / ')][1]
                    return 
                        if ($designation[@displayName]) then
                        if (matches($designation/@displayName,'\s+')) then (
                            concat('&quot;',$designation/@displayName,'&quot;')
                        ) else (
                            $designation/@displayName
                        )
                        else ('')
                ,';'))
            ,'&#13;&#10;')
        )
    ))
    
};    

(:~ Returns a valueset for publication in xml-svs format 

@param $cvalueSetss required, is retrieved from request id/effectiveDate
@param $language optional from request parameter or derived from artLanguage 
@param $projectPrefix optional from parameter request 
@returns a valueSet in xml-svs format

KH 20190815
format mdibagch is a proprietay special type of svs in a different namespace "urn:ch:admin:bag:epr:2017:MdiImport"
for customer Bundesamt für Gesundheits, Bern, Schweiz
:)

declare function vsapi:convertValueSet2Svs($valueSets as element()*, $language as xs:string?, $projectVersion as xs:string?, $format as xs:string) as element()* {

    let $valuesetcount       := count($valueSets//*:valueSet[@id])
    let $valuesetelementname := if (($valuesetcount > 1) and ($format!='mdibagch')) then 'DescribedValueSet' else 'ValueSet'
    (: KH 20190815 speacial handling of value set element name for mdibagch :)
    let $decor               := utillib:getDecorByPrefix($valueSets//project/@ident[1], $projectVersion)[1]
    let $url                 := if ($decor) then $decor/project else ()
    let $projectname         := if ($url) then ($url/name)[1] else ()
    let $svs1language        := if (empty($language)) then '' else $language 
    let $language            := if (empty($language)) then $setlib:strArtLanguage else $language
    
    let $svsresult           :=  
        for $valueset in $valueSets//*:valueSet[@id]
        let $intensional := $valueset/*:completeCodeSystem or $valueset/*:conceptList/*:include or $valueset/*:conceptList/*:exclude
        let $includerefonly := not($intensional) or ($valueset/*:conceptList/*:include[@ref] and not($valueset/*:completeCodeSystem or $valueset/*:conceptList/*:include[empty(@ref)] or $valueset/*:conceptList/*:exclude))
        let $defaultNamespace-uri   :=
            if ($format='mdibagch')
            then "urn:ch:admin:bag:epr:2017:MdiImport" 
            else "urn:ihe:iti:svs:2008"
            (: KH 20190815 special handling of namespaces for mdibagch :)
        let $otherLanguages    := $valueset/*:conceptList/*:concept/*:designation[@language != $svs1language] | 
                                  $valueset/*:conceptList/*:exception/*:designation[@language != $svs1language]
        let $tmpLang           := $valueset/*:conceptList/*:concept/*:designation/@language
        let $hasnodesignations := count($tmpLang) = 0
        let $allLanguages      := if ($hasnodesignations) then $setlib:strArtLanguage else $tmpLang
        return
            (:<result xmlns="urn:ihe:iti:svs:2008" xmlns:ihesvs="urn:ihe:iti:svs:2008" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">:)
            element {QName($defaultNamespace-uri,"result")} {
                element {QName($defaultNamespace-uri, $valuesetelementname)} {
                   attribute { "id" } {$valueset/@id},
                   attribute { "displayName" } {if ($valueset/@displayName) then $valueset/@displayName else $valueset/@name},
                   attribute { "version" } {if ($valueset/@versionLabel) then $valueset/@versionLabel else $valueset/@effectiveDate},

                   (: Source: This is the source of the value set, identifying the originator or publisher of the information :)
                   let $pa := if ($valueset/*:publishingAuthority/@name) then data($valueset/*:publishingAuthority/@name) else ()
                   let $cr := if ($valueset/*:copyright) then $valueset/*:copyright else ()
                   return if ($pa or $cr) then 
                       <Source>
                       {
                           $pa
                       }
                       {
                           if ($pa) then ' - ' else (),
                           string-join($cr/text(), ' - ')
                       }
                       </Source> 
                       else (),

                   (: Purpose: Brief description about the general purpose of the value set :)
                   if ($valueset/*:desc[@language="en-US"])
                   then <Purpose>{$valueset/*:desc[@language="en-US"]/text()}</Purpose> 
                   else if ($valueset/*:desc[not(@language="en-US")][1]) 
                   then <Purpose>{concat(data($valueset/*:desc/@language), ': ')}{$valueset/*:desc[not(@language="en-US")][1]/text()}</Purpose> 
                   else (),

                   (: Active, Inactive, local extensions :)
                   if ($valueset/@statusCode=("final", "draft"))
                   then <Status>Active</Status>
                   else if ($valueset/@statusCode=("rejected", "cancelled", "deprecated"))
                   then <Status>Inactive</Status>
                   else <Status>{data($valueset/@statusCode)}</Status>,
               
                   (: Type: This describes the type of the value set. It shall be Intensional, Extensional, or Expanded.
                            Note: This is the type of the value set in the repository. The ConceptList that will also be returned is the current expansion of the value set.
                   :)
                   if ($intensional) 
                   then <Type>Intensional</Type>
                   else <Type>Extensional</Type>,
              
                   (: Effective Date The date when the value set is expected to be effective, defaults to effectiveDate :)
                   if ($valueset/@officialReleaseDate)
                   then <EffectiveDate>{data(substring($valueset/@officialReleaseDate,1, 10))}</EffectiveDate>
                   else <EffectiveDate>{data(substring($valueset/@effectiveDate,1, 10))}</EffectiveDate>,
               
                   (: Expiration Date: The date when the value set is no longer expected to be used :)
                   if ($valueset/@expirationDate)
                   then <ExpirationDate>{data(substring($valueset/@expirationDate,1, 10))}</ExpirationDate>
                   else (),
               
                   (: Creation Date: The date of creation of the value set :)
                   if ($valueset/@effectiveDate)
                   then <CreationDate>{data(substring($valueset/@effectiveDate,1, 10))}</CreationDate>
                   else (),
               
                   if (not($intensional) or $includerefonly)
                   then
                        (
                        if ( (string-length($svs1language) = 0) or ($hasnodesignations) )
                        then
                            (
                             for $lang in distinct-values($allLanguages)
                             return
                                <ConceptList xml:lang="{$lang}">
                                {
                                    for $concept in ($valueset/*:conceptList/*:concept | $valueset/*:conceptList/*:exception)
                                    let $d := if ($concept/designation[@language=$lang]) then $concept/designation[@language=$lang] else $concept
                                    let $conceptCode              := data($concept/@code)
                                    let $conceptCodeSystem        := data($concept/@codeSystem)
                                    let $conceptDisplayName       := data($d/@displayName)
                                    let $conceptCodeSystemVersion := data($concept/@codeSystemVersion)
                                    return
                                    <Concept code="{$conceptCode}" codeSystem="{$conceptCodeSystem}" displayName="{$conceptDisplayName}">
                                    {
                                        if (string-length($conceptCodeSystemVersion) > 0) then $conceptCodeSystemVersion else ()
                                    }
                                    </Concept>
                                }
                                </ConceptList>
                           )
                        else
                            (: a specific language is specified, find the designations :)
                            <ConceptList xml:lang="{$svs1language}">
                            {
                                for $concept in ($valueset/*:conceptList/*:concept | $valueset/*:conceptList/*:exception)
                                let $d := if ($concept/designation[@language=$svs1language]) then $concept/designation[@language=$svs1language] else $concept
                                let $conceptCode              := data($concept/@code)
                                let $conceptCodeSystem        := data($concept/@codeSystem)
                                let $conceptDisplayName       := data($d/@displayName)
                                let $conceptCodeSystemVersion := data($concept/@codeSystemVersion)
                                return
                                <Concept code="{$conceptCode}" codeSystem="{$conceptCodeSystem}" displayName="{$conceptDisplayName}">
                                {
                                    if (string-length($conceptCodeSystemVersion) > 0) then $conceptCodeSystemVersion else ()
                                }
                                </Concept>
                            
                            }
                            </ConceptList>
                        )
                   else (<!-- Intensional Value Set not expanded -->),
                   (:
                       Place Project OID if available in Group
                       <Group id="2.16.840.1.113883.3.1937.777.13" displayName="International Patient Summary (IPS)" sourceOrganization="ART-DECOR Expert Group">
                   :)
                   if ($url)
                       then
                           <Group id="{$url/@id}" displayName="{$projectname}" sourceOrganization="ART-DECOR Expert Group">
                           {
                               <Keyword>ART-DECOR Project</Keyword>
                           }
                           </Group>
                       else () 
               }
          }
        
    return
        if ($format='mdibagch')
        then
            (: KH 20190815 special handling for mdibagch, no cacheExpirationHint="{format-dateTime(current-dateTime(), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')}" :)
            <RetrieveMultipleValueSetsResponse 
                    xmlns="urn:ch:admin:bag:epr:2017:MdiImport" xmlns:ihesvs="urn:ihe:iti:svs:2008" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            {
                    $svsresult/*
            }
            </RetrieveMultipleValueSetsResponse>
        else
            (: standard svs format :)
            if ($valuesetcount > 1)
            then
                <RetrieveMultipleValueSetsResponse 
                    xmlns="urn:ihe:iti:svs:2008" xmlns:ihesvs="urn:ihe:iti:svs:2008" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                    cacheExpirationHint="{format-dateTime(current-dateTime(), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')}">
                {
                    $svsresult/*
                }
                </RetrieveMultipleValueSetsResponse>
            else
                <RetrieveValueSetResponse 
                    xmlns="urn:ihe:iti:svs:2008" xmlns:ihesvs="urn:ihe:iti:svs:2008" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                    cacheExpirationHint="{format-dateTime(current-dateTime(), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')}">
                {
                    $svsresult/*
                }
                </RetrieveValueSetResponse>

};

(: ================================================== :)
(: ============== VALUE SET EXPANSIONS ============== :)
(: ================================================== :)

(:~ Retrieves DECOR valueSet expansion based on $id (oid)
    @param $id                      - required parameter denoting the id of the valueSet
    @return as-is or as compiled as JSON
    @since 2023-08-010
:)
declare function vsapi:getValueSetExpansionSet($request as map(*)) {
    
    let $id                             := $request?parameters?id
    
    let $results                        := collection($setlib:strValuesetExpansionData)/valueSetExpansionSet[@id = $id]
    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple valueSet expansions for id '", $id, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        )
        else (
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
        )
};

(:~ Update valueSet expansion statusCode, and/or expirationDate

@param $authmap required. Map derived from token
@param $id required. valueSetExpansionset/@id
@param $newStatusCode optional as xs:string. Default: empty. If empty, does not update the statusCode
@param $newExpirationDate optional as xs:string. Default: empty. If empty, does not update the expirationDate 
@return list object with success and/or error elements or error
:)
declare function vsapi:postValueSetExpansionSetStatus($request as map(*)) {

    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    let $statusCode                     := $request?parameters?statusCode
    let $expirationDate                 := $request?parameters?expirationDate
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    
    return
        vsapi:setValueSetExpansionSetStatus($authmap, $id, $statusCode, $expirationDate)
};

(:~ Delete valueSet expansion

@param $authmap required. Map derived from token
@param $id required. alueSetExpansionset/@id
@return nothing
:)
declare function vsapi:deleteValueSetExpansionSet($request as map(*)) {

    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    
    let $check                          :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    (: don't want to update cache or external expansions :)
    let $check                  :=
        for $expansion in collection($setlib:strValuesetExpansionData)/valueSetExpansionSet[@id = $id]
        let $location1              := tokenize(util:collection-name($expansion), '/')[last() - 1]
        let $projectPrefix          := tokenize(util:collection-name($expansion), '/')[last()]
        let $location2              := string-join(($location1, $projectPrefix), '/')
        let $decor                  := if ($location1 = 'projects') then utillib:getDecorByPrefix($projectPrefix) else ()
        return
            if (empty($decor)) then
                if ($authmap?dba) then () else (
                    error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to delete valueSet expansions in ', $location1, '. You need to be a dba'))
                )
            else
            if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-TERMINOLOGY)) then () else (
                error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to delete valueSet expansions in project ', $projectPrefix, '. You have to be an active author in the project.'))
            )
        
    let $delete                         := 
        for $expansion in collection($setlib:strValuesetExpansionData)/valueSetExpansionSet[@id = $id]
        return
            xmldb:remove(util:collection-name($expansion), util:document-name($expansion))
    return
        roaster:response(204, ())
};

(:~ Update DECOR valueSet expansion set parts. Expect array of parameter objects, each containing RFC 6902 compliant contents.

{ "op": "[add|remove|replace]", "path": "e.g. [/statusCode|/expirationDate]", "value": "[string]" }

where

* op - add & replace (statusCode, expirationDate) or remove (expirationDate)

* path - see above

* value - (empty) string
@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $id                       - required. the id for the valueSet to update 
@param $request-body             - required. json body containing array of parameter objects each containing RFC 6902 compliant contents
@return valueSet expansion set structure
@since 2023-08-14
@see http://tools.ietf.org/html/rfc6902
:)
declare function vsapi:patchValueSetExpansionSet($request as map(*)) {

    let $authmap                := $request?user
    let $id                     := $request?parameters?id
    let $data                   := utillib:getBodyAsXml($request?body, 'parameters', ())
    
    (:let $s                      := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data) :)
    
    let $check                  :=
        if ($data) then () else (
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        )
    
    let $return                 := vsapi:patchValueSetExpansionSet($authmap, string($id), $data)
    return (
        roaster:response(200, $return)
    )
};

(:~ Central logic for patching an existing valueSet

@param $authmap         - required. Map derived from token
@param $id              - required. DECOR valueSet/@id to update
@param $effectiveDate   - required. DECOR valueSet/@effectiveDate to update
@param $data            - required. DECOR xml element parameter elements each containing RFC 6902 compliant contents
@return dataset object as xml with json:array set on elements
:)
declare function vsapi:patchValueSetExpansionSet($authmap as map(*), $id as xs:string, $data as element(parameters)) as element(valueSetExpansionSet) {

    let $storedExpansion        := collection($setlib:strValuesetExpansionData)/valueSetExpansionSet[@id = $id]
    
    let $check                  :=
        if (count($storedExpansion) = 1) then () else
        if ($storedExpansion) then
            error($errors:SERVER_ERROR, 'Value set expansion id ''' || $id || ''' cannot be updated because multiple (' || count($storedExpansion) || ') were found. Please inform your database administrator.')
        else (
            error($errors:BAD_REQUEST, 'Value set expansion id ' || $id || ' does not exist')
        )
        
    (: don't want to update cache or external expansions :)
    let $location1              := tokenize(util:collection-name($storedExpansion), '/')[last() - 1]
    let $projectPrefix          := tokenize(util:collection-name($storedExpansion), '/')[last()]
    let $location2              := string-join(($location1, $projectPrefix), '/')
    let $decor                  := if ($location1 = 'projects') then utillib:getDecorByPrefix($projectPrefix) else ()
    
    let $check                  :=
        if (empty($decor)) then
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify valueSet expansions in ', $location1, '. These expansions can only be updated through server admin install'))
        else
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-TERMINOLOGY)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify valueSet expansions in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    
    let $check                  :=
        if ($data[parameter]) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall be an array of ''parameter''.')
        )
    let $unsupportedops         := $data/parameter[not(@op = ('add', 'replace', 'remove'))]
    let $check                  :=
        if ($unsupportedops) then
            error($errors:BAD_REQUEST, 'Submitted parameters shall have supported ''op'' value. Found ''' || string-join(distinct-values($unsupportedops/@op), ''', ''') || ''', expected ''add'', ''replace'', or ''remove''') 
        else ()
    
    let $check                  :=
        for $param in $data/parameter
        let $op         := $param/@op
        let $path       := $param/@path
        let $value      := $param/@value
        let $pathpart   := substring-after($path, '/')
        return
            switch ($path)
            case '/statusCode' return (
                if ($op = 'remove') then
                    'Parameter path ''' || $path || ''' does not support ' || $op
                else 
                if (utillib:isStatusChangeAllowable($storedExpansion, $value)) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Supported are: ' || string-join(map:get($utillib:valuesetexpansionstatusmap, string($storedExpansion/@statusCode)), ', ')
                )
            )
            case '/expirationDate' return (
                if ($op = 'remove') then () else 
                if ($value castable as xs:dateTime) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL be yyyy-mm-ddThh:mm:ss.'
                )
            )
            default return (
                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Path value not supported'
            )
     let $check                 :=
        if (empty($check)) then () else (
            error($errors:BAD_REQUEST,  string-join ($check, ' '))
        )
    
    let $update                 :=
        for $param in $data/parameter
        return
            switch ($param/@path)
            case '/statusCode'
            case '/expirationDate' return (
                let $attname  := substring-after($param/@path, '/')
                let $new      := 
                    if ($attname = 'expirationDate') then
                        attribute {$attname} {substring($param/@value, 1, 19)}
                    else (
                        attribute {$attname} {$param/@value}
                    )
                let $stored   := $storedExpansion/@*[name() = $attname]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedExpansion
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            default return ( (: unknown path :) )
    
    return
        element {name($storedExpansion)} {
            $storedExpansion/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($storedExpansion/*)
        }
};

(:~ Central logic for patching an existing valueSet expansion statusCode, versionLabel, expirationDate and/or officialReleaseDate

@param $authmap required. Map derived from token
@param $id required. valueSetExpansionset/@id
@param $newStatusCode optional as xs:string. Default: empty. If empty, does not update the statusCode
@param $newExpirationDate optional as xs:string. Default: empty. If empty, does not update the expirationDate 
@return list object with success and/or error elements or error
:)
declare function vsapi:setValueSetExpansionSetStatus($authmap as map(*), $id as xs:string, $newStatusCode as xs:string?, $newExpirationDate as xs:string?) as element(list) {
    (:get object for reference:)
    let $object                 := collection($setlib:strValuesetExpansionData)/valueSetExpansionSet[@id = $id]
    
    let $check                  :=
        if (count($object) = 1) then () else
        if ($object) then
            error($errors:SERVER_ERROR, 'Value set expansion id ''' || $id || ''' cannot be updated because multiple ' || count($object) || ' were found. Please inform your database administrator.')
        else (
            error($errors:BAD_REQUEST, 'Value set expansion id ''' || $id || ''' cannot be updated because it cannot be found.')
        )
    let $check                  :=
        if (empty($newStatusCode)) then () else
        if (utillib:isStatusChangeAllowable($object, $newStatusCode)) then () 
        else (
            error($errors:BAD_REQUEST, 'Status transition not allowed from ' || $object/@statusCode || ' to ' || $newStatusCode || '. Allowed: ' || string-join(map:get($utillib:valuesetexpansionstatusmap, string($object/@statusCode)), ', or '))
        )
    let $check                  :=
        if (empty($newExpirationDate)) then () else if ($newExpirationDate castable as xs:dateTime or $newExpirationDate = '') then () 
        else (
            error($errors:BAD_REQUEST, 'New value set expansion expirationDate ''' || $newExpirationDate || ''' is not supported. Please support a valid dateTime or empty string')
        )
        
    (: don't want to update cache or external expansions :)
    let $location1              := tokenize(util:collection-name($object), '/')[last() - 1]
    let $projectPrefix          := tokenize(util:collection-name($object), '/')[last()]
    let $location2              := string-join(($location1, $projectPrefix), '/')
    let $decor                  := if ($location1 = 'projects') then utillib:getDecorByPrefix($projectPrefix) else ()
    
    let $check                  :=
        if (empty($decor)) then
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify valueSet expansions in ', $location1, '. These expansions can only be updated through server admin install'))
        else
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-TERMINOLOGY)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify valueSet expansions in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
        
    let $update                 :=
        if ($newStatusCode) then
            update value $object/@statusCode with $newStatusCode
        else ()
    let $update                 :=
        if ($newExpirationDate) then
            if ($newExpirationDate = '') then
                update delete $object/@expirationDate
            else
            if ($object/@expirationDate) then
                update value $object/@expirationDate with $newExpirationDate
            else (
                update insert attribute expirationDate {$newExpirationDate} into $object 
            )
        else ()

    return
        <list artifact="STATUS" current="1" total="1" all="1" lastModifiedDate="1" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements(
                <success itemcode="VSE" itemname="{local-name($object)}">
                {
                    $object/(@* except (@itemcode | @itemname))
                }
                </success>
            )
        }
        </list>
};
