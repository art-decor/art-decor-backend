xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:~ Code system API allows read, create, update on DECOR codeSystems :)
module namespace tcsapi             = "http://art-decor.org/ns/api/terminology/codesystem";


import module namespace roaster     = "http://e-editiones.org/roaster";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

import module namespace utillib     = "http://art-decor.org/ns/api/util" at "library/util-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "library/settings-lib.xqm";
import module namespace decorlib    = "http://art-decor.org/ns/api/decor" at "library/decor-lib.xqm";
import module namespace vsapi       = "http://art-decor.org/ns/api/valueset" at "valueset-api.xqm";

(:import module namespace cs          = "http://art-decor.org/ns/decor/codesystem" at "/db/apps/art/api/api-decor-codesystem.xqm";:)
import module namespace adterm      = "http://art-decor.org/ns/terminology" at "/db/apps/terminology/api/api-terminology.xqm";

declare namespace json      = "http://www.json.org";
declare namespace rest      = "http://exquery.org/ns/restxq";
declare namespace resterr   = "http://exquery.org/ns/restxq/error";
declare namespace http      = "http://expath.org/ns/http-client";
declare namespace output    = "http://www.w3.org/2010/xslt-xquery-serialization";

declare %private variable $tcsapi:STATUSCODES-FINAL    := ('final', 'pending', 'rejected', 'cancelled', 'deprecated');

(:~ Search code systems for string in designation or search for code
    @param $context           - optional parameter denoting the context of the search
    @param $codeSystemId      - optional parameter denoting a list of codesytem ids to search
    @param $language          - optional parameter specifying search language, only relevant for string search ignored with code search
    @param $string            - optional parameter search string
    @param $code              - optional parameter code, either string or code must be present
    @param $ancestor          - optiona parameter denoting list of ancestor concept ids
    @return as JSON
    @since 2020-05-11
:)
declare function tcsapi:getCodeSystem($request as map(*)) {

   let $results := tcsapi:getCodeSystem($request?parameters?context, $request?parameters?loincContext, $request?parameters?codeSystemId, $request?parameters?language, $request?parameters?string, $request?parameters?code, $request?parameters?statusCode, $request?parameters?ancestor, $request?parameters?isa, $request?parameters?refset)
   return
     if (empty($results)) then (
         roaster:response(404, ())
     )
     else $results
};

(:~ Get information on the code system including version and language (if the system pertains to one language only)

@param $id       - required. Identification of the code system to get info for
@return information on the code system as json
@since 2020-05-11
:)
declare function tcsapi:getCodeSystemInfo($request as map(*)) {
   tcsapi:getCodeSystemVersionInfo($request?parameters?id)
};

(:~Get information on all distinct languages supported by all code systems combined

@return languages as json
@since 2020-06-19
:)
declare function tcsapi:getCodeSystemLanguages($request as map(*)) {
   tcsapi:getCodeSystemLanguages()
};

(:~Get SNOMED-CT toplevel and refset filter list for use in terminology browser

@return toplevel concepts and reference set concepts as json
@since 2023-10-20
:)
declare function tcsapi:getSnomedFilters($request as map(*)) {
   tcsapi:getSnomedFilters()
};
(:~Get code system concept

@return code system concept as json
@since 2020-05-11
:)
declare function tcsapi:getConcept($request as map(*)) {
   tcsapi:getConcept($request?parameters?codeSystemId, $request?parameters?conceptId, $request?parameters?preferred, $request?parameters?language, $request?parameters?children)
};

(:~Get child concepts of code system concept

@return concepts as json
@since 2020-05-13
:)
declare function tcsapi:getConceptChildren($request as map(*)) {
   tcsapi:getConceptChildren($request?parameters?codeSystemId, $request?parameters?conceptId, $request?parameters?preferred, $request?parameters?start, $request?parameters?children)
};

(:~ Expand a composition (with filters or concepts) of an intensional value set denoted by id / effectiveDate and returns preview count (default) or all expanded concepts including count (expand=true)

@return concepts as json
@since 2021-11-11
:)
declare function tcsapi:getExpandedComposition($request as map(*)) {
    tcsapi:postExpandedComposition($request)
};
declare function tcsapi:postExpandedComposition($request as map(*)) {

    let $debug              := $request?parameters?debug = true()
    let $authmap            := $request?user
    let $id                 := $request?parameters?id[string-length() gt 0]
    let $effectiveDate      := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $expand             := $request?parameters?expand = true()
    let $store              := if ($expand) then $request?parameters?store = true() else false()
    let $project            := $request?parameters?project[string-length() gt 0]
    let $projectVersion     := $request?parameters?release[string-length() gt 0]
    let $projectLanguage    := $request?parameters?language[string-length() gt 0]
    let $searchString       := for $s in $request?parameters?search[string-length() gt 0] return normalize-space(lower-case($s))
    
    let $check                  :=
        if ($store and empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first if you want to store the expansion')
        else ()
    let $check              :=
        if (empty($id) or empty($effectiveDate)) 
        then error($errors:BAD_REQUEST, 'Parameter id and effectiveDate are required.') 
        else ()

    (: never return more that maxResults codes in expansion set :)
    let $maxResults         := 100
    
    (: get and check thevalue set specified :)
    let $valueSet           := tcsapi:getValueSetForExpansion($id, $effectiveDate)
    let $check              :=
        if (empty($valueSet)) then 
            error($errors:BAD_REQUEST, 'Value set with id ' || $id || ' as of ' || $effectiveDate || ' does not exist') 
        else
        if ($store) then (
            let $decorProject   := utillib:getDecor($valueSet/@ident, (), ())
            return
            if ($valueSet and empty($decorProject)) then 
                if ($authmap?dba) then () else (
                    error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions for cached terminology from project ', $valueSet/@ident, 'from ', $valueSet/@url,'. You have to be an active author in the project.'))
                )
            else
            if (decorlib:authorCanEditP($authmap, $decorProject, $decorlib:SECTION-TERMINOLOGY)) then () else (
                error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions for terminology in project ', $valueSet/@ident, '. You have to be an active author in the project.'))
            )
        )
        else ()
    
    let $params         :=
        map {
            "maxResults": $maxResults,
            "expand": $expand,
            "debug": $debug,
            "searchString": string-join($searchString, ' ')
        }
    let $result         := tcsapi:getValueSetExpansionSet($valueSet, $params)
    
    let $store            :=
        if ($store) then (
            let $xml    := element {name($result)} { $result/(@* except @query), $result/(node() except composition) }
            let $coll   := if ($valueSet/@url) then '/cache' else '/projects'
            let $coll   := xmldb:create-collection($setlib:strValuesetExpansionData || $coll, $valueSet/@ident)
            let $store  := xmldb:store($coll, $valueSet/@id || '-' || $result/@id || '.xml', $xml)
            let $del    := update delete doc($store)/(@current | @count | @nonactive)
            return ()
                
        ) else ()
    
    return
        <valueSetExpansionSet xmlns:json="http://www.json.org">
        {
            $result/@id, $result/@effectiveDate, $result/@expirationDate, $result/@statusCode, $result/@current, $result/@count, $result/@nonactive,
            (:present when in debug mode:)
            $result/@query,
            utillib:addJsonArrayToElements((
                $result/valueSet,
                (:present when in debug mode:)
                $result/composition,
                <expansion expanded="{$expand}" max="{$maxResults}">
                {
                    $result/@id,
                    if ($expand) then tcsapi:copyExpansionList(subsequence($result/expansion/*, 1, $maxResults)) else ()
                }
                </expansion>
            ))
        }
        </valueSetExpansionSet>
};

declare function tcsapi:getValueSetExpansionSet($valueSet as element(valueSet), $params as map(*)) {
    let $maxResults           := ($params?maxResults[. instance of xs:positiveInteger][. ge 1], 1000)[1]
    let $expand               := $params?expand[. instance of xs:boolean] = true()
    let $debug                := $params?debug[. instance of xs:boolean] = true()
    let $searchString         := $params?searchString[not(. = '')]
    let $valueSet             := tcsapi:getValueSetForExpansion($valueSet)
    let $composition          := 
        <composition>
        {
            $valueSet/@*
        }
        {
            for $ccs in $valueSet/completeCodeSystem
            return
                <include>{$ccs/@*, $ccs/*}</include>
            ,
            $valueSet/conceptList/*
        }
        </composition>
        
    (: is the value set intensional at all? :)
    let $intensional        := count($composition/completeCodeSystem) + count($composition/include) + count($composition/exclude) gt 0
    
    (: get expansions as a query to execute :)
    let $luceneQuery        := if (empty($searchString)) then () else utillib:getSimpleLuceneQuery($searchString)
    let $luceneOptions      := if (empty($searchString)) then () else utillib:getSimpleLuceneOptions()
    let $expansion          := tcsapi:getExpansionQueryForCompose($composition)
    let $queryStringAll     := string-join($expansion/query/@q, '|')
    
    (: first concepts, then exceptions :)
    let $result             := 
        for $qq in $expansion/query
        let $type           := if ($qq[@exception='true']) then 'exception' else 'concept'
        order by $type
        return
            <concepts codeSystem="{$qq/@codeSystem}" collection="{$qq/@collection}" type="{$type}">
            {
                if (empty($searchString)) then 
                    util:eval($qq/@q)
                else (
                    util:eval($qq/@q)[ft:query(designation, $luceneQuery, $luceneOptions)]
                )
            }
            </concepts>
    let $resultCount        := count($result/*)
    
    (: count any code in include or exclude construct that is NOT active, for warnings on incomplete expansions :)
    let $nonActiveCount     := count(util:eval(string-join($expansion/query/@nonactive, '|')))
    
    let $current            :=
        if ($expand) then if ($resultCount > $maxResults) then $maxResults else $resultCount else $resultCount
    
    let $timestamp          := current-dateTime()
    
    return
        <valueSetExpansionSet current="{$current}" count="{$resultCount}" nonactive="{$nonActiveCount}" search="{string-length($searchString) gt 0}">
        {
            
            if ($debug) then attribute query { $queryStringAll } else ()
        }
        {
            if ($expand) then (
                attribute id {util:uuid()},
                attribute effectiveDate {substring(string($timestamp), 1, 19)},
                attribute statusCode {'final'},
                for $vs in $valueSet
                return
                    element {name($vs)} {
                        $vs/(@* except @inode),
                        $vs/*
                    }
                ,
                if ($debug) then $composition else ()
                ,
                <expansion timestamp="{$timestamp}" total="{$resultCount}">
                {
                    tcsapi:cropc(subsequence($result/*, 1, $maxResults))
                }
                </expansion>
            ) else ()
        }
        </valueSetExpansionSet>
};

declare function tcsapi:importLoincPanelPreview($request as map(*)) {

    let $authmap                        := $request?user
    let $datasetId                      := $request?parameters?datasetId
    let $datasetEffectiveDate           := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $conceptBaseId                  := $request?parameters?baseId
    let $insertMode                     := $request?parameters?insertMode
    let $insertRef                      := $request?parameters?insertRef
    let $insertFlexibility              := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?insertFlexibility)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?insertFlexibility[string-length() gt 0]
        }
    let $panelCode                       := $request?parameters?panelCode
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    
    return
    tcsapi:createConceptFromPanel($authmap, $datasetId, $datasetEffectiveDate, $conceptBaseId, $insertMode, $insertRef, $insertFlexibility, $panelCode, xs:boolean('false'))
};

declare function tcsapi:importLoincPanel($request as map(*)) {

    let $authmap                        := $request?user
    let $data                           := utillib:getBodyAsXml($request?body, (), ())
    
    let $datasetId                      := $data/@datasetId
    let $datasetEffectiveDate           := $data/@effectiveDate
    let $conceptBaseId                  := $data/@baseId
    let $insertMode                     := $data/@insertMode
    let $insertRef                      := $data/@insertRef
    let $insertFlexibility              := $data/@insertFlexibility
    let $panelCode                      := $data/@panelCode    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
    if (empty($request?body)) then 
        error($errors:BAD_REQUEST, 'Request SHALL have data')
    else ()
    
    return
    tcsapi:createConceptFromPanel($authmap, $datasetId, $datasetEffectiveDate, $conceptBaseId, $insertMode, $insertRef, $insertFlexibility, $panelCode, xs:boolean('true'))
};

(: private functions for expansion sets
   =================
:)

(: return value set with required ref and optional flexibility (default: DYNAMIC) in raw format :)
declare %private function tcsapi:getValueSetForExpansion($ref as xs:string, $flexibility as xs:string?) as element(valueSet) {
    let $valueSets      := vsapi:getValueSetByRef($ref, ($flexibility[string-length() gt 0], 'dynamic')[1])
    return
        switch (count($valueSets))
        case 0 return 
            error($errors:BAD_REQUEST, 'Cannot expand. Missing valueSet (inclusion) ''' || $ref || ''' ''' || ($flexibility[. castable as xs:dateTime], 'dynamic')[1] || '''')
        case 1 return
            tcsapi:getValueSetForExpansion($valueSets)
        default return
            error($errors:SERVER_ERROR, 'Cannot expand. Multiple (included) valueSets (' || count($valueSets) || ') found  for ''' || $ref || ''' ''' || ($flexibility[. castable as xs:dateTime], 'dynamic')[1] || '''. See your admin for solving this')
};
declare %private function tcsapi:getValueSetForExpansion($valueSet as element(valueSet)) as element(valueSet) {
    <valueSet>
    {
        $valueSet/(@* except @inode),
        if ($valueSet/@ident) then () else (
            attribute ident {($valueSet/ancestor::decor/project/@prefix)[1]}
        ),
        if ($valueSet/@url) then () else if ($valueSet/ancestor::*/@bbrurl) then
            attribute url   {($valueSet/ancestor::cacheme/@bbrurl)[1]}
        else (),
        attribute inode {
            concat(
                'collection(''', util:collection-name($valueSet), ''')//terminology/valueSet[@id=''', $valueSet/@id, '''][@effectiveDate=''', $valueSet/@effectiveDate, ''']'
            )
        },
        $valueSet/*
    }
    </valueSet>
};

(: resolve include/@ref value sets: all refs are replaced by their corresponding concepts, recursively :)
declare %private function tcsapi:resolveIncludeRefs($cp as element()*, $inode as xs:string, $sofar as element(valueSet)*) {
    for $c in $cp/*
    return
    if ($c[name() = 'include'][@ref]) then (
        let $ref  := $c/@ref
        let $flx  := $c/@flexibility
        let $xvs  := $sofar[@ref = $ref][@flexibility = string($flx)]
        
        let $xvs2 := if (empty($xvs)) then tcsapi:getValueSetForExpansion($ref, $flx) else ()
        return 
            if ($xvs)         then  <error type="valueset-recursion" ref="{$ref}" flexibility="{$flx}"/> else
            if (empty($xvs2)) then  <error type="valueset-not-found" ref="{$ref}" flexibility="{$flx}"/>
            else (
                tcsapi:resolveIncludeRefs($xvs2/conceptList, $xvs2/@inode, $sofar | <valueSet ref="{$ref}" flexibility="{$flx}"/>)
            )
    )
    else
        element { name($c) } {
            $c/@*,
            if (string-length($inode) > 0 ) then attribute { 'inode' } { $inode } else (),
            $c/*
        }
};

declare %private function tcsapi:copyExpansionList($nodes as element()*) {
    for $node in $nodes
    let $elname := name($node)
    return
        <items>
        {
             attribute { 'is' } { $elname },
             $node/(@* except @is),
             $node/node()
        }
        </items>
};

(: return the collection of queries to be executed for counting or exapnsion, wrapped by code system :)
declare %private function tcsapi:getExpansionQueryForCompose($compose as element()*)  {
    (: 
        first, create a new composition with all include/@ref replaced by their corresponding concepts, recursively
    :)
    let $newc := 
        for $x in $compose
        return
            <composition>
            {
                $x/@*,
                tcsapi:resolveIncludeRefs ($x, '', <valueSet ref="{$compose/@id}" flexibility="{$compose/@effectiveDate}"/>)
            }
            </composition>
    
    let $errors := $newc//error
    
    (: Question: why don't we include exception in this for loop? It means exceptions are never part of an expansion :)
    let $queries :=
        for $includes in $newc/(completeCodeSystem|include|concept|exception)
        (: get code system OID :)
        let $codeSystemOid    := $includes/@codeSystem
        let $codeSystemFlex   := $includes/@flexibility
        group by $codeSystemOid
        return (
            (:  for CADTS codesystems: get the name of the collection where the code system data reside
                for coded concepts from value sets use the inode parameter, ie direct path to value set
            :)
            let $browsableCodeSystem  := 
                if (empty($codeSystemFlex)) then 
                    collection($setlib:strCodesystemStableData)/browsableCodeSystem[@oid = $codeSystemOid]
                else if ($codeSystemFlex='dynamic') then
                    let $bCodeSys := collection($setlib:strCodesystemStableData)/browsableCodeSystem[@oid = $codeSystemOid]
                    let $latestDate := max($bCodeSys/xs:dateTime(@effectiveDate))
                    return
                    $bCodeSys[@effectiveDate=$latestDate]                 
                else (
                    collection($setlib:strCodesystemStableData)/browsableCodeSystem[@oid = $codeSystemOid][@effectiveDate = $codeSystemFlex]
                )
            let $check                := 
                switch (count($browsableCodeSystem))
                case 1 return ()
                case 0 return (
                    let $otherVersion := collection($setlib:strCodesystemStableData)/browsableCodeSystem[@oid = $codeSystemOid]
                    let $r            :=
                        if (empty($codeSystemFlex)) then
                            'Cannot expand. Missing codeSystem ' || $codeSystemOid
                        else (
                            'Cannot expand. Missing codeSystem ' || $codeSystemOid || ' flexibility ' || $codeSystemFlex
                        )
                    return
                        if ($otherVersion) then
                            error($errors:SERVER_ERROR, $r || ' in central terminology services (CADTS). We currently have version ' || string-join($otherVersion/@effectiveDate, ', ') || '. CADTS supports 1 version at a time.')
                        else (
                            error($errors:SERVER_ERROR, $r || ' in central terminology services (CADTS). If this is an inline definition that has no formal code system yet, please create that. If this an externally defined code system, you might be able to get this installed through your server administrator depending on license and availability.')
                        )
                )
                default return
                    error($errors:SERVER_ERROR, 'Cannot expand. Multiple codeSystems for ' || $codeSystemOid || ' in central terminology services (CADTS). This is a db error for an administrator to fix.')
                    
            let $collectionPath       := util:collection-name($browsableCodeSystem)
            (: Now that we write every codeSystem in its own collection, this is no longer needed :)
            (:let $codeSystemCount      := count(collection($collectionPath)/browsableCodeSystem):)
            
            for $include in $includes
            let $isException          := $include/name()='exception' or $include/@exception='true'
            (: matching excludes i.e. same codesystem, and optimize if current include is a simple code:
               only excludes that are not also a simple code, unless the exclude is for exactly the same include code
                
                This prevents things like [@code=('195967001')][not(@code=('54070000'))][not(@code=('54837006'))][not(@code=('726738003'))] based on
                <concept code="195967001" codeSystem="2.16.840.1.113883.6.96" displayName="Asthma (disorder)" level="0" type="S"/>
                <exclude code="54070000" codeSystem="2.16.840.1.113883.6.96" displayName="Postpartum education (procedure)"/>
                <exclude code="54837006" codeSystem="2.16.840.1.113883.6.96" displayName="Straight back syndrome (disorder)"/>
                <exclude code="726738003" codeSystem="2.16.840.1.113883.6.96" displayName="Cytology report (record artifact)"/>
                
            :)                       
            let $exclPredicates     := 
                  if ($include[name()=('concept', 'exception')][@code] | $include[name()=('include')][@code][@op = 'equal' or empty(@op)]) then
                      tcsapi:getPredicatesForIncludeExclude(
                          $compose/exclude/(.[@codeSystem=$codeSystemOid] except .[@code][@op = 'equal' or empty(@op)][not(@code = $include/@code)])
                      )
                  else (
                      tcsapi:getPredicatesForIncludeExclude($compose/exclude[@codeSystem=$codeSystemOid])
                  )
            return
                <query codeSystem="{$codeSystemOid}" collection="{$collectionPath}">
                {
                    if ($codeSystemFlex) then attribute codeSystemVersion {$codeSystemFlex} else ()
                }
                {
                    (: concepts with a @code, eg <concept code="112144000" codeSystem="2.16.840.1.113883.6.96" displayName="Blood group A (finding)" level="0" type="L"/> :)
                    if ($include[name()=('concept', 'exception')][@code = $newc/exclude[@codeSystem=$codeSystemOid]/@code]) then ()
                    else
                    if ($include[name()=('concept', 'exception')][@code]) then (
                        let $concepts         := $include/@code
                        let $conceptSelect    := if ($concepts) then '[@code=(''' || string-join($concepts,''',''') || ''')]' else()
                        let $ieconcept        := ($include/@code | $compose/exclude[@codeSystem=$codeSystemOid]/@code)
                        let $ieconceptSelect  := if ($ieconcept) then '[@code=(''' || string-join($ieconcept,''',''') || ''')]' else()
                        return
                            (
                                attribute exception { $isException }
                                ,
                                attribute { 'q' } {
                                    if ($include/@inode) then 
                                        $include/@inode || '//concept[@codeSystem=''' || $codeSystemOid || ''']' || $conceptSelect
                                    else (
                                        'collection(''' || $collectionPath || ''')//concept' || $conceptSelect || string-join($exclPredicates,'')
                                    )
                                },
                                (:
                                    query to count any code in include or exclude construct that is NOT active, for warnings on incomplete expansions.
                                    For "stable" CADTS codesystems only "draft", "active", "retired" and "experimental" are valid values anyway,
                                    add query for all nonactive ie not "retired"
                                :)
                                attribute { 'nonactive' } {
                                    if ($include/@inode) then 
                                        $include/@inode || '//concept[@statusCode=''' || 'retired' || '''][@codeSystem=''' || $codeSystemOid || ''']' || $conceptSelect
                                    else (
                                        'collection(''' || $collectionPath || ''')//concept[@statusCode=''' || 'retired' || ''']' || $conceptSelect
                                    )
                                }
                            )
                    )
                    (: includes with no @code and no @op but @codeSystem, eg <include codeSystem="1.2.276.0.76.5.533" flexibility="2022-04-01T00:00:00" displayName="KDL"/> or with filter
                       <include codeSystem="1.2.276.0.76.5.533" flexibility="2022-04-01T00:00:00" displayName="KDL">
                         <filter property="child" op="exist" value="false"/>
                       </include>
                    :)
                    else if ($include[name()='include'][empty(@op | @code)][@codeSystem]) then (
                        (: filters :)
                        let $predicates := tcsapi:getPredicatesForIncludeExclude($include)
                        return (
                            attribute exception { $isException }
                            ,
                            attribute { 'q' } { 'collection(''' || $collectionPath || ''')//concept' || string-join($predicates,'') },
                            (:
                                query to count any code in include or exclude construct that is NOT active, for warnings on incomplete expansions.
                                For "stable" CADTS codesystems only "draft", "active", "retired" and "experimental" are valid values anyway,
                                add query for all nonactive ie not "retired"
                            :)
                            attribute { 'nonactive' } { 'collection(''' || $collectionPath || ''')//concept' || string-join($predicates,'') || '[@statusCode=''' || 'retired' || ''']' }
                        )
                    (: includes with @op, @code and @codeSystem, eg <include op="is-a" code="49062001" codeSystem="2.16.840.1.113883.6.96" displayName="Device (physical object)"/> :)
                    )
                    else (
                        (: filters :)
                        let $predicates       := tcsapi:getPredicatesForIncludeExclude($include)
                        let $ieconcept        := ($include/@code | $compose/exclude[@codeSystem=$codeSystemOid]/@code)
                        let $ieconceptSelect  := if ($ieconcept) then '[@code=(''' || string-join($ieconcept,''',''') || ''')]' else()
                        return (
                            attribute exception { $isException }
                            ,
                            attribute { 'q' } { 
                                'collection(''' || $collectionPath || ''')//concept' || string-join($predicates,'') || string-join($exclPredicates,'')
                            },
                            (:  query to count any code in include or exclude construct that is NOT active, for warnings on incomplete expansions.
                                For "stable" CADTS codesystems only "draft", "active", "retired" and "experimental" are valid values anyway,
                                add query for all nonactive ie not "retired"
                            :)
                            attribute { 'nonactive' } { 
                                'collection(''' || $collectionPath || ''')//concept[@statusCode=''' || 'retired' || ''']' || $ieconceptSelect }
                        )
                    )
                }
                </query>
        )
    return
        <expansion>
        {
            $queries[@q],
            $errors
        }
        </expansion>
};

(: function to get all predicates for include and exclude ops :)
declare %private function tcsapi:getPredicatesForIncludeExclude($include as element()*) as xs:string* {
    let $doExclude  := $include/name() = 'exclude'
(:
    get predicates for following constructs
    include op code codeSystem
        with op = equal | is-a | descendent-of
        => results in [@code=, [ancSlf=, [ancestor=
    completeCodeSystem without filter => ()
    completeCodeSystem with filter property op value
        with property = child | parent (so far, any property of the code system in the future)
        with op = exist (so far, any valid op in the future)
        with value = true | false (so far, any valid string in the future)
        
        <property code="X">
            <valueString value="B"/>
        </property>
:)
(: <include codeSystem="2.16.840.1.113883.5.22" codeSystemName="v3-CodeSystem" flexibility="2019-03-20T00:00:00"/> :)
(: collection('/db/apps/terminology-data/codesystem-stable-data/external/hl7/CodeSystem')//concept[include--codeSystem-codeSystemName-flexibility-not-supported] :)
    for $filter in $include
    return (
        if ($filter/@op='equal' or $filter[empty(@op)][@code]) then
            if ($doExclude) then
                concat('[not(@code=(''',string-join($filter/@code,''','''),'''))]')
            else (
                concat('[@code=(''',string-join($filter/@code,''','''),''')]')
            )
        else
        if ($filter/@op='is-a') then
            if ($doExclude) then
                concat('[not(ancSlf=''',$filter/@code,''')]')
            else (
                concat('[ancSlf=''',$filter/@code,''']')
            )
        else
        if ($filter/@op='descendent-of') then
            if ($doExclude) then
                concat('[not(ancestor=''',$filter/@code,''')]')
            else (
                concat('[ancestor=''',$filter/@code,''']')
            )
        else
        if ($filter/@op='in') then
            if ($doExclude) then
                concat('[not(refsets/refset/@refsetId=''',$filter/@code,''')]')
            else (
                concat('[refsets/refset/@refsetId=''',$filter/@code,''']')
            )
        else
        if ($filter[empty(@op | @code)][@codeSystem]) then
            ()
        else
        if ($filter[filter]) then 
            ()
        else (
            '['|| $filter/name() ||'-' || $filter/@op || '-' || string-join(for $att in $filter/(@* except @op) order by name($att) return name($att), '-') || '-not-supported]'
        )
        ,
        for $ccsf in $filter/filter
        let $ccc := concat($ccsf/@property , '-', $ccsf/@op)
        return
            if ($ccc = 'child-exist' and $ccsf/@value = 'true') then
                if ($doExclude) then '[not(child)]' else '[child]'
            else
            if ($ccc = 'child-exist' and $ccsf/@value = 'false') then
                if ($doExclude) then '[child]' else '[not(child)]'
            else
            if ($ccc = 'parent-exist' and $ccsf/@value = 'true') then
                if ($doExclude) then '[not(parent)]' else '[parent]'
            else
            if ($ccc = 'parent-exist' and $ccsf/@value = 'false') then
                if ($doExclude) then '[parent]' else '[not(parent)]'
            else
            if ($ccsf/@op = 'equal') then
                if ($doExclude) then
                    concat('[not(property[@code=''',$ccsf/@property,'''][valueString/@value=''',$ccsf/@value,'''])]')
                else (
                    concat('[property[@code=''',$ccsf/@property,'''][valueString/@value=''',$ccsf/@value,''']]')
                )
            else ()
    )
};
(: function to expose all expanded concepts found, with the important attributes per concept, and all concept children :)
declare %private function tcsapi:cropc ($c as element()*) {
    for $cc in $c
    let $elmname  := $cc/../@type 
    let $csid     := $cc/../@codeSystem
    let $dp       := ($cc/designation[@use = 'fsn'], $cc/designation[@use = 'pref'], $cc/@displayName)[1]
    return
        element {$elmname}
        {
            $cc/@code,
            $csid,
            attribute displayName {$dp},
            attribute level {($cc/@level, 0)[1]},
            attribute type {($cc/@type, 'L')[1]},
            $cc/designation,
            $cc/desc
        }
};

(: private functions for other tasks
   =================
:)

declare %private function tcsapi:getCodeSystem($context as xs:string*, $loincContext as xs:string*, $codeSystemId as xs:string*, $searchLanguage as xs:string*, $searchString as xs:string*, $searchCode as xs:string*, $statusCode as xs:string*, $ancestors as xs:string*, $isa as xs:string*, $refsets as xs:string*) {

let $searchString       := if (not(empty($searchString))) then util:unescape-uri($searchString,'UTF-8') else ()
let $statusCodes        := tokenize(normalize-space($statusCode),'\s')
let $ancestors          := if (empty($ancestors)) then '' else tokenize(util:unescape-uri($ancestors,'UTF-8'),'\s')
let $refsets            := if (empty($refsets)) then '' else tokenize(util:unescape-uri($refsets,'UTF-8'),'\s')
let $maxResults         := xs:integer('50')

let $cs :=
   if (not(empty($searchString))) then
      adterm:searchDesignation($context, $loincContext, $codeSystemId, $searchLanguage, $searchString, $maxResults, $statusCodes, $ancestors, $isa, $refsets, ())
   else 
   if (not(empty($searchCode))) then
      adterm:searchCode($context,$codeSystemId, $searchCode,$maxResults, $statusCodes)
   else ()

return
    $cs
};

declare %private function tcsapi:getCodeSystemVersionInfo($codeSystemId as xs:string*) {

let $codeSystem := if (empty($codeSystemId)) then () else adterm:getCodeSystemInfo($codeSystemId)
let $license      := 
      <license>
      {
           (: for backward compatibility with older packages that have license.text instead of just license :)
           if ($codeSystem/license/text) then $codeSystem/license/text/node() else $codeSystem/license/node()
      }
      </license>
let $cs           := 
   <codeSystem>
   {
      $codeSystem/@*,
      $codeSystem/name,
      $codeSystem/desc,
      $codeSystem/language,
      $codeSystem/logo,
      $codeSystem/description,
      if ($license/node()) then utillib:serializeNode($license) else ()
   }
   </codeSystem>

return
    if (empty($cs)) then <rest:response><http:response status="404"/></rest:response> else $cs
};

declare %private function tcsapi:getConcept($codeSystemId as xs:string?, $code as xs:string?, $preferred as xs:string?, $language as xs:string?, $children as xs:string?) {

let $compose         := ()
let $cs := 
      if (not(empty($codeSystemId)) and not(empty($code))) then
         adterm:getConceptForLanguages($codeSystemId,$code,$preferred,$language,$children,$compose)
      else ()

return
    if (empty($cs)) then <rest:response><http:response status="404"/></rest:response> else $cs
};

declare %private function tcsapi:getConceptChildren($codeSystemId as xs:string?, $code as xs:string?, $preferred as xs:string?, $start as xs:string?, $length as xs:string?) {

let $compose         := ()
let $cs := 
      if (not(empty($codeSystemId)) and not(empty($code))) then
         adterm:getConceptChildrenForLanguage($codeSystemId,$code,$preferred,$start,$length,$compose)
      else ()

return
    if (empty($cs)) then <rest:response><http:response status="404"/></rest:response> else $cs
};

declare %private function tcsapi:getCodeSystemLanguages() {

let $languages   := distinct-values(collection($setlib:strCodesystemStableData)//browsableCodeSystem/language)

return
    <languages preferred="">
    {
        for $lang in $languages
        order by $lang
        return
        <language code="{$lang}">
            <preferred json:literal="true">false</preferred>
            <display json:literal="true">true</display>
            <search json:literal="true">false</search>
        </language>
    }
    </languages>
};

declare %private function tcsapi:getSnomedFilters() {

let $toplevels:= collection(concat($setlib:strCodesystemStableData,'/external/snomed/meta'))/topLevels
let $refsets:= doc(concat($setlib:strCodesystemStableData,'/external/snomed/meta/refsets.xml'))/refsets
let $refsetDisplay:= doc(concat($setlib:strCodesystemStableData,'/external/snomed/meta/refsetsDisplay.xml'))/refsets

return
   <filters>
   {
   <toplevels>
      {
      for $level in $toplevels/concept
      return
      <concept json:array="true">
         {
         $level/@*,
         for $desc in $level/desc
         let $languageCode :=
               if ($desc/@languageCode='nl') then 'nl-NL'
               else if ($desc/@languageCode='en' and $desc/@languageRefsetId='900000000000509007') then 'en-US'
               else $desc/@languageCode
         return
         <desc language="{$languageCode}" json:array="true">
            {
            $desc/text()
            }
         </desc>
         }
      </concept>
      }
   </toplevels>
   ,
   <refsets>
      {
      for $concept in $refsets/refset
      return
      <refset json:array="true">
         {
         $concept/@id,
         for $desc in $concept/desc
         let $languageCode :=
               if ($desc/@languageCode='nl') then 'nl-NL'
               else if ($desc/@languageCode='en' and $desc/@languageRefsetId='900000000000509007') then 'en-US'
               else $desc/@languageCode
         return
         <desc language="{$languageCode}" json:array="true">
            {
            $desc/text()
            }
         </desc>
         }
      </refset>
      }
   </refsets>
   ,
   <refsetDisplay>
      {
      for $concept in $refsetDisplay/refset
      return
      <refset json:array="true">
         {
         $concept/@id,
         for $desc in $concept/desc
         let $languageCode :=
               if ($desc/@languageCode='nl') then 'nl-NL'
               else if ($desc/@languageCode='en' and $desc/@languageRefsetId='900000000000509007') then 'en-US'
               else $desc/@languageCode
         return
         <desc language="{$languageCode}" json:array="true">
            {
            $desc/text()
            }
         </desc>
         }
      </refset>
      }
   </refsetDisplay>
   }
   </filters>
};

(: private functions for LOINC panel import
   ========================================
:)

(: Main function for creating concept group from panel, assemble value sets, create terminology associations :)

declare %private function tcsapi:createConceptFromPanel($authmap as map(*), $datasetId as xs:string, $datasetEffectiveDate as xs:string, $conceptBaseId as xs:string?, $insertMode as xs:string, $insertRef as xs:string?, $insertFlexibility as xs:string?, $panelCode as xs:string, $insertIntoDatabase as xs:boolean) {

    (: check if dataset not final or deprecated ? (security):)
    let $dataset                := utillib:getDataset($datasetId, $datasetEffectiveDate)
    let $decor                  := $dataset/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix
    let $loincPanel             := collection(concat($setlib:strCodesystemStableData,'/external/regenstrief'))//concept[@code=$panelCode]
      let $loincEffectiveDate     := if ($loincPanel/ancestor::browsableCodeSystem/@effectiveDate castable as xs:date) then
    format-dateTime(xs:dateTime(xs:date($loincPanel/ancestor::browsableCodeSystem/@effectiveDate)), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')
                                   else()
      
    (: get insert ref concept :)
    let $storedConcept          := 
        if ($insertFlexibility castable as xs:dateTime) then
            $dataset//concept[@id = $insertRef][@effectiveDate = $insertFlexibility][not(ancestor::history)]
        else 
            $dataset//concept[@id = $insertRef]
            
    (: check if dataset exists :)
    let $check                  :=
        if ($dataset) then () else (
            error($errors:BAD_REQUEST, 'Dataset with id ' || $datasetId || ' and effectiveDate ' || $datasetEffectiveDate || ' , does not exist. Cannot create in non-existent dataset.')
        )
        
    (: check if panel already in dataset :)
    let $check                   :=
         if ($decor//terminologyAssociation[@code=$panelCode]) then
            let $existingConceptAssociation := $decor//terminologyAssociation[@code=$panelCode]/@conceptId
            return
            if ($dataset//concept[@id=$existingConceptAssociation]) then
               error($errors:BAD_REQUEST, 'Panel with code ' || $panelCode || ' , already exists in the dataset')
            else ()
         else ()        
        
    let $check                  :=
        if ($storedConcept/ancestor-or-self::concept/@statusCode = $tcsapi:STATUSCODES-FINAL) then
            error($errors:BAD_REQUEST, concat('Concept cannot be added while it or any of its parents or the dataset itself have one of status: ', string-join($tcsapi:STATUSCODES-FINAL, ', ')))
        else ()
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-DATASETS)) then () else (
            error($errors:FORBIDDEN, 'User ' || $authmap?name || ' does not have sufficient permissions to add concepts to a dataset in project ' || $projectPrefix || '. You have to be an active author in the project.')
        )
    let $check                      :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-TERMINOLOGY)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify terminology in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )

    let $check                  :=
        switch ($insertMode)
        case 'into' 
        case 'following'
        case 'preceding' return ()
        default return (
            error($errors:BAD_REQUEST, 'Parameter insertMode ' || $insertMode || ' SHALL be one of ''into'', ''following'', or ''preceding''')
        )
    let $check                  :=
        if (empty($insertRef)) then 
            if ($insertMode = 'into') then () else (
                error($errors:BAD_REQUEST, 'Parameter insertRef SHALL have a value when insertMode is ' || $insertMode || '. Otherwise we don''t know where to insert')
            )
        else ()
    let $check                  :=
        if (empty($conceptBaseId)) then () else (
            let $conceptBaseIds         := decorlib:getBaseIdsP($decor, $decorlib:OBJECTTYPE-DATASETCONCEPT)
            return
                if ($conceptBaseIds[@id = $conceptBaseId]) then () else (
                    error($errors:BAD_REQUEST, 'Parameter conceptBaseId SHALL have a value that is declared as baseId with type DE (dataset concepts) in the project. Allowable are ' || string-join($conceptBaseIds/@id, ', '))
                )
        )

      let $check :=
            if (empty($loincPanel)) then 
               error($errors:BAD_REQUEST, 'LOINC code not found')
            else
               if (starts-with($loincPanel/loinc/class,'PANEL')) then ()
               else
                  error($errors:BAD_REQUEST, 'LOINC code is not a Panel')
               
    (: all basic check performed, now lock the dataset :)
    let $lock       := decorlib:setLock($authmap, $datasetId, $datasetEffectiveDate, false())

    (: Note: not protected from missing defaultBaseId for given type ... :)
    let $baseId           := if (string-length($conceptBaseId)=0) then decorlib:getDefaultBaseIdsP($decor, $decorlib:OBJECTTYPE-DATASETCONCEPT)/@id else ($conceptBaseId)
    let $newId            := decorlib:getNextAvailableIdP($decor, $decorlib:OBJECTTYPE-DATASETCONCEPT, $baseId)/@id
    let $newEffectiveDate := format-dateTime(current-dateTime(), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')
    let $datasetLanguages := distinct-values($decor/project/name/@language)
    let $newConceptId      := xs:integer(tokenize($newId, '\.')[last()])
    
    let $concept          :=
        <concept loincCode="{$loincPanel/@code}" id="{$newId}" type="group" statusCode="draft" effectiveDate="{$newEffectiveDate}" lastModifiedDate="{$newEffectiveDate}">
        {
            for $language in $datasetLanguages
            let $designation := if (string-length($loincPanel/designation[@lang=$language][@use='pref']) gt 0) then
                                 $loincPanel/designation[@lang=$language][@use='pref']
                                else
                                 $loincPanel/designation[@lang='en-US'][@use='pref']
            return
            <name language="{$language}">{$designation/text()}</name>
            ,
            for $language in $datasetLanguages
            let $designation := if (string-length($loincPanel/designation[@lang=$language][@use='pref']) gt 0) then
                                 $loincPanel/designation[@lang=$language][@use='pref']
                                else
                                 $loincPanel/designation[@lang='en-US'][@use='pref']
            return
            <desc language="{$language}">{$designation/text()}</desc>
            ,
            tcsapi:processChildConcepts($loincPanel,$newEffectiveDate,$datasetLanguages)
        }
        </concept>
    

    (: get list of unique value sets, check if value set already exists, create or reuse id's :)
    let $valueSetBaseId    := decorlib:getDefaultBaseIdsP($decor, $decorlib:OBJECTTYPE-VALUESET)/@id
    let $newValueSetId     := decorlib:getNextAvailableIdP($decor, $decorlib:OBJECTTYPE-VALUESET, $valueSetBaseId)/@id
    let $newValueSetEndId  := xs:integer(tokenize($newValueSetId, '\.')[last()])
    let $valueSetList := 
         for $set at $pos in distinct-values($concept//valueSet/@code)
         let $answerList := collection(concat($setlib:strValuesetStableData,'/external/regenstrief'))//valueSet[@code=$set]
         let $setId    := if ($decor//valueSet/relationship[@ref=$answerList/@id]) then
                              $decor//valueSet/relationship[@ref=$answerList/@id]/../@id
                          else
                              string-join(($valueSetBaseId, $newValueSetEndId + $pos - 1), '.')
         return
         <valueSet loincCode="{$set}" id="{$setId}">
         {
         if ($decor//valueSet/relationship[@ref=$answerList/@id]) then 
    attribute exists {''}
         else ()
         }
         </valueSet>

    (: create list of concepts with loinc code and new id, conceptlist with id, loinc code and valueset id :)
    let $conceptIdList :=
         (
         <concept loincCode="{$concept/@loincCode}" id="{$concept/@id}" displayName="{$concept/name[@language='en-US']/string()}"/>
         ,
        for $cpt at $pos in $concept//concept[not(parent::conceptList)]
        let $cptId := string-join(($baseId, $newConceptId + $pos), '.')
        (: let $existingAssociation := $decor//terminologyAssociation[@code=$cpt/@loincCode] :)
        return
        <concept loincCode="{$cpt/@loincCode}" id="{$cptId}" displayName="{$cpt/name[@language='en-US']/string()}">
         {
         (: if ($existingAssociation) then 
               attribute exists {''}
         else ()
         , :)
            if ($cpt/valueDomain/valueSet) then
            <conceptList id="{concat($cptId,'.0')}" loincCode="{$cpt/valueDomain/valueSet/@code}" valueSetId="{$valueSetList[@loincCode=$cpt/valueDomain/valueSet/@code]/@id}"/>
         else ()
         }
        </concept>
        )


    let $conceptForInsert :=
         <concept id="{$newId}" type="group" statusCode="draft" effectiveDate="{$newEffectiveDate}" lastModifiedDate="{$newEffectiveDate}">
        {
            for $language in $datasetLanguages
            let $designation :=  if (string-length($loincPanel/designation[@lang=$language][@use='pref']) gt 0) then
                                 $loincPanel/designation[@lang=$language][@use='pref']
                                else
                                 $loincPanel/designation[@lang='en-US'][@use='pref']
            return
            <name language="{$language}">{$designation/text()}</name>
            ,
            for $language in $datasetLanguages
            let $designation :=  if (string-length($loincPanel/designation[@lang=$language][@use='pref']) gt 0) then
                                 $loincPanel/designation[@lang=$language][@use='pref']
                                else
                                 $loincPanel/designation[@lang='en-US'][@use='pref']
            return
            <desc language="{$language}">{$designation/text()}</desc>
            ,
            tcsapi:processChildConceptsForInsert($concept,$conceptIdList,$decor)
        }
        </concept>
   
   let $valueSetsForInsert :=
      for $vset in $valueSetList[not(@exists)]
      let $answerList := collection(concat($setlib:strValuesetStableData,'/external/regenstrief'))//valueSet[@code=$vset/@loincCode]
      return
      <valueSet id="{$vset/@id}" effectiveDate="{$newEffectiveDate}" name="{replace($answerList/@name,'[^A-Za-z0-9_]','')}" displayName="{$answerList/@displayName}" statusCode="draft">
         <relationship type="DRIV" ref="{$answerList/@id}" flexibility="{$loincEffectiveDate}"/>
         {
         $answerList/publishingAuthority,
         $answerList/copyright
         }
         <conceptList>
         {
         for $listConcept in $answerList/conceptList/concept
         return
         <concept>
            {
            $listConcept/@*[not(name()='seqNo')]
            }
         </concept>
         }
         </conceptList>
      </valueSet>

    let $report :=
      <report concepts="{count($conceptIdList)}" valueSets="{count($valueSetList)}"/>
    
    let $conceptTerminologyAssociations :=
         for $item in $conceptIdList[not(@exists)]
         return
         <terminologyAssociation conceptId="{$item/@id}" effectiveDate="{$newEffectiveDate}" code="{$item/@loincCode}" codeSystem="2.16.840.1.113883.6.1" displayName="{$item/@displayName}" equivalence="equal"/>
    
    let $valueSetTerminologyAssociations :=
         for $item in $conceptIdList[not(@exists)]
         return
          if ($item/conceptList) then
            <terminologyAssociation conceptId="{$item/conceptList/@id}" effectiveDate="{$newEffectiveDate}" valueSet="{$item/conceptList/@valueSetId}" flexibility="dynamic" equivalence="equal"/>
         else ()
    
    let $insert           :=
      if ($insertIntoDatabase) then
      (
        if ($storedConcept) then
            if ($insertMode='following') then
                update insert $conceptForInsert following $storedConcept[last()]
            else
            if ($insertMode='preceding') then
                update insert $conceptForInsert preceding $storedConcept[1]
            else
            if ($storedConcept[history | concept]) then
                update insert $conceptForInsert preceding ($storedConcept/history | $storedConcept/concept)[1]
            else (
                update insert $conceptForInsert into $storedConcept
            )
        else
           if ($dataset[concept]) then 
               update insert $conceptForInsert following $dataset/concept[last()]
           else (
               update insert $conceptForInsert into $dataset
           )
         ,
         utillib:addTerminologyAssociations($conceptTerminologyAssociations | $valueSetTerminologyAssociations, $decor, false(), false())
         ,
         for $vset in $valueSetsForInsert
         return
         if ($decor/terminology/conceptMap) then
            update insert $vset preceding $decor/terminology/conceptMap[1] 
         else 
            update insert $vset into $decor/terminology
         )
      else ()
   (: all finished, clear dataset lock :)
   
   let $clearLock       := update delete $lock
return
   <response concepts="{count($conceptForInsert//concept)}" valueSets="{count($valueSetsForInsert)}" associations="{count($conceptForInsert//concept) + count($conceptForInsert//conceptList)}"/>

};


(: recursive function to process child concepts of LOINC panel to determine datatype and possible linked value sets :)
declare %private function tcsapi:processChildConcepts ($loincConcept as element(),$newEffectiveDate as xs:string,$datasetLanguages as xs:string*) {
   for $child in $loincConcept/child
   let $childConcept := collection(concat($setlib:strCodesystemStableData,'/external/regenstrief'))//concept[@code=$child/@cCode]
   let $conceptType  := if ($childConcept/child) then 'group' else 'item'
   return
   <concept loincCode="{$childConcept/@code}" id="" type="{$conceptType}" statusCode="draft" effectiveDate="{$newEffectiveDate}" lastModifiedDate="{$newEffectiveDate}">
      {
         for $language in $datasetLanguages
         let $designation :=   if (string-length($childConcept/designation[@lang=$language][@use='pref']) gt 0) then
                                 $childConcept/designation[@lang=$language][@use='pref']
                                else
                                 $childConcept/designation[@lang='en-US'][@use='pref']
         return
         (
         <name language="{$language}">{$designation/text()}</name>,
         <desc language="{$language}">{$designation/text()}</desc>,
         <operationalization language="{$language}">{$designation/text()}</operationalization>
         )
         ,
         if ($childConcept/child) then
            tcsapi:processChildConcepts($childConcept,$newEffectiveDate,$datasetLanguages)
         else
            (: check if there are multiple valuesets linked and select accordingly :)
            let $valueSetLink := if ($childConcept/valueSet) then
                                    if (count($childConcept/valueSet) gt 1) then
                                       if ($childConcept/valueSet/context/@contextCode=$loincConcept/@code) then
                                          $childConcept/valueSet[context/@contextCode=$loincConcept/@code]
                                       else
                                          $childConcept/valueSet[not(context)]
                                    else $childConcept/valueSet
                                 else()
            let $valueSet     := if ($valueSetLink) then
                                    collection(concat($setlib:strValuesetStableData,'/external/regenstrief'))//valueSet[@code=$valueSetLink/@id]
                                 else ()
            let $valueDomainType := if ($valueSet) then
                                       if ($valueSet//concept/@ordinal) then
                                          'ordinal'
                                       else
                                          'code'
                                    else
                                       if (string-length($childConcept/loinc/exUCUMunits) gt 0) then
                                          'quantity'
                                       else
                                          'string'
            return
            <valueDomain type="{$valueDomainType}">
               {
               if ($valueDomainType='quantity') then
                  let $unit := if (contains($childConcept/loinc/exUCUMunits,';')) then
                                 substring-before($childConcept/loinc/exUCUMunits,';')
                               else
                                 $childConcept/loinc/exUCUMunits
                  return
                  <property unit="{$unit}"></property>
               else ()
               ,
               $valueSet
               }
            </valueDomain>
         }
   </concept>
};

(: recursive function to process child concepts of panel for insert into database, removing all attributes and element not needed. :)
declare %private  function tcsapi:processChildConceptsForInsert ($concept as element(), $conceptIdList as element()*, $decor as element()) {
   for $child in $concept/concept
   let $childId := $conceptIdList[@loincCode=$child/@loincCode]/@id
   (: let $existingAssociation := $decor//terminologyAssociation[@code=$child/@loincCode] :)
   return
   <concept id="{$childId}" statusCode="draft" effectiveDate="{$child/@effectiveDate}" lastModifiedDate="{$child/@lastModifiedDate}">
      {  
         (: if ($existingAssociation) then
            <inherit ref="{$existingAssociation/@conceptId}" effectiveDate="{$existingAssociation/@effectiveDate}"/>
         else :)
            (
         attribute type {$child/@type},
         $child/name,
         $child/desc,
         if ($child/@type='item') then
            $child/operationalization
         else()
         ,
         if ($child/concept) then
            tcsapi:processChildConceptsForInsert($child,$conceptIdList,$decor)
         else
            <valueDomain type="{$child/valueDomain/@type}">
            {
            if ($child/valueDomain/valueSet) then
               <conceptList id="{concat($childId,'.0')}"/>
            else
               $child/valueDomain/property
            }
            </valueDomain>
          )
      }
   </concept>
};

(: function to check for existing terminology association where concept is available in project :)
declare %private function tcsapi:findConceptTerminologyAssociation($loincCode as xs:string, $decor as element()) as element() {
   let $associations := 
                     for $asc in $decor//terminologyAssociation[@code=$loincCode]
                     let $concept := 
                                    if ($asc/@conceptFlexibility) then
                                       $decor//concept[@id=$asc/conceptId][@effectiveDate=$asc/@conceptFlexibility]
                                    else
                                       $decor//concept[@id=$asc/conceptId]
                     return
                     if ($concept) then
                        $asc
                     else()
    return
      $associations[1]
                        
};

