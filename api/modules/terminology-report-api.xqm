xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:    
    enumeration:
      noAssociation              No terminology association defined
      noValuesetAssociated       No valueset associatiated with conceptlist
      noValuesetItem             No valueset item associated with concept in conceptlist
      noUcumUnit                       The unit of the value domain is not a valid UCUM essence unit
      conceptNotFound            Concept not found in codesystem
      codesystemNotfound         Codesystem not found on server
      valuesetNotFound           Valueset not found
      valueSetSizeNotConceptListSize   The linked value set is not of the same size as the concept list
      noMatchingDesignation      Display name does not match any designation in concept
      designationCaseMismatch    Case of display name does not match designation case in concept
      conceptRetired             Concept is retired
      conceptDraft               Concept is in draft
      conceptExperimental        Concept is experimental
      cannotParseExpression      Postcoordinated expression cannot be parsed
      equalSignNotAtStart        equal sign is not allowed at start of a postcoordinated code
      uriNotValidCode            URI is not a valid code
      errorInExpression          Error in Snomed expression
      notCoreModule                    Concept is not part of Snomed core module
      ok                         OK
      
   enumeration severity:
      info
      warning
      error
:)
(:~ Terminology Report API creates Terminology Reports for a Project (CADTS-aware) :)
module namespace trepapi            = "http://art-decor.org/ns/api/terminology/report";


import module namespace roaster     = "http://e-editiones.org/roaster";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

import module namespace utillib     = "http://art-decor.org/ns/api/util" at "library/util-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "library/settings-lib.xqm";
import module namespace decorlib    = "http://art-decor.org/ns/api/decor" at "library/decor-lib.xqm";
import module namespace adterm      = "http://art-decor.org/ns/terminology" at "../../terminology/api/api-terminology.xqm";

declare namespace json      = "http://www.json.org";
declare namespace rest      = "http://exquery.org/ns/restxq";
declare namespace resterr   = "http://exquery.org/ns/restxq/error";
declare namespace http      = "http://expath.org/ns/http-client";
declare namespace output    = "http://www.w3.org/2010/xslt-xquery-serialization";


declare %private variable $trepapi:PROGRESS-REPORT-10                := 'Checking request parameters ...';
declare %private variable $trepapi:PROGRESS-DATASET-20               := 'Checking datasets ...';
declare %private variable $trepapi:PROGRESS-SCENARIO-40              := 'Checking scenarios';
declare %private variable $trepapi:PROGRESS-VALUESET-60              := 'Checking valuesets ...';

declare %private variable $trepapi:supportedLanguages   := ('en-US','en-GB','nl-NL','de-DE','fr-FR','it-IT','pl-PL');


(:~ Get a terminology report (list) for a given project
    @param $projectPrefix     - required path part which may be its prefix or its oid
    @return as JSON
    @since 2022-11-30
:)
declare function trepapi:getTerminologyReport($request as map(*)) {
    
    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    let $specificref            := $request?parameters?reportId[string-length() gt 0]
    let $retrieveReport       := $request?parameters?retrieve = true()
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
        
    let $decor                  := if (utillib:isOid($project)) then utillib:getDecorById($project) else utillib:getDecorByPrefix($project)
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $project, ''', does not exist.'))
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-PROJECT)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to create a runtime version of project ', $project, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if (empty($specificref)) then () else if (matches($specificref, '^([1-9][0-9]*(\.[0-9]+)*)|([0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12})$', 'i')) then () else (
            error($errors:BAD_REQUEST, 'Parameter reportId SHALL be an oid or uuid. Found: ' || $specificref)
        )
    
    let $results                := trepapi:getTerminologyReport($projectPrefix,$specificref,$retrieveReport)
     
    return
        if (empty($results)) then 
            roaster:response(404, ())
        else (
            $results
        )
};

(:~ Create new terminology report and return the listing of terminology reports

@param $project - required. project prefix or id of the project to create the environment for
@return xml listing of terminology reports
:)
declare function trepapi:createTerminologyReportRequest($request as map(*)) {
    
    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    let $check                  :=
        if (empty($request?body)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
        
    let $decor                  := if (utillib:isOid($project)) then utillib:getDecorById($project) else utillib:getDecorByPrefix($project)
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $project, ''', does not exist.'))
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-PROJECT)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to create a runtime version of project ', $project, '. You have to be an active author in the project.'))
        )
    
    let $now                    := substring(xs:string(current-dateTime()), 1, 19)
    let $stamp                  := util:uuid()
    let $author                 := $decor/project/author[@username = $authmap?name]
    let $author                 := if (empty($author)) then $authmap?name else $author
    let $language               := $decor/project/@defaultLanguage
    let $timeStamp              := 'development'
    
    let $check                  :=
        if (count($projectPrefix) = 1) then () else (
            error($errors:BAD_REQUEST, 'Project parameter ' || $project || ' SHALL refer to exactly one project. Found: ' || count($decor))
        )
        
    
    let $terminology-report-request   :=   <terminology-report-request uuid="{$stamp}" for="{$projectPrefix}" on="{$now}" as="{$stamp}" by="{$author}" language="{$language}" progress="Added to process queue ..." progress-percentage="0"/>

    let $write                  := xmldb:store($setlib:strDecorScheduledTasks, $projectPrefix || replace($now, '\D', '') || '.xml', $terminology-report-request)
    let $tt                     := sm:chmod($write, 'rw-rw----')
    
    return
        trepapi:getTerminologyReport($projectPrefix, (),())
};

(:~ 
   Get list of terminology reports or specific report

   @param $project - required. project prefix or id of the project
   @return list of terminology reports or specific report
:)
declare function trepapi:getTerminologyReport($projectPrefix as xs:string*, $specificref as xs:string?, $retrieve as xs:boolean?) {
   if ($retrieve) then (
        let $fullReport := $setlib:colDecorVersion//terminologyReport[@for = $projectPrefix][@as = $specificref]
      return
      $fullReport
      )
    else (
        let $reportrequestF        :=
            if (string-length($specificref) > 0)
            then
                collection($setlib:strDecorScheduledTasks)/terminology-report-request[@for = $projectPrefix][@as = $specificref]
            else
                collection($setlib:strDecorScheduledTasks)/terminology-report-request[@for = $projectPrefix]
        let $terminologyReportF              :=
            if (string-length($specificref) > 0)
            then
                $setlib:colDecorVersion//terminologyReport[@for = $projectPrefix][@as = $specificref]
            else
                $setlib:colDecorVersion//terminologyReport[@for = $projectPrefix]
    
        let $count                  := count($terminologyReportF | $reportrequestF)
        
        let $terminologyReportF              :=
            for $c in $terminologyReportF
            let $as   := $c/@as
            group by $as
            order by $c/@on descending
            return $c[1]
        
        let $results                :=
            <list artifact="TERMINOLOGYREPORT" current="{$count}" total="{$count}" all="{$count}" lastModifiedDate="{current-dateTime()}">
            {
                for $c in $reportrequestF
                order by $c/@on descending
                return
                    element {name($c)}
                    {
                        $c/@*
                    }
                ,
                for $c at $count in $terminologyReportF
                return
                    <terminologyReport>
                    {
                        $c/@* except $c/@status,
                        if ($count < 3) then $c/@status else attribute {'status'} {'retired'},
                        $c/dir
                    }
                    </terminologyReport>
            }
            </list>
    
        for $result in $results
        return
            element {name($result)} {
                $result/@*,
                namespace {"json"} {"http://www.json.org"},
                utillib:addJsonArrayToElements($result/*)
            }
    )
};


(:~ Process terminology report requests :)
declare function trepapi:process-terminology-report-request($threadId as xs:string, $request as element(terminology-report-request)) {   
    let $xsltParameters     :=  <parameters></parameters>
    let $logsignature           := 'trepapi:process-terminology-report-request'
    let $mark-busy              := update insert attribute busy {'true'} into $request
    let $progress               := 
        if ($request/@progress) then (
            update value $request/@progress with $trepapi:PROGRESS-REPORT-10 
        )
        else (
            update insert attribute progress {$trepapi:PROGRESS-REPORT-10} into $request
        )
    let $progress               := 
        if ($request/@progress-percentage) then (
            update value $request/@progress-percentage with round((100 div 9) * 1) 
        )
        else (
            update insert attribute progress-percentage {round((100 div 9) * 1)} into $request
        )
    let $timeStamp              := current-dateTime()
    let $projectPrefix          := $request/@for[not(. = '*')]
    let $decor                  := if (empty($projectPrefix)) then () else utillib:getDecorByPrefix($projectPrefix)
    let $language               := $request/@language[not(. = '')]
    let $reportCollection       := concat($setlib:strDecorVersion,'/',substring($projectPrefix, 1, string-length($projectPrefix) - 1),'/development')

    return
        if (count($decor) = 1) then (
            update value $request/@progress with $trepapi:PROGRESS-DATASET-20,
            update value $request/@progress-percentage with round((100 div 9) * 2),
            let $terminologyReport := 
                <terminologyReport  for="{$request/@for}" on="{$request/@on}" as="{$request/@as}" by="{$request/@by}" status="active">
                    <datasets datasetCount="{count($decor/datasets/dataset)}">
                    {
                        trepapi:traverseDatasets($decor)
                    }
                    </datasets>
                {
                    update value $request/@progress with $trepapi:PROGRESS-SCENARIO-40,
                    update value $request/@progress-percentage with round((100 div 9) * 4)
                }
                    <transactions transactionCount="{count($decor/scenarios/scenario/transaction/transaction[representingTemplate][@statusCode=('draft','final','pending')])}">
                    {
                        (: datasets may have versions and concepts may have versions. we need to use both id and effectiveDate to get to the dataset. within a dataset there can be only 1 version of a concept :)
                        for $transaction in $decor/scenarios/scenario/transaction/transaction[representingTemplate][@statusCode=('draft','final','pending')]
                        let $ds  := if ($transaction/representingTemplate/@sourceDataset) then utillib:getDataset($transaction/representingTemplate/@sourceDataset, $transaction/representingTemplate/@sourceDatasetFlexibility) else ()
                        return
                        <transaction json:array="true">
                        {
                            $transaction/@*,
                            $transaction/name,
                            <representingTemplate conceptCount="{count($transaction/representingTemplate//concept)}">
                            {
                                $transaction/representingTemplate/@*
                            }
                                <dataset>
                                {
                                    $ds/name
                                }
                                </dataset>
                            {
                                for $concept in $transaction/representingTemplate/concept
                                return
                                    for $association in $concept/terminologyAssociation
                                        let $sourceconcept := ($ds//concept[@id=$concept/@ref])[1]
                                        return
                                            if ($association/@conceptId=$concept/@ref) then
                                    (: check concept association :)
                                      <concept json:array="true">
                                            {
                                                $sourceconcept/@*,
                                                $sourceconcept/name,
                                                trepapi:handleTerminologyAssociation($decor, $concept/terminologyAssociation)
                                            }
                                            </concept>
                                    else if ($association/@conceptId=$sourceconcept/conceptList/@id) then
                                       (: check conceptList association :)
                                       trepapi:handleConceptListAssociation($decor,$association)
                                    else if ($association/@conceptId=$sourceconcept/conceptList/concept/@id) then
                                       (: check conceptList concept associaation :)
                                       trepapi:handleTerminologyAssociation($decor,$association)
                                    else ()
                            
                            }
                            </representingTemplate>
                        }
                        </transaction>
                    }
                    </transactions>
                {
                    update value $request/@progress with $trepapi:PROGRESS-VALUESET-60,
                    update value $request/@progress-percentage with round((100 div 9) * 6)
                }
                    <valueSets valueSetCount="{count($decor/terminology/valueSet[@id])}">
                    {
                        trepapi:traverseValuesets($decor)   
                    }
                    </valueSets>
                    <ids>
                    {
                        let $codeSystemIds := distinct-values($decor/terminology/terminologyAssociation/@codeSystem | $decor/terminology/valueSet/conceptList/concept/@codeSystem)
                        let $idCheck :=
                            for $codeSystemId in $codeSystemIds
                            let $id := $decor/ids/id[@root = $codeSystemId]
                            return
                                <id>
                                {
                                    if ($id) then ($id/@*, $id/*) else (
                                        <message severity="warning" type="idRefMissing" codeSystem="{$codeSystemId}"></message>
                                    )
                                }
                                </id>
                        return
                            $idCheck
                    }
                    </ids>
                </terminologyReport>
            return (
                if (not(xmldb:collection-available($reportCollection))) then
                    try {
                        let $ttt    := xmldb:create-collection($setlib:strDecorVersion, concat(substring($projectPrefix, 1, string-length($projectPrefix) - 1),'/development'))
                        let $tt     := try { sm:chmod($ttt, 'rwxrwsr-x') } catch * {()}
                        return $ttt
                    } 
                    catch * {
                        <error>+++ Create collection failed '{concat(substring($projectPrefix, 1, string-length($projectPrefix) - 1),'/development')}': {$err:code}: {$err:description}</error>
                    }
                else()
                ,
                xmldb:store($reportCollection,concat($projectPrefix,'terminology-report-',$request/@as,'.xml'),transform:transform($terminologyReport ,xs:anyURI(concat('xmldb:exist://',$setlib:strApi ,'/resources/stylesheets/add-metadata-to-terminologyReport.xsl')), $xsltParameters))
                ,
                xmldb:remove(util:collection-name($request), util:document-name($request))
            )                
        )
        else (
            let $message                := 'ERROR. Found ' || count($decor) || ' projects for prefix ' || $projectPrefix || '. Expected: 1. Compile id: ' || $request/@as
            let $log                    := utillib:log-scheduler-event('error', $threadId, $logsignature, $message)
            let $progress               := update value $request/@progress with $message
            
            return ()
        )
    

};


(:~ Deletes terminology report

@param $project - required. project prefix or id of the project
@param $specificref - required. reference to the runtime environmnent
@return xml listing of environments
:)
declare function trepapi:deleteTerminologyReport($request as map(*)) {
    
    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    let $specificref            := $request?parameters?reportId[string-length() gt 0]
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    let $check                  :=
        if (matches($specificref, '^([1-9][0-9]*(\.[0-9]+)*)|([0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12})$', 'i')) then () else (
            error($errors:BAD_REQUEST, 'Parameter compilationId SHALL be an oid or uuid. Found: ' || $specificref)
        )
    
    let $decor                  := if (utillib:isOid($project)) then utillib:getDecorById($project) else utillib:getDecorByPrefix($project)
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if (count($projectPrefix) = 1) then () else (
            error($errors:BAD_REQUEST, 'Project parameter ' || $project || ' SHALL refer to exactly one project. Found: ' || count($decor))
        )
    
    let $part1                  := concat('xmldb:exist://', $setlib:strDecorVersion, '/', substring($projectPrefix, 1, string-length($projectPrefix) - 1), '/development/')
    let $tmp1                   := 
        if (xmldb:collection-available($part1)) then (
            if (doc-available(concat($part1, $projectPrefix,'terminology-report-', $specificref, '.xml'))) then xmldb:remove($part1, concat($projectPrefix,'terminology-report-', $specificref, '.xml')) else ()
        )
        else () 
    
    let $part1                  := collection($setlib:strDecorScheduledTasks)/terminology-report-request[@for = $projectPrefix][@as = $specificref]
    let $tmp3                   := if ($part1) then xmldb:remove(util:collection-name($part1), util:document-name($part1)) else ()
    
    return
        roaster:response(204, ())
};


(:~ Validate a given code against a codesystem
    @param $projectPrefix     - required path part which may be its prefix or its oid
    @return as JSON
    @since 2023-06-27
:)
declare function trepapi:validateCode($request as map(*)) {
    
    let $data                 := utillib:getBodyAsXml($request?body, (), ())
    let $codeSystemId         :=  $data/@codeSystemId
    let $code                 := $data/@code
    let $displayName          := $data/@displayName
   
    let $check                  :=
        if (empty($codeSystemId)) then () else if (matches($codeSystemId, '^([1-9][0-9]*(\.[0-9]+)*)|([0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12})$', 'i')) then () else (
            error($errors:BAD_REQUEST, 'Parameter codeSystemId SHALL be an oid or uuid. Found: ' || $codeSystemId)
        )
        
    let $check                  :=
        if (empty($code)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter code')
        else ()

    let $concept :=
         <concept code="{$code}" codeSystem="{$codeSystemId}" displayName="{$displayName}">
            {
            if (starts-with($code,'http')) then
               <message severity="error" type="uriNotValidCode" json:array="true">URI is not a valid code</message>
            else
            (: handle SNOMED post coordinated codes :)
                if ($codeSystemId='2.16.840.1.113883.6.96') then
                    if ($code castable as xs:integer) then
                        let $snomedConcept := collection($setlib:strCodesystemStableData)//concept[@code=$code][ancestor::browsableCodeSystem/@oid=$codeSystemId]
                        return
                        if ($snomedConcept) then
                            trepapi:checkValueSetConcept($displayName,$snomedConcept)
                        else
                            (: check if code system is present :)
                            if (collection($setlib:strCodesystemStableData)//browsableCodeSystem/@oid=$codeSystemId) then
                                <message severity="error" type="conceptNotFound" json:array="true"><codeSystem oid="{$codeSystemId}">{collection($setlib:strCodesystemStableData)//browsableCodeSystem[@oid=$codeSystemId]/name}</codeSystem></message>
                            else
                            <message severity="warning" type="codesystemNotfound" json:array="true">Codesystem not found</message>
                    else 
                        trepapi:checkSnomedExpression($code,$displayName)
                else
                     (: find code system first, local or cadts :)
                     if (collection($setlib:strCodesystemStableData)//browsableCodeSystem/@oid=$codeSystemId) then
                        let $codeSystemConcept := collection($setlib:strCodesystemStableData)//concept[@code=$code][ancestor::browsableCodeSystem/@oid=$codeSystemId]
                        return
                        if ($codeSystemConcept) then
                           trepapi:checkValueSetConcept($displayName,$codeSystemConcept)
                        else
                           <message severity="warning" type="conceptNotFound" json:array="true"><codeSystem oid="{$codeSystemId}">{collection($setlib:strCodesystemStableData)//browsableCodeSystem[@oid=$codeSystemId]/name}</codeSystem></message>
                     else if (collection($setlib:strDecorData)//codeSystem[@id=$codeSystemId]) then
                        let $localCodeSystemConcept := collection($setlib:strDecorData)//codedConcept[@code=$code]
                        return
                        if ($localCodeSystemConcept) then
                           let $localConcept :=
                              <concept>
                                 {
                                 $localCodeSystemConcept/@*,
                                 for $designation in $localCodeSystemConcept/designation
                                 return
                                 <designation lang="{$designation/@language}" type="{if ($designation/@type='preferred') then 'pref' else 'syn'}">{$designation/@displayName/string()}</designation>
                                 }
                              </concept>
                           return
                           trepapi:checkValueSetConcept($displayName,$localConcept)
                        else
                           <message severity="error" type="conceptNotFound" json:array="true"><codeSystem oid="{$codeSystemId}">{collection($setlib:strDecorData)//codeSystem[@id=$codeSystemId]/@displayName}</codeSystem></message>
                     else
                        <message severity="warning" type="codesystemNotfound" json:array="true">Codesystem not found</message>
            }
         </concept>
     
    return
        if (empty($concept)) then 
            roaster:response(404, ())
        else (
            $concept
        )
};


(:
    private functions
    =================
:)

(: Traverse datasets :)
declare %private function trepapi:traverseDatasets($decor as element()) as element()* {
   for $dataset in $decor/datasets/dataset[@statusCode=('draft','final','pending')]
   let $concepts := $dataset//concept[not(parent::conceptList)][@statusCode=('draft','final','pending')][not(inherit)]
   return
   <dataset conceptCount="{count($concepts)}" json:array="true">
   {
      $dataset/@*,
      $dataset/name,
      for $concept in $concepts
      let $terminologyAssociations := $decor/terminology/terminologyAssociation[@conceptId=$concept/@id]
      let $checkedConcept :=
         <concept json:array="true">
   {
      $concept/@*,
      $concept/name,
      if ($terminologyAssociations) then
         for $association in $terminologyAssociations
         return
         trepapi:handleTerminologyAssociation($decor,$association)
      else
      <message severity="info" type="noAssociation" json:array="true">No terminology association defined</message>
       ,
       if ($concept/valueDomain/conceptList) then
         let $conceptListAssociation:= $decor/terminology/terminologyAssociation[@conceptId=$concept/valueDomain/conceptList/@id]
         return
         <conceptList>
            {
            (
            if ($conceptListAssociation) then
               for $association in $conceptListAssociation
               return
               trepapi:handleConceptListAssociation($decor,$association)
            else
            <message severity="warning" type="noValuesetAssociated" json:array="true">No valueset associated with conceptlist</message>
            ,
            for $listConcept in $concept/valueDomain/conceptList/concept
               let $listConceptAssociations := $decor/terminology/terminologyAssociation[@conceptId=$listConcept/@id]
               return
               <listConcept>
               {
               $listConcept/@id,
               $listConcept/name,
               if($listConceptAssociations) then
                  for $association in $listConceptAssociations
                  return
                  trepapi:handleTerminologyAssociation($decor,$association)
               
               else
               <message severity="warning" type="noValuesetItem" json:array="true">No valueset item associated with concept in list</message>
             }
               </listConcept>
             )
             }
          </conceptList>
       else if ($concept/valueDomain/@type='quantity') then
               let $ucum := $setlib:colDecorCore/ucums/ucum
               for $valDomain in $concept/valueDomain[@type='quantity']
                  for $property in $valDomain/property
                  let $ucumCheck := $ucum[@unit=$property/@unit]
                  return
                  if (empty($ucumCheck)) then
                     <message severity="info" type="noUcumUnit" json:array="true">{$property/@unit}</message>
                  else ()
             else ()
    }  
   </concept>
    return
         <concept>
         {
         $checkedConcept/@*,
         $checkedConcept/name,
         if ($checkedConcept//message[not(@type='ok')]) then
            $checkedConcept/message[not(@type='ok')]
         else
            <message severity="info" type="ok" json:array="true">OK</message>
         ,
         $checkedConcept/conceptList
         }  
   </concept>
    }  
   </dataset>
};


(: handle terminology association :)
declare %private function trepapi:handleTerminologyAssociation($decor as element(),$terminologyAssociation as element()){
   (: check if codesystem is in project :)
   if (starts-with($terminologyAssociation/@codeSystem,$decor/project/@id)) then
      let $localConcept :=  $decor//codedConcept[@code=$terminologyAssociation/@code][ancestor::codeSystem/@id=$terminologyAssociation/@codeSystem]
      return
      if ($localConcept) then
         trepapi:checkConcept($terminologyAssociation,$localConcept)
      else
         (: check if code system is present :)
         if ($decor//codeSystem[@id=$terminologyAssociation/@codeSystem]) then
            <message severity="error" type="conceptNotFound" json:array="true">
            {
               $terminologyAssociation
            }
            </message>
         else
            let $codeSystemName := if ($terminologyAssociation/@codeSystemName) then $terminologyAssociation/@codeSystemName else '?'
            return
            <message severity="warning" type="codesystemNotfound" json:array="true">
            {
               $terminologyAssociation
            }
            </message>
   else
      (: handle SNOMED post coordinated codes :)
      if ($terminologyAssociation/@codeSystem='2.16.840.1.113883.6.96') then
         if ($terminologyAssociation/@code castable as xs:integer) then
            let $snomedConcept := collection($setlib:strCodesystemStableData)//concept[@code=$terminologyAssociation/@code][ancestor::browsableCodeSystem/@oid=$terminologyAssociation/@codeSystem]
            return
            if ($snomedConcept) then
               trepapi:checkConcept($terminologyAssociation,$snomedConcept)
            else
               (: check if code system is present :)
               if (collection($setlib:strCodesystemStableData)//browsableCodeSystem/@oid=$terminologyAssociation/@codeSystem) then
                  <message severity="error" type="conceptNotFound" json:array="true"><codeSystem                  oid="{$terminologyAssociation/@codeSystem}">{collection($setlib:strCodesystemStableData)//browsableCodeSystem[@oid=$terminologyAssociation/@codeSystem]/name}</codeSystem>
                  {
                     $terminologyAssociation
                  }
                  </message>
               else
                  <message severity="warning" type="codesystemNotfound" json:array="true">
                  {
                     $terminologyAssociation
                  }</message>
         else trepapi:checkAssociationSnomedExpression($terminologyAssociation/@code, $terminologyAssociation/@displayName)
         
      else
         let $codeSystemConcept := collection($setlib:strCodesystemStableData)//concept[@code=$terminologyAssociation/@code][ancestor::browsableCodeSystem/@oid=$terminologyAssociation/@codeSystem]
         return
         if ($codeSystemConcept) then
            trepapi:checkConcept($terminologyAssociation,$codeSystemConcept)
         else
            (: check if code system is present :)
            if (collection($setlib:strCodesystemStableData)//browsableCodeSystem/@oid=$terminologyAssociation/@codeSystem) then
               <message severity="error" type="conceptNotFound" json:array="true"><codeSystem               oid="{$terminologyAssociation/@codeSystem}">{collection($setlib:strCodesystemStableData)//browsableCodeSystem[@oid=$terminologyAssociation/@codeSystem]/name}</codeSystem>
               {
                  $terminologyAssociation
               }
               </message>
            else
               let $codeSystemName := if ($terminologyAssociation/@codeSystemName) then $terminologyAssociation/@codeSystemName else '?'
               return
               <message severity="warning" type="codesystemNotfound" json:array="true">
               {
                  $terminologyAssociation
               }
               </message>
};


(: handle conceptlist association :)
declare %private function trepapi:handleConceptListAssociation($decor as element(), $conceptListAssociation as element()) {
   (: check if valueset is in project :)
   if (starts-with($conceptListAssociation/@valueSet,$decor/project/@id)) then
      let $valueSet := 
               if ($conceptListAssociation/@flexibility='dynamic') then
                  let $vSet := $decor/terminology/valueSet[@id=$conceptListAssociation/@valueSet]
      let $latestDate := max($vSet/xs:dateTime(@effectiveDate))
                  return
      $vSet[@effectiveDate=$latestDate]
               else if ($conceptListAssociation/@flexibility castable as xs:dateTime) then
                  $decor/terminology/valueSet[@id=$conceptListAssociation/@valueSet][@effectiveDate=$conceptListAssociation/@flexibility]
               else ()
      return
      if ($valueSet) then
         (: check if conceptlist contains concepts and is of the same size as the valueset :)
         let $conceptList := $decor//conceptList[@id=$conceptListAssociation/@conceptId]
         return
         if ($conceptList/concept) then
            if (count($conceptList/concept) = count($valueSet//concept | $valueSet//exception)) then
               <message severity="info" type="ok" json:array="true"><valueSet displayName="{$valueSet/@displayName}" size="{count($valueSet//concept | $valueSet//exception)}"/></message>
      else
         <message severity="info" type="valueSetSizeNotConceptListSize" json:array="true"><conceptList size="{count($conceptList/concept)}"/><valueSet displayName="{$valueSet/@displayName}" size="{count($valueSet//concept | $valueSet//exception)}"/></message>
         else
            <message severity="info" type="ok" json:array="true">OK</message>
      else
         <message severity="error" type="valuesetNotFound" json:array="true">Valueset not found</message>
   else()

};


declare %private function trepapi:checkConcept($terminologyAssociation as element(), $concept as element()) as element()*{
   let $displayName := $terminologyAssociation/@displayName
   let $designations :=
         for $designation in $concept/designation[not(@use='rel')][@lang=$trepapi:supportedLanguages]
               order by $designation/@lang
               return
               <designation json:array="true">
                  {
                  $designation/@*,
                  $designation/text(),
                  if ($designation/*) then
                     let $parts := 
                     for $part in $designation/*
                     return
                     $part/text()
                     return
                     string-join($parts,' ')
                  else ()
                  }
               </designation>
   let $statusMessage :=
      if ($concept/@statusCode='retired') then
         <message severity="warning" type="conceptRetired" json:array="true">
            <concept code="{$concept/@code}">
               {
            for $designation in $concept/designation[@use='pref']
               return
               <designation json:array="true">
                  {
                  $designation/@*,
                  $designation/text()
                  }
               </designation>
               }
            </concept>
            {
            $terminologyAssociation,
            for $association in $concept/association
            return
            <association json:array="true">
               {
               $association/@*,
               $association/*
               }
            </association>
            }
         </message>
      else if ($concept/@statusCode='draft') then
         <message severity="warning" type="conceptDraft" json:array="true">
            {$terminologyAssociation}
         </message>
      else if ($concept/@statusCode='experimental') then
         <message severity="warning" type="conceptExperimental" json:array="true">
            {$terminologyAssociation}
         </message>
      else
         <message severity="info" type="ok" json:array="true">OK</message>
      
   let $designationMessage :=
      if ($displayName=$designations) then
            <message severity="info" type="ok" json:array="true">OK</message>
         else 
            let $lowerCase    := 
                                for $designation in $designations
                                return
            lower-case($designation)
            return
            if (lower-case($displayName)=$lowerCase) then
               <message severity="warning" type="designationCaseMismatch" json:array="true" displayName="{$displayName}">Case of display name does not match designation case in concept
               {
            $designations,
            $terminologyAssociation
 } </message>
               else
            <message severity="warning" type="noMatchingDesignation" json:array="true" displayName="{$displayName}">Display name does not match concept designation
               {
               $designations,
            $terminologyAssociation
            }
            </message>
            let $moduleMessage :=
      if ($concept/@moduleId) then
         if ($concept/@moduleId=('900000000000207008','900000000000012004')) then
            <message severity="info" type="ok" json:array="true">OK</message>
         else
            <message severity="info" type="notCoreModule" json:array="true">Concept is not part of core module</message>
      else ()
            
   return
               if ($statusMessage/@type='ok' and $designationMessage/@type='ok' and $moduleMessage/@type='ok') then
         <message severity="info" type="ok" json:array="true">OK</message>
      else
         ($statusMessage[not(@type='ok')],$designationMessage[not(@type='ok')],$moduleMessage[not(@type='ok')])
};

declare %private function trepapi:checkValueSetConcept($displayName as xs:string, $concept as element()) as element()*{
   let $designations :=
         for $designation in $concept/designation[not(@use='rel')][@lang=$trepapi:supportedLanguages]
               order by $designation/@lang
               return
   <designation json:array="true">
               {
                  $designation/@*,
                  $designation/text(),
               if               ($designation/*)   then
   let $parts := 
 for $part in $designation/*
 return
 $part/text()
 return
 string-join($parts,' ')
 else ()
   }
               </designation>
   let $statusMessage :=
      if ($concept/@statusCode='retired') then
         <message severity="warning" type="conceptRetired" json:array="true">
            <concept code="{$concept/@code}">
               {
            for $designation in $concept/designation[@use='pref']
               return
               <designation json:array="true">
                  {
                  $designation/@*,
                  $designation/text()
                  }
               </designation>
               }
            </concept>
            {
            for $association in $concept/association
            return
            <association json:array="true">
               {
               $association/@*,
               $association/*
               }
            </association>
            }
         </message>
      else if ($concept/@statusCode='draft') then
         <message severity="warning" type="conceptDraft" json:array="true">Concept is in draft</message>
      else if ($concept/@statusCode='experimental') then
         <message severity="warning" type="conceptExperimental" json:array="true">Concept is experimental</message>
      else
         <message severity="info" type="ok" json:array="true">OK</message>
   let $designationMessage :=
      if ($displayName=$concept/designation) then
            <message severity="info" type="ok" json:array="true">OK</message>
      else
         let $lowerCase    := 
                                for $designation in $designations
                                return
                                lower-case($designation)
            return
            if (lower-case($displayName)=$lowerCase) then
               <message severity="warning" type="designationCaseMismatch" displayName="{$displayName}" json:array="true">Case of display name does not match designation case in concept
               {
               $designations
               }
               </message>
            else
               <message severity="warning" type="noMatchingDesignation" displayName="{$displayName}" json:array="true">Display name does not match concept designation
               {
               $designations
               }
               </message>
   let $moduleMessage :=
      if ($concept/@moduleId) then
         if ($concept/@moduleId=('900000000000207008','900000000000012004')) then
            <message severity="info" type="ok" json:array="true">OK</message>
         else
            <message severity="info" type="notCoreModule" json:array="true">Concept is not part of core module</message>
      else()      
   return
 if ($statusMessage/@type='ok' and         $designationMessage/@type='ok' and $moduleMessage/@type='ok') then
         <message severity="info" type="ok"      json:array="true">OK</message>
   else
   ($statusMessage[not(@type='ok')],$designationMessage[not(@type='ok')],$moduleMessage[not(@type='ok')])
};


(: Traverse valuesets :)
declare %private function trepapi:traverseValuesets($decor as element()) as element()*{
   for $valueSet in $decor/terminology/valueSet[@id][@statusCode=('draft','final','pending')]
   let $sourceCodeSystems := 
            for $system in distinct-values($valueSet/conceptList/concept/@codeSystem)
            return
   <sourceCodeSystem id="{$system}">
               {
               (: check if codesystem is local :)
               if (starts-with($system,$decor/project/@id)) then
                  if ($decor//codeSystem[@id=$system]) then
                     <message severity="info" type="ok">OK</message>
                  else
                     <message severity="warning" type="codesystemNotfound">Code stystem not found</message>
               else
                  if (collection($setlib:strCodesystemStableData)//browsableCodeSystem/@oid=$system) then
                     <message severity="info" type="ok">OK</message>
                  else
                     <message severity="warning" type="codesystemNotfound">Code stystem not found</message>
               }
            </sourceCodeSystem>
   order by $valueSet/@displayName
   return
   <valueSet conceptCount="{count($valueSet/conceptList/concept)}" json:array="true">
      {
      $valueSet/@*,
      $sourceCodeSystems,
      for $concept in $valueSet/conceptList/concept
         return
         <concept json:array="true">
         {
         $concept/@*,
         $concept/desc,
         if (starts-with($concept/@code,'http')) then
            <message severity="error" type="uriNotValidCode" json:array="true">URI is not a valid code</message>
         (: check if codesystem is in project :)
         else if (starts-with($concept/@codeSystem,$decor/project/@id)) then
            let $localConcept :=  $decor//codedConcept[@code=$concept/@code][ancestor::codeSystem/@id=$concept/@codeSystem]
            return
            if ($localConcept) then
               trepapi:checkValueSetConcept($concept/@displayName,$localConcept)
            else
               (: check if code system is present :)
               if ($decor//codeSystem[@id=$concept/@codeSystem]) then
                  <message severity="error" type="conceptNotFound" json:array="true">Concept not found</message>
               else
                  let $codeSystemName := if ($concept/@codeSystemName) then $concept/@codeSystemName else '?'
                  return
                  <message severity="warning" type="codesystemNotfound" json:array="true">Codesystem not found</message>
         else
            (: handle SNOMED post coordinated codes :)
            if ($concept/@codeSystem='2.16.840.1.113883.6.96') then
               if ($concept/@code castable as xs:integer) then
                  let $snomedConcept := collection($setlib:strCodesystemStableData)//concept[@code=$concept/@code][ancestor::browsableCodeSystem/@oid=$concept/@codeSystem]
                  return
                  if ($snomedConcept) then
                     trepapi:checkValueSetConcept($concept/@displayName,$snomedConcept)
                  else
                     (: check if code system is present :)
                     if (collection($setlib:strCodesystemStableData)//browsableCodeSystem/@oid=$concept/@codeSystem) then
                        <message severity="error" type="conceptNotFound" json:array="true"><codeSystem oid="{$concept/@codeSystem}">{collection($setlib:strCodesystemStableData)//browsableCodeSystem[@oid=$concept/@codeSystem]/name}</codeSystem></message>
                     else
                        <message severity="warning" type="codesystemNotfound" json:array="true">Codesystem not found</message>
               else 
                  trepapi:checkSnomedExpression($concept/@code,$concept/@displayName)
               
            
                     else
               let $codeSystemConcept := collection($setlib:strCodesystemStableData)//concept[@code=$concept/@code][ancestor::browsableCodeSystem/@oid=$concept/@codeSystem]
               return
               if ($codeSystemConcept) then
                  trepapi:checkValueSetConcept($concept/@displayName,$codeSystemConcept)
               else
                  (: check if code system is present :)
                  if (collection($setlib:strCodesystemStableData)//browsableCodeSystem/@oid=$concept/@codeSystem) then
                     <message severity="error" type="conceptNotFound" json:array="true"><codeSystem oid="{$concept/@codeSystem}">{collection($setlib:strCodesystemStableData)//browsableCodeSystem[@oid=$concept/@codeSystem]/name}</codeSystem></message>
                  else
                     let $codeSystemName := if ($concept/@codeSystemName) then $concept/@codeSystemName else '?'
                     return
                     <message severity="warning" type="codesystemNotfound" json:array="true">Codesystem not found</message>
             }
            </concept>         
            }
   </valueSet>

};

(:
   Check SNOMED postcoordinated expression
   Removes terms from expression and calls parseSnomedExpression
:)

declare function trepapi:checkAssociationSnomedExpression ($expression as xs:string, $displayName as xs:string) {
   if (starts-with($expression,'=')) then
      <message severity="error" type="equalSignNotAtStart" json:array="true">equal sign is not allowed at start of a postcoordinated code</message>
   else if (starts-with($expression,'http')) then
      <message severity="error" type="uriNotValidCode" json:array="true">URI is not a valid code</message>
   else
      let $result := trepapi:parseSnomedExpression(replace($expression,'\|(.*?)\|',''))
   return
   if ($result//message or name($result[1])='message') then
      let $errorType :=
            if ($result//message) then
               $result//message[1]/@type
            else 
               $result[1]/@type
          return
         <message severity="error" type="errorInExpression" json:array="true">
         <expression string="{$expression}" displayName="{$displayName}">
            {
            $result
            }
         </expression>
      </message>
   else
      <message severity="info" type="ok" json:array="true">
         <expression string="{$expression}" displayName="{$displayName}">
            {
            $result
            }
         </expression>
      </message>
};

declare function trepapi:checkSnomedExpression ($expression as xs:string,$displayName as xs:string) {
   if (starts-with($expression,'=')) then
      <message severity="error" type="equalSignNotAtStart" json:array="true">equal sign is not allowed at start of a postcoordinated code</message>
   else if (starts-with($expression,'http')) then
      <message severity="error" type="uriNotValidCode" json:array="true">URI is not a valid code</message>
   else
      let $result := trepapi:parseSnomedExpression(replace($expression,'\|(.*?)\|',''))
   return
   if ($result//message or name($result[1])='message') then
      let $errorType :=
            if ($result//message) then
               $result//message[1]/@type
            else 
               $result[1]/@type
          return
         <message severity="error" type="errorInExpression" json:array="true">
      <expression string="{$expression}" displayName="{$displayName}">
            {
      $result
      }
      </expression>
      </message>
   else
      <message severity="info" type="ok" json:array="true">
      <expression string="{$expression}" displayName="{$displayName}">
            {
         $result
      }
      </expression>
      </message>
};

(:
   Recursive function for parsing Snomed expression.
   Returns xml structure of the expression with detected errors
:)

declare %private function trepapi:parseSnomedExpression ($expression as xs:string) {
   (: check if refset member :)
   if (starts-with(normalize-space($expression),'^')) then 
      if (normalize-space(substring-after($expression,'^')) castable as      xs:integer) then
         let $memberCode := normalize-space(substring-after($expression,'^'))
         return
         <memberOf>
            <concept code="{$memberCode}">{trepapi:getSnomedConceptDesignations($memberCode)}</concept>
         </memberOf>
      else
      <message severity="warning" type="cannotParseExpression" json:array="true">Expression cannot be parsed</message>
   (: check for multiple focus concepts :)
   else if (normalize-space(substring-before($expression,'+')) castable as      xs:integer) then
   let $focusCode := normalize-space(substring-before($expression,'+'))
 return
      (
      <concept code="{$focusCode}" json:array="true">{trepapi:getSnomedConceptDesignations($focusCode)}</concept>,
   <additionalFocus/>,
      trepapi:parseSnomedExpression(normalize-space(substring-after($expression,'+')))
   )
   else if (normalize-space(substring-before($expression,':')) castable as xs:integer) then
      let $conceptCode := normalize-space(substring-before($expression,':'))
      return
      (: check for attribute groups :)
      if (contains(substring-after($expression,':'),'{')) then
      let $attributeGroups := tokenize(substring-after($expression,':'),'\{')
         return
         (
         <concept code="{$conceptCode}" json:array="true">{trepapi:getSnomedConceptDesignations($conceptCode)}</concept>,
      for $group in $attributeGroups
      let $groupRefinements:= tokenize(substring-before($group,'}'),',')
            return
            if (string-length($group) > 3) then
      if (contains($group,'}')) then
   <group json:array="true">
                   {
                   for $refinement in $groupRefinements
                   return
                   if (normalize-space(substring-before($refinement,'=')) castable as xs:integer) then
      let $refinementCode := normalize-space(substring-before($refinement,'='))
      return
                   <refinement json:array="true">
                   <concept code="{$refinementCode}">{trepapi:getSnomedConceptDesignations($refinementCode)}</concept>
                      {
                      if (normalize-space(substring-after($refinement,'=')) castable as xs:integer) then
                         let $equalsCode :=normalize-space(substring-after($refinement,'='))
                         return
                         <equals code="{$equalsCode}">{trepapi:getSnomedConceptDesignations($equalsCode)}</equals>
                      else
                      <message severity="warning" type="cannotParseExpression" json:array="true">Expression cannot be parsed</message>
                      }
                   </refinement>
                   else <message severity="warning" type="cannotParseExpression" json:array="true">Expression cannot be parsed</message>
                   }
                   </group>
                else <message severity="warning" type="cannotParseExpression" json:array="true">Expression cannot be parsed</message>
             else ()
          )
      else
         let $refinements := tokenize(normalize-space(substring-after($expression,':')),',')
      return
      (
      <concept code="{$conceptCode}" json:array="true">{trepapi:getSnomedConceptDesignations($conceptCode)}</concept>,
         
         for $refinement in $refinements
         return
         if (normalize-space(substring-before($refinement,'=')) castable as xs:integer) then
         let $refinementCode := normalize-space(substring-before($refinement,'='))
         return
         <refinement json:array="true">
         <concept code="{$refinementCode}">{trepapi:getSnomedConceptDesignations($refinementCode)}</concept>
            {
            if (normalize-space(substring-after($refinement,'=')) castable as xs:integer) then
               let $equalsCode :=normalize-space(substring-after($refinement,'='))
               return
               <equals code="{$equalsCode}">{trepapi:getSnomedConceptDesignations($equalsCode)}</equals>
            else
            <message severity="warning" type="cannotParseExpression" json:array="true">Expression cannot be parsed</message>
            }
         </refinement>
         else <message severity="warning" type="cannotParseExpression" json:array="true">Expression cannot be parsed</message>
         )
   else if (normalize-space($expression) castable as xs:integer) then
      <concept code="{normalize-space($expression)}" json:array="true">{trepapi:getSnomedConceptDesignations($expression)}</concept>
      else
         <message severity="warning" type="cannotParseExpression" json:array="true">Expression cannot be parsed</message>
};

declare function trepapi:getSnomedConceptDesignations($code as xs:integer) as element()*{
   let $concept := collection(concat($setlib:strCodesystemStableData,'/external/snomed'))//concept[@code=$code]
   return
   if ($concept) then
       if ($concept/@statusCode='retired') then
         let $designations := $concept/designation
         return
         <message severity="warning" type="conceptRetired" json:array="true">
            <concept code="{$concept/@code}">
               {
            for $designation in $designations[@use='pref']
               return
               <designation json:array="true">
                  {
                  $designation/@*,
                  $designation/text()
                  }
               </designation>
               }
            </concept>
            {
            for $association in $concept/association
            return
            <association json:array="true">
               {
               $association/@*,
               $association/*
               }
            </association>
            }
         </message>
      else if ($concept/@statusCode='draft') then
         <message severity="warning" type="conceptDraft" json:array="true">Concept is in draft</message>
      else if ($concept/@statusCode='experimental') then
         <message severity="warning" type="conceptExperimental" json:array="true">Concept is experimental</message>
      else
  let $designations := $concept/designation
         return
         for $designation in $designations[@use='pref']
   order by $designation/@lang
         return
         <designation json:array="true">
            {
            $designation/@*,
            $designation/text()
            }
         </designation>
   else
      <message severity="error" type="conceptNotFound" json:array="true">Concept not found</message>
};
