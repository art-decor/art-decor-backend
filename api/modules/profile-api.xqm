xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

(:~ MyCommunity API allows read, create, update of community properties for a DECOR project :)
module namespace fpapi              = "http://art-decor.org/ns/api/profile";

import module namespace roaster     = "http://e-editiones.org/roaster";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "library/settings-lib.xqm";
import module namespace utillib     = "http://art-decor.org/ns/api/util" at "library/util-lib.xqm";
import module namespace decorlib    = "http://art-decor.org/ns/api/decor" at "library/decor-lib.xqm";

import module namespace tmapi       = "http://art-decor.org/ns/api/template" at "template-api.xqm";

declare namespace json      = "http://www.json.org";

(:~ Returns a list of zero or more profiles
    
    @param $projectPrefix    - optional. determines search scope. null is full server, pfx- limits scope to this project only
    @param $projectVersion   - optional. if empty defaults to current version. if valued then the profile will come explicitly from that archived project version which is expected to be a compiled version
    @param $id               - optional. Identifier of the profile to retrieve
    @param $name             - optional. Name of the profile to retrieve (valueSet/@name)
    @param $effectiveDate    - optional. null gets all versions, 'dynamic' gets the newest version based on id or name, yyyy-mm-ddThh:mm:ss gets this specific version
    @param $treetype         - optional. Default $tmapi:TREETYPELIMITEDMARKED
    @param $resolve          - optional. Boolean. Default true if governanceGroupId is empty. If true retrieves all versions for references, otherwise just returns the references.
    @return Zero value sets in case no matches are found, one if only one exists or if a specific version was requested, or more if more versions exist and no specific version was requested
    @since 2013-06-14
:)
declare function fpapi:getProfileList($request as map(*)) {
    let $governanceGroupId      := $request?parameters?governanceGroupId[not(. = '')]
    let $projectPrefix          := $request?parameters?prefix[not(. = '')]
    let $projectVersion         := $request?parameters?release[not(. = '')]
    let $projectLanguage        := $request?parameters?language[not(. = '')]
    let $searchTerms            := 
        array:flatten(
            for $s in $request?parameters?search[string-length() gt 0]
            return
                tokenize(lower-case($s),'\s')
        )
    let $id                     := $request?parameters?id[not(. = '')]
    let $nm                     := $request?parameters?name[not(. = '')]
    let $ed                     := $request?parameters?effectiveDate[not(. = '')]
    let $treetype               := $request?parameters?treetype
    let $resolve                :=  if (empty($governanceGroupId)) then not($request?parameters?resolve = false()) else $request?parameters?resolve = true()
    
    let $check                  :=
        if (count($governanceGroupId) + count($projectPrefix) = 0) then 
            error($errors:BAD_REQUEST, 'Request SHALL have either parameter governanceGroupId or prefix')
        else 
        if (count($governanceGroupId) + count($projectPrefix) gt 1) then 
            error($errors:BAD_REQUEST, 'Request SHALL have exactly one parameter governanceGroupId or prefix, not both or multiple')
        else
        if (count($governanceGroupId) gt 0 and not(empty($projectVersion))) then
            error($errors:BAD_REQUEST, 'Request SHALL NOT have both governanceGroupId and release of a single project')
        else
        if (count($governanceGroupId) gt 0 and not(empty($searchTerms))) then
            error($errors:BAD_REQUEST, 'Search only supported in a project')
        else ()

    let $results                :=
        tmapi:getTemplateList($governanceGroupId, $projectPrefix, $projectVersion, $projectLanguage, $searchTerms, $id, $ed, $treetype, $resolve)/*[@type = "fhirprofile"]

    let $countTN                := count($results//template/version)
    
    return
        <list artifact="FP" current="{$countTN}" total="{$countTN}" all="{$countTN}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements($results)
        }
        </list>
};