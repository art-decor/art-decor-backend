xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:
:   Library of DECOR administrative types functions. Most if not all functions are available in two flavors.
:   One based on project prefix and the other based on some element inside the project usually the decor
:   root element. It is the expectation that these function are sometimes called somewhere in the middle of 
:   other logic. Therefor you may be at any point in the decor project at the stage where to you need to call
:   a function in this library.
:   The $decor parameter is always used as $decor/ancestor-or-self::decor[project] to make sure we have the 
:   correct starting point.
:   Note: unlike Java, XQuery doesn't distinguish functions by the same based on signature of the parameters
:         so all functions that take $decor are postfixed with a P in the function name.
:)
module namespace decorlib       = "http://art-decor.org/ns/api/decor";

import module namespace errors          = "http://e-editiones.org/roaster/errors";

import module namespace userapi         = "http://art-decor.org/ns/api/user" at "../user-api.xqm";
import module namespace setlib          = "http://art-decor.org/ns/api/settings" at "settings-lib.xqm";
(:import module namespace utillib         = "http://art-decor.org/ns/api/util" at "util-lib.xqm";:)

declare namespace error         = "http://art-decor.org/ns/decor/error";
declare namespace sm            = "http://exist-db.org/xquery/securitymanager";
declare namespace xs            = "http://www.w3.org/2001/XMLSchema";

(:~ Legend:
:   Any guest/user always has read access
:
:   @email/         Not supported: handled by api-user-settings for now.
:   @notifier
:
:   @id             User id is an integer that increments with 1 for every user. The id is project unique and does not (necessarily) match across projects
:                           Used e.g. to bind issue authors/assignments to project authors
:
:   @username       Username that corresponds with his exist-db username
:
:   @active         User is active if there is a project/author[@username=$authmap?name][not(@active = 'false')]. Note that they might be deactivated at exist-db level
:                           Maps to: (author[@username=$authmap?name][empty(@active)] and $authmap?groups = 'decor')
:
:   @datasets       User has write access to datasets (and terminologyAssociations) if there is a project/author[@username=$authmap?name][not(@active = 'false')][not(@datasets = 'false')]
:                           Maps to: (author[@username=$authmap?name][empty(@active)] and $authmap?groups = 'decor')
:
:   @scenarios      User has write access to scenarios (and terminologyAssociations) if there is a project/author[@username=$authmap?name][not(@active = 'false')][not(@scenarios = 'false')]
:                           Maps to: (author[@username=$authmap?name][empty(@active)] and $authmap?groups = 'decor')
:
:   @terminology    User has write access to terminologyAssociations/valueSets/codeSystems/conceptMaps if there is a project/author[@username=$authmap?name][not(@active = 'false')][not(@terminology = 'false')]
:                           Maps to: (author[@username=$authmap?name][empty(@active)] and $authmap?groups = 'decor')
:
:   @rules          User has write access to templateAssociations/templates if there is a project/author[@username=$authmap?name][not(@active = 'false')][not(@rules = 'false')]
:                           Maps to: (author[@username=$authmap?name][empty(@active)] and $authmap?groups = 'decor')
:
:   @ids            User has write access to ids/id, but not to ids/(baseId|defaultBaseId) if there is a project/author[@username=$authmap?name][not(@active = 'false')][not(@ids = 'false')]
:                           Maps to: (author[@username=$authmap?name][empty(@active)] and $authmap?groups = 'decor')
:
:   @issues         User has write access to issues if there is a project/author[@username=$authmap?name][not(@active = 'false')][not(@issues = 'false')]
:                           Maps to: (author[@username=$authmap?name][empty(@active)] and $authmap?groups = 'decor' and $authmap?groups = 'issues')
:
:   @admin          User is admin if there is a project/author[@username=$authmap?name][not(@active = 'false')][not(@admin = 'false')]
:                   Admin users may edit project properties such as: decor/(@*), project, ids/(baseId|defaultBaseId), issues/@notifier
:                           Maps to: (author[@username=$authmap?name][empty(@active)] and $authmap?groups = 'decor' and $authmap?groups = 'decor-admin')
:
:   @effectiveDate  User has been added to this project since this date. This date is independent from being added as a db user
:
:   @expirationDate User has been deactivated in this project since this date. This date is independent from being deactivated or removed as a db user
:)
declare variable $decorlib:AUTHOR-TEMPLATE as element(author)  := 
    <author id="" username="" notifier="off" effectiveDate="{substring(string(current-dateTime()), 1, 19)}" expirationDate="" active="true" datasets="false" scenarios="false" terminology="false" rules="false" ids="false" issues="false" admin="false"/>;

(:~ These statics are needed for function decorlib:authorCanEditP($authmap, $decor, $sections). 
:   If you change something here, you need to update the function too.
:)
declare variable $decorlib:SECTION-PROJECT                     := 'project';
declare variable $decorlib:SECTION-DATASETS                    := 'datasets';
declare variable $decorlib:SECTION-IDS                         := 'ids';
declare variable $decorlib:SECTION-ISSUES                      := 'issues';
declare variable $decorlib:SECTION-RULES                       := 'rules';
declare variable $decorlib:SECTION-SCENARIOS                   := 'scenarios';
declare variable $decorlib:SECTION-TERMINOLOGY                 := 'terminology';
declare variable $decorlib:SECTIONS-ALL as xs:string+          := ($decorlib:SECTION-PROJECT,
                                                                $decorlib:SECTION-DATASETS,
                                                                $decorlib:SECTION-IDS,
                                                                $decorlib:SECTION-ISSUES,
                                                                $decorlib:SECTION-RULES,
                                                                $decorlib:SECTION-SCENARIOS,
                                                                $decorlib:SECTION-TERMINOLOGY);

declare variable $decorlib:OBJECTTYPE-DATASET                  := 'DS';
declare variable $decorlib:OBJECTTYPE-DATASETCONCEPT           := 'DE';
declare variable $decorlib:OBJECTTYPE-DATASETCONCEPTLIST       := 'CL';
declare variable $decorlib:OBJECTTYPE-SCENARIO                 := 'SC';
declare variable $decorlib:OBJECTTYPE-TRANSACTION              := 'TR';
declare variable $decorlib:OBJECTTYPE-ACTOR                    := 'AC';
declare variable $decorlib:OBJECTTYPE-VALUESET                 := 'VS';
declare variable $decorlib:OBJECTTYPE-ISSUE                    := 'IS';
(:declare variable $decorlib:OBJECTTYPE-RULE                     := 'RL';:)
declare variable $decorlib:OBJECTTYPE-TEMPLATE                 := 'TM';
declare variable $decorlib:OBJECTTYPE-TEMPLATEELEMENT          := 'EL';
(:declare variable $decorlib:OBJECTTYPE-TESTSCENARIO             := 'SX';
declare variable $decorlib:OBJECTTYPE-TESTTRANSACTION          := 'TX';
declare variable $decorlib:OBJECTTYPE-EXAMPLETRANSACTION       := 'EX';
declare variable $decorlib:OBJECTTYPE-TESTREQUIREMENT          := 'EX';:)
declare variable $decorlib:OBJECTTYPE-COMMUNITY                := 'CM';
declare variable $decorlib:OBJECTTYPE-CODESYSTEM               := 'CS';
declare variable $decorlib:OBJECTTYPE-MAPPING                  := 'MP';
declare variable $decorlib:OBJECTTYPE-QUESTIONNAIRE            := 'QQ';
declare variable $decorlib:OBJECTTYPE-QUESTIONNAIRERESPONSE    := 'QR';
declare variable $decorlib:OBJECTTYPE-IMPLEMENTATIONGUIDE      := 'IG';
declare variable $decorlib:OBJECTTYPES-ALL as xs:string*       := ($decorlib:OBJECTTYPE-DATASET ,
                                                                $decorlib:OBJECTTYPE-DATASETCONCEPT ,
                                                                (:$decorlib:OBJECTTYPE-DATASETCONCEPTLIST ,:)
                                                                $decorlib:OBJECTTYPE-SCENARIO ,
                                                                $decorlib:OBJECTTYPE-TRANSACTION ,
                                                                $decorlib:OBJECTTYPE-ACTOR ,
                                                                $decorlib:OBJECTTYPE-VALUESET ,
                                                                $decorlib:OBJECTTYPE-ISSUE ,
                                                                (:$decorlib:OBJECTTYPE-RULE ,:)
                                                                $decorlib:OBJECTTYPE-TEMPLATE ,
                                                                (:$decorlib:OBJECTTYPE-TEMPLATEELEMENT ,
                                                                $decorlib:OBJECTTYPE-TESTSCENARIO ,
                                                                $decorlib:OBJECTTYPE-TESTTRANSACTION ,
                                                                $decorlib:OBJECTTYPE-EXAMPLETRANSACTION ,
                                                                $decorlib:OBJECTTYPE-TESTREQUIREMENT ,:)
                                                                $decorlib:OBJECTTYPE-COMMUNITY ,
                                                                $decorlib:OBJECTTYPE-CODESYSTEM,
                                                                $decorlib:OBJECTTYPE-MAPPING,
                                                                $decorlib:OBJECTTYPE-QUESTIONNAIRE,
                                                                $decorlib:OBJECTTYPE-QUESTIONNAIRERESPONSE,
                                                                $decorlib:OBJECTTYPE-IMPLEMENTATIONGUIDE);

declare variable $decorlib:SECTION_TYPE_MAPPING                :=
    <maps>
        <map type="{$decorlib:OBJECTTYPE-DATASET}" section="{$decorlib:SECTION-DATASETS}"/>
        <map type="{$decorlib:OBJECTTYPE-DATASETCONCEPT}" section="{$decorlib:SECTION-DATASETS}"/>
        <!--<map type="{$decorlib:OBJECTTYPE-DATASETCONCEPTLIST}" section="{$decorlib:SECTION-DATASETS}"/>-->
        <map type="{$decorlib:OBJECTTYPE-SCENARIO}" section="{$decorlib:SECTION-SCENARIOS}"/>
        <map type="{$decorlib:OBJECTTYPE-TRANSACTION}" section="{$decorlib:SECTION-SCENARIOS}"/>
        <map type="{$decorlib:OBJECTTYPE-ACTOR}" section="{$decorlib:SECTION-SCENARIOS}"/>
        <map type="{$decorlib:OBJECTTYPE-VALUESET}" section="{$decorlib:SECTION-TERMINOLOGY}"/>
        <map type="{$decorlib:OBJECTTYPE-ISSUE}" section="{$decorlib:SECTION-ISSUES}"/>
        <!--<map type="{$decorlib:OBJECTTYPE-RULE}" section="{$decorlib:SECTION-RULES}"/>-->
        <map type="{$decorlib:OBJECTTYPE-TEMPLATE}" section="{$decorlib:SECTION-RULES}"/>
        <!--<map type="{$decorlib:OBJECTTYPE-TEMPLATEELEMENT}" section="{$decorlib:SECTION-RULES}"/>-->
        <!--<map type="{$decorlib:OBJECTTYPE-TESTSCENARIO}" section=""/>-->
        <!--<map type="{$decorlib:OBJECTTYPE-TESTTRANSACTION}" section=""/>-->
        <!--<map type="{$decorlib:OBJECTTYPE-EXAMPLETRANSACTION}" section=""/>-->
        <!--<map type="{$decorlib:OBJECTTYPE-TESTREQUIREMENT}" section=""/>-->
        <map type="{$decorlib:OBJECTTYPE-COMMUNITY}" section=""/>
        <map type="{$decorlib:OBJECTTYPE-CODESYSTEM}" section="{$decorlib:SECTION-TERMINOLOGY}"/>
        <map type="{$decorlib:OBJECTTYPE-MAPPING}" section="{$decorlib:SECTION-TERMINOLOGY}"/>
        <map type="{$decorlib:OBJECTTYPE-QUESTIONNAIRE}" section="{$decorlib:SECTION-SCENARIOS}"/>
        <map type="{$decorlib:OBJECTTYPE-QUESTIONNAIRERESPONSE}" section="{$decorlib:SECTION-SCENARIOS}"/>
        <map type="{$decorlib:OBJECTTYPE-IMPLEMENTATIONGUIDE}" section="{$decorlib:SECTION-SCENARIOS}"/>
    </maps>;

(: ======== GENERAL/READ FUNCTIONS ======== :)

(:~ Return full project contents based on $prefix or error(). If no project is found this is not an error, just a fact. :)
declare function decorlib:getDecorProject($prefix as xs:string) as element(decor)? {
    let $decor      := if (matches($prefix, '^\d+(\.\d+)*$')) then $setlib:colDecorData/decor[project[@id = $prefix]] else $setlib:colDecorData/decor[project[@prefix = $prefix]]
    
    return
    switch (count($decor))
    case 0 (: fall through :)
    case 1 return $decor
    default return error($errors:SERVER_ERROR,'Project ''' || $prefix || ''' has multiple hits (' || count($decor) || '). Contact your server administrator as this should not be possible.') 
};

(:~ Return full project contents based on setlib:strCurrentUserName() or error()
:)
declare function decorlib:getDecorProjectsForCurrentUser($authmap as map(*)) as element(decor)* {
    for $decor in $setlib:colDecorData/decor
    return
        if (decorlib:isActiveAuthorP($authmap, $decor)) then $decor else ()
};

(:~ Returns boolean true|false whether or not the given project exists :)
declare function decorlib:isProject($prefix as xs:string) as xs:boolean {
    exists(decorlib:getDecorProject($prefix))
};

(:~ Returns boolean true|false whether or not the given project is a repository/BBR :)
declare function decorlib:isRepository($prefix as xs:string) as xs:boolean {
    decorlib:isRepositoryP(decorlib:getDecorProject($prefix))
};

(:~ Returns boolean true|false whether or not the given project is a private project :)
declare function decorlib:isRepositoryP($decor as element()) as xs:boolean {
    $decor/ancestor-or-self::decor/@repository = 'true'
};

(:~ Returns boolean true|false whether or not the given project is a private project :)
declare function decorlib:isPrivate($prefix as xs:string) as xs:boolean {
    decorlib:isPrivateP(decorlib:getDecorProject($prefix))
};

(:~ Returns boolean true|false whether or not the given project is a private project :)
declare function decorlib:isPrivateP($decor as element()) as xs:boolean {
    $decor/ancestor-or-self::decor/@private ='true'
};

(:~ Returns boolean true|false whether or not the given project is an experimental project :)
declare function decorlib:isExperimental($prefix as xs:string) as xs:boolean {
    decorlib:isExperimentalP(decorlib:getDecorProject($prefix))
};

(:~ Returns boolean true|false whether or not the given project is an experimental project :)
declare function decorlib:isExperimentalP($decor as element()) as xs:boolean {
    $decor/ancestor-or-self::decor/project/@experimental = 'true'
};

(:~ Returns all authors from a project. Note that this does not take the exist-db level into account :)
declare function decorlib:getAuthors($prefix as xs:string) as xs:string* {
    decorlib:getAuthorsP(decorlib:getDecorProject($prefix))
};

(:~ Returns all authors from a project. Note that this does not take the exist-db level into account :)
declare function decorlib:getAuthorsP($decor as element()) as xs:string* {
    $decor/ancestor-or-self::decor/project/author/@username
};

(:~ Returns all active authors from a project. Note that this does not take the exist-db level into account :)
declare function decorlib:getActiveAuthors($prefix as xs:string) as xs:string* {
    decorlib:getActiveAuthorsP(decorlib:getDecorProject($prefix))
};

(:~ Returns all active authors from a project. Note that this does not take the exist-db level into account :)
declare function decorlib:getActiveAuthorsP($decor as element()) as xs:string* {
    $decor/ancestor-or-self::decor/project/author[not(@active = 'false')]/@username
};

(:~ Returns all inactive authors from a project. Note that this does not take the exist-db level into account :)
declare function decorlib:getInactiveAuthors($prefix as xs:string) as xs:string* {
    decorlib:getInactiveAuthorsP(decorlib:getDecorProject($prefix))
};

(:~ Returns all inactive authors from a project. Note that this does not take the exist-db level into account :)
declare function decorlib:getInactiveAuthorsP($decor as element()) as xs:string* {
    $decor/ancestor-or-self::decor/project/author[@active = 'false']/@username
};

(:~ Returns boolean true|false whether or not the given user is active+admin in the given project. Note that they might be deactivated at exist-db level :)
declare function decorlib:isProjectAdmin($authmap as map(*), $prefix as xs:string) as xs:boolean {
    decorlib:isProjectAdminP($authmap, decorlib:getDecorProject($prefix))
};

(:~ Returns boolean true|false whether or not the given user is active+admin in the given project. Note that they might be deactivated at exist-db level :)
declare function decorlib:isProjectAdminP($authmap as map(*), $decor as element()) as xs:boolean {
    let $author   := $decor/ancestor-or-self::decor/project/author[@username = $authmap?name][not(@active = 'false')]
    return
        exists($author[@admin = 'true'] | $author['decor-admin' = $authmap?groups])
};

(:~ Returns boolean true|false whether or not the given user is author in the given project. Note that they might be inactive and/or deactivated at exist-db level :)
declare function decorlib:isAuthor($authmap as map(*), $prefix as xs:string) as xs:boolean {
    decorlib:isAuthorP($authmap, decorlib:getDecorProject($prefix))
};

(:~ Returns boolean true|false whether or not the given author is active in the given project. Note that they might be inactive and/or deactivated at exist-db level :)
declare function decorlib:isAuthorP($authmap as map(*), $decor as element()) as xs:boolean {
    decorlib:getAuthorsP($decor) = $authmap?name
};

(:~ Returns boolean true|false whether or not the given user is active in the given project. Note that they might be deactivated at exist-db level :)
declare function decorlib:isActiveAuthor($authmap as map(*), $prefix as xs:string) as xs:boolean {
    decorlib:isActiveAuthorP($authmap, decorlib:getDecorProject($prefix))
};

(:~ Returns boolean true|false whether or not the given user is active in the given project. Note that they might be deactivated at exist-db level :)
declare function decorlib:isActiveAuthorP($authmap as map(*), $decor as element()) as xs:boolean {
    decorlib:getActiveAuthorsP($decor) = $authmap?name
};

(:~ Returns boolean true|false whether or not the current user is active in the given project and can edit ALL of the indicated sections.
:   Note that they might be deactivated at exist-db level :)
declare function decorlib:authorCanEdit($authmap as map(*), $prefix as xs:string, $sections as xs:string+) as xs:boolean {
    decorlib:authorCanEditP($authmap, decorlib:getDecorProject($prefix), $sections)
};

(:~ Returns boolean true|false whether or not the current user is active in the given project and can edit ALL of the indicated sections.
:   Note that they might be deactivated at exist-db level :)
declare function decorlib:authorCanEditP($authmap as map(*), $decor as element(), $sections as xs:string+) as xs:boolean {
    let $check          :=
        if ($sections[not(. = $decorlib:SECTIONS-ALL)]) then
            error($errors:BAD_REQUEST, 'You have at least one unsupported section: ' || string-join($sections[not(.= $decorlib:SECTIONS-ALL)], ' '))
        else ()

    let $decor          := $decor/ancestor-or-self::decor[project]
    let $activeAuthor   := decorlib:getActiveAuthor($decor, $authmap?name)
    (: if someone is an author then these permissions prevail so projects can disallow access for a user regardless of his dba status :)
    let $isDbaNotAuthor := empty(decorlib:getAuthor($decor, $authmap?name)) and $authmap?groups = 'dba'
    (:decor-admin may not need to be a group at some point when all author properties are stored in the decor files:)
    let $isDecorAdmin   := ($authmap?groups = ('decor-admin', 'dba'))
    (:editor and issues may not need to be a group at some point when all author properties are stored in the decor files:)
    let $isEditor       := ($authmap?groups = ('decor', 'editor', 'dba'))
    (:any editor may edit issues:)
    let $isIssueEditor  := $isEditor or ($authmap?groups = 'decor' and $authmap?groups = 'issues')
    
    let $return :=
        if ($activeAuthor[@active]) then (
            for $section in $sections
            return
                switch ($section) 
                case $decorlib:SECTION-PROJECT      return exists($activeAuthor[not(@admin = 'false')])
                case $decorlib:SECTION-DATASETS     return exists($activeAuthor[not(@datasets = 'false')])
                case $decorlib:SECTION-IDS          return exists($activeAuthor[not(@ids = 'false')])
                case $decorlib:SECTION-ISSUES       return exists($activeAuthor[not(@issues = 'false')])
                case $decorlib:SECTION-RULES        return exists($activeAuthor[not(@rules = 'false')])
                case $decorlib:SECTION-SCENARIOS    return exists($activeAuthor[not(@scenarios = 'false')])
                case $decorlib:SECTION-TERMINOLOGY  return exists($activeAuthor[not(@terminology = 'false')])
                (:default would mean some section we don't support, but that would be impossible at this point:)
                default                             return false()
        )
        else
        if ($activeAuthor) then (
            for $section in $sections
            return
                switch ($section) 
                case $decorlib:SECTION-PROJECT      return exists($activeAuthor[$isDecorAdmin][$isEditor])
                case $decorlib:SECTION-DATASETS     return exists($activeAuthor[$isEditor])
                case $decorlib:SECTION-IDS          return exists($activeAuthor[$isEditor])
                case $decorlib:SECTION-ISSUES       return exists($activeAuthor[$isIssueEditor])
                case $decorlib:SECTION-RULES        return exists($activeAuthor[$isEditor])
                case $decorlib:SECTION-SCENARIOS    return exists($activeAuthor[$isEditor])
                case $decorlib:SECTION-TERMINOLOGY  return exists($activeAuthor[$isEditor])
                (:default would mean some section we don't support, but that would be impossible at this point:)
                default                             return false()
        )
        else if ($isDbaNotAuthor) then
            (: apparently the user was not an author in the project but since he is a dba, he should have access :)
            true()
        else (
            false()
        )
    
    return not($return = false())
};

(:~ Return xs:dateTime(@date) of last release or version in the project or null if none exists :)
declare function decorlib:getLastRevisionDate($prefix as xs:string) as xs:dateTime? {
    decorlib:getLastRevisionDateP(decorlib:getDecorProject($prefix))
};

(:~ Return xs:dateTime(@date) of last release or version in the project or null if none exists :)
declare function decorlib:getLastRevisionDateP($decor as element()) as xs:dateTime? {
    max($decor/ancestor-or-self::decor/project/(version|release)/xs:dateTime(@date))
};

(:~ Return xs:dateTime(@date) of last release in the project or null if none exists :)
declare function decorlib:getLastReleaseDate($prefix as xs:string) as xs:dateTime? {
    decorlib:getLastReleaseDateP(decorlib:getDecorProject($prefix))
};

(:~ Return xs:dateTime(@date) of last release in the project or null if none exists :)
declare function decorlib:getLastReleaseDateP($decor as element()) as xs:dateTime? {
    max($decor/ancestor-or-self::decor/project/release/xs:dateTime(@date))
};

(:~ Return xs:dateTime(@date) of last version in the project or null if none exists :)
declare function decorlib:getLastVersionDate($prefix as xs:string) as xs:dateTime? {
    decorlib:getLastVersionDateP(decorlib:getDecorProject($prefix))
};

(:~ Return xs:dateTime(@date) of last version in the project or null if none exists :)
declare function decorlib:getLastVersionDateP($decor as element()) as xs:dateTime? {
    max($decor/ancestor-or-self::decor/project/version/xs:dateTime(@date))
};

(:~ Return base ids in the project or null if none exists. Each baseId carries attribute @default=true|false to 
:   indicate if it is also a defaultBaseId or not.
:   Using parameter $types you may optionally filter on certain types only. See DECOR format for valid values.
:
:   <baseId id="1.2.3" type="DS" prefix="xyz" default="true"/>
:)
declare function decorlib:getBaseIds($prefix as xs:string, $types as xs:string*) as element(baseId)* {
    decorlib:getBaseIdsP(decorlib:getDecorProject($prefix),$types)
};

(:~ Return base ids in the project or null if none exists. Each baseId carries attribute @default=true|false to 
:   indicate if it is also a defaultBaseId or not.
:   Using parameter $types you may optionally filter on certain types only. See DECOR format for valid values.
:
:   <baseId id="1.2.3" type="DS" prefix="xyz" default="true"/>
:)
declare function decorlib:getBaseIdsP($decor as element(), $types as xs:string*) as element(baseId)* {
    let $baseIds        := 
        if (empty($types)) then
            $decor/ancestor-or-self::decor/ids/baseId
        else (
            $decor/ancestor-or-self::decor/ids/baseId[@type=$types]
        )
    
    let $defaultBaseIds := 
        if (empty($types)) then
            $decor/ancestor-or-self::decor/ids/defaultBaseId
        else (
            $decor/ancestor-or-self::decor/ids/defaultBaseId[@type=$types]
        )
    
    return 
        if ($baseIds[@default='true']) then (
            (:assume new style:)
            $baseIds
        ) else (
            (:assume old style:)
            for $baseId in $baseIds
            return
                <baseId>
                {
                    $baseId/@id,
                    $baseId/@type,
                    attribute {'default'} {$baseId/@id=$defaultBaseIds/@id},
                    $baseId/@prefix
                }
                </baseId>
        )
};

(:~ Return default base ids in the project or null if none exists. Each carries attribute @prefix=text as readable form.
:   Using parameter $types you may optionally filter on certain types only. See DECOR format for valid values.
:
:   <defaultBaseId id="1.2.3" type="DS" prefix="xyz"/>
:)
declare function decorlib:getDefaultBaseIds($prefix as xs:string, $types as xs:string*) as element(defaultBaseId)* {
    decorlib:getDefaultBaseIdsP(decorlib:getDecorProject($prefix),$types)
};

(:~ Return default base ids in the project or null if none exists. Each carries attribute @prefix=text as readable form.
:   Using parameter $types you may optionally filter on certain types only. See DECOR format for valid values.
:
:   <defaultBaseId id="1.2.3" type="DS" prefix="xyz"/>
:)
declare function decorlib:getDefaultBaseIdsP($decor as element(), $types as xs:string*) as element(defaultBaseId)* {
    let $baseIds        := 
        if (empty($types)) then
            $decor/ancestor-or-self::decor/ids/baseId
        else (
            $decor/ancestor-or-self::decor/ids/baseId[@type=$types]
        )
    
    let $defaultBaseIds := 
        if (empty($types)) then
            $decor/ancestor-or-self::decor/ids/defaultBaseId
        else (
            $decor/ancestor-or-self::decor/ids/defaultBaseId[@type=$types]
        )
    
    return 
        if ($baseIds[@default='true']) then (
            (:assume new style:)
            for $defaultBaseId in $baseIds[@default='true']
            return
                <defaultBaseId>
                {
                    $defaultBaseId/@id,
                    $defaultBaseId/@type,
                    $defaultBaseId/@prefix
                }
                </defaultBaseId>
        ) else (
            (:assume old style:)
            for $defaultBaseId in $defaultBaseIds
            return
                <defaultBaseId>
                {
                    $defaultBaseId/@id,
                    $defaultBaseId/@type,
                    $defaultBaseId/@prefix,
                    if ($defaultBaseId[@prefix]) then () else (
                        $baseIds[@id=$defaultBaseId/@id]/@prefix
                    )
                }
                </defaultBaseId>
        )
};

(:~ Return next available id(s) for a given baseId (OID) or for one or more types in a given project. If baseId has a value then
:   $projectPrefix/$types are not considered.
:   Using parameter $types you may optionally filter on certain types only. See DECOR format for valid values.
:
:   <next base="1.2" max="2" next="{$max + 1}" id="1.2.3" type="DS"/>    (only when $projectPrefix/$types is supplied)
:   <next base="1.2" max="2" next="{$max + 1}" id="1.2.3"/>              (only when $baseId is supplied)
:)
declare function decorlib:getNextAvailableId($projectPrefix as xs:string?, $types as xs:string*, $baseId as xs:string?) as element(next)* {
    if (empty($projectPrefix)) then
        decorlib:getNextAvailableIdP((), $types, $baseId)
    else (
        decorlib:getNextAvailableIdP(decorlib:getDecorProject($projectPrefix), $types, $baseId)
    )
};
declare function decorlib:getNextAvailableIdP($decor as element()?, $types as xs:string*, $baseId as xs:string?) as element(next)* {
    let $baseIds    := 
        if (empty($baseId)) then 
            if ($decor) then decorlib:getDefaultBaseIdsP($decor, $types) else () 
        else (
            $baseId
        )
    
    for $base in $baseIds
    let $baseId     := if ($base instance of element()) then $base/@id else $base
    let $searchId   := concat($baseId, '.')
    let $all        := ($setlib:colDecorData//@id | collection($setlib:strDecorDataIG)/implementationGuide/@id)[starts-with(., $searchId)][substring-after(., $searchId) castable as xs:integer] 
    let $max        := if ($all) then max($all/xs:integer(tokenize(., '\.')[last()])) else 0
    return
        <next base="{$baseId}" max="{$max}" next="{$max + 1}" id="{concat($baseId, '.', $max + 1)}">
        {
            if ($base instance of element()) then $base/@type else (),
            if ($all) then attribute current {string-join(($baseId, $max), '.')} else ()
        }
        </next>
};

(:~ Return lock elements, which have varying element names, for a given object. Most objects have an effectiveDate but 
:   some do not, like issues and older transactions. Communities do not have an id|ref but are name based. 
:   Submit community/@name as param $ref for community locks.
:   Submit empty parameter and $forCurrentUser=true()|false() to get ALL lock for the current user or any user respectively
:)
declare function decorlib:getLocks($authmap as map(*)?, $ref as xs:string?, $effectiveDate as xs:string?, $projectId as xs:string?) as element()* {
    let $locks  := $setlib:docDecorLocks//lock
    let $locks  := if (string-length($ref) gt 0)            then $locks[@ref = $ref]                      else ($locks)
    let $locks  := if (string-length($effectiveDate) gt 0)  then $locks[@effectiveDate = $effectiveDate]  else ($locks)
    let $locks  := if (string-length($projectId) gt 0)      then $locks[@projectId = $projectId]          else ($locks)
    let $locks  := if (empty($authmap))                     then $locks                                   else $locks[@user = $authmap?name]
    
    return $locks
};
declare function decorlib:getLocks($authmap as map(*)?, $object as element()?, $recurse as xs:boolean) as element()* {
    let $ref            := if ($object/self::community) then $object/@name else ($object/@id)
    let $effectiveDate  := $object/@effectiveDate
    let $projectId      := if ($object/self::community) then $object/@projectId else ()
    
    return (
        decorlib:getLocks($authmap, $ref, $effectiveDate, $projectId)
        ,
        if ($recurse) then (
            if ($object[name()=('dataset','concept')]) then
                for $child in $object/concept
                return 
                    decorlib:getLocks($authmap, $child, $recurse)
            else 
            if ($object[name()=('scenario','transaction')]) then
                for $child in $object/transaction
                return 
                    decorlib:getLocks($authmap, $child, $recurse)
            else ()
        ) else ()
    )
};

(:~ Return boolean true|false if a given object has any lock on it. Most objects have an effectiveDate but 
:   some do not, like issues and older transactions. Communities do not have an id|ref but are name based. 
:   Submit community/@name as param $ref for community locks
:)
declare function decorlib:canLock($authmap as map(*)?, $object as element()) as xs:boolean {
    let $ref            := if ($object/self::community) then $object/@name else ($object/@id)
    let $effectiveDate  := $object/@effectiveDate
    let $projectId      := if ($object/self::community) then $object/@projectId else ()
    let $objecthasLock  := decorlib:getLocks((), $ref, $effectiveDate, $projectId)
    let $userHasLock    := $objecthasLock[@user = $authmap?name]
    
    return
        if ($userHasLock) then (true()) else if ($objecthasLock) then (false()) else (
            if ($object/self::community) then (
                decorlib:authorCanEditCommunityP($authmap, $object)
            ) else (
                let $decor      := $object/ancestor::decor
                let $section    := $object/ancestor::*[name()=$decorlib:SECTIONS-ALL][parent::decor]/name()
                
                return decorlib:authorCanEditP($authmap, $decor, $section)
            )
        )
};

(:~ Return boolean true|false if a given object has any lock on it. Most objects have an effectiveDate but 
:   some do not, like issues and older transactions. Communities do not have an id|ref but are name based. 
:   Submit community/@name as param $ref for community locks
:)
declare function decorlib:isLocked($ref as xs:string, $effectiveDate as xs:string?, $projectId as xs:string?) as xs:boolean {
    exists(decorlib:getLocks((), $ref, $effectiveDate, $projectId))
};

(:~ Return boolean true|false if a given object has a lock on it that was set by the current logged in user. 
:   Most objects have an effectiveDate but  some do not, like issues and older transactions. Communities do not 
:   have an id|ref but are name based. Submit community/@name as param $ref for community locks
:)
declare function decorlib:isLockedByUser($authmap as map(*), $ref as xs:string, $effectiveDate as xs:string?, $projectId as xs:string?) as xs:boolean {
    exists(decorlib:getLocks($authmap, $ref, $effectiveDate, $projectId))
};

(:~ Return all communities for $projectId based on setlib:strCurrentUserName() or error()
:)
declare function decorlib:getDecorCommunitiesForCurrentUser($authmap as map(*), $projectId as xs:string) as element(community)* {
    decorlib:getDecorCommunity((), $projectId, ())/access/author[@username=$authmap?name]
};

(:~ Return zero or more full community contents based on $name (optional) and $projectId. If $name 
:   is not valued then all communities for the given project are returned. If $forCurrentUser=true()
:   only those communities are returned that the current user has at least read access to
:)
declare function decorlib:getDecorCommunity($name as xs:string*, $projectId as xs:string) as element(community)* {
    decorlib:getDecorCommunity($name, $projectId, ())
};
(:~ Return zero or more full community contents based on $name (optional) and $projectId. If $name 
:   is not valued then all communities for the given project are returned. I
:)
declare function decorlib:getDecorCommunity($name as xs:string*, $projectId as xs:string, $projectVersion as xs:string?) as element(community)* {
    let $communities    := 
        if (empty($projectVersion)) then $setlib:colDecorData//community[@projectId = $projectId] else (
            let $release    := 
                if (empty($projectVersion)) then 
                    $setlib:colDecorData/decor[project[@id = $projectId]]
                else (
                    $setlib:colDecorVersion/decor[@versionDate = $projectVersion][project[@id = $projectId]]
                )
            
            return
                if ($release) then collection(util:collection-name($release[1]))//community[@projectId = $projectId] else ()
        )
    return
        if (string-length($name) gt 0) then $communities[@name = $name] else ($communities)
};

(:~ Returns all authors from a community. Note that this does not take the exist-db level into account :)
declare function decorlib:getCommunityAuthors($name as xs:string, $projectId as xs:string) as xs:string* {
    decorlib:getCommunityAuthorsP(decorlib:getDecorCommunity($name, $projectId))
};

(:~ Returns all authors from a community. Note that this does not take the exist-db level into account :)
declare function decorlib:getCommunityAuthorsP($community as element()) as xs:string* {
    $community/ancestor-or-self::community/access/author/@username
};

(:~ Returns all active authors from a project. Note that this does not take the exist-db level into account :)
declare function decorlib:getActiveCommunityAuthors($name as xs:string, $projectId as xs:string) as xs:string* {
    decorlib:getActiveCommunityAuthorsP(decorlib:getDecorCommunity($name, $projectId))
};

(:~ Returns all active authors from a project. Note that this does not take the exist-db level into account :)
declare function decorlib:getActiveCommunityAuthorsP($community as element()) as xs:string* {
    $community/ancestor-or-self::community/access/author[not(@active = 'false')][contains(@rights, 'w')]/@username
};

(:~ Returns all inactive authors from a project. Note that this does not take the exist-db level into account :)
declare function decorlib:getInactiveCommunityAuthors($name as xs:string, $projectId as xs:string) as xs:string* {
    decorlib:getInactiveCommunityAuthorsP(decorlib:getDecorCommunity($name, $projectId))
};

(:~ Returns all inactive authors from a project. Note that this does not take the exist-db level into account :)
declare function decorlib:getInactiveCommunityAuthorsP($community as element()) as xs:string* {
    $community/ancestor-or-self::community/access/author[@active='false']/@username
};

(:~ Returns boolean true|false whether or not the current user is active in the given community and has write permissions
:   Note that they might be deactivated at exist-db level :)
declare function decorlib:authorCanEditCommunity($authmap as map(*), $name as xs:string, $projectId as xs:string) as xs:boolean {
    decorlib:authorCanEditCommunityP($authmap, decorlib:getDecorCommunity($name, $projectId))
};

(:~ Returns boolean true|false whether or not the current user is active in the given community and has write permissions
:   Note that they might be deactivated at exist-db level :)
declare function decorlib:authorCanEditCommunityP($authmap as map(*), $community as element()?) as xs:boolean {
    exists(decorlib:getCommunityAuthor($community/ancestor-or-self::community, $authmap?name)[matches(@rights,'w')])
};

(:~ Returns boolean true|false whether or not the current user is active in the given community and has read permissions
:   Note that they might be deactivated at exist-db level :)
declare function decorlib:authorCanReadCommunity($authmap as map(*), $name as xs:string, $projectId as xs:string) as xs:boolean {
    exists(decorlib:getDecorCommunity($name, $projectId))/access/author[@username=$authmap?name]
};

(:~ Returns boolean true|false whether or not the current user or guest user (not logged in) is active in the given community 
:   and has read or write permissions. (write only does not exist)
:   Note that they might be deactivated at exist-db level :)
declare function decorlib:authorCanReadCommunityP($authmap as map(*), $community as element()) as xs:boolean {
    let $eligibleUsers  := (decorlib:getActiveCommunityAuthor($community/ancestor-or-self::community, $authmap?name) |
                            decorlib:getActiveCommunityAuthor($community/ancestor-or-self::community, 'guest'))

    return exists($eligibleUsers[contains(@rights,'r')] | $eligibleUsers[contains(@rights,'w')])
};

(:~ Return full author element to caller. Caller needs to do write access checking if necessary.
:   Should multiple authors by that username exist, then this is a problem of the project, not this function
:)
declare %private function decorlib:getAuthor($decor as element(decor)?, $username as xs:string) as element(author)? {
    $decor/project/author[@username=$username]
};

(:~ Return full author element to caller. Caller needs to do write access checking if necessary.
:   Should multiple authors by that username exist, then this is a problem of the project, not this function
:)
declare %private function decorlib:getActiveAuthor($decor as element(decor)?, $username as xs:string) as element(author)? {
    decorlib:getAuthor($decor, $username)[not(@active = 'false')]
};

(:~ Return full author element to caller. Caller needs to do write access checking if necessary.
:   Should multiple authors by that username exist, then this is a problem of the community, not this function
:)
declare %private function decorlib:getCommunityAuthor($community as element(community)?, $username as xs:string) as element(author)? {
    $community/access/author[@username=$username]
};

(:~ Return full author element to caller. Caller needs to do write access checking if necessary.
:   Should multiple authors by that username exist, then this is a problem of the community, not this function
:)
declare %private function decorlib:getActiveCommunityAuthor($community as element(community)?, $username as xs:string) as element(author)? {
    $community/access/author[not(@active = 'false')][@username=$username]
};

declare function decorlib:isPrivateCommunity($community as element(community)) as xs:boolean {
    not(exists(decorlib:getActiveCommunityAuthor($community/ancestor-or-self::community, 'guest'))) 
};

(: ======== ADMIN/WRITE FUNCTIONS ======== :)

(:~ Set/update value of decor/@repository if $isRepository=true. Delete decor/@repository if $isRepository=false :)
declare function decorlib:setIsRepository($authmap as map(*), $prefix as xs:string, $isRepository as xs:boolean) {
    decorlib:setIsRepositoryP($authmap, decorlib:getDecorProject($prefix), $isRepository)
};

(:~ Set/update value of decor/@repository if $isRepository=true. Delete decor/@repository if $isRepository=false :)
declare function decorlib:setIsRepositoryP($authmap as map(*), $decor as element(), $isRepository as xs:boolean) {
    let $check      := decorlib:checkAdminPermissionsP($authmap, $decor)
    
    let $decor      := $decor/ancestor-or-self::decor[project]
    
    return
        if ($decor) then
            if ($isRepository) then (
                if ($decor/@repository) then (
                    update value $decor/@repository with $isRepository
                )
                else (
                    update insert attribute repository {$isRepository} into $decor
                )
            )
            else (
                (:default value is false() so delete the attribute if exists:)
                update delete $decor/@repository
            )
        else (
            error($errors:BAD_REQUEST,'Project with this prefix does not exist.')
        )
};

(:~ Set/update value of decor/@private if $isPrivate=true. Delete decor/@private if $isPrivate=false :)
declare function decorlib:setIsPrivate($authmap as map(*), $prefix as xs:string, $isPrivate as xs:boolean) {
    decorlib:setIsPrivateP($authmap, decorlib:getDecorProject($prefix), $isPrivate)
};

(:~ Set/update value of decor/@private if $isPrivate=true. Delete decor/@private if $isPrivate=false :)
declare function decorlib:setIsPrivateP($authmap as map(*), $decor as element(), $isPrivate as xs:boolean) {
    let $check      := decorlib:checkAdminPermissionsP($authmap, $decor)
    
    let $decor      := $decor/ancestor-or-self::decor[project]
    
    return
        if ($decor) then
            if ($isPrivate) then (
                if ($decor/@private) then (
                    update value $decor/@private with $isPrivate
                )
                else (
                    update insert attribute private {$isPrivate} into $decor
                )
            )
            else (
                (:default value is false() so delete the attribute if exists:)
                update delete $decor/@private
            )
        else (
            error($errors:BAD_REQUEST,'Project with this prefix does not exist.')
        )
};

(:~ Set/update value of decor/project/@experimental if $isExperimental=true. Delete decor/project/@experimental if $isExperimental=false :)
declare function decorlib:setIsExperimental($authmap as map(*), $prefix as xs:string, $isExperimental as xs:boolean) {
    decorlib:setIsExperimentalP($authmap, decorlib:getDecorProject($prefix), $isExperimental)
};

(:~ Set/update value of decor/project/@experimental if $isExperimental=true. Delete decor/project/@experimental if $isExperimental=false :)
declare function decorlib:setIsExperimentalP($authmap as map(*), $decor as element(), $isExperimental as xs:boolean) {
    let $check          := decorlib:checkAdminPermissionsP($authmap, $decor)
    
    let $decorProject   := $decor/ancestor-or-self::decor/project
    
    return
        if ($decorProject) then
            if ($isExperimental) then (
                if ($decor/@experimental) then (
                    update value $decorProject/@experimental with $isExperimental
                )
                else (
                    update insert attribute experimental {$isExperimental} into $decorProject
                )
            )
            else (
                (:default value is false() so delete the attribute if exists:)
                update delete $decorProject/@experimental
            )
        else (
            error($errors:BAD_REQUEST,'Project with this prefix does not exist.')
        )
};

(:~ Set/update value of decor/project/@defaultLanguage :)
declare function decorlib:setDefaultLanguage($authmap as map(*), $prefix as xs:string, $defaultLanguage as xs:string) {
    decorlib:setDefaultLanguageP($authmap, decorlib:getDecorProject($prefix), $defaultLanguage)
};

(:~ Set/update value of decor/project/@experimental if $isExperimental=true. Delete decor/project/@experimental if $isExperimental=false :)
declare function decorlib:setDefaultLanguageP($authmap as map(*), $decor as element(), $defaultLanguage as xs:string) {
    let $check          := decorlib:checkAdminPermissionsP($authmap, $decor)
    
    let $decorProject   := $decor/ancestor-or-self::decor/project
    
    return
        if ($decorProject) then
            if ($decor/@defaultLanguage) then (
                update value $decorProject/@defaultLanguage with $defaultLanguage
            )
            else (
                update insert attribute defaultLanguage {$defaultLanguage} into $decorProject
            )
        else (
            error($errors:BAD_REQUEST,'Project with this prefix does not exist.')
        )
};

(:~ Updates a user in a project with given username and the first available increment as id. Adds the user if necessary when $addIfNecessary=true
:   Upon adding the user, all permissions are defaulted from $decorlib:AUTHOR-TEMPLATE. In updating the user settings only those $setting attributes 
:   are considered that match an attribute in $decorlib:AUTHOR-TEMPLATE.
:   Returns the resulting <author.../> element or error
:)
declare function decorlib:setProjectAuthor($authmap as map(*), $prefix as xs:string, $username as xs:string, $settings as element(author), $addIfNecessary as xs:boolean) as element(author) {
    decorlib:setProjectAuthorP($authmap, decorlib:getDecorProject($prefix), $username, $settings, $addIfNecessary)
};

(:~ Updates a user in a project with given username and the first available increment as id. Adds the user if necessary when $addIfNecessary=true
:   Upon adding the user, all permissions are defaulted from $decorlib:AUTHOR-TEMPLATE. In updating the user settings only those $setting attributes 
:   are considered that match an attribute in $decorlib:AUTHOR-TEMPLATE.
:   Returns the resulting <author.../> element or error
:)
declare function decorlib:setProjectAuthorP($authmap as map(*), $decor as element(), $username as xs:string, $settings as element(author), $addIfNecessary as xs:boolean) as element(author) {
    let $check      := decorlib:checkAdminPermissionsP($authmap, $decor)
    
    let $decor      := $decor/ancestor-or-self::decor[project]
    let $prefix     := $decor/project/@prefix
    
    (: get update set. skip anything empty. skip anything we don't know (includes email and notifier) :)
    let $update-atts    :=
        for $att in $settings/(@active|@datasets|@scenarios|@terminology|@rules|@ids|@issues|@admin)[not(.='')]
        return
        if ($att castable as xs:boolean) then ($att) else (
            error($errors:BAD_REQUEST,concat('Permission ',$att/name(),' must be a boolean value. Found ''',$att/string(),''''))
        )
    
    let $author         := 
        if ($addIfNecessary=true()) then
            (: add the user with default template, this takes care of @id/@username/full name :)
            decorlib:setProjectAuthor($authmap, $decor, $username)
        else (
            (: author must already exist :)
            $decor/project/author[@username=$username]
        )
    
    (: update properties according to what we determined :)
    let $update         :=
        if ($author) then (
            for $att in $update-atts
            let $storedAttribute    := $author/@*[name()=$att/name()]
            return
                if ($storedAttribute) then update value $storedAttribute with $att else update insert $att into $author
            ,
            if ($update-atts[name() = 'active'] = 'true') then update delete $author/@expirationDate else ()
        ) else (
            error($errors:BAD_REQUEST,'Author with this username does not exist in this project. Cannot update properties.')
        )
    
    return
        $decor/project/author[@username=$username]
};

(:~ Adds a user to a project with given username and the first available increment as id. 
:   All permissions are taken from $decorlib:AUTHOR-TEMPLATE
:   Returns author element or error
:
:   Dependency userapi:getUserDisplayName($authmap, $username)
:)
declare %private function decorlib:setProjectAuthor($authmap as map(*), $decor as element(decor)?, $username as xs:string) as element(author) {
    (:permissions are checked by caller, don't need new check:)
    
    let $project    := $decor/project
    let $author     := $project/author[@username=$username]
    let $newid      := if ($project/author[@id castable as xs:integer]) then max($project/author/xs:integer(@id)) else 0
    let $newid      := $newid + 1
    let $newname    := userapi:getUserDisplayName($username)
    let $newauthor  :=
        <author>
        {
            attribute id {$newid},
            attribute username {$username},
            attribute notifier {'off'},
            attribute effectiveDate {substring(string(current-dateTime()), 1, 19)},
            $decorlib:AUTHOR-TEMPLATE/(@* except (@id|@username|@notifier|@effectiveDate|@expirationDate)),
            normalize-space($newname)
        }
        </author>
    
    return
        if ($decor) then
            if ($author) then
                (:unexpected but possible... do not need to add anything, just return author as-is:)
                $author
            else (
                (:add user and return what we added:)
                let $add    := update insert $newauthor following $project/(name|desc|copyright|author)[last()]
                return $newauthor
            )
        else (
            error($errors:BAD_REQUEST,'Project with this prefix does not exist.')
        )
};

(:~ Shortcut call to decorlib:setProjectAuthor to activate a project user. Note: does not manipulate the exist-db setting :)
declare function decorlib:activateAuthor($authmap as map(*), $prefix as xs:string, $username as xs:string) as element(author) {
    decorlib:setProjectAuthor($authmap, $prefix, $username, <author active="true"/>, false())
};

(:~ Shortcut call to decorlib:setProjectAuthor to activate a project user. Note: does not manipulate the exist-db setting :)
declare function decorlib:activateAuthorP($authmap as map(*), $decor as element(), $username as xs:string) as element(author) {
    decorlib:setProjectAuthorP($authmap, $decor, $username, <author active="true"/>, false())
};

(:~ Shortcut call to decorlib:setProjectAuthor to deactivate a project user. Note: does not manipulate the exist-db setting :)
declare function decorlib:deactivateAuthor($authmap as map(*), $prefix as xs:string, $username as xs:string) as element(author) {
    decorlib:setProjectAuthor($authmap, $prefix, $username, <author active="false"/>, false())
};

(:~ Shortcut call to decorlib:setProjectAuthor to deactivate a project user. Note: does not manipulate the exist-db setting :)
declare function decorlib:deactivateAuthorP($authmap as map(*), $decor as element(), $username as xs:string) as element(author) {
    decorlib:setProjectAuthorP($authmap, $decor, $username, <author active="false"/>, false())
};

(:~ Returns nothing if the currently logged in user has administrative privileges or error :)
declare function decorlib:checkAdminPermissions($authmap as map(*), $prefix as xs:string) {
    if ($authmap?groups = 'dba') then () 
    else 
    if (decorlib:isProjectAdmin($authmap, $prefix)) then () 
    else (
        error($errors:FORBIDDEN,'You must have decor administrative privileges for this action.')
    )
};

(:~ Returns nothing if the currently logged in user has administrative privileges or error :)
declare function decorlib:checkAdminPermissionsP($authmap as map(*), $decor as element()) {
    if ($authmap?groups = 'dba') then () 
    else 
    if (decorlib:isProjectAdminP($authmap, $decor)) then () 
    else (
        error($errors:FORBIDDEN,'You must have decor administrative privileges for this action.')
    )
};

(:~ Returns <true/> element containing the lock that was successfully set, OR a <false/> element containing someone elses 
:   lock, or an <false/> element the user was not allowed edit rights for the object.
:
:   Most objects have an effectiveDate but some do not, like issues and older transactions. Communities do not 
:   have an id|ref but are name based. Submit community/@name as param $ref for community locks
:
:   To acquire a lock on an object the currently logged in user must have edit rights for the (rights parts of the) project 
:   or in the case of a community lock the user must have edit rights in the community. Secondly a lock must not have been 
:   set by another user, unless $breakLock is true in which case the lock from the other user is replaced by a new lock 
:   for the current user.
:)
declare function decorlib:setLock($authmap as map(*), $ref as item(), $effectiveDate as xs:string?, $breakLock as xs:boolean) as element() {
    decorlib:setLock($authmap, (), $ref, $effectiveDate, $breakLock)
};
declare function decorlib:setLock($authmap as map(*), $projectId as xs:string?, $ref as item(), $effectiveDate as xs:string?, $breakLock as xs:boolean) as element(lock) {
    let $object             := 
        if ($ref instance of element()) then $ref else
        if (string-length($effectiveDate)=0) then
            $setlib:colDecorData//*[@id = $ref][not(ancestor::history|self::object|ancestor::ids)] | $setlib:colDecorData//community[@name = $ref][@projectId = $projectId]
        else (
            for $o in ( $setlib:colDecorData//dataset[@id = $ref] | $setlib:colDecorData//concept[@id = $ref] | 
                        $setlib:colDecorData//scenario[@id = $ref] | $setlib:colDecorData//transaction[@id = $ref] |
                        $setlib:colDecorData//valueSet[@id = $ref] | $setlib:colDecorData//template[@id = $ref] | 
                        $setlib:colDecorData//codeSystem[@id = $ref] | $setlib:colDecorData//conceptMap[@id = $ref] |
                        $setlib:colDecorData//questionnaire[@id = $ref] | $setlib:colDecorData//questionnaireresponse[@id = $ref] |
                        $setlib:colDecorDataIG/implementationGuide[@id = $ref])
            return 
                if ($o[@effectiveDate = $effectiveDate][not(ancestor::history|self::object)]) then $o else ()
        )
    
    let $check              := 
        if ($object) then () else (
            if (string-length($effectiveDate)=0) then
                error($errors:BAD_REQUEST, concat('Object with ref=''',$ref,''' projectId=''',$projectId,''' is not found. Cannot create lock for an unknown object'))
            else (
                error($errors:BAD_REQUEST, concat('Object with ref=''',$ref,''' effectiveDate=''',$effectiveDate,''' is not found. Cannot create lock for an unknown object'))
            )
        )
    let $check              := 
        if (count($object) = 1) then () else (
            if (string-length($effectiveDate)=0) then
                error($errors:SERVER_ERROR, concat('Object with ref="',$ref,'" projectId="',$projectId,'" is found multiple times. This needs fixing in the database by an administrator.'))
            else (
                error($errors:SERVER_ERROR, concat('Object with ref="',$ref,'" effectiveDate="',$effectiveDate,'" is found multiple times (prefixes: ', string-join($object/ancestor::decor/project/@prefix, ', '), '). This needs fixing in the database by an administrator.'))
            )
        )
    
    return decorlib:setLock($authmap, $object, $breakLock)
};
declare function decorlib:setLock($authmap as map(*), $object as element()?, $breakLock as xs:boolean) as element(lock) {
    let $ref                := if ($object) then $object/self::community/@name | $object/@id else ()
    let $effectiveDate      := $object[not(self::community)]/@effectiveDate
    let $projectId          := $object/self::community/@projectId | $object/self::implementationGuide/@projectId
    let $objectName         := 
        if ($object[@displayName]) then $object/@displayName else if ($object[@name]) then $object/@name else $object/name[1]
    let $objectType         := decorlib:getObjectDecorType($object[1])

    let $check              := 
        if ($object) then () else (
            error($errors:BAD_REQUEST, concat('Object is not found. Cannot create lock for an unknown object'))
        )
    let $check              := 
        if (count($object) = 1) then () else (
            if ($object/ancestor::decor) then 
                error($errors:SERVER_ERROR, concat('Object is found multiple times (prefixes: ', string-join($object/ancestor::decor/project/@prefix, ', '), '). This needs fixing in the database by an administrator.'))
            else (
                error($errors:SERVER_ERROR, concat('Object is found multiple times. This needs fixing in the database by an administrator.'))
            )
        )
    let $check              := 
        if ($objectType = $decorlib:OBJECTTYPES-ALL) then () else (
            error($errors:BAD_REQUEST, concat('The lock type "',$objectType,'" is not supported. Please choose one of: ',string-join($decorlib:OBJECTTYPES-ALL,', ')))
        )
    
    let $decor              :=
        if (empty($projectId)) then
            $object/ancestor::decor
        else (
            $setlib:colDecorData//decor[project/@id=$projectId]
        )
    let $projectPrefix      := $decor/project/@prefix
    let $user               := $authmap?name
    let $userDisplayName    := userapi:getUserDisplayName($user)
    
    let $currentLocks       :=
        if ($object[name()='object']) then () else (
            decorlib:getLocks((), $object/@id, $object/@effectiveDate, $projectId)
        )
    let $currentTreeLocks  :=
        for $a in $object/ancestor::*[@id] | $object/descendant::*[@id]
        return
            if ($a[name()='object']) then () else (
                decorlib:getLocks((), $a/@id, $a/@effectiveDate, $projectId)
            )
    let $lockFromOtherUser  := $currentLocks[not(@user = $authmap?name)] | $currentTreeLocks[not(@user = $authmap?name)]
    
    let $check              := 
        if ($objectType = $decorlib:OBJECTTYPE-COMMUNITY) then (
            if (decorlib:authorCanEditCommunityP($authmap, $object)) then () else (
                error($errors:FORBIDDEN, 'User ' || $user || ' is not an active author for community ' || $objectName || '.')
            )
        ) else (
            if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION_TYPE_MAPPING//map[@type = $objectType]/@section)) then () else (
                error($errors:FORBIDDEN, 'User ' || $user || ' is not an active author for project ' || $decor/project/@prefix || ' that this object belongs to.')
            )
        )
    let $newLock            :=
        if (empty($lockFromOtherUser) or $breakLock) then (
            let $newLock    :=
                <lock type="{$objectType}" ref="{$ref}">
                {
                    if (string-length($effectiveDate)>0) then attribute effectiveDate {$effectiveDate} else (),
                    if (string-length($projectId)>0) then attribute projectId {$projectId} else (),
                    if (string-length($objectName)>0) then attribute displayName {$objectName} else (),
                    attribute user {$user},
                    attribute userName {$userDisplayName},
                    attribute since {current-dateTime()},
                    attribute prefix {$projectPrefix}
                }
                </lock>
            let $locks      := if ($currentLocks) then update delete $currentLocks else ()
            
            let $insertLock := update insert $newLock into $setlib:docDecorLocks//decorLocks
            
            return decorlib:getLocks($authmap, $newLock/@ref, $newLock/@effectiveDate, $newLock/@projectId)
        ) else (
            error($errors:FORBIDDEN,
                'Could not lock object because a lock on this object or one of its parents or children already exists and you did not specify to breakLock.' ||
                'Current lock(s) held by ' || 
                string-join(
                    for $l in $lockFromOtherUser
                    return
                        $l/@userName || ' (' || $l/@since || ')'
                    ,
                ', ') ||
                ' (h_unlk).'
                )
        )
    
    return $newLock
};

(:~ Deletes locks for the given object. Returns true if any lock at all was deleted. Returns false if no locks were found to delete.
:
:   Most objects have an effectiveDate but some do not, like issues and older transactions. Communities do not 
:   have an id|ref but are name based. Submit community/@name as param $ref for community locks
:)
declare function decorlib:deleteLock($authmap as map(*), $ref as xs:string, $effectiveDate as xs:string?, $projectId as xs:string?) as xs:boolean {
    let $locks  := decorlib:getLocks((), $ref, $effectiveDate, $projectId)
    let $r      := exists($locks)
    let $clear  := update delete $locks
    
    return $r
};

(:~ Deletes locks for the given object. Returns true if any lock at all was deleted. Returns false if no locks were found to delete.
:
:   Most objects have an effectiveDate but some do not, like issues and older transactions. Communities do not 
:   have an id|ref but are name based. Submit community/@name as param $ref for community locks
:)
declare function decorlib:deleteLock($authmap as map(*), $object as element()?) as xs:boolean {
    let $ref            := if ($object/self::community) then $object/@name else if ($object[@id]) then ($object/@id) else ($object/@ref)
    let $effectiveDate  := $object/@effectiveDate
    let $projectId      := $object/@projectId
    
    return decorlib:deleteLock($authmap, $ref, $effectiveDate, $projectId)
};

(:~ Deletes locks for the given object if they were set by the supplied user. Returns true if any lock at all was deleted. 
:   Returns false if no locks were found to delete.
:
:   Most objects have an effectiveDate but  some do not, like issues and older transactions. Communities do not 
:   have an id|ref but are name based. Submit community/@name as param $ref for community locks
:)
declare function decorlib:deleteLocksUser($authmap as map(*), $username as xs:string) as xs:boolean {
    let $locks          := 
        if ($authmap?name = $username or $authmap?groups[. = ('dba', 'decor-admin')]) then
            decorlib:getLocks((), (), (), false())[@user=$username]
        else ()
    let $r      := exists($locks)
    let $clear  := update delete $locks
    
    return $r
};

(:~ Utility function to go from DECOR object to lock type :)
declare function decorlib:getObjectDecorType($object as element()?) as xs:string? {
    switch ($object/name()) 
    case 'community' return $decorlib:OBJECTTYPE-COMMUNITY
    case 'dataset' return $decorlib:OBJECTTYPE-DATASET
    case 'concept' return $decorlib:OBJECTTYPE-DATASETCONCEPT
    case 'valueSet' return $decorlib:OBJECTTYPE-VALUESET
    case 'codeSystem' return $decorlib:OBJECTTYPE-CODESYSTEM
    case 'conceptMap' return $decorlib:OBJECTTYPE-MAPPING
    case 'scenario' return $decorlib:OBJECTTYPE-SCENARIO
    case 'actor' return $decorlib:OBJECTTYPE-ACTOR
    case 'transaction' return $decorlib:OBJECTTYPE-TRANSACTION
    case 'questionnaire' return $decorlib:OBJECTTYPE-QUESTIONNAIRE
    case 'questionnaireresponse' return $decorlib:OBJECTTYPE-QUESTIONNAIRERESPONSE
    case 'template' return $decorlib:OBJECTTYPE-TEMPLATE
    case 'element' return $decorlib:OBJECTTYPE-TEMPLATEELEMENT
    case 'issue' return $decorlib:OBJECTTYPE-ISSUE
    case 'implementationGuide' return $decorlib:OBJECTTYPE-IMPLEMENTATIONGUIDE
    default return ()
};