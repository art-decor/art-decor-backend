xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

module namespace utillib                = "http://art-decor.org/ns/api/util";

import module namespace errors          = "http://e-editiones.org/roaster/errors";

import module namespace setlib          = "http://art-decor.org/ns/api/settings" at "settings-lib.xqm";
import module namespace jx              = "http://joewiz.org/ns/xquery/json-xml" at "json-xml.xqm";
import module namespace decorlib        = "http://art-decor.org/ns/api/decor" at "decor-lib.xqm";
import module namespace histlib         = "http://art-decor.org/ns/api/history" at "history-lib.xqm";
import module namespace tcsapi          = "http://art-decor.org/ns/api/terminology/codesystem" at "../terminology-codesystem-api.xqm";
import module namespace serverapi       = "http://art-decor.org/ns/api/server" at "server-api.xqm";
import module namespace vsapi           = "http://art-decor.org/ns/api/valueset" at "../valueset-api.xqm";
import module namespace markdown        = "http://art-decor.org/ns/api/markdown" at "markdown-lib.xqm";


declare namespace request           = "http://exist-db.org/xquery/request";
declare namespace response          = "http://exist-db.org/xquery/response";
declare namespace datetime          = "http://exist-db.org/xquery/datetime";
declare namespace hl7               = "urn:hl7-org:v3";
declare namespace xhtml             = "http://www.w3.org/1999/xhtml";
declare namespace util              = "http://exist-db.org/xquery/util";
declare namespace sch               = "http://purl.oclc.org/dsdl/schematron";
declare namespace xforms            = "http://www.w3.org/2002/xforms";
declare namespace expath            = "http://expath.org/ns/pkg";
declare namespace xsi               = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace error             = "http://art-decor.org/ns/decor/error";
declare namespace json              = "http://www.json.org";
declare namespace xsl               = "http://www.w3.org/1999/XSL/Transform";

declare variable $utillib:strDecorServicesURL       := utillib:getServerURLServices();
declare variable $utillib:strFhirServicesURL        := utillib:getServerURLFhirServices();
declare variable $utillib:strFhirCanonicalBaseURL   := utillib:getServerURLFhirCanonicalBase();
declare variable $utillib:strAD3ServerURL           := utillib:getServerURLArt3();

(:~ Source ItemStatusCodeLifeCycle in DECOR-datatypes.xsd :)
declare variable $utillib:itemstatusmap     := 
        map:merge((
            map:entry('',           ('new', 'draft')) ,
            map:entry('new',        ('new', 'draft', 'cancelled')) ,
            map:entry('draft',      ('draft', 'pending', 'final', 'cancelled', 'rejected')) ,
            map:entry('cancelled',  ('cancelled', 'draft')) ,
            map:entry('pending',    ('draft', 'pending', 'final')) ,
            map:entry('final',      ('final', 'deprecated')) ,
            map:entry('rejected',   ('rejected')) ,
            map:entry('deprecated', ('deprecated'))
        ));
(:~ Source CodedConceptStatusCodeLifeCycle in DECOR-datatypes.xsd :)
declare variable $utillib:codedconceptstatusmap     := 
        map:merge((
            map:entry('',           ('draft', 'active', 'deprecated', 'retired', 'cancelled', 'rejected', 'experimental')) ,
            map:entry('draft',      ('draft', 'active', 'deprecated', 'retired', 'cancelled', 'rejected', 'experimental')),
            map:entry('active',     ('active', 'deprecated')),
            map:entry('deprecated', ('deprecated', 'retired')),
            map:entry('retired',    ('retired')),
            map:entry('cancelled',  ('cancelled')),
            map:entry('rejected',   ('rejected')),
            map:entry('experimental',('experimental', 'active'))
        ));
(:~ Source TemplateStatusCodeLifeCycle in DECOR-datatypes.xsd :)
declare variable $utillib:templatestatusmap := 
        map:merge((
            map:entry('',           ('draft')) ,
            map:entry('draft',      ('draft', 'pending', 'active', 'cancelled', 'rejected')) ,
            map:entry('cancelled',  ('cancelled', 'draft')) ,
            map:entry('pending',    ('pending', 'draft', 'active', 'rejected')) ,
            map:entry('active',     ('active', 'review', 'retired')) ,
            map:entry('review',     ('review', 'active', 'retired')) ,
            map:entry('rejected',   ('rejected')) ,
            map:entry('retired',    ('retired'))
        ));
(:~ Source ReleaseStatusCodeLifeCycle in DECOR-datatypes.xsd :)
declare variable $utillib:releasestatusmap := 
        map:merge((
            map:entry('',           'draft'),
            map:entry('draft',      ('draft', 'pending', 'active', 'cancelled', 'failed')),
            map:entry('pending',    ('pending', 'draft', 'active', 'cancelled', 'retired', 'failed')),
            map:entry('active',     ('active', 'retired')),
            map:entry('retired',    'retired'),
            map:entry('cancelled',  ('cancelled', 'draft')),
            map:entry('failed',     'failed')
        ));
(:~ Source IssueStatusCodeLifeCycle in DECOR-datatypes.xsd :)
declare variable $utillib:issuestatusmap := 
        map:merge((
            map:entry('',           'new') ,
            map:entry('new',        'open'),
            map:entry('open',       ('open', 'inprogress', 'feedback', 'closed', 'rejected', 'deferred', 'cancelled')),
            map:entry('inprogress', ('inprogress', 'feedback', 'closed', 'rejected', 'deferred', 'cancelled')),
            map:entry('feedback',   ('feedback', 'inprogress', 'closed', 'rejected', 'deferred', 'cancelled')),
            map:entry('closed',     ('closed', 'inprogress', 'feedback', 'rejected', 'deferred', 'cancelled')),
            map:entry('rejected',   ('rejected', 'inprogress', 'feedback', 'closed', 'deferred', 'cancelled')),
            map:entry('deferred',   ('deferred', 'inprogress', 'feedback', 'closed', 'rejected', 'cancelled')),
            map:entry('cancelled',  ('cancelled', 'inprogress'))
        ));
(:~ Source FHIRQuestionnaireResponseStatusCodeLifeCycle in DECOR-datatypes.xsd :)
declare variable $utillib:questionnaireresponsestatusmap := 
        map:merge((
            map:entry('',                   ('in-progress', 'entered-in-error', 'completed', 'amended', 'stopped')) ,
            map:entry('in-progress',        ('in-progress', 'entered-in-error', 'completed', 'amended', 'stopped')),
            map:entry('completed',          ('in-progress', 'entered-in-error', 'completed', 'amended', 'stopped')) ,
            map:entry('amended',            ('in-progress', 'entered-in-error', 'completed', 'amended', 'stopped')) ,
            map:entry('entered-in-error',   ('in-progress', 'entered-in-error', 'completed', 'amended', 'stopped')) ,
            map:entry('stopped',            ('in-progress', 'entered-in-error', 'completed', 'amended', 'stopped'))
        ));
declare variable $utillib:valuesetexpansionstatusmap := 
        map:merge((
            map:entry('',           ('final', 'deprecated')),
            map:entry('final',      ('final', 'deprecated')),
            map:entry('deprecated', 'deprecated')
        ));

declare variable $utillib:emailCssStyles := 
" * {
        font-family: Verdana, Arial, sans-serif;
        font-size: 9.0pt;
    }
    body {
        color: #1a202c;
        text-align: left;
        background-color: #e2e8f0;
    }
    .container {
        margin: 7px;
    }
    .main-body {
        padding: 10px;
    }
    h3 {
        font-size: 10.0pt;
    }
    .button {
        border: none;
        text-align: center;
        margin: 2px;
        text-decoration: none;
        display: inline-block;
        cursor: pointer;
    }
    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 0 solid rgba(0, 0, 0, .125);
        border-radius: .25rem;
        box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06);
    }
    .card-body {
        flex: 1 1 auto;
        min-height: 1px;
        padding: 1rem;
    }
    .gutters-sm {
        margin-right: -8px;
        margin-left: -8px;
    }
    .gutters-sm &gt; .col,
    .gutters-sm &gt; [class *= col-] {
        padding-right: 8px;
        padding-left: 8px;
    }
    .mb-3,
    .my-3 {
        margin-bottom: 1rem !important;
    }
    .mt-3 {
        margin-top: 1rem !important;
    }
    .newlabel {
        color: #bbbbbb;
    }
    div.labelouterbox {
        background-color: #eaeaea;
        float: right;
        display: inline;
        color: black;
        font-weight: bold;
        font-size: smaller;
        margin-left: 5px;
    }
    div.labelinnerbox {
        display: inline;
        padding: 0 5px
    }
    .status-green {
        border-left: 7px solid #c9e9c9;
        border-top: 1px solid #c9e9c9;
        padding: 3px 0 0 15px;
    }
    .status-blue {
        border-left: 7px solid #D1DDFF;
        border-top: 1px solid #D1DDFF;
        padding: 3px 0 0 15px;
    }
    .status-red {
        border-left: 7px solid #fadbdb;
        border-top: 1px solid #fadbdb;
        padding: 3px 0 0 15px;
    }
    .status-orange {
        border-left: 7px solid #fffaf4;
        border-top: 1px solid #fffaf4;
        padding: 3px 0 0 15px;
    }
    .status-grey {
        border-left: 7px solid #eaeaea;
        border-top: 1px solid #eaeaea;
        padding: 3px 0 0 15px;
    }
    .tt {
        font-family: Monaco, Courier, monospace !important;
    }
";

(:~ Return if a new statusCode is allowable given the old statusCode and the object type :)
declare function utillib:isStatusChangeAllowable($object as element(), $newStatus as xs:string) as xs:boolean {
    if (string($object/@statusCode) = string($newStatus)) then true() else (
        switch (local-name($object))
        case 'template' return map:get($utillib:templatestatusmap, string($object/@statusCode)) = $newStatus
        case 'release' return map:get($utillib:releasestatusmap, string($object/@statusCode)) = $newStatus
        case 'version' return map:get($utillib:releasestatusmap, string($object/@statusCode)) = $newStatus
        case 'codedConcept' return map:get($utillib:codedconceptstatusmap, string($object/@statusCode)) = $newStatus
        case 'valueSetExpansionSet' return map:get($utillib:valuesetexpansionstatusmap, string($object/@statusCode)) = $newStatus
        case 'issue' return map:get($utillib:issuestatusmap, string($object/@statusCode)) = $newStatus
        case 'questionnaireresponse'
        case 'QuestionnaireResponse' return map:get($utillib:questionnaireresponsestatusmap, string($object/@statusCode)) = $newStatus
        default return map:get($utillib:itemstatusmap, string($object/@statusCode)) = $newStatus
    )
};

(:~ Type switch on $request-body and return valid xml or nothing

@param $request-body           - required. Input from $request?body
@return valid xml
:)
declare function utillib:getBodyAsXml($request-body as item()?) as element()? {
    utillib:getBodyAsXml($request-body, (), ())
};
(:~ Type switch on $request-body and return valid xml or nothing. 

Example input
<map xmlns="http://www.w3.org/2005/xpath-functions">
   <number key="number">557</number>
   <string key="name">Fleece Pullover</string>
   <array key="colorChoices">
      <string>navy</string>
      <string>black</string>
   </array>
   <boolean key="is-current">true</boolean>
   <null key="other"/>
   <map key="priceInfo">
      <number key="price">19.99</number>
      <number key="discount">10.00</number>
   </map>
</map>

Example output
<unknown_object number="557" name="Fleece Pullover" is-current="true" other="">
   <colorChoices>navy</colorChoices>
   <colorChoices>black</colorChoices>
   <priceInfo price="19.99" discount="10.00"/>
</unknown_object>

@param $request-body           - required. Input from $request?body
@param $root                   - optional. Name of the root element if not in json input. Defaults to unknown_object
@param $namespace              - optional. Namespace of the root element. Defaults to empty string.
@return valid xml
:)
declare function utillib:getBodyAsXml($request-body as item()?, $root as xs:string?, $namespace as xs:string?) as element()? {
    let $xslt           :=
        <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" exclude-result-prefixes="#all" version="3.0">
            <xsl:param name="root" select="'unknown_object'"/>
            <xsl:param name="namespace" select="''"/>
        
            <xsl:template match="/">
                <xsl:apply-templates/>
            </xsl:template>
            
            <xsl:template match="/fn:map">
                <xsl:choose>
                  <xsl:when test=".[empty(fn:map[@key]) or count(*) gt 1]">
                    <xsl:element name="{{$root}}" namespace="{{$namespace}}">
                      <xsl:apply-templates select="(fn:string | fn:boolean | fn:number | fn:null) except fn:*[@key = '#text']"/>
                      <xsl:apply-templates select="(fn:array | fn:map) except fn:*[@key = '#text']"/>
                      <xsl:apply-templates select="fn:*[@key = '#text']"/>
                    </xsl:element>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:apply-templates select="(fn:string | fn:boolean | fn:number | fn:null) except fn:*[@key = '#text']"/>
                    <xsl:apply-templates select="(fn:array | fn:map) except fn:*[@key = '#text']"/>
                    <xsl:apply-templates select="fn:*[@key = '#text']"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:template>
            <xsl:template match="fn:map">
              <xsl:element name="{{(parent::fn:array/@key, @key)}}">
                <xsl:apply-templates select="(fn:string | fn:boolean | fn:number | fn:null) except fn:*[@key = '#text']"/>
                <xsl:apply-templates select="(fn:array | fn:map) except fn:*[@key = '#text']"/>
                <xsl:apply-templates select="fn:*[@key = '#text']"/>
              </xsl:element>
            </xsl:template>
            <xsl:template match="fn:array">
              <xsl:apply-templates select="(fn:string | fn:boolean | fn:number | fn:null) except fn:*[@key = '#text']"/>
              <xsl:apply-templates select="(fn:array | fn:map) except fn:*[@key = '#text']"/>
              <xsl:apply-templates select="fn:*[@key = '#text']"/>
            </xsl:template>
            <xsl:template match="fn:string | fn:boolean | fn:number | fn:null">
              <xsl:choose>
                <xsl:when test="empty(@key) and parent::fn:array/@key">
                  <xsl:element name="{{parent::fn:array/@key}}">
                    <xsl:try select="fn:parse-xml('&lt;div&gt;' || . || '&lt;/div&gt;')/div/node()">
                      <xsl:catch select="text()"/>
                    </xsl:try>
                  </xsl:element>
                </xsl:when>
                <xsl:when test="@key = '#text'">
                  <xsl:try select="fn:parse-xml('&lt;div&gt;' || . || '&lt;/div&gt;')/div/node()">
                    <xsl:catch select="text()"/>
                  </xsl:try>
                </xsl:when>
                <!-- "ext:dummy-1": "urn:hl7-org:v3/ext" -->
                <xsl:when test="matches(@key, '^[^:]+:dummy')">
                  <xsl:attribute name="{{QName(., @key)}}" select="." namespace="."/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="{{@key}}" select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:template>
        </xsl:stylesheet>
    let $xsltParameters :=
        <parameters>
        {   if (empty($root)) then () else (
                <param name="root" value="{$root}"/>,
                if (empty($namespace)) then () else (
                    <param name="namespace" value="{$namespace}"/>
                )
            )
        }
        </parameters>
    return
    if (empty($request-body)) then () else (
        typeswitch ($request-body)
        case element() return $request-body
        case document-node() return $request-body/*
        default return (
            (:fn:serialize($request-body, map{'method':'json', 'indent': true(), 'json-ignore-whitespace-text-nodes':'yes'}) => 
            json-to-xml() => transform:transform($xslt, $xsltParameters):)
            fn:serialize($request-body, map{'method':'json', 'indent': true(), 'json-ignore-whitespace-text-nodes':'yes'}) =>
            jx:json-to-xml() => transform:transform($xslt, $xsltParameters)
        )
    )
};

(:  Returns SVRL version of schematron
    
    Input:  schematron grammar
    Output: SVRL for schematron grammar
    
    @author Kai Heitmann, Alexander Henket, Marc de Graauw 
    @since 2014
:)
declare function utillib:get-iso-schematron-svrl($grammar as element(sch:schema)) as element() {
    let $parameters :=
        <parameters>
            <param name="allow-foreign" value="'true'"/>
            <param name="generate-fired-rule" value="'true'"/>
            <param name="generate-paths" value="'true'"/>
            <param name="diagnose" value="'yes'"/>
        </parameters>
    return utillib:get-iso-schematron-svrl($grammar, $parameters)
};

(:~ Returns SVRL version of schematron
    
    Input:  schematron grammar
            parameters 
    Output: SVRL for schematron grammar
    
    @author Kai Heitmann, Alexander Henket, Marc de Graauw 
    @since 2014
:)
declare function utillib:get-iso-schematron-svrl($grammar as element(sch:schema), $parameters as element(parameters)) as element() {
    (: create ISO schematron conversion to XSL :)
    let $sch-include            := xs:anyURI('xmldb:exist://' || $setlib:strUtilISOSCH2SVRL || "/iso_dsdl_include.xsl")
    let $sch-expand             := xs:anyURI('xmldb:exist://' || $setlib:strUtilISOSCH2SVRL || "/iso_abstract_expand.xsl")
    let $sch-compile            := xs:anyURI('xmldb:exist://' || $setlib:strUtilISOSCH2SVRL || "/iso_svrl_for_xslt2.xsl")
    
    (: store the work-around wrapper XSL next to schematron. Without this workaround, includes are skipped :)
    let $xslwrap                :=
        <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" version="2.0">
            <xsl:include href="{$sch-include}"/>
        </xsl:stylesheet>
    (: if we got an in-memory node that is not persisted in the database yet or if don't have write access to the collection, 
       assume that we don't need inclusion resolving and use the normal $sch-include 
       See: https://github.com/eXist-db/exist/issues/4014
    :)
    let $sch-collection         := util:collection-name($grammar)
    let $sch-resource           := util:document-name($grammar)
    let $svrl-resource          := $sch-resource || '_svrl.xsl'
    
    (: recreate if the input sch or result svrl does not exist, or svrl is older than the sch input :)
    let $recreate               :=
        try {
            let $schdate    := xmldb:last-modified($sch-collection, $sch-resource)
            let $svrldate   := xmldb:last-modified($sch-collection, $svrl-resource)
            
            return
                empty($schdate) or empty($svrldate) or $schdate gt $svrldate
        }
        catch * {
            true()
        }
    
    return
        if ($recreate) then (
            let $sch-include            := 
                if (empty($sch-collection)) then 
                    $sch-include
                else
                if (doc-available($sch-collection || '/sch2xsl.xsl')) then
                    $sch-collection || '/sch2xsl.xsl'
                else
                if (xmldb:collection-available($sch-collection) and sm:has-access(xs:anyURI($sch-collection), 'rw')) then
                    xmldb:store($sch-collection, 'sch2xsl.xsl', $xslwrap)
                else (
                    $sch-include
                )
            (:
                the original pipe statement we used here:
                let $xsl                    :=
                   $grammar => transform:transform(doc($sch-include), $parameters) => 
                               transform:transform(doc($sch-expand), $parameters) => 
                               transform:transform(doc($sch-compile), $parameters)
                is unbelievable elegant but for large transforms this can cause java heap space conditions
                KH 20240910 split that into three transforms all based on one variable, thus transform to var and then use that as input for next step
            :)
            let $xsl := transform:transform($grammar, doc($sch-include), $parameters)
            let $xsl := transform:transform($xsl, doc($sch-expand), $parameters)
            let $xsl := transform:transform($xsl, doc($sch-compile), $parameters)
            
            (: might not have write access. silent fail, maybe the next user who does this, has. worst that can happen is recreation :)
            let $store                  := try { xmldb:store($sch-collection, $svrl-resource, $xsl) } catch * {()}
            
            return $xsl
        )
        else (
            doc($sch-collection || '/' || $svrl-resource)/xsl:*
        )
};

(:~ Update @location contents in SVRL output by replacing element[namespace-uri()='...'] with *:element for readability sake :)
declare function utillib:strip-namespace-in-svrl-locations($input as node()?) as element() {
    let $xslt := 
        <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svrl="http://purl.oclc.org/dsdl/svrl" version="2.0">
             <xsl:template match="/">
                <xsl:copy>
                     <xsl:apply-templates select="@*|node()"/>
                </xsl:copy>
             </xsl:template>
             <xsl:template match="svrl:failed-assert">
                <xsl:copy>
                     <xsl:apply-templates select="@* except @location"/>
                     <xsl:if test="@location">
                        <!-- [namespace-uri()='urn:hl7-org:v3'] -->
                        <xsl:attribute name="location" select="replace(replace(@location, '\[namespace-uri\(\)=[^\]]*\]', ''), '/\*:', '/')"/>
                     </xsl:if>
                     <xsl:apply-templates select="node()"/>
                </xsl:copy>
             </xsl:template>
             <xsl:template match="@*|node()">
                 <xsl:copy>
                     <xsl:apply-templates select="@*|node()"/>
                 </xsl:copy>
             </xsl:template>
         </xsl:stylesheet>
    return transform:transform($input, $xslt, ())
};

(:~  Return live specific or repository/non-private DECOR project OR compiled, archived/released DECOR project based on project/@id and decor/@versionDate.
:   Released/compiled matches could be present in multiple languages. Use the parameters @language to select the right one.
:
:   @param $decorOrPrefixOrId - required element(decor) or decor/project/@prefix or decor/project/@id or '*' for any project. If empty returns all non private BBRs
:   @param $decorVersion      - optional decor/@versionDate, Empty uses live projects, '*' selects all versions, dateTime() selects that versionDate, otherwise selects latest version for language
:   @param $language          - optional decor/@language. Only relevant with parameter $decorVersion. Empty uses decor/project/@defaultLanguage, '*' selects all versions, e.g. 'en-US' selects the US English version (if available)
:   @return all repository/non-private project files, exactly 1 DECOR live project file, or 1 or more archived DECOR version instances or nothing if not found
:   @since 2023-09-25
:)
declare function utillib:getDecor($decorOrPrefixOrId as item()?, $decorVersion as xs:string?, $language as xs:string?) as element(decor)* {
    typeswitch ($decorOrPrefixOrId)
    case element(decor) return $decorOrPrefixOrId
    default return
        if (utillib:isOid($decorOrPrefixOrId)) then
            utillib:getDecorById($decorOrPrefixOrId, $decorVersion, $language)
        else (
            utillib:getDecorByPrefix($decorOrPrefixOrId, $decorVersion, $language)
        )
};
(:~  Return one live specific or all repository/non-private DECOR projects
:
:   @param $projectPrefix   - required decor/project/@prefix
:   @return exactly 1 DECOR project file or nothing if $projectPrefix and nothing found. If $projectPrefix is empty returns all non-private repositories
:   @since 2015-04-29
:)
declare function utillib:getDecorByPrefix($projectPrefix as xs:string?) as element(decor)* {
    utillib:getDecorByPrefix($projectPrefix, (), ())
};
(:~  Return one live specific or repository/non-private DECOR project OR compiled, archived/released DECOR 
:   project based on project/@prefix and decor/@versionDate.
:   Released/compiled matches could be present in multiple languages. Use the parameters @language to select the right one.
:
:   @param $projectPrefix   - required decor/project/@prefix or '*' for any project. If empty returns all non private BBRs
:   @param $decorVersion    - optional decor/@versionDate, Empty uses live projects, '*' selects all versions, 'latest' selects the latest versionDate, dateTime() selects that versionDate, otherwise selects latest version for language
:   @param $language        - optional decor/@language. Only relevant with parameter $decorVersion. Empty uses decor/project/@defaultLanguage, '*' selects all versions, e.g. 'en-US' selects the US English version (if available)
:   @return all repository/non-private project files, exactly 1 DECOR live project file, or 1 or more archived 
:           DECOR version instances or nothing if not found
:   @since 2015-04-29
:)
declare function utillib:getDecorByPrefix($projectPrefix as xs:string?, $decorVersion as xs:string?) as element(decor)* {
    utillib:getDecorByPrefix($projectPrefix, $decorVersion, ())
};
declare function utillib:getDecorByPrefix($projectPrefix as xs:string?, $decorVersion as xs:string?, $language as xs:string?) as element(decor)* {
    (: sometimes people forget the trailing hyphen :)
    let $projectPrefix  := 
        if (ends-with($projectPrefix, '-'))     then $projectPrefix else
        if ($projectPrefix = '*')               then $projectPrefix else
        if (string-length($projectPrefix) = 0)  then $projectPrefix else (
            concat($projectPrefix, '-')
        )
    
    return
    if (empty($projectPrefix)) then
        $setlib:colDecorData/decor[@repository = 'true'][not(@private = 'true')]
    else
    if (empty($decorVersion)) then
        if ($projectPrefix = '*') then (
            $setlib:colDecorData/decor
        )
        else (
            $setlib:colDecorData/decor[project[@prefix = $projectPrefix]]
        )
    else 
    if ($decorVersion = '*') then (
        let $d      := 
            if ($projectPrefix = '*') 
            then ($setlib:colDecorVersion/decor[@versionDate])
            else ($setlib:colDecorVersion/decor[@versionDate][project[@prefix = $projectPrefix]])
        return
        if (empty($language)) then
            ($d[@language = $d/project/@defaultLanguage], $d)[1]
        else if ($language = '*') then
            $d
        else
            $d[@language = $language]
    )
    else 
    if (string-length($decorVersion) gt 0) then (
        let $d      := 
            if ($projectPrefix = '*') 
            then ($setlib:colDecorVersion/decor[@versionDate = $decorVersion])
            else ($setlib:colDecorVersion/decor[@versionDate = $decorVersion][project[@prefix = $projectPrefix]])
        return
        if (empty($language)) then
            ($d[@language = $d/project/@defaultLanguage], $d)[1]
        else if ($language = '*') then
            $d
        else
            $d[@language = $language]
    )
    else (
        let $d      := 
            if ($projectPrefix = '*') then 
                for $decor in $setlib:colDecorVersion/decor[@versionDate castable as xs:dateTime]
                let $pfx    := $decor/project/@prefix
                group by $pfx
                return
                    $decor[@versionDate = max($decor/xs:dateTime(@versionDate))]
            else (
                for $decor in $setlib:colDecorVersion/decor[@versionDate castable as xs:dateTime][project[@prefix = $projectPrefix]]
                let $pfx    := $decor/project/@prefix
                group by $pfx
                return
                    $decor[@versionDate = max($decor/xs:dateTime(@versionDate))]
            )
        return
        if (empty($language)) then
            ($d[@language = $d/project/@defaultLanguage], $d)[1]
        else if ($language = '*') then
            $d
        else
            $d[@language = $language]
    )
};
(:~  Return one live specific or all repository/non-private DECOR projects
:
:   @param $projectId       - required decor/project/@id
:   @return exactly 1 DECOR project file or nothing if not found
:   @since 2015-04-29
:)
declare function utillib:getDecorById($projectId as xs:string?) as element(decor)? {
    utillib:getDecorById($projectId, (), ())
};
(:~  Return one live specific or repository/non-private DECOR project OR compiled, archived/released DECOR 
:   project based on project/@id and decor/@versionDate.
:   Released/compiled matches could be present in multiple languages. Use the parameters @language to select the right one.
:
:   @param $projectId       - required decor/project/@id or '*' for any project. If empty returns all non private BBRs
:   @param $decorVersion    - optional decor/@versionDate, Empty uses live projects, '*' selects all versions, dateTime() selects that versionDate, otherwise selects latest version for language
:   @param $language        - optional decor/@language. Only relevant with parameter $decorVersion. Empty uses decor/project/@defaultLanguage, '*' selects all versions, e.g. 'en-US' selects the US English version (if available)
:   @return all repository/non-private project files, exactly 1 DECOR live project file, or 1 or more archived 
:           DECOR version instances or nothing if not found
:   @since 2015-04-29
:)
declare function utillib:getDecorById($projectId as xs:string?, $decorVersion as xs:string?) as element(decor)* {
    utillib:getDecorById($projectId, $decorVersion, ())
};
declare function utillib:getDecorById($projectId as xs:string?, $decorVersion as xs:string?, $language as xs:string?) as element(decor)* {
    if (empty($projectId)) then
        $setlib:colDecorData/decor[@repository = 'true'][not(@private = 'true')]
    else
    if (empty($decorVersion)) then
        if ($projectId = '*') then (
            $setlib:colDecorData/decor
        )
        else (
            $setlib:colDecorData/decor[project[@id = $projectId]]
        )
    else 
    if ($decorVersion = '*') then (
        let $d      := 
            if ($projectId = '*') 
            then ($setlib:colDecorVersion/decor[@versionDate])
            else ($setlib:colDecorVersion/decor[@versionDate][project[@id = $projectId]])
        return
        if (empty($language)) then
            ($d[@language = $d/project/@defaultLanguage], $d)[1]
        else if ($language = '*') then
            $d
        else
            $d[@language = $language]
    )
    else 
    if (string-length($decorVersion) gt 0) then (
        let $d      := 
            if ($projectId = '*') 
            then ($setlib:colDecorVersion/decor[@versionDate = $decorVersion])
            else ($setlib:colDecorVersion/decor[@versionDate = $decorVersion][project[@id = $projectId]])
        return
        if (empty($language)) then
            ($d[@language = $d/project/@defaultLanguage], $d)[1]
        else if ($language = '*') then
            $d
        else
            $d[@language = $language]
    )
    else (
        let $d              := 
            if ($projectId = '*') 
            then ($setlib:colDecorVersion/decor[@versionDate castable as xs:dateTime])
            else ($setlib:colDecorVersion/decor[@versionDate castable as xs:dateTime][project[@id = $projectId]])
        let $d              :=
            if (empty($language)) then
                ($d[@language = $d/project/@defaultLanguage], $d)[1]
            else if ($language = '*') then
                $d
            else
                $d[@language = $language]
        return
            $d[@versionDate = max($d/xs:dateTime(@versionDate))]
    )
};

(:~ Returns a full dataset tree for either a transaction/@id or dataset/@id,
    or when called with a specific conceptId, it returns this concept and child concepts
    
    Input:  id                      - transaction/@id or dataset/@id
            effectiveDate           - transaction/@effectiveDate or dataset/@effectiveDate
                                        If @param effectiveDate is not xs:dateTime, will act as dynamic and pick newest
            conceptId               - concept/@id
            conceptEffectiveDate    - concept/@effectiveDate
                                        If @param conceptEffectiveDate is not xs:dateTime, will act as dynamic and pick newest
            language, used to filter names
    Output: dataset
        - filtered for language
        - inherits resolved
        - conceptList/@ref resolved
        - no history
        - concept/valueDomain[@type='code'] are provided with conceptList/valueSet 
            with enhanced valueSet (valueSet with names from conceptList where terminologyAssociations are present)
        - concept contains implementation element with @shortName (usable as SQL, XML name) and @xpath
    
    In a number of cases like RetrieveTransaction and template mapping we do not care about absent concepts to just omit those when $fullTree=false()
    Note: only active concepts are traversed. A concept with an absent parent is not retrieved. This situation should never occur.
    
    @author Alexander Henket 
    @since 2013
:)
declare function utillib:getFullDataset($id as xs:string, $effectiveDate as xs:string?, $conceptId as xs:string?, $conceptEffectiveDate as xs:string?, $language as xs:string?, $xpathDoc as node()?, $fullTree as xs:boolean, $oidnamemap as item()?) as element() {
    let $datasetOrTransaction   := utillib:getDataset($id, $effectiveDate) | utillib:getTransaction($id, $effectiveDate)
    
    return 
    utillib:getFullDatasetTree($datasetOrTransaction, $conceptId, $conceptEffectiveDate, $language, $xpathDoc, $fullTree, $oidnamemap, ())
};
declare function utillib:getFullDatasetTree($datasetOrTransaction as element(), $conceptId as xs:string?, $conceptEffectiveDate as xs:string?, $language as xs:string?, $xpathDoc as node()?, $fullTree as xs:boolean, $oidnamemap as item()?) as element() {
    utillib:getFullDatasetTree($datasetOrTransaction, $conceptId, $conceptEffectiveDate, $language, $xpathDoc, $fullTree, $oidnamemap, ())
};
declare function utillib:getFullDatasetTree($datasetOrTransaction as element(), $conceptId as xs:string?, $conceptEffectiveDate as xs:string?, $language as xs:string?, $xpathDoc as node()?, $fullTree as xs:boolean, $oidnamemap as item()?, $communityName as xs:string*) as element() {
    let $isDataset          := exists($datasetOrTransaction[self::dataset])
    let $isTransaction      := $datasetOrTransaction[self::transaction]
    let $decor              := $datasetOrTransaction/ancestor::decor
    let $projectId          := $decor/project/@id
    let $prefix             := $decor/project/@prefix
    let $defaultLanguage    := $decor/project/@defaultLanguage
    let $language           := if (string-length($language)=0) then $defaultLanguage else ($language)
    
    let $representingTemplate   := $datasetOrTransaction/representingTemplate
    let $dataset                := 
        if ($datasetOrTransaction[self::dataset]) then $datasetOrTransaction else 
        if ($representingTemplate[@sourceDataset]) then
            utillib:getDataset($representingTemplate/@sourceDataset, $representingTemplate/@sourceDatasetFlexibility)
        else
        if ($datasetOrTransaction[@datasetId]) then
            utillib:getDataset($datasetOrTransaction/@datasetId, $datasetOrTransaction/@datasetEffectiveDate)
        else ()
    let $datasetDecor               := $dataset/ancestor::decor
    let $datasetDecorProjectId      := $datasetDecor/project/@id
    let $datasetDecorProjectPrefix  := $datasetDecor/project/@prefix
    
    (:let $datasetTree    := 
        if ($isTransaction) then 
            utillib:getDatasetTree((), (), $datasetOrTransaction/@id, $datasetOrTransaction/@effectiveDate) 
        else (
            utillib:getDatasetTree($datasetOrTransaction/@id, $datasetOrTransaction/@effectiveDate, (), ())
        ):)
    (: only for transactions: if the xpaths are provided, insert xpath and hl7 datatype :)
    let $xpaths         := if ($xpathDoc and $isTransaction) then $xpathDoc//transactionXpaths[@ref = $datasetOrTransaction/@id][1] else ()

    let $name           := 
        if ($datasetOrTransaction/name[@language=$language]) 
        then $datasetOrTransaction/name[@language=$language][1] 
        else <name language="{if ($language = '*') then $datasetOrTransaction/name[1]/@language else $language}">{$datasetOrTransaction/name[1]/node()}</name>
    
    (: build map of oid names. is more costly when you do that deep down :)
    let $oidnamemap             :=
        if (empty($oidnamemap)) then (
            map:merge((
                let $oidName    := utillib:getNameForOID($datasetOrTransaction/@id, $language, $decor)
                return
                    if (string-length($oidName) = 0) then () else map:entry($datasetOrTransaction/@id, $oidName)
                ,
                for $oid in ($dataset//@id | $dataset//@ref)
                group by $oid
                return (
                    let $oidName    := 
                        if ($datasetOrTransaction[@id = $oid[1]]) then () else 
                        if ($fullTree or $isDataset) then
                            utillib:getNameForOID($oid[1], $language, $oid[1]/ancestor::decor)
                        else
                        if ($dataset[@id = $oid[1]] or $datasetOrTransaction//concept[@ref = $oid/ancestor-or-self::concept[1]/@id]) then 
                            utillib:getNameForOID($oid[1], $language, $oid[1]/ancestor::decor) 
                        else ()
                    return
                    if (string-length($oidName) = 0) then () else map:entry($oid, $oidName)
                )
            ))
        )
        else ($oidnamemap)
    let $transactionconceptmap  :=
        if ($isTransaction) then
            map:merge((
                map:entry('transaction', <transaction>{$datasetOrTransaction/@*}</transaction>)
                ,
                for $concept in $representingTemplate/concept[@ref]
                let $de-id      := $concept/@ref
                group by $de-id
                return
                    map:entry($de-id[1], $concept[1])
            ))
        else ()
    
    (: get communities for current project, but also get communities from project the dataset is from, if that is not the same project. :)
    let $communities            := 
        if (empty($projectId)) then () else if ($datasetOrTransaction//community) then () else (
            decorlib:getDecorCommunity($communityName, $projectId, $decor/@versionDate),
            if ($prefix = $datasetDecorProjectPrefix) then () else (
                if ($representingTemplate or not($isTransaction))
                then decorlib:getDecorCommunity($communityName, $datasetDecorProjectId, $datasetDecor/@versionDate)
                else ()
            )
        )
    let $communityIdMap         :=
        map:merge(
            if (empty($datasetDecorProjectId) or $projectId = $datasetDecorProjectId) then () else map:entry($datasetDecorProjectId, ($datasetDecorProjectPrefix))
        )
    
    let $fullDatasetTree := 
        <dataset>
        {
            $dataset/@id, $dataset/@effectiveDate, $dataset/@statusCode, $dataset/@versionLabel, $dataset/@expirationDate, 
            $dataset/@officialReleaseDate, $dataset/@canonicalUri, $dataset/@lastModifiedDate, $decor/@versionDate, $prefix, 
            if ($datasetDecorProjectPrefix = $prefix) then () else attribute datasetPrefix {$datasetDecorProjectPrefix}
            ,
            if ($isTransaction) then (
                attribute transactionId {$datasetOrTransaction/@id}, 
                attribute transactionEffectiveDate {$datasetOrTransaction/@effectiveDate},
                attribute transactionStatusCode {$datasetOrTransaction/@statusCode},
                if ($datasetOrTransaction/@expirationDate) then
                    attribute transactionExpirationDate {$datasetOrTransaction/@expirationDate}
                else (),
                if ($datasetOrTransaction/@versionLabel) then
                    attribute transactionVersionLabel {$datasetOrTransaction/@versionLabel}
                else (),
                if ($datasetOrTransaction/@canonicalUri) then
                    attribute transactionCanonicalUri {$datasetOrTransaction/@canonicalUri}
                else (),
                if ($datasetOrTransaction/@type) then
                    attribute transactionType {$datasetOrTransaction/@type}
                else ()
                ,
                let $iddisplay  := map:get($oidnamemap, $datasetOrTransaction/@id)
                let $iddisplay  := if (empty($iddisplay)) then utillib:getNameForOID($datasetOrTransaction/@id, $language, $decor) else ($iddisplay)
                return
                    attribute transactionIddisplay {$iddisplay}
                ,
                if ($representingTemplate[@ref]) then (
                    attribute templateId {$representingTemplate/@ref},
                    if ($representingTemplate[@flexibility]) then
                        attribute templateEffectiveDate {$representingTemplate/@flexibility}
                    else ()
                ) else ()
                ,
                if ($representingTemplate[@representingQuestionnaire]) then (
                    attribute questionnaireId {$representingTemplate/@representingQuestionnaire},
                    if ($representingTemplate[@representingQuestionnaireFlexibility]) then
                        attribute questionnaireEffectiveDate {$representingTemplate/@representingQuestionnaireFlexibility}
                    else ()
                ) else ()
            ) else ()
            ,
            attribute shortName {utillib:shortName($name)}, 
            let $iddisplay  := if ($dataset/@id and ($representingTemplate or not($isTransaction))) then map:get($oidnamemap, $dataset/@id) else ()
            let $iddisplay  := if (empty($iddisplay)) then utillib:getNameForOID($dataset/@id, $language, $datasetDecor) else ($iddisplay)
            return
                attribute iddisplay {$iddisplay}
            ,
            attribute url {$utillib:strDecorServicesURL},
            attribute ident {$prefix}
        }
        {
            comment {concat(
                '&#10; This is a view of the ', if ($isTransaction) then 'transaction' else 'dataset', ' "', $name, '" containing DECOR specifications for a transaction or dataset in a single, hierarchical view. ',
                '&#10; All inheritances are resolved, for transactions the dataset is filtered for those concepts occurring in the transaction.',
                '&#10; Valuesets are contained within the concept for easy reference. ',
                '&#10; Xpaths are calculated on a best effort basis. The should be considered a starting point for application logic, not an endpoint. '
            )}
        }
        {
            if ($language = '*') then (
                $datasetOrTransaction/name,
                $datasetOrTransaction/desc
            )
            else (
                $name,
                if ($datasetOrTransaction/desc[@language = $language]) 
                then $datasetOrTransaction/desc[@language = $language][1] 
                else <desc language="{if ($language = '*') then $datasetOrTransaction/desc[1]/@language else $language}">{$datasetOrTransaction/desc[1]/node()}</desc>
            ), 
            (: new ... 2021-05-21 :)
            if ($datasetOrTransaction/publishingAuthority) then $datasetOrTransaction/publishingAuthority else $dataset/publishingAuthority
            ,
            (: new since 2015-04-21, updated 2021-05-21 :)
            if ($datasetOrTransaction/property) then $datasetOrTransaction/property else $dataset/property
            ,
            (: new ... 2021-05-21 :)
            if ($datasetOrTransaction/copyright) then $datasetOrTransaction/copyright else $dataset/copyright
            ,
            (: new since 2015-04-21 :)
            for $relationship in $dataset/relationship
            let $referredDataset    :=  
                if (string-length($relationship/@ref)=0) then () else (
                    utillib:getDataset($relationship/@ref, $relationship/@flexibility)
                )
            return
                <relationship type="{$relationship/@type}" ref="{$relationship/@ref}" flexibility="{$relationship/@flexibility}">
                {
                    if ($referredDataset) then (
                        $referredDataset/ancestor::decor/project/@prefix,
                        attribute iStatusCode {$referredDataset/@statusCode}, 
                        if ($referredDataset/@expirationDate) then attribute iExpirationDate {$referredDataset/@expirationDate} else (),
                        if ($referredDataset/@versionLabel) then attribute iVersionLabel {$referredDataset/@versionLabel} else (),
                        let $iddisplay  := 
                            if (map:contains($oidnamemap, $relationship/@ref)) then map:get($oidnamemap, $relationship/@ref) else (
                                utillib:getNameForOID($relationship/@ref, $language, $referredDataset/ancestor::decor)
                            )
                        return attribute refdisplay {$iddisplay},
                        attribute localInherit {$referredDataset/ancestor::decor/project/@prefix = $prefix},
                        if ($language = '*') then ($referredDataset/name) else
                        if ($referredDataset/name[@language = $language]) then ($referredDataset/name[@language = $language][1]) else (
                            <name language="{if ($language = '*') then $referredDataset/name[1]/@language else $language}">{data($referredDataset/name[1])}</name>
                        )
                    ) else ()
                }
                </relationship>
        }
        {
            (: only children with non-absent descendants, or everything if this is a plain dataset expand :)
            (: the conceptnamemap is for performance on datasets with many sibling concepts so we calculate names only once :)
            if (empty($transactionconceptmap)) then
                if (empty($conceptId)) then (
                    let $conceptset         := $dataset/concept
                    let $conceptnamemap     := utillib:getConceptNameMap($conceptset, $language)
                    
                    for $concept in $dataset/concept
                    return utillib:getFullConcept($datasetDecor, $concept, $language, $xpaths, (), $oidnamemap, $transactionconceptmap, $conceptnamemap)
                )
                else (
                    let $conceptset         := $dataset/concept[(@id|@ref) = $conceptId]
                    let $conceptnamemap     := utillib:getConceptNameMap($conceptset, $language)
                    
                    for $concept in $dataset//concept[(@id|@ref) = $conceptId]
                    return utillib:getFullConcept($datasetDecor, $concept, $language, $xpaths, (), $oidnamemap, $transactionconceptmap, $conceptnamemap)
                )
            else (
                if (empty($conceptId)) then (
                    let $conceptset         := $dataset/concept[.[map:contains($transactionconceptmap, @id)] | descendant::concept[map:contains($transactionconceptmap, @id)]]
                    let $conceptnamemap     := utillib:getConceptNameMap($conceptset, $language)
                    
                    for $concept in $conceptset
                    return utillib:getFullConcept($datasetDecor, $concept, $language, $xpaths, (), $oidnamemap, $transactionconceptmap, $conceptnamemap)
                )
                else (
                    let $conceptset         := $dataset//concept[.[map:contains($transactionconceptmap, @id)] | descendant::concept[map:contains($transactionconceptmap, @id)]]
                    let $conceptnamemap     := utillib:getConceptNameMap($conceptset, $language)
                    
                    for $concept in $conceptset
                    return utillib:getFullConcept($datasetDecor, $concept, $language, $xpaths, (), $oidnamemap, $transactionconceptmap, $conceptnamemap)
                )
            )
        }
        </dataset>
    
    let $fullDatasetTree :=
        if ($communities) then utillib:mergeDatasetTreeWithCommunity($fullDatasetTree, $communities, $communityIdMap) else ($fullDatasetTree)
    
    return $fullDatasetTree
};

declare function utillib:getConceptNameMap($conceptset as element(concept)*, $language as xs:string?) as item() {
    map:merge(
        for $c in $conceptset
        let $oc         := utillib:getOriginalForConcept($c)
        let $cname      := $oc/name[@language = $language][.//text()]
        let $cname      := if ($cname) then $cname else <name language="{$language}">{$oc/name[.//text()][1]/node()}</name>
        let $shortName  := utillib:shortName($cname)
        return
            map:entry($c/@id, $shortName)
    )
};

(:~  Returns a full concept for getFullDatasetTree 
    Input:  id 
            language, used to filter names
    Output: full concept, see description of getFullDatasetTree
    
    @author Marc de Graauw, Alexander Henket 
    @since 2013
:)
declare function utillib:getFullConcept($decor as element(decor), $concept as element(), $language as xs:string, $xpaths as node()?, $parentXpath as xs:string?, $oidnamemap as item()?, $transactionconceptmap as item()?) as element(concept) {
    utillib:getFullConcept($decor, $concept, $language, $xpaths, $parentXpath, $oidnamemap, $transactionconceptmap, ())
};
declare function utillib:getFullConcept($decor as element(decor), $concept as element(), $language as xs:string, $xpaths as node()?, $parentXpath as xs:string?, $oidnamemap as item()?, $transactionconceptmap as item()?, $conceptnamemap as item()?) as element(concept) {
    (:let $projectPrefix      := $decor/project/@prefix:)
    (:let $defaultLanguage    := $decor/project/@defaultLanguage:)
    
    (: build map of oid names. is more costly when you do that deep down :)
    let $oidnamemap         :=
        if (empty($oidnamemap)) then 
            map:merge(
                for $oid in distinct-values($concept//@id | $concept//@ref)
                let $oidName    := utillib:getNameForOID($oid, $language, $decor)
                return
                    if (string-length($oidName) = 0) then () else map:entry($oid, $oidName)
            )
        else ($oidnamemap)
    
    let $conceptnamemap     :=
        if (empty($conceptnamemap)) then
            utillib:getConceptNameMap($concept/preceding-sibling::concept, $language)
        else (
            $conceptnamemap
        )
    
    let $transactionconcept := if (empty($transactionconceptmap)) then () else map:get($transactionconceptmap, ($concept/@id | $concept/@ref)[1])
    
    (:  usually you inherit from the original, but if you inherit from something that inherits, then these two will differ
        originalConcept has the type and the associations. The rest comes from the inheritConcept :)
    let $inheritConcept     := if ($concept[inherit]) then utillib:getConcept($concept/inherit/@ref, $concept/inherit/@effectiveDate) else ()
    (:  normally we inherit directly from the source, but if we don't we need to know if the final thing before the original has a 
        contains relationship with the original. If it does we want to add that contains element into the compiled/fullConcept. The 
        function utillib:getOriginalConcept will get the hierarchy of nested inherit/contains elements all the way up to the originalConcept :)
    let $finalInherit       := if ($inheritConcept[inherit]) then utillib:getOriginalConcept($inheritConcept/inherit)/descendant-or-self::inherit[last()] else ()
    let $finalInherit       := if ($finalInherit) then utillib:getConcept($finalInherit/@ref, $finalInherit/@effectiveDate) else ()
    
    let $originalConcept    := utillib:getOriginalForConcept($concept)
    let $shortName          := map:get($conceptnamemap, $concept/@id)
    
    (: when multiple names of the same type exist on the same level, we need to make it unique. We postfix with _<leaf node of id>. 
        The leaf node should be more stable than just the sequence in the group :)
    let $matchingPreviousShortNames := false()
    let $matchingPreviousShortNames := 
        for $c in $concept/preceding-sibling::concept/@id
        return if (map:get($conceptnamemap, $c) = $shortName) then true() else ()
    let $shortName          :=
        if ($matchingPreviousShortNames = true()) then concat($shortName,'_',tokenize(($concept/@id, $concept/@ref)[1],'\.')[last()]) else $shortName
    
    (: Calculate the right xpath expression for this concept :)
    (: A single row in xpaths will look like:
    <concept ref="..." effectiveDate="..." elementId="..." xpath="/hl7:ClinicalDocument... etc..." valueLocation="@extension" hl7Type="II"/>
    :)
    (: Find the elements in xpaths which contain a concept corresponding to concept/@id :) 
    (: If no xpaths file is available (it's a dataset, xpaths not generated yet, no xpath for this concept (usually no corresponding elementId in templateAssociation)), then empty :) 
    let $xpathRows := if ($xpaths) then ($xpaths//concept[@ref=$concept/@id]/../..) else ()

    (: Try to find the Xpath :)
    let $xpathRow :=
        if (empty($xpathRows)) then 
            ((:<concept message="Warning: Unable to calculate xpath."/>:)) 
        else
        
        (: If there's only one xpath for this concept, that's the one :)
        if (count($xpathRows) = 1) 
        then $xpathRows[1]
        
        (: If there's more than one, see if we have (exactly) one which starts with our parent's xpath, and pick that one :)
        else (
            if (empty($parentXpath)) 
            then 
                <concept message="Warning: More than one xpath for concept, but no xpath for parent.">
                {for $xpathRow in $xpathRows return <xpath>{data($xpathRow/@xpath)}</xpath>}
                </concept> 
            else if (count($xpathRows[starts-with(@xpath, $parentXpath)]) = 1) 
            then $xpathRows[starts-with(@xpath, $parentXpath)][1]
            (: If there is more than one xpath for a concept, and the parent's xpath is not the first part of one of those, then this warning will be hit :)
            else 
                <concept message="Warning: More than one xpath, unable to calculate">
                {for $xpathRow in $xpathRows return <xpath>{data($xpathRow/@xpath)}</xpath>}
                </concept>
        )
    (:                             utillib:getConceptAssociations($concept, utillib:getOriginalForConcept(utillib:getConcept($concept/@ref, $concept/@flexibility)), $mode, true()):)
    let $conceptAssociations    := 
        if ($transactionconcept) then (
            let $tr     := map:get($transactionconceptmap, 'transaction')
            let $tc     := <transaction>{$tr/@*, $transactionconcept}</transaction>
            return utillib:getConceptAssociations($tc/concept, $originalConcept, false(), 'normal', true())
        )
        else (
            utillib:getConceptAssociations($concept, $originalConcept, false(), 'normal', true())
        )
    
    let $valueDomainSets            :=
        for $valueDomain in $originalConcept/valueDomain
        let $conceptLists :=
            for $conceptList in $valueDomain/conceptList
            return utillib:getOriginalConceptList($conceptList)
        return (
            <valueDomain>
            {
                $valueDomain/@*,
                for $conceptList in $conceptLists
                return
                    <conceptList>
                    {
                        $conceptList/@*,
                        for $conceptListItem in $conceptList/concept 
                        return (
                            <concept>
                            {
                                $conceptListItem/@*,
                                if ($language = '*') then (
                                    $conceptListItem/name,
                                    $conceptListItem/synonym,
                                    $conceptListItem/desc
                                ) else (
                                    $conceptListItem/name[@language=$language][1],
                                    $conceptListItem/synonym[@language=$language],
                                    $conceptListItem/desc[@language=$language][1]
                                )
                            }
                            </concept>
                        )
                    }
                    </conceptList>
                ,
                $valueDomain/property[@*[string-length() gt 0]]
                ,
                for $ex in $valueDomain/example
                return
                    <example type="{($ex/@type, 'neutral')[1]}">{$ex/@caption, $ex/node()}</example>
            }
            </valueDomain>
            ,
            (: add enhanced valueSet :)
            for $conceptList in $conceptLists
            return utillib:getEnhancedValueSet($decor, $conceptList, $conceptAssociations, $language)
        )
    
    return 
        <concept>
        {
            (: attributes from concept except @type from originalConcept and @navkey which is irrelevant here :)
            $concept/(@* except (@navkey|@type|@minimumMultiplicity|@maximumMultiplicity|@conformance|@isMandatory))[not(.='')], 
            $originalConcept/@type,
            if (empty($transactionconceptmap)) then () else (
                attribute minimumMultiplicity {if ($transactionconcept) then utillib:getMinimumMultiplicity($transactionconcept) else ('0')},
                attribute maximumMultiplicity {if ($transactionconcept) then utillib:getMaximumMultiplicity($transactionconcept) else ('*')},
                if ($transactionconcept/@isMandatory='true') then
                    attribute conformance {'M'}
                else if ($transactionconcept/@conformance[not(.='')]) then
                    $transactionconcept/@conformance
                else (),
                attribute isMandatory {$transactionconcept/@isMandatory='true'}
            ),
            if ($concept[@iddisplay | @refdisplay]) then () else 
            if ($concept[@id]) then (
                let $iddisplay  := map:get($oidnamemap, $concept/@id)
                let $iddisplay  := if (empty($iddisplay)) then utillib:getNameForOID($concept/@id, $language, $decor) else ($iddisplay)
                return
                    attribute iddisplay {$iddisplay}
            ) else
            if ($concept[@ref]) then (
                let $iddisplay  := map:get($oidnamemap, $concept/@ref)
                let $iddisplay  := if (empty($iddisplay)) then utillib:getNameForOID($concept/@ref, $language, $decor) else ($iddisplay)
                return
                    attribute refdisplay {$iddisplay}
            ) else (),
            (: Move shortName to concept, but keep in implementation (at least for a while) for backward compatibility :)
            if ($shortName) then attribute shortName {$shortName} else (),
            $transactionconcept/@enableBehavior[not(.='')]
        }
        {
            <implementation>
            {
                if ($shortName) then attribute shortName {$shortName} else (), 
                if ($xpathRow/@datatype) then attribute hl7Type {$xpathRow/@datatype} else (),
                $xpathRow/@xpath,
                $xpathRow/@message,
                if ($originalConcept/desc) then attribute markdown { markdown:html2markdown(utillib:parseNode($originalConcept/desc[.//text()][1])/node()) } else (),
                if ($xpathRow/ancestor::template) then element templateLocation {$xpathRow/ancestor::template[1]/@*} else (),
                $xpathRow/xpath
            }
            </implementation>
        }
        {
            if ($transactionconcept) then (
                if ($language = '*') then (
                    $transactionconcept/context
                )
                else (
                    $transactionconcept/context[@language=$language][1]
                )
                ,
                (: element children, not concept &amp; history children, filtered for language :)
                for $condition in $transactionconcept/condition
                let $isMandatory    := $condition/@isMandatory='true'
                let $conformance    := if ($isMandatory) then 'M' else ($condition/@conformance)
                return
                <condition>
                {
                    attribute minimumMultiplicity {utillib:getMinimumMultiplicity($condition)},
                    attribute maximumMultiplicity {utillib:getMaximumMultiplicity($condition)},
                    if (empty($conformance)) then () else attribute conformance {$conformance},
                    attribute isMandatory {$isMandatory}
                    ,
                    if ($language = '*') then
                        $condition/desc
                    else (
                        $condition/desc[@language = $language][1]
                    )
                    ,
                    if ($condition[desc]) then () else 
                    if ($condition[string-length(.)=0]) then () else (
                        if ($language = '*') then
                            <desc language="{$originalConcept/name[1]/@language}">{$condition/text()}</desc>
                        else (
                            <desc language="{$language}">{$condition/text()}</desc>
                        )
                    )
                }
                </condition>
                ,
                $transactionconcept/enableWhen
            )
            else ()
        }
        {
            if ($concept[inherit]) then 
                if ($concept[@refdisplay]) then (
                    $concept
                ) else (
                    <inherit>
                    {
                        $concept/inherit/@ref, $concept/inherit/@effectiveDate,
                        $inheritConcept/ancestor::decor/project/@prefix,
                        if ($inheritConcept) then (
                            attribute datasetId {$inheritConcept/ancestor::dataset/@id},
                            attribute datasetEffectiveDate {$inheritConcept/ancestor::dataset/@effectiveDate},
                            attribute datasetStatusCode {$inheritConcept/ancestor::dataset/@statusCode},
                            if ($inheritConcept/ancestor::dataset[@expirationDate]) then attribute datasetExpirationDate {$inheritConcept/ancestor::dataset/@expirationDate} else (),
                            if ($inheritConcept/ancestor::dataset[@versionLabel]) then attribute datasetVersionLabel {$inheritConcept/ancestor::dataset/@versionLabel} else (),
                            attribute iType {$originalConcept/@type}, 
                            attribute iStatusCode {$inheritConcept/@statusCode}, 
                            attribute iEffectiveDate {$inheritConcept/@effectiveDate},
                            if ($inheritConcept[@expirationDate]) then attribute iExpirationDate {$inheritConcept/@expirationDate} else (),
                            if ($inheritConcept[@versionLabel]) then attribute iVersionLabel {$inheritConcept/@versionLabel} else ()
                        ) else ()
                        ,
                        let $compare    := $inheritConcept[@id = $originalConcept/@id]
                        return
                        if ($compare[@effectiveDate = $originalConcept/@effectiveDate]) then () else (
                            attribute originalId {$originalConcept/@id}, 
                            attribute originalEffectiveDate {$originalConcept/@effectiveDate}, 
                            attribute originalStatusCode {$originalConcept/@statusCode}, 
                            if ($originalConcept[@expirationDate]) then attribute originalExpirationDate {$originalConcept/@expirationDate} else (),
                            if ($originalConcept[@versionLabel]) then attribute originalVersionLabel {$originalConcept/@versionLabel} else (),
                            attribute originalPrefix {$originalConcept/ancestor::decor/project/@prefix}
                        )
                        ,
                        let $iddisplay  := map:get($oidnamemap, $concept/inherit/@ref)
                        let $iddisplay  := if (empty($iddisplay)) then utillib:getNameForOID($concept/inherit/@ref, $concept/ancestor::decor/project/@defaultLanguage, $inheritConcept/ancestor::decor) else ($iddisplay)
                        return
                            attribute refdisplay {$iddisplay}
                    }
                    </inherit>
                )
            else ()
        }
        {
            let $containedConcept           :=
                if ($concept[contains]) then $concept else
                if ($inheritConcept[contains]) then $inheritConcept else
                if ($finalInherit[contains]) then $finalInherit else ()
            let $originalContainedConcept   :=
                if ($concept[contains]) then $originalConcept else
                if ($containedConcept[contains]) then utillib:getOriginalForConcept($containedConcept) else ()
            
            return
            if ($containedConcept) then
                <contains>
                {
                    $containedConcept/contains/@ref, $containedConcept/contains/@flexibility,
                    $originalContainedConcept/ancestor::decor/project/@prefix,
                    if ($originalContainedConcept) then (
                        attribute datasetId {$originalContainedConcept/ancestor::dataset/@id},
                        attribute datasetEffectiveDate {$originalContainedConcept/ancestor::dataset/@effectiveDate},
                        attribute datasetStatusCode {$originalContainedConcept/ancestor::dataset/@statusCode},
                        if ($originalConcept/ancestor::dataset[@expirationDate]) then attribute datasetExpirationDate {$originalContainedConcept/ancestor::dataset/@expirationDate} else (),
                        if ($originalConcept/ancestor::dataset[@versionLabel]) then attribute datasetVersionLabel {$originalContainedConcept/ancestor::dataset/@versionLabel} else (),
                        attribute iType {$originalContainedConcept/@type}, 
                        attribute iStatusCode {$originalContainedConcept/@statusCode}, 
                        attribute iEffectiveDate {$originalContainedConcept/@effectiveDate},
                        if ($originalContainedConcept[@expirationDate]) then attribute iExpirationDate {$originalContainedConcept/@expirationDate} else (),
                        if ($originalContainedConcept[@versionLabel]) then attribute iVersionLabel {$originalContainedConcept/@versionLabel} else ()
                    ) else ()
                    ,
                    let $iddisplay  := map:get($oidnamemap, $containedConcept/contains/@ref)
                    let $iddisplay  := if (empty($iddisplay)) then utillib:getNameForOID($containedConcept/contains/@ref, $containedConcept/ancestor::decor/project/@defaultLanguage, $originalContainedConcept/ancestor::decor) else ($iddisplay)
                    return
                        attribute refdisplay {$iddisplay}
                }
                </contains>
            else ()
        }
        {
            if ($language = '*') then (
                $originalConcept/name[.//text()],
                $originalConcept/synonym[.//text()],
                $originalConcept/desc[.//text()],
                $originalConcept/source[.//text()],
                $originalConcept/rationale[.//text()],
                if ($concept[inherit | contains]) then $concept/comment[.//text()] else (),
                $originalConcept/comment[.//text()],
                $originalConcept/operationalization[.//text()]
            )
            else (
                let $name   := $originalConcept/name[@language=$language][.//text()][1]
                return 
                if ($name) then $name else (
                    <name language="{$language}">{$originalConcept/name[.//text()][1]/node()}</name>
                )
                ,
                $originalConcept/synonym[@language=$language][.//text()]
                ,
                let $desc   := $originalConcept/desc[@language=$language][.//text()][1]
                return 
                if ($desc) then $desc else (
                    <desc language="{$language}">{$originalConcept/desc[.//text()][1]/node()}</desc>
                )
                ,
                $originalConcept/source[@language=$language][.//text()][1],
                $originalConcept/rationale[@language=$language][.//text()][1],
                if ($concept[inherit | contains]) then $concept/comment[@language=$language][.//text()] else (),
                $originalConcept/comment[@language=$language][.//text()],
                $originalConcept/operationalization[@language=$language][.//text()][1]
            ), 
            $originalConcept/property (:new since 2015-04-21:)
            ,
            (:new since 2015-04-21:)
            for $node in $originalConcept/relationship
            let $relatedConcept           := utillib:getConcept($node/@ref, $node/@flexibility)
            let $relatedOriginalConcept   := utillib:getOriginalForConcept($relatedConcept)
            return
                <relationship>
                {
                    $node/@type, $node/@ref, $node/@flexibility, $node/@refdisplay,
                    $relatedConcept/ancestor::decor/project/@prefix,
                    if ($relatedConcept) then (
                        attribute datasetId {$relatedConcept/ancestor::dataset/@id},
                        attribute datasetEffectiveDate {$relatedConcept/ancestor::dataset/@effectiveDate},
                        attribute datasetStatusCode {$relatedConcept/ancestor::dataset/@statusCode},
                        if ($relatedConcept/ancestor::dataset[@expirationDate]) then attribute datasetExpirationDate {$relatedConcept/ancestor::dataset/@expirationDate} else (),
                        if ($relatedConcept/ancestor::dataset[@versionLabel]) then attribute datasetVersionLabel {$relatedConcept/ancestor::dataset/@versionLabel} else (),
                        attribute iType {$relatedOriginalConcept/@type}, 
                        attribute iStatusCode {$relatedConcept/@statusCode}, 
                        attribute iEffectiveDate {$relatedConcept/@effectiveDate},
                        if ($relatedConcept[@expirationDate]) then attribute iExpirationDate {$relatedConcept/@expirationDate} else (),
                        if ($relatedConcept[@versionLabel]) then attribute iVersionLabel {$relatedConcept/@versionLabel} else ()
                    ) else (),
                    if ($node[@refdisplay]) then () else (
                        let $iddisplay  := map:get($oidnamemap, $node/@ref)
                        let $iddisplay  := if (empty($iddisplay)) then utillib:getNameForOID($node/@ref, $relatedConcept/ancestor::decor/project/@defaultLanguage, $relatedConcept/ancestor::decor) else ($iddisplay)
                        return
                            attribute refdisplay {$iddisplay}
                    ),
                    $relatedOriginalConcept/name
                }
                </relationship>
            ,
            $originalConcept/operationalization[@language=$language][.//text()][1]
        }
        {
            (: this part is not necessary as we already solve the right stuff using utillib:getConceptAssociations() 
                with transaction concept as perspective when relevant :)
            (:(\:skip empty @strength attributes:\)
            for $node in $transactionconcept/terminologyAssociation[empty(@code)]
            return
                <terminologyAssociation>{$node/@*[not(. = '')], $node/node()}</terminologyAssociation>
            ,
            for $item in $transactionconcept/terminologyAssociation[@code]
            (\: Quite often the displayName is not present on terminologyAssociation, but is there in $concept/valueSet/concept
            If it is, we pick it up there to avoid hitting the expensive getDisplayNameFromCodesystem :\)
            let $displayName    := 
                if ($item/@displayName[not(. = '')]) then $item/@displayName else (
                    $valueDomainSets//concept[@code = $item/@code][@codeSystem = $item/@codeSystem]/@displayName |
                    $valueDomainSets//exception[@code = $item/@code][@codeSystem = $item/@codeSystem]/@displayName
                )[1]
            (\:let $displayName    := if (empty($displayName)) then (utillib:getDisplayNameFromCodesystem($item/@code, $item/@codeSystem, $language)) else $displayName:\)
            return
                <terminologyAssociation>
                {
                    $item/@*[not(. = '')],
                    if ($item/@displayName[not(. = '')]) then () else if (empty($displayName)) then () else (
                        attribute displayName {$displayName}
                    )
                }
                </terminologyAssociation>
            ,
            for $node in $transactionconcept/identifierAssociation
            return
                <identifierAssociation>{$node/@*[not(. = '')], $node/node()}</identifierAssociation>:)
            
            $valueDomainSets
            , 
            for $item in $conceptAssociations/terminologyAssociation
            (: Quite often the displayName is not present on terminologyAssociation, but is there in $concept/valueSet/concept
            If it is, we pick it up there to avoid hitting the expensive getDisplayNameFromCodesystem :)
            let $displayName    := 
                if ($item/@code) then
                    if ($item/@displayName[not(. = '')]) then () else (
                        $valueDomainSets//concept[@code = $item/@code][@codeSystem = $item/@codeSystem]/@displayName |
                        $valueDomainSets//exception[@code = $item/@code][@codeSystem = $item/@codeSystem]/@displayName
                    )[1]
                else ()
            (:let $displayName    := if (empty($displayName)) then (utillib:getDisplayNameFromCodesystem($item/@code, $item/@codeSystem, $language)) else $displayName:)
            order by $item/@conceptId/replace(replace(., '\.', '.0000000000'), '.0*([0-9]{9,})', '.$1'), $item/@codeSystem/replace(replace(concat(., '.'), '\.', '.0000000000'), '.0*([0-9]{9,})', '.$1'), $item/@valueSet/replace(replace(concat(., '.'), '\.', '.0000000000'), '.0*([0-9]{9,})', '.$1')
            return
                <terminologyAssociation>
                {
                    $item/@*[not(. = '')],
                    if (empty($displayName)) then () else (
                        attribute displayName {$displayName}
                    )
                }
                </terminologyAssociation>
            ,
            for $node in $conceptAssociations/identifierAssociation
            order by $node/@ref/replace(replace(concat(., '.'), '\.', '.0000000000'), '.0*([0-9]{9,})', '.$1')
            return
                <identifierAssociation>{$node/@*[not(. = '')], $node/node()}</identifierAssociation>
        }
        {
            if (empty($transactionconceptmap)) then (
                let $conceptset         := $concept/concept
                let $conceptnamemap2    := utillib:getConceptNameMap($conceptset, $language)
                    
                for $conceptChild in $conceptset
                return utillib:getFullConcept($decor, $conceptChild, $language, $xpaths, $xpathRow/@xpath, $oidnamemap, $transactionconceptmap, $conceptnamemap2)
            )
            else (
                let $conceptset         := $concept/concept[.[map:contains($transactionconceptmap, @id)] | descendant::concept[map:contains($transactionconceptmap, @id)]]
                let $conceptnamemap2    := utillib:getConceptNameMap($conceptset, $language)
                    
                for $conceptChild in $conceptset
                return utillib:getFullConcept($decor, $conceptChild, $language, $xpaths, $xpathRow/@xpath, $oidnamemap, $transactionconceptmap, $conceptnamemap2)
            )
        }
        </concept>
};

(:~ Traverses the fullDatasetTree to add community[@name] elements as child of a matching concept. code originated from RetrieveTransaction 

    @param $fullDatasetTree a dataset to merge community info into
    @param $communities is potentially empty list of communities to check for data associated to $fullDatasetTree concepts
    @param $communityIdMap is a map object that holds the mapping for 'foreign' projectId to projectPrefix. key is expected to be the projectId, the value is expected to be an array where array[1] = prefix and array[position() gt 1] = (currently undecided)
:)
declare function utillib:mergeDatasetTreeWithCommunity ($fullDatasetTree as element(), $communities as element()*, $communityIdMap as item()) as element(dataset) {
    element {name($fullDatasetTree)}
    {
        $fullDatasetTree/@*
        ,
        for $node in $fullDatasetTree/(* | comment())
        return 
            if ($node instance of element(concept)) then
                utillib:mergeConceptTreeWithCommunity($node, $communities, $communityIdMap)
            else ($node)
    }
};

(:~ Traverses the fullDatasetTree concept to add community[@name] elements as child of a matching concept. code originated from RetrieveTransaction
    
    @param $concept a dataset concept to merge community info into
    @param $communities is potentially empty list of communities to check for data associated to $concept
    @param $communityIdMap is a map object that holds the mapping for 'foreign' projectId to projectPrefix. key is expected to be the projectId, the value is expected to be an array where array[1] = prefix and array[position() gt 1] = (currently undecided)
:)
declare function utillib:mergeConceptTreeWithCommunity ($concept as element(concept), $communities as element()*, $communityIdMap as item()) as element(concept) {
    if (empty($communities)) then ($concept) else (
        let $communityInfo      := 
            $communities//object[@ref = $concept/@id][@flexibility = $concept/@effectiveDate] |
            $communities//object[@ref = $concept/@id][empty(@flexibility)]
        let $communityInfo      := $communityInfo/ancestor::association[data]
        
        return
        <concept>
        {
            $concept/@*,
            $concept/(node() except concept)
        }
        {
            for $commInfo in $communityInfo
            let $prototypes    :=
                if ($commInfo/ancestor::community/prototype/@ref) then 
                    doc(xs:anyURI($commInfo/ancestor::community/prototype/@ref))/prototype
                else
                    $commInfo/ancestor::community/prototype
            return
                <community name="{$commInfo/ancestor::community/@name}">
                {
                    (: 2019-07-22 New ... :)
                    (: allows distinction between relevant info from communities by the same name from different projects. 
                        Add only if info in $communityIdMap which should mean it is not from our transaction project :)
                    let $projectId      := $commInfo/ancestor::community/@projectId
                    let $projectIdProps := if ($projectId) then map:get($communityIdMap, $projectId) else ()
                    return (
                        if (empty($projectIdProps)) then () else (
                            (: don't write @projectId as that clashes with main queries on community like decor:getDecorCommunity() :)
                            attribute projectRef {$projectId}
                            ,
                            attribute ident {$projectIdProps[1]}
                        )
                    )
                }
                {
                    for $association in $commInfo/data
                    let $assocType  := $association/@type
                    let $typeDef    := $prototypes/data[@type = $assocType]
                    return
                        <data>
                        {
                            $assocType,
                            $typeDef/(@* except @type),
                            $association/node()
                        }
                        </data>
                }
                </community>
        }
        {
            for $node in $concept/concept
            return 
                utillib:mergeConceptTreeWithCommunity($node, $communities, $communityIdMap)
        }
        </concept>
    )
};

(:~ Returns a name which (in most cases) should be acceptable as XML element or SQL column name
    Most common diacritics are replaced
    
    Note: an implementation of this function exists in XSL too in the ADA package shortName.xsl. Changes here should be reflected there too and vice versa.
    
    Input:  xs:string, example: "Underscored Lowercase ë"
    Output: xs:string, example: "underscored_lowercase_e"
    
    @author Marc de Graauw, Alexander Henket
    @since 2013
:)
declare function utillib:shortName($name as xs:string?) as xs:string? {
    let $shortname := 
        if ($name) then (
            (: add some readability to CamelCased names. E.g. MedicationStatement to Medication_Statement. :)
            let $r0 := replace($name, '([a-z])([A-Z])', '$1_$2')
            
            (: find matching alternatives for <=? smaller(equal) and >=? greater(equal) :)
            let $r1 := replace($r0, '<\s*=', 'le')
            let $r2 := replace($r1, '<', 'lt')
            let $r3 := replace($r2, '>\s*=', 'ge')
            let $r4 := replace($r3, '>', 'gt')
            
            (: find matching alternatives for more or less common diacriticals, replace single spaces with _ , replace ? with q (same name occurs quite often twice, with and without '?') :)
            let $r5 := translate(normalize-space(lower-case($r4)),' àáãäåèéêëìíîïòóôõöùúûüýÿç€ßñ?','_aaaaaeeeeiiiiooooouuuuyycEsnq')
            (: ditch anything that's not alpha numerical or underscore :)
            let $r6 := replace($r5,'[^a-zA-Z\d_]','')
            (: make sure we do not start with a digit :)
            let $r7 := replace($r6, '^(\d)' , '_$1')
            return $r7
        ) else ()
    
    return if (matches($shortname, '^[a-zA-Z_][a-zA-Z\d_]+$')) then $shortname else ()
};

(:~ Returns an enhanced valueSet for a conceptList
    Input:  collection (take care to send more than project if conceptList/@ref points outside project)
            conceptList (from a dataset/concept/valueDomain)
            language, used to filter names, may be '' for no filtering
    Output: 
        valueSet with attributes from valueSet:
            <valueSet id="2.16.840.1.113883.2.4.3.46.99.3.11.5" effectiveDate="2012-07-25T15:22:56" name="tfw-meting-door" displayName="tfw-meting-door" statusCode="draft">
        conceptList with id from concept:
                <conceptList id="2.16.840.1.113883.2.4.3.46.99.3.2.55.0">
        all concepts from valueSet, 
        if a terminologyAssociation for concept exists, name from dataset, filtered by language if $language is not ''
                    <concept localId="1" code="P" codeSystem="2.16.840.1.113883.2.4.3.46.99.50" displayName="displayName-from-valueset" level="0" type="L">
                        <name language="nl-NL">Name from dataset conceptList's concept</name>
                    </concept>
        if no terminologyAssociation for concept exists, <name> is taken from @displayName in valueSet
                    <exception localId="6" code="OTH" codeSystem="2.16.840.1.113883.5.1008" displayName="Anders" level="0" type="L">
                        <name>Anders</name>
                    </exception>
                </conceptList>
            </valueSet>
        if no terminologyAssociation for valueSet exists, returns empty valueSet/conceptList:
            <valueSet><conceptList id="2.16.840.1.113883.2.4.3.46.99.3.2.55.0"/></valueSet>
        localId is a unique number within this particular conceptList (since code without codeSystem may not be unique) for use in code generators
        localId is only guaranteed to be unique within a single call, may change after dataset changes
        
    @author Marc de Graauw, Alexander Henket 
    @since 2013
:)
declare function utillib:getEnhancedValueSet($decor as element(decor), $conceptList as element(), $language as xs:string) as element()* {
    let $concept    := $conceptList/ancestor::concept[1]
    let $assocs     := utillib:getConceptAssociations($concept, $concept, false(), 'normal', true())
    return
    utillib:getEnhancedValueSet($decor, $conceptList, $assocs, $language)
};
declare function utillib:getEnhancedValueSet($decor as element(decor), $conceptList as element(), $conceptAssociations as element()*, $language as xs:string) as element()* {
    let $prefix                     := $decor/project/@prefix
    let $language                   := ($decor/@language[not(. = ('*', ''))], $decor/project/@defaultLanguage)[1]
    let $conceptList                := utillib:getOriginalConceptList($conceptList)
    let $conceptListAssociations    := $conceptAssociations/terminologyAssociation[@conceptId=$conceptList/@id][empty(@expirationDate)]
    let $conceptAssociations        := $conceptAssociations/terminologyAssociation[@conceptId=$conceptList/concept/@id][empty(@expirationDate)]
    
    return
        if ($conceptListAssociations) then (
            for $association in $conceptListAssociations
            let $flexibility        := if ($association/@flexibility) then $association/@flexibility else 'dynamic'
            let $valueSet           :=
                if ($association[string-length(@valueSet)=0]) then
                    error(xs:QName('error:NotEnoughArguments'),concat('Terminology association for concept ',$association/@conceptId,' is missing a valueSet.'))
                else (
                    let $vs         := (vsapi:getValueSetById($association/@valueSet, $flexibility, $prefix, $decor/@versionDate, $language), vsapi:getValueSetById($association/@valueSet, $flexibility))[1]                    
                    let $check := if (empty($vs)) then error(xs:QName('error:NotEnoughArguments'),concat('Terminology association value set ',$association/@valueSet,' is missing.')) else ()
                    return vsapi:getValueSetExtracted($vs, $prefix, $decor/@versionDate, $language)
                )
            return (
                <valueSet>
                {
                    $valueSet[1]/(@* except (@strength)),
                    $association/@strength[not(.='')],
                    (:skip empty @strength attributes:)
                    for $node in $association
                    return
                        element {name($node)} {$node/@*[not(. = '')], $node/node()}
                    ,
                    for $node in $valueSet[1]/(* except conceptList)
                    return
                        if (name($node) = 'desc') then utillib:parseNode($node) else
                        if (name($node) = 'revisionHistory') then
                            <revisionHistory>
                            {
                                $node/@*
                                ,
                                for $subnode in $node/desc
                                return
                                    utillib:parseNode($subnode)
                            }
                            </revisionHistory>
                        else (
                            $node
                        )
                    ,
                    <conceptList id="{$conceptList/@id}">
                    {
                        for $concept at $pos in $valueSet[1]/conceptList/*
                        (: rewrite concept with NullFlavor to exception. NullFlavor never is a concept :)
                        let $conceptName            := 
                            if ($concept[self::concept][@codeSystem = '2.16.840.1.113883.5.1008']) then 
                                'exception'
                            else (name($concept))
                        let $conceptAssociation     := $conceptAssociations[@code = $concept/@code][@codeSystem = $concept/@codeSystem]
                        let $conceptListConcept     := if ($conceptAssociation) then $conceptList/concept[@id = $conceptAssociation/@conceptId] else ()
                        let $conceptDesignations    := 
                            if ($language = '*') then $concept/designation else (
                                (: SNOMED CT does not do full lang-COUNTRY, just lang :)
                                $concept/designation[@language = $language] | $concept/designation[@language = substring-before($language, '-')]
                            )
                        return 
                            element {$conceptName}
                            {
                                attribute localId {$pos},
                                $concept/(@* except (@localId|@exception|@equivalence)),
                                (: rewrite/add @exception of includes for the HL7 ValueSet for 
                                NullFlavor to exception="true". NullFlavor never is a concept :)
                                if ($concept[self::include][@ref = '2.16.840.1.113883.1.11.10609'][not(@exception = 'true')]) then
                                    attribute exception {'true'}
                                else ($concept/@exception)
                                ,
                                $conceptAssociation[1]/@equivalence[not(.='')]
                                ,
                                if ($language = '*') then (
                                    $conceptListConcept/name,
                                    for $lang in $conceptList/parent::concept/@language
                                    return
                                        if ($conceptListConcept/name[@language = $lang]) then () else
                                        if ($conceptDesignations[@type = 'fsn']) then
                                            <name language="{$lang}">{data($conceptDesignations[@language = ($lang, substring-before($lang, '-'))][@type = 'fsn'][1]/@displayName)}</name>
                                        else
                                        if ($conceptDesignations[@type = 'preferred']) then
                                            <name language="{$lang}">{data($conceptDesignations[@language = ($lang, substring-before($lang, '-'))][@type = 'preferred'][1]/@displayName)}</name>
                                        else (
                                            <name language="{$lang}">{data($concept/(@displayName, @codeSystemName)[1])}</name>
                                        )
                                    ,
                                    for $synonym in $conceptListConcept/synonym
                                    return
                                        <designation language="{$synonym/@language}" type="synonym" displayName="{data($synonym)}"/>
                                    ,
                                    $conceptDesignations
                                )
                                else if ($conceptListConcept) then (
                                    if ($conceptListConcept/name[@language = $language]) then 
                                        $conceptListConcept/name[@language = $language]
                                    else
                                    if ($conceptDesignations[@type = 'fsn']) then
                                        <name language="{$language}">{data($conceptDesignations[@type = 'fsn'][1]/@displayName)}</name>
                                    else
                                    if ($conceptDesignations[@type = 'preferred']) then
                                        <name language="{$language}">{data($conceptDesignations[@type = 'preferred'][1]/@displayName)}</name>
                                    else (
                                        <name language="{$language}">{data($concept/(@displayName, @codeSystemName)[1])}</name>
                                    )
                                    ,
                                    for $synonym in $conceptListConcept/synonym[@language = $language]
                                    return
                                        <designation language="{$language}" type="synonym" displayName="{data($synonym)}"/>
                                    ,
                                    $conceptDesignations
                                )
                                else (
                                    if ($conceptDesignations[@type = 'fsn']) then
                                        <name language="{$language}">{data($conceptDesignations[@type = 'fsn'][1]/@displayName)}</name>
                                    else
                                    if ($conceptDesignations[@type = 'preferred']) then
                                        <name language="{$language}">{data($conceptDesignations[@type = 'preferred'][1]/@displayName)}</name>
                                    else (
                                        <name language="{$language}">{data($concept/(@displayName, @codeSystemName)[1])}</name>
                                    )
                                    ,
                                    $conceptDesignations
                                )
                                ,
                                if ($language = '*') then (
                                    for $node in $concept/desc
                                    return
                                        if (name($node)) then utillib:parseNode($node) else *
                                ) else (
                                    for $node in $concept/desc[@language = $language]
                                    return
                                        if (name($node)) then utillib:parseNode($node) else *
                                )
                            }
                    }
                    </conceptList>
                }
                </valueSet>
            )
        )
        else (
            (: concept list is not related to a valueSet :)
            (:<valueSet><conceptList id="{$conceptList/@id}"/></valueSet>:)
        )
};

(:~ Xquery function for retrieving the concept tree of a dataset for navigation purposes.
   Requires either:
   - id of a dataset.
   - transactionId of a transaction containing a representingTemplate
   Returns the concept hierarchy for the dataset, 
   inherits are resolved to retrieve the name of the concept.
   Providing a transactionId will return the cardinalities and conformance of the concept
   and an attribute absent='true' on concepts not in the representingTemplate
:)
declare function utillib:getDatasetTree($datasetId as xs:string?, $transactionId as xs:string?) as element(dataset)? { 
    utillib:getDatasetTree($datasetId, (), $transactionId, (), true(), ())
};
declare function utillib:getDatasetTree($datasetId as xs:string?, $datasetEffectiveDate as xs:string?, $transactionId as xs:string?, $transactionEffectiveDate as xs:string?) as element(dataset)? {
    utillib:getDatasetTree($datasetId, $datasetEffectiveDate, $transactionId, $transactionEffectiveDate, true(), ())
};
declare function utillib:getDatasetTree($datasetId as xs:string?, $datasetEffectiveDate as xs:string?, $transactionId as xs:string?, $transactionEffectiveDate as xs:string?, $fullTree as xs:boolean) as element(dataset)? {
    utillib:getDatasetTree($datasetId, $datasetEffectiveDate, $transactionId, $transactionEffectiveDate, $fullTree, ())
};
(:~  In a number of cases like RetrieveTransaction and template mapping we do not care about absent concepts to just omit those when $fullTree=false()
    Note: only active concepts are traversed. A concept with an absent parent is not retrieved. This situation should never occur.
:)
declare function utillib:getDatasetTree($datasetId as xs:string?, $datasetEffectiveDate as xs:string?, $transactionId as xs:string?, $transactionEffectiveDate as xs:string?, $fullTree as xs:boolean, $propertiesMap as map(*)?) as element(dataset)? {
    let $representingTemplate   :=
        if (string-length($transactionId)=0) then () else (
            utillib:getTransaction($transactionId, $transactionEffectiveDate)/representingTemplate
        )
    
    let $dataset                := 
        if ($representingTemplate[@sourceDataset]) then
            utillib:getDataset($representingTemplate/@sourceDataset, $representingTemplate/@sourceDatasetFlexibility)
        else
        if (string-length($datasetId) gt 0) then
            utillib:getDataset($datasetId, $datasetEffectiveDate)
        else ()
    
    let $projectPrefix          := $dataset/ancestor::decor/project/@prefix
    let $defaultLanguage        := $dataset/ancestor::decor/project/@defaultLanguage
    let $projectLanguages       := $dataset/ancestor::decor/project/name/@language
    
    return
    if ($dataset) then 
        <dataset>
        {
            $dataset/@id | $dataset/@effectiveDate | $dataset/@statusCode | $dataset/@versionLabel | $dataset/@expirationDate | $dataset/@officialReleaseDate | $dataset/@canonicalUri | $dataset/@lastModifiedDate
            ,
            attribute iddisplay {utillib:getNameForOID($dataset/@id, (), $dataset/ancestor::decor)}
            ,
            if ($representingTemplate) then (
                attribute transactionId {$representingTemplate/parent::transaction/@id}, 
                attribute transactionEffectiveDate {$representingTemplate/parent::transaction/@effectiveDate},
                attribute transactionStatusCode {$representingTemplate/parent::transaction/@statusCode},
                if ($representingTemplate/parent::transaction/@expirationDate) then
                    attribute transactionExpirationDate {$representingTemplate/parent::transaction/@expirationDate}
                else (),
                if ($representingTemplate/parent::transaction/@versionLabel) then
                    attribute transactionVersionLabel {$representingTemplate/parent::transaction/@versionLabel}
                else (),
                if ($representingTemplate[@ref]) then (
                    attribute templateId {$representingTemplate/@ref},
                    if ($representingTemplate[@flexibility]) then
                        attribute templateEffectiveDate {$representingTemplate/@flexibility}
                    else ()
                ) else ()
            ) else ()
        }
        {
            $dataset/name
            ,
            for $lang in $projectLanguages
            return
                if ($dataset/name[@language = $lang]) then () else (<name language="{$lang}"/>)
            ,
            for $desc in $dataset/desc
            return
                utillib:serializeNode($desc)
            ,
            for $lang in $projectLanguages
            return
                if ($dataset/desc[@language = $lang]) then () else (<desc language="{$lang}"/>)
            ,
            (:new since 2015-04-21:)
            for $n in $dataset/property 
            return 
                utillib:serializeNode($n)
            ,
            (:new since 2015-04-21:)
            for $relationship in $dataset/relationship
            let $referredDataset    :=  
                if (string-length($relationship/@ref)=0) then () else (
                    utillib:getDataset($relationship/@ref,$relationship/@flexibility)
                )
            return
                <relationship type="{$relationship/@type}" ref="{$relationship/@ref}" flexibility="{$relationship/@flexibility}">
                {
                    if ($referredDataset) then (
                        $referredDataset/ancestor::decor/project/@prefix,
                        attribute iStatusCode {$referredDataset/@statusCode}, 
                        if ($referredDataset/@expirationDate) then attribute iExpirationDate {$referredDataset/@expirationDate} else (),
                        if ($referredDataset/@versionLabel) then attribute iVersionLabel {$referredDataset/@versionLabel} else (),
                        attribute iddisplay {utillib:getNameForOID($relationship/@ref, $defaultLanguage, $referredDataset/ancestor::decor)},
                        attribute localInherit {$referredDataset/ancestor::decor/project/@prefix=$projectPrefix},
                        $referredDataset/name,
                        if ($referredDataset/name[@language=$defaultLanguage]) then () else (
                            <name language="{$defaultLanguage}">{data($referredDataset/name[1])}</name>
                        )
                    ) else ()
                }
                </relationship>
        }
        {
            for $concept in $dataset/concept
            return
                if ($representingTemplate) then
                    utillib:transactionConceptBasics($concept, $representingTemplate, $fullTree, $defaultLanguage, $propertiesMap)
                else (
                    utillib:conceptBasics($concept, false(), $propertiesMap)
                )
        }
        {
            $dataset/recycle
        }
        </dataset>
    else ()
};
declare function utillib:getConceptTree($conceptId as xs:string?, $conceptEffectiveDate as xs:string?, $transactionId as xs:string?, $transactionEffectiveDate as xs:string?, $fullTree as xs:boolean) as element(dataset)? {
    utillib:getConceptTree($conceptId, $conceptEffectiveDate, $transactionId, $transactionEffectiveDate, $fullTree, ())
};
declare function utillib:getConceptTree($conceptId as xs:string?, $conceptEffectiveDate as xs:string?, $transactionId as xs:string?, $transactionEffectiveDate as xs:string?, $fullTree as xs:boolean, $propertiesMap as map(*)?) as element(dataset)? {
    let $concept                := (utillib:getConcept($conceptId, $conceptEffectiveDate))[1]
    
    let $representingTemplate   :=
        if (string-length($transactionId)=0) then () else (
            utillib:getTransaction($transactionId, $transactionEffectiveDate)/representingTemplate
        )
    
    (: if our concept is child of a parent concept that inherits: is our concept represented in the original (missing-in-source = false) or has it been added (missing-in-source = true)? :)
    let $missing-in-source  := 
        if ($concept/parent::concept[inherit]) then (
            let $originalConcept    := utillib:getOriginalForConcept($concept/parent::concept)
            return
                if ($originalConcept[
                    concept[@id = $concept/inherit/@ref][@effectiveDate = $concept/inherit/@effectiveDate] |
                    concept[concat(@id, @effectiveDate) = $concept/relationship/concat(@ref, @flexibility)]
                ]) then false() else true()
        )
        else false()
    
    let $projectPrefix          := $concept/ancestor::decor/project/@prefix
    let $defaultLanguage        := $concept/ancestor::decor/project/@defaultLanguage
    let $projectLanguages       := $concept/ancestor::decor/project/name/@language
    
    return
    if ($concept) then 
        if ($representingTemplate) then
            utillib:transactionConceptBasics($concept, $representingTemplate, $fullTree, $defaultLanguage, $propertiesMap)
        else (
            utillib:conceptBasics($concept, false(), $propertiesMap)
        )
    else ()
};

(:~ Function for retrieving the basic scenario info for a scenario hierarchy, optionally filtered to include only scenarios/transations connected to a particular dataset
   Used for creating scenario navigation.
:)
declare function utillib:scenarioTree($scenario as element(scenario), $language as xs:string?) as element(scenario) {
    utillib:scenarioTree($scenario, $language, (), ())
};
declare function utillib:scenarioTree($scenario as element(scenario), $language as xs:string?, $datasetId as xs:string?, $datasetEffectiveDate as xs:string?) as element(scenario) {
    <scenario>
    {
        $scenario/@id | 
        $scenario/@effectiveDate | 
        $scenario/@statusCode | 
        $scenario/@versionLabel | 
        $scenario/@expirationDate | 
        $scenario/@officialReleaseDate |
        $scenario/@canonicalUri |
        $scenario/@lastModifiedDate
    }
    {
        attribute iddisplay {utillib:getNameForOID($scenario/@id, $language, $scenario/ancestor::decor)},
        if (empty($language)) then () else (
            if ($scenario/name[@language=$language]) then () else (
                let $n := if ($scenario/name[@language='en-US']) then $scenario/name[@language='en-US'] else ($scenario/name[1])
                return
                utillib:serializeNode(<name language="{$language}">{$n/node()}</name>)
            )
        )
        ,
        for $name in $scenario/name
        return
        utillib:serializeNode($name)
        ,
        let $filteredTransactions :=
            if (empty($datasetId))
            then $scenario/transaction
            else utillib:getTransactionsByScenarioAndDataset($scenario/@id, $scenario/@effectiveDate, $datasetId, $datasetEffectiveDate)/ancestor-or-self::transaction[parent::scenario]
        for $transaction in $filteredTransactions
        return
        utillib:transactionBasics($transaction, $language)
    }
    </scenario>
};

(:~ Recursive function for retrieving the basic transaction (group) info for a transaction hierarchy.
   Used for creating transaction navigation.
:)
declare function utillib:transactionBasics($transaction as element(transaction), $language as xs:string?) as element(transaction) {
    let $projectPrefix  := $transaction/ancestor::decor/project/@prefix
    return
    <transaction>
    {
        $transaction/@id | 
        $transaction/@effectiveDate | 
        $transaction/@statusCode | 
        $transaction/@versionLabel | 
        $transaction/@expirationDate | 
        $transaction/@officialReleaseDate | 
        $transaction/@type | 
        $transaction/@label | 
        $transaction/@model | 
        $transaction/@canonicalUri |
        $transaction/@lastModifiedDate
    }
    {
        attribute iddisplay {utillib:getNameForOID($transaction/@id, $language, $transaction/ancestor::decor)},
        (:these have not always been there. copy from ancestor scenario if missing:)
        if ($transaction[@statusCode]) then () else ($transaction/ancestor::scenario/@statusCode),
        if ($transaction[@effectiveDate]) then () else ($transaction/ancestor::scenario/@effectiveDate)
        ,
        if (empty($language)) then () else (
            if ($transaction/name[@language=$language]) then () else (
                let $n := if ($transaction/name[@language='en-US']) then $transaction/name[@language='en-US'] else ($transaction/name[1])
                return
                utillib:serializeNode(<name language="{$language}">{$n/node()}</name>)
            )
        )
        ,
        for $name in $transaction/name
        return
            utillib:serializeNode($name)
        ,
        if ($transaction[@type='group']) then (
            for $t in $transaction/transaction
            return
            utillib:transactionBasics($t, $language)
        ) else (
            $transaction/actors
            ,
            for $representingTemplate in $transaction/representingTemplate
            return
                <representingTemplate>
                {
                    $representingTemplate/@ref | 
                    $representingTemplate/@flexibility | 
                    $representingTemplate/@sourceDataset | 
                    $representingTemplate/@sourceDatasetFlexibility |
                    $representingTemplate/@representingQuestionnaire | 
                    $representingTemplate/@representingQuestionnaireFlexibility
                }
                </representingTemplate>
        )
    }
    </transaction>
};

(:~ Recursive function for retrieving the basic concept info for a concept hierarchy.
   Used for creating dataset navigation.
   
   TODO A concept can be in any of these states:
    1. concept[@added]              -- a concept that was willfully added under an inherited group, 
                                        concept actually lives in the dataset
    2. concept[@absent]             -- a concept that was willfully omitted under an inherited group, 
                                        concept actually lives in the dataset and could be activated by deleting @absent at any time
    3. concept[@missing-in-source]  -- a concept exists in the current dataset, but not in the original concept group that the parent of the current concept inherits from, 
                                        concept does not live in the current dataset, but is created runtime to flag this exceptional state. 
                                        There are two possible reasons for this situation, both can only occur post inheriting:
                                        - Someone hand-edited the origin in the DECOR project file (you cannot do this using ART)
                                        - Someone inherited from a concept that inherits and this concept was updated to inherit from a different concept group
    
    A concept could have both @absent and @missing-in-source.
    
    4. concept[@missing-in-target]  -- a concept exists in the original concept group that the parent of the current concept inherits from, but not in the current dataset, 
                                        concept does not live in the current dataset, but is created runtime to flag this exceptional state. 
                                        There are two possible reasons for this situation, both can only occur post inheriting:
                                        - Someone edited the origin in the DECOR project file and added the concept (group)
                                        - Someone inherited from a concept that inherits and this concept was updated to inherit from a different concept group
    
    5. If the concept does not have any of the markers above, it should be considered a normal concept (group)
:)
declare function utillib:conceptBasics($concept as element(), $missing-in-source as xs:boolean?) as element(concept) {
    utillib:conceptBasics($concept, false(), ())
};
declare function utillib:conceptBasics($concept as element(), $missing-in-source as xs:boolean?, $propertiesMap as map(*)?) as element(concept) {
    (:let $inheritConcept     := if ($concept[inherit]) then utillib:getConcept($concept/inherit/@ref, $concept/inherit/@effectiveDate) else ():)
    (:let $originalConcept    := if ($concept[name]) then $concept else if ($inheritConcept[name]) then $inheritConcept else utillib:getOriginalForConcept($concept):)
    let $originalConcept    := if ($concept[name]) then $concept else utillib:getOriginalForConcept($concept)
    let $inheritConcept     := if ($concept[inherit]) then utillib:getConcept($concept/inherit/@ref, $concept/inherit/@effectiveDate) else ()
    
    return
        <concept>
        {
            (:useful for targeted nagivation in the tree. alternative is keeping @id/@effectiveDate together all the time:)
            attribute navkey {util:uuid()},
            $originalConcept/@type,
            $concept/(@* except (@type | @navkey))
        }
        {
            $concept/inherit,
            $concept/contains,
            $originalConcept/name,
            $originalConcept/synonym,
            if ($propertiesMap?desc) then $originalConcept/desc else (),
            if ($propertiesMap?source) then $originalConcept/source else (),
            if ($propertiesMap?rationale) then $originalConcept/source else (),
            if ($propertiesMap?comment) then $concept[inherit | contains]/comment[.//text()] | $originalConcept/comment else (),
            if ($propertiesMap?property) then $originalConcept/property else $originalConcept/property[@name = 'Keyword'],
            if ($propertiesMap?inheritedcontains) then (
                for $ic in $inheritConcept/contains
                return
                    <inheritedContains>
                    {
                        $ic/@*,
                        $ic/*
                    }
                    </inheritedContains>
            ) else (),
            if ($propertiesMap?relationship) then (
                (:new since 2015-04-21:)
                for $node in $originalConcept/relationship
                let $relatedConcept           := utillib:getConcept($node/@ref, $node/@flexibility)
                let $relatedOriginalConcept   := utillib:getOriginalForConcept($relatedConcept)
                return
                    <relationship>
                    {
                        $node/@type, $node/@ref, $node/@flexibility, $node/@refdisplay,
                        $relatedConcept/ancestor::decor/project/@prefix,
                        if ($relatedConcept) then (
                            attribute datasetId {$relatedConcept/ancestor::dataset/@id},
                            attribute datasetEffectiveDate {$relatedConcept/ancestor::dataset/@effectiveDate},
                            attribute datasetStatusCode {$relatedConcept/ancestor::dataset/@statusCode},
                            if ($relatedConcept/ancestor::dataset[@expirationDate]) then attribute datasetExpirationDate {$relatedConcept/ancestor::dataset/@expirationDate} else (),
                            if ($relatedConcept/ancestor::dataset[@versionLabel]) then attribute datasetVersionLabel {$relatedConcept/ancestor::dataset/@versionLabel} else (),
                            attribute iType {$relatedOriginalConcept/@type}, 
                            attribute iStatusCode {$relatedConcept/@statusCode}, 
                            attribute iEffectiveDate {$relatedConcept/@effectiveDate},
                            if ($relatedConcept[@expirationDate]) then attribute iExpirationDate {$relatedConcept/@expirationDate} else (),
                            if ($relatedConcept[@versionLabel]) then attribute iVersionLabel {$relatedConcept/@versionLabel} else ()
                        ) else ()
                        (:,
                        if ($node[@refdisplay]) then () else (
                            let $iddisplay  := map:get($oidnamemap, $node/@ref)
                            let $iddisplay  := if (empty($iddisplay)) then utillib:getNameForOID($node/@ref, $relatedConcept/ancestor::decor/project/@defaultLanguage, $relatedConcept/ancestor::decor) else ($iddisplay)
                            return
                                attribute refdisplay {$iddisplay}
                        ):)
                        ,
                        $relatedOriginalConcept/name
                    }
                    </relationship>
            ) else (),
            if ($propertiesMap?valuedomain) then 
                for $valueDomain in $originalConcept/valueDomain
                return
                    <valueDomain>
                    {
                        $valueDomain/@*, 
                        $valueDomain/conceptList,
                        $valueDomain/property[@*[string-length() gt 0]]
                        ,
                        for $ex in $valueDomain/example
                        return
                            <example type="{($ex/@type, 'neutral')[1]}">{$ex/@caption, $ex/node()}</example>
                    }
                    </valueDomain>
             else ()
            ,
            if ($concept[contains]) then ( (: don't venture into potential circular references :) ) else (
                for $c in $concept/concept
                return utillib:conceptBasics($c, false(), $propertiesMap)
            )
        }
        </concept>
};

(:~ Recursive function for retrieving the basic concept info for a concept hierarchy in the context of a representingTemplate.
   Adds @absent='true' to concepts not in representingTemplate.
   Used for creating dataset navigation and  by transaction editor
   In a number of cases like RetrieveTransaction and template mapping we do not care about absent concepts to just omit those when $fullTree=false()
   Note: only active concepts are traversed. A concept with an absent parent is not retrieved. This situation should never occur.
:)
declare function utillib:transactionConceptBasics($concept as element(concept), $representingTemplate as element(representingTemplate), $fullTree as xs:boolean, $defaultLanguage as xs:string) as element(concept)? {
    utillib:transactionConceptBasics($concept, $representingTemplate, $fullTree, $defaultLanguage, ())
};
declare function utillib:transactionConceptBasics($concept as element(concept), $representingTemplate as element(representingTemplate), $fullTree as xs:boolean, $defaultLanguage as xs:string, $propertiesMap as map(*)?) as element(concept)? {
    let $matchingConcept    := $representingTemplate/concept[@ref=$concept/@id] 
    let $matchingConcept    := if ($matchingConcept[@flexibility]) then $matchingConcept[@flexibility=$concept/@effectiveDate][1] else $matchingConcept[1]
    
    return
        if ($matchingConcept or $fullTree) then (
            let $originalConcept    := if ($concept[name]) then $concept else utillib:getOriginalForConcept($concept)
            let $isMandatory        := $matchingConcept/@isMandatory='true'
            let $conditions         := 
                for $condition in $matchingConcept/condition
                let $isMandatory    := $condition/@isMandatory='true'
                return
                <condition>
                {
                    attribute minimumMultiplicity {utillib:getMinimumMultiplicity($condition)},
                    attribute maximumMultiplicity {utillib:getMaximumMultiplicity($condition)},
                    attribute conformance {if ($isMandatory) then 'M' else ($condition/@conformance)},
                    attribute isMandatory {$isMandatory},
                    $condition/desc,
                    if ($condition[desc]) then () else if ($condition[string-length(.)=0]) then () else (
                        <desc language="{$defaultLanguage}">{$condition/text()}</desc>
                    )
                    }
                </condition>
            
            return 
            <concept>
            {
                (:useful for targeted nagivation in the tree. alternative is keeping @id/@effectiveDate together all the time:)
                attribute navkey {util:uuid()}
            }
            {
                $originalConcept/@type,
                $concept/(@* except (@type | @ref | @flexibility | @navkey))
                ,
                if ($matchingConcept) then ((:purposefully do not add anything here!!:)) else (attribute absent {'true'}),
                attribute minimumMultiplicity {if ($matchingConcept) then utillib:getMinimumMultiplicity($matchingConcept) else ('0')},
                attribute maximumMultiplicity {if ($matchingConcept) then utillib:getMaximumMultiplicity($matchingConcept) else ('*')},
                attribute conformance {if ($isMandatory) then 'M' else ($matchingConcept/@conformance)},
                attribute isMandatory {$isMandatory},
                $matchingConcept/@enableBehavior,
                $concept/inherit,
                $concept/contains,
                $originalConcept/name,
                $originalConcept/synonym,
                if ($propertiesMap?desc) then $originalConcept/desc else (),
                if ($propertiesMap?source) then $originalConcept/source else (),
                if ($propertiesMap?rationale) then $originalConcept/source else (),
                if ($propertiesMap?comment) then $concept[inherit | contains]/comment[.//text()] | $originalConcept/comment else (),
                if ($propertiesMap?property) then $originalConcept/property else $originalConcept/property[@name = 'Keyword'],
                if ($propertiesMap?relationship) then (
                    (:new since 2015-04-21:)
                    for $node in $originalConcept/relationship
                    let $relatedConcept           := utillib:getConcept($node/@ref, $node/@flexibility)
                    let $relatedOriginalConcept   := utillib:getOriginalForConcept($relatedConcept)
                    return
                        <relationship>
                        {
                            $node/@type, $node/@ref, $node/@flexibility, $node/@refdisplay,
                            $relatedConcept/ancestor::decor/project/@prefix,
                            if ($relatedConcept) then (
                                attribute datasetId {$relatedConcept/ancestor::dataset/@id},
                                attribute datasetEffectiveDate {$relatedConcept/ancestor::dataset/@effectiveDate},
                                attribute datasetStatusCode {$relatedConcept/ancestor::dataset/@statusCode},
                                if ($relatedConcept/ancestor::dataset[@expirationDate]) then attribute datasetExpirationDate {$relatedConcept/ancestor::dataset/@expirationDate} else (),
                                if ($relatedConcept/ancestor::dataset[@versionLabel]) then attribute datasetVersionLabel {$relatedConcept/ancestor::dataset/@versionLabel} else (),
                                attribute iType {$relatedOriginalConcept/@type}, 
                                attribute iStatusCode {$relatedConcept/@statusCode}, 
                                attribute iEffectiveDate {$relatedConcept/@effectiveDate},
                                if ($relatedConcept[@expirationDate]) then attribute iExpirationDate {$relatedConcept/@expirationDate} else (),
                                if ($relatedConcept[@versionLabel]) then attribute iVersionLabel {$relatedConcept/@versionLabel} else ()
                            ) else ()
                            (:,
                            if ($node[@refdisplay]) then () else (
                                let $iddisplay  := map:get($oidnamemap, $node/@ref)
                                let $iddisplay  := if (empty($iddisplay)) then utillib:getNameForOID($node/@ref, $relatedConcept/ancestor::decor/project/@defaultLanguage, $relatedConcept/ancestor::decor) else ($iddisplay)
                                return
                                    attribute refdisplay {$iddisplay}
                            ):)
                            ,
                            $relatedOriginalConcept/name
                        }
                        </relationship>
                ) else (),
                if ($propertiesMap?valuedomain) then
                    for $valueDomain in $originalConcept/valueDomain
                    return
                        <valueDomain>
                        {
                            $valueDomain/@*, 
                            $valueDomain/conceptList,
                            $valueDomain/property[@*[string-length() gt 0]]
                            ,
                            for $ex in $valueDomain/example
                            return
                                <example type="{($ex/@type, 'neutral')[1]}">{$ex/@caption, $ex/node()}</example>
                        }
                        </valueDomain>
                else (),
                for $node in $matchingConcept/context
                return
                    utillib:serializeNode($node)
                ,
                $conditions,
                $matchingConcept/enableWhen,
                $matchingConcept/terminologyAssociation,
                $matchingConcept/identifierAssociation,
                if ($concept[contains]) then ( (: don't venture into potential circular references :) ) else (
                    for $c in $concept/concept
                    return utillib:transactionConceptBasics($c, $representingTemplate, $fullTree, $defaultLanguage, $propertiesMap)
                )
            }
            </concept>
        ) else ()
};

(:~ Function for getting all details of a concept without its children if any. In the case of a concept that inherits/contains, 
the whole concept is in fact readonly, and the only property that is actually of this concept, might be comment. To distinguish 
between this concepts comments and those of the original, the original comments are added as "inheritedComment".
    
Originally this code was part of package art in modules/get-decor-concept.xq
@param $authmap optional. map of user credentials. This determines access to community info for example
@param $concept required. dataset or transaction concept
@param $associationMode. required. Include associations or not
:)
declare function utillib:conceptExpanded($authmap as map(*)?, $concept as element(concept)?, $associationMode as xs:boolean) as element(concept)? {
    let $projectVersion     := $concept/ancestor::decor/@versionDate
    let $projectId          := $concept/ancestor::decor/project/@id
    let $projectPrefix      := $concept/ancestor::decor/project/@prefix
    let $language           := $concept/ancestor::decor/project/@defaultLanguage
    
    let $trid               := $concept/ancestor::transaction[1]/@id
    let $tred               := $concept/ancestor::transaction[1]/@effectiveDate
        
    let $transactionConcept := if ($concept/ancestor::transaction) then $concept else ()
    let $concept            := if ($concept/ancestor::transaction) then utillib:getConcept($concept/@ref, $concept/@flexibility, $projectVersion, $concept/ancestor::decor/@language) else $concept

    let $associations       :=
        if ($associationMode) then 
            if ($transactionConcept) then 
                utillib:getConceptAssociations($transactionConcept, utillib:getOriginalForConcept($concept), false(), 'all', true()) 
            else
            if ($concept) then 
                utillib:getConceptAssociations($concept, utillib:getOriginalForConcept($concept), false(), 'normal', true()) 
            else ()
        else ()
    
    (:  usually you contain/inherit from the original, but if you contain/inherit from something that inherits, then these two will differ
        originalConcept has the type and the associations. The rest comes from the inheritConcept/containConcept :)
    let $inheritConcept   := if ($concept[inherit]) then utillib:getConcept($concept/inherit/@ref, $concept/inherit/@effectiveDate) else ()
    let $containConcept   := if ($concept[contains]) then utillib:getConcept($concept/contains/@ref, $concept/contains/@flexibility) else ()
    let $originalConcept  := utillib:getOriginalForConcept($concept)

    let $communityInfo    := 
        if ($projectId) then (
            decorlib:getDecorCommunity((), $projectId, $projectVersion)//association[object[@ref = ($concept/@id, $originalConcept/@id)][@flexibility = ($concept/@effectiveDate, $originalConcept/@effectiveDate)]]
        ) else ()

    let $iddisplay        := utillib:getNameForOID($concept/@id, $language, $concept/ancestor::decor)
    return
    if (empty($concept)) then () else
    if (empty($projectVersion)) then
        <concept>
        {
            $concept/@id,
            $concept/@effectiveDate,
            $concept/@statusCode,
            $originalConcept/@type,
            $concept/@expirationDate,
            $concept/@officialReleaseDate,
            $concept/@versionLabel,
            $concept/@canonicalUri,
            $concept/@lastModifiedDate,
            if (string-length($iddisplay) = 0) then () else (
                attribute iddisplay {$iddisplay}
            ),
            if ($transactionConcept) then (
                attribute minimumMultiplicity {if ($transactionConcept) then utillib:getMinimumMultiplicity($transactionConcept) else ('0')},
                attribute maximumMultiplicity {if ($transactionConcept) then utillib:getMaximumMultiplicity($transactionConcept) else ('*')},
                if ($transactionConcept/@isMandatory='true') then
                    attribute conformance {'M'}
                else (
                    $transactionConcept/@conformance[not(.='')]
                ),
                attribute isMandatory {$transactionConcept/@isMandatory='true'},
                $transactionConcept/@enableBehavior[not(.='')]
            )
            else ()
        }
        {   
            if ($transactionConcept) then (
                 $transactionConcept/context
                ,
                (: element children, not concept &amp; history children, filtered for language :)
                for $condition in $transactionConcept/condition
                return
                <condition>
                {
                    attribute minimumMultiplicity {utillib:getMinimumMultiplicity($condition)},
                    attribute maximumMultiplicity {utillib:getMaximumMultiplicity($condition)},
                    if ($condition/@isMandatory='true') then
                        attribute conformance {'M'}
                    else (
                        $condition/@conformance[not(.='')]
                    ),
                    attribute isMandatory {$condition/@isMandatory='true'}
                    ,
                    $condition/desc
                    ,
                    if ($condition[desc]) then () else 
                    if ($condition[normalize-space() = '']) then () else (
                        <desc language="{$language}">{$condition/text()}</desc>
                    )
                    }
                </condition>
                ,
                $transactionConcept/enableWhen
                )
            else ()
        }
        {
            $associations/*[@conceptId = ($concept/@id | $concept/inherit/@ref | $concept/contains/@ref)],
            (: we return just a count of issues and leave it up to the calling party to get those if required :)
            <issueAssociation count="{count($setlib:colDecorData//issue/object[@id = $concept/@id][@effectiveDate = $concept/@effectiveDate])}"/>       
        }
        {
            if ($concept[inherit]) then 
                utillib:conceptExpandedInherit($concept/inherit, $inheritConcept, $originalConcept) 
            else ()
            ,
            if ($concept[contains]) then
                utillib:conceptExpandedContains($concept/contains, $containConcept, $originalConcept)
            else 
            if ($inheritConcept[contains]) then (
                utillib:conceptExpandedContains($inheritConcept/contains, $inheritConcept, utillib:getOriginalForConcept($inheritConcept))
            )
            else ()
        }
        {
            for $name in $originalConcept/name
            return
            utillib:serializeNode($name)
            ,
            for $synonym in $originalConcept/synonym
            return
            utillib:serializeNode($synonym)
            ,
            for $desc in $originalConcept/desc
            return
            utillib:serializeNode($desc)
            ,
            for $source in $originalConcept/source
            return
            utillib:serializeNode($source)
            ,
            for $rationale in $originalConcept/rationale
            return
            utillib:serializeNode($rationale)
            ,
            if ($concept[inherit | contains]) then 
                for $comment in $originalConcept/comment
                return
                <inheritedComment>{$comment/@*, utillib:serializeNode($comment)/node()}</inheritedComment>
            else ()
            ,
            for $comment in $concept/comment
            return
            utillib:serializeNode($comment)
            ,
            (:new since 2015-04-21:)
            for $property in $originalConcept/property
            return
            utillib:serializeNode($property)
            ,
            (:new since 2015-04-21:)
            for $relationship in $originalConcept/relationship
            let $referredConcept            := utillib:getConcept($relationship/@ref, $relationship/@flexibility)
            let $originalReferredConcept    := utillib:getOriginalForConcept($referredConcept)[1]
            return
                utillib:conceptExpandedRelationship($relationship, $referredConcept, $originalReferredConcept)
            ,
            for $operationalization in $originalConcept/operationalization
            return
            utillib:serializeNode($operationalization)
        }
        {
            for $valueDomain in $originalConcept/valueDomain
            return
                <valueDomain>
                {
                    $valueDomain/@*,
                    for $conceptList in $valueDomain/conceptList 
                    let $originalConceptList := utillib:getOriginalConceptList($conceptList)
                    return
                        <conceptList>
                        {
                            (:note that this will retain conceptList[@ref] if applicable:)
                            $conceptList/(@* except @conceptId),
                            if ($conceptList[@ref] and $originalConceptList) then (
                                attribute prefix {$originalConceptList/ancestor::decor/project/@prefix},
                                attribute conceptId {$originalConceptList/ancestor::concept[1]/@id},
                                attribute conceptEffectiveDate {$originalConceptList/ancestor::concept[1]/@effectiveDate},
                                attribute datasetId {$originalConceptList/ancestor::dataset[1]/@id},
                                attribute datasetEffectiveDate {$originalConceptList/ancestor::dataset[1]/@effectiveDate}
                            )
                            else (),
                            
                            $associations/*[@conceptId = $conceptList/(@ref | @id)],
                            
                            for $conceptListNode in $originalConceptList/*
                            return
                                if ($conceptListNode/self::concept) then (
                                    <concept>
                                    {
                                        $conceptListNode/@*,
                                        $associations/*[@conceptId = $conceptListNode/@id],
                                        $conceptListNode/name,
                                        $conceptListNode/synonym,
                                        for $node in $conceptListNode/desc
                                        return utillib:serializeNode($node)
                                    }
                                    </concept>
                                )
                                else (
                                    $conceptListNode
                                )
                        }
                        </conceptList>
                    ,
                    $valueDomain/property[@*[string-length() gt 0]]
                    ,
                    for $ex in $valueDomain/example
                    return
                        <example type="{($ex/@type, 'neutral')[1]}">{$ex/@caption, $ex/node()}</example>
                }
                </valueDomain>
        }
        {
            utillib:conceptExpandedCommunity($communityInfo)
            ,
            $concept/history
        }
        </concept>
    else (
        (: already compiled, don't redo :)
        <concept>
        {
            $concept/@*, 
            $concept/node(),
            if ($concept/community) then () else utillib:conceptExpandedCommunity($communityInfo)
        }
        </concept>
    )
};
(:~ Function to get a number of additional properties for a concept/inherit that helps a user understand what the inheritance is pointing to. 
    
@param $inherit concept/inherit element expected to carry ref and in most cases effectiveDate
@param $inheritConcept target concept of $inherit. If you already got that concept in the calling function, you may supply it, otherwise this function looks up the concept
@param $originalConcept original concept of $inheritConcept. If you already got that concept in the calling function, you may supply it, otherwise this function looks up the concept, which MAY be the very same concept as $inheritConcept
@return At the very least the properties already present on $contains are returned. If the target is found, additional properties are also included
    
    datasetId - inherited concept dataset id
    datasetEffectiveDate - inherited concept dataset effectiveDate
    iType - inherited concept type
    iStatusCode - inherited concept status
    iExpirationDate - inherited concept expirationDate (optional)
    iVersionLabel - inherited concept versionLabel (optional)
    iddisplay - human label for the inherited concept id
    localInherit - boolean which is true if the inheritance points to a concept in the same project
    
    if the $originalConcept is NOT the same as the $inheritConcept:
        originalId - original concept id
        originalEffectiveDate - original concept effectiveDate
        originalStatusCode - original concept statusCode
        originalExpirationDate - original concept expirationDate (optional)
        originalVersionLabel - original concept versionLabel (optional)
        originalPrefix - original concept project prefix
:)
declare function utillib:conceptExpandedInherit($inherit as element(inherit), $inheritConcept as element(concept)?, $originalConcept as element(concept)?) as element(inherit) {
    let $inheritConcept     := if ($inheritConcept) then $inheritConcept else utillib:getConcept($inherit/@ref, $inherit/@effectiveDate)
    let $originalConcept    := if ($originalConcept) then $originalConcept else utillib:getOriginalForConcept($inheritConcept)
    
    return
    <inherit>
    {
        $inherit/@ref,
        $inherit/@effectiveDate
    }
    {
        if ($inheritConcept) then (
            $inheritConcept/ancestor::decor/project/@prefix,
            attribute datasetId {$inheritConcept/ancestor::dataset/@id},
            attribute datasetEffectiveDate {$inheritConcept/ancestor::dataset/@effectiveDate},
            attribute iType {$originalConcept/@type}, 
            attribute iStatusCode {$inheritConcept/@statusCode}, 
            attribute iEffectiveDate {$inheritConcept/@effectiveDate},
            if ($inheritConcept[@expirationDate]) then attribute iExpirationDate {$inheritConcept/@expirationDate} else (),
            if ($inheritConcept[@versionLabel]) then attribute iVersionLabel {$inheritConcept/@versionLabel} else (),
            attribute iddisplay {utillib:getNameForOID($inherit/@ref, (), $inheritConcept/ancestor::decor)},
            attribute localInherit {$inheritConcept/ancestor::decor/project/@prefix = $inherit/ancestor::decor/project/@prefix},
            if ($inheritConcept[@id = $originalConcept/@id][@effectiveDate = $originalConcept/@effectiveDate]) then () else (
                attribute originalId {$originalConcept/@id}, 
                attribute originalEffectiveDate {$originalConcept/@effectiveDate}, 
                attribute originalStatusCode {$originalConcept/@statusCode}, 
                if ($originalConcept[@expirationDate]) then attribute originalExpirationDate {$originalConcept/@expirationDate} else (),
                if ($originalConcept[@versionLabel]) then attribute originalVersionLabel {$originalConcept/@versionLabel} else (),
                attribute originalPrefix {$originalConcept/ancestor::decor/project/@prefix}
            )
        ) else ()
    }
    </inherit>
};
(:~ Function to get a number of additional properties for a concept/contains that helps a user understand what the containment is pointing to. 
    
@param $contains concept/contains element expected to carry ref and in most cases flexibility
@param $containConcept target concept of $contains. If you already got that concept in the calling function, you may supply it, otherwise this function looks up the concept
@param $originalConcept original concept of $containConcept. If you already got that concept in the calling function, you may supply it, otherwise this function looks up the concept, which MAY be the very same concept as $containConcept
@return At the very least the properties already present on $contains are returned. If the target is found, additional properties are also included
    
    datasetId - contained concept dataset id
    datasetEffectiveDate - contained concept dataset effectiveDate
    iType - contained concept type
    iStatusCode - contained concept status
    iExpirationDate - contained concept expirationDate (optional)
    iVersionLabel - contained concept versionLabel (optional)
    iddisplay - human label for the contained concept id
    localInherit - boolean which is true if the containment points to a concept in the same project
    
    if the $originalConcept is NOT the same as the $containConcept:
        originalId - original concept id
        originalEffectiveDate - original concept effectiveDate
        originalStatusCode - original concept statusCode
        originalExpirationDate - original concept expirationDate (optional)
        originalVersionLabel - original concept versionLabel (optional)
        originalPrefix - original concept project prefix
:)
declare function utillib:conceptExpandedContains($contains as element(contains), $containConcept as element(concept)?, $originalConcept as element(concept)?) as element(contains) {
    let $containConcept     := if ($containConcept) then $containConcept else utillib:getConcept($contains/@ref, $contains/@flexibility)
    let $originalConcept    := if ($originalConcept) then $originalConcept else utillib:getOriginalForConcept($containConcept)
    
    return
    <contains>
    {
        $contains/@ref,
        $contains/@flexibility
    }
    {
        if ($containConcept) then (
            $containConcept/ancestor::decor/project/@prefix,
            attribute datasetId {$containConcept/ancestor::dataset/@id},
            attribute datasetEffectiveDate {$containConcept/ancestor::dataset/@effectiveDate},
            attribute iType {$originalConcept/@type}, 
            attribute iStatusCode {$containConcept/@statusCode}, 
            attribute iEffectiveDate {$containConcept/@effectiveDate},
            if ($containConcept[@expirationDate]) then attribute iExpirationDate {$containConcept/@expirationDate} else (),
            if ($containConcept[@versionLabel]) then attribute iVersionLabel {$containConcept/@versionLabel} else (),
            attribute iddisplay {utillib:getNameForOID($contains/@ref, (), $containConcept/ancestor::decor)},
            attribute localInherit {$containConcept/ancestor::decor/project/@prefix = $contains/ancestor::decor/project/@prefix},
            if ($containConcept[@id = $originalConcept/@id][@effectiveDate = $originalConcept/@effectiveDate]) then () else (
                attribute originalId {$originalConcept/@id}, 
                attribute originalEffectiveDate {$originalConcept/@effectiveDate}, 
                attribute originalStatusCode {$originalConcept/@statusCode}, 
                if ($originalConcept[@expirationDate]) then attribute originalExpirationDate {$originalConcept/@expirationDate} else (),
                if ($originalConcept[@versionLabel]) then attribute originalVersionLabel {$originalConcept/@versionLabel} else (),
                attribute originalPrefix {$originalConcept/ancestor::decor/project/@prefix}
            )
        ) else ()
    }
    </contains>
    
};
(:~ Function to get a number of additional properties for a concept/relationship that helps a user understand what the relationship is pointing to. 
    
@param $relationship concept/relationship element expected to carry type and ref and in most cases flexibility
@param $referredConcept target concept of $relationship. If you already got that concept in the calling function, you may supply it, otherwise this function looks up the concept
@param $originalConcept original concept of $referredConcept. If you already got that concept in the calling function, you may supply it, otherwise this function looks up the concept, which MAY be the very same concept as $referredConcept
@return At the very least the properties already present on $relationship are returned. If the target is found, additional properties are also included
    
    datasetId - related concept dataset id
    datasetEffectiveDate - related concept dataset effectiveDate
    iType - related concept type
    iStatusCode - related concept statusCode
    iExpirationDate - related concept expirationDate (optional)
    iVersionLabel - related concept versionLabel (optional)
    iddisplay - human label for the related concept id
    localInherit - boolean which is true if the relationship points to a concept in the same project
    name - related concept name (could be multiple for different languages)
:)
declare function utillib:conceptExpandedRelationship($relationship as element(relationship), $referredConcept as element(concept)?, $originalConcept as element(concept)?) as element(relationship) {
    let $language                   := $relationship/ancestor::decor/project/@defaultLanguage
    let $referredConcept            := if ($referredConcept) then $referredConcept else utillib:getConcept($relationship/@ref, $relationship/@flexibility)
    let $originalReferredConcept    := if ($originalConcept) then $originalConcept else utillib:getOriginalForConcept($referredConcept)[1]
    return
        <relationship>
        {
            $relationship/@type,
            $relationship/@ref,
            $relationship/@flexibility
        }
        {
            if ($referredConcept) then (
                $referredConcept/ancestor::decor/project/@prefix,
                attribute datasetId {$referredConcept/ancestor::dataset/@id},
                attribute datasetEffectiveDate {$referredConcept/ancestor::dataset/@effectiveDate},
                attribute iType {$originalConcept/@type}, 
                attribute iStatusCode {$referredConcept/@statusCode}, 
                if ($referredConcept[@expirationDate]) then attribute iExpirationDate {$referredConcept/@expirationDate} else (),
                if ($referredConcept[@versionLabel]) then attribute iVersionLabel {$referredConcept/@versionLabel} else (),
                attribute iddisplay {utillib:getNameForOID($relationship/@ref, (), $referredConcept/ancestor::decor)},
                attribute localInherit {$referredConcept/ancestor::decor/project/@prefix = $relationship/ancestor::decor/project/@prefix},
                $originalConcept/name,
                if ($originalConcept/name[@language = $language]) then () else (
                    <name language="{$language}">{data($originalConcept/name[1])}</name>
                )
            ) else ()
        }
        </relationship>
};
(:~ Function to get community info on a concept (if any) 

@param $associationset Set of associations from one or more communities
@return community element carrying its name, displayName, desc, relevant prototypes, and relevant associations
:)
declare function utillib:conceptExpandedCommunity($associationset as element(association)*) as element(community)* {
    for $associations in $associationset
    let $communityPrefix := $associations/ancestor::community/@name
    group by $communityPrefix
    return
        <community name="{$communityPrefix}">
        {
            $associations[1]/ancestor::community/@displayName,
            $associations[1]/ancestor::community/desc,
            <prototype>
            {
                let $prototypes     :=
                    if ($associations[1]/ancestor::community/prototype/@ref) then 
                        doc(xs:anyURI($associations[1]/ancestor::community/prototype/@ref))/prototype
                    else
                        $associations[1]/ancestor::community/prototype
                
                return $prototypes/data[@type = $associations//data/@type]
            }
            </prototype>,
            <associations>
            {
                for $association in $associations
                return
                    <association>
                    {
                        $association/@*,
                        for $data in $association/data
                        return
                            utillib:serializeNode($data)
                    }
                    </association>
            }
            </associations>
        }
        </community>
};

(:~  Return all live repository/non-private data sets or from a specific project.
:
:   @param $projectPrefix   - optional project/@prefix
:   @return all live repository/non-private data sets, all data sets for the given $projectPrefix or nothing if not found
:   @since 2015-09-21
:)
declare function utillib:getDatasets($projectPrefix as xs:string?) as element(dataset)* {
    utillib:getDatasets($projectPrefix, (), ())
};
(:~  Return all repository/non-private data sets and/or from a specific project. Returns live data sets 
:   if param decorVersion is not a dateTime OR compiled, archived/released DECOR data sets based on 
:   project/@prefix and decor/@versionDate.
:   Released/compiled matches could be present in multiple languages. Use the parameters @language to select the right one.
:
:   @param $projectPrefix   - optional project/@prefix
:   @param $decorVersion    - optional decor/@versionDate
:   @param $language        - optional decor/@language. Only relevant with parameter $decorVersion. Empty uses decor/project/@defaultLanguage, '*' selects all versions, e.g. 'en-US' selects the US English version (if available)
:   @return all live repository/non-private data sets, all data sets for the given $projectPrefix, or all archived 
:           DECOR version instances for the given $projectPrefix and $decorVersion or nothing if not found
:   @since 2015-09-21
:)
declare function utillib:getDatasets($projectPrefix as xs:string?, $decorVersion as xs:string?) as element(dataset)* {
    utillib:getDatasets($projectPrefix, $decorVersion, ())
};
declare function utillib:getDatasets($projectPrefix as xs:string?, $decorVersion as xs:string?, $language as xs:string?) as element(dataset)* {
    utillib:getDecorByPrefix($projectPrefix, $decorVersion, $language)/datasets/dataset
};
(:~  Return the live data set based on id and optionally effectiveDate. If effectiveDate is empty, the latest version is returned
:
:   @param $ds-id   - required dataset/@id
:   @param $ds-ed   - optional dataset/@effectiveDate
:   @return exactly 1 dataset or nothing if not found
:   @since 2015-04-27
:)
declare function utillib:getDataset($ds-id as xs:string, $ds-ed as xs:string?) as element(dataset)? {
    utillib:getDataset($ds-id, $ds-ed, (), ())
};
(:~  Return the live data set if param decorVersion is not a dateTime OR 
:   compiled, archived/released DECOR data set based on dataset/@id|@effectiveDate and decor/@versionDate.
:   Released/compiled matches could be present in multiple languages. Use the parameters @language to select the right one.
:
:   @param $ds-id           - required dataset/@id
:   @param $ds-ed           - optional dataset/@effectiveDate
:   @param $decorVersion    - optional decor/@versionDate
:   @param $language        - optional decor/@language. Only relevant with parameter $decorVersion. Empty uses decor/project/@defaultLanguage, '*' selects all versions, e.g. 'en-US' selects the US English version (if available)
:   @return exactly 1 live data set, or 1 or more archived DECOR version instances or nothing if not found
:   @since 2015-04-29
:)
declare function utillib:getDataset($ds-id as xs:string, $ds-ed as xs:string?, $decorVersion as xs:string?) as element(dataset)* {
    utillib:getDataset($ds-id, $ds-ed, $decorVersion, ())
};
declare function utillib:getDataset($ds-id as xs:string, $ds-ed as xs:string?, $decorVersion as xs:string?, $language as xs:string?) as element(dataset)* {
    let $decor          :=
        if ($decorVersion[string-length() gt 0]) then
            let $d      := $setlib:colDecorVersion/decor[@versionDate = $decorVersion]
            return
            if (empty($language)) then
                ($d[@language = $d/project/@defaultLanguage], $d)[1]
            else if ($language='*') then
                $d
            else
                $d[@language = $language]
        else (
            $setlib:colDecorData/decor
        )
    
    let $datasets       := 
        if ($ds-ed castable as xs:dateTime) then
            let $c  := $decor//dataset[@id = $ds-id][@effectiveDate = $ds-ed]
            return $c[ancestor::datasets]
        else (
            let $c  := $decor//dataset[@id = $ds-id]
            let $c  := $c[ancestor::datasets]
            return $c[@effectiveDate = max($c/xs:dateTime(@effectiveDate))]
        )
    
    return
        $datasets
};
(:~ Return dataset or transaction data for projectindex purpose :)
declare function utillib:getDatasetExtract($datasetOrTransaction as element()*, $id as xs:string, $effectiveDate as xs:string?, $conceptId as xs:string?, $conceptEffectiveDate as xs:string?, $decorVersion as xs:string?, $language as xs:string?, $communityName as xs:string*) as element()* {

    let $datasetOrTransaction           := if (empty($language) or $language = '*') then ($datasetOrTransaction[ancestor::decor[@language = '*']], $datasetOrTransaction)[1] else $datasetOrTransaction[1]
    
    (: ==== PATCHZONE for live onco version ===== :)
    let $latestOncoVersion              := utillib:patchLatestOncoVersion($datasetOrTransaction, $decorVersion, $language)
    let $decorVersion                   := if ($latestOncoVersion) then $latestOncoVersion else $decorVersion
    let $datasetOrTransaction           := 
        if ($latestOncoVersion) then   
            utillib:getDataset($id, $effectiveDate, $latestOncoVersion) | 
            utillib:getTransaction($id, $effectiveDate, $latestOncoVersion) | 
            utillib:getConcept($id, $effectiveDate, $latestOncoVersion)/ancestor::dataset
        else $datasetOrTransaction
    (: ==== PATCHZONE for live onco version ===== :)
    
    let $language                       := if (empty($language)) then $datasetOrTransaction/ancestor::decor[1]/project/@defaultLanguage else $language
    (: fix for de-CH :)
    let $language                       := if ($language = 'de-CH') then 'de-DE' else $language
 
    let $fullDatasetTree                := 
        if (empty($decorVersion)) then utillib:getFullDatasetTree($datasetOrTransaction, $conceptId, $conceptEffectiveDate, $language, (), false(), (), $communityName)
        
        else 
            let $datasets               :=
                if ($datasetOrTransaction[self::dataset]) then $datasetOrTransaction 
                else if ($language = '*') then $setlib:colDecorVersion//transactionDatasets[@versionDate = $decorVersion]//dataset[@transactionId = $datasetOrTransaction/@id][@transactionEffectiveDate = $datasetOrTransaction/@effectiveDate]
                else $setlib:colDecorVersion//transactionDatasets[@versionDate = $decorVersion][@language = $language]//dataset[@transactionId = $datasetOrTransaction/@id][@transactionEffectiveDate = $datasetOrTransaction/@effectiveDate]
            let $datasets               :=
                if ($datasets and $conceptId) then (
                for $dataset in $datasets
                return
                    element {$dataset/name()} {
                        $dataset/@*,
                        $dataset/(* except (concept|history)),
                        $dataset//concept[@id = $conceptId]
                    }
                ) else ($datasets)
        
            return $datasets[1]

    let $projectId                      := $datasetOrTransaction/ancestor::decor[1]/project/@id
    let $community                      := 
        if (empty($projectId)) then () else if ($fullDatasetTree//community) then () else decorlib:getDecorCommunity($communityName, $projectId, $decorVersion)
    
    return
        if ($community) then utillib:mergeDatasetTreeWithCommunity($fullDatasetTree, $community, map {}) else $fullDatasetTree

};

(:~  Return concept based on id and optionally effectiveDate. If effectiveDate is empty, the latest version is returned
:
:   @param $de-id   - required concept/@id
:   @param $de-ed   - optional concept/@effectiveDate
:   @return exactly 1 concept or nothing if not found
:   @since 2015-04-27
:)
declare function utillib:getConcept($de-id as xs:string, $de-ed as xs:string?) as element(concept)? {
    utillib:getConcept($de-id, $de-ed, (), ())
};
(:~  Return the live concept if param decorVersion is not a dateTime OR 
compiled, archived/released DECOR concept based on concept/@id|@effectiveDate and decor/@versionDate.
Released/compiled matches could be present in multiple languages. Use the parameters @language to select the right one.

@param $de-id           - required concept/@id
@param $de-ed           - optional concept/@effectiveDate
@param $decorVersion    - optional decor/@versionDate
@param $language        - optional decor/@language. Only relevant with parameter $decorVersion. Empty uses decor/project/@defaultLanguage, '*' selects all versions, e.g. 'en-US' selects the US English version (if available)
Released/compiled matches could be present in multiple languages. Use the parameters @language to select the right one.
@return exactly 1 live concept, or 1 or more archived DECOR version instances or nothing if not found
@since 2015-04-29
:)
declare function utillib:getConcept($de-id as xs:string, $de-ed as xs:string?, $decorVersion as xs:string?) as element(concept)* {
    utillib:getConcept($de-id, $de-ed, $decorVersion, ())
};
declare function utillib:getConcept($de-id as xs:string, $de-ed as xs:string?, $decorVersion as xs:string?, $language as xs:string?) as element(concept)* {
    let $concepts       := 
        if ($decorVersion castable as xs:dateTime) then
            $setlib:colDecorVersion//concept[@id = $de-id]
        else (
            $setlib:colDecorData//concept[@id = $de-id]
        )
    
    
    let $concepts       := 
        (: are we in a release of the project? :)
        if ($decorVersion castable as xs:dateTime) then
            (: treat concepts as group per release, retuirning only those that belong to the right release:)
            for $concept in $concepts
            let $versionDate    := $concept/ancestor::decor/@versionDate
            group by $versionDate
            return
                if ($versionDate = $decorVersion) then (
                    (: now have any number of concepts that could have different versions :)
                    (: use requested effectiveDate $de-ed or calculate the newest one
                        <concept id="1" effectiveDate="2020" language="nl-NL"/>
                        <concept id="1" effectiveDate="2020" language="en-US"/>
                        <concept id="1" effectiveDate="2020" language="*"/>
                        
                        <concept id="1" effectiveDate="2021" language="nl-NL"/>
                        <concept id="1" effectiveDate="2021" language="en-US"/>
                        <concept id="1" effectiveDate="2021" language="*"/>
                    :)
                    let $effectiveDate      := if ($de-ed castable as xs:dateTime) then $de-ed else string(max($concept/xs:dateTime(@effectiveDate)))
                    let $concept            :=
                        for $de in $concept
                        return
                            if ($de[@effectiveDate = $de-ed]) then $de else ()
                    
                    (:  get all matching concepts for language '*', 
                        get first of matching concepts for default language or at all if language is empty,
                        get requested language if there is one (could lead to nothing) :)
                    return
                        if ($language = '*') then
                            $concept
                        else 
                        if (empty($language)) then (
                            ($concept[ancestor::decor[@language = project/@defaultLanguage]], $concept)[1]
                        )
                        else (
                            $concept[ancestor::decor[@language = $language]]
                        )
                )
                else ()
        else (
            (: we want 'live' versions of the concept :)
            (: use requested effectiveDate $de-ed or calculate the newest one :)
            let $effectiveDate  := if ($de-ed castable as xs:dateTime) then $de-ed else string(max($concepts/xs:dateTime(@effectiveDate)))
            
            for $concept in $concepts
            return
                if ($concept[@effectiveDate = $effectiveDate]) then $concept else ()
        )
    (:let $decor          :=
        if ($decorVersion castable as xs:dateTime) then (
            let $d      := $setlib:colDecorVersion/decor[@versionDate = $decorVersion]
            return
            if (empty($language)) then (
                ($d[@language = $d/project/@defaultLanguage], $d)[1]
            )
            else 
            if ($language = '*') then (
                $d
            )
            else (
                $d[@language = $language]
            )
        )
        else (
            $setlib:colDecorData
        )
    
    let $concepts       := 
        if ($de-ed castable as xs:dateTime) then
            $decor//concept[@id = $de-id][@effectiveDate = $de-ed]
        else (
            let $c  := $decor//concept[@id = $de-id]
            return $c[@effectiveDate = max($c/xs:dateTime(@effectiveDate))]
        ):)
    
    return
        $concepts[ancestor::datasets][not(ancestor::history)]
};

(:~ Return concept from a specific dataset. This is useful when you input concept is a transaction concept they may not have a flexibility specified. 
If we would just go and fetch the latest concept, you might end up with a concept from a different dataset because that is a later version.

@param $ds-id           - required dataset/@id
@param $ds-ed           - optional dataset/@effectiveDate
@param $de-id           - required concept/@id
@param $de-ed           - optional concept/@effectiveDate
@param $decorVersion    - optional decor/@versionDate
@param $language        - optional decor/@language. Only relevant with parameter $decorVersion. Empty uses decor/project/@defaultLanguage, '*' selects all versions, e.g. 'en-US' selects the US English version (if available)
Released/compiled matches could be present in multiple languages. Use the parameters @language to select the right one.
@return exactly 1 live concept, or 1 or more archived DECOR version instances or nothing if not found
@since 2015-04-29
:)
declare function utillib:getDatasetConcept($ds-id as xs:string, $ds-ed as xs:string?, $de-id as xs:string, $de-ed as xs:string?, $decorVersion as xs:string?, $language as xs:string?) as element(concept)* {
    let $dataset        := utillib:getDataset($ds-id, $ds-ed, $decorVersion, $language)
    
    let $concepts       := 
        if ($de-ed castable as xs:dateTime) then
            $dataset//concept[@id = $de-id][@effectiveDate = $de-ed]
        else (
            let $c  := $dataset//concept[@id = $de-id]
            return $c[@effectiveDate = max($c/xs:dateTime(@effectiveDate))]
        )
    
    return
        $concepts[not(ancestor::history)]
};

(:~ Return list of live repository/non-private scenarios or from a specific project
:
:   @param $projectPrefix   - optional project/@prefix
:   @param $listtype        - optional code.
:                               all     = all transactions
:                               ds      = only transactions that have a representingTemplate/@sourceDataset
:                               tm      = only transactions that have a representingTemplate/@ref
:                               qq      = only transactions that have a representingTemplate/@representingQuestionnaire
:                               dstm    = only transactions that have a representingTemplate with @sourceDataset and @ref
:                               dsqq    = only transactions that have a representingTemplate with @sourceDataset and @representingQuestionnaire
:                               tmqq    = only transactions that have a representingTemplate with @ref and @representingQuestionnaire
:                               dstmqq  = only transactions that have a representingTemplate with @sourceDataset and @ref and @representingQuestionnaire
:                               group   = only transactions of type group (transactions that group other transactions)
:                               item    = only transactions of type item (transactions that are stationary, initial or back)
:   @return all live repository/non-private scenarios, all scenarios for the given $projectPrefix or nothing if not found
:   @since 2017-09-12
:)
declare function utillib:getScenarioList($projectPrefix as xs:string?, $listtype as xs:string) as element(scenarios) {
    utillib:getScenarioList($projectPrefix, (), (), $listtype)
};
(:~ Return all repository/non-private scenarios or from a specific project. Returns live scenarios 
:   if param decorVersion is not a dateTime OR compiled, archived/released DECOR scenarios based on project/@prefix and decor/@versionDate.
:   Released/compiled matches could be present in multiple languages. Use the parameters @language to select the right one.
:
:   @param $projectPrefix   - optional project/@prefix
:   @param $decorVersion    - optional decor/@versionDate
:   @param $language        - optional decor/@language. Only relevant with parameter $decorVersion. Empty uses decor/project/@defaultLanguage, '*' selects all versions, e.g. 'en-US' selects the US English version (if available)
:   @param $listtype        - optional code.
:                               all     = all transactions
:                               ds      = only transactions that have a representingTemplate/@sourceDataset
:                               tm      = only transactions that have a representingTemplate/@ref
:                               qq      = only transactions that have a representingTemplate/@representingQuestionnaire
:                               dstm    = only transactions that have a representingTemplate with @sourceDataset and @ref
:                               dsqq    = only transactions that have a representingTemplate with @sourceDataset and @representingQuestionnaire
:                               tmqq    = only transactions that have a representingTemplate with @ref and @representingQuestionnaire
:                               dstmqq  = only transactions that have a representingTemplate with @sourceDataset and @ref and @representingQuestionnaire
:                               group   = only transactions of type group (transactions that group other transactions)
:                               item    = only transactions of type item (transactions that are stationary, initial or back)
:   @return all live repository/non-private scenarios, all scenarios for the given $projectPrefix, or all archived 
:           DECOR version instances for the given $projectPrefix and $decorVersion or nothing if not found
:   @since 2017-09-12
:)
declare function utillib:getScenarioList($projectPrefix as xs:string?, $decorVersion as xs:string?, $listtype as xs:string?) as element(scenarios) {
    utillib:getScenarioList($projectPrefix, $decorVersion, (), $listtype)
};
declare function utillib:getScenarioList($projectPrefix as xs:string?, $decorVersion as xs:string?, $language as xs:string?, $listtype as xs:string?) as element(scenarios) {
let $decor          := utillib:getDecorByPrefix($projectPrefix, $decorVersion, $language)
let $scenarios      := $decor/scenarios/scenario

(: if transactions is empty, this equates to listtype = 'all' and we return every scenario/transaction based on the other params :)
let $transactions   := 
    switch ($listtype)
    case 'ds'       return $scenarios//transaction[representingTemplate[@sourceDataset][concept]]
    case 'tm'       return $scenarios//transaction[representingTemplate[@ref][concept]]
    case 'qq'       return $scenarios//transaction[representingTemplate[@representingQuestionnaire][concept]]
    case 'dstm'     return $scenarios//transaction[representingTemplate[@sourceDataset][@ref][concept]]
    case 'dsqq'     return $scenarios//transaction[representingTemplate[@sourceDataset][@representingQuestionnaire][concept]]
    case 'tmqq'     return $scenarios//transaction[representingTemplate[@ref][@representingQuestionnaire][concept]]
    case 'dstmqq'   return $scenarios//transaction[representingTemplate[@sourceDataset][@ref][@representingQuestionnaire][concept]]
    case 'group'    return $scenarios//transaction[@type='group']
    case 'item'     return $scenarios//transaction[not(@type='group')]
    default return ()

(: build name maps because that is much faster than lookup :)
let $scenarionames  := 
    for $name in $scenarios
    let $versionLabel   := $name/../@versionLabel
    return
        string-join(($name, $versionLabel, $name/@language), '')
let $scnameentries  :=
    for $name in $scenarionames
    let $nm         := $name
    group by $nm
    return map:entry($name[1], count($name))
let $scnamemap      := map:merge($scnameentries)

(: get names for any transaction regardless of whether or not it is in scope :)
let $transactionnames   := 
    for $name in $scenarios//transaction/name
    let $versionLabel   := $name/../@versionLabel
    return
        string-join(($name, $versionLabel, $name/@language), '')
let $trnameentries  :=
    for $name in $scenarionames
    let $nm         := $name
    group by $nm
    return map:entry($name[1], count($name))
let $trnamemap      := map:merge($trnameentries)

let $sclist         := if ($transactions) then $scenarios/transaction[.//@id = $transactions/@id] else $scenarios

return
<scenarios listtype="{$listtype}">
{
    for $scenario in $scenarios
    let $ident          := $scenario/ancestor::decor/project/@prefix
    let $name           := if ($scenario/name[@language = $language]) then $scenario/name[@language = $language] else $scenario/name[1]
    let $versionLabel   := $scenario/@versionLabel
    order by lower-case($name)
    return
        <scenario>
        {
            $scenario/@id,
            $scenario/@effectiveDate,
            $scenario/@statusCode,
            $scenario/@versionLabel,
            $scenario/@expirationDate,
            $scenario/@officialReleaseDate,
            $scenario/@canonicalUri,
            $scenario/@lastModifiedDate,
            if ($ident = $projectPrefix) then () else (attribute ident {$ident}),
            attribute iddisplay {utillib:getNameForOID($scenario/@id, $language, $decor)}
        }
        {
            for $name in $scenario/name
            let $label      := if ($versionLabel) then concat(' (',$versionLabel,')') else ()
            let $labeldate  := 
                if ($scenario/@effectiveDate castable as xs:dateTime) then 
                    replace(format-dateTime(xs:dateTime($scenario/@effectiveDate),'[Y0001]-[M01]-[D01] [H01]:[m01]:[s01]', (), (), ()),' 00:00:00','')
                else ()
            (: functional people find the date attached to the name in the selector/drop down list in the UI confusing
                and it is not necessary when the name for a given language is unique enough. Often the effectiveDate
                is not very helpful at all and rather random. So we generate a language dependent name specifically for 
                the drop down selector that only concatenates the effectiveDate when the name for the given language is 
                not unique in itself.
            :)
            let $selectorName   := string-join(($name, $versionLabel, $name/@language), '')
            let $selectorName   := 
                if (map:get($scnamemap, $selectorName) gt 1) then 
                    concat($name, $label, ' :: ', $labeldate)
                else (
                    concat($name, $label)
                )
            return
                (: use this for a drop down selector :)
                <name>
                {
                    $name/@*,
                    attribute selectorName {$selectorName},
                    $name/node()
                }
                </name>
        }
        {
            let $trlist     := if ($transactions) then $scenario/transaction[.//@id = $transactions/@id] else $scenario/transaction
            
            for $transaction in $trlist
            return utillib:getTransactionList($transaction, $projectPrefix, $transactions, $trnamemap)
        }
        </scenario>
}
</scenarios>
};
(: Recursive function for retrieving the basic transaction (group) info for a transaction hierarchy.
   Used for creating transaction navigation.
:)
declare function utillib:getTransactionList($transaction as element(transaction), $projectPrefix as xs:string?, $filter as element()*, $trnamemap as item()) as element(transaction) {
    let $ident          := $transaction/ancestor::decor/project/@prefix
    let $effectiveDate  := if ($transaction[@effectiveDate]) then ($transaction/@effectiveDate) else ($transaction/ancestor::scenario/@effectiveDate)
    let $versionLabel   := $transaction/@versionLabel
    return
    <transaction>
    {
        $transaction/@id,
        (:effectiveDate and statusCode have not always been there. copy from ancestor scenario if missing:)
        $effectiveDate,
        if ($transaction[@statusCode]) then ($transaction/@statusCode) else ($transaction/ancestor::scenario/@statusCode),
        $versionLabel,
        $transaction/@expirationDate,
        $transaction/@officialReleaseDate,
        $transaction/@type,
        $transaction/@label,
        $transaction/@model,
        $transaction/@canonicalUri,
        $transaction/@lastModifiedDate,
        if ($ident = $projectPrefix) then () else (attribute ident {$ident}),
        attribute iddisplay {utillib:getNameForOID($transaction/@id, (), $transaction/ancestor::decor)}
    }
    {
        for $name in $transaction/name
        let $label      := if ($versionLabel) then concat(' (',$versionLabel,')') else ()
        let $labeldate  := 
            if ($effectiveDate castable as xs:dateTime) then 
                replace(format-dateTime(xs:dateTime($effectiveDate),'[Y0001]-[M01]-[D01] [H01]:[m01]:[s01]', (), (), ()),' 00:00:00','')
            else ()
        (: functional people find the date attached to the name in the selector/drop down list in the UI confusing
            and it is not necessary when the name for a given language is unique enough. Often the effectiveDate
            is not very helpful at all and rather random. So we generate a language dependent name specifically for 
            the drop down selector that only concatenates the effectiveDate when the name for the given language is 
            not unique in itself.
        :)
        let $selectorName   := string-join(($name, $versionLabel, $name/@language), '')
        let $selectorName   := 
            if (map:get($trnamemap, $selectorName) gt 1) then 
                concat($name, $label, ' :: ', $labeldate)
            else (
                concat($name, $label)
            )
        return
            (: use this for a drop down selector :)
            <name>
            {
                $name/@*,
                attribute selectorName {$selectorName},
                $name/node()
            }
            </name>
    }
    {
        if ($transaction[@type='group']) then (
            let $trlist     := if ($filter) then $transaction/transaction[.//@id = $filter/@id] else $transaction/transaction
            for $t in $trlist
            return
                utillib:getTransactionList($t, $projectPrefix, $filter, $trnamemap)
        ) else (
            $transaction/actors
            ,
            for $representingTemplate in $transaction/representingTemplate
            return
                <representingTemplate>
                {
                    $representingTemplate/@sourceDataset,
                    $representingTemplate/@sourceDatasetFlexibility,
                    $representingTemplate/@ref,
                    $representingTemplate/@flexibility,
                    $representingTemplate/@representingQuestionnaire,
                    $representingTemplate/@representingQuestionnaireFlexibility
                }
                </representingTemplate>
        )
    }
    </transaction>
};
(:~  Return scenario based on id and optionally effectiveDate. If effectiveDate is empty, the latest version is returned
:
:   @param $sc-id   - required dataset/@id
:   @param $sc-ed   - optional dataset/@effectiveDate
:   @return exactly 1 scenario or nothing if not found
:   @since 2015-04-27
:)
declare function utillib:getScenario($sc-id as xs:string, $sc-ed as xs:string?) as element(scenario)? {
    utillib:getScenario($sc-id, $sc-ed,(),())
};
(:~  Return the live scenario if param decorVersion is not a dateTime OR 
:   compiled, archived/released DECOR scenario based on scenario/@id|@effectiveDate and decor/@versionDate.
:   Released/compiled matches could be present in multiple languages. Use the parameters @language to select the right one.
:
:   @param $sc-id           - required scenario/@id
:   @param $sc-ed           - optional scenario/@effectiveDate
:   @param $decorVersion    - optional decor/@versionDate
:   @param $language        - optional decor/@language. Only relevant with parameter $decorVersion. Empty uses decor/project/@defaultLanguage, '*' selects all versions, e.g. 'en-US' selects the US English version (if available)
:   @return exactly 1 live scenario, or 1 or more archived DECOR version instances or nothing if not found
:   @since 2015-04-29
:)
declare function utillib:getScenario($sc-id as xs:string, $sc-ed as xs:string?, $decorVersion as xs:string?) as element(scenario)* {
    utillib:getScenario($sc-id, $sc-ed,$decorVersion,())
};
declare function utillib:getScenario($sc-id as xs:string, $sc-ed as xs:string?, $decorVersion as xs:string?, $language as xs:string?) as element(scenario)* {
    let $decor          :=
        if ($decorVersion[string-length()>0]) then
            let $d      := $setlib:colDecorVersion/decor[@versionDate=$decorVersion]
            return
            if (empty($language)) then
                $d[@language=$d/project/@defaultLanguage]
            else if ($language='*') then
                $d
            else
                $d[@language=$language]
        else (
            $setlib:colDecorData/decor
        )
    let $scenarios      := $decor//scenario[@id = $sc-id][ancestor::scenarios]
    
    return
        if ($sc-ed castable as xs:dateTime) then 
            $scenarios[@effectiveDate = $sc-ed] 
        else 
        if ($scenarios[2]) then 
            $scenarios[@effectiveDate = max($scenarios/xs:dateTime(@effectiveDate))]
        else (
            $scenarios
        )
};

(:~  Return transaction based on id and optionally effectiveDate. If effectiveDate is empty, the latest version is returned
:
:   @param $tr-id   - required dataset/@id
:   @param $tr-ed   - optional dataset/@effectiveDate
:   @return exactly 1 transaction or nothing if not found
:   @since 2015-04-27
:)
declare function utillib:getTransaction($tr-id as xs:string, $tr-ed as xs:string?) as element(transaction)? {
    utillib:getTransaction($tr-id, $tr-ed, (), ())
};
(:~  Return the live transaction if param decorVersion is not a dateTime OR 
:   compiled, archived/released DECOR transaction based on transaction/@id|@effectiveDate and decor/@versionDate.
:   Released/compiled matches could be present in multiple languages. Use the parameters @language to select the right one.
:
:   @param $tr-id           - required transaction/@id
:   @param $tr-ed           - optional transaction/@effectiveDate
:   @param $decorVersion    - optional decor/@versionDate
:   @param $language        - optional decor/@language. Only relevant with parameter $decorVersion. Empty uses decor/project/@defaultLanguage, '*' selects all versions, e.g. 'en-US' selects the US English version (if available)
:   @return exactly 1 live scenario, or 1 or more archived DECOR version instances or nothing if not found
:   @since 2015-04-29
:)
declare function utillib:getTransaction($tr-id as xs:string, $tr-ed as xs:string?, $decorVersion as xs:string?) as element(transaction)* {
    utillib:getTransaction($tr-id, $tr-ed, $decorVersion, ())
};
declare function utillib:getTransaction($tr-id as xs:string, $tr-ed as xs:string?, $decorVersion as xs:string?, $language as xs:string?) as element(transaction)* {
    let $decor          :=
        if ($decorVersion[string-length()>0]) then
            let $d      := $setlib:colDecorVersion/decor[@versionDate=$decorVersion]
            return
            if (empty($language)) then
                $d[@language=$d/project/@defaultLanguage]
            else if ($language='*') then
                $d
            else
                $d[@language=$language]
                
        else (
            $setlib:colDecorData/decor
        )
    let $transactions   := $decor//transaction[@id = $tr-id][ancestor::scenarios]
    
    return
        if ($tr-ed castable as xs:dateTime) then 
            $transactions[@effectiveDate = $tr-ed] 
        else 
        if ($transactions[2]) then 
            $transactions[@effectiveDate = max($transactions/xs:dateTime(@effectiveDate))]
        else (
            $transactions
        )
};

(:~  Return the live scenarios based on having any contained transaction that binds the requested dataset.
:
:   @param $ds-id           - required representingTemplate/@sourceDataset
:   @param $ds-ed           - optional representingTemplate/@sourceDatasetFlexibility
:   @return zero or more live scenarios
:   @since 2015-05-04
:)
declare function utillib:getScenariosByDataset($ds-id as xs:string, $ds-ed as xs:string?) as element(scenario)* {
    utillib:getScenariosByDataset($ds-id, $ds-ed, (), ())
};
(:~  Return the live scenarios if param decorVersion is not a dateTime OR 
:   compiled, archived/released DECOR scenarios based on having any contained transaction that binds the requested dataset.
:   Released/compiled matches could be present in multiple languages. Use the parameters @language to select the right one.
:
:   @param $ds-id           - required representingTemplate/@sourceDataset
:   @param $ds-ed           - optional representingTemplate/@sourceDatasetFlexibility
:   @param $decorVersion    - optional decor/@versionDate
:   @param $language        - optional decor/@language. Only relevant with parameter $decorVersion. Empty uses decor/project/@defaultLanguage, '*' selects all versions, e.g. 'en-US' selects the US English version (if available)
:   @return zero or more live scenarios, or 1 or more archived DECOR version instances or nothing if not found
:   @since 2015-05-04
:)
declare function utillib:getScenariosByDataset($ds-id as xs:string, $ds-ed as xs:string?, $decorVersion as xs:string?) as element(scenario)* {
    utillib:getScenariosByDataset($ds-id, $ds-ed, $decorVersion, ())
};
declare function utillib:getScenariosByDataset($ds-id as xs:string, $ds-ed as xs:string?, $decorVersion as xs:string?, $language as xs:string?) as element(scenario)* {
    let $decor          :=
        if ($decorVersion[string-length()>0]) then
            let $d      := $setlib:colDecorVersion/decor[@versionDate=$decorVersion]
            return
            if (empty($language)) then
                $d[@language=$d/project/@defaultLanguage]
            else if ($language='*') then
                $d
            else
                $d[@language=$language]
                
        else (
            $setlib:colDecorData/decor
        )
    let $scenarios      := $decor//scenario[ancestor::scenarios]
    let $dataset        := utillib:getDataset($ds-id, $ds-ed, $decorVersion)
    
    for $scenario in $scenarios
    let $datasets       := 
        for $representingTemplate in $scenario//representingTemplate[@sourceDataset = $ds-id]
        return utillib:getDataset($representingTemplate/@sourceDataset, $representingTemplate/@sourceDatasetFlexibility, $decorVersion)
    return
        if ($datasets[@effectiveDate = $dataset/@effectiveDate]) then $scenario else ()
};
(:~  Return the live transactions based on having a representingTemplate that binds the requested dataset.
:
:   @param $ds-id           - required representingTemplate/@sourceDataset
:   @param $ds-ed           - optional representingTemplate/@sourceDatasetFlexibility
:   @param $decorVersion    - optional decor/@versionDate
:   @return zero or more live transactions
:   @since 2015-05-04
:)
declare function utillib:getTransactionsByDataset($ds-id as xs:string, $ds-ed as xs:string?) as element(transaction)* {
    utillib:getTransactionsByScenarioAndDataset( (), (), $ds-id, $ds-ed, ())
};
(:~  Return the live transactions if param decorVersion is not a dateTime OR 
:   compiled, archived/released DECOR transactions based on having a representingTemplate that binds the requested dataset.
:   Could return multiple instances of the same transaction if the project was compiled in multiple languages.
:
:   @param $ds-id           - required representingTemplate/@sourceDataset
:   @param $ds-ed           - optional representingTemplate/@sourceDatasetFlexibility
:   @param $decorVersion    - optional decor/@versionDate
:   @return zero or more live scenarios, or 1 or more archived DECOR version instances or nothing if not found
:   @since 2015-05-04
:)
declare function utillib:getTransactionsByDataset($ds-id as xs:string, $ds-ed as xs:string?, $decorVersion as xs:string?) as element(transaction)* {
    utillib:getTransactionsByScenarioAndDataset((), (), $ds-id, $ds-ed, $decorVersion, ())
};
(:~  Return the live transactions based on having a representingTemplate that binds the requested dataset, 
:   and optionally filtered based on a specific scenario
:
:   @param $sc-id           - required scenario/@sourceDataset
:   @param $sc-ed           - optional scenario/@sourceDatasetFlexibility
:   @param $ds-id           - required representingTemplate/@sourceDataset
:   @param $ds-ed           - optional representingTemplate/@sourceDatasetFlexibility
:   @param $decorVersion    - optional decor/@versionDate
:   @return zero or more live transactions
:   @since 2015-05-04
:)
declare function utillib:getTransactionsByScenarioAndDataset($sc-id as xs:string, $sc-ed as xs:string?, $ds-id as xs:string, $ds-ed as xs:string?) as element(transaction)* {
    utillib:getTransactionsByScenarioAndDataset($sc-id, $sc-ed, $ds-id, $ds-ed, (), ())
};
(:~  Return the live transactions if param decorVersion is not a dateTime OR 
:   compiled, archived/released DECOR transactions based on having a representingTemplate that binds the requested dataset, 
:   and optionally filtered based on a specific scenario
:   Released/compiled matches could be present in multiple languages. Use the parameters @language to select the right one.
:   -- note: this function effectively performs the same as utillib:getTransactionsByDataset($ds-id, $ds-ed, $decorVersion) when attribute $sc-id is missing.
:
:   @param $sc-id           - optional scenario/@sourceDataset
:   @param $sc-ed           - optional scenario/@sourceDatasetFlexibility
:   @param $ds-id           - required representingTemplate/@sourceDataset
:   @param $ds-ed           - optional representingTemplate/@sourceDatasetFlexibility
:   @param $decorVersion    - optional decor/@versionDate
:   @param $language        - optional decor/@language. Only relevant with parameter $decorVersion. Empty uses decor/project/@defaultLanguage, '*' selects all versions, e.g. 'en-US' selects the US English version (if available)
:   @return zero or more live transactions, or 1 or more archived DECOR version instances
:   @since 2015-05-04
:)
declare function utillib:getTransactionsByScenarioAndDataset($sc-id as xs:string?, $sc-ed as xs:string?, $ds-id as xs:string, $ds-ed as xs:string?, $decorVersion as xs:string?) as element(transaction)* {
    utillib:getTransactionsByScenarioAndDataset($sc-id, $sc-ed, $ds-id, $ds-ed, $decorVersion, ())
};
declare function utillib:getTransactionsByScenarioAndDataset($sc-id as xs:string?, $sc-ed as xs:string?, $ds-id as xs:string, $ds-ed as xs:string?, $decorVersion as xs:string?, $language as xs:string?) as element(transaction)* {
    let $decor          :=
        if ($decorVersion[string-length()>0]) then
            let $d      := $setlib:colDecorVersion/decor[@versionDate=$decorVersion]
            return
            if (empty($language)) then
                $d[@language=$d/project/@defaultLanguage]
            else if ($language='*') then
                $d
            else
                $d[@language=$language]
                
        else (
            $setlib:colDecorData/decor
        )
    let $scenarios      := if (empty($sc-id)) then $decor/scenarios/scenario else (utillib:getScenario($sc-id, $sc-ed, $decorVersion))
    let $dataset        := utillib:getDataset($ds-id, $ds-ed, $decorVersion)
    
    for $representingTemplate in $scenarios//representingTemplate[@sourceDataset = $ds-id]
    return
        if ($representingTemplate/@sourceDatasetFlexibility[. castable as xs:dateTime] and 
            $dataset[@effectiveDate = $representingTemplate/@sourceDatasetFlexibility]) then
            $representingTemplate/parent::transaction
        else (
            let $ds     := utillib:getDataset($ds-id, $representingTemplate/@sourceDatasetFlexibility, $decorVersion, $language)
            return
                if ($dataset/@effectiveDate[.=$ds/@effectiveDate]) then $representingTemplate/parent::transaction else ()
        )
};

(:~  Return live transaction concept based on id and optionally effectiveDate + transaction id and optionally effectiveDate. If an effectiveDate is empty, the latest version is returned
:
:   @param $de-id   - required concept/@id
:   @param $de-ed   - optional concept/@effectiveDate
:   @param $tr-id   - required transaction/@id
:   @param $tr-ed   - optional transaction/@effectiveDate
:   @return exactly 1 concept or nothing if not found
:   @since 2018-06-21
:)
declare function utillib:getTransactionConcept($de-id as xs:string, $de-ed as xs:string?, $tr-id as xs:string?, $tr-ed as xs:string?) as element(concept)* {
    utillib:getTransactionConcept($de-id, $de-ed, $tr-id, $tr-ed, (), ())
};
(:~  Return live transaction concept if param decorVersion is not a dateTime OR 
:   compiled, archived/released DECOR transaction concept based on concept/@id|@effectiveDate and decor/@versionDate.
:   Released/compiled matches could be present in multiple languages. Use the parameters @language to select the right one.
:
:   @param $de-id           - required concept/@id
:   @param $de-ed           - optional concept/@effectiveDate
:   @param $tr-id           - required transaction/@id
:   @param $tr-ed           - optional transaction/@effectiveDate
:   @param $decorVersion    - optional decor/@versionDate
:   @param $language        - optional decor/@language. Only relevant with parameter $decorVersion. Empty uses decor/project/@defaultLanguage, '*' selects all versions, e.g. 'en-US' selects the US English version (if available)
:   Released/compiled matches could be present in multiple languages. Use the parameters @language to select the right one.
:   @return exactly 1 live concept, or 1 or more archived DECOR version instances or nothing if not found
:   @since 2015-04-29
:)
declare function utillib:getTransactionConcept($de-id as xs:string, $de-ed as xs:string?, $tr-id as xs:string?, $tr-ed as xs:string?, $decorVersion as xs:string?) as element(concept)* {
    utillib:getTransactionConcept($de-id, $de-ed, $tr-id, $tr-ed, $decorVersion, ())
};
declare function utillib:getTransactionConcept($de-id as xs:string, $de-ed as xs:string?, $tr-id as xs:string?, $tr-ed as xs:string?, $decorVersion as xs:string?, $language as xs:string?) as element(concept)* {
    let $concepts        := 
        if (empty($tr-id)) then
            $setlib:colDecorData//concept[@ref = $de-id][ancestor::transaction]
        else 
            utillib:getTransaction($tr-id, $tr-ed, $decorVersion, $language)//concept[@ref = $de-id]
    
    (: Note that we currently do not implement doing much with the concept effectivedate. In a normal dataset and hence in a normal transaction there will be only 1 version of a given concept so we should not really worry about getting 'the right version' :)
    return
        if ($concepts[@flexibility = $de-ed]) then $concepts[@flexibility = $de-ed][1] else
        if ($de-ed castable as xs:dateTime or $concepts[2]) then 
            for $concept in $concepts
            let $dsid   := $concept/ancestor::representingTemplate/@sourceDataset
            let $dsed   := $concept/ancestor::representingTemplate/@sourceDatasetFlexibility
            let $trdeed := $concept/@flexibility
            group by $dsid, $dsed, $trdeed
            return (
                let $datasetConcept     := utillib:getDatasetConcept($dsid, $dsed, $de-id, $trdeed, $decorVersion, $language)
                return
                    if ($de-ed castable as xs:dateTime) then (
                        if ($datasetConcept[@effectiveDate = $de-ed]) then $concept[1] else ()
                    )
                    else (
                        $concepts[not(@flexibility castable as xs:dateTime)] | $concepts[@flexibility = max($datasetConcept/xs:dateTime(@effectiveDate))]
                    )
            )
        else (
            $concepts
        )
};

(:~ retrieve contains/inherit hierarchy for decor concept
:   input: inherit or contains element of decor concept
:   returns: hierarchy of contains/inherit elements containing decor comment elements and the original concept
:)
declare function utillib:getOriginalConcept($inherit as element()) as element()? {
let $concept    := utillib:getOriginalForConcept(<concept>{$inherit}</concept>)

return
    element {if (name($inherit) = 'contains') then 'contains' else 'inherit'}
    {
        $inherit/@ref, 
        $inherit/@effectiveDate,
        $inherit/@flexibility,
        attribute prefix {$concept/ancestor::decor/project/@prefix},
        attribute datasetId {$concept/ancestor::dataset[1]/@id},
        attribute datasetEffectiveDate {$concept/ancestor::dataset[1]/@effectiveDate}
        ,
        if ($concept/inherit) then (
            $concept/comment,
            utillib:getOriginalConcept($concept/inherit)
        ) else 
        if ($concept/contains) then (
            $concept/comment,
            utillib:getOriginalConcept($concept/contains)
        )
        else (
            $concept
        )
    }
};

(:~  retrieve original concept, no added elements, no preservation of comments that may have been added in inheritance trail. 
:   Just comments of original. Caller needs to keep track of current added comments
:   input: decor concept
:   returns: the original concept
:)
declare function utillib:getOriginalForConcept($concept as element(concept)?) as element(concept)? {
    if ($concept[inherit | contains]) then (
        let $ref        := if ($concept[inherit]) then $concept/inherit/@ref           else $concept/contains/@ref
        let $eff        := if ($concept[inherit]) then $concept/inherit/@effectiveDate else $concept/contains/@flexibility 
        
        let $concepts   := utillib:getConcept($ref, $eff, (), ())
        let $error      := 
            if (count($concepts) le 1) then () else (
                error($errors:SERVER_ERROR, 'Concept id="' || $ref || '" effectiveDate="' || $eff || '" gave multiple hits. Expected exactly 1. Found in: ' || string-join(distinct-values($concepts/ancestor::decor/project/@prefix), ' '))
            )
        return
            utillib:getOriginalForConcept($concepts)
    )
    else (
        $concept
    )
};

(:~ equivalent to getOriginalConcept, but returns name and desc of original concept only and without inherit hierarchy
    <inherit ref="1.2.3" effectiveDate="yyyy-mm-ddThh:mm:ss"/>
    <contains ref="1.2.3" flexibility="yyyy-mm-ddThh:mm:ss"/>
:)
declare function utillib:getOriginalConceptName($inherit as element()) as element() {
let $concept    := utillib:getOriginalForConcept(<concept><inherit>{$inherit/@ref | $inherit/@effectiveDate}</inherit></concept>)

return
    <concept id="{$inherit/@ref}" effectiveDate="{$inherit/@effectiveDate | $inherit/@flexibility}">
    {
        $concept/name,
        $concept/desc
    }
    </concept>
};

(:~ Return the conceptList by this id :)
declare function utillib:getConceptList($id as xs:string, $projectPrefix as xs:string?) as element(conceptList)* {
    let $results  :=
        if (empty($projectPrefix)) then 
            $setlib:colDecorData//conceptList[@id = $id][ancestor::datasets][not(ancestor::history)]
        else 
            $setlib:colDecorData//conceptList[@id = $id][ancestor::datasets][not(ancestor::history)][ancestor::decor[project[@prefix=$projectPrefix]]]

    return
        if (count($results) le 1) then $results else (
            error(xs:QName('utillib:getConceptList'), 'ConceptList ' || $id || ' with prefix ''' || $projectPrefix || ''' yields ' || count($results) || ' results. Projects involved: ' || string-join(distinct-values($results/ancestor::decor/project/@prefix), ', '))
        )
};
(:~ Return the conceptList by this id :)
declare function utillib:getConceptListConcept($id as xs:string, $projectPrefix as xs:string?) as element(concept)? {
    let $results  :=
        if (empty($projectPrefix)) then 
            $setlib:colDecorData//conceptList/concept[@id = $id][ancestor::datasets][not(ancestor::history)]
        else 
            $setlib:colDecorData//conceptList/concept[@id = $id][ancestor::datasets][not(ancestor::history)][ancestor::decor[project[@prefix=$projectPrefix]]]

    return
        if (count($results) le 1) then $results else (
            error(xs:QName('utillib:getConceptListConcept'), 'ConceptList concept ' || $id || ' with prefix ''' || $projectPrefix || ''' yields ' || count($results) || ' results. Projects involved: ' || string-join(distinct-values($results/ancestor::decor/project/@prefix), ', '))
        )
};
(:~ Return the conceptLists that refer to this conceptList id. Principally you may only refer to a conceptList from within the same project, but 
    inconsistencies might have occurred that we don't know about. We'll leave that for a calling function to decide :)
declare function utillib:getConceptListRef($ref as xs:string, $projectPrefix as xs:string) as element(conceptList)* {
    if (empty($projectPrefix)) then 
        $setlib:colDecorData//conceptList[@ref = $ref][ancestor::datasets][not(ancestor::history)]
    else 
        $setlib:colDecorData//conceptList[@ref = $ref][ancestor::datasets][not(ancestor::history)][ancestor::decor[project[@prefix=$projectPrefix]]]
};

(:~  Return DECOR questionnaire wrapper based on id and optionally effectiveDate. If effectiveDate is empty, the latest version is returned
:
:   @param $qq-id   - required questionnaire/@id
:   @param $qq-ed   - optional questionnaire/@effectiveDate
:   @return exactly 1 questionnaire or nothing if not found
:   @since 2022-03-24
:)
declare function utillib:getQuestionnaire($qq-id as xs:string, $qq-ed as xs:string?) as element(questionnaire)? {
    utillib:getQuestionnaire($qq-id, $qq-ed, (), ())
};
(:~  Return the live DECOR questionnaire wrapper if param decorVersion is not a dateTime OR 
:   compiled, archived/released DECOR questionnaire based on questionnaire/@id|@effectiveDate and decor/@versionDate.
:   Released/compiled matches could be present in multiple languages. Use the parameters @language to select the right one.
:
:   @param $qq-id           - required questionnaire/@id
:   @param $qq-ed           - optional questionnaire/@effectiveDate
:   @param $decorVersion    - optional decor/@versionDate
:   @param $language        - optional decor/@language. Only relevant with parameter $decorVersion. Empty uses decor/project/@defaultLanguage, '*' selects all versions, e.g. 'en-US' selects the US English version (if available)
:   @return exactly 1 live questionnaire, or 1 or more archived DECOR version instances or nothing if not found
:   @since 2022-03-24
:)
declare function utillib:getQuestionnaire($qq-id as xs:string, $qq-ed as xs:string?, $decorVersion as xs:string?) as element(questionnaire)* {
    utillib:getQuestionnaire($qq-id, $qq-ed, (), $decorVersion, ())
};
declare function utillib:getQuestionnaire($qq-id as xs:string, $qq-ed as xs:string?, $decorVersion as xs:string?, $language as xs:string?) as element(questionnaire)* {
    utillib:getQuestionnaire($qq-id, $qq-ed, (), $decorVersion, $language)
};
declare function utillib:getQuestionnaire($qq-id as xs:string, $qq-ed as xs:string?, $project as xs:string?, $decorVersion as xs:string?, $language as xs:string?) as element(questionnaire)* {
    let $decor                      :=
        if (string-length($project) gt 0) then
            if (utillib:isOid($project)) then 
                utillib:getDecorById($project, $decorVersion, $language) 
            else (
                utillib:getDecorByPrefix($project, $decorVersion, $language)
            )
        else
        if (string-length($decorVersion) gt 0) then
            let $d      := $setlib:colDecorVersion/decor[@versionDate = $decorVersion]
            return
            if (empty($language)) then
                $d[@language = $d/project/@defaultLanguage]
            else if ($language='*') then
                $d
            else
                $d[@language = $language]
                
        else (
            $setlib:colDecorData/decor
        )
    let $buildingBlockRepositories  := utillib:getBuildingBlockRepositories($decor, (), ())
    
    return
        if ($qq-ed castable as xs:dateTime) then
            $buildingBlockRepositories//questionnaire[@id = $qq-id][@effectiveDate = $qq-ed]
        else (
            let $q := $buildingBlockRepositories//questionnaire[@id = $qq-id]
            return
                $q[@effectiveDate = string(max($q/xs:dateTime(@effectiveDate)))]
        )
};

(:~ Recursive function for getting all (cached) decor projects in scope for the input $decor project(s) :)
declare function utillib:getBuildingBlockRepositories($decor as element(decor)*, $sofar as map(*)?, $strDecorServicesURL as xs:string?) as element(decor)* {
    let $strDecorServicesURL        := if (empty($strDecorServicesURL)) then $utillib:strDecorServicesURL else $strDecorServicesURL
    let $sofar                      := 
        if (empty($sofar)) then
            map:merge(
                for $p in $decor 
                let $repourl                := ($p/ancestor::cacheme/@bbrurl, $strDecorServicesURL)[1]
                let $repoident              := $p/project/@prefix
                group by $repourl, $repoident
                return 
                    map:entry($repourl || '-' || $repoident, ())
            )
        else (
            $sofar
        )
    let $bbrs                       :=
        for $p in $decor/project/buildingBlockRepository[empty(@format)] | $decor/project/buildingBlockRepository[@format='decor']
        let $repourl                := $p/@url
        let $repoident              := $p/@ident
        group by $repourl, $repoident
        return (
            if (map:keys($sofar)[. = $repourl || '-' || $repoident]) then () else
            if ($repourl = $strDecorServicesURL) then 
                $setlib:colDecorData//decor[project/@prefix = $repoident] 
            else (
                $setlib:colDecorCache//cacheme[@bbrident = $repoident][@bbrurl = $repourl]/decor
            )
        )
    let $new-sofar                  :=
        map:merge(
            for $cc in (map:keys($sofar), $bbrs/concat((ancestor::cacheme/@bbrurl, $strDecorServicesURL)[1], '-', project/@prefix))
            return map:entry($cc, ())
        )
    let $other-bbrs                 := if ($bbrs) then utillib:getBuildingBlockRepositories($bbrs, $new-sofar, $strDecorServicesURL) else () 
    return
        $decor | $bbrs | $other-bbrs
};

(:~  Return DECOR questionnaireresponse wrapper based on id and optionally effectiveDate. If effectiveDate is empty, the latest version is returned
:
:   @param $qq-id   - required questionnaireresponse/@id
:   @param $qq-ed   - optional questionnaireresponse/@effectiveDate
:   @return exactly 1 questionnaire or nothing if not found
:   @since 2022-03-24
:)
declare function utillib:getQuestionnaireResponse($qq-id as xs:string, $qq-ed as xs:string?) as element(questionnaireresponse)? {
    utillib:getQuestionnaireResponse($qq-id, $qq-ed, (), ())
};
(:~  Return the live DECOR questionnaireresponse wrapper if param decorVersion is not a dateTime OR 
:   compiled, archived/released DECOR questionnaireresponse based on questionnaireresponse/@id|@effectiveDate and decor/@versionDate.
:   Released/compiled matches could be present in multiple languages. Use the parameters @language to select the right one.
:
:   @param $qq-id           - required questionnaireresponse/@id
:   @param $qq-ed           - optional questionnaireresponse/@effectiveDate
:   @param $decorVersion    - optional decor/@versionDate
:   @param $language        - optional decor/@language. Only relevant with parameter $decorVersion. Empty uses decor/project/@defaultLanguage, '*' selects all versions, e.g. 'en-US' selects the US English version (if available)
:   @return exactly 1 live questionnaireresponse, or 1 or more archived DECOR version instances or nothing if not found
:   @since 2022-03-24
:)
declare function utillib:getQuestionnaireResponse($qq-id as xs:string, $qq-ed as xs:string?, $decorVersion as xs:string?) as element(questionnaireresponse)* {
    utillib:getQuestionnaireResponse($qq-id, $qq-ed, $decorVersion, ())
};
declare function utillib:getQuestionnaireResponse($qq-id as xs:string, $qq-ed as xs:string?, $decorVersion as xs:string?, $language as xs:string?) as element(questionnaireresponse)* {
    let $decor          :=
        if ($decorVersion[string-length()>0]) then
            let $d      := $setlib:colDecorVersion/decor[@versionDate=$decorVersion]
            return
            if (empty($language)) then
                $d[@language=$d/project/@defaultLanguage]
            else if ($language='*') then
                $d
            else
                $d[@language=$language]
                
        else (
            $setlib:colDecorData/decor
        )
    let $qq             := $decor//questionnaireresponse[@id = $qq-id][ancestor::scenarios]
    
    return
        if ($qq-ed castable as xs:dateTime) then 
            $qq[@effectiveDate = $qq-ed] 
        else 
        if ($qq[2]) then 
            $qq[@effectiveDate = max($qq/xs:dateTime(@effectiveDate))]
        else (
            $qq
        )
};

(:~ Return the original conceptList. If input has @id, return input. If input has @ref, find original with @id :)
declare function utillib:getOriginalConceptList($conceptList as element(conceptList)?) as element(conceptList)? {
    if ($conceptList[@ref]) then (
        let $projectPrefix := 
            if ($conceptList/ancestor::decor) then 
                $conceptList/ancestor::decor/project/@prefix
            else (
                (:when called through getOriginalConcept the prefix is on a parent inherit or contains element:)
                ($conceptList/ancestor::inherit/@prefix | $conceptList/ancestor::contains/@prefix)[1]
            )
        (:get original from same project:)
        let $originalConceptList := $setlib:colDecorData//conceptList[@id = $conceptList/@ref][ancestor::datasets][not(ancestor::history)][ancestor::decor[project[@prefix=$projectPrefix]]]
        return
            if ($originalConceptList) then (
                $originalConceptList[1] (:got original, return it:)
            ) else (
                $conceptList            (:could not find original, just return reference:)
            )
    )
    else (
        (: original or empty :)
        $conceptList
    )
};

(:~ Return id from input if it leads to an original concept. Recurse to return id of the original concept based on inherit :)
declare function utillib:getOriginalConceptId($conceptId as xs:string, $conceptEffectiveDate as xs:string?) as xs:string? {
let $concept    := utillib:getOriginalForConcept(<concept><inherit ref="{$conceptId}" effectiveDate="{$effectiveDate}"/></concept>)

return
    $concept/@id
};

(:~ Get .//terminologyAssociation and identifierAssociation element directly from the $concept, or if none exist connected to the dataset concept. 
Note that an incoming $concept might be a compiled $concept, a regular dataset concept or a transaction concept.
:)
declare function utillib:getConceptAssociations($concept as element(concept)?) as element()* {
    let $datasetConcept     :=
        if ($concept[ancestor::dataset]) then $concept else
        if ($concept[ancestor::transaction]) then (utillib:getDataset($concept/ancestor::representingTemplate/@sourceDataset, $concept/ancestor::representingTemplate/@sourceDatasetFlexibility)//concept[@id = $concept/@ref]) else
        if ($concept[@id]) then (utillib:getConcept($concept/@id, $concept/@effectiveDate)) else
        if ($concept[@ref]) then (utillib:getConcept($concept/@ref, $concept/@flexibility)) else ()
    return
    utillib:getConceptAssociations($concept, utillib:getOriginalForConcept($datasetConcept), false())
};
(:~ Get .//terminologyAssociation and identifierAssociation element directly from the $concept, or if none exist connected to the dataset concept. 
Note that an incoming $concept might be a compiled $concept, a regular dataset concept or a transaction concept.
@param $concept DECOR dataset concept
@param $originalConcept the original dataset concept
@param $allAssociations only looks for terminologyAssociation and identifierAssociation if false. Otherwise also includes associations to transaction, issue, template, and other concepts
:)
declare function utillib:getConceptAssociations($concept as element(concept)?, $originalConcept as element(concept)?, $allAssociations as xs:boolean) as element()* {
if (empty($concept)) then () else
if ($concept/valueSet/terminologyAssociation | $concept/terminologyAssociation | $concept/identifierAssociation) then (
    (: if for some reason sent us a concept with contained terminologyAssociation then we should assume this is the result of utillib:getFullConcept and just return what was 'calculated' there.
        caveat: utillib:getFullConcept does not keep the conceptList/concept associations, only concept and valueSet.
    :)
    let $decor                  := $concept/ancestor::decor
    let $language               := ($decor/project/@defaultLanguage, 'en-US')[1]
    let $associations           := $concept/valueSet/terminologyAssociation | $concept/terminologyAssociation | $concept/identifierAssociation
    (: build map of oid names. is more costly when you do that deep down :)
    let $oidnamemap             :=
        map:merge(
            for $oid in distinct-values($associations/(@ref, @codeSystem))
            let $oidName    := utillib:getNameForOID($oid, $language, $decor)
            return
                if (string-length($oidName) = 0) then () else map:entry($oid, $oidName)
        ) 
    return
    utillib:conceptExpandedAssociation($concept, $associations, $oidnamemap, $language)
)
else 
if ($concept[ancestor::transaction]) then ()
else (
    let $decor              := $concept/ancestor::decor
    (: if for some reason someone sent us a concept from memory and not from the db, then we will not have a decor ancestor... :)
    let $language           := ($decor/project/@defaultLanguage, 'en-US')[1]
    let $datasetConcept     :=
        if ($concept[ancestor::dataset]) then $concept else
        if ($concept[ancestor::transaction]) then (utillib:getDataset($concept/ancestor::representingTemplate/@sourceDataset, $concept/ancestor::representingTemplate/@sourceDatasetFlexibility)//concept[@id = $concept/@ref]) else
        if ($concept[@id]) then (utillib:getConcept($concept/@id, $concept/@effectiveDate)) else
        if ($concept[@ref]) then (utillib:getConcept($concept/@ref, $concept/@flexibility)) else ()
    let $originalConcept    := if ($originalConcept) then $originalConcept else utillib:getOriginalForConcept($datasetConcept)
    
    let $decor              :=  if ($decor) then $decor else $datasetConcept/ancestor::decor
    
    let $conceptLists       :=
        for $conceptList in $originalConcept/valueDomain/conceptList
        return utillib:getOriginalConceptList($conceptList)
    
    (: map for all concept(List) ids, without any entry value :)
    let $oidset             := distinct-values($concept/@id | $datasetConcept/@id | $originalConcept/@id | $conceptLists//@id)
    
    let $terminologyAssociations    :=
        for $association in (   $decor/terminology/terminologyAssociation[@conceptId = $oidset][not(@conceptFlexibility castable as xs:dateTime)] | 
                                $decor/terminology/terminologyAssociation[@conceptId = $datasetConcept/@id][@conceptFlexibility = $datasetConcept/@effectiveDate] | 
                                $decor/terminology/terminologyAssociation[@conceptId = $originalConcept/@id][@conceptFlexibility = $originalConcept/@effectiveDate]
                                )
        let $associationType := $association/concat(@conceptId,@valueSet,@flexibility,@code,@codeSystem)
        group by $associationType
        return $association[1]
    let $identifierAssociations     :=
        if ($originalConcept[valueDomain[@type= 'identifier']]) then (
            for $association in $decor/ids/identifierAssociation[@conceptId = $oidset][not(@conceptFlexibility castable as xs:dateTime)] | 
                                $decor/ids/identifierAssociation[@conceptId = $datasetConcept/@id][@conceptFlexibility = $datasetConcept/@effectiveDate] | 
                                $decor/ids/identifierAssociation[@conceptId = $originalConcept/@id][@conceptFlexibility = $originalConcept/@effectiveDate]
            let $associationType := $association/concat(@conceptId,@ref)
            group by $associationType
            return $association[1]
        ) else ()
    (: build map of oid names. is more costly when you do that deep down :)
    let $oidnamemap             :=
        map:merge(
            for $oid in distinct-values($terminologyAssociations/@codeSystem | $identifierAssociations/@ref)
            let $oidName    := utillib:getNameForOID($oid, $language, $decor)
            return
                if (string-length($oidName) = 0) then () else map:entry($oid, $oidName)
        ) 
    return (
        utillib:conceptExpandedAssociation($concept, $terminologyAssociations | $identifierAssociations, $oidnamemap, $decor/project/@defaultLanguage)
        ,
        (: associations of a concept with a concept, transaction, template and issue would be part of different calls before. As of ART-DECOR 3 we now regard them as associations of the concept :)
        if ($allAssociations) then (
            let $isLatestDataset    := utillib:getDataset($datasetConcept/ancestor::dataset/@id, ())/@effectiveDate = $datasetConcept/ancestor::dataset/@effectiveDate
            let $transactions       :=
                $setlib:colDecorData//concept[@ref = $datasetConcept/@id][ancestor::representingTemplate[@sourceDataset = $datasetConcept/ancestor::dataset/@id][@sourceDatasetFlexibility = $datasetConcept/ancestor::dataset/@effectiveDate]]
            let $transactions       :=
                if ($isLatestDataset) then
                    $transactions |
                    $setlib:colDecorData//concept[@ref = $datasetConcept/@id][ancestor::representingTemplate[@sourceDataset = $datasetConcept/ancestor::dataset/@id][empty(@sourceDatasetFlexibility)]]
                else (
                    $transactions
                )
            (: returns a concept associated with a transaction :)
            for $association in     $transactions
            let $associationType    := $association/concat($association/ancestor::transaction[1]/@id, $association/ancestor::transaction[1]/@effectiveDate, $association/@ref)
            group by $associationType
            return
                utillib:doTransactionAssociation(
                    $concept, 
                    $association[1]
                )
            ,
            (: returns a concept associated with a template :)
            for $association in     $setlib:colDecorData//templateAssociation/concept[@ref = $datasetConcept/@id][@effectiveDate = $datasetConcept/@effectiveDate]
            let $associationType    := $association/concat($association/parent::templateAssociation/@templateId, $association/parent::templateAssociation/@effectiveDate, $association/@elementId, $association/@elementPath)
            group by $associationType
            return 
                utillib:doTemplateAssociation(
                    $concept, 
                    $association[1]
                )
            ,
            (: returns a concept associated with a questionnaire :)
            for $association in     $setlib:colDecorData//questionnaireAssociation/concept[@ref = $datasetConcept/@id][@effectiveDate = $datasetConcept/@effectiveDate]
            let $associationType    := $association/concat($association/parent::questionnaireAssociation/@templateId, $association/parent::questionnaireAssociation/@effectiveDate, $association/@elementId, $association/@elementPath)
            group by $associationType
            return 
                utillib:doQuestionnaireAssociation(
                    $concept, 
                    $association[1]
                )
            ,
            (: returns a concept associated with an issue :)
            (:for $association in     $setlib:colDecorData//issue/object[@id = $datasetConcept/@id][@effectiveDate = $datasetConcept/@effectiveDate]
            let $associationType    := $association/ancestor::issue/@id
            group by $associationType
            return 
                <issueAssociation conceptId="{$datasetConcept/@id}" conceptFlexibility="{$datasetConcept/@effectiveDate}">
                {
                    for $attr in $association[1]/ancestor::issue[1]/@*
                    return
                        attribute {'target' || upper-case(substring(local-name($attr), 1, 1)) || substring(local-name($attr), 2)} {$attr}
                    ,
                    (\: Mark the thing with the transaction id/effectiveDate to smooth downstream processing :\)
                    for $attr in $concept/ancestor::transaction[1]/(@id, @effectiveDate, @versionLabel, @statusCode, @expirationDate)
                    return
                        attribute {'transaction' || upper-case(substring(local-name($attr), 1, 1)) || substring(local-name($attr), 2)} {$attr}
                    ,
                    (\:for $name in $concept/ancestor::transaction[1]/name
                    return 
                        <transactionName>{$name/@*, $name/node()}</transactionName>
                    ,:\)
                    <name language="{$association[1]/ancestor::decor/project/@defaultLanguage}">{data($association[1]/ancestor::issue[1]/@displayName)}</name>
                }
                </issueAssociation>
            ,:)
            (: returns a concept associated with another concept through an inherit, contains, or relationship :)
            for $association in     (   $setlib:colDecorData//inherit[@ref = $datasetConcept/@id][@effectiveDate = $datasetConcept/@effectiveDate] |
                                        $setlib:colDecorData//contains[@ref = $datasetConcept/@id][@flexibility = $datasetConcept/@effectiveDate] |
                                        $setlib:colDecorData//relationship[@ref = $datasetConcept/@id][@flexibility = $datasetConcept/@effectiveDate]
                                    )
            let $associationType    := $association/concat($association/parent::concept[1]/@id, $association/parent::concept[1]/@effectiveDate)
            group by $associationType
            return 
                utillib:doConceptAssociation(
                    $concept, 
                    $association[1],
                    if ($association[1]/parent::concept[1]/name) then $association[1]/parent::concept[1]/name else if ($datasetConcept/name) then $datasetConcept/name else $originalConcept/name
                )
                
        ) else ()
    )
)
};
(:~ Function for retrieving list of terminology associations for (transaction) concept and contained conceptList/concept Modes: all, diff, normal (default)
    - all: retrieve any dataset and transaction terminologyAssociations and identifierAssociations for this concept
    - diff: as 'all', but trimmed only to those 
        - in any transactions if this a dataset concept ($trid is empty)
        - at dataset level if this a transaction concept ($trid is not empty)
    - normal (default): retrieve any terminologyAssociations and identifierAssociations for this concept at the appropriate level
        - in transaction ($trid is not empty)
        - at dataset level ($trid is empty)
:)
declare function utillib:getConceptAssociations($concept as element(concept)?, $originalConcept as element(concept)?, $allAssociations as xs:boolean, $mode as xs:string?, $doNameForOID as xs:boolean) as element(associations) {

let $mode                           := if ($mode = ('all', 'diff')) then $mode else 'normal'
let $transactionConcept             := $concept[ancestor::transaction]
let $datasetConcept                 := 
    if ($concept[ancestor::dataset]) then $concept else
    if ($concept[@id]) then (utillib:getConcept($concept/@id, $concept/@effectiveDate)) else
    if ($concept[@ref]) then (utillib:getConcept($concept/@ref, $concept/@flexibility[. castable as xs:dateTime])) else ()
let $originalConcept                := if ($datasetConcept) then utillib:getOriginalForConcept($datasetConcept) else ()

let $conceptId                      := $datasetConcept/@id
let $conceptEffectiveDate           := $datasetConcept/@effectiveDate
let $transactionId                  := $transactionConcept/ancestor::transaction[1]/@id
let $transactionEffectiveDate       := $transactionConcept/ancestor::transaction[1]/@effectiveDate

let $decor                          := $concept/ancestor::decor
let $projectLanguage                := $decor/project/@defaultLanguage
let $language                       := if (request:exists()) then request:get-parameter('language', $projectLanguage) else ($projectLanguage)

let $datasetConceptAssociations     := utillib:getConceptAssociations($datasetConcept, $originalConcept, $allAssociations)

let $transactionConceptAssocations  := 
    if (empty($concept))        then () else
    if ($transactionConcept)    then (
        (: this is a transaction concept :)
        switch ($mode)
        case 'all' return utillib:getConceptAssociations($transactionConcept, $originalConcept, false())
        default return utillib:getConceptAssociations($concept, $originalConcept, false())
    ) else (
        (: this is a dataset concept :)
        switch ($mode)
        case 'diff' return (
            for $tc in utillib:getTransactionConcept($conceptId, $conceptEffectiveDate, (), (), (), ())
            return
                utillib:getConceptAssociations($tc, $originalConcept, false())
        )
        default return ()
    )

(: logic for transaction concepts: 
    - get associations from transaction concept if they exist else get from dataset concept
    - note that a transaction concept association might be 'empty', i.e. have no code/valueSet/ref. If that's the case then this counts as 'do not use dataset association(s), if any'
        This is relevant e.g. when you have a dataset conceptList with concepts, but in the transaction you do not support some of the concepts. In this case there's no real overriding association possible
:)
let $associations       :=
    switch ($mode)
    case 'all' return (
        (: mark active if this is transaction concept only :)
        if ($transactionConcept) then 
            for $tca in $datasetConceptAssociations
            let $tcaId              : = 
                if (not($originalConcept/@id = $transactionConcept/@ref) and $originalConcept/@id = $tca/@conceptId) then $transactionConcept/@ref else $tca/@conceptId
            return
                element {name($tca)} {
                    (: a dataset association is active in absence of an overriding transaction association :)
                    attribute active {empty($transactionConceptAssocations[name() = name($tca)][@conceptId = $tcaId])},
                    $tca/(@* except (@active | @*[starts-with(name(), 'transaction')]))
                }
        else (
            $datasetConceptAssociations
        )
        ,
        (: Mark the thing with the transaction id/effectiveDate to smooth downstream processing :)
        for $tca in $transactionConceptAssocations
        return
            element {name($tca)} {
                (: a transaction association is always active and overrides and dataset association :)
                attribute active {'true'},
                $tca/(@* except (@active | @*[starts-with(name(), 'transaction')])),
                for $attr in $transactionConcept/ancestor::transaction[1]/(@id, @effectiveDate, @versionLabel, @statusCode, @expirationDate)
                return
                    attribute {'transaction' || upper-case(substring(local-name($attr), 1, 1)) || substring(local-name($attr), 2)} {$attr}
            }
    )
    case 'diff' return (
        if ($concept[ancestor::transaction]) then (
            for $dsa in $datasetConceptAssociations[self::terminologyAssociation]
            return $dsa[@conceptId = $transactionConceptAssocations[self::terminologyAssociation]/@conceptId]
            ,
            for $dsa in $datasetConceptAssociations[self::identifierAssociation]
            return $dsa[@conceptId = $transactionConceptAssocations[self::identifierAssociation]/@conceptId]
            ,
            $datasetConceptAssociations[not(self::terminologyAssociation | self::identifierAssociation)]
        )
        else (
            $datasetConceptAssociations
            ,
            (: Mark the thing with the transaction id/effectiveDate to smooth downstream processing :)
            for $tca in $transactionConceptAssocations
            return
                element {name($tca)} {
                    $tca/(@* except (@*[starts-with(name(), 'transaction')])),
                    for $attr in $transactionConcept/ancestor::transaction[1]/(@id, @effectiveDate, @versionLabel, @statusCode, @expirationDate)
                    return
                        attribute {'transaction' || upper-case(substring(local-name($attr), 1, 1)) || substring(local-name($attr), 2)} {$attr}
                }
        )
    )
    default return (
        if ($concept[ancestor::transaction]) then
            for $conceptId in $transactionConceptAssocations/@conceptId | $datasetConceptAssociations/@conceptId
            let $id := $conceptId
            group by $id
            return (
                let $trassoc    := $transactionConceptAssocations[self::terminologyAssociation][@conceptId = $conceptId[1]]
                return 
                if ($trassoc) then $trassoc else ($datasetConceptAssociations[self::terminologyAssociation][@conceptId = $conceptId[1]])
                ,
                let $trassoc    := $transactionConceptAssocations[self::identifierAssociation][@conceptId = $conceptId[1]]
                return
                if ($trassoc) then $trassoc else ($datasetConceptAssociations[self::identifierAssociation][@conceptId = $conceptId[1]])
                ,
                $datasetConceptAssociations[not(self::terminologyAssociation | self::identifierAssociation)]
            )
        else (
            $datasetConceptAssociations
            , 
            (: Mark the thing with the transaction id/effectiveDate to smooth downstream processing :)
            for $tca in $transactionConceptAssocations
            return
                element {name($tca)} {
                    $tca/(@* except (@*[starts-with(name(), 'transaction')])),
                    for $attr in $transactionConcept/ancestor::transaction[1]/(@id, @effectiveDate, @versionLabel, @statusCode, @expirationDate)
                    return
                        attribute {'transaction' || upper-case(substring(local-name($attr), 1, 1)) || substring(local-name($attr), 2)} {$attr}
                }
        )
    )
return
    <associations mode="{$mode}">{$associations}</associations>
};

(:~ Return a valueSetAssociation based on $sourceObject which is usually element(valueSet) or element(codeSystem) and a targetObject which is element(valueSet), 
    element(completeCodeSystem), or an element in the valueSet/conceptList (concept, exception, include, exclude)  
:)
declare function utillib:doValueSetAssociation($sourceObject as element(), $targetObject as element()) as element(valueSetAssociation) {
    <valueSetAssociation objectId="{$sourceObject/@id}">
    {
        if ($sourceObject/@effectiveDate) then attribute objectFlexibility {$sourceObject/@effectiveDate} else ()
    }
    {
        for $attr in $targetObject/ancestor-or-self::valueSet[1]/(@* except (@name|@url|@ident))
        return
            attribute {'target' || upper-case(substring(local-name($attr), 1, 1)) || substring(local-name($attr), 2)} {$attr}
        ,
        attribute url {$utillib:strDecorServicesURL},
        attribute ident {$targetObject/ancestor::decor/project/@prefix}
        ,
        <name language="{$targetObject/ancestor::decor/project/@defaultLanguage}">{data($targetObject/ancestor-or-self::valueSet[1]/(@displayName, @name)[1])}</name>
        ,
        for $name in $targetObject/ancestor::decor/project/name
        return
            <projectName>{$name/@*, $name/node()}</projectName>
    }
    </valueSetAssociation>
};

(:~ Return a valueSetAssociation based on $sourceObject which is usually element(valueSet) or element(codeSystem) and a targetObject which is element(valueSet), 
    element(completeCodeSystem), or an element in the valueSet/conceptList (concept, exception, include, exclude)  
:)
declare function utillib:doValueSetExpansionAssociation($sourceObject as element(), $targetObject as element(), $decor as element(decor)?) as element(valueSetExpansionAssociation) {
    <valueSetExpansionAssociation objectId="{$sourceObject/@id}">
    {
        if ($sourceObject/@effectiveDate) then attribute objectFlexibility {$sourceObject/@effectiveDate} else ()
    }
    {
        for $attr in $targetObject/ancestor-or-self::valueSetExpansionSet[1]/(@* except @name)
        return
            attribute {'target' || upper-case(substring(local-name($attr), 1, 1)) || substring(local-name($attr), 2)} {$attr}
        ,
        if ($decor) then (
            <name language="{$decor/project/@defaultLanguage}">{data($sourceObject/ancestor-or-self::valueSet[1]/(@displayName, @name)[1])}</name>
            ,
            for $name in $decor/project/name
            return
                <projectName>{$name/@*, $name/node()}</projectName>
        )
        else ()
    }
    </valueSetExpansionAssociation>
};

(:~ Return a transactionAssociation based on $sourceObject which is usually element(concept) and a targetObject which is element(transaction) or element(concept)
:)
declare function utillib:doTransactionAssociation($sourceObject as element(), $targetObject as element()) as element(transactionAssociation) {
    <transactionAssociation objectId="{$sourceObject/@id}">
    {
        if ($sourceObject/@effectiveDate) then attribute objectFlexibility {$sourceObject/@effectiveDate} else ()
    }
    {
        if ($targetObject[self::concept]) then (
            attribute minimumMultiplicity {utillib:getMinimumMultiplicity($targetObject)},
            attribute maximumMultiplicity {utillib:getMaximumMultiplicity($targetObject)},
            $targetObject/@conformance[not(.='')],
            attribute isMandatory {$targetObject/@isMandatory='true'}
        )
        else ()
        ,
        for $attr in $targetObject/ancestor-or-self::transaction[1]/(@* except @name)
        return
            attribute {'target' || upper-case(substring(local-name($attr), 1, 1)) || substring(local-name($attr), 2)} {$attr}
        ,
        if ($targetObject/ancestor::decor/project/@prefix = $sourceObject/ancestor::decor[1]/project/@prefix) then () else (
            if ($targetObject/ancestor-or-self::transaction[1]/@url) then () else attribute url {$utillib:strDecorServicesURL},
            if ($targetObject/ancestor-or-self::transaction[1]/@ident) then () else attribute ident {$targetObject/ancestor::decor/project/@prefix}
        ),
        (: Mark the thing with the transaction id/effectiveDate to smooth downstream processing :)
        for $attr in $sourceObject/ancestor::transaction[1]/(@id, @effectiveDate, @versionLabel, @statusCode, @expirationDate)
        return
            attribute {'transaction' || upper-case(substring(local-name($attr), 1, 1)) || substring(local-name($attr), 2)} {$attr}
        ,
        $targetObject/ancestor-or-self::transaction[1]/name
        ,
        for $name in $targetObject/ancestor::decor[1]/project/name
        return 
            <projectName>{$name/@*, $name/node()}</projectName>
    }
    </transactionAssociation>
};

(:~ Return a templateAssociation based on $sourceObject which is usually element(concept) or element(valueSet) or element(codeSystem) and 
    a targetObject which is element(concept), element(vocabulary) or element(attribute) :)
declare function utillib:doTemplateAssociation($sourceObject as element(), $targetObject as element()) as element(templateAssociation) {
    <templateAssociation objectId="{$sourceObject/@id}">
    {
        if ($sourceObject/@effectiveDate) then attribute objectFlexibility {$sourceObject/@effectiveDate} else ()
    }
    {
        $targetObject/@elementId,
        $targetObject/@elementPath,
        let $tmid       := ($targetObject/ancestor::templateAssociation[1]/@templateId, $targetObject/ancestor-or-self::template[1]/@templateId)[1]
        let $tmed       := ($targetObject/ancestor::templateAssociation[1]/@effectiveDate, $targetObject/ancestor-or-self::template[1]/@effectiveDate)[1]
        let $template   := $targetObject/ancestor-or-self::template[1]
        let $template   := 
            if ($template) then $template else (
                $setlib:colDecorData//template[@id = $tmid][@effectiveDate = $tmed] |
                $setlib:colDecorCache//template[@id = $tmid][@effectiveDate = $tmed]
            )
        (: you would think that this is much more efficient than doing it the way we do below, but eXist refuses to match for @elementId this way and I'm tired of looking for why :)
        (:let $targets    := 
            if (empty($template)) then () else (
                $template//element[@id = $targetObject/@elementId] | $template//attribute[@id = $targetObject/@elementId]
            ):)
        return (
            if ($template) then (
                for $attr in $template[1]/(@* except (@name|@elementId|@elementPath))
                return
                    attribute {'target' || upper-case(substring(local-name($attr), 1, 1)) || substring(local-name($attr), 2)} {$attr}
                ,
                if ($targetObject/ancestor::decor/project/@prefix = $sourceObject/ancestor::decor[1]/project/@prefix) then () else (
                    if ($targetObject/ancestor::transaction[1]/@url) then () else attribute url {$utillib:strDecorServicesURL},
                    if ($targetObject/ancestor::transaction[1]/@ident) then () else attribute ident {$targetObject/ancestor::decor/project/@prefix}
                )
            ) else (
                attribute targetId {$tmid},
                attribute targetEffectiveDate {$tmed}
            ),
            (: Mark the thing with the transaction id/effectiveDate to smooth downstream processing :)
            for $attr in $sourceObject/ancestor::transaction[1]/(@id, @effectiveDate, @versionLabel, @statusCode, @expirationDate)
            return
                attribute {'transaction' || upper-case(substring(local-name($attr), 1, 1)) || substring(local-name($attr), 2)} {$attr}
            ,
            (:for $name in $sourceObject/ancestor::transaction[1]/name
            return 
                <transactionName>{$name/@*, $name/node()}</transactionName>
            ,:)
            if ($template) then (
                <name language="{$template[1]/ancestor::decor/project/@defaultLanguage}">{data(($template[1]/@displayName, $template[1]/@name)[1])}</name>
                ,
                for $name in $template[1]/ancestor::decor[1]/project/name
                return 
                    <projectName>{$name/@*, $name/node()}</projectName>
                ,
                (: in inconsistent templates we might encounter multiple elements and/or attributes with the same elementId :)
                for $target in $template//element[@id] | $template//attribute[@id]
                return
                    if ($target/@id = $targetObject/@elementId) then
                        <path type="{name($target)}">
                        {
                            string-join(
                                for $ancestor in $target/ancestor-or-self::*[self::element | self::attribute]
                                let $min    := if ($ancestor/@conformance='NP') then () else (utillib:getMinimumMultiplicity($ancestor))
                                let $max    := if ($ancestor/@conformance='NP') then () else (utillib:getMaximumMultiplicity($ancestor))
                                let $conf   := if ($ancestor/@isMandatory='true') then ('M') else ($target/@conformance)
                                let $mmc    := string-join((string-join(($min,$max), '…'),$conf),' ')
                                return
                                $ancestor/concat(if (self::attribute) then '@' else (),@name,if (empty($mmc)) then () else concat(' (',$mmc,')'))
                            ,' / ')
                        }
                        </path>
                    else ()
            )    
            else ()
        )
    }
    </templateAssociation>
};

(:~ Return a questionnaireAssociation based on $sourceObject which is usually element(concept) or element(valueSet) or element(codeSystem) and 
    a targetObject which is element(concept), element(vocabulary) or element(attribute) :)
declare function utillib:doQuestionnaireAssociation($sourceObject as element(), $targetObject as element()) as element(questionnaireAssociation) {
    <questionnaireAssociation objectId="{$sourceObject/@id}">
    {
        if ($sourceObject/@effectiveDate) then attribute objectFlexibility {$sourceObject/@effectiveDate} else ()
    }
    {
        $targetObject/@elementId,
        $targetObject/@elementPath,
        let $qid            := ($targetObject/ancestor::questionnaireAssociation[1]/@questionnaireId, $targetObject/ancestor-or-self::questionnaire[1]/@templateId)[1]
        let $qed            := ($targetObject/ancestor::questionnaireAssociation[1]/@questionnaireEffectiveDate, $targetObject/ancestor-or-self::questionnaire[1]/@effectiveDate)[1]
        let $questionnaire  := $targetObject/ancestor-or-self::questionnaire[1]
        let $questionnaire  := 
            if ($questionnaire) then $questionnaire else (
                $setlib:colDecorData//questionnaire[@id = $qid][@effectiveDate = $qed] |
                $setlib:colDecorCache//questionnaire[@id = $qid][@effectiveDate = $qed]
            )
        return (
            if ($questionnaire) then (
                for $attr in $questionnaire[1]/(@* except (@name|@elementId|@elementPath))
                return
                    attribute {'target' || upper-case(substring(local-name($attr), 1, 1)) || substring(local-name($attr), 2)} {$attr}
                ,
                if ($targetObject/ancestor::decor/project/@prefix = $sourceObject/ancestor::decor[1]/project/@prefix) then () else (
                    if ($targetObject/ancestor::transaction[1]/@url) then () else attribute url {$utillib:strDecorServicesURL},
                    if ($targetObject/ancestor::transaction[1]/@ident) then () else attribute ident {$targetObject/ancestor::decor/project/@prefix}
                )
            ) else (
                attribute targetId {$qid},
                attribute targetEffectiveDate {$qed}
            ),
            (: Mark the thing with the transaction id/effectiveDate to smooth downstream processing :)
            for $attr in $sourceObject/ancestor::transaction[1]/(@id, @effectiveDate, @versionLabel, @statusCode, @expirationDate)
            return
                attribute {'transaction' || upper-case(substring(local-name($attr), 1, 1)) || substring(local-name($attr), 2)} {$attr}
            ,
            (:for $name in $sourceObject/ancestor::transaction[1]/name
            return 
                <transactionName>{$name/@*, $name/node()}</transactionName>
            ,:)
            if ($questionnaire) then (
                $questionnaire/name
                ,
                for $name in $questionnaire[1]/ancestor::decor[1]/project/name
                return 
                    <projectName>{$name/@*, $name/node()}</projectName>
                ,
                (: in inconsistent questionnaires we might encounter multiple elements and/or attributes with the same elementId :)
                for $target in $questionnaire//item[@id]
                return
                    if ($target/@id = $targetObject/@elementId) then
                        <path type="{name($target)}">
                        {
                            string-join(
                                for $ancestor in $target/ancestor-or-self::item
                                let $min    := if ($ancestor/enableWhen) then 0 else if ($ancestor/@required='true') then 1 else 0
                                let $max    := if ($ancestor/@repeats='true') then '*' else 1
                                let $conf   := if ($ancestor/enableWhen) then 'C' else if ($ancestor/@required='true') then 'R' else ()
                                let $mmc    := string-join((string-join(($min,$max), '…'),$conf),' ')
                                return
                                $ancestor/concat(if (self::attribute) then '@' else (),@name,if (empty($mmc)) then () else concat(' (',$mmc,')'))
                            ,' / ')
                        }
                        </path>
                    else ()
            )    
            else ()
        )
    }
    </questionnaireAssociation>
};

(:~ Return a conceptAssociation or conceptMapAssociation based on $sourceObject which is usually element(concept) and 
    a targetObject which is element(inherit), element(contains), element(relationship), element(sourceScope) or element(targetScope) :)
declare function utillib:doConceptAssociation($sourceObject as element(), $targetObject as element(), $targetObjectNames as element(name)*) as element() {
    (: bugfix AD30-1507 - in valueset usage the termonology association leads to ancestor targets instead of the self target :)
    
    let $conceptTarget             :=
        if ($targetObject/name = $targetObjectNames) then $targetObject/self::concept else $targetObject/parent::concept
    
    return
    element {if ($targetObject/ancestor::conceptMap) then 'conceptMapAssociation' else 'conceptAssociation'}
    {
        attribute objectId {$sourceObject/@id},
        if ($sourceObject/@effectiveDate) then attribute objectFlexibility {$sourceObject/@effectiveDate} else ()
        ,
        attribute associationType {
         if ($targetObject[ancestor::conceptMap][self::sourceScope | self::source]) then 'source' else
         if ($targetObject[ancestor::conceptMap][self::targetScope | self::target]) then 'target' else
         if ($sourceObject[self::valueSet]) then 'terminologyAssociation' else local-name($targetObject)
        
        },
        for $attr in ($targetObject/ancestor-or-self::conceptMap | $conceptTarget)[1]/(@* except (@url | @ident))
        return
            attribute {'target' || upper-case(substring(local-name($attr), 1, 1)) || substring(local-name($attr), 2)} {$attr}
        ,
        if ($targetObject[ancestor::conceptMap]) then () else (
            (: only if target is not a conceptMap which is never in a dataset/transaction :)
            for $attr in $targetObject/ancestor::dataset[1]/(@* except (@url | @ident))
            return
                attribute {'targetDataset' || upper-case(substring(local-name($attr), 1, 1)) || substring(local-name($attr), 2)} {$attr}
            ,
            if ($targetObject/ancestor::dataset[1][@url | @ident]) then (
                $targetObject/ancestor::dataset[1]/@url,
                $targetObject/ancestor::dataset[1]/@ident
            ) else
            if ($targetObject/ancestor::decor/project/@prefix = $sourceObject/ancestor::decor/project/@prefix) then () else (
                if ($targetObject/ancestor-or-self::concept[1]/@url) then () else attribute url {$utillib:strDecorServicesURL},
                if ($targetObject/ancestor-or-self::concept[1]/@ident) then () else attribute ident {$targetObject/ancestor::decor/project/@prefix}
            )
            ,
            (: Mark the thing with the transaction id/effectiveDate to smooth downstream processing :)
            for $attr in $sourceObject/ancestor::transaction[1]/(@id, @effectiveDate, @versionLabel, @statusCode, @expirationDate)
            return
                attribute {'transaction' || upper-case(substring(local-name($attr), 1, 1)) || substring(local-name($attr), 2)} {$attr}
        )
        ,
        $targetObjectNames
        ,
        for $name in $targetObject/ancestor::decor[1]/project/name
        return 
            <projectName>{$name/@*, $name/node()}</projectName>
    }
};

(:~ Return a conceptMapAssociation based on $sourceObject which is usually element(concept) and a targetObject which is element(sourceScope) or element(targetScope) :)
declare function utillib:doConceptMapAssociation($sourceObject as element(), $targetObject as element(), $targetObjectNames as element(name)*) as element(conceptMapAssociation) {
    utillib:doConceptAssociation($sourceObject, $targetObject, $targetObjectNames)
};

(:~ Function for getting designations for a terminologyAssociation :)
declare function utillib:conceptExpandedAssociation($concept as element()?, $associations as element()*, $oidnamemap as map(*)?, $language as xs:string?) as element()* {
    for $n in $associations
    let $decor                  := $n/ancestor::decor
    let $codeSystemName         := if (empty($oidnamemap) or empty($n/@codeSystem)) then () else map:get($oidnamemap, $n/@codeSystem)
    let $identifierName         := if (empty($oidnamemap) or empty($n/@ref)) then () else map:get($oidnamemap, $n/@ref)
    let $codeSystemConcept      :=
        if ($n[@code][@codeSystem]) then 
            tcsapi:getConcept(
                map {
                    "parameters": map {
                        "codeSystemId": $n/@codeSystem,
                        "conceptId": $n/@code,
                        "language": $language,
                        "children": 0
                    }
                }
            )
        else ()
    let $vsref                  := $n/@valueSet
    let $vsflex                 := $n/@flexibility
    let $vs                     := if (empty($vsref)) then () else vsapi:getValueSetById($vsref, ($vsflex, 'dynamic')[1])[@id]
    let $valueSetName           := ($vs/@displayName, $vs/@name)[1]
    let $valueSetStatusCode     := $vs[1]/@statusCode
    let $valueSetVersionLabel   := $vs[1]/@versionLabel
    order by $n/@conceptId, $n/@codeSystem, $n/@expirationDate descending
    return
        element {name($n)} {
            $n/(@* except (@codeSystemName|@valueSetName|@valueSetStatusCode|@valueSetVersionLabel|@refdisplay|@designation|@transactionId|@transactionEffectiveDate|@transactionVersionLabel|@transactionStatusCode|@transactionExpirationDate|@transactionName)),
            if (empty($codeSystemName))         then () else attribute codeSystemName {$codeSystemName},
            if (empty($valueSetName))           then () else attribute valueSetName {$valueSetName},
            if (empty($valueSetStatusCode))     then () else attribute valueSetStatusCode {$valueSetStatusCode},
            if (empty($valueSetVersionLabel))   then () else attribute valueSetVersionLabel {$valueSetVersionLabel},
            if (empty($identifierName))         then () else attribute refdisplay {$identifierName},
            
            (: Mark the thing with the transaction id/effectiveDate to smooth downstream processing :)
            for $attr in $concept/ancestor::transaction[1]/(@id, @effectiveDate, @versionLabel, @statusCode, @expirationDate)
            return
                attribute {'transaction' || upper-case(substring(local-name($attr), 1, 1)) || substring(local-name($attr), 2)} {$attr}
            ,
            (:for $name in $concept/ancestor::transaction[1]/name
            return 
                <transactionName>{$name/@*, $name/node()}</transactionName>
            ,:)
            if ($codeSystemConcept/designation/@languageCode) then
                for $l in distinct-values($codeSystemConcept/designation/@languageCode)
                let $d := $codeSystemConcept/designation[@languageCode = $l][1]
                return
                    <designation language="{$l}" displayName="{$d/node()}">
                    {
                        switch ($d/@use = 'fsn') 
                        case 'fsn' return attribute type {'fsn'} 
                        case 'pref'
                        case 'lfsn' return attribute type {'preferred'}
                        default return ()
                        
                    }
                    </designation>
            else 
            if ($n/@displayName) then
                <designation language="{$language}">{$n/@displayName}</designation>
            else (
            )
        }
};

(:~  Return live terminologyAssociations for the project with the given @prefix
:
:   @param $projectPrefix   - required project/@prefix
:   @return all live terminologyAssociations for the given $projectPrefix or nothing if not found
:   @since 2015-09-22
:)
declare function utillib:getTerminologyAssociations($projectPrefix as xs:string) as element(terminologyAssociation)* {
    utillib:getTerminologyAssociations($projectPrefix, (), ())
};
(:~  Return live terminologyAssociations for the project with the given @prefix or terminologyAssociations from an archived $decorVersion
:   Released/compiled matches could be present in multiple languages. Use the parameters @language to select the right one.
:
:   @param $projectPrefix   - required project/@prefix
:   @param $decorVersion    - optional decor/@versionDate
:   @param $language        - optional decor/@language. Only relevant with parameter $decorVersion. Empty uses decor/project/@defaultLanguage, '*' selects all versions, e.g. 'en-US' selects the US English version (if available)
:   @return all live terminologyAssociations for the given $projectPrefix, or all terminologyAssociations from an archived $decorVersion or nothing if not found
:   @since 2015-09-22
:)
declare function utillib:getTerminologyAssociations($projectPrefix as xs:string, $decorVersion as xs:string?) as element(terminologyAssociation)* {
    utillib:getTerminologyAssociations($projectPrefix, $decorVersion, ())
};
declare function utillib:getTerminologyAssociations($projectPrefix as xs:string, $decorVersion as xs:string?, $language as xs:string?) as element(terminologyAssociation)* {
    utillib:getTerminologyAssociationsP(utillib:getDecorByPrefix($projectPrefix, $decorVersion, $language))
};
(:~  Return terminologyAssociations for the project in parameter $decor
:
:   @param $decor           - optional decor project
:   @return all terminologyAssociations for the given project in $decor, or nothing if not found
:   @since 2015-09-22
:)
declare function utillib:getTerminologyAssociationsP($decor as element(decor)?) as element(terminologyAssociation)* {
    $decor/terminology/terminologyAssociation
};

(:~  Return live identifierAssociations for the project with the given @prefix
:
:   @param $projectPrefix   - required project/@prefix
:   @return all live identifierAssociations for the given $projectPrefix or nothing if not found
:   @since 2017-04-05
:)
declare function utillib:getIdentifierAssociations($projectPrefix as xs:string) as element(identifierAssociation)* {
    utillib:getIdentifierAssociations($projectPrefix, (), ())
};
(:~  Return live identifierAssociations for the project with the given @prefix or identifierAssociations from an archived $decorVersion
:   Released/compiled matches could be present in multiple languages. Use the parameters @language to select the right one.
:
:   @param $projectPrefix   - required project/@prefix
:   @param $decorVersion    - optional decor/@versionDate
:   @param $language        - optional decor/@language. Only relevant with parameter $decorVersion. Empty uses decor/project/@defaultLanguage, '*' selects all versions, e.g. 'en-US' selects the US English version (if available)
:   @return all live identifierAssociations for the given $projectPrefix, or all identifierAssociations from an archived $decorVersion or nothing if not found
:   @since 2017-04-05
:)
declare function utillib:getIdentifierAssociations($projectPrefix as xs:string, $decorVersion as xs:string?) as element(identifierAssociation)* {
    utillib:getIdentifierAssociations($projectPrefix, $decorVersion, ())
};
declare function utillib:getIdentifierAssociations($projectPrefix as xs:string, $decorVersion as xs:string?, $language as xs:string?) as element(identifierAssociation)* {
    utillib:getIdentifierAssociationsP(utillib:getDecorByPrefix($projectPrefix, $decorVersion, $language))
};
(:~  Return identifierAssociations for the project in parameter $decor
:
:   @param $decor           - optional decor project
:   @return all identifierAssociations for the given project in $decor, or nothing if not found
:   @since 2017-04-05
:)
declare function utillib:getIdentifierAssociationsP($decor as element(decor)?) as element(identifierAssociation)* {
    $decor/ids/identifierAssociation
};

(:~ Convenience function that adds a list of identifierAssociations and terminologyAssociations through respective functions and returns the combined output thereof :)
declare function utillib:addAssociations($associations as element()*, $decorOrtransactionConcept as element(), $replacemode as xs:boolean, $testmode as xs:boolean) as element(result) {
    <return>
    {
        utillib:addTerminologyAssociations($associations[self::terminologyAssociation], $decorOrtransactionConcept, $replacemode, $testmode)/node()
        ,
        utillib:addIdentifierAssociations($associations[self::identifierAssociation], $decorOrtransactionConcept, $replacemode, $testmode)/node()
    }
    </return>
};
(:~  Add a list of terminologyAssociations in the project in parameter $decor. Each association is checked for existence so we don't add twice. With $replacemode=true we delete first and add as new. 
:   With $testmode=true we do not change anything in the project but only return what would happen *if* the given list was applied. For a terminology association to a value set to work it is important
:   that there is a matching value set (ref) and the project contains the right building block repository reference in the project section if applicable. This is all checked and handled.
:
:   Known issue: does not currently handle adding multiple building block repository reference when a value set lives in multiple
:
:   @param $terminologyAssociations     - 0..* terminologyAssociation elements to add
:   @param $decor                       - 1..1 decor project to add the associations to
:   @param $replacemode                 - 1..1 boolean. true will delete each association first and add as new. false will only add if not present
:   @param $testmode                    - 1..1 boolean. true will give back what would happen if changes would be applied but doesn't change anything. false will apply whatever changes necessary
:   @return a report of changes there were done ($testmode=false) or would be done ($testmode=true)
:           <result>
:               <terminologyAssociation conceptId="x" @*>               (contains only attributes as they would be written, which might be less than what was supplied)
:                   <add>true|false</add>                               (contains true if association is/would be added with attributes as indicated)
:                   <add-ref>true|false</add-ref>                       (only present for value set associations. contains true if association is a value set that did not exist in the project yet)
:                   <add-bbr url="x" ident="y">true|false</add-bbr>     (only present for value set associations. contains true if value set lives in a repository that this project does not reference yet. @url|@ident contain the reference details)
:               </terminologyAssociation>
:           </result>
:   @since 2015-09-22
:)
declare function utillib:addTerminologyAssociations($terminologyAssociations as element(terminologyAssociation)*, $decorOrtransactionConcept as element(), $replacemode as xs:boolean, $testmode as xs:boolean) as element(result) {
    let $decor                          := $decorOrtransactionConcept/ancestor-or-self::decor
    let $concept                        := $decorOrtransactionConcept[self::concept][ancestor::representingTemplate]
    let $allStoredAssociations         := utillib:getTerminologyAssociationsP($decor)
    
    (: radical solution: delete first, insert later, might be too harsh :)
    return
    <result>
    {
        for $terminologyAssociation in $terminologyAssociations
        let $s_cid                  := $terminologyAssociation/@conceptId[string-length()>0]
        let $s_ceff                 := $terminologyAssociation/@conceptFlexibility[string-length()>0]
        let $s_valueSet             := $terminologyAssociation/@valueSet[string-length()>0]
        let $s_valueSetFlexibility  := $terminologyAssociation/@flexibility[string-length()>0]
        let $s_code                 := $terminologyAssociation/@code[string-length()>0]
        let $s_codeSystem           := $terminologyAssociation/@codeSystem[string-length()>0]
        let $rewrittedAssociation   := utillib:prepareTerminologyAssociationForUpdate($terminologyAssociation, exists($concept))
        let $currentStoredAssocs    := ($allStoredAssociations[@conceptId=$s_cid][@valueSet=$s_valueSet] | 
                                        $allStoredAssociations[@conceptId=$s_cid][@code=$s_code][@codeSystem=$s_codeSystem])
        let $deleteAssoc            :=
            if ($testmode) then () else if ($replacemode) then (update delete $currentStoredAssocs) else ()
        let $storeAssoc             :=
            if ($currentStoredAssocs or empty($rewrittedAssociation)) then (false()) else (
                let $x              :=
                    if ($testmode) then ()
                    else if ($concept) then (
                        if ($concept/identifierAssociation) then
                            update insert $rewrittedAssociation preceding $concept/identifierAssociation[1]
                        else (
                            update insert $rewrittedAssociation into $concept
                        )
                    )
                    else if (not($decor/terminology)) then (
                        update insert <terminology>{$rewrittedAssociation}</terminology> following $decor/ids
                    )
                    else if ($decor/terminology/terminologyAssociation) then (
                        update insert $rewrittedAssociation preceding $decor/terminology/terminologyAssociation[1]
                    )
                    else if ($decor/terminology/*) then (
                        update insert $rewrittedAssociation preceding $decor/terminology/*[1]
                    )
                    else (
                        update insert $rewrittedAssociation into $decor/terminology
                    )
                return true()
            )
        
        (: === ValuetSet and BuildingBlockRepository handling === :)
        let $currentStoredRefs      := ($decor/terminology/valueSet[@id=$s_valueSet] | 
                                        $decor/terminology/valueSet[@ref=$s_valueSet] | 
                                        $decor/terminology/valueSet[@name=$s_valueSet])
        let $valueSetExists         := if ($s_valueSet) then (exists($currentStoredRefs)) else (true())
        let $conceptList            := if ($valueSetExists) then () else $setlib:colDecorData//conceptList[@id=$s_cid][not(ancestor::history)]
        let $valueSet               := 
            if ($conceptList) then (
                vsapi:getValueSetByRef($s_valueSet, $s_valueSetFlexibility, $conceptList/ancestor::decor/project/@prefix, (), ())//valueSet[@id] 
            )[1] else ()
        
        let $valueSetRef            :=
            if ($valueSet) then (
                <valueSet ref="{$valueSet/@id}" name="{$valueSet/@name}" displayName="{if ($valueSet/@displayName[not(.='')]) then $valueSet/@displayName else $valueSet/@name}"/>
            ) else ()
        let $deleteValueSetRef      :=
            if ($testmode) then () else if ($replacemode) then (update delete $currentStoredRefs) else ()
        let $storeValueSetRef       :=
            if ($valueSetRef) then (
                let $x              :=
                    if ($testmode) then ()
                    else if (not($decor/terminology)) then (
                        update insert <terminology>{$valueSetRef}</terminology> following $decor/ids
                    )
                    else if ($decor/terminology/valueSet) then (
                        update insert $valueSetRef following $decor/terminology/valueSet[last()]
                    )
                    else if ($decor/terminology/*) then (
                        update insert $valueSetRef following $decor/terminology/*[last()]
                    )
                    else (
                        update insert $valueSetRef into $decor/terminology
                    )
                return true()
            ) else (false())
            
        let $bbrRef                 := 
            if ($valueSet) then (
                if ($valueSet/parent::repository) then (
                    <buildingBlockRepository url="{$valueSet/../@url}" ident="{$valueSet/../@ident}"/>
                ) else if ($valueSet/parent::project) then (
                    <buildingBlockRepository url="{$utillib:strDecorServicesURL}" ident="{$valueSet/../@ident}"/>
                ) else ()
            ) else ()
        let $storeBBR               :=
            if ($bbrRef and not($decor/project/buildingBlockRepository[@url=$bbrRef/@url][@ident=$bbrRef/@ident][empty(@format)] |
                                $decor/project/buildingBlockRepository[@url=$bbrRef/@url][@ident=$bbrRef/@ident][@format='decor'])) then (
                let $x              :=
                    if ($testmode) then ()
                    else (update insert $bbrRef following $decor/project/(author|reference|restURI|defaultElementNamespace|contact)[last()])
                return true()
            ) else (false())
        return (
            element {$rewrittedAssociation/name()} {
                $rewrittedAssociation/@*,
                <add>{$storeAssoc}</add>,
                if ($s_valueSet) then (
                    <add-ref>{$storeValueSetRef}</add-ref>,
                    <add-bbr>{if ($bbrRef) then ($bbrRef/(@url|@ident)) else (),$storeBBR}</add-bbr>
                ) else ()
            }
        )
    }
    </result>
};

(:~  Return only valid, non empty attributes on a terminology association as defined in the DECOR format. Note: does not check co-occurence problems such as @valueSet + @code
:
:   @param $terminologyAssociation     - 0..1 terminologyAssociation elements
:   @param $allowEmpty                - true|false only relevant for transaction concepts when you need to explicitly exclude a dataset association in a transaction
:   @return the pruned terminologyAssociation element for the given project in $decor, or nothing input was empty
:   @since 2015-09-22
:)
declare function utillib:prepareTerminologyAssociationForUpdate($terminologyAssociation as element()?) as element(terminologyAssociation)? {
    utillib:prepareTerminologyAssociationForUpdate($terminologyAssociation, false())
};
declare function utillib:prepareTerminologyAssociationForUpdate($terminologyAssociation as element()?, $allowEmpty as xs:boolean) as element(terminologyAssociation)? {
    if ($terminologyAssociation[@valueSet[not(.='')]] | $terminologyAssociation[@code[not(.='')]][@codeSystem[not(.='')]] | $terminologyAssociation[$allowEmpty][not(@valueSet | @code | @codeSystem)]) then (
        <terminologyAssociation>
        {
            $terminologyAssociation/@conceptId,
            $terminologyAssociation[string-length(@valueSet) = 0]/@conceptFlexibility[not(.='')]
            ,
            if ($terminologyAssociation[@valueSet[not(.='')]]) then (
                $terminologyAssociation/@valueSet,
                $terminologyAssociation/@flexibility[not(.='')],
                $terminologyAssociation/@strength[not(.='')]
            )
            else
            if ($terminologyAssociation[@code[not(.='')]][@codeSystem[not(.='')]]) then (
                $terminologyAssociation/@code,
                $terminologyAssociation/@codeSystem,
                $terminologyAssociation/@codeSystemName[not(.='')],
                $terminologyAssociation/@codeSystemVersion[not(.='')],
                $terminologyAssociation/@displayName[not(.='')],
                $terminologyAssociation/@equivalence[not(.='')]
            )
            else ()
            ,
            if ($terminologyAssociation/@effectiveDate[. castable as xs:dateTime]) then (
                $terminologyAssociation/@effectiveDate
            )
            else (
                attribute effectiveDate {format-dateTime(current-dateTime(),'[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]', (), (), ())}
            )
            ,
            $terminologyAssociation/@expirationDate[. castable as xs:dateTime], 
            $terminologyAssociation/@officialReleaseDate[. castable as xs:dateTime], 
            $terminologyAssociation/@versionLabel[not(.='')]
        }
        </terminologyAssociation>
    ) else ()
};

(:~  Return only valid, non empty attributes on a valueSet as defined in the DECOR format.
:   Example baseValueset:
:       <valueSet id="..." name="..." displayName="..." effectiveDate="yyyy-mm-ddThh:mm:ss" statusCode="draft" versionLabel="..."/>
:
:   @param $editedValueSet     - 1..1 valueSet as edited
:   @param $baseValueset       - 1..1 valueSet element containing all attributes that should be set on the prepared valueSet, like new id/status etc.
:   @return the pruned valueSet element for the given project in $decor, or nothing input was empty
:   @since 2015-09-22
:)
declare function utillib:prepareValueSetForUpdate($editedValueset as element(valueSet), $baseValueset as element(valueSet)) as element(valueSet) {
    <valueSet>
    {
        $baseValueset/(@*[not(. = '')] except @lastModifiedDate),
        attribute lastModifiedDate {substring(string(current-dateTime()), 1, 19)}
    }
    {
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($editedValueset/desc)
        ,
        utillib:prepareValueSetRelationshipForUpdate($editedValueset/relationship)
        ,
        utillib:preparePublishingAuthorityForUpdate($editedValueset/publishingAuthority[empty(@inherited)])
        ,
        (:utillib:preparePublishingAuthorityForUpdate($editedValueset/endorsingAuthority)
        ,:)
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($editedValueset/purpose)
        ,
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($editedValueset/copyright[empty(@inherited)])
    }
    {
        ()
        (:want at least a @date on revisionHistory for it to persist:)
        (:for $revisionHistory in $editedValueset/revisionHistory[string-length(@date)>0]
        return
        <revisionHistory>
        {
            $revisionHistory/@*[string-length()>0],
            for $desc in $revisionHistory/desc
            return
                utillib:parseNode($desc)
        }
        </revisionHistory>:)
    }
    {
        utillib:prepareValueSetConceptListForUpdate(
            $editedValueset/completeCodeSystem | 
            $editedValueset/items[@is = 'completeCodeSystem'] | 
            $editedValueset/conceptList/* | 
            $editedValueset/items[not(@is = 'completeCodeSystem')]
        )
    }
    </valueSet>
};

(:~ Prepares a node of type relationship as found on valuesets for update in the database. Only returns nodes that have a type 
    and a ref attribute. 
    This element is **not** the same as template/relationship
    
@param $in array of relationship nodes
returns array of relationship nodes in the same order but without empty ones
:)
declare function utillib:prepareValueSetRelationshipForUpdate($in as element(relationship)*) as element(relationship)* {
    for $node in $in[@type[not(. = '')]][@ref[not(. = '')]]
    let $id := $node/@ref
    let $fl := ($node/@flexibility[. castable as xs:dateTime], 'dynamic')[1]
    group by $id, $fl
    return
        element {name($node[1])} {
            $node[1]/@type,
            $node[1]/@ref,
            $node[1]/@flexibility[. castable as xs:dateTime or . = 'dynamic']
        }
};

declare function utillib:prepareValueSetConceptListForUpdate($editedConceptList as element()*) as element(conceptList)? {
    if ($editedConceptList[self::items[@is = ('completeCodeSystem','concept', 'include', 'exclude', 'exception')]|self::completeCodeSystem|self::concept|self::include|self::exclude|self::exception]) then
        <conceptList>
        {
            for $completeCodeSystem in  $editedConceptList[self::completeCodeSystem][string-length(@codeSystem) gt 0] | 
                                        $editedConceptList[self::items[@is = 'completeCodeSystem']][string-length(@codeSystem) gt 0]
            return
                <include>
                {
                    $completeCodeSystem/@codeSystem, 
                    $completeCodeSystem/@codeSystemName[not(. = '')], 
                    $completeCodeSystem/@codeSystemVersion[not(. = '')], 
                    $completeCodeSystem/@flexibility[. castable as xs:dateTime], 
                    $completeCodeSystem/@type[. = 'D'],
                    for $f in $completeCodeSystem/filter[(@property|@op|@value)[not(. = '')]]
                    return
                        <filter>{$f/@property[not(. = '')], $f/@op[not(. = '')], $f/@value[not(. = '')]}</filter>
                }
                </include>
            ,
            for $concept in (   $editedConceptList[self::items][@is='concept'][string-length(@code)>0][string-length(@codeSystem)>0] |
                                $editedConceptList[self::items][@is='include'][(@ref|@op|@codeSystem)[string-length()>0]][not(@exception='true')] |
                                $editedConceptList[self::items][@is='exclude'][(@op|@codeSystem)[string-length()>0]][not(@exception='true')] |
            
                                $editedConceptList[self::concept][string-length(@code)>0][string-length(@codeSystem)>0] |
                                $editedConceptList[self::include][(@ref|@op|@codeSystem)[string-length()>0]][not(@exception='true')] |
                                $editedConceptList[self::exclude][(@op|@codeSystem)[string-length()>0]][not(@exception='true')])
            let $elmname    := ($concept/@is, $concept/name())[1]
            return
            element {$elmname}
            {
                switch ($elmname)
                case 'concept' return ($concept/@code, $concept/@codeSystem, $concept/@codeSystemName, $concept/@codeSystemVersion, $concept/@displayName, $concept/@ordinal[. castable as xs:decimal], $concept/@level, $concept/@type)
                default return (
                    $concept/@op, $concept/@code, $concept/@codeSystem, $concept/@codeSystemName, $concept/@codeSystemVersion, $concept/@displayName, $concept/@ref, $concept/@flexibility[. castable as xs:dateTime] | $concept/@flexibility[. = 'dynamic'], $concept/@exception[. = ('true', 'false')], $concept/@type[. = 'D']
                )
                ,
                for $desc in $concept/designation[@displayName[string-length()>0]] return utillib:parseNode($desc),
                utillib:prepareFreeFormMarkupWithLanguageForUpdate($concept/desc),
                for $f in $concept/filter[(@property|@op|@value)[not(. = '')]]
                return
                    <filter>{$f/@property[not(. = '')], $f/@op[not(. = '')], $f/@value[not(. = '')]}</filter>
            }
            ,
            for $concept in (   $editedConceptList[self::items][@is='exception'][string-length(@code) gt 0][string-length(@codeSystem) gt 0] |
                                $editedConceptList[self::items][@is='include'][(@ref|@op|@codeSystem)[string-length()>0]][@exception='true'] |
                                $editedConceptList[self::items][@is='exclude'][(@op|@codeSystem)[string-length() gt 0]][@exception='true'] |
            
                                $editedConceptList[self::exception][string-length(@code) gt 0][string-length(@codeSystem) gt 0] |
                                $editedConceptList[self::include][(@ref|@op|@codeSystem)[string-length() gt 0]][@exception='true'] |
                                $editedConceptList[self::exclude][(@op|@codeSystem)[string-length() gt 0]][@exception='true'])
            let $elmname  := ($concept/@is, $concept/name())[1]
            return
            element {$elmname}
            {
                switch ($elmname)
                case 'exception' return ($concept/@code, $concept/@codeSystem, $concept/@codeSystemName, $concept/@codeSystemVersion, $concept/@displayName, $concept/@ordinal[. castable as xs:decimal], $concept/@level, $concept/@type)
                default return (
                    $concept/@op, $concept/@code, $concept/@codeSystem, $concept/@codeSystemName, $concept/@codeSystemVersion, $concept/@displayName, $concept/@ref, $concept/@flexibility[. castable as xs:dateTime] | $concept/@flexibility[ . = 'dynamic'], $concept/@exception[. = ('true', 'false')], $concept/@type[. = 'D']
                )
                ,
                for $desc in $concept/designation[@displayName[string-length() gt 0]] return utillib:parseNode($desc),
                utillib:prepareFreeFormMarkupWithLanguageForUpdate($concept/desc),
                for $f in $concept/filter[(@property|@op|@value)[not(. = '')]]
                return
                    <filter>{$f/@property[not(. = '')], $f/@op[not(. = '')], $f/@value[not(. = '')]}</filter>
            }
        }
        </conceptList>
    else ()
};

(:~  Return only valid, non empty attributes on a conceptMap as defined in the DECOR format.
:   Example baseConceptMap:
:       <conceptMap id="..." name="..." displayName="..." effectiveDate="yyyy-mm-ddThh:mm:ss" statusCode="draft" versionLabel="..."/>
:
:   @param $editedConceptMap   - 1..1 conceptMap as edited
:   @param $baseConceptMap     - 1..1 conceptMap element containing all attributes that should be set on the prepared conceptMap, like new id/status etc.
:   @return the pruned conceptMap element for the given project in $decor, or nothing input was empty
:   @since 2015-09-22
:)
declare function utillib:prepareConceptMapForUpdate($editedConceptMap as element(conceptMap), $baseConceptMap as element(conceptMap)) as element(conceptMap) {
    <conceptMap>
    {
        $baseConceptMap/(@*[not(. = '')] except @lastModifiedDate),
        attribute lastModifiedDate {substring(string(current-dateTime()), 1, 19)}
    }
    {
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($editedConceptMap/desc)
        ,
        utillib:preparePublishingAuthorityForUpdate($editedConceptMap/publishingAuthority[empty(@inherited)])
        ,
        (:utillib:preparePublishingAuthorityForUpdate($editedConceptMap/endorsingAuthority)
        ,:)
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($editedConceptMap/purpose)
        ,
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($editedConceptMap/copyright[empty(@inherited)])
        ,
        utillib:prepareConceptMapScopeForUpdate($editedConceptMap/sourceScope)
        ,
        utillib:prepareConceptMapScopeForUpdate($editedConceptMap/targetScope)
        ,
        utillib:prepareConceptMapGroupForUpdate($editedConceptMap/group)
    }
    </conceptMap>
};
(: expect: element(sourceScope) or element(targetScope) :)
declare function utillib:prepareConceptMapScopeForUpdate($in as element()?) as element()? {
    for $node in $in
    return
        element {name($node)}
        {
            $node/@ref[not(. = '')], $node/@flexibility[not(. = '')], $node/@displayName[not(. = '')], $node/@canonicalUri[not(. = '')] 
        }
};
declare function utillib:prepareConceptMapGroupForUpdate($groups as element(group)*) as element(group)* {
    for $group in $groups
    return
        <group>
        {
            utillib:prepareConceptMapSourceTargetForUpdate($group/source),
            utillib:prepareConceptMapSourceTargetForUpdate($group/target),
            utillib:prepareConceptMapElementForUpdate($group/element)
        }
        </group>
};
(: expect: element(source) or element(target) :)
declare function utillib:prepareConceptMapSourceTargetForUpdate($in as element()*) as element()* {
    for $node in $in
    let $key := $node/@codeSystem[not(. = '')] || $node/@codeSystemVersion[not(. = '')] || $node/@displayName[not(. = '')] || $node/@canonicalUri[not(. = '')]
    group by $key
    return
        element {name($node[1])}
        {
            $node[1]/@codeSystem[not(. = '')], $node[1]/@codeSystemVersion[not(. = '')], $node[1]/@displayName[not(. = '')], $node[1]/@canonicalUri[not(. = '')] 
        }
};
declare function utillib:prepareConceptMapElementForUpdate($in as element(element)*) as element(element)* {
    for $node in $in
    return
        <element>
        {
            $node/@code[not(. = '')], $node/@displayName[not(. = '')],
            $in/target
        }
        </element>
};

declare function utillib:prepareCodeSystemForUpdate($editedCodeSystem as element(codeSystem), $baseCodeSystem as element(codeSystem)) as element(codeSystem) {
    <codeSystem>
    {
        $baseCodeSystem/(@*[not(. = '')] except @lastModifiedDate),
        attribute lastModifiedDate {substring(string(current-dateTime()), 1, 19)}
    }
    {
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($editedCodeSystem/desc)
        ,
        utillib:preparePublishingAuthorityForUpdate($editedCodeSystem/publishingAuthority[empty(@inherited)])
        ,
        (:utillib:preparePublishingAuthorityForUpdate($editedValueset/endorsingAuthority)
        ,:)
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($editedCodeSystem/purpose)
        ,
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($editedCodeSystem/copyright[empty(@inherited)])
        ,
        utillib:prepareCodeSystemPropertyForUpdate($editedCodeSystem/property)
    }
    {
        ()
        (:want at least a @date on revisionHistory for it to persist:)
        (:for $revisionHistory in $editedValueset/revisionHistory[string-length(@date)>0]
        return
        <revisionHistory>
        {
            $revisionHistory/@*[string-length()>0],
            for $desc in $revisionHistory/desc
            return
                utillib:parseNode($desc)
        }
        </revisionHistory>:)
    }

    {
       (:utillib:prepareCodeSystemConceptListForUpdate($editedCodeSystem/conceptList/* | $editedCodeSystem/items[not(@is = 'completeCodeSystem')]):)
       let $conceptList    := 
            utillib:prepareCodeSystemConceptListForUpdate($editedCodeSystem/conceptList, $baseCodeSystem/conceptList)
       return (
            for $prop in $conceptList//property
            let $propertyCode   := $prop/@code
            group by $propertyCode
            return
             if ($editedCodeSystem/property[@code = $propertyCode]) then () else (
                 <property>
                 {
                     $propertyCode 
                     ,
                     switch ($propertyCode)
                     case 'retirementDate' return (
                         attribute uri {'http://hl7.org/fhir/concept-properties#retirementDate'},
                         attribute description {'Date Concept was retired'},
                         attribute type {'dateTime'}
                     )
                     case 'deprecationDate' return (
                         attribute uri {'http://hl7.org/fhir/concept-properties#deprecationDate'},
                         attribute description {'Date Concept was deprecated'},
                         attribute type {'dateTime'}
                     )
                     case 'notSelectable' return (
                         attribute uri {'http://hl7.org/fhir/concept-properties#notSelectable'},
                         attribute description {'This concept is a grouping concept and not intended to be used in the normal use of the code system (though may be used for filters etc.). This is also known as ''Abstract'''},
                         attribute type {'boolean'}
                     )
                     default return (
                         attribute type {
                             switch (($prop/*)[1]/local-name())
                             case 'valueCode'        return 'code'
                             case 'valueCoding'      return 'Coding'
                             case 'valueString'      return 'string'
                             case 'valueInteger'     return 'integer'
                             case 'valueBoolean'     return 'boolean'
                             case 'valueDateTime'    return 'dateTime'
                             case 'valueDecimal'     return 'decimal'
                             default return (
                                 error($errors:SERVER_ERROR, 'CodedConcept property ' || $propertyCode || ' has unsupported contents: ' || ($prop/*)[1]/local-name() || '. This is a data error or the util-lib.xqm needs to be updated for it.')
                             )
                         }
                     )
                 }
                 </property>
             )
            ,
            $conceptList
         )
    }
    </codeSystem>
};
declare function utillib:prepareCodeSystemConceptListForUpdate($editedConceptList as element(conceptList)?, $baseConceptList as element(conceptList)?) as element(conceptList)? {
    if ($editedConceptList/codedConcept[@code]) then
        <conceptList>
        {
            utillib:prepareCodeSystemCodedConceptForUpdate($editedConceptList/codedConcept[@code], $baseConceptList/codedConcept)
        }
        </conceptList>
    else ()
};
declare function utillib:prepareCodeSystemCodedConceptForUpdate($editedConcept as element(codedConcept)*, $baseConcept as element(codedConcept)*) as element(codedConcept)* {
    for $concept in $editedConcept[@code]
    let $elmname    := ($concept/@is, $concept/name())[1]
    let $base       := $baseConcept[@code = $concept/@code]
    return
    element {$elmname}
    {
        $concept/@code, $concept/@statusCode, $concept/@displayName, 
        $concept/@level, 
        switch ($concept/@statusCode)
        case 'deprecated'
        case 'retired'
        case 'rejected'
        case 'cancelled' return attribute type {'D'}
        default return 
            if ($concept/@type[not(. = 'D')]) then
                $concept/@type
            else
            (: http://hl7.org/fhir/concept-properties#notSelectable :)
            if ($concept/property[@code = 'notSelectable']/valueBoolean[@value = 'true']) then 
                attribute type {'A'}
            else 
            if ($editedConcept[parent[@code = $concept/@code]] | $baseConcept[parent[@code = $concept/@code]]) then 
                attribute type {'S'} 
            else ( 
                attribute type {'L'}
            )
        ,
        $concept/@ordinal[. castable as xs:decimal],
        $concept/@effectiveDate[. castable as xs:dateTime],
        $concept/@expirationDate[. castable as xs:dateTime],
        $concept/@officialReleaseDate[. castable as xs:dateTime],
        $concept/@lastModifiedDate[. castable as xs:dateTime],
        $concept/@versionLabel
        ,
        for $desc in $concept/designation[@displayName[string-length() gt 0]] return utillib:parseNode($desc)
        ,
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($concept/desc)
        , 
        (:utillib:prepareCodedConceptPropertyForUpdate($concept/(property except property[@code = ('retirementDate', 'deprecationDate', 'notSelectable')])):)
        utillib:prepareCodedConceptPropertyForUpdate($concept/(property except property[@code = ('retirementDate', 'deprecationDate')]))
        ,
        (:http://hl7.org/fhir/concept-properties#retirementDate : date	Date Concept was retired:)
        if ($concept/@statusCode = 'retired' and empty($base[@statusCode = 'retired'])) then
            <property code="retirementDate"><valueDateTime value="{current-dateTime()}"/></property>
        else 
        if ($concept/@statusCode = ('retired', 'deprecated')) then
            utillib:prepareCodedConceptPropertyForUpdate($concept/property[@code = 'retirementDate'])
        else ()
        ,
        (:http://hl7.org/fhir/concept-properties#deprecationDate : date	Date Concept was deprecated:)
        if ($concept/@statusCode = 'deprecated' and empty($base[@statusCode = 'deprecated'])) then
            <property code="deprecationDate"><valueDateTime value="{current-dateTime()}"/></property>
        else 
        if ($concept/@statusCode = ('retired', 'deprecated')) then
            utillib:prepareCodedConceptPropertyForUpdate($concept/property[@code = 'deprecationDate'])
        else ()
        (:,
        (\:http://hl7.org/fhir/concept-properties#notSelectable : boolean	This concept is a grouping concept and not intended to be used in the normal use of the code system (though may be used for filters etc.). This is also known as 'Abstract':\)
        if ($concept/@type = 'A') then
            <property code="notSelectable"><valueBoolean value="true"/></property>
        else (
            utillib:prepareCodedConceptPropertyForUpdate($concept/property[@code = 'notSelectable'])
        ):)
        ,
        for $parent in $concept/parent[@code]
        return
            <parent>{$parent/@code}</parent>
        (:,
        for $child in $concept/child[@code]
        return
            <child>{$child/@code}</child>:)
    }
};
(:~ Prepares a node of type property for update in the database. Only returns nodes that have a name attribute and descendant text contents. 
    Examples of property include codeSystem/property. This property element is **not** the same as concept/property, 
    concept/valueDomain/property, template/classification/property or template//element/property
    
@param $in array of property nodes
returns array of property nodes in the same order but without empty ones
:)
declare function utillib:prepareCodeSystemPropertyForUpdate($in as element(property)*) as element(property)* {
    for $node in $in[@code[not(. = '')]]
    return
        element {name($node)} {
            $node/@code,
            $node/@uri[not(. = '')],
            $node/@description[not(. = '')],
            $node/@type[not(. = '')]
        }
};
(:~ Prepares a node of type property for update in the database. Only returns nodes that have a name attribute and descendant text contents. 
    Examples of property include codeSystem/conceptList/codedConcept/property. This property element is **not** the same as codeSystem/property, 
    concept/property, concept/valueDomain/property, template/classification/property or template//element/property
    
    <property code="code">
        <valueCode code="code"/>
    </property>
    <property code="Coding">
        <valueCoding code="code" canonicalUri="urn:hl7-v3:org" codeSystem="1.2.3"/>
    </property>
    <property code="string">
        <valueString value="string"/>
    </property>
    <property code="integer">
        <valueInteger value="5"/>
    </property>
    <property code="boolean">
        <valueBoolean value="true"/>
    </property>
    <property code="dateTime">
        <valueDateTime value="2022-05-05T00:00:00+02:00"/>
    </property>
    <property code="decimal">
        <valueDecimal value="12.0"/>
    </property>
    
@param $in array of property nodes
returns array of property nodes in the same order but without empty ones
:)
declare function utillib:prepareCodedConceptPropertyForUpdate($in as element(property)*) as element(property)* {
    for $node in $in[@code[not(. = '')]]
    let $propertyCode       := $node/@code
    let $codedConceptCode   := $node/../@code
    return
        element {name($node)} {
            $node/@code,
            for $dt in $node/*
            let $propertyDatatype   := name($dt)
            return
                switch ($propertyDatatype)
                case 'valueCode' return (
                    if ($dt/@code) then 
                        if ($dt[@* except @code] | $dt[*]) then 
                            error($errors:BAD_REQUEST, 'CodedConcept ' || $codedConceptCode || ' property ' || $propertyCode || ' has unsupported contents: ' || $propertyDatatype || ' SHALL NOT have any other particles than code.')
                        else (
                            element {$propertyDatatype} {attribute code {normalize-space($dt/@code)}}
                        )
                    else (
                        error($errors:BAD_REQUEST, 'CodedConcept ' || $codedConceptCode || ' property ' || $propertyCode || ' has unsupported contents: ' || $propertyDatatype || ' SHALL have a code.')
                    )
                )
                case 'valueCoding' return (
                    if ($dt[@code][@canonicalUri | @codeSystem]) then 
                        if ($dt[@* except (@code | @canonicalUri | @codeSystem)] | $dt[*]) then 
                            error($errors:BAD_REQUEST, 'CodedConcept ' || $codedConceptCode || ' property ' || $propertyCode || ' has unsupported contents: ' || $propertyDatatype || ' SHALL NOT have any other particles than code, canonicalUri and/or codeSystem.')
                        else
                        if ($dt[@codeSystem[not(. = '')]][not(utillib:isOid(@codeSystem))]) then
                            error($errors:BAD_REQUEST, 'CodedConcept ' || $codedConceptCode || ' property ' || $propertyCode || ' has unsupported contents: ' || $propertyDatatype || ' codeSystem SHALL be a valid OID if present. Found: "' || $dt/@codeSystem || '"' )
                        else (
                            element {$propertyDatatype} {attribute code {normalize-space($dt/@code)}, $dt/@canonicalUri[not(. = '')], $dt/@codeSystem[not(. = '')]}
                        )
                    else (
                        error($errors:BAD_REQUEST, 'CodedConcept ' || $codedConceptCode || ' property ' || $propertyCode || ' has unsupported contents: ' || $propertyDatatype || ' SHALL have a code with at least one of canonicalUri or codeSystem.')
                    )
                )
                case 'valueString' return (
                    if ($dt/@value) then 
                        if ($dt[@* except @value] | $dt[*]) then 
                            error($errors:BAD_REQUEST, 'CodedConcept ' || $codedConceptCode || ' property ' || $propertyCode || ' has unsupported contents: ' || $propertyDatatype || ' SHALL NOT have any other particles than value.')
                        else (
                            element {$propertyDatatype} {$dt/@value}
                        )
                    else (
                        error($errors:BAD_REQUEST, 'CodedConcept ' || $codedConceptCode || ' property ' || $propertyCode || ' has unsupported contents: ' || $propertyDatatype || ' SHALL have a value.')
                    )
                )
                case 'valueInteger' return (
                    if ($dt/@value castable as xs:integer) then 
                        if ($dt[@* except @value] | $dt[*]) then 
                            error($errors:BAD_REQUEST, 'CodedConcept ' || $codedConceptCode || ' property ' || $propertyCode || ' has unsupported contents: ' || $propertyDatatype || ' SHALL NOT have any other particles than value.')
                        else (
                            element {$propertyDatatype} {attribute value {xs:integer($dt/@value)}}
                        )
                    else (
                        error($errors:BAD_REQUEST, 'CodedConcept ' || $codedConceptCode || ' property ' || $propertyCode || ' has unsupported contents: ' || $propertyDatatype || ' SHALL have an integer value. Found: "' || $dt/@value || '"' )
                    )
                )
                case 'valueBoolean' return (
                    if ($dt/@value castable as xs:boolean) then 
                        if ($dt[@* except @value] | $dt[*]) then 
                            error($errors:BAD_REQUEST, 'CodedConcept ' || $codedConceptCode || ' property ' || $propertyCode || ' has unsupported contents: ' || $propertyDatatype || ' SHALL NOT have any other particles than value.')
                        else (
                            element {$propertyDatatype} {$dt/@value}
                        )
                    else (
                        error($errors:BAD_REQUEST, 'CodedConcept ' || $codedConceptCode || ' property ' || $propertyCode || ' has unsupported contents: ' || $propertyDatatype || ' SHALL have a boolean value. Found: "' || $dt/@value || '"' )
                    )
                )
                case 'valueDateTime' return (
                    if ($dt/@value castable as xs:dateTime) then 
                        if ($dt[@* except @value] | $dt[*]) then 
                            error($errors:BAD_REQUEST, 'CodedConcept ' || $codedConceptCode || ' property ' || $propertyCode || ' has unsupported contents: ' || $propertyDatatype || ' SHALL NOT have any other particles than value.')
                        else (
                            element {$propertyDatatype} {$dt/@value}
                        )
                    else (
                        error($errors:BAD_REQUEST, 'CodedConcept ' || $codedConceptCode || ' property ' || $propertyCode || ' has unsupported contents: ' || $propertyDatatype || ' SHALL have a dateTime value. Found: "' || $dt/@value || '"' )
                    )
                )
                case 'valueDecimal' return (
                    if ($dt/@value castable as xs:decimal) then 
                        if ($dt[@* except @value] | $dt[*]) then 
                            error($errors:BAD_REQUEST, 'CodedConcept ' || $codedConceptCode || ' property ' || $propertyCode || ' has unsupported contents: ' || $propertyDatatype || ' SHALL NOT have any other particles than value.')
                        else (
                            element {$propertyDatatype} {$dt/@value}
                        )
                    else (
                        error($errors:BAD_REQUEST, 'CodedConcept ' || $codedConceptCode || ' property ' || $propertyCode || ' has unsupported contents: ' || $propertyDatatype || ' SHALL have a decimal value. Found: "' || $dt/@value || '"' )
                    )
                )
                default return (
                    error($errors:SERVER_ERROR, 'CodedConcept ' || $codedConceptCode || ' property ' || $propertyCode || ' has unsupported contents: ' || $propertyDatatype || '. This is a data error or the util-lib.xqm needs to be updated for it.')
                )
        }
};

(:~  Add a list of identifierAssociations in the project in parameter $decor. Each association is checked for existence so we don't add twice. With $replacemode=true we delete first and add as new. 
:   With $testmode=true we do not change anything in the project but only return what would happen *if* the given list was applied. 
:
:   @param $identifierAssociations      - 0..* identifierAssociation elements to add
:   @param $decor                       - 1..1 decor project to add the associations to
:   @param $replacemode                 - 1..1 boolean. true will delete each association first and add as new. false will only add if not present
:   @param $testmode                    - 1..1 boolean. true will give back what would happen if changes would be applied but doesn't change anything. false will apply whatever changes necessary
:   @return a report of changes there were done ($testmode=false) or would be done ($testmode=true)
:           <result>
:               <identifierAssociation conceptId="x" @*>                (contains only attributes as they would be written, which might be less than what was supplied)
:                   <add>true|false</add>                               (contains true if association is/would be added with attributes as indicated)
:               </identifierAssociation>
:           </result>
:   @since 2017-04-05
:)
declare function utillib:addIdentifierAssociations($identifierAssociations as element(identifierAssociation)*, $decorOrtransactionConcept as element(), $replacemode as xs:boolean, $testmode as xs:boolean) as element(result) {
    let $decor                          := $decorOrtransactionConcept/ancestor-or-self::decor
    let $concept                        := $decorOrtransactionConcept[self::concept][ancestor::representingTemplate]
    let $allStoredAssociations          := utillib:getIdentifierAssociationsP($decor)
    
    (: radical solution: delete first, insert later, might be too harsh :)
    return
    <result>
    {
        for $identifierAssociation in $identifierAssociations
        let $s_cid                  := $identifierAssociation/@conceptId[string-length()>0]
        let $s_ceff                 := $identifierAssociation/@conceptFlexibility[string-length()>0]
        let $s_ref                  := $identifierAssociation/@ref[string-length()>0]
        let $rewrittedAssociation   := utillib:prepareIdentifierAssociationForUpdate($identifierAssociation, exists($concept))
        let $currentStoredAssocs    := $allStoredAssociations[@conceptId=$s_cid][@ref=$s_ref]
        
        let $deleteAssoc            :=
            if ($testmode) then () else if ($replacemode) then (update delete $currentStoredAssocs) else ()
        let $storeAssoc             :=
            if ($currentStoredAssocs or empty($rewrittedAssociation)) then (false()) else (
                let $x              :=
                    if ($testmode) then ()
                    else if ($concept) then (
                        update insert $rewrittedAssociation into $concept
                    )
                    else if ($decor/ids/identifierAssociation) then (
                        update insert $rewrittedAssociation preceding $decor/ids/identifierAssociation[1]
                    )
                    else if ($decor/ids/*) then (
                        update insert $rewrittedAssociation following $decor/ids/*[last()]
                    )
                    else (
                        update insert $rewrittedAssociation into $decor/ids
                    )
                return true()
            )
        
        return (
            element {$rewrittedAssociation/name()} {
                $rewrittedAssociation/@*,
                if ($rewrittedAssociation[@refdisplay]) then () else 
                if ($s_ref) then (attribute refdisplay {utillib:getNameForOID($s_ref, (), ())}) else (),
                <add>{$storeAssoc}</add>
            }
        )
    }
    </result>
};

(:~  Return only valid, non empty attributes on an identifier association as defined in the DECOR format.
:
:   @param $identifierAssociation     - 0..1 identifierAssociation elements
:   @param $allowEmpty                - true|false only relevant for transaction concepts when you need to explicitly exclude a dataset association in a transaction
:   @return the pruned identifierAssociation element for the given project in $decor, or nothing input was empty
:   @since 2017-04-05
:)
declare function utillib:prepareIdentifierAssociationForUpdate($identifierAssociation as element()?) as element(identifierAssociation)? {
    utillib:prepareIdentifierAssociationForUpdate($identifierAssociation, false())
};
declare function utillib:prepareIdentifierAssociationForUpdate($identifierAssociation as element()?, $allowEmpty as xs:boolean) as element(identifierAssociation)? {
    if ($identifierAssociation[@ref[not(.='')]] | $identifierAssociation[$allowEmpty]) then (
        <identifierAssociation>
        {
            $identifierAssociation/@conceptId,
            $identifierAssociation/@conceptFlexibility[not(.='')],
            $identifierAssociation/@ref[not(.='')]
            ,
            if ($identifierAssociation/@effectiveDate[. castable as xs:dateTime]) then (
                $identifierAssociation/@effectiveDate
            )
            else (
                attribute effectiveDate {format-dateTime(current-dateTime(),'[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]', (), (), ())}
            )
            ,
            $identifierAssociation/@expirationDate[. castable as xs:dateTime], 
            $identifierAssociation/@officialReleaseDate[. castable as xs:dateTime], 
            $identifierAssociation/@versionLabel[not(.='')]
        }
        </identifierAssociation>
    ) else ()
};

(:~ Prepares a node of type FreeFormMarkupWithLanguage for update in the database. Only returns nodes that have a language attribute and descendant text contents. 
    Examples of FreeFormMarkupWithLanguage include concept/desc, concept/operationalization, dataset/desc, dataset/copyright, scenario/copyright, and transaction/copyright. 
    These last copyright elements are **not** the same as project/copyright, or (governance) group/copyright
    
@param $in array of FreeFormMarkupWithLanguage nodes
returns array of FreeFormMarkupWithLanguage nodes in the same order but without empty ones and with unique language attributes. If multiple exist with the same language, only the first is returned
:)
declare function utillib:prepareFreeFormMarkupWithLanguageForUpdate($in as element()*) as element()* {
    for $node in $in[empty(@inherited)][@language[not(. = '')]][exists(node())][.//text()[not(normalize-space() = '')]]
    let $l  := $node/@language
    group by $l
    return
        element {name($node[1])} {
            $node[1]/@language, 
            if ($node[1][@lastTranslated castable as xs:dateTime][1]) then $node[1]/@lastTranslated else attribute lastTranslated {substring(string(current-dateTime()), 1, 19)},
            $node[1]/@mimeType[not(. = '')],
            if ($node[1][*]) then 
                $node[1]/(node() except *:div[@data-source = 'inherited']) 
            else (
                utillib:parseNode($node[1])/(node() except *:div[@data-source = 'inherited'])
            )
        }
};

(:~ Prepares a node of type TransactionTrigger for update in the database. Only returns nodes that have a language attribute and descendant text contents. 
    A TransactionTrigger is essentially a FreeFormMarkupWithLanguage but has the option for an id attribute. It occurs only transaction/trigger.
    Note that transaction/trigger is **not** the same as scenario/trigger which does not have an id attribute.
    
@param $in array of TransactionTrigger nodes
returns array of TransactionTrigger nodes in the same order but without empty ones and with unique language attributes. If multiple exist with the same language, only the first is returned
:)
declare function utillib:prepareTransactionTriggerForUpdate($in as element(trigger)*) as element(trigger)* {
    for $node in $in[@language[not(. = '')]][exists(node())][.//text()[not(normalize-space() = '')]]
    let $l  := $node/@language
    group by $l
    return
        element {name($node[1])} {
            $node[1]/@language, 
            if ($node[1][@lastTranslated castable as xs:dateTime][1]) then $node[1]/@lastTranslated else attribute lastTranslated {substring(string(current-dateTime()), 1, 19)}, 
            $node[1]/@mimeType[not(. = '')],
            $node[1]/@id[not(. = '')],
            if ($node[1][*]) then $node[1]/node() else utillib:parseNode($node[1])/node()
        }
};

(:~ Prepares a node of type endorsingAuthority for update in the database. Only returns nodes that have a name attribute and descendant text contents. 
    Examples of endorsingAuthority may be found in dataset, scenario, and transaction
    
@param $in endorsingAuthority node
returns endorsingAuthority node if it had a name attribute
:)
declare function utillib:prepareEndorsingAuthorityForUpdate($in as element(endorsingAuthority)?) as element(endorsingAuthority)? {
    for $node in $in[empty(@inherited)][@name[not(. = '')]]
    return
        element {name($node)} {
            $node/@id[not(. = '')],
            $node/@name,
            for $addrLine in $node/addrLine[exists(node())][.//text()[not(normalize-space() = '')]]
            return
                <addrLine>
                {
                    $addrLine/@type[not(. = '')],
                    if ($addrLine[*]) then $addrLine/node() else utillib:parseNode($addrLine)/node()
                }
                </addrLine>
        }
};

(:~ Prepares a node of type publishingAuthority for update in the database. Only returns nodes that have a name attribute and descendant text contents. 
    Examples of publishingAuthority may be found in dataset, scenario, and transaction
    
@param $in publishingAuthority node
returns publishingAuthority node if it had a name attribute
:)
declare function utillib:preparePublishingAuthorityForUpdate($in as element(publishingAuthority)?) as element(publishingAuthority)? {
    for $node in $in[empty(@inherited)][@name[not(. = '')]]
    return
        element {name($node)} {
            $node/@id[not(. = '')],
            $node/@name,
            for $addrLine in $node/addrLine[exists(node())][.//text()[not(normalize-space() = '')]]
            return
                <addrLine>
                {
                    $addrLine/@type[not(. = '')],
                    if ($addrLine[*]) then $addrLine/node() else utillib:parseNode($addrLine)/node()
                }
                </addrLine>
        }
};

(:~ Prepares a node of type valueCodingType for update in the database. Only returns nodes that have a code + codeSystem (OID) attribute and descendant text contents. 
    Examples of valueCoding may be found in questionnaire
    
@param $in node
returns node with trimmed contents
:)
declare function utillib:prepareValueCodingTypeForUpdate($in as element()?) as element()? {
    utillib:prepareValueCodingTypeForUpdate($in, 
        map:merge(
            for $oid in $in/@codeSystem[utillib:isOid(.)][empty(../@codeSystemName)]
            return
                map:entry($oid[1], utillib:getNameForOID($oid[1], $oid[1]/ancestor::decor/project/@defaultLanguage, $oid[1]/ancestor::decor))
        )
    )
};
declare function utillib:prepareValueCodingTypeForUpdate($in as element()?, $oidnamemap as map(*)?) as element()? {
    for $node in $in[matches(@code, '^\S+$')][@codeSystem[utillib:isOid(.)] | @canonicalUri[. castable as xs:anyURI]]
    return
        element {name($node)} {
            (: prevent leading and trailing spaces to satisfy FHIR later on :)
            for $att in $node/(@code | @codeSystem | @codeSystemName | @canonicalUri | @displayName)
            order by name($att)
            return
                if (normalize-space($att) = '') then () else (
                    attribute {name($att)} {replace($att, '(^\s+)|(\s+$)', '')}
                )
            ,
            if ($node[@codeSystem]/normalize-space(@codeSystemName) = '' and not(empty($oidnamemap))) then (
                let $name := map:get($oidnamemap, $node/@codeSystem)
                return
                    if (empty($name)) then () else attribute codeSystemName {$name}
            )
            else (),
            if ($node/normalize-space(@canonicalUri) = '') then (
                let $uri := utillib:getCanonicalUriForOID('CodeSystem', $node/@codeSystem, $node/@codeSystemVersion, $node/ancestor::decor, ())
                return
                    if (empty($uri)) then () else attribute canonicalUri {$uri}
            )
            else ()
        }
};

(:~ Prepares a node of type valueQuantityType for update in the database. Only returns nodes that have at least one attribute with content. 
    Examples of valueQuantity may be found in questionnaire
    
@param $in node
returns node with trimmed contents
:)
declare function utillib:prepareValueQuantityTypeForUpdate($in as element()?) as element()? {
    utillib:prepareValueQuantityTypeForUpdate($in, 
        map:merge(
            for $oid in $in/@codeSystem[utillib:isOid(.)][empty(../@codeSystemName)]
            return
                map:entry($oid[1], utillib:getNameForOID($oid[1], $language, $oid[1]/ancestor::decor))
        )
    )
};
declare function utillib:prepareValueQuantityTypeForUpdate($in as element()?, $oidnamemap as map(*)?) as element()? {
    for $node in $in[@*[not(normalize-space() = '')]]
    return
        element {name($node)} {
            (: prevent leading and trailing spaces to satisfy FHIR later on :)
            for $att in $node/(@code | @codeSystem | @codeSystemName | @canonicalUri | @value | @comparator | @unit)
            order by name($att)
            return
                if (normalize-space($att) = '') then () else (
                    attribute {name($att)} {replace($att, '(^\s+)|(\s+$)', '')}
                )
            ,
            if ($node[@codeSystem]/normalize-space(@codeSystemName) = '' and not(empty($oidnamemap))) then (
                let $name := map:get($oidnamemap, $node/@codeSystem)
                return
                    if (empty($name)) then () else attribute codeSystemName {$name}
            )
            else (),
            if ($node/normalize-space(@canonicalUri) = '') then (
                let $uri := utillib:getCanonicalUriForOID('CodeSystem', $node/@codeSystem, $node/@codeSystemVersion, $node/ancestor::decor, ())
                return
                    if (empty($uri)) then () else attribute canonicalUri {$uri}
            )
            else ()
        }
};

(:~ Prepares a node of type ObjectHistory for update in the database. Only returns nodes that have a language attribute and descendant text contents. 
    Examples of ObjectHistory include template/revisionHistory.
    
@param $in array of ObjectHistory nodes
returns array of ObjectHistory nodes in the same order but without empty ones and ordered by @date
:)
declare function utillib:prepareObjectHistoryForUpdate($in as element()*) as element()* {
    for $node in $in[@date[not(. = '')]][@by[not(. = '')]][desc][desc//text()[not(normalize-space() = '')]]
    order by $node/@date
    return
        element {name($node)} {
            $node/@by,
            $node/@date,
            utillib:prepareFreeFormMarkupWithLanguageForUpdate($node/desc)
        }
};

(:~ Prepares a node of type Example for update in the database. Only returns nodes that have a language attribute and descendant text contents. 
    Examples of Example include template/example, template//element/example, dataset//concept/conceptDomain/example.
    
@param $in array of Example nodes
returns array of Example nodes in the same order but without empty ones.
:)
declare function utillib:prepareExampleForUpdate($in as element()*) as element()* {
    for $node in $in[exists(node())]
    let $parsedExample  := utillib:parseNode($node)
    let $parsedExample  := 
        if ($parsedExample/*:placeholder[namespace-uri() = 'urn:art-decor:example']) then $parsedExample/*:placeholder/node() else $parsedExample/node()
    return
        <example>
        {
            $node/@type[. = ('neutral', 'valid', 'error')],
            $node/@caption[not(. = '')],
            $parsedExample
        }
        </example>
};

(:~ Prepares a node of type property for update in the database. Only returns nodes that have a name attribute and descendant text contents. 
    Examples of property include dataset/property, concept/property, and transaction/representingTemplate/concept/property. This property 
    element is **not** the same as concept/valueDomain/property, template/classification/property or template//element/property
    
@param $in array of property nodes
returns array of property nodes in the same order but without empty ones
:)
declare function utillib:prepareConceptPropertyForUpdate($in as element(property)*) as element(property)* {
    for $node in $in[@name[not(. = '')]][exists(node())][.//text()[not(normalize-space() = '')]]
    return
        element {name($node)} {
            $node/@name,
            $node/@datatype[not(. = '')],
            if ($node[*]) then $node/node() else utillib:parseNode($node)/node()
        }
};

(:~ Prepares a node of type relationship as found on dataset and concepts for update in the database. Only returns nodes that have a type 
    and a ref attribute. 
    This element is **not** the same as template/relationship
    
@param $in array of relationship nodes
returns array of relationship nodes in the same order but without empty ones
:)
declare function utillib:prepareDatasetRelationshipForUpdate($in as element(relationship)*) as element(relationship)* {
    for $node in $in[@type[not(. = '')]][@ref[not(. = '')]]
    let $id := $node/@ref
    let $fl := ($node/@flexibility[. castable as xs:dateTime], 'dynamic')[1]
    group by $id, $fl
    return
        element {name($node[1])} {
            $node[1]/@type,
            $node[1]/@ref,
            $node[1]/@flexibility[. castable as xs:dateTime or . = 'dynamic']
        }
};

(: Get the minimumMultiplicity from conditions, or zero if none :)
declare function utillib:getMinimumMultiplicity($concept as element()) as xs:string {
    let $minimumMultiplicity := 
        if ($concept[enableWhen] | $concept[@conformance='NP'] | $concept[@prohibited='true'] | $concept[@isOptional='true'] | $concept[@minimumMultiplicity = '']) then
            '0'
        else
        if ($concept/@minimumMultiplicity) then 
            $concept/@minimumMultiplicity
        else 
        if ($concept/condition/@minimumMultiplicity) then 
            if ($concept/condition[@minimumMultiplicity = ''] | $concept/condition[@conformance = 'NP']) then '0' else string(min($concept/condition/@minimumMultiplicity))
        else
        if ($concept[self::attribute]) then 
            '1'
        else ('0')
    return $minimumMultiplicity
};

(:~ Get the maximumMultiplicity from conditions, or * if none :)
declare function utillib:getMaximumMultiplicity($concept as element()) as xs:string {
    let $maximumMultiplicity := 
        if ($concept[@conformance = 'NP'] | $concept[@prohibited = 'true']) then
            '0'
        else
        if ($concept[@maximumMultiplicity = '']) then
            '*'
        else
        if ($concept/@maximumMultiplicity) then 
            $concept/@maximumMultiplicity
        else 
        if ($concept/condition/@maximumMultiplicity) then 
            if ($concept/condition[@maximumMultiplicity = ''] | $concept/condition[@maximumMultiplicity = '*']) then '*' else string(max($concept/condition/@maximumMultiplicity))
        else
        if ($concept[self::attribute]) then 
            '1'
        else ('*')
    return $maximumMultiplicity
};

(:~ serialize textWithMarkup for display in xforms:)
declare function utillib:serializeNode($textWithMarkup as element()) as element() {
    element {QName(namespace-uri($textWithMarkup), name($textWithMarkup))} {
        $textWithMarkup/@*,
        if ($textWithMarkup[* | processing-instruction() | comment()]) then 
            fn:serialize($textWithMarkup/node(),
                <output:serialization-parameters xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization">
                    <output:method>xml</output:method>
                    <output:encoding>UTF-8</output:encoding>
                </output:serialization-parameters>
            )
        else $textWithMarkup/node()
    }
};

(:~ parse serialized content to html for storage :)
declare function utillib:parseNode($textWithMarkup as element()?) as element()? {
    utillib:parseNodeByType($textWithMarkup, 'html')
};

(:~ When you parse XML as HTML sometimes funky things happen. Example: when you have a <code/> element in an
    observation, the util:parse-html will assume that the following-siblings of code should have been inside
    code and actually move stuff into it, rendering the example invalid. Hence, when we parse, we only parse
    into html when explicitly instructed to do so, otherwise we parse into XML
:)
declare function utillib:parseNodeByType($textWithMarkup as element()?, $type as xs:string) as element()? {
let $parsed := 
    if ($textWithMarkup) then ( 
        if ($textWithMarkup[*]) then (
            (:return as-is. It is XML already:)
            $textWithMarkup
        )
        else if ($type=('html','xhtml')) then (
            element {name($textWithMarkup)} {
                $textWithMarkup/@*,
                let $node := util:parse-html($textWithMarkup)
                
                return
                    $node/*/(BODY | body | xhtml:body)/node()
            }
        )
        else ( 
            element {name($textWithMarkup)} {
                $textWithMarkup/@*,
                (:need to wrap in to level element in case there isn't one, e.g. assert with just text:)
                fn:parse-xml(string-join(('&lt;x&gt;',$textWithMarkup,'&lt;/x&gt;'),''))/x/node()
            }
        )
    )
    else ()
return $parsed
};

declare function utillib:serializeNodes($textWithMarkup as element()*) as element()* {
    utillib:serializeNodes($textWithMarkup, ())
};
declare function utillib:serializeNodes($textWithMarkup as element()*, $project-languages as item()*) as element()* {
    (: process whatever is in the set :)
    for $node in $textWithMarkup
    return utillib:serializeNode($node)
    ,
    (: add any missing languages, compared to project-languages and art-languages on this server :)
    if ($textWithMarkup) then (
        let $language-set   := 
            for $lang in distinct-values(($project-languages, utillib:getArtLanguages()))
            return if ($textWithMarkup[@language = $lang]) then () else ($lang)
        
        for $lang in $language-set
        let $matchNode      := $textWithMarkup[starts-with(@language, substring($lang,1,2))]
        let $matchNode      := if ($matchNode) then $matchNode else $textWithMarkup[@language='en-US']
        let $matchNode      := if ($matchNode) then $matchNode else $textWithMarkup[1]
        return
            utillib:serializeNode(
                element {name($textWithMarkup[1])}
                {
                    attribute language {$lang}
                    ,
                    $matchNode[1]/node()
                }
            )
    )
    else ()
};
declare function utillib:getLanguageNodes($textNodes as element()*) as element()* {
    utillib:getLanguageNodes($textWithMarkup, ())
};
declare function utillib:getLanguageNodes($textNodes as element()*, $project-languages as item()*) as element()* {
    (: process whatever is in the set :)
    $textNodes
    ,
    (: add any missing languages, compared to project-languages and art-languages on this server :)
    if ($textNodes) then (
        let $language-set   := 
            for $lang in distinct-values(($project-languages, utillib:getArtLanguages()))
            return if ($textNodes[@language = $lang]) then () else ($lang)
        
        for $lang in $language-set
        let $matchNode      := $textNodes[starts-with(@language, substring($lang,1,2))]
        let $matchNode      := if ($matchNode) then $matchNode else $textNodes[@language='en-US']
        let $matchNode      := if ($matchNode) then $matchNode else $textNodes[1]
        return
            element {name($textNodes[1])}
            {
                attribute language {$lang}
                ,
                $matchNode[1]/node()
            }
    )
    else ()
};

(:  Return language supported in ART by returning all @xml:lang attributes in the art/resources/form-resources.xml
:   Example: ('en-US','nl-NL','de-DE')
:
:   Note: using utillib:getFormResourcesLocal('art') would have made sense but this kills xforms (http 400) without clear reason
:         for now copy logic from there. Weird. Fixme.
:)
declare function utillib:getArtLanguages() as xs:string+ {
let $packageRoot            := 'art'
let $localResources         := collection($setlib:strArtData)//artXformResources[@packageRoot = $packageRoot]
return 
    if ($localResources) then (
        $localResources/resources/@xml:lang
    )
    else (
        let $packageResources   := doc(concat($setlib:root,$packageRoot,'/resources/form-resources.xml'))/artXformResources
        return (
            $packageResources/resources/@xml:lang
        )
    )
};

(:~ Returns all namespaces in scope of a DECOR project in Schematron convention. 
:   Either @param $projectId or @param $projectPrefix is required. $projectId takes precedence over $projectPrefix.
:   <ns uri="xx" prefix="yy"/>
:   
:   There are two default elements that will always be returned:
:   <ns uri="urn:hl7-org:v3" prefix="hl7"/>
:   <ns uri="urn:hl7-org:v3" prefix="cda"/>
:   
:   @param $projectId       - optional. Shall match decor/project/@id
:   @param $projectPrefix   - optional. Shall match decor/project/@prefix
:   @return zero or more ns elements
:   @since 2015-03-19
:)
declare function utillib:getDecorNamespaces($projectId as xs:string?, $projectPrefix as xs:string?) as element(ns)* {
    let $decor          :=
        if ($projectId) then 
            $setlib:colDecorData//project[@id=$projectId]/ancestor::decor
        else (
            $setlib:colDecorData//project[@prefix=$projectPrefix]/ancestor::decor
        )
    
    return
        utillib:getDecorNamespaces($decor[1])
};
declare function utillib:getDecorNamespaces($decor as element(decor)?) as element(ns)* {
    let $reservedPrefixes   := ('xml','xsi','hl7','cda')

    return
    if ($decor) then (
        
        let $ns-default     := 
            if ($decor/project/defaultElementNamespace[string-length(@ns)>0]) then
                replace($decor/project/defaultElementNamespace/@ns,':.*','')
            else ('hl7')
        
        let $ns-set         :=
            if ($decor) then (
                for $ns-prefix at $i in in-scope-prefixes($decor)[not(.=$reservedPrefixes)]
                let $ns-uri := namespace-uri-for-prefix($ns-prefix, $decor)
                return
                    <ns uri="{$ns-uri}" prefix="{$ns-prefix}" default="{$ns-prefix=$ns-default}"/>
            ) else ()
        let $ns-xml         := <ns uri="http://www.w3.org/XML/1998/namespace" prefix="xml" default="{$ns-default='xml'}" readonly="true"/>
        let $ns-hl7         := <ns uri="urn:hl7-org:v3" prefix="hl7" default="{$ns-default='hl7'}" readonly="true"/>
        let $ns-cda         := <ns uri="urn:hl7-org:v3" prefix="cda" default="{$ns-default='cda'}" readonly="true"/>
        let $ns-xsi         := 
            if (in-scope-prefixes($decor)[. = 'xsi']) then
                <ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi" default="{$ns-default='xsi'}" readonly="true"/>
             else ()
        
        return
            ($ns-set | $ns-xml | $ns-hl7 | $ns-cda | $ns-xsi)
        
    ) else ()
    
};

(:~ Updates the current DECOR namespace declarations with the given set. The given set is assumed to be the 
:   full set you want to have on the project. The default namespace declarations for hl7 and cda are never updated or deleted
:   The set should have zero or more elements that look like:
:   <ns uri="xx" prefix="yy"/>
:   
:   @param $project         - required. Shall be element(decor), match decor/project/@id or match decor/project/@prefix
:   @param $namespace-nodes - optional. Zero nodes will delete every declaration except xml, hl7 and cda
:   @return nothing
:   @since 2015-03-19
:)
declare function utillib:setDecorNamespaces($project as item(), $namespace-nodes as element(ns)*) {
    let $reservedPrefixes   := ('xml','xsi','hl7','cda')
    let $currentDecor       := 
        typeswitch ($project)
        case element() return $project/ancestor-or-self::decor[1]
        case document-node() return $project/decor
        default return (
            if (utillib:isOid($project)) then utillib:getDecorById($project) else utillib:getDecorByPrefix($project)
        )
    let $ns-set             := 
        for $ns in $namespace-nodes[string-length(@uri) gt 0][string-length(@prefix) gt 0][not(@prefix = 'xml')]
        return
            (: protected prefixes :)
            if ($ns[not(@uri = 'http://www.w3.org/XML/1998/namespace')][@prefix = 'xml']) then
                error(xs:QName('utillib:setDecorNamespaces'), 'Protected prefix ''' || $ns/@prefix || ''' SHALL be http://www.w3.org/XML/1998/namespace. Found: "' || $ns/@url || '"')
            else
            if ($ns[not(@uri = 'http://www.w3.org/2001/XMLSchema-instance')][@prefix = 'xsi']) then
                error(xs:QName('utillib:setDecorNamespaces'), 'Protected prefix ''' || $ns/@prefix || ''' SHALL be http://www.w3.org/2001/XMLSchema-instance. Found: "' || $ns/@url || '"')
            else
            if ($ns[not(@uri = 'urn:hl7-org:v3')][@prefix = ('hl7', 'cda')]) then
                error(xs:QName('utillib:setDecorNamespaces'), 'Protected prefix ''' || $ns/@prefix || ''' SHALL be urn:hl7-org:v3. Found: "' || $ns/@url || '"')
            else
            if ($ns[not(@uri = 'urn:hl7-org:sdtc')][@prefix = 'sdtc']) then
                error(xs:QName('utillib:setDecorNamespaces'), 'Protected prefix ''' || $ns/@prefix || ''' SHALL be urn:hl7-org:sdtc. Found: "' || $ns/@url || '"')
            else (
                $ns
            )
    
    let $delete             := update delete $currentDecor/@*[matches(local-name(),'^dummy-\d*')]
    
    let $add                :=
        for $ns at $i in $ns-set
        let $ns-uri         := $ns/@uri
        let $ns-prefix      := $ns/@prefix
        order by lower-case($ns/@prefix)
        return
            update insert attribute {QName($ns-uri,concat($ns-prefix,':dummy-',$i))} {$ns-uri} into $currentDecor
    
    (: workaround for eXist-db not recognizing namespaces on the root unless you re-save the whole thing :)
    let $coll               := util:collection-name($currentDecor)
    let $res                := util:document-name($currentDecor)
    let $doc                := doc(concat($coll, '/', $res))
    let $store              := xmldb:store($coll, $res, $doc)
    
    return ()
};

(:~
:   The UI has dealt with whether or not this status transition is allowable, but will check here too.
:   We get these properties to set:
:   <statusChange ref="object-id" effectiveDate="object-effectiveDate?" statusCode="xs:string" versionLabel="xs:sting?" expirationDate="xs:dateTime?" recurse="true|false"/>
:
:   Step 1: find object
:           if not: <response>NOT FOUND</response>
:   Step 2: check if user is author in object project
:           if not: <response>NO PERMISSION</response>
:   Step 3: check if object has lock set and if @recurse='true' check all underlying objects
:           recursion if only applicable to dataset|concept|scenario|transaction
:           if lock: <response>{first-lock}</response>
:   Step 4: apply status/versionLabel/expirationDate
:           statusCode is added/updated (assumed to be valued)
:           versionLabel/expirationDate is added/updated if valued, or deleted otherwise
:           recursion is applied if @recurse='true', but only if the child object 
:           (concept|transaction) matches the statusCode of the main object
:
:   So if the main object was draft, then recursion stops when the first @statusCode!=draft 
:   is encounted. This is not considered an error so the user needs to check before/after 
:   calling this function
:
:   This function is called from:
:       decor-codeSystems       codeSystem          ItemStatusLifeCycle
:       decor-datasets          dataset             ItemStatusLifeCycle
:                               concept             ItemStatusLifeCycle
:       decor-scenarios         scenario            ItemStatusLifeCycle
:                               transaction         ItemStatusLifeCycle
:       decor-valuesets         valueSet            ItemStatusLifeCycle
:
:       decor-templates         templates           TemplateStatusLifeCycle
:       decor-template-mapping  templates           TemplateStatusLifeCycle
:
:   This function is currently not called for updating status of Releases and Issues
:
:)
declare function utillib:applyStatusProperties($authmap as map(*), $object as element(), $originalStatus as xs:string?, $newStatus as xs:string?, $newVersionLabel as xs:string?, $newExpirationDate as xs:string?, $newOfficialReleaseDate as xs:string?, $recurse as xs:boolean, $list as xs:boolean, $projectPrefix as xs:string) as element()* {
    utillib:applyStatusProperties($authmap, $object, $originalStatus, $newStatus, $newVersionLabel, $newExpirationDate, $newOfficialReleaseDate, $recurse, $list, $projectPrefix, ())
};

declare function utillib:applyStatusProperties($authmap as map(*), $object as element(), $originalStatus as xs:string?, $newStatus as xs:string?, $newVersionLabel as xs:string?, $newExpirationDate as xs:string?, $newOfficialReleaseDate as xs:string?, $recurse as xs:boolean, $list as xs:boolean, $projectPrefix as xs:string, $antiresursionlist as xs:string*) as element()* {
    let $newStatus              := $newStatus[string-length(normalize-space()) gt 0]
    let $activeObject           := if ($object[self::codedConcept]) then $object/ancestor::codeSystem else $object
    
    let $statuschangeallowed    := if (empty($newStatus)) then true() else utillib:isStatusChangeAllowable($object, $newStatus)
    let $canLock                := decorlib:canLock($authmap, $activeObject)
    let $sourceType             := decorlib:getObjectDecorType($activeObject)
    
    let $unfinishedTransactions := $object/descendant-or-self::transaction[representingTemplate[descendant::*[@minimumMultiplicity = ''] | descendant::*[@maximumMultiplicity = '']]]
    let $trstatuschangeallowed  := not(exists($unfinishedTransactions) and $newStatus = ('final', 'cancelled', 'rejected', 'deprecated'))
    
    let $newExpirationDate      := 
        if ($newExpirationDate castable as xs:dateTime) then $newExpirationDate else 
        if ($newExpirationDate castable as xs:date) then concat($newExpirationDate,'T00:00:00')
        else ($newExpirationDate)
    let $newOfficialReleaseDate := 
        if ($newOfficialReleaseDate castable as xs:dateTime) then $newOfficialReleaseDate else 
        if ($newOfficialReleaseDate castable as xs:date) then concat($newOfficialReleaseDate,'T00:00:00')
        else ($newOfficialReleaseDate)
    
    let $update                 :=
        if ($list) then () else 
        if ($statuschangeallowed and $trstatuschangeallowed and $canLock) then (
            if (empty($sourceType)) then () else histlib:AddHistory($authmap?name, $sourceType, $projectPrefix, if ($object[@statusCode=('active', 'final')]) then 'patch' else 'version', $object)
            ,
            if (empty($newStatus)) then () else
            if ($object/@statusCode) then
                update value $object/@statusCode with $newStatus
            else (
                update insert attribute statusCode {$newStatus} into $object
            ),
            if (empty($newVersionLabel)) then () else
            if (string-length($newVersionLabel) gt 0) then
                if ($object/@versionLabel) then
                    update value $object/@versionLabel with $newVersionLabel
                else (
                    update insert attribute versionLabel {$newVersionLabel} into $object
                )
            else (
                update delete $object/@versionLabel
            ),
            if (empty($newExpirationDate)) then () else
            if (string-length($newExpirationDate) gt 0) then
                if ($object/@expirationDate) then
                    update value $object/@expirationDate with $newExpirationDate
                else (
                    update insert attribute expirationDate {$newExpirationDate} into $object
                )
            else (
                update delete $object/@expirationDate
            ),
            if (empty($newOfficialReleaseDate)) then () else
            if (string-length($newOfficialReleaseDate) gt 0) then
                if ($object/@officialReleaseDate) then
                    update value $object/@officialReleaseDate with $newOfficialReleaseDate
                else (
                    update insert attribute officialReleaseDate {$newOfficialReleaseDate} into $object
                )
            else (
                update delete $object/@officialReleaseDate
            ),
            if ($object/self::transaction) then (
                if ($object[@effectiveDate]) then () else (
                    update insert attribute effectiveDate {($object/ancestor::*/@effectiveDate)[1]} into $object
                )
            ) else ()
        ) else ()
    
    return (
        element {if ($statuschangeallowed and $trstatuschangeallowed and $canLock) then 'success' else 'error'} {
            attribute itemcode {$sourceType},
            attribute itemname {local-name($object)},
            $object/(@* except (@itemcode | @itemname)),
            if ($statuschangeallowed) then () else (
                'Status transition not allowed from ' || $object/@statusCode || ' to ' || $newStatus || '.'
            ),
            if ($trstatuschangeallowed) then () else 
                'Status transition not allowed from ' || $object/@statusCode || ' to ' || $newStatus || ', because ' || 
                    count($object/descendant::*[@minimumMultiplicity = ''] | $object/descendant::*[@maximumMultiplicity = '']) || 
                ' concepts exist in one or more underlying transactions with undefined minimumMultiplicities and/or maximumMultiplicities. Please check transactions: ' || 
                string-join(
                    for $transaction in $unfinishedTransactions
                    return
                        concat('"', $transaction/name[1], '"')
                , ' '),
            if ($canLock) then () else 'Unable to get a lock. This is likely because it is locked by someone else.'
        }
        ,
        (:do recursion, but stop as soon we get to an artifact with a different status than our main artifact to respect the status machine:)
        if ($recurse) then (
            if ($object[name()=('dataset','concept')]) then
                for $child in $object/concept[utillib:isStatusChangeAllowable(., $newStatus)]
                return utillib:applyStatusProperties($authmap, $child, $originalStatus, $newStatus, $newVersionLabel, $newExpirationDate, $newOfficialReleaseDate, $recurse, $list, $projectPrefix, ())
            else 
            if ($object[name()=('scenario','transaction')]) then
                for $child in $object/transaction[utillib:isStatusChangeAllowable(., $newStatus)]
                return utillib:applyStatusProperties($authmap, $child, $originalStatus, $newStatus, $newVersionLabel, $newExpirationDate, $newOfficialReleaseDate, $recurse, $list, $projectPrefix, ())
            else 
            if ($object[name()=('codedConcept')]) then (
                let $children   := $object/../codedConcept[parent[@code = $object/@code]][utillib:isStatusChangeAllowable(., $newStatus)]
                
                for $child in $children
                return 
                    if ($child[@code = $antiresursionlist]) then () else (
                        utillib:applyStatusProperties($authmap, $child, $originalStatus, $newStatus, $newVersionLabel, $newExpirationDate, $newOfficialReleaseDate, $recurse, $list, $projectPrefix, distinct-values(($object/@code, $children/@code, $antiresursionlist)))
                    )
            )
            else ()
        ) else ()
    )
};

(: **** ESSENTIAL SERVER FUNCTIONS TO AVOID CIRCULAR IMPORT **** :)

(:~ Return the installed FHIR server versions. Example endpoints: 1.0, 3.0, 4.0
Any collection under $setlib:strFhir that holds an expath-pkg.xml as a sign of an installed package is returned.

@return sequence of strings
@since 2015-03-27
:)
declare function utillib:getInstalledFhirServices() as xs:string* {
    if (xmldb:collection-available($setlib:strFhir)) then (
        for $child-collection in xmldb:get-child-collections($setlib:strFhir)
        return
            if (xmldb:get-child-resources(concat($setlib:strFhir,'/',$child-collection))[. = 'expath-pkg.xml']) then
                $child-collection
            else ()
    ) else ()
};

(:~ Return the configured server-url http or https for ART-DECOR services or empty string. Example: http://art-decor.org/decor/services/

@return xs:anyURI('http://.../services/')
@since 2014-03-27
:)
declare function utillib:getServerURLServices() as xs:string? {
    doc($setlib:strServerInfo)/server-info/url-art-decor-services/string()
};

(:~ Return the configured server-url http or https for ART-DECOR or empty string. Example: https://art-decor.org/art-decor/

@return xs:anyURI('http://.../art-decor/')
@since 2014-03-27
:)
declare function utillib:getServerURLArt() as xs:string? {
    doc($setlib:strServerInfo)/server-info/url-art-decor-deeplinkprefix/string()
};

(:~ Return the configured server-url http or https for ART-DECOR 3 Vue or empty string. Example: https://art-decor.org/ad/

@return xs:anyURI('http://.../ad/')
@since 2014-03-27
:)
declare function utillib:getServerURLArt3() as xs:string? {
    (doc($setlib:strServerInfo)/server-info/url-art-decor3-deeplinkprefix, replace(utillib:getServerURLArt(), '/art-decor.*$', '/ad/#/'))[not(. = '')][1]
};

(:~ Return the configured server-url http or https for ART-DECOR FHIR services or empty string. Example: https://art-decor.org/fhir/

@return xs:anyURI('http://.../services/')
@since 2015-02-27
:)
declare function utillib:getServerURLFhirServices() as xs:string? {
    doc($setlib:strServerInfo)/server-info/url-fhir-services
};

(:~ Return the configured server-url http or https for ART-DECOR FHIR services or empty string.
    Example: http://art-decor.org/fhir/
    
    @return 'http://.../services/' as xs:string
    @since 2015-02-27
:)
declare function utillib:getServerURLFhirCanonicalBase() as xs:string? {
    let $d  := doc($setlib:strServerInfo)/server-info
    
    return
        if ($d/url-fhir-canonical-base) then $d/url-fhir-canonical-base else $d/url-fhir-services
};

(: **** / ESSENTIAL SERVER FUNCTIONS TO AVOID CIRCULAR IMPORT **** :)

(: **** DECOR.xsd functions **** :)

(: get Decor Types from DECOR.xsd:)
declare function utillib:getLabelsAndHints($type as element()) as element()* {
let $art-languages  := utillib:getArtLanguages()
let $labels         := $type/xs:annotation/xs:appinfo/xforms:label
let $hints          := $type/xs:annotation/xs:appinfo/xforms:hint
let $hints          := if ($hints) then $hints else $type/xs:annotation/xs:documentation
return (
    for $label in $labels
    return
    <label language="{$label/@xml:lang}">{$label/text()}</label>
    ,
    (:add any missing language as copy from en-US:)
    if ($labels) then (
        for $lang in $art-languages[not(. = $labels/@xml:lang)]
        return
        <label language="{$lang}">{$labels[@xml:lang = 'en-US']/text()}</label>
    ) else ()
    ,
    for $hint in $hints
    return
    <hint language="{$hint/@xml:lang}">{$hint/text()}</hint>
    ,
    (:add any missing language as copy from en-US:)
    if ($hints) then (
        for $lang in $art-languages[not(. = $hints/@xml:lang)]
        return
        <hint language="{$lang}">{$hints[@xml:lang = 'en-US']/text()}</hint>
    ) else ()
)
};

(:~ Called from all DECOR oriented forms :)
declare function utillib:getDecorTypes() as element()* {
    utillib:getDecorTypes(false())
};

(:~ Called from DECOR-core post-install.xql with parameter true() to recreate the normalized xml file from a potentially updated DECOR.xsd file :)
declare function utillib:getDecorTypes($recreate as xs:boolean) as element()* {
    let $decorTypes     := 
        if ($recreate) then () else if (doc-available($setlib:strDecorTypes)) then doc($setlib:strDecorTypes)/list[@artifact = 'TYPES'] else ()
    
    return
    if ($recreate or empty($decorTypes)) then (
        let $types      :=
            (:<xs:simpleType name="DecorAndOtherObjectFormats">
                <xs:union memberTypes="DecorObjectFormat FhirObjectFormat NonEmptyString"/>
            </xs:simpleType>:)
            for $simpleType in $setlib:docDecorSchema/xs:schema/xs:simpleType[xs:union] | $setlib:docDecorBasicSchema/xs:schema/xs:simpleType[xs:union]
            let $memberTypes    := $setlib:docDecorSchema/xs:schema/xs:simpleType[@name = $simpleType/xs:union/tokenize(@memberTypes, '\s')] |
                                   $setlib:docDecorBasicSchema/xs:schema/xs:simpleType[@name = $simpleType/xs:union/tokenize(@memberTypes, '\s')]
            return
                <xs:simpleType>
                {
                    $simpleType/@name,
                    $simpleType/xs:annotation
                }
                {
                    <xs:restriction base="xs:string">
                    {
                        $memberTypes//xs:enumeration
                    }
                    </xs:restriction>
                }
                </xs:simpleType>
            

        let $types      := ($types|
                            $setlib:docDecorSchema/xs:schema/xs:element|
                            $setlib:docDecorSchema/xs:schema/xs:complexType|
                            $setlib:docDecorSchema/xs:schema/xs:simpleType[.//xs:enumeration]|
                            $setlib:docDecorBasicSchema/xs:schema/xs:element|
                            $setlib:docDecorBasicSchema/xs:schema/xs:complexType|
                            $setlib:docDecorBasicSchema/xs:schema/xs:simpleType[.//xs:enumeration])
        let $decorTypes := 
            <list artifact="TYPES" current="{count($types)}" total="{count($types)}" all="{count($types)}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
            {
                for $type in $types
                order by $type/@name
                return
                    switch ($type/@name)
                    case 'InstalledFhirEndpoints' return (
                        utillib:addJsonArrayToElements(
                            element {$type/@name} {
                                utillib:getLabelsAndHints($type)
                                ,
                                (: eliminate need to calling serverapi, avoiding need for circular reference :)
                                for $endpoint in utillib:getInstalledFhirServices()
                                let $label      :=
                                    switch ($endpoint)
                                    case '1.0'
                                    case 'dstu2' return 'DSTU2'
                                    case '3.0'
                                    case 'stu3' return 'STU3'
                                    default return (
                                        if (matches($endpoint, '^\d+\.\d+(\.\d+)?$')) then
                                            concat('R', string-join(tokenize($endpoint, '\.')[position() le 2], '.'))
                                        else (
                                            $endpoint
                                        )
                                    )
                                order by $endpoint
                                return 
                                    <enumeration name="{$endpoint}">
                                    {
                                        utillib:getLabelsAndHints(
                                            <xs:enumeration value="{$endpoint}">
                                                <xs:annotation>
                                                    <xs:appinfo>
                                                        <xforms:label xml:lang="en-US">{$label}</xforms:label>
                                                    </xs:appinfo>
                                                </xs:annotation>
                                            </xs:enumeration>
                                        )
                                    }
                                    </enumeration>
                            }
                        )
                    )
                    case 'InstalledMenuTemplates' return (
                        utillib:addJsonArrayToElements(
                            element {$type/@name} {
                                utillib:getLabelsAndHints($type)
                                ,
                                (: eliminate need to calling serverapi, avoiding need for circular reference :)
                                (:for $xls in serverapi:getServerMenuTemplates():)
                                for $xls in ('art-menu-template.xml', 'artdecororg-menu-template.xml', 'lab-menu-template.xml', 'terminology-menu-template.xml', 'xis-menu-template.xml')
                                order by $xls
                                return 
                                    <enumeration name="{$xls}">
                                    {
                                        utillib:getLabelsAndHints(
                                            <xs:enumeration value="{$xls}">
                                                <xs:annotation>
                                                    <xs:appinfo>
                                                        <xforms:label xml:lang="en-US">{$xls}</xforms:label>
                                                    </xs:appinfo>
                                                </xs:annotation>
                                            </xs:enumeration>
                                        )
                                    }
                                    </enumeration>
                            }
                        )
                    )
                    default return (
                        utillib:addJsonArrayToElements(
                            element {$type/@name} {
                                utillib:getLabelsAndHints($type),
                                for $element in $type//xs:element
                                return
                                <element>
                                {
                                    $element/@name | $element/@ref,
                                    utillib:getLabelsAndHints($element)
                                }
                                </element>
                                ,
                                for $attribute in $type//xs:attribute
                                return
                                <attribute>
                                {
                                    $attribute/@name | $attribute/@ref,
                                    utillib:getLabelsAndHints($attribute)
                                }
                                </attribute>
                                ,
                                for $enumeration in $type//xs:enumeration
                                return
                                <enumeration>
                                {
                                    $enumeration/@value,
                                    utillib:getLabelsAndHints($enumeration)
                                }
                                </enumeration>
                            }
                        )
                )
            }
            </list>
        
        (:don't store if we cannot write:)
        let $f          := tokenize($setlib:strDecorTypes,'/')[last()]
        let $upd        := 
            if (doc-available($setlib:strDecorTypes)) then
                if (sm:has-access(xs:anyURI(concat($setlib:strArtData, '/', $f)),'w')) then (
                    sm:chgrp(xs:anyURI(xmldb:store($setlib:strArtData, $f, $decorTypes)), 'decor')
                )
                else ()
            else if (sm:has-access(xs:anyURI($setlib:strArtData),'w')) then
                sm:chgrp(xs:anyURI(xmldb:store($setlib:strArtData, $f, $decorTypes)), 'decor')
            else ()
        
        return $decorTypes
    ) else (
        $decorTypes
    )
};

(:~ Parse string into xs:dateTime. String may be a (partial) ISO 8601. Year is required. Missing month will be 01 for January. Missing day will 
    be 01. Missing hours, minutes or seconds will be 00. String may also be a relative statement T((([-+]nn(.nn)[YMDhms])({hh:mm:ss})) where 
    T is current-dateTime(). You may deduct nn(.nn) from or add nn(.nn) to T where nn(.nn) is a number and [YMDhms] determines if those are 
    years, months, days, hours, minutes, or seconds. Finally you may give the result a fixed time string.
    
    @param $variablevaguedatetime - optional. String to parse into an xs:dateTime
    @return a valid xs:dateTime or nothing

:)
declare function utillib:parseAsDateTime($variablevaguedatetime as xs:string?) as xs:dateTime? {
    if ($variablevaguedatetime castable as xs:dateTime) then
        xs:dateTime($variablevaguedatetime)
    else
    if ($variablevaguedatetime castable as xs:date) then
        xs:dateTime(concat($variablevaguedatetime, 'T00:00:00'))
    else
    (: variable logic :)
    if (starts-with($variablevaguedatetime, 'T')) then (
        let $nowdate            := current-date()
        let $nowdatetime        := current-dateTime()
        let $plusminus          := substring($variablevaguedatetime, 2, 1)
        let $plusminus          := if ($plusminus = '+' or $plusminus = '-') then $plusminus else ()
        let $plusminusstring    := 
            if (empty($plusminus)) then () else 
            if (matches($variablevaguedatetime, '^T.(\d+(\.\d+)?[YMDhms]).*')) then replace($variablevaguedatetime, '^T.(\d+(\.\d+)?[YMDhms]).*', '$1') 
            else ()
        let $plusminusnumber    := replace($plusminusstring, '.$', '')[string-length() gt 0]
        let $plusminustype      := replace($plusminusstring, '.+(.)$', '$1')[string-length() gt 0]
        let $fixedtime          := 
            if (matches($variablevaguedatetime, 'T.*\{\d{2}:\d{2}:\d{2}\}.*')) then
                substring-before(substring-after($variablevaguedatetime, '{'), '}')
            else ()
        
        return
        if (empty($plusminus) and empty($fixedtime)) then 
            $nowdatetime
        else
        if (empty($plusminus)) then
            if ($fixedtime castable as xs:time) then xs:dateTime(concat(string($nowdate), 'T', $fixedtime)) else ()
        else (
            let $duration       := 
                switch  ($plusminustype)
                case 'Y' return xs:yearMonthDuration(concat('P', $plusminusnumber, $plusminustype))
                case 'M' return xs:yearMonthDuration(concat('P', $plusminusnumber, $plusminustype))
                case 'D' return xs:dayTimeDuration(concat('P', $plusminusnumber, $plusminustype))
                case 'h' return xs:dayTimeDuration(concat('PT', $plusminusnumber, upper-case($plusminustype)))
                case 'm' return xs:dayTimeDuration(concat('PT', $plusminusnumber, upper-case($plusminustype)))
                case 's' return xs:dayTimeDuration(concat('PT', $plusminusnumber, upper-case($plusminustype)))
                default return ()
            let $calcdatetime   := 
                switch  ($plusminus)
                case '-' return $nowdatetime - $duration
                case '+' return $nowdatetime + $duration
                default return $nowdatetime
            
            return
                if (empty($fixedtime)) then $calcdatetime else 
                if ($fixedtime castable as xs:time) then xs:dateTime(concat(substring-before($nowdatetime, 'T'), 'T', $fixedtime))
                else ()
            )
    )
    (: partial dateTime logic :)
    else (
        let $year       := substring($variablevaguedatetime, 1, 4)
        let $month      := substring($variablevaguedatetime, 5, 2)
        let $month      := if ($month castable as xs:integer) then $month else '01'
        let $day        := substring($variablevaguedatetime, 7, 2)
        let $day        := if ($day castable as xs:integer) then $day else '01'
        let $hour       := substring($variablevaguedatetime, 10, 2)
        let $hour       := if ($hour castable as xs:integer) then $hour else '00'
        let $minute     := substring($variablevaguedatetime, 12, 2)
        let $minute     := if ($minute castable as xs:integer) then $minute else '00'
        let $second     := substring($variablevaguedatetime, 14, 2)
        let $second     := if ($second castable as xs:integer) then $second else '00'
        
        let $dateTime   := concat($year, '-', $month, '-', $day, 'T', $hour, ':', $minute, ':', $second)
        
        return
            if ($dateTime castable as xs:dateTime) then xs:dateTime($dateTime) else ()
    )
};

(: **** HTTP **** :)
(:~ Last-Modified: <day-name>, <day> <month> <year> <hour>:<minute>:<second> GMT 
    Last-Modified: Wed, 21 Oct 2015 07:28:00 GMT:)
declare function utillib:getHttpLastModified($dateTime as xs:string?) as map()? {
    let $dateTime := utillib:parseAsDateTime($dateTime) 
    return
    if (empty($dateTime)) then () else (
        map { "Last-Modified": format-dateTime(adjust-dateTime-to-timezone($dateTime, xs:dayTimeDuration("PT0H")), ' [FNn,*-3], [D] [MNn] [Y0001] [h01]:[m01]:[s01] GMT', 'en', (), ()) }
    )
};

(: **** / HTTP **** :)

(:~ Add an Amsterdam timezone to an xs:dateTime without one. Return input unaltered otherwise.
@param $in xs:dateTime with or without timezone
:)
declare function utillib:add-Amsterdam-timezone($in as xs:dateTime) as xs:dateTime {
    if (empty(timezone-from-dateTime($in))) then ( 
        (: Since 1996 DST starts last Sunday of March 02:00 and ends last Sunday of October at 03:00/02:00 (clock is set backwards) :)
        (: There is one hour in october (from 02 - 03) for which we can't be sure if no timezone is provided in the input, 
            we default to standard time (+01:00), the correct time will be represented if a timezone was in the input, 
            otherwise we cannot know in which hour it occured (DST or standard time) :)
        let $March31                    := xs:date(concat(year-from-dateTime($in), '-03-31'))
        let $DateTime-Start-SummerTime  := xs:dateTime(concat(year-from-dateTime($in), '-03-', (31 - utillib:day-of-week($March31)), 'T02:00:00Z'))
        let $October31                  := xs:date(concat(year-from-dateTime($in), '-10-31'))
        let $DateTime-End-SummerTime    := xs:dateTime(concat(year-from-dateTime($in), '-10-', (31 - utillib:day-of-week($October31)), 'T02:00:00Z'))
        
        return
            if ($in ge $DateTime-Start-SummerTime and $in lt $DateTime-End-SummerTime) then
                (: return UTC +2 in summer :)
                adjust-dateTime-to-timezone($in, xs:dayTimeDuration('PT2H'))
            else (
                (: return UTC +1 in winter :)
                adjust-dateTime-to-timezone($in, xs:dayTimeDuration('PT1H'))
            )
    )
    else (
        $in
    )
};

(:~ Returns day of week of a certain date as integer. Sunday = 0, Saturday = 6.
@param $date xs:date for which the day of week needs to be returned
:)
declare function utillib:day-of-week($date as xs:date?) as xs:integer? {
    if (empty($date)) then () else (
        (:let $ancientSunday := xs:date('1901-01-06'):)
        let $ancientSunday := xs:date('0001-01-07')
        
        return
            if (xs:date($date) ge $ancientSunday) then
                xs:integer((xs:date($date) - $ancientSunday) div xs:dayTimeDuration('P1D')) mod 7
            else (
                xs:integer(($ancientSunday - xs:date($date)) div xs:dayTimeDuration('P1D')) mod 7
            )
    )
};

(:~ Check if input string is an ISO OID
    @param $oid - optional. If empty result is false
    @return true if complies with OID pattern or false otherwise
:)
declare function utillib:isOid($oid as xs:string?) as xs:boolean {
    matches($oid, "^[0-2](\.(0|[1-9][0-9]*))*$")
};

(:~ get a name for an OID, e.g. "SNOMED-CT" for 2.16.840.1.113883.6.96
    - First check the OID Registry Lookup file
    - Then check any DECOR defined ids in repositories, or specific to a project
    - Then check any DECOR defined codeSystems in repositories, or specific to the project
    - If no name could be found, return empty string
    
    @param $oid - optional. The OID to get the display name for
    @param $language - required. The language to get the name in
    @param $projectPrefix - optional. The prefix of the project or element(decor) itself. Falls back to 'all' projects marked as repository 
:)
declare function utillib:getNameForOID($oid as xs:string?, $language as xs:string?, $decorOrPrefix as item()?) as xs:string {
let $language       := if (string-length($language) gt 0) then $language else ('en-US')
let $return         :=
    if (string-length($oid)=0) then ('') else (
        try {
            let $registryOids   := $setlib:colOidsData//oid[@oid = $oid]
            
            return
            if ($registryOids/name[@language = $language]) then 
                $registryOids/name[@language = $language][1]
            else
            if ($registryOids/name) then (
                $registryOids/name[1]
            )
            else (
                let $projectSystems   := 
                    if (empty($decorOrPrefix)) then
                        $setlib:colDecorData/decor
                    else 
                    if ($decorOrPrefix instance of element(decor)) then 
                        $decorOrPrefix
                    else (
                        utillib:getDecorByPrefix($decorOrPrefix)
                    )
                let $projectSystems   := $projectSystems/(terminology/codeSystem[@id = $oid] | terminology/codeSystem[@ref = $oid] | ids/id[@root = $oid]/designation)
                
                return
                if ($projectSystems[@language = $language][@displayName]) then
                    $projectSystems[@language = $language][1]/@displayName
                else
                if ($projectSystems[@displayName]) then
                    $projectSystems/@displayName
                else 
                if ($projectSystems[@name]) then (
                    $projectSystems/@name
                )
                else (
                    (: OID coming in: 2.16.840.1.113883.3.1937.99.62.3.1.1
                        Look for the longest matching OID we can get a name for
                        2.16.840.1.113883.3.1937.99.62.3.1
                        2.16.840.1.113883.3.1937.99.62.3
                        2.16.840.1.113883.3.1937.99.62
                        2.16.840.1.113883.3.1937.99
                        2.16.840.1.113883.3.1937
                        2.16.840.1.113883.3
                        2.16.840.1.113883
                        2.16.840.1
                        2.16.840
                        2.16
                        2
                    :)
                    (:let $basemap    :=
                        map:merge(
                            for $baseId in $decor/ids/baseId
                            let $bid    := $baseId/@id
                            group by $bid
                            return  map:entry($bid, $baseId[1])
                        ):)
                    let $baseId     := utillib:getBaseId($oid)
                    let $id         := if ($baseId) then replace($oid, concat($baseId/@id,'\.?'), $baseId/@prefix) else ($oid)
                    return
                        if ($oid = $id) then () else ($id)
                )
            )
        }
        catch * {()}
    )
return if ($return) then $return[1] else ('')
};
declare %private function utillib:getBaseId($oid as xs:string?) as element(baseId)? {
    if (string-length($oid) = 0) then () else (
        (:let $baseId := map:get($basemap, $oid):)
        let $baseId := $setlib:colDecorData//baseId[@id = $oid][@prefix]
        
        return
        if ($baseId) then $baseId[1] else (
            utillib:getBaseId(string-join(tokenize($oid,'\.')[position() lt last()], '.'))
        )
    )
};

(:~ get a HL7 FHIR URI for an OID, e.g. "http://snomed.info/sct" for 2.16.840.1.113883.6.96
    - First check the OID Registry Lookup file
    - Then check any DECOR defined ids in repositories, or specific to a project
    - If no URI could be found, but OID was provided return urn:oid:"input"
    - If OID was empty, return empty
    
    params:
    @param $oid - optional. The OID to get the FHIR URI for
    @param $flexibility - optional. the flexibility of the object to get it from, if applicable. Latest if missing
    @param $decorOrPrefix - optional. The prefix of the project or element(decor). Falls back to 'all' projects marked as repository 
    @param $fhirVersion - optional. The version of FHIR to retrieve the canonical for. Use any value from $setlib:arrKeyFHIRVersions, 
            or leave empty for the most current value. FHIR has changed the URIs a lot in various versions, hence the hassle
:)
declare function utillib:getCanonicalUriForOID($resourceType as xs:string, $object as element(), $decorOrPrefix as item()?, $fhirVersion as xs:string?) as xs:string* {
    if ($object[@canonicalUri]) then $object/@canonicalUri else
        switch (local-name($object))
        case 'include' return utillib:getCanonicalUriForOID($resourceType, $object/@ref, $object/@flexibility, $decorOrPrefix, $fhirVersion)
        case 'element' return utillib:getCanonicalUriForOID($resourceType, $object/@contains, $object/@flexibility, $decorOrPrefix, $fhirVersion)
        case 'relationship' return utillib:getCanonicalUriForOID($resourceType, $object/(@ref | @template), $object/@flexibility, $decorOrPrefix, $fhirVersion)
        case 'vocabulary' return utillib:getCanonicalUriForOID($resourceType, $object/@valueSet, $object/@flexibility, $decorOrPrefix, $fhirVersion)
        default return utillib:getCanonicalUriForOID($resourceType, $object/(@id, @ref)[1], $object/(@effectiveDate, @flexibility)[1], $decorOrPrefix, $fhirVersion)
};
declare function utillib:getCanonicalUriForOID($resourceType as xs:string, $oid as xs:string?, $flexibility as xs:string?, $decorOrPrefix as item()?, $fhirVersion as xs:string?) as xs:string* {
let $isoid          := utillib:isOid($oid)
let $return         :=
    if ($isoid) then (
        try {
            let $fhirKey     := setlib:strKeyCanonicalUriFhirPrefd($fhirVersion)
            
            (: oid registry :)
            let $oids        := $setlib:colOidsData//oid[@oid = $oid]/property[@type = $fhirKey]
            let $oids        := if ($oids) then $oids else $setlib:colOidsData//oid[@oid = $oid]/property[@type = $setlib:strKeyCanonicalUriPrefd]
            
            (: project :)
            let $oids        := if ($oids) then $oids else $setlib:colDecorData//codeSystem[@id = $oid]
            let $oids        := if ($oids) then $oids else $setlib:colDecorData//valueSet[@id = $oid]
            let $oids        := if ($oids) then $oids else $setlib:colDecorData//dataset[@id = $oid]
            let $oids        := if ($oids) then $oids else $setlib:colDecorData//transaction[@id = $oid]
            let $oids        := if ($oids) then $oids else $setlib:colDecorData//template[@id = $oid]
            let $oids        := if ($oids) then $oids else $setlib:colDecorData//concept[@id = $oid][not(ancestor::history)]
            let $oids        := if ($oids) then $oids else $setlib:colDecorData//questionnaire[@id = $oid]
            let $oids        := if ($oids) then $oids else $setlib:colDecorData//conceptMap[@id = $oid]
            let $oids        := 
                if ($oids) then $oids else (
                    let $ids    := $setlib:colDecorData//id[@root = $oid]/property[@name = $fhirKey]
                    let $ids   := if ($ids) then $oids else $setlib:colDecorData//id[@root = $oid]/property[@name = $setlib:strKeyCanonicalUriPrefd]
                    return  
                        if (empty($decorOrPrefix)) then
                            $ids[ancestor::decor/@repository = 'true']
                        else
                        if ($decorOrPrefix instance of element(decor)) then 
                            $ids[ancestor::decor/@repository = 'true'] | $ids[ancestor::decor/project/@prefix = $decorOrPrefix/project/@prefix]
                        else (
                            $ids[ancestor::decor/@repository = 'true'] | $ids[ancestor::decor/project/@prefix = $decorOrPrefix]
                        )
                )
            let $resourceType :=
                switch (local-name($oids[1]))
                case 'codeSystem' return 'CodeSystem'
                case 'questionnaire' return 'Questionnaire'
                case 'template' return 'StructureDefinition'
                case 'valueSet' return 'ValueSet'
                default return $resourceType
            return
                distinct-values(
                    if ($oids[@id]) then 
                        (: project related artifacts :)
                        if ($flexibility castable as xs:dateTime) then
                            utillib:getCanonicalUri($resourceType, $oids[@effectiveDate = $flexibility])
                        else
                            utillib:getCanonicalUri($resourceType, $oids[@effectiveDate = max($oids/xs:dateTime(@effectiveDate))])
                    else 
                    if ($oids[@name]) then
                    (: project related ids :)
                        $oids
                    else (
                    (: oid registry :)
                        $oids/@value
                    )
                )
        }
        catch * {()}
    )
    else ()
return 
    if ($isoid) then
        ($return[not(. = '')], 'urn:oid:' || $oid)[1] 
    else ()
};

(:~ Get canonicalUri from object if present or construct from id and effectiveDate :) 
declare function utillib:getCanonicalUri($resourceType as xs:string, $object as element()) as xs:string {
    if ($object[@canonicalUri]) then $object/@canonicalUri else (
        utillib:getCanonicalUri($resourceType, $object/@id, $object/@effectiveDate)
    )
};
(:~ Construct canonicalUri from server registered FHIR Canonical Base, the type and [id]--[effectiveDate as yyyymmddhhmmss] :)
declare function utillib:getCanonicalUri($resourceType as xs:string, $objectId as xs:string, $objectEffectiveDate as xs:string?) as xs:string {
    (: AD30-1399 Real world trouble from (re)generated canonicals :)
    if ($resourceType = 'CodeSystem') then
        'urn:oid:' || $objectId
    else (
        $utillib:strFhirCanonicalBaseURL || $resourceType || '/' || string-join(($objectId, replace($objectEffectiveDate, '\D', '')), '--')
    )
};

(:~ get an OID for a HL7 FHIR URI, e.g. 2.16.840.1.113883.6.96 for "http://snomed.info/sct"
    - First check the OID Registry Lookup file
    - Then check any DECOR defined ids in repositories, or specific to a project
    - If no URI could be found, but OID was provided return urn:oid:"input"
    - If OID was empty, return empty
    
    params:
    @param $uri - optional. The FHIR URI to get the OID for
    @param $flexibility - optional. the flexibility of the object to get it from, if applicable. Latest if missing
    @param $decorOrPrefix - optional. The prefix of the project or element(decor). Falls back to 'all' projects marked as repository
    @param $fhirVersion - optional. The version of FHIR to retrieve the oid for. Use any value from $setlib:arrKeyFHIRVersions, 
            or leave empty for the most current value. FHIR has changed the URIs a lot in various versions, hence the hassle
:)
declare function utillib:getOIDForCanonicalUri($uri as xs:string?, $flexibility as xs:string?, $decorOrPrefix as item()?, $fhirVersion as xs:string?) as xs:string* {
let $return         :=
    if (string-length($uri)=0) then () else (
        try {
            let $fhirKey     := setlib:strKeyCanonicalUriFhirPrefd($fhirVersion)
            
            (: oid registry :)
            let $oids        := $setlib:colOidsData//property[@type = $fhirKey][@value = $uri]
            let $oids        := if ($oids) then $oids else $setlib:colOidsData//property[@type = $setlib:strKeyCanonicalUriPrefd][@value = $uri]
            
            (: project :)
            let $oids        := if ($oids) then $oids else $setlib:colDecorData//codeSystem[@id][@canonicalUri= $uri]
            let $oids        := if ($oids) then $oids else $setlib:colDecorData//valueSet[@id][@canonicalUri= $uri]
            let $oids        := if ($oids) then $oids else $setlib:colDecorData//dataset[@id][@canonicalUri= $uri]
            let $oids        := if ($oids) then $oids else $setlib:colDecorData//transaction[@id][@canonicalUri= $uri]
            let $oids        := if ($oids) then $oids else $setlib:colDecorData//template[@id][@canonicalUri= $uri]
            let $oids        := if ($oids) then $oids else $setlib:colDecorData//concept[@id][@canonicalUri= $uri][not(ancestor::history)]
            let $oids        := if ($oids) then $oids else $setlib:colDecorData//questionnaire[@id][@canonicalUri= $uri]
            let $oids        := if ($oids) then $oids else $setlib:colDecorData//conceptMap[@id][@canonicalUri= $uri]
            let $oids        := 
                if ($oids) then $oids else (
                    let $ids    := $setlib:colDecorData//property[@name = $fhirKey][. = $uri]
                    let $ids    := if ($ids) then $ids else $setlib:colDecorData//property[@name = $setlib:strKeyCanonicalUriPrefd][. = $uri]
                    return  
                        if (empty($decorOrPrefix)) then
                            $ids[ancestor::decor/@repository = 'true']
                        else
                        if ($decorOrPrefix instance of element(decor)) then 
                            $ids[ancestor::decor/@repository = 'true'] | $ids[ancestor::decor/project/@prefix = $decorOrPrefix/project/@prefix]
                        else (
                            $ids[ancestor::decor/@repository = 'true'] | $ids[ancestor::decor/project/@prefix = $decorOrPrefix]
                        )
                )
            
            return
                distinct-values(
                    if ($oids[@id]) then 
                    (: project related artifacts :)
                        if ($flexibility castable as xs:dateTime) then
                            $oids[@effectiveDate = $flexibility]/@id
                        else
                            $oids[@effectiveDate = max($oids/xs:dateTime(@effectiveDate))]/@id
                    else 
                    if ($oids[@name]) then
                    (: project related ids :)
                        $oids/ancestor::id[1]/@root
                    else (
                    (: oid registry :)
                        $oids/ancestor::oid[1]/@oid
                    )
                )
        }
        catch * {()}
    )
return $return[1]
};

(:~ set/update/delete a HL7 FHIR URI for an OID, e.g. "http://snomed.info/sct" for 2.16.840.1.113883.6.96
    - First check if the OID has been registered in the project, 
        - if it hasn't, get the name for the OID as preferred displayName and use project/@defaultLanguage
        - if it has, add or overwrite the preferred FHIR URI

@param $oid - required. The OID to set the FHIR URI for
@param $uri - optional. The FHIR URI to connect the OID to. Connection property be removed if uri is empty
@param $decorOrPrefix - optional. The prefix of the project or element(decor). Falls back to 'all' projects marked as repository
@param $fhirVersion - optional. The version of FHIR to connect the canonical for. Use any value from $setlib:arrKeyFHIRVersions, 
        or leave empty for the most current value. FHIR has changed the URIs a lot in various versions, hence the hassle
@return xs:boolean true() if the URI was set/updated/deleted or false() if not
:)
declare function utillib:setCanonicalUriForOID($oid as xs:string, $uri as xs:anyURI?, $decorOrPrefix as item(), $fhirVersion as xs:string?) as xs:boolean {
let $decor          := if ($decorOrPrefix instance of element(decor)) then $decorOrPrefix else utillib:getDecorByPrefix($decorOrPrefix)
let $fhirKey        := setlib:strKeyCanonicalUriFhirPrefd($fhirVersion)
let $return         :=
    if (string-length($oid) = 0 or not($decor)) then ( false() ) else (
        let $language       := $decor/project/@defaultLanguage
        let $displayName    := utillib:getNameForOID($oid, $language, $decor)
        
        let $id             := $decor//id[@root = $oid]
        let $newproperty    := <property name="{$fhirKey}">{$uri}</property>
        let $newid          :=
            <id root="{$oid}">
                <designation language="{$language}" type="preferred" displayName="{$displayName}">{$displayName}</designation>
                {$newproperty}
            </id>
            
        let $updateDelete   :=
            if (string-length($uri) = 0) then
                update delete $id/property[@name = $fhirKey]
            else
            if ($id[property[@name = $setlib:strKeyCanonicalUriPrefd]]) then
                update replace $id/property[@name = $fhirKey] with $newproperty
            else
            if ($id) then
                update insert $newproperty into $id
            else (
                update insert $newid following ($decor/ids/baseId | $decor/ids/defaultBaseId)[last()]
            )
        return
            true()
    )
return $return
};

declare function utillib:addJsonArrayToElements($nodes as node()*) {
    utillib:addJsonArrayToElements($nodes, 'json')
};

declare function utillib:addJsonArrayToElements($nodes as node()*, $format as xs:string) {
            for $node in $nodes
            return
                if (not($format = 'json')) then $node 
                else if ($node instance of element() and $format = 'json') then
                    (: oid registry contains HL7 DTr2 datatypes with a language that we don't need/want any extras on, distinguishable by @value :)
             
                    let $node       := 
                        if ($node[@language][not(@value | *:data | self::compiled | self::compile-request | self::codeSystem)][*] | $node[self::example] | $node[self::property][not(ancestor::codedConcept)])
                        then utillib:serializeNode($node) 
                        else $node
                    
                    let $ns         := namespace-uri-from-QName(fn:resolve-QName(name($node), $node))
                    return
                        element {QName(string($ns), name($node))} {
                            (: https://github.com/eXist-db/exist/issues/4008 :)
                            if ($node[@json:*]) then () else 
                            if (empty($node/node() | $node/@*)) then () else attribute json:array {'true'}, 
                            $node/@*, 
                            for $child in $node/node() return utillib:addJsonArrayToElements($child)
                        }
                else
                if ($node instance of text()) then
                    if (normalize-space($node) = '') then () else $node
                else (
                    $node
                )
};

(:~ Returns appropriate copyright texts based on $usedCodeSystems <copyright language="en-US">Licensing note: .......</copyright> :)
declare function utillib:handleCodeSystemCopyrights($copyrightsource as element()*, $usedCodeSystems as item()*) as element()* {

    let $extracopyrights :=
        <extracopyright oid="2.16.840.1.113883.6.96" urn="http://snomed.info/sct">This artefact includes content from SNOMED Clinical Terms® (SNOMED CT®) which is copyright of the International Health Terminology Standards Development Organisation (IHTSDO). Implementers of these artefacts must have the appropriate SNOMED CT Affiliate license - for more information contact http://www.snomed.org/snomed-ct/getsnomed-ct or info@snomed.org.</extracopyright>
        
    let $extra :=
        for $t in $extracopyrights[@oid = $usedCodeSystems]
        return
            (: if text is not yet in the copyright texts :)
            if (contains($copyrightsource[@language='en-US'], $t/text())) then ()
            else 
                if ($copyrightsource[@language='en-US']) then 
                    <copyright language="en-US">{$copyrightsource[@language='en-US']/node()}<div data-source="inherited" style="border-top: 1px solid black">{$t/text()}</div></copyright>
                else (
                    <copyright language="en-US" inherited="true">{$t/text()}</copyright>
                )
    return
        if (empty($extra)) then $copyrightsource else ($copyrightsource[not(@language = $extra/@language)], $extra)
};


(:~ Issue messages to the scheduler log :)
declare function utillib:log-scheduler-event($level as xs:string, $threadId as xs:string, $event as xs:string, $message as item()*) {
    let $log-collection     := $setlib:strDecorScheduledTasks
    let $log                := "scheduled-log.xml"
    let $log-uri            := concat($log-collection, "/", $log)
    
    (: create the log file if it does not exist :)
    let $create-doc         := 
        if (doc-available($log-uri)) then () else (
            xmldb:store($log-collection, $log, <scheduler-log/>)
        )
    (: log the trigger details to the log file :)
    let $insert             :=
        update insert <log timestamp="{current-dateTime()}" thread="{$threadId}" event="{$event}">{$message}</log> into doc($log-uri)/scheduler-log
    let $logevent           :=
        util:log($level, current-dateTime() || ' ' || $threadId || ' ' || $event || ' ' || string-join(data($message), ' '))
    return
        ()
};

(: **** LUCENE SEARCHES **** :)
(:~ Returns lucene config xml for a sequence of strings. The search will find yield results that match all terms+trailing wildcard in the sequence
:   Example output:
:   <query>
:       <bool>
:           <wildcard occur="must">term1*</wildcard>
:           <wildcard occur="must">term2*</wildcard>
:       </bool>
:   </query>
:
:   @param $searchTerms required sequence of terms to look for
:   @return lucene config
:   @since 2014-06-06
:)
declare function utillib:getSimpleLuceneQuery($searchTerms as xs:string+) as element(query) {
    utillib:getSimpleLuceneQuery($searchTerms, 'wildcard')
};

(:~
:   http://exist-db.org/exist/apps/doc/lucene.xml?q=lucene&field=all&id=D2.2.4.28#D2.2.4.28
:)
declare function utillib:getSimpleLuceneQuery($searchTerms as xs:string+, $searchType as xs:string) as element(query) {
    <query>
        <bool>
        {
            for $term in $searchTerms
            let $term := tokenize($term, '[^A-Za-z\d]')
            return
                for $subterm in $term[not(. = '')]
                return
                    if ($searchType='fuzzy') then
                        <fuzzy occur="must">{concat($subterm,'~')}</fuzzy>
                    
                    else if ($searchType='fuzzy-not') then
                        <fuzzy occur="not">{concat($subterm,'~')}</fuzzy>
                        
                    else if ($searchType='regex') then
                        <regex occur="must">{$subterm}</regex>
                        
                    else if ($searchType='regex-not') then
                        <regex occur="not">{$subterm}</regex>
                        
                    else if ($searchType='phrase') then
                        <phrase occur="must">{$subterm}</phrase>
                        
                    else if ($searchType='phrase-not') then
                        <phrase occur="not">{$subterm}</phrase>
                        
                    else if ($searchType='term') then
                        <term occur="must">{$subterm}</term>
                        
                    else if ($searchType='term-not') then
                        <term occur="not">{$subterm}</term>
                        
                    else if ($searchType='wildcard') then
                        <wildcard occur="must">{replace($subterm, '([&amp;\|!\(\){}\[\]^"~*\?:\\/+-])', '\\$1') || '*'}</wildcard>
                        
                    else if ($searchType='wildcard-not') then
                        <wildcard occur="not">{replace($subterm, '([&amp;\|!\(\){}\[\]^"~*\?:\\/+-])', '\\$1') || '*'}</wildcard>
                        
                    else ()
        }
        </bool>
    </query>
};

(:~
:   Returns lucene options xml that instruct filter-rewrite=yes
:)
declare function utillib:getSimpleLuceneOptions() as element() {
    <options>
        <filter-rewrite>yes</filter-rewrite>
    </options>
};

(:~ After a call to load-xquery-module(), this function can be called on its result to retrieve a function in a way that resembles the standard function-lookup() function. The difference is the module (a map) should be passed as the first parameter.

@param $module The module map, e.g. as obtained by means of load-xquery-module()
@param $qname  The xs:QName representing the function, as in the standard function fn:function-lookup()
@param $arity  The number of parameters of the function given by $qname.
@return The function, or an empty sequence if not found.
@since 2021-05-31
:)
declare function utillib:function-lookup($module as map(*), $qname as xs:QName, $arity as xs:nonNegativeInteger) as function(*)? {
    $module?functions?($qname)?($arity)
};

declare function utillib:mergeDatasetWithUsage($dataset as element(), $transactions as element(transaction)*, $datasetPrefix as xs:string) as element() {
    if (empty($transactions)) then $dataset else (
        element {name($dataset)} {
            $dataset/@*,
            $dataset/(node() except concept),
            for $concept in $dataset/concept
            return
                utillib:mergeDatasetWithUsage($concept, $transactions, $datasetPrefix)
            ,
            if ($dataset[usage]) then () else (
                <usage>
                {
                    let $inscope    := if ($dataset instance of element(dataset)) then $transactions else $transactions[.//concept[@ref = $dataset/@id]]
                    
                    for $tx in $inscope
                    let $txpfx  := $tx/ancestor::decor/project/@prefix
                    return
                        <transaction>
                        {   $tx/@*, 
                            if ($tx[@url | @ident]) then () else 
                            if ($txpfx = $datasetPrefix) then () else (
                                attribute url {serverapi:getServerURLServices()},
                                attribute ident {$txpfx}
                            ),
                            ($tx/name[@language = $dataset/name/@language], $tx/name)[1]
                        }
                        </transaction>
                }
                </usage>
            )
        }
    )
};

declare function utillib:inheritPublishingAuthority($object as element()?) as element(publishingAuthority)? {
(: AD30-1632 populating publishing authority using a cascade from object-ancestor-or-self::/dataset/project :) 
    let $object2inherit                         :=
        if (not(lower-case(local-name($object)) = 'questionnaire')) then $object else if ($object[relationship]/relationship/@type = 'DRIV') then utillib:getTransaction($object/relationship/@ref, $object/relationship/@flexibility) else ()

    let $publishingAuthority            :=
        if ($object2inherit) then (
            (: inherit within nested element :)
            let $inheritedFrom          := $object2inherit/ancestor-or-self::*[publishingAuthority][1]
            let $publishingAuthority    := $inheritedFrom/publishingAuthority
                
            (: inherit from dataset :)
            return    
            if ($publishingAuthority) then (
                <publishingAuthority name="{$publishingAuthority/@name}" inherited="{lower-case(local-name($inheritedFrom))}">
                   {$publishingAuthority/*}
                </publishingAuthority>
                )
                else if ($object2inherit/representingTemplate/@sourceDataset) then (
                    let $publishingAuthority    := utillib:getDataset($object2inherit/representingTemplate/@sourceDataset, $object2inherit/representingTemplate/@sourceDatasetFlexibility)/publishingAuthority
                    return
                    if ($publishingAuthority) then
                        <publishingAuthority name="{$publishingAuthority/@name}" inherited="dataset">
                           {$publishingAuthority/*}
                        </publishingAuthority>
                    else ()
                )
            else ()
        )
        else ()
    
    (: inherit from project :)
    return
        if (empty($publishingAuthority)) then utillib:inheritPublishingAuthorityFromProject($object/ancestor::decor) else ($publishingAuthority)
};

declare function utillib:inheritPublishingAuthorityFromProject($decor as element(decor)) as element(publishingAuthority)? {
            
    let $copyright          := ($decor/project/copyright[empty(@type)] | $decor/project/copyright[@type = 'author'])[1]
    return
    if ($copyright) then
        <publishingAuthority name="{$copyright/@by}" inherited="project">
        {
            (: Currently there is no @id, but if it ever would be there ...:)
            $copyright/@id,
            $copyright/addrLine
        }
        </publishingAuthority>
        else ()
};

declare function utillib:inheritCopyright($object as element()) as element(copyright)* {

(: AD30-1632 populating copyright using a cascade from object-ancestor-or-self::/dataset :) 
    let $object2inherit                         :=
        if (not(lower-case(local-name($object))= 'questionnaire')) then $object else if ($object[relationship]/relationship/@type = 'DRIV') then utillib:getTransaction($object/relationship/@ref, $object/relationship/@flexibility) else ()
    
     return
        if ($object2inherit) then (
            (: inherit within nested element :)
            let $inheritedFrom              := $object2inherit/ancestor-or-self::*[copyright][1]
                
            (: inherit from dataset :)
            return  
                if ($inheritedFrom) then 
                (
                    for $c in $inheritedFrom/copyright
                    return
                    <copyright>
                    {
                       $c/@*,
                       attribute inherited {lower-case(local-name($inheritedFrom))},
                       if ($c/text()) then ($c/text()) else $c/* 
                    }
                    </copyright>
                 )
                 else if ($object2inherit/representingTemplate/@sourceDataset) then
                 (
                    for $c in utillib:getDataset($object2inherit/representingTemplate/@sourceDataset, $object2inherit/representingTemplate/@sourceDatasetFlexibility)/copyright
                    return 
                    <copyright>
                    {
                        $c/@*,
                        attribute inherited {'dataset'},
                        if ($c/text()) then ($c/text()) else $c/* 
                    }
                    </copyright>
                )
                else ()
        )
        else ()
   
};

declare function utillib:patchLatestOncoVersion($datasetOrTransaction as element(), $decorVersion as xs:string?, $projectLanguage as xs:string? ) as xs:dateTime? {

    let $latestOncoVersion              :=
        if (empty($decorVersion)) then (
            if ($datasetOrTransaction/ancestor::decor/project[starts-with(@prefix, 'onco-')]) then (
                let $projectPrefix  := $datasetOrTransaction/ancestor::decor/project/@prefix
                return utillib:getDecorByPrefix($projectPrefix, 'dynamic', $projectLanguage)[@versionDate castable as xs:dateTime]/@versionDate
            ) else ()
        ) else ()
    
    return $latestOncoVersion
};