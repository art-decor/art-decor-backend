xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:~
 : Variables to indicate common errors
 :)
module namespace errorslib       = "http://art-decor.org/ns/api/errors";

import module namespace roaster     = "http://e-editiones.org/roaster";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

declare function errorslib:get-map-as-xml($errmap as map(*)) {

    let $responsecode   := errors:get-status-code-from-error($errmap?code)
    return
        roaster:response($responsecode, head((roaster:accepted-content-types()[matches(., 'xml|json')], 'application/json')), 
            <error>
                <code>{$errmap?code}</code>
                <description>{$errmap?description}</description>
            {
                if ($responsecode lt 500) then () else (
                    <module>{$errmap?module}</module>,
                    <line>{$errmap?line}</line>,
                    <column>{$errmap?column}</column>
                )
            }
            </error>
        )
};