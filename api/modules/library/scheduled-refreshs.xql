xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:~ This xql is called only from the scheduler process configured in the eXist-db application directory.
    The file etc/conf.xml is expected to have these two lines in the /exist/scheduler element:
    
       <!-- Run a cache refresh every 4 hours, unschedule if fails -->
        <job 
           type="user"
           name="scheduled-refreshs"
           xquery="/db/apps/api/modules/library/scheduled-refreshs.xql"
           cron-trigger="0 */4 * * * ?"
           unschedule-on-exception="true">
           <parameter name="what" value="cache"/>
       </job>
:)
import module namespace sm          = "http://exist-db.org/xquery/securitymanager";
import module namespace decor-cache = "http://art-decor.org/ns/api/decor-cache" at "decor-cache.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "settings-lib.xqm";

import module namespace errors      = "http://e-editiones.org/roaster/errors";

(: set timeout to 15 minutes = 900 seconds :)
declare option exist:timeout "900000";

(:
    get parameters from the call
    
    topic
        what the topic of the refresh call is, any of 'chache' | 'sandbox'
    format
        useful for topic=cache, tells what format shall be assumed, any of 'decor' | ...
:)

declare variable $local:topic external;
declare variable $local:format external;

(: check availability of required parameters, fail if undefined or empty :)
let $topic :=
    if (
        try { $local:topic != '' }
        catch err:XPDY0002 {
            let $err := util:log('ERROR', 'scheduled-refresh: error +++ variable/parameter "topic" not set in server scheduler configuration')
            let $err := error($errors:BAD_REQUEST,'Required scheduled-refresh variable/parameter "topic" not set in server scheduler configuration')
            return ''
        }
        )
    then
        $local:topic
    else
        let $err := util:log('ERROR', 'scheduled-refresh: error +++ variable/parameter "topic" not set in server scheduler configuration')
        let $err := error($errors:BAD_REQUEST,'Required scheduled-refresh variable/parameter "topic" not set in server scheduler configuration')
        return ''

let $format :=
    if (
        try { $local:format != '' }
        catch err:XPDY0002 {
            let $err := util:log('ERROR', 'scheduled-refresh: error +++ variable/parameter "format" not set in server scheduler configuration')
            let $err := error($errors:BAD_REQUEST,'Required scheduled-refresh variable/parameter "format" not set in server scheduler configuration')
            return ''
        }
        )
    then
        $local:format
    else
        let $err := util:log('ERROR', 'scheduled-refresh: error +++ variable/parameter "format" not set in server scheduler configuration')
        let $err := error($errors:BAD_REQUEST,'Required scheduled-refresh variable/parameter "format" not set in server scheduler configuration')
        return ''

(:
    local variable settings
:)
let $cacheCollectionStr := $setlib:strDecorCache

(: check permissions first :)
let $result :=
    if (string-length($topic) > 0 and string-length($format) > 0)
    then 
        if (sm:has-access($cacheCollectionStr, 'rw')) then (
    
            (: logged in, proceed :)
            let $dummy := util:log('INFO', 'scheduled-refreshs called, topic "' || $local:topic || '"')
                                
            (: create some local vars and do some checks :)
            let $statusonly := false()
            
            (: see if last refresh is less than an hour ago :)
            let $hlimit                 := 1  (: no start again before hlimit hours have elpsed :)
            let $now                    := current-dateTime()
            
            let $check := 
                switch ($local:format)
                    case 'decor' return (
                        let $lastRefresh := decor-cache:getLastCacheRefresh('bbr')/@time
                        let $lastStarted := decor-cache:getProgressCacheRefresh('bbr')/@started
                        let $lastt := if ($lastStarted castable as xs:dateTime) then xs:dateTime($lastStarted) else $now
                        let $hourssince := if ($lastStarted = '') then $hlimit * 2 else ($now - $lastt) div xs:dayTimeDuration('PT1H')
                        let $check2 :=
                            if (($hourssince > $hlimit) or not($hourssince))
                            then (
                                let $dummy := util:log('INFO', 'scheduled-refreshs called, format "' || $local:format || '" last refresh at ' || $lastRefresh)
                                let $authmap := map { "name": "adbot", "groups": [ "dba" ] } (: fake authmap, we are already logged in :)
                                let $dummy := decor-cache:updateDecorCache($authmap, $statusonly, $local:format)
                                let $dummy := util:log('INFO', 'scheduled-refreshs done, format "' || $local:format || '"')
                                return "success"
                            ) else (
                                let $dummy := util:log('WARN', 'scheduled-refreshs called to early after last call started, hours elapsed: ' || format-number($hourssince, '0.00'))
                                return "postponed"
                            )
                        return $check2
                    )
                    default return (
                        let $check := util:log('ERROR', 'scheduled-refreshs called with unknown format"' || encode-for-uri($local:format) || '"')
                        return "failed-unknowntype"
                    )
            return $check
        ) else (
            let $check := util:log('ERROR', 'scheduled-refreshs: unsufficient access rights, check admin docs')
            return "failed-noaccess"
        )
    else (
        let $check := util:log('ERROR', 'scheduled-refreshs: error +++ variable/parameter not set in server scheduler configuration')
        return "error-in-parameter-error"
    )

return <result status="{$result}"/>