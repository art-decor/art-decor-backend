xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

module namespace utilhtml               = "http://art-decor.org/ns/api/util-html";

import module namespace utillib         = "http://art-decor.org/ns/api/util" at "util-lib.xqm";
import module namespace setlib          = "http://art-decor.org/ns/api/settings" at "settings-lib.xqm";
import module namespace adterm          = "http://art-decor.org/ns/terminology" at "/db/apps/terminology/api/api-terminology.xqm";
import module namespace tmapi           = "http://art-decor.org/ns/api/template" at "../template-api.xqm";
import module namespace serverapi       = "http://art-decor.org/ns/api/server" at "server-api.xqm";

(: order is important!! :)
declare variable $utilhtml:columnNamesConcept   := (
    'columnName', 
    'columnID', 
    'columnMandatory',
    'columnConformance',
    'columnCardinality', 
    'columnMax',
    'columnCCD',
    'columnDatatype',
    'columnProperty',
    'columnExample',
    'columnCodes',
    'columnDescription',
    'columnContext',
    'columnSource',
    'columnRationale',
    'columnOperationalization',
    'columnComment',
    'columnMapping', 
    'columnCondition',
    'columnEnableWhen',
    'columnStatus',
    'columnCommunity',
    'columnTerminology',
    'columnValueSet',
    'columnType',
    'columnParent',
    'columnInherit'
);

declare variable $utilhtml:STATUSCODES-INACTIVE := ('cancelled','rejected','deprecated');
declare variable $utilhtml:STATUSCODES-DRAFT    := ('new','draft','pending');
declare variable $utilhtml:seetype              := 'live-services-vue';
declare variable $utilhtml:strDecorServicesURL  := utillib:getServerURLServices();
declare variable $utilhtml:strAD3ServerURL      := utillib:getServerURLArt3() || '#/';
declare variable $utilhtml:strArtURL            := utillib:getServerURLArt();
declare variable $utilhtml:strFhirServicesURL   := utillib:getServerURLFhirServices();
declare variable $utilhtml:serverLogo           := serverapi:getServerLogo();
declare variable $utilhtml:strServerURLArtApi   := serverapi:getServerURLArtApi();
declare variable $utilhtml:docHtmlNames         := local:getHtmlNames(); 

(: Build the main HTML view for valueset :)

declare function utilhtml:convertObject2Html(
    $objects as node()*, 
    $language as xs:string,
    $header as xs:boolean, 
    $collapsable as xs:boolean, 
    $version as xs:string?,
    $decor as element(decor)?
    )  as node() {
    
    
    
    (: set all generic variables :)
(:    let $bindingBehaviorValueSetsURL    := $utilhtml:strDecorServicesURL || 'RetrieveValueSet?prefix=' || $decor/project/@prefix || '&amp;language=' || $language || '&amp;version=' || $version:)
    let $bindingBehaviorValueSetsURL    := $utilhtml:strServerURLArtApi || '/valueset/$view?project=' || $decor/project/@prefix || '&amp;release=' || $version || '&amp;language=' || $language
    
    let $xsltParameters                 :=  
    <parameters>
        <param name="projectDefaultLanguage"        value="{$language}"/>
        <param name="artdecordeeplinkprefix"        value="{$utilhtml:strArtURL}"/>
        <param name="seeThisUrlLocation"            value="{$utilhtml:seetype}"/>
        <param name="displayHeader"                 value="{$header}"/>
        <param name="bindingBehaviorValueSetsURL"   value="{$bindingBehaviorValueSetsURL}"/>
        <param name="switchCreateTreeTableHtml"     value="{$collapsable}"/>
    </parameters>

    let $logo                           := local:getLogo($decor/project/@prefix)

    let $url                            := if ($decor) then $decor/project/reference[@url castable as xs:anyURI]/@url else ()       
    
    let $strCollapse                    := $utilhtml:docHtmlNames[@key = 'collapse']/text[@language = $language]
    let $strExpand                      := $utilhtml:docHtmlNames[@key = 'expand']/text[@language = $language]
    let $strGoTo                        := $utilhtml:docHtmlNames[@key = 'goTo']/text[@language = $language]
    
    let $resourcePath                   := if(contains($utilhtml:strDecorServicesURL,'localhost')) then '/exist/apps/decor/core/assets' else '/decor/core/assets'

    
    (: set all object dependent variables - we have ValusSet, CodeSystem  and Template using a stylesheet for transforming to an html-view:)
    let $objectName                     := 
        if (exists($objects/descendant-or-self::valueSet)) then 'ValueSet'
        else if (exists($objects/descendant-or-self::codeSystem)) then 'CodeSystem'
        else if (exists($objects/descendant-or-self::template)) then 'Template'
        else if (exists($objects/descendant-or-self::conceptMap)) then 'ConceptMap'
        else ()
     
    let $objectCount                          := 
        switch ($objectName)
        case 'ValueSet' return count($objects/descendant-or-self::valueSet[@id])
        case 'CodeSystem' return count($objects/descendant-or-self::codeSystem[@id])
        case 'Template' return count($objects/template/template[@id])
        case 'ConceptMap' return count($objects/descendant-or-self::conceptMap[@id])
        default return 0
    
    let $strHeader                      := $utilhtml:docHtmlNames[@key = $objectName]/text[@language = $language]
    let $strHeaders                     := $utilhtml:docHtmlNames[@key = $objectName || 's']/text[@language = $language]
    
    let $title                          := 
        if ($objectCount=1) then $strHeader || ' ' || $objects//@name[1]/string() else $strHeaders

    let $href                           := $utilhtml:strAD3ServerURL || $decor/project/@prefix || '/project/project-index/' || lower-case($objectName || 's')
    
    let $xslt                           := 'xmldb:exist://' || $setlib:strDecorCore || '/' || $objectName || '2html.xsl'
    
    (:this preserves configuration like @deeplinkprefix, and project/restURI for FHIR:)
    let $inputPackage            := 
        if ($objectName = 'Template') then $objects else
            <decor>
            {
                $decor/@*, 
                if ($decor/@deeplinkprefix) then () else if ($utilhtml:strArtURL) then 
                    attribute deeplinkprefix {$utilhtml:strArtURL}
                else (),
                if ($decor/@deeplinkprefixservices) then () else if ($utilhtml:strDecorServicesURL) then 
                    attribute deeplinkprefixservices {$utilhtml:strDecorServicesURL}
                else (),
                if ($decor/@deeplinkprefixservicesfhir) then () else if ($utilhtml:strFhirServicesURL) then 
                    attribute deeplinkprefixservicesfhir {$utilhtml:strFhirServicesURL}
                else (),
                $decor/project
            }
                <terminology>{$objects}</terminology>
            </decor>

    return 
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{substring($language,1,2)}" lang="{substring($language,1,2)}">
            <head>
                <meta http-equiv="Content-Type" content="text/xhtml; charset=utf-8"/>
                <title>{$title}</title>
                {
                    if ($collapsable) then (
                        <link href="{$resourcePath}/css/retrieve-template.css" rel="stylesheet" type="text/css"></link> |
                        <script type="text/javascript" xmlns="http://www.w3.org/1999/xhtml">
                            window.treeTableCollapsed = true;
                            window.treeTableStringCollapse = '{$strCollapse/string()}';
                            window.treeTableStringExpand = '{$strExpand/string()}';
                            window.treeTableColumn = 0;
                        </script> |
                        <script src="{$resourcePath}/scripts/jquery-1.11.3.min.js" type="text/javascript"></script> |
                        <script src="{$resourcePath}/scripts/jquery.treetable.js" type="text/javascript"></script> |
                        <script src="{$resourcePath}/scripts/retrieve-transaction.js" type="text/javascript"></script> |
                        <link href="{$resourcePath}/decor.css" rel="stylesheet" type="text/css"></link> |
                        <style type="text/css">body {{ background-color: white; }}</style>
                    ) else (
                        <link href="{$resourcePath}/decor.css" rel="stylesheet" type="text/css"></link>
                    )
                }
            </head>
            <body>
            {
                if ($header) then (     
                 <table width="100%">
                    <tr>
                        <td align="left">
                            <h1>
                            {
                                if ($objectCount = 1) then $strHeader/string() else $strHeaders/string()                                      
                            }
                            </h1>
                        </td>
                        <td align="right">
                        {if ($logo and $url) then 
                            <a href="{$url}">
                                <img src="{$logo}" alt="" title="{$url}" height="50px"/>
                            </a>
                         else if ($logo) then
                            <img src="{$logo}" alt="" height="50px"/>
                         else ()
                        }
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            {$strGoTo/string()}
                            <a href="{$href}" target="_blank" alt="">index</a> 
                        </td>
                    </tr>
                </table>
                )
                else ()
            }
            {
                transform:transform($inputPackage, $xslt, $xsltParameters)
            }
            </body>
        </html>

};    


(: Build the main HTML view for dataset or transaction
Accepts hidecolumns string, i.e.: '123456789a' or '54'
Hides columns based on (hex) number. 
:)
declare function utilhtml:convertTransactionOrDataset2Html(
    $fullDatasetTree as node(), 
    $language as xs:string?,
    $ui-lang as xs:string?,
    $hidecolumns as xs:string?, 
    $showonlyactiveconcepts as xs:boolean, 
    $collapsed as xs:boolean, 
    $draggable as xs:boolean, 
    $version as xs:string?,
    $url as xs:anyURI?,
    $projectPrefix as xs:string,
    $templateChain as element()*,
    $doSubConcept as xs:boolean,
    $fileNameforDownload as xs:string,
    $download as xs:boolean
    )  as node() {
    
    let $language                       := if (empty($language)) then $fullDatasetTree/name/@language[1] else $language
    let $ui-lang                        := if (empty($ui-lang)) then $setlib:strArtLanguage else $ui-lang
    let $hidecolumns                    := if (empty($hidecolumns)) then '' else $hidecolumns
    let $docommunity                    := exists($fullDatasetTree//community)
    let $isTransaction                  := exists($fullDatasetTree/@transactionId)
    let $displayTitle                   := ' ' || $fullDatasetTree/name[@language=$language][1]/string() || ' ' || $fullDatasetTree/@versionLabel || ' '
    let $title                          := 
        if ($isTransaction) then $utilhtml:docHtmlNames[@key = 'transaction']/text[@language = $ui-lang] || $displayTitle
        else $utilhtml:docHtmlNames[@key = 'dataset']/text[@language = $ui-lang] || $displayTitle
     
    let $title                          := 
        if ($doSubConcept) then $title || ' - ' || $utilhtml:docHtmlNames[@key = 'concept']/text[@language = $ui-lang] || ' ' || $fullDatasetTree/concept[1]/name[@language=$language][1]/string()
        else $title
        
    let $logo                           := local:getLogo($projectPrefix)
    
    (: column name can't be hidden :)
    let $columnmap                      := 
        map:merge((
            map:entry('columnName', '') ,
            map:entry('columnID', if (contains($hidecolumns, '2'))                      then true() else false()) ,
            if ($isTransaction) then (
            
                map:entry('columnMandatory', if (contains($hidecolumns, '3'))           then true() else false()) ,
                map:entry('columnConformance', if (contains($hidecolumns, '4'))         then true() else false()) ,
                map:entry('columnCardinality', if (contains($hidecolumns, '5'))         then true() else false()) ,
                map:entry('columnMax', if (contains($hidecolumns, '6'))                 then true() else false()) ,
                map:entry('columnCCD', if (contains($hidecolumns, '0'))                 then true() else false())
            
            ) else (),
            map:entry('columnDatatype', if (contains($hidecolumns, '7'))                then true() else false()) ,
            map:entry('columnProperty', if (contains($hidecolumns, '8'))                then true() else false()) ,
            map:entry('columnExample', if (contains($hidecolumns, '9'))                 then true() else false()) ,
            map:entry('columnCodes', if (contains($hidecolumns, 'a'))                   then true() else false()) ,
            map:entry('columnDescription', if (contains($hidecolumns, 'b'))             then true() else false()) ,
            if ($isTransaction) then (
            
                map:entry('columnContext', if (contains($hidecolumns, 'p'))             then true() else false())
            
            ) else (),
            map:entry('columnSource', if (contains($hidecolumns, 'c'))                  then true() else false()) ,
            map:entry('columnRationale', if (contains($hidecolumns, 'd'))               then true() else false()) ,
            map:entry('columnOperationalization', if (contains($hidecolumns, 'e'))      then true() else false()) ,
            map:entry('columnComment', if (contains($hidecolumns, 'f'))                 then true() else false()) ,
            if ($isTransaction) then (
            
                map:entry('columnMapping', if (contains($hidecolumns, 'o'))             then true() else false()) ,
                map:entry('columnCondition', if (contains($hidecolumns, 'g'))           then true() else false()) ,
                map:entry('columnEnableWhen', if (contains($hidecolumns, 'q'))          then true() else false())
            
            ) else (),
            map:entry('columnStatus', if (contains($hidecolumns, 'h'))                  then true() else false()) ,
            if (not($docommunity)) then () else (
            
                map:entry('columnCommunity', if (contains($hidecolumns, 'i'))           then true() else false())
            
            ),
            map:entry('columnTerminology', if (contains($hidecolumns, 'j'))             then true() else false()) ,
            map:entry('columnValueSet', if (contains($hidecolumns, 'k'))                then true() else false()) ,
            map:entry('columnType', if (contains($hidecolumns, 'l'))                    then true() else false()) ,
            map:entry('columnParent', if (contains($hidecolumns, 'm'))                  then true() else false()) ,
            map:entry('columnInherit', if (contains($hidecolumns, 'n'))                 then true() else false())
        ))

    (: for testing only :)
    (:let $linkroot := 'http://localhost:8877':)
    let $resourcePath                   :=  
        if ($download) then 'https://assets.art-decor.org/ADAR/rv/assets' else if(contains($utilhtml:strDecorServicesURL,'localhost')) then '/exist/apps/decor/core/assets' else '/decor/core/assets'
    let $html                           := 
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>{$title}</title>
            <link href="{$resourcePath}/css/default.css" rel="stylesheet" type="text/css"></link>
            <link href="{$resourcePath}/css/retrieve-transaction.css" rel="stylesheet" type="text/css"></link>
            <script type="text/javascript">window.treeTableCollapsed={if ($collapsed) then 'true' else 'false'};</script>
            <script src="{$resourcePath}/scripts/jquery-1.11.3.min.js" type="text/javascript"></script>
            <script src="{$resourcePath}/scripts/jquery.treetable.js" type="text/javascript"></script>
            <script src="{$resourcePath}/scripts/retrieve-transaction.js" type="text/javascript"></script>
            <script src="{$resourcePath}/scripts/jquery.cookie.js" type="text/javascript"></script>
            {if ($draggable) then <script src="{$resourcePath}/scripts/dragtable.js" type="text/javascript"></script> else ()}
        </head>
        <body>
            <table style="width: 100%;">
                <tbody class="list">
                    <tr>
                        <td style="text-align: left;">
                            <h1>{$title}</h1>
                            <p>
                                <span style="margin-right: 1em;">{$utilhtml:docHtmlNames[@key = 'columnID']/text[@language = $ui-lang]}: {if ($isTransaction) then ($fullDatasetTree/@transactionId/string()) else ($fullDatasetTree/@id/string())}</span>
                                <span style="margin-right: 1em;">{$utilhtml:docHtmlNames[@key = 'columnVersionLabel']/text[@language = $ui-lang]}: {if ($version) then $version else $utilhtml:docHtmlNames[@key = 'columnLiveVersion']/text[@language = $ui-lang]}</span>
                                <span style="margin-right: 1em;">
                                {
                                    string-join((
                                        $utilhtml:docHtmlNames[@key = 'concepts']/text[@language = $ui-lang], ': ', count($fullDatasetTree//concept[@type][not(ancestor::conceptList)]), 
                                        ' (', 
                                        $utilhtml:docHtmlNames[@key = 'groups']/text[@language = $ui-lang], ': ', count($fullDatasetTree//concept[@type = 'group']),
                                        ' - ',
                                        $utilhtml:docHtmlNames[@key = 'items']/text[@language = $ui-lang], ': ', count($fullDatasetTree//concept[@type = 'item']),
                                        ')'
                                    ), '')
                                }
                                </span>
                            </p>
                        </td>
                        <td style="text-align: right;">
                        {if ($logo and $url) then 
                            <a href="{$url}">
                                <img src="{$logo}" alt="" title="{$url}" style="max-height: 50px;"/>
                            </a>
                         else if ($logo) then
                            <img src="{$logo}" alt="" style="max-height: 50px;"/>
                         else ()
                        }
                        </td>
                    </tr>
                </tbody>
            </table>
            
            <div id="concepts">
                <button id="expandAll" type="button">{$utilhtml:docHtmlNames[@key = 'buttonExpandAll']/text[@language = $ui-lang]}</button> 
                <button id="collapseAll" type="button">{$utilhtml:docHtmlNames[@key = 'buttonCollapseAll']/text[@language = $ui-lang]}</button> 
                <input id="nameSearch" placeholder="{$utilhtml:docHtmlNames[@key = 'textSearch']/text[@language = $ui-lang]}" />
                <button id="nameSort" type="button">{$utilhtml:docHtmlNames[@key = 'buttonSortByName']/text[@language = $ui-lang]}</button> 
                <select id="hiddenColumns"> 
                    <option value="title">{$utilhtml:docHtmlNames[@key = 'showCcolumn']/text[@language = $ui-lang]}</option>
                {
                    for $column in $utilhtml:columnNamesConcept[not(. = 'columnName')]
                    let $columnName := $utilhtml:docHtmlNames[@key = $column]/text[@language = $ui-lang]
                    return 
                        if (map:keys($columnmap)[. = $column]) then (
                            <option value="{$column}">
                            {
                                if (map:get($columnmap, $column)) then () else (attribute disabled {'disabled'}), 
                                $columnName
                            }
                            </option>
                        ) else ()
                }
                </select> 
                <button id="noTree" type="button">{$utilhtml:docHtmlNames[@key = 'buttonListView']/text[@language = $ui-lang]}</button> 
                <button id="resetToDefault" type="button">{$utilhtml:docHtmlNames[@key = 'resetToDefault']/text[@language = $ui-lang]}</button>
                <button id="download" type="button" onclick="javascript:location.href=window.location.href+'&amp;download=true';" title="{$utilhtml:docHtmlNames[@key = 'buttonDownloadHelp']/text[@language = $ui-lang]}">{$utilhtml:docHtmlNames[@key = 'buttonDownload']/text[@language = $ui-lang]}</button>
                <button id="excelExport" onclick="exportTableToExcel('transactionTable', '{$fileNameforDownload}')" title="{$utilhtml:docHtmlNames[@key = 'buttonExportTableToExcelHelp']/text[@language = $ui-lang]}">{$utilhtml:docHtmlNames[@key = 'buttonExportTableToExcel']/text[@language = $ui-lang]}</button>
                <p class="helptext" style="font-size:x-small;">{$utilhtml:docHtmlNames[@key = 'helpText']/text[@language = $ui-lang]}</p>
                <table id="transactionTable" class="draggable">
                    <thead>
                        <tr>
                            <th class="columnName">
                                <b>{$utilhtml:docHtmlNames[@key = 'columnName']/text[@language = $ui-lang]}</b>
                            </th>
                            {
                                for $column in $utilhtml:columnNamesConcept[not(. = 'columnName')]
                                let $columnName := $utilhtml:docHtmlNames[@key = $column]/text[@language = $ui-lang]
                                return
                                if (map:keys($columnmap)[. = $column]) then (
                                    <th class="{$column}">
                                        {if (map:get($columnmap, $column)) then (attribute style {'display:none;'}) else ()}
                                        <b>{$columnName}</b>
                                        <span class="hideMe" type="button">[&#8209;]</span>
                                    </th>
                                ) else ()
                            }
                        </tr>
                    </thead>
                    <tbody class="list">
                    {
                        let $children := 
                            if ($showonlyactiveconcepts) then (
                                for $concept in $fullDatasetTree/concept 
                                return if ($concept[@statusCode = $utilhtml:STATUSCODES-INACTIVE]) then () else ($concept)
                            )
                            else $fullDatasetTree/concept
                        for $concept in $children
                        return 
                            local:getConceptRows($concept, '', $language, $ui-lang, $columnmap, $showonlyactiveconcepts, $docommunity, $isTransaction, $version, $projectPrefix, $templateChain, $download)
                    }
                    </tbody>
                </table>
            </div>
            <script>
            function exportTableToExcel(tableID, filename = ''){{
                var downloadLink;
                var dataType = 'application/vnd.ms-excel';
                var tableSelect = document.getElementById(tableID);
                var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20').replace(/#/g, '%23');
                
                // Specify file name
                filename = filename?filename+'.xls':'excel_data.xls';
                
                // Create download link element
                downloadLink = document.createElement("a");
                
                document.body.appendChild(downloadLink);
                
                if (navigator.msSaveOrOpenBlob) {{
                    var blob = new Blob(['\ufeff', tableHTML], {{
                        type: dataType
                    }});
                    navigator.msSaveOrOpenBlob( blob, filename);
                }} else {{
                    // Create a link to the file
                    downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
                
                    // Setting the file name
                    downloadLink.download = filename;
                    
                    //triggering the function
                    downloadLink.click();
                }}
            }}
            </script>
            </body>
    </html>
    return 
        element {QName(namespace-uri($html), name($html))} { $html/node()}
};

(: 
    Build a hierarchical list view in HTML
:)
declare function utilhtml:convertTransactionOrDataset2SimpleHtml(
    $fullDatasetTree as node(), 
    $language as xs:string?,
    $ui-lang as xs:string?,
    $hidecolumns as xs:string?, 
    $showonlyactiveconcepts as xs:boolean,
    $version as xs:string?,
    $url as xs:anyURI?,
    $projectPrefix as xs:string,
    $doSubConcept as xs:boolean,
    $download as xs:boolean
    )  as node() {
    
    let $language                       := if (empty($language)) then $fullDatasetTree/name/@language[1] else $language
    let $ui-lang                        := if (empty($ui-lang)) then $setlib:strArtLanguage else $ui-lang
    let $hidecolumns                    := if (empty($hidecolumns)) then '' else $hidecolumns
    let $isTransaction                  := exists($fullDatasetTree/@transactionId)
    let $displayTitle                   := ' ' || $fullDatasetTree/name[@language=$language][1]/string() || ' ' 
    let $title                          := 
        if ($isTransaction) then $utilhtml:docHtmlNames[@key = 'transaction']/text[@language = $ui-lang] || $displayTitle
        else $utilhtml:docHtmlNames[@key = 'dataset']/text[@language = $ui-lang] || $displayTitle
     
    let $title                          := 
        if ($doSubConcept) then $title || ' - ' || $utilhtml:docHtmlNames[@key = 'concept']/text[@language = $ui-lang] || ' ' || $fullDatasetTree/concept[1]/name[@language=$language][1]/string()
        else $title

    let $logo                           := local:getLogo($projectPrefix)
    
    let $columnmap      := map:merge((
        map:entry('columnDescription', if (contains($hidecolumns, 'b'))             then true() else false()),
        map:entry('columnMapping',     if (contains($hidecolumns, 'o'))             then true() else false())
    ))
 
    let $resourcePath                   :=  
        if ($download) then 'https://assets.art-decor.org/ADAR/rv/assets' else if(contains($utilhtml:strDecorServicesURL,'localhost')) then '/exist/apps/decor/core/assets' else '/decor/core/assets'
    let $html := 
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>{$title}</title>
            <link href="{$resourcePath}/css/default.css" rel="stylesheet" type="text/css"></link>
            <style type="text/css">
                <![CDATA[
                 ol.ad-dataset-group { list-style-type: none;}
                 li.ad-dataset-group { font-weight: bold; padding: 7px 0 0 0;}
                 li.ad-dataset-item, li.ad-template { font-weight: normal; list-style-type: none; padding: 7px 0 0 0; }
                 ul.ad-terminology-code { list-style-position: inside; font-weight: normal; padding: 7px 0 3px 20px; border: 0px; list-style-type: disc;  }
                 ul.ad-transaction-condition { padding: 7px 0 3px 20px; list-style-position: inside;}
                 li.ad-transaction-condition { font-style: italic; font-weight: normal; list-style-type: circle; padding: 0 3px 0 5px; border-left: 5px solid #ddd; }
                 div.ad-dataset-itemnumber, div.ad-templatetype { margin: -2px 0 5px 6px; display: inline-block; border: 1px solid #c0c0c0; 
                    background-color: #eee; border-radius: 3px 3px 3px 3px; -moz-border-radius: 3px 3px 3px 3px; -webkit-border-radius: 3px 3px 3px 3px; 
                    width: auto !important; padding: 1px 5px 1px 5px;}
                 div.ad-dataset-level1 { font-size: 2ex; font-weight: bold; border-bottom: 2px solid #ddd;}
                 table.ad-transaction-table, table.ad-template-table {border: 1px solid #888; border-collapse: collapse; width:100%;}
                 div.cdadocumentlevel { background-color: #eef; }
                 div.cdaheaderlevel { background-color: #ffe; }
                 div.cdasectionlevel { background-color: #efe; }
                 div.cdaentrylevel { background-color: #fef; }
                 div.ad-itemnumber-green { background-color: #efe; }
                 div.ad-itemnumber-blue { background-color: #cef; }
                 div.ad-itemnumber-yellow { background-color: #ffe; }
                 div.description, div.context {font-weight: normal; font-size: 12px; color: #aaa;}
                 ul.tmap {list-style-type: none; padding: 0; margin: 0; color:#8b0000; font-weight: normal; font-size: 10px; }
                 ]]>
            </style>
        </head>
        <body>
            <table style="width: 100%;">
                <tbody class="list">
                    <tr>
                        <td style="text-align: left;">
                            <h1>{$title}</h1>
                        </td>
                        <td style="text-align: right;">
                        {if ($logo and $url) then 
                            <a href="{$url}">
                                <img src="{$logo}" alt="" title="{$url}" style="max-height: 50px;"/>
                            </a>
                         else if ($logo) then
                            <img src="{$logo}" alt="" style="max-height: 50px;"/>
                         else ()
                        }
                        </td>
                    </tr>
                </tbody>
            </table>
            <p>
                <span style="margin-right: 1em;">{$utilhtml:docHtmlNames[@key = 'columnID']/text[@language = $ui-lang]}: {if ($isTransaction) then ($fullDatasetTree/@transactionId/string()) else ($fullDatasetTree/@id/string())}</span>
                <span style="margin-right: 1em;">{$utilhtml:docHtmlNames[@key = 'columnVersionLabel']/text[@language = $ui-lang]}: {if ($version) then $version else $utilhtml:docHtmlNames[@key = 'columnLiveVersion']/text[@language = $ui-lang]}</span>
                <span style="margin-right: 1em;">
                {
                    string-join((
                        $utilhtml:docHtmlNames[@key = 'concepts']/text[@language = $ui-lang], ': ', count($fullDatasetTree//concept[@type][not(ancestor::conceptList)]), 
                        ' (', 
                        $utilhtml:docHtmlNames[@key = 'groups']/text[@language = $ui-lang], ': ', count($fullDatasetTree//concept[@type = 'group']),
                        ' - ',
                        $utilhtml:docHtmlNames[@key = 'items']/text[@language = $ui-lang], ': ', count($fullDatasetTree//concept[@type = 'item']),
                        ')'
                    ), '')
                }
                </span>
            </p>
        <table class="ad-transaction-table">
            <tr>
            <td style="padding: 7px 7px 7px 7px;">
            {
                let $children := 
                    if ($showonlyactiveconcepts) then (
                        for $concept in $fullDatasetTree/concept 
                        return if ($concept[@statusCode = $utilhtml:STATUSCODES-INACTIVE]) then () else ($concept)
                    )
                    else $fullDatasetTree/concept
                for $concept in $children
                    return local:getSimpleConceptRows($concept, $language, $ui-lang, $projectPrefix, $columnmap, $showonlyactiveconcepts, 1, $isTransaction)
            }
            </td>
            </tr>
            </table>
        </body>
    </html>
    return $html
};

(: 
    Build HTML rows for concept 
:)
declare %private function local:getConceptRows(
    $concept as element(), 
    $parentId as xs:string, 
    $language as xs:string,
    $ui-lang as xs:string,
    $columnmap as item()?, 
    $showonlyactiveconcepts as xs:boolean,
    $docommunity as xs:boolean,
    $isTransaction as xs:boolean,
    $version as xs:string?,
    $projectPrefix as xs:string,
    $templateChain as element()*,
    $download as xs:boolean
    ) as element()* {
    let $rowId                          := concat('id_', replace($concept/@id, '\.', '_'))
    (: gather all names for display in the right language :)
    let $nameStatic                     := $utilhtml:docHtmlNames[@key = 'static']/text[@language = $ui-lang]
    let $nameDynamic                    := $utilhtml:docHtmlNames[@key = 'dynamic']/text[@language = $ui-lang]
    let $nameValueSet                   := $utilhtml:docHtmlNames[@key = 'ValueSet']/text[@language = $ui-lang]
    let $columnCodeSystemWithFilters    := $utilhtml:docHtmlNames[@key = 'columnCodeSystemWithFilters']/text[@language = $ui-lang]
    let $columnCodeSystem               := $utilhtml:docHtmlNames[@key = 'columnCodeSystem']/text[@language = $ui-lang]
    let $nameLength                     := $utilhtml:docHtmlNames[@key = 'length']/text[@language = $ui-lang]
    let $nameFractionDigits             := $utilhtml:docHtmlNames[@key = 'fractionDigits']/text[@language = $ui-lang]
    let $nameRange                      := $utilhtml:docHtmlNames[@key = 'range']/text[@language = $ui-lang]
    let $nameTimeStampPrecision         := $utilhtml:docHtmlNames[@key = 'timeStampPrecision']/text[@language = $ui-lang]
    let $nameUnit                       := $utilhtml:docHtmlNames[@key = 'unit']/text[@language = $ui-lang]
    let $nameGroup                      := $utilhtml:docHtmlNames[@key = 'group']/text[@language = $ui-lang]
    let $nameItem                       := $utilhtml:docHtmlNames[@key = 'item']/text[@language = $ui-lang]
    let $nameCurrency                   := $utilhtml:docHtmlNames[@key = 'currency']/text[@language = $ui-lang]
    let $nameDefaultValue               := $utilhtml:docHtmlNames[@key = 'defaultValue']/text[@language = $ui-lang]
    let $nameFixedValue                 := $utilhtml:docHtmlNames[@key = 'fixedValue']/text[@language = $ui-lang]
    let $nameOrWord                     := $utilhtml:docHtmlNames[@key = 'orWord']/text[@language = $ui-lang]
    let $nameElse                       := $utilhtml:docHtmlNames[@key = 'else']/text[@language = $ui-lang]
    let $msgUndefined                   := $utilhtml:docHtmlNames[@key = 'undefined']/text[@language = $ui-lang]
    
    let $decortypes                     := utillib:getDecorTypes()
    let $decorDataSetValueType          := 
    map:merge(
        for $node in $decortypes//DataSetValueType/enumeration
        return map:entry($node/@value,
            if ($node/label[@language = $ui-lang]) 
            then string-join($node/label[@language = $ui-lang]/string(), $nameOrWord)
            else string-join($node/label[@language = 'en-US']/string(), $nameOrWord)
        )
    )
    let $decorCodingStrength            :=
    map:merge(
        for $node in $decortypes//CodingStrengthType/enumeration
        return map:entry($node/@value,
            if ($node/label[@language = $ui-lang]) 
            then string-join($node/label[@language = $ui-lang]/string(), $nameOrWord)
            else string-join($node/label[@language = 'en-US']/string(), $nameOrWord)
        )
    )
    
    let $decorDataSetTimeStampPrecision := $decortypes//DataSetTimeStampPrecision/enumeration | $decortypes//DataSetTimePrecision/enumeration
    
    (: assemble termlist from terminologyAssociations on the concept, and conceptList(s) :)
    let $termList                       := 
        for $item in $concept/terminologyAssociation[@conceptId = $concept/@id][@code][@codeSystem]
        let $codeSystemName             := if ($item/@codeSystemName) then $item/@codeSystemName else utillib:getNameForOID($item/@codeSystem, $language, ())
        let $codeSystemName             := if (string-length($codeSystemName)=0) then $item/@codeSystem else $codeSystemName
        (: Quite often the displayName is not present on terminologyAssociation, but is there in $concept/valueSet/concept
        If it is, we pick it up there to avoid hitting the expensive adterm:getConceptForLanguages :)
        let $displayName                := $item/@displayName
        (:let $displayName    := if (empty($displayName)) then (local:getDisplayNameFromCodesystem($item/@code, $item/@codeSystem, $language)) else $displayName:)
        let $displayName                := if (empty($displayName)) then adterm:getConceptForLanguages($item/@code, $item/@codeSystem, $language, (), (), ())//designation/text() else $displayName
        
        let $deeplinktocode             := $utilhtml:strAD3ServerURL || $projectPrefix || '/terminology/browser/' || $item/@codeSystem || '/concept/' || encode-for-uri($item/@code)
        
            
        (: Don't cast displayName with $displayName/string(), since it already may be as string :)
        let $name                       := if ($displayName) then xs:string($displayName) else $item/@code/string()
        let $cidlevel                   := ($concept/@level, 0)[1]
        let $cidtype                    := ($item/@type, 'L')[1]
        let $css-margin                 := concat('margin-left: ', (0 + number($cidlevel)) * 15,'px;')
        return 
        <div class="concepttype{$cidtype}" style="{$css-margin}">
        {
            if ($deeplinktocode) then (<a href="{$deeplinktocode}">{$name}</a>, concat(' (',$codeSystemName,': ', $item/@code, ')'))
            else concat($name, ' (',$codeSystemName,': ', $item/@code, ')')
        }
        </div>
    let $termList                       := 
    (
        $termList |
        <ul>
        {
            for $cid at $i in $concept/valueDomain/conceptList/concept/@id
            for $item in $concept/terminologyAssociation[@conceptId = $cid][@code][@codeSystem]
            let $codeSystemName         := if ($item/@codeSystemName) then $item/@codeSystemName else utillib:getNameForOID($item/@codeSystem, $language, ())
            let $codeSystemName         := if (string-length($codeSystemName)=0) then $item/@codeSystem else $codeSystemName
            (: Quite often the displayName is not present on terminologyAssociation, but is there in $concept/valueSet/concept
            If it is, we pick it up there to avoid hitting the expensive adterm:getConceptForLanguages :)
            let $displayName            := $item/@displayName
            (:let $displayName          := if (empty($displayName)) then (local:getDisplayNameFromCodesystem($item/@code, $item/@codeSystem, $language)) else $displayName:)
            let $displayName            := if (empty($displayName)) then adterm:getConceptForLanguages($item/@code, $item/@codeSystem, $language, (), (), ())//designation/text() else $displayName
            let $deeplinktocode             := $utilhtml:strAD3ServerURL || $projectPrefix || '/terminology/browser/' || $item/@codeSystem || '/concept/' || encode-for-uri($item/@code)
            
            (: Don't cast displayName with $displayName/string(), since it already may be as string :)
            let $name                   := if ($displayName) then xs:string($displayName) else $item/@code/string()
            let $cidlevel               := ($cid/../@level, 0)[1]
            let $cidtype                := ($item/@type, 'L')[1]
            let $css-margin             := concat('margin-left: ', (0 + number($cidlevel)) * 15,'px;')
            return 
            <li class="concepttype{$cidtype}" style="{$css-margin}">
            {
                if ($deeplinktocode) then (<a href="{$deeplinktocode}" target="_blank">{$name}</a>, concat(' (',$codeSystemName,': ', $item/@code, ')'))
                else concat($name, ' (',$codeSystemName,': ', $item/@code, ')')
            }
            </li>
        }
        </ul>
     )
     
     let $valueSetList                  :=
        for $valueSet in $concept/valueSet[@id]
        let $vsid                       := $valueSet/@id/string()
        let $vsnm                       := $valueSet/@name/string()
        let $vsef                       := $valueSet/@effectiveDate/string()
        let $vsdn                       := if ($valueSet/@displayName) then ($valueSet/@displayName/string()) else ($valueSet/@name/string())
        let $tatype                     := if ($valueSet/terminologyAssociation[@flexibility castable as xs:dateTime]) then $nameStatic else $nameDynamic
        let $theStrength                := ($valueSet/terminologyAssociation/@strength[not(.='')])[1]
        let $tastrength                 := if ($theStrength) then map:get($decorCodingStrength, $theStrength) else ()
        let $tastrength                 := ($tastrength, $theStrength)[1]
        let $vshref                     := $utilhtml:strServerURLArtApi || '/valueset/' || $vsid || '/' || encode-for-uri($vsef) || '/$view?project=' || $projectPrefix || '&amp;release=' || $version || '&amp;language=' || $language 
        return
            <div>
                <a href="{$vshref}">{$vsdn}</a>{concat(' ', $vsef,' (', string-join(($tatype, $tastrength), ' / '),')')}
            </div>
    (: assemble codelist :)
    let $codeList                       := 
        if ($concept/valueSet/conceptList/concept) then (
            let $vscnt                  := count($concept/valueSet) >= 1
            
            for $valueSet in $concept/valueSet
            let $vsid                   := $valueSet/@id/string()
            let $vsnm                   := $valueSet/@name/string()
            let $vsef                   := $valueSet/@effectiveDate/string()
            let $vsdn                   := if ($valueSet/@displayName) then ($valueSet/@displayName/string()) else ($valueSet/@name/string())
            let $tatype                 := if ($valueSet/terminologyAssociation[@flexibility castable as xs:dateTime]) then $nameStatic else $nameDynamic
            let $list-style             := if ($vscnt) then ('margin-bottom: 10px;') else ()
            let $list-style             := if ($valueSet[following-sibling::valueSet]) then ('margin-bottom: 10px;') else ()
            let $list-title             := if ($vscnt) then if ($valueSet/@displayName) then ($valueSet/@displayName) else ($valueSet/@name) else ()
            let $list-effdt             := if ($vscnt) then if ($valueSet/@effectiveDate) then ($valueSet/@effectiveDate) else () else ()
            let $vshref                 := $utilhtml:strServerURLArtApi || '/valueset/' || $vsid || '/' || encode-for-uri($vsef) || '/$view?project=' || $projectPrefix || '&amp;release=' || $version || '&amp;language=' || $language
            return (
            <div style="font-style: italic;">
                <a href="{$vshref}">
                {
                    if (string-length($list-title)>0) then concat($nameValueSet, ': ', $list-title) else (),
                    if (string-length($list-effdt)>0) then concat(' (', $list-effdt, ')') else ()
                }
                </a>
            </div>
            ,
            <ul style="{$list-style}" title="{$list-title}">
            {
                for $item in $valueSet/conceptList/*
                let $name               := if ($item/name[@language=$language]) then $item/name[@language=$language] else $item/name[1]
                let $synonym            := if ($item/synonym[string-length(.)>0]) then concat(' (', string-join($item/synonym, ', '), ')') else ()
                let $ordinal            := if ($item/@ordinal[string-length(.)>0]) then concat('(', string-join($item/@ordinal, ', '), ') :') else ()
                let $css-margin         := concat('margin-left: ',$item/number(@level) * 15,'px;')
                return
                <li class="concepttype{$item/@type}" style="{$css-margin}">{$ordinal, $name/string(), $synonym}</li>
            }
            {
                for $item in $valueSet/completeCodeSystem | $valueSet/conceptList/include[@codeSystem][not(@code)]
                let $csid               := $item/@codeSystem
                return 
                <li class="concepttype{$item/@type}">{concat(if ($item[filter]) then $columnCodeSystemWithFilters else $columnCodeSystem,': ', if ($item/@codeSystemName/string()) then $item/@codeSystemName/string() else utillib:getNameForOID($csid,$language,''))}</li>
            }
            </ul>
            )
        ) else 
        if ($concept/valueDomain/conceptList/concept) then (
            <ul>
            {
                for $item in $concept/valueDomain/conceptList/concept
                let $name               := if ($item/name[@language=$language]) then $item/name[@language=$language] else $item/name[1]
                let $synonym            := if ($item/synonym) then concat(' (', string-join($item/synonym, ', '), ')') else ()
                let $ordinal            := if ($item/@ordinal[string-length(.)>0]) then concat('(', string-join($item/@ordinal, ', '), ') :') else ()
                let $css-margin         := concat('margin-left: ',$item/number(@level) * 15,'px;')
                return 
                <li class="concepttype{$item/@type}" style="{$css-margin}">{$ordinal, $name/string(), $synonym}</li>
            }
            </ul>
        ) else ()
    (: assemble the data content for table details :)
    let $cardinality    := 
        if ($isTransaction) then (
            let $min                    := utillib:getMinimumMultiplicity($concept)
            let $max                    := utillib:getMaximumMultiplicity($concept)
            
            return
                string-join((if ($min = '*' or $min castable as xs:integer) then $min else ('?'), '…', if ($max = '*' or $max castable as xs:integer) then $max else ('?')), ' ')
        ) else ()
    let $conformance                    := $concept/@conformance/string()
    
    let $dtkey                          := ($concept/valueDomain/@type)[1]
    let $datatype                       := if (empty($dtkey)) then () else map:get($decorDataSetValueType, $dtkey)
        
    let $tr                             :=
            <tr class="bg-{$concept/@type}" data-tt-id="{$rowId}" id="{$rowId}">
                {if (empty($parentId)) then () else attribute data-tt-parent-id {$parentId}}
                <td class="columnName node-s{$concept/@statusCode}">
                {   
                    if ($download) then for $i in (1 to count($concept/ancestor::concept)) return '&#160;&#160;&#160;&#160;' else (),
                    $concept/name[@language=$language][1]/string(), if ($concept/synonym) then concat(' (', string-join($concept/synonym[@language=$language], ', '), ')') else ()
                }
                </td>
                <td class="columnID" style="{if (map:get($columnmap, 'columnID')) then ('display:none;') else ()}">
                {
                    if ($concept/@iddisplay) then data($concept/@iddisplay) else if ($concept/@refdisplay) then data($concept/@refdisplay) else data($concept/(@id|@ref))
                }
                </td>
                {
                    if ($isTransaction) then (
                    <td class="columnMandatory" style="{if (map:get($columnmap, 'columnMandatory')) then ('display:none;') else ()}">
                    {
                        if ($concept[@isMandatory="true"]) then "+" else "-"
                    }
                    </td>
                    ,
                    <td class="columnConformance" style="{if (map:get($columnmap, 'columnConformance')) then ('display:none;') else ()}">
                    {
                        $conformance
                    }
                    </td>
                    ,
                    <td class="columnCardinality" style="{if (map:get($columnmap, 'columnCardinality')) then ('display:none;') else ()}">
                    {
                        $cardinality
                    }
                    </td>
                    ,
                    <td class="columnMax" style="{if (map:get($columnmap, 'columnMax')) then ('display:none;') else ()}">
                    {
                        if ($concept/@conformance='NP') then () else replace($concept/@maximumMultiplicity, '\*', 'n')
                    }
                    </td>
                    ,
                    (: Single column with: 0..1 R Datetime etc. :)
                    <td class="columnCCD" style="{if (map:get($columnmap, 'columnCCD')) then ('display:none;') else ()}">
                    {
                        $cardinality, $conformance, ' ', <a href="https://docs.art-decor.org/introduction/dataset/#{$dtkey}">{$datatype}</a>
                    }
                    </td>
                    ) else ()
                }
                <td class="columnDatatype" style="{if (map:get($columnmap, 'columnDatatype')) then ('display:none;') else ()}">
                    <a href="https://docs.art-decor.org/introduction/dataset/#{$dtkey}">{$datatype}</a>
                </td>
                <td class="columnProperty" style="{if (map:get($columnmap, 'columnProperty')) then ('display:none;') else ()}">
                {
                    (:<property currency="" default="" fixed="" fractionDigits="" minInclude="" maxInclude="" minLength="" maxLength="" timeStampPrecision="" unit=""/>:)
                    for $property in $concept/valueDomain/property
                    return
                        <ul style="margin-bottom: 10px;">
                        {
                            if ($property/@minLength | $property/@maxLength) then
                                <li>{concat($nameLength,': ',$property/@minLength,'…',$property/@maxLength)}</li>
                            else ()
                        }
                        {
                            if ($property/@minInclude | $property/@maxInclude) then
                                <li>{concat($nameRange,': ',$property/@minInclude,'…',$property/@maxInclude)}</li>
                            else ()
                        }
                        {
                            if ($property/@fractionDigits) then
                                <li>{concat($nameFractionDigits,': ',$property/@fractionDigits)}</li>
                            else ()
                        }
                        {
                            if ($property/@default) then
                                <li>{concat($nameDefaultValue,': ',$property/@default)}</li>
                            else ()
                        }
                        {
                            if ($property/@fixed) then
                                <li>{concat($nameFixedValue,': ',$property/@fixed)}</li>
                            else ()
                        }
                        {
                            if ($property/@timeStampPrecision) then
                                <li>{concat($nameTimeStampPrecision,': ',$decorDataSetTimeStampPrecision[@value=$property/@timeStampPrecision]/label[@language=$ui-lang]/string())}</li>
                            else ()
                        }
                        {
                            if ($property/@currency) then
                                <li>{concat($nameCurrency,': ',$property/@currency)}</li>
                            else ()
                        }
                        {
                            if ($property/@unit) then
                                <li>{concat($nameUnit,': ',$property/@unit)}</li>
                            else ()
                        }
                        {
                            for $att in $property/(@* except (@currency|@default|@fixed|@fractionDigits|@minInclude|@maxInclude|@minLength|@maxLength|@timeStampPrecision|@unit))
                            return
                                <li>{concat($att/name(),': ',$att/string())}</li>
                        }
                        </ul>
                }
                </td>
                <td class="columnExample" style="{if (map:get($columnmap, 'columnExample')) then ('display:none;') else ()}">
                {
                    string-join($concept/valueDomain/example, '; ')
                }
                </td>
                <td class="columnCodes" style="{if (map:get($columnmap, 'columnCodes')) then ('display:none;') else ()}">
                {
                    $codeList
                }
                </td>
                <td class="columnDescription" style="{if (map:get($columnmap, 'columnDescription')) then ('display:none;') else ()}">
                {
                    (:utillib:parseNode($concept/desc)/node():)
                    $concept/desc[@language=$language][1]/node()
                }
                </td>
                {
                    if ($isTransaction) then (
                    <td class="columnContext" style="{if (map:get($columnmap, 'columnContext')) then ('display:none;') else ()}">
                    {
                        $concept/context[@language=$language][1]/node()
                    }
                    </td>
                    ) else ()
                }
                <td class="columnSource" style="{if (map:get($columnmap, 'columnSource')) then ('display:none;') else ()}">
                {
                    $concept/source[@language=$language][1]/string()
                }
                </td>
                <td class="columnRationale" style="{if (map:get($columnmap, 'columnRationale')) then ('display:none;') else ()}">
                {
                    $concept/rationale[@language=$language][1]/string()
                }
                </td>
                <td class="columnOperationalization" style="{if (map:get($columnmap, 'columnOperationalization')) then ('display:none;') else ()}">
                {
                    $concept/operationalization[@language=$language][1]/string()
                }
                </td>
                <td class="columnComment" style="{if (map:get($columnmap, 'columnComment')) then ('display:none;') else ()}">
                    <ul>
                    {
                        for $comment in $concept/comment[@language = $language] return <li>{$comment/string()}</li>
                    }
                    </ul>
                </td>
                {
                    if ($isTransaction) then (
                    <td class="columnMapping" style="{if (map:get($columnmap, 'columnMapping')) then ('display:none;') else ()}">
                    {
                        (:get mappings from templates in or connected to the current representingTemplate:)
                        if ($templateChain) then (
                            let $decor      := utillib:getDecorByPrefix($projectPrefix)
                            for $templateAssociation in $decor//templateAssociation/concept[@ref=$concept/@id][@effectiveDate=$concept/@effectiveDate]
                            let $tmid       := $templateAssociation/../@templateId
                            let $tmed       := $templateAssociation/../@effectiveDate
                            let $elid       := $templateAssociation/@elementId
                            (:check decor templates first as those are indexed:)
                            let $tm         := $decor//template[@id = $tmid][@effectiveDate = $tmed]
                            (:check templateChain next which is in memory and as such slower:)
                            let $tm         := if ($tm) then $tm else ($templateChain[@id = $tmid][@effectiveDate = $tmed])
                            let $element    := $tm[1]//*[@id = $elid]
                            return (
                                for $e in $element 
                                let $d      := if ($e/desc[@language = $language]) then concat(' (',$e/desc[@language = $language],')') else if ($e[desc]) then concat(' (',$e/desc[1],')') else ()
                                let $cf     := if ($e/@isMandatory='true') then 'M' else ($e/@conformance)
                                let $mc     := concat(utillib:getMinimumMultiplicity($e),' … ',utillib:getMaximumMultiplicity($e),' ',$cf)
                                let $tmhref := $utilhtml:strServerURLArtApi || '/template/' || $tmid || '/' || encode-for-uri($tmed) || '/$view?project=' || $projectPrefix || '&amp;release=' || $version || '&amp;language=' || $language
                                return 
                                    <div><a href="{$tmhref}">{replace($e/@name,'hl7v2:','')}</a>{concat(' - ', $mc, $d)}</div>
                            )
                        ) else ()
                    }
                    </td>
                    ,
                    <td class="columnCondition" style="{if (map:get($columnmap, 'columnCondition')) then ('display:none;') else ()}">
                    {
                        if ($concept/condition) then (
                            <ul>
                            {
                                for $condition in $concept/condition 
                                return
                                if ($condition[desc]) then (
                                <li>{   $condition/@minimumMultiplicity/string(), '…', $condition/@maximumMultiplicity/string(), 
                                        $condition/@conformance/string(), ' ', data($condition/desc[1])}</li>
                                )
                                else
                                if ($condition[position()=last()]) then (
                                <li>{   $condition/@minimumMultiplicity/string(), '…', $condition/@maximumMultiplicity/string(), 
                                        $condition/@conformance/string(), ' ', $nameElse}</li>
                                )
                                else (
                                <li>{   $condition/@minimumMultiplicity/string(), '…', $condition/@maximumMultiplicity/string(), 
                                        $condition/@conformance/string(), ': ', data($condition)}</li>
                                )
                            }
                            </ul>
                        ) else ()
                    }
                    </td>
                    ,
                    <td class="columnEnableWhen" style="{if (map:get($columnmap, 'columnEnableWhen')) then ('display:none;') else ()}">
                    {
                        if ($concept/enableWhen) then (
                            if (count($concept/enableWhen) gt 1) then
                                <div>{$msgUndefined}</div>
                            else ()
                            ,
                            <ul>
                            {
                                for $condition in $concept/enableWhen
                                let $question         := $condition/@question
                                let $theReffedConcept := $concept/../concept[@id = $question]
                                return
                                    <li>
                                    {
                                        '"',
                                        <a href="#{concat('id_', replace($question, '\.', '_'))}" title="{string-join($theReffedConcept/ancestor-or-self::concept/name[1], ' / '), ' - ', $question}">{data(($theReffedConcept/name, $question)[1])}</a>,
                                        '"',
                                        data($condition/@operator),
                                        ' ',
                                        for $dt in $condition/*
                                        let $theCSN   := 
                                                if ($dt/@codeSystemName) then $dt/@codeSystemName else ()
                                                (:if ($dt/@codeSystem) then utillib:getNameForOID($dt/@codeSystem, $language, ()) else $dt/@codeSystem:)
                                        return
                                        switch (name($dt))
                                        case 'answerCoding' return (
                                                data($dt/@code),
                                                ' ',
                                                <span title="{$theCSN}">{data($dt/@codeSystem)}</span>,
                                                if ($dt/@displayName) then ( 
                                                    ' "' || data($dt/@displayName) || '"'
                                                ) else ()
                                                ,
                                                if ($dt/@canonicalUri) then ( 
                                                    ' - FHIR: ', <i>{data($dt/@canonicalUri)}</i>
                                                ) else ()
                                        )
                                        case 'answerQuantity' return (
                                            data($dt/@comparator),
                                            ' ',
                                            data($dt/@value),
                                            ' ',
                                            data($dt/@unit),
                                            if ($dt/@code) then (
                                                ' (',
                                                data($dt/@code),
                                                ' ',
                                                <span title="{$theCSN}">{data($dt/@codeSystem)}</span>,
                                                if ($dt/@canonicalUri) then ( 
                                                    ' - FHIR: ', <i>{data($dt/@canonicalUri)}</i>
                                                ) else ()
                                                ,
                                                ')'
                                            ) else ()
                                        )
                                        default return data($dt/@value)
                                    }
                                    </li>
                            }
                            </ul>
                        ) else ()
                    }
                    </td>
                    ) else ()
                }
                <td class="columnStatus" style="{if (map:get($columnmap, 'columnStatus')) then ('display:none;') else ()}">
                {
                    $concept/@statusCode/string()
                }
                </td>
                {
                    if ($docommunity) then (
                        <td class="columnCommunity" style="{if (map:get($columnmap, 'columnCommunity')) then ('display:none;') else ()}">
                        {
                            for $commInfo in $concept/community
                            return (
                                <div><b>{data($commInfo/@name)}</b></div>,
                                <ul title="{$commInfo/@name}">
                                {
                                    for $association in $commInfo/data
                                    return
                                        <li>
                                            <span title="{$association/@label}" style="padding: inherit;">{data($association/@type)}</span>: {$association/node()}
                                        </li>
                                }
                                </ul>
                            )
                        }
                        </td>
                    ) else ()
                }
                <td class="columnTerminology" style="{if (map:get($columnmap, 'columnTerminology')) then ('display:none;') else ()}">
                {
                    $termList
                }
                </td>
                <td class="columnValueSet" style="{if (map:get($columnmap, 'columnValueSet')) then ('display:none;') else ()}">
                {
                    $valueSetList
                }
                </td>
                <td class="columnType" style="{if (map:get($columnmap, 'columnType')) then ('display:none;') else ()}">
                {
                    if ($concept[@type = 'group']) then $nameGroup else $nameItem
                }
                </td>
                <td class="columnParent" style="{if (map:get($columnmap, 'columnParent')) then ('display:none;') else ()}">
                {
                    $concept/parent::concept/name[@language=$language][1]/string()
                }
                </td>
                <td class="columnInherit" style="{if (map:get($columnmap, 'columnInherit')) then ('display:none;') else ()}">
                {   
                    if ($concept/inherit or  $concept/contains) then 
                    (
                        let $conceptId      := ($concept/inherit/@ref | $concept/contains/@ref)[1]
                        let $conceptEd      := ($concept/inherit/@effectiveDate | $concept/contains/@flexibility)[1]
                        let $concepthref    := $utilhtml:strServerURLArtApi || '/concept/' || $conceptId || '/' || encode-for-uri($conceptEd) || '/$view?project=' || $projectPrefix || '&amp;release=' || $version || '&amp;language=' || $language
                        let $conceptDisplay :=
                            if ($concept/inherit/@refdisplay) then 
                                $concept/inherit/@refdisplay/string()
                            else
                            if ($concept/contains/@refdisplay) then 
                                $concept/contains/@refdisplay/string()
                            else
                            if ($concept/inherit/@ref) then 
                                utillib:getNameForOID($concept/inherit/@ref,$language,$projectPrefix)
                            else
                            if ($concept/contains/@ref) then 
                                utillib:getNameForOID($concept/contains/@ref,$language,$projectPrefix)
                            else ()
                      return 
                        <a href="{$concepthref}">{$conceptDisplay}</a>
                    )
                    else ()
                }
                    
                </td>
            </tr>
    
    return ($tr, 
        let $children := 
            if ($showonlyactiveconcepts) then (
                for $concept in $concept/concept 
                return if ($concept[@statusCode = $utilhtml:STATUSCODES-INACTIVE]) then () else ($concept)
            )
            else $concept/concept
        for $conceptChild in $children
        return local:getConceptRows($conceptChild, $rowId, $language, $ui-lang, $columnmap, $showonlyactiveconcepts, $docommunity, $isTransaction, $version, $projectPrefix, $templateChain, $download)) 
};

declare %private function local:getSimpleConceptRows(
    $concept as element(), 
    $language as xs:string,
    $ui-lang as xs:string,
    $projectPrefix as xs:string,
    $columnmap as item()?, 
    $showonlyactiveconcepts as xs:boolean,
    $level as xs:integer,
    $isTransaction as xs:boolean
    ) as element()* {
    
    let $columnCodeSystemWithFilters    := $utilhtml:docHtmlNames[@key = 'columnCodeSystemWithFilters']/text[@language = $ui-lang]
    let $columnCodeSystem               := $utilhtml:docHtmlNames[@key = 'columnCodeSystem']/text[@language = $ui-lang]
    let $nameOrWord                     := $utilhtml:docHtmlNames[@key = 'orWord']/text[@language = $ui-lang]
    let $nameElse                       := $utilhtml:docHtmlNames[@key = 'else']/text[@language = $ui-lang]
    
    let $decortypes                     := utillib:getDecorTypes()
    let $decorDataSetValueType          := 
    map:merge(
        for $node in $decortypes//DataSetValueType/enumeration
        return map:entry($node/@value,
            if ($node/label[@language = $ui-lang]) 
            then string-join($node/label[@language = $ui-lang]/string(), $nameOrWord)
            else string-join($node/label[@language = 'en-US']/string(), $nameOrWord)
        )
    )
    
    (: assemble codelist :)
    let $codeList := 
        if ($concept/valueSet/conceptList/concept) then (
            <ul class="ad-terminology-code">
            {
            for $item in $concept/valueSet/conceptList/*
            return <li>{$item/name[1]/string()}</li>
            }
            {
            for $item in $concept/valueSet/completeCodeSystem | $concept/valueSet/conceptList/include[@codeSystem][not(@code)]
            let $csid := $item/@codeSystem
            return 
            <li>{concat(if ($item[filter]) then $columnCodeSystemWithFilters else $columnCodeSystem, ': ', if ($item/@codeSystemName/string()) then $item/@codeSystemName/string() else utillib:getNameForOID($csid,$language,''))}</li>
            }
            </ul>
        ) else if ($concept/valueDomain/conceptList/concept) then (
            <ul class="ad-terminology-code">
            {
            for $item in $concept/valueDomain/conceptList/concept
            return 
            <li>
            {
                if ($item/@type='D') then (attribute {'style'} {'text-decoration: line-through; font-style: italic; opacity: 0.5;'}) else (),
                $item/name[1]/string()
            }
            </li>
            }
            </ul>
        ) else ()
    let $condition := 
        if ($concept/condition) then (
            <ul class="ad-transaction-condition">
            {
                for $condition at $x in $concept/condition 
                return
                    <li class="ad-transaction-condition"> 
                    {
                        if ($condition[$x=count($concept/condition)]) then
                            concat($condition/@minimumMultiplicity, '..', $condition/@maximumMultiplicity, ' ', $condition/@conformance, ' ', $nameElse)
                        else (
                            concat($condition/@minimumMultiplicity, '..', $condition/@maximumMultiplicity, ' ', $condition/@conformance, ': ', $condition)
                        )
                    }
                    </li>
            }
            </ul>
        ) else ()
    
    let $children       := 
        if ($showonlyactiveconcepts) then (
            for $c in $concept/concept 
            return if ($c[@statusCode = $utilhtml:STATUSCODES-INACTIVE]) then () else ($c)
        )
        else $concept/concept
    
    let $dtkey          := ($concept/valueDomain/@type)[1]
    let $datatype       := if (empty($dtkey)) then () else map:get($decorDataSetValueType, $dtkey)
    
    let $ci             := 
        concat(($concept/name[@language=$language])[1]/string(), 
            if ($concept/synonym) then concat(' (', string-join($concept/synonym, ', '), ')') else(), ' ',
            if ($concept/@type='group') then '' else concat(' (', $datatype, ') '),
            if ($isTransaction)
            then (
                if ($concept/@conformance='NP') 
                then 'NP' 
                else concat($concept/@minimumMultiplicity/string(), '..', $concept[1]/@maximumMultiplicity/string(), ' ')
            ) else '', 
            $concept/@conformance/string())
            
    let $statusColor := 
        if ($concept/@statusCode = $utilhtml:STATUSCODES-INACTIVE)
        then 'ad-itemnumber-blue'
        else if ($concept/@statusCode = $utilhtml:STATUSCODES-DRAFT)
        then 'ad-itemnumber-yellow'
        else 'ad-itemnumber-green'
    
    let $cdesc    := if ($concept/desc[@language = $language]) then $concept/desc[@language = $language] else if ($concept[desc]) then ($concept/desc)[1] else ()
    let $ccontext := if ($concept/context[@language = $language]) then $concept/context[@language = $language] else if ($concept[context]) then ($concept/context)[1] else ()
    
    let $decor    := utillib:getDecorByPrefix($projectPrefix)
    let $mappings :=
        if (map:get($columnmap, 'columnMapping')) then () else
            for $templateAssociation in $decor//templateAssociation/concept[@ref=$concept/@id][@effectiveDate=$concept/@effectiveDate]
            let $tmid       := $templateAssociation/../@templateId
            let $tmed       := $templateAssociation/../@effectiveDate
            (:check decor templates first as those are indexed:)
            let $tm         := $decor//template[@id=$tmid][@effectiveDate=$tmed]
            (: get the template by ref if nothing else found so far :)
            let $tm         := if ($tm) then $tm else tmapi:getTemplateById($tmid, $tmed)
            let $element    := $tm[1]//*[@id=$templateAssociation/@elementId]
            return (
                for $e in $element 
                let $d  := if ($e/desc[@language = $language]) then concat(' (',$e/desc[@language = $language],')') else if ($e[desc]) then concat(' (',($e/desc)[1],')') else ()
                let $cf := if ($e/@isMandatory='true') then 'M' else ($e/@conformance)
                let $mc := concat(utillib:getMinimumMultiplicity($e),' … ',utillib:getMaximumMultiplicity($e),' ',$cf)
                return
                    <li>
                    {
                        <i>{concat('&#x21a6; ', string($tm/@displayName))}</i>,
                        <div class="nowrapinline ad-templatetype {string(($tm[1]//classification/@type)[1])}">{string($tmid)}</div>,
                        concat(' ', replace($e/@name,'hl7v2:',''),' - ', $mc, $d)
                    }
                    </li>
                )
    let $maps := <ul class="tmap">{$mappings}</ul>
    
    let $html := 
        if ($level = 1) 
        then (
            <div class="ad-dataset-level1">{$ci}
              <div class="{concat('ad-dataset-itemnumber ', $statusColor)}">{tokenize($concept/@id,'\.')[last()]}</div>
              {if (map:get($columnmap, 'columnDescription')) then () else <div class="description"><i>{$cdesc/node()}</i></div>}
              {if ($isTransaction) then (if (map:get($columnmap, 'columnContext')) then () else <div class="context"><i>{$ccontext/node()}</i></div>) else ()}
              {if (empty($mappings)) then () else $maps}
            </div>,
            $condition,
            $codeList,
            for $conceptChild in $children
            return local:getSimpleConceptRows($conceptChild, $language, $ui-lang, $projectPrefix, $columnmap, $showonlyactiveconcepts, $level+1, $isTransaction)
        )
        else (
            <ol class="ad-dataset-group">
                <li class="{if ($concept/@type='group') then 'ad-dataset-group' else 'ad-dataset-item'}">
                {
                    $ci
                } 
                <div class="ad-dataset-itemnumber {$statusColor}">{tokenize($concept/@id,'\.')[last()]}</div>
                {
                    if (map:get($columnmap, 'columnDescription')) then () else <div class="description"><i>{$cdesc/node()}</i></div>
                }
                {
                    if ($isTransaction) then (if (map:get($columnmap, 'columnContext')) then () else <div class="context"><i>{$ccontext/node()}</i></div>) else ()
                }
                {
                    if (empty($mappings)) then () else $maps
                }
                {
                    $condition
                }
                {
                    $codeList
                }
                {
                    for $conceptChild in $children
                    return local:getSimpleConceptRows($conceptChild, $language, $ui-lang, $projectPrefix, $columnmap, $showonlyactiveconcepts, $level+1, $isTransaction)
                }
                </li>
            </ol>
        )
    return
        $html
    
};

declare %private function local:getLogo($projectPrefix as xs:string?) as xs:string {

    let $logo                       := 
        if ($projectPrefix) then $utilhtml:strServerURLArtApi || '/project/' || $projectPrefix || '/logo' 
        else if (starts-with($utilhtml:serverLogo, 'http')) then $utilhtml:serverLogo else '/art-decor/img/' || $utilhtml:serverLogo
       
    return $logo  
        

};


declare %private function local:getHtmlNames() as element(entry)* {
    let $names                      :=
    <names>
        <entry key="columnName">
            <text language="en-US">Name</text>
            <text language="de-DE">Name</text>
            <text language="nl-NL">Naam</text>
        </entry>
        <entry key="columnID">
            <text language="en-US">ID</text>
            <text language="de-DE">ID</text>
            <text language="nl-NL">ID</text>
        </entry>
        <entry key="columnLiveVersion">
            <text language="en-US">Live Version</text>
            <text language="de-DE">Live Version</text>
            <text language="nl-NL">Live versie</text>
        </entry>
        <entry key="columnVersionLabel">
            <text language="en-US">Version Label</text>
            <text language="de-DE">Versions-Label</text>
            <text language="nl-NL">Versielabel</text>
        </entry>
        <entry key="columnMandatory">
            <text language="en-US">Mandatory</text>
            <text language="de-DE">Mandatory</text>
            <text language="nl-NL">Mandatory</text>
        </entry>
        <entry key="columnConformance">
            <text language="en-US">Conf</text>
            <text language="de-DE">Conf</text>
            <text language="nl-NL">Conf</text>
        </entry>
        <entry key="columnCardinality">
            <text language="en-US">Card</text>
            <text language="de-DE">Kard</text>
            <text language="nl-NL">Card</text>
        </entry>
        <entry key="columnMax">
            <text language="en-US">Max</text>
            <text language="de-DE">Max</text>
            <text language="nl-NL">Max</text>
        </entry>
        <entry key="columnCCD">
            <text language="en-US">Datatype CC</text>
            <text language="de-DE">Datentyp CC</text>
            <text language="nl-NL">Datatype CC</text>
        </entry>
        <entry key="columnDatatype">
            <text language="en-US">Datatype</text>
            <text language="de-DE">Datentyp</text>
            <text language="nl-NL">Datatype</text>
        </entry>
        <entry key="columnProperty">
            <text language="en-US">Property</text>
            <text language="de-DE">Eigenschaft</text>
            <text language="nl-NL">Eigenschap</text>
        </entry>
        <entry key="columnExample">
            <text language="en-US">Example</text>
            <text language="de-DE">Beispiel</text>
            <text language="nl-NL">Voorbeeld</text>
        </entry>
        <entry key="columnCodes">
            <text language="en-US">Codes</text>
            <text language="de-DE">Codes</text>
            <text language="nl-NL">Codes</text>
        </entry>
        <entry key="columnDescription">
            <text language="en-US">Description</text>
            <text language="de-DE">Beschreibung</text>
            <text language="nl-NL">Omschrijving</text>
        </entry>
        <entry key="columnContext">
            <text language="en-US">Context</text>
            <text language="de-DE">Kontext</text>
            <text language="nl-NL">Context</text>
        </entry>
        <entry key="columnOperationalization">
            <text language="en-US">Operationalizations</text>
            <text language="de-DE">Operationalisierungen</text>
            <text language="nl-NL">Operationalisaties</text>
        </entry>
        <entry key="columnRationale">
            <text language="en-US">Rationale</text>
            <text language="de-DE">Rationale</text>
            <text language="nl-NL">Rationale</text>
        </entry>
        <entry key="columnSource">
            <text language="en-US">Source</text>
            <text language="de-DE">Quelle</text>
            <text language="nl-NL">Bron</text>
        </entry>
        <entry key="columnComment">
            <text language="en-US">Comment</text>
            <text language="de-DE">Kommentar</text>
            <text language="nl-NL">Opmerking</text>
        </entry>
        <entry key="columnCondition">
            <text language="en-US">Condition</text>
            <text language="de-DE">Condition</text>
            <text language="nl-NL">Conditie</text>
        </entry>
        <entry key="columnEnableWhen">
            <text language="en-US">Enable when</text>
            <text language="de-DE">Aktiviert wann</text>
            <text language="nl-NL">Actief als</text>
        </entry>
        <entry key="columnMapping">
            <text language="en-US">Mapping</text>
            <text language="de-DE">Mapping</text>
            <text language="nl-NL">Mapping</text>
        </entry>
        <entry key="columnStatus">
            <text language="en-US">Status</text>
            <text language="de-DE">Status</text>
            <text language="nl-NL">Status</text>
        </entry>
        <entry key="columnCommunity">
            <text language="en-US">Community</text>
            <text language="de-DE">Community</text>
            <text language="nl-NL">Community</text>
        </entry>
        <entry key="columnTerminology">
            <text language="en-US">Terminology</text>
            <text language="de-DE">Terminologie</text>
            <text language="nl-NL">Terminologie</text>
        </entry>
        <entry key="columnValueSet">
            <text language="en-US">Value Set</text>
            <text language="de-DE">Value Set</text>
            <text language="nl-NL">Waardelijst</text>
        </entry>
        <entry key="columnType">
            <text language="en-US">Type</text>
            <text language="de-DE">Type</text>
            <text language="nl-NL">Type</text>
        </entry>
        <entry key="columnParent">
            <text language="en-US">Parent concept</text>
            <text language="de-DE">Elternkonzept</text>
            <text language="nl-NL">Ouderconcept</text>
        </entry>
        <entry key="columnInherit">
            <text language="en-US">Inherit from</text>
            <text language="de-DE">Erbt von</text>
            <text language="nl-NL">Erft van</text>
        </entry>
        <entry key="columnCodeSystem">
            <text language="en-US">Code System</text>
            <text language="de-DE">Codesystem</text>
            <text language="nl-NL">Codesysteem</text>
        </entry>
        <entry key="columnCodeSystemWithFilters">
            <text language="en-US">Code System with Filters</text>
            <text language="de-DE">Codesystem met Filters</text>
            <text language="nl-NL">Codesysteem met filters</text>
        </entry>
        <entry key="dynamic">
            <text language="en-US">dynamic</text>
            <text language="de-DE">dynamisch</text>
            <text language="nl-NL">dynamisch</text>
        </entry>
        <entry key="static">
            <text language="en-US">static</text>
            <text language="de-DE">statisch</text>
            <text language="nl-NL">statisch</text>
        </entry>
        <entry key="length">
            <text language="en-US">Length</text>
            <text language="nl-NL">Lengte</text>
            <text language="de-DE">Länge</text>
        </entry>
        <entry key="range">
            <text language="en-US">Min/max</text>
            <text language="nl-NL">Min/max</text>
            <text language="de-DE">Min/max</text>
        </entry>
        <entry key="fractionDigits">
            <text language="en-US">Decimals</text>
            <text language="nl-NL">Decimalen</text>
            <text language="de-DE">Ziffern</text>
        </entry>
        <entry key="defaultValue">
            <text language="en-US">Default value</text>
            <text language="nl-NL">Standaardwaarde</text>
            <text language="de-DE">Standard Wert</text>
        </entry>
        <entry key="fixedValue">
            <text language="en-US">Fixed value</text>
            <text language="nl-NL">Vaste waarde</text>
            <text language="de-DE">Fixen Wert</text>
        </entry>
        <entry key="currency">
            <text language="en-US">Currency</text>
            <text language="nl-NL">Valuta</text>
            <text language="de-DE">Währung</text>
        </entry>
        <entry key="timeStampPrecision">
            <text language="en-US">Timestamp precision</text>
            <text language="nl-NL">Tijdstempelprecisie</text>
            <text language="de-DE">Genauigkeit Zeitangabe</text>
        </entry>
        <entry key="unit">
            <text language="en-US">Unit</text>
            <text language="nl-NL">Eenheid</text>
            <text language="de-DE">Einheit</text>
        </entry>
        <entry key="group">
            <text language="en-US">Group</text>
            <text language="de-DE">Gruppe</text>
            <text language="nl-NL">Groep</text>
        </entry>
        <entry key="item">
            <text language="en-US">Item</text>
            <text language="de-DE">Item</text>
            <text language="nl-NL">Item</text>
        </entry>
        <entry key="groups">
            <text language="en-US">Groups</text>
            <text language="de-DE">Gruppen</text>
            <text language="nl-NL">Groepen</text>
        </entry>
        <entry key="items">
            <text language="en-US">Items</text>
            <text language="de-DE">Items</text>
            <text language="nl-NL">Items</text>
        </entry>
        <entry key="ValueSet">
            <text language="en-US">Value Set</text>
            <text language="de-DE">Value Set</text>
            <text language="nl-NL">Waardelijst</text>
        </entry>
        <entry key="ValueSets">
            <text language="en-US">Value Sets</text>
            <text language="de-DE">Value Sets</text>
            <text language="nl-NL">Waardelijsten</text>
        </entry>
        <entry key="CodeSystem">
            <text language="en-US">Code System</text>
            <text language="de-DE">Code System</text>
            <text language="nl-NL">Codesysteem</text>
        </entry>
        <entry key="CodeSystems">
            <text language="en-US">Code Systems</text>
            <text language="de-DE">Code Systeme</text>
            <text language="nl-NL">Codesystemen</text>
        </entry>
        <entry key="Template">
            <text language="en-US">Template</text>
            <text language="de-DE">Template</text>
            <text language="nl-NL">Template</text>
        </entry>
        <entry key="Templates">
            <text language="en-US">Templates</text>
            <text language="de-DE">Templates</text>
            <text language="nl-NL">Templates</text>
        </entry>
            <entry key="ConceptMap">
            <text language="en-US">Concept Map</text>
            <text language="de-DE">Concept Map</text>
            <text language="nl-NL">Conceptmap</text>
        </entry>
        <entry key="ConceptMaps">
            <text language="en-US">Concept Maps</text>
            <text language="de-DE">Concept Maps</text>
            <text language="nl-NL">Conceptmaps</text>
        </entry>   
        <entry key="orWord">
            <text language="en-US">or</text>
            <text language="de-DE">oder</text>
            <text language="nl-NL">of</text>
        </entry>
        <entry key="else">
            <text language="en-US">Else</text>
            <text language="de-DE">Sonst</text>
            <text language="nl-NL">Anders</text>
         </entry>
         <entry key="undefined">
             <text language="en-US">All/any undefined</text>
             <text language="de-DE">Alles/irgendein undefiniert</text>
             <text language="nl-NL">Alles/tenminste één niet gedefinieerd</text>
         </entry>
         <entry key="dataset">
            <text language="en-US">Data Set</text>
            <text language="de-DE">Datensatz</text>
            <text language="nl-NL">Dataset</text>
         </entry>
         <entry key="concept">
             <text language="en-US">Concept</text>
             <text language="de-DE">Konzept</text>
             <text language="nl-NL">Concept</text>
         </entry>
         <entry key="concepts">
             <text language="en-US">Concepts</text>
             <text language="de-DE">Konzepte</text>
             <text language="nl-NL">Concepten</text>
         </entry>
         <entry key="scenario">
             <text language="en-US">Scenario</text>
             <text language="de-DE">Szenario</text>
             <text language="nl-NL">Scenario</text>
         </entry>
         <entry key="transaction">
             <text language="en-US">Transaction</text>
             <text language="de-DE">Transaktion</text>
             <text language="nl-NL">Transactie</text>
         </entry>
         <entry key="textSearch">
            <text language="en-US">Search by name</text>
            <text language="de-DE">Suche auf Name</text>
            <text language="nl-NL">Zoek op naam</text>
        </entry>
        <entry key="buttonListView">
            <text language="en-US">List/tree view</text>
            <text language="de-DE">Liste/Baum Ansicht</text>
            <text language="nl-NL">Lijst/Tree view</text>
        </entry>
        <entry key="resetToDefault">
            <text language="en-US">Reset View</text>
            <text language="de-DE">Originalansicht</text>
            <text language="nl-NL">Reset view</text>
        </entry>
        <entry key="buttonSortByName">
            <text language="en-US">Sort by name</text>
            <text language="de-DE">Sortiere auf Name</text>
            <text language="nl-NL">Sorteer op naam</text>
        </entry>
        <entry key="buttonCollapseAll">
            <text language="en-US">Collapse All</text>
            <text language="de-DE">Alles einklappen</text>
            <text language="nl-NL">Alles inklappen</text>
        </entry>
        <entry key="buttonDownload">
            <text language="en-US">Download</text>
            <text language="de-DE">Download</text>
            <text language="nl-NL">Downloaden</text>
        </entry>
        <entry key="buttonDownloadHelp">
            <text language="en-US">Downloads the full page as HTML. This sends a new request to the server to make it suitable for opening in Excel. Tries to retain markup.</text>
            <text language="de-DE">Lädt die ganze Seite als HTML herunter. Dadurch wird eine neue Anforderung an den Server gesendet, damit dieser zum Öffnen in Excel geeignet ist. Versucht, das Markup beizubehalten.</text>
            <text language="nl-NL">Downloadt de hele pagina als HTML. Hiervoor wordt een nieuwe verzoek naar de server gestuurd om deze geschikt te maken voor openen in Excel. Probeert alle opmaak te behouden.</text>
        </entry>
        <entry key="buttonExportTableToExcel">
            <text language="en-US">Export Table to Excel</text>
            <text language="de-DE">Tabelle nach Excel exportieren</text>
            <text language="nl-NL">Exporteer tabel naar Excel</text>
        </entry>
        <entry key="buttonExportTableToExcelHelp">
            <text language="en-US">Downloads the table as currently visible as Excel (xls). Looses some of the markup.</text>
            <text language="de-DE">Lädt die Tabelle so aktuell wie Excel (xls) herunter. Verliert einen Teil des Markups.</text>
            <text language="nl-NL">Downloadt de tabel zoals weergegeven als Excel (xls). Verliest een deel van de opmaak.</text>
        </entry>
        <entry key="collapse">
            <text language="en-US">Collapse</text>
            <text language="de-DE">Einklappen</text>
            <text language="nl-NL">Inklappen</text>
        </entry>
        <entry key="expand">
            <text language="en-US">Expand</text>
            <text language="de-DE">Ausklappen</text>
            <text language="nl-NL">Uitklappen</text>
        </entry>
        <entry key="buttonExpandAll">
            <text language="en-US">Expand All</text>
            <text language="de-DE">Alles ausklappen</text>
            <text language="nl-NL">Alles uitklappen</text>
        </entry>
        <entry key="buttonCollapseAllCodes">
            <text language="en-US">Collapse All Codes</text>
            <text language="de-DE">Alle Codes einklappen</text>
            <text language="nl-NL">Alle codes inklappen</text>
        </entry>
        <entry key="buttonShow">
            <text language="en-US">HTML</text>
            <text language="de-DE">HTML</text>
            <text language="nl-NL">HTML</text>
        </entry>
        <entry key="buttonConvert">
            <text language="en-US">CDAr3</text>
            <text language="de-DE">CDAr3</text>
            <text language="nl-NL">CDAr3</text>
        </entry>
        <entry key="buttonGo">
            <text language="en-US">Show</text>
            <text language="de-DE">Zeigen</text>
            <text language="nl-NL">Tonen</text>
        </entry>
        <entry key="helpText">
            <text language="en-US">Click on triangles in the first column to open/close groups. Click the [-] sign to hide a column. Choose a column with  'Show column' to show it again. You can drag 
                columns to re-arrange the view.</text>
            <text language="nl-NL">Klik op de driehoeken in de eerste kolom om groepen te openen of te sluiten. Klik op [-] om een kolom te verbergen. Kies een kolom in 'Toon kolom' om deze weer te tonen. 
                Kolommen kunnen versleept worden naar een nieuwe positie.</text>
            <text language="de-DE">Klicken Sie auf die Dreiecke in der ersten Spalte zum Öffnen oder Schließen von Gruppen. Klicken Sie auf das [-] Zeichen, um eine Spalte zu verbergen. Wählen Sie eine 
                Spalte mit "Zeige Spalte", um diese wieder zu zeigen. Man kann die Spalten ziehen, um diese neu einzuordnen.</text>
        </entry>
        <entry key="goTo">
            <text language="en-US">Show </text>
            <text language="de-DE">Zeige </text>
            <text language="nl-NL">Toon </text>
        </entry>
    </names>
    
    return $names/entry
};