xquery version "3.1";
(:
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace userapi   = "http://art-decor.org/ns/api/user" at "../user-api.xqm";
import module namespace serverapi = "http://art-decor.org/ns/api/server" at "../server-api.xqm";
import module namespace setlib    = "http://art-decor.org/ns/api/settings" at "library/settings-lib.xqm";
import module namespace utillib   = "http://art-decor.org/ns/api/util" at "util-lib.xqm";

import module namespace errors    = "http://e-editiones.org/roaster/errors";

(:
    Email through Sendmail from eXist about recently changed issues per user per trigger
:)

declare namespace mail            = "http://exist-db.org/xquery/mail";
declare namespace xmldb           = "http://exist-db.org/xquery/xmldb";
declare namespace sm              = "http://exist-db.org/xquery/securitymanager";

declare %private variable $LOCALES   := $setlib:strApiLocales;
declare %private variable $SCAN-COLL := collection($setlib:strDecorScheduledTasks);

(:
    get parameters from the call, these parameters are mandatory but maybe empty
    
    sendmail
        if set to "true" the emails will really be sent out and 
        user records will be updated; in any other case the
        summary of the potentially processed records are returned
    mysender
        the email address denotes the sender
    accounting
        this denotes the account team email address, if any
    myserverurl
        the URL of the server name, e.g. https://art-decor.org

    internal note: defaults for external vars such as declare variable $local:myserverurl as xs:string external := ''; do not work,
    ie a value must be supplied from outside.
:)

declare variable $local:sendmail external;
declare variable $local:mysender external;
declare variable $local:accounting external;
declare variable $local:myserverurl external;

(:~ creates a random password string of $length chars, $length <= 20

@return random password, conformant ro the rules
@since 2022-02-02
:)
declare %private function local:getpwd () {

    let $length := 10 
    let $lowerCharacters := 'abcdefghijklmnopqrstuvwxyz'
    let $upperCharacters := 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    let $specialCharacters := '!§_*/=.-'
    let $numbers := '0123456789'
    
    (: generate 20 random chars from scrambled char classes above, always start with $upperCharacters :)
    let $ari := ($upperCharacters, local:scramble(($lowerCharacters, $upperCharacters, $numbers, $specialCharacters)))
    let $pwd1 :=
        for $cc in (1 to 4)
            for $ix in $ari
            return substring ($ix, util:random(string-length($ix)), 1)
    let $pwd := substring(string-join($pwd1, ''), 1, $length)
    return $pwd

};

(:~ scramble the array randomly

@return randomly sramble array
@since 2022-02-02
:)
declare %private function local:scramble ($items) {
    let $count := count($items)
    return
        if ($count = 1) then
            $items
        else
            let $selection-index := util:random ($count - 1) + 1
            let $remaining := (subsequence ($items, 1, $selection-index - 1), subsequence ($items, $selection-index + 1))
            return (subsequence ($items, $selection-index, 1), local:scramble ($remaining))
};

(:~ update progres for the task

@return true()
@since 2022-02-04
:)
declare %private function local:updateTaskProgress ($un, $msg, $pct) {
    let $upt := update value $un/@progress with $msg
    let $upt := update value $un/@progress-percentage with $pct
    return true()
};

(:~ convert to output, \n becomes <br>

@return converted output as item list
@since 2022-02-02
:)
declare %private function local:brout ($txt) {
    let $y := tokenize($txt, '\n')
    let $z :=
        for $x in $y[position() < last()]
        return ($x, <br/>)
    return ($z, $y[last()])
};


(:~ find scheduled notification requests for users and notify them.
Example user-notify-request
    <user-notify-request 
        for="kai"
        email="info@kheitmann.de"
        name="dr Kai Heitmann"
        on="2022-02-02T14:26:17.989+01:00"
        as="a6f143b3-a914-43db-b143-b3a91433dbe8"
        setpassword="true"
        by="kai"
        language="en-US"
        progress="Added to notification queue ..."
        progress-percentage="0"/>

Process
- the user is checked whether active
- he gets a newly assigned password if setpassword is "true"
- an email notifcation is sent to him containg username, 
  optionally a new password and
  a list of all his projects where he is author or contributor

:)

(: check availability of required parameters, fail if undefined or empty :)
let $doSendmail   :=
    if (
        try { $local:sendmail != '' }
        catch err:XPDY0002 {
            let $err := util:log('ERROR', 'scheduled-notifier: error +++ variable/parameter "sendmail" not set in server scheduler configuration')
            let $err := error($errors:BAD_REQUEST,'Required scheduled-notifier variable/parameter "sendmail" not set in server scheduler configuration')
            return false()
        }
        )
    then
        $local:sendmail = 'true'
    else (
        let $err := util:log('ERROR', 'scheduled-notifier: error +++ variable/parameter "sendmail" not set in server scheduler configuration')
        let $err := error($errors:BAD_REQUEST,'Required scheduled-notifier variable/parameter "sendmail" not set in server scheduler configuration')
        return false()
    )
    
let $myAccounting :=
    if (
        try { $local:accounting != '' }
        catch err:XPDY0002 {
            let $err := util:log('ERROR', 'scheduled-notifier: error +++ variable/parameter "accounting" not set in server scheduler configuration')
            let $err := error($errors:BAD_REQUEST,'Required scheduled-notifier variable/parameter "accounting" not set in server scheduler configuration')
            return ''
        }
    )
    then
        $local:accounting
    else (
        let $err := util:log('ERROR', 'scheduled-notifier: error +++ variable/parameter "accounting" not set in server scheduler configuration')
        let $err := error($errors:BAD_REQUEST,'Required scheduled-notifier variable/parameter "accounting" not set in server scheduler configuration')
        return ''
    )

let $theSender :=
    if (
        try { $local:mysender != '' }
        catch err:XPDY0002 {
            let $err := util:log('ERROR', 'scheduled-notifier: error +++ variable/parameter "mysender" not set in server scheduler configuration')
            let $err := error($errors:BAD_REQUEST,'Required scheduled-notifier variable/parameter "mysender" not set in server scheduler configuration')
            return ''
        }
     )
    then
        $local:mysender
    else (
        let $err := util:log('ERROR', 'scheduled-notifier: error +++ variable/parameter "mysender" not set in server scheduler configuration')
        let $err := error($errors:BAD_REQUEST,'Required scheduled-notifier variable/parameter "mysender" not set in server scheduler configuration')
        return ''
    )

let $theServer :=
    if (
        try { $local:myserverurl != '' }
        catch err:XPDY0002 {
            let $err := util:log('ERROR', 'scheduled-notifier: error +++ variable/parameter "myserverurl" not set in server scheduler configuration')
            let $err := error($errors:BAD_REQUEST,'Required scheduled-notifier variable/parameter "myserverurl" not set in server scheduler configuration')
            return ''
        }
    )
    then
        $local:myserverurl
    else (
        let $err := util:log('ERROR', 'scheduled-notifier: error +++ variable/parameter "myserverurl" not set in server scheduler configuration')
        let $err := error($errors:BAD_REQUEST,'Required scheduled-notifier variable/parameter "myserverurl" not set in server scheduler configuration')
        return ''
    )

let $allRequests            := 
    if (string-length($theSender) > 0 and string-length($theServer) > 0)
    then $SCAN-COLL/user-notify-request
    else ()

(: get all but oldest first :)
let $request                :=
    for $request in $allRequests[@progress-percentage = '0']
    order by $request/@on
    return $request

let $onoff   := if ($doSendmail) then 'on' else 'off'
let $check   := util:log('INFO', 'scheduled-notifier: starting, ' || count($request) || ' request active, sending email is ' || $onoff)

(: run through all open (@progress-percentage = '0') requests and handle them :)
let $result :=
    for $un in $request
        let $name      := string($un/@name)
        let $user      := string($un/@for)
        let $lang      := $un/@language
        let $to        := string($un/@email)
        let $asid      := string($un/@as)
        let $setpass   := if ($un/@setpassword = "true") then true() else false()
        
        let $check   := util:log('INFO', 'scheduled-notifier: processing, ' || $user || ' (' || $asid || ')')

        (: first check whether the account is active :)
        return
            if (sm:is-account-enabled($user))
            then (
            
                (: mark request as busy :)
                let $update    := local:updateTaskProgress($un, "Notifications started...", 10)
    
                (: get a new password and set it for the user, if required :)
                let $pass      := local:getpwd()
                let $update    := if ($setpass) then sm:passwd($user, $pass) else ()
        
                (: get the desired locale or en-US as a fallback :)
                let $l1        := util:binary-doc(concat($LOCALES, '/', $lang, '.json'))
                let $l2        :=
                    if (string-length($l1) = 0)
                    then util:binary-doc(concat($LOCALES,'/en-US.json'))
                    else $l1
                let $l3        := util:binary-to-string($l2)
                let $locale    := parse-json($l3)
                let $welcome   := $locale?welcome
                
                (: get all projects the user is in :)
                let $alluserprojects        :=
                    $setlib:colDecorData/decor/project[author[@username = $user]]
                let $p1        :=
                    for $p in $alluserprojects
                    let $plang := $p/@defaultLanguage
                    let $pname := $p/name[@language=$plang]
                    order by $pname
                    return <li>{$pname/text()} ({string($p/@prefix)})</li>
            
                (: build notification email string :)
                let $notifm    :=
                    <mail>
                        <from>{$theSender}</from>
                        <to>{$to}</to>
                        <subject>[art-decor] {$welcome}</subject>
                        <message>
                            <xhtml>
                                <html>
                                    <head>
                                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                        <title>{$welcome}</title>
                                        <style>{$utillib:emailCssStyles}</style>
                                    </head>
                                    <body>
                                        {
                                            <div class="container">
                                                <div class="main-body">
                                                {
                                                    <h3>{$welcome}</h3>,
                                                    <div class="row gutters-sm">
                                                        <div>
                                                            <div class="card">
                                                                <div class="card-body">
                                                                {
                                                                    local:brout($locale?account-notification-1), " ",
                                                                    <strong>{$name}</strong>,
                                                                    <p> </p>,
                                                                    local:brout($locale?account-notification-2), " ",
                                                                    <a href="{$theServer}">{$theServer}</a>,
                                                                    <p> </p>,
                                                                    local:brout($locale?account-username), " ",
                                                                    <span class="tt">{$user}</span>,
                                                                    <br> </br>,
                                                                    local:brout($locale?account-password), " ",
                                                                    if ($setpass) then <span class="tt">{$pass}</span> else local:brout($locale?not-reset),
                                                                    <p> </p>,
                                                                    local:brout($locale?account-notification-3),
                                                                    <p> </p>,
                                                                    if (count($p1) > 0) then (
                                                                        local:brout($locale?account-notification-projectlist),
                                                                        <ul>{$p1}</ul>
                                                                    ) else (
                                                                        local:brout($locale?account-notification-noprojects)
                                                                    ),
                                                                    local:brout($locale?account-notification-4),
                                                                    <p> </p>,
                                                                    local:brout($locale?account-signature),
                                                                    <br> </br>,
                                                                    $myAccounting
                                                                }
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                }
                                                </div>
                                            </div>
                                        }
                                    </body>
                                </html>
                            </xhtml>
                        </message>
                    </mail>
                    
                let $smtp := ()   (:~ use localhost :)
                let $sent := if ($doSendmail) then mail:send-email($notifm, $smtp, "UTF-8") else true()
                
                (: mark request as done :)
                let $update    :=
                    if ($sent) then (
                        local:updateTaskProgress($un, "Notifications done...", 100)
                    ) else (
                        local:updateTaskProgress($un, "Sending notification failed...", -1)
                    )

                let $check   := util:log('INFO', 'scheduled-notifier: processed, ' || $user || ' (' || $asid || ')')

                return <notified to="{$user}" email="{$to}" status="success-send-{$onoff}">{$notifm}</notified>

            ) else (
                (: mark request with error :)
                let $message := "Notifications failed, user not active..."
                let $update  := local:updateTaskProgress($un, $message, -1)
                let $check   := util:log('ERROR', 'scheduled-notifier: error processing, ' || $user)
                return <notified to="{$user}" email="{$to}" status="{$message}"/>
            )

let $check   := util:log('INFO', 'scheduled-notifier: finished')

return $result
