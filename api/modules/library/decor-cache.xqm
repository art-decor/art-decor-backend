xquery version "3.0";
(:
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:~ API for maintenance of the Decor cache :)
module namespace decor-cache = "http://art-decor.org/ns/api/decor-cache";

import module namespace errors      = "http://e-editiones.org/roaster/errors";

(: TODO refactor references to art modules? :)
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "settings-lib.xqm";
import module namespace serverapi   = "http://art-decor.org/ns/api/server" at "../server-api.xqm";


declare namespace http              = "http://expath.org/ns/http-client";
declare namespace f                 = "http://hl7.org/fhir";
declare namespace util              = "http://exist-db.org/xquery/util";

(: set timeout to 60 minutes = 3600 seconds = 3600000 :)
declare option exist:timeout "3600000";

declare option exist:output-size-limit "9000000";

declare variable $decor-cache:strDecorServices   := serverapi:getServerURLServices();

(:recursive function to get all templates hanging off from the current template. has circular reference protection. Does not return the start templates :)
declare %private function decor-cache:getRepositories($bbrs as element(buildingBlockRepository)*, $foundResults as item(), $trustedservermap as item(), $mkdir as xs:string?) as element(cacheme)* {
    let $results        := 
        for $bbr in $bbrs
        let $url    := $bbr/@url
        let $ident  := $bbr/@ident
        group by $url, $ident
        return
            if (map:contains($foundResults, concat($ident, $url))) then () else (
                decor-cache:getRepository($url, $ident, $trustedservermap, $mkdir)
            )
    
    (: map:merge is not supported by eXist 2.2 :)
    let $foundResults   := 
        map:merge((
            for $key in map:keys($foundResults) return map:entry($key, ''),
            for $key in distinct-values($bbrs/concat(@ident, @url)) return if (map:contains($foundResults, $key)) then () else map:entry($key, '')
        ))
    
    return
        if ($results) then (
            $results | decor-cache:getRepositories(
                $results/buildingBlockRepository[empty(@format)] | $results/buildingBlockRepository[@format = 'decor'],
                $foundResults,
                $trustedservermap,
                $mkdir
            )
        ) else ()
};

(: get stuff that doesn't live on our own server :)
declare %private function decor-cache:getRepository($url as xs:string?, $ident as xs:string?, $trustedservermap as item(), $mkdir as xs:string?) as element()? {
    if ($url = $decor-cache:strDecorServices) then () else (
        let $service-uri    := concat($url,'RetrieveProject?format=xml&amp;mode=cache&amp;prefix=',$ident)
        let $requestHeaders := 
            <http:request method="GET" href="{$service-uri}">
                <http:header name="Content-Type" value="application/xml"/>
                <http:header name="Cache-Control" value="no-cache"/>
                <http:header name="Max-Forwards" value="1"/>
            </http:request>
        let $cacheresponse  := 
            try {
                let $server-response := http:send-request($requestHeaders)
                return 
                    if ($server-response[1]/@status='200') then
                        $server-response[position() gt 1]/descendant-or-self::decor
                    else (
                        <error>{'HTTP ' || $server-response[1]/@status || ' ' || $server-response[1]/@message}</error>
                    )
            } 
            catch * { 
                <error>Caught error {$err:code}: {$err:description}. Data: {$err:value}</error>
            } 
        let $projectId      := $cacheresponse//project/@id
        let $rootisdecor    := name(($cacheresponse)[1]) = 'decor'
        
        let $fileId         := util:uuid()
        let $result         := 
            if ($rootisdecor) then (
                let $previous   := collection($mkdir)//cacheme[@bbrurl = $url][@bbrident = $ident]
                let $delete     := 
                    for $instance in $previous
                    return xmldb:remove($mkdir, util:document-name($instance))
                return ()
            ) else ()
        
        let $xml            := 
            <cacheme bbrurl="{$url}" bbrident="{$ident}" format="decor">
            {
                if ($projectId) then attribute projectId {$projectId} else (),
                if (map:contains($trustedservermap, concat($ident, $url))) then
                    attribute isTrusted {'true'}
                else (),
                if ($rootisdecor) then (
                    attribute fileId {$fileId},
                    $cacheresponse
                )
                else ( 
                    attribute error {$cacheresponse}
                )
            }
            </cacheme>
        let $result         := if ($rootisdecor) then xmldb:store($mkdir, concat($ident, $fileId, '.xml'), $xml) else ()
            
        return
            <cacheme>{$xml/@*, $xml//project/buildingBlockRepository}</cacheme>
    )
};

declare function decor-cache:updateDecorCache($authmap as map(*), $statusonly as xs:boolean, $cacheformat as xs:string) as element() {
    let $check              :=
        if ($authmap?groups = 'dba') then () else (
            error($errors:FORBIDDEN, 'You have to be dba for this function')
        )
    return
    switch ($cacheformat)
    case "decor" return
        if ($statusonly) then (
            $setlib:colDecorCache/cachedBuildingBlockRepositories
        )
        else (
            (: make directory :)
            let $dirname := "bbr"
            let $mkdir := xmldb:create-collection($setlib:strDecorCache, concat('/', $dirname))
            
            (: create or update progress indication :)
            let $timeStamp  := current-dateTime()
            let $result := xmldb:store($mkdir, 'progress.xml', <progress started="{$timeStamp}" dir="{$dirname}"/>)
            
            (: overall bbr references <buildingBlockRepository url="http://localhost:8877/decor/services/" ident="ad1bbr-"/> :)
            let $trustedservermap :=
                map:merge(
                    for $bbr in serverapi:getServerExternalRepositories()
                    let $urlident := concat($bbr/@ident, $bbr/@url)
                    group by $urlident
                    return
                        map:entry($urlident, $bbr[1])
                )
            let $bbrrefs    := $setlib:colDecorData//project/buildingBlockRepository[empty(@format)] | 
                               $setlib:colDecorData//project/buildingBlockRepository[@format = 'decor']
            let $bbrsfound  := decor-cache:getRepositories($bbrrefs, map:merge(()), $trustedservermap, $mkdir)
            
            let $foundok    :=
                for $x in $bbrsfound[empty(@error)]
                let $url    := $x/@bbrurl
                let $ident  := $x/@bbrident
                group by $url, $ident
                order by $x/@bbrurl, $x/@bbrident
                return (
                    let $e    := ($x[@isTrusted], $x)[1]
                    return
                        <ok url="{$e/@bbrurl}" ident="{$e/@bbrident}" format="{$cacheformat}">{$e/@isTrusted}</ok>
                )
            
            let $founderror :=
                for $x in $bbrsfound[@error]
                let $url    := $x/@bbrurl
                let $ident  := $x/@bbrident
                group by $url, $ident
                order by $url, $ident
                return  (
                    let $e    := ($x[@isTrusted], $x)[1]
                    return
                        <unreachable url="{$e/@bbrurl}" ident="{$e/@bbrident}" format="{$cacheformat}">{$e/@isTrusted, $e/@error}</unreachable>
                )
            
            let $bbrcount   := count($foundok)
            let $bbrerrors  := count($founderror)
            let $timeStamp  := current-dateTime()
            
            let $allbbrs :=
                <cachedBuildingBlockRepositories status="OK" count="{$bbrcount}" errors="{$bbrerrors}" time="{$timeStamp}" dir="{$dirname}">
                {
                    (: store only BBRs with unique project ids :)
                    $foundok,
                    $founderror
                }
                </cachedBuildingBlockRepositories>
            
            (: renew current cache :)
            let $result := xmldb:store($mkdir, 'cache.xml', $allbbrs)
            
            return 
                doc($result)/*
        )
    case "fhir" return
        if ($statusonly) then (
            $setlib:colDecorCache/cachedBuildingBlockRepositories
        )
        else (
            
            (: make directory :)
            let $dirname := "fhir"
            let $mkdir := xmldb:create-collection($setlib:strDecorCache, concat('/', $dirname))
            
            (: create or update progress indictaion :)
            let $timeStamp  := current-dateTime()
            let $result := xmldb:store($mkdir, 'progress.xml', <progress started="{$timeStamp}" dir="{$dirname}"/>)
            
            (: overall bbr references <buildingBlockRepository url="http://localhost:8877/decor/services/" ident="ad1bbr-"/> :)
            let $thisserverbbrs := $setlib:colDecorData//project/buildingBlockRepository[@format = 'fhir']
            
            let $requestHeaders :=
                <http:request method="GET">
                    <http:header name="Content-Type" value="application/fhir+xml"/>
                    <http:header name="Cache-Control" value="no-cache"/>
                    <http:header name="Max-Forwards" value="1"/>
                </http:request>
            
            let $bbrsfound      :=
                for $repository in $thisserverbbrs
                return
                    try {
                        let $url := concat($repository/@url, 'StructureDefinition')
                        let $server-response := http:send-request($requestHeaders, $url)
                        return (
                            <cacheme bbrurl="{$repository/@url}" bbrident="{$repository/@ident}" fileId="{util:uuid()}" format="{$cacheformat}" projectId="{$repository/ancestor::project[1]/@id}">
                            {
                                if ($server-response[1]/@status = '200' and $server-response instance of element()) then 
                                    $server-response[position() gt 1]/descendant-or-self::f:Bundle 
                                else
                                if ($server-response[1]/@status = '200') then () else (
                                     attribute error {'HTTP ' || $server-response[1]/@status || ' ' || $server-response[1]/@message }
                                )
                            }
                            </cacheme>
                        )
                    }
                    catch * {
                        <unreachable url="{$repository/@url}" ident="{$repository/@ident}" format="{$cacheformat}">
                        {
                            attribute error {'Caught error ' || $err:code || ': ' || $err:description || '. Data: ' || $err:value}
                        }
                        </unreachable>
                    }
            
            let $foundok    :=
                for $x in $bbrsfound[empty(@error)]
                let $id     := $x/@projectId
                group by $id
                order by $x/@bbrurl, $x/@bbrident
                return (
                    let $e    := ($x[@isTrusted], $x)[1]
                    return
                        <ok url="{$e/@bbrurl}" ident="{$e/@bbrident}" format="{$cacheformat}">{$e/@isTrusted, $e}</ok>
                )
            
            let $founderror :=
                for $x in $bbrsfound[@error]
                let $url    := $x/@bbrurl
                let $ident  := $x/@bbrident
                group by $url, $ident
                order by $url, $ident
                return  (
                    let $e    := ($x[@isTrusted], $x)[1]
                    return
                        <unreachable url="{$e/@bbrurl}" ident="{$e/@bbrident}" format="{$cacheformat}">{$e/@isTrusted, $e/@error}</unreachable>
                )
                
            let $bbrcount   := count($foundok/f:Bundle)
            let $bbrerrors  := count($founderror)
            let $timeStamp  := current-dateTime()
            
            let $allbbrs :=
                <cachedBuildingBlockRepositories status="OK" count="{$bbrcount}" errors="{$bbrerrors}" time="{$timeStamp}" dir="{$dirname}">
                {
                    (: store only BBRs with unique project ids :)
                    $foundok,
                    $founderror
                }
                </cachedBuildingBlockRepositories>
            
            (: renew current cache :)
            let $result := xmldb:store($mkdir, 'cache.xml', $allbbrs)
            
            return
                doc($result)/*
        )
    default return (
        error($errors:FORBIDDEN, 'Unsupported cache format. Found ''' || $cacheformat || '''. Expected one of: ''decor'', ''fhir''.')
    )
};

declare function decor-cache:getLastCacheRefresh($dir as xs:string) as element()? {
    let $lrf := <cachedBuildingBlockRepositories time="{$setlib:colDecorCache/cachedBuildingBlockRepositories[@dir = $dir]/@time}"/>
    return 
        $lrf
};

declare function decor-cache:getProgressCacheRefresh($dir as xs:string) as element()? {
    let $lrf := <progress started="{$setlib:colDecorCache/progress[@dir = $dir]/@started}"/>
    return 
        $lrf
};