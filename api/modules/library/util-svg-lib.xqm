xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

module namespace utilsvg       = "http://art-decor.org/ns/api/util-svg";

declare namespace svg          = "http://www.w3.org/2000/svg";

declare variable $utilsvg:levelSpacing  := 45;

(:~  Function takes a DECOR concept hierarchy as argument and returns the concept hierarchie as a set of nested svg:g elements

@param $concept         - required concept
@param $filterStatuses  - required an array of statuses not allowed in the svg diagram
@param $language        - optional decor/@language. 
@return concept hierarchie as a set of nested svg:g elements
@since 2024-01-04
:)
declare function utilsvg:convertConcept2Svg($concept as element(), $language as xs:string, $filterStatuses as xs:string*, $dconcept as element()?, $baseUrl as xs:string?) as element() {

    let $classes            := <classes>{local:conceptClassbox($concept[not(@statusCode = $filterStatuses)][1], $language, $filterStatuses, $baseUrl)}</classes>
        
    let $depths             := 
        for $group in $classes//svg:g
        return
            count($group/ancestor::svg:g)
    let $depth              := max($depths)
    
    let $scan               := local:reverseScan($classes, $depth)
    let $process            := 
        for $i in (1 to $depth)
        return
            if ($i=1 and $depth>1) then
                <step>let {concat('$step',$i,':= local:procesScan($scan)')}</step>
            else if ($i=$depth and $depth=1) then
                <step>let {concat('$step',$i,':= local:procesScan($scan)')}</step>|
                <step>return {concat('$step',$i)}</step>
            else if ($i=$depth and $depth>1) then
                <step>let {concat('$step',$i,':= local:procesScan($step',$i -1,')')}</step>|
                <step>return {concat('$step',$i)}</step>
            else(<step>let {concat('$step',$i,':= local:procesScan($step',$i -1,')')}</step>)
    
    let $processed          := util:eval(string-join($process,' '))
    
    let $width              :=  
        if (count($classes//svg:g)=1) then
            $classes//svg:rect[1]/@width
        else ($processed//level[@depth=0]/class/@width)
    
    let $height :=
        if (count($classes//svg:g)=1) then
            $classes//svg:rect[1]/@height + $utilsvg:levelSpacing 
        else (
            sum($processed//@maxHeight) + $utilsvg:levelSpacing + $utilsvg:levelSpacing * $depth
        )
        
    let $atTopLevel                 := not($concept/name()='concept')
    let $conceptParentId            := if ($dconcept) then $dconcept/parent::concept/@id else ()
    let $conceptParentEd            := if ($dconcept) then $dconcept/parent::concept/@effectiveDate else ()    
    
    return (
      <svg xmlns="http://www.w3.org/2000/svg" version="1.1" height="{$height+5}" width="{$width+5}">
          
          <style type="text/css"><![CDATA[
              body {
                  font-family: Roboto, sans-serif;
              }
          ]]>
          </style>
          <style type="text/css"><![CDATA[
              .dataset-bold {
                  font-family: Roboto, sans-serif;
                  font-size:11px;
                  font-weight:700;
                  font-style: italic;
                  text-align:start;
                  line-height:125%;
                  text-anchor:middle;
                  fill:#000000;
                  fill-opacity:1;
                  stroke:none;
              }
          ]]>
          </style>
          <style type="text/css"><![CDATA[
              .normal-bold {
                  font-family: Roboto, sans-serif;
                  font-size:12px;
                  font-weight:700;
                  text-align:start;
                  line-height:125%;
                  text-anchor:middle;
                  fill:#000000;
                  fill-opacity:0.8;
                  stroke:none;
              }
          ]]>
          </style>
          <style type="text/css"><![CDATA[
              .inherit-bold {
                  font-family: Roboto, sans-serif;
                  font-size:12px;
                  font-weight:700;
                  text-align:start;
                  line-height:125%;
                  text-anchor:middle;
                  fill:#9eeb47;
                  fill-opacity:0.8;
                  stroke:none;
              }
              .inherit {
                  fill:#9eeb47;
                  fill-opacity:0.8;
              }
          ]]>
          </style>
          <style type="text/css"><![CDATA[
              .normal-start {
                  font-family: Roboto, sans-serif;
                  font-weight:300;
                  font-size:12px;
                  text-align:start;
                  text-anchor:start;
                  fill:#000000;
                  fill-opacity:0,8;
                  stroke:none;
              }
          ]]>
          </style>
          <style type="text/css"><![CDATA[
              .class-box {
                  fill: #9eeb47;
                  fill-opacity:0.2;
                  fill-rule:evenodd;
                  stroke:#ffffff;
                  stroke-width:0;
                  stroke-linecap:round;
                  stroke-linejoin:round;
                  stroke-opacity:1;
                  stroke-dasharray:none
              }
              .class-box-hover {
                  cursor: pointer;
              }
              .class-box-hover:hover {
                  fill-opacity:0.1;
              }
          ]]>
          </style>
          <defs>
              <filter id="shadow" height="200%" width="200%" y="0" x="0">
                  <feOffset result="offOut" dx="3" dy="3" in="SourceGraphic"/>
                  <feColorMatrix result="matrixOut" values="0.2 0 0 0 0 0 0.2 0 0 0 0 0 0.2 0 0 0 0 0 1 0" type="matrix" in="offOut"/>
                  <feGaussianBlur result="blurOut" stdDeviation="2" in="matrixOut"/>
                  <feBlend in2="blurOut" in="SourceGraphic" mode="normal"/>
              </filter>
          </defs>
      
          <rect style="fill:#ffffff;fill-opacity:1;stroke:#000000;stroke-width:0" width="{$width}" height="{$height+5}" x="0" y="0">
              <desc>Background rectangle in white to avoid transparency.</desc>
          </rect>
          {
              if ($classes/svg:g/svg:g) then
                  local:positionClasses($classes/svg:g, $processed, $conceptParentId, $conceptParentEd, $atTopLevel, $baseUrl)
              else (
              (:$classes/*:)
                  element {QName('http://www.w3.org/2000/svg','g')}
                  {
                      attribute transform {concat('translate(',0,',',$utilsvg:levelSpacing,')')},
                      $classes/svg:g/(@* except @transform),
                      $classes/svg:g/svg:*,
                      if (not($atTopLevel)) then (
                          local:getCircle($conceptParentId, $conceptParentEd, ($classes//svg:rect[1]/@width div 2), -25, $baseUrl),
                          element {QName('http://www.w3.org/2000/svg','path')}
                          {
                              attribute style {'fill:none;stroke:#9eeb47;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:0.8'},
                              attribute d {concat('m ',($classes//svg:rect[1]/@width div 2),', 0 ','v -',($utilsvg:levelSpacing div 3))}
                          }
                      ) else ()
                  }
              )
          }
      </svg>
  )

};

(:~  Returns the svg content for a classbox groep and children 

@param $concept         - required concept
$filterStatuses         - required an array of statuses not allowed in the svg diagram
@param $language        - optional decor/@language. 
@return concept hierarchie as a set of nested svg:g elements
@since 2024-01-04
:)
declare %private function local:conceptClassbox($concept as element(), $language as xs:string, $filterStatuses as xs:string*, $baseUrl as xs:string?) as element() {
    let $id                 := if ($concept/@ref) then $concept/@ref else $concept/@id
    let $effectiveDate      := if ($concept/@ref) then $concept/@flexibility else $concept/@effectiveDate
    let $name               := $concept/name[@language=$language][1]
    let $maxConceptName     := max($concept/(name[@language=$language][1]|concept/name[@language=$language])/string-length(normalize-space(.)))
    
    let $minClassboxWidth   := 200
    (:
        the width of the 'right hand side'. 
        for transaction based concepts this is valueDomain/@type + card/conf. 
        for dataset based concepts this valueDomain/@type 
    :)
    let $typeCardConfWidth  := if ($concept/ancestor-or-self::dataset[@transactionId]) then (130) else (80)
    let $classboxWidth      :=
        if ($typeCardConfWidth + $maxConceptName*6 > $minClassboxWidth) then
            $typeCardConfWidth + $maxConceptName*6
        else (
            $minClassboxWidth
        )
    let $classboxHeight     := 60 + count($concept/concept[@type='item']) *15
    
    return
        (: group for the classbox and children :)
        element {QName('http://www.w3.org/2000/svg','g')} 
        {
            attribute id {concat('_',util:uuid())}
            ,
            (: green rectangle in the classbox :)
            element {QName('http://www.w3.org/2000/svg','rect')}
            {
                attribute x {0},
                attribute y {0},
                attribute rx {10},
                attribute ry {10},
                attribute height {$classboxHeight},
                attribute width {$classboxWidth},
                (:attribute filter {"url(#shadow)"},:) 
                (:attribute id {$id},:)
                if ($concept/self::dataset) then (
                    attribute class {'class-box'}
                ) else (
                    attribute class {'class-box class-box-hover'},
                    if ($baseUrl) then
                        attribute onclick {'javascript:location.href=&quot;' || xs:anyURI($baseUrl || '&amp;conceptId=' || $id || '&amp;conceptEffectiveDate=' || $effectiveDate) || '&quot;'}
                    else ()
                )
            }
            ,
            (: line between classbox name and classbox content :)
            element {QName('http://www.w3.org/2000/svg','path')}
            {
                attribute style {'fill:none;stroke:#9eeb47;stroke-width:0.5px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:0.8'},
                attribute d {concat('m 5, 30  h ',$classboxWidth -10)}
            }
            ,
            (: classbox name (card/conf) :)
            element {QName('http://www.w3.org/2000/svg','text')}
            {
                attribute x {$classboxWidth div 2},
                attribute y {20},
                if ($concept/inherit) then
                    attribute class {'inherit-bold', concat('node-s', $concept/@statusCode)}
                else if ($concept/self::dataset) then
                    attribute class {'dataset-bold', concat('node-s', $concept/@statusCode)}
                else (
                    attribute class {'normal-bold', concat('node-s', $concept/@statusCode)}
                )
                ,
                let $conceptName := normalize-space(string-join($name/text(),''))
                return if (string-length($conceptName)=0) then '&#160;' else $conceptName
                ,
                if ($concept/ancestor::dataset[@transactionId]) then (
                    element {QName('http://www.w3.org/2000/svg','tspan')}
                    {
                        attribute x {$classboxWidth - 5},
                        attribute dy {0},
                        attribute style {'text-anchor:end;font-style:italic;'},
                        ' ',
                        if ($concept/@conformance=('NP','C')) then (
                            string($concept/@conformance)
                        )
                        else (
                            if ($concept/@minimumMultiplicity or $concept/@maximumMultiplicity) then (
                                concat($concept/@minimumMultiplicity,'..',$concept/@maximumMultiplicity)
                            ) else ()
                            ,
                            if ($concept/@isMandatory='true') then 'M' else string($concept/@conformance)
                        )
                    }
                ) else ()
                ,
                element {QName('http://www.w3.org/2000/svg','tspan')}
                {
                    attribute x {5},
                    attribute dy {15},
                    '&#160;'
                }
            }
            ,
            (: classbox content :)
            element {QName('http://www.w3.org/2000/svg','text')}
            {
                attribute x {5},
                attribute y {40},
                attribute class {'normal-start'},
                for $c in $concept/concept[@type='item'][not(@statusCode = $filterStatuses)]
                return
                    element {QName('http://www.w3.org/2000/svg','tspan')}
                    {
                        attribute x {5},
                        attribute dy {15},
                        attribute class {
                            if ($c/inherit) then ('inherit') else (),
                            concat('node-s', $concept/@statusCode)
                        }
                        ,
                        let $conceptName := normalize-space(string-join($c/name[@language=$language],''))
                        return if (string-length($conceptName)=0) then '&#160;' else $conceptName
                        ,
                        element {QName('http://www.w3.org/2000/svg','tspan')}
                        {
                            attribute x {$classboxWidth - 5},
                            attribute dy {0},
                            attribute style {'text-anchor:end;'}
                            ,
                            let $conceptType := normalize-space(string-join($c/valueDomain/@type,''))
                            return if (string-length($conceptType)=0) then '&#160;' else $conceptType
                            ,
                            element {QName('http://www.w3.org/2000/svg','tspan')}
                            {
                                attribute dy {0},
                                attribute style {'font-style:italic;'},
                                ' ',
                                if ($c/@conformance=('NP','C')) then (
                                    string($c/@conformance)
                                )
                                else (
                                    if ($c/@minimumMultiplicity or $c/@maximumMultiplicity) then (
                                        concat($c/@minimumMultiplicity,'..',$c/@maximumMultiplicity)
                                    ) else ()
                                    ,
                                    if ($c/@isMandatory='true') then 'M' else string($c/@conformance)
                                )
                            }
                        }
                    }
                ,
                element {QName('http://www.w3.org/2000/svg','tspan')}
                {
                    attribute x {5},
                    attribute dy {15},
                    '&#160;'
                }
            }
            ,
            (: get classboxes for child concepts :)
            for $g in $concept/concept[@type='group'][not(@statusCode = $filterStatuses)]
            return
            local:conceptClassbox($g,$language,$filterStatuses, $baseUrl)
        }
};

declare %private function local:reverseScan($classes as element(), $startDepth as item()) as element() {
    let $current := $classes//svg:g[count(ancestor::svg:g)=$startDepth]
    let $spacing := 10
    return
    <level depth="{$startDepth}" count="{count($current)}" maxHeight="{max($current/svg:rect/@height)}" width="{sum($current/svg:rect/@width)}">
    {
        for $item in $current
        return
            <class id="{$item/@id}" parentId="{$item/parent::svg:g/@id}" height="{$item/svg:rect/@height}" width="{$item/svg:rect/@width + ((count($current)-1) * $spacing)}"/>
    }
    {
        if ($startDepth > 0) then
            local:reverseScan($classes,$startDepth -1)
        else()
    }
    </level>
};

declare %private function local:procesScan($scan as element()) as element() {
    let $level := $scan
    return
    <level depth="{$level/@depth}" count="{$level/@count}" maxHeight="{$level/@maxHeight}" width="{$level/@width}">
    {
        for $class in $level/class
        let $id := $class/@id
        return
            <class id="{$class/@id}" parentId="{$class/@parentId}" height="{$class/@height}" width="{if (sum($class/preceding::*[@parentId=$id]/@width)>$class/@width) then sum($class/preceding::*[@parentId=$id]/@width) else ($class/@width)}"/>
    }
    {
        if ($level/level) then
            local:procesScan($level/level)
        else()
    }
    </level>
};

declare %private function local:positionClasses($classes as element(), $processedScan as element(), $de-parent-id as xs:string?, $de-parent-ed as xs:string?, $atTopLevel as xs:boolean, $baseUrl as xs:string?) as element() {
    let $id             := $classes/@id
    
    let $parentId       := $processedScan//class[@id=$id]/@parentId
    let $parentWidth    := $classes/ancestor::svg:g[@id=$parentId]/svg:rect/@width div 2
    let $parentHeight   := $classes/ancestor::svg:g[@id=$parentId]/svg:rect/@height
    let $level          := $processedScan//class[@id=$id]/parent::level/@depth
    let $xShift         := 
        if ($level=0) then
            ($processedScan//class[@id=$id]/@width div 2) - ($classes/svg:rect/@width div 2)
        else( 
            ($processedScan//class[@id=$parentId]/@width div -2) + ($processedScan//class[@id=$id]/@width div 2) - ($classes/svg:rect/@width div 2) + $parentWidth + sum($processedScan//class[@id=$id]/preceding-sibling::class[@parentId=$parentId]/@width)
        )
    let $xPath          :=
        if ($level>0) then
            $xShift + (($classes/svg:rect/@width div 2) - $parentWidth)
        else()
    let $yShift         :=
        if ($level=0) then
            $utilsvg:levelSpacing
        else(
            $processedScan//level[@depth=$level -1]/@maxHeight + $utilsvg:levelSpacing
        )
    let $startnode      :=
        if ($level=0 and not($atTopLevel)) then (
            local:getCircle($de-parent-id, $de-parent-ed, ($classes/svg:rect/@width div 2),($utilsvg:levelSpacing * -1) + 20, $baseUrl)
        ) else()
    let $path           :=
        if ($level>0 or ($level=0 and not($atTopLevel))) then
            element {QName('http://www.w3.org/2000/svg','path')}
            {
                attribute style {'fill:none;stroke:#9eeb47;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:0.8'},
                attribute d {concat('m ',($classes/svg:rect/@width div 2),', 0 ','v -',($utilsvg:levelSpacing div 3),
                    let $hVal := $xPath * -1
                    return if ($hVal castable as xs:integer) then (concat(' h ',$hVal)) else ()
                    ,
                    let $vVal := ($yShift - $parentHeight - ($utilsvg:levelSpacing div 3)) * -1 
                    return if ($vVal castable as xs:integer) then (concat(' v ',$vVal)) else ())}
            }
        else()
    return
    element {QName('http://www.w3.org/2000/svg','g')}
    {
        attribute id {$id},
        attribute transform {concat('translate(',$xShift,',',$yShift,')')},
        $classes/(@* except @id|@transform),
        $classes/svg:rect,
        $classes/svg:path,
        $classes/svg:text,
        $classes/svg:a,
        $startnode,
        $path,
        for $grp in $classes/svg:g
        return
        local:positionClasses($grp, $processedScan, $de-parent-id, $de-parent-ed, $atTopLevel, $baseUrl)
    }
};

declare %private function local:getCircle($de-parent-id as xs:string?, $de-parent-ed as xs:string?, $cx as xs:integer, $cy as xs:integer, $baseUrl as xs:string?) as element() {
            
    let $detailUri          :=
        if ($de-parent-id) 
            then 'javascript:location.href=&quot;' || xs:anyURI($baseUrl || '&amp;conceptId=' || $de-parent-id || '&amp;conceptEffectiveDate=' || $de-parent-ed) || '&quot;'
            else 'javascript:location.href=&quot;' || xs:anyURI($baseUrl) || '&quot;'
        
    return
    element {QName('http://www.w3.org/2000/svg','circle')}
    {
        attribute cx {$cx},
        attribute cy {$cy},
        attribute r {'10'},
        attribute class {'class-box class-box-hover'},
        if ($baseUrl) then attribute onclick {$detailUri} else ()        
    }
};

(:~  Function takes a DECOR templatechain as argument and returns the templatechain as a set of nested svg:g elements

@param $templateChain   - required concept
@param $classboxWidth   - required width of the classbox. 
@return templateChain as a set of nested svg:g elements
@since 2024-03-08
:)
declare function utilsvg:convertTemplate2Svg($templateChain as element()) as element() {
    let $classboxWidth  := 30 + max($templateChain//@len)*7
    let $height         := max($templateChain//@pos) * 47 + 50
    let $width          := max($templateChain//@indent) * 50 + $classboxWidth + 50
    
    return
        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" height="{$height+5}" width="{$width+5}">
 
            <style type="text/css"><![CDATA[
              body {
                  font-family: Roboto, sans-serif;
              }
            ]]>
            </style>
 
            <style type="text/css"><![CDATA[
              .normal-bold {
                  font-family: Roboto, sans-serif;
                  font-size:11px;
                  font-weight:700;
                  text-align:start;
                  line-height:125%;
                  text-anchor:middle;
                  fill:#000000;
                  fill-opacity:0.8;
                  stroke:none;
                  }
            ]]>
            </style>
            <style type="text/css"><![CDATA[
              .text-bold {
                  font-family: Roboto, sans-serif;
                  font-size:11px;
                  font-weight:700;
                  line-height:125%;
              }
            ]]>
            </style>
            <style type="text/css"><![CDATA[
              .normal-start {
                  font-family: Roboto, sans-serif;
                  font-weight:300;
                  font-size:11px;
                  text-align:start;
                  text-anchor:start;
                  fill:#000000;
                  fill-opacity:0,8;
                  stroke:none;
              }
            ]]>
            </style>
            <style type="text/css"><![CDATA[
              .class-rect {
                  fill: #F59794;
                  fill-opacity:0.2;
                  fill-rule:evenodd;
                  stroke:#ffffff;
                  stroke-width:0;
                  stroke-linecap:round;
                  stroke-linejoin:round;
                  stroke-opacity:1;
                  stroke-dasharray:none
              }
            ]]>
            </style>
            <style type="text/css"><![CDATA[
              .class-rect-recurse {
                  fill:#F59794;
              }
            ]]>
            </style>
    
            <defs>
                <filter id="shadow" height="200%" width="200%" y="0" x="0">
                    <feOffset result="offOut" dx="3" dy="3" in="SourceGraphic"/>
                    <feColorMatrix result="matrixOut" values="0.2 0 0 0 0 0 0.2 0 0 0 0 0 0.2 0 0 0 0 0 1 0" type="matrix" in="offOut"/>
                    <feGaussianBlur result="blurOut" stdDeviation="2" in="matrixOut"/>
                    <feBlend in2="blurOut" in="SourceGraphic" mode="normal"/>
                </filter>
            </defs>
    
            <rect style="fill:#ffffff;fill-opacity:1;stroke:#000000;stroke-width:0"
                      id="backgroundObject" width="{$width}" height="{$height+5}" x="0" y="0">
                <desc>Background rectangle in white to avoid transparency.</desc>
            </rect>
               
            {
                local:drawtree ($templateChain, $classboxWidth)
            }
       
    </svg>

};

declare %private function local:drawbox ($template as element(), $ty as xs:string, $classboxWidth as xs:int) as element()* {
let $txt    := concat ($template/@id,' (', $template/@effectiveDate,')')
let $tn     := concat (if (string-length($template/@displayName)>0) then $template/@displayName else $template/@name, $template/@contains, $template/@ref)
let $xx     := 10 + 15 * $template/@indent
let $yy     := -20 + 45 * $template/@pos
let $stripx := $xx - 10
let $stripy := $yy + 18
(:
    get vertical angle connector offset:
    0: no angle at all
    1: short angle connection, first item under parent
    2: long angle connection, second and later item under parent
    3: multiple blocks angle connection, back to parent level
:)
let $path := if ($template/@connector = 1) then (
        <svg:path>
        {
            <svg:path style="fill:none;stroke:#000000;stroke-width:0.5px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                      d="m {$stripx}, {$stripy - 25} v 25" id="path2872"/>,
            <svg:path style="fill:none;stroke:#000000;stroke-width:0.5px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                      d="m {$stripx}, {$stripy} h 10" id="path2872"/>
        }
        </svg:path>
    ) else if ($template/@connector = 2) then (
        <svg:path>
        {
            <svg:path style="fill:none;stroke:#000000;stroke-width:0.5px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                      d="m {$stripx}, {$stripy - 45} v 45" id="path2872"/>,
            <svg:path style="fill:none;stroke:#000000;stroke-width:0.5px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                      d="m {$stripx}, {$stripy} h 10" id="path2872"/>
        }
        </svg:path>
    ) else if ($template/@connector > 2) then (
        <svg:path>
        {
            <svg:path style="fill:none;stroke:#000000;stroke-width:0.5px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                      d="m {$stripx}, {$stripy - $template/@connector * 25 - 10} v {$template/@connector * 25 + 10}" id="path2872"/>,
            <svg:path style="fill:none;stroke:#000000;stroke-width:0.5px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                      d="m {$stripx}, {$stripy} h 10" id="path2872"/>
        }
        </svg:path>
    )
    else (
        <svg:path/>
    )
return (
    $path/*,
    <svg:rect x="{$xx}" y="{$yy}" rx="10" ry= "10" stroke-width="0.25" stroke-miterlimit="10" width="{$classboxWidth}" height="37.668" class="class-rect{if ($template[@recurse]) then ('class-rect-recurse') else ()}"/>,
    <svg:text transform="matrix(1 0 0 1 {$xx+5} {$yy+15})" class="normal-start">
        <svg:tspan x="0" y="0"><svg:tspan class="text-bold">{$tn}</svg:tspan> ({$ty})</svg:tspan>
        <svg:tspan x="0" y="14">{$txt}</svg:tspan>
    </svg:text>
    )
};

declare %private function local:drawtree ($template as element()*, $classboxWidth as xs:int) as element()* {
    if ($template[self::tree]) then (
         for $i at $step in $template/*
         return local:drawtree($i, $classboxWidth)
    ) else (
        for $i at $step in $template
        return (
            local:drawbox ($i, if ($i/@by) then $i/@by else name($i) , $classboxWidth),
            for $j at $step in $i/*
            return local:drawtree($j, $classboxWidth)
        )
    )
};
