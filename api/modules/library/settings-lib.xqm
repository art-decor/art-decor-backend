xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
module namespace setlib                 = "http://art-decor.org/ns/api/settings";

declare namespace repo                  = "http://exist-db.org/xquery/repo";
declare namespace sm                    = "http://exist-db.org/xquery/securitymanager";

declare variable $setlib:root               := repo:get-root();
declare variable $setlib:strSecureConfig    := '/db/secure'; 

(:~ String variable with everything under art :)
declare variable $setlib:strArt             := concat($setlib:root,'art');
(:~ String variable with location of art/resources :)
declare variable $setlib:strArtResources    := concat($setlib:root,'art/resources');
(:~ Collection variable with everything under art/resources :)
declare variable $setlib:colArtResources    := collection($setlib:strArtResources);

(:~ String variable with everything under art-data :)
declare variable $setlib:strArtData         := concat($setlib:root,'art-data');
(: Should not expose these user-info variables. Use the api-user-settings.xqm instead :)
(:~ String variable to the art user-info.xml contents :)
declare variable $setlib:strUserInfo        := $setlib:strArtData || '/user-info';
(:~ String variable to the art server-functions.xml contents :)
declare variable $setlib:strServerFunctions := concat($setlib:strArtData, '/server-functions.xml');
(:~ String variable to the art urgentnews.xml contents :)
declare variable $setlib:strUrgentnews      := concat($setlib:strArtData,'/urgentnews.xml');
(:~ Document variable with the art server-functions.xml contents :)
(:declare variable $setlib:docServerFunctions := doc($setlib:strServerFunctions);:)

(:~ String variable with the ART-DECOR server config:)
declare variable $setlib:strServerInfo      := concat($setlib:strArtData,'/server-info.xml');
(:~ String variable with the default ART-DECOR server language. May be user overridden :)
declare variable $setlib:strArtLanguage     := 
    if (doc-available($setlib:strServerInfo) and doc($setlib:strServerInfo)/server-info/defaultLanguage) then
        doc($setlib:strServerInfo)/server-info/defaultLanguage/text()
    else ('en-US');

(:~ String variable with everything under api :)
declare variable $setlib:strApi             := concat($setlib:root,'api');
(:~ String variable with locales for the api :)
declare variable $setlib:strApiLocales      := concat($setlib:strApi,'/modules/locales');

(:~ String variable to the art decor-locks.xml contents :)
declare variable $setlib:strDecorLocks      := concat($setlib:strArtData,'/decor-locks.xml');
(:~ Document variable with the art decor-locks.xml contents :)
declare variable $setlib:docDecorLocks      := doc($setlib:strDecorLocks);

(:~ The path to the install-data server info file :)
declare variable $setlib:strServerInfoDefault           := concat($setlib:strArt,'/install-data/server-info.xml');
(:~ The document contents of the server info :)
(:declare variable $setlib:docServerInfoDefault         := doc($setlib:strServerInfoDefault);:)
(:~ The server default language :)
declare variable $setlib:strDefaultServerLanguage       := 'en-US';
(:~ The collection holding valid stylesheets for ART in its various forms (default, terminology, qualification server, ...) :)
declare variable $setlib:strServerXSLPath               := concat($setlib:strArtResources,'/stylesheets');
(:~ The default art do-it-all XSL on an ART-DECOR instance unless configured otherwise :)
declare variable $setlib:strDefaultServerXSL            := 'apply-rules.xsl';
(:~ The collection holding valid menu-structures for ART :)
declare variable $setlib:strServerMenuPath              := concat($setlib:strArtData,'/resources');
(:~ The default art menu template an ART-DECOR instance unless configured otherwise :)
declare variable $setlib:strDefaultServerMenu           := 'art-menu-template.xml';
(:~ The default logo name for an ART-DECOR instance unless configured otherwise :)
declare variable $setlib:strDefaultServerLogo           := 'art-decor-logo40.png';
(:~ The default logo url for an ART-DECOR instance unless configured otherwise :)
declare variable $setlib:strDefaultServerLogoUrl        := 'https://art-decor.org';
(:~ The list of users that may be regarded as 'system-level' users :)
declare variable $setlib:arrSystemLevelUsers            := ('xis-webservice');

(:~ String variable with the DECOR types :)
declare variable $setlib:strDecorTypes      := concat($setlib:strArtData,'/decor-xsd-types.xml');
(:~ String variable with the database location of decor collection :)
declare variable $setlib:strDecor           := concat($setlib:root,'decor');
(:~ String variable with the database location of decor/core collection :)
declare variable $setlib:strDecorCore       := concat($setlib:strDecor,'/core');
(:~ String variable with the database location of decor/cache collection :)
declare variable $setlib:strDecorCache      := concat($setlib:strDecor,'/cache');
(:~ String variable with the database location of decor/data collection :)
declare variable $setlib:strDecorData       := concat($setlib:strDecor,'/data');
(:~ String variable with the database location of decor/implementationguides collection :)
declare variable $setlib:strDecorDataIG     := concat($setlib:strDecor,'/implementationguides');
(:~ String variable with the database location of decor/example collection :)
declare variable $setlib:strDecorExample    := concat($setlib:strDecor,'/example');
(:~ String variable with the database location of version collection :)
declare variable $setlib:strDecorVersion    := concat($setlib:strDecor,'/releases');
(:~ String variable with the database location of decor/scheduled-tasks collection. This is where 
    requests for long running tasks should be picked up by a scheduled process :)
declare variable $setlib:strDecorScheduledTasks  := concat($setlib:strDecor,'/scheduled-tasks');
(:~ String variable with the database location of decor/services collection :)
declare variable $setlib:strDecorServices   := concat($setlib:strDecor,'/services');
(:~ String variable with the database location of decor/develop collection :)
(:declare variable $setlib:strDecorDevelop    := concat($setlib:strDecor,'/develop');:)
(:~ String variable with the database location of decor/history collection :)
declare variable $setlib:strDecorHistory    := concat($setlib:strDecor,'/history');
(:~ String variable with the database location of decor/tmp collection :)
declare variable $setlib:strDecorTemp       := concat($setlib:strDecor,'/tmp');
(:~ String variable with the database location of /decor/core/DECOR-implementationguide.xsd :)
declare variable $setlib:strDecorSchemaIG   := concat($setlib:strDecorCore,'/DECOR-implementationguide.xsd');

(:~ Collection variable with the database location of version collection :)
declare variable $setlib:colDecorVersion    := collection($setlib:strDecorVersion);
(:~ Collection variable with everything under decor/cache :)
declare variable $setlib:colDecorCache      := collection($setlib:strDecorCache);
(:~ Collection variable with everything under decor/data :)
declare variable $setlib:colDecorData       := collection($setlib:strDecorData);
(:~ Collection variable with everything under decor/implementationguides :)
declare variable $setlib:colDecorDataIG     := collection($setlib:strDecorDataIG);
(:~ Collection variable with everything under decor/example :)
declare variable $setlib:colDecorExample    := collection($setlib:strDecorExample);
(:~ String variable with path to ISO Schematron transformations to XSL :)
declare variable $setlib:strUtilISOSCH2SVRL := concat($setlib:strArtResources,'/iso-schematron');
(:~ Collection variable with everything under decor/core :)
declare variable $setlib:colDecorCore       := collection($setlib:strDecorCore);
(:~ Doc variable with DECOR.xsd contents:)
declare variable $setlib:docDecorSchema     := doc(concat($setlib:strDecorCore,'/DECOR.xsd'));
(:~ Doc variable with DECOR.xsd contents:)
declare variable $setlib:docDecorBasicSchema:= doc(concat($setlib:strDecorCore,'/DECOR-datatypes.xsd'));

(:~ String variable with everything under fhir :)
declare variable $setlib:strFhir            := concat($setlib:root,'fhir');

(:~ Collection variable with everything under hl7 :)
declare variable $setlib:strHl7             := concat($setlib:root,'hl7');
(:~ String variable to CDA stylesheet :)
declare variable $setlib:strCdaXsl          := concat($setlib:strHl7,'/CDAr2/xsl/CDA.xsl');

(:~ Collection variable with everything under lab :)
declare variable $setlib:strLab     := concat($setlib:root,'lab');
(:~ Collection variable with everything under lab-data :)
declare variable $setlib:strLabData := concat($setlib:root,'lab-data/data');

(:~ Collection variable with everything under terminology :)
declare variable $setlib:strTerminology     := concat($setlib:root,'terminology');
(:~ Collection variable with everything under terminology :)
declare variable $setlib:strTerminologyLogo := concat($setlib:root,'terminology/resources/logos');
(:~ Collection variable with everything under terminology-data :)
declare variable $setlib:strTerminologyData := concat($setlib:root,'terminology-data');
(:~ Collection variable with everything under terminology-data/codesystem-stable-data :)
declare variable $setlib:strCodesystemStableData := concat($setlib:root,'terminology-data/codesystem-stable-data');
(:~ Collection variable with everything under terminology-data/codesystem-authoring-data :)
declare variable $setlib:strCodesystemAuthoringData     := concat($setlib:root,'terminology-data/codesystem-authoring-data');
(:~ Collection variable with everything under terminology-data/codesystem-stable-data :)
declare variable $setlib:strValuesetStableData := concat($setlib:root,'terminology-data/valueset-stable-data');
(:~ Collection variable with everything under terminology-data/codesystem-authoring-data :)
declare variable $setlib:strValuesetAuthoringData        := concat($setlib:root,'terminology-data/valueset-authoring-data');
(:~ Collection variable with everything under terminology-data/codesystem-stable-data :)
declare variable $setlib:strConcepmapStableData          := concat($setlib:root,'terminology-data/conceptmap-stable-data');
(:~ Collection variable with everything under terminology-data/codesystem-authoring-data :)
declare variable $setlib:strConceptmapAuthoringData      := concat($setlib:root,'terminology-data/conceptmap-authoring-data');
(:~ Collection variable with everything under terminology-data/valueset-expansion-data :)
declare variable $setlib:strValuesetExpansionData         := concat($setlib:root,'terminology-data/valueset-expansion-data');

(:~ Collection variable with everything under dhd :)
declare variable $setlib:strDhd     := concat($setlib:root,'dhd');
(:~ Collection variable with everything under dhd-data :)
declare variable $setlib:strDhdData := concat($setlib:root,'dhd-data');

(:~ String variable with everything under xis :)
declare variable $setlib:strXis             := concat($setlib:root,'xis');
(:~ String variable with everything under xis resources :)
declare variable $setlib:strXisResources    := concat($setlib:root,'xis/resources');
(:~ String variable with the database location of xis-data collection :)
declare variable $setlib:strXisData         := concat($setlib:root,'xis-data');
(:~ String variable with everything under xis accounts :)
declare variable $setlib:strXisAccounts     := concat($setlib:strXisData,'/accounts');
(:~ String variable with the database location of xis-data/data collection :)
declare variable $setlib:strXisHelperConfig := concat($setlib:strXisData,'/data');
(:~ String variable with the database location of test accounts :)
declare variable $setlib:strTestAccounts    := concat($setlib:strXisData,'/test-accounts.xml');
(:~ String variable with the database location of test suites :)
declare variable $setlib:strTestSuites      := concat($setlib:strXisData,'/test-suites.xml');
(:~ String variable with the database location of test suites :)
declare variable $setlib:strSoapServiceList := concat($setlib:strXisData,'/soap-service-list.xml');

(:~ String variable with the database location of OIDS data collection :)
declare variable $setlib:strOidsData        := concat($setlib:root,'tools/oids-data');
(:~ Collection variable with everything under oids/data :)
declare variable $setlib:colOidsData        := collection(concat($setlib:root,'tools/oids-data'));
(:~ String variable with the database location of OIDS core collection :)
declare variable $setlib:strOidsCore        := concat($setlib:root,'tools/oids/core');
(:~ String variable with the database location of OIDS resources collection :)
declare variable $setlib:strOidsResources   := concat($setlib:root,'tools/oids/resources');
(:~ String variable for the name of the ISO schema :)
declare variable $setlib:strISO13582schema := 'iso-13582-2015.xsd';

(:~ String variable for the key to HL7 V2 Table 0396 mnemonics (https://terminology.hl7.org/CodeSystem-v2-0396.html) :)
declare variable $setlib:strKeyHL7v2Table0396CodePrefd := 'HL7-V2-Table-0396-Code';
(:~ String variable for the key to preferred FHIR URIs :)
declare variable $setlib:strKeyCanonicalUriPrefd       := 'HL7-FHIR-System-URI-Preferred';
(:~ String variable for the key to FHIR DSTU2 :)
declare variable $setlib:strKeyFHIRDSTU2               := 'DSTU2';
(:~ String variable for the key to preferred FHIR URIs in DSTU2 :)
declare variable $setlib:strKeyCanonicalUriPrefdDSTU2  := concat($setlib:strKeyCanonicalUriPrefd, '-', $setlib:strKeyFHIRDSTU2);
(:~ String variable for the key to FHIR STU3 :)
declare variable $setlib:strKeyFHIRSTU3                := 'STU3';
(:~ String variable for the key to preferred FHIR URIs in STU3 :)
declare variable $setlib:strKeyCanonicalUriPrefdSTU3   := concat($setlib:strKeyCanonicalUriPrefd, '-', $setlib:strKeyFHIRSTU3);
(:~ String variable for the key to FHIR R4 :)
declare variable $setlib:strKeyFHIRR4                  := 'R4';
(:~ String variable for the key to preferred FHIR URIs in R4 :)
declare variable $setlib:strKeyCanonicalUriPrefdR4     := concat($setlib:strKeyCanonicalUriPrefd, '-', $setlib:strKeyFHIRR4);
(:~ String variable for the key to FHIR R4B :)
declare variable $setlib:strKeyFHIRR4B                 := 'R4B';
(:~ String variable for the key to preferred FHIR URIs in R4B :)
declare variable $setlib:strKeyCanonicalUriPrefdR4B    := concat($setlib:strKeyCanonicalUriPrefd, '-', $setlib:strKeyFHIRR4B);
(:~ String variable for the key to FHIR R5 :)
declare variable $setlib:strKeyFHIRR5                  := 'R5';
(:~ String variable for the key to preferred FHIR URIs in R5 :)
declare variable $setlib:strKeyCanonicalUriPrefdR5     := concat($setlib:strKeyCanonicalUriPrefd, '-', $setlib:strKeyFHIRR5);
(:~ String variable for the key to FHIR URIs that exist, but are not preferred over other identifiers like OIDs or URIs :)
declare variable $setlib:strKeyCanonicalUri            := 'HL7-FHIR-System-URI';
(:~ Array variable containing all FHIR version strings :)
declare variable $setlib:arrKeyFHIRVersions            := ($setlib:strKeyFHIRDSTU2, $setlib:strKeyFHIRSTU3, $setlib:strKeyFHIRR4);

(:~ Returns the property key for the preferred canonical uri for a system for a given FHIR version :)
declare function setlib:strKeyCanonicalUriFhirPrefd($fhirVersion as xs:string?) as xs:string? {
    if ($fhirVersion = $setlib:arrKeyFHIRVersions) then
        string-join(($setlib:strKeyCanonicalUriPrefd, $fhirVersion), '-')
    else
    if ($fhirVersion = '1.0') then $setlib:strKeyCanonicalUriPrefdDSTU2 else
    if ($fhirVersion = '3.0') then $setlib:strKeyCanonicalUriPrefdSTU3 else
    if ($fhirVersion = '4.0') then $setlib:strKeyCanonicalUriPrefdR4 else
    if ($fhirVersion = '4.3') then $setlib:strKeyCanonicalUriPrefdR4B else
    if ($fhirVersion = '5.0') then $setlib:strKeyCanonicalUriPrefdR5
    else
        $setlib:strKeyCanonicalUriPrefd
};

(: Collection for storing check-decor results, e.g.
    /db/apps/decor/releases/abr/development/
:)
declare function setlib:strProjectDevelopment($projectPrefix as xs:string) as xs:string {
    concat(
        $setlib:strDecorVersion, '/', 
        replace($projectPrefix, '-$', ''), 
        '/development/'
    )
};
(: Full path for storing check-decor results, e.g.
    /db/apps/decor/releases/abr/development/abr-decorcheck-report.xml
   or if $doconly is false()
    abr-decorcheck-report.xml
:)
declare function setlib:strProjectDevelopmentDoc($projectPrefix as xs:string, $doconly as xs:boolean) as xs:string {
    if ($doconly) then
        concat($projectPrefix, 'decorcheck-report.xml')
    else (
        concat(setlib:strProjectDevelopment($projectPrefix), $projectPrefix, 'decorcheck-report.xml')
    )
};

(:~ Get current user name. effective user name if available or real user name otherwise. The function xmldb:get-current-user() was removed 
:   in eXist-db 5.0 and replaced with sm:id(). By centralizing the new slightly more complicated way of doing this, you can avoid making 
:   mistakes throughout the code base
:   @return user name string.
:   @since 2019-11-11
:)
declare function setlib:strCurrentUserName() as xs:string? {
    let $current-user-function  := function-lookup(xs:QName('xmldb:get-current-user'), 0)
    let $sm-id-function         := function-lookup(xs:QName('sm:id'), 0)
    
    return
    if (exists($current-user-function)) then 
        $current-user-function()
    else 
    if (exists($sm-id-function)) then (
        let $smid   := $sm-id-function()
        
        return
            ($smid//sm:effective/sm:username, $smid//sm:real/sm:username)[1]
    ) 
    else 
        'guest'
};
