xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:~
 : example implementation to use exist-JWT in combination with OAS-router
 :)
module namespace jwt-auth = "http://art-decor.org/ns/api/auth";

import module namespace jwt         = "http://existsolutions.com/ns/jwt";
import module namespace roaster     = "http://e-editiones.org/roaster";
import module namespace rutil       = "http://e-editiones.org/roaster/util";
import module namespace errors      = "http://e-editiones.org/roaster/errors";
import module namespace userapi     = "http://art-decor.org/ns/api/user" at "../user-api.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "settings-lib.xqm";

(:~ configure and get JWT instance :)
declare %private variable $jwt-auth:secret := collection($setlib:strSecureConfig)/authentication/secret;
declare %private variable $jwt-auth:token-lifetime := collection($setlib:strSecureConfig)/authentication/token-lifetime;
declare %private variable $jwt-auth:jwt := jwt:instance($jwt-auth:secret, $jwt-auth:token-lifetime);

(:~
 : The name of the securityScheme in API definition
 :)
declare variable $jwt-auth:METHOD := "JWTAuth";

(:~
 : The name of the securityScheme in API definition
 :)
declare variable $jwt-auth:handler := map { $jwt-auth:METHOD : jwt-auth:bearer-auth#1 };

(:~
 : which header to check for the token 
 : TODO: Authorization header seems to be swallowed by jetty
 : TODO: implement function to cut off scheme (BEARER )
 :)
declare variable $jwt-auth:AUTH_HEADER := "X-Auth-Token";

declare function jwt-auth:issue-token ($request as map(*)) {
    let $username               :=
        typeswitch ($request?body)
        case map(*) return $request?body?username
        case element() return ($request?body)/username
        case document-node() return ($request?body)/*/username
        default return ()
    let $password               :=
        typeswitch ($request?body)
        case map(*) return $request?body?password
        case element() return ($request?body)/password
        case document-node() return ($request?body)/*/password
        default return ()
    
    let $check                  :=
        if (empty($username) or empty($password)) then
            error($errors:BAD_REQUEST, "Missing parameters 'username' and/or 'password'")
        else ()
    
    let $loggedin               := xmldb:login("/db/apps/", $username, $password, false())
    let $user                   := rutil:getDBUser()
    let $fullname               := userapi:getUserDisplayName($user?name)
    let $defaultlanguage        := userapi:getUserLanguage($username, true())
    
    let $token                  := 
        if ($loggedin and $username = $user?name) then
            $jwt-auth:jwt?create($user)
        else (
            error($errors:UNAUTHORIZED, 'Username or password incorrect')
        )
            
    (: since this is a login: update user info :)
    let $update                 := userapi:setUserLastLoginTime(map { "name": $username }, $username, current-dateTime(), $jwt-auth:token-lifetime)

    return
        if (roaster:accepted-content-types()[contains(., 'xml')]) then
            roaster:response(201, 
                <response>
                    <user>
                        <name>{$user?name}</name>
                        <fullName>{$fullname}</fullName>
                        <defaultLanguage>{$defaultlanguage}</defaultLanguage>
                        <dba>{$user?dba}</dba>
                   {    for $grp in $user?groups return <groups>{$grp}</groups>}
                   </user>
                   <token>{$token}</token>
                   <issued>{current-dateTime()}</issued>
                   <expires>{current-dateTime() + xs:dayTimeDuration('PT' || $jwt-auth:token-lifetime || 'S')}</expires>
                </response>
            )
        else (
            roaster:response(201, map {
                "user": map {
                    "name": $user?name, 
                    "fullName": $fullname,
                    "defaultLanguage": $defaultlanguage,
                    "dba": $user?dba,
                    "groups": $user?groups
                },
                "token": $token,
                "issued": current-dateTime(),
                "expires": current-dateTime() + xs:dayTimeDuration('PT' || $jwt-auth:token-lifetime || 'S')
            })
        )
};

declare function jwt-auth:refresh-token ($request as map(*)) {
    let $disabled               :=
        error($errors:FORBIDDEN, 'This service has been disabled until further notice')
    
    let $authmap                := $request?user
    let $username               := $authmap?name
    let $check                  :=
        if (empty($authmap)) then
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    
    let $loggedin               := not(empty($authmap))
    let $user                   := $authmap
    let $fullname               := userapi:getUserDisplayName($username)
    let $defaultlanguage        := userapi:getUserLanguage($username, true())
    
    let $token                  := $jwt-auth:jwt?create($user)
    
    (: since this is a login: update user info :)
    let $update                 := userapi:setUserLastLoginTime(map { "name": $username }, $username, current-dateTime(), $jwt-auth:token-lifetime)

    return
        if (roaster:accepted-content-types()[contains(., 'xml')]) then
            roaster:response(201, 
                <response>
                    <user>
                        <name>{$username}</name>
                        <fullName>{$fullname}</fullName>
                        <defaultLanguage>{$defaultlanguage}</defaultLanguage>
                        <dba>{$user?dba}</dba>
                   {    for $grp in $user?groups return <groups>{$grp}</groups>}
                   </user>
                   <token>{$token}</token>
                   <issued>{current-dateTime()}</issued>
                   <expires>{current-dateTime() + xs:dayTimeDuration('PT' || $jwt-auth:token-lifetime || 'S')}</expires>
                </response>
            )
        else (
            roaster:response(201, map {
                "user": map {
                    "name": $username, 
                    "fullName": $fullname,
                    "defaultLanguage": $defaultlanguage,
                    "dba": $user?dba,
                    "groups": $user?groups
                },
                "token": $token,
                "issued": current-dateTime(),
                "expires": current-dateTime() + xs:dayTimeDuration('PT' || $jwt-auth:token-lifetime || 'S')
            })
        )
};

declare function jwt-auth:bearer-auth ($request as map(*)) as map(*)? {
    try {
        (: need to access request header directly because it will not be part of parameters :)
        let $token := request:get-header($jwt-auth:AUTH_HEADER)
        return
            if (exists($token))
            then (
                let $payload := $jwt-auth:jwt?read($token)
                return map {
                    "name": $payload?name,
                    "groups": $payload?groups,
                    "dba": $payload?dba,
                    "issued": jwt:epoch-to-dateTime($payload?iat),
                    "iat": $payload?iat
                }
            )
            else ()
    }
    catch too-old {
        error($errors:UNAUTHORIZED, "Token lifetime exceeded, please request a new one")
    }
    catch invalid-token | invalid-header | invalid-signature | future-date {
        error($errors:BAD_REQUEST, "token invalid")
    }
    catch * {
        error($errors:SERVER_ERROR, "Server error")
    }
};

declare function jwt-auth:is-dba-token ($request as map(*)) {
    rutil:debug($request)
};
declare function jwt-auth:test-token ($request as map(*)) {
    rutil:debug($request)
};
declare function jwt-auth:test-token-post ($request as map(*)) {
    rutil:debug($request)
};
