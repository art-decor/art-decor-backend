xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

module namespace utillibx       = "http://art-decor.org/ns/api/util-xpath";

declare namespace lab       = "urn:oid:2.16.840.1.113883.2.4.6.10.35.81";
declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";
declare namespace hl7       = "urn:hl7-org:v3";
declare namespace util      = "http://exist-db.org/xquery/util";

declare option exist:serialize "indent=no";
declare option exist:serialize "omit-xml-declaration=no";

declare function utillibx:getXpaths($decor as element(), $representingTemplate as element()) as node() {
(:  input: $decor node, $representingTemplate id
    output: <xpaths> containing an <xpath> for each concept in transaction
:)  
    let $nl           := "&#10;"
    let $tab          := "&#9;"
    let $lang         := data($decor//project/@defaultLanguage)
    
    let $tmid         := $representingTemplate/@ref
    let $tmed         := $representingTemplate/@flexibility 
    let $template     :=
        if ($tmed castable as xs:dateTime) then
            $decor//template[@id = $tmid][@effectiveDate = $tmed]
        else (
            let $tm   := $decor//template[@id = $tmid]
            let $tmmx := max($tm/xs:dateTime(@effectiveDate))
            return
                $tm[@effectiveDate = $tmmx]
        )
    let $xpaths       := 
        if (empty($template)) then 
            utillibx:report('Template not found: ' || $tmid || ' ' || $tmed)
        else
        (: todo ... make this smarter for non HL7 V3 templates that have no @root/@code/@codeSystem etc. :)
        if (1=1 or $template/classification[@format = 'hl7v3xml1'] or empty($template/classification/@format)) then 
            let $templateMap  := map:merge(
                for $tms in $decor//template
                let $tmid     := $tms/@id
                let $tmedmax  := max($tms/xs:dateTime(@effectiveDate))
                group by $tmid
                return
                    for $tm in $tms
                    let $tmed := $tm/@effectiveDate
                    let $ta   := $decor//templateAssociation[@templateId = $tmid][@effectiveDate = $tmed]
                    group by $tmed
                    return (
                        map:entry($tmid || $tmed, $ta | $tm[1]),
                        if ($tmed = $tmedmax) then map:entry($tmid || 'dynamic', $tm[1]) else ()
                    )
            )
            return
            element {name($template)} {
                $template/@*, 
                for $el in $template/(attribute | element | choice | include | template)
                return 
                    utillibx:getXpathFromTemplate($el, $decor, utillibx:getContextPath($template, ''), (), $representingTemplate, map { $template/@id || $template/@effectiveDate: "" }, $templateMap)
            }
        else ()
    return 
    <transactionXpaths ref="{$representingTemplate/../@id}">
    {
        (: copy atts from transaction :) 
        $representingTemplate/../(@* except @id)
    }
        <templateWithXpaths>{$xpaths}</templateWithXpaths>
    </transactionXpaths>
};

declare %private function utillibx:report($error as xs:string) as element() {
    element error {$error}
};

declare %private function utillibx:getXpathFromTemplate($node as element()*, $decor as element()*, $xpath as xs:string, $overrides as element()?, $representingTemplate as node(), $templateTrail as map(*), $templateMap as map(*)) as node()* {
    (:  input: some node, all templates and associated concepts, Xpath up to node
        output: if some template//element has @id, outputs all associated concepts with @ref, @effectiveDate, @elementId and xpath
    
        recursively walks through templates, resolving contains and includes and choices :)

    (: do elements and attributes :)
    let $xpath := 
        if ($node[self::template]) then 
            (utillibx:getContextPath($node,$xpath))
        else 
        if ($node[self::element]) then 
            (concat($xpath, if ($xpath=('/','//')) then () else ('/'), $node/@name)) 
        else 
        if ($node[self::attribute]) then 
            (concat($xpath, '/@', $node/@name)) 
        else 
            ($xpath) 
    
    (:let $actualIsMandatory :=
        switch (($overrides/@isMandatory, $node[self::element]/@isMandatory)[1])
        case 'true' return attribute isMandatory {'true'}
        default return attribute isMandatory {'false'}
        
    (\: for mandatory elements, use 'M' :\)
    let $actualConformance :=
        if ($actualIsMandatory = 'true') then 
            attribute conformance {'M'} 
        else (
            ($overrides/@conformance, $node[self::element]/@conformance, attribute conformance {'O'})[1]
        )
    
    (\: if nothing specified, then max=* :\) 
    let $actualMaximumMultiplicity :=
        if ($overrides/@maximumMultiplicity) then
            $overrides/@maximumMultiplicity
        else 
        if ($node[self::element][@maximumMultiplicity]) then
            $node/@maximumMultiplicity
        else attribute maximumMultiplicity {'*'}
    
    (\: mandatory elements have min=1, if nothing specified, then min=0 :\) 
    let $actualMinimumMultiplicity :=
        if ($actualIsMandatory = 'true') then attribute minimumMultiplicity {'1'} 
        else 
        if ($overrides/@minimumMultiplicity) then
            $overrides/@minimumMultiplicity
        else 
        if ($node[self::element][@minimumMultiplicity]) then
            $node/@minimumMultiplicity
        else attribute minimumMultiplicity {'0'}:)
    
    (: add root to xpath for id :)
    let $xpathpart  :=
        for $attr in $node/attribute/@root | $node/attribute[@name = 'root']/@value
        return
            '@root=''' || replace($attr, '''', '''''') || ''''
    let $xpath := 
        if (empty($xpathpart)) then $xpath else (
            $xpath || '[' || string-join($xpathpart, ' or ') || ']'
        )

    (: add code/codesystem :)
    let $xpathpart  :=
        for $attr in $node/vocabulary[@code | @codeSystem]
        let $predcode       := if ($attr/@code)       then '@code='''       || replace($attr/@code, '''', '''''') || '''' else ()
        let $predcodeSystem := if ($attr/@codeSystem) then '@codeSystem=''' || replace($attr/@codeSystem, '''', '''''') || '''' else ()
        return
            if ($predcode and $predcodeSystem) then '(' || string-join(($predcode, $predcodeSystem), ' and ') || ')' else ($predcode, $predcodeSystem)
    let $xpath      := 
        if (empty($xpathpart)) then $xpath else (
            $xpath || '[' || string-join($xpathpart, ' or ') || ']'
        )
    
    (: HL7 datatype :)
    let $hl7Type := 
        if ($node[self::element][@datatype])
        then attribute hl7Type {$node/@datatype} else ()

    (:  for include and @contains, pass on multiplicities, conformance, mandatory
        for templates, pass on again to element(s) :)
    let $overrides := 
        if ($node[self::include] | $node[@contains]) then
            <overrides>{$node/@*}</overrides>
        else $overrides
    
    let $template   := ()
return
    if ($node[self::attribute][@id] | $node[self::element] | $node[self::choice] | $node[self::include] | $node[self::template])
    then 
    (: output the element :)
        element {name($node)} 
        {   (: with it's own attributes :)
            $node/@*, 
            (: xpath so far with HL7Type:)
            attribute xpath {$xpath}, $hl7Type,
            (: for elements, associated concepts :)
            if ($node[self::attribute][@id] | $node[self::element][@id]) 
            then (
                let $tmid           := $node/ancestor::template[1]/@id
                let $tmed           := $node/ancestor::template[1]/@effectiveDate
                let $concepts       := map:get($templateMap, $tmid || $tmed)/concept[@elementId = $node/@id][@ref = $representingTemplate/concept/@ref]
                (:element actualCardConf {$actualMinimumMultiplicity, $actualMaximumMultiplicity, $actualConformance, $actualIsMandatory},:)
                (: return a concept for every templateAssociation/concept which corresponds to this element/@id and occurs in representingTemplate being processed :)
                return
                    if ($concepts) then
                        <associatedConcepts>{for $concept in  $concepts return <concept>{$concept/@ref, $concept/@effectiveDate}</concept>}</associatedConcepts>
                    else (
                        (:<associatedConcepts>{$tmid, $tmed}</associatedConcepts>:)
                    )
            )
            else (),
            (: for includes, include template :)
            if ($node[self::include]) 
            then (
                let $include    := map:get($templateMap, $node/@ref || ($node/@flexibility, 'dynamic')[1])[self::template]
                return 
                    if (map:contains($templateTrail, $include/@id || $include/@effectiveDate)) then (
                        (:circular reference found. now what?:)
                        <include>{$node/@*}<warning>Circular reference detected</warning></include>
                    ) else (
                        <include>{$node/@*, utillibx:getXpathFromTemplate($include, $decor, $xpath, $overrides, $representingTemplate, map:merge(($templateTrail, map { $include/@id || $include/@effectiveDate: ""})), $templateMap)}</include>
                    )
            )
            else (),
            (: ditto for contains :)
            if ($node[self::element][@contains]) 
            then (
                let $include    := map:get($templateMap, $node/@contains || ($node/@flexibility, 'dynamic')[1])[self::template]
                return 
                    if (map:contains($templateTrail, $include/@id || $include/@effectiveDate)) then (
                        (:circular reference found. now what?:)
                        <warning>Circular reference detected</warning>
                    ) else (
                        utillibx:getXpathFromTemplate($include, $decor, $xpath, $overrides, $representingTemplate, map:merge(($templateTrail, map { $include/@id || $include/@effectiveDate: ""})), $templateMap)
                    )
            )
            else (),
            (: process the children :)
            for $el in $node/(attribute | element | choice | include | template)
            return 
                utillibx:getXpathFromTemplate($el, $decor, $xpath, (), $representingTemplate, map:merge(($templateTrail, map { $template/@id || $template/@effectiveDate: ""})), $templateMap)
        }
    else ()
};

declare %private function utillibx:getContextPath($template as node(), $xpath as xs:string) as xs:string {
(:  input:  template, xpath up to that template
    output: context path for template
:)
    (: Look if there's a templateId node which matches template/@id :)
    let $templateIdNode     := 
        $template//element[@minimumMultiplicity][xs:integer(@minimumMultiplicity) gt 0][attribute/@root = $template/@id][@name = ('hl7:templateId', 'cda:templateId')] |
        $template//element[@minimumMultiplicity][xs:integer(@minimumMultiplicity) gt 0][attribute[@name = 'root']/@value = $template/@id][@name = ('hl7:templateId', 'cda:templateId')]
    let $predContent        := 
        for $attr in $templateIdNode/attribute/@root | $templateIdNode/attribute[@name = 'root']/@value
        return
            concat("@root='", $attr ,"'")
    return
    switch ($template/context[1]/@id)
    case '*' return (
        (:  situation 1: template has <context id="*"/>
            - template must have template id
            - it does not have a fixed containing element (i.e. template's content may reside in element with any name :)
        let $pred := if (empty($predContent)) then '' else (concat("hl7:templateId[",string-join($predContent,' or '),"]"))
        return
            if ($xpath and $pred) then 
                (concat($xpath,'[',$pred,']')) 
            else if (not($xpath) and $pred) then
                (concat('*[',$pred,']')) 
            else 
                ($xpath)
    )
    case '**' return (
        (:  situation 2: template has <context id="**"/>
            - template must have template id
            - it does have a fixed containing element (i.e. template's content resides in element with a particular name :)
        let $initialelementName := $template/element[1]/@name
        let $pred := if (string-length($predContent[1])>0) then (concat($initialelementName,"[hl7:templateId[",string-join($predContent,' or '),"]]")) else ($initialelementName)
        return
            if ($xpath and $pred) then 
                (concat($xpath,'[',$pred,']')) 
            else if (not($xpath) and $pred) then 
                (concat('*[',$pred,']')) 
            else 
                ($xpath)
    )
    default return (
        (:  situation 3: template has <context> without @id
            - if template has path in context (<context path='...'/>, use path 
            - template may have template id
            - if template has no path in context, make path from code and codeSystem  
        :)
        (: if context/@path, use it without further ado :)
        if ($template/context[1]/@path)
        (: if <context path='/'/> do not output the '/', leading slash is already appended for each element :)
        then $template/context[1]/@path 
        else (
            (: situation 4: ask AH to check this, since he may understand this :) 
            let $predContent := 
                if ($template/element[@name = ('hl7:code', 'cda:code')][@minimumMultiplicity='1'][exists(vocabulary/@codeSystem)]) then (
                    for $attr in $template/element[@name = ('hl7:code', 'cda:code')]/vocabulary[@code | @codeSystem]
                    let $predcode       := if ($attr/@code)       then '@code='''       || replace($attr/@code, '''', '''''') || '''' else ()
                    let $predcodeSystem := if ($attr/@codeSystem) then '@codeSystem=''' || replace($attr/@codeSystem, '''', '''''') || '''' else ()
                    return
                        '(' || string-join(($predcode, $predcodeSystem), ' and ') || ')'
                ) else 
                if ($template/element[1]/element[@name = ('hl7:code', 'cda:code')][@minimumMultiplicity='1'][exists(vocabulary/@codeSystem)]) then (
                    for $attr in $template/element[1]/element[@name = ('hl7:code', 'cda:code')]/vocabulary[@code | @codeSystem]
                    let $predcode       := if ($attr/@code)       then '@code='''       || replace($attr/@code, '''', '''''') || '''' else ()
                    let $predcodeSystem := if ($attr/@codeSystem) then '@codeSystem=''' || replace($attr/@codeSystem, '''', '''''') || '''' else ()
                    return
                        '(' || string-join(($predcode, $predcodeSystem), ' and ') || ')'
                )
                else ()
            let $pred := 
                if (empty($predContent)) then '' else (
                    if ($template/element[@name = ('hl7:code', 'cda:code')]) then (
                        concat("hl7:code[", string-join($predContent,' or '),"]")
                    )
                    else (
                        concat($template/element[1]/@name, "[hl7:code[",string-join($predContent,' or '),"]]")
                    )
                )
            let $ctxPath := $template/context[1]/@path
            let $outPath :=
                if (not($xpath)) then '*' else $xpath
            let $outPath :=
                if ($ctxPath) then concat($outPath, if (starts-with($ctxPath, '/')) then () else '/', $ctxPath) 
                else ($outPath)
            let $outPath :=
                if ($pred) then (concat($outPath,'[',$pred,']'))
                else $outPath
            return $outPath
        )
    )
};

