xquery version "3.1";

declare namespace api                   = "http://art-decor.org/ns/api/api";
declare namespace output                = "http://www.w3.org/2010/xslt-xquery-serialization";

import module namespace roaster         = "http://e-editiones.org/roaster";

import module namespace rutil           = "http://e-editiones.org/roaster/util";
import module namespace errors          = "http://e-editiones.org/roaster/errors";
import module namespace auth            = "http://e-editiones.org/roaster/auth";

(:~
 : For the bearer token authentication example to work install following packages 
 : - exist-jwt 1.0.1
 : - crypto-lib 1.0.0
 :)
(:import module namespace jwt-auth="https://e-editiones.org/oas-router/xquery/jwt-auth" at "jwt-auth.xqm";:)
import module namespace jwt-auth        = "http://art-decor.org/ns/api/auth" at "jwt-auth.xqm";

import module namespace errorslib       = "http://art-decor.org/ns/api/errors" at "errors-lib.xqm";
import module namespace serverapi       = "http://art-decor.org/ns/api/server" at "../server-api.xqm";
import module namespace servermgmtapi   = "http://art-decor.org/ns/api/server-mgmt" at "../server-mgmt-api.xqm";
import module namespace apiversion      = "http://art-decor.org/ns/api/apiversion" at "../api-api.xqm";
import module namespace csapi           = "http://art-decor.org/ns/api/codesystem" at "../codesystem-api.xqm";
import module namespace deapi           = "http://art-decor.org/ns/api/concept" at "../concept-api.xqm";
import module namespace mpapi           = "http://art-decor.org/ns/api/conceptmap" at "../conceptmap-api.xqm";
import module namespace dsapi           = "http://art-decor.org/ns/api/dataset" at "../dataset-api.xqm";
import module namespace ggapi           = "http://art-decor.org/ns/api/governancegroups" at "../governancegroups-api.xqm";
import module namespace isapi           = "http://art-decor.org/ns/api/issue" at "../issue-api.xqm";
import module namespace lockapi         = "http://art-decor.org/ns/api/lock" at "../lock-api.xqm";
import module namespace mcapi           = "http://art-decor.org/ns/api/mycommunity" at "../mycommunity-api.xqm";
import module namespace newsapi         = "http://art-decor.org/ns/api/news" at "../news-api.xqm";
import module namespace oidregistry     = "http://art-decor.org/ns/api/oidregistry" at "../oidregistry-api.xqm";
import module namespace fpapi           = "http://art-decor.org/ns/api/profile" at "../profile-api.xqm";
import module namespace prapi           = "http://art-decor.org/ns/api/project" at "../project-api.xqm";
import module namespace qapi            = "http://art-decor.org/ns/api/questionnaire" at "../questionnaire-api.xqm";
import module namespace scapi           = "http://art-decor.org/ns/api/scenario" at "../scenario-api.xqm";
import module namespace tcsapi          = "http://art-decor.org/ns/api/terminology/codesystem" at "../terminology-codesystem-api.xqm";
import module namespace tmapi           = "http://art-decor.org/ns/api/template" at "../template-api.xqm";
import module namespace trepapi         = "http://art-decor.org/ns/api/terminology/report" at "../terminology-report-api.xqm";
import module namespace testapi         = "http://art-decor.org/ns/api/test" at "../test-api.xqm";
import module namespace userapi         = "http://art-decor.org/ns/api/user" at "../user-api.xqm";
import module namespace vsapi           = "http://art-decor.org/ns/api/valueset" at "../valueset-api.xqm";

(:~
 : list of definition files to use
 :)
declare variable $api:definitions := (
    "modules/library/api.json"
);

(:~
 : You can add application specific route handlers here.
 : Having them in imported modules is preferred.
 :)

(:~
 : This function "knows" all modules and their functions
 : that are imported here 
 : You can leave it as it is, but it has to be here
 :)
declare function api:lookup ($name as xs:string) {
    function-lookup(xs:QName($name), 1)
};

(:~
 : Define authentication/authorization middleware 
 : with a custom authentication strategy
 : 
 : All securitySchemes in any api definition that is
 : included MUST have an entry in this map.
 : Otherwise the router will throw an error. 
 :)
declare variable $api:use-custom-authentication := auth:use-authorization($jwt-auth:handler);

(:~
 : Example of a app-specific middleware that
 : will add the "beep" field to each request
 :)
declare function api:use-beep-boop ($request as map(*)) as map(*) {
    map:put($request, "beep", "boop")
};

declare variable $api:use := (
    $api:use-custom-authentication(:,
    api:use-beep-boop#1:)
);

roaster:route($api:definitions, api:lookup#1, $api:use)
