xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
module namespace permissionslib         = "http://art-decor.org/ns/api/permissions";
import module namespace setlib          = "http://art-decor.org/ns/api/settings" at "settings-lib.xqm";
import module namespace utillib          = "http://art-decor.org/ns/api/util" at "util-lib.xqm";
import module namespace repo            = "http://exist-db.org/xquery/repo";

declare namespace sm            = "http://exist-db.org/xquery/securitymanager";
declare namespace util          = "http://exist-db.org/xquery/util";
declare namespace xforms        = "http://www.w3.org/2002/xforms";
declare namespace xxforms       = "http://orbeon.org/oxf/xml/xforms";
declare namespace xhtml         = "http://www.w3.org/1999/xhtml";
declare namespace xs            = "http://www.w3.org/2001/XMLSchema";
declare namespace f             = "http://hl7.org/fhir";

(:  Mode            Octal
 :  rw-r--r--   ==  0644
 :  rw-rw-r--   ==  0664
 :  rwxr-xr--   ==  0754
 :  rwxr-xr-x   ==  0755
 :  rwxrwxr-x   ==  0775
 :)

(:install path for art (normally /db/apps/), includes trailing slash :)
declare variable $permissionslib:root   := concat(repo:get-root(), 'api/');

(:~ Call to fix any potential permissions problems in the path /db/apps/api/modules
:   Dependency: $setlib:strArt, $setlib:strArtResources, $setlib:strArtData
:)
declare function permissionslib:setApiPermissions() {
    permissionslib:checkIfUserDba(),
    
    sm:chown(xs:anyURI(concat($permissionslib:root, 'modules')),'admin:decor'),
    sm:chmod(xs:anyURI(concat($permissionslib:root, 'modules')), 'rwxr-sr-x'),
    sm:clear-acl(xs:anyURI(concat($permissionslib:root, 'modules'))),
    
    permissionslib:setPermissions(concat($permissionslib:root, 'modules'), 'api-user', 'decor', 'rwxr-sr-x', 'api-user', 'decor', 'rwsr-sr-x'),
    
    for $adminapi in ('user-api.xqm', 'library/api-admin.xql', 'library/periodic-notifier.xql', 'library/scheduled-refreshs.xql', 'library/scheduled-notifier.xql', 'library/scheduled-hoover-sth.xql')
    let $path   := concat($permissionslib:root, 'modules/', $adminapi)
    return (
        sm:chown(xs:anyURI($path), 'api-admin')
    ),
    
    for $res in xmldb:get-child-resources(xs:anyURI($permissionslib:root))
    return (
        (:sm:chown(xs:anyURI(concat($permissionslib:root,'/',$res)), 'admin:decor'),:)
        if ($res = 'api.html') then
            sm:chmod(xs:anyURI(concat($permissionslib:root,'/',$res)), 'rwxr-xr-x')
        else ()
    ),
    
    (: create to set a document /dp/secure/security.xml to settle some secutrity features and keys :)
    if (xmldb:collection-available($setlib:strSecureConfig)) then () else xmldb:create-collection('/db', substring-after($setlib:strSecureConfig, '/db/')),

    if (doc-available($setlib:strSecureConfig || '/security.xml')) then () else (
        xmldb:store($setlib:strSecureConfig, 'security.xml', document {
                comment { ' ART-DECOR security ' } |
                <authentication>
                    <!-- 
                        This resource supports ART-DECOR security aspects such as
                            - the ART-DECOR API login process based on JWT tokens
                            - ADBot token for automated processes
                    
                        Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
                        see https://docs.art-decor.org/copyright/
                    -->
                    <!-- 
                        This is this server's signature for cloud operations
                    -->
                    <server signature="---server-signature---"/>
                    <!-- 
                        token-lifetime: how long will the token be valid in seconds: 
                        - 30 minutes = 30*60 seconds = 1800 s
                        - 1 hour = 60*60 seconds = 3600 s
                        - 4 hours 4*60*60 seconds = 14440 s
                        - 8 hours = 8*60*60 = 28800 s
                    -->
                    <token-lifetime>28800</token-lifetime>
                    <!-- 
                        secret: used in token generation and validation, 
                        this should be unique on every server to avoid token reuse across servers.
                        
                        You may change this value to anything later on.
                        This renders tokens based on the previous value invalid.
                    -->
                    <secret>{util:uuid()}</secret>
                    <!--
                        ADBot token for automated processes
                    -->
                    <adbot username="adbot" token="---adbot-token---"/>
                </authentication>
            }
        )
    ),
    
    permissionslib:setPermissions($setlib:strSecureConfig, 'api-user', 'dba', 'r-xrws---', 'api-user', 'dba', 'r-srws---')
};

(: Call to fix any potential permissions problems in the paths:
:       /db/apps/decor/cache
:       /db/apps/decor/data
:       /db/apps/decor/releases
:   Dependency: $setlib:strArtData, $setlib:strDecorCache, $setlib:strDecorData, $setlib:strDecorVersion
:   NOTE: path /db/apps/decor/core has its own installer
:)
declare function permissionslib:setDecorPermissions() {
    permissionslib:checkIfUserDba(),
    sm:chown(xs:anyURI(repo:get-root() || '/decor'),'admin:decor'),
    sm:chmod(xs:anyURI(repo:get-root() || '/decor'),sm:octal-to-mode('0775')),
    sm:clear-acl(xs:anyURI(repo:get-root() || '/decor')),
    
    permissionslib:setPermissions($setlib:strDecorCache,   'admin', 'decor', 'rwxrwsr-x', 'admin', 'decor', 'rw-r--r--'),
    permissionslib:setPermissions($setlib:strDecorData,    'admin', 'decor', 'rwxrwsr-x', 'admin', 'decor', 'rw-rw-r--'),
    if (xmldb:collection-available($setlib:strDecorHistory)) then () else (
        xmldb:create-collection(string-join(tokenize($setlib:strDecorHistory,'/')[not(position()=last())],'/'), tokenize($setlib:strDecorHistory,'/')[last()])
    ),
    permissionslib:setPermissions($setlib:strDecorHistory, 'admin', 'decor', 'rwxrwsr-x', 'admin', 'decor', 'rw-rw-r--')
    ,
    permissionslib:setPermissions($setlib:strDecorDataIG, 'admin', 'decor', 'rwxrwsr-x', 'admin', 'decor', 'rw-rw-r--')
    ,
    (:if (xmldb:collection-available($setlib:strDecorDevelop)) then (
        permissionslib:setPermissions($setlib:strDecorDevelop, 'admin', 'decor', 'rwxrwsrwx', 'admin', 'decor', 'rw-rw-r--')
    ) else ()
    ,:)
    if (xmldb:collection-available($setlib:strDecorVersion)) then (
        permissionslib:setPermissions($setlib:strDecorVersion, 'admin', 'decor', 'rwxrwsr-x', 'admin', 'decor', 'rw-r--r--')
        ,
        for $coll in xmldb:get-child-collections($setlib:strDecorVersion)
        let $devcoll1   := concat($setlib:strDecorVersion,'/',$coll,'/development')
        let $devcoll2   := concat($setlib:strDecorVersion,'/',$coll,'/version-development')
        return (
            if (xmldb:collection-available($devcoll1)) then (
                permissionslib:setPermissions($devcoll1, 'admin', 'decor', 'rwxrwsr-x', 'admin', 'decor', 'rw-rw-r--')
            ) else (),
            if (xmldb:collection-available($devcoll2)) then (
                permissionslib:setPermissions($devcoll2, 'admin', 'decor', 'rwxrwsr-x', 'admin', 'decor', 'rw-rw-r--')
            ) else ()
        )
    ) else ()
    ,
    xmldb:create-collection(string-join(tokenize($setlib:strDecorTemp,'/')[not(position()=last())],'/'), tokenize($setlib:strDecorTemp,'/')[last()])
    ,
    permissionslib:setPermissions($setlib:strDecorTemp, 'admin', 'decor', 'rwxrwsrwx', 'admin', 'decor', 'rw-rw-rw-')
    ,
    xmldb:create-collection(string-join(tokenize($setlib:strDecorScheduledTasks,'/')[not(position()=last())],'/'), tokenize($setlib:strDecorScheduledTasks,'/')[last()])
    ,
    permissionslib:setPermissions($setlib:strDecorScheduledTasks, 'admin', 'decor', 'rwxrwsr--', 'admin', 'decor', 'rw-rw----')
};

(: Helper function with recursion for :setDecorPermissions() :)
declare function permissionslib:setPermissions($path as xs:string, $collusrown as xs:string?, $collgrpown as xs:string?, $collmode as xs:string, $resusrown as xs:string?, $resgrpown as xs:string?, $resmode as xs:string) {
    if (string-length($collusrown) = 0) then () else sm:chown(xs:anyURI($path),$collusrown),
    if (string-length($collgrpown) = 0) then () else sm:chgrp(xs:anyURI($path),$collgrpown),
    sm:chmod(xs:anyURI($path),$collmode),
    sm:clear-acl(xs:anyURI($path)),
    for $res in xmldb:get-child-resources(xs:anyURI($path))
    return (
        if (empty($resusrown)) then () else sm:chown(xs:anyURI(concat($path,'/',$res)),$resusrown),
        if (empty($resgrpown)) then () else sm:chgrp(xs:anyURI(concat($path,'/',$res)),$resgrpown),
        sm:chmod(xs:anyURI(concat($path,'/',$res)),$resmode),
        sm:clear-acl(xs:anyURI(concat($path,'/',$res)))
    )
    ,
    for $collection in xmldb:get-child-collections($path)
    return
        permissionslib:setPermissions(concat($path,'/',$collection), $collusrown, $collgrpown, $collmode, $resusrown, $resgrpown, $resmode)
};

declare %private function permissionslib:checkIfUserDba() {
    if (sm:is-dba(setlib:strCurrentUserName())) then () else (
        error(xs:QName('permissionslib:NotAllowed'), concat('Only dba user can use this module. ',setlib:strCurrentUserName()))
    )
};

(:~ Add patches we need to do at post-install of the api installation :)
declare function permissionslib:applyPostInstallPatches() {

    (: fix permissions (any are needed) :)
    let $fix                        := permissionslib:setPermissions($setlib:strDecorData, 'admin', 'decor', 'rwxrwsr-x', 'admin', 'decor', 'rw-rw-r--')
    let $ifx                        := permissionslib:setDecorPermissions()
    
    (: ======== create or update all CADTS CodeSystems ======== :)
    let $now                        := substring(string(current-dateTime()), 1, 19)
    let $projectPrefixes            := '*'
    let $write                      := 
        for $projectPrefix in $projectPrefixes
        let $stamp                  := util:uuid()
        let $projectcheck-request   :=  
            <projectcodesystemconvert-request uuid="{$stamp}" for="{$projectPrefix}" on="{current-dateTime()}" as="{$stamp}" by="{setlib:strCurrentUserName()}"
                                              progress="Added to process queue ..." progress-percentage="0"/>
        let $write                  := xmldb:store($setlib:strDecorScheduledTasks, 'projectcodesystemconvert-' || $projectPrefix[not(. = '*')] || replace($now, '\D', '') || '.xml', $projectcheck-request)
        let $tt                     := sm:chmod($write, 'rw-rw----')
        return
            ()
    (: ======== / create or update all CADTS CodeSystems ======== :)
    
(: ------------------------------------------------------------------------------------------------------------ :)

    (: ======== update all decor/ids/baseId and defaultBaseIds to match the DECOR.xsd DecorObjectTypes ======== :)
    (:<baseId id="2.16.840.1.113883.3.1937.99.62.3.1" type="DS" prefix="demo1-"/>:)
    (:<defaultBaseId id="2.16.840.1.113883.3.1937.99.62.3.1" type="DS"/>:)
    let $baseIdTypes                := $setlib:docDecorSchema//xs:simpleType[@name="DecorObjectType"]//xs:enumeration/@value
    let $update                     :=
        for $decorIds in $setlib:colDecorData/decor/ids
        let $projectPrefix          := $decorIds/ancestor::decor/project/@prefix
        let $projectId              := $decorIds/ancestor::decor/project/@id
        let $missingBaseIds         := $baseIdTypes[not(. = $decorIds/baseId/@type)]
        let $missingDefaultBaseIds  := $baseIdTypes[not(. = $decorIds/defaultBaseId/@type)]
        let $maxBaseId              := max($decorIds/(baseId | defaultBaseId)/xs:integer(tokenize(substring-after(@id, $projectId || '.'), '\.')[1]))
        return (
            for $mbi at $i in $missingBaseIds
            (: trying to give a reasonable label based on <xforms:label xml:lang="en-US">Dataset</xforms:label> :)
            let $mbiType          := (replace(lower-case($mbi/..//*:label[@xml:lang = 'en-US']), '\s', ''), lower-case($mbi))[string-length() gt 0][1]
            let $newBaseId        := <baseId id="{$projectId}.{$maxBaseId + $i}" type="{$mbi}" prefix="{$projectPrefix || $mbiType || '-'}"/>
            let $newDefaultBaseId := <defaultBaseId id="{$projectId}.{$maxBaseId + $i}" type="{$mbi}"/>
            return (
                update insert $newBaseId following $decorIds/baseId[last()],
                (: insert as defaultBaseId using the same id :)
                if ($missingDefaultBaseIds[. = $mbi]) then
                    update insert $newDefaultBaseId following $decorIds/defaultBaseId[last()]
                else ()
            ),
            for $mbi at $i in $missingDefaultBaseIds[not(. = $missingBaseIds)]
            let $baseId           := $decorIds/baseId[@type = $mbi][last()]
            let $newDefaultBaseId := <defaultBaseId id="{$baseId/@id}" type="{$mbi}"/>
            return
                update insert $newDefaultBaseId following $decorIds/defaultBaseId[last()]
        )
    (: ======== / update all decor/ids/baseId and defaultBaseIds to match the DECOR.xsd DecorObjectTypes ======== :)
    
(: ------------------------------------------------------------------------------------------------------------ :)

    (: ======== delete all DECOR dataset concept history ======== :)
    let $update                     := update delete $setlib:colDecorData//datasets//history[not(ancestor::history)]
    (: ======== / delete all DECOR dataset concept history ======== :)
    
(: ------------------------------------------------------------------------------------------------------------ :)

    (: ======== add new attributes to DECOR index ======== :)
    (: AD30-1125 Added new active, cache, release attributes that need indexing. No need to reindex and don't want to reindex as they did not exist before, just add definition :)
    (: rules/questionnaireAssociation/@questionnaireId :)
    (: rules/questionnaireAssociation/@questionnaireEffectiveDate :)
    (: transaction/representingTemplate/@representingQuestionnaire :)
    (: transaction/representingTemplate/@representingQuestionnaireFlexibility :)
    let $extraIndexes           :=
        <collection xmlns="http://exist-db.org/collection-config/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
            <index>
                <range>
                    <create qname="@questionnaireId" type="xs:string"/>
                    <create qname="@questionnaireEffectiveDate" type="xs:string"/>
                    <create qname="@representingQuestionnaire" type="xs:string"/>
                    <create qname="@representingQuestionnaireFlexibility" type="xs:string"/>
                </range>
            </index>
        </collection>
    let $updateIndexes          :=
        for $coll in ('decor/cache', 'decor/data', 'decor/releases')
        let $index-file := concat('/db/system/config', repo:get-root(), $coll, '/collection.xconf')
        return
            if (doc-available($index-file)) then (
                for $ei in $extraIndexes//*:create
                return
                    if (doc($index-file)//*:index/*:range/*:create[@qname = $ei/@qname]) then () else (
                        update insert $ei into doc($index-file)//*:index/*:range
                    )
            ) 
            else ((: moved? anyway ... not adding anything today :))
    (: / ======== add new attributes to DECOR index ======== :)
    
(: ------------------------------------------------------------------------------------------------------------ :)

    (: ======== move questionnaire from decor/scenarios to decor/rules and add questionnaireResponse ======== :)
    (: AD30-1125 Move all active, cached, and released /decor/scenarios/questionnaire to /decor/rules/questionnaire so we just work from there instead of being left wondering :)
    (: Build questionnaireAssociation contents as best we can from item/@linkId:)
    let $moveQuestionnaires     :=
        for $qq in $setlib:colDecorData//scenarios/questionnaire | $setlib:colDecorCache//scenarios/questionnaire | $setlib:colDecorVersion//scenarios/questionnaire
        let $decor  := $qq/ancestor::decor[1]
        (: <relationship type="DRIV" ref="2.16.840.1.113883.3.1937.99.60.10.4.2" flexibility="2022-02-16T00:00:00"/> :)
        let $tr     := if ($qq/relationship[@type = 'DRIV']) then utillib:getTransaction($qq/relationship[@type = 'DRIV'][1]/@ref, $qq/relationship[@type = 'DRIV'][1]/@flexibility) else ()
        let $ds     := if ($tr/representingTemplate/@sourceDataset) then utillib:getDataset($tr/representingTemplate/@sourceDataset, $tr/representingTemplate/@sourceDatasetFlexibility) else ()
        let $qqc    := comment {' ' || data($qq/name[1]) || ' '}
        let $qqa    := 
            <questionnaireAssociation questionnaireId="{$qq/@id}" questionnaireEffectiveDate="{$qq/@effectiveDate}">
            {
                (: demo10-dataelement-84 :)
                (: 2.16.840.1.113883.3.1937.99.60.10.2.3 :)
                (: 2.16.840.1.113883.3.1937.99.60.10.2.3--20220103000000 :)
                for $qqi in $qq//item[not(@type='display')]/@linkId
                let $deid     := 
                    if (contains($qqi, '--')) then substring-before($qqi, '--') else
                    if (matches($qqi, '-\d+$')) then (
                        (:<baseId id="2.16.840.1.113883.3.1937.99.60.10.2" type="DE" prefix="demo10-dataelement-"/>:)
                        let $deid     := replace($qqi, '.*-(\d+)$', '$1')
                        let $depfx    := replace($qqi, '\d+$', '')
                        return
                            $decor/ids/baseId[@type = 'DE'][@prefix =  $depfx]/@id || '.' || $deid
                    )
                    else (
                        $qqi
                    )
                let $deed     := if (contains($qqi, '--')) then replace(substring-after($qqi, '--'), '\D', '') else ()
                let $deed     := if (string-length($deed) = 14) then substring($qqi, 1, 4) || '-' || substring($qqi, 5, 2) || '-' || substring($qqi, 7, 2) || 'T' || substring($qqi, 9, 2) || ':' || substring($qqi, 11, 2) || ':' || substring($qqi, 13, 2) else ()
                return
                    if (utillib:isOid($deid)) then
                        if ($deed castable as xs:dateTime) then
                            <concept ref="{$deid}" effectiveDate="{$deed}" elementId="{$qqi}"/>
                        else (
                            let $concept  := if ($ds) then $ds//concept[@id = $deid] else $setlib:colDecorData//concept[@id = $deid]
                            return
                                if (count($concept[@effectiveDate]) = 1) then
                                    <concept ref="{$deid}" effectiveDate="{$concept/@effectiveDate}" elementId="{$qqi}"/>
                                else ()
                        )
                    else (
                        comment { 'Does not point to a dataset concept: ' || $qqi || ' ' }
                    )
            }
            </questionnaireAssociation>
        return (
            if ($decor[rules]) then
                update insert ($qqc, $qqa, $qq) into $decor/rules
            else 
            if ($decor/issues) then 
                update insert <rules>{$qqc, $qqa, $qq}</rules> preceding $decor/issues
            else (
                update insert <rules>{$qqc, $qqa, $qq}</rules> into $decor
            )
            ,
            update delete $qq
        )
    (: / ======== move questionnaire from decor/scenarios to decor/rules and add questionnaireResponse ======== :)
    
(: ------------------------------------------------------------------------------------------------------------ :)

    (: ======== move DECOR questionnaireresponse from decor/scenarios to new collection prototypes as FHIR object ======== :)
    (: AD30-1048 Move all /decor/scenarios/questionnaireResponse to a folder prototypes/Bundle-{fhirVersion}-QuestionnaireResponse.xml so we just work from there instead of being left wondering :)
    (: Check/update questionnaire/@canonicalUri compared to QuestionnaireResponse.questionnaire :)
    let $moveQuestionnaires     :=
        for $qr in $setlib:colDecorData//questionnaireresponse
        let $fhirVersion  := $qr/@fhirVersion
        let $bundleColl   := xmldb:create-collection(util:collection-name($qr), 'prototypes')
        let $bundleName   := 'Bundle-' || $fhirVersion || '-QuestionnaireResponse.xml'
        
        let $doc          :=
            if (doc-available($bundleColl || $bundleName)) then doc($bundleColl || $bundleName)/f:Bundle else (
                doc(xmldb:store($bundleColl, $bundleName, document {
                    processing-instruction xml-model {'href="../../../../../fhir/' || $fhirVersion || '/resources/schemas/fhir-invariants.sch" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"'},
                    <Bundle xmlns="http://hl7.org/fhir" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://hl7.org/fhir ../../../../../fhir/{$fhirVersion}/resources/schemas/fhir-single.xsd">
                        <type value="collection"/>
                    </Bundle>
                }))/f:Bundle
            )
        let $fhirId   := concat($qr/@id, '--', replace($qr/@effectiveDate,'\D',''))
        let $canUri   := if ($qr/@canonicalUri) then $qr/@canonicalUri else $utillib:strFhirCanonicalBaseURL || 'QuestionnaireResponse/' || $fhirId
        let $update   := update insert <entry xmlns="http://hl7.org/fhir"><fullUrl value="{$canUri}"/><resource>{$qr/f:QuestionnaireResponse}</resource></entry> into $doc
        let $storedQr := $doc//f:QuestionnaireResponse[last()]
        let $update   := update value $storedQr/f:id/@value with $fhirId
        let $update   := if ($storedQr/f:meta) then () else update insert <meta xmlns="http://hl7.org/fhir"/> following $storedQr/f:id[last()]
        let $lastUpdated  := if ($qr/@lastModifiedDate) then $qr/@lastModifiedDate || '+00:00' else current-dateTime()
        let $update   := 
            if ($storedQr/f:meta) then
                if ($storedQr/f:meta/f:lastUpdated) then 
                    update value $storedQr/f:meta/f:lastUpdated/@value with $lastUpdated
                else
                if ($storedQr/f:meta/*) then
                   update insert <lastUpdated xmlns="http://hl7.org/fhir" value="{$lastUpdated}"/> preceding $storedQr/f:meta/f:*[1]
                else (
                    update insert <lastUpdated xmlns="http://hl7.org/fhir" value="{$lastUpdated}"/> into $storedQr/f:meta
                )
            else (
                update insert <meta xmlns="http://hl7.org/fhir"><lastUpdated value="{$lastUpdated}"/></meta> following $storedQr/f:id[last()]
            )
        let $source   := $utillib:strAD3ServerURL || '#/' || $qr/ancestor::decor/project/@prefix || '/project/overview'
        let $update   := 
            if ($storedQr/f:meta/f:source) then 
                update value $storedQr/f:meta/f:source/@value with $source
            else (
                update insert <source xmlns="http://hl7.org/fhir" value="{$source}"/> following $storedQr/f:meta/f:lastUpdated[last()]
            )
        let $tag      :=
            if ($storedQr/f:meta/f:tag[f:system/@value = 'http://hl7.org/fhir/FHIR-version']) then 
                update value $storedQr/f:meta/f:tag[f:system/@value = 'http://hl7.org/fhir/FHIR-version']/f:code/@value with $fhirVersion
            else (
                update insert <tag xmlns="http://hl7.org/fhir"><system value="http://hl7.org/fhir/FHIR-version"/><code value="{$fhirVersion}"/></tag> into $storedQr/f:meta
            )
        (: <relationship type="ANSW" ref="2.16.840.1.113883.3.1937.99.60.10.27.1" flexibility="2022-02-16T00:00:00"/> :)
        let $qq       := if ($qr/relationship[@type = 'ANSW']) then utillib:getQuestionnaire($qr/relationship[@type = 'ANSW'][1]/@ref, $qr/relationship[@type = 'ANSW'][1]/@flexibility) else ()
        let $fhirId   := concat($qq/@id, '--', replace($qq/@effectiveDate,'\D',''))
        (: get canonicalUri from questionnaire or create for questionnaire :)
        let $canUri   := if ($qq/@canonicalUri) then $qq/@canonicalUri else $utillib:strFhirCanonicalBaseURL || 'Questionnaire/' || $fhirId
        (: if we can find the questionnaire, make sure the canonicalUri in QuestionnaireResponse.questionnaire and that in questionnaire/@canonicalUri actually align
           in the new setup, there is no relationship element anymore
        :)
        let $update   := 
            if ($qq) then ( 
                (: update canonicalUri in questionnaire/@canonicalUri. Maybe it already matched ... maybe it did not :)
                if ($qq/@canonicalUri) then update value $qq/@canonicalUri with $canUri else update insert attribute canonicalUri {$canUri} into $qq,
                (: update canonicalUri in QuestionnaireResponse.questionnaire. Maybe it already matched ... maybe it did not :)
                update value $qr/f:questionnaire/@value with $canUri
            ) else ()
        return
            update delete $qr
    (: / ======== move DECOR questionnaireresponse from decor/scenarios to new collection prototypes as FHIR object ======== :)
    
(: ------------------------------------------------------------------------------------------------------------ :)

        return ()
};