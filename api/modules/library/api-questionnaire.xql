xquery version "3.1";

declare namespace api                   = "http://art-decor.org/ns/api/api-questionnaire";
declare namespace output                = "http://www.w3.org/2010/xslt-xquery-serialization";

import module namespace roaster         = "http://e-editiones.org/roaster";

(:import module namespace rutil           = "http://e-editiones.org/roaster/util";
import module namespace errors          = "http://e-editiones.org/roaster/errors";:)
import module namespace auth            = "http://e-editiones.org/roaster/auth";

(:~
 : For the bearer token authentication example to work install following packages 
 : - exist-jwt 1.0.1
 : - crypto-lib 1.0.0
 :)
(:import module namespace jwt-auth="https://e-editiones.org/oas-router/xquery/jwt-auth" at "jwt-auth.xqm";:)
import module namespace jwt-auth        = "http://art-decor.org/ns/api/auth" at "jwt-auth.xqm";

import module namespace errorslib       = "http://art-decor.org/ns/api/errors" at "errors-lib.xqm";
import module namespace qapi            = "http://art-decor.org/ns/api/questionnaire" at "../questionnaire-api.xqm";

(:~
 : list of definition files to use
 :)
declare variable $api:definitions := (
    "modules/library/api.json"
);

(:~
 : You can add application specific route handlers here.
 : Having them in imported modules is preferred.
 :)

(:~
 : This function "knows" all modules and their functions
 : that are imported here 
 : You can leave it as it is, but it has to be here
 :)
declare function api:lookup ($name as xs:string) {
    function-lookup(xs:QName($name), 1)
};

(:~
 : Define authentication/authorization middleware 
 : with a custom authentication strategy
 : 
 : All securitySchemes in any api definition that is
 : included MUST have an entry in this map.
 : Otherwise the router will throw an error. 
 :)
declare variable $api:use-custom-authentication := auth:use-authorization($jwt-auth:handler);

(:~
 : Example of a app-specific middleware that
 : will add the "beep" field to each request
 :)
declare function api:use-beep-boop ($request as map(*)) as map(*) {
    map:put($request, "beep", "boop")
};

declare variable $api:use := (
    $api:use-custom-authentication(:,
    api:use-beep-boop#1:)
);

roaster:route($api:definitions, api:lookup#1, $api:use)
