xquery version "3.1";
(:
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get       = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";

import module namespace userapi   = "http://art-decor.org/ns/api/user" at "../user-api.xqm";
import module namespace serverapi = "http://art-decor.org/ns/api/server" at "../server-api.xqm";
import module namespace setlib    = "http://art-decor.org/ns/api/settings" at "settings-lib.xqm";
import module namespace utillib   = "http://art-decor.org/ns/api/util" at "util-lib.xqm";

import module namespace errors    = "http://e-editiones.org/roaster/errors";

(:
    Email through Sendmail from eXist about recently changed issues per user per trigger
:)
declare namespace mail            = "http://exist-db.org/xquery/mail";
declare namespace datetime        = "http://exist-db.org/xquery/datetime";
declare namespace xmldb           = "http://exist-db.org/xquery/xmldb";
declare namespace sm              = "http://exist-db.org/xquery/securitymanager";

(: 
 : some constants presets
:)
declare variable $MISSING   := "*MISSING*";
declare variable $TALKATIVE := false();

(:css settings in html head goes poof somewhere in mail relay, so use style attribute instead:)

declare %private variable $LOCALES             := $setlib:strApiLocales;
declare %private variable $UPDATETIMESTAMP     := current-dateTime();

(:
    get parameters from the call
    
    sendmail
        if set to "true" the emails will really be sent out and 
        user records will be updated; in any other case the
        summary of the potentially processed records are returned
    mysender
        the email address denotes the sender
:)

declare variable $local:sendmail external;
declare variable $local:mysender external;

(: 
    debug variables
:)
let $thedebugaddress := () (:debug only, send everything to this address:)

(: check availability of required parameters, fail if undefined or empty :)
let $doSendmail :=
    if (
        try { $local:sendmail != '' }
        catch err:XPDY0002 {
            let $err := util:log('ERROR', 'periodic-notifier: error +++ variable/parameter "sendmail" not set in server scheduler configuration')
            let $err := error($errors:BAD_REQUEST,'Required periodic-notifier variable/parameter "sendmail" not set in server scheduler configuration')
            return false()
        }
    )
    then
        $local:sendmail = 'true'
    else
        let $err := util:log('ERROR', 'periodic-notifier: error +++ variable/parameter "sendmail" not set in server scheduler configuration')
        let $err := error($errors:BAD_REQUEST,'Required periodic-notifier variable/parameter "sendmail" not set in server scheduler configuration')
        return false()

let $mysender :=
    if (
        try { $local:mysender != '' }
        catch err:XPDY0002 {
            let $err := util:log('ERROR', 'periodic-notifier: error +++ variable/parameter "mysender" not set in server scheduler configuration')
            let $err := error($errors:BAD_REQUEST,'Required periodic-notifier variable/parameter "mysender" not set in server scheduler configuration')
            return ''
         }
     )
     then
        $local:mysender
     else
        let $err := util:log('ERROR', 'periodic-notifier: error +++ variable/parameter "mysender" not set in server scheduler configuration')
        let $err := error($errors:BAD_REQUEST,'Required periodic-notifier variable/parameter "mysender" not set in server scheduler configuration')
        return ''
        
(:
    local variable settings
:)
let $userInfoCollectionStr := $setlib:strUserInfo

let $deeplinkprefix := "---"
let $deeplinkprefix := doc(concat($setlib:strArtData,'/server-info.xml'))/server-info/url-art-decor-deeplinkprefix

let $onoff   := if ($doSendmail) then 'on' else 'off'
let $check   := util:log('INFO', 'periodic-notifier: starting for "' || $mysender || '", sending email is ' || $onoff)

(: multi language form resource :)
let $userlang-map               :=
    map:merge(
        for $author in $get:colDecorData//project[@id][not(ancestor::decor/issues/@notifier='off')]/author[@email][@username[not(. = 'guest')]]
        let $username   := $author/@username
        group by $username
        return
            map:entry($username, userapi:getUserLanguage($username))
    )

(:  presets for formatted current date :)
let $now := format-dateTime(current-dateTime(), '[Y0001]-[M01]-[D01] [H01]:[m01]:[s01]')

(:
    Run through the notification tasks and notify users if necessary about recent events on artefacts and publications
    
    PROCESS is
    
        LOOP: hush through every project (that does not have the issue notifier switch set to 'off')
        
        with those projects, then
        
        LOOP: hush through every user (author, contrinutor) of every project with an email address and notifier on
        
        with those users, then process the following two sections
        
            SECTION I: process all issues 
                determine per user whether he has already gotten a message about the latest changes
                of the respective issue or he needs to get such a message
            SECTION II: process all publications, check whether there is a new release publication for this project
                determine per user whether he has already gotten a message about the latest changes
                of the respective publications or he needs to get such a message
:)
  
let $notifyresult :=
    if (sm:has-access($userInfoCollectionStr, 'rw')) then
        <notify>
        {
            (:
                go thru every project, that does not have the notifier switch set to 'off'
            :)
            for $pa in $get:colDecorData//project[@id][@prefix='hl7itig-'][not(ancestor::decor/issues/@notifier='off')]
            let $issues             := $pa/ancestor::decor/issues
            let $projectprefix      := $pa/@prefix
            let $projectlanguage    := $pa/@defaultLanguage
            let $projectname        := $pa/name[@language=$projectlanguage]/text()
            (: 
                plan the notifications per project
            :)
            return
                <project projectid="{$pa/@id}" projectname="{$projectname}" 
                    projectprefix="{$projectprefix}" projectlanguage="{$projectlanguage}" 
                    issuecount="{count($issues/issue)}" notifier="on">
                {
                    (: bench :)
                    (::)
                    let $check := if ($TALKATIVE) then util:log('INFO', 'periodic-notifier: project "' || $projectprefix || '"') else ()
                    (::)
                    
                    (:
                        create list of the project authors (no guest users) 
                        with an email and notifier ON and that are active users in system
                    :)
                    let $activeauthors :=
                        for $ac in $pa/author[@email][@notifier='on'][not(@username='guest')]
                        let $user := $ac/@username
                        return
                            if (sm:is-account-enabled($user)) then $ac else ()

                    (: 
                        go thru every user of this project with an email address and notifier on 
                    :)
                    for $pu in $activeauthors
                    (: store user id :)
                    let $userid             := $pu/@id
                    (: store user name :)
                    let $user               := $pu/@username
                    (: store user preferred language :)
                    let $userlang           := map:get($userlang-map, $user)
                    
                    (: get the desired locale or en-US as a fallback :)
                    let $l1        := util:binary-doc(concat($LOCALES, '/', $userlang, '.json'))
                    let $l2        :=
                        if (string-length($l1) = 0)
                        then util:binary-doc(concat($LOCALES,'/en-US.json'))
                        else $l1
                    let $l3        := util:binary-to-string($l2)
                    let $locale    := parse-json($l3)
                    
                    (: store email of this user :)
                    let $tmpeml             := userapi:getUserEmail($user)
                    let $email              :=
                        if ($thedebugaddress) 
                        then $thedebugaddress 
                        else if (starts-with($tmpeml, 'mailto:')) 
                        then substring-after($tmpeml, 'mailto:') 
                        else string($tmpeml)
                    (: when was the last time issues were notified, if never (or error) then assume "very long ago" :)
                    let $tmplin             := userapi:getUserLastIssueNotify($user)
                    let $lastIssueNotify    := 
                        if (empty($tmpeml))
                        then (xs:dateTime('2100-01-01T00:00:00'))  (: unsure that user exists, some very future date, so gets no notification :)
                        else if (empty($tmplin))
                        then (xs:dateTime('1981-01-01T00:00:00'))  (: some very past future date, so they get all notification :)
                        else $tmplin
                    (: bench :)
                    (::)
                    let $check := if ($TALKATIVE) then util:log('INFO', 'periodic-notifier: ...user "' || $user || '" ' || $email || ' last ' || $lastIssueNotify) else ()
                    (::)
                    (: store notifications :)
                    let $notificationperuser :=
                        <notifies user="{$user}" lastIssueNotify="{$lastIssueNotify}">
                        {
                            (: 
                                SECTION I: process all issues 
                                determine per user whether he has already gotten a message about the latest changes
                                of the respective issue or he needs to get such a message
                            :)
                            for $i at $issuecount in $issues/issue
                            let $issueid                    := $i/@id/string()
                            let $userissubscribed           := userapi:userHasIssueSubscription($user, $issueid)
                            (: only the issue # :)
                            let $issuenumber                := tokenize($issueid, '\.')[last()]
                            (: title of the issue :)
                            let $issuetitle                 := $i/@displayName
                            
                            let $maxEffEvent                := max(($i/tracking/xs:dateTime(@effectiveDate), $i/assignment/xs:dateTime(@effectiveDate)))
                            let $lastTouchedObject          := $i/tracking[@effectiveDate = $maxEffEvent] | $i/assignment[@effectiveDate = $maxEffEvent]
                            
                            (: only tracking has statusCode :)
                            let $lastTouchedObjectStatus    := ($lastTouchedObject/@statusCode)[last()]
                            let $lastTouchedAuthorIsUserId  := $lastTouchedObject[last()]/author[@id = $userid]
                            
                            return
                                (:only if user is subscribed and is not the last author in the issue:)
                                if ($userissubscribed and empty($lastTouchedAuthorIsUserId)) then (
                                    <issue issueid="{$issueid}" issuenumber="{$issuenumber}" issuetitle="{$issuetitle}" laststatus="{$lastTouchedObjectStatus}">
                                    {
                                        (: return all issue's trackings or assignments touched after $lastIssueNotify for this user :)
                                        for $ii in ($i/tracking | $i/assignment)
                                        (: when tracking|assignment was last touched :)
                                        let $thisTouch          := $ii/@effectiveDate
                                        let $thisTouchFormatted := 
                                            if ($ii/@effectiveDate castable as xs:dateTime) 
                                            then format-dateTime(xs:dateTime($ii/@effectiveDate),'[Y0001]-[M01]-[D01] [H01]:[m01]', (), (), ()) 
                                            else $ii/@effectiveDate
                                        (: touched by whom :)
                                        let $modifiedby      :=
                                            if ($ii/author[string-length(.)>0])
                                            then $ii/author[string-length(.)>0][1]/text()
                                            else if ($ii/author[string-length(@id)>0])
                                            then $ii/author[string-length(@id)>0][1]/@id
                                            else 'unknown'
                                        return
                                            if ($lastIssueNotify < $thisTouch) then
                                                <notify issueid="{$issueid}" issuenumber="{$issuenumber}" issuetitle="{$issuetitle}"
                                                    modifier="{$modifiedby}" what="{$ii/name()}" assignmentto="{$ii/@name}"
                                                    touched="{$thisTouchFormatted}">
                                                {
                                                     $ii/@statusCode,
                                                     $ii/@labels,
                                                     $ii
                                                }
                                                </notify>
                                            else ()
                                      }
                                      </issue>
                                ) else <none>{$userissubscribed}</none>
                        }
                        {
                            (: 
                                SECTION II: process all publications, check whether there is a new release publication for this project
                                determine per user whether he has already gotten a message about the latest changes
                                of the respective publications or he needs to get such a message
                                        
                            :)
                            for $rel in $pa/release
                            let $thisTouch   := 
                                if ($rel/@date castable as xs:dateTime)
                                    then ($rel/@date)
                                    else (xs:dateTime('1981-01-01T00:00:00'))
                            let $thisTouchFormatted := 
                            if ($rel/@date castable as xs:dateTime) 
                                then format-dateTime(xs:dateTime($rel/@date),'[Y0001]-[M01]-[D01] [H01]:[m01]', (), (), ()) 
                                else $rel/@date
                            return
                                if ($lastIssueNotify < $thisTouch) then (
                                <notify release="{$thisTouchFormatted}" by="{$rel/@by}" statusCode="{$rel/@statusCode}" versionLabel="{$rel/@versionLabel}">
                                {
                                    $rel/note
                                }
                                </notify>
                            ) else ()
                        }
                        </notifies>
                        
                    (: 
                        After SECTION I and II all notofication requests are collected in "notifies"
                        go thru all notifications for this user, prepare message and send email, one per project 
                    :)
                    let $message1 :=
                        if (count($notificationperuser//notify[@issueid])=0) then () else (
                            <mail>
                                <from>{$mysender}</from>
                                <to>{$email}</to>
                                <subject>{concat($locale?changeonissues, ' ', $projectname)}</subject>
                                <message>
                                    <xhtml>
                                        <html>
                                            <head>
                                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                                <title>{concat($locale?changeonissues, ' ', $projectname)}</title>
                                                <style>{$utillib:emailCssStyles}</style>
                                            </head>
                                            <body>
                                            {
                                                <div class="container">
                                                    <div class="main-body">
                                                    {
                                                        <h3>{concat($locale?changeonissues, ' ', $projectname, ' ', $locale?as-of, ' ', $now)}</h3>,
                                                        <span>{concat($locale?compiledforuser, ' ', $user, ' (', $email, '). ', $locale?dontreply)}</span>,
                                                        <p> </p>,
                                                        for $issue in $notificationperuser/issue[notify[@issueid]]
                                                        let $iheading := concat($locale?issue, ' #', $issue/@issuenumber, ': ', $issue/@issuetitle)
                                                        let $istatus := if (string-length($issue/@laststatus)>0) then concat(' [', $locale?issue-status ,': ', $issue/@laststatus, '].') else '.'
                                                        return (
                                                            <div class="row gutters-sm">
                                                                <div>
                                                                    <div class="card">
                                                                        <div class="card-body">
                                                                            <div class="mb-3">
                                                                                <strong>{$iheading}{$istatus}</strong>
                                                                            </div>
                                                                            <div>
                                                                            {
                                                                                for $nf at $icnt in $issue/notify
                                                                                let $txt := concat(
                                                                                    if ($nf/@what='tracking') then 
                                                                                        $locale?anewtracking 
                                                                                    else if ($nf/@what='assignment') then 
                                                                                        $locale?anewassignment 
                                                                                    else 
                                                                                        concat($locale?anew, ' ''',$nf/@what,''' ', $locale?hasbeenadded)
                                                                                    , ' ', $nf/@modifier
                                                                                    , ' ', $locale?at, ' ', $nf/@touched
                                                                                    , 
                                                                                    if ($nf/@what='assignment') then 
                                                                                        concat(', ', $locale?nowassignedto,' ', $nf/@assignmentto) 
                                                                                    else ''
                                                                                    , '.'
                                                                                )
                                                                                let $cstatusclass := 
                                                                                    if ($nf/@what='assignment')
                                                                                    then 'mb-3'
                                                                                    else
                                                                                        concat('status-', 
                                                                                            if ($nf/@statusCode='closed') then 'green' 
                                                                                            else if ($nf/@statusCode=('rejected','deferred','cancelled')) then 'blue' 
                                                                                            else 'red')
                                                                                return
                                                                                    <div class="{$cstatusclass}">
                                                                                    {
                                                                                        <span class="{if ($nf/@what='assignment') then '' else 'newlabel'}">{$txt}</span>,
                                                                                        if ($nf/@statusCode) then 
                                                                                            (' ', $locale?status, ': ', $nf/@statusCode/string())
                                                                                        else (),
                                                                                        for $label in tokenize($nf/@labels,'\s')
                                                                                        let $selectedLabelColor := $issues/labels/label[@code=$label]/@color
                                                                                        let $selectedLabelName  := $issues/labels/label[@code=$label]/@name
                                                                                        return
                                                                                            <div title="{$selectedLabelName}" class="labelouterbox">
                                                                                                <div style="background-color:{$selectedLabelColor}; display: inline; padding-left: 10px; margin: -3px;">&#160;</div>
                                                                                                <div class="labelinnerbox">
                                                                                                {
                                                                                                    $label
                                                                                                }
                                                                                                </div>
                                                                                            </div>,
                                                                                        for $nd in $nf/(tracking|assignment)/desc[.//text()[string-length()>0]]
                                                                                        return
                                                                                            <p>{$nd/node()}</p>
                                                                                    }
                                                                                    </div>
                                                                            }
                                                                            </div>
                                                                            <div>
                                                                                 <div class="mt-3">
                                                                                     <a href="{$deeplinkprefix}{$projectprefix}/issues/issues/{$issue/@issueid}"><button class="button">Visit</button></a>
                                                                                     <i>{$locale?directlink2}</i>
                                                                                 </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>,
                                                            <p> </p>
                                                        ),
                                                        <p style="text-align: center; font-size: -1">
                                                         {
                                                             $locale?you-are-receiving-this-because, 
                                                             ' ', 
                                                             <a href="{$deeplinkprefix}/home" alt="" title="{$locale?unsubscribe-hint}">{$locale?unsubscribe-here}</a>, 
                                                             '. '
                                                         }
                                                         </p>
                                                     }
                                                        </div>
                                                </div>
                                                }
                                            </body>
                                        </html>
                                    </xhtml>
                                </message>
                            </mail>
                        )
                    let $message2 :=
                        if (count($notificationperuser//notify[@release])=0) then () else (
                            <mail>
                                    <from>{$mysender}</from>
                                    <to>{$email}</to>
                                    <subject>{concat($locale?changeonreleases, ' ', $projectname)}</subject>
                                    <message>
                                        <xhtml>
                                            <html>
                                                <head>
                                                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                                    <title>{concat($locale?changeonreleases, ' ', $projectname)}</title>
                                                    <style>{$utillib:emailCssStyles}</style>
                                                </head>
                                                <body>
                                                    {
                                                        <div class="container">
                                                            <div class="main-body">
                                                            {
                                                                <h3>{concat($locale?changeonreleases, ' ', $projectname, ' ', $locale?as-of, ' ', $now)}</h3>,
                                                                <span>{concat($locale?compiledforuser, ' ', $user, ' (', $email, '). ')}</span>,
                                                                <p> </p>,
                                                                for $rel in $notificationperuser//notify[@release]
                                                                return (
                                                                    <div>
                                                                        <div class="card">
                                                                            <div class="card-body">
                                                                                <div class="mb-3">
                                                                                    <strong>
                                                                                    {
                                                                                        concat($locale?release, ' ', $rel/@release/string(), ' - ', 
                                                                                            $locale?action-by, ': ', $rel/@by/string(), ' - ',
                                                                                            $locale?status, ': ', $rel/@statusCode/string(), ' - ',
                                                                                            $locale?versionLabel, ': ', $rel/@versionLabel/string())
                                                                                    }
                                                                                    </strong>
                                                                                </div>
                                                                                <div>
                                                                                {
                                                                                    let $cstatusclass :=
                                                                                        concat('status-', 
                                                                                            if ($rel/@statusCode='active') then 'green' 
                                                                                            else if ($rel/@statusCode=('draft','pending')) then 'orange' 
                                                                                            else 'grey')
                                                                                    return
                                                                                        <div class="{$cstatusclass}" style="margin: 10px 0 0 10px; padding: 3px;">
                                                                                        {
                                                                                            $rel/*
                                                                                        }
                                                                                        </div>
                                                                                }
                                                                                </div>
                                                                                <div>
                                                                                    <div class="mt-3">
                                                                                         <a href='{$deeplinkprefix}{$projectprefix}/project/publication'>
                                                                                             <button class="button">Visit</button>
                                                                                         </a>
                                                                                         <i>{$locale?directlink3}</i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>,
                                                                    <p> </p>
                                                                ),
                                                                <p style="text-align: center; font-size: -1">
                                                                {
                                                                    $locale?you-are-receiving-this-because, 
                                                                    ' ', 
                                                                    <a href="{$deeplinkprefix}/home" alt="" title="{$locale?unsubscribe-hint}">{$locale?unsubscribe-here}</a>, 
                                                                    '. '
                                                                }
                                                                </p>
                                                            }
                                                            </div>
                                                    </div>
                                                    }
                                                </body>
                                            </html>
                                        </xhtml>
                                    </message>
                                </mail>
                        )

                    (: send emails :)
                    let $smtp := ()   (:~ use localhost :)
                    let $sent1 :=
                        try {
                            if ($doSendmail) then mail:send-email($message1, $smtp, "UTF-8") else true()
                        }
                        catch * {
                            false()
                        }
                    let $sent2 :=
                        try {
                             if ($doSendmail) then mail:send-email($message2, $smtp, "UTF-8") else true()
                        }
                        catch * {
                            false()
                        }

                    (: update user notifications, if sucessfully send email :)
                    let $authmap := map { "name": "adbot", "groups": [ "dba" ] } (: fake authmap, we are already authenticated :)
                    let $usernotifyupdate :=
                        if ($doSendmail and ($sent1 or count($message1//mail) = 0) and ($sent2 or count($message2//mail) = 0)) then (
                            for $up in $notificationperuser
                            let $upuser := $up/@user
                            let $oldnot := $up/@lastIssueNotify
                            group by $upuser
                            return
                                let $updateUser         := 
                                    if (empty(userapi:getUserInfo($upuser)) or not(sm:is-account-enabled($upuser)))
                                    then $MISSING
                                    else userapi:setUserLastIssueNotify($authmap, $upuser, $UPDATETIMESTAMP) 
                                let $nownot             := userapi:getUserLastIssueNotify($upuser)
                                let $check              := if ($TALKATIVE) then util:log('INFO', 'periodic-notifier: notify user=' || $upuser || ' old=' || $oldnot || ' new=' || $nownot) else ()
                                return <usernotifyupdate user="{$upuser}" updated="true" old="{$oldnot}" new="{$nownot}"/>
                        ) else (
                            for $up in $notificationperuser
                            let $upuser := $up/@user
                            let $oldnot := $up/@lastIssueNotify
                            let $nownot := userapi:getUserLastIssueNotify($upuser)
                            let $check  := if ($TALKATIVE) then util:log('INFO', 
                                'periodic-notifier: not notify user=' || $upuser || ' old=' || $oldnot || ' new=' || $nownot || ' issue=' || $sent1 || ' publication=' || $sent2) else ()
                            return <usernotifyupdate user="{$upuser}" updated="false" old="{$oldnot}" new="{$nownot}"/>
                        )
                        
                    return
                        <statistics 
                            user="{$user}"
                            issues="{count($issues/issue)}" 
                            releases="{count($pa/release)}"
                            mails="{count($message1//mail) + count($message2//mail)}"
                            sentok="{$sent1 and $sent2}">
                        {
                            $usernotifyupdate
                        }
                        </statistics>
                }
                </project>
        }
        </notify>
    else (
        let $check := util:log('ERROR', 'periodic-notifier: unsufficient access rights, check admin docs')
        return
            <unauthorized/>
    )
  
(: summary :)
let $nprojects := count($notifyresult/project)
let $nissues := sum($notifyresult/project/statistics/@issues)
let $nreleases := sum($notifyresult/project/statistics/@releases)
let $nmails := count($notifyresult/project/statistics/@mails)
let $success := 
    if (not($doSendmail)) then "deferred" else if ($notifyresult/project/statistics/@sentok) then "success" else "failed"
let $check := 
    if ($nprojects = 0)
    then util:log('WARN', 'periodic-notifier: no projects found to process')
    else util:log('INFO', 'periodic-notifier summary: project: ' || $nprojects || ', issues: ' || $nissues || ', releases: ' || $nreleases || ', mails: ' || $nmails || ' - ' || $success)

return
   <result status="{$success}-send-{$onoff}" project="{$nprojects}" issues="{$nissues}" releases="{$nreleases}" mails="{$nmails}">
   {
       $notifyresult
   }
   </result>