xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

module namespace git           = "http://art-decor.org/ns/api/git";

import module namespace http        ="http://expath.org/ns/http-client";
import module namespace compression ="http://exist-db.org/xquery/compression";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace json   = "http://www.json.org";
declare namespace f      = "http://hl7.org/fhir";


(:~
 : Blacklist - these files are not checkout from git and are ignored by default
 :)
declare variable $git:blacklist := ["build.xml", "expath-pkg.xml", "repo.xml", "controller.xql", ".gitignore", "collection.xconf", ".github"];

(:~
 : DB User and Permissions as fallback if "permissions" not set in repo.xml
 :)
declare variable $git:sm := map {
    "user"  : "nobody",
    "group" : "nogroup",
    "mode"  : "rw-r--r--"
};

(:~
:   scripts to access github or gitlab or bitbucket repositories
:   test modus for now
:
:   overall story see here: https://art-decor.atlassian.net/browse/AD30-1021
:)

(: test modus config, read-only, token unused/unset yet
REPOINFO := 
  map {
    "vcs"        : "github",
    "baseurl"    : "https://api.github.com/",
    "repo"       : "artdecorgittest",
    "owner"      : "kheitmann",
    "ref"        : "master",
    "token"      : "xxxxxxx",
    "hookuser"   :  "admin",
    "hookpasswd" : "yyyyyyy"
};
:)

(:~
:    Return the list of all folders in Git repository specified in $repoinfo
:)
declare function git:getGitFolderStructure ($repoinfo as map(*)) as element()* {
    let $r := local:getGitFolderStructure($repoinfo, '')
    return
        <list type="folder" url="{$repoinfo?baseurl}" owner="{$repoinfo?owner}" repo="{$repoinfo?repo}" branch="{$repoinfo?ref}">
        {
            $r/@status,
            $r/@message,
            $r/folder
        }
        </list>
};
declare function git:getGitFolderStructure ($repoinfo as map(*), $subfolder as xs:string) as element()* {
    let $r := local:getGitFolderStructure($repoinfo, $subfolder)
    return
        <list type="folder" url="{$repoinfo?baseurl}" owner="{$repoinfo?owner}" repo="{$repoinfo?repo}" branch="{$repoinfo?ref}">
        {
            $r/@status,
            $r/@message,
            $r/folder
        }
        </list>
};
declare %private function local:getGitFolderStructure ($repoinfo as map(*), $subfolder as xs:string) as element(result)* {

    let $token := $repoinfo?token
    let $vcs := $repoinfo?vcs

    (: URL to get files in subfolder :)
    let $url := $repoinfo?baseurl || "/repos/" || $repoinfo?owner ||  "/" || $repoinfo?repo || "/contents/" || $subfolder

    let $request := git:request($vcs, $url, $token)

    let $requeststatus := $request[1]/@status
    let $bodydata    := util:base64-decode($request[2])
    let $bodymessage := parse-json($bodydata)
    let $arsize := if ($requeststatus != '200') then 0 else array:size($bodymessage)

    let $dirs :=
        for $i in 1 to $arsize
            let $name := $bodymessage($i)?name
            let $type := $bodymessage($i)?type
            let $sf := concat($subfolder, if ($subfolder != '') then '/' else '', $name)
            return
                if (($type = 'dir') and (git:not-in-blacklist($name)))
                then (
                    <folder name="{$sf}"/>,
                    git:getGitFolderStructure($repoinfo, $sf)
                )
                else ()
    
    return
        <result status="{$requeststatus}">
        {
            if ($requeststatus != '200') then attribute { 'message' } { $bodymessage?message } else (),
            $dirs
        }
        </result>
};

(:~
 : Filter function to blacklist resources
 :)
declare %private function git:not-in-blacklist($path as xs:string) as xs:boolean { 
    let $blacklist := $git:blacklist
    return
        if (contains($path, $blacklist)) then
            false()
        else
            true()
};

(:~
:    Return the list of all files in subfolder of Git repository specified in $repoinfo
:)
declare function git:getGitFilesInFolder ($repoinfo as map(*), $subfolder as xs:string) as element() {
    let $r := local:getGitFilesInFolder ($repoinfo, $subfolder)
    return
        <list type="file" url="{$repoinfo?baseurl}" owner="{$repoinfo?owner}" repo="{$repoinfo?repo}" branch="{$repoinfo?ref}">
        {
            $r/@status,
            $r/@message,
            $r/file
        }
        </list>
};
declare %private function local:getGitFilesInFolder ($repoinfo as map(*), $subfolder as xs:string) as element() {
    
    (: token, not used yet :)
    let $token := $repoinfo?token
    let $vcs := $repoinfo?vcs

    let $url := $repoinfo?baseurl || "/projects/" || $repoinfo?project-id ||  "/repository/" || "295665947" ||"/commits"
    let $url := $repoinfo?baseurl || "/repos/" || $repoinfo?owner || "/" || $repoinfo?repo || "/commits?sha=" || $repoinfo?ref
    
    (: URL to get files in subfolder :)
    let $url := $repoinfo?baseurl || "/repos/" || $repoinfo?owner ||  "/" || $repoinfo?repo || "/contents/"  || $subfolder
    let $check := util:log('INFO', 'git-lib: url – ' || $url)

    let $request := git:request($vcs, $url, $token)
    let $requeststatus := $request[1]/@status
    let $bodydata := util:base64-decode($request[2])
    let $bodymessage := parse-json($bodydata)
    let $arsize := if ($requeststatus != '200') then 0 else array:size($bodymessage)

    let $files := 
        for $i in 1 to $arsize
            let $url := $bodymessage($i)?download_url
            let $contents := git:request($vcs, $url, $token)
            let $xml := parse-xml($contents[2])/*
            (: no store for now, 2do check/create folders and subdirectories
            let $store := xmldb:store('/db/apps/decor/data/examples/demo10/demo10-repositories/' || $config?repo, $body($i)?name, $xml)
            :)
            let $store := "not-stored-yet"
            return 
                <file name="{$bodymessage($i)?name}" path="{$store}">
                {
                    (: testing: assumed for now it is a Value Set :)
                    let $ename := local-name($xml)
                    let $id := if ($xml/f:identifier/f:system[@value='urn:ietf:rfc:3986'])
                        then substring($xml/f:identifier[f:system[@value='urn:ietf:rfc:3986']]/f:value/@value/string(), 9)
                        else '1.2.3.4.5'
                    let $name := $xml/f:name/@value/string()
                    let $displayName := if ($xml/f:title) then  $xml/f:title/@value/string() else $name
                    let $effectiveDate := if ($xml/f:extension[@url='http://hl7.org/fhir/StructureDefinition/resource-effectivePeriod']/f:valuePeriod/f:start)
                        then $xml/f:extension[@url='http://hl7.org/fhir/StructureDefinition/resource-effectivePeriod']/f:valuePeriod/f:start/@value/string()
                        else $xml/f:date/@value/string()
                    let $statusCode := "draft"
                    return
                        if ($ename = 'ValueSet') then
                            <valueSet element="{$ename}" id="{$id}" name="{$name}" displayName="{$displayName}" effectiveDate="{$effectiveDate}" statusCode="{$statusCode}"/>
                else <unknow element="{$ename}"/>

                }
                </file>

    return 
        <result status="{$requeststatus}"  url="{$repoinfo?baseurl}" owner="{$repoinfo?owner}" repo="{$repoinfo?repo}" branch="{$repoinfo?ref}" count="{count($files)}">
        {
            if ($requeststatus != '200') then attribute { 'message' } { $bodymessage?message } else (),
            $files
        }
        </result>
};

(:~
 : Git request
:)
declare %private function git:request($vcs as xs:string, $url as xs:string, $token as xs:string) {
    
    let $request :=
        if ($vcs = 'github') 
        then
            http:send-request(
                <http:request http-version="1.1" href="{xs:anyURI($url)}" method="get">
                    <http:header name="Accept" value="application/vnd.github.v3+json" />,
                    { 
                        if (string-length($token) > 0) 
                        then <http:header name="Authorization" value="{concat('token ',$token)}"/>
                        else ()
                    }
                </http:request>
            )
        else
        if ($vcs = 'gitlab')
        then
            http:send-request(
                <http:request http-version="1.1" href="{xs:anyURI($url)}" method="get">
                    { 
                        if (string-length($token) > 0) 
                        then <http:header name="PRIVATE-TOKEN" value="{$token}"/>
                        else ()
                    }
                </http:request>
            )
        else ()
    
    return 
        try {
            $request 
        }
        catch * {
            map {
                "_error": map {
                    "code": $err:code, "description": $err:description, "value": $err:value, 
                    "line": $err:line-number, "column": $err:column-number, "module": $err:module
                },
                "_request": $request?status
            }
        }
};
