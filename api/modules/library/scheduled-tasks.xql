xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:~ This xql is called only from the scheduler process configured in the eXist-db application directory.
    The file etc/conf.xml is expected to have these lines in the /exist/scheduler element:

       <!-- Run every 15 seconds to scan/process project or server related requests such as compilation -->
       <job
           type="user"
           name="scheduled-tasks"
           xquery="/db/apps/api/modules/library/scheduled-tasks.xql"
           period="15000"
           unschedule-on-exception="false"/>
:)
(:import module namespace utillib             = "http://art-decor.org/ns/api/util" at "util-lib.xqm";:)
import module namespace setlib              = "http://art-decor.org/ns/api/settings" at "settings-lib.xqm";
import module namespace prapi               = "http://art-decor.org/ns/api/project" at "../project-api.xqm";
import module namespace scapi               = "http://art-decor.org/ns/api/scenario" at "../scenario-api.xqm";
import module namespace csapi               = "http://art-decor.org/ns/api/codesystem" at "../codesystem-api.xqm";
import module namespace trepapi             = "http://art-decor.org/ns/api/terminology/report" at "../terminology-report-api.xqm";
import module namespace grepapi             = "http://art-decor.org/ns/api/generic-report" at "../generic-report-api.xqm";

(: ================================================================= :)
(: ==== WE DO NOT ACTIVATE TIMEOUT IN PRODUCTION ... LIKE EVER ===== :)
(: 
    some compilations take multiple hours when the size and depth of the template tree is really big 
    when timeout kicks in, tasks stay in busy and never get out unless manually, publications are half finished etc.
    declare option exist:timeout "900000";    - time out for this function after 15 min = 900000 ms
    declare option exist:timeout "5400000";   - time out for this function after 90 min = 5400 seconds = 5400000 ms
    declare option exist:timeout "14400000";  - time out for this function after 4 hour = 14.400.000 ms
:)
(: ================================================================= :)


declare %private variable $SCAN-COLL := collection($setlib:strDecorScheduledTasks);

(: ==================  SCHEDULED TASKS ==================== :)
(:~ Find project related requests such as compilation requests. Called from library/scheduled-tasks.xql :)
declare %private function local:find-scheduled-project-request() {

    let $threadId               := util:uuid()

    let $logsignature           := 'local:find-scheduled-project-request'
    (:let $log                    := prapi:log-event('debug', $threadId, $logsignature, 'start'):)
    
    let $allRequests            := 
        $SCAN-COLL/compile-request | 
        $SCAN-COLL/publication-request | 
        $SCAN-COLL/projectcheck-request |
        $SCAN-COLL/questionnaire-transform-request |
        $SCAN-COLL/projectcodesystemconvert-request|
        $SCAN-COLL/terminology-report-request |
        $SCAN-COLL/generic-report-request
    
    (: get oldest first :)
    let $request                :=
        head(
            for $request in $allRequests[empty(@busy)]
            order by $request/@on
            return $request
        )
    let $all-requests           := count($allRequests)
    let $todo-requests          := count($allRequests[empty(@busy)])
    let $active-requests        := count($allRequests[@busy[not(. = 'error')]])
    let $error-requests         := count($allRequests[@busy = 'error'])
    
    let $log                    := 'requests found: ' || $all-requests
    let $log                    :=
        if ($all-requests gt 0) then 
            util:log('INFO', $log || ', in-error: ' || $error-requests || ', active: ' || $active-requests || ', todo: ' || $todo-requests)
        else ()
    let $log                    := 
        switch ($error-requests)
        case 0 return ()
        case 1 return  util:log('WARN', $error-requests || ' request in error found, please check...')
        default return util:log('WARN', $error-requests || ' requests in error found, please check...')
    
    (: add a threshold of max 5 active requests at the same time :)
    let $action                 := 
        try {
            if ($active-requests gt 5) then
                util:log('INFO', 'max number of ' || $active-requests || ' active requests reached. Wait until one ore more finish ...')
            else ( 
                switch ($request/name())
                (: handle compile request :)
                case 'compile-request'                  return prapi:process-compile-request($threadId, $request)
                (: 
                   handle publication request, 
                   check wether this is the prepare phase or already an ADRAM procress phase
                   ADRAM takes care of its own publication requests
                :)
                case 'publication-request'              return if ($request/@adram) then () else prapi:process-publication-request($threadId, $request)
                (: handle project check request :)
                case 'projectcheck-request'             return prapi:process-projectcheck-request($threadId, $request)
                (: handle questionnaire transform request :)
                case 'questionnaire-transform-request'  return scapi:process-questionnairetransform-request($threadId, $request)
                case 'projectcodesystemconvert-request' return csapi:process-decorprojectcodesystemsincadts-request($threadId, $request)
                case 'terminology-report-request'       return trepapi:process-terminology-report-request($threadId, $request)
                case 'generic-report-request'           return grepapi:process-generic-report-request($threadId, $request)
                default return ()
            )
        }
        catch * {
            let $mark-busy      := 
                if ($request/@busy) then 
                    update value $request/@busy with 'error' 
                else (
                    update insert attribute busy {'error'} into $request
                )
            let $message        := 'ERROR. ' || $err:code || ': ' || $err:description || ' ' || $err:module || ' ' || $err:line-number || ':' || $err:column-number
            let $log            := local:log-scheduler-event('error', $threadId, 'scheduled-tasks', $message)
            let $log            := util:log('ERROR', 'scheduled-tasks: error +++ ' || $message)
            let $progress       := update value $request/@progress with $message
        
            return ()
        }
    
    let $log                    := 
        if ($active-requests gt 5)
        then util:log('WARN', 'scheduled-tasks: warning +++ There are more than 5 requests queued, please check (' || $threadId || ').')
        else ()
    let $log                    := 
        if (string-length($request/name()) > 0)
        then util:log('WARN', 'scheduled-tasks: Request ' || $request/name() || ' processed (' || $threadId || ').')
        else ()
    
    return ()
};

(:~ Issue messages to the scheduler log :)
declare %private function local:log-scheduler-event($level as xs:string, $threadId as xs:string, $event as xs:string, $message as item()*) {
    let $log-collection     := $setlib:strDecorScheduledTasks
    let $log                := "scheduled-log.xml"
    let $log-uri            := concat($log-collection, "/", $log)
    
    (: create the log file if it does not exist :)
    let $create-doc         := 
        if (doc-available($log-uri)) then () else (
            xmldb:store($log-collection, $log, <scheduler-log/>)
        )
    (: log the trigger details to the log file :)
    let $insert             :=
        update insert <log timestamp="{current-dateTime()}" thread="{$threadId}" event="{$event}">{$message}</log> into doc($log-uri)/scheduler-log
    let $logevent           :=
        util:log($level, current-dateTime() || ' ' || $threadId || ' ' || $event || ' ' || string-join(data($message), ' '))
    return
        ()
};

(: find all project related scheduled tasks such as compile or check and execute them :)
let $check := local:find-scheduled-project-request()

return ()
