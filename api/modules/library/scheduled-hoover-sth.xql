xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

(:~

    Module: list / hoover history and releases
    
    Takes a couple of parameters
        project - a project prefix parameter, if not specified and action is list, all projects are listed
        action  - the action to be performed
                  one of the following
                    list-history: list statistics about history of project or all projects
                    hoover-history: hoover (clean up) history of project or all projects
                    -- later maybe implemented: list-releases and hoover-releases
                    
                    if no action is specified, nothing happens and a succesful result is returned
                    
                    hint: on hoover the history is not deleted but a skeletton is created
                    to be still in place and all body data of the artefacts is written to
                    the the hoover bag collection in the same history collection
    
    BACKGROUND
    ==========
    The Hoover Company is a home appliance company founded in Ohio, United States, in 1915. It also established 
    a major base in the United Kingdom; and, mostly in the 20th century, it dominated the electric vacuum cleaner 
    industry, to the point where the Hoover brand name became synonymous with vacuum cleaners and vacuuming 
    in the United Kingdom and Ireland.
    -- Wikipedia 2024 https://en.wikipedia.org/wiki/The_Hoover_Company
    
    HOOVERING HISTORY
    =================
    A project may have a collection in decor/history where several XML files may reside such as TM.xml or VS.xml
    for collection the history of changes of templates, value sets and other artefacts.
    Every time an artefact is changed and will be saved, the old version is stored in the history
    along with some meta data.
    
    An example
    
    <history
        date="2021-06-24T10:03:24.524+02:00" 
        authorid="kai" author="Kai Heitmann" 
        id="693a099d-c095-4264-ba09-9dc095b26408"
        intention="version"
        artefactId="2.16.840.1.113883.2.6.60.3.2.6.3"
        artefactEffectiveDate="2018-01-25T00:00:00"
        artefactStatusCode="draft" 
    >
        <!-- this is the real artefact in detail stored hereafter -->
        <concept id="2.16.840.1.113883.2.6.60.3.2.6.3" effectiveDate="2018-01-25T00:00:00" statusCode="draft" type="item">
            <name language="de-DE">Patienten ID</name>
            ... all the body of the artefact if not hoovered, 
            ... or only the root element and attributes (on cocnepts and datasets
            ... also with the name to allow proper display on hoovered versions.
        </concept>
    </history>
    
    When a lot of work is done, artefact history can be very lengthy. On servers where this is the case,
    hoovering the history files is appropriate. 
    
    A side note: we do have busy projects that have a total history size of more than 50 GB.
    
    Hoovering essentially means to gzip so far unhoovered artefact files that are outdated
    (ie their creation date is before the $KEEP-HISTORY-UNTIL days period), store the
    gzip files in a subfolder of the project history file called $HOOVER-BAG-COLLECTION
    and replace each hoovered history item by a skeleton that looks like the following example.
    
    <history 
        date="2022-11-07T00:39:51.903+02:00"
        authorid="kai" author="Kai Heitmann"
        id="86f43b6f-8986-4a9a-ad0b-43a95fad3090"
        intention="version"
        artefactId="2.16.840.1.113883.2.6.60.3.4.13"
        artefactEffectiveDate="2019-12-05T08:17:05"
        artefactStatusCode="draft"
        hoovered="true"
        bag="cd6dcacb-76c2-43cf-adca-cb76c253cf8d"
     />
    
     The artefact body is omitted, instead two extra attributes are presented:
        hoovered - always true, to indicate that the history item is hoovered (in the hoover bag, compressed as gzip)
        bag      - the id of the hoover bag in subfolder $HOOVER-BAG-COLLECTION, the gzipped file is named bag id + suffix .xml.gz
        
     Hoovering large histories brings down re-index time dramatically by approximatedly factor 10.
        
:)

(:~ This xqm is called only from the scheduler process configured in the eXist-db application directory.
    The file etc/conf.xml is expected to have these two lines in the /exist/scheduler element:
    
       <!-- Run a hoover history action every month, unschedule if fails -->
       <!--
            Parameter:
              threshold
                total size of a project history in MB when hoover shall take place
              project
                the project(s), expressed as prefixes, for which the hoovering shall take place
                either *ALL* which processes all folders in history folder $setlib:strDecorHistory
                or it is a single project
                or a list of projects, all separated by blanks
                empty is not allowed
             action
                the actual action to be taken, either "list-history" or "hoover-history"
       -->
       <job type="user" name="scheduled-hoover-sth" xquery="/db/apps/api/modules/library/scheduled-hoover-sth.xql"
           period="300000" unschedule-on-exception="true">
           <parameter name="threshold" value="100"/>
           <parameter name="project" value="demo5-"/>
       </job>
:)

import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "settings-lib.xqm";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

(: set timeout to 60 minutes = 3600 seconds = 3600000 :)
declare option exist:timeout "3600000";

(: some constants :)
declare variable $HOOVER-BAG-COLLECTION  := 'hooverbag';
declare variable $KEEP-HISTORY-UNTIL     := 180;   (: history items not to hoover until they are older than setting (in days) :)
declare variable $ACTION-LIST-HISTORY    := 'list-history';
declare variable $ACTION-HOOVER-HISTORY  := 'hoover-history';
declare variable $ACTION-LIST-RELEASES   := 'list-releases';
declare variable $ACTION-HOOVER-RELEASES := 'hoover-releases';
declare variable $THRESHOLD-MIN          := 10;
declare variable $THRESHOLD-MAX          := 500;

(:
    get external parameters from the caller
    
    threshold
        total size of a project history in MB when hoover shall take place
    project
        the project prefix for which the hoovering shall take place, or "*ALL*" to process all; empty is not allowed.
:)

declare variable $local:threshold external;
declare variable $local:project external;
declare variable $local:action external;

(:~ Acts on a specific history file and - depending on action - list or really hoovers the file
  
    @param $historyfile  - required. the history file in decor/history of a project that will be acted on
    @param $hooverbag    - optional. the path of the collection in the project history folder to store the hoovered zip files, ends with $HOOVER-BAG-COLLECTION
    @param $action       - required. either $ACTION-LIST-HISTORY (just list tasks) vs $ACTION-HOOVER-HISTORY (really hoover history file)
    @since 2023-06-14
:)
declare function local:hoover-history-file ($historyfile as xs:string, $hooverbag as xs:string?, $action as xs:string) {
                        
    for $histories in (doc($historyfile)/histories)
    let $p := $histories/@projectPrefix
    let $a := $histories/@artefactType
    (: store compressed history file as gzip file in the bag :)
    let $uuid := util:uuid()
    let $bagfile := concat($uuid, '.xml.gz')
    (: get all history items that are not yet hoovered and older than KEEP-HISTORY-UNTIL days :)
    let $historyitemsnothoovered := $histories/history[not(@hoovered)]
    let $today := current-dateTime()
    let $historyitems2hoover := $historyitemsnothoovered[($today - xs:dateTime(@date)) div xs:dayTimeDuration('P1D') > $KEEP-HISTORY-UNTIL]
    let $item2behoovered := count($historyitems2hoover)
    return
        if ($action = $ACTION-HOOVER-HISTORY and $item2behoovered > 0)
        then
            <part artefactType="{$a}" nothoovered="{count($historyitemsnothoovered)}" todo="{$item2behoovered}" remaining="{count($historyitemsnothoovered)-$item2behoovered}" date="{current-dateTime()}">
                {
                    (: copy all not-yet-hoovered history of artefacts items in file to the hoover bag, zipped :)
                    let $comp := compression:gzip(
                        util:string-to-binary(
                            serialize(
                                <histories>
                                {
                                    $histories/@*,
                                    $historyitems2hoover
                                }
                                </histories>
                            )
                        )
                    )
                    let $hzstore := xmldb:store($hooverbag, $bagfile, $comp)
                    
                    (: then hoover all bodies of the history and store skeleton :)
                    for $hitem in $historyitems2hoover
                    let $hid := $hitem/@id
                    let $hitemskeleton :=
                        <history>
                        {
                            $hitem/@*,
                            attribute { 'hoovered' } { 'true' },
                            attribute { 'bag' } { $uuid }
                        }
                        </history>
                    (: update history item :)
                    let $hitemupdate := update replace $hitem with $hitemskeleton
                    return
                        $hitemskeleton
                }
            </part>
        else
            <part artefactType="{$a}" nothoovered="{count($historyitemsnothoovered)}" todo="{$item2behoovered}" remaining="{count($historyitemsnothoovered)-$item2behoovered}" date="{current-dateTime()}"/>
};

(:

   MAIN
   
:)

(: check availability of required parameters, fail if undefined or empty :)
let $threshold :=
    if (
        try { $local:threshold != '' }
        catch err:XPDY0002 {
            let $err := util:log('ERROR', 'scheduled-hoover-sth: error +++ variable/parameter "threshold" not set in server scheduler configuration')
            let $err := error($errors:BAD_REQUEST,'Required scheduled-hoover-sth variable/parameter "threshold" not set in server scheduler configuration')
            return ''
        }
    )
    then
        $local:threshold
    else
        let $err := util:log('ERROR', 'scheduled-hoover-sth: error +++ variable/parameter "threshold" not set in server scheduler configuration')
        let $err := error($errors:BAD_REQUEST,'Required scheduled-hoover-sth variable/parameter "threshold" not set in server scheduler configuration')
        return ''
let $threshold := xs:integer($threshold)
let $projectPrefix :=
    if (
        try { $local:project != '' }
        catch err:XPDY0002 {
            let $err := util:log('ERROR', 'scheduled-hoover-sth: error +++ variable/parameter "project" not set in server scheduler configuration')
            let $err := error($errors:BAD_REQUEST,'Required scheduled-hoover-sth variable/parameter "project" not set in server scheduler configuration')
            return ''
        }
    )
    then
        $local:project
    else
        let $err := util:log('ERROR', 'scheduled-hoover-sth: error +++ variable/parameter "project" not set in server scheduler configuration')
        let $err := error($errors:BAD_REQUEST,'Required scheduled-hoover-sth variable/parameter "project" not set in server scheduler configuration')
        return ''
let $action :=
    if (
        try { $local:action != '' }
        catch err:XPDY0002 {
            let $err := util:log('ERROR', 'scheduled-hoover-sth: error +++ variable/parameter "action" not set in server scheduler configuration')
            let $err := error($errors:BAD_REQUEST,'Required scheduled-hoover-sth variable/parameter "action" not set in server scheduler configuration')
            return ''
        }
    )
    then
        $local:action
    else
        let $err := util:log('ERROR', 'scheduled-hoover-sth: error +++ variable/parameter "action" not set in server scheduler configuration')
        let $err := error($errors:BAD_REQUEST,'Required scheduled-hoover-sth variable/parameter "action" not set in server scheduler configuration')
        return ''
        
(: check validitiy of parameters :)
let $check := if ($threshold castable as xs:integer) then () 
    else (
        util:log('ERROR', 'scheduled-hoover-sth: error +++ variable/parameter "threshold" must be an integer ' 
        || string($THRESHOLD-MIN) || '..' || string($THRESHOLD-MAX) || ' in server scheduler configuration'),
        error($errors:BAD_REQUEST,'Required scheduled-hoover-history variable/parameter "threshold" must be an integer ' 
        || string($THRESHOLD-MIN) || '..' || string($THRESHOLD-MAX) || ' in server scheduler configuration')
    )
let $check := if ($THRESHOLD-MIN <= $threshold and $threshold <= $THRESHOLD-MAX) then () 
    else (
        util:log('ERROR', 'scheduled-hoover-sth: error +++ variable/parameter "threshold" must be an integer ' 
        || string($THRESHOLD-MIN) || '..' || string($THRESHOLD-MAX) || ' in server scheduler configuration'),
        error($errors:BAD_REQUEST,'Required scheduled-hoover-sth variable/parameter "threshold" must be an integer ' 
        || string($THRESHOLD-MIN) || '..' || string($THRESHOLD-MAX) || ' in server scheduler configuration')
    )
let $check := if (string-length($projectPrefix) > 0) then () 
    else (
        util:log('ERROR', 'scheduled-hoover-sth: error +++ variable/parameter "project" is empty in server scheduler configuration'),
        error($errors:BAD_REQUEST,'Required scheduled-hoover-sth variable/parameter "project" is empty in server scheduler configuration')
    )
let $check := if ($action = ($ACTION-LIST-HISTORY, $ACTION-HOOVER-HISTORY)) then ()
    else (
        util:log('ERROR', 'scheduled-hoover-sth: error +++ variable/parameter "action" has invalid value in server scheduler configuration'),
        error($errors:BAD_REQUEST,'Required scheduled-hoover-history variable/parameter "action" has invalid value in server scheduler configuration')
    )
    
(:
    action starts here
    ==================
:)
let $check   := util:log('INFO', 'scheduled-hoover-sth: start processing ' || $action || ' with threshold ' || $threshold || ' MB')
       
let $result-history :=
    (:
        ====================== history: list or hoover ======================
    :)
    if ($action = ($ACTION-LIST-HISTORY, $ACTION-HOOVER-HISTORY))
    then
        <hoovered date="{current-dateTime()}" action="{$action}" threshold="{$threshold}" project="{$projectPrefix}">
        {
            (: 
                history directory variables
                either *ALL* which processes all folders in history folder $setlib:strDecorHistory
                or it is a single project
                or a list of projects, all separated by blanks
            :)
            let $historyDirs :=
                if ($projectPrefix = "*ALL*") then (
                    (: if *ALL* projects is given, get all projects available :)
                    xmldb:get-child-collections($setlib:strDecorHistory)
                ) else
                    for $pp in tokenize($projectPrefix, "\s+")
                    return
                        if (ends-with($pp, '-'))
                        then substring($pp, 1, string-length($pp) - 1)
                        else (
                            util:log('ERROR', 'scheduled-hoover-sth: error +++ variable/parameter "project" ' 
                                || ' all particles must end on "-" in server scheduler configuration, found ' || $pp),
                            error($errors:BAD_REQUEST,'Required scheduled-hoover-sth variable/parameter "project" ' 
                                || ' all particles must end on "-" in server scheduler configuration, found ' || $pp)
                        )
            
            return
                if (false()) then (
                    util:log('INFO', 'scheduled-hoover-sth: debug ' || $projectPrefix),
                    util:log('INFO', 'scheduled-hoover-sth: debug ' || string-join($historyDirs, '|')),
                    <historyset project="{$projectPrefix}" dirs="{count($historyDirs)}"/>
                ) else if (count($historyDirs) = 0)
                then
                    (: no history files found for requested project(s) :)
                    <historyset project="{$projectPrefix}" files="0"/>
                else
                    for $hdir in $historyDirs
                    
                    let $hpath := concat($setlib:strDecorHistory, '/', $hdir)
                    
                    let $c := xmldb:get-child-resources($hpath)
                    
                    (: determine total estimated size of this history folder :)
                    let $tsizes :=
                        for $hf in $c[ends-with(., '.xml')]
                        return
                            <size bytes="{xmldb:size($hpath, $hf)}"/>
                    let $historysize := sum($tsizes/@bytes) div 1048576 (:1024 * 1024:)
                    let $overthreshold := $historysize > $threshold
                    
                    let $pmsg := if ($overthreshold) then 'processing' else 'below threshold, skipping'
                    let $check := util:log('INFO', 'scheduled-hoover-sth: investigating project ' || $hdir ||
                        ' size: ' || format-number($historysize, '0.00') || ' MB -> ' || $pmsg
                        )   
                    
                    (: on HOOVER-HISTORY, check whether the hoover bag is already existing :)
                    let $hoovercollection := concat($hpath, '/', $HOOVER-BAG-COLLECTION)
                    let $hooverbag :=
                        try {
                            if ($overthreshold and $action = $ACTION-HOOVER-HISTORY) then (
                                if (not(xmldb:collection-available($hoovercollection)))
                                then xmldb:create-collection($hpath, $HOOVER-BAG-COLLECTION)
                                else (),
                                sm:chmod(xs:anyURI($hoovercollection), 'rw-rw-r--'),
                                sm:chgrp(xs:anyURI($hoovercollection), 'decor'),
                                sm:chown(xs:anyURI($hoovercollection), 'admin')
                            ) else ()
                        }
                        catch * {
                            let $message := 'ERROR. ' || $err:code || ': ' || $err:description || ' ' || $err:module || ' ' || $err:line-number || ':' || $err:column-number
                            let $check := util:log('ERROR', 'scheduled-hoover-sth: error +++ ' || $message || '. '
                                || $HOOVER-BAG-COLLECTION || ' for project ' || $projectPrefix || ' cannot be created')
                            return ()
                        }
                        
                    let $tempresult :=
                        if ($overthreshold or $ACTION-LIST-HISTORY)
                        then
                            for $hf in $c[ends-with(., '.xml')]
                            let $hp := concat($hpath, '/', $hf)
                            let $res :=
                                try {
                                switch ($action) 
                                    case $ACTION-HOOVER-HISTORY
                                    case $ACTION-LIST-HISTORY
                                        return if (string-length($hooverbag) > 0) then local:hoover-history-file ($hp, $hooverbag, $action) else ()
                                    default
                                        return ()
                                 }
                                 catch * {
                                    <error>Caught error {$err:code}: {$err:description}. Data: {$err:value}</error>
                                 }
                            return
                                $res
                        else ()
                            
                    let $factaction := if ($overthreshold) then () else '-skipped-under-threshold'
                    let $hooverresult :=
                        <historyset
                            project="{$hdir}"
                            path="{$hpath}"
                            files="{count($c)}"
                            objects="{string-join($c, ' ')}"
                            hooverbag="{$hooverbag}"
                            nodes="{count(collection($hpath)/histories/history)}"
                            size="{$historysize}"
                            action="{concat($action, $factaction)}"
                            totalhooveredever="{count(collection($hpath)/histories/history[@hoovered='true'])}"
                        >
                        {
                            if ($action = $ACTION-LIST-HISTORY)
                            then attribute hoovercandidates { sum($tempresult/@todo) }
                            else (),
                            $tempresult        
                        }
                        </historyset>
                    
                    (: reindex this history collection if any items have been hoovered :)
                    let $reindex := 
                        if ($action = $ACTION-HOOVER-HISTORY and count($hooverresult/part[@todo > 0]) > 0)
                        then xmldb:reindex($hpath)
                        else ()
                    
                    return
                        $hooverresult
                
        }
        </hoovered>
    else ()

(: temp write log :)
let $write   := xmldb:store($setlib:strDecorTemp, 'hoover-log.xml', $result-history)
        
let $check := util:log('INFO', 'scheduled-hoover-sth: finished, statistiscs: ')

let $check := if ($action = $ACTION-HOOVER-HISTORY) then 
    util:log('INFO', 'scheduled-hoover-sth: ...' || sum($result-history//part/@todo) || ' items hoovered in this run') else ()

let $check := if ($action = $ACTION-LIST-HISTORY) then 
    util:log('INFO', 'scheduled-hoover-sth: ...' || sum($result-history/historyset/@hoovercandidates) || ' items identified to be hoovered, but skipped due to list only') else ()

let $check := util:log('INFO', 'scheduled-hoover-sth: ...' || sum($result-history/historyset/@totalhooveredever)
    || ' items hoovered ever out of ' || sum($result-history/historyset/@nodes) || ' items total')

return $result-history