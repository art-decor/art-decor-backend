xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:~ Project API allows read, create, update, delete of DECOR project properties :)
module namespace prapi              = "http://art-decor.org/ns/api/project";

import module namespace roaster     = "http://e-editiones.org/roaster";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

import module namespace utillib     = "http://art-decor.org/ns/api/util" at "library/util-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "library/settings-lib.xqm";
import module namespace decorlib    = "http://art-decor.org/ns/api/decor" at "library/decor-lib.xqm";
import module namespace userapi     = "http://art-decor.org/ns/api/user" at "user-api.xqm";
import module namespace serverapi   = "http://art-decor.org/ns/api/server" at "server-api.xqm";
import module namespace utillibx    = "http://art-decor.org/ns/api/util-xpath" at "library/util-xpath-lib.xqm";

import module namespace ggapi       = "http://art-decor.org/ns/api/governancegroups" at "governancegroups-api.xqm";
import module namespace hist        = "http://art-decor.org/ns/api/history" at "library/history-lib.xqm";

import module namespace ada         = "http://art-decor.org/ns/ada-common" at "../../ada/modules/ada-common.xqm";
import module namespace comp        = "http://art-decor.org/ns/art-decor-compile" at "../../art/api/api-decor-compile.xqm";

declare namespace xmldb             = "http://exist-db.org/xquery/xmldb";
declare namespace util              = "http://exist-db.org/xquery/util";
declare namespace http              = "http://expath.org/ns/http-client";
declare namespace json              = "http://www.json.org";
declare namespace output            = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace sch               = "http://purl.oclc.org/dsdl/schematron";
declare namespace sm                = "http://exist-db.org/xquery/securitymanager";
declare namespace svrl              = "http://purl.oclc.org/dsdl/svrl";
declare namespace xs                = "http://www.w3.org/2001/XMLSchema";
declare namespace hl7               = "urn:hl7-org:v3";
declare namespace xsi               = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace xsl               = "http://www.w3.org/1999/XSL/Transform";

(:~ All functions support their own override, but this is the fallback for the maximum number of results returned on a search :)
declare %private variable $prapi:maxResults                             := 50;

declare %private variable $prapi:PROJECT-COPYRIGHT-TYPE                 := utillib:getDecorTypes()/CopyrightType;
declare %private variable $prapi:PROJECT-RESTURI-FOR                    := utillib:getDecorTypes()/DecorAndOtherObjectTypes;
declare %private variable $prapi:PROJECT-RESTURI-FORMAT                 := utillib:getDecorTypes()/DecorAndOtherObjectFormats;
declare %private variable $prapi:PROJECT-BBR-FORMAT                     := utillib:getDecorTypes()/BuildingBlockRepositoryFormat;
declare %private variable $prapi:DECOR-OBJECT-TYPES                     := utillib:getDecorTypes()/DecorObjectType;
declare %private variable $prapi:DESIGNATION-TYPES                      := utillib:getDecorTypes()/DesignationType;
declare %private variable $prapi:LICENSE-TYPES                          := utillib:getDecorTypes()/LicenseTypes;

declare %private variable $prapi:artServerURL                           := serverapi:getServerURLArt();
declare %private variable $prapi:decorServicesURL                       := serverapi:getServerURLServices();
declare %private variable $prapi:fhirServicesURL                        := serverapi:getServerURLFhirServices();

declare %private variable $prapi:decorServicesURLlower                  := lower-case($prapi:decorServicesURL);
declare %private variable $prapi:fhirServicesURLlower                   := lower-case($prapi:fhirServicesURL);

(: https://developer.mozilla.org/en-US/docs/Web/Media/Formats/Image_types :)
(:declare %private variable $prapi:acceptableMediaTypes                   := ('image/apng', 'image/avif', 'image/gif', 'image/jpeg', 
                                                                            'image/png', 'image/svg+xml', 'image/webp');:)
declare %private variable $prapi:acceptableMediaTypeMap                 :=
    map {
        "image/apng": "apng", 
        "image/avif": "avif", 
        "image/gif": "gif", 
        "image/jpeg": ["jpg", "jpeg", "jfif", "pjpeg", "pjp"], 
        "image/png": "png", 
        "image/svg+xml": "svg", 
        "image/webp": "webp"
    };
declare %private variable $prapi:PROGRESS-COMPILE-10                    := 'Checking request parameters ...';
declare %private variable $prapi:PROGRESS-COMPILE-20                    := 'Checking for previous compilations ...';
declare %private variable $prapi:PROGRESS-COMPILE-30                    := 'Compiling ...';
declare %private variable $prapi:PROGRESS-COMPILE-40                    := 'Removing task from queue ...';
declare %private variable $prapi:PROGRESS-RUNTIME-10                    := 'Creating schematron target collections ...';
declare %private variable $prapi:PROGRESS-RUNTIME-20                    := 'Creating schematron ...';
declare %private variable $prapi:PROGRESS-RUNTIME-30                    := 'Creating zip ...';
declare %private variable $prapi:PROGRESS-RUNTIME-40                    := 'Converting schematron to svrl ...';
declare %private variable $prapi:PROGRESS-RUNTIME-50                    := 'Storing runtime index ...';
declare %private variable $prapi:PROGRESS-PUBLICATION-10                := 'Creating publication target collections ...';
declare %private variable $prapi:PROGRESS-PUBLICATION-20                := 'Compiling ...';
declare %private variable $prapi:PROGRESS-PUBLICATION-30                := 'Copy referenced communities  ...';
declare %private variable $prapi:PROGRESS-PUBLICATION-40                := 'Creating xpath files ...';
declare %private variable $prapi:PROGRESS-PUBLICATION-50                := 'Creating transaction files ...';
declare %private variable $prapi:PROGRESS-PUBLICATION-60                := 'Creating publication request ...';
declare %private variable $prapi:PROGRESS-PUBLICATION-70                := 'Removing task from queue ...';
declare %private variable $prapi:PROGRESS-PROJECTCHECK-10-a             := 'Compiling ...';
declare %private variable $prapi:PROGRESS-PROJECTCHECK-10-b             := 'Starting checks ...';
declare %private variable $prapi:PROGRESS-PROJECTCHECK-30               := 'Transforming project checks ...';
declare %private variable $prapi:PROGRESS-PROJECTCHECK-40               := 'Applying project checks ...';
declare %private variable $prapi:PROGRESS-PROJECTCHECK-50               := 'Creating project check report ...';
declare %private variable $prapi:PROGRESS-PROJECTCHECK-60               := 'Storing project check report ...';

(: this is a "missing image" binary inline image to return upon failure :)
declare %private variable $prapi:MISSING-IMAGE-INLINE-IMG               := xs:base64Binary(concat(
        'iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAIAAAAnX375AAAAAXNSR0IArs4c6QAAAapJREFUeJzsl21vwVAUgP3/f8T6grEQIhFELUQVMfESIZga9iQ3a0RWuvZosqTPp6p7z9N7es5', 
        'tm7rETipRxqBcrVaWZb2LQsDZbOarHI1G6XRa07QXIQhFwG6366scj8cMMk2zWCy+SZDNZgnY6/UeKHO53Hq9/orMfr/n0llrIOV2uw1aDP64rvuvlJvN5nA4xKQ8n89UuWEYhUKBFr',
        'ojYKSMcrfbUXXUMDPb7bafbzqdOo4jozwej+VyWTXZcDj81Udo+qFUKp1OJwElcEwv85eXupscdjodZpH8xWIho7yBVNdqNW/AfD5HRuZ1XW82m09RtlqtTCZDGul0+p0DpYR8Ps8FC',
        'ShJo5ex6zVVKhWWxYH5A3Nt2xZQLpdLNIPBQJWS5+DktU+dYYBXROGV9Xpd7fXVatXLoR9cBJmIpFRLVBDOeATTG41GeCV3kbvFz9fAUEFoVBGFXOVnKNTt/JuSBrhEBnFQJZsq240T',
        'mX6/T54DvRWowtMkUM0aSClIokyUz1PygcCzUBeFgPc+ENgbJ5PJhygE5J3UVxkPifJJfAMAAP//AwDyvE7WftbmXgAAAABJRU5ErkJggg=='));

(:~ Retrieves DECOR project list

    @param $bearer-token            - optional. provides authorization for the user. The token, if available, should be on the X-Auth-Token HTTP Header
    @param $searchTerms             - optional. Comma separated list of terms that results should match in project/name element. Results all if empty
    @param $projectPrefix           - optional. limits scope to this project only
    @param $repository              - optional. boolean. limits scope to projects marked as repository, or not
    @param $experimental            - optional. boolean. limits scope to projects marked as experimental, or not
    @param $author                  - optional. boolean. limits scope to projects where user is marked as author, or not
    @param $changeddate             - optional. optional. by project changed date. yyyy-mm-dd that may be prefixed with gt (greater than), lt (smaller than), ge (greater or equal than), le (smaller than)
    @param $sortorder               - optional. Sort order. Default is ascending. Only other option is descending.
    @param $max                     - optional integer, maximum number of results
    @return list with zero to many projects
    @since 2020-08-11
:)
declare function prapi:getDecorProjectList($request as map(*)) {

    let $searchTerms        := $request?parameters?search
    let $projectIdOrPrefix  := $request?parameters?prefix
    let $repository         := $request?parameters?repository
    let $experimental       := $request?parameters?experimental
    let $author             := $request?parameters?author
    let $changedafter       := $request?parameters?changeddate
    let $sort               := $request?parameters?sort
    let $sortorder          := $request?parameters?sortorder
    let $max                := $request?parameters?max
    
    return
        prapi:getDecorProjectList($request?user, $searchTerms, $projectIdOrPrefix, $repository, $experimental, $author, $changedafter, $sort, $sortorder, $max)
        
};

(:~ Retrieves DECOR project based on $projectPrefixOrId. Optionally includes ART-DECOR Release and Archive Manager (ADRAM) information
    @param $projectPrefixOrId required path part which may be its prefix or its oid
    @param $language optional string parameter to return certain information with. Defaults for project default language
    @param $checkadram optional boolean parameter to include ADRAM information or not. Default is false
    @return project as JSON object
    @since 2020-05-03
:)
declare function prapi:getDecorProject($request as map(*)) {

    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    let $checkadram             := $request?parameters?checkadram = true()
    
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    
    let $results                := prapi:getDecorInfo($authmap, $project, $checkadram)

    return
        if (empty($results)) then (
            roaster:response(404, "application/json", ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple projects for project '", $project, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        )
        else (
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
        )

};

(:~ Create DECOR project. Optionally includes ART-DECOR Release and Archive Manager (ADRAM) information
    @param $projectPrefixOrId required path part which may be its prefix or its oid
    @param $language optional string parameter to return certain information with. Defaults for project default language
    @param $checkadram optional boolean parameter to include ADRAM information or not. Default is false
    @return project as JSON object
    @since 2020-05-03
:)
declare function prapi:postDecorProject($request as map(*)) {

    let $authmap                := $request?user
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($request?body)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $data                   := utillib:getBodyAsXml($request?body, 'project', ())
    let $results                := prapi:postDecorInfo($authmap, $data)

    return
        if (empty($results)) then (
            error($errors:SERVER_ERROR, 'Project not returned after successful creation. Please inform your server administrator.')
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple projects for project '", string-join(distinct-values($results/@prefix), ' '), "'. Expected 1..1. Alert your administrator as this should not be possible."))
        )
        else (
            roaster:response(201, 
              for $result in $results
              return
                  element {name($result)} {
                      $result/@*,
                      namespace {"json"} {"http://www.json.org"},
                      utillib:addJsonArrayToElements($result/*)
                  }
            )
        )

};

(:~ Update DECOR project parts. Expect array of parameter objects, each containing RFC 6902 compliant contents. Note: RestXQ does not do PATCH (yet), but that would be the preferred option.

    { "op": "[add|remove|replace]", "path": "[e.g. /repository|/name]", "value": "[string|object]" }

    where

    * op - add or remove or replace
    * path - paths to the object: e.g. /repository|/name
    * value - as relevant for the path

    @param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
    @param $project                  - required. the id or prefix for the project to update 
    @param $request-body             - required. json body containing array of parameter objects each containing RFC 6902 compliant contents
    @return project structure including generated meta data
    @since 2020-05-03
    @see http://tools.ietf.org/html/rfc6902
:)
declare function prapi:patchProject($request as map(*)) {

    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]

    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($request?body)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    
    let $checkadram             := $request?parameters?checkadram = true()
    let $data                   := utillib:getBodyAsXml($request?body, 'parameters', ())
    let $return                 := prapi:patchProject($authmap, $project, $data, $checkadram)
    return (
        roaster:response(200, $return)
    )

};

(:~ Deletes DECOR project based on $projectPrefixOrId. **dba only**
    @param $projectPrefixOrId required path part which may be its prefix or its oid
    @return nothing
    @since 2020-05-03
:)
declare function prapi:deleteDecorProject($request as map(*)) {

    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else if ($authmap?groups = 'dba') then ()
        else (
            error($errors:FORBIDDEN, 'You need to be dba for this feature')
        )
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    
    let $results                :=  if (utillib:isOid($project)) then utillib:getDecorById($project) else utillib:getDecorByPrefix($project)
    
    let $check                  :=
        if (count($results) gt 1) then
            error($errors:SERVER_ERROR, concat("Found multiple projects for project '", $project, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        else ()
    
    let $delete                 := for $r in $results return xmldb:remove(util:collection-name($r))
    
    return
        roaster:response(204, ())

};

(:~ Retrieves DECOR project author list based on $projectPrefixOrId
    @param $projectPrefixOrId required path part which may be its prefix or its oid
    @return project author list
    @since 2020-05-03
:)
declare function prapi:getProjectAuthorList($request as map(*)) {

    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    
    let $results                := prapi:getProjectAuthorList($authmap, $project)

    return
        $results
};

(:~ Retrieves DECOR project history of artefacts based on $projectPrefixOrId
    @param $projectPrefixOrId required path part which may be its prefix or its oid
    @param $language optional string parameter to return certain information with. Defaults for project default language
    @return project as JSON object
    @since 2021-01-19
:)
declare function prapi:getDecorProjectHistory($request as map(*)) {

    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    
    (: set the limit, defaults to 50, later as a parameter between 0 ... maybe 1,000? :)
    let $results                := prapi:getDecorProjectHistory($authmap, $project, 50)

    return
        if (empty($results)) then (
            roaster:response(404, "application/json", ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple projects for project '", $project, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        )
        else (
            $results
        )

};

(:~ Retrieves DECOR project ids list based on $projectPrefixOrId
    @param $projectPrefixOrId required path part which may be its prefix or its oid
    @param $language optional string parameter to return certain information with. Defaults for project default language
    @return list
    @since 2022-07-21
:)
declare function prapi:getDecorProjectIds($request as map(*)) {

    let $projectPrefixOrId      := $request?parameters?project[string-length() gt 0]
    let $projectVersion         := $request?parameters?release[string-length() gt 0]
    let $projectLanguage        := $request?parameters?language[string-length() gt 0]
    let $max                    := $request?parameters?max[. castable as xs:integer]
    let $searchTerms            := 
        array:flatten(
            for $s in ($request?parameters?search, $request?parameters?id, $request?parameters?name)[string-length() gt 0]
            return
                tokenize(normalize-space(lower-case($s)),'\s')
        )
    let $localonly              := $request?parameters?localonly = true()
    let $sort                   := $request?parameters?sort[string-length() gt 0]
    let $sort                   := if (empty($sort)) then () else lower-case($sort[1])
    let $sortorder              := ($request?parameters?sortorder[. = 'descending'])[1]
    
    let $check                  :=
        if (empty($projectPrefixOrId)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    
    let $decor                  := 
        if (utillib:isOid($projectPrefixOrId)) then 
            utillib:getDecorById($projectPrefixOrId, $projectVersion, $projectLanguage) 
        else (
            utillib:getDecorByPrefix($projectPrefixOrId, $projectVersion, $projectLanguage)
        )
    
    let $projectIds             := $decor/ids/id
    
    (: Yields
        <oid oid="1.0.3166.1.2.2">
            <name language="en-US">ISO 3166 Alpha 2</name>
            <desc language="en-US">ISO 3166 2 alpha Landcodes</desc>
            <name language="nl-NL">ISO 3166 Alpha 2</name>
            <desc language="nl-NL">ISO 3166 2 alpha Landcodes</desc>
        </oid>
    :)
    let $registryResults        := 
        if (empty($searchTerms)) then () else if ($localonly) then () else (
            let $luceneQuery    := utillib:getSimpleLuceneQuery($searchTerms)
            let $luceneOptions  := utillib:getSimpleLuceneOptions()
            let $objects        := $setlib:colOidsData//oid[@oid]
            return
                if (count($searchTerms)=1 and matches($searchTerms[1],'^\d+(\.\d+)*$')) then
                    $objects[ends-with(@oid, $searchTerms)]
                else 
                if (count($searchTerms)=1 and matches($searchTerms[1],'^[0-2](\.(0|[1-9][0-9]*))*$')) then
                    $objects[@oid = $searchTerms] | $objects[starts-with(@oid, concat($searchTerms, '.'))]
                else (
                    $objects[ft:query(name | desc, $luceneQuery)]
                )
        )
    
    (: Yields
        <id root="2.16.840.1.113883.2.4.6.3">
            <designation language="nl-NL" type="preferred" displayName="Burgerservicenummer BSN">Landelijk Nederlands Burger Service Nummer, dient ter identificatie van patiënten of zorgcliënten. Formaat: 9N, met voorloopnullen indien korter dan 9 cijfers. </designation>
            <designation language="en-US" type="preferred" displayName="Dutch National Person ID">Dutch National Person ID</designation>
        </id>
    :)
    let $projectResults         := 
        if (count($searchTerms)=0) then 
            $projectIds
        else (
            let $luceneQuery    := utillib:getSimpleLuceneQuery($searchTerms)
            let $luceneOptions  := utillib:getSimpleLuceneOptions()
            return
                if (count($searchTerms)=1 and matches($searchTerms[1],'^\d+(\.\d+)*$')) then
                    $projectIds[ends-with(@root, $searchTerms)]
                else 
                if (count($searchTerms)=1 and matches($searchTerms[1],'^[0-2](\.(0|[1-9][0-9]*))*$')) then
                    $projectIds[@root = $searchTerms] | $projectIds[starts-with(@root, concat($searchTerms, '.'))]
                else (
                    $projectIds[ft:query(designation/@displayName, $luceneQuery)]
                )
        )
    
    (: shortest match first when searching. otherwise respect sort parameter :)
    let $results                :=
        if (empty($searchTerms)) then
            switch ($sort)
            case 'id'
            case 'oid' return (
                switch ($sortorder)
                case 'descending' return
                    for $object in $registryResults | $projectResults
                    let $oid            := $object/@root
                    order by replace(replace(concat($oid, '.'), '\.', '.0000000000'), '.0*([0-9]{9,})', '.$1') descending
                    return $object
                default return
                    for $object in $registryResults | $projectResults
                    let $oid            := $object/@root
                    order by replace(replace(concat($oid, '.'), '\.', '.0000000000'), '.0*([0-9]{9,})', '.$1')
                    return $object
            )
            case 'name'
            case 'displayname' return
                switch ($sortorder)
                case 'descending' return
                    for $object in $registryResults | $projectResults
                    let $oid            := $object/@root
                    order by (if ($object[name]) then $object/name else $object/*/@displayName)[1] descending
                    return $object
                default return
                    for $object in $registryResults | $projectResults
                    let $oid            := $object/@root
                    order by (if ($object[name]) then $object/name else $object/*/@displayName)[1]
                    return $object
            (: not sorting today apparently :)
            default return
                $registryResults | $projectResults
        else (
            for $object in $registryResults | $projectResults
            let $oid           := $object/@oid | $object/@root
            group by $oid
            order by string-length((if ($object[name]) then $object/name else $object/*/@displayName)[1])
            return $object[1]
        )
    
    let $count          := count($results)
    let $max            := $count
    let $allcnt         := $count
    
    (: Rewrite all results as if they were all the project, and mark where they came from :)
    return
        <list artifact="IDS" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements(
                if (empty($searchTerms) or $localonly) then $results else (
                    for $object in $results
                    return
                    <id root="{$object/@oid | $object/@root}">
                    {
                        $object/@extension,
                        $object/@assigningAuthority
                        ,
                        if ($object[ancestor::oidList/@name]) then 
                            attribute registry {$object/ancestor::oidList/@name} 
                        else ( 
                            attribute prefix {$object/ancestor::decor/project/@prefix}
                        )
                        ,
                        $object/designation
                        ,
                        for $node in $object/name
                        let $lang   := $node/@language
                        return
                            <designation language="{$lang}" type="preferred" displayName="{data($node)}"/>
                        ,
                        $object/desc
                        ,
                        for $node in $object/property
                        return
                            <property name="{($node/@name | $node/@type)[1]}">{$node/@datatype, data(($node/@value, $node)[1])}</property>
                    }
                    </id>
                )
            )
        }
        </list>
};

(:~ Retrieves DECOR id from project based on $projectPrefixOrId.
    @param $projectPrefixOrId required path part which may be its prefix or its oid
    @param $id required path part which is the oid of the id to retrieve information for
    @param $version optional date time of a DECOR release instead of the live version
    @param $language optional string parameter for the exact release language to retrieve from
    @return project as JSON object
    @since 2020-05-03
:)
declare function prapi:getDecorProjectId($request as map(*)) {

    let $project                := $request?parameters?project[string-length() gt 0]
    let $id                     := $request?parameters?id[string-length() gt 0]
    let $projectVersion         := $request?parameters?release[string-length() gt 0]
    let $projectLanguage        := $request?parameters?language[string-length() gt 0]
    
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    let $check                  :=
        if (empty($id)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter id')
        else ()
    
    let $results                := prapi:getDecorId($project, $id, $projectVersion, $projectLanguage)

    return
        if (empty($results)) then (
            roaster:response(404, "application/json", ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat('"Found multiple ids for "', $id, '" in project "', $project, '". Expected 0..1. Alert your administrator as this should not be possible.'))
        )
        else (
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
        )

};

(:~ Retrieves DECOR project logo or a list thereof

    @param $project                 - required path part which may be its prefix or its oid
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @param $projectLanguage         - optional parameter to select from a specific compiled language
    @param $projectLogo             - optional uri. request a specific logo to be returned. param is ignored when mode='list'
    @param $mode                    - optional boolean. If true returns the list of available logos
    @return project logo or list object with avaialble logos
    @since 2020-05-03
:)
declare function prapi:getDecorProjectLogo($request as map(*)) {

    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    let $projectVersion         := $request?parameters?release
    let $projectLanguage        := $request?parameters?language
    let $projectLogo            := $request?parameters?logo[string-length() gt 0]
    let $mode                   := $request?parameters?mode
    
    let $decor                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else (
            if (utillib:isOid($project)) then utillib:getDecorById($project) else utillib:getDecorByPrefix($project)
        )
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $project, ''', does not exist.'))
        )
    let $projectPrefix          := $decor/project/@prefix
    let $decorProject           := $decor/project

    return
        if ($mode = 'list') then (
            let $logodir    := concat(util:collection-name($decorProject),'/',$decorProject/@prefix,'logos/')
            let $configured := $decorProject/reference/@logo | $decorProject/copyright/@logo
            (: These logos exist in the logos collection but are not used :)
            let $notinuse   := xmldb:get-child-resources($logodir)[not(. = $configured)]
            let $count      := count($configured) + count($notinuse)
            let $max        := $count
            let $allcnt     := $count
            return
            <list artifact="LOGOS" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
            {
                utillib:addJsonArrayToElements(
                    (for $logo in $configured
                    return
                        element {name($logo/..)} {$logo/../@by, $logo})
                    |
                    (for $logo in $notinuse
                    return
                        <otherlogo logo="{$logo}"/>)
                )
            }
            </list>
        )
        else (
            let $logo           := if (empty($projectLogo)) then $decorProject/reference/@logo[string-length()>0] else $projectLogo
            let $logosrc        := 
                if (empty($logo)) then () else (
                    if (starts-with($logo,'http')) then ($logo) else (
                        (:assume the logo is in project-logos collection:)
                        concat(util:collection-name($decorProject),'/',$decorProject/@prefix,'logos/',$logo)
                    )
                )
            let $logosrc        :=
                if (util:binary-doc-available($logosrc)) then ($logosrc) else (
                    (:  final fallback is server logo. note that we only reach this point if no specific logo was 
                        requested or configured or if the requested/configured logo does not exist. if the server-logo 
                        does not load then this server should be configured better anyway... :)
                    let $projectLogo   := serverapi:getServerLogo()
                    return
                    if (starts-with($projectLogo,'http')) then ($projectLogo) else (
                        concat($prapi:artServerURL,'img/',$projectLogo)
                    )
                )
            let $logoext        := tokenize($logosrc,'\.')[last()]
            let $logofilename   := tokenize($logosrc,'/')[last()]
            
            return
            if (response:exists()) then (
                if (starts-with($logosrc,'http')) then (
                    response:redirect-to(xs:anyURI($logosrc))
                    (:roaster:response(302, (), (), map { "Location": xs:anyURI($logosrc) }):)
                ) else (
                    (: instruct client to cache 7 days. which is 7 * 24 * 60 * 60 = 604800 seconds :)
                    response:set-header("Cache-Control", "max-age=604800"),
                    response:stream-binary(util:binary-doc($logosrc), concat('image/',$logoext), $logofilename)
                    (:roaster:response(200, concat('image/', $logoext), util:binary-doc($logosrc), map { "Cache-Control": "max-age=604800" }):)
                )
            )
            else (
                (: testing from eXide or oXygen:)
                <logo src="{$logosrc}" mediaType="{concat('image/',$logoext)}" name="{$logofilename}"/>
            )
        )
};

(:~ Retrieves DECOR image/blob from project subfolder

    @param $project                 - required path part which may be its prefix or its oid
    @param $name                    - name of the specific blob to be returned
    @return image/blob
    @since 2022-10-12
:)
declare function prapi:getDecorProjectBlob($request as map(*)) {

    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    let $blob                   := $request?parameters?name[string-length() gt 0]
    
    let $decor                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else
            if (utillib:isOid($project)) then utillib:getDecorById($project) else utillib:getDecorByPrefix($project)
    
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $project, ''', does not exist.'))
        )
    
    let $projectPrefix          := $decor/project/@prefix
    let $decorProject           := $decor/project
    
    (:let $check                  :=
        error(xs:QName('prapi:BlobName'), 'Blob name: ' || $blob):)
    
    (: get possible blob/image loction and check whether it exists :)
    let $blobsrc1               :=
        if (empty($blob)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter name (of blob)')
        else
            concat(util:collection-name($decorProject), '/', $projectPrefix, 'blobs/', $blob)
        
    let $supportedExtensions    := ('.png', '.jpg', 'jpeg', '.gif')
    let $blobext                := tokenize($blob,'\.')[last()]
    let $blobsrc                :=
        if (concat('.', $blobext) = $supportedExtensions) then
            if (util:binary-doc-available($blobsrc1)) then $blobsrc1 else ()
        else (
            for $fns in $supportedExtensions
            return 
                if (util:binary-doc-available(concat($blobsrc1, $fns))) then concat($blobsrc1, $fns) else ()
        )
        
    let $blobext                := tokenize($blobsrc,'\.')[last()]
    let $blobfilename           := tokenize($blobsrc,'/')[last()]
    
    (:let $check                  :=
        if (string-length($blobsrc) = 0) then error(xs:QName('prapi:BlobNotFound'), 'Blob ' || $blob || ' not found as ' || $blobsrc1) else ():)
 
    return
        if (response:exists() and string-length($blobfilename) > 6) then (
            (: instruct client to cache 7 days. which is 7 * 24 * 60 * 60 = 604800 seconds :)
            response:set-header("Cache-Control", "max-age=604800"),
            response:stream-binary(util:binary-doc($blobsrc), concat('image/',$blobext), $blobfilename)
        ) else (
            (: this is a "missing image" binary inline image to return upon failure :)
            response:stream-binary($prapi:MISSING-IMAGE-INLINE-IMG, 'image/png', 'missing-blob.png')
        )
};

(:~ Creates DECOR image/blob in project subfolder

    @param $project                 - required path part which may be project's prefix or oid
    @body  <file type=“{media type}“ base64=“{img b64 encoded}“/>
    @return name of the blob in blob directory
    @since 2022-10-12
:)
declare function prapi:postDecorProjectBlob($request as map(*)) {

    let $authmap                := $request?user
    let $project                := tokenize(normalize-space(string-join($request?parameters?project, ' ')), ' ')[string-length() gt 0][not(. = '*')]
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    let $check                  :=
        if (empty($request?body)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()

    let $embeddeduri            := $request?body?base64
    (: split this embedded img, format eg "data:image/png;base64,iVBORw0KGgoAAAANSUh…", into media type and b64 binary data of the image :)

    let $check                  :=
        if (string-length($embeddeduri) < 5) then 
            error($errors:BAD_REQUEST, 'Request too small (' || string-length($embeddeduri) || ' bytes) ' || $embeddeduri)
        else ()

    let $blobdata               := substring-after($embeddeduri, 'base64')
    let $blobdata               := normalize-space(substring-after($blobdata, ','))
    let $mediaType              := $request?body?type

    let $decor                  := if (utillib:isOid($project)) then utillib:getDecorById($project) else utillib:getDecorByPrefix($project)
    let $projectPrefix          := $decor/project/@prefix
    let $decorProject           := $decor/project

    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $project, ''', does not exist.'))
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-SCENARIOS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify scenarios in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )

    let $stamp                  := util:uuid()
    let $basecollectionpath     := util:collection-name($decorProject)
    let $blobcollection         := concat($projectPrefix, 'blobs')
    
    (: make sure a blob collection is available and set access rights :)
    let $bcreate                := xmldb:create-collection($basecollectionpath, $blobcollection)
    let $return                 := sm:chmod(xs:anyURI(concat('xmldb:exist://', $bcreate)), 'rwxrwsr-x')

    (: get the blob suffix based onn the media type :)
    let $blobsuffix             := if ($mediaType = 'image/png') then 'png' else if ($mediaType = ('image/jpg','image/jpeg')) then 'jpg' else if ($mediaType = 'image/gif') then 'gif' else ''
    let $blobtarget             := concat($stamp, '.', $blobsuffix)
        
    (: write the blob with proper suffix if everything is ok :)
    let $return             :=
        if (string-length($blobsuffix) gt 0) then
            xmldb:store(concat($basecollectionpath, '/', $blobcollection), xmldb:encode($blobtarget), xs:base64Binary($blobdata), $mediaType)
        else ()
    
    let $alttext                 :=
        if (string-length($blobdata) < 2)
        then 'ART-DECOR Missing Blob Image, no data'
        else if (string-length($blobsuffix) = 0)
        then 'ART-DECOR Missing Blob Image, unknown media type'
        else concat('ART-DECOR Blob Image ', $stamp)
    
    return <img src="{concat('/api/project/', $projectPrefix, '/blob/', $stamp)}" alt="{$alttext}"/>
        
};

declare function prapi:postDecorProjectCodeSystemsInCADTS($request as map(*)) {
    
    let $authmap                := $request?user
    let $project                := tokenize(normalize-space(string-join($request?parameters?project, ' ')), ' ')[string-length() gt 0][not(. = '*')]
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    
    let $decor                  := 
        for $p in $project
        return
            if (utillib:isOid($p)) then utillib:getDecorById($p) else utillib:getDecorByPrefix($p)
    let $check                  :=
        if (empty($project)) then
            if ($authmap?groups = 'dba') then () else (
                error($errors:FORBIDDEN, 'You have to be dba to run for the full server')
            )
        else (
            for $d in $decor
            return
                if (decorlib:authorCanEditP($authmap, $d, $decorlib:SECTION-TERMINOLOGY)) then () else (
                    error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify codeSystems in project ', $d/project/@prefix, '. You have to be an active author in the project.'))
                )
        )
    
    let $now                    := substring(string(current-dateTime()), 1, 19)
    let $projectPrefixes        := if ($decor) then sort($decor/project/@prefix) else ('*')
    let $write                  := 
        for $projectPrefix in $projectPrefixes
        let $stamp                  := util:uuid()
        let $projectcheck-request   :=  
            <projectcodesystemconvert-request uuid="{$stamp}" for="{$projectPrefix}" on="{current-dateTime()}" as="{$stamp}" by="{$authmap?name}"
                                              progress="Added to process queue ..." progress-percentage="0"/>
        let $write                  := xmldb:store($setlib:strDecorScheduledTasks, 'projectcodesystemconvert-' || $projectPrefix[not(. = '*')] || replace($now, '\D', '') || '.xml', $projectcheck-request)
        let $tt                     := sm:chmod($write, 'rw-rw----')
        return
            ()
    return
        prapi:getProjectScheduledTasks($authmap, $projectPrefixes, (), (), (), (), ())
};

declare %private function prapi:svrl2report($resulttransform)  as element(report) {
    <report>
        <status rulescnt="{count($resulttransform//svrl:failed-assert | $resulttransform/svrl:successful-report)}">
        {
            if ($resulttransform/svrl:failed-assert[empty(@role)] | $resulttransform/svrl:successful-report[empty(@role)] |
                $resulttransform/svrl:failed-assert[@role = 'error'] | $resulttransform/svrl:successful-report[@role = 'error'])
            then 'invalid'
            else
            if ($resulttransform/svrl:failed-assert[@role = 'warning'] | $resulttransform/svrl:successful-report[@role = 'warning'] |
                $resulttransform/svrl:failed-assert[@role = 'warn'] | $resulttransform/svrl:successful-report[@role = 'warn'])
            then 'warning'
            else
            if ($resulttransform/svrl:failed-assert[@role = 'info'] | $resulttransform/svrl:successful-report[@role = 'info'])
            then 'info'
            else 'valid'
        }
        </status>
        <message>
        {
            (:
            <svrl:failed-assert location="/Q{}decor[1]/Q{}datasets[1]/Q{}dataset[1]/Q{}concept[1]/Q{}concept[3]" role="info" test="not($isInTransactionWithConnectedTemplate and $allTemplates) or $isInTemplate">
                <svrl:text>INFO: concept (type='group') is used in at least one transaction connected to a template, but does not have a templateAssociation. | Location &lt;concept id="2.16.840.1.113883.3.1937.99.62.3.2.18" effectiveDate="2011-01-28T00:00:00" name="Gegevens gewichtstoename"/&gt;</svrl:text>
            </svrl:failed-assert>
            :)
        
            for $node in $resulttransform/svrl:failed-assert | $resulttransform/svrl:successful-report
            return
                element {local-name($node)} {
                    $node/@*,
                    for $subnode in $node/*
                    return
                        element {local-name($subnode)} {$subnode/@*, $subnode/node()}
                }
        }
        </message>
    </report>
};

(: ======================= RUNTIME START  ======================== :)

declare function prapi:getProjectRuntime($request as map(*)) {
    
    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    let $specificref            := $request?parameters?compilationId[string-length() gt 0]
    let $download               := $request?parameters?download = true()
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
        
    let $decor                  := if (utillib:isOid($project)) then utillib:getDecorById($project) else utillib:getDecorByPrefix($project)
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $project, ''', does not exist.'))
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-PROJECT)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to create a runtime version of project ', $project, '. You have to be an active author in the project.'))
        )
    (: if $specificref = 'dynamic' then return the last compilation if any, if a uuid return the one specified, otherwise it is in error :)
    let $theref                  :=
        if (lower-case($specificref) = 'dynamic')
        then (
            let $compilations           := $setlib:colDecorVersion//compiled[@for = $projectPrefix][ancestor::compilation][@status = 'active']
            let $latestdt               := max($compilations/xs:dateTime(@on))
            return string($compilations[@on = $latestdt]/@as)
        ) else $specificref
        let $check                  :=
            if (empty($theref))
                then ()
                else
                    if (matches($theref, '^([1-9][0-9]*(\.[0-9]+)*)|([0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12})$', 'i')) 
                    then () 
                    else (
                        error($errors:BAD_REQUEST, 'Parameter compilationId SHALL be an oid or uuid or the keyword "dynamic". Found: ' || $theref)
                    )
    let $check                  :=
        if ($download) then 
            if (empty($theref)) then
                error($errors:BAD_REQUEST, 'To download you also need to specify parameter compilationId')
            else ()
        else ()
    
    let $results                := prapi:getProjectRuntime($projectPrefix, $theref, $download)
     
    return
        if (empty($results)) then 
            roaster:response(404, ())
        else (
            $results
        )
};

(:~ Retrieve listing of runtime environments

@param $projectPrefix - required. project prefix of the project to retrieve the listing for
@param $specificref - optional. reference to a particular runtime environmnent. Required only if $download is true
@param $download - required. If true retieves the $specificref as zip
@return zip file or xml listing of environments
:)
declare function prapi:getProjectRuntime($projectPrefix as xs:string, $specificref as xs:string?, $download as xs:boolean) {
    if ($download) then (
        let $compiledF              := $setlib:colDecorVersion//compiled[@for = $projectPrefix][@as = $specificref][ancestor::compilation][@status = 'active']/zip
        
        return
            if ($compiledF) then (
                response:set-header("Cache-Control", "max-age=604800"),
                response:stream-binary(xs:base64Binary($compiledF/text()), 'application/zip', $specificref || '.zip')
            )
            else (
                roaster:response(404, 'application/zip')
            )
    )
    else (
        let $compilerequestF        :=
            if (string-length($specificref) > 0)
            then
                collection($setlib:strDecorScheduledTasks)/compile-request[@for = $projectPrefix][@as = $specificref]
            else
                collection($setlib:strDecorScheduledTasks)/compile-request[@for = $projectPrefix]
        let $compiledF              :=
            if (string-length($specificref) > 0)
            then
                $setlib:colDecorVersion//compiled[@for = $projectPrefix][@as = $specificref][ancestor::compilation]
            else
                $setlib:colDecorVersion//compiled[@for = $projectPrefix][ancestor::compilation]
    
        let $count                  := count($compiledF | $compilerequestF)
        
        let $compiledF              :=
            for $c in $compiledF
            let $as   := $c/@as
            group by $as
            order by $c/@on descending
            return $c[1]
        
        let $results                :=
            <list artifact="RUNTIME" current="{$count}" total="{$count}" all="{$count}" lastModifiedDate="{current-dateTime()}">
            {
                for $c in $compilerequestF
                order by $c/@on descending
                return
                    element {name($c)}
                    {
                        $c/@*
                    }
                ,
                for $c at $count in $compiledF
                return
                    <compiled>
                    {
                        $c/@* except $c/@status,
                        if ($count < 3) then $c/@status else attribute {'status'} {'retired'},
                        $c/dir
                    }
                    </compiled>
            }
            </list>
    
        for $result in $results
        return
            element {name($result)} {
                $result/@*,
                namespace {"json"} {"http://www.json.org"},
                utillib:addJsonArrayToElements($result/*)
            }
    )
};

(:~ Create new runtime environment and return the listing of runtime environments

@param $project - required. project prefix or id of the project to create the environment for
@return xml listing of environments
:)
declare function prapi:postProjectRuntime($request as map(*)) {
    
    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    let $check                  :=
        if (empty($request?body)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $data                   := utillib:getBodyAsXml($request?body, 'decor-parameters', ()) 
    
    let $doClosed               := $data/@switchCreateSchematronWithWarningsOnOpen = 'true'
    let $doExplicitIncludes     := $data/@switchCreateSchematronWithExplicitIncludes = 'true'
    let $doDieOnRecursion       := false()
    let $forceRecompile         := true()
    let $testFilters            := false()
    let $runtimeOnly            := true()
           
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
       
    let $decor                  := if (utillib:isOid($project)) then utillib:getDecorById($project) else utillib:getDecorByPrefix($project)
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $project, ''', does not exist.'))
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-PROJECT)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to create a runtime version of project ', $project, '. You have to be an active author in the project.'))
        )
    
    let $now                    := substring(xs:string(current-dateTime()), 1, 19)
    let $stamp                  := util:uuid()
    let $author                 := $decor/project/author[@username = $authmap?name]
    let $author                 := if (empty($author)) then $authmap?name else $author
    let $language               := $decor/project/@defaultLanguage
    let $timeStamp              := 'development'
    
    let $check                  :=
        if (count($projectPrefix) = 1) then () else (
            error($errors:BAD_REQUEST, 'Project parameter ' || $project || ' SHALL refer to exactly one project. Found: ' || count($decor))
        )
        
    (: will create version/develop collection if it does not exist :)
    let $openOrClosed           := if ($doClosed) then <switchCreateSchematronWithWarningsOnOpen1/> else <switchCreateSchematronWithWarningsOnOpen0/>
    let $includeExplicitOrAll   := if ($doExplicitIncludes) then <switchCreateSchematronWithExplicitIncludes1/> else <switchCreateSchematronWithExplicitIncludes0/>
    let $dieOrContinue          := if ($doDieOnRecursion) then 'die' else 'continue'
    
    let $publication-parameters :=
        <parameters>
            <param name="outputBaseUriPrefix" value="{concat('xmldb:exist://', $setlib:strDecorVersion, '/', replace($projectPrefix, '-$', ''), '/development/', $stamp, '/')}"/>
            <param name="scriptBaseUriPrefix" value="{concat('xmldb:exist://', $setlib:strDecorCore, '/')}"/>
            <param name="inDevelopmentString" value="true"/>
            <param name="onCircularReferences" value="{$dieOrContinue}"/>
            <param name="seeThisUrlLocation" value="live"/>
            <param name="createSchematronbasedOn" value="scenario"/>
            <param name="switchCreateSchematronWithWarningsOnOpenString" value="{$doClosed}"/>
            <param name="switchCreateSchematronWithExplicitIncludesString" value="{$doExplicitIncludes}"/>
            <param name="logLevel" value="OFF"/>
        </parameters>
        
    let $filterset              := comp:prepareFilterSet($data/filters)
    let $filters                := 
        if (comp:isCompilationFilterActive($filterset)) then 
            comp:getFinalCompilationFilters($decor, comp:getCompilationFilters($decor, $filterset, ()))
        else ()
    
    let $compile-request        :=  
        <compile-request uuid="{$stamp}" for="{$projectPrefix}" on="{$now}" as="{$stamp}" by="{normalize-space($author)}" language="{$language}" testfilters="{$testFilters}" runtimeonly="{$runtimeOnly}" forcecompile="{$forceRecompile}"
            closed="{$doClosed}" explicitIncludes="{$doExplicitIncludes}" progress="Added to process queue ..." progress-percentage="0">
        {
            if ($filters) then 
                attribute filterlabel {($filters/@label, $filters/@id)[1]} |
                attribute transactions {$filters/transaction/concat(@ref, '--', @flexibility)}
            else (),
            $filters, $publication-parameters
        }
        </compile-request>
    let $write                  := xmldb:store($setlib:strDecorScheduledTasks, $projectPrefix || replace($now, '\D', '') || '.xml', $compile-request)
    let $tt                     := sm:chmod($write, 'rw-rw----')
    
    (:
        1. Compile project
        2. Build schematron
    :)
    return
        prapi:getProjectRuntime($projectPrefix, (), false())
};

(:~ Deletes runtime environment

@param $project - required. project prefix or id of the project to create the environment for
@param $specificref - required. reference to the runtime environmnent
@return xml listing of environments
:)
declare function prapi:deleteProjectRuntime($request as map(*)) {
    
    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    let $specificref            := $request?parameters?compilationId[string-length() gt 0]
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    let $check                  :=
        if (matches($specificref, '^([1-9][0-9]*(\.[0-9]+)*)|([0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12})$', 'i')) then () else (
            error($errors:BAD_REQUEST, 'Parameter compilationId SHALL be an oid or uuid. Found: ' || $specificref)
        )
    
    let $decor                  := if (utillib:isOid($project)) then utillib:getDecorById($project) else utillib:getDecorByPrefix($project)
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if (count($projectPrefix) = 1) then () else (
            error($errors:BAD_REQUEST, 'Project parameter ' || $project || ' SHALL refer to exactly one project. Found: ' || count($decor))
        )
    
    let $part1                  := concat('xmldb:exist://', $setlib:strDecorVersion, '/', substring($projectPrefix, 1, string-length($projectPrefix) - 1), '/development/')
    let $tmp1                   := 
        if (xmldb:collection-available($part1)) then (
            if (doc-available(concat($part1, $projectPrefix, $specificref, '.xml'))) then xmldb:remove($part1, concat($projectPrefix, $specificref, '.xml')) else (),
            if (xmldb:collection-available(concat($part1, $specificref))) then xmldb:remove(concat($part1, $specificref)) else ()
        )
        else () 
    
    let $part1                  := collection($setlib:strDecorScheduledTasks)/compile-request[@for = $projectPrefix][@as = $specificref]
    let $tmp3                   := if ($part1) then xmldb:remove(util:collection-name($part1), util:document-name($part1)) else ()
    
    return
        roaster:response(204, ())
};

(: ======================= RUNTIME END  ======================== :)

(: ======================= SCHEDULED-TASK START  ======================== :)

declare function prapi:getProjectScheduledTasks($request as map(*)) {
    
    let $authmap                := $request?user
    let $project                := tokenize(normalize-space(string-join($request?parameters?project, ' ')), ' ')[string-length() gt 0]
    let $taskId                 := $request?parameters?id[string-length() gt 0]
    let $taskType               := $request?parameters?type[string-length() gt 0]
    let $taskBy                 := $request?parameters?by[string-length() gt 0]
    let $sort                   := $request?parameters?sort[string-length() gt 0]
    let $sortOrder              := $request?parameters?sortorder[string-length() gt 0]
    
    (:let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ():)
    
    let $results                := prapi:getProjectScheduledTasks($authmap, $project, $taskId, $taskType, $taskBy, $sort, $sortOrder)
    
    return
        $results
};

declare function prapi:getProjectScheduledTasks($authmap as map(*)?, $project as xs:string*, $taskId as xs:string*, $taskType as xs:string*, $taskBy as xs:string*, $sort as xs:string?, $sortorder as xs:string?) {
    
    let $decor                  := 
        for $p in $project[not(. = ('*', '*cache*'))]
        let $d  := if (utillib:isOid($p)) then utillib:getDecorById($p) else utillib:getDecorByPrefix($p)
        return
            if ($d) then $d else (
                error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $p, ''', does not exist.'))
            )
    let $projectPrefixes        := if ($decor) then sort($decor/project/@prefix) else if ($project = '*cache*') then $project else ('*')
    
    (:if you are author in one, then you are author in all:)
    let $userIsAuthor           := if (empty($authmap)) then false() else decorlib:authorCanEditP($authmap, $decor[1], $decorlib:SECTION-PROJECT)
    
    let $sortorder              := $sortorder[. = 'descending']
    (: get all scheduled tasks for this project :)
    let $allRequests            := collection($setlib:strDecorScheduledTasks)/*[@for = $projectPrefixes]
    (:let $allRequests            := collection($setlib:strDecorScheduledTasks)/*[@for = $projectPrefix]:)
    let $allCount               := count($allRequests)
    let $allRequests            := if (empty($taskId)) then $allRequests else $allRequests[@uuid = $taskId]
    let $allRequests            := if (empty($taskType)) then $allRequests else $allRequests[name() = $taskType]
    let $allRequests            := if (empty($taskBy)) then $allRequests else $allRequests[@by= $taskBy]
    
    let $count                  := count($allRequests)
    
    let $results                :=
        switch ($sort)
        case 'type' return
            if (empty($sortorder)) then 
                for $request in $allRequests order by $request/name() return $request
            else
                for $request in $allRequests order by $request/name() descending return $request
              
        case 'by' return
            if (empty($sortorder)) then 
                for $request in $allRequests order by $request/@by return $request
            else
                for $request in $allRequests order by $request/@by descending return $request   
              
        default return
            if (empty($sortorder)) then 
                for $request in $allRequests order by $request/@on return $request
            else
                for $request in $allRequests order by $request/@on descending return $request     

    let $results                :=
        if ($userIsAuthor) then $results else (
            for $request in $results return element {name($request)} {$request/(@* except @by), $request/node()}
        )
    
    return
        <list artifact="SCHEDULEDTASKS" current="{$count}" total="{$count}" all="{$allCount}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org"> 
        {
            utillib:addJsonArrayToElements($results)
        }
        </list>
};

declare function prapi:deleteProjectScheduledTasks($request as map(*)) {
    
    let $authmap                := $request?user
    let $project                := tokenize(normalize-space(string-join($request?parameters?project, ' ')), ' ')[string-length() gt 0][not(. = '*')]
    let $taskId                 := $request?parameters?id[string-length() gt 0]
    let $taskType               := $request?parameters?type[string-length() gt 0]
    let $taskBy                 := $request?parameters?by[string-length() gt 0]
    let $tasksInError           := $request?parameters?error[string-length() gt 0] = true()
    let $sort                   := $request?parameters?sort[string-length() gt 0]
    let $sortOrder              := $request?parameters?sortorder[string-length() gt 0]
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
   
    let $decor                  := 
        for $p in $project[not(. = ('*', '*cache*'))]
        return
            if (utillib:isOid($p)) then utillib:getDecorById($p) else utillib:getDecorByPrefix($p)
    
    let $check                  :=
        for $d in $decor
        return
            if ($authmap?groups = 'dba') then true() else if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-PROJECT)) then () else (
                error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to delete scheduled tasks for project ', $d/project/@prefix, '. You have to be an active decor-admin author in the project.'))
            )
    let $projectPrefixes        := if ($decor) then sort($decor/project/@prefix) else if ($project = '*cache*') then $project else ('*')
    
    let $allRequests            := collection($setlib:strDecorScheduledTasks)/*[@for = $projectPrefixes]
    let $allRequests            := if (empty($taskId)) then $allRequests else $allRequests[@uuid = $taskId]
    let $allRequests            := if (empty($taskType)) then $allRequests else $allRequests[name() = $taskType]
    let $allRequests            := if (empty($taskBy)) then $allRequests else $allRequests[@by= $taskBy]
    let $allRequests            := if ($tasksInError) then $allRequests[@busy = 'error'] else $allRequests
    
    (: if a specific id was requested, give error if that is running. In all other cases just skip those without mention :)
    let $check                  :=
        if (empty($taskId)) then () else if ($allRequests[@busy = 'true']) then
            error($errors:BAD_REQUEST, 'You cannot delete a running task. Please wait until the task is either done or in error state, before trying again.')
        else ()
    
    (:let $check                  := error($errors:BAD_REQUEST, 'Found ' || count($allRequests) || ' requests for deletion.' ):)
    
    let $delete                 := 
        for $r in $allRequests
        return
            if ($r[@busy = 'true']) then () else (
                xmldb:remove(util:collection-name($r), util:document-name($r))
            )
    
    let $results                := prapi:getProjectScheduledTasks($authmap, sort($decor/project/@prefix), $taskId, $taskType, $taskBy, $sort, $sortOrder)
     
    return
        roaster:response(200, (), $results, map { "Last-Modified": $results/@lastModifiedDate })
};

(: =======================  CHECK START  ======================== :)

(:~ Get a projects consistency check results
    @param $projectPrefixOrId required path part which may be its prefix or its oid
    @return check results
    @since 2021-01-19
:)
declare function prapi:getProjectCheck($request as map(*)) {

    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    let $datesOnly              := $request?parameters?datesonly = true()
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    
    let $decor                  := if (utillib:isOid($project)) then utillib:getDecorById($project) else utillib:getDecorByPrefix($project)
    
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $project, ''', does not exist.'))
        )
    
    let $projectPrefix          := $decor/project/@prefix
    let $results                := doc(setlib:strProjectDevelopmentDoc($projectPrefix, false()))/*
    let $requests               := collection($setlib:strDecorScheduledTasks)/projectcheck-request[@for = $projectPrefix] 
  
    let $current                := count($results)
    let $total                  := count($requests | $results)
    let $allcnt                 := $total
    
    return
        <list artifact="REPORTS" current="{$current}" total="{$total}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            if ($datesOnly) then (
                <report json:array="true">{$results/@*}</report>
            ) else (
                for $request in $requests
                order by $request/@on descending
                return
                    <report json:array="true" checked="false">{$request/@compiled, $request/@busy, $request/@progress, $request/@progress-percentage}</report>
                ,
                utillib:addJsonArrayToElements($results)
            )
            
        }
        </list>
};

(:~ Check a projects consistency
    @param $projectPrefixOrId required path part which may be its prefix or its oid
    @return check results
    @since 2021-01-19
:)
declare function prapi:postProjectCheck($request as map(*)) {

    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    let $check                  :=
        if (empty($request?body)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $decor                  := if (utillib:isOid($project)) then utillib:getDecorById($project) else utillib:getDecorByPrefix($project)
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $project, ''', does not exist.'))
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-PROJECT)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to check the project ', $project, '. You have to be an active author in the project.'))
        )
    
    let $data                   := utillib:getBodyAsXml($request?body, 'decor-parameters', ())
    let $doCompile              := $data/@compile = 'true'
    let $filters                := $data/filters
    let $filters                := if ($decor and $doCompile) then comp:getFinalCompilationFilters($decor, comp:getCompilationFilters($decor, $filters, ())) else ()
    let $now                    := substring(string(current-dateTime()), 1, 19)
    let $stamp                  := util:uuid()
    
    let $projectcheck-request   :=  
        <projectcheck-request uuid="{$stamp}" for="{$projectPrefix}" on="{current-dateTime()}" as="{$stamp}" by="{$authmap?name}" 
                             language="*" testfilters="false" runtimeonly="false" forcecompile="false" development="false" 
                             compile="{$doCompile}" progress="Added to process queue ..." progress-percentage="0">
        {
            if ($filters) then 
                attribute filterlabel {($filters/@label, $filters/@id)[1]} |
                attribute transactions {$filters/transaction/concat(@ref, '--', @flexibility)}
            else (),
            $filters
        }
        </projectcheck-request>
    let $write                  := xmldb:store($setlib:strDecorScheduledTasks, $projectPrefix || replace($now, '\D', '') || '.xml', $projectcheck-request)
    let $tt                     := sm:chmod($write, 'rw-rw----')
    
    return
        prapi:getProjectCheck($request)

};
(: =======================  CHECK END  ======================== :)

(: =================  INSTANCE VALIDATION START  ================ :)

(:~ Validate an incoming instance against some project transaction

(the base64 instance says: <instance>This is a test</instance>)

<validate-request transaction="[uuid]">
    <file name="LogoTest.png" size="2060" type="application/xml" base64="data:application/xml;base64,PGluc3RhbmNlPlRoaXMgaXMgYSB0ZXN0PC9pbnN0YW5jZT4="/>
</validate-request>
:)
declare function prapi:projectInstanceValidation($request as map(*)) {
    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    let $check                  :=
        if (empty($request?body)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $data                   := utillib:getBodyAsXml($request?body, 'validate-request', ())
    let $result                 := prapi:projectInstanceValidation($project, $data)
    
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

declare function prapi:projectInstanceValidation($projectPrefixOrId as xs:string, $data as element(validate-request)) as element(report) {
    (: check parameter project :)
    let $decor                  := if (utillib:isOid($projectPrefixOrId)) then utillib:getDecorById($projectPrefixOrId, (), ()) else utillib:getDecorByPrefix($projectPrefixOrId, (), ())
    
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $projectPrefixOrId, ''', does not exist.'))
        )
    
    let $projectPrefix          := $decor/project/@prefix
    let $schref                 := $data/@transaction
    let $compiledF              := $setlib:colDecorVersion//compiled/dir/resource[@ref=$schref]
    
    let $check                  :=
        if ($compiledF) then () else (
            error($errors:BAD_REQUEST, 'Project ' || $projectPrefixOrId || ' does not have a transaction associated with id ' || $schref)
        )
        
    let $mediatype              := $data/file/@type
    let $check                  :=
        if ($mediatype = ('application/xml', 'text/xml')) then () else (
            error($errors:BAD_REQUEST, 'Validation of file type ' || $mediatype || ' is not supported. Supported is application.xml or text/xml')
        )
    
    let $schematron             := if (empty($compiledF)) then '' else $compiledF/@path
    let $svrlfile               := replace($schematron, 'sch$', 'xsl')
    let $svrl                   := 
        if (doc-available($svrlfile)) then doc($svrlfile) else (
            error($errors:SERVER_ERROR, 'Project ' || $projectPrefixOrId || ', id ' || $schref || ' schematron ' || $compiledF/@name || 
                  ' does not have a matching SVRL file ending in .xsl. This should have been created in the runtime creation process.')
        )
    
    let $tmp                    := if ($data/file/@base64[starts-with(., 'data:')]) then substring-after($data/file/@base64, 'base64,') else $data/file/@base64
    let $tmp                    := util:base64-decode($tmp)
    (:Hack alert: upload fails when content has UTF-8 Byte Order Marker. the UTF-8 representation of the BOM is the byte sequence 0xEF,0xBB,0xBF:)
    let $filecontent            := if (string-to-codepoints(substring($tmp,1,1))=65279) then substring($tmp,2) else $tmp
    
    (: must store the document because of the crappy @attribute transformation bug in exist :)
    let $input-coll             := string-join(tokenize($schematron, '/')[not(position()=last())], '/')
    let $testxml                := concat(util:uuid(), '.xml')
    let $inputfile              := xmldb:store($input-coll, $testxml, $filecontent)
    let $filecontent            := doc(concat($input-coll, '/', $testxml))
    (: :)
    
    (: CDA schema (if input is a ClinicalDocument :)
    let $baseCDAXSD             := '/db/apps/hl7/CDAr2/schemas_codeGen_flat/CDA.xsd'

    (: prepare for the use of fn:transform :)
    let $serialization-options    := map{
                                        "method": "xml",
                                        "media-type": "text/xml",
                                        "omit-xml-declaration": true(),
                                        "indent": false()
                                    }
    let $serialization-options    :=  "method=xml media-type='text/xml' omit-xml-declaration=no indent=no"           
    let $exist-parameters         := map:merge(
                                        map:entry(xs:QName("exist:stop-on-error"), "yes")
                                    )
    let $exist-parameters         := <parameters><param name="exist:stop-on-error" value="yes"/></parameters>
     
    return
        if (empty($filecontent)) then (
            <report at="{current-dateTime()}">
                <error>Missing input file</error>
                <request>{$filecontent}</request>
            </report>
        )
        else 
        if (string-length($schematron)=0) then (
            <report at="{current-dateTime()}">
                <error>Cannot find schematron package {$schref} {$compiledF}</error>
                <request>{$compiledF}</request>
            </report>
        )
        else (
            <report at="{current-dateTime()}" isCda="{exists($filecontent/hl7:ClinicalDocument)}" schema-path="{$baseCDAXSD}" schematron-path="{$schematron}" transaction="{$schref}">
                <schema>
                {
                    if (empty(doc($baseCDAXSD))) then
                        <report>
                            <status>invalid</status>
                            <duration unit="msec">0</duration>
                            <message level="Warning" line="1" column="1">+++ Validating against schema failed because the CDA schema cannot be found. Please (let) install the package "HL7 - CDA Release 2 XML-materials" on the server.</message>
                        </report>
                    else try { 
                        (: validate against CDA schema if needed :)
                        (:
                        <report>
                          <status>invalid</status>
                          <duration unit="msec">290</duration>
                          <message level="Error" line="10" column="67">cvc-complex-type.2.4.a: Invalid content was found starting with element '{"urn:hl7-org:v3":typerId}'. One of '{"urn:hl7-org:v3":realmCode, "urn:hl7-org:v3":typeId}' is expected.</message>
                        </report>
                        :)
                        if ($filecontent/hl7:ClinicalDocument) then 
                            validation:jaxv-report($filecontent, doc($baseCDAXSD))
                        else (
                            <report><status>valid</status><duration unit="msec">0</duration><message level="Error" line="1" column="1">XML Schema validation only implemented for HL7 ClinicalDocument</message></report>
                        )
                    }
                    catch * { 
                        <report><status>invalid</status><duration unit="msec">0</duration><message level="Error" line="1" column="1">+++ Validating against schema failed (0): {$err:code}: {$err:description}</message></report>
                    }
                }
                </schema>
                <schematron>
                {
                    (: validate against schematron :)
                    try {
                        let $resulttransform            := transform:transform($filecontent, $svrl, $exist-parameters)
                        (:
                        let $resulttransform            :=
                                fn:transform(map{
                                "source-node": $filecontent,
                                "stylesheet-node": $svrl,
                                "base-output-uri": $input-coll,
                                "serialization-params": $serialization-options,
                                "stylesheet-params": $exist-parameters,
                                "cache": false(),
                                "enable-messages": true()
                            })?output
                        :)

                        return (
                            <report>
                                <status rulescnt="{count($svrl//svrl:failed-assert)}">
                                {
                                    if ($resulttransform/svrl:failed-assert[@role='error'] | $resulttransform/svrl:failed-assert[empty(@role)])
                                    then 'invalid'
                                    else if ($resulttransform/svrl:failed-assert[@role='warning'] | $resulttransform/svrl:successful-report)
                                    then 'warning'
                                    else if ($resulttransform/svrl:failed-assert[@role='hint'] | $resulttransform/svrl:successful-report)
                                    then 'info'
                                    else 'valid'
                                }
                                </status>
                                <message>
                                {
                                    utillib:strip-namespace-in-svrl-locations($resulttransform)
                                }
                                </message>
                                <x>{$resulttransform}</x>
                            </report>
                        )
                    }
                    catch * {
                        <report>
                            <status>invalid</status>
                            <error>+++ Validating against schematron failed ({$err:code}): {$err:description}</error>
                        </report>
                    }
                }
                </schematron>
                <file>{fn:serialize($filecontent)}</file>
            </report>
        )
};

(: =================  INSTANCE VALIDATION END  ================ :)

(: =======================  FILTERS START  ======================== :)

(:~ Retrieve project compilation filters. Retrieves all available filter sets, and which if any is active :)
declare function prapi:getProjectFilters($request as map(*)) {
    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    
    let $decor                  := if (utillib:isOid($project)) then utillib:getDecorById($project) else utillib:getDecorByPrefix($project)
    
    let $check                  :=
        if (empty($decor)) then
            error($errors:BAD_REQUEST, 'Parameter project ' || $project || ' does not lead to a DECOR project.')
        else
        if (count($decor) = 1) then () else (
            error($errors:SERVER_ERROR, 'Parameter project ' || $project || ' leads to multiple (' || count($decor) || ') DECOR projects. Contact your system administrator.')
        )
    
    let $result                 := comp:getCompilationFilterSet($decor)
    
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

(:~ Update project filter settings. Expect array of parameter objects, each containing RFC 6902 compliant contents.

{ "op": "[add|remove|replace]", "path": "[/filter|/filterId|/filters]", "value": "[string|object]" }

@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $request-body             - required. json body containing array of parameter objects each containing RFC 6902 compliant contents
@return filter settings after patch
@since 2021-10-20
@see http://tools.ietf.org/html/rfc6902
:)
declare function prapi:patchProjectFilters($request as map(*)) {
    
    let $authmap                        := $request?user
    let $project                        := $request?parameters?project[string-length() gt 0]
    let $data                           := utillib:getBodyAsXml($request?body, 'parameters', ())
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    let $check                  :=
        if ($data) then () else (
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        )
    
    let $return                 := prapi:patchProjectFilters($authmap, $project, $data)
    return
        prapi:getProjectFilters($request)
};

(:~ Central logic for updating project filter settings

@param $authmap           - required. Map derived from token
@param $projectPrefixOrId - required. DECOR project id or prefix
@param $data              - required. DECOR xml element parameter elements each containing RFC 6902 compliant contents
@return filters object as xml with json:array set on elements
:)
declare function prapi:patchProjectFilters($authmap as map(*), $projectPrefixOrId as xs:string, $data as element(parameters)) {
    let $decor                  := 
        if (utillib:isOid($projectPrefixOrId)) then utillib:getDecorById($projectPrefixOrId) else utillib:getDecorByPrefix($projectPrefixOrId)
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $projectPrefixOrId, ''', does not exist.'))
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-PROJECT)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify filters of project ', $projectPrefix, '. You have to be an active decor-admin author in the project.'))
        )
    let $check                  :=
        if ($data[parameter]) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall be an array of ''parameter''.')
        )
    let $unsupportedops         := $data/parameter[not(@op = ('add', 'replace', 'remove'))]
    let $check                  :=
        if ($unsupportedops) then
            error($errors:BAD_REQUEST, 'Submitted parameters shall have supported ''op'' value. Found ''' || string-join(distinct-values($unsupportedops/@op), ''', ''') || ''', expected ''add'', ''replace'', or ''remove''') 
        else ()
    
    let $storedFilters          := comp:getCompilationFilterSet($decor)
    
    let $check                  :=
        for $param in $data/parameter
        let $op         := $param/@op
        let $path       := $param/@path
        let $value      := $param/@value
        let $pathpart   := substring-after($path, '/')
        return
            switch ($path)
            case '/filter' return (
                if ($op = 'remove') then
                    'Parameter path ''' || $path || ''' does not support ' || $op
                else 
                if ($value = ('true', 'false', 'on', 'off')) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Supported are: ' || string-join(('true', 'false', 'on', 'off'), ', ')
                )
            )
            case '/filterId' return (
                (: we 'could' check if the filterId matches a filters.id 'after' all patches are applied. But ... if the client really
                   wants to annoy himself with a non-existent filterId ... be my guest, the function comp:prepareFilterSet() overrides 
                   invalid ids and substitutes with the newest filter id or the first if available
                :)
            )
            case '/filters' return (
                if ($param/value[count(filters[@id]) = 1]) then (
                    if ($op = 'remove') then () else (
                        if ($param/value/filter[string-length(@label) = 0]) then
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Filter with id ' || $param/value/filter/@id || ' SHALL have a label with content.'
                        else (),
                        if ($param/value/filter[empty(transaction)]) then
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Filter with id ' || $param/value/filter/@id || ' SHALL have at least one transaction. Otherwise nothing is included in the compilation.'
                        else (),
                        if (($storedFilters | $data/parameter/value)/filters[not(@id = $param/value/filters/@id)][@label = $param/value/filters/@label]) then
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Filter with id ' || $param/value/filter/@id || ' SHALL have a unique label. Found a different filter with the same label ' || $param/value/filters/@label || '.'
                        else (),
                        for $transaction in $param/value/filter/transaction
                        let $trid                   := $transaction/@ref
                        let $tred                   := $transaction/@flexibility
                        let $storedTransaction      := utillib:getTransaction($trid, $tred)
                        return
                            if ($storedTransaction/ancestor::decor/project[@prefix = $projectPrefix]) then () else (
                                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Filter with id ' || $param/value/filter/@id || ' transaction ref ' || $trid || ' flexibility ' || $tred || ' SHALL exist in the project to allow filtering on it.'
                            )
                    )
                    
                )
                else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || '''. Input SHALL have exactly one filters under value.'
                )
            )
            default return (
                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Path value not supported'
            )
     let $check                 :=
        if (empty($check)) then () else (
            error($errors:BAD_REQUEST,  string-join($check, ' '))
        )
    
    let $updatePreparation      := comp:updateCompilationFilterSet($decor, $storedFilters)
    let $filtersFile            := comp:getCompilationFilterFile($decor)
    let $update                 :=
        for $param in $data/parameter
        return
            switch ($param/@path)
            case '/filter' return (
                let $attname  := substring-after($param/@path, '/')
                let $new      := 
                    attribute {$attname} {
                        if ($param/@value = ('true', 'on')) then 'on' else 'off'
                    }
                let $stored   := $filtersFile/@*[name() = $attname]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $filtersFile
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/filterId' return (
                let $attname  := substring-after($param/@path, '/')
                let $new      := attribute {$attname} {$param/@value}
                let $stored   := $filtersFile/@*[name() = $attname]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $filtersFile
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/filters' return (
                let $stored   := $filtersFile/filters[@id = $param/value/filters/@id]
                let $new      :=
                    if ($param/@op = 'remove') then () else (
                        <filters>
                        {
                            $param/value/filters/@id,
                            $param/value/filters/@label,
                            if ($stored/@created) then $stored/@created else (
                                attribute created {current-dateTime()}
                            ),
                            attribute modified {current-dateTime()}
                        }
                        {
                            for $transaction in $param/value/filters/transaction
                            let $trid   := $transaction/@ref
                            let $tred   := $transaction/@flexibility
                            group by $trid, $tred
                            return (
                                let $tr := utillib:getTransaction($trid, $tred)
                                return
                                    <transaction>{$trid, $tred, $tr/@statusCode, attribute name {($tr/name)[1]}}</transaction>
                            )
                                
                        }
                        </filters>
                    )
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $filtersFile
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            default return ( (: unknown path :) )
    
    return
        comp:getCompilationFilterSet($decor)
    
};

(: ======================= FILTERS END ======================== :)

(: ================ EXTRA CHECK PARAMETERS START  ================ :)

(:~ Get project extra checks. This includes checks from decor/core/DECOR-optional-checks.sch. This may also include any check stored 
at project level. This feature is not documented in any way, is discouraged for widespread use and can only be added by someone with 
direct database access. Checks added this way are marked with readonly=true and do not have an id :)
declare function prapi:getProjectExtraCheckParameters($request as map(*)) {

    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    
    let $decor                  := if (utillib:isOid($project)) then utillib:getDecorById($project) else utillib:getDecorByPrefix($project)
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if (empty($decor)) then
            error($errors:BAD_REQUEST, 'Parameter project ' || $project || ' does not lead to a DECOR project.')
        else
        if (count($decor) = 1) then () else (
            error($errors:SERVER_ERROR, 'Parameter project ' || $project || ' leads to multiple (' || count($decor) || ') DECOR projects. Contact your system administrator.')
        )
    
    let $projectChecks          := collection(setlib:strProjectDevelopment($projectPrefix))//@optionalChecks[1]
    let $optionalCheckFile      := concat($setlib:strDecorCore, '/DECOR-optional-checks.sch')
    let $optionalChecks         := if (doc-available($optionalCheckFile)) then doc($optionalCheckFile) else ()
    let $projectSchematronFile  := concat(setlib:strProjectDevelopment($projectPrefix), $projectPrefix, 'checks.sch')
    let $projectSchematron      := if (doc-available($projectSchematronFile)) then doc($projectSchematronFile) else ()
    let $checks                 := tokenize($projectChecks, ' ')
    let $result                 :=
            <checks>
            {
                for $pattern in $optionalChecks//sch:pattern
                return
                    <pattern on="{$pattern/@id = $checks}">
                    {
                        $pattern/@id
                    }
                        <title>{$pattern/sch:title/text()}</title>
                    </pattern>
                ,
                for $pattern in $projectSchematron//sch:pattern
                return
                    <pattern on="true" readonly="true">
                    {
                        $pattern/@id
                    }
                        <title>{$pattern/sch:title/text()}</title>
                    </pattern>
            }
            </checks>
    
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

(:~ Update project extra checks. This includes checks from decor/core/DECOR-optional-checks.sch. This may also include any check stored 
at project level. This feature is not documented in any way, is discouraged for widespread use and can only be added by someone with 
direct database access. Checks added this way are marked with readonly=true and do not have an id :)
declare function prapi:putProjectExtraCheckParameters($request as map(*)) {

    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    let $data                   := utillib:getBodyAsXml($request?body, 'checks', ())
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    let $check                  :=
        if ($data) then () else (
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        )
    
    let $decor                  := if (utillib:isOid($project)) then utillib:getDecorById($project) else utillib:getDecorByPrefix($project)
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if (empty($decor)) then
            error($errors:BAD_REQUEST, 'Parameter project ' || $project || ' does not lead to a DECOR project.')
        else
        if (count($decor) = 1) then () else (
            error($errors:SERVER_ERROR, 'Parameter project ' || $project || ' leads to multiple (' || count($decor) || ') DECOR projects. Contact your system administrator.')
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-RULES)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify project extra parameters in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    
    let $optionalCheckFile      := concat($setlib:strDecorCore, '/DECOR-optional-checks.sch')
    let $optionalChecks         := if (doc-available($optionalCheckFile)) then doc($optionalCheckFile) else ()    
    let $editedChecks           := 
        for $pattern in $optionalChecks//sch:pattern[@id = $data/pattern[@on = 'true']/@id]
        return
            $pattern/@id
    let $checkFile              := concat($projectPrefix, 'check-config.xml')
    
    let $projectDevelopmentColl := setlib:strProjectDevelopment($projectPrefix)
    let $make                   := xmldb:create-collection($setlib:strDecorVersion, replace($projectDevelopmentColl, $setlib:strDecorVersion, ''))
    let $save                   := xmldb:store($make, $checkFile, <config optionalChecks="{$editedChecks}"/>)
    let $save                   := sm:chmod(xs:anyURI($save), 'rw-rw-r--')
    
    return
        prapi:getProjectExtraCheckParameters($request)
};

(: ================ EXTRA CHECK PARAMETERS END  ================ :)

(: ====================  PUBLICATION START  ===================== :)

(:~ Retrieves DECOR project releases or versions (publications) based on $projectPrefixOrId

    @param $project required path part which may be its prefix or its oid
    @param $language optional string parameter to return certain information with. Defaults for project default language
    @return project as JSON object
    @since 2020-05-03
:)
declare function prapi:getProjectPublicationList($request as map(*)) {

    let $project                := $request?parameters?project[string-length() gt 0]
    let $effectiveDate          := $request?parameters?effectiveDate[string-length() gt 0]
    
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    
    let $results                := prapi:getProjectPublicationList($project, $effectiveDate)

    return
        $results

};

(:~ Create DECOR project release/version object

    @param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
    @param $project                  - required. path part which may be its prefix or its oid
    @param $request-body             - required. json body containing the updated release/version
    @return project structure with the persisted release/version
    @since 2020-05-03
:)
declare function prapi:postProjectPublication($request as map(*)) {

    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    let $check                  :=
        if (empty($request?body)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $data                   := utillib:getBodyAsXml($request?body, 'decor-parameters', ()) 
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
       
    let $decor                  := if (utillib:isOid($project)) then utillib:getDecorById($project) else utillib:getDecorByPrefix($project)
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $project, ''', does not exist.'))
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-PROJECT)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to create a version of project ', $project, '. You have to be an active author in the project.'))
        )
    
    let $forceRecompile         := true()
    let $testFilters            := false()
    let $runtimeOnly            := false()
    let $compile-language       := ($data/compile-language | $data/version/compile-language)[. = $decor/project/name/@language]
    let $compile-language       := if (empty($compile-language)) then $decor/project/@defaultLanguage else $compile-language
    
    let $by                     := $authmap?name
    let $release                := $data/version/@release = 'true'
    let $development            := $data/version/@development = 'true'
    let $publication-request    := if ($development) then false() else ($data/version/@publication-request = 'true')
    (: AH: deactivated until we full understand how to publish - creation logic moved to implementationguide-api.xqm :)
    let $implementationguide    := (:$data/version/@implementationguide = 'true':) false()
    let $noteOrDesc             := $data/version/desc
    
    let $timeStamp              := if ($development) then 'development' else substring(string(current-dateTime()), 1, 19)
    let $now                    := substring(string(current-dateTime()), 1, 19)
    let $projectAuthor          := $decor/project/author[@username = $by]
        
    (: store the version itself :)
    let $relvername             := if ($data/version[@release = 'true'] or $implementationguide) then 'release' else 'version'
    let $versionOrRelease       := 
        element {$relvername}
        {
            if ($implementationguide) then attribute implementationguide { 'true' } else (),
            attribute date {$now},
            attribute by {normalize-space(data(($projectAuthor, $authmap?name)[1]))},
            $data/version/@statusCode[. = map:keys($utillib:releasestatusmap)], 
            if ($relvername = 'release') then $data/version/@versionLabel else (),
            for $desc in $data/version/desc | $data/version/note
            let $elmname  := if ($relvername = 'release') then 'note' else 'desc'
            return 
                element {$elmname} {
                    attribute language {($desc/@language[not(. = '')] , $decor/project/@defaultLanguage)[1]},
                    attribute lastTranslated {substring(string(current-dateTime()), 1, 19)},
                    utillib:parseNode($desc)/node()
                }
        }
        
    let $updateProject           := 
        if ($development) then () else
        if ($decor/project[release | version]) 
        then update insert $versionOrRelease preceding $decor/project/(release | version)[1]
        else update insert $versionOrRelease into $decor/project
    
    (: create a version = archive only into history :)
    let $writeHistory            :=
        if ($development or $release or $implementationguide) then () else (
            hist:AddHistory ($authmap?name, 'DECOR', $projectPrefix, 'version', $decor)
        )
    
    let $createRequest           :=
        if (not($release or $implementationguide)) then () else (
            let $decor-params           := collection(util:collection-name($decor))/decor-parameters[util:document-name(.) = 'decor-parameters.xml']
            let $decor-parameters       := 
                if (empty($decor-params)) then
                    <decor-parameters>
                        <switchCreateSchematron1/>
                        <switchCreateSchematronWithWrapperIncludes0/>
                        <switchCreateSchematronWithWarningsOnOpen0/>
                        <switchCreateSchematronClosed0/>
                        <switchCreateSchematronWithExplicitIncludes0/>
                        <switchCreateDocHTML1/>
                        <switchCreateDocSVG1/>
                        <switchCreateDocDocbook0/>
                        <switchCreateDocPDF0/>
                        <useLocalAssets1/>
                        <useLocalLogos1/>
                        <useCustomLogo0/>
                        <useLatestDecorVersion1/>
                        <hideColumns>45gh</hideColumns>
                        <inDevelopment0/>
                        <switchCreateDatatypeChecks1/>
                        <createDefaultInstancesForRepresentingTemplates0/>
                        <!--<artdecordeeplinkprefix></artdecordeeplinkprefix>-->
                        <!--<useCustomRetrieve1 hidecolumns=""/>-->
                        <logLevel>INFO</logLevel>
                        <switchCreateTreeTableHtml1/>
                    </decor-parameters>
                else (
                    <decor-parameters> 
                    {
                        $decor-params/(* except (inDevelopment0|inDevelopment1|useLatestDecorVersion0|useLatestDecorVersion1)),
                        <inDevelopment0/>,
                        <useLatestDecorVersion1/>
                    }
                    </decor-parameters>
                )
            let $implementationguide-parameters := 
                if ($implementationguide) then
                    <implementationguide-parameters>
                    {
                        <fhir1/>,
                        <cda1/>
                    }
                    </implementationguide-parameters>
                else ()
            let $filters                := comp:getCompilationFilters($decor, (), ())
            let $filters                := comp:getFinalCompilationFilters($decor, $filters)
            
            let $stamp                  := util:uuid()
            let $publication-request    :=  
                <publication-request uuid="{$stamp}" for="{$projectPrefix}" on="{$versionOrRelease/@date}" as="{$stamp}" by="{$versionOrRelease/@by}" 
                                     language="{$compile-language}" testfilters="{$testFilters}" runtimeonly="{$runtimeOnly}" 
                                     forcecompile="{$forceRecompile}" development="{$development}" publication-request="{$publication-request}"
                                     implementationguide="{$implementationguide}" progress="Added to process queue ..." progress-percentage="0">
                {
                    if ($filters) then 
                        attribute filterlabel {($filters/@label, $filters/@id)[1]} |
                        attribute transactions {$filters/transaction/concat(@ref, '--', @flexibility)}
                    else (),
                    $versionOrRelease, 
                    $filters,
                    if ($implementationguide) then $implementationguide-parameters else $decor-parameters
                }
                </publication-request>
            let $write                  := xmldb:store($setlib:strDecorScheduledTasks, $projectPrefix || replace($now, '\D', '') || '.xml', $publication-request)
            let $tt                     := sm:chmod($write, 'rw-rw----')
            
            (:
                1. Compile project
                2. Publish or submit to ADRAM
            :)
            return ()
        )
     
    return
        prapi:getProjectPublicationList($projectPrefix, ())
};

(:~ Update project release/version. Expect array of parameter objects, each containing RFC 6902 compliant contents.

{ "op": "[add|remove|replace]", "path": "[/]", "value": "[string|object]" }

@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $request-body             - required. json body containing array of parameter objects each containing RFC 6902 compliant contents
@return release/versions after patch
@since 2021-10-20
@see http://tools.ietf.org/html/rfc6902
:)
declare function prapi:patchProjectPublication($request as map(*)) {

    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    let $check                  :=
        if (empty($request?body)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $data                   := utillib:getBodyAsXml($request?body, 'parameters', ())
    let $return                 := prapi:patchProjectPublication($authmap, string($project), $data)
    return (
        prapi:getProjectPublicationList($project, ())
    )

};

(:~ Delete DECOR project release/version label if it exists. If it does not: no error is returned.

    @param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
    @param $effectiveDate            - required. the date for the release/version to update 
    @param $project                  - required. the project id or prefix to delete from
    @return nothing
    @since 2020-05-03
:)
declare function prapi:deleteProjectPublication($request as map(*)) {

    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else if ($authmap?groups = 'dba') then ()
        else (
            error($errors:FORBIDDEN, 'You need to be dba for this feature')
        )
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    
    let $effectiveDate          := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $return                 := prapi:deleteProjectPublication($authmap, $project, $effectiveDate)
    return (
        roaster:response(204, $return)
    )

};

(:~ Retrieve a list of versions/releases in a project :)
declare function prapi:getProjectPublicationList($projectPrefixOrId as item(), $effectiveDate as xs:string?) {
    let $decor                := 
        if (utillib:isOid($projectPrefixOrId)) then utillib:getDecorById($projectPrefixOrId) else utillib:getDecorByPrefix($projectPrefixOrId)
    
    let $check                  :=
        if (empty($decor)) then
            error($errors:BAD_REQUEST, 'Parameter project ' || $projectPrefixOrId || ' does not lead to a DECOR project.')
        else
        if (count($decor) = 1) then () else (
            error($errors:SERVER_ERROR, 'Parameter project ' || $projectPrefixOrId || ' leads to multiple (' || count($decor) || ') DECOR projects. Contact your system administrator.')
        )
    
    let $prefix             := $decor/project/@prefix
    let $pprefix            := replace($prefix,'-$','')

    let $results              := 
        for $r in $decor/project/release | $decor/project/version
        let $pub-request    := if ($r[self::release]) then collection($setlib:strDecorScheduledTasks)/publication-request[@on = $r/@date] else ()
        (: find out whether there is already a releases / version directory with a process status in publication-completed.xml :)
        let $dsuffix        := replace($r/@date, '[:\-]', '')
        let $strVersion     := concat($setlib:strDecorVersion, '/', $pprefix, '/version-', $dsuffix)
        let $strComplete    := concat($prefix, $dsuffix, '-publication-completed.xml')
        let $strProcess     := concat($strVersion,'/',$strComplete)
        let $withRequest    := count(collection($strVersion)//publication/request[@status='OK'])
        let $adramprocess   := doc($strProcess)/publication
        order by $r/@date descending
        return
            <version release="{name($r) = 'release'}">
            {
                $r/(@* except (@release | @busy | @progress | @progress-percentage)),
                $pub-request[1]/(@busy | @progress | @progress-percentage),
                attribute {'publicationrequests'} { $withRequest },
                $adramprocess,
                for $n in $r/node()
                return
                    if (name($n) = 'note') then element desc {$n/@*, $n/node()} else $n
            }
            </version>
    
    let $allcnt               := count($results)
    let $total                := count($results)
    
    let $results              := if (empty($effectiveDate)) then $results else $results[@date = $effectiveDate]
    let $current              := count($results)
    
    return
        <list artifact="VERSIONS" current="{$current}" total="{$total}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements($results)
        }
        </list>
};

(:~ Central logic for updating an existing project version|release

@param $authmap           - required. Map derived from token
@param $projectPrefixOrId - required. DECOR project/@id | project/@prefix to update in
@param $data              - required. DECOR xml element parameter elements each containing RFC 6902 compliant contents
@return project object as xml with json:array set on elements
:)
declare function prapi:patchProjectPublication($authmap as map(*), $projectPrefixOrId as xs:string, $request-body as element(parameters)) {
    let $decor                  := 
        if (count($projectPrefixOrId[not(. = '')]) = 1) then
            if (utillib:isOid($projectPrefixOrId)) then utillib:getDecorById($projectPrefixOrId) else utillib:getDecorByPrefix($projectPrefixOrId)
        else (
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        )
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $projectPrefixOrId, ''', does not exist.'))
        )
    let $projectPrefix          := $decor/project/@prefix
    let $projectAuthor          := $decor/project/author[@username = $authmap?name]
    let $now                    := substring(string(current-dateTime()), 1, 19)
    
    (: Start checking :)
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-PROJECT)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify the project ', $projectPrefix, '. You have to be an active admin in the project.'))
        )
    let $check                  :=
        if ($request-body[parameter]) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall be an array of ''parameter''.')
        )
    let $unsupportedops         := $request-body/parameter[not(@op = ('replace', 'remove'))]
    let $check                  :=
        if ($unsupportedops) then
            error($errors:BAD_REQUEST, 'Submitted parameters shall have supported ''op'' value. Found ''' || string-join(distinct-values($unsupportedops/@op), ''', ''') || ''', expected ''replace'', or ''remove''') 
        else ()
    
    let $check                  :=
        for $param in $request-body/parameter
        let $op               := $param/@op
        let $relvername       := if ($param/value/version[@release = 'true'] | $param/value/release) then 'release' else 'version'
        let $storedVersion    := $decor/project/version[@date = $param/value/version[not(@release = 'true')]/@date] | $decor/project/release[@date = ($param/value/release/@date, $param/value/version[@release = 'true']/@date)]
        return
            if ($param[@value]) then
                'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Parameter SHALL be an object, not a string.'
            else
            if ($param[count(value) gt 1]) then
                'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Multiple value nodes are not supported. Please separate each into its own parameter.'
            else
            if ($param/value[count(*) gt 1]) then
                'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Multiple nodes in value are not supported. Please separate each into its own parameter.'
            else
            if ($param/value[*[not(name() = ('version', 'release'))]]) then
                'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Parameter value SHALL contain either a version or release object.'
            else
            if ($param[@path = '/']) then
                if ($op = 'remove') then
                   if ($relvername = 'release') then () else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported for releases. Use DELETE /project/{' || $projectPrefixOrId || '}/$publication'
                    )
                else
                if ($op = 'add') then () else (
                    if ($storedVersion) then (
                        if ($relvername = $storedVersion/name()) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. You SHALL NOT replace a ' || name($storedVersion[1]) || ' with a ' || $relvername 
                        ),
                        if (utillib:isStatusChangeAllowable($storedVersion, string($param/value/*[1]/@statusCode))) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Release/version statusCode SHALL NOT be updated from ''' || $storedVersion/@statusCode || ''' to ''' || $param/value/*[1]/@statusCode || '''. Allowed: ' || string-join(map:get($utillib:releasestatusmap, string($storedVersion/@statusCode)), ', ')
                        )
                    )
                    else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. ' || $relvername || ' with date=' || string-join($param/value/*/@date, ' ') || ' SHALL exist in the project.' 
                    ),
                    if ($param/value/version[not(@release = 'true')]/@versionLabel) then 
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A ' || $relvername || ' SHALL NOT have a versionLabel' 
                    else (),
                    if ($param/value/release) then
                        if ($param/value/release/note) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A release SHALL have a .note'
                        )
                    else 
                    if ($param/value/version) then
                        if ($param/value/version/desc) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A version SHALL have a .desc'
                        )
                    else ()
                )
            else (
                'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported.'
            )
     let $check                 :=
        if (empty($check)) then () else (
            error($errors:BAD_REQUEST,  string-join ($check, ' '))
        )
    (: End checking :)
    
    (: Start applying :)
    let $update                 :=
        for $param in $request-body/parameter
        return
            switch ($param/@path)
            case '/' return (
                let $stored         := $decor/project/version[@date = $param/value/version[not(@release = 'true')]/@date] | $decor/project/release[@date = ($param/value/release/@date, $param/value/version[@release = 'true']/@date)]
                let $projectAuthor  := ($param/value/*/@by, $stored/@by, $decor/project/author[@username = $authmap?name])[1]
                let $new            := 
                    for $relver in $param/value/*
                    let $relvername   := if ($relver[@release = 'true'] or name($relver) = 'release') then 'release' else 'version'
                    return
                    element {$relvername}
                    {
                        attribute date {$relver/@date},
                        attribute by {normalize-space(data(($projectAuthor, $authmap?name)[1]))},
                        $relver/@statusCode, 
                        if ($relvername = 'release') then $relver/@versionLabel else (),
                        for $desc in $relver/desc | $relver/note
                        let $elmname  := if ($relvername = 'release') then 'note' else 'desc'
                        return 
                            element {$elmname} {
                                attribute language {($desc/@language[not(. = '')] , $decor/project/@defaultLanguage)[1]},
                                attribute lastTranslated {substring(string(current-dateTime()), 1, 19)},
                                utillib:parseNode($desc)/node()
                            }
                    }
                
                return
                switch ($param/@op)
                case 'replace' return update replace $stored with $new
                case 'remove' return update delete $stored
                default return ( (: unknown op :) )
            )
            default return ( (: unknown path :) )
    (: Done :)
    
    return
        ()
};

(:~ Central logic for deleting a propject release/version. If the release/version does not exist, then no error is given.

@param $authmap                 - required. Map derived from token
@param $projectPrefix           - required. DECOR project/@prefix to get issues from
@param $effectiveDate           - required. the effectiveDate for the issue label to delete
@return nothing
:)
declare function prapi:deleteProjectPublication($authmap as map(*), $projectPrefixOrId as xs:string, $effectiveDate as xs:string) {
    let $check                  :=
        if ($authmap?groups = 'dba') then () else (
            error($errors:FORBIDDEN, 'You need to be dba for this feature')
        )
    
    let $decor                  := 
        if (count($projectPrefix[not(. = '')]) = 1) then
            if (utillib:isOid($projectPrefixOrId)) then utillib:getDecorById($projectPrefixOrId) else utillib:getDecorByPrefix($projectPrefixOrId)
        else (
            error($errors:BAD_REQUEST, 'You are missing required parameter prefix')
        )
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $projectPrefixOrId, ''', does not exist.'))
        )
    
    let $projectPrefix          := $decor/project/@prefix
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-PROJECT)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify project ', $projectPrefix, '. You have to be an active admin in the project.'))
        )
    
    let $releaseVersion         := $decor/project/release[@date = $effectiveDate] | $decor/project/version[@date = $effectiveDate]
    let $update                 := update delete $releaseVersion
    
    let $releaseVersion         := if ($effectiveDate castable as xs:dateTime) then utillib:getDecorByPrefix($projectPrefix, $effectiveDate) else ()
    let $update                 := if ($releaseVersion) then xmldb:remove(util:collection-name($releaseVersion[1])) else ()
    
    return
        ()
};

(: ====================  PUBLICATION END  ===================== :)

(: ===============  PUBLICATION PARAMETERS START  =============== :)

(:~ Retrieve the decor-parameters for a project if not present a default set is returned 
@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $project                  - required. path part which may be its prefix or its oid
:)
declare function prapi:getProjectPublicationParameters($request as map(*)) {
    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    
    let $decor                  := if (utillib:isOid($project)) then utillib:getDecorById($project) else utillib:getDecorByPrefix($project)
    
    let $check                  :=
        if (empty($decor)) then
            error($errors:BAD_REQUEST, 'Parameter project ' || $project || ' does not lead to a DECOR project.')
        else
        if (count($decor) = 1) then () else (
            error($errors:SERVER_ERROR, 'Parameter project ' || $project || ' leads to multiple (' || count($decor) || ') DECOR projects. Contact your system administrator.')
        )
    
    let $parent                 := util:collection-name($decor)
    let $parameters             := collection(util:collection-name($decor))/decor-parameters[util:document-name(.) = 'decor-parameters.xml']
    
    let $result                 :=
        (:<decor-parameters xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="https://assets.art-decor.org/ADAR/rv/decor-parameters.xsd">:)
        <publication-parameters>
        {
            (: default is false :)
            attribute switchCreateSchematron {exists($parameters/switchCreateSchematron1)}
            ,
            (: default is false :)
            attribute switchCreateSchematronWithWrapperIncludes {exists($parameters/switchCreateSchematronWithWrapperIncludes1)}
            ,
            (: default is false :)
            attribute switchCreateSchematronWithWarningsOnOpen {exists($parameters/switchCreateSchematronWithWarningsOnOpen1)}
            ,
            (: default is false :)
            attribute switchCreateSchematronClosed {exists($parameters/switchCreateSchematronClosed1)}
            ,
            (: default is false :)
            attribute switchCreateSchematronWithExplicitIncludes {exists($parameters/switchCreateSchematronWithExplicitIncludes1)}
            ,
            (: default is false :)
            attribute switchCreateDocHTML {exists($parameters/switchCreateDocHTML1)}
            ,
            (: default is true :)
            attribute switchCreateDocSVG {empty($parameters/switchCreateDocSVG0)}
            ,
            (: default is false :)
            attribute switchCreateDocDocbook {exists($parameters/switchCreateDocDocbook1)}
            ,
            (: default is true :)
            attribute useLocalAssets {empty($parameters/useLocalAssets0)}
            ,
            (: default is true :)
            attribute useLocalLogos {empty($parameters/useLocalLogos0)}
            ,
            (: default is false :)
            attribute useLatestDecorVersion {exists($parameters/useLatestDecorVersion1)}
            ,
            (: default is false :)
            attribute inDevelopment {exists($parameters/inDevelopment1)}
            ,
            (: default is true :)
            attribute switchCreateDatatypeChecks {empty($parameters/switchCreateDatatypeChecks0)}
            ,
            (: default is true :)
            attribute createDefaultInstancesForRepresentingTemplates {empty($parameters/createDefaultInstancesForRepresentingTemplates0)}
            ,
            (: default is serverapi:getServerURLArt() :)
            attribute artdecordeeplinkprefix {
                if ( $parameters[artdecordeeplinkprefix castable as xs:anyURI] )
                then $parameters/artdecordeeplinkprefix
                else serverapi:getServerURLArt()
            }
            ,
            (: default is 'freeze' :)
            attribute bindingBehavior {
                if ( $parameters[bindingBehavior/@valueSets] )
                then $parameters/bindingBehavior/@valueSets
                else 'freeze'
            },
            (: default is true :)
            attribute switchCreateTreeTableHtml {empty($parameters/switchCreateTreeTableHtml0)}
            ,
            (: default is project/@defaultLanguage :)
            attribute defaultLanguage {
                if ( $parameters[defaultLanguage = $decor/project/name/@language] )
                then $parameters/defaultLanguage
                else data($decor/project/@defaultLanguage)
            },
            (: special handling for switchCreateDocPDF1: turn all @include switches (chars) into element content :)
            if ($parameters[switchCreateDocPDF1]) then 
                if ($parameters/switchCreateDocPDF1/@include[not(. = '')]) then
                    for $char in distinct-values($parameters/switchCreateDocPDF1/tokenize(normalize-space(replace(@include,'(.)','$1 ')), '\s'))
                    return
                        <switchCreateDocPDF json:array="true">{$char}</switchCreateDocPDF>
                else (
                    <switchCreateDocPDF json:array="true"/>
                )
            else ()
            ,
            if ( $parameters[useCustomLogo1/@*] ) then
                <useCustomLogo>{$parameters/useCustomLogo1[1]/@*[not(. = '')]}</useCustomLogo>
            else ()
            ,
            (: special handling for switchCreateDocPDF1: turn all @include switches (chars) into element content :)
            if ($parameters[useCustomRetrieve1]) then
                for $char in distinct-values($parameters/useCustomRetrieve1/tokenize(normalize-space(replace(@hidecolumns,'(.)','$1 ')), '\s'))
                return
                    <useCustomRetrieve json:array="true">{$char}</useCustomRetrieve>
            else ()
        }
        </publication-parameters>
    
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

(:~ Update project publication parameters. Expect array of parameter objects, each containing RFC 6902 compliant contents.

{ "op": "[add|remove|replace]", "path": "[/]", "value": "[string|object]" }

@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $project                  - required. path part which may be its prefix or its oid
@param $request-body             - required. json body containing array of parameter objects each containing RFC 6902 compliant contents
@return publication parameters after patch
@since 2021-10-20
@see http://tools.ietf.org/html/rfc6902
:)
declare function prapi:patchProjectPublicationParameters($request as map(*)) {
    
    let $authmap                        := $request?user
    let $project                        := $request?parameters?project[string-length() gt 0]
    let $data                           := utillib:getBodyAsXml($request?body, 'parameters', ())
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    let $check                  :=
        if ($data) then () else (
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        )
    
    let $return                 := prapi:patchProjectPublicationParameters($authmap, $project, $data)
    return
        prapi:getProjectPublicationParameters($request)
};

(:~ Central logic for updating project publication parameters

@param $authmap           - required. Map derived from token
@param $projectPrefixOrId - required. DECOR project id or prefix
@param $data              - required. DECOR xml element parameter elements each containing RFC 6902 compliant contents
@return filters object as xml with json:array set on elements
:)
declare function prapi:patchProjectPublicationParameters($authmap as map(*), $projectPrefixOrId as xs:string, $data as element(parameters)) {
    let $decor                  := 
        if (utillib:isOid($projectPrefixOrId)) then utillib:getDecorById($projectPrefixOrId) else utillib:getDecorByPrefix($projectPrefixOrId)
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $projectPrefixOrId, ''', does not exist.'))
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-PROJECT)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify publication parameters of project ', $projectPrefix, '. You have to be an active decor-admin author in the project.'))
        )
    let $check                  :=
        if ($data[parameter]) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall be an array of ''parameter''.')
        )
    let $unsupportedops         := $data/parameter[not(@op = ('add', 'replace', 'remove'))]
    let $check                  :=
        if ($unsupportedops) then
            error($errors:BAD_REQUEST, 'Submitted parameters shall have supported ''op'' value. Found ''' || string-join(distinct-values($unsupportedops/@op), ''', ''') || ''', expected ''add'', ''replace'', or ''remove''') 
        else ()
    
    let $supportedLogLevels     := ("INFO", "ALL", "DEBUG", "WARN", "ERROR", "FATAL", "OFF")
    let $supportedPdfSections   := ('d', 's', 'n', 't', 'r', 'i')
    let $check                  :=
        for $param in $data/parameter
        let $op         := $param/@op
        let $path       := $param/@path
        let $value      := $param/@value
        let $pathpart   := substring-after($path, '/')
        return
            switch ($path)
            case '/switchCreateSchematron' 
            case '/switchCreateSchematronWithWrapperIncludes'
            case '/switchCreateSchematronWithWarningsOnOpen'
            case '/switchCreateSchematronClosed'
            case '/switchCreateSchematronWithExplicitIncludes'
            case '/switchCreateDocHTML'
            case '/switchCreateDocSVG'
            case '/switchCreateDocDocbook'
            case '/useLocalAssets'
            case '/useLocalLogos'
            case '/useLatestDecorVersion'
            case '/inDevelopment'
            case '/switchCreateDatatypeChecks'
            case '/createDefaultInstancesForRepresentingTemplates'
            case '/switchCreateTreeTableHtml' return (
                if ($value = ('true', 'false')) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || '''. value SHALL be true or false.'
                )
            )
            case '/defaultLanguage' return (
                if ($value = $decor/project/name/@language) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || '''. value SHALL be ' || string-join($decor/project/name/@language, ', or ')
                )
            )
            case '/logLevel' return (
                if ($value = $supportedLogLevels) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || '''. value SHALL be one of ' || string-join($supportedLogLevels, ', ')
                )
            )
            case '/artdecordeeplinkprefix' return (
                if ($value castable as xs:anyURI) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || '''. value SHALL be castable to xs:anyURI. Found ' || $value
                )
            )
            case '/bindingBehavior' return (
                if ($value = ('freeze', 'preserve')) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || '''. value SHALL be freeze or preserve.'
                )
            )
            case '/useCustomLogo' return (
                if (count($param/value/useCustomLogo[@src]) = 1) then (
                    if ($param/value/useCustomLogo/@src castable as xs:anyURI) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || '''. useCustomLogo.src SHALL be castable to xs:anyURI. Found ' || $param/value/useCustomLogo/@src
                    ),
                    if (tokenize($param/value/useCustomLogo/@src, '\.')[last()] = $prapi:acceptableMediaTypeMap?*) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || '''. useCustomLogo.src SHALL have a web compatible extension. Found "' || tokenize($param/value/useCustomLogo/@src, '\.')[last()] || '". Supported are ' || string-join($prapi:acceptableMediaTypeMap?*, ', ')
                    ),
                    if (empty($param/value/useCustomLogo/@href)) then () else if ($param/value/useCustomLogo/@href castable as xs:anyURI) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || '''. useCustomLogo.href SHALL be castable to xs:anyURI if present. Found ' || $param/value/useCustomLogo/@href
                    )
                )
                else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || '''. value SHALL contain exactly one useCustomLogo with .src.'
                )
            )
            case '/switchCreateDocPDF' return (
                for $pdfSection in $param/value/switchCreateDocPDF
                return 
                    if ($pdfSection = $supportedPdfSections) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || '''. value "' || $pdfSection || '" SHALL be one of ' || string-join($supportedPdfSections, ', ')
                    )
            )
            case '/useCustomRetrieve' return (
                for $customParam in $param/value/useCustomRetrieve
                return 
                    if ($param/value/useCustomRetrieve[string-length() = 1]) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || '''. value SHALL be exactly 1 character or digit. Found "' || $param/value/useCustomRetrieve || '"'
                    )
            )
            default return (
                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Path value not supported'
            )
     let $check                 :=
        if (empty($check)) then () else (
            error($errors:BAD_REQUEST,  string-join($check, ' '))
        )
    
    let $storedParameters       := collection(util:collection-name($decor))/decor-parameters[util:document-name(.) = 'decor-parameters.xml']
    let $storedParameters       := 
        if ($storedParameters) then $storedParameters else (
            let $ttt            := xmldb:store(util:collection-name($decor), 'decor-parameters.xml', <decor-parameters/>)
            let $tt             := sm:chmod($ttt, 'rwsrwsr--')
            
            return
                doc($ttt)/decor-parameters
        )
    
    let $update                 :=
        for $param in $data/parameter
        let $op         := $param/@op
        let $path       := $param/@path
        let $value      := $param/@value
        let $pathpart   := substring-after($path, '/')
        return
            switch ($path)
            case '/switchCreateSchematron'
            case '/switchCreateSchematronWithWrapperIncludes'
            case '/switchCreateSchematronWithWarningsOnOpen'
            case '/switchCreateSchematronClosed'
            case '/switchCreateSchematronWithExplicitIncludes'
            case '/switchCreateDocHTML'
            case '/switchCreateDocSVG'
            case '/switchCreateDocDocbook'
            case '/useLocalAssets'
            case '/useLocalLogos'
            case '/useLatestDecorVersion'
            case '/inDevelopment'
            case '/switchCreateDatatypeChecks'
            case '/createDefaultInstancesForRepresentingTemplates'
            case '/switchCreateTreeTableHtml' return (
                let $new      := element {concat($pathpart, if ($value = ('true', 'on')) then 1 else 0)} {}
                let $stored   := $storedParameters/*[name() = ($pathpart || 1, $pathpart || 0)]
                
                return
                    if ($stored) then update replace $stored with $new else update insert $new into $storedParameters
            )
            case '/defaultLanguage'
            case '/logLevel'
            case '/artdecordeeplinkprefix' return (
                let $new      := element {$pathpart} {data($value)}
                let $stored   := $storedParameters/*[name() = $pathpart]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedParameters
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/bindingBehavior' return (
                let $new      := element {$pathpart} {attribute valueSets {$value}}
                let $stored   := $storedParameters/*[name() = $pathpart]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedParameters
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/useCustomLogo' return (
                let $new      := 
                    if ($param/@op = 'remove') then <useCustomLogo0/> else (
                        element {concat($pathpart, 1)} {$param/value/useCustomLogo/(@src, @href)[not(. = '')]}
                    )
                let $stored   := $storedParameters/useCustomLogo0 | $storedParameters/useCustomLogo1
                
                return
                    if ($stored) then update replace $stored with $new else update insert $new into $storedParameters
            )
            case '/switchCreateDocPDF' return (
                let $new      :=
                    if ($param/@op = 'remove') then <switchCreateDocPDF0/> else 
                    if ($param/value/switchCreateDocPDF[string-length() = 0]) then <switchCreateDocPDF1/> else (
                        <switchCreateDocPDF1 include="{string-join(distinct-values($param/value/switchCreateDocPDF), '')}"/>
                    )
                let $stored   := $storedParameters/switchCreateDocPDF0 | $storedParameters/switchCreateDocPDF1
                
                return
                    if ($stored) then update replace $stored with $new else update insert $new into $storedParameters
            )
            case '/useCustomRetrieve' return (
                let $new      :=
                    if ($param/@op = 'remove') then () else (
                        <useCustomRetrieve1 hidecolumns="{string-join(distinct-values(($storedParameters/useCustomRetrieve/tokenize(normalize-space(replace(@hidecolumns,'(.)','$1 ')), '\s'), $param/value/useCustomRetrieve)), '')}"/>
                    )
                let $stored   := $storedParameters/useCustomRetrieve0 | $storedParameters/useCustomRetrieve1
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedParameters
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            default return ( (: unknown path :) )
    
    let $parameterDoc           := xmldb:store(util:collection-name($decor), 'decor-parameters.xml', prapi:handlePublicationParameters($storedParameters))
    
    return
        doc($parameterDoc)/decor-parameters
};

(:~ Restore order in the decor-parameters, in accordance with its schema :)
declare function prapi:handlePublicationParameters($storedParameters as element(decor-parameters)) as element(decor-parameters) {
    <decor-parameters xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="https://assets.art-decor.org/ADAR/rv/decor-parameters.xsd">
    {
        $storedParameters/switchCreateSchematron0 | $storedParameters/switchCreateSchematron1,
        $storedParameters/switchCreateSchematronWithWrapperIncludes0 | $storedParameters/switchCreateSchematronWithWrapperIncludes1,
        $storedParameters/switchCreateSchematronWithWarningsOnOpen0 | $storedParameters/switchCreateSchematronWithWarningsOnOpen1,
        $storedParameters/switchCreateSchematronClosed0 | $storedParameters/switchCreateSchematronClosed1,
        $storedParameters/switchCreateSchematronWithExplicitIncludes0 | $storedParameters/switchCreateSchematronWithExplicitIncludes1,
        $storedParameters/switchCreateDocHTML0 | $storedParameters/switchCreateDocHTML1,
        $storedParameters/switchCreateDocSVG0 | $storedParameters/switchCreateDocSVG1,
        $storedParameters/switchCreateDocDocbook0 | $storedParameters/switchCreateDocDocbook1,
        $storedParameters/switchCreateDocPDF0 | $storedParameters/switchCreateDocPDF1,
        $storedParameters/useLocalAssets0 | $storedParameters/useLocalAssets1,
        $storedParameters/useLocalLogos0 | $storedParameters/useLocalLogos1,
        $storedParameters/useCustomLogo0 | $storedParameters/useCustomLogo1,
        $storedParameters/useLatestDecorVersion0 | $storedParameters/useLatestDecorVersion1,
        $storedParameters/inDevelopment0 | $storedParameters/inDevelopment1,
        $storedParameters/defaultLanguage,
        $storedParameters/switchCreateDatatypeChecks0 | $storedParameters/switchCreateDatatypeChecks1,
        $storedParameters/createDefaultInstancesForRepresentingTemplates0 | $storedParameters/createDefaultInstancesForRepresentingTemplates1,
        $storedParameters/useCustomRetrieve1,
        $storedParameters/logLevel,
        $storedParameters/artdecordeeplinkprefix,
        $storedParameters/bindingBehavior,
        $storedParameters/switchCreateTreeTableHtml0 | $storedParameters/switchCreateTreeTableHtml1,
        $storedParameters/(* except (switchCreateSchematron0 | switchCreateSchematron1 | 
                                     switchCreateSchematronWithWrapperIncludes0 | switchCreateSchematronWithWrapperIncludes1 | 
                                     switchCreateSchematronWithWarningsOnOpen0 | switchCreateSchematronWithWarningsOnOpen1 | 
                                     switchCreateSchematronClosed0 | switchCreateSchematronClosed1 | 
                                     switchCreateSchematronWithExplicitIncludes0 | switchCreateSchematronWithExplicitIncludes1 | 
                                     switchCreateDocHTML0 | switchCreateDocHTML1 | 
                                     switchCreateDocSVG0 | switchCreateDocSVG1 | 
                                     switchCreateDocDocbook0 | switchCreateDocDocbook1 | 
                                     switchCreateDocPDF0 | switchCreateDocPDF1 | 
                                     useLocalAssets0 | useLocalAssets1 | 
                                     useLocalLogos0 | useLocalLogos1 | 
                                     useCustomLogo0 | useCustomLogo1 | 
                                     useLatestDecorVersion0 | useLatestDecorVersion1 | 
                                     inDevelopment0 | inDevelopment1 | 
                                     defaultLanguage | 
                                     switchCreateDatatypeChecks0 | switchCreateDatatypeChecks1 | 
                                     createDefaultInstancesForRepresentingTemplates0 | createDefaultInstancesForRepresentingTemplates1 | 
                                     useCustomRetrieve1 | 
                                     logLevel | 
                                     artdecordeeplinkprefix | 
                                     bindingBehavior | 
                                     switchCreateTreeTableHtml0 | switchCreateTreeTableHtml1)
                             )
    }
    </decor-parameters>
};

(: ===============  PUBLICATION PARAMETERS END  =============== :)

(: =================  PUBLICATION NOTES START  ================== :)

(:~ Retrieves decor-parameters object, suitable for creation of a new version/release. Included is a skeleton release note that 
    may be updated before submission. This skeleton is based on issues changed since a given date. If you do not specify a date, 
    the latest available version or release is assumed.
@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $project                  - required. path part which may be its prefix or its oid
@param $effectiveDate            - required. the date (for the release/version) to use as basis
@param decor-parameters object
:)
declare function prapi:getProjectPublicationNotes($request as map(*)) {

    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    let $effectiveDate          := $request?parameters?effectiveDate[string-length() gt 0]
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    
    let $decor                  := if (utillib:isOid($project)) then utillib:getDecorById($project) else utillib:getDecorByPrefix($project)
    let $projectPrefix          := $decor/project/@prefix
    let $language               := $decor/project/@defaultLanguage
    let $check                  :=
        if (empty($decor)) then
            error($errors:BAD_REQUEST, 'Parameter project ' || $project || ' does not lead to a DECOR project.')
        else
        if (count($decor) = 1) then () else (
            error($errors:SERVER_ERROR, 'Parameter project ' || $project || ' leads to multiple (' || count($decor) || ') DECOR projects. Contact your system administrator.')
        )
    
    let $decorSchemaTypes           := utillib:getDecorTypes()
    
    let $moduleNS                   := xs:anyURI('http://art-decor.org/ns/api/issue')
    let $modulePaths                := 'issue-api.xqm'
    
    let $module                     := load-xquery-module($moduleNS, map{'location-hints': $modulePaths})
    
    let $function1                  := prapi:function-lookup($module, QName($moduleNS, 'getIssueMeta'), 7)
    
    let $currentUser                := $authmap?name
    let $currentUserDisplayName     := try {userapi:getUserDisplayName($currentUser)} catch * {$currentUser}
    let $currentUserName            := if (string-length($currentUserDisplayName)=0) then $currentUser else $currentUserDisplayName
    
    let $compileLanguages           := string-join($decor/project/name/@language,' ')
    
    let $lastProjectDate            := utillib:parseAsDateTime($effectiveDate)
    let $lastProjectDate            := 
        if (empty($lastProjectDate)) then
            max(($decor/project/version/xs:dateTime(@date),$decor/project/release/xs:dateTime(@date)))
        else (
            $lastProjectDate
        )
    let $lastReleaseNote            := 
        ($decor/project/release[@date = $lastProjectDate], $decor/project/version[@date = $lastProjectDate])[1]
    
    let $includeSubscriptionInfo    := false()
    let $includeObjects             := false()
    let $includeEvents              := true()
    
    let $issueStatusMap             := 
        map:merge(
            for $k in map:keys($utillib:issuestatusmap)
            return
                map:entry($k, $decorSchemaTypes//IssueStatusCodeLifeCycle/*[@value=$k]/(label[@language=$language], label[@language='en-US'], label)[1])
        )
    
    let $result                     := 
        <decor-parameters development="false" compile="true" publication-request="false">
	    {
	         for $lang in distinct-values($decor/project/name/@language)
	         order by $lang
	         return <compile-language>{$lang}</compile-language>
	    
	    }
            <version release="false" date="{substring(string(current-dateTime()), 1, 19)}" statusCode="draft">
            {
                if (string-length($currentUserName) = 0) then () else attribute by {$currentUserName}
            }
                <desc language="{$language}">
                {
                    let $issues :=
                        for $issue in $decor/issues/issue
                        let $meta           := $function1($authmap, $issue, $includeSubscriptionInfo, $includeObjects, $includeEvents, $decor/project/author, $projectPrefix)
                        let $issueId        := $issue/@id
                        let $issueName      := $issue/@displayName
                        let $issueStatus    := (map:get($issueStatusMap, string($meta/@currentStatusCode)), $meta/@currentStatusCode)[1]
                        return
                            if (empty($lastProjectDate) or $meta/xs:dateTime(@lastDate) gt $lastProjectDate) then (
                                <li>
                                    <div class="node-issue-{data($meta/@currentStatusCode)}">
                                    {
                                        'Issue ' || tokenize($issueId,'\.')[last()] || ' - ' || data($issueStatus) || ' - ' || data($meta/@lastDate) ||' - "' || data($issueName) || '":'
                                    }
                                    </div>
                                    <div style="padding: 10px;">{$issue/tracking[@effectiveDate = max($issue/tracking/xs:dateTime(@effectiveDate))]/desc[1]/node()}</div>
                                </li>
                            ) else ()
                   
                    return
                        if ($issues) then (
                            if (empty($lastProjectDate)) then () else ( 
                                <div>{'Changed issues since', $lastProjectDate, if ($lastReleaseNote/@versionLabel) then '-' else (), data($lastReleaseNote/@versionLabel)}</div>
                            )
                            ,
                            <ul>{$issues}</ul>
                        ) else (
                            <div>{'There have been no changes to issues', if (empty($lastProjectDate)) then () else ('since', ($effectiveDate, $lastProjectDate)[1])}</div>
                        )
                }
                </desc>
            </version> 
        </decor-parameters>
    
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

(: =================  PUBLICATION NOTES END  ================== :)

(: =================   PROCESS REQUESTS START  ================== :)

(:~ Process compilation requests :)
declare function prapi:process-compile-request($threadId as xs:string, $request as element(compile-request)) {   
    let $logsignature           := 'prapi:process-compile-request'
    let $mark-busy              := update insert attribute busy {'true'} into $request
    let $progress               := 
        if ($request/@progress) then (
            update value $request/@progress with $prapi:PROGRESS-COMPILE-10 
        )
        else (
            update insert attribute progress {$prapi:PROGRESS-COMPILE-10} into $request
        )
    let $progress               := 
        if ($request/@progress-percentage) then (
            update value $request/@progress-percentage with round((100 div 9) * 1) 
        )
        else (
            update insert attribute progress-percentage {round((100 div 9) * 1)} into $request
        )
    let $timeStamp              := current-dateTime()
    let $projectPrefix          := $request/@for[not(. = '*')]
    let $decor                  := if (empty($projectPrefix)) then () else utillib:getDecorByPrefix($projectPrefix)
    let $language               := $request/@language[not(. = '')]
    let $publication-parameters := $request/parameters
    let $filters                := $request/filters
    let $testFilters            := $request/@testfilters = 'true'
    let $runtimeOnly            := $request/@runtimeonly = 'true'
    let $forceCompile           := $request/@forcecompile = 'true'
    let $doIGid                 := $request/@implementationguideId
    let $doIGed                 := $request/@implementationguideEffectiveDate
    
    (: implementation guide stuff :)
    let $implementationguide    := 
        if ($doIGid) then
            collection($setlib:strDecorDataIG)/implementationGuide[@id = $doIGid][@effectiveDate = $doIGed]
        else ()
    let $check                  :=
        if ($doIGid) then
            if ($implementationguide) then () else (
                error(xs:QName($logsignature), 'Could not find implementationguide ' || $implementationguide || ' collection.')
            )
        else ()
    (: / implementation guide stuff :)
    
    (: check if a compiled full result was saved earlier, or create it :)
    let $compiledDecor          := 
        if (count($decor) = 1) then (
            let $progress               := update value $request/@progress with $prapi:PROGRESS-COMPILE-20
            let $progress               := update value $request/@progress-percentage with round((100 div 9) * 2)
            
            let $t                      := 
                if ($forceCompile) then () else (
                    comp:getCompiledResult($decor, $timeStamp, $language, (), false(), false(), false())
                )
            
            let $progress               := update value $request/@progress with $prapi:PROGRESS-COMPILE-30
            let $progress               := update value $request/@progress-percentage with round((100 div 9) * 3)
            
            let $t                      :=
                if ($t[descendant-or-self::decor]) then ( $t ) else (
                    comp:getCompiledResult($decor, $timeStamp, $language, $filters, $testFilters, $runtimeOnly, $forceCompile)
                )
            
            (: implementation guide stuff :)
            (: https://art-decor.atlassian.net/browse/AD30-825 :)
            let $igupdate               :=
                if ($implementationguide) then (
                    let $compiledDecor          := $t/descendant-or-self::decor[1]
                    let $compiledLanguages      := $compiledDecor/project/name/@language
                    let $results                := 
                        for $object in (
                                          $compiledDecor/datasets/dataset        |
                                          $compiledDecor/scenarios/actors/actor  | 
                                          $compiledDecor/scenarios/scenario      | $compiledDecor/scenarios/scenario/transaction | $compiledDecor/scenarios/scenario/transaction/transaction |
                                          $compiledDecor/scenarios/questionnaire | $compiledDecor/scenarios/questionnaireresponse |
                                          $compiledDecor/terminology/valueSet    | $compiledDecor//datasets//valueSet | $compiledDecor/terminology/codeSystem | 
                                          $compiledDecor/rules/template          | $compiledDecor/rules/structuredefinition
                                       )[@id]
                        let $id   := $object/@id
                        let $ed   := $object/@effectiveDate
                        let $nm   := local-name($object)
                        let $tp   :=
                            switch ($nm)
                            case 'dataset'                return attribute artefact {'DS'}
                            case 'actor'                  return attribute artefact {'AC'}
                            case 'scenario'               return attribute artefact {'SC'}
                            case 'transaction'            return attribute artefact {'TR'}
                            case 'questionnaire'          return attribute artefact {'QQ'}
                            case 'questionnaireresponse'  return attribute artefact {'QR'}
                            case 'valueSet'               return attribute artefact {'VS'}
                            case 'codeSystem'             return attribute artefact {'CS'}
                            case 'template'               return attribute artefact {'TM'}
                            case 'structuredefinition'    return attribute artefact {'SD'}
                            default return (
                                let $message  := 'ERROR. Code error. Unknown resource artefact: ' || $nm || '. Compile id: ' || $request/@as
                                let $log      := utillib:log-scheduler-event('error', $threadId, $logsignature, $message)
                                return attribute artefact {'UNK'}
                            )
                        group by $id, $ed, $nm
                        order by $nm, $id, $ed
                        return
                            <resource>
                            {
                                attribute generated {true()},
                                $tp[1],
                                $object[1]/@id,
                                $object[1]/@displayName,
                                $object[1]/@effectiveDate,
                                $object[1]/@statusCode,
                                $object[1]/@versionLabel,
                                $object[1]/@lastModifiedDate,
                                $object[1]/@canonicalUri,
                                $object[1]/@expirationDate,
                                $object[1]/@officialReleaseDate,
                                $object[1]/@experimental,
                                $object[1]/@type,
                                $object[1]/@label,
                                if ($object[@url][@ident]) then
                                    if (($object[@url][@ident]/@ident)[1] != $projectPrefix) then (
                                        attribute url   { ($object[@url][@ident]/@url)[1] },
                                        attribute ident { ($object[@url][@ident]/@ident)[1] }
                                ) else ()
                                else (),
                                if ($object[1]/name) then $object[1]/name else (
                                    for $cl in $compiledLanguages
                                    return
                                        <name>{$cl, data($object[1]/(@name, @displayName)[1])}</name>
                                )
                                ,
                                $object[1]/desc,
                                if ($ed) then 
                                    $implementationguide/definition/resource[@artefact = $tp][@id = $id][@effectiveDate = $ed]/example
                                else (
                                    $implementationguide/definition/resource[@artefact = $tp][@id = $id]/example
                                )
                            }
                            </resource>
                    let $delete         := update delete $implementationguide/definition/resource[@generated]
                    let $update         := 
                        if ($implementationguide[definition]) then
                            if ($implementationguide/definition[*]) then
                                update insert $results preceding $implementationguide/definition/*[1]
                            else (
                                update insert $results into $implementationguide/definition
                            )
                        else 
                            update insert <definition>{$results}</definition> into $implementationguide
                    let $update         :=
                        if ($implementationguide[@lastModifiedDate]) then 
                            update value $implementationguide/@lastModifiedDate with substring(string(current-dateTime()), 1, 19)
                        else (
                            update insert attribute lastModifiedDate {substring(string(current-dateTime()), 1, 19)} into $implementationguide
                        )
                        
                    return ()
                )
                else ()
            (: / implementation guide stuff :)
             
            let $t                      := if ($publication-parameters) then prapi:createRuntimeFromCompilation($t/descendant-or-self::decor[1], $request) else ()
            
            let $progress               := 
                if ($request/@busy = 'true') then (
                    update value $request/@progress with $prapi:PROGRESS-COMPILE-40,
                    update value $request/@progress-percentage with 100
                )
                else ()
            
            
            let $remove                 := if ($request/@busy = 'true') then xmldb:remove(util:collection-name($request), util:document-name($request)) else ()
            
            return $t
        )
        else (
            let $message                := 'ERROR. Found ' || count($decor) || ' projects for prefix ' || $projectPrefix || '. Expected: 1. Compile id: ' || $request/@as
            let $log                    := utillib:log-scheduler-event('error', $threadId, $logsignature, $message)
            let $progress               := update value $request/@progress with $message
            
            return ()
        )
    
    return ()
};

(:~ Process publication requests :)
declare function prapi:process-publication-request($threadId as xs:string, $request as element(publication-request)) as element(decor)* {   
    let $logsignature           := 'prapi:process-publication-request'
    let $mark-busy              := update insert attribute busy {'true'} into $request
    let $progress               := 
        if ($request/@progress) then (
            update value $request/@progress with $prapi:PROGRESS-PUBLICATION-10 
        )
        else (
            update insert attribute progress {$prapi:PROGRESS-PUBLICATION-10} into $request
        )
    let $progress               := 
        if ($request/@progress-percentage) then (
            update value $request/@progress-percentage with round((100 div 7) * 1) 
        )
        else (
            update insert attribute progress-percentage {round((100 div 7) * 1)} into $request
        )
    
    (: =================== PREPARE VARIABLES BEGIN ==================== :)
    
    let $projectPrefix          := $request/@for[not(. = '*')]
    let $decor                  := if (empty($projectPrefix)) then () else utillib:getDecorByPrefix($projectPrefix)
    let $language               := $request/@language[not(. = '')]
    let $versionOrRelease       := $request/version | $request/release
    let $publication-parameters := $request/decor-parameters
    let $testFilters            := $request/@testfilters = 'true'
    let $runtimeOnly            := $request/@runtimeonly = 'true'
    let $forceCompile           := $request/@forcecompile = 'true'
    let $compile-language       := $request/@language
    let $development            := $request/@development = 'true'
    let $timeStamp              := if ($development) then 'development' else $request/@on
    let $implementationguide    := $request/@implementationguide = 'true'
    (: never do pub requests for development releases :)
    let $publication-request    := if ($development) then false() else $request/@publication-request = 'true'
    let $languages              := distinct-values((tokenize($compile-language, "\s+"), '*'))
    let $filters                := $request/filters
    let $filters                := comp:getCompilationFilters($decor, (), ())
    let $filters                := comp:getFinalCompilationFilters($decor, $filters)
    (:let $filters                :=  <filters filter="off"/>:)
    
    (: =================== PREPARE VARIABLES END ===================== :)
    
    (: =========== CREATE REQUIRED PUB COLLECTIONS BEGIN ============= :) 
    
    (: check if a compiled full result was saved earlier, or create it :)
    let $releaseFolder          := replace($projectPrefix, '-$', '')
    let $versioncol             := concat('version-', translate($timeStamp, '-:', ''))
    
    let $targetDir              := 
        try {
            (: create decor/releases/[prefix] :)
            let $projectRelease := xmldb:create-collection($setlib:strDecorVersion, $releaseFolder)
            let $ttp            := sm:chmod($projectRelease, 'rwxrwsr-x')
            (: for development='true' we want a clean folder :)
            let $removeDev      :=
                if ($development and xmldb:collection-available($projectRelease || '/' || $versioncol)) then
                    xmldb:remove(concat($projectRelease, '/', $versioncol))
                else ()
            (: create decor/releases/[prefix]/version-[version date or 'development'] :)
            let $thisRelease    := xmldb:create-collection($projectRelease, $versioncol)
            let $ttp            := sm:chmod($thisRelease, 'rwxrwsr-x')
            return
                $thisRelease
        }
        catch * {
            let $message        := 'Create collection / set permissions failed ' || $releaseFolder || '/' || $versioncol || ': ' || $err:code || ' - ' || $err:description
            let $log            := utillib:log-scheduler-event('error', $threadId, $logsignature, $message)
            return
                error($errors:SERVER_ERROR, $message)
        }
    
    (: this is where the [prefix]xpaths.xml will be :)
    let $resourcedir            := xmldb:create-collection($targetDir, 'resources')
                
    (: =========== CREATE REQUIRED PUB COLLECTIONS END =============== :) 
    
    (: ========= COPY ORIGINAL STUFF TO PUB COLLECTION BEGIN ========= :)
    
    (: Copy all child collections from project collection, logos/resources and potential others :)
    let $result                 := 
        for $child-collection in xmldb:get-child-collections(util:collection-name($decor))
        return
            xmldb:copy-collection(util:collection-name($decor) || '/' || $child-collection, $targetDir)
    
    (: store original decor file :)
    let $result                 := xmldb:store($targetDir, concat($projectPrefix, translate($timeStamp, '-:', ''), '-decor.xml'), $decor)
    
    (: copy any communities :)
    let $result                 := 
        for $community in $setlib:colDecorData//community[@projectId=$decor/project/@id]
        let $newReourceName          := concat('community-',$community/@name/string(),'-', translate($timeStamp, '-:', ''), '.xml')
        return
            xmldb:store($targetDir, $newReourceName, $community)
    
    (: ========= COPY ORIGINAL STUFF TO PUB COLLECTION END =========== :)
    
    (: ============ COMPILE ALL REQUESTED LANGUAGES BEGIN ============ :)
    
    let $progress               := update value $request/@progress with $prapi:PROGRESS-PUBLICATION-20
    let $progress               := update value $request/@progress-percentage with round((100 div 7) * 2)
    
    (: first produce the 'all languages' compilation. then for every requested language:
        - use an xslt to create a single language version out of the 'all languages' result store that
        - merge xpaths into fullDatasetTree and store transactions.xml :)
    let $language               := '*'
    let $filelang               := 'all'
    let $progress               := update value $request/@progress with 'Compiling language [1/' || count($languages) || ']: ' || $filelang || ' ...'
    (: if $compiledProject is empty then something went wrong (e.g. no connection to repositories possible to resolve references) and simply give up with an error, otherwise continue to save the artifacts :)
    let $compiledResourceName   := concat($projectPrefix, translate($timeStamp, '-:', ''), '-', $filelang, '-decor-compiled.xml')
    let $storedAllLanguages     := 
        doc(xmldb:store($targetDir, $compiledResourceName, 
            comp:compileDecor($decor, $language, $timeStamp, $filters, $testFilters, $runtimeOnly)
        ))/decor
    
    let $xsl                    := prapi:getDecorIntoSingleLanguageStylesheet()
    let $compiledProjects       := 
        for $language at $i in $languages[not(. = '*')]
        let $filelang                   := if ($language = '*') then ('all') else ($language)
        let $progress                   := update value $request/@progress with 'Compiling language [' || $i + 1 || '/' || count($languages) || ']: ' || $filelang || ' ...'
        (: if $compiledProject is empty then something went wrong (e.g. no connection to repositories possible to resolve references) and simply give up with an error, otherwise continue to save the artifacts :)
        let $compiledResourceName       := concat($projectPrefix, translate($timeStamp, '-:', ''), '-', $filelang, '-decor-compiled.xml')
        let $xsltParameters     :=
            <parameters>
                <param name="language" value="{$language}"/>
                <param name="defaultLanguage" value="{$decor/project/@defaultLanguage}"/>
            </parameters>
        order by $language
        return
            doc(xmldb:store($targetDir, $compiledResourceName, transform:transform($storedAllLanguages, $xsl, $xsltParameters)))/decor
    
    let $compiledProjects       := $storedAllLanguages | $compiledProjects
    
    let $addDevelopmentProps    :=
        if ($development) then
            for $c in $compiledProjects
            (: for development releases the @versionLabel will normally be empty. this populates the label provided by $data, if any :)
            let $storeVersionLabel      :=
                if ($versionOrRelease/@versionLabel[not(. = '')]) then 
                    if ($c/@versionLabel[not(. = '')]) then () else
                    if ($c/@versionLabel) then (
                        update value $c/@versionLabel with $versionOrRelease/@versionLabel
                    )
                    else (
                        update insert $versionOrRelease/@versionLabel into $c
                    )
                else ()
            (: for development releases the $versionOrRelease is not inserted into the live project and thus not compiled either. add it here as if it was always there :)
            let $storeVersionOrRelease  :=
                    if ($c/project[release | version]) then 
                        update insert $versionOrRelease preceding $c/project/(release | version)[1]
                    else (
                        update insert $versionOrRelease into $c/project
                    )
            return ()
        else ()
    
    (: ============ COMPILE ALL REQUESTED LANGUAGES END ============== :)
    
    (: ================ GET FOREIGN COMMUINITIES BEGIN =============== :)
    
    let $progress               := update value $request/@progress with $prapi:PROGRESS-PUBLICATION-30
    let $progress               := update value $request/@progress-percentage with round((100 div 7) * 3)
    
    (: copy any foreign communities compiled in during compilation by utillib:getFullDataset() :)
    let $communityProjectMap    := 
        map:merge(
            for $pfx in $compiledProjects//dataset/@prefix[not(. = $decor/project/@prefix)]
            let $commPrefix     := $pfx
            group by $commPrefix
            return (
                let $commDecor  := utillib:getDecorByPrefix($commPrefix[1])
                return
                    if ($commDecor) then map:entry($commDecor/project/@id, $commPrefix) else ()
            )
        )
    let $commProjectIds         := map:keys($communityProjectMap)
    let $result                 := 
        for $community in $setlib:colDecorData//community[@projectId = $commProjectIds]
        let $commPrefix             := map:get($communityProjectMap, $community/@projectId)
        let $newReourceName         := concat('community-', $commPrefix, $community/@name/string(),'-', translate($timeStamp, '-:', ''), '.xml')
        return
            xmldb:store($targetDir, $newReourceName, 
                element {name($community)} {
                    $community/(@* except @ident), 
                    attribute ident {$commPrefix}, 
                    $community/node()
                }
            )
    
    (: ================ GET FOREIGN COMMUINITIES END ================= :)
    
    (: ====================== HANDLE XPATHS BEGIN  ===================== :)
    
    let $progress               := update value $request/@progress with $prapi:PROGRESS-PUBLICATION-40
    let $progress               := update value $request/@progress-percentage with round((100 div 7) * 4)
    
    (: store Xpaths in decor/releases/{projectPrefix}/version-{timestamp}/resources :)
    let $xpathResourceName          := concat($projectPrefix, 'xpaths.xml')
    let $storeXpaths                :=
        if ($compiledProjects[1]) then (
            let $compiledProject    := $compiledProjects[1]
            let $cpreprefcount      := count($compiledProject//representingTemplate[@ref][concept])
            return
            doc(xmldb:store($resourcedir, $xpathResourceName,
                <xpaths status="draft" version="{$timeStamp}" generated="{current-dateTime()}">
                {
                    (: can only calculate meaningful paths if there are dataset concepts tied to them :)
                    if ($compiledProject[datasets//concept][rules/templateAssociation/concept]) then (
                        for $representingTemplate at $pos in $compiledProject//representingTemplate[@ref][concept]
                        let $tr     := $representingTemplate/ancestor::transaction[1]
                        let $trid   := $tr/@id
                        let $tred   := $tr/@effectiveDate
                        (: let $logevent := util:log("INFO", current-dateTime() || ' XPATH TR ' || $trid || ' ' || $tred || ' ' || $pos || '/' || $cpreprefcount) :)
                        (: add additional progress information for the impatient :)
                        let $progress := update value $request/@progress with concat($prapi:PROGRESS-PUBLICATION-40, '[', $pos, '/', $cpreprefcount, ']')
                        return 
                            utillibx:getXpaths($compiledProject, $representingTemplate)
                    ) else ()
                }
                </xpaths>
            ))
        ) else ()
    
    (: ====================== HANDLE XPATHS END ====================== :)
    
    (: ================== HANDLE TRANSACTIONS BEGIN ================== :)
    
    let $progress               := update value $request/@progress with $prapi:PROGRESS-PUBLICATION-50
    let $progress               := update value $request/@progress-percentage with round((100 div 7) * 5)
    
    (: store transactions in decor/releases/{projectPrefix}/version-{timestamp}/{projectPrefix}-{timestamp}-{$language}-transactions.xml :)
    let $compiledProjectsCount  := count($compiledProjects)
    let $storeTransactions      := 
        for $compiledProject at $ppos in $compiledProjects
        let $language                   := $compiledProject/@language
        let $filelang                   := if ($language = '*') then ('all') else ($language)
        let $transactionResourceName    := concat($projectPrefix, translate($timeStamp, '-:', ''), '-', $filelang, '-transactions.xml')
        let $compiledTransactionCount   := count($compiledProject//transaction[representingTemplate[@sourceDataset]])
        return
            xmldb:store($targetDir, $transactionResourceName,
                <transactionDatasets projectId="{$decor/project/@id}" prefix="{$projectPrefix}" versionDate="{$timeStamp}">
                {
                    $compiledProject/@versionLabel[not(. = '')],
                    $compiledProject/@language[not(. = '')]
                }
                {
                    (:compilation handles filtering so it would contain exactly those ids we need:)
                    for $compiledTransaction at $tpos in $compiledProject//transaction[representingTemplate[@sourceDataset]]
                    let $progressdesc   := concat($prapi:PROGRESS-PUBLICATION-50,
                        " [project ", $ppos, "/", $compiledProjectsCount,
                        " - transaction ", $tpos, "/", $compiledTransactionCount, "]")
                    let $progress       := update value $request/@progress with $progressdesc
                    let $transaction    := utillib:getTransaction($compiledTransaction/@id, $compiledTransaction/@effectiveDate)
                    return utillib:getFullDatasetTree($transaction, (), (), $language, $storeXpaths, true(), ())
                }
                </transactionDatasets>
            )
    (: ================== HANDLE TRANSACTIONS END ==================== :)
    
    (: ============= CREATING PUBLICATION REQUEST BEGIN ============== :)

    let $progress               := update value $request/@progress with $prapi:PROGRESS-PUBLICATION-60
    let $progress               := update value $request/@progress-percentage with round((100 div 7) * 6)
    
    (: last action: store publication request - if given - for further processing by ADRAM :)
    let $result := 
        if ($publication-request) then (
            (: create publication request :)
            let $publicationdata := 
                <publication projectId="{$decor/project/@id}" prefix="{$projectPrefix}" versionDate="{$timeStamp}" versionSignature="{translate($timeStamp, '-:', '')}" 
                             reference="{$decor/project/reference/@url}" compile-language="{$compile-language}" deeplinkprefix="{serverapi:getServerURLArt()}">
                {
                    $versionOrRelease,
                    $publication-parameters,
                    $filters
                }
                    <request on="{$timeStamp}" status="OK"/>
                </publication>
            (:let $prqlsuffix := if (string-length($language) > 0) then concat('-', $language) else '':)
            return
                xmldb:store($targetDir, concat($projectPrefix, replace($timeStamp, '\D', ''), '-publication-request.xml'), $publicationdata) 
        )
        else ()
            
     (: prepare already an ADRAM request in case needed later, some variables are no longer accessible when deleting request phase 1 file :)
     let $stamp          := util:uuid()
     let $now            := substring(xs:string(current-dateTime()), 1, 19)
     let $adramfile      := concat($projectPrefix, replace($now, '\D', ''), '.xml')
     let $adram-request  :=  
        <publication-request adram="true" uuid="{$stamp}" for="{$projectPrefix}" on="{$timeStamp}" as="{$stamp}" by="{$versionOrRelease/@by}" 
            language="{$compile-language}" publicationrequests="1"/>
        
     (: ============= CREATING PUBLICATION REQUEST END ================ :)
            
     (: ========== ADD IMPLEMENTATIONGUIDE FIRST DRAFT BEGIN ========== :)
     
     (: if an implementationguide first draft is requested, add it :)
     let $stamp      := util:uuid()
     let $igcontent  :=
        <implementationGuide 
            id="{$stamp}"
            name="{concat($projectPrefix, '-ig-', $stamp)}" 
            displayName="{concat('ART-DECOR Implementation Guide skeletton ', replace($now, '\D', ''))}"
            effectiveDate="{$now}" 
            statusCode="draft">
            <definition>
                <resource/>
                <page title="Index Page">
                    <content language="en-US">Index Page</content>
                </page>
            </definition>
        </implementationGuide>
     let $store := 
        if ($implementationguide) then 
            xmldb:store($targetDir, concat($projectPrefix, translate($timeStamp, '-:', ''), '-implementationguide.xml'), $igcontent)
        else ()
     
     (: ========== ADD IMPLEMENTATIONGUIDE FIRST DRAFT END ========== :)
            
     (: =================== HAND OVER TO ADRAM BEGIN ================ :)
     
     (: first delete the first phase publication request if finished :)
     let $progress               := 
        if ($request/@busy = 'true' or $implementationguide) then (
            update value $request/@progress with $prapi:PROGRESS-PUBLICATION-70,
            update value $request/@progress-percentage with 100,
            try {
                let $dele := xmldb:remove(util:collection-name($request), util:document-name($request))
                return true()
            } catch * {
                false()
            }
        ) else false()
    (: if requested and previous request phase completed: create a new scheduled task for ADRAM where the adram flag is set to signal ADRAM to continue :)
    let $adram          :=
        if ($publication-request and $progress and not($implementationguide)) then (
            let $write          := xmldb:store($setlib:strDecorScheduledTasks, $adramfile, $adram-request)
            let $tt             := sm:chmod($write, 'rw-rw----')
            return ()
        ) else ()

    (: =================== HAND OVER TO ADRAM END ==================== :)
    
    (: return compile project :)
    return 
        $compiledProjects
};

(:~ Stylesheet for converting multilinugal decor project into a single language decor project 
Input : (compiled) DECOR project with all languages (*)
Output: same as input but iwth just a single language per element type qualified with @language
  
Parameter language should contain the language signifier - eg nl-NL - that if available should be kept. Default value is the project default language
Parameter defaultLanguage should contain the language signifier - eg en-US - that if available should be kept in case no value is present for the requested language. Default value is the project default language

If there is no data both for language and defaultLanguage, defaults to the first available, if any

Example 1 for language nl-NL and defaultLanguage en-US

<desc language="nl-NL">Patiënt</desc>  - this is kept (language present)
<desc language="en-US">Patient</desc>  - this is skipped

Example 2 for language nl-NL and defaultLanguage en-US

<desc language="it-IT">Paziente</desc>
<desc language="en-US">Patient</desc>  - this is kept (language not present, use defaultLanguage)

Example 3 for language nl-NL and defaultLanguage en-US

<desc language="it-IT">Paziente</desc> - this is kept (language not present, defaultLanguage not present, take the first of the set)
<desc language="de-DE">Patient</desc>

Example 4 for language nl-NL and defaultLanguage en-US

[no desc]                              - no output
-->
:)
declare %private function prapi:getDecorIntoSingleLanguageStylesheet() as element(xsl:stylesheet) {
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:math="http://www.w3.org/2005/xpath-functions/math" exclude-result-prefixes="xs math" version="2.0">
  
  <!--<xsl:output indent="yes"/>-->
  <xsl:param name="language" select="(//project/@defaultLanguage)[1]" as="xs:string"/>
  <xsl:param name="defaultLanguage" select="(//project/@defaultLanguage)[1]" as="xs:string"/>
  
  <xsl:template match="decor/@language" priority="+1">
    <xsl:attribute name="language" select="$language"/>
  </xsl:template>
  
  <xsl:template match="project//*[@language] | dataset//*[@language]">
    <xsl:variable name="nodes" select="../*[name() = current()/name()]"/>
    
    <xsl:choose>
      <!-- when comment, just copy -->
      <xsl:when test="self::comment">
        <xsl:copy-of select="."/>
      </xsl:when>
      <!-- only do something upon first encounter of a given node type -->
      <xsl:when test="empty(preceding-sibling::*[name() = current()/name()])">
        <xsl:apply-templates select="($nodes[@language = $language], $nodes[@language = $defaultLanguage], $nodes)[1]" mode="filter"/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="@* | node()" mode="#all">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
};

(:~ Process check requests :)
declare function prapi:process-projectcheck-request($threadId as xs:string, $request as element(projectcheck-request)) {
    let $logsignature           := 'prapi:process-projectcheck-request'
    let $doCompile              := $request/@compile = 'true'
    let $progress-initial       :=
        if ($doCompile) then $prapi:PROGRESS-PROJECTCHECK-10-a else $prapi:PROGRESS-PROJECTCHECK-10-b
    
    let $mark-busy              := update insert attribute busy {'true'} into $request
    let $progress               := 
        if ($request/@progress) then (
            update value $request/@progress with $progress-initial 
        )
        else (
            update insert attribute progress {$progress-initial} into $request
        )
    let $progress               := 
        if ($request/@progress-percentage) then (
            update value $request/@progress-percentage with round((100 div 6) * 1) 
        )
        else (
            update insert attribute progress-percentage {round((100 div 6) * 1)} into $request
        )
    
    (: ===================  PREPARE VARIABLES START  ==================== :)
    
    let $timeStamp              := $request/@on
    let $projectPrefix          := $request/@for[not(. = '*')]
    let $decor                  := if (empty($projectPrefix)) then () else utillib:getDecorByPrefix($projectPrefix)
    let $testFilters            := $request/@testfilters = 'true'
    let $runtimeOnly            := $request/@runtimeonly = 'true'
    let $forceCompile           := $request/@forcecompile = 'true'
    let $doCompile              := $request/@compile = 'true'
    let $compile-language       := $request/@language
    let $development            := $request/@development = 'true'
    let $languages              := distinct-values((tokenize($compile-language, "\s+"), '*'))
    let $filters                := $request/filters
    let $filters                := comp:getCompilationFilters($decor, (), ())
    let $filters                := comp:getFinalCompilationFilters($decor, $filters)
    (:let $filters                :=  <filters filter="off"/>:)
    
    (: ===================  PREPARE VARIABLES END  ==================== :)
    
    let $decorProject           := $decor/project
    let $projectId              := $decorProject/@id
    let $projectPrefix          := $decorProject/@prefix
    (:let $language               := if (empty($language)) then $decor/project/@defaultLanguage else $language:)
    
    (: check if a compiled result was saved earlier, or create it :)
    let $compiledDecor          := 
        if ($doCompile) then
            comp:getCompiledResult($decor, current-dateTime(), (), $filters, $testFilters, $runtimeOnly, $forceCompile)
        else (
            $decor
        )
    
    let $lastChecked            := current-dateTime()
    let $lastModified           := xmldb:last-modified(util:collection-name($decorProject), util:document-name($decorProject))
    let $checked                := true()
    
    return
        if ($compiledDecor[descendant-or-self::decor]) then (
            (:let $progress               := update value $request/@progress-percentage with round((100 div 5) * 2) 
            let $progress               := update value $request/@progress with $prapi:PROGRESS-PROJECTCHECK-20:)
            
            let $progress               := update value $request/@progress with $prapi:PROGRESS-PROJECTCHECK-30
            let $progress               := update value $request/@progress-percentage with round((100 div 6) * 3) 
            
            let $compiledDecor          := $compiledDecor/descendant-or-self::decor[1]
            let $strDecorDevelopment    := setlib:strProjectDevelopment($projectPrefix)
            
            let $decorSchFile           := concat($setlib:strDecorCore, '/DECOR.sch')
            let $decorSch               := if (doc-available($decorSchFile)) then doc($decorSchFile)/sch:schema else ()
            let $projectChecks          := collection($strDecorDevelopment)//@optionalChecks[1]
            let $optionalCheckFile      := concat($setlib:strDecorCore, '/DECOR-optional-checks.sch')
            let $optionalChecks         := if (doc-available($optionalCheckFile)) then doc($optionalCheckFile)/sch:schema else ()
            let $projectSchFile         := concat($strDecorDevelopment, $projectPrefix, 'checks.sch')
            let $projectSch             := if (doc-available($projectSchFile)) then doc($projectSchFile)/sch:schema else ()
            let $checkrpt               := concat($projectPrefix, 'decorcheck-report.xml')
            
            (: Projectschematron are: 
            - checks in DECOR-optional-checks.sch which are turned on in project/@optionalChecks (for reusable checks which can be useful to multiple projects)
            - a project-specific file [prefix]-checks.sch in decor/checks (for checks which are specific to just one project - schematron must be uploaded by eXist admin)
            :)
            let $projectSchematron      := 
                if ($projectChecks and $optionalChecks) then
                    <sch:schema queryBinding="xslt2" xmlns:sch="http://purl.oclc.org/dsdl/schematron">
                        <sch:ns uri="http://purl.oclc.org/dsdl/schematron" prefix="sch"/>
                    {
                        for $check in tokenize($projectChecks, '\s+')
                        return $optionalChecks//sch:pattern[@id = $check]
                    }
                    {
                        for $pattern in $projectSch//sch:pattern
                        return $pattern
                    }
                    </sch:schema>
                else ()
            
            let $progress               := update value $request/@progress-percentage with round((100 div 6) * 4) 
            let $progress               := update value $request/@progress with $prapi:PROGRESS-PROJECTCHECK-40
            
            let $decorschresult         := transform:transform($compiledDecor, utillib:get-iso-schematron-svrl($decorSch), ())
            let $projectschresult       := 
                if ($projectSchematron) then transform:transform($compiledDecor, utillib:get-iso-schematron-svrl($projectSchematron), ()) else ()
            
            let $progress               := update value $request/@progress-percentage with round((100 div 6) * 5) 
            let $progress               := update value $request/@progress with $prapi:PROGRESS-PROJECTCHECK-50
            
            let $report                 := 
                <report lastChecked="{$lastChecked}" lastModified="{$lastModified}" checked="{$checked}">
                {
                    attribute compiled {exists($compiledDecor/@versionDate)},
                    if (comp:isCompilationFilterActive($filters)) then (
                        attribute filterid {($filters/@id)[1]},
                        attribute filterlabel {($filters/@label, $filters/@id)[1]} 
                    )
                    else ()
                }
                    <schema>
                    {
                        (: validate against DECOR schema :)
                        validation:jaxv-report($decor, $setlib:docDecorSchema)
                    }
                    </schema>
                    <schematron>
                    {
                        (: validate against DECOR schematron :)
                        if ($decorschresult) then prapi:svrl2report($decorschresult) else ()
                    }
                    </schematron>
                {
                    if ($projectschresult) then <projectSchematron>{prapi:svrl2report($projectschresult)}</projectSchematron> else ()
                }
                {
                    if ($projectPrefix = 'peri20-') then (
                        <checkWarning>Compiled checking and project schematron disabled for performance reasons for project {data($projectPrefix)} '{data($decor/project/name[1])}'</checkWarning>
                    ) else ()
                }
                </report>
            
            (: store the result in cache checks :)
            let $coll                   := xmldb:create-collection($setlib:strDecorVersion, replace(substring-after($strDecorDevelopment, $setlib:strDecorVersion), '^/+', ''))
            let $delete                 := try { xmldb:remove($strDecorDevelopment, $checkrpt) } catch * {()}
            let $save                   := xmldb:store($strDecorDevelopment, $checkrpt, $report)
            let $tt                     := try { sm:chmod($save, 'rw-rw----') } catch * {()}
            
            let $progress               := 
              if ($request/@busy = 'true') then (
                  update value $request/@progress with $prapi:PROGRESS-PROJECTCHECK-60,
                  update value $request/@progress-percentage with 100, 
                  xmldb:remove(util:collection-name($request), util:document-name($request))
              )
              else ()
            
            return ()
        )
        else (
            update value $request/@progress with 'Compilation running since ' || $compiledDecor/string(@compileDate) || '. Will try again later ...', 
            update delete $request/@busy
        )
};

(:~ Create runtime from compilation :)
declare function prapi:createRuntimeFromCompilation($compiledDecor as element(decor), $request as element(compile-request)) as element(compilation)? {
    let $now                      := $request/@on
    let $stamp                    := data($request/@as)
    let $projectPrefix            := $compiledDecor/project/@prefix
    let $filters                  := $request/filters

    (: the following assignment serves for the fn:transform use TEST :)
    let $publication-parameters   := map:merge(
            for $p in $request/parameters/param
            return map:entry(xs:QName(string($p/@name)), string($p/@value))
        )
    (: this following assignment serves for the transform:transform use :)
    let $publication-parameters   := $request/parameters

    let $startT                   := util:system-time()

    let $progress                 := update value $request/@progress with $prapi:PROGRESS-RUNTIME-10
    let $progress                 := update value $request/@progress-percentage with 11
    
    (: create collections if allowed, propagate the errors :)
    let $targetDir                := 
        try {
            let $ttt              := xmldb:create-collection($setlib:strDecorVersion, concat(replace($projectPrefix, '-$', ''), '/development'))
            let $tt               := if (sm:has-access($ttt, 'rwx')) then () else sm:chmod($ttt, 'rwxrwsr-x')
            return $ttt
        }
        catch * {
            <error>+++ Error creating targetDir: {$err:code}: {$err:description} {$err:module} [at line {$err:line-number}, ref: {$stamp}]</error>
        }
    
    let $schDir                   := 
        try {
            let $ttt              := if ($targetDir instance of element()) then () else xmldb:create-collection($targetDir, $stamp)
            let $tt               := if ($targetDir instance of element()) then () else if (sm:has-access($ttt, 'rwx')) then () else sm:chmod($ttt, 'rwxrwsrwx')
            return $ttt
        }
        catch * {
            <error>+++ Error creating schDir: {$err:code}: {$err:description} {$err:module} [at line {$err:line-number}, ref: {$stamp}]</error>
        }

    let $inclDir                  := 
        try {
            let $ttt              := if ($targetDir instance of element()) then () else xmldb:create-collection($schDir, 'include')
            let $tt               := if ($targetDir instance of element()) then () else if (sm:has-access($ttt, 'rwx')) then () else sm:chmod($ttt, 'rwxrwsrwx')
            return $ttt
        }
        catch * {
            <error>+++ Error creating inclDir: {$err:code}: {$err:description} {$err:module} [at line {$err:line-number}, ref: {$stamp}]</error>
        }
        
    let $progress                 := update value $request/@progress with $prapi:PROGRESS-RUNTIME-20
    let $progress                 := update value $request/@progress-percentage with 22
    
    let $xslt                     := concat('xmldb:exist://', $setlib:strDecorCore, '/DECOR-templatefragment2schematron.xsl')

    (: the following assignment serves for the fn:transform use TEST :)
    let $serialization-options    := map{
                                        "method": "xml",
                                        "media-type": "text/xml",
                                        "omit-xml-declaration": true(),
                                        "indent": false()
                                    }
    (: this following assignment serves for the transform:transform use :)
    let $serialization-options    := 'method=xml media-type=text/xml omit-xml-declaration=yes indent=no'
    
    let $transform                :=
        try {
            let $ttt              := if ($targetDir instance of element()) then () 
                else transform:transform($compiledDecor, xs:anyURI($xslt), $publication-parameters, (), $serialization-options)
                (: prep for using fn:transform 
                    fn:transform(map{
                        "source-node": $compiledDecor,
                        "stylesheet-location": $xslt,
                        "stylesheet-params": $publication-parameters,
                        "serialization-params": $serialization-options,
                        "enable-messages": true()
                    })?output
                :)
            return 
                if (empty($ttt)) then <error>+++ Transformation to schematron failed: empty result</error> else $ttt
        }
        catch * {
            <error>+++ Transformation to schematron failed: {$err:code}: {$err:description} {$err:module} [at line {$err:line-number}, ref: {$stamp}]</error>
        }
    (: the conversion process possibly write a last survivor message on error, catch it :)
    let $last-survivor-message    :=
        if ($targetDir instance of element()) then () else if (doc-available($schDir || '/last-survivor-message.xml')) then doc($schDir || '/last-survivor-message.xml') else <empty/>
        
    let $SCHtranserror            := count($transform/descendant-or-self::error) gt 0
    
    let $progress                 := update value $request/@progress with $prapi:PROGRESS-RUNTIME-30
    let $progress                 := update value $request/@progress-percentage with 44
    
    let $zipfile                  := if ($SCHtranserror) then () else compression:zip(xs:anyURI($schDir), true(), $schDir)
    
    let $progress                 := update value $request/@progress with $prapi:PROGRESS-RUNTIME-40
    let $progress                 := update value $request/@progress-percentage with 55

    (: create ISO schematron conversion to XSL :)
    let $isoschematrons           := $setlib:strUtilISOSCH2SVRL
    let $path2schematroninclude   := xs:anyURI(concat('xmldb:exist://', $isoschematrons, "/iso_dsdl_include.xsl"))
    let $path2schematronabstract  := xs:anyURI(concat('xmldb:exist://', $isoschematrons, "/iso_abstract_expand.xsl"))
    let $path2schematronxsl       := xs:anyURI(concat('xmldb:exist://', $isoschematrons, "/iso_svrl_for_xslt2.xsl"))
    let $isosch-xsltparams        :=
        <parameters>
            <param name="allow-foreign" value="true"/>
            <param name="generate-fired-rule" value="true"/>
            <param name="generate-paths" value="true"/>
            <param name="diagnose" value="yes"/>
            <param name="exist:stop-on-error" value="yes"/>
        </parameters>
    
    (: store the work-around wrapper XSL here to be picked up later by validator in validate-single-instance :)
    let $xslwrap                  :=
        <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" version="2.0">
            <xsl:include href="{$path2schematroninclude}"/>
        </xsl:stylesheet>
    let $xslstore                 := 
        try { 
            let $ttt    := if (empty($schDir)) then () else xmldb:store($schDir, 'sch2xsl.xsl', $xslwrap)
            let $tt     := if (empty($schDir)) then () else sm:chmod($ttt, 'rw-rw----')
            return $ttt
        } 
        catch * {
            <error>+++ Saving sch2xsl.xsl in the schematron dir failed: {$err:code}: {$err:description} {$err:module} [at line {$err:line-number}]</error>
        }
    
    let $dir :=
        if (empty($schDir)) then () else (
            <dir>
            {
                for $child in xmldb:get-child-resources($schDir)
                let $path := concat($schDir, '/', $child)
                let $title := doc($path)/*[name() = 'schema']/*[name() = 'title']/text()
                return
                    if (string-length($title) = 0)
                    then ()
                    else <resource name="{$child}" path="{$path}" title="{$title}" ref="{util:uuid()}"/>
            }
            </dir>
        )

    (: the following assignment serves for the fn:transform use TEST :)
    let $isosch-serialization-options := map{
            "method": "xml",
            "media-type": "text/xml",
            "omit-xml-declaration": false(),
            "indent": false()
        }
    (: this following assignment serves for the transform:transform use :)
    let $isosch-serialization-options := 'method=xml media-type=text/xml omit-xml-declaration=no indent=no'

    let $resourcecount  := count($dir/resource)
    let $babystepsrange := 88 - 55 (: round((100 div 9) * 8) - round((100 div 9) * 5) :)
    let $babystep       := $babystepsrange div $resourcecount
    let $schxsl         :=
        for $input at $rposition in $dir/resource
        let $input-coll           := string-join(tokenize($input/@path,'/')[not(position()=last())],'/')
        let $input-res            := replace(tokenize($input/@path,'/')[last()],'.sch$','.xsl')
        let $progress             := update value $request/@progress with $prapi:PROGRESS-RUNTIME-40 || ' (' || $rposition || '/' || $resourcecount || ')'

        (:
            the original type of pipe statement we used:
            let $xsl                    :=
               $grammar => transform:transform(doc($sch-include), $parameters) => 
                           transform:transform(doc($sch-expand), $parameters) => 
                           transform:transform(doc($sch-compile), $parameters)
            is unbelievable elegant but for large transforms this can cause java heap space conditions
            KH 20240910 split that into three transforms all based on one variable, thus transform to var and then use that as input for next step
        :)
        let $transform            := 
            try {
                let $x := transform:transform(doc($input/@path), doc(xs:anyURI(concat("xmldb:exist://", $input-coll, "/sch2xsl.xsl"))), $isosch-xsltparams, (), $isosch-serialization-options)
                let $x := transform:transform($x, $path2schematronabstract, $isosch-xsltparams)
                return transform:transform($x, $path2schematronxsl, $isosch-xsltparams)
                (: prep for using fn:transform 
                let $x := fn:transform(map{
                    "source-node": doc($input/@path),
                    "stylesheet-node": doc(xs:anyURI(concat("xmldb:exist://", $input-coll, "/sch2xsl.xsl"))),
                    "stylesheet-params": $isosch-xsltparams,
                    "serialization-params": $isosch-serialization-options,
                    "enable-messages": true()
                    })?output
                let $x := fn:transform(map{
                    "source-node": $x,
                    "stylesheet-node": doc($path2schematronabstract),
                    "stylesheet-params": $isosch-xsltparams,
                    "enable-messages": true()
                    })?output
                return fn:transform(map{
                    "source-node": $x,
                    "stylesheet-node": doc($path2schematronxsl),
                    "stylesheet-params": $isosch-xsltparams,
                    "enable-messages": true()
                    })?output
                :)
            } catch * {
                <error>+++ ISO schematron to SVRL (xsl) conversion failed: {$err:code}: {$err:description}</error>
            }
        
        let $progress             := update value $request/@progress-percentage with fn:round( 55 + ($rposition * $babystep), 2 )

        let $store                := 
            if ($transform instance of element(error)) then () else (
                let $store  := xmldb:store($input-coll, $input-res, $transform)
                let $tt     := sm:chmod($store, 'rw-rw----')
                return $store
            )
        return
            if ($transform instance of element(error))
            then <transform status="failed">{$transform}</transform>
            else <transform coll="{$input-coll}" xsl="{$input-res}" path="{$input/@path}"/>
    
    let $progress                 := update value $request/@progress with $prapi:PROGRESS-RUNTIME-50
    let $progress                 := update value $request/@progress-percentage with 90
    
    let $errortxt := 
        normalize-space(
            string-join((
                $targetDir[. instance of element(error)], 
                $schDir[. instance of element(error)], 
                $inclDir[. instance of element(error)], 
                $transform[. instance of element(error)], 
                $xslstore[. instance of element(error)]
            ), ' ')
        )
    (: add here $schxsl[transform/* instance of element(error)] to errortxt? :)

    let $durationT := (util:system-time() - $startT) div xs:dayTimeDuration("PT0.001S")
    
    let $res                      :=
        <compilation>
            <compiled>
            {
                $request/(@* except (@uuid|@status|@filterlabel|@transactions|@error|@lastsurvivormessage|@busy|@progress|@progress-percentage))
                ,
                attribute elapsed { $durationT }
                ,
                attribute uuid { util:uuid() }
                ,
                attribute status {if ($SCHtranserror) then 'failed' else 'active'}
                ,
                if ($filters) then 
                    attribute filterlabel {($filters/@label, $filters/@id)[1]} |
                    attribute transactions {$filters/transaction/concat(@ref, '--', @flexibility)}
                else (),
                if (string-length($errortxt) = 0) then () else attribute {'error'} {$errortxt},
                if (string-length($last-survivor-message) = 0) then () else attribute {'lastsurvivormessage'} {$last-survivor-message},
                $dir,
                if (empty($zipfile)) then () else <zip>{$zipfile}</zip>
            }
            </compiled>
            {
                $filters
            }
            <input>
            {
                $compiledDecor
            }
            </input>
            {
                $schxsl
            }
        </compilation>
    
    let $result         := if ($targetDir instance of element()) then () else xmldb:store($targetDir, concat($projectPrefix, $stamp, '.xml'), $res)
    let $tt             := if ($targetDir instance of element()) then () else sm:chmod(xs:anyURI($result), 'rw-rw-r--')
    
    let $haserrors      := string-length(normalize-space(string-join(($errortxt, $last-survivor-message), ''))) > 0

    let $busy-state     := 
        if ($haserrors) then (
            update value $request/@busy with 'error', 
            update value $request/@progress with $errortxt,
            if (string-length($last-survivor-message) > 0) then
                if (not($request/@lastsurvivormessage))
                then update insert attribute lastsurvivormessage {$last-survivor-message} into $request else ()
            else (),
            update value $request/@progress-percentage with 100
        ) else ()
    return
        if ($targetDir instance of element()) then () else doc($result)/compilation
};

(: =================   PROCESS REQUESTS END  ================== :)

(: ==================  SCHEDULED TASKS END ==================== :)

(: ==================  ADA ENVIRONMENTS START  ==================== :)

declare function prapi:getDecorProjectADAList($request as map(*)) {
    
    (:let $authmap                := $request?user:)
    let $project                := $request?parameters?project[string-length() gt 0]
    
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    
    let $decor          := if (utillib:isOid($project)) then utillib:getDecorById($project) else utillib:getDecorByPrefix($project)
    let $language       := $decor/project/@defaultLanguage
    let $art-decor-url  := serverapi:getServerURLArt()
    (:
    To build the links, all you need is '$url-art-decor-deeplinkprefix || @xforms' as url and xformsName as display text
    
    <app app="mp-mp9" 
         xforms="ada-data/projects/mp-mp9/views/medicatieproces_9_index.xhtml" 
         xformsName="mp-mp9 [Medicatieproces 9 Index]"
         admin="index-admin.xquery?app=mp-mp9&amp;language=nl-NL"
         url="$art-decor-url || @xforms"
         admin-url="$art-decor-url || '../ada/modules/' || @admin">
        <view transactionId="2.16.840.1.113883.2.4.3.11.60.20.77.4.138" transactionEffectiveDate="2016-07-13T09:29:58"/>
        <view transactionId="2.16.840.1.113883.2.4.3.11.60.20.77.4.107" transactionEffectiveDate="2016-04-13T17:06:07"/>
        <view transactionId="2.16.840.1.113883.2.4.3.11.60.20.77.4.148" transactionEffectiveDate="2016-11-08T10:34:41"/>
        <view transactionId="2.16.840.1.113883.2.4.3.11.60.20.77.4.104" transactionEffectiveDate="2016-03-23T16:54:50"/>
        <view transactionId="2.16.840.1.113883.2.4.3.11.60.20.77.4.95" transactionEffectiveDate="2015-12-01T10:32:15"/>
        <view transactionId="2.16.840.1.113883.2.4.3.11.60.20.77.4.131" transactionEffectiveDate="2016-07-12T11:39:14"/>
        <view transactionId="2.16.840.1.113883.2.4.3.11.60.20.77.4.102" transactionEffectiveDate="2016-03-23T16:32:43"/>
    </app>
    <xforms:trigger ref=".[@xforms]" appearance="minimal">
        <xforms:label ref="@xformsName"/>
        <xforms:load ev:event="DOMActivate" resource="{concat('/', @xforms)}" show="new"/>
    </xforms:trigger>
    :)
    let $ada-apps       := if ($decor) then ada:getApplications((), $language) else ()
    let $results        :=
        for $ada-app in $ada-apps
        return
            if ($decor//transaction[@id = $ada-app/view/@transactionId]) then 
                element {name($ada-app)} {
                    $ada-app/(@* except (@url|@admin-url)),
                    if ($ada-app[@xforms]) then attribute url {$art-decor-url || $ada-app/@xforms} else (),
                    if ($ada-app[@xforms]) then attribute admin-url {$art-decor-url || '../ada/modules/' || $ada-app/@admin} else (),
                    $ada-app/node()
                }
            else ()
    
    let $count          := count($results)
    let $max            := $count
    let $allcnt         := count($ada-apps)
    return
        <list artifact="ADA" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements(subsequence($results, 1, $max))
        }
        </list>
};

(: ==================  ADA ENVIRONMENTS END  ==================== :)


(: ==================  CHANGE ID START  ==================== :)

(:~ Function to use as a pre-flight before changing the ID of an artifact: get usage list of the original $id in project based on $projectPrefixOrId. 
    @param $projectPrefixOrId required path part which may be its prefix or its oid
    @param $artifact required path part being the artifact in question: valueSet template codeSystem conceptMap questionnaires profile.
    @param $id required path part being the id of the artifact in question
    @param $effectiveDate required path part being the effectiveDate of the artifact in question
    @param $newid required path part being the planned/desired id of the artifact in question after change
    @return list as JSON object with zero to many artifact where $id is used with short descriptions to display as the result of the pre-flight
    GET /project/{project}/{artifact}/{id}/{effectivedate}/$change-id/{newid}
    @since 2024-04-24
:)

declare function prapi:getChangeProjectArtifactIdEffects($request as map(*)) {
    local:getChangeProjectArtifactIdEffects($request, 'check')
};
declare function prapi:patchChangeProjectArtifactIdEffects($request as map(*)) {
    local:getChangeProjectArtifactIdEffects($request, 'change')
};

declare %private function local:getChangeProjectArtifactIdEffects($request as map(*), $action as xs:string) {

    let $authmap                := $request?user
    let $projectPrefixOrId      := $request?parameters?project[string-length() gt 0]
    let $artifactType           := $request?parameters?artifact[string-length() gt 0]
    let $artifactId             := $request?parameters?id[string-length() gt 0]
    let $artifactEffectiveDate  := $request?parameters?effectiveDate[string-length() gt 0]
    let $newId                  := $request?parameters?newid[string-length() gt 0]
    
    let $check                  :=
        if (empty($projectPrefixOrId)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    
    let $check                  :=
        if (empty($artifactType)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter artifact type')
        else ()
        
    let $check                  :=
        if (empty($artifactId)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter artifact id')
        else ()
        
    let $check                  :=
        if (empty($artifactEffectiveDate)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter artifact effective date')
        else ()

    let $check                  :=
        if (empty($newId)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter newId')
        else ()
        
    let $check                  :=
        if (not(utillib:isOid($newId))) then
            error($errors:BAD_REQUEST, "Parameter newId '" || $newId || "' is not a valid OID / Identifier")
        else ()
        
    let $check                  :=
        if ($artifactType = ('template', 'valueSet', 'codeSystem', 'conceptMap', 'questionnaire', 'profile')) then ()
        else error($errors:BAD_REQUEST, 'Artifact type parameter contains unexpected contents')

    (: let us get more seroius now :)
    
    let $decor                  := 
        if ($projectPrefixOrId instance of element(decor)) then $projectPrefixOrId else 
        if (utillib:isOid($projectPrefixOrId)) then utillib:getDecorById($projectPrefixOrId) else utillib:getDecorByPrefix($projectPrefixOrId)
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $projectPrefixOrId, ''', does not exist.'))
        )
    let $check                  :=    
        if (count($decor) gt 1) then
            error($errors:SERVER_ERROR, concat("Found multiple projects for project '", $projectPrefixOrId, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        else ()
        
    (: check editor rights if needed, ie if action is 'change', pass if action is 'check' :)
    let $check                  :=
        if ($action = 'check') then () else
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if ($action = 'check') then () else
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-PROJECT)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify ids in project ', $projectPrefixOrId, '. You have to be an active author in the project.'))
        )

    let $find                   := $decor//*[@id=$artifactId][@effectiveDate=$artifactEffectiveDate][not(name()='object')]
    
    let $newIdInUseCount        := count($decor//*[@id=$newId])
        
    let $usedIn                 := if (empty($find)) then () else $decor//*[$artifactId = (@*)]
    let $count                  := count($usedIn)
    
    (: some last checks :)
    
    let $check                  :=
        if (empty($find)) then (
            error($errors:BAD_REQUEST, concat("Artifact with id '", $artifactId, " as of ", $artifactEffectiveDate,
              "' not found in project '", $projectPrefixOrId, "' (expected exactly 1)."))
        )
        else ()
        
    let $check                  :=
        if (count($find) gt 1) then (
            error($errors:BAD_REQUEST, concat("Found multiple artifacts with id '", $artifactId, "' (expected exactly 1) in project '", $projectPrefixOrId, "'."))
        )
        else ()
        
    let $check                  :=
        if (not(($find/name())[1] = $artifactType)) then (
            error($errors:BAD_REQUEST, concat("Artifact with id '", $artifactId, "' is of a different type (", ($find/name())[1], ") than specified type parameter (", $artifactType, ")."))
        ) else ()
        
    (: if requested (action = 'change') do the ID change of the artefact :)
    let $stored   := $find/@id
    let $new      := $newId
    let $update                 :=
        if ($action = 'change') then (
            (: we know that $find points to exactly the single artifact where the id change is requested, go ahead :)
            if ($stored) then update replace $stored with $new else ()
        ) else ()
    
    let $results                :=
        <list artifact="IDUSAGE" all="{$count}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            if ($action = 'change') then attribute { 'changed' } { '1' } else (),
            attribute { 'artifactId' } { $artifactId },
            attribute { 'artifactEffectiveDate' } { $artifactEffectiveDate },
            attribute { 'newIdInUseCount' } { $newIdInUseCount },
            for $a in $usedIn
            let $revp := reverse($a/ancestor::*)
            let $itemname := name($a)
            let $itemtype := if (count($revp) > 2) then $revp[last()-2]/name() else $itemname
            return
                <item xmlns:json="http://www.json.org">
                {
                    attribute { 'name' } { $itemname },
                    for $b in $a/@*
                    return
                        (
                            if ($b = $artifactId)
                            then attribute { 'attribute' } { name($b) }
                            else ()
                        ),
                    attribute { 'path' } { string-join($a/ancestor::*/name(), '.') },
                    attribute { 'artifact' } { $itemtype },
                    if ($a/name() = ('valueSet', 'template')) then (
                            (: an element with the id in question :)
                            attribute { 'id' } { $a/@id },
                            attribute { 'effectiveDate' } { $a/@effectiveDate },
                            attribute { 'displayName' } { $a/@displayName }
                    ) else
                    if ($a/name() = ('terminologyAssociation', 'xxxxx')) then (
                            (: an element with the id in question :)
                            attribute { 'id' } { $a/@conceptId }
                    ) else
                        for $c in $revp
                        return
                            if ($c/name() = ('template', 'valueSet', 'codeSystem', 'conceptMap', 'questionnaire', 'profile')) then (
                                (: a template with an element with the id in question :)
                                attribute { 'id' } { $c/@id },
                                attribute { 'effectiveDate' } { $c/@effectiveDate },
                                attribute { 'displayName' } { $c/@displayName }
                            ) else
                            if ($c/name() = 'terminologyAssociation') then (
                                (: a terminologyAssociation with the id in question :)
                                attribute { 'id' } { $c/@conceptId }
                            ) else
                            if ($c/name() = 'issue') then (
                                (: an issue referring to an object with the id in question :)
                                attribute { 'id' } { $c/@id },
                                attribute { 'effectiveDate' } { $c/tracking[1]/@effectiveDate },
                                attribute { 'displayName' } { $c/@displayName }
                            ) else ()
                }
                </item>
        }
        </list>

    return
        for $result in $results
        return utillib:addJsonArrayToElements($results)

};


(: ==================  CHANGE ID END  ==================== :)

declare function prapi:getDecorId($projectPrefixOrId as xs:string, $id as xs:string, $projectVersion as xs:string?, $projectLanguage as xs:string?) as element(id)* {
    let $decor                  := if (utillib:isOid($projectPrefixOrId)) then utillib:getDecorById($projectPrefixOrId, $projectVersion, $projectLanguage) else utillib:getDecorByPrefix($projectPrefixOrId, $projectVersion, $projectLanguage)
    
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $projectPrefixOrId, ''', does not exist.'))
        )
    
    for $n in $decor/ids/id[@root = $id]
    return
        <id>
        {
            $n/@root, $n/@assigningAuthority[not(. = '')], $n/@extension[not(. = '')],
            for $sn in $n/designation
            return
                <designation>{$sn/@language[not(. = '')], $sn/@type[not(. = '')], $sn/@displayName[not(. = '')]}</designation>
            ,
            for $sn in $n/property
            return
                utillib:serializeNode($sn)
        }
        </id>
};

declare %private function prapi:getDecorProjectList($authmap as map(*)?, $searchTerms as xs:string*, $projectPrefixOrId as xs:string?, $repository as xs:boolean?, $experimental as xs:boolean?, $author as xs:boolean?, $changeddate as xs:string?, $sort as xs:string?, $sortorder as xs:string?, $max as xs:integer?) as element(list) {
    let $userLanguage       := userapi:getUserLanguage(string($authmap?name))
    let $searchTerms        := tokenize(lower-case($searchTerms[string-length() gt 0]),'\s')
    let $projectPrefixOrId  := $projectPrefixOrId[string-length() gt 0]
    let $sortorder          := $sortorder[. = 'descending']
    
    let $decors             := 
        if (empty($projectPrefixOrId)) then 
            utillib:getDecorByPrefix('*') 
        else 
        if (utillib:isOid($projectPrefixOrId)) then 
            utillib:getDecorById($projectPrefixOrId) 
        else (
            utillib:getDecorByPrefix($projectPrefixOrId)
        )
       
    let $allcnt             := count($decors)
    let $termIsId           := count($searchTerms)=1 and $searchTerms[1] castable as xs:integer
    
    let $searchUpdate       := replace($changeddate, '^[a-z]{2}', '')
    let $searchUpcomp       := replace($changeddate, '^([a-z]{2}).*', '$1')
    
    let $check              :=
        if (empty($changeddate)) then () else if ($searchUpdate castable as xs:date) then () else (
            error($errors:BAD_REQUEST, 'Parameter changeddate SHALL contain/be a valid date yyyy-mm-dd. Found: ' || $changeddate)
        )
        
    let $results            := 
        if ($termIsId) then
            $decors[project[ends-with(@id,concat('.',$searchTerms[1]))]]
        else (
            let $r  := 
                if (empty($searchTerms)) then $decors else (
                    let $luceneQuery    := utillib:getSimpleLuceneQuery($searchTerms)
                    let $luceneOptions  := utillib:getSimpleLuceneOptions()
                    return 
                        $decors[ft:query(project/name,$luceneQuery,$luceneOptions)]
                )
            let $r  := if (empty($repository))                then $r else if ($repository) then $decors[@repository = 'true'] else $decors[not(@repository = 'true')]
            let $r  := if (empty($experimental))              then $r else if ($experimental) then $decors[project[@experimental = 'true']] else $decors[not(project[@experimental = 'true'])]
            let $r  := 
                if (empty($authmap)) then $r[not(@private = 'true')] else (
                    for $decor in $r
                    let $private   := $decor/@private = 'true'
                    let $include   := 
                        if (empty($authmap)) then not($private) else 
                        if (not($private)) then true() else decorlib:isAuthorP($authmap, $decor)
                    return
                        if ($include) then $decor else ()
                )
            let $r  := if (empty($changeddate))               then $r else (
                for $decor in $r
                let $modifiedDate   := xs:date(substring(string(xmldb:last-modified(util:collection-name($decor), util:document-name($decor))), 1, 10))
                return
                    switch ($searchUpcomp)
                    case 'gt' return if (xs:date($modifiedDate) gt xs:date($searchUpdate)) then $decor else ()
                    case 'ge' return if (xs:date($modifiedDate) ge xs:date($searchUpdate)) then $decor else ()
                    case 'lt' return if (xs:date($modifiedDate) lt xs:date($searchUpdate)) then $decor else ()
                    case 'le' return if (xs:date($modifiedDate) le xs:date($searchUpdate)) then $decor else ()
                    default return if (xs:date($modifiedDate) = xs:date($searchUpdate)) then $decor else ()
            )
            
            return $r
        )

    (: handle sorting. somehow reverse() does not do what I expect :)
    (:can sort on indexed db content -- faster:)
    let $results            :=
        switch ($sort)
        case 'id' return 
            if ($sortorder = 'descending') then 
                for $r in $results/project
                order by replace(replace($r/@id, '\.', '.0000000000'), '.0*([0-9]{9,})', '.$1') descending 
                return prapi:createProjectListItem($authmap, $r)
            else (
                for $r in $results/project
                order by replace(replace($r/@id, '\.', '.0000000000'), '.0*([0-9]{9,})', '.$1')
                return prapi:createProjectListItem($authmap, $r)
            )
        default            return 
            if ($sortorder = 'descending') then 
                for $r in $results/project
                let $defaultLanguage    := $r/@defaultLanguage
                order by ($r/name[@language = $userLanguage], $r/name[@language = $defaultLanguage], $r/name)[1] descending 
                return prapi:createProjectListItem($authmap, $r)
            else (
                for $r in $results/project
                let $defaultLanguage    := $r/@defaultLanguage
                order by ($r/name[@language = $userLanguage], $r/name[@language = $defaultLanguage], $r/name)[1]
                return prapi:createProjectListItem($authmap, $r)
            )
    
    let $count              := count($results)
    (:let $max                := if ($max ge 1) then $max else $prapi:maxResults:)
    let $max                := if ($max ge 1) then $max else $count
    
    return
        <list artifact="PROJECT" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements(subsequence($results, 1, $max))
        }
        </list>
        
};

declare %private function prapi:createProjectListItem($authmap as map(*)?, $p as element(project)) as element(project) {
    <project> 
    {
        $p/@id, 
        $p/@prefix,
        $p/@defaultLanguage,
        attribute lastmodified {xmldb:last-modified(util:collection-name($p), util:document-name($p))}, 
        attribute experimental {$p/@experimental = 'true'},
        attribute repository {$p/../@repository = 'true'}, 
        attribute private {$p/../@private = 'true'},
        attribute author {if (empty($authmap)) then false() else decorlib:isActiveAuthorP($authmap, $p/ancestor::decor[1])},
        $p/name
    }
    </project>
};

declare %private function prapi:getDecorInfo($authmap as map(*)?, $projectPrefixOrId as item(), $checkADRAM as xs:boolean) {

    let $decor                  := 
        if ($projectPrefixOrId instance of element(decor)) then $projectPrefixOrId else 
        if (utillib:isOid($projectPrefixOrId)) then utillib:getDecorById($projectPrefixOrId) else utillib:getDecorByPrefix($projectPrefixOrId)
    let $decorProject           := $decor/project
    let $projectId              := $decorProject/@id
    let $projectPrefix          := $decorProject/@prefix

    (:XFORMS extras. This is used as prefix for project logos:)
    let $projectColl            := replace(util:collection-name($decor),'^.*data/','')

    let $ggl                    := try { if ($projectId) then ggapi:getLinkedGovernanceGroups($projectId) else () } catch * {()}
    let $updateUserPrefs        := try { userapi:updateProjectPreference($authmap, $authmap?name, $projectId) } catch * {()}
    
    let $isAuthorOrDba          := if (empty($authmap)) then false() else if (empty($decor)) then false() else decorlib:isActiveAuthorP($authmap, $decor)
    
    let $result                 :=
        if (empty($decor)) then () else (
            <project 
                id="{$projectId}" 
                prefix="{$projectPrefix}" 
                experimental="{$decorProject/@experimental='true'}"
                defaultLanguage="{$decorProject/@defaultLanguage}"
                repository="{$decor/@repository='true'}"
                private="{$decor/@private='true'}"
                lastmodified="{xmldb:last-modified(util:collection-name($decor), util:document-name($decor))}">
            {
                attribute collection {$projectColl}
            }
            {
                $decorProject/name | $decorProject/desc
                ,
                for $copyright in $decorProject/copyright
                return
                    <copyright>
                    {
                        attribute years {$copyright/@years},
                        $copyright/@by[not(. = '')],
                        $copyright/@logo[not(. = '')],
                        $copyright/@type[not(. = '')],
                        for $addr in $copyright/addrLine
                        return 
                            <addrLine>
                            {
                                attribute type {
                                    if ($addr/@type) then $addr/@type else
                                    if ($addr[matches(., 'https?:/')]) then 'uri' else
                                    if ($addr[matches(., 'ftps?:/')]) then 'uri' else
                                    if ($addr[matches(., 'tel:')]) then 'phone' else
                                    if ($addr[matches(., 'fax:')]) then 'fax' else
                                    if ($addr[matches(., 'mailto:')]) then 'email' else
                                    if ($addr[string-length(replace(., '[^\d]', '')) ge 10]) then 'phone' else
                                    if ($addr[matches(., '\S+@[^\s\.]+\.\S+')]) then 'email' else ()
                                },
                                $addr/(@* except @type),
                                $addr/node()
                            }
                            </addrLine>
                    }
                    </copyright>
                
                (:AD30-762 - Replaced by separate privileged service for GDPR reasons:) 
                (:,
                prapi:handleAuthorForAPI($decorProject/author, $isAuthorOrDba):)
            }
            {
                $decorProject/license
            }
            {
                $decorProject/reference
            }
            {
                (:ADRAM extras:)
                <service>
                {
                    attribute type {'adram'},
                    if ($checkADRAM and $decorProject/reference/@url[string-length()>0]) then (
                        prapi:getADRAMStatus($decorProject/reference/@url)/@*
                    ) else (
                        attribute status {'unchecked'}
                    )
                }
                </service>
            }
            {
                (:ADAWIB extras:)
                <service>
                {
                    attribute type {'adawib'},
                    attribute status {'unknown'} (:not yet implemented:)
                }
                </service>
            }
            {
                $decorProject/restURI
                ,
                if ($decorProject[defaultElementNamespace]) then 
                    $decorProject/defaultElementNamespace
                else (
                    <defaultElementNamespace ns="hl7:"/>
                )
            }
            {
                $decorProject/contact,
                for $bbr in $decorProject/buildingBlockRepository
                return
                    <buildingBlockRepository>
                    {
                        $bbr/@*,
                        if ($bbr[@format]) then () else (attribute format {'decor'}),
                        $bbr/node()
                    }
                    </buildingBlockRepository>
                ,
                for $gg in $ggl/partOf/@ref
                return 
                    <group id="{$gg}"/>
                (:for $ggl in $setlib:colDecorData/governance-group-links[@ref = $projectId], $gg in $ggl/partOf/@ref
                return <group id="{$gg}"/>:)
                ,
                utillib:getDecorNamespaces($decor)
            }
                <statistics>
                    <datasets count="{count($decor/datasets/dataset)}" concepts="{count($decor/datasets/dataset//concept)}"/>
                    <scenarios count="{count($decor/scenarios/scenario)}" transactions="{count($decor/scenarios//transaction)}"/>
                    <terminologies count="{count($decor/terminology/(valueSet|codeSystem))}"/>
                    <profiles count="{count($decor/rules/(template|profile))}"/>
                    <issues count="{count($decor/issues/issue)}" open="{count($decor/issues/issue[@statusCode = ('open')])}"/>
                    <releases count="{count($decor/project/release)}"/>
                </statistics>,
                <ids>
                {
                    (:
                    Add empty designation for language, otherwise you cannot edit the designation in the project form. TODO: fix empty designations before/on save 
                    <id root="1.0.639.2">
                        <designation language="nl-NL" type="" displayName="ISO-639-2 Alpha 3" lastTranslated="" mimeType="">ISO-639-2 Alpha 3 Language</designation>
                    </id>
                    :)
                    (:
                        Old style:
                            <baseId id="1.2.3" type="DS" prefix="xyz"/>
                            <defaultBaseId id="1.2.3" type="DS"/>
                        New style:
                            <baseId id="1.2.3" type="DS" prefix="xyz" default="true"/>
                            
                        Rewrite old style to new style.
                    :)
                    for $baseId in $decor/ids/baseId
                    return
                        <baseId>
                        {
                            $baseId/@*[string-length() gt 0]
                            ,
                            if ($baseId[@default]) then () else (
                                attribute default {$decor//defaultBaseId/@id = $baseId/@id}
                            )
                        }
                        </baseId>
                    ,
                    (: For now: keep old style so we can fix all dependent code later :)
                    $decor/ids/defaultBaseId,
                    $decor/ids/id
                }
                </ids>
            {
            (: issues, @notifier is a must-have here :)
            <issues notifier="{($decor/issues/@notifier[. = ('on', 'off')], 'on')[1]}">
            {
                    $decor/issues/(@* except (@notifier)),
                    $decor/issues/labels
            }
            </issues>
            }
            </project>
        )

    return
        if (empty($decor)) then () else (
            element {name($result)} {
                $result/@*,
                namespace {"json"} {"http://www.json.org"},
                utillib:addJsonArrayToElements($result/*)
            }
        )
};

declare %private function prapi:postDecorInfo($authmap as map(*)?, $data as element(project)) {
    let $projectBaseId          := if ($data/@baseId[utillib:isOid(.)]) then $data/@baseId else serverapi:getServerDefaultId('decor-projects')[utillib:isOid(.)] 
    let $projectPrefix          := $data/@prefix[matches(., '^\S+-$')]
    let $projectId              := $data/@id[utillib:isOid(.)]
    let $projectLanguage        := $data/@defaultLanguage[matches(., '^[a-z]{2}-[A-Z]{2}$')]
    let $projectExperimental    := $data/@experimental[. = 'true']
    let $projectStoreParent     := concat($setlib:strDecorData,'/projects')
    let $projectStoreCollection := substring($projectPrefix, 1, string-length($projectPrefix)-1)
    
    let $check                  := 
        if (empty($projectId)) then 
            if (empty($projectBaseId)) then
                error($errors:BAD_REQUEST, 'Your project does not have an id, nor a baseId. Since your server also does not have a default baseId, it is not possible to generate an id for your project')
            else ()
        else if (empty(utillib:getDecorById($projectId))) then () else (
            error($errors:BAD_REQUEST, 'Project with id ' || $projectId || ' already exists. Cannot create.')
        )
    let $check                  := 
        if (empty($projectPrefix)) then 
            error($errors:BAD_REQUEST, 'Project SHALL have a prefix without wihtespace and ending in '-'. Found: ' || $data/@prefix)
        else if (empty(utillib:getDecorByPrefix($projectPrefix))) then () else (
            error($errors:BAD_REQUEST, 'Project with prefix ' || $projectPrefix || ' already exists. Cannot create.')
        )
    let $check                  :=
        if (xmldb:collection-available($projectStoreParent)) then 
            if (xmldb:collection-available(concat($projectStoreParent,'/',$projectStoreCollection))) then
                error($errors:SERVER_ERROR, 'Project collection already exists: ' || $projectStoreCollection)
            else
            if (sm:has-access($projectStoreParent,'rwx')) then () else (
                error($errors:SERVER_ERROR, 'User ' || setlib:strCurrentUserName() || ' does not have write access to package collection: ' || $projectStoreParent || '. Please inform your server admin.')
            )
        else (
            error($errors:SERVER_ERROR, 'Package collection not available: ' || $projectStoreParent || '. Please inform your server admin.')
        )
    let $check                  := 
        if (empty($projectLanguage)) then 
            error($errors:BAD_REQUEST, 'Project SHALL have a defaultLanguage as ll-CC. Found: ' || $data/@defaultLanguage)
        else ()
    let $check                  := 
        if (empty($data/name[@language = $projectLanguage])) then 
            error($errors:BAD_REQUEST, 'Project SHALL have at least a name matching its defaultLanguage ' || $projectLanguage || '. Cannot create.')
        else ()
    (: we cannot check existence of username as we are under api-user privileges :)
    let $check                  := 
        if (empty($data/author[@username])) then 
            error($errors:BAD_REQUEST, 'Project SHALL have at least one author with property username. Cannot create.')
        else ()
    
    let $allDecors              := utillib:getDecorByPrefix('*', (), ())
    let $projectId              :=
        if ($projectId) then $projectId else $projectBaseId || '.' || (max($allDecors/project[matches(@id, concat('^', $projectBaseId, '\.\d$'))]/xs:integer(tokenize(@id, '\.')[last()])), 0)[1] + 1
    
    let $check                  :=
        if (matches($projectId, '^[0-2](\.(0|[1-9][0-9]*)){0,3}$')) then
            error($errors:BAD_REQUEST, 'Project with id ''' || $projectId || ''' is reserved. You cannot reuse a reserved identifier. See http://oid-info.com/get/' || $projectId)
        else ()
        
    let $decor                  := 
        document {
            processing-instruction {'xml-model'} {' href="https://assets.art-decor.org/ADAR/rv/DECOR.sch" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"'},
            processing-instruction {'xml-stylesheet'} {' type="text/xsl" href="https://assets.art-decor.org/ADAR/rv/DECOR2schematron.xsl"'},
            <decor>
            {
                $data/@repository[. = 'true'],
                $data/@private[. = 'true'],
                for $ns at $i in $data/ns[@uri][@prefix]
                let $ns-uri         := $ns/@uri
                let $ns-prefix      := $ns/@prefix
                group by $ns-prefix
                order by lower-case($ns/@prefix)
                return
                    attribute {QName(head($ns-uri), head($ns-prefix) || ':dummy-' || $i)} {$ns-uri}
                ,
                prapi:handleProject($data)
            }
                <datasets>
                    <dataset id="{$projectId}.1.1" effectiveDate="{substring(string(current-dateTime()), 1, 19)}" statusCode="draft">
                        <name language="{$projectLanguage}">??</name>
                        <desc language="{$projectLanguage}">??</desc>
                        <concept id="{$projectId}.2.1.1" effectiveDate="{substring(string(current-dateTime()), 1, 19)}" type="item" statusCode="draft">
                            <name language="{$projectLanguage}">??</name>
                            <desc language="{$projectLanguage}">??</desc>
                            <valueDomain type="string"/>
                        </concept>
                    </dataset>
                </datasets>
                <scenarios>
                    <actors/>
                </scenarios>
                <ids>
                    <baseId id="{$projectId}.1" type="DS" prefix="{$projectPrefix}dataset-"/>
                    <baseId id="{$projectId}.2.1" type="DE" prefix="{$projectPrefix}dataelement-"/>
                    <baseId id="{$projectId}.3" type="SC" prefix="{$projectPrefix}scenario-"/>
                    <baseId id="{$projectId}.4" type="TR" prefix="{$projectPrefix}transaction-"/>
                    <baseId id="{$projectId}.5" type="CS" prefix="{$projectPrefix}codesystem-"/>
                    <baseId id="{$projectId}.6" type="IS" prefix="{$projectPrefix}issue-"/>
                    <baseId id="{$projectId}.7" type="AC" prefix="{$projectPrefix}actor-"/>
                    <baseId id="{$projectId}.8" type="CL" prefix="{$projectPrefix}conceptlist-"/>
                    <baseId id="{$projectId}.9" type="EL" prefix="{$projectPrefix}template-element-"/>
                    <baseId id="{$projectId}.10" type="TM" prefix="{$projectPrefix}template-"/>
                    <baseId id="{$projectId}.11" type="VS" prefix="{$projectPrefix}valueset-"/>
                    <baseId id="{$projectId}.16" type="RL" prefix="{$projectPrefix}rule-intern-"/>
                    <baseId id="{$projectId}.18" type="SX" prefix="{$projectPrefix}test-scenario-"/>
                    <baseId id="{$projectId}.19" type="EX" prefix="{$projectPrefix}example-instance-"/>
                    <baseId id="{$projectId}.20" type="QX" prefix="{$projectPrefix}qualification-test-instance-"/>
                    <baseId id="{$projectId}.21" type="CM" prefix="{$projectPrefix}community-"/>
                    <baseId id="{$projectId}.22" type="TX" prefix="{$projectPrefix}test-dataelement-"/>
                    <baseId id="{$projectId}.24" type="MP" prefix="{$projectPrefix}map-"/>
                    <baseId id="{$projectId}.26" type="SD" prefix="{$projectPrefix}structuredefinition-"/>
                    <baseId id="{$projectId}.27" type="QQ" prefix="{$projectPrefix}questionnaire-"/>
                    <baseId id="{$projectId}.28" type="QR" prefix="{$projectPrefix}questionnaire-response-"/>
                    <baseId id="{$projectId}.29" type="IG" prefix="{$projectPrefix}implementationguide-"/>
                    <defaultBaseId id="{$projectId}.1" type="DS"/>
                    <defaultBaseId id="{$projectId}.2.1" type="DE"/>
                    <defaultBaseId id="{$projectId}.3" type="SC"/>
                    <defaultBaseId id="{$projectId}.4" type="TR"/>
                    <defaultBaseId id="{$projectId}.5" type="CS"/>
                    <defaultBaseId id="{$projectId}.6" type="IS"/>
                    <defaultBaseId id="{$projectId}.7" type="AC"/>
                    <defaultBaseId id="{$projectId}.8" type="CL"/>
                    <defaultBaseId id="{$projectId}.9" type="EL"/>
                    <defaultBaseId id="{$projectId}.10" type="TM"/>
                    <defaultBaseId id="{$projectId}.11" type="VS"/>
                    <defaultBaseId id="{$projectId}.16" type="RL"/>
                    <defaultBaseId id="{$projectId}.18" type="SX"/>
                    <defaultBaseId id="{$projectId}.19" type="EX"/>
                    <defaultBaseId id="{$projectId}.20" type="QX"/>
                    <defaultBaseId id="{$projectId}.21" type="CM"/>
                    <defaultBaseId id="{$projectId}.22" type="TX"/>
                    <defaultBaseId id="{$projectId}.24" type="MP"/>
                    <defaultBaseId id="{$projectId}.26" type="SD"/>
                    <defaultBaseId id="{$projectId}.27" type="QQ"/>
                    <defaultBaseId id="{$projectId}.28" type="QR"/>
                    <defaultBaseId id="{$projectId}.29" type="IG"/>
                </ids>
                <terminology/>
                <rules/>
                <issues/>
            </decor>
        }
    let $initialVersion         :=
        <version date="{substring(string(current-dateTime()), 1, 19)}" by="{$authmap?name}">
        {
            switch (lower-case(tokenize($projectLanguage, '-')[1]))
            case 'nl' return <desc language="{$projectLanguage}">Project aangemaakt</desc>
            case 'de' return <desc language="{$projectLanguage}">Projekt erstellt</desc>
            case 'fr' return <desc language="{$projectLanguage}">Projet créé</desc>
            case 'it' return <desc language="{$projectLanguage}">Progetto creato</desc>
            case 'pl' return <desc language="{$projectLanguage}">Utworzono projekt</desc>
            default return   <desc language="{$projectLanguage}">Project created</desc>
        }   
        </version>
    
    let $projectStoreResource   := concat($projectPrefix,'decor.xml')
    let $projectLogosCollection := concat($projectPrefix,'logos')
    
    let $create-parent          := xmldb:create-collection($projectStoreParent, $projectStoreCollection)
    (:let $parent-chown           := sm:chown($create-parent, 'admin:decor'):)
    let $parent-chmod           := sm:chmod($create-parent, 'rwxrwxr-x')
    
    let $create-logos           := xmldb:create-collection($create-parent, $projectLogosCollection)
    (:let $logos-chown            := sm:chown($create-logos, 'admin:decor'):)
    let $logos-chmod            := sm:chmod($create-logos, 'rwxrwxr-x')
    
    let $create-project         := xmldb:store($create-parent, $projectStoreResource, $decor)
    (:let $project-chown          := sm:chown($create-project, 'admin:decor'):)
    let $project-chmod          := sm:chmod($create-project, 'rw-rw-r--')
    
    let $decorProject           := doc($create-project)/decor
    let $updateid               := update value $decorProject/project/@id with $projectId
    let $insertVersion          := update insert $initialVersion into $decorProject/project
    
    let $governanceGroups       := 
        for $gg in $data/group[@id]
        let $ggid   := $gg/@id
        group by $ggid
        return ggapi:linkGroup($ggid, $decorProject)
    
    return prapi:getDecorInfo($authmap, $decorProject, true())
};

declare %private function prapi:getProjectAuthorList($authmap as map(*), $projectPrefixOrId as item()) {

    let $decor                  := 
        if ($projectPrefixOrId instance of element(decor)) then $projectPrefixOrId else 
        if (utillib:isOid($projectPrefixOrId)) then utillib:getDecorById($projectPrefixOrId) else utillib:getDecorByPrefix($projectPrefixOrId)
    let $decorProject           := $decor/project
    let $projectId              := $decorProject/@id
    let $projectPrefix          := $decorProject/@prefix

    (:let $check                  :=
        if (empty($decor)) then
            error($errors:BAD_REQUEST, 'Project ' || $projectPrefixOrId || ' does not exist')
        else ():)
        
    let $isAuthorOrDba          := if (empty($authmap)) then false() else if (empty($decor)) then false() else decorlib:isActiveAuthorP($authmap, $decor)
    
    (:let $check                  :=
        if ($isAuthorOrDba) then () else (
            error($errors:UNAUTHORIZED, 'You have to be an active admin in the project or a dba.')
        ):)
    let $authors                := if ($isAuthorOrDba) then $decorProject/author else ()
    
    let $current                := count($authors)
    let $total                  := count($authors)
    let $allcnt                 := count($authors)
    
    return
        <list artifact="PROJECTAUTHORS" current="{$current}" total="{$total}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements(
                prapi:handleAuthorForAPI($authors, $isAuthorOrDba)
            )
        }
        </list>
    
};

declare %private function prapi:handleAuthorForAPI($authors as element(author)*, $isAuthorOrDba as xs:boolean) as element(author)* {
    for $author in $authors
    let $active       :=
        if ($author[@expirationDate castable as xs:dateTime][xs:dateTime(@expirationDate) le current-dateTime()]) then false() else (
            not($author/@active = 'false')
        )
    let $dbactive     :=
        try {
            if (sm:user-exists($author/@username)) then sm:is-account-enabled($author/@username) else false()
        } 
        catch * {true()}
    let $projectadmin :=
        try {
            if (sm:user-exists($author/@username)) then decorlib:isProjectAdminP(map { "name": $author/@username, "groups": sm:get-user-groups($author/@username) }, $author) else false()
        } 
        catch * {true()}
    return
        if ($isAuthorOrDba or ($active and $dbactive)) then 
            <author>
            {
                $author/@id
                ,
                (: mind the GDPR law, see https://art-decor.atlassian.net/browse/AD30-762 :)
                if ($isAuthorOrDba) then (
                    $author/@username,
                    $author/@email,
                    attribute notifier {if ($author/@notifier = ('on', 'true')) then 'on' else 'off'}
                    ,
                    (: is the user active as author? :)
                    attribute active {$active},
                    (: does the user have an active eXist-db account? :)
                    attribute dbactive {$dbactive},
                    (: can the user edit the project section :)
                    attribute projectadmin {$projectadmin}
                    ,
                    $author/(@* except (@id|@username|@email|@active|@dbactive|@notifier))
                ) else ()
                ,
                normalize-space(string-join($author/text(), ' ')) 
            }
            </author>
        else ()
};

declare %private function prapi:getDecorProjectHistory($authmap as map(*)?, $projectPrefixOrId as xs:string, $max as xs:integer) as element(project)? {
    let $decor                  := decorlib:getDecorProject($projectPrefixOrId)
    let $projectId              := $decor/project/@id
    let $projectPrefix          := $decor/project/@prefix
    let $defaultLanguage        := $decor/project/@defaultLanguage
    let $limit                  := if (empty($max)) then 50 else $max
    
    (: where are the histories for this project located :)
    let $where                  := concat($setlib:strDecorHistory, '/', substring($projectPrefix, 1, string-length($projectPrefix) - 1))

    return
        if (empty($decor)) then () else (
            let $collhistories          := collection($where)/histories
         
            (: hush through all history records found and add the artefactType to each item :)
            let $histories              := hist:ListHistory($authmap, (), $projectPrefix, (), (), $limit)
            let $historyCount           := count($histories)
            let $totalCount             := count($collhistories/history)
            
            return
            <project id="{$projectId}" prefix="{$projectPrefix}" defaultLanguage="{$defaultLanguage}" current="{if ($limit > $historyCount) then $historyCount else $limit}" total="{$totalCount}" xmlns:json="http://www.json.org">
            {
                utillib:addJsonArrayToElements($histories)
            }
            </project>
        )
};

declare %private function prapi:getADRAMStatus($decorReferenceURL as xs:string?) as element() {
    let $requeststatus      := 
        <http:request method="GET" href="{xs:anyURI($decorReferenceURL)}" status-only="true" follow-redirect="true" timeout="5">
            <http:header name="Content-Type" value="text/xml"/>
            <http:header name="Cache-Control" value="no-cache"/>
            <http:header name="Max-Forwards" value="1"/>
        </http:request>
    let $requestadram       := 
        <http:request method="GET" href="{xs:anyURI(concat($decorReferenceURL, '/adram.config.xml'))}" status-only="false" follow-redirect="true" timeout="5">
            <http:header name="Content-Type" value="text/xml"/>
            <http:header name="Cache-Control" value="no-cache"/>
            <http:header name="Max-Forwards" value="2"/>
        </http:request>
    (: check availablility of reference/@url :)
    let $OK                 := '200'
    let $notfound           := '404'
    let $unavailable        := '400'
    let $longtimeago        := '1900-01-01T00:00:00'
    (: first check whether the publication location is reachable :)
    let $servicestatus1     :=
        if (string-length($decorReferenceURL) = 0) then 
            <result status="{$unavailable}" message="no publication location set"/>
        else (
            (: get headers -:)
            (:
                <hc:response xmlns:hc="http://expath.org/ns/http-client" status="200" message="OK" spent-millis="314">
                    <hc:header name="server" value="nginx"/>
                    <hc:header name="date" value="Tue, 08 Oct 2019 06:30:21 GMT"/>
                    <hc:header name="content-type" value="text/xml"/>
                    <hc:header name="transfer-encoding" value="chunked"/>
                    <hc:header name="connection" value="keep-alive"/>
                    <hc:header name="x-xquery-cached" value="true"/>
                    <hc:body media-type="text/xml"/>
                </hc:response>
                [body]
            :)
            try {
                let $pubDir         := http:send-request($requeststatus)
                let $statusCode     := $pubDir[1]/@status
                return 
                    if ($statusCode=$OK) then <result status="{$OK}"/> else <result status="{$notfound}" http="{$statusCode}"/>
            }
            catch * { 
                <result status="{$unavailable}" message="{concat('code: ', $err:code, ' / description: ', $err:description, ' / value: ', $err:value)}"/>
            }
        )
        (: if publication location is reachable check adram sttaus file :)
        let $servicestatus2 :=
            if ($servicestatus1/@status = $OK) then (
                try {
                    let $adramConfig    := http:send-request($requestadram)
                    let $adramStatus    := $adramConfig[1]/@status
                    (:let $adramHeader := exists($response1/httpclient:headers/httpclient:header[@name='X-Powered-By'][@value='ART-DECOR-ADRAM']):)
                    (: check wheter adram is configured there, if so get the @touch attribute to find out when the cron job last touched the config :) 
                    let $response2      := 
                        if ($adramStatus=$OK) then (
                            let $adramLastRun := 
                                if (string-length($adramConfig//@touched)>0) 
                                then $adramConfig//@touched 
                                else $longtimeago
                            let $timediff     := days-from-duration(current-dateTime() - xs:dateTime($adramLastRun))
                            
                            (: if last touch is more than 2 days ago assume halted :)
                            return if ($timediff <= 2) then "adram" else "halted"
                        )
                        else (
                            (: if adram.config.xml does not exist then assume 'not configured' :)
                        )
                    return 
                        if ($adramStatus=$OK) then <result status="{$OK}" step="{$response2}"/> else <result status="{$unavailable}" http="{$adramStatus}"/>
                }
                catch * { 
                    <result status="{$unavailable}" message="{concat('code: ', $err:code, ' / description: ', $err:description, ' / value: ', $err:value)}"/>
                }
            ) 
            else (
                <result status="{$unavailable}"/>
            )
        
        let $servicestatus :=
            if ($servicestatus1/@status != $OK) then 
                <result status="{$unavailable}" message="{$servicestatus1/@message}" http="{$servicestatus1/@http}" step="contacting publication location"/>
            else 
            if ($servicestatus2/@status != $OK) then 
                <result status="{$unavailable}" message="{$servicestatus2/@message}" http="{$servicestatus1/@http}" step="contacting adram status at publication location failed"/>
            else (
                <result status="{$servicestatus2/@status}" message="{$servicestatus2/@message}" step="{$servicestatus2/@step}"/>
            )
            
    return $servicestatus
};

(:~ Central logic for updating an existing project properties

@param $authmap         - required. Map derived from token
@param $id              - required. DECOR issue/@id to update
@param $request-body    - required. DECOR xml element parameter elements each containing RFC 6902 compliant contents
@return project object as xml with json:array set on elements
:)
declare function prapi:patchProject($authmap as map(*), $projectPrefixOrId as xs:string, $request-body as element(parameters), $checkADRAM as xs:boolean) as element(project) {
    let $decor                  := 
        if (count($projectPrefixOrId[not(. = '')]) = 1) then
            if (utillib:isOid($projectPrefixOrId)) then utillib:getDecorById($projectPrefixOrId) else utillib:getDecorByPrefix($projectPrefixOrId)
        else (
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        )
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $projectPrefixOrId, ''', does not exist.'))
        )
    let $projectPrefix          := $decor/project/@prefix
    let $projectAuthor          := $decor/project/author[@username = $authmap?name]
    let $now                    := substring(string(current-dateTime()), 1, 19)
    (: a full editor can edit all project aspects. he is dba or decor-admin :)
    let $fullEditor             := if (empty($authmap)) then false() else decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-PROJECT)
    (: a limited editor can edit his own author record to some extent. he is a regular (issue) editor :)
    let $limitedEditor          := decorlib:isActiveAuthorP($authmap, $decor)
    
    (: Start checking :)
    let $check                  :=
        if ($fullEditor) then () else 
        if ($limitedEditor) then
            if ($request-body/parameter[not(@path = '/author')] | $request-body/parameter[@path = '/author']/value/author[not(@username = $authmap?name)]) then
                error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify other properties than his own details (email, notifier setting and full name) in the project ', $projectPrefix, '.'))
            else ()
        else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify the project ', $projectPrefix, '. You have to be a DBA or an active admin in the project.'))
        )
    let $check                  :=
        if ($request-body[parameter]) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall be an array of ''parameter''.')
        )
    let $unsupportedops         := $request-body/parameter[not(@op = ('add', 'replace', 'remove'))]
    let $check                  :=
        if ($unsupportedops) then
            error($errors:BAD_REQUEST, 'Submitted parameters shall have supported ''op'' value. Found ''' || string-join(distinct-values($unsupportedops/@op), ''', ''') || ''', expected ''add'', ''replace'', or ''remove''') 
        else ()
    let $checkn                 :=
        if (count($decor/project/name) - count($request-body/parameter[@op = 'remove'][@path = '/name']) + count($request-body/parameter[@op = 'add'][@path = '/name']) ge 1) then () else (
            'A project SHALL have at least one name. You cannot remove every name.'
        )
    let $checkc                 :=
        if (count($decor/project/copyright) = 0) then () else 
        if (count($decor/project/copyright) - count($request-body/parameter[@op = 'remove'][@path = '/copyright']) + count($request-body/parameter[@op = ('add', 'replace')][@path = '/copyright']) ge 1) then () else (
            'A project SHALL have at least one copyright. You cannot remove all copyright.'
        )
    let $checkd                 :=
        if (count($decor/project/defaultElementNamespace) - count($request-body/parameter[@op = 'remove'][@path = '/defaultElementNamespace']) + count($request-body/parameter[@op = 'add'][@path = '/defaultElementNamespace']) le 1) then () else (
            'A project SHALL NOT have more than one defaultElementNamespace.'
        )
    let $check                  :=
        for $param in $request-body/parameter
        let $op := $param/@op
        return
            if ($param[count(value) gt 1]) then
                'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Multiple value nodes are not supported. Please separate each into its own parameter.'
            else
            if ($param/value[count(*) gt 1]) then
                'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Multiple nodes in value are not supported. Please separate each into its own parameter.'
            else
            if ($param[@path = '/private']) then
                if ($decor[@repository='true'] and ($param[@value = 'true'])) then
                   'Parameter ' || $param/@op || ' of path ' || $param/@path || ' with value ''' || $param/@value || ''' not supported. You cannot make a repository private'
                else
                if ($param[@op = 'remove'][empty(@value)]) then () else if ($param[@value = ('true', 'false')]) then () else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Value SHALL be a boolean. Found: ''' || $param/@value || ''''
                )
            else
            if ($param[@path = '/experimental']) then
                if ($decor[@repository='true'] and ($param[@value = 'true'])) then
                   'Parameter ' || $param/@op || ' of path ' || $param/@path || ' with value ''' || $param/@value || ''' not supported. You cannot make an experimental project of a repository'
                else
                if ($param[@value = ('true', 'false')]) then () else if ($param[@op = 'remove'][empty(@value)]) then () else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Value SHALL be a boolean. Found: ''' || $param/@value || ''''
                )
            else
            if ($param[@path = '/notifier']) then
                if ($param[@value = ('true', 'false', 'on', 'off')]) then () else if ($param[@op = 'remove'][empty(@value)]) then () else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Value SHALL be a boolean or ''on'' or ''off''. Found: ''' || $param/@value || ''''
                )
            else
            (: Principally we could have 1 request-body that switches repository on and off again. We don't check 
                for that because there is no consequence for other projects in that fraction of a second :)
            if ($param[@path = ('/repository')]) then
                if ($decor[project/@experimental='true'] and ($param[@value = 'true'])) then
                   'Parameter ' || $param/@op || ' of path ' || $param/@path || ' with value ''' || $param/@value || ''' not supported. You cannot make a repository of an experimental project'
                else
                if ($decor[@private='true'] and $param[@value = 'true']) then
                   'Parameter ' || $param/@op || ' of path ' || $param/@path || ' with value ''' || $param/@value || ''' not supported. You cannot make a repository of a private project'
                else
                if ($decor[@repository='true'] and ($param[@op = 'remove'] | $param[@value = 'false'])) then
                   'Parameter ' || $param/@op || ' of path ' || $param/@path || ' with value ''' || $param/@value || ''' not supported. It is not possible to undo this setting once it is active'
                else
                if ($param[@op = 'remove'][empty(@value)]) then () else if ($param[@value = ('true', 'false')]) then () else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Value SHALL be a boolean. Found: ''' || $param/@value || ''''
                )
            else
            if ($param[@path = '/defaultLanguage']) then
                if ($op = 'remove') then 
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported.'
                else
                if ($op = 'add' and $decor/project/@defaultLanguage) then
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' already exists and there can only be one.'
                else
                if ($decor/project/name[@language = $param/@value]) then () else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A project defaultLanguage SHALL be one of the languages the project supports. Supported: ''' || string-join($decor/project/name/@language, ''', ''') || '''. Found: ''' || $param/@value || '''' 
                )
            else
            if ($param[@path = '/defaultElementNamespace']) then
                if ($op = 'remove') then 
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported.'
                else
                if ($op = 'add' and $decor/project/defaultElementNamespace) then
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' already exists and there can only be one.'
                else
                if ($param[count(value/defaultElementNamespace) = 1]) then
                    if ($param/value/defaultElementNamespace[matches(@ns, '^[^\s:\.\d\-][^\s:]*:$')]) then () else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. This path SHALL have a value.defaultElementNamespace.ns as valid xml namespace prefix ending in a colon (:). Found: ''' || $param/@value || '''' 
                    )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one defaultElementNamespace under value. Found ' || count($param/value/defaultElementNamespace) 
                )
            else
            if ($param[@path = '/reference']) then
                if ($op = 'remove') then 
                    ()
                else
                if ($op = 'add' and $decor/project/reference) then
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' already exists and there can only be one.'
                else
                if ($param[count(value/reference) = 1]) then
                    if ($param/value/reference[@url[not(. = '')][. castable as xs:anyURI] | @logo[not(. = '')][. castable as xs:anyURI]]) then
                        if ($param/value/reference[@logo | logo]) then prapi:checkLogoConfig($op, $param/@path, $param/value/reference, $decor/project/reference) else ()
                    else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' SHALL have value.reference with either a non-empty url (full url) or a non-empty logo'
                    )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one reference under value. Found ' || count($param/value/reference) 
                )
            else
            if ($param[@path = '/name']) then (
                if ($param[count(value/name) = 1]) then
                    if ($param/value/name[matches(@language, '[a-z]{2}-[A-Z]{2}')]) then 
                        if ($param[@op = 'remove'] | $param/value/name[.//text()[not(normalize-space() = '')]]) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A project name SHALL have contents.'
                        )
                    else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A project name SHALL have a language with pattern ll-CC.'
                    )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one name under value. Found ' || count($param/value/desc) 
                )
            )
            else
            if ($param[@path = '/desc']) then (
                if ($param[count(value/desc) = 1]) then
                    if ($param/value/desc[matches(@language, '[a-z]{2}-[A-Z]{2}')]) then 
                        if ($param[@op = 'remove'] | $param/value/desc[.//text()[not(normalize-space() = '')]]) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A project desc SHALL have contents.'
                        )
                    else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A project desc SHALL have a language with pattern ll-CC.'
                    )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one desc under value. Found ' || count($param/value/desc) 
                )
            )
            else
            (:    <copyright years="2021-" by="LogoTest" author="author">
                    <logo name="LogoTest.png" size="38220" type="image/jpeg" base64="data:image/jpeg;base64,/blabla..."/>
                  </copyright>
            :)
            if ($param[@path = '/copyright']) then (
                if ($param[count(value/copyright) = 1]) then (
                    if ($param/value/copyright[@years]) then () else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A project copyright SHALL have at least years.'
                    ),
                    if ($param/value/copyright[@by]) then () else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A project copyright SHALL have at least by.'
                    ),
                    if ($param/value/copyright[@logo | logo]) then prapi:checkLogoConfig($op, $param/@path, $param/value/copyright, $decor/project/copyright[@by = $param/value/copyright/@by]) else ()
                    ,
                    if ($param/value/copyright[empty(@type)] | $param/value/copyright[@type = $prapi:PROJECT-COPYRIGHT-TYPE/enumeration/@value]) then () else (
                        'Parameter value ''' || $param/value/copyright/@type || ''' for copyright/type not supported. Supported are: ' || string-join($prapi:PROJECT-COPYRIGHT-TYPE/enumeration/@value, ' ')
                    )
                )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one copyright under value. Found ' || count($param/value/copyright)
                )
            )
            else
            if ($param[@path = '/author']) then (
                if ($param[count(value/author) = 1]) then (
                    if ($param[@op = 'add']/value/author[empty(@id)] | $param[not(@op = 'add')]/value/author[matches(@id, '^\d+$')]) then () else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A project author id SHALL be numeric if present. It SHALL NOT be present on add'
                    ),
                    if ($param/value/author[empty(@username)]) then
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A project author username SHALL be present'
                    else
                    if ($decor/project/author[@username = $param/value/author/@username]) then ()
                    else (
                        try {if (sm:user-exists($param/value/author/@username)) then () else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Author username ' || $param/value/author/@username || ' SHALL be a valid database user'
                        )}
                        catch * {()}
                    )
                )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one author under value. Found ' || count($param/value/author)
                )
            )
            else
            if ($param[@path = '/restURI']) then (
                if ($param[count(value/restURI) = 1]) then (
                    if ($param/value/restURI[empty(@for)] | $param/value/restURI[@for = $prapi:PROJECT-RESTURI-FOR/enumeration/@value]) then () else (
                        'Parameter value ''' || $param/value/restURI/@for || ''' for restURI/for not supported. Supported are: ' || string-join($prapi:PROJECT-RESTURI-FOR/enumeration/@value, ' ')
                    ),
                    if ($param/value/restURI[empty(@format)] | $param/value/restURI[@format = $prapi:PROJECT-RESTURI-FORMAT/enumeration/@value]) then () else (
                        'Parameter value ''' || $param/value/restURI/@format || ''' for restURI/format not supported. Supported are: ' || string-join($prapi:PROJECT-RESTURI-FORMAT/enumeration/@value, ' ')
                    )
                )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one restURI under value.'
                )
            )
            else
            if ($param[@path = '/buildingBlockRepository']) then (
                if ($param[count(value/buildingBlockRepository) = 1]) then (
                    if ($param/value/buildingBlockRepository[@format = $prapi:PROJECT-BBR-FORMAT/enumeration/@value]) then () else (
                        'Parameter value ''' || $param/value/buildingBlockRepository/@format || ''' for buildingBlockRepository/format not supported. Supported are: ' || string-join($prapi:PROJECT-BBR-FORMAT/enumeration/@value, ' ')
                    )
                )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one buildingBlockRepository under value. Found ' || count($param/value/buildingBlockRepository)
                )
            )
            else
            if ($param[@path = ('/ids/baseId', '/ids/defaultBaseId')]) then (
                let $targetElement    := tokenize($param/@path, '/')[last()]
                return
                if ($param[@op = 'remove']) then
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Once a baseId is set you can update the label or add a different default, but you may never delete it.'
                else (
                    if ($param[count(value/*[name() = $targetElement]) = 1]) then (
                        if ($param/value/*[utillib:isOid(@id)]) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A ' || $targetElement || ' SHALL have an non-empty id of type OID. Found: ''' || string-join($param/value/*/@id, ''', ''') || '''.'
                        ),
                        if ($param[@path = '/ids/baseId']) then
                            if ($param/value/baseId[@prefix[not(. = '')]]) then () else (
                                'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A ' || $targetElement || ' SHALL have a non-empty prefix.'
                            )
                        else (),
                        if ($param/value/*[@type = $prapi:DECOR-OBJECT-TYPES/enumeration/@value]) then () else (
                            'Parameter value ''' || $param/value/*/@type || ''' for ' || ($param/value/name(*))[1] || '/type not supported. Supported are: ' || string-join($prapi:DECOR-OBJECT-TYPES/enumeration/@value, ' ')
                        ),
                        if ($param/value/*[@default = 'false'] and ($decor/ids/defaultBaseId[@id = $param/value/*/@id] | $decor/ids/baseId[@default='true'][@id = $param/value/*/@id])) then
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. To unset the current id as default id, you have to supply a new default id which unsets the current default.'
                        else (),
                        if ($decor/ids/baseId[@id = $param/value/*/@id][not(@type = $param/value/*/@type)] | $decor/ids/defaultBaseId[@id = $param/value/*/@id][not(@type = $param/value/*/@type)]) then
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. You cannot reassign an id to a different type. A ' || $targetElement || ' can only be used for one type.'
                        else ()
                    )
                    else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one ' || $targetElement || ' under value.'
                    )
                )
            ) else
            if ($param[@path = '/group']) then (
                if ($param[@value]) then
                    if ($param[@op = 'remove']) then () else if ($param[@value = ggapi:getHostedGovernanceGroups()/group/@id]) then () else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A governance group with id ''' || $param/@value || ''' SHALL exist.'
                    )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have a value as string containing the governance group id.'
                )
            )
            else
            if ($param[@path = '/ns']) then (
                if ($param[count(value/ns) = 1]) then (
                    if ($param/value/ns[@uri][@prefix]) then (
                        (: protected prefixes :)
                        if ($param/value/ns[not(@uri = 'http://www.w3.org/XML/1998/namespace')][@prefix = 'xml']) then
                            'Parameter value for ''' || $param/@path || ''' and protected prefix ''' || $param/value/ns/@prefix || ''' SHALL be http://www.w3.org/XML/1998/namespace'
                        else
                        if ($param/value/ns[not(@uri = 'http://www.w3.org/2001/XMLSchema-instance')][@prefix = 'xsi']) then
                            'Parameter value for ''' || $param/@path || ''' and protected prefix ''' || $param/value/ns/@prefix || ''' SHALL be http://www.w3.org/2001/XMLSchema-instance'
                        else
                        if ($param/value/ns[not(@uri = 'urn:hl7-org:v3')][@prefix = ('hl7', 'cda')]) then
                            'Parameter value for ''' || $param/@path || ''' and protected prefix ''' || $param/value/ns/@prefix || ''' SHALL be urn:hl7-org:v3'
                        else
                        if ($param/value/ns[not(@uri = 'urn:hl7-org:sdtc')][@prefix = 'sdtc']) then
                            'Parameter value for ''' || $param/@path || ''' and protected prefix ''' || $param/value/ns/@prefix || ''' SHALL be urn:hl7-org:sdtc'
                        else ()
                    ) else (
                        'Parameter value for ''' || $param/@path || ''' SHALL have both uri and prefix.'
                    )
                )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one ns under value.'
                )
            )
            else
            if ($param[@path = '/license']) then (
                if ($param[count(value/license) = 1]) then (
                    (: technically @displayName is required as well but we can calculate that from the code :)
                    if ($param/value/license[@code]) then () else (
                        'Parameter value for ''' || $param/@path || ''' SHALL have a code.'
                    ),
                    if ($param/value/license[@code = $prapi:LICENSE-TYPES/enumeration/@value]) then () else (
                        'Parameter value ''' || $param/@path || ''' code ''' || $param/value/license/@code || ''' SHALL be one of ' || string-join($prapi:LICENSE-TYPES/enumeration/@value, ', ')
                    )
                )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one license under value.'
                )
            )
            else
            if ($param[@path = '/ids/id']) then (
                if ($param[count(value/id) = 1]) then (
                    if ($param/value/id[utillib:isOid(@root)]) then () else (
                        'Parameter ' || $param/@op || ' of path ''' || $param/@path || ''' not supported. An id SHALL have at least a root containing a valid OID.'
                    ),
                    if ($param[not(@op = 'remove')]) then () else (
                        if (count($param/value/id/designation) gt 1) then () else (
                            'Parameter ' || $param/@op || ' of path ''' || $param/@path || ''' not supported. An id SHALL have at least one designation.'
                        ),
                        if (count($param/value/id/designation[empty(@displayName)]) = 0) then () else (
                            'Parameter value ''' || $param/@path || ''': each id.designation SHALL have at least a displayName.'
                        ),
                        if (count($param/value/id/designation[not(@type = $prapi:DESIGNATION-TYPES/enumeration/@value)]) = 0) then () else (
                            'Parameter value ''' || $param/@path || ''': each id.designation type SHALL be one of ' || string-join($prapi:DESIGNATION-TYPES/enumeration/@value, ', ')
                        ),
                        if (count($param/value/id/property[empty(@name)]) = 0) then () else (
                            'Parameter value ''' || $param/@path || ''': each id.property SHALL have a name.'
                        )
                    )
                )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one id under value.'
                )
            )
            else (
                'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported.'
            )
     let $check                 :=
        if (empty($check) and empty($checkn) and empty($checkc) and empty($checkd)) then () else (
            error($errors:BAD_REQUEST,  string-join (($check, $checkn, $checkc, $checkd), ' '))
        )
    (: End checking :)
    (: Start applying :)
    let $update                 :=
        for $param in $request-body/parameter
        return
            switch ($param/@path)
            case '/repository' return decorlib:setIsRepositoryP($authmap, $decor, $param/@value = 'true')
            case '/private' return decorlib:setIsPrivateP($authmap, $decor, $param/@value = 'true')
            case '/experimental' return decorlib:setIsExperimentalP($authmap, $decor, $param/@value = 'true')
            case '/notifier' return (
                let $new    := if ($param/@value = ('true', 'on')) then 'on' else 'off'
                
                return
                if ($decor/issues) then
                    if ($decor/issues[@notifier]) then 
                        update value $decor/issues/@notifier with $new
                    else (
                        update insert attribute notifier {$new} into $decor/issues
                    )
                else (
                    update insert <issues notifier="{$new}"/> into $decor
                )
            )
            case '/defaultLanguage' return decorlib:setDefaultLanguageP($authmap, $decor, data(($param/@value, 'en-US')[1]))
            (:<xs:element name="name" type="BusinessNameWithLanguage" minOccurs="1" maxOccurs="unbounded"/>:)
            case '/name' (: fall through :)
            (:<xs:element name="desc" type="FreeFormMarkupWithLanguage" minOccurs="0" maxOccurs="unbounded"/>:)
            case '/desc' return (
                let $elmname  := substring-after($param/@path, '/')
                let $new      := utillib:prepareFreeFormMarkupWithLanguageForUpdate($param/value/*)
                let $stored   := $decor/project/*[name() = $elmname][@language = $param/value/*/@language]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $decor/project
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            (:<xs:element ref="copyright" minOccurs="1" maxOccurs="unbounded"/>:)
            case '/copyright' return (
                let $elmname  := substring-after($param/@path, '/')
                let $by       := $param/value/copyright/@by
                let $logotype := $param/value/copyright/logo/@type
                let $logoname := $param/value/copyright/logo/@name
                let $logodata := $param/value/copyright/logo/@base64
                let $logodata := if (starts-with($logodata, 'data:')) then substring-after($logodata, 'base64,') else $logodata
                
                let $new      := prapi:handleProjectCopyright($param/value/copyright)
                let $stored   := $decor/project/copyright[@by = $by]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return (
                    if ($stored) then update replace $stored with $new else update insert $new into $decor/project,
                    prapi:storeProjectCopyrightLogo($decor, $logotype, $logoname, xs:base64Binary($logodata))
                )
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            (:<xs:element ref="author" minOccurs="0" maxOccurs="unbounded"/>:)
            case '/author' return (
                let $elmname  := substring-after($param/@path, '/')
                let $id       := $param/value/author/@id
                let $usr      := $param/value/author/@username
                let $new      :=
                    if ($fullEditor) then
                        $param/value/author
                    else (
                        <author>{$param/value/author/@email, $param/value/author/@notifier, $projectAuthor/(@* except (@email | @notifier)), normalize-space(data($param/value/author))}</author>
                    )
                let $new      := prapi:handleProjectAuthor($new, $decor/project, true())
                (: 'guest' may have multiple entries with different ids :)
                let $stored   := 
                    if ($fullEditor) then
                        $decor/project/author[@id = $id]
                    else (
                        $projectAuthor
                    )
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $decor/project
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            (:<xs:element ref="reference" minOccurs="0" maxOccurs="1"/>:)
            case '/reference' return (
                let $elmname  := substring-after($param/@path, '/')
                let $logotype := $param/value/copyright/logo/@type
                let $logoname := $param/value/copyright/logo/@name
                let $logodata := $param/value/copyright/logo/@base64
                let $logodata := if (starts-with($logodata, 'data:')) then substring-after($logodata, 'base64,') else $logodata
                
                let $new      := prapi:handleProjectReference($param/value/reference)
                let $stored   := $decor/project/reference
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return (
                    if ($stored) then update replace $stored with $new else update insert $new into $decor/project,
                    prapi:storeProjectCopyrightLogo($decor, $logotype, $logoname, xs:base64Binary($logodata))
                )
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            (:<xs:element ref="license" minOccurs="0" maxOccurs="1"/>:)
            case '/license' return (
                let $elmname  := substring-after($param/@path, '/')
                let $new      := prapi:handleProjectLicense($param/value/license)
                let $stored   := $decor/project/license
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $decor/project
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            (:<xs:element ref="restURI" minOccurs="0" maxOccurs="unbounded"/>:)
            case '/restURI' return (
                let $elmname  := substring-after($param/@path, '/')
                let $uri      := $param/value/restURI
                let $new      := prapi:handleProjectRestURI($param/value/restURI)
                let $stored   := $decor/project/restURI[. = $uri][@for = $uri/@for][@format = $uri/@format]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $decor/project
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            (:<xs:element ref="defaultElementNamespace" minOccurs="0" maxOccurs="1"/>:)
            case '/defaultElementNamespace' return (
                let $elmname  := substring-after($param/@path, '/')
                let $new      := prapi:handleProjectDefaultElementNamespace($param/value/defaultElementNamespace)
                let $stored   := $decor/project/defaultElementNamespace
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $decor/project
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            (:<xs:element ref="contact" minOccurs="0" maxOccurs="unbounded"/>:)
            case '/contact' return (
                let $elmname  := substring-after($param/@path, '/')
                let $uri      := $param/value/contact/@email
                let $new      := prapi:handleProjectContact($param/value/contact)
                let $stored   := $decor/project/contact[@email = $uri]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $decor/project
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            (:<xs:element ref="buildingBlockRepository" minOccurs="0" maxOccurs="unbounded"/>:)
            case '/buildingBlockRepository' return (
                let $elmname  := substring-after($param/@path, '/')
                let $uri      := $param/value/buildingBlockRepository/@url
                let $new      := prapi:handleProjectBBR($param/value/buildingBlockRepository)
                let $stored   := $decor/project/buildingBlockRepository[@url = $uri]
                let $stored   := if ($param/value/buildingBlockRepository[@ident]) then $stored[@ident = $param/value/buildingBlockRepository/@ident] else $stored[empty(@ident)]
                let $stored   := if ($param/value/buildingBlockRepository[@licenseKey]) then $stored[@licenseKey = $param/value/buildingBlockRepository/@licenseKey] else $stored[empty(@licenseKey)]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $decor/project
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            (:<baseId id="2.16.840.1.113883.3.1937.99.62.3.1" type="DS" prefix="demo1-"/>
              <defaultBaseId id="2.16.840.1.113883.3.1937.99.62.3.1" type="DS"/>:)
            case '/ids/baseId' return (
                let $new                    := prapi:handleProjectBaseId($param/value/baseId)
                let $storedBaseIds          := $decor/ids/baseId[@type=$new/@type]
                let $storedBaseId           := $storedBaseIds[@id = $new/@id]
                let $storedDefaultBaseId    := $decor/ids/defaultBaseId[@type=$new/@type]
                let $newdfltbaseid          := <defaultBaseId id="{$new/@id}" type="{$new/@type}"/>
                
                return
                    switch ($param/@op)
                    case ('add') (: fall through :)
                    case ('replace') return (
                        if ($new/@default = 'true') then (
                            update value $storedBaseIds/@default with 'false',
                            if ($storedDefaultBaseId) then 
                                update value $storedDefaultBaseId/@id with $new/@id
                            else (
                                update insert $newdfltbaseid following $decor/ids/defaultBaseId[last()]
                            )
                        ) else ()
                        ,
                        if ($storedBaseId) then 
                            update replace $storedBaseId with $new
                        else
                            update insert $new following $decor/ids/baseId[last()]
                    )
                    case ('remove') return update delete $storedBaseId
                    default return ( (: unknown op :) )
            )
            case '/ids/defaultBaseId' return (
                let $new                    := prapi:handleProjectDefaultBaseId($param/value/defaultBaseId)
                let $storedBaseIds          := $decor/ids/baseId[@type=$new/@type]
                let $storedBaseId           := $storedBaseIds[@id = $new/@id]
                let $storedDefaultBaseId    := $decor/ids/defaultBaseId[@type=$new/@type]
                let $newbaseid              :=
                    <baseId id="{$new/@id}" type="{$new/@type}" prefix="{concat($decor/project/@prefix, $new/@type, '-')}">
                    {
                        if ($storedBaseIds[@default]) then attribute {'default'} {'true'} else ()
                    }
                    </baseId>
                
                return
                    switch ($param/@op)
                    case ('add') (: fall through :)
                    case ('replace') return (
                        update value $storedBaseIds/@default with 'false',
                        (: mark/add baseId for this defaultBaseId :)
                        if ($storedBaseId) then
                            update value $storedBaseId/@default with 'true'
                        else (
                            update insert $newbaseid following $decor/ids/baseId[last()]
                        ),
                        if ($storedDefaultBaseId) then 
                            update replace $storedDefaultBaseId with $new
                        else
                            update insert $new following $decor/ids/defaultBaseId[last()]
                    )
                    case ('remove') return update delete $storedDefaultBaseId
                    default return ( (: unknown op :) )
            )
            case '/group' return (
                let $new                    := $param/@value
                (:let $stored                 := ggapi:getLinkedGovernanceGroups($decor/project/@id)/partOf/@ref[. = $new]:)
                
                return
                    switch ($param/@op)
                    case ('add') (: fall through :)
                    case ('replace') return ggapi:linkGroup($new, $decor)
                    case ('remove') return ggapi:unlinkGroup($new, $decor/project/@id)
                    default return ( (: unknown op :) )
            )
            case '/ids/id' return (
                let $new                    := prapi:handleProjectId($param/value/id)
                let $stored                 := $decor/ids/id[@root = $new/@root]
                
                return
                    switch ($param/@op)
                    case ('add') (: fall through :)
                    case ('replace') return (
                        if ($stored) then
                            update replace $stored with $new
                        else
                        if ($decor/ids/identifierAssociation) then
                            update insert $new preceding $decor/ids/identifierAssociation[1]
                        else
                        if ($decor/ids) then
                            update insert $new into $decor/ids
                        else
                            update insert <ids>{$new}</ids> following ($decor/project | $decor/datasets | $decor/scenarios)[last()]
                    )
                    case ('remove') return update delete $stored
                    default return ( (: unknown op :) )
            )
            default return ( (: unknown path :) )
    
    (: after all the updates, the XML Schema order is likely off. Restore order :)
    let $update                 := update replace $decor/project with prapi:handleProject($decor/project)
    
    (: final step: namespaces :)
    (:suppose we have ns elements in the project data, then slap these onto the decor element overwriting any existing declarations:)
    (:<ns uri="..." prefix="..." default="..."/>:)
    let $ns-update              := 
        if ($request-body/parameter[@path = '/ns']) then (
            (: we need to handle namespaces :)
            let $currentSet     := utillib:getDecorNamespaces($decor)
            let $prunedSet      := $currentSet[not(@prefix = $request-body/parameter[@op = 'remove'][@path = '/ns']/value/ns/@prefix)]
            let $addSet         := $request-body/parameter[@op = 'add'][@path = '/ns']/value/ns
            let $replaceSet     := 
                for $ns in $prunedSet 
                let $replacement    := $request-body/parameter[@op = 'add'][@path = '/ns']/value/ns[@prefix = $ns/@prefix]
                return ($replacement, $ns)[1]
            
            return utillib:setDecorNamespaces($decor, array:flatten($replaceSet | $addSet))
        ) else ()
    (: Done :)
    
    return
        prapi:getDecorInfo($authmap, $projectPrefixOrId, $checkADRAM)
};

declare function prapi:checkLogoConfig($op as xs:string, $path as xs:string, $in as element(), $storedElement as element()*) {
    if ($in[empty(@logo)][empty(logo)]) then () else
    (: old style upload :)
    if ($in[@logo = $storedElement/@logo]) then () else
    (: new style upload :)
    if ($in/logo[@name = $storedElement/@logo]) then () else
    if ($in[logo][not(normalize-space(data(logo)) = '')]) then
        'Parameter ' || $op || ' of path ' || $path || ' not supported. Reference logo SHALL HAVE attributes like name, size, and base64. Found: ' || string-join($in/logo, ' ')
    else
    if ($in[contains(@logo, '/')] | $in/logo[contains(@name, '/')]) then
        'Parameter ' || $op || ' of path ' || $path || ' not supported. Reference logo SHALL be project local and thus be filename only. Found: ' || string-join($in/@logo | $in/logo/@name, ' ') 
    else
    if ($in/logo[@base64]) then
        if ($in/logo[empty(@name)] | $in/logo[not(@size castable as xs:double)] | $in/logo[empty(@type)]) then
            'Parameter ' || $op || ' of path ' || $path || ' not supported. Reference logo SHALL HAVE attributes name, size (as xs:double and smaller than or equal to 500000 bytes (500Kb), found ' || $in/logo/@size || '), and type if there is base64 content (found ' || $in/logo/@type || ').'
        else
        if ($in/logo[xs:double(@size) gt 500000]) then
            'Parameter ' || $op || ' of path ' || $path || ' not supported. Reference logo size SHALL be smaller than or equal to 500000 bytes (500Kb). Found: ' || string-join($in/logo/@size, ' ')
        else
        if ($in/logo[not(@type = map:keys($prapi:acceptableMediaTypeMap))]) then
            'Parameter ' || $op || ' of path ' || $path || ' not supported. Reference logo type ' || $in/logo/@type || ' SHALL one of the supported type. Supported are: ' || string-join(map:keys($prapi:acceptableMediaTypeMap), ', or ')
        else
        if ($in/logo[not(tokenize(@name, '\.')[last()] = $prapi:acceptableMediaTypeMap?($in/logo/@type))]) then
            'Parameter ' || $op || ' of path ' || $path || ' not supported. Reference logo extension in the name ' || $in/logo/tokenize(@name, '\.')[last()] || ' SHALL be consistent with the stated type ' || $in/logo/@type || '. Expected: ' || string-join($prapi:acceptableMediaTypeMap?($in/logo/@type), ', or ')
        else (
            try {
                let $str    := $in/logo/@base64
                let $str    := if (starts-with($str, 'data:')) then substring-after($str, 'base64,') else $str
                let $img    := util:base64-decode($str)
                return ()
            }
            catch * {
                'Parameter ' || $op || ' of path ' || $path || ' not supported. Reference logo base64 string SHALL be decodable. Error: ' || $err:code || ' ' || $err:description
            }
        )
    else ()
};

declare function prapi:handleProject($in as element(project)) as element(project) {
    <project>
    {
        attribute id {$in/@id},
        attribute prefix {$in/@prefix},
        $in/@experimental[. = 'true'],
        attribute defaultLanguage {$in/@defaultLanguage},
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($in/name),
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($in/desc),
        if ($in[copyright]) then 
            for $n in $in/copyright return prapi:handleProjectCopyright($n)
        else (
            prapi:handleProjectCopyright(<copyright years="{year-from-date(current-date())}" by="ART-DECOR"/>)
        )
        ,
        for $n in $in/license return prapi:handleProjectLicense($n),
        for $n in $in/author return prapi:handleProjectAuthor($n, $in, false()),
        for $n in $in/reference[1] return prapi:handleProjectReference($n),
        for $n in $in/restURI return prapi:handleProjectRestURI($n),
        for $n in $in/defaultElementNamespace[1] return prapi:handleProjectDefaultElementNamespace($n),
        for $n in $in/contact return prapi:handleProjectContact($n),
        for $bbrs in $in/buildingBlockRepository
        let $b          := prapi:handleProjectBBR($bbrs)
        let $url        := $b/@url 
        let $ident      := $b/@ident
        let $licenseKey := $b/@licenseKey
        let $format     := $b/@format
        group by $url, $ident, $licenseKey
        return
            prapi:handleProjectBBR($bbrs[1])
        ,
        for $r in $in/version | $in/release
        order by $r/@date descending
        return
            $r
    }
    </project>
};
declare function prapi:handleProjectCopyright($in as element(copyright)) as element(copyright)? {
    if ($in[@years]) then
        element {name($in)} {
            $in/@years,
            $in/@by,
            if ($in[logo/@name]) then
                attribute logo {$in/logo/@name}
            else (
                $in/@logo[not(normalize-space() = '')]
            )
            ,
            $in/@type[not(normalize-space() = '')],
            for $node in $in/addrLine[exists(node())][.//text()[not(normalize-space() = '')]]
            return
                <addrLine>{$node/@type[not(. = '')], $node/node()}</addrLine>
        }
    else ()
};
declare function prapi:handleProjectLicense($in as element(license)) as element(license)? {
    if ($in[@code]) then (
        let $theCode  := normalize-space($in/@code)
        return
        element {name($in)} {
            attribute code {$theCode},
            attribute displayName {
                if ($in/@displayName[not(normalize-space(.) = '')]) then 
                    $in/@displayName 
                else (
                    $prapi:LICENSE-TYPES/enumeration[@value = $theCode]/label, $theCode
                )[1]
            }
        }
    )
    else ()
};
declare %private function prapi:storeProjectCopyrightLogo($decor as element(decor), $mediaType as xs:string?, $filename as xs:string?, $filecontent as xs:base64Binary?) {
    if (empty($filename) or empty($filecontent)) then () else (
        let $projectPrefix      := $decor/project/@prefix
        
        let $basecollection     := util:collection-name($decor)
        let $logoscollection    := concat($projectPrefix,'logos')
        let $fullcollection     := concat($basecollection,'/',$logoscollection)
        
        let $return             := xmldb:create-collection($basecollection,$logoscollection)
        let $return             := sm:chmod(xs:anyURI(concat('xmldb:exist://',$return)), 'rwxrwsr-x')
        
        let $return             :=
            if (string-length($mediaType) gt 0) then
                xmldb:store($fullcollection, xmldb:encode($filename), $filecontent, $mediaType)
            else (
                xmldb:store($fullcollection, xmldb:encode($filename), $filecontent)
            )
        let $return             := sm:chmod(xs:anyURI(concat('xmldb:exist://',$return)), 'rw-rw-r--')
        
        return
            ()
    )
};
declare function prapi:handleProjectAuthor($in as element(author), $decorProject as element(project)?, $addEffectiveDate as xs:boolean) as element(author)? {
    let $storedAuthor   := 
        if ($in[@id]) then
            $decorProject/author[@id = $in/@id][@username = $in/@username]
        else (
            $decorProject/author[@username = $in/@username]
        )
    
    return
    if ($in[@username]) then
        element {name($in)} {
            (: preserve stored author/@id from project if possible, otherwise create a new one :)
            if ($storedAuthor) then
                $storedAuthor[1]/@id
            else (
                attribute id {(max($decorProject/author/xs:integer(@id)) + 1, '1')[1]}
            ),
            $in/@username[not(. = '')],
            $in/@email[not(. = '')],
            attribute notifier {if ($in[@notifier = ('on', 'true')]) then 'on' else 'off'},
            if ($in[@effectiveDate castable as xs:dateTime]) then $in/@effectiveDate else 
            if (empty($storedAuthor) or $addEffectiveDate) then
                attribute effectiveDate {substring(string(current-dateTime()), 1, 19)}
            else (),
            $in/@expirationDate[. castable as xs:dateTime]
            ,
            (: don't write active unless false or at least some other related properties are present on input too :)
            if ($in[@admin | @datasets | @scenarios | @terminology | @rules | @ids | @issues]) then
                for $att in $decorlib:AUTHOR-TEMPLATE/(@active | @admin | @datasets | @scenarios | @terminology | @rules | @ids | @issues)
                return
                    switch (name($att))
                    case 'active' return
                        if ($in[@expirationDate castable as xs:dateTime][xs:dateTime(@expirationDate) le current-dateTime()]) then 
                            attribute active {false()}
                        else (
                            $in/@*[name() = name($att)], attribute active {true()}
                        )[1]
                    default return (
                        $in/@*[name() = name($att)], attribute {name($att)} {true()}
                    )[1]
            else
            if ($in[@expirationDate castable as xs:dateTime][xs:dateTime(@expirationDate) le current-dateTime()]) then 
                attribute active {false()}
            else ()
            ,
            normalize-space(string-join($in/node(), ' '))
        }
    else ()
};
declare function prapi:handleProjectReference($in as element(reference)?) as element(reference)? {
    if (empty($in)) then () else 
        element {name($in)} {
            $in/@url[not(normalize-space() = '')],
            if ($in[logo/@name]) then
                attribute logo {$in/logo/@name}
            else (
                $in/@logo[not(normalize-space() = '')]
            )
        }
};
declare function prapi:handleProjectRestURI($in as element(restURI)) as element(restURI)? {
    if ($in[not(. = '')]) then
        element {name($in)} {
            $in/@for[not(normalize-space() = '')], 
            $in/@format[not(normalize-space() = '')], 
            $in/node()
        }
    else ()
};
declare function prapi:handleProjectDefaultElementNamespace($in as element(defaultElementNamespace)?) as element(defaultElementNamespace) {
    <defaultElementNamespace ns="{($in/@ns[not(normalize-space() = '')], 'hl7:')[1]}"/>
};
declare function prapi:handleProjectContact($in as element(contact)) {
    element {name($in)} {
        if ($in/@email[not(normalize-space() = '')]) then
            attribute email {$in/@email[not(normalize-space() = '')]}
        else ()
    }
};
declare function prapi:handleProjectBBR($in as element(buildingBlockRepository)) as element(buildingBlockRepository) {
    element {name($in)} {
        let $lurl       := lower-case($in/@url)
        let $url        := 
            (: prevent mismatches in casing :)
            if ($lurl = $prapi:decorServicesURLlower) then $prapi:decorServicesURL else 
            (: prevent mismatches in casing :)
            if ($lurl = $prapi:fhirServicesURLlower) then $prapi:fhirServicesURL else
            (: prevent mismatches in http versus https :)
            if (substring-after($lurl, ':') = substring-after($prapi:decorServicesURLlower, ':')) then $prapi:decorServicesURL else
            (: prevent mismatches in http versus https :)
            if (substring-after($lurl, ':') = substring-after($prapi:fhirServicesURLlower, ':')) then $prapi:fhirServicesURL else (
                $in/@url
            )
        let $ident      := $in/@ident[not(. = '')]
        let $licenseKey := $in/@licenseKey[not(. = '')]
        let $format     := $in/@format[not(. = ('', 'decor'))]
        
        return (
            if (string-length($url) gt 0) then attribute url {$url} else (), $ident, $licenseKey, $format
        )
    }
};
declare function prapi:handleProjectBaseId($in as element(baseId)) as element(baseId) {
    element {name($in)} {
        $in/@id[not(. = '')],
        $in/@type[not(. = '')],
        $in/@prefix[not(. = '')],
        $in/@default[. = ('true', 'false')]
    }
};
declare function prapi:handleProjectDefaultBaseId($in as element(defaultBaseId)) as element(defaultBaseId) {
    element {name($in)} {
        $in/@id[not(. = '')],
        $in/@type[not(. = '')]
    }
};
declare function prapi:handleProjectId($in as element(id)) as element(in) {
    element {name($in)} {
        $in/@root, $in/@assigningAuthority[not(. = '')], $in/@extension[not(. = '')],
        for $sn in $in/designation
        return
            <designation>{$sn/@language[not(. = '')], $sn/@type[not(. = '')], $sn/@displayName[not(. = '')], utillib:parseNode($sn)/node()}</designation>
        ,
        for $sn in $in/property
        return
            utillib:serializeNode($sn)
    }
};
(:~ A wrapper around utillib:function-lookup() that throws a SERVER_ERROR, if the function is not found. :)
declare %private function prapi:function-lookup($module as map(*), $qname as xs:QName, $arity as xs:nonNegativeInteger) as function(*)? {
    let $function := utillib:function-lookup($module, $qname, $arity)
    return
       if (exists($function))
       then $function
       else error($errors:SERVER_ERROR, 'Dynamically loaded function not found, function "' || $qname || '", arity ' || $arity)
};