xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:~ Test API allows supports testing functions for dev purposes :)
module namespace testapi            = "http://art-decor.org/ns/api/test";

import module namespace roaster     = "http://e-editiones.org/roaster";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

import module namespace serverapi   = "http://art-decor.org/ns/api/server" at "server-api.xqm";
import module namespace jx          = "http://joewiz.org/ns/xquery/json-xml" at "library/json-xml.xqm";

declare namespace json              = "http://www.json.org";
declare namespace expath            = "http://expath.org/ns/pkg";

declare namespace rest              = "http://exquery.org/ns/restxq";
declare namespace req               = "http://exquery.org/ns/request";
declare namespace http              = "http://expath.org/ns/http-client";
declare namespace output            = "http://www.w3.org/2010/xslt-xquery-serialization";

declare namespace f                 = "http://hl7.org/fhir";

(:~Test. Returns some server info :)
declare function testapi:getTestDynamic($request as map(*)) {
let $mimetype   := testapi:getMimeType()
let $method     := testapi:getMethodFromMimeType($mimetype)

return (
    <rest:response>
        <http:response>
            <http:header name="Content-Type" value="{$mimetype}"/>
        </http:response>
        <output:serialization-parameters>
            <output:method value="{$method}"/>
            <output:media-type value="{$mimetype}"/>
        {   if ($method = 'json') then
                <exist:json-ignore-whitespace-text-nodes value="yes"/>
            else ()
        }
        </output:serialization-parameters>
    </rest:response>
    ,
    <serverinfo accept="{req:header("Accept")}" method="{$method}" mimetype="{$mimetype}">
        <desc language="en-US"/>
        <database version="{system:get-version()}"/>
    </serverinfo>
)
};

(:~Test. Returns database version we are running :)
declare function testapi:getTest($request as map(*)) {
    <serverinfo>
        <database version="{system:get-version()}"/>
    </serverinfo>
};

(:~ Accept FHIR in JSON and return FHIR in XML. Does not know about anything legal in FHIR, just does conversion as per the rules.
    @param $request-body            - FHIR
    @return FHIR XML or error
    @since 2020-05-03
:)
declare function testapi:convertFhirJsonToXml($request as map(*)) {
    if (empty($request-body)) then
        <rest:response><http:response status="400" message="Missing FHIR JSON in body"/></rest:response>
    else (
        let $json2xml   := jx:json-to-xml(util:base64-decode($request?body))/fn:map
        
        return
            if ($json2xml/fn:string[@key = 'resourceType']) then 
                transform:transform($json2xml, doc('/db/apps/art/resources/stylesheets/fhir-j2x.xsl'), ())
            else
                <rest:response><http:response status="400" message="JSON is missing resource type"><http:header name="Content-Type" value="application/xml"/></http:response></rest:response> |
                <OperationOutcome xmlns="http://hl7.org/fhir">
                    <text><status value="generated"/><div xmlns="http://www.w3.org/1999/xhtml">Received valid JSON file that appears not to be a FHIR resource. Missing FHIR resource type.</div></text> 
                    <issue>
                        <severity value="error"/>
                        <code value="structure"/>
                        <details><text value="Received valid JSON file that appears not to be a FHIR resource. Missing FHIR resource type."/></details>
                    </issue>
                </OperationOutcome>
    )
};

(:~ Accept FHIR in JSON and return FHIR in XML. Does not know about anything legal in FHIR, just does conversion as per the rules.
    @param $request-body            - FHIR
    @return FHIR XML or error
    @since 2020-05-03
:)
declare function testapi:convertFhirXmlToJson($request as map(*)) {
    let $supportedFhirVersions              := try { xmldb:get-child-collections(concat(repo:get-root(), '/fhir')) } catch * {()}
    let $defaultFhirVersion                 := try { serverapi:getServerFhirDefaultVersion() } catch * { () }
    let $defaultFhirVersion                 := if (empty($defaultFhirVersion[not(. = '')])) then $supportedFhirVersions[1] else $defaultFhirVersion
    
    let $fhirVersion    := $request?parameters?fhirversion[not(. = '')][1]
    let $fhirVersion    := if (empty($fhirVersion)) then $defaultFhirVersion else $fhirVersion
    let $fhirConversion := concat('/db/apps/fhir/',$fhirVersion,'/resources/stylesheets/fhir-xml2json.xsl')
    
    return
    if (empty($request?body)) then
        <rest:response><http:response status="400" message="Missing FHIR JSON in body"/></rest:response>
    else
    if ($fhirVersion = $supportedFhirVersions and doc-available($fhirConversion)) then (
        if ($request?body) then (
            <rest:response><http:response><http:header name="Content-Type" value="application/json"/></http:response></rest:response>,
            transform:transform($request?body, doc($fhirConversion), ())
        )
        else
            <rest:response><http:response status="400" message="FHIR Resource missing"><http:header name="Content-Type" value="application/json"/></http:response></rest:response> |
            <OperationOutcome xmlns="http://hl7.org/fhir">
                <issue>
                    <severity value="error"/>
                    <code value="structure"/>
                    <details><text value="You are missing a FHIR Resource in your HTTP Body."/></details>
                </issue>
            </OperationOutcome>
    )
    else
    if (empty($supportedFhirVersions)) then
        <rest:response><http:response status="500" message="This server does not support HL7 FHIR"/></rest:response> |
        <OperationOutcome xmlns="http://hl7.org/fhir">
            <issue>
                <severity value="error"/>
                <code value="unsupported"/>
                <details><text value="This server does not support HL7 FHIR, because there are no FHIR servers installed. Please talk to your sysadmin."/></details>
            </issue>
        </OperationOutcome>
    else
    if ($fhirVersion != $supportedFhirVersions) then (
        <rest:response><http:response status="400" message="Unsupported FHIR version"><http:header name="Content-Type" value="application/json"/></http:response></rest:response> |
        <OperationOutcome xmlns="http://hl7.org/fhir">
            <issue>
                <severity value="error"/>
                <code value="unsupported"/>
                <details><text value="FHIR version '{$fhirVersion}' is not supported. Please use any of '{$supportedFhirVersions}'"/></details>
            </issue>
        </OperationOutcome>
    )
    else (
        <rest:response><http:response status="500" message="Server is missing FHIR conversion"><http:header name="Content-Type" value="application/json"/></http:response></rest:response> |
        <OperationOutcome xmlns="http://hl7.org/fhir">
            <issue>
                <severity value="error"/>
                <code value="unsupported"/>
                <details><text value="FHIR version '{$fhirVersion}' does not have the required conversion '{$fhirConversion}' installed. Please update."/></details>
            </issue>
        </OperationOutcome>
    )
};

(:~Just echo back what was sent in. Request body, parameters, headers, and more:)
declare function testapi:getEcho($request as map(*)) {
    let $accept := tokenize(replace(tokenize(request:get-header('Accept'), ';')[1], '\s', ''), ',')
    
    return
        if ($accept[contains(., 'html')]) then (
            response:set-header('Content-Type', 'text/html; charset=utf-8'),
            roaster:response(200, "text/html", 
            <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <meta http-equiv="Content-Type" content="text/xhtml; charset=utf-8"/>
                    <title>echo</title>
                </head>
                <body>
                {
                    for $k in map:keys($request)
                    return
                        <h1>{$k}</h1> |
                        <pre>
                        {
                            fn:serialize(map:get($request, $k), map{'method':'json', 'indent': true(), 'json-ignore-whitespace-text-nodes':'yes'})
                        }
                        </pre>
                }
                </body>
            </html>
            )
        )
        else
        if ($accept[contains(., 'json')]) then (
            response:set-header('Content-Type', 'application/json; charset=utf-8'), 
            roaster:response(200, "application/json", parse-json(fn:serialize($request, map{'method':'json', 'indent': true(), 'json-ignore-whitespace-text-nodes':'yes'})))
        )
        else (
            response:set-header('Content-Type', 'application/xml; charset=utf-8'),
            roaster:response(200, "application/xml", jx:json-to-xml(fn:serialize($request, map{'method':'json', 'indent': true(), 'json-ignore-whitespace-text-nodes':'yes'})))
        )
};
declare function testapi:postEcho($request as map(*)) {
    testapi:getEcho($request)
};
declare function testapi:putEcho($request as map(*)) {
    testapi:getEcho($request)
};
declare function testapi:patchEcho($request as map(*)) {
    testapi:getEcho($request)
};
declare function testapi:deleteEcho($request as map(*)) {
    testapi:getEcho($request)
};
declare function testapi:headEcho($request as map(*)) {
    testapi:getEcho($request)
};
declare function testapi:optionsEcho($request as map(*)) {
    testapi:getEcho($request)
};

(:~ Determine which of the accepted mime-types is to be used here

    Accept:text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
:)
declare %private function testapi:getMimeType() as xs:string? {
let $defaultmimetype    := 'text/json'
let $mimetype           := tokenize(normalize-space(tokenize(req:header("Accept"), ';')[1]), ',')

return
    if ($mimetype[. = 'application/json']) then 'application/json' else
    if ($mimetype[. = 'text/json']) then 'text/json' else
    if ($mimetype[. = '*/json']) then 'text/json' else
    if ($mimetype[. = 'application/xml']) then 'application/xml' else
    if ($mimetype[. = 'text/xml']) then 'text/xml' else
    if ($mimetype[. = '*/xml']) then 'text/xml' else
    if ($mimetype[. = 'text/html']) then '*/json' else
    if ($mimetype[. = 'text/plain']) then 'text/plain' else
    if ($mimetype[. = '*/*']) then $defaultmimetype
    else ()
};

declare %private function testapi:getMethodFromMimeType($mimetype as xs:string?) as xs:string? {
    switch ($mimetype)
    case 'application/xml' return 'xml'
    case 'text/xml' return 'xml'
    case 'application/json' return 'json'
    case 'text/json' return 'json'
    case 'text/html' return 'html'
    case 'text/plain' return 'text'
    default return ()
};