xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

(:~ Retrieval of API meta data :)
module namespace apiversion = "http://art-decor.org/ns/api/apiversion";

(:~ This returns the API version

@return {  "api": "vvv" } as JSON where vvv is the version of the API 
@author ART-DECOR® Expert Group
@see https://art-decor.org
@since 2020-10-30
:) 
declare function apiversion:getAPIversion($request as map(*)) {
    <api version="{$request?spec?info?version}"/>
    (:let $version := doc('../expath-pkg.xml')/*/@version
    return
         map {
            "api": map {
                "version": string($version)
            }
        }:)
};