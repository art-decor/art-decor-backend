xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:~ Dataset API allows read, create, update on DECOR datasets :)
module namespace dsapi              = "http://art-decor.org/ns/api/dataset";
import module namespace deapi       = "http://art-decor.org/ns/api/concept" at "concept-api.xqm";

import module namespace roaster     = "http://e-editiones.org/roaster";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

import module namespace utillib     = "http://art-decor.org/ns/api/util" at "library/util-lib.xqm";
import module namespace utilsvg     = "http://art-decor.org/ns/api/util-svg" at "library/util-svg-lib.xqm";
import module namespace utilhtml    = "http://art-decor.org/ns/api/util-html" at "library/util-html-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "library/settings-lib.xqm";
import module namespace decorlib    = "http://art-decor.org/ns/api/decor" at "library/decor-lib.xqm";
import module namespace histlib     = "http://art-decor.org/ns/api/history" at "library/history-lib.xqm";
import module namespace serverapi   = "http://art-decor.org/ns/api/server" at "server-api.xqm";
import module namespace tmapi       = "http://art-decor.org/ns/api/template" at "template-api.xqm";

declare namespace json      = "http://www.json.org";

(:~ All functions support their own override, but this is the fallback for the maximum number of results returned on a search :)
declare %private variable $dsapi:maxResults         := 50;
declare %private variable $dsapi:STATUSCODES-FINAL  := ('final', 'pending', 'rejected', 'cancelled', 'deprecated');
declare %private variable $dsapi:ADDRLINE-TYPE      := utillib:getDecorTypes()/AddressLineType;
declare %private variable $dsapi:VALUEDOMAIN-TYPE   := utillib:getDecorTypes()/DataSetValueType;

(:~ local debug 0 or 1 - or 2,... later :)
declare %private variable $dsapi:DEBUG := 0;

(:~ Retrieves latest DECOR dataset based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the dataset
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the dataset. If not given assumes latest version for id
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @param $projectLanguage         - optional parameter to select from a specific compiled language
    @param $treeonly                - optional boolean parameter to get the tree structure only if true
    @param $fullTree                - optional boolean parameter relevant if $treeonly = 'true' to include absent concepts
    @return as-is or as compiled as JSON
    @since 2020-05-03
:)
declare function dsapi:getLatestDataset($request as map(*)) {
    dsapi:getDataset($request)
};

(:~ Retrieves DECOR dataset based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the dataset
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the dataset. If not given assumes latest version for id
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @param $projectLanguage         - optional parameter to select from a specific compiled language
    @param $treeonly                - optional boolean parameter to get the tree structure only if true
    @param $fullTree                - optional boolean parameter relevant if $treeonly = 'true' to include absent concepts
    @return as-is or as compiled as JSON
    @since 2020-05-03
:)
declare function dsapi:getDataset($request as map(*)) {

    let $id                 := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $projectVersion     := $request?parameters?release
    let $projectLanguage    := $request?parameters?language
    let $treeOnly           := $request?parameters?treeonly = true()
    let $fullTree           := $request?parameters?fulltree = true()
    let $propertiesMap      := 
        map:merge((
            for $s in $request?parameters?conceptproperty[string-length() gt 0]
            for $item in tokenize(lower-case($s),'\s') 
            return map:entry($item, true())
        ))
    
    let $results            := dsapi:getDataset($id, $effectiveDate, $projectVersion, $projectLanguage, $treeOnly, $fullTree, $propertiesMap)

    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple datasets for id '", $id, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        )
        else (
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
        )

};

(:~ Retrieves DECOR dataset as a diagram based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the dataset
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the dataset. If not given assumes latest version for id
    @param $conceptId               - optional parameter denoting the id of the concept in the dataset
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the concept in the dataset. If not given assumes latest version for id
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @param $projectLanguage         - optional parameter to select from a specific compiled language
    @param $filter                  - optional parameter to exclude statusses from the selection
    @param $interactive             - boolean
    @param $format                  - optional. overrides the accept-header for frontend purposes
    @return as-is 
    @since 2023-12-21
:)

declare function dsapi:getDatasetDiagram($request as map(*)) {

    let $conceptId          := $request?parameters?conceptId
    let $conceptEffectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?conceptEffectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?conceptEffectiveDate[string-length() gt 0]
        }
    let $projectVersion                 := $request?parameters?release[not(. = '')]
    let $projectLanguage                := $request?parameters?language[not(. = '')]
    let $filter                         := $request?parameters?filter[not(. = '')]
    let $interactive                    := not($request?parameters?interactive = false())
    let $format                         := $request?parameters?format[not(. = '')]
    
    let $id                             := $request?parameters?id[not(. = '')]
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    
    let $check                          :=
        if (empty($conceptId) and not(empty($conceptEffectiveDate))) then 
            error($errors:BAD_REQUEST, 'Request SHALL NOT have conceptEffectiveDate without conceptId') 
        else ()
    
    let $dconcept                       :=
        if ($conceptId) then (
            let $dconcept               := utillib:getConcept($conceptId, $conceptEffectiveDate, (), $projectLanguage)
            let $check                  :=
                if ($dconcept) then () 
                else error($errors:BAD_REQUEST, 'Concept id ''' || $conceptId || ''' effectiveDate ''' || $conceptEffectiveDate || ''' requested but not found. ')
            return $dconcept
        )
        else ()
        
    let $format                         :=
        if (not($format = 'xml')) then 'svg' else 'xml'
    (: 
       overwrite format in the backend is not always possible because of middleware behaviour
       - if in backend the format is xml and accept is not application/xml, roaster always gives a json payload 
    :) 
    let $check                          :=
        if ($format = 'xml' and not((roaster:accepted-content-types()[. = ('image/svg+xml', 'application/xml')])[1] = 'application/xml')) then 
            error($errors:BAD_REQUEST, 'In case of format parameter is xml the accept header should be application/xml')
        else ()   
    
    let $filterStatuses                 := 
        if ($filter) then
            for $p in $filter
            return tokenize($p, '\s')
        else ('cancelled', 'rejected', 'deprecated')

    let $dataset                        := utillib:getDataset($id, $effectiveDate, $projectVersion, $projectLanguage)
       
    let $results                        :=
        if (empty($dataset)) then () else (
 
        let $projectLanguage            := if (empty($projectLanguage)) then $dataset/ancestor::decor[1]/project/@defaultLanguage else $projectLanguage
                                
            let $fullDatasetTree        := 
                if (empty($projectVersion)) then utillib:getFullDatasetTree($dataset, $conceptId, $conceptEffectiveDate, $projectLanguage, (), false(), (), ())
                else if ($dataset[self::dataset]) then $dataset 
                else if ($projectLanguage = '*') then $setlib:colDecorVersion//transactionDatasets[@versionDate = $projectVersion]//dataset[@transactionId = $dataset/@id][@transactionEffectiveDate = $dataset/@effectiveDate]
                else $setlib:colDecorVersion//transactionDatasets[@versionDate = $projectVersion][@language = $projectLanguage]//dataset[@transactionId = $dataset/@id][@transactionEffectiveDate = $dataset/@effectiveDate]    
     
            let $concept                := if (empty($conceptId)) then $fullDatasetTree else ($fullDatasetTree/concept[1])
             
            return            
                (: if xml is chosen just give the retrieved concept :)
                if ($format = 'xml') then
                for $c in $concept
                    return
                        element {name($c)} {
                            $c/@*,
                            namespace {"json"} {"http://www.json.org"},
                            utillib:addJsonArrayToElements($c/*, $format)
                        }
                else
                (: if svg return the concept as an svg-diagram with interactive features :)
                if ($concept[not(@statusCode = $filterStatuses)]) then 
                    let $baseUrl           := 
                        if ($interactive) then
                            '?language=' || $projectLanguage || '&amp;filter=' || encode-for-uri(fn:string-join($filterStatuses, ' ')) || '&amp;interactive=' || $interactive || '&amp;format=' || $format || '&amp;datasetId=' || $id || '&amp;datasetEffectiveDate=' || encode-for-uri($dataset/@effectiveDate)
                        else ()
                    
                    return (
                        response:set-header('Content-Type','image/svg+xml'),
                        response:set-header('X-Robots-Tag', 'noindex'), 
                        utilsvg:convertConcept2Svg($concept, $projectLanguage, $filterStatuses, $dconcept, $baseUrl)
                    )
                else ()
        )
 
    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else $results
};

(:~ Retrieves DECOR dataset for publication based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the dataset
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the dataset. If not given assumes latest version for id
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @param $projectLanguage         - optional parameter to select from a specific compiled language
    @param $format                  - optional. overrides the accept-header for frontend purposes
    @param $download                - optional as xs:boolean. Default: false. 
    @return as-is or as compiled as JSON
    @since 2023-11-10
:)

declare function dsapi:getDatasetExtract($request as map(*)) {

    let $projectVersion                 := $request?parameters?release[not(. = '')]
    let $projectLanguage                := $request?parameters?language[not(. = '')]
    let $communityName                  := $request?parameters?community[not(. = '')]
    let $format                         := $request?parameters?format[not(. = '')]
    let $download                       := $request?parameters?download=true()
    
    let $id                             := $request?parameters?id[not(. = '')]
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }

    (: parameter format overrides accept-header as mime-type for using the api in the browser :)
    
    (: 
        this call is used in backend and frontend (browser) - default behaviour mime-types:
        - backend:   default mime-type is json - response payload is json - if no accept type is given
        - frontend:  default mime-type is xml - response payload is xml - if no format is given
    :)
    
    let $acceptTypes                    := roaster:accepted-content-types()
    let $acceptedType                   := ($acceptTypes[. = ('application/xml', 'application/json')],'application/json')[1]
    
    let $format                         := if ($format) then $format else tokenize($acceptedType, '/')[2]

    (: 
       overwrite format in the backend is not always possible because of middleware behaviour
       - if in backend the format is xml and accept is not application/xml, roaster always gives a json payload 
    :) 
    let $check                          :=
        if ($format = 'xml' and not($acceptedType = 'application/xml')) then 
            error($errors:BAD_REQUEST, 'In case of format parameter is xml the accept header should be application/xml')
        else ()   

    let $dataset                        := utillib:getDataset($id, $effectiveDate, $projectVersion, $projectLanguage)
    
    let $results                        := 
        if (empty($dataset)) then () else 
 
            let $datasetExtract         := utillib:getDatasetExtract($dataset, $id, $effectiveDate, (), (),$projectVersion, $projectLanguage, $communityName)
            let $latestVersion          := utillib:getDataset($dataset/@id, ())/@effectiveDate = $dataset/@effectiveDate
            let $transactions           := $dataset/ancestor::decor//transaction[representingTemplate[@sourceDataset = $datasetExtract/@id][@sourceDatasetFlexibility = $datasetExtract/@effectiveDate]]
            let $transactions           := 
                if ($latestVersion) then 
                    $transactions | $dataset/ancestor::decor//transaction[representingTemplate[@sourceDataset = $datasetExtract/@id][not(@sourceDatasetFlexibility castable as xs:dateTime)]] 
                else $transactions
                
            return
                utillib:mergeDatasetWithUsage($datasetExtract, $transactions, $dataset/ancestor::decor/project/@prefix)    
        
    let $filename                       := 'DS_' || $results/@shortName || '_(download_' || substring(fn:string(current-dateTime()),1,19) || ').' || $format
    
    let $results                        :=
        for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/node(), $format)
                }
    
    (: in this case the json format overrides header application/xml and works with a text header :)
    let $results                        :=
        if ($format = 'json' and not($acceptedType = 'application/json')) then
            fn:serialize($results, map{"method": $format , "indent": true()})
            else $results     
           
    let $r-header                       := 
        (response:set-header('Content-Type', 'application/' || $format || '; charset=utf-8'),
        if ($download) then response:set-header('Content-Disposition', 'attachment; filename='|| $filename) else ())
    
    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple datasets for id '", $id, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        )
        else $results        

};

(:~ Retrieves DECOR dataset view based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the dataset
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the dataset. If not given assumes latest version for id
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @param $projectLanguage         - optional parameter to select from a specific compiled language
    @param $ui-language             - optional parameter to select from a specific language for the ui
    @param $hidecolumns             - optional parameter to hide columns in the view based on (hex) number
    @param $format                  - optional. if not given it is html
    @param $download                - optional as xs:boolean. Default: false. 
    @return as-is 
    @since 2024-09-24
:)

declare function dsapi:getDatasetView($request as map(*)) {

    let $projectVersion                 := $request?parameters?release[not(. = '')]
    let $projectLanguage                := $request?parameters?language[not(. = '')]
    let $communityName                  := $request?parameters?community[not(. = '')]
    let $ui-lang                        := $request?parameters?ui-language[not(. = '')]
    let $hidecolumns                    := $request?parameters?hidecolumns[not(. = '')]
    let $format                         := $request?parameters?format[not(. = '')]
    let $download                       := $request?parameters?download=true()
    
    let $id                             := $request?parameters?id[not(. = '')]
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }

    let $format                         :=
        if (not($format = 'list')) then 'html' else 'list'
    
    let $dataset                        := utillib:getDataset($id, $effectiveDate, $projectVersion, $projectLanguage)
    let $decor                          := $dataset/ancestor::decor[1]

    let $results                        := if (empty($dataset)) then () else utillib:getDatasetExtract($dataset, $id, $effectiveDate, (), (), $projectVersion, $projectLanguage, $communityName)

    let $filename                       := 'DS_' || $results/@shortName || '_(download_' || substring(fn:string(current-dateTime()),1,19) || ').html' 
    let $r-header                       := 
        (response:set-header('Content-Type', 'text/html; charset=utf-8'),
        if ($download) then response:set-header('Content-Disposition', 'attachment; filename='|| $filename) else ())
    
    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple datasets for id '", $id, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        )
        else 
        
        (: prepare for Html :)
        let $referenceUrl               := $decor/project/reference[@url castable as xs:anyURI]/@url
        let $projectPrefix              := $decor/project/@prefix
                    
        return 
            if ($format = 'list') 
                then utilhtml:convertTransactionOrDataset2SimpleHtml($results, $projectLanguage, $ui-lang, $hidecolumns, true(), $projectVersion, $referenceUrl, $projectPrefix, false(), $download) 
                else utilhtml:convertTransactionOrDataset2Html($results, $projectLanguage, $ui-lang, $hidecolumns, true(), true(), true(), $projectVersion, $referenceUrl, $projectPrefix, (), false(), $filename, $download)
};


(:~ Retrieves DECOR dataset usage based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the dataset
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the dataset. If not given assumes latest version for id
    @param $projectPrefix           - optional. limits scope to this project only
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @return as-is or as compiled as JSON
    @since 2020-05-03
:)
declare function dsapi:getDatasetUsage($request as map(*)) {
    
    let $projectPrefix                  := $request?parameters?prefix
    let $projectVersion                 := $request?parameters?release
    let $projectLanguage                := $request?parameters?language
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    
    let $datasetsAll                    := utillib:getDataset($id, ())
    let $mostRecent                     := $datasetsAll/@effectiveDate
    let $ds                             := $datasetsAll
    let $isMostRecent                   := $mostRecent = $effectiveDate or empty($effectiveDate)
    
    let $allTransactions                := utillib:getTransactionsByDataset($id, $effectiveDate)
    
    let $results                        := (
        for $item in $allTransactions
        let $trid := $item/@id
        let $tred := $item/@effectiveDate
        group by $trid, $tred
        return
            utillib:doTransactionAssociation($ds, $item[1])
    )
    
    let $count                          := count($results)
    let $max                            := $count
    let $allcnt                         := $count
    
    return
        <list artifact="USAGE" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements(subsequence($results, 1, $max))
        }
        </list>
};

(:~ Retrieves DECOR dataset history based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id
    @param $effectiveDate           - optional parameter denoting the effectiveDate. If not given assumes latest version for id
    @return list
    @since 2022-08-16
:)
declare function dsapi:getDatasetHistory($request as map(*)) {
    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $projectPrefix                  := ()
    let $results                        := histlib:ListHistory($authmap, $decorlib:OBJECTTYPE-DATASET, (), $id, $effectiveDate, 0)
    
    return
        <list artifact="{$decorlib:OBJECTTYPE-DATASET}" current="{count($results)}" total="{count($results)}" all="{count($results)}" lastModifiedDate="{current-dateTime()}">
        {
            utillib:addJsonArrayToElements($results)
        }
        </list>
};

(:~ Returns a list of zero or more datasets
    
@param $projectPrefix           - required. limits scope to this project only
@param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
@param $projectLanguage         - optional parameter to select from a specific compiled language
@param $includeBBR              - optional. Include BBR datasets in the list. BBRs have to be declared in the project
@param $scenariosonly           - optional boolean. If true only includes datasets bound in a scenario transaction
@return all live repository/non-private datasets as JSON, all data sets for the given $projectPrefix or nothing if not found
@since 2020-05-03
:)
declare function dsapi:getDatasetList($request as map(*)) {

    let $searchTerms            := 
        array:flatten(
            for $s in $request?parameters?search[string-length() gt 0]
            return
                tokenize(lower-case($s),'\s')
        )
    let $projectPrefix      := $request?parameters?prefix
    let $projectVersion     := $request?parameters?release
    let $projectLanguage    := $request?parameters?language
    let $includeBbr         := $request?parameters?includebbr = true()
    let $scenariosOnly      := $request?parameters?scenariosonly = true()
    let $treeType           := $request?parameters?treetype
    
    (:let $sort               := $request?parameters?sort
    let $sortorder          := $request?parameters?sortorder
    let $max                := $request?parameters?max:)
    
    return
        dsapi:getDatasetList($projectPrefix, $projectVersion, $projectLanguage, $includeBbr, $scenariosOnly, $treeType, $searchTerms)

};

(: Create a dataset, either empty or based on another dataset

@param $projectPrefix project to create this dataset in
@param $targetDate If true invokes effectiveDate of the new dataset and concepts as [DATE]T00:00:00. If false invokes date + time [DATE]T[TIME] 
@param $sourceId parameter denoting the id of a dataset to use as a basis for creating the new dataset
@param $sourceEffectiveDate parameter denoting the effectiveDate of a dataset to use as a basis for creating the new dataset",
@param $keepIds Only relevant if source dataset is specified. If true, the new dataset will keep the same ids for the new dataset, and only update the effectiveDate
@param $baseDatasetId Only relevant when a source dataset is specified and `keepIds` is false. This overrides the default base id for datasets in the project. The value SHALL match one of the projects base ids for datasets
@param $baseConceptId Only relevant when a source dataset is specified and `keepIds` is false. This overrides the default base id for concepts in the project. The value SHALL match one of the projects base ids for concepts
@param $templateAssociations Only relevant if source dataset is specified, and commit=true. If 'true' then template associations are generated as copy of existing template associations for the selected dataset concepts and are immediately committed in the project\n**Note that this immediately affects your project and that there is no undo possible!!**\nIf 'false' then template associations are generated and returned, but not committed to the project.",
@param $skipDeprecated Only relevant if source dataset is specified.\n- If 'true' then deprecated concepts in the base dataset/templateAssociations are included. This is recommended\n- If 'false' the deprecated concepts in the base dataset/templateAssociations are NOT included\nNote that cancelled, and rejected concept are never included as they are supposed to never have been published. Deprecated concepts however have been published and (potentially) implemented.",
@param $testMode If you only want to try out what woould happen with all your selected parameters and don't want to commit the result just yet, this value should be true
@return (empty) dataset object as xml with json:array set on elements
:)
declare function dsapi:postDataset($request as map(*)) {

    let $authmap                := $request?user
    let $projectPrefix          := $request?parameters?prefix[not(. = '')]
    let $targetDate             := $request?parameters?targetDate = true()
    let $sourceId               := $request?parameters?sourceId
    let $sourceEffectiveDate    := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?sourceEffectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?sourceEffectiveDate[string-length() gt 0]
        }
    let $keepIds                := $request?parameters?keepIds = true()
    let $baseDatasetId          := $request?parameters?baseDatasetId
    let $baseConceptId          := $request?parameters?baseConceptId
    let $templateAssociations   := $request?parameters?templateAssociations = true()
    let $skipDeprecated         := $request?parameters?skipDeprecated = true()
    let $testMode               := $request?parameters?testMode = true()
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($projectPrefix)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter prefix')
        else ()
    
    let $results                := dsapi:createDataset($authmap, $projectPrefix, $targetDate, $sourceId, $sourceEffectiveDate, $keepIds, $baseDatasetId, $baseConceptId, $templateAssociations, $skipDeprecated, $testMode)
    
    return
        roaster:response(if ($testMode) then 200 else 201, 
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
        )
};

(:~ Update dataset

@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $id                       - required. the id for the dataset to update 
@param $effectiveDate            - required. the effectiveDate for the dataset to update 
@param $request-body             - required. body containing new concept structure
@return concept structure
@since 2020-05-03
:)
declare function dsapi:putDataset($request as map(*)) {

    let $authmap                := $request?user
    let $dsid                   := $request?parameters?id
    let $dsed                   := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $data                   := utillib:getBodyAsXml($request?body, 'dataset', ())
    let $treeOnly               := $request?parameters?treeonly = true()
    let $fullTree               := $request?parameters?fulltree = true()
    
    (:let $s                      := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data):) 
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($data)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $return                 := dsapi:putDataset($authmap, string($dsid), $dsed, $data)
    
    return
        dsapi:getDataset($request)
};

(:~ Update DECOR dataset parts. Does not touch any concepts. Expect array of parameter objects, each containing RFC 6902 compliant contents. Note: RestXQ does not do PATCH (yet), but that would be the preferred option.

{ "op": "[add|remove|replace]", "path": "[/|/displayName|/priority|/type]", "value": "[string|object]" }

where

* op - add (object associations, tracking, event) or remove (object associations only) or replace (displayName, priority, type, tracking, assignment)

* path - / (object, tracking, assignment) or /displayName or /priority or /type

* value - string when the path is not /. object when path is /
@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $id                       - required. the id for the issue to update 
@param $request-body             - required. json body containing array of parameter objects each containing RFC 6902 compliant contents
@return dataset structure including generated meta data
@since 2020-05-03
@see http://tools.ietf.org/html/rfc6902
:)
declare function dsapi:patchDataset($request as map(*)) {

    let $authmap                        := $request?user
    let $dsid                           := $request?parameters?id
    let $dsed                           := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $projectVersion                 := ()
    let $projectLanguage                := ()
    let $treeOnly                       := $request?parameters?treeonly = true()
    let $fullTree                       := $request?parameters?fulltree = true()
    let $data                           := utillib:getBodyAsXml($request?body, 'parameters', ())
    
    (:let $s                      := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data) :)
    
    let $check                  :=
        if ($data) then () else (
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        )
    
    let $return                 := dsapi:patchDataset($authmap, string($dsid), $dsed, $data)
    let $results                := dsapi:getDataset($request)
    return 
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple datasets for id '", $dsid, "' effectiveDate '", $dsed, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        )
        else (
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
        )

};

(:~ Update dataset statusCode, versionLabel, expirationDate and/or officialReleaseDate

@param $authmap required. Map derived from token
@param $id required. DECOR concept/@id to update
@param $effectiveDate required. DECOR concept/@effectiveDate to update
@param $recurse optional as xs:boolean. Default: false. Allows recursion into child particles to apply the same updates
@param $listOnly optional as xs:boolean. Default: false. Allows to test what will happen before actually applying it
@param $newStatusCode optional as xs:string. Default: empty. If empty, does not update the statusCode
@param $newVersionLabel optional as xs:string. Default: empty. If empty, does not update the versionLabel
@param $newExpirationDate optional as xs:string. Default: empty. If empty, does not update the expirationDate 
@param $newOfficialReleaseDate optional as xs:string. Default: empty. If empty, does not update the officialReleaseDate 
@return list object with success and/or error elements or error
:)
declare function dsapi:putDatasetStatus($request as map(*)) {

    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $recurse                        := $request?parameters?recurse = true()
    let $list                           := $request?parameters?list = true()
    let $statusCode                     := $request?parameters?statusCode
    let $versionLabel                   := $request?parameters?versionLabel
    let $expirationDate                 := $request?parameters?expirationDate
    let $officialReleaseDate            := $request?parameters?officialReleaseDate
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    
    return
        dsapi:setDatasetStatus($authmap, $id, $effectiveDate, $recurse, $list, $statusCode, $versionLabel, $expirationDate, $officialReleaseDate)
};

declare function dsapi:getDataset($id as xs:string, $effectiveDate as xs:string?, $projectVersion as xs:string?, $projectLanguage as xs:string?, $treeonly as xs:boolean?, $fullTree as xs:boolean?, $propertiesMap as map(*)?) {
    let $id                         := $id[not(. = '')]
    let $effectiveDate              := $effectiveDate[not(. = '')]
    let $projectVersion             := $projectVersion[not(. = '')]
    let $projectLanguage            := $projectLanguage[not(. = '')]
    let $treeonly                   := $treeonly = true()
    let $fullTree                   := $fullTree = true()
    
    let $dataset                    := utillib:getDataset($id, $effectiveDate, $projectVersion[1], $projectLanguage[1])
    let $decor                      := $dataset/ancestor::decor
    let $projectPrefix              := $decor/project/@prefix
    
    let $dataset                    :=    
        if ($treeonly) then 
            utillib:getDatasetTree($id, $effectiveDate, (), (), $fullTree, $propertiesMap)
        else $dataset

    let $dataset                    :=
        for $ds in $dataset
        return
        element {name($ds)} {
            $ds/@*,
            $ds/name,
            $ds/desc,
            if ($ds[@id]) then 
                if ($ds[publishingAuthority]) then () else utillib:inheritPublishingAuthorityFromProject($decor)
            else ()
            ,
            $ds/(* except (name | desc))
         }
    
    for $ds in $dataset
    return
        element {name($ds)} {
            $ds/@*,
            (: would already have taken care of this :)
            if ($treeonly) then 
                $ds/(* except (concept|recycle)) 
            else ( 
                for $node in $ds/(* except (concept|recycle))
                return
                    (: but just in case relationship already is good, no need to redo that :)
                    if ($node[self::relationship][empty(@iStatusCode)]) then (
                        (:new since 2015-04-21:)
                        let $referredDataset    :=  
                            if (string-length($node/@ref)=0) then () else (
                                utillib:getDataset($node/@ref, $node/@flexibility)
                            )
                        return
                            <relationship type="{$node/@type}" ref="{$node/@ref}" flexibility="{$node/@flexibility}">
                            {
                                if ($referredDataset) then (
                                    $referredDataset/ancestor::decor/project/@prefix,
                                    attribute iStatusCode {$referredDataset/@statusCode}, 
                                    if ($referredDataset/@expirationDate) then attribute iExpirationDate {$referredDataset/@expirationDate} else (),
                                    if ($referredDataset/@versionLabel) then attribute iVersionLabel {$referredDataset/@versionLabel} else (),
                                    attribute iddisplay {utillib:getNameForOID($node/@ref, $referredDataset/ancestor::decor/project/@defaultLanguage, $referredDataset/ancestor::decor)},
                                    attribute localInherit {$referredDataset/ancestor::decor/project/@prefix = $projectPrefix},
                                    $referredDataset/name
                                ) else ()
                            }
                            </relationship>
                    )
                    else (
                        $node
                    )
            )
            ,
            (: we return just a count of issues and leave it up to the calling party to get those if required :)
            <issueAssociation count="{count($setlib:colDecorData//issue/object[@id = $ds/@id][@effectiveDate = $ds/@effectiveDate])}"/>
            ,
            $ds/concept
            ,
            (: recycle bin if present after all concepts :)
            $ds/recycle
        }
};

(:~ Creates a list object with datasets

@param $projectPrefix           - required. limits scope to this project only
@param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
@param $projectLanguage         - optional parameter to select from a specific compiled language
@param $includeBBR              - optional. Include BBR datasets in the list. BBRs have to be declared in the project
@param $scenariosonly           - optional boolean. If true only includes datasets bound in a scenario transaction
@param $searchTerms             - optional array of string. Searches for datasets by name or ending with id
@return all live repository/non-private datasets as JSON, all data sets for the given $projectPrefix or nothing if not found
@since 2020-05-03
:)
declare function dsapi:getDatasetList($projectPrefix as xs:string?, $projectVersion as xs:string?, $projectLanguage as xs:string?, $includeBBR as xs:boolean, $scenariosonly as xs:boolean, $treetype as xs:string?, $searchTerms as xs:string*) as element(list) {
    let $projectPrefix      := $projectPrefix[not(. = '')]
    let $projectVersion     := $projectVersion[not(. = '')]
    let $projectLanguage    := $projectLanguage[not(. = '')]
    let $includeBBR         := $includeBBR = true()
    let $scenariosonly      := $scenariosonly = true()
    let $treetype           := $treetype[not(. = '')]
    
    let $datasets           := 
        if (empty($searchTerms)) then 
            if (empty($projectPrefix)) then () else utillib:getDatasets($projectPrefix, $projectVersion, $projectLanguage)
        else (
            dsapi:searchDataset($projectPrefix, $searchTerms, $projectVersion, $projectLanguage, $includeBBR)
        )
    let $allcnt             := count($datasets)
    
    let $datasets           := 
        (: if we aren't searching, and we are in 'current' project scope, and we are asked for inclusion of BBRs :)
        if (empty($searchTerms) and not(empty($projectPrefix)) and empty($projectVersion) and $includeBBR) then 
            $datasets | utillib:getDatasets((), (), ())
        else (
            $datasets
        )
        
    let $datasets       :=
        if ($scenariosonly) then
            if (empty($projectPrefix)) then 
                $datasets[@id = ancestor::decor//representingTemplate/@sourceDataset]
            else (
                let $decor  := utillib:getDecorByPrefix($projectPrefix, $projectVersion, $projectLanguage)
                return $datasets[@id = $decor//representingTemplate/@sourceDataset]
            )
        else (
            $datasets
        )
    
    let $datasets        :=
        for $ds in $datasets
        return
        element {name($ds)} {
            $ds/@*,
            $ds/name,
            $ds/desc,
            if ($ds[@id]) then 
                if ($ds[publishingAuthority]) then () else utillib:inheritPublishingAuthorityFromProject($ds/ancestor::decor)
            else ()
            ,
            $ds/(* except (name | desc))
         }
    
    let $datasetnames           := 
        for $name in $datasets/name
        let $versionLabel   := $name/../@versionLabel
        return
            string-join(($name, $versionLabel, $name/@language), '')
    let $datasetnameentries     :=
        for $datasetname in $datasetnames
        let $dsnm   := $datasetname
        group by $dsnm
        return map:entry($datasetname[1], count($datasetname))
    let $datasetnamemap         := map:merge($datasetnameentries)
    
    let $results                := 
        for $datasetById in $datasets
        let $ds-id          := $datasetById/@id
        group by $ds-id
        (:assumption is that newest comes after oldest. cheaper than date calculations:)
        order by ($datasetById[last()]/name[@language = $projectLanguage], $datasetById[last()]/name)[1], $ds-id descending
        return (
            for $dataset in $datasetById
            let $statusCode     := 
                if ($dataset/@statusCode) then $dataset/@statusCode else (
                    if ($dataset//concept[@statusCode = ('draft','new','review', 'pending')]) then
                        'draft'
                    else (
                        'final'
                    )
                )
            let $versionLabel   := $dataset/@versionLabel
            order by $dataset/@effectiveDate descending
            return
                <dataset>
                {
                    $dataset/@id,
                    $dataset/@effectiveDate,
                    attribute statusCode {$statusCode},
                    $dataset/@versionLabel,
                    $dataset/@expirationDate,
                    $dataset/@officialReleaseDate,
                    $dataset/@canonicalUri,
                    $dataset/@lastModifiedDate,
                    attribute ident {$dataset/ancestor::decor/project/@prefix},
                    attribute uuid {util:uuid()}
                }
                {
                    for $name in $dataset/name[exists(node())][.//text()[not(normalize-space() = '')]]
                    let $label      := if ($versionLabel) then concat(' (',$versionLabel,')') else ()
                    let $labeldate  := 
                        if ($dataset/@effectiveDate castable as xs:dateTime) then 
                            replace(format-dateTime(xs:dateTime($dataset/@effectiveDate),'[Y0001]-[M01]-[D01] [H01]:[m01]:[s01]', (), (), ()),' 00:00:00','')
                        else ()
                    (: functional people find the date attached to the name in the selector/drop down list in the UI confusing
                        and it is not necessary when the name for a given language is unique enough. Often the effectiveDate
                        is not very helpful at all and rather random. So we generate a language dependent name specifically for 
                        the drop down selector that only concatenates the effectiveDate when the name for the given language is 
                        not unique in itself.
                    :)
                    let $selectorName   := string-join(($name, $versionLabel, $name/@language), '')
                    let $selectorName   := 
                        if (map:get($datasetnamemap, $selectorName) gt 1) then 
                            concat($name, $label, ' :: ', $labeldate)
                        else (
                            concat($name, $label)
                        )
                    return
                        <name>
                        {
                            (: use this for the drop down selector :)
                            $name/(@* except @selectorName)
                            ,
                            attribute selectorName {$selectorName}
                            ,
                            $name/node()
                        }
                        </name>
                }
                {
                     $dataset/(* except (name|concept|history|recycle))[exists(node())][.//text()[not(normalize-space() = '')]]
                }
                </dataset>
            )
    
    let $count              := count($results)
    let $max                := $count(:if ($max ge 1) then $max else $dsapi:maxResults:)
    
    return
        <list artifact="DS" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements(subsequence($results, 1, $max))
        }
        </list>
};

(:~ Central logic for updating an existing dataset

@param $authmap         - required. Map derived from token
@param $id              - required. DECOR dataset/@id to update
@param $effectiveDate   - required. DECOR dataset/@effectiveDate to update
@param $data            - required. DECOR concept xml element containing everything that should be in the updated dataset
@return concept object as xml with json:array set on elements
:)
declare function dsapi:putDataset($authmap as map(*), $id as xs:string, $effectiveDate as xs:string, $data as element(dataset)) {

    let $editedDataset          := $data
    let $storedDataset          := utillib:getDataset($id, $effectiveDate)
    let $decor                  := $storedDataset/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix

    let $check                  :=
        if ($storedDataset) then () else (
            error($errors:BAD_REQUEST, 'Dataset with id ' || $id || ' and effectiveDate ' || $effectiveDate || ' does not exist')
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-DATASETS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify datasets in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if ($storedDataset/@statusCode = $dsapi:STATUSCODES-FINAL) then
            if ($editedDataset/@statusCode = $dsapi:STATUSCODES-FINAL) then    
                error($errors:BAD_REQUEST, concat('Dataset cannot be edited in status ', $storedDataset/@statusCode, '. ', if ($storedDataset/@statusCode = 'pending') then 'You should switch back to status draft to edit.' else ()))
            else ()
        else ()
    let $check                  :=
        if ($data[@id = $id] | $data[empty(@id)]) then () else (
            error($errors:BAD_REQUEST, concat('Dataset SHALL have the same id as the dataset id ''', $id, ''' used for updating. Found in request body: ', ($data/@id, 'null')[1]))
        )
    let $check                  :=
        if ($data[@effectiveDate = $effectiveDate] | $data[empty(@effectiveDate)]) then () else (
            error($errors:BAD_REQUEST, concat('Dataset SHALL have the same effectiveDate as the dataset effectiveDate ''', $effectiveDate, ''' used for updating. Found in request body: ', ($data/@effectiveDate, 'null')[1]))
        )
       
    let $check                  :=
        for $node in $data/relationship[@ref[not(. = '')]]
        let $target             := utillib:getDataset($node/@ref, $node/@flexibility)
        return
            if ($storedDataset[@id = $target/@id][@effectiveDate = $target/@effectiveDate]) then
                'Relationship with ref ''' || $node/@ref || ''' flexibility ''' || $node/@flexibility || ''' on the dataset SHALL point to a different (version of the) dataset, i.e. SHALL NOT reference itself.'
            else
            if ($target[name() = 'dataset']) then () else (
                'Relationship with ref ''' || $node/@ref || ''' flexibility ''' || $node/@flexibility || ''' on the dataset SHALL point to a dataset.'
            )
    
    let $movelock               :=
        for $c in $editedDataset/descendant-or-self::concept[move] | 
                  $editedDataset/descendant-or-self::concept[edit/@mode='delete']
        let $storedConcept      := $storedDataset//concept[@id = $c/@id][@effectiveDate = $c/@effectiveDate]
        let $locks              := decorlib:getLocks($authmap, $storedConcept, false())
        let $locks              := if ($locks) then $locks else decorlib:setLock($authmap, $storedConcept, false())
        return
            if ($locks) then ($locks) else (
                error($errors:BAD_REQUEST, concat('Dataset id=''',$id,''' and effectiveDate=''',$effectiveDate,''', concept id=''',$c/@id,''' and effectiveDate=''',$c/@effectiveDate,''' does not have a lock for current user (anymore) and it cannot be obtained.'))
            )
    let $editlock               :=
        for $c in $editedDataset/descendant-or-self::concept[edit[not(@mode = 'delete')]]
        let $storedConcept      := $storedDataset//concept[@id = $c/@id][@effectiveDate = $c/@effectiveDate]
        let $locks              := decorlib:getLocks($authmap, $storedConcept, false())
        let $locks              := if ($locks) then $locks else decorlib:setLock($authmap, $storedConcept, false())
        return (
            if ($locks) then ($locks) else (
                error($errors:BAD_REQUEST, concat('Dataset id=''',$id,''' and effectiveDate=''',$effectiveDate,''', concept id=''',$c/@id,''' and effectiveDate=''',$c/@effectiveDate,''' does not have a lock for current user (anymore) and it cannot be obtained.'))
            ),
            if (empty($storedConcept)) then
                error($errors:BAD_REQUEST, 'Concept ' || $c/@id || ' effectiveDate ''' || $c/@effectiveDate || ''' SHALL exist in stored dataset on the server. Cannot update non-existent concept.')
            else ()
            (:,
            if (empty(decorlib:getLocks($authmap, $storedConcept, false()))) then
                if (empty(decorlib:setLock($authmap, $storedConcept, false()))) then
                    error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have a lock for concept ' || $c/@id || ' (anymore). Get a lock first.'))
                else ()
            else ():)
            ,
            if (utillib:isStatusChangeAllowable($storedConcept, $c/@statusCode)) then () else (
                error($errors:BAD_REQUEST, 'Concept ' || $c/@id || ' SHALL have the same statusCode ''' || $storedConcept/@statusCode || ''' or a supported status change. Found in request body: ''' || ($c/@statusCode, 'null')[1] || '''')
            )
            ,
            if ($c[not(name | inherit | contains)]) then
                error($errors:BAD_REQUEST, 'Concept ' || $c/@id || ' SHALL have at least one name if it does not inherit or contains')
            else ()
            ,
            if ($c/valueDomain/conceptList/concept[not(name)]) then
                error($errors:BAD_REQUEST, 'Concept ' || $c/@id || ' conceptList.concept ' || string-join($c/valueDomain/conceptList/concept[not(name)]/@id, ', ') || ' SHALL have at least one name if it does not inherit or contains')
            else ()
            ,
            if ($c/valueDomain/conceptList/concept[inherit | contains]) then 
                error($errors:BAD_REQUEST, 'Concept ' || $c/@id || ' conceptList.concept SHALL NOT have inherit or contains')
            else ()
            ,
            if ($c[@type = $deapi:CONCEPT-TYPE/enumeration/@value]) then () else (
                error($errors:BAD_REQUEST, 'Concept ' || $c/@id || ' type ' || $c/@type || ' SHALL be one of ' || string-join($deapi:CONCEPT-TYPE/enumeration/@value, ', '))
            )
            ,
            if ($c[not(@type = 'item')] | $c[@type = 'item'][inherit | contains | valueDomain]) then () else (
                error($errors:BAD_REQUEST, 'Concept ' || $c/@id || ' type ' || $c/@type || ' SHALL have an inherit, a contains or valueDomain.')
            )
            ,
            if ($c/inherit[not(@ref = $storedConcept/inherit/@ref)]) then
                if ($c/inherit[utillib:isOid(@ref)][@effectiveDate castable as xs:dateTime]) then
                    if (exists(utillib:getConcept($c/inherit/@ref, $c/inherit/@effectiveDate))) then () else (
                        error($errors:BAD_REQUEST, 'Concept ' || $c/@id || ' inherit SHALL point to an existing concept. Found: ref=''' || $c/inherit/@ref || ''' effectiveDate=''' || $c/inherit/@effectiveDate || '''')
                    )
                else (
                    error($errors:BAD_REQUEST, 'Concept ' || $c/@id || ' inherit SHALL have both ref as oid and effectiveDate as dateTime. Found: ref=''' || $c/inherit/@ref || ''' effectiveDate=''' || $c/inherit/@effectiveDate || '''')
                )
            else ()
            ,
            if ($c/contains[not(@ref = $storedConcept/contains/@ref)]) then
                if ($c/contains[utillib:isOid(@ref)][@flexibility castable as xs:dateTime]) then
                    if (exists(utillib:getConcept($c/contains/@ref, $c/contains/@flexibility))) then () else (
                        error($errors:BAD_REQUEST, 'Concept ' || $c/@id || ' contains SHALL point to an existing concept. Found: ref=''' || $c/contains/@ref || ''' flexibility=''' || $c/contains/@flexibility || '''')
                    )
                else (
                    error($errors:BAD_REQUEST, 'Concept ' || $c/@id || ' contains SHALL have both ref as oid and flexibility as dateTime. Found: ref=''' || $c/contains/@ref || ''' effectiveDate=''' || $c/contains/@flexibility || '''')
                )
            else ()
            ,
            let $checkVal               := $c/valueDomain[not(ancestor::concept[1][inherit | contains])][not(@type = $deapi:VALUEDOMAIN-TYPE/enumeration/@value)]/@type
            return
                if (empty($checkVal)) then () else (
                    error($errors:BAD_REQUEST, 'Concept ' || $c/@id || ' valueDomain type ''' || string-join(distinct-values($checkVal), ''', ''') || ''' SHALL be one of ' || string-join($deapi:VALUEDOMAIN-TYPE/enumeration/@value, ', '))
                )
            ,
            let $checkVal               := $c/valueDomain[not(ancestor::concept[1][inherit | contains])]/example[not(@type = $deapi:EXAMPLE-TYPE/enumeration/@value)]/@type
            return
                if (empty($checkVal)) then () else (
                    error($errors:BAD_REQUEST, 'Concept ' || $c/@id || ' valueDomain example type ''' || string-join(distinct-values($checkVal), ''', ''') || ''' SHALL be one of ' || string-join($deapi:EXAMPLE-TYPE/enumeration/@value, ', '))
                )
            ,
            for $node in $c/relationship[@ref[not(. = '')]]
            let $target   := utillib:getConcept($node/@ref, $node/@flexibility)
            return
                if ($storedDataset[@id = $target/@id][@effectiveDate = $target/@effectiveDate]) then
                    error($errors:BAD_REQUEST, 'Concept ' || $c/@id || ' relationship ref ''' || $node/@ref || ''' flexibility ''' || $node/@flexibility || '''. Relationship on a concept SHALL point to a different (version of the) concept, i.e. SHALL NOT reference itself.')
                else
                if ($target[name() = 'concept']) then () else (
                    error($errors:BAD_REQUEST, 'Concept ' || $c/@id || ' relationship ref ''' || $node/@ref || ''' flexibility ''' || $node/@flexibility || '''. Relationship on a concept SHALL point to a different (version of the) concept.')
                )
            ,
            if (empty($c/valueDomain[not(ancestor::concept[1][inherit | contains])][@type = 'code'][not(conceptList[@id | @ref])])) then () else (
                error($errors:BAD_REQUEST, 'Concept with coded valueDomain SHALL have a conceptList with an id or ref')
            )
            ,
            for $clid in $c/valueDomain[not(ancestor::concept[1][inherit | contains])]/conceptList[not(@ref = $storedConcept//valueDomain/conceptList/@ref)]/@ref
            return
                if (utillib:getConceptList($clid, $projectPrefix)) then () else (
                    error($errors:BAD_REQUEST, 'Concept ' || $c/@id || ' valueDomain conceptList with ref ' || $clid || ' SHALL refer to a conceptList in this project ' || $projectPrefix)
                )
            ,
            for $clid in $c/valueDomain[not(ancestor::concept[1][inherit | contains])]/conceptList[not(@id = $storedConcept//valueDomain/conceptList/@id)]/@id
            return
                if (utillib:getConceptList($clid, ())) then
                    error($errors:BAD_REQUEST, 'Concept ' || $c/@id || ' valueDomain conceptList with new id ' || $clid ||' SHALL NOT already exist anywhere')
                else ()
            ,
            for $clid in $c/valueDomain[not(ancestor::concept[1][inherit | contains])]/conceptList/concept[not(@id = $storedConcept//valueDomain/conceptList/concept/@id)]/@id
            return
                if (utillib:getConceptListConcept($clid, ())) then
                    error($errors:BAD_REQUEST, 'Concept ' || $c/@id || ' valueDomain conceptList concept with new id ' || $clid ||' SHALL NOT already exist anywhere')
                else ()
            ,
            for $clid in $storedConcept//valueDomain/conceptList[not(@id = $c/valueDomain/conceptList/@id)]/@id
            let $referred           := utillib:getConceptListRef($clid, $projectPrefix)
            return
                if ($referred) then
                    error($errors:BAD_REQUEST, 'Concept ' || $c/@id || ' uses a new valueDomain conceptList id ' || $clid ||' but this is still being referred to by ' || count($referred) || ' concepts. Referring concepts: ' || string-join($referred/concat('concept ', @id, ' / ', @effectiveDate), ', '))
                else ()
        )
    
    (: save history:)
    let $intention                      :=
        if ($storedDataset[@statusCode = 'final']) then 'patch' else 'version'
    let $history                        :=
        if ($storedDataset) then 
            histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-DATASET, $projectPrefix, $intention, $storedDataset)
        else ()
        
    (: start with deleting concepts:
       if statusCode=new     delete concept
       if statusCode!=new    set statusCode=deprecated
    :)
    let $debug := if ($dsapi:DEBUG > 0) then util:log('INFO', 'putDataset: delete concepts ' || count($editedDataset//concept[edit/@mode='delete'])) else ()
    let $deletes :=
        for $concept in $editedDataset//concept[edit/@mode='delete']
        let $storedConcept := $storedDataset//concept[@id = $concept/@id][@effectiveDate = $concept/@effectiveDate][not(ancestor::history)]
        return
            if ($editedDataset/@statusCode != 'new') then
                if ($storedConcept/@statusCode = 'new') then (
                    histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-DATASETCONCEPT, $projectPrefix, 'delete', $storedConcept),
                    update delete $storedConcept
                )
                else (
                    if ($concept/@type='item') then (
                        histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-DATASETCONCEPT, $projectPrefix, 'delete', $storedConcept),
                        update replace $storedConcept with deapi:prepareItemForUpdate($concept, $storedConcept)
                    )
                    else if ($concept/@type='group') then (
                        histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-DATASETCONCEPT, $projectPrefix, 'delete', $storedConcept),
                        update replace $storedConcept with deapi:prepareGroupForUpdate($concept, $storedConcept)
                    ) else ()
                )
            else (
                histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-DATASETCONCEPT, $projectPrefix, 'delete', $storedConcept),
                update delete $storedConcept
            )
    (:
       move concepts
       - local move
          - delete stored concept
          - insert moved concept after editedDataset preceding-sibling or into parent dataset/concept
       - move to other dataset (unsupported!!)
    :)
    let $debug := if ($dsapi:DEBUG > 0) then util:log('INFO', 'putDataset: move concepts ' || count($editedDataset//concept[move])) else ()
    let $moves :=
        for $concept in $editedDataset//concept[move]
        let $de-id              := $concept/@id
        let $de-ed              := $concept/@effectiveDate
        let $storedConcept      := $storedDataset//concept[@id = $de-id][@effectiveDate = $de-ed][not(ancestor::history)]
        
        let $debug := if ($dsapi:DEBUG > 0) then util:log('INFO', 'putDataset: move concept ' || $de-id) else ()
        
        let $sourceHistory      := $storedConcept/parent::*
        let $sourceType         := if (name($sourceHistory) = 'dataset') then $decorlib:OBJECTTYPE-DATASET else $decorlib:OBJECTTYPE-DATASETCONCEPT
        let $sourceIntention    := if ($concept[edit/@mode = 'edit']) then 'move/version' else 'move'
        
        let $check              :=
            if (count($storedConcept)=1) then () else (
                error($errors:SERVER_ERROR, concat('Concept id=''',$de-id,''' and effectiveDate=''',$de-ed,''' gave ',count($storedConcept),' stored concepts. Expected exactly 1.', if (count($storedConcept)=0) then () else concat(' Found concept in projects: ',string-join(for $s in $storedConcept return $s/ancestor::decor/project/@prefix,' / '))))
            )
        (:  if move + edit: use what was sent to us
            if just move: use what we already had stored on the server
        :)
        let $preparedConcept    :=
            if ($concept[@type = 'item']) then
                if ($concept[edit/@mode='edit']) then
                    deapi:prepareItemForUpdate($concept, $storedConcept)
                else
                    deapi:prepareItemForUpdate($storedConcept, $storedConcept)
            else
            if ($concept[@type = 'group']) then
                if ($concept[edit/@mode='edit']) then
                    deapi:prepareGroupForUpdate($concept, $storedConcept)
                else
                    deapi:prepareGroupForUpdate($storedConcept, $storedConcept)
            else (
                (:huh? ... :)
            )
        let $check              :=
            if (count($preparedConcept)=1) then () else (
                error($errors:SERVER_ERROR, concat('Concept id=''',$de-id,''' and effectiveDate=''',$de-ed,''' has unknown type ',$concept/@type,'. Expected item or group.'))
            )
        return
            <update>
            {
                $de-id,
                $de-ed,
                (: preceding-concept should already be at the correct position as it was processed in the previous 
                   iteration of the 'for' loop
                :)
                if ($concept[preceding-sibling::concept]) then (
                    let $destHistory    := $storedDataset//concept[@id = $concept/@id][not(ancestor::history)]/parent::*
                    let $destType       := if (name($destHistory) = 'dataset') then $decorlib:OBJECTTYPE-DATASET else $decorlib:OBJECTTYPE-DATASETCONCEPT
                    return (
                        histlib:AddHistory($authmap?name, $sourceType, $projectPrefix, $sourceIntention, $sourceHistory),
                        histlib:AddHistory($authmap?name, $destType, $projectPrefix, $sourceIntention, $destHistory),
                        update insert $preparedConcept following $storedDataset//concept[@id = $concept/preceding-sibling::concept[1]/@id][not(ancestor::history)]
                    )
                )
                else 
                if ($concept[parent::dataset]) then (
                    if ($storedDataset[concept | history]) then (
                        let $destHistory    := $storedDataset
                        let $destType       := if (name($destHistory) = 'dataset') then $decorlib:OBJECTTYPE-DATASET else $decorlib:OBJECTTYPE-DATASETCONCEPT
                        return (
                            histlib:AddHistory($authmap?name, $sourceType, $projectPrefix, $sourceIntention, $sourceHistory),
                            histlib:AddHistory($authmap?name, $destType, $projectPrefix, $sourceIntention, $destHistory),
                            (: top level concept in dataset, but others already exist in stored dataset :)
                            update insert $preparedConcept preceding ($storedDataset/concept | $storedDataset/history)[1]
                        )
                    )
                    else (
                        let $destHistory    := $storedDataset
                        let $destType       := if (name($destHistory) = 'dataset') then $decorlib:OBJECTTYPE-DATASET else $decorlib:OBJECTTYPE-DATASETCONCEPT
                        return (
                            histlib:AddHistory($authmap?name, $sourceType, $projectPrefix, $sourceIntention, $sourceHistory),
                            histlib:AddHistory($authmap?name, $destType, $projectPrefix, $sourceIntention, $destHistory),
                            (: top level concept in dataset, and no others exist in stored dataset :)
                            update insert $preparedConcept into $storedDataset
                        )
                    )
                )
                else 
                if ($concept[parent::concept] and $storedDataset//concept[@id = $concept/parent::concept/@id][not(ancestor::history)]) then (
                    let $storedParent   := $storedDataset//concept[@id = $concept/parent::concept/@id][not(ancestor::history)]
                    let $destHistory    := $storedParent
                    let $destType       := if (name($destHistory) = 'dataset') then $decorlib:OBJECTTYPE-DATASET else $decorlib:OBJECTTYPE-DATASETCONCEPT
                    return (
                        histlib:AddHistory($authmap?name, $sourceType, $projectPrefix, $sourceIntention, $sourceHistory),
                        histlib:AddHistory($authmap?name, $destType, $projectPrefix, $sourceIntention, $destHistory),
                        if ($storedParent[concept | history]) then
                            (: first concept in concept group, but others already exist in stored dataset concept group :)
                            update insert $preparedConcept preceding ($storedParent/concept | $storedParent/history)[1]
                        else (
                            (: first concept in concept group, and no others exist in stored dataset concept group :)
                            update insert $preparedConcept into $storedParent
                        )
                    )
                )
                else (
                    error(QName('http://art-decor.org/ns/art/dataset', 'ContextNotFound'), concat('Could not determine context for saving concept with id ',$concept/@id,' and name ',$concept/name[1]))
                )
                ,
                update delete $storedConcept
            }
            </update>
    
    let $debug := if ($dsapi:DEBUG > 0) then util:log('INFO', 'putDataset: update concepts ' || count($editedDataset//concept[edit/@mode='edit'][not(move)])) else ()
    let $updates :=
        for $concept in $editedDataset//concept[edit/@mode='edit'][not(move)]
        let $de-id              := $concept/@id
        let $de-ed              := $concept/@effectiveDate
        let $storedConcept      := $storedDataset//concept[@id = $de-id][@effectiveDate = $de-ed][not(ancestor::history)]
        let $check              :=
            if (count($storedConcept)=1) then () else (
                error($errors:SERVER_ERROR, concat('Concept id=''',$de-id,''' and effectiveDate=''',$de-ed,''' gave ',count($storedConcept),' stored concepts. Expected exactly 1.', if (count($storedConcept)=0) then () else concat(' Found concept in projects: ',string-join(for $s in $storedConcept return $s/ancestor::decor/project/@prefix,' / '))))
            )
        let $preparedConcept    :=
            if ($concept[@type = 'item']) then
                deapi:prepareItemForUpdate($concept,$storedConcept)
            else if ($concept[@type = 'group']) then
                deapi:prepareGroupForUpdate($concept,$storedConcept)
            else (
                (:huh? ... :)
            )
        let $check              :=
            if (count($preparedConcept)=1) then () else (
                error($errors:SERVER_ERROR, concat('Concept id=''',$de-id,''' and effectiveDate=''',$de-ed,''' has unknown type ',$concept/@type,'. Expected item or group.'))
            )
        return
            <update>
            {
                $de-id, $de-ed,
                histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-DATASETCONCEPT, $projectPrefix, if ($storedConcept[@statusCode = 'final']) then 'patch' else 'version', $storedConcept),
                update replace $storedConcept with $preparedConcept
            }
            </update>
    
    let $datasetNameUpdate := utillib:prepareFreeFormMarkupWithLanguageForUpdate($editedDataset/name)
    let $datasetDescUpdate := utillib:prepareFreeFormMarkupWithLanguageForUpdate($editedDataset/desc)
    
    (:new since 2015-04-21:)
    let $datasetPropUpdate :=
        for $property in $editedDataset/property[@name[not(.='')]]
        return
            utillib:parseNode(<property>{$property/@name, $property/node()}</property>)
    (:new since 2015-04-21:)
    let $datasetRelsUpdate :=
        for $relationship in $editedDataset/relationship[string-length(@ref) gt 0]
        return
        <relationship>{$relationship/@type[not(. = '')], $relationship/@ref[not(.='')], $relationship/@flexibility[not(.='')]}</relationship>
    let $debug := if ($dsapi:DEBUG > 0) then util:log('INFO', 'putDataset: update dataset' || count($storedDataset)) else ()
    let $datasetUpdate :=
        (
            update delete $storedDataset/(name|desc|property|relationship),
            if ($storedDataset[*]) then
                update insert ($datasetNameUpdate|$datasetDescUpdate|$datasetPropUpdate|$datasetRelsUpdate) preceding $storedDataset/*[1]
            else (
                update insert ($datasetNameUpdate|$datasetDescUpdate|$datasetPropUpdate|$datasetRelsUpdate) into $storedDataset
            )
            ,
            update value $storedDataset/@statusCode with $editedDataset/@statusCode,
            if ($editedDataset/@versionLabel[string-length(normalize-space()) gt 0]) then (
                if ($storedDataset/@versionLabel) then 
                    update value $storedDataset/@versionLabel with $editedDataset/@versionLabel
                else (
                    update insert attribute versionLabel {$editedDataset/@versionLabel} into $storedDataset
                )
            ) else (
                update delete $storedDataset/@versionLabel
            ),
            if ($editedDataset/@canonicalUri[string-length(normalize-space()) gt 0]) then (
                if ($storedDataset/@canonicalUri) then 
                    update value $storedDataset/@canonicalUri with $editedDataset/@canonicalUri
                else (
                    update insert attribute canonicalUri {$editedDataset/@canonicalUri} into $storedDataset
                )
            ) else (
                update delete $storedDataset/@canonicalUri
            )
        )
    
    (:store any terminologyAssociations that were saved in the dataset after deInherit
        only store stuff where the concept(List) still exists
    :)
    let $terminologyAssociations        := ($editedDataset//terminologyAssociation[ancestor::concept/edit/@mode='edit'][not(ancestor::history)] |
                                            $editedDataset//terminologyAssociation[ancestor::concept[move]][not(ancestor::history)])
    let $terminologyAssociationUpdates  :=
        utillib:addTerminologyAssociations($terminologyAssociations, $decor, false(), false())
    
    (:store any identifierAssociations that were saved in the dataset after deInherit
        only store stuff where the concept(List) still exists
    :)
    let $identifierAssociations         := ($editedDataset//identifierAssociation[ancestor::concept/edit/@mode='edit'][not(ancestor::history)] |
                                           $editedDataset//identifierAssociation[ancestor::concept[move]][not(ancestor::history)])
    let $identifierAssociationUpdates   :=
        utillib:addIdentifierAssociations($identifierAssociations, $decor, false(), false())
    
    let $deleteLock                     := update delete ($editlock | $movelock)
    
    return
        ()
};

(:~ Central logic for patching an existing dataset

@param $authmap         - required. Map derived from token
@param $id              - required. DECOR dataset/@id to update
@param $effectiveDate   - required. DECOR dataset/@effectiveDate to update
@param $data            - required. DECOR xml element parameter elements each containing RFC 6902 compliant contents
@return dataset object as xml with json:array set on elements
:)
declare function dsapi:patchDataset($authmap as map(*), $id as xs:string, $effectiveDate as xs:string, $data as element(parameters)) as element(dataset) {

    let $storedDataset          := utillib:getDataset($id, $effectiveDate)
    let $decor                  := $storedDataset/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix

    let $lock                   := decorlib:getLocks($authmap, $id, $effectiveDate, ())
    let $lock                   := if ($lock) then $lock else decorlib:setLock($authmap, $id, $effectiveDate, false())
    
    let $check                  :=
        if ($storedDataset) then () else (
            error($errors:BAD_REQUEST, 'Dataset with id ' || $id || ' and effectiveDate ' || $effectiveDate || ' does not exist')
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-DATASETS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify datasets in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if ($storedDataset[@statusCode = $dsapi:STATUSCODES-FINAL]) then
            if ($data/parameter[@path = '/statusCode'][@op = 'replace'][not(@value = $dsapi:STATUSCODES-FINAL)]) then () else
            if ($data[count(parameter) = 1]/parameter[@path = '/statusCode']) then () else (
                error($errors:BAD_REQUEST, concat('Dataset cannot be patched while it has one of status: ', string-join($dsapi:STATUSCODES-FINAL, ', '), if ($storedDataset/@statusCode = 'pending') then 'You should switch back to status draft to edit.' else ()))
            )
        else ()
    let $check                  :=
        if ($lock) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have a lock for this dataset (anymore). Get a lock first.'))
        )
    let $check                  :=
        if ($data[parameter]) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall be an array of ''parameter''.')
        )
    let $unsupportedops         := $data/parameter[not(@op = ('add', 'replace', 'remove'))]
    let $check                  :=
        if ($unsupportedops) then
            error($errors:BAD_REQUEST, 'Submitted parameters shall have supported ''op'' value. Found ''' || string-join(distinct-values($unsupportedops/@op), ''', ''') || ''', expected ''add'', ''replace'', or ''remove''') 
        else ()
    let $checkn                 := (
        if ($data/parameter[@path = '/name']) then
            if (count($storedDataset/name) - count($data/parameter[@op = 'remove'][@path = '/name']) + count($data/parameter[@op = ('add', 'replace')][@path = '/name']) ge 1) then () else (
                'A dataset SHALL have at least one name. You cannot remove every name.'
            )
        else (),
        if ($data/parameter[@path = '/desc']) then
            if (count($storedDataset/desc) - count($data/parameter[@op = 'remove'][@path = '/desc']) + count($data/parameter[@op = ('add', 'replace')][@path = '/desc']) ge 1) then () else (
                'A dataset SHALL have at least one description. You cannot remove every desc.'
            )
        else ()
    )
    
    let $check                  :=
        for $param in $data/parameter
        let $op         := $param/@op
        let $path       := $param/@path
        let $value      := $param/@value
        let $pathpart   := substring-after($path, '/')
        return
            switch ($path)
            case '/statusCode' return (
                if ($op = 'remove') then
                    'Parameter path ''' || $path || ''' does not support ' || $op
                else 
                if (utillib:isStatusChangeAllowable($storedDataset, $value)) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Supported are: ' || string-join(map:get($utillib:itemstatusmap, string($storedDataset/@statusCode)), ', ')
                )
            )
            case '/expirationDate'
            case '/officialReleaseDate' return (
                if ($op = 'remove') then () else 
                if ($value castable as xs:dateTime) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL be yyyy-mm-ddThh:mm:ss.'
                )
            )
            case '/canonicalUri'
            case '/versionLabel' return (
                if ($op = 'remove') then () else 
                if ($value[not(. = '')]) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL NOT be empty.'
                )
            )
            case '/name' 
            case '/desc' 
            case '/comment'
            case '/copyright' return (
                if ($param[count(value/*[name() = $pathpart]) = 1]) then (
                    if ($param/value/*[name() = $pathpart][@inherited]) then 
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Contents SHALL NOT be marked ''inherited''.'
                    else (),
                    if ($param/value/*[name() = $pathpart][matches(@language, '[a-z]{2}-[A-Z]{2}')]) then 
                        if ($param[@op = 'remove'] | $param/value/*[name() = $pathpart][.//text()[not(normalize-space() = '')]]) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A ' || $pathpart || ' SHALL have contents.'
                        )
                    else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A ' || $pathpart || ' SHALL have a language with pattern ll-CC.'
                    )
                )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/*[name() = $pathpart]) 
                )
            )
            case '/publishingAuthority' return (
                if ($param[count(value/publishingAuthority) = 1]) then (
                    if ($param/value/publishingAuthority[@inherited]) then 
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Contents SHALL NOT be marked ''inherited''.'
                    else (),
                    if ($param/value/publishingAuthority[@id]) then
                        if ($param/value/publishingAuthority[utillib:isOid(@id)]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || ''' publishingAuthority.id SHALL be an OID is present. Found ' || string-join($param/value/publishingAuthority/@id, ', ')
                        )
                    else (),
                    if ($param/value/publishingAuthority[@name[not(. = '')]]) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || ''' publishingAuthority.name SHALL be a non empty string.'
                    ),
                    let $unsupportedtypes := $param/value/publishingAuthority/addrLine/@type[not(. = $dsapi:ADDRLINE-TYPE/enumeration/@value)]
                    return
                        if ($unsupportedtypes) then
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Publishing authority has unsupported addrLine.type value(s) ' || string-join($unsupportedops, ', ') || '. SHALL be one of: ' || string-join($dsapi:ADDRLINE-TYPE/enumeration/@value, ', ')
                        else ()
                ) else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' SHALL have a single publishingAuthority under value. Found ' || count($param/value/publishingAuthority)
                )
            )
            case '/property' return (
                if ($param[count(value/property) = 1]) then
                    if ($param/value/property/@name[not(. = '')]) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || '''. Property.name SHALL be a non empty string.'
                    )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/property) 
                ),
                if ($param/value/property[@datatype]) then 
                    if ($param/value/property[@datatype = $dsapi:VALUEDOMAIN-TYPE/enumeration/@value]) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || ''' with type ''' || string-join($param/value/property/@datatype, ' ') || '''. Supported are: ' || string-join($dsapi:VALUEDOMAIN-TYPE/enumeration/@value, ', ')
                    )
                else ()
            )
            case '/relationship' return (
                if ($op = 'remove') then () else (
                    if ($param/value/relationship[@type = $deapi:RELATIONSHIP-TYPE/enumeration/@value]) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || ''' with type ''' || string-join($param/value/relationship/@type, ' ') || '''. Supported are: ' || string-join($deapi:RELATIONSHIP-TYPE/enumeration/@value, ', ')
                    ),
                    if ($param/value/relationship[@ref[utillib:isOid(.)]]) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || ''' with ref ''' || string-join($param/value/relationship/@ref, ' ') || '''. Relationship/ref SHALL be an OID.'
                    ),
                    if ($param/value/relationship[empty(@flexibility) or @flexibility castable as xs:dateTime or @flexibility = 'dynamic']) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || ''' with flexibility ''' || string-join($param/value/relationship/@flexibility, ' ') || '''. Relationship/flexibility SHALL be omitted, yyyy-MM-DDThh:mm:ss or ''dynamic''.'
                    ),
                    for $node in $param/value/relationship[@ref[not(. = '')]]
                    let $target   := utillib:getDataset($node/@ref, $node/@flexibility)
                    return
                        if ($storedDataset[@id = $target/@id][@effectiveDate = $target/@effectiveDate]) then
                            'Parameter ' || $op || ' not allowed for ''' || $path || ''' with ref ''' || $node/@ref || ''' flexibility ''' || $node/@flexibility || '''. Relationship on a dataset SHALL point to a different (version of the) dataset, i.e. SHALL NOT reference itself.'
                        else
                        if ($target[name() = 'dataset']) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || ''' with ref ''' || $node/@ref || ''' flexibility ''' || $node/@flexibility || '''. Relationship on a dataset SHALL point to a dataset.'
                        )
                )
            )
            default return (
                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Path value not supported'
            )
     let $check                 :=
        if (empty($checkn) and empty($check)) then () else (
            error($errors:BAD_REQUEST,  string-join (($checkn, $check), ' '))
        )
    
    let $intention              := if ($storedDataset[@statusCode = 'final']) then 'patch' else 'version'
    let $update                 := histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-DATASET, $projectPrefix, $intention, $storedDataset)
    
    let $update                 :=
        for $param in $data/parameter
        return
            switch ($param/@path)
            case '/statusCode'
            case '/expirationDate'
            case '/officialReleaseDate'
            case '/canonicalUri'
            case '/versionLabel' return (
                let $attname  := substring-after($param/@path, '/')
                let $new      := attribute {$attname} {$param/@value}
                let $stored   := $storedDataset/@*[name() = $attname]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedDataset
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/name' 
            case '/desc' return (
                (: only one per language :)
                let $elmname  := substring-after($param/@path, '/')
                let $new      := utillib:prepareFreeFormMarkupWithLanguageForUpdate($param/value/*[name() = $elmname])
                let $stored   := $storedDataset/*[name() = $elmname][@language = $param/value/*[name() = $elmname]/@language]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedDataset
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/relationship' return (
                let $new      := utillib:prepareDatasetRelationshipForUpdate($param/value/relationship)
                let $stored   := $storedDataset/relationship[@ref = $new/@ref]
                let $stored   := if ($new/@flexibility) then $stored[@flexibility = $new/@flexibility] else $stored
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedDataset
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/publishingAuthority' return (
                (: only one possible :)
                let $new      := utillib:preparePublishingAuthorityForUpdate($param/value/publishingAuthority)
                let $stored   := $storedDataset/publishingAuthority
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedDataset
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/property' return (
                (: multiple possible per language :)
                let $new      := utillib:prepareConceptPropertyForUpdate($param/value/property)
                let $stored   := $storedDataset/property[@name = $new/@name][lower-case(normalize-space(string-join(.//text(), ''))) = lower-case(normalize-space(string-join($new//text(), '')))]
                
                return
                switch ($param/@op)
                case ('add') return update insert $new into $storedDataset
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedDataset
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/copyright' return (
                (: only one per language :)
                let $new      := utillib:prepareFreeFormMarkupWithLanguageForUpdate($param/value/copyright)
                let $stored   := $storedDataset/copyright[@language = $param/value/copyright/@language]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedDataset
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            default return ( (: unknown path :) )
    
    (: after all the updates, the XML Schema order is likely off. Restore order :)
    let $preparedDataset        := dsapi:handleDataset($storedDataset)
    
    let $update                 := update replace $storedDataset with $preparedDataset
    let $update                 := update delete $lock
    
    return
        $preparedDataset
};

(:~ Central logic for patching an existing dataset statusCode, versionLabel, expirationDate and/or officialReleaseDate

@param $authmap required. Map derived from token
@param $id required. DECOR concept/@id to update
@param $effectiveDate required. DECOR concept/@effectiveDate to update
@param $recurse optional as xs:boolean. Default: false. Allows recursion into child particles to apply the same updates
@param $listOnly optional as xs:boolean. Default: false. Allows to test what will happen before actually applying it
@param $newStatusCode optional as xs:string. Default: empty. If empty, does not update the statusCode
@param $newVersionLabel optional as xs:string. Default: empty. If empty, does not update the versionLabel
@param $newExpirationDate optional as xs:string. Default: empty. If empty, does not update the expirationDate 
@param $newOfficialReleaseDate optional as xs:string. Default: empty. If empty, does not update the officialReleaseDate 
@return list object with success and/or error elements or error
:)
declare function dsapi:setDatasetStatus($authmap as map(*), $id as xs:string, $effectiveDate as xs:string?, $recurse as xs:boolean, $listOnly as xs:boolean, $newStatusCode as xs:string?, $newVersionLabel as xs:string?, $newExpirationDate as xs:string?, $newOfficialReleaseDate as xs:string?) as element(list) {
    (:get object for reference:)
    let $object                 := utillib:getDataset($id, $effectiveDate)
    let $decor                  := $object/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if (count($object) = 1) then () else
        if ($object) then
            error($errors:SERVER_ERROR, 'Dataset id ''' || $id || ''' effectiveDate ''' || $effectiveDate || ''' cannot be updated because multiple ' || count($object) || ' were found in: ' || string-join(distinct-values($object/ancestor::decor/project/@prefix), ' / ') || '. Please inform your database administrator.')
        else (
            error($errors:BAD_REQUEST, 'Dataset id ''' || $id || ''' effectiveDate ''' || $effectiveDate || ''' cannot be updated because it cannot be found.')
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-DATASETS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify datasets in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
        
    let $testUpdate             :=
        utillib:applyStatusProperties($authmap, $object, $object/@statusCode, $newStatusCode, $newVersionLabel, $newExpirationDate, $newOfficialReleaseDate, $recurse, true(), $projectPrefix)
    let $results                :=
        if ($testUpdate[self::error]) then $testUpdate else
        if ($listOnly) then $testUpdate else (
            utillib:applyStatusProperties($authmap, $object, $object/@statusCode, $newStatusCode, $newVersionLabel, $newExpirationDate, $newOfficialReleaseDate, $recurse, false(), $projectPrefix)
        )
    return
    if ($results[self::error] and not($listOnly)) then
        error($errors:BAD_REQUEST, string-join(
            for $e in $results[self::error]
            return
                $e/@itemname || ' id ''' || $e/@id || ''' effectiveDate ''' || $e/@effectiveDate || ''' cannot be updated: ' || data($e)
            , ' ')
        )
    else (
        <list artifact="STATUS" current="{count($results)}" total="{count($results)}" all="{count($results)}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            (: max 2 arrays: one with error and one with success :)
            utillib:addJsonArrayToElements($results)
        }
        </list>
    )
};

(:~ Clears all lock on a dataset and it concepts, and deletes all concepts with status 'new'. This is effectively like hitting cancel on editing in progress :)
declare function dsapi:postDatasetClearLocks($request as map(*)) {

    let $authmap                := $request?user
    let $dsid                   := $request?parameters?id
    let $dsed                   := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $treeOnly               := $request?parameters?treeonly = true()
    let $fullTree               := $request?parameters?fulltree = true()
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    
    let $dataset                := utillib:getDataset($dsid, $dsed)
    let $decor                  := $dataset/ancestor::decor
    let $projectPrefix          := $dataset/ancestor::decor/project/@prefix
    let $check                  :=
        if (count($dataset) = 1) then () else
        if ($dataset) then
            error($errors:SERVER_ERROR, 'Dataset id ''' || $dsid || ''' effectiveDate ''' || $dsed || ''' cannot be updated because multiple ' || count($dataset) || ' were found in: ' || string-join(distinct-values($projectPrefix), ' / ') || '. Please inform your database administrator.')
        else (
            error($errors:BAD_REQUEST, 'Dataset id ''' || $dsid || ''' effectiveDate ''' || $dsed || ''' cannot be updated because it cannot be found.')
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-DATASETS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify datasets in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    
    let $clear          := 
        for $lock in decorlib:getLocks($authmap, $dataset, true())
        let $rfid           := $lock/@ref
        let $rfed           := $lock/@effectiveDate
        let $projectId      := ()
        return
            decorlib:deleteLock($authmap, $rfid, $rfed, $projectId)
    
    (: delete everything thas has status new and does not have a lock on itself or one of its children :)
    let $delete        := 
        for $concept in $dataset//concept[@statusCode='new'][not(ancestor::history)]
        let $locks          := decorlib:getLocks((), $concept, true())
        return
            if (empty($locks)) then update delete $concept else ()
    
    return
        dsapi:getDataset($request)

};

declare function dsapi:handleDataset($in as element(dataset)) as element(dataset) {
    <dataset>
    {
        $in/@id,
        $in/@effectiveDate,
        $in/@statusCode,
        $in/@versionLabel[not(. = '')],
        $in/@expirationDate[not(. = '')],
        $in/@officialReleaseDate[not(. = '')],
        $in/@canonicalUri[not(. = '')],
        $in/@lastModifiedDate[not(. = '')], 
        $in/name,
        $in/desc,
        $in/publishingAuthority,
        $in/property,
        $in/copyright,
        $in/relationship,
        $in/concept,
        $in/recycle
    }
    </dataset>
};

(: Central logic for creating an empty dataset

@param $authmap         - required. Map derived from token
@return (empty) dataset object as xml with json:array set on elements
:)
declare function dsapi:createDataset($authmap as map(*), $projectPrefix as xs:string, $targetDate as xs:boolean, $sourceId as xs:string?, $sourceEffectiveDate as xs:string?, $keepIds as xs:boolean?, $baseDatasetId as xs:string?, $baseConceptId as xs:string?, $templateAssociations as xs:boolean?, $skipDeprecated as xs:boolean?, $testMode as xs:boolean?) {

    let $decor                  := utillib:getDecorByPrefix($projectPrefix)
    let $now                    := if ($targetDate) then substring(string(current-date()), 1, 10) || 'T00:00:00' else substring(string(current-dateTime()), 1, 19)
    let $sourceDataset          := if (empty($sourceId)) then () else utillib:getDataset($sourceId, $sourceEffectiveDate)
    
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, 'Project with prefix ' || $projectPrefix || ' does not exist')
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-DATASETS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify datasets in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $baseDatasetId          :=
        if (empty($baseDatasetId)) then decorlib:getDefaultBaseIdsP($decor, $decorlib:OBJECTTYPE-DATASET)[1]/@id else
        if (decorlib:getBaseIdsP($decor, $decorlib:OBJECTTYPE-DATASET)/@id = $baseDatasetId) then $baseDatasetId else (
            error($errors:BAD_REQUEST, 'Dataset base id ' || $baseDatasetId || ' is not configured as baseId for datasets in this project. Expected one of: ' || string-join(decorlib:getBaseIdsP($decor, $decorlib:OBJECTTYPE-DATASET)/@id, ' '))
        )
    let $baseConceptId          :=
        if (empty($baseConceptId)) then decorlib:getDefaultBaseIdsP($decor, $decorlib:OBJECTTYPE-DATASETCONCEPT)[1]/@id else
        if (decorlib:getBaseIdsP($decor, $decorlib:OBJECTTYPE-DATASETCONCEPT)/@id = $baseConceptId) then $baseConceptId else (
            error($errors:BAD_REQUEST, 'Concept base id ' || $baseConceptId || ' is not configured as baseId for concepts in this project. Expected one of: ' || string-join(decorlib:getBaseIdsP($decor, $decorlib:OBJECTTYPE-DATASETCONCEPT)/@id, ' '))
        )
    let $check                  :=
        if (empty($sourceId)) then () else if ($sourceDataset) then () else (
            error($errors:BAD_REQUEST, 'Source dataset based on id ' || $sourceId || ' and effectiveDate ' || $sourceEffectiveDate || ' does not exist. Cannot use this as source')
        )
    let $keepIds                := $keepIds = true()
    let $templateAssociations   := $templateAssociations = true() 
    let $skipDeprecated         := not($skipDeprecated = false())
    let $testMode               := $testMode = true()   
    
    (: decorlib:getNextAvailableIdP() returns <next base="1.2" max="2" next="{$max + 1}" id="1.2.3" type="DS"/> :)
    let $newDatasetId           := if ($keepIds) then $sourceDataset/@id else (decorlib:getNextAvailableIdP($decor, $decorlib:OBJECTTYPE-DATASET, $baseDatasetId)/@id)
    let $delmbaseId             := decorlib:getNextAvailableIdP($decor, $decorlib:OBJECTTYPE-DATASETCONCEPT, $baseConceptId)
    let $nextConceptId          := $delmbaseId/@id
    
    let $projectLanguage        := $decor/project/@defaultLanguage

    let $check                  :=
        if (string-length($newDatasetId) > 0 and string-length($nextConceptId) > 0 and string-length($projectLanguage) > 0) then () else (
            error($errors:BAD_REQUEST, 'Default base ID for datasets and/or concepts not set or project default language not set')
        )
        
    (: keep counter in db, so we make sure we don't issue the same id twice for sibling concepts :)
    let $insert                 :=
        if ($decor/datasets[@nextConceptLeaf]) then
            update value $decor/datasets/@nextConceptLeaf with xs:integer($delmbaseId/@max) + 1
        else (
            update insert attribute nextConceptLeaf {xs:integer($delmbaseId/@max) + 1} into $decor/datasets
        )
        
    let $result                 :=
        if ($sourceDataset) then 
            <dataset id="{$newDatasetId}" effectiveDate="{$now}" statusCode="new" lastModifiedDate="{$now}">
            {
                $sourceDataset/(* except concept)
                ,
                <relationship type="VERSION" ref="{$sourceDataset/@id}" flexibility="{$sourceDataset/@effectiveDate}"/>
                ,
                for $node in $sourceDataset/concept
                return
                    switch ($node/@statusCode) 
                    case 'pending'      return dsapi:inheritConcept($node, $node/@statusCode, $baseConceptId, $decor, $now, $skipDeprecated, $keepIds)
                    case 'deprecated'   return if ($skipDeprecated) then () else dsapi:inheritConcept($node, $node/@statusCode, $baseConceptId, $decor, $now, $skipDeprecated, $keepIds)
                    case 'cancelled'    return ()
                    case 'rejected'     return ()
                    default             return dsapi:inheritConcept($node, 'draft', $baseConceptId, $decor, $now, $skipDeprecated, $keepIds)
            }
            </dataset>
        else (
            <dataset id="{$newDatasetId}" effectiveDate="{$now}" statusCode="draft" lastModifiedDate="{$now}">
                <name language="{$projectLanguage}">Dataset</name>
                <desc language="{$projectLanguage}">Dataset</desc>
                <concept id="{$nextConceptId}" effectiveDate="{$now}" type="item" statusCode="draft">
                    <name language="{$projectLanguage}">Concept</name>
                    <desc language="{$projectLanguage}">Concept</desc>
                    <valueDomain type="string"/>
                </concept>
            </dataset>
        )
        
    let $delete                 := update delete $decor/datasets/@nextConceptLeaf
        
    (: get rid of helper attribute //concept/(@oldId | @oldEffectiveDate) :)
    let $resultDataset          := dsapi:copy-ds($result)
    let $updateDataset          := 
        if ($testMode) then () else 
        if ($decor/datasets) then
            update insert $resultDataset into $decor/datasets
        else (
            update insert <datasets>{$resultDataset}</datasets> following $decor/project
        )
            
    let $updateTemplateAssocs   := 
        if ($testMode or not($templateAssociations)) then () else (
            for $subconcept in $result/concept
            return
                dsapi:add-template-assocs($decor, $subconcept, $templateAssociations)
        )

    return
        if ($testMode) then $resultDataset else utillib:getDataset($newDatasetId, $now)
};

(:~ Create new concept that inherits from input concept :)
declare %private function dsapi:inheritConcept($concept as element(), $statusCode as xs:string, $baseId as xs:string, $decor as element(decor), $now as xs:string, $skipDeprecated as xs:boolean, $keepIds as xs:boolean) as item()* {
(:
if keep ids then only the effective date is set to now
if not keep ids then 
    (1) either new ids are generated (later) based on base id for datalelements
    (2) or if the format of the id is datalement.version.itemid then the new id is datalement.version+1.itemid
:)
let $deid               := $concept/@id
let $deed               := $concept/@effectiveDate

let $nextdelmLeaf       := $decor/datasets/@nextConceptLeaf
let $id                 := if ($keepIds) then $deid else concat($baseId, '.', $nextdelmLeaf)
let $nextid             := update value $nextdelmLeaf with xs:integer($nextdelmLeaf) + 1

let $originalConcept    := utillib:getOriginalForConcept($concept)

let $nme                := $originalConcept/name[1]
(: Use id of concept this concept inherited from if applicable, else use id :)
(:let $inheritId          := if ($concept/inherit) then ($concept/inherit/@ref) else ($cid):)
(: Use effectiveDate of concept this concept inherited from if applicable, else use effectiveDate :)
(:let $inheritEff         := if ($concept/inherit) then ($concept/inherit/@effectiveDate) else ($eff):)
return (
    comment {'Inherits from: ',$nme,' (status:',$concept/@statusCode/string(),', type:',$originalConcept/@type/string(),')'},
    <concept id="{$id}" statusCode="{$statusCode}" effectiveDate="{$now}" oldId="{$deid}" oldEffectiveDate="{$deed}">
        <inherit ref="{$deid}" effectiveDate="{$deed}"/>
    {
        for $subConcept in $concept/concept
        return
            switch ($subConcept/@statusCode) 
            case 'deprecated'   return if ($skipDeprecated) then () else dsapi:inheritConcept($subConcept, $subConcept/@statusCode, $baseId, $decor, $now, $skipDeprecated, $keepIds)
            case 'cancelled'    return ()
            case 'rejected'     return ()
            default             return dsapi:inheritConcept($subConcept, 'draft', $baseId, $decor, $now, $skipDeprecated, $keepIds)
    }
    </concept>
)
};

(:~ Strip extra attributes oldId | oldEffectiveDate :)
declare %private function dsapi:copy-ds($input as node()) {
    let $xslt := 
        <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
             <xsl:template match="concept">
                <xsl:copy>
                     <xsl:apply-templates select="@* except (@oldId | @oldEffectiveDate)"/>
                     <xsl:apply-templates select="node()"/>
                </xsl:copy>
             </xsl:template>
             <xsl:template match="@*|node()">
                 <xsl:copy>
                     <xsl:apply-templates select="@*|node()"/>
                 </xsl:copy>
             </xsl:template>
         </xsl:stylesheet>
    return transform:transform($input, $xslt, ())
};

(:
<templateAssociation templateId="2.16.840.1.113883.2.4.3.36.10.900804" effectiveDate="2012-07-04T00:00:00">
    <concept ref="2.16.840.1.113883.2.4.3.36.77.2.1.150050" effectiveDate="2012-04-10T00:00:00" elementId="2.16.840.1.113883.2.4.3.36.10.900804.1"/>
    <concept ref="2.16.840.1.113883.2.4.3.36.77.2.2.150050" effectiveDate="2013-11-03T00:00:00" elementId="2.16.840.1.113883.2.4.3.36.10.900804.1"/>
    <concept ref="2.16.840.1.113883.2.4.3.36.77.2.3.150050" effectiveDate="2014-07-12T00:00:00" elementId="2.16.840.1.113883.2.4.3.36.10.900804.1"/>
</templateAssociation>
:)
declare %private function dsapi:add-template-assocs($decor as node(), $concept as node(), $commitUpdate as xs:boolean) as item()* {
    for $templateAssociation in $decor//templateAssociation[concept[@ref=$concept/@oldId][@effectiveDate=$concept/@oldEffectiveDate]]
    return (
        <templateAssociation>
        {
            $templateAssociation/@*,
            for $association in $templateAssociation/concept[@ref=$concept/@oldId][@effectiveDate=$concept/@oldEffectiveDate]
            let $newAssociation     :=
                <concept ref="{$concept/@id}" effectiveDate="{$concept/@effectiveDate}">
                {
                    $association/(@* except (@ref | @effectiveDate))
                }
                </concept>
            let $dummy1 := 
                if ($commitUpdate) then (update insert $newAssociation following $templateAssociation/*[last()]) else ()
            return
                $newAssociation
        }
        </templateAssociation>
    )
    ,
    for $subconcept in $concept/concept
    return
        dsapi:add-template-assocs($decor, $subconcept, $commitUpdate)
};

declare function dsapi:searchDataset($projectPrefix as xs:string?, $searchTerms as xs:string*, $projectVersion as xs:string?, $language as xs:string?, $includeBBR as xs:boolean) as element(dataset)* {
    let $queryOnId          := if (count($searchTerms)=1 and matches($searchTerms[1],'^\d+(\.\d+)*$')) then true() else false()
    
    let $results            := 
        if ($queryOnId) then (
            if ($projectPrefix = '*') then
                $setlib:colDecorData//dataset[ends-with(@id, $searchTerms[1])][ancestor::decor[not(@private='true')]]
            else
            if (empty($projectPrefix)) then
                $setlib:colDecorData//dataset[ends-with(@id, $searchTerms[1])][ancestor::decor[not(@private='true')][@repository = 'true']]
            else (
                utillib:getDecorByPrefix($projectPrefix, $projectVersion, $language)//dataset[ends-with(@id, $searchTerms[1])]
            )
        ) 
        else
        if (empty($searchTerms)) then (
            let $datasets   := 
                if (empty($projectVersion)) then
                    if ($projectPrefix = '*') then
                        $setlib:colDecorData//dataset[ancestor::decor[not(@private='true')]]
                    else
                    if ($includeBBR) then
                        $setlib:colDecorData//dataset[ancestor::decor[not(@private='true')][@repository = 'true']]
                    else ()
                else ()
            
            return
                if ($projectPrefix = '*' or empty($projectPrefix)) then $datasets else (
                    $datasets | utillib:getDecorByPrefix($projectPrefix, $projectVersion, $language)//dataset
                )
        ) 
        else (
            let $luceneQuery    := utillib:getSimpleLuceneQuery($searchTerms)
            let $luceneOptions  := utillib:getSimpleLuceneOptions()
            let $datasets       := 
                if (empty($projectVersion)) then
                    if ($projectPrefix = '*') then
                        $setlib:colDecorData//dataset[ft:query(name, $luceneQuery)][ancestor::decor[not(@private='true')]]
                    else
                    if ($includeBBR) then
                        $setlib:colDecorData//dataset[ft:query(name, $luceneQuery)][ancestor::decor[not(@private='true')][@repository = 'true']]
                    else ()
                else ()
            
            return
                if ($projectPrefix = '*' or empty($projectPrefix)) then $datasets else (
                    $datasets | utillib:getDecorByPrefix($projectPrefix, $projectVersion, $language)//dataset[ft:query(name, $luceneQuery)]
                )
        )
        
    (:shortest match first:)
    let $results        :=
        for $r in $results
        let $displayName   := if ($r/name[@language = $language]) then $r/name[@language = $language][1] else $r/name[1]
        order by string-length($displayName)
        return $r
    
    return 
        $results
};