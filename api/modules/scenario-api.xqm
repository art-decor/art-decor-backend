xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:~ Scenario API allows read, create, update of DECOR scenarios :)
module namespace scapi              = "http://art-decor.org/ns/api/scenario";

import module namespace roaster     = "http://e-editiones.org/roaster";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

import module namespace utillib     = "http://art-decor.org/ns/api/util" at "library/util-lib.xqm";
import module namespace utilsvg     = "http://art-decor.org/ns/api/util-svg" at "library/util-svg-lib.xqm";
import module namespace utilhtml    = "http://art-decor.org/ns/api/util-html" at "library/util-html-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "library/settings-lib.xqm";
import module namespace decorlib    = "http://art-decor.org/ns/api/decor" at "library/decor-lib.xqm";
import module namespace histlib     = "http://art-decor.org/ns/api/history" at "library/history-lib.xqm";
import module namespace serverapi   = "http://art-decor.org/ns/art-decor-server" at "server-api.xqm";
(:import module namespace trapi       = "http://art-decor.org/ns/api/transaction" at "transaction-api.xqm";:)
import module namespace tmapi       = "http://art-decor.org/ns/api/template" at "template-api.xqm";
import module namespace qapi        = "http://art-decor.org/ns/api/questionnaire" at "questionnaire-api.xqm";

declare namespace json      = "http://www.json.org";
declare namespace f         = "http://hl7.org/fhir";

(:~ All functions support their own override, but this is the fallback for the maximum number of results returned on a search :)
declare %private variable $scapi:maxResults             := 50;
declare %private variable $scapi:TRANSACTION-TYPE       := utillib:getDecorTypes()/TransactionType;
declare %private variable $scapi:RELATIONSHIP-TYPE      := utillib:getDecorTypes()/RelationshipTypes;
declare %private variable $scapi:ITEM-STATUS            := utillib:getDecorTypes()/ItemStatusCodeLifeCycle;
declare %private variable $scapi:ACTOR-TYPE             := utillib:getDecorTypes()/ScenarioActorType;
declare %private variable $scapi:ACTOR-ROLE             := utillib:getDecorTypes()/ActorType;
declare %private variable $scapi:STATUSCODES-FINAL      := ('final', 'pending', 'rejected', 'cancelled', 'deprecated');
declare %private variable $scapi:ADDRLINE-TYPE          := utillib:getDecorTypes()/AddressLineType;
declare %private variable $scapi:VALUEDOMAIN-TYPE       := utillib:getDecorTypes()/DataSetValueType;
declare %private variable $scapi:STATUSCODES-NONDEPRECS := ('draft', 'final', 'new');

(:~ Retrieves latest DECOR scenario based on $id (oid)

@param $id                      - required parameter denoting the id of the scenario
@param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
@param $projectLanguage         - optional parameter to select from a specific compiled language
@return as-is or as compiled as JSON
@since 2020-05-03
:)
declare function scapi:getLatestScenario($request as map(*)) {
    scapi:getScenario($request)
};

(:~ Retrieves DECOR scenario based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version

@param $id                      - required parameter denoting the id of the scenario
@param $effectiveDate           - optional parameter denoting the effectiveDate of the scenario. If not given assumes latest version for id
@param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
@param $projectLanguage         - optional parameter to select from a specific compiled language
@return as-is or as compiled as JSON
@since 2020-05-03
:)
declare function scapi:getScenario($request as map(*)) {

    let $id                     := $request?parameters?id
    let $effectiveDate          := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $projectPrefix          := $request?parameters?prefix
    let $projectVersion         := $request?parameters?release
    let $projectLanguage        := $request?parameters?language
    
    let $results                := scapi:getScenario($id, $effectiveDate, $projectPrefix, $projectVersion, $projectLanguage)

    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple scenarios for id '", $id, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        )
        else (
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
        )

};

(:~ Retrieves DECOR scenario history based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id
    @param $effectiveDate           - optional parameter denoting the effectiveDate. If not given assumes latest version for id
    @return list
    @since 2022-08-16
:)
declare function scapi:getScenarioHistory($request as map(*)) {
    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $projectPrefix                  := ()
    let $results                        := histlib:ListHistory($authmap, $decorlib:OBJECTTYPE-SCENARIO, (), $id, $effectiveDate, 0)
    
    return
        <list artifact="{$decorlib:OBJECTTYPE-SCENARIO}" current="{count($results)}" total="{count($results)}" all="{count($results)}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements($results)
        }
        </list>
};

(:~ Returns a list of zero or more scenarios and child transactions
    
@param $projectPrefix           - required. limits scope to this project only
@param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
@param $projectLanguage         - optional parameter to select from a specific compiled language
@param $datasetId               - optional. Filter output to just those based on this dataset-id
@param $datasetEffectiveDate    - optional. null filters to the newest version based on $datasetId, yyyy-mm-ddThh:mm:ss for this specific version
@param $sort                    - optional parameter to indicate sorting. Only option 'name'
@param $sortorder               - optional parameter to indicate sort order. Only option 'descending'
@return all live repository/non-private scenarios as JSON, all scenarios for the given $projectPrefix or nothing if not found
@since 2020-05-03
:)
declare function scapi:getScenarioList($request as map(*)) {

    let $projectPrefix        := $request?parameters?prefix
    let $projectVersion       := $request?parameters?release
    let $projectLanguage      := $request?parameters?language
    let $datasetId            := $request?parameters?datasetId
    let $datasetEffectiveDate := $request?parameters?datasetEffectiveDate
    let $sort                 := $request?parameters?sort
    let $sortorder            := $request?parameters?sortorder
    let $max                  := $request?parameters?max
    
    return
        scapi:getScenarioList($projectPrefix, $projectVersion, $projectLanguage, $datasetId, $datasetEffectiveDate, $sort, $sortorder, $max)

};

(:~ Create scenario, new scenario with its own id and effectiveDate based on defaultBaseId for scenarios

Situation 1: completely new scenario with its own id and effectiveDate
- Input MAY be "empty" then we create a single scenario with id/effectiveDate/empty name and desc/single transaction group containing single transaction
- Input MAY be a scenario without id/effectiveDate but with name/desc and other direct properties. If there are no transactions, we do the same as for empty input. 
- Transaction handling is sent to transaction api where a similar function works in a similar way

Situation 2: new scenario version based on existing scenario with new id and effectiveDate
- Parameter sourceId and sourceEffectiveDate are required. Input SHALL be empty
- Return a copy of existing scenario with new id and effectiveDates on scenario and child transactions 

Note: if you want to update a transaction (group) from one dataset to the other, you can do that by adding 
      transaction.representingTemplate.targetDataset and optionally .targetDatasetFlexibility next to the original 
      transaction.representingTemplate.sourceDataset and .sourceDatasetFlexibility 

The scenario creation process then tries to find matches in the new dataset for the concepts listed from the old dataset. This logic is handed off to the transaction api  

@param $bearer-token required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $projectPrefix required. what project to create into             
@param $sourceId optional. the scenario id to create the new scenario from 
@param $sourceEffectiveDate optional. the scenario effectiveDate to create the scenario from 
@param $request-body optional. body containing new scenario structure
@return scenario structure
@since 2020-05-03
:)
declare function scapi:postScenario($request as map(*)) {

    let $authmap                := $request?user
    let $projectPrefix          := $request?parameters?prefix[string-length() gt 0]
    let $scid                   := $request?parameters?sourceId
    let $sced                   := $request?parameters?sourceEffectiveDate
    let $data                   := utillib:getBodyAsXml($request?body, 'scenario', ())
    
    (:let $s                      := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data):) 
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($projectPrefix)) then 
            error($errors:BAD_REQUEST, 'Parameter prefix SHALL have a value')
        else ()
    let $check                  :=
        if (empty($scid) and empty($data)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have either parameter sourceId OR data')
        else
        if (not(empty($scid)) and not(empty($data))) then 
            error($errors:BAD_REQUEST, 'Request SHALL have either parameter sourceId OR data, not both')
        else
        if ($sced[not(. = '')] and empty($scid)) then
            error($errors:BAD_REQUEST, 'Request SHALL have non-empty parameter sourceId if parameter sourceEffectiveDate has a value')
        else ()
    
    let $return                 := scapi:postScenario($authmap, $projectPrefix, $scid, $sced, $data)
    let $result                 := scapi:getScenario($return/@id, $return/@effectiveDate, (), (), ())
    
    return
        if (empty($result)) then () else (
            roaster:response(201, 
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
            )
        )

};

(:~ Update scenario

@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $id                       - required. the id for the scenario to update 
@param $effectiveDate            - required. the effectiveDate for the scenario to update 
@param $request-body             - required. body containing new scenario structure
@return concept structure
@since 2020-05-03
:)
declare function scapi:putScenario($request as map(*)) {

    let $authmap                := $request?user
    let $scid                   := $request?parameters?id
    let $sced                   := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $data                   := utillib:getBodyAsXml($request?body, 'scenario', ())
    let $deletelock             := $request?parameters?deletelock = true()
    
    (:let $s                      := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data):) 
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($data)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $return                 := scapi:putScenario($authmap, string($scid), $sced, $data, $deletelock)
    let $result                 := scapi:getScenario($scid, $sced, (), (), ())
    
    return
        if (empty($result)) then () else (
            element {name($result)} {
                $result/@*,
                namespace {"json"} {"http://www.json.org"},
                utillib:addJsonArrayToElements($result/*)
            }
        )

};

(:~ Update DECOR scenario parts. Does not touch any transactions. Expect array of parameter objects, each containing RFC 6902 compliant contents. Note: RestXQ does not do PATCH (yet), but that would be the preferred option.

{ "op": "[add|remove|replace]", "path": "/...", "value": "[string|object]" }

where
* op - add (object associations, tracking, event) or remove (object associations only) or replace (displayName, priority, type, tracking, assignment)
* path - / (object, tracking, assignment) or /displayName or /priority or /type
* value - string when the path is not /. object when path is /

@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $id                       - required. the id for the issue to update 
@param $request-body             - required. json body containing array of parameter objects each containing RFC 6902 compliant contents
@return dataset structure including generated meta data
@since 2020-05-03
@see http://tools.ietf.org/html/rfc6902
:)
declare function scapi:patchScenario($request as map(*)) {

    let $authmap                        := $request?user
    let $scid                           := $request?parameters?id
    let $sced                           := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $projectPrefix                  := ()
    let $projectVersion                 := ()
    let $projectLanguage                := ()
    let $data                           := utillib:getBodyAsXml($request?body, 'parameters', ())
    
    (:let $s                      := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data) :)
    
    let $check                  :=
        if ($data) then () else (
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        )
    
    let $return                 := scapi:patchScenario($authmap, string($scid), $sced, $data)
    let $results                := scapi:getScenario($scid, $sced, $projectPrefix, $projectVersion, $projectLanguage)
    return 
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple scenarios for id '", $scid, "' effectiveDate '", $sced, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        )
        else (
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
        )

};

(:~ Delete DECOR scenario
@return nothing
@since 2021-02-19
:)
declare function scapi:deleteScenario($request as map(*)) {
    
    let $authmap                := $request?user
    let $id                     := $request?parameters?id
    let $effectiveDate          := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    (:let $projectPrefix          := $request?parameters?prefix
    let $projectVersion         := $request?parameters?release
    let $projectLanguage        := $request?parameters?language:)
        
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($id)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have parameter id')
        else ()
    
    let $return                 := scapi:deleteScenario($authmap, $id, $effectiveDate)
    return (
        roaster:response(204, $return)
    )
};

(:~ Update scenario statusCode, versionLabel, expirationDate and/or officialReleaseDate

@param $authmap required. Map derived from token
@param $id required. DECOR concept/@id to update
@param $effectiveDate required. DECOR concept/@effectiveDate to update
@param $recurse optional as xs:boolean. Default: false. Allows recursion into child particles to apply the same updates
@param $listOnly optional as xs:boolean. Default: false. Allows to test what will happen before actually applying it
@param $newStatusCode optional as xs:string. Default: empty. If empty, does not update the statusCode
@param $newVersionLabel optional as xs:string. Default: empty. If empty, does not update the versionLabel
@param $newExpirationDate optional as xs:string. Default: empty. If empty, does not update the expirationDate 
@param $newOfficialReleaseDate optional as xs:string. Default: empty. If empty, does not update the officialReleaseDate 
@return list object with success and/or error elements or error
:)
declare function scapi:putScenarioStatus($request as map(*)) {

    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $recurse                        := $request?parameters?recurse = true()
    let $list                           := $request?parameters?list = true()
    let $statusCode                     := $request?parameters?statusCode
    let $versionLabel                   := $request?parameters?versionLabel
    let $expirationDate                 := $request?parameters?expirationDate
    let $officialReleaseDate            := $request?parameters?officialReleaseDate
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    
    return
        scapi:setScenarioStatus($authmap, $id, $effectiveDate, $recurse, $list, $statusCode, $versionLabel, $expirationDate, $officialReleaseDate)
};

(:~ Returns an actors object with zero or more child actors from project
    
@param $projectPrefix           - required. limits scope to this project only
@param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
@param $projectLanguage         - optional parameter to select from a specific compiled language
@param $sort                    - optional parameter to indicate sorting. Only option 'name'
@param $sortorder               - optional parameter to indicate sort order. Only option 'descending'
@return list with actors
@since 2020-05-03
:)
declare function scapi:getScenarioActorList($request as map(*)) {

    let $projectPrefix        := $request?parameters?prefix[string-length() gt 0]
    let $projectVersion       := $request?parameters?release
    let $projectLanguage      := $request?parameters?language
    (:let $id                   := $request?parameters?id:)
    let $sort                 := $request?parameters?sort
    let $sortorder            := $request?parameters?sortorder
    let $max                  := $request?parameters?max
    
    let $check              :=
        if (empty($projectPrefix)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter prefix')
        else ()
    
    return
        scapi:getScenarioActorList($projectPrefix, $projectVersion, $projectLanguage, $sort, $sortorder, $max)

};

(:~ Retrieves DECOR scenario actor

@param $projectPrefix            - required. limits scope to this project only
@param $id                       - required. id for the actor to retrieve
@return one or zero actor object
@since 2020-08-11
:)
declare function scapi:getScenarioActor($request as map(*)) {

    let $projectPrefix      := $request?parameters?prefix[string-length() gt 0]
    let $id                 := $request?parameters?id
    (:let $sort               := $request?parameters?sort
    let $max                := $request?parameters?max:)
    
    let $actors             := scapi:getScenarioActor($projectPrefix, $id)
    
    let $check              :=
        if (empty($id)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter id')
        else ()
    
    return
        if (empty($actors)) then 
            roaster:response(404, ())
        else
        if (count($actors) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple actors for id '", $id, "'. Expected 0..1. You should delete and recreate this actor. Consider alerting your administrator as this should not be possible."))
        )
        else (
            for $actor in $actors
            return
                element {name($actor)} {
                    $actor/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($actor/*)
                }
        )

};

(:~ Create scenario actor, new scenario actor with its own id and effectiveDate based on defaultBaseId for actors
- Input MAY be "empty" then we create a single scenario actor with id/effectiveDate/empty name and desc
- Input MAY be a scenario actor without id/effectiveDate but with name/desc which we'll use for the new actor

Situation 2: new scenario version based on existing scenario with new id and effectiveDate
- Parameter sourceId and sourceEffectiveDate are required. Input SHALL be empty
- Return a copy of existing scenario with new id and effectiveDates on scenario and child transactions 

Note: if you want to update a transaction (group) from one dataset to the other, you can do that by adding 
      transaction.representingTemplate.targetDataset and optionally .targetDatasetFlexibility next to the original 
      transaction.representingTemplate.sourceDataset and .sourceDatasetFlexibility 

The scenario creation process then tries to find matches in the new dataset for the concepts listed from the old dataset. This logic is handed off to the transaction api  

@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $sourceId                 - optional. the scenario id to create the new scenario from 
@param $sourceEffectiveDate      - optional. the scenario effectiveDate to create the scenario from 
@param $request-body             - optional. body containing new scenario structure
@return scenario structure
@since 2020-05-03
:)
declare function scapi:postScenarioActor($request as map(*)) {

    let $authmap                := $request?user
    let $projectPrefix          := $request?parameters?prefix[string-length() gt 0]
    let $data                   := utillib:getBodyAsXml($request?body, 'actor', ())
    
    (:let $s                      := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data):) 
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($projectPrefix)) then 
            error($errors:BAD_REQUEST, 'Parameter prefix SHALL have a value')
        else ()
    let $check                  :=
        if (empty($data)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $return                 := scapi:postScenarioActor($authmap, $projectPrefix, $data)
    let $result                 := scapi:getScenarioActor($projectPrefix, $return/@id)
    
    return
        if (empty($result)) then () else (
            roaster:response(201, 
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
            )
        )

};

(:~ Update DECOR scenario actor parts. Expect array of parameter objects, each containing RFC 6902 compliant contents. Note: RestXQ does not do PATCH (yet), but that would be the preferred option.

{ "op": "[add|remove|replace]", "path": "[/type|/name|/desc]", "value": "[string|object]" }

where

* op - add (name|desc) or remove (name|desc) or replace (type|name|desc)

* path - /type, or /name or /desc 

* value - string when the path is /type, otherwise object
@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $id                       - required. the id for the scenario actor to update 
@param $request-body             - required. body containing array of parameter objects each containing RFC 6902 compliant contents
@return actor structure
@since 2020-05-03
@see http://tools.ietf.org/html/rfc6902
:)
declare function scapi:patchScenarioActor($request as map(*)) {

    let $authmap                := $request?user
    let $projectPrefix          := $request?parameters?prefix[string-length() gt 0]
    let $actorId                := $request?parameters?id
    let $data                   := utillib:getBodyAsXml($request?body)
    
    (:let $s                      := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data) :)
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($actorId)) then 
            error($errors:BAD_REQUEST, 'Parameter id SHALL have a value')
        else ()
    let $check                  :=
        if (empty($data)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $return                 := scapi:patchScenarioActor($authmap, $projectPrefix, $actorId, $data)
    return (
        roaster:response(200, $return)
    )

};

(:~ Delete DECOR scenario actor
@return nothing
@since 2021-02-19
:)
declare function scapi:deleteScenarioActor($request as map(*)) {
    
    let $authmap                := $request?user
    let $projectPrefix          := $request?parameters?prefix[string-length() gt 0]
    let $actorId                := $request?parameters?id
        
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($actorId)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have parameter id')
        else ()
    
    let $return                 := scapi:deleteScenarioActor($authmap, $projectPrefix, $actorId)
    return (
        roaster:response(204, $return)
    )
};

declare function scapi:getScenario($id as xs:string, $effectiveDate as xs:string?, $projectPrefix as xs:string?, $projectVersion as xs:string?, $projectLanguage as xs:string?) {
    let $id                     := $id[not(. = '')]
    let $effectiveDate          := $effectiveDate[not(. = '')]
    let $projectPrefix          := $projectPrefix[not(. = '')]
    let $projectVersion         := $projectVersion[not(. = '')]
    let $projectLanguage        := $projectLanguage[not(. = '')]
    
    let $scenario               := utillib:getScenario($id, $effectiveDate, $projectVersion[1], $projectLanguage[1])
    
    for $sc in $scenario
    return
        element {name($sc)} {
            $sc/@*,
            $sc/name,
            $sc/desc,
            if ($sc[publishingAuthority]) then $sc/publishingAuthority else utillib:inheritPublishingAuthority($scenario),
            if ($sc[copyright]) then $sc/copyright else utillib:inheritCopyright($scenario),
            $sc/(* except (name | desc | publishingAuthority | copyright | transaction))
            ,
            (: we return just a count of issues and leave it up to the calling party to get those if required :)
            <issueAssociation count="{count($setlib:colDecorData//issue/object[@id = $sc/@id][@effectiveDate = $sc/@effectiveDate])}"/>
            ,
            for $trg in $sc/transaction
            return
                element {name($trg)} {
                $trg/@*,
                $trg/name,
                $trg/desc,
                if ($trg[publishingAuthority]) then $trg/publishingAuthority else utillib:inheritPublishingAuthority($trg),
                if ($trg[copyright]) then $trg/copyright else utillib:inheritCopyright($trg),
                $trg/(* except (name | desc | publishingAuthority | copyright | transaction)),
                for $trl in $trg/transaction
                return
                    element {name($trl)} {
                    $trl/@*,
                    $trl/name,
                    $trl/desc,
                    if ($trl[publishingAuthority]) then $trl/publishingAuthority else utillib:inheritPublishingAuthority($trl),
                    if ($trl[copyright]) then $trl/copyright else utillib:inheritCopyright($trl),
                    $trl/(* except (name | desc | publishingAuthority | copyright))
                }
            }
        }
};

(:~ Create scenario

@param $authmap      - required. Map derived from token
@param $prefix       - required. DECOR project/@prefix
@param $data         - required. DECOR actor object as xml
@return scenario object as xml
:)
declare function scapi:postScenario($authmap as map(*), $projectPrefix as xs:string, $sourceScenarioId as xs:string?, $sourceScenarioEffectiveDate as xs:string?, $data as element(scenario)?) as element(scenario) {
    let $decor                  := utillib:getDecorByPrefix($projectPrefix)
    let $defaultLanguage        := $decor/project/@defaultLanguage
    let $storedScenario         := if (empty($sourceScenarioId)) then () else utillib:getScenario($sourceScenarioId, $sourceScenarioEffectiveDate)
    
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix ''', $projectPrefix, ''', does not exist. Cannot create in non-existent project.'))
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-SCENARIOS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify scenarios in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if (empty($sourceScenarioId)) then () else if ($storedScenario) then () else (
            error($errors:BAD_REQUEST, concat('Source scenario id ''', $sourceScenarioId, ''' effectiveDate ''', $sourceScenarioEffectiveDate, ''' requested but not found. Could not create new scenario from source scenario.'))
        )
    
    let $newId                  := decorlib:getNextAvailableIdP($decor, $decorlib:OBJECTTYPE-SCENARIO, decorlib:getDefaultBaseIdsP($decor, $decorlib:OBJECTTYPE-SCENARIO)/@id)/@id
    let $newEffectiveDate       := format-date(current-date(), '[Y0001]-[M01]-[D01]') || 'T00:00:00'
    let $tridnext               := decorlib:getNextAvailableIdP($decor, $decorlib:OBJECTTYPE-TRANSACTION, decorlib:getDefaultBaseIdsP($decor, $decorlib:OBJECTTYPE-TRANSACTION)/@id)
    
    (: guarantee defaultLanguage. Preserve any incoming other languages. Guarantee name. Leave desc optional :)
    let $data                   := if ($storedScenario) then $storedScenario else $data
    let $newScenario            :=
        <scenario id="{$newId}" effectiveDate="{$newEffectiveDate}" statusCode="draft"> 
        {
            $data/@versionLabel[string-length() gt 0]
            (: don't copy expirationDate, officialReleaseDate, canonicalUri :),
            attribute lastModifiedDate {$newEffectiveDate}
        }
        {   
            scapi:handleScenarioElements($data, $defaultLanguage, exists($storedScenario))
            ,
            if ($data/transaction) then (
                (: TODO: handoff to transaction function :)
                for $tr at $i in $data/transaction
                let $trid     := $tridnext/@base || '.' || xs:integer($tridnext/@max) + $i + count($tr/preceding-sibling::transaction/transaction)
                return
                <transaction id="{$trid}" effectiveDate="{$newEffectiveDate}" statusCode="draft" type="group">
                {
                    attribute lastModifiedDate {$newEffectiveDate},
                    scapi:handleTransactionElements($tr, $defaultLanguage)
                }
                {
                    for $trc at $j in $tr/transaction
                    let $trid  := $tridnext/@base || '.' || xs:integer($tridnext/@max) + $i + count($tr/preceding-sibling::transaction/transaction) + $j
                    return
                        <transaction id="{$trid}" effectiveDate="{$newEffectiveDate}" statusCode="draft" type="{($trc/@type, 'stationary')[1]}">
                        {
                            $trc/@model,
                            attribute lastModifiedDate {$newEffectiveDate},
                            scapi:handleTransactionElements($trc, $defaultLanguage)
                        }
                        </transaction>
                }
                </transaction>
            )
            else (
                let $trgroupid  := $tridnext/@id
                let $tr1id      := $tridnext/@base || '.' || xs:integer($tridnext/@next) + 1
                return
                <transaction id="{$trgroupid}" effectiveDate="{$newEffectiveDate}" statusCode="draft" type="group" lastModifiedDate="{$newEffectiveDate}">
                    <name language="{$defaultLanguage}"/>
                    <transaction id="{$tr1id}" effectiveDate="{$newEffectiveDate}" statusCode="draft" type="stationary" lastModifiedDate="{$newEffectiveDate}">
                        <name language="{$defaultLanguage}"/>
                        <actors/>
                    </transaction>
                </transaction>
            )
        }
        </scenario>
        
    let $updateScenarios        :=
        if ($decor/scenarios[questionnaire | questionnaireresponse]) then
            update insert $newScenario preceding $decor/scenarios/(questionnaire | questionnaireresponse)[1]
        else
        if ($decor/scenarios) then
            update insert $newScenario into $decor/scenarios 
        else
            update insert <scenarios><actors/>{$newScenario}</scenarios> preceding $decor/ids

    return
        $newScenario
};

(:~ Central logic for updating an existing scenario

@param $authmap         - required. Map derived from token
@param $id              - required. DECOR scenario/@id to update
@param $effectiveDate   - required. DECOR scenario/@effectiveDate to update
@param $data            - required. DECOR scenario xml element containing everything that should be in the updated scenario
@return concept object as xml with json:array set on elements
:)
declare function scapi:putScenario($authmap as map(*), $id as xs:string, $effectiveDate as xs:string, $data as element(scenario), $deletelock as xs:boolean) {

    let $editedScenario         := $data
    let $storedScenario         := utillib:getScenario($id, $effectiveDate)
    let $decor                  := $storedScenario/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix

    let $check                  :=
        if (empty($storedScenario)) then 
            error($errors:BAD_REQUEST, 'Scenario with id ''' || $id || ''' and effectiveDate ''' || $effectiveDate || ''' does not exist')
        else
        if (count($storedScenario) gt 1) then
            error($errors:SERVER_ERROR, 'Scenario with id ''' || $id || ''' and effectiveDate ''' || $effectiveDate || ''' occurs ' || count($storedScenario) || ' times. Inform your database administrator.')
        else
        if ($storedScenario[@statusCode = $scapi:STATUSCODES-FINAL]) then
            error($errors:BAD_REQUEST, 'Scenario with id ''' || $id || ''' and effectiveDate ''' || $effectiveDate || ''' cannot be updated while it one of status: ' || string-join($scapi:STATUSCODES-FINAL, ', '))
        else ()
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-SCENARIOS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify scenarios in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if ($data[@id = $id] | $data[empty(@id)]) then () else (
            error($errors:BAD_REQUEST, concat('Scenario SHALL have the same id as the scenario id ''', $id, ''' used for updating. Found in request body: ', ($data/@id, 'null')[1]))
        )
    let $check                  :=
        if ($data[@effectiveDate = $effectiveDate] | $data[empty(@effectiveDate)]) then () else (
            error($errors:BAD_REQUEST, concat('Scenario SHALL have the same effectiveDate as the scenario effectiveDate ''', $effectiveDate, ''' used for updating. Found in request body: ', ($data/@effectiveDate, 'null')[1]))
        )
    let $check                  :=
        if ($data[@statusCode = $storedScenario/@statusCode] | $data[empty(@statusCode)]) then () else (
            error($errors:BAD_REQUEST, concat('Scenario SHALL have the same statusCode as the scenario statusCode ''', $storedScenario/@statusCode, ''' used for updating. Found in request body: ', ($data/@statusCode, 'null')[1]))
        )
    let $check                  :=
        for $transaction in $storedScenario//transaction
        let $trid               := $transaction/@id
        let $tred               := $transaction/@effectiveDate
        return
            if (count($data//transaction[@id = $trid][@effectiveDate = $tred])= 1) then () else (
                error($errors:BAD_REQUEST, 'Updated scenario SHALL contain at least all stored transactions for this scenario. You cannot delete transactions in an update. Missing transaction id ''' || $trid || ''' effectiveDate ''' || $tred || ''' ' || $transaction/name[1])
            )
    let $check                  :=
        for $transaction in $data//transaction[@id]
        let $trid               := $transaction/@id
        let $tred               := $transaction/@effectiveDate
        return
            if (count($storedScenario//transaction[@id = $trid][@effectiveDate = $tred])= 1) then () else (
                error($errors:BAD_REQUEST, 'Updated scenario SHALL NOT contain transaction from a different scenario. You cannot move transactions from one scenario to another in an update. Missing transaction id ''' || $trid || ''' effectiveDate ''' || $tred || ''' ' || $transaction/name[1])
            )
    
    let $check                  :=
        for $c in $editedScenario[edit] | $editedScenario/descendant::transaction[edit]
        let $storedObject       := $storedScenario[self::scenario][@id = $c/@id][@effectiveDate = $c/@effectiveDate] | $storedScenario/descendant::transaction[@id = $c/@id][@effectiveDate = $c/@effectiveDate]
        let $storedObjectType   := upper-case(substring(local-name($c), 1, 1)) || substring(local-name($c), 2)
        let $dataset            := 
            if ($c[count(representingTemplate) = 1]/representingTemplate/@sourceDataset) then
                utillib:getDataset($c/representingTemplate/@sourceDataset, $c/representingTemplate/@sourceDatasetFlexibility)
            else ()
        let $template           := 
            if ($c[count(representingTemplate) = 1]/representingTemplate/@ref) then
                tmapi:getTemplateById($c/representingTemplate/@ref, $c/representingTemplate/@flexibility, $decor, $decor/@versionDate, $decor/@language)[@id]
            else ()
        let $questionnaire      := 
            if ($c[count(representingTemplate) = 1]/representingTemplate/@representingQuestionnaire) then
                utillib:getQuestionnaire($c/representingTemplate/@representingQuestionnaire, $c/representingTemplate/@representingQuestionnaireFlexibility, (), ())/descendant-or-self::questionnaire[@id]
            else ()
        return (
            if ($c/@id) then (
                if (empty($storedObject)) then
                    error($errors:BAD_REQUEST, $storedObjectType || ' ' || $c/@id || ' effectiveDate ''' || $c/@effectiveDate || ''' SHALL exist as/in stored scenario on the server. Cannot update non-existent parts.')
                else
                if (count($storedObject) = 1) then () else (
                    error($errors:SERVER_ERROR, $storedObjectType || ' ' || $c/@id || ' effectiveDate ''' || $c/@effectiveDate || ''' occurs ' || count($storedObject) || ''' times. Inform your database administrator.')
                ),
                if (empty(decorlib:getLocks($authmap, $storedObject, false()))) then
                    if (empty(decorlib:setLock($authmap, $storedObject, false()))) then
                        error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have a lock for ' || local-name($c) || ' ' || $c/@id || ' (anymore). Get a lock first.'))
                    else ()
                else ()
                ,
                if ($c[@statusCode = $storedObject/@statusCode]) then () else (
                    error($errors:BAD_REQUEST, concat($storedObjectType || ' ' || $c/@id || ' SHALL have the same statusCode as the statusCode ''', $storedObject/@statusCode, ''' used for updating. Found in request body: ', ($data/@statusCode, 'null')[1]))
                )
            )
            else (
                (: we are apparently adding something :)
                if (empty($c/@statusCode)) then () else if ($c[@statusCode = $scapi:ITEM-STATUS]) then () else (
                    error($errors:BAD_REQUEST, $storedObjectType || ' ' || $c/@statusCode || ' SHALL be one of ', string-join($scapi:ITEM-STATUS, ', '))
                )
            )
            ,
            if ($c[not(name)]) then
                error($errors:BAD_REQUEST, $storedObjectType || ' ' || $c/@id || ' SHALL have at least one name')
            else ()
            ,
            if ($c[self::transaction]) then (
                if (empty($c/@id) and empty($c/@type)) then () else
                if ($c[@type = $scapi:TRANSACTION-TYPE/enumeration/@value]) then () else (
                    error($errors:BAD_REQUEST, $storedObjectType || ' ' || $c/@id || ' type ' || $c/@type || ' SHALL be one of ' || string-join($scapi:TRANSACTION-TYPE/enumeration/@value, ', '))
                ),
                if ($c[@type = 'group']) then (
                    if ($c[transaction]) then () else (
                        error($errors:BAD_REQUEST, $storedObjectType || ' ' || $c/@id || ' type ' || $c/@type || ' SHALL contain one or more transactions.')
                    )
                    ,
                    if ($c[representingTemplate]) then 
                        error($errors:BAD_REQUEST, $storedObjectType || ' ' || $c/@id || ' type ' || $c/@type || ' SHALL NOT contain representingTemplate.')
                    else ()
                )    
                else (
                    if ($c[@type][transaction]) then 
                        error($errors:BAD_REQUEST, $storedObjectType || ' ' || $c/@id || ' type ' || $c/@type || ' SHALL NOT contain transactions.')
                    else ()
                    ,
                    if ($c[count(representingTemplate) gt 1]) then 
                        error($errors:BAD_REQUEST, $storedObjectType || ' ' || $c/@id || ' type ' || $c/@type || ' SHALL NOT contain multiple representingTemplates.')
                    else ()
                    ,
                    if ($c[count(representingTemplate/@sourceDataset) = 1]) then 
                        if ($dataset) then () else ( 
                            error($errors:BAD_REQUEST, $storedObjectType || ' ' || $c/@id || ' type ' || $c/@type || ' SHALL refer to an existing dataset.')
                        )
                    else ()
                    ,
                    if ($c[count(representingTemplate/@ref) = 1]) then 
                        if ($template) then () else ( 
                            error($errors:BAD_REQUEST, $storedObjectType || ' ' || $c/@id || ' type ' || $c/@type || ' SHALL refer to an existing template.')
                        )
                    else ()
                    ,
                    if ($c[count(representingTemplate/@representingQuestionnaire) = 1]) then 
                        if ($questionnaire) then () else ( 
                            error($errors:BAD_REQUEST, $storedObjectType || ' ' || $c/@id || ' type ' || $c/@type || ' SHALL refer to an existing questionnaire.')
                        )
                    else ()
                )
            )
            else ()
        )
    (:let $check              :=
        for $c in $editedScenario[move] | $editedScenario/descendant::transaction[move]
        let $storedObject       := $storedScenario[@id = $c/@id][@effectiveDate = $c/@effectiveDate] | $storedScenario/descendant::transaction[@id = $c/@id][@effectiveDate = $c/@effectiveDate]
        let $storedObjectType   := upper-case(substring(local-name($c), 1, 1)) || substring(local-name($c), 2)
        return (
            if ($c/@id and empty($storedObject)) then
                error($errors:BAD_REQUEST, $storedObjectType || ' ' || $c/@id || ' effectiveDate ''' || $c/@effectiveDate || ''' SHALL exist as/in stored scenario on the server. Cannot move non-existent parts.')
            else ()
        ):)
    
    (: save history:)
    let $intention                      :=
        if ($storedScenario[@statusCode='final'][@effectiveDate = $editedScenario/@effectiveDate]) then 'patch' else 'version'
    let $history                        :=
        if ($storedScenario and $editedScenario/descendant-or-self::*[edit | move]) then 
            histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-SCENARIO, $projectPrefix, $intention, $storedScenario)
        else ()
        
    (: start with deleting concepts:
       if statusCode=new     delete concept
       if statusCode!=new    set statusCode=deprecated
    :)
    
    (:let $deletes           :=
        for $concept in $editedScenario//concept[edit/@mode='delete']
        let $storedObject := $storedScenario//concept[@id = $concept/@id][@effectiveDate = $concept/@effectiveDate][not(ancestor::history)]
        let $lock          := decorlib:getLocks($authmap, $storedObject, false())
        return
            if ($editedScenario/@statusCode != 'new') then
                if ($storedObject/@statusCode = 'new' and $lock) then (
                    histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-DATASETCONCEPT, $projectPrefix, 'delete', $storedObject),
                    update delete $storedObject,
                    update delete $lock
                )
                else 
                if ($storedObject/@statusCode != 'new' and $lock) then (
                    if ($concept/@type='item') then (
                        histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-DATASETCONCEPT, $projectPrefix, 'delete', $storedObject),
                        update replace $storedObject with deapi:prepareItemForUpdate($concept, $storedObject),
                        update delete $lock
                    )
                    else if ($concept/@type='group') then (
                        histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-DATASETCONCEPT, $projectPrefix, 'delete', $storedObject),
                        update replace $storedObject with deapi:prepareGroupForUpdate($concept, $storedObject),
                        update delete $lock
                    ) else ()
                ) 
                else ()
            else (
                if ($lock) then (
                    histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-DATASETCONCEPT, $projectPrefix, 'delete', $storedObject),
                    update delete $storedObject,
                    update delete $lock
                ) else ()
            ):)
    (:
       move transactions
       - local move
          - delete stored transaction
          - insert moved transaction after editedScenario preceding-sibling or into parent scenario/transaction
       - move to other scenario (unsupported!!)
    :)
    let $baseIdTransaction          := decorlib:getDefaultBaseIdsP($decor, $decorlib:OBJECTTYPE-TRANSACTION)[1]/@id
    let $nextTransactionId          := decorlib:getNextAvailableIdP($decor, $decorlib:OBJECTTYPE-TRANSACTION, $baseIdTransaction)
    let $insert                     :=
        if ($decor/scenarios[@nextTransactionLeaf]) then
            update value $decor/scenarios/@nextTransactionLeaf with $nextTransactionId/@next
        else (
            update insert attribute nextTransactionLeaf {$nextTransactionId/@next} into $decor/scenarios
        )
    let $preparedScenario           := scapi:prepareScenarioForUpdate($editedScenario, $storedScenario, $baseIdTransaction)
    
    let $locks                      := if ($deletelock) then decorlib:getLocks($authmap, $storedScenario, true()) else ()
    let $updates                    := update replace $storedScenario with $preparedScenario
    let $delete                     := update delete $decor/scenarios/@nextTransactionLeaf
    
    let $delete                     := if ($locks) then update delete $locks else ()
    
    return ()
};

(:~ Central logic for patching an existing scenario

@param $authmap         - required. Map derived from token
@param $id              - required. DECOR scenario/@id to update
@param $effectiveDate   - required. DECOR scenario/@effectiveDate to update
@param $data            - required. DECOR scenario xml element containing everything that should be in the updated scenario
@return concept object as xml with json:array set on elements
:)
declare function scapi:patchScenario($authmap as map(*), $id as xs:string, $effectiveDate as xs:string, $data as element(parameters)) as element(scenario) {

    let $storedScenario         := utillib:getScenario($id, $effectiveDate)
    let $decor                  := $storedScenario/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix

    let $lock                   := decorlib:getLocks($authmap, $id, $effectiveDate, ())
    let $lock                   := if ($lock) then $lock else decorlib:setLock($authmap, $id, $effectiveDate, false())
    
    let $check                  :=
        if ($storedScenario) then () else (
            error($errors:BAD_REQUEST, 'Scenario with id ' || $id || ' and effectiveDate ' || $effectiveDate || ' does not exist')
        )
    let $check                  :=
        if ($storedScenario[@statusCode = $scapi:STATUSCODES-FINAL]) then
            if ($data/parameter[@path = '/statusCode'][@op = 'replace'][not(@value = $scapi:STATUSCODES-FINAL)]) then () else 
            if ($data[count(parameter) = 1]/parameter[@path = '/statusCode']) then () else (
                error($errors:BAD_REQUEST, concat('Scenario cannot be patched while it has one of status: ', string-join($scapi:STATUSCODES-FINAL, ', '), if ($storedScenario/@statusCode = 'pending') then 'You should switch back to status draft to edit.' else ()))
            )
        else ()
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-SCENARIOS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify scenarios in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if ($lock) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have a lock for this scenario (anymore). Get a lock first.'))
        )
    let $check                  :=
        if ($data[parameter]) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall be an array of ''parameter''.')
        )
    let $unsupportedops         := $data/parameter[not(@op = ('add', 'replace', 'remove'))]
    let $check                  :=
        if ($unsupportedops) then
            error($errors:BAD_REQUEST, 'Submitted parameters shall have supported ''op'' value. Found ''' || string-join(distinct-values($unsupportedops/@op), ''', ''') || ''', expected ''add'', ''replace'', or ''remove''') 
        else ()
    let $checkn                 := (
        if ($data/parameter[@path = '/name']) then
            if (count($storedScenario/name) - count($data/parameter[@op = 'remove'][@path = '/name']) + count($data/parameter[@op = ('add', 'replace')][@path = '/name']) ge 1) then () else (
                'A scenario SHALL have at least one name. You cannot remove every name.'
            )
        else ()
        (: https://art-decor.atlassian.net/browse/AD30-451 :)
        (:,
        if ($data/parameter[@path = '/desc']) then
            if (count($storedScenario/desc) - count($data/parameter[@op = 'remove'][@path = '/desc']) + count($data/parameter[@op = ('add', 'replace')][@path = '/desc']) ge 1) then () else (
                'A scenario SHALL have at least one description. You cannot remove every desc.'
            )
        else ():)
    )
    
    let $check                  :=
        for $param in $data/parameter
        let $op         := $param/@op
        let $path       := $param/@path
        let $value      := $param/@value
        let $pathpart   := substring-after($path, '/')
        return
            switch ($path)
            case '/statusCode' return (
                if ($op = 'remove') then
                    'Parameter path ''' || $path || ''' does not support ' || $op
                else 
                if (utillib:isStatusChangeAllowable($storedScenario, $value)) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Supported are: ' || string-join(map:get($utillib:itemstatusmap, string($storedScenario/@statusCode)), ', ')
                )
            )
            case '/expirationDate'
            case '/officialReleaseDate' return (
                if ($op = 'remove' or ($op = 'replace' and string($value) = '')) then () else 
                if ($value castable as xs:dateTime) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL be yyyy-mm-ddThh:mm:ss.'
                )
            )
            case '/canonicalUri'
            case '/versionLabel' return (
                if ($op = 'remove') then () else
                (:if ($op = 'remove' or ($op = 'replace' and string($value) = '')) then () else:) 
                if ($value[not(. = '')]) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL NOT be empty.'
                )
            )
            case '/name' 
            case '/desc' 
            case '/trigger'
            case '/condition'
            case '/copyright' return (
                if ($param[count(value/*[name() = $pathpart]) = 1]) then (
                    if ($param/value/*[name() = $pathpart][@inherited]) then 
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Contents SHALL NOT be marked ''inherited''.'
                    else (),
                    if ($param/value/*[name() = $pathpart][matches(@language, '[a-z]{2}-[A-Z]{2}')]) then 
                        if ($param[@op = 'remove'] | $param/value/*[name() = $pathpart][.//text()[not(normalize-space() = '')]]) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A ' || $pathpart || ' SHALL have contents.'
                        )
                    else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A ' || $pathpart || ' SHALL have a language with pattern ll-CC.'
                    )
                )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/*[name() = $pathpart]) 
                )
            )
            case '/publishingAuthority' return (
                if ($param[count(value/publishingAuthority) = 1]) then (
                    if ($param/value/publishingAuthority[@inherited]) then 
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Contents SHALL NOT be marked ''inherited''.'
                    else (),
                    if ($param/value/publishingAuthority[@id]) then
                        if ($param/value/publishingAuthority[utillib:isOid(@id)]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || ''' publishingAuthority.id SHALL be an OID is present. Found ' || string-join($param/value/publishingAuthority/@id, ', ')
                        )
                    else (),
                    if ($param/value/publishingAuthority[@name[not(. = '')]]) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || ''' publishingAuthority.name SHALL be a non empty string.'
                    ),
                    let $unsupportedtypes := $param/value/publishingAuthority/addrLine/@type[not(. = $scapi:ADDRLINE-TYPE/enumeration/@value)]
                    return
                        if ($unsupportedtypes) then
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Publishing authority has unsupported addrLine.type value(s) ' || string-join($unsupportedops, ', ') || '. SHALL be one of: ' || string-join($scapi:ADDRLINE-TYPE/enumeration/@value, ', ')
                        else ()
                ) else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' SHALL have a single publishingAuthority under value. Found ' || count($param/value/publishingAuthority)
                )
            )
            case '/property' return (
                if ($param[count(value/property) = 1]) then
                    if ($param/value/property/@name) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || '''. Property.name SHALL be a non empty string.'
                    )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/property) 
                ),
                if ($param/value/property[@datatype]) then 
                    if ($param/value/property[@datatype = $scapi:VALUEDOMAIN-TYPE/enumeration/@value]) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || ''' with type ''' || string-join($param/value/property/@datatype, ' ') || '''. Supported are: ' || string-join($scapi:VALUEDOMAIN-TYPE/enumeration/@value, ', ')
                    )
                else ()
            )
            default return (
                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Path value not supported'
            )
     let $check                 :=
        if (empty($checkn) and empty($check)) then () else (
            error($errors:BAD_REQUEST,  string-join (($checkn, $check), ' '))
        )
    
    let $intention              := if ($storedScenario[@statusCode = 'final']) then 'patch' else 'version'
    let $update                 := histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-SCENARIO, $projectPrefix, $intention, $storedScenario)
    
    let $update                 :=
        for $param in $data/parameter
        return
            switch ($param/@path)
            case '/statusCode' return (
                let $attname  := substring-after($param/@path, '/')
                let $new      := attribute {$attname} {$param/@value}
                let $stored   := $storedScenario/@*[name() = $attname]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedScenario
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/expirationDate'
            case '/officialReleaseDate'
            case '/canonicalUri'
            case '/versionLabel' return (
                let $attname  := substring-after($param/@path, '/')
                let $op       := if ($param[string-length(@value) = 0]) then 'remove' else $param/@op
                let $new      := attribute {$attname} {$param/@value}
                let $stored   := $storedScenario/@*[name() = $attname]
                
                return
                switch ($op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedScenario
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/name' 
            case '/desc'
            case '/condition' return (
                (: only one per language :)
                let $elmname  := substring-after($param/@path, '/')
                let $new      := utillib:prepareFreeFormMarkupWithLanguageForUpdate($param/value/*[name() = $elmname])
                let $stored   := $storedScenario/*[name() = $elmname][@language = $param/value/*[name() = $elmname]/@language]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedScenario
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/trigger' return (
                (: only one per language :)
                let $elmname  := substring-after($param/@path, '/')
                let $new      := utillib:prepareTransactionTriggerForUpdate($param/value/*[name() = $elmname])
                let $stored   := $storedScenario/*[name() = $elmname][@language = $param/value/*[name() = $elmname]/@language]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedScenario
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/publishingAuthority' return (
                (: only one possible :)
                let $new      := utillib:preparePublishingAuthorityForUpdate($param/value/publishingAuthority)
                let $stored   := $storedScenario/publishingAuthority
                
                return
                switch ($param/@op)
                case ('add') return update insert $new into $storedScenario
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedScenario
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/property' return (
                (: multiple possible per language :)
                let $new      := utillib:prepareConceptPropertyForUpdate($param/value/property)
                let $stored   := $storedScenario/property[@name = $new/@name][lower-case(normalize-space(string-join(.//text(), ''))) = lower-case(normalize-space(string-join($new//text(), '')))]
                
                return
                switch ($param/@op)
                case ('add') return update insert $new into $storedScenario
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedScenario
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/copyright' return (
                (: only one per language :)
                let $new      := utillib:prepareFreeFormMarkupWithLanguageForUpdate($param/value/copyright)
                let $stored   := $storedScenario/copyright[@language = $param/value/copyright/@language]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedScenario
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            default return ( (: unknown path :) )
    
    (: after all the updates, the XML Schema order is likely off. Restore order :)
    let $preparedScenario       := scapi:handleScenario($storedScenario)
    
    let $update                 := update replace $storedScenario with $preparedScenario
    let $update                 := update delete $lock
    
    return
        element {name($preparedScenario)} {
            $preparedScenario/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($preparedScenario/*)
        }
};

(:~ Central logic for deleting a scenario. If the item does not exist, then no error is given.

@param $id              - required. DECOR scenario/@id to delete
@param $effectiveDate   - optional. DECOR scenario/@effectiveDate to delete
@return nothing
:)
declare function scapi:deleteScenario($authmap as map(*)?, $id as xs:string, $effectiveDate as xs:string?) {
    let $storedScenario         := scapi:getScenario($id, $effectiveDate, (), (), ())
    let $decor                  := $storedScenario/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if ($authmap?groups = 'dba') then () else (
            error($errors:FORBIDDEN, 'You need to be dba for this feature')
        )    
    let $update                 := update delete $storedScenario
    
    return
        ()
};

(:~ Returns list object with zero or more child scenarios from project
    
@param $projectPrefix           - required. limits scope to this project only
@param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
@param $projectLanguage         - optional parameter to select from a specific compiled language
@param $datasetId               - optional. Filter output to just those based on this dataset-id
@param $datasetEffectiveDate    - optional. null filters to the newest version based on $datasetId, yyyy-mm-ddThh:mm:ss for this specific version
@param $sort                    - optional parameter to indicate sorting. Only option 'name'
@param $sortorder               - optional parameter to indicate sort order. Only option 'descending'
@return list with actors
@since 2020-05-03
:)
declare function scapi:getScenarioList($projectPrefix as xs:string, $projectVersion as xs:string?, $projectLanguage as xs:string?, $datasetId as xs:string?, $datasetEffectiveDate as xs:string?, $sort as xs:string?, $sortorder as xs:string?, $max as xs:integer?) {

    let $sort                 := $sort[string-length() gt 0]
    let $sortorder            := $sortorder[. = 'descending']
    
    let $projectPrefix        := $projectPrefix[not(. = '')]
    let $projectVersion       := $projectVersion[not(. = '')]
    let $decor                := if (empty($projectPrefix)) then () else utillib:getDecorByPrefix($projectPrefix, $projectVersion, $projectLanguage)
    
    let $allcnt               := count($decor/scenarios/scenario)
    
    let $datasetId            := $datasetId[not(. = '')]
    let $datasetEffectiveDate := $datasetEffectiveDate[not(. = '')]
    
    let $filteredScenarios    := 
        if (empty($datasetId)) then $decor/scenarios/scenario else (
            utillib:getScenariosByDataset($datasetId, $datasetEffectiveDate, $projectVersion, $projectLanguage)
        )
    (: only want to offer scenarios from our own project. If someone added a dataset from our project 
        onto his transaction, we would have scenarios from other projects :)
    let $filteredScenarios    := $filteredScenarios[ancestor::decor/project[@prefix = $decor/project/@prefix]]

    let $transactionmap       :=
        map:merge(
            if (empty($datasetId)) then (
                for $key in distinct-values($filteredScenarios/transaction/concat(@id, @effectiveDate))
                return
                    map:entry($key, ())
            )
            else (
                for $key in distinct-values(utillib:getTransactionsByDataset($datasetId, $datasetEffectiveDate)/ancestor-or-self::transaction[parent::scenario]/concat(@id, @effectiveDate))
                return
                    map:entry($key, ())
            )
        )
    let $projectLanguage      := 
        if (empty($projectLanguage)) 
        then 
            if (empty($projectPrefix)) then
                $filteredScenarios/ancestor::decor/project/@defaultLanguage
            else
                $decor/project/@defaultLanguage
        else $projectLanguage
    
    (: build map of oid names. is more costly when you do that deep down :)
    let $oidnamemap           := scapi:buildOidNameMap($filteredScenarios, $projectLanguage[1])
    (: build map of template attributes. is more costly when you do that deep down :)
    let $templatemap          := 
        for $sc in $filteredScenarios
        let $pfx    := $sc/ancestor::decor/project/@prefix
        group by $pfx
        return
            scapi:buildTemplateMap($sc, $sc[1]/ancestor::decor)
    
    let $filteredScenarios    :=
        for $sc in $filteredScenarios
        return
        element {name($sc)} {
            $sc/@*,
            $sc/name,
            $sc/desc,
            if ($sc[publishingAuthority]) then $sc/publishingAuthority else utillib:inheritPublishingAuthority($sc),
            if ($sc[copyright]) then $sc/copyright else utillib:inheritCopyright($sc),
            $sc/(* except (name | desc | publishingAuthority | copyright | transaction)),
            for $trg in $sc/transaction
            return
                element {name($trg)} {
                $trg/@*,
                $trg/name,
                $trg/desc,
                if ($trg[publishingAuthority]) then $trg/publishingAuthority else utillib:inheritPublishingAuthority($trg),
                if ($trg[copyright]) then $trg/copyright else utillib:inheritCopyright($trg),
                $trg/(* except (name | desc | publishingAuthority | copyright | transaction)),
                for $trl in $trg/transaction
                return
                    element {name($trl)} {
                    $trl/@*,
                    $trl/name,
                    $trl/desc,
                    if ($trl[publishingAuthority]) then $trl/publishingAuthority else utillib:inheritPublishingAuthority($trl),
                    if ($trl[copyright]) then $trl/copyright else utillib:inheritCopyright($trl),
                    $trl/(* except (name | desc | publishingAuthority | copyright))
                }
            }
         }

    (:can sort on indexed db content -- faster:)
    let $results              :=
        switch ($sort) 
        case 'effectiveDate'    return 
            if (empty($sortorder)) then 
                for $scenario in $filteredScenarios order by $scenario/@effectiveDate return $scenario
            else
                for $scenario in $filteredScenarios order by $scenario/@effectiveDate descending return $scenario
        case 'name'    return 
            if (empty($sortorder)) then 
                for $scenario in $filteredScenarios order by lower-case(($scenario/name[@language = $decor/project/@defaultLanguage], $scenario/name)[1]) return $scenario
            else
                for $scenario in $filteredScenarios order by lower-case(($scenario/name[@language = $decor/project/@defaultLanguage], $scenario/name)[1]) descending return $scenario
        default                 return $filteredScenarios
    
    let $count              := count($results)
    let $max                := if ($max ge 1) then $max else $scapi:maxResults
    
    return
    <list artifact="SC" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        <actors json:array="true"/>
    {
        utillib:addJsonArrayToElements(
            for $scenario in $results
            return
                scapi:scenarioBasics($scenario, $transactionmap, $projectPrefix, $projectLanguage[1], $oidnamemap, $templatemap)
        )
    }
    </list>
};

(:~ Build a map that has oid as key and human readable version as value :)
declare %private function scapi:buildOidNameMap($scenariosOrTransactions as element()*, $language as xs:string?) as map(*) {
    (:map:merge(
        for $oid in ($scenariosOrTransactions/@id | $scenariosOrTransactions/descendant-or-self::transaction/@id)
        let $grp := $oid
        group by $grp
        return
            map:entry($oid[1], utillib:getNameForOID($oid[1], $language, $oid[1]/ancestor::decor))
    ):)
    map {}
};

(:~ Build a map that has template id+effectiveDate as key and all template meta properties (name, displayName, versionLabel, ident, url etc.) as value :)
declare %private function scapi:buildTemplateMap($scenariosOrTransactions as element()*, $decor as element(decor)) as map(*)? {
    map:merge(
        for $representingTemplate in $scenariosOrTransactions/descendant-or-self::representingTemplate[@ref[not(. = '')]]
        let $tmid       := $representingTemplate/@ref
        let $tmed       := $representingTemplate/@flexibility[. castable as xs:dateTime]
        let $tmided     := concat($tmid, $tmed)
        group by $tmided
        return (
            let $tmid           := $representingTemplate[1]/@ref
            let $tmed           := $representingTemplate[1]/@flexibility[. castable as xs:dateTime]
            let $templates      := $setlib:colDecorCache//template[@id = $tmid] | $setlib:colDecorData//template[@id = $tmid]
            let $flexibility    := if ($tmed) then $tmed else max($templates/xs:dateTime(@effectiveDate))
            let $templates      := $templates[@effectiveDate = $flexibility]
            let $templates      := if ($templates) then $templates else $decor//template[@ref = $tmid]
            
            return
                map:entry($tmided, <template>{$templates[1]/(@* except (@ident|@url)),
                    if ($templates[1]/ancestor::cacheme) then (
                        attribute ident {$templates[1]/ancestor::cacheme/@bbrident},
                        attribute url {$templates[1]/ancestor::cacheme/@bbrurl}
                    ) else
                    if ($decor/project/@prefix = $templates[1]/ancestor::decor/project/@prefix) then () else (
                        attribute ident {$templates[1]/ancestor::decor/project/@prefix},
                        attribute url {serverapi:getServerURLServices()}
                    )
                }</template>)
        )
    )
};

(:~ Returns list object with zero or more child actors from project
    
@param $projectPrefix           - required. limits scope to this project only
@param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
@param $projectLanguage         - optional parameter to select from a specific compiled language
@param $sort                    - optional parameter to indicate sorting. Only option 'name'
@param $sortorder               - optional parameter to indicate sort order. Only option 'descending'
@return list with actors
@since 2020-05-03
:)
declare function scapi:getScenarioActorList($projectPrefix as xs:string, $projectVersion as xs:string?, $projectLanguage as xs:string?, $sort as xs:string?, $sortorder as xs:string?, $max as xs:integer?) {

    let $sort                 := $sort[string-length() gt 0]
    let $sortorder            := $sortorder[. = 'descending']
    let $decor                := if (empty($projectPrefix)) then () else utillib:getDecorByPrefix($projectPrefix, $projectVersion, $projectLanguage)
    let $actors               := $decor/scenarios/actors/actor
    
    let $allcnt               := count($actors)
    
    (:can sort on indexed db content -- faster:)
    let $results              :=
        switch ($sort) 
        case 'name'         return 
            if (empty($sortorder)) then 
                for $actor in $actors order by lower-case(($actor/name[@language = $decor/project/@defaultLanguage], $actor/name)[1]) return $actor
            else
                for $actor in $actors order by lower-case(($actor/name[@language = $decor/project/@defaultLanguage], $actor/name)[1]) descending return $actor
        default             return $actors
    
    let $count              := count($results)
    let $max                := if ($max ge 1) then $max else $scapi:maxResults
    
    return
        <list artifact="AC" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements(subsequence($results, 1, $max))
        }
        </list>

};

(:~ Return zero or one actor as-is

@param $projectPrefix           - optional. the project prefix to get the actor from (only relevant when some kind of clash between projects occurred)
@param $id                      - required. the id for the actor to retrieve. gets all actors if empty
@return actor or nothing
@since 2014-07-09
:)
declare function scapi:getScenarioActor($projectPrefix as xs:string?, $id as xs:string) as element(actor)* {
    let $actor    := $setlib:colDecorData//scenarios/actors/actor[@id = $id]
    
    return
        if (empty($projectPrefix)) then $actor else $actor[ancestor::decor/project[@prefix = $projectPrefix]]
};

(:~ Create scenario actor

@param $authmap      - required. Map derived from token
@param $prefix       - required. DECOR project/@prefix
@param $data         - required. DECOR actor object as xml
@return actor object as xml
:)
declare function scapi:postScenarioActor($authmap as map(*), $projectPrefix as xs:string, $data as element(actor)?) as element(actor) {
    let $decor                  := utillib:getDecorByPrefix($projectPrefix)
    let $defaultLanguage        := $decor/project/@defaultLanguage
    let $actorType              := ($data/@type, $scapi:ACTOR-TYPE/enumeration/@value)[1]
    
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix ''', $projectPrefix, ''', does not exist. Cannot create in non-existent project.'))
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-SCENARIOS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify scenarios in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if ($actorType = $scapi:ACTOR-TYPE/enumeration/@value) then () else (
            error($errors:BAD_REQUEST, concat('Actor type ''', $actorType, ''' not supported. Supported are: ', string-join($scapi:ACTOR-TYPE/enumeration/@value, ', ')))
        )
    let $check                  :=
        if ($data[name[text()[not(. = '')]]/@language]) then
            if ($data[count(name/@language) = count(distinct-values(name/@language))]) then () else (
                error($errors:BAD_REQUEST, 'Actor name SHALL be unique in language. Found languages: ' || string-join($data/name/@language, ', '))
            )
        else (
            error($errors:BAD_REQUEST, 'Actor SHALL have at least one non-empty name marked with a language format ll-CC')
        )
    let $check                  :=
        if ($data[desc/@language]) then
            if ($data[count(desc/@language) = count(distinct-values(desc/@language))]) then () else (
                error($errors:BAD_REQUEST, 'Actor desc SHALL be unique in language. Found languages: ' || string-join($data/desc/@language, ', '))
            )
        else (
            (: optional element :)
            (:error($errors:BAD_REQUEST, 'Actor SHALL have at least one desc marked with a language'):)
        )
    
    let $newId                  := decorlib:getNextAvailableIdP($decor, $decorlib:OBJECTTYPE-ACTOR, decorlib:getDefaultBaseIdsP($decor, $decorlib:OBJECTTYPE-ACTOR)/@id)/@id
    
    (: guarantee defaultLanguage. Preserve any incoming other languages. Guarantee name. Leave desc optional :)
    let $newActor               :=
        <actor id="{$newId}" type="{$actorType}">
        {
            if ($data/name[@language = $defaultLanguage]) then 
                $data/name[@language = $defaultLanguage][1]
            else (
                <name language="{$defaultLanguage}">
                {
                    data($data/name[1])
                }
                </name>
            ),
            for $lang in distinct-values($data/name/@language[not(. = $defaultLanguage)])
            return 
                $data/name[@language = $lang][1]
            ,
            utillib:prepareFreeFormMarkupWithLanguageForUpdate($data/desc)
        }
        </actor>
        
    let $actorsAvailable    :=
        if ($decor/scenarios/actors) then
            update insert $newActor into $decor/scenarios/actors
        else
        if ($decor/scenarios) then
            update insert <actors>{$newActor}</actors> preceding $decor/scenarios/node()[1]
        else
            update insert <scenarios><actors>{$newActor}</actors></scenarios> preceding $decor/ids

    return
        $newActor
};

(:~ Central logic for patching an existing scenario actor type|name|desc

@param $authmap         - required. Map derived from token
@param $id              - required. DECOR actor/@id to update
@param $projectPrefix   - optional. DECOR project/@prefix to update
@param $request-body    - required. DECOR xml element parameter elements each containing RFC 6902 compliant contents
@return actor object as xml with json:array set on elements
:)
declare function scapi:patchScenarioActor($authmap as map(*), $projectPrefix as xs:string?, $id as xs:string, $data as element()) as element(actor) {
(:
<parameters>
    <parameter op="replace" path="/displayName" value="new value"/>
    <parameter op="replace" path="/priority" value="new value"/>
    <parameter op="add" path="/">
        <value>
            <tracking statusCode="closed">
                <desc language="de-DE">yadadadad</desc>
            </tracking>
        </value>
    </parameter>
</parameters>
:)
    let $actor                  := scapi:getScenarioActor($projectPrefix, $id)
    let $decor                  := $actor/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if ($actor) then () else (
            error($errors:BAD_REQUEST, concat('Actor with id ''', $id, ''' does not exist'))
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-SCENARIOS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify actors in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if ($data[parameter]) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall be an array of ''parameter''.')
        )
    let $unsupportedops         := $data/parameter[not(@op = ('add', 'replace', 'remove'))]
    let $check                  :=
        if ($unsupportedops) then
            error($errors:BAD_REQUEST, 'Submitted parameters shall have supported ''op'' value. Found ''' || string-join(distinct-values($unsupportedops/@op), ''', ''') || ''', expected ''add'', ''replace'', or ''remove''') 
        else ()
    let $checkn                 := (
        if ($data/parameter[@path = '/name']) then
            if (count($actor/name) - count($data/parameter[@op = 'remove'][@path = '/name']) + count($data/parameter[@op = ('add', 'replace')][@path = '/name']) ge 1) then () else (
                'An actor SHALL have at least one name. You cannot remove every name.'
            )
        else ()
        (: https://art-decor.atlassian.net/browse/AD30-451 :)
        (:,
        if ($data/parameter[@path = '/desc']) then
            if (count($actor/desc) - count($data/parameter[@op = 'remove'][@path = '/desc']) + count($data/parameter[@op = ('add', 'replace')][@path = '/desc']) ge 1) then () else (
                'An actor SHALL have at least one description. You cannot remove every desc.'
            )
        else ():)
    )
    let $check                  :=
        for $param in $data/parameter
        let $op         := $param/@op
        let $path       := $param/@path
        let $value      := $param/@value
        let $pathpart   := substring-after($path, '/')
        return
            switch ($path)
            case '/type' return (
                if ($op = 'remove') then
                    'Parameter path ''' || $path || ''' does not support ' || $op
                else 
                if ($param[@value = $scapi:ACTOR-TYPE/enumeration/@value]) then () else (
                    concat('Parameter value ''', $param/@value, ''' for type not supported. Supported are: ', string-join($scapi:ACTOR-TYPE/enumeration/@value, ' '))
                )
            )
            case '/name' 
            case '/desc' return (
                if ($param[count(value/*[name() = $pathpart]) = 1]) then
                    if ($param/value/*[name() = $pathpart][matches(@language, '[a-z]{2}-[A-Z]{2}')]) then 
                        if ($param[@op = 'remove'] | $param/value/*[name() = $pathpart][.//text()[not(normalize-space() = '')]]) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A ' || $pathpart || ' SHALL have contents.'
                        )
                    else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A ' || $pathpart || ' SHALL have a language with pattern ll-CC.'
                    )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/*[name() = $pathpart]) 
                )
            )
            default return ( 
                concat('Parameter combination not supported: op=''', $param/@op, ''' path=''', $param/@path, '''', if ($param[@value]) then concat(' value=''', $param/@value, '''') else string-join($param/value/*/name(), ' '), '.')
            )
     let $check                 :=
        if (empty($checkn) and empty($check)) then () else (
            error($errors:BAD_REQUEST,  string-join (($checkn, $check), ' '))
        )
    
    let $update                 :=
        for $param in $data/parameter
        return
            switch ($param/@path)
            case '/type' return (
                let $attname  := substring-after($param/@path, '/')
                let $new      := attribute {$attname} {$param/@value}
                let $stored   := $actor/@*[name() = $attname]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $actor
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/name' 
            case '/desc' return (
                (: only one per language :)
                let $elmname  := substring-after($param/@path, '/')
                let $new      := utillib:prepareFreeFormMarkupWithLanguageForUpdate($param/value/*[name() = $elmname])
                let $stored   := $actor/*[name() = $elmname][@language = $param/value/*[name() = $elmname]/@language]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $actor
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            default return ( (: unknown path :) )
    
    let $result                     := scapi:getScenarioActor($projectPrefix, $id)
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

(:~ Central logic for deleting a scenario actor. If the item does not exist, then no error is given.

@param $projectPrefix   - optional. DECOR project/@prefix to update
@param $id              - required. DECOR actor/@id to update
@return nothing
:)
declare function scapi:deleteScenarioActor($authmap as map(*)?, $projectPrefix as xs:string?, $id as xs:string) {
    let $actor                  := scapi:getScenarioActor($projectPrefix, $id)
    let $decor                  := $actor/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if ($authmap?groups = 'dba') then () else (
            error($errors:FORBIDDEN, 'You need to be dba for this feature')
        )
    let $inUse                  := $setlib:colDecorData//actor[@id = $id][ancestor::transaction]
    let $check                  :=
        if ($setlib:colDecorData//actor[@id = $id][ancestor::transaction]) then
            error($errors:BAD_REQUEST, 'Actor ' || $id || ' cannot be deleted while it is in use. Found usage in transaction(s): ' ||
                string-join(
                    for $a in $inUse
                    let $t  := $a/ancestor::transaction[1]
                    let $p  := $a/ancestor::decor[1]/project/@prefix
                    let $p  := if ($p = $projectPrefix) then () else (' project prefix=' || $a/ancestor::decor[1]/project/@prefix)
                    return
                        '''' || $t/name[1] || ''' (id=' || $t/@id || $p || ')'''
                , ', ')
            )
        else ()
    
    let $update                 := update delete $actor
    
    return
        ()
};

declare %private function scapi:filterDataset($concept as element(),$representingTemplate as element()) as element()* {
    let $id := $concept/@id
    return
        if (exists($representingTemplate/concept[@ref=$id])) then
            $concept
        else (),
        for $subConcept in $concept/concept
        return
            scapi:filterDataset($subConcept,$representingTemplate)

};

declare %private function scapi:scenarioBasics($scenario as element(), $transactionmap as map(*), $projectPrefix as xs:string?, $projectLanguage as xs:string?, $oidnamemap as map(*), $templatemap as map(*)) as element() {
    let $iddisplay              := () (:map:get($oidnamemap, $scenario/@id):)
    return
        <scenario>
        {
            attribute uuid {util:uuid()},
            $scenario/@id,
            $scenario/@effectiveDate,
            $scenario/@statusCode,
            attribute versionLabel {$scenario/@versionLabel},
            attribute expirationDate {$scenario/@expirationDate},
            attribute officialReleaseDate {$scenario/@officialReleaseDate},
            attribute canonicalUri {$scenario/@canonicalUri},
            $scenario/@lastModifiedDate,
            if (string-length($iddisplay) = 0) then () else attribute iddisplay {$iddisplay},
            if ($scenario/ancestor::decor/project[@prefix = $projectPrefix]) then () else (
                attribute ident {$scenario/ancestor::decor/project/@prefix}
            )
            (:AD30-748 disabled because it triggers utillib:addJsonArrayToElements() into serializing all of the content :)
            (:,
            $scenario/ancestor::decor/@versionDate,
            $scenario/ancestor::decor/@language:)
            ,
            if (empty($projectLanguage)) then () else
            if ($projectLanguage='*') then () else
            if ($scenario/name[@language = $projectLanguage]) then () else (
                <name language="{$projectLanguage}">{($scenario/name[@language = 'en-US'], $scenario/name)[1]/node()}</name>
            )
            ,
            for $node in $scenario/name
            return
                utillib:serializeNode($node)
            ,
            for $node in $scenario/desc
            return
                utillib:serializeNode($node)
            ,
            if ($scenario[desc/@language=$projectLanguage]) then () else (
                <desc language="{$projectLanguage}"/>
            ),
            for $node in $scenario/trigger
            return
                utillib:serializeNode($node)
            ,
            for $node in $scenario/condition
            return
                utillib:serializeNode($node)
            ,
            (: new ... 2021-05-21 :)
            for $node in $scenario/publishingAuthority
            return
                <publishingAuthority>
                {
                    $node/@id[not(. = '')],
                    $node/@name[not(. = '')],
                    $node/@inherited[not(. = '')],
                    for $addr in $node/addrLine
                    return
                        utillib:serializeNode($addr)
                }
                </publishingAuthority>
            ,
            (: new ... 2021-05-21 :)
            for $node in $scenario/property
            return
                utillib:serializeNode($node)
            ,
            (: new ... 2021-05-21 :)
            for $node in $scenario/copyright
            return
                utillib:serializeNode($node)
            ,
            for $transaction in $scenario/transaction[map:contains($transactionmap, concat(@id, @effectiveDate))]
            return
                scapi:transactionBasics($transaction, $projectPrefix, $projectLanguage, $oidnamemap, $templatemap)
        }
        </scenario>
};

declare %private function scapi:transactionBasics($transaction as element(), $projectPrefix as xs:string?, $projectLanguage as xs:string?, $oidnamemap as map(*), $templatemap as map(*)) as element() {
    let $iddisplay              := () (:map:get($oidnamemap, $transaction/@id):)
    return
        (:  transactions got versionable later in time, so may need to patch for content. Assume that it inherits 
            from e.g. scenario if it doesn't have its own data :)
        <transaction>
        {
            attribute uuid {util:uuid()},
            $transaction/@id,
            (:DO NOT CREATE HERE. This attribute, if missing gets created in the save-process:)
            $transaction/@effectiveDate,
            $transaction/@statusCode,
            attribute versionLabel {$transaction/@versionLabel},
            $transaction/@expirationDate,
            $transaction/@officialReleaseDate,
            attribute type {$transaction/@type},
            attribute label {$transaction/@label},
            attribute model {$transaction/@model},
            attribute canonicalUri {$transaction/@canonicalUri},
            $transaction/@lastModifiedDate,
            if (string-length($iddisplay) = 0) then () else attribute iddisplay {$iddisplay},
            if ($transaction/ancestor::decor/project[@prefix = $projectPrefix]) then () else (
                attribute ident {$transaction/ancestor::decor/project/@prefix}
            )
            ,
            if (empty($projectLanguage)) then () else
            if ($projectLanguage='*') then () else
            if ($transaction/name[@language=$projectLanguage]) then () else (
                <name language="{$projectLanguage}">{($transaction/name[@language = 'en-US'], $transaction/name)[1]/node()}</name>
            )
            ,
            for $node in $transaction/name
            return
                utillib:serializeNode($node)
            ,
            for $node in $transaction/desc
            return
                utillib:serializeNode($node)
            ,
            if (empty($projectLanguage)) then () else
            if ($projectLanguage='*') then () else
            if ($transaction[desc/@language=$projectLanguage]) then () else (
                <desc language="{$projectLanguage}"/>
            ),
            for $node in $transaction/trigger
            return
                utillib:serializeNode($node)
            ,
            for $node in $transaction/condition
            return
                utillib:serializeNode($node)
            ,
            for $node in $transaction/dependencies
            return
                utillib:serializeNode($node)
            ,
            (: new ... 2021-05-21 :)
            for $node in $transaction/publishingAuthority
            return
                <publishingAuthority>
                {
                    $node/@id[not(. = '')],
                    $node/@name[not(. = '')],
                    $node/@inherited[not(. = '')],
                    for $addr in $node/addrLine
                    return
                        utillib:serializeNode($addr)
                }
                </publishingAuthority>
            ,
            (: new ... 2021-05-21 :)
            for $node in $transaction/property
            return
                utillib:serializeNode($node)
            ,
            (: new ... 2021-05-21 :)
            for $node in $transaction/copyright
            return
                utillib:serializeNode($node)
        }
        {
            if ($transaction/@type='group') then (
                for $t in $transaction/transaction
                return
                    scapi:transactionBasics($t, $projectPrefix, $projectLanguage, $oidnamemap, $templatemap)
            )
            else (
                if ($transaction[actors]) then
                    $transaction/actors
                else (
                    <actors/>
                ),
                if ($transaction[representingTemplate]) then
                    scapi:representingTemplateBasics($transaction/representingTemplate, $oidnamemap, $templatemap, false())
                else (
                    <representingTemplate/>
                )
            )
        }
        </transaction>
};

declare %private function scapi:representingTemplateBasics($representingTemplate as element(representingTemplate)?, $oidnamemap as map(*)?, $templatemap as map(*)?, $doContents as xs:boolean) as element(representingTemplate)? {
    let $dataset        := 
        if ($representingTemplate/@sourceDataset) then 
            utillib:getDataset($representingTemplate/@sourceDataset, $representingTemplate/@sourceDatasetFlexibility)
        else ()
    let $template       := 
        if ($representingTemplate[@ref]) then 
            map:get($templatemap, concat($representingTemplate/@ref, $representingTemplate/@flexibility[. castable as xs:dateTime]))
        else ()
    let $questionnaire  := 
        if ($representingTemplate/@representingQuestionnaire) then 
            utillib:getQuestionnaire($representingTemplate/@representingQuestionnaire, $representingTemplate/@representingQuestionnaireFlexibility)
        else ()
    return
        <representingTemplate>
        {
            $representingTemplate/@sourceDataset,
            $representingTemplate/@sourceDatasetFlexibility,
            $representingTemplate/@ref,
            $representingTemplate/@flexibility,
            $representingTemplate/@representingQuestionnaire,
            $representingTemplate/@representingQuestionnaireFlexibility,
            if ($template) then (
                attribute templateId {$template/(@id|@ref)},
                attribute templateDisplayName {($template/@displayName, $template/@name)[not(. = '')][1]},
                for $att in $template/(@effectiveDate, @statusCode, @versionLabel, @expirationDate, @officialReleaseDate, @ident, @url, @canonicalUri)
                return
                    (:E.g.: attribute templateEffectiveDate {$template/@effectiveDate} :)
                    attribute {'template' || upper-case(substring(local-name($att), 1, 1)) || substring(local-name($att), 2)} {$att}
            ) else ()
            ,
            for $att in $dataset/(@effectiveDate, @statusCode, @versionLabel, @expirationDate, @officialReleaseDate, @ident, @url, @canonicalUri)
            return
                (:E.g.: attribute sourceDatasetEffectiveDate {$dataset/@effectiveDate} :)
                attribute {'sourceDataset' || upper-case(substring(local-name($att), 1, 1)) || substring(local-name($att), 2)} {$att}
            ,
            for $att in $questionnaire/(@effectiveDate, @statusCode, @versionLabel, @expirationDate, @officialReleaseDate, @ident, @url, @canonicalUri)
            return
                (:E.g.: attribute questionnaireEffectiveDate {$questionnaire/@effectiveDate} :)
                attribute {'questionnaire' || upper-case(substring(local-name($att), 1, 1)) || substring(local-name($att), 2)} {$att}
            ,
            for $node in $dataset/name
            return
                <sourceDatasetName>{$node/@*, $node/node()}</sourceDatasetName>
            ,
            for $node in $questionnaire/name
            return
                <questionnaireName>{$node/@*, $node/node()}</questionnaireName>
        }
        {
            if ($doContents) then $representingTemplate/node() else ()
        }
        </representingTemplate>
};

(:~ Central logic for patching an existing scenario statusCode, versionLabel, expirationDate and/or officialReleaseDate

@param $authmap required. Map derived from token
@param $id required. DECOR concept/@id to update
@param $effectiveDate required. DECOR concept/@effectiveDate to update
@param $recurse optional as xs:boolean. Default: false. Allows recursion into child particles to apply the same updates
@param $listOnly optional as xs:boolean. Default: false. Allows to test what will happen before actually applying it
@param $newStatusCode optional as xs:string. Default: empty. If empty, does not update the statusCode
@param $newVersionLabel optional as xs:string. Default: empty. If empty, does not update the versionLabel
@param $newExpirationDate optional as xs:string. Default: empty. If empty, does not update the expirationDate 
@param $newOfficialReleaseDate optional as xs:string. Default: empty. If empty, does not update the officialReleaseDate 
@return list object with success and/or error elements or error
:)
declare function scapi:setScenarioStatus($authmap as map(*), $id as xs:string, $effectiveDate as xs:string?, $recurse as xs:boolean, $listOnly as xs:boolean, $newStatusCode as xs:string?, $newVersionLabel as xs:string?, $newExpirationDate as xs:string?, $newOfficialReleaseDate as xs:string?) as element(list) {
    (:get object for reference:)
    let $object                 := utillib:getScenario($id, $effectiveDate)
    let $decor                  := $object/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if (count($object) = 1) then () else
        if ($object) then
            error($errors:SERVER_ERROR, 'Scenario id ''' || $id || ''' effectiveDate ''' || $effectiveDate || ''' cannot be updated because multiple ' || count($object) || ' were found in: ' || string-join(distinct-values($object/ancestor::decor/project/@prefix), ' / ') || '. Please inform your database administrator.')
        else (
            error($errors:BAD_REQUEST, 'Scenario id ''' || $id || ''' effectiveDate ''' || $effectiveDate || ''' cannot be updated because it cannot be found.')
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-SCENARIOS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify scenarios in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    
    let $testUpdate             :=
        utillib:applyStatusProperties($authmap, $object, $object/@statusCode, $newStatusCode, $newVersionLabel, $newExpirationDate, $newOfficialReleaseDate, $recurse, true(), $projectPrefix)
    let $results                :=
        if ($testUpdate[self::error]) then $testUpdate else
        if ($listOnly) then $testUpdate else (
            utillib:applyStatusProperties($authmap, $object, $object/@statusCode, $newStatusCode, $newVersionLabel, $newExpirationDate, $newOfficialReleaseDate, $recurse, false(), $projectPrefix)
        )
    return
    if ($results[self::error] and not($listOnly)) then
        error($errors:BAD_REQUEST, string-join(
            for $e in $results[self::error]
            return
                $e/@itemname || ' id ''' || $e/@id || ''' effectiveDate ''' || $e/@effectiveDate || ''' cannot be updated: ' || data($e)
            , ' ')
        )
    else (
        <list artifact="STATUS" current="{count($results)}" total="{count($results)}" all="{count($results)}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            (: max 2 arrays: one with error and one with success :)
            utillib:addJsonArrayToElements($results)
        }
        </list>
    )
};

declare %private function scapi:prepareScenarioForUpdate($scenario as element(), $storedScenario as element()?, $baseIdTransaction as xs:string) as element() {
    let $editedScenario   := if ($scenario[edit]) then $scenario else $storedScenario
    return
    <scenario>
    {
        if ($storedScenario/@id) then 
            $storedScenario/@id 
        else (
            $scenario/@id
        ),
        if ($storedScenario/@effectiveDate[. castable as xs:dateTime]) then 
            $storedScenario/@effectiveDate 
        else 
        if ($scenario/@effectiveDate[. castable as xs:dateTime]) then
            $scenario/@effectiveDate
        else (
            attribute effectiveDate {substring(string(current-dateTime()),1,19)}
        ),
        if ($storedScenario/@statusCode) then 
            $storedScenario/@statusCode
        else 
        if ($scenario/@statusCode) then 
            $scenario/@statusCode
        else (
            attribute statusCode {'draft'}
        )
    }
    {
        $editedScenario/@versionLabel[not(.='')],
        $editedScenario/@expirationDate[not(.='')],
        $editedScenario/@officialReleaseDate[not(.='')],
        $editedScenario/@canonicalUri[not(.='')],
        $editedScenario/@lastModifiedDate[not(.='')]
    }
    {
        scapi:handleScenarioElements($editedScenario, ($storedScenario/ancestor::decor/project/@defaultLanguage, $storedScenario/name/@language, $scenario/name/@language, 'en-US')[1], false())
        ,
        for $transaction in $editedScenario/transaction
        return
            scapi:prepareTransactionForUpdate($transaction, $storedScenario, $baseIdTransaction)
    }
    </scenario>
};

declare function scapi:handleScenario($in as element(scenario)) as element(scenario) {
    <scenario>
    {
        $in/@id,
        $in/@effectiveDate,
        $in/@statusCode,
        $in/@versionLabel[not(. = '')],
        $in/@expirationDate[not(. = '')],
        $in/@officialReleaseDate[not(. = '')],
        $in/@canonicalUri[not(. = '')],
        $in/@lastModifiedDate[not(.='')],
        $in/name,
        $in/desc,
        $in/trigger,
        $in/condition,
        $in/publishingAuthority,
        $in/property,
        $in/copyright,
        for $tr in $in/transaction
        return
            scapi:handleTransaction($tr)
    }
    </scenario>
};

declare function scapi:handleTransaction($in as element(transaction)) as element(transaction) {
    <transaction>
    {
        $in/@id,
        $in/@effectiveDate,
        $in/@statusCode,
        $in/@versionLabel[not(. = '')],
        $in/@expirationDate[not(. = '')],
        $in/@officialReleaseDate[not(. = '')],
        $in/@type[not(. = '')],
        $in/@label[not(. = '')],
        $in/@model[not(. = '')],
        $in/@canonicalUri[not(. = '')],
        $in/@lastModifiedDate[not(.='')],
        $in/name,
        $in/desc,
        $in/trigger,
        $in/condition,
        $in/dependencies,
        $in/publishingAuthority,
        $in/property,
        $in/copyright,
        if ($in/transaction) then
            for $tr in $in/transaction
            return
                scapi:handleTransaction($tr)
        else (
            $in/actors,
            $in/representingTemplate
        )
    }
    </transaction>
};

declare %private function scapi:handleScenarioElements($data as element(), $defaultLanguage as xs:string, $clone as xs:boolean) as element()* {
    (: name :)
    if ($data/name[@language = $defaultLanguage]) then () else (
        <name language="{$defaultLanguage}">
        {
            data($data/name[1]),
            if ($clone) then '(2)' else()
        }
        </name>
    ),
    
    (:<name>Scenario</name>

na kloon:

<name>Scenario (2)</name> :)

    for $lang in distinct-values($data/name/@language)
    let $node     := $data/name[@language = $lang][1]
    let $node     := if ($node[*]) then $node else utillib:parseNode($node)
    return 
        element {name($node)} {$node/@*[not(.='')], data($node), if ($clone) then '(2)' else()}
    ,
    (: desc :)
    utillib:prepareFreeFormMarkupWithLanguageForUpdate($data/desc)
    ,
    (: trigger :)
    utillib:prepareTransactionTriggerForUpdate($data/trigger)
    ,
    (: condition :)
    utillib:prepareFreeFormMarkupWithLanguageForUpdate($data/condition)
    ,
    (: new ... 2021-05-21 :)
    utillib:preparePublishingAuthorityForUpdate($data/publishingAuthority)
    ,
    (: new ... 2021-05-21 :)
    utillib:prepareConceptPropertyForUpdate($data/property)
    ,
    (: new ... 2021-05-21 :)
    utillib:prepareFreeFormMarkupWithLanguageForUpdate($data/copyright)
};

declare %private function scapi:handleTransactionActors($in as element(actors)) as element(actors) {
    <actors>
    {
        for $actor in $in/actor[string-length(@id) gt 0] 
        return
            <actor>{$actor/@id, $actor/@role[not(. = '')]}</actor>
    }
    </actors>
};

(: =========================================================================================================================================== :)
(: ===============                                              TRANSACTION LOGIC                                              =============== :)
(: =========================================================================================================================================== :)

(:~ Retrieves latest DECOR transaction based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the transaction
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the transaction. If not given assumes latest version for id
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @param $projectLanguage         - optional parameter to select from a specific compiled language
    @param $treeonly                - optional boolean parameter to get the tree structure only if true
    @param $fullTree                - optional boolean parameter relevant if $treeonly = 'true' to include absent concepts
    @return as-is or as compiled as JSON
    @since 2020-05-03
:)
declare function scapi:getLatestTransaction($request as map(*)) {
    scapi:getTransaction($request)
};

(:~ Retrieves DECOR transaction based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version

@param $id                      - required parameter denoting the id of the transaction
@param $effectiveDate           - optional parameter denoting the effectiveDate of the transaction. If not given assumes latest version for id
@param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
@param $projectLanguage         - optional parameter to select from a specific compiled language
@param $treeonly                - optional boolean parameter to get the tree structure only if true
@param $fullTree                - optional boolean parameter relevant if $treeonly = 'true' to include absent concepts
@return as-is or as compiled as JSON
@since 2020-05-03
:)
declare function scapi:getTransaction($request as map(*)) {

    let $id                 := $request?parameters?id
    let $effectiveDate      := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $projectVersion     := $request?parameters?release
    let $projectLanguage    := $request?parameters?language
    let $treeOnly           := $request?parameters?treeonly = true()
    let $fullTree           := $request?parameters?fulltree = true()
    
    let $results            := scapi:getTransaction($id, $effectiveDate, $projectVersion, $projectLanguage, $treeOnly, $fullTree)

    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple transactions for id '", $id, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        )
        else (
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
        )

};

(:~ Retrieves DECOR transaction for publication based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version

@param $id                      - required parameter denoting the id of the transaction
@param $effectiveDate           - optional parameter denoting the effectiveDate of the transaction. If not given assumes latest version for id
@param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
@param $projectLanguage         - optional parameter to select from a specific compiled language
@param $format                  - optional. overrides the accept-header for frontend purposes
@param $download                - optional as xs:boolean. Default: false. 
@return as-is or as compiled as JSON
@since 2023-11-13
:)
declare function scapi:getTransactionExtract($request as map(*)) {

    let $projectVersion                 := $request?parameters?release[not(. = '')]
    let $projectLanguage                := $request?parameters?language[not(. = '')]
    let $communityName                  := $request?parameters?community[not(. = '')]
    let $format                         := $request?parameters?format[not(. = '')]
    let $download                       := $request?parameters?download=true()
    
    let $id                             := $request?parameters?id[not(. = '')]
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    
    (: default behaviour mime-types:
    - backend:   default mime-type is json - response payload is json - if accept type is omitted   
    - frontend:  default mime-type is xml - response payload is xml - if format is omitted :)

    let $acceptTypes                    := roaster:accepted-content-types()
    let $acceptedType                   := ($acceptTypes[. = ('application/xml', 'application/json')],'application/json')[1]
    
    let $format                         := if ($format) then $format else tokenize($acceptedType, '/')[2]

    (: 
       overwrite format in the backend is not always possible because of middleware behaviour
       - if in backend the format is xml and accept is not application/xml, roaster always gives a json payload 
    :) 
    let $check                          :=
        if ($format = 'xml' and not($acceptedType = 'application/xml')) then 
            error($errors:BAD_REQUEST, 'In case of format parameter is xml the accept header should be application/xml')
        else ()
 
    let $transaction                    := utillib:getTransaction($id, $effectiveDate, $projectVersion, $projectLanguage)
    
    let $results                        := if (empty($transaction)) then () else utillib:getDatasetExtract($transaction, $id, $effectiveDate, (), (), $projectVersion, $projectLanguage, $communityName)
 
    let $filename                       := 'DS_' || $results/@shortName || '_(download_' || substring(fn:string(current-dateTime()),1,19) || ').' || $format
    
    let $results                        :=
        for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/node(), $format)
                }
    
    (: in this case the json format overrides header application/xml and works with a text header :)
    let $results                        :=
        if ($format = 'json' and not($acceptedType = 'application/json')) then
            fn:serialize($results, map{"method": $format , "indent": true()})
            else $results     
    
    let $r-header                       := 
        (response:set-header('Content-Type', 'application/' || $format || '; charset=utf-8'),
        if ($download) then response:set-header('Content-Disposition', 'attachment; filename='|| $filename) else ())
    
    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple transactions for id '", $id, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        )
        else $results      

};

(:~ Retrieves DECOR transaction view based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the dataset
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the dataset. If not given assumes latest version for id
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @param $projectLanguage         - optional parameter to select from a specific compiled language
    @param $ui-language             - optional parameter to select from a specific language for the ui
    @param $hidecolumns             - optional parameter to hide columns in the view based on (hex) number
    @param $format                  - optional. if not given it is html
    @param $download                - optional as xs:boolean. Default: false. 
    @return as-is 
    @since 2024-09-24
:)

declare function scapi:getTransactionView($request as map(*)) {

    let $projectVersion                 := $request?parameters?release[not(. = '')]
    let $projectLanguage                := $request?parameters?language[not(. = '')]
    let $communityName                  := $request?parameters?community[not(. = '')]
    let $ui-lang                        := $request?parameters?ui-language[not(. = '')]
    let $hidecolumns                    := $request?parameters?hidecolumns[not(. = '')]
    let $format                         := $request?parameters?format[not(. = '')]
    let $download                       := $request?parameters?download=true()
    
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    
    let $format                         :=
        if (not($format = 'list')) then 'html' else 'list'
    
    let $transaction                    := utillib:getTransaction($id, $effectiveDate, $projectVersion, $projectLanguage)
    let $decor                          := $transaction/ancestor::decor[1]

    let $results                        := if (empty($transaction)) then () else utillib:getDatasetExtract($transaction, $id, $effectiveDate, (), (), $projectVersion, $projectLanguage, $communityName)

    let $filename                       := 'DS_' || $results/@shortName || '_(download_' || substring(fn:string(current-dateTime()),1,19) || ').html' 
        let $r-header                   := 
            (response:set-header('Content-Type', 'text/html; charset=utf-8'),
            if ($download) then response:set-header('Content-Disposition', 'attachment; filename='|| $filename) else ())
    
    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple transactions for id '", $id, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        )
        else 
        
        (: prepare for Html :)
        let $referenceUrl               := $decor/project/reference[@url castable as xs:anyURI]/@url
        let $projectPrefix              := $decor/project/@prefix
        
        (:get everything hanging off from the current template so we don't miss anything, but may have duplicates in the result:)
        let $templateChain              :=
            if ($format = ('list')) then () else (
                if ($results/@templateId) then (
                    let $transactionTemplate:= tmapi:getTemplateById(string($results/@templateId), string($results/@templateEffectiveDate), $decor, $decor/@version, $projectLanguage)[@id]
                    let $decors             := utillib:getBuildingBlockRepositories($decor, (), $utillib:strDecorServicesURL)
                    return 
                        $transactionTemplate | tmapi:getTemplateChain($transactionTemplate, $decors, map:merge(map:entry(concat($transactionTemplate/@id, $transactionTemplate/@effectiveDate), '')))
                )
                else ()
            )
            
       return 
            if ($format = 'list') 
                then utilhtml:convertTransactionOrDataset2SimpleHtml($results, $projectLanguage, $ui-lang, $hidecolumns, true(), $projectVersion, $referenceUrl, $projectPrefix, false(), $download) 
                else utilhtml:convertTransactionOrDataset2Html($results, $projectLanguage, $ui-lang, $hidecolumns, true(), true(), true(), $projectVersion, $referenceUrl, $projectPrefix, $templateChain, false(), $filename, $download)
};

(:~ Retrieves DECOR transaction as a diagram based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the dataset
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the dataset. If not given assumes latest version for id
    @param $conceptId               - optional parameter denoting the id of the concept in the dataset
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the concept in the dataset. If not given assumes latest version for id
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @param $projectLanguage         - optional parameter to select from a specific compiled language
    @param $interactive             - boolean
    @param $format                  - optional. overrides the accept-header for frontend purposes
    @return as-is 
    @since 2023-12-21
:)

declare function scapi:getTransactionDiagram($request as map(*)) {

    let $conceptId          := $request?parameters?conceptId[not(. = '')]
    let $conceptEffectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?conceptEffectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?conceptEffectiveDate[string-length() gt 0]
        }
    let $projectVersion                 := $request?parameters?release[not(. = '')]
    let $projectLanguage                := $request?parameters?language[not(. = '')]
    let $interactive                    := not($request?parameters?interactive = false())
    let $format                         := $request?parameters?format[not(. = '')]
    
    let $id                             := $request?parameters?id[not(. = '')]
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    
    let $check                          :=
        if (empty($conceptId) and not(empty($conceptEffectiveDate))) then 
            error($errors:BAD_REQUEST, 'Request SHALL NOT have conceptEffectiveDate without conceptId') 
        else ()
    
    let $dconcept                       :=
        if ($conceptId) then (
            let $dconcept               := utillib:getConcept($conceptId, $conceptEffectiveDate, (), $projectLanguage)
            let $check                  :=
                if ($dconcept) then () 
                else error($errors:BAD_REQUEST, 'Concept id ''' || $conceptId || ''' effectiveDate ''' || $conceptEffectiveDate || ''' requested but not found. ')
            return $dconcept
        )
        else ()
    
    let $format                         :=
        if (not($format = 'xml')) then 'svg' else 'xml'

    (: 
       overwrite format in the backend is not always possible because of middleware behaviour
       - if in backend the format is xml and accept is not application/xml, roaster always gives a json payload 
    :) 
    let $check                          :=
        if ($format = 'xml' and not((roaster:accepted-content-types()[. = ('image/svg+xml', 'application/xml')])[1] = 'application/xml')) then 
            error($errors:BAD_REQUEST, 'In case of format parameter is xml the accept header should be application/xml')
        else ()   

    let $transaction                    := utillib:getTransaction($id, $effectiveDate, $projectVersion, $projectLanguage)
                            
    let $results                        :=
    if (empty($transaction)) then () else (
        
        let $projectLanguage            := if (empty($projectLanguage)) then $transaction/ancestor::decor[1]/project/@defaultLanguage else $projectLanguage
                            
        let $fullDatasetTree            := 
            if (empty($projectVersion)) then utillib:getFullDatasetTree($transaction, $conceptId, $conceptEffectiveDate, $projectLanguage, (), false(), (), ())
            else if ($transaction[self::dataset]) then $transaction 
            else if ($projectLanguage = '*') then $setlib:colDecorVersion//transactionDatasets[@versionDate = $projectVersion]//dataset[@transactionId = $transaction/@id][@transactionEffectiveDate = $transaction/@effectiveDate]
            else $setlib:colDecorVersion//transactionDatasets[@versionDate = $projectVersion][@language = $projectLanguage]//dataset[@transactionId = $transaction/@id][@transactionEffectiveDate = $transaction/@effectiveDate]    
 
        let $concept                    := if (empty($conceptId)) then $fullDatasetTree else ($fullDatasetTree/concept[@id = $conceptId])
        
        return
            (: if xml is chosen just give the retrieved concept :)
            if ($format = 'xml') then
            for $c in $concept
                return
                    element {name($c)} {
                        $c/@*,
                        namespace {"json"} {"http://www.json.org"},
                        utillib:addJsonArrayToElements($c/*, $format)
                    }
            else
            (: if svg return the concept as an svg-diagram with interactive features :)
            if ($concept) then 
                let $baseUrl            := 
                if ($interactive) then
                        '?language=' || $projectLanguage || '&amp;filter=' || '&amp;interactive=' || $interactive || '&amp;format=' || $format || '&amp;transactionId=' || $id || '&amp;transactionEffectiveDate=' || encode-for-uri($transaction/@effectiveDate)
                else ()    
                
                return (
                    response:set-header('Content-Type','image/svg+xml'),
                    response:set-header('X-Robots-Tag', 'noindex'), 
                    utilsvg:convertConcept2Svg($concept, $projectLanguage, (), $dconcept, $baseUrl)
                )
            else ()
        )

    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else $results
};

(:~ Retrieves DECOR transaction history based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id
    @param $effectiveDate           - optional parameter denoting the effectiveDate. If not given assumes latest version for id
    @return list
    @since 2022-08-16
:)
declare function scapi:getTransactionHistory($request as map(*)) {
    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $projectPrefix                  := ()
    let $results                        := histlib:ListHistory($authmap, $decorlib:OBJECTTYPE-TRANSACTION, (), $id, $effectiveDate, 0)
    
    return
        <list artifact="{$decorlib:OBJECTTYPE-TRANSACTION}" current="{count($results)}" total="{count($results)}" all="{count($results)}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements($results)
        }
        </list>
};

(:~ Create transaction, new transaction with its own id and effectiveDate based on defaultBaseId for transactions

Situation 1: completely new transaction with its own id and effectiveDate
- Input MAY be "empty" then we create a single transaction of the requested type and if this is a group containing single transaction
- Input MAY be a transaction without id/effectiveDate but with name/desc and other direct properties. If there are no child transactions, we do the same as for empty input. 

Situation 2: new transaction version based on existing transaction with new id and effectiveDate
- Parameter sourceId and sourceEffectiveDate are required. Input SHALL be empty
- Return a copy of existing transaction with new id and effectiveDates on transaction and child transactions 

Note: if you want to update a transaction (group) from one dataset to the other, you can do that by adding 
      transaction.representingTemplate.targetDataset and optionally .targetDatasetFlexibility next to the original 
      transaction.representingTemplate.sourceDataset and .sourceDatasetFlexibility 

The transaction creation process then tries to find matches in the new dataset for the concepts listed from the old dataset. This logic is handed off to the transaction api  

@param $scenarioId              - required. scenario id to insert transaction into.
@param $scenarioEffectiveDate   - required. scenario effectiveDate to insert transaction into.
@param $transactionBaseId       - optional. baseId to create the new transaction id out of. Defaults to defaultBaseId for type TR if omitted
@param $transactionType         - optional if request-body is provided. 'group' or 'stationary' or 'initial' or 'back'
@param $insertMode              - required. 'into' or 'preceding' or 'following'. For preceding and for following, $insertRef is required
@param $insertRef               - optional. transaction/@id reference for insert. Inserts as new transaction in scenario if empty
@param $insertFlexibility       - optional. transaction/@effectiveDate reference for insert. Only relevant when two versions of the same transaction are in the same scenario which is logically highly unlikely
@param $sourceId                - optional. the transaction id to create the new transaction from 
@param $sourceEffectiveDate     - optional. the transaction effectiveDate to create the transaction from 
@param $request-body optional. body containing new transaction structure
@return created transaction structure
@since 2020-05-03
:)
declare function scapi:postTransaction($request as map(*)) {

    let $authmap                        := $request?user
    let $scenarioId                     := $request?parameters?id[not(. = '')]
    let $scenarioEffectiveDate          := $request?parameters?effectiveDate[not(. = '')]
    let $transactionBaseId              := $request?parameters?baseId[not(. = '')]
    let $transactionType                := $request?parameters?transactionType[not(. = '')]
    let $insertMode                     := $request?parameters?insertMode[not(. = '')]
    let $insertRef                      := $request?parameters?insertRef[not(. = '')]
    let $insertFlexibility              := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?insertFlexibility)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?insertFlexibility[string-length() gt 0]
        }
    let $sourceId                       := $request?parameters?sourceId[not(. = '')]
    let $sourceEffectiveDate            := $request?parameters?sourceEffectiveDate[not(. = '')]
    let $data                           := utillib:getBodyAsXml($request?body, 'transaction', ())
    let $treeOnly                       := $request?parameters?treeonly = true()
    let $fullTree                       := $request?parameters?fulltree = true()
    
    (:let $s                      := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data):) 
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else
        if (empty($scenarioId) or empty($scenarioEffectiveDate) or empty($insertMode)) then 
            error($errors:BAD_REQUEST, 'Parameter id, effectiveDate and insertMode SHALL have a value')
        else
        if (empty($transactionType) and empty($data/@type)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have either parameter transactionType OR data with a type')
        else ()
    
    let $return                 := scapi:postTransaction($authmap, $scenarioId, $scenarioEffectiveDate, $transactionBaseId, $transactionType, $insertMode, $insertRef, $insertFlexibility, $sourceId, $sourceEffectiveDate, $data)
    let $result                 := scapi:getTransaction($return/@id, $return/@effectiveDate, (), (), $treeOnly, $fullTree)
    
    return
        if (empty($result)) then
            error($errors:SERVER_ERROR, 'A transaction was supposed to be created with id ' || $return/@id || ' and effectiveDate ' || $return/@effectiveDate || ' but upon requesting for it, it turns out not to be found.')
        else (
            roaster:response(201, 
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
            )
        )

};

(:~ Update DECOR transaction parts. Does not touch any child transactions or representingTemplate contents. Expect array of parameter objects, each containing RFC 6902 compliant contents. Note: RestXQ does not do PATCH (yet), but that would be the preferred option.

{ "op": "[add|remove|replace]", "path": "/...", "value": "[string|object]" }

where
* op - add or remove or replace
* path - e.g. /type, /model, /label, /canonicalUri, /name, /desc
* value - string when the path is not /. object when path is /

@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $id                       - required. the id for the issue to update 
@param $request-body             - required. json body containing array of parameter objects each containing RFC 6902 compliant contents
@return dataset structure including generated meta data
@since 2020-05-03
@see http://tools.ietf.org/html/rfc6902
:)
declare function scapi:patchTransaction($request as map(*)) {

    let $authmap                        := $request?user
    let $trid                           := $request?parameters?id
    let $tred                           := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $projectPrefix                  := ()
    let $projectVersion                 := ()
    let $projectLanguage                := ()
    let $treeOnly                       := $request?parameters?treeonly = true()
    let $fullTree                       := $request?parameters?fulltree = true()
    let $data                           := utillib:getBodyAsXml($request?body, 'parameters', ())
    
    (:let $s                      := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data) :)
    
    let $check                  :=
        if ($data) then () else (
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        )
    
    let $return                 := scapi:patchTransaction($authmap, string($trid), $tred, $data)
    let $results                := scapi:getTransaction($trid, $tred, $projectVersion, $projectLanguage, $treeOnly, $fullTree)
    return 
        if (empty($results)) then (
            error($errors:SERVER_ERROR, 'A transaction was supposed to be patched with id ' || $trid || ' and effectiveDate ' || $tred || ' but upon requesting for it, it turns out not to be found.')
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, 'Found multiple transactions for id ''' || $trid || ''' effectiveDate ''' || $tred || '''. Expected 0..1. Alert your administrator as this should not be possible.')
        )
        else (
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
        )

};

(:~ Delete DECOR transaction
@return nothing
@since 2021-02-19
:)
declare function scapi:deleteTransaction($request as map(*)) {
    
    let $authmap                := $request?user
    let $id                     := $request?parameters?id
    let $effectiveDate          := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
        
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($id)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have parameter id')
        else ()
    
    let $return                 := scapi:deleteTransaction($authmap, $id, $effectiveDate)
    return (
        roaster:response(204, $return)
    )
};

(:~ Retrieves DECOR transaction based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) with operator $questionnaire

@param $id                      - required parameter denoting the id of the transaction
@param $effectiveDate           - optional parameter denoting the effectiveDate of the transaction. If not given assumes latest version for id
@return as-is or as compiled as JSON
@since 2020-05-08
:)
declare function scapi:postLatestTransactionQuestionnaire($request as map(*)) {
    scapi:postTransactionQuestionnaire($request)
};

(:~ Retrieves DECOR transaction based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) with operator $questionnaire
    @param $id                      - required parameter denoting the id of the transaction
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the transaction. If not given assumes latest version for id
    @return as-is or as compiled as JSON
    @since 2020-05-08
:)
declare function scapi:postTransactionQuestionnaire($request as map(*)) {
    
    let $authmap                    := $request?user
    let $data                       := utillib:getBodyAsXml($request?body, 'transactions', ())             
    let $project                    := $request?parameters?project[string-length() gt 0]
    let $baseId                     := $request?parameters?baseId[string-length() gt 0]
    
    let $check                      :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                      :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    let $check                      :=
        if (empty($data)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else 
        if (empty($data/transaction)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have one or more transactions')
        else 
        if (count($data/transaction[@id][@effectiveDate]) != count($data/transaction)) then
            error($errors:BAD_REQUEST, 'All transactions SHALL have an id or effectiveDate')       
        else ()
    
    let $decor                      := 
        if (utillib:isOid($project)) then utillib:getDecorById($project) else (
            utillib:getDecorByPrefix($project)
        )
    (: The user may request a specific baseId to be used for the Questionnaires. This baseId SHALL be declared as a baseId in the project with type QQ in that case. 
        If the user did not request a specific baseId, then the defaultBaseId for QQ should be assumed. note that QQ as type may not be present in a given project :)
    let $baseId                     := 
        if (empty($baseId)) then 
            decorlib:getDefaultBaseIdsP($decor, $decorlib:OBJECTTYPE-QUESTIONNAIRE)/@id 
        else (
            $baseId[. = decorlib:getBaseIdsP($decor, $decorlib:OBJECTTYPE-QUESTIONNAIRE)/@id]
        )
    let $check                      :=
        if (empty($decor)) then
            error($errors:BAD_REQUEST, 'Cannot find DECOR project ' || $project || ' to write the Questionnaire(s) into.')
        else ()
    let $check                      :=
        if (empty($baseId)) then 
            error($errors:SERVER_ERROR, 'Project ''' || $project || ''' is missing a default base id for type Questionnaire (' || $decorlib:OBJECTTYPE-QUESTIONNAIRE || '). Please work with a decor-admin for your project to add it and QuestionnaireResponse (' || $decorlib:OBJECTTYPE-QUESTIONNAIRERESPONSE ||') first.')
        else ()
    let $check                      :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-SCENARIOS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify scenarios/transactions in project ', $project, '. You have to be an active author in the project.'))
        )
        
    let $projectPrefix              := $decor/project/@prefix
    let $projectLanguage            := $decor/project/@defaultLanguage
    let $now                        := substring(xs:string(current-dateTime()), 1, 19)
    let $request-files              :=
        for $transaction in $data/transaction
        let $uuid                   := util:uuid($transaction)
        let $transformation-request := 
            <questionnaire-transform-request uuid="{$uuid}" for="{$projectPrefix}" on="{current-dateTime()}" by="{$authmap?name}" baseId="{$baseId}" progress="Added to process queue ..." progress-percentage="0">
            {$transaction}
            </questionnaire-transform-request>
        let $write                  := xmldb:store($setlib:strDecorScheduledTasks, $projectPrefix || replace($now, '\D', '') || '-' || $uuid || '.xml', $transformation-request)
        let $tt                     := sm:chmod($write, 'rw-rw----')   
        return
            $transformation-request
    
    return 
        roaster:response(200,
            <list artifact="SCHEDULEDTASKS" current="{count($request-files)}" total="{count($request-files)}" all="{count($request-files)}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
            {
                utillib:addJsonArrayToElements($request-files)
            }
            </list>
        )

};

(:~ Accepts a threadId (for logging purposes) and a questionnaire-transform-request element, which shall be dirently form the request file, containing all things required for doing transaction to questionnaire conversion. 
It processes all requested transactions into questionnaires and then commits them all to the project, or none if something fails.
:)
declare function scapi:process-questionnairetransform-request($threadId as xs:string, $request as element(questionnaire-transform-request)) {
    let $logsignature           := 'scapi:process-questionnairetransform-request'
    let $mark-busy              := update insert attribute busy {'true'} into $request
    let $progress               := 
        if ($request/@progress) then
            update value $request/@progress with 'Checking request parameters ...' 
        else (
            update insert attribute progress {'Checking request parameters ...'} into $request
        )
    let $transactionCount       := count($request/transaction)
    let $progress               := 5
    let $progress               := 
        if ($request/@progress-percentage) then
            update value $request/@progress-percentage with $progress 
        else (
            update insert attribute progress-percentage {$progress} into $request
        )
        
    (: =================== PREPARE VARIABLES ==================== :)
        
    let $timeStamp              := $request/@on
    let $projectPrefix          := $request/@for[not(. = '*')]
    let $baseId                 := $request/@baseId[not(. = '')]
    let $transactions           := $request/transaction
 
    (: =================== PREPARE VARIABLES ==================== :)
 
    let $newEd                  := substring(string(current-dateTime()),        1, 19)
    let $decor                  := 
        if (utillib:isOid($projectPrefix)) then utillib:getDecorById($projectPrefix) else (
            utillib:getDecorByPrefix($projectPrefix)
        )
 
    let $questionnaires :=
        for $transaction at $i in $transactions
        let $transactionId              := $transaction/@id
        let $transactionEffectiveDate   := $transaction/@effectiveDate
        let $progress                   := update value $request/@progress with 'Retrieving transaction ' || $i || ' of ' || $transactionCount || ' id ' || $transactionId || ' effectiveDate ' || $transactionEffectiveDate || ' ...'
        let $storedTransaction          := utillib:getTransaction($transactionId, $transactionEffectiveDate)
        let $check                      :=
            if (empty($storedTransaction)) then 
                error($errors:SERVER_ERROR, 'Transaction not found with id=' || $transactionId || ' effectiveDate=' || $transactionEffectiveDate)
            else ()
            (:if ($storedTransaction/representingTemplate[@sourceDataset]) then () else ( 
                error($errors:SERVER_ERROR, 'Transaction (type=' || $storedTransaction/@type || ')  with id=' || $transactionId || ' effectiveDate=' || $transactionEffectiveDate || ' does not have any connected dataset to create a questionnaire out of' )
            ):)
        let $progress                   := update value $request/@progress with 'Expanding dataset tree for transaction ' || $i || ' of ' || $transactionCount || ' id ' || $transactionId || ' effectiveDate ' || $transactionEffectiveDate || ' ...'
        
        (: getFullDatasetTree shall return name, desc etc in ALL languages (*) KH 20240531 :)
        let $fullDatasetTree            := utillib:getFullDatasetTree($storedTransaction, (), (), '*', (), false(), ())
        
        let $progress                   := update value $request/@progress-percentage with (5 + round((95 div ($transactionCount + 1)) * $i)) 
        let $progress                   := update value $request/@progress with 'Transforming transaction ' || $i || ' of ' || $transactionCount || ' id ' || $transactionId || ' effectiveDate ' || $transactionEffectiveDate || ' ...'
        let $newId                      := decorlib:getNextAvailableIdP($decor, $decorlib:OBJECTTYPE-QUESTIONNAIRE, $baseId)/@max    + $i
        let $qqid                       := $baseId || '.' || $newId
        let $canonicalUri               := serverapi:getServerURLFhirCanonicalBase() || 'Questionnaire/'|| $qqid || '--' || replace($newEd, '\D','')
        return (
            comment { ' ' || replace($storedTransaction/name[1], '--', '-') || ' ' },
            <questionnaireAssociation questionnaireId="{$qqid}" questionnaireEffectiveDate="{$newEd}">
            {
                for $c in $fullDatasetTree//concept[not(ancestor::conceptList | ancestor-or-self::concept[@maximumMultiplicity[. = '0'] | @conformance[. = 'NP']])]
                return
                    (: NB keep value of @elementId in sync with scapi:process-transaction-concept($concept) for @linkId :)
                    <concept ref="{$c/@id}" effectiveDate="{$c/@effectiveDate}" elementId="{$c/@id}"/>
            }
            </questionnaireAssociation>
            ,
            <questionnaire id="{$qqid}" effectiveDate="{$newEd}" statusCode="draft" lastModifiedDate="{$newEd}" canonicalUri="{$canonicalUri}">
            {
                (: we cannot copy the canonicalUri from the transaction. we 'could' generate it here as appropriate for a Questionnaire :)
                $storedTransaction/name,
                $storedTransaction/desc,
                (:classification:)
                <relationship type="DRIV" ref="{$transactionId}" flexibility="{$transactionEffectiveDate}"/>,
                (:subjectType:)
                $storedTransaction/publishingAuthority,
                (:code:)
                (:purpose:)
                $storedTransaction/copyright,
                (:jurisdiction:)
                (:contained:)
                scapi:process-transaction-concept($fullDatasetTree/concept[not(@conformance='NP')])
            }
            </questionnaire>
        )
    
    let $updateQuestionnaire    := 
        if ($decor/rules) then
            update insert $questionnaires into $decor/rules
        else
        if ($decor/issues) then
            update insert <rules>{$questionnaires}</rules> preceding $decor/issues
        else (
            update insert <rules>{$questionnaires}</rules> into $decor
        )

    let $progress                       := update value $request/@progress-percentage with 100 
    let $progress                       := update value $request/@progress with 'Transforming ' || $transactionCount ||' transaction(s) done ...'
    
    let $remove                         := xmldb:remove(util:collection-name($request), util:document-name($request))

    return ()
};
declare function scapi:process-transaction-concept($concepts as element(concept)*) as element(item)* {
    for $concept in $concepts
    let $defaultValue  := ($concept/valueDomain/property/@default)[1]
    let $operhasqm     := contains(string-join($concept/operationalization, ' '), '?')
    return
        <item>
        {
            (: NB keep value of @linkId in sync with scapi:process-questionnairetransform-request#2 for @elementId :)
            attribute linkId {$concept/@id},
            (:attribute definition:)
            (:attribute prefix:)
            attribute type {scapi:decor2questionnaireType($concept)},
            attribute required {if ($concept[@conformance = 'NP'] | $concept[@maximumMultiplicity = '0']) then false() else exists($concept/@minimumMultiplicity[not(. = '0')])},
            attribute repeats { if ($concept[@conformance = 'NP'] | $concept[@maximumMultiplicity = ('0', '1')]) then false() else true()},
            attribute readOnly {exists($concept/valueDomain/property[@fixed = 'true'])},
            if (count($concept/valueDomain/property) = 1) then (
                $concept/valueDomain/property/@minLength[. castable as xs:integer],
                $concept/valueDomain/property/@maxLength[. castable as xs:integer],
                if ($concept/valueDomain/property/@minInclude) then
                    attribute minValue {$concept/valueDomain/property/@minInclude}
                else (),
                if ($concept/valueDomain/property/@maxInclude) then
                    attribute maxValue {$concept/valueDomain/property/@maxInclude}
                else ()
            )
            else (),
            if ($concept/valueDomain/property/@fractionDigits) then
                attribute maxDecimalPlaces { 
                    max(
                        for $t in $concept/valueDomain/property/@fractionDigits
                        let $md   := replace($t, '[^\d]', '')[not(. = '')]
                        return
                            if ($md castable as xs:integer) then xs:integer($md) else ()
                    )
                }
            else (),
            $concept/@enableBehavior,
            attribute hiddenItem {false()}
        }
        {
            (:
                text
                to get Questionnaire item text populated: if there is an operationalization in the concept
                then this operationalization text becomes the Questionnaire item text
                otherwise the Questionnaire item text is the name of the concept
                
                see https://art-decor.atlassian.net/browse/AD30-1379 and https://art-decor.atlassian.net/browse/AD30-1642
                
                so modifed 2024-07-30
                use operationalization in data elements / scenarios for Questionnaire item text only, if the text contains a question mark
                if operationalization does not contain a question mark simply put it down as help text candidate
            :)
            let $itemText   := if ($operhasqm) then $concept/operationalization else $concept/name
            
            for $text in $itemText
            return
                <text>
                {
                    $text/@*,
                    $text/node()
                }
                </text>
            ,
            (: 
                if operationalization contains a question mark keep the original concept name(s) as synonym, 
                otherwise keep the concept operationalization
                see https://art-decor.atlassian.net/browse/AD30-1642
            :)
            if ($operhasqm) then
                for $cn in $concept/name
                return
                    <synonym>
                    {
                        $cn/@*,
                        $cn/node()
                    }
                    </synonym>
                else $concept/operationalization
            ,
            (:code:)
            for $terminologyAssociation in $concept/terminologyAssociation[@conceptId = ($concept/@id | $concept/inherit/@ref)][@code]
            return
                <code>{$terminologyAssociation/(@code, @codeSystem, @displayName, @canonicalUri)}</code>
            ,
            (:designNote:)
            (:itemControl:)
            (:unitOption:)
            (:enableWhen:)
            for $enableWhen in $concept/enableWhen
            return
                <enableWhen>
                {
                    for $attr in $enableWhen/@*[not(. = '')]
                    return
                        switch (local-name($attr))
                        case 'question' return (
                            if (utillib:isOid($attr)) then
                                $attr
                            else
                            if (utillib:isOid(substring-before($attr, '--'))) then
                                attribute question {substring-before($attr, '--')}
                            else (
                                (: we really tried but don't know what this links to :)
                                $attr
                            )
                        )
                        default return $attr
                }
                {
                    $enableWhen/node()
                }
                </enableWhen>
        }
        {
            (: http://hl7.org/fhir/questionnaire.html: que-4: A question cannot have both answerOption and answerValueSet:)
            (:answerValueSet:)
            for $vs in $concept/valueSet
                let $flexibility    :=
                    if ($vs/terminologyAssociation/@flexibility = 'dynamic') then ('dynamic') else $vs/@effectiveDate 
                let $canonicalUri   := 
                     if ($vs[@canonicalUri]) then $vs/@canonicalUri 
                            else if ($flexibility = 'dynamic') then 
                            concat(serverapi:getServerURLFhirCanonicalBase(),'ValueSet/', $vs/@id)
                            else 
                            concat(serverapi:getServerURLFhirCanonicalBase(),'ValueSet/', $vs/@id, '--', replace($flexibility,'[^\d]',''))
                
                return    
                <answerValueSet>
                {
                    attribute ref { $vs/@id },
                    attribute flexibility { $flexibility },
                    attribute canonicalUri { $canonicalUri },
                    $vs/@name,
                    $vs/@displayName,
                    $vs/@statusCode,
                    $vs/@strength
                }    
                </answerValueSet>
                
         }
         {
            (:answerOptions:)
            (:uncoded answerOptions:)
            for $c in $concept[empty(valueSet)]/valueDomain[@type = ('code', 'ordinal')]/conceptList/(concept | exception)
            let $tas    := $c/ancestor::concept[1]/terminologyAssociation[@conceptId = $c/@id]
            return
                <answerOption>
                {
                    $c/@ordinal[not(. = '')],
                    if ($tas) then
                        for $ta in $tas
                        return
                            <valueCoding>
                            {
                                $ta/@code[not(. = '')],
                                $ta/@codeSystem[not(. = '')],
                                $ta/@codeSystemName[not(. = '')],
                                $ta/@displayName[not(. = '')],
                                $ta/@canonicalUri[not(. = '')]
                            }
                            </valueCoding>
                    else (
                        for $cn in $c/name
                        return
                            <valueString>
                            {
                                $cn/@language,
                                attribute value { $cn/text() }
                            }
                            </valueString>
                    )
                }
                </answerOption>
        }
        {
            if ($defaultValue) then 
                (: For code, Questionnaire expect Coding, DECOR default is just a simple string :)
                if ($concept/*:valueDomain/@type != 'code') then
                    <initial>
                    {
                        element {scapi:decor2questionnaireAnswerType($concept)} {
                            attribute value {$defaultValue}
                        }
                    }
                    </initial>
                else ()
            else ()
         }
         {
            (:terminologyServer:)
            
            (: the terminologyServer is runtime derived from the FHIR server, if it's needed in the questionnaire it has to be explicit mentioned :)
            
            (:let $endpoint := utillib:getServerURLFhirServices()
            (\: trim last '/' :\)
            let $length   := string-length($endpoint)

            let $endpoint :=
                if (substring($endpoint, $length, $length) = "/") then 
                substring($endpoint, 1, ($length)-1)
                else ($endpoint)
            return
            <terminologyServer>
            {
                attribute ref {$endpoint} 
            }
            </terminologyServer>:)
            (:itemExtractionContext:)
        }
        {
            scapi:process-transaction-concept($concept/concept[not(@conformance='NP')])
        }
        </item>
};

declare %private function scapi:decor2questionnaireType($concept as element(concept)) as xs:string {
    let $valueDomainType    := $concept/valueDomain/@type
    let $conceptItemType    := $concept/property[@name = 'Quest::ItemType']
    let $conceptItem        := $conceptItemType/normalize-space(string-join(.//text()))
    return
    (: allow override of all other options by explicitly setting the questionnaire item type value.
        https://hl7.org/fhir/R4/valueset-item-type.html
    :)
    if ($conceptItemType) then 
        switch (lower-case($conceptItem))
        case 'group'
        case 'display'
        case 'question'
        case 'boolean'
        case 'decimal'
        case 'integer'
        case 'date'
        case 'time'
        case 'string'
        case 'text'
        case 'url'
        case 'choice'
        case 'open-choice'
        case 'attachment'
        case 'reference'
        case 'quantity' return $conceptItem
        case 'datetime' return 'dateTime'
        default return error(xs:QName('quest:IllegalItemType'), concat('Concept id=', $concept/@id, ' effectiveDate=', $concept/@effectiveDate, ' ', $concept/name[1], ' defines unknown property Quest::ItemType value: ''', $conceptItem, ''''))
    else
    (: Input param is a concept, since we need to look at group :)
    if ($concept/@type = 'group') then 'group' else
    if ($valueDomainType = 'boolean') then $valueDomainType else
    if ($valueDomainType = 'date') then $valueDomainType else
    if ($valueDomainType = 'decimal') then $valueDomainType else
    if ($valueDomainType = 'quantity') then $valueDomainType else
    if ($valueDomainType = 'text') then $valueDomainType else
    if ($valueDomainType = 'time') then $valueDomainType else
    if ($valueDomainType = 'count') then 'integer' else
    if ($valueDomainType = 'datetime') then 'dateTime' else
    if ($valueDomainType = 'duration') then 'quantity' else
    if ($valueDomainType = 'currency') then 'quantity' else
    if ($valueDomainType = 'identifier') then 'string' else
    if ($valueDomainType = 'ordinal') then 'choice' else
    if ($valueDomainType = 'blob') then 'attachment' else
    if ($valueDomainType = 'code') then (
        if ($concept/*:valueSet) then
            if ($concept/*:terminologyAssociation[@strength[not(. = 'required')]]) then
                'open-choice'
            else
            if ($concept/*:valueSet[*:completeCodeSystem | *:conceptList/*:include | *:conceptList/*:exclude]) then
                'open-choice'
            else
            if ($concept/*:valueSet/*:conceptList/*[@code = 'OTH'][@codeSystem = '2.16.840.1.113883.65.1008']) then
                'open-choice'
            else (
                'choice'
            )
        else (
            'string'
        )
    )
    else (
        'string'
    )
};
declare %private function scapi:decor2questionnaireAnswerType($concept as element(concept)) as xs:string {
    (: Input param is a concept, since we need to look at group :)
    switch ($concept/valueDomain/@type)
    case 'count' return 'valueInteger'
    case 'date' return 'valueDate'
    case 'time'  return 'valueTime'
    case 'code' return 'valueCoding'
    case 'blob' return 'valueReference'
    default return 'valueString'
};

(:~ Update representingTemplate

@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $id                       - required. the id for the transaction to update 
@param $effectiveDate            - required. the effectiveDate for the transaction to update 
@param $request-body             - required. body containing new representingTemplate structure
@return concept structure
@since 2020-05-03
:)
declare function scapi:putRepresentingTemplate($request as map(*)) {

    let $authmap                := $request?user
    let $trid                   := $request?parameters?id
    let $tred                   := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $data                   := utillib:getBodyAsXml($request?body, 'dataset', ())
    
    let $cloneFromTrxId         := $request?parameters?cloneFromTransactionId[string-length() gt 0]
    let $cloneFromTrxEd         := $request?parameters?cloneFromTransactionEffectiveDate[string-length() gt 0]
    let $cloneFromCptId         := $request?parameters?cloneFromConceptId[string-length() gt 0]
    let $cloneFromCptEd         := $request?parameters?cloneFromConceptEffectiveDate[string-length() gt 0]
    let $treeOnly               := true()
    let $fullTree               := $request?parameters?fulltree = true()
    let $deletelock             := $request?parameters?deletelock = true()
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($cloneFromTrxId) and empty($data)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have either data or parameter cloneFromTransactionId that points to an existing transaction')
        else
        if (not(empty($data)) and not(empty($cloneFromTrxId))) then
            error($errors:BAD_REQUEST, 'Request SHALL NOT have both data and parameter cloneFromTransactionId')
        else ()
    
    (:let $s                      := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data):) 
    
    let $return                 := scapi:putRepresentingTemplate($authmap, string($trid), $tred, $data, $cloneFromTrxId, $cloneFromTrxEd, $cloneFromCptId, $cloneFromCptEd, $deletelock)
    let $result                 := scapi:getTransaction($trid, $tred, (), (), $treeOnly, $fullTree)
    
    return
        if (empty($result)) then () else (
            element {name($result)} {
                $result/@*,
                namespace {"json"} {"http://www.json.org"},
                utillib:addJsonArrayToElements($result/*)
            }
        )

};

(:~ Central logic for patching an existing transaction statusCode, versionLabel, expirationDate and/or officialReleaseDate

@param $authmap required. Map derived from token
@param $id required. DECOR concept/@id to update
@param $effectiveDate required. DECOR concept/@effectiveDate to update
@param $recurse optional as xs:boolean. Default: false. Allows recursion into child particles to apply the same updates
@param $listOnly optional as xs:boolean. Default: false. Allows to test what will happen before actually applying it
@param $newStatusCode optional as xs:string. Default: empty. If empty, does not update the statusCode
@param $newVersionLabel optional as xs:string. Default: empty. If empty, does not update the versionLabel
@param $newExpirationDate optional as xs:string. Default: empty. If empty, does not update the expirationDate 
@param $newOfficialReleaseDate optional as xs:string. Default: empty. If empty, does not update the officialReleaseDate 
@return list object with success and/or error elements or error
:)
declare function scapi:setTransactionStatus($authmap as map(*), $id as xs:string, $effectiveDate as xs:string?, $recurse as xs:boolean, $listOnly as xs:boolean, $newStatusCode as xs:string?, $newVersionLabel as xs:string?, $newExpirationDate as xs:string?, $newOfficialReleaseDate as xs:string?) as element(list) {
    (:get object for reference:)
    let $object                 := utillib:getTransaction($id, $effectiveDate)
    let $decor                  := $object/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if (count($object) = 1) then () else
        if ($object) then
            error($errors:SERVER_ERROR, 'Transaction id ''' || $id || ''' effectiveDate ''' || $effectiveDate || ''' cannot be updated because multiple ' || count($object) || ' were found in: ' || string-join(distinct-values($object/ancestor::decor/project/@prefix), ' / ') || '. Please inform your database administrator.')
        else (
            error($errors:BAD_REQUEST, 'Transaction id ''' || $id || ''' effectiveDate ''' || $effectiveDate || ''' cannot be updated because it cannot be found.')
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-SCENARIOS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify scenarios/transactions in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    (: https://art-decor.atlassian.net/browse/AD30-259 :)
    let $check                  :=
        if ($object[@statusCode = 'cancelled'] and $object[not(@statusCode = $newStatusCode)]) then
            if ($object/ancestor::scenario[@statusCode = 'draft']) then () else (
                error($errors:BAD_REQUEST, 'Status transition not allowed from ' || $object/@statusCode || ' to ''' || $newStatusCode || ''', because the scenario is not in status draft but in status ''' || $object/ancestor::scenario/@statusCode || '''')
            )
        else ()
    
    let $testUpdate             :=
        utillib:applyStatusProperties($authmap, $object, $object/@statusCode, $newStatusCode, $newVersionLabel, $newExpirationDate, $newOfficialReleaseDate, $recurse, true(), $projectPrefix)
    let $results                :=
        if ($testUpdate[self::error]) then $testUpdate else
        if ($listOnly) then $testUpdate else (
            utillib:applyStatusProperties($authmap, $object, $object/@statusCode, $newStatusCode, $newVersionLabel, $newExpirationDate, $newOfficialReleaseDate, $recurse, false(), $projectPrefix)
        )
    return
    if ($results[self::error] and not($listOnly)) then
        error($errors:BAD_REQUEST, string-join(
            for $e in $results[self::error]
            return
                $e/@itemname || ' id ''' || $e/@id || ''' effectiveDate ''' || $e/@effectiveDate || ''' cannot be updated: ' || data($e)
            , ' ')
        )
    else (
        <list artifact="STATUS" current="{count($results)}" total="{count($results)}" all="{count($results)}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            (: max 2 arrays: one with error and one with success :)
            utillib:addJsonArrayToElements($results)
        }
        </list>
    )
};

(:~ Retrieve transaction

@param $id                      - required parameter denoting the id of the transaction
@param $effectiveDate           - optional parameter denoting the effectiveDate of the transaction. If not given assumes latest version for id
@param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
@param $projectLanguage         - optional parameter to select from a specific compiled language
@param $treeonly                - optional boolean parameter to get the tree structure only if true
@param $fullTree                - optional boolean parameter relevant if $treeonly = 'true' to include absent concepts
@return as-is or as compiled as JSON
@since 2020-05-03
:)
declare function scapi:getTransaction($id as xs:string, $effectiveDate as xs:string?, $projectVersion as xs:string*, $projectLanguage as xs:string?, $treeonly as xs:boolean?, $fullTree as xs:boolean?) {
    let $id                         := $id[not(. = '')]
    let $effectiveDate              := $effectiveDate[not(. = '')]
    let $projectVersion             := $projectVersion[not(. = '')]
    let $projectLanguage            := $projectLanguage[not(. = '')]
    let $treeonly                   := $treeonly = true()
    let $fullTree                   := $fullTree = true()
    
    let $result                     := utillib:getTransaction($id, $effectiveDate, $projectVersion[1], $projectLanguage[1])
    
    (: build map of oid names. is more costly when you do that deep down :)
    let $oidnamemap                 := map {}(:scapi:buildOidNameMap($result, $projectLanguage[1]):)
    (: build map of template attributes. is more costly when you do that deep down :)
    let $templatemap                := if ($result) then scapi:buildTemplateMap($result, $result/ancestor::decor) else ()
    
    return
    if ($result) then 
        let $representingTemplate   := scapi:representingTemplateBasics($result/representingTemplate, $oidnamemap, $templatemap, false())
        let $publishingAuthority    := 
            if ($result[@id]) then 
                    if ($result[publishingAuthority]) then $result/publishingAuthority else utillib:inheritPublishingAuthority($result)
                else ()
        let $copyright              :=
            if ($result[@id]) then 
                    if ($result[copyright]) then $result/copyright else utillib:inheritCopyright($result)
                else ()

        return
        if ($treeonly) then 
            for $tr in utillib:getDatasetTree((), (), $id, $effectiveDate, $fullTree)
            return
                element {name($tr)} {
                    $tr/@*,
                    (: copy all template attributes that we do not already have in the $tr/@* set :)
                    for $att in $representingTemplate/@*[starts-with(name(), 'template')]
                    return
                        if ($tr/@*[name() = name($att)]) then () else $att
                    ,
                    $tr/name,
                    $tr/desc,
                    $publishingAuthority,
                    $copyright,
                    $tr/(* except (name | desc | publishingAuthority | copyright | transaction | representingTemplate))
                    ,
                    (: we return just a count of issues and leave it up to the calling party to get those if required :)
                    <issueAssociation count="{count($setlib:colDecorData//issue/object[@id = $tr/@id][@effectiveDate = $tr/@effectiveDate])}"/>
                }
        else (
            for $tr in $result
            return
                element {name($tr)} {
                    $tr/@*,
                    $tr/name,
                    $tr/desc,
                    (:
                    $publishingAuthority,
                    $copyright,
                    :)
                    $tr/(* except (name | desc | publishingAuthority | copyright | transaction | representingTemplate))
                    ,
                    (: we return just a count of issues and leave it up to the calling party to get those if required :)
                    <issueAssociation count="{count($setlib:colDecorData//issue/object[@id = $tr/@id][@effectiveDate = $tr/@effectiveDate])}"/>
                    ,
                    $tr/transaction
                    ,
                    (: just create a representingTemplate element if this is a leaf transaction, i.e. has a parent transaction :)
                    if ($tr/parent::transaction) then scapi:representingTemplateBasics($tr/representingTemplate, $oidnamemap, $templatemap, true()) else ()
                }
            )
        else ()
    
};

(:~ Create scenario

@param $authmap      - required. Map derived from token
@param $prefix       - required. DECOR project/@prefix
@param $data         - required. DECOR actor object as xml
@return scenario object as xml
:)
declare function scapi:postTransaction($authmap as map(*), $scenarioId as xs:string, $scenarioEffectiveDate as xs:string, $transactionBaseId as xs:string?, $transactionType as xs:string?, $insertMode as xs:string, $insertRef as xs:string?, $insertFlexibility as xs:string?, $sourceId as xs:string?, $sourceEffectiveDate as xs:string?, $data as element(transaction)?) as element(transaction) {
    let $storedScenario         := utillib:getScenario($scenarioId, $scenarioEffectiveDate)
    let $sourceTransaction      := if (empty($sourceId)) then () else utillib:getTransaction($sourceId, $sourceEffectiveDate)
    let $insertTransaction      := 
        if (empty($insertRef)) then () else 
        if (empty($insertFlexibility)) then 
            $storedScenario//transaction[@id = $insertRef] else (
            $storedScenario//transaction[@id = $insertRef][@effectiveDate = $insertFlexibility]
        )
    
    let $decor                  := $storedScenario/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix
    let $defaultLanguage        := $decor/project/@defaultLanguage
    let $transactionType        := ($transactionType[not(. = '')], $data/@type)[1]
    
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Scenario with ''', $scenarioId, ''' and effectiveDate ''', $scenarioEffectiveDate, ''', does not exist. Cannot create in non-existent scenario.'))
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-SCENARIOS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify scenarios in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if (empty($sourceId)) then () else if ($sourceTransaction) then () else (
            error($errors:BAD_REQUEST, concat('Source transaction id ''', $sourceId, ''' effectiveDate ''', $sourceEffectiveDate, ''' requested but not found. Could not create new transaction from source transaction.'))
        )
    let $check                  :=
        if (empty($insertRef)) then () else if ($insertTransaction) then () else (
            error($errors:BAD_REQUEST, concat('Insert transaction id ''', $insertRef, ''' effectiveDate ''', $insertFlexibility, ''' requested but not found. Could not create new transaction ', $insertMode, ' the referenced transaction.'))
        )
    let $check                  :=
        switch ($insertMode)
        case 'into' 
        case 'following'
        case 'preceding' return ()
        default return (
            error($errors:BAD_REQUEST, 'Parameter insertMode ' || $insertMode || ' SHALL be one of ''into'', ''following'', or ''preceding''')
        )
    let $check                  :=
        if (empty($insertRef)) then 
            if ($insertMode = 'into') then () else (
                error($errors:BAD_REQUEST, 'Parameter insertRef SHALL have a value when insertMode is ' || $insertMode || '. Otherwise we don''t know where to insert')
            )
        else ()
    let $check                  :=
        if (empty($transactionBaseId)) then () else (
            let $transactionBaseIds         := decorlib:getBaseIdsP($decor, $decorlib:OBJECTTYPE-TRANSACTION)
            return
                if ($transactionBaseIds[@id = $transactionBaseId]) then () else (
                    error($errors:BAD_REQUEST, 'Parameter transactionBaseId SHALL have a value that is declared as baseId with type TR (transactions) in the project. Allowable are ' || string-join($transactionBaseIds/@id, ', '))
                )
        )
    let $check                  :=
        if ($transactionType = $scapi:TRANSACTION-TYPE/enumeration/@value) then (
            if ($data[@type = 'group'][empty(transaction)]) then
                error($errors:BAD_REQUEST, 'Transaction of type group SHALL contain child transactions')
            else
            if ($data[not(@type = 'group')][transaction]) then
                error($errors:BAD_REQUEST, 'Transaction of type ' || $data/@type || ' SHALL NOT contain child transactions')
            else
            if ($data/descendant::transaction[transaction] | $data/descendant::transaction[@type = 'group']) then
                error($errors:BAD_REQUEST, 'Transaction groups SHALL NOT be nested. There SHALL be only one level of nesting in the input')
            else
            if ($insertTransaction) then 
                switch ($insertMode)
                case 'into' return 
                    if ($insertTransaction[not(@type = 'group')]) then
                        error($errors:BAD_REQUEST, 'Cannot create a new transaction under a transaction of type ' || $insertTransaction/@type || '. Only type group allows child transactions.')
                    else
                    if ($transactionType = 'group') then
                        error($errors:BAD_REQUEST, 'Cannot create a new group under a transaction of type ' || $insertTransaction/@type || '. Transaction groups SHALL NOT be nested.')
                    else ()
                default return
                    if ($insertTransaction/@type = 'group' and $transactionType = 'group') then () else 
                    if (not($insertTransaction/@type = 'group' or $transactionType = 'group')) then () else (
                        error($errors:BAD_REQUEST, 'Cannot create a new transaction of type ' || $transactionType || ' ' || $insertMode || ' a transaction of type ' || $insertTransaction/@type || '. This combination of types is logically incompatible at the same level.')
                    )
            else (
            )
        ) else (
            error($errors:BAD_REQUEST, concat('Transaction type ''', $transactionType, ''' not supported. Supported are: ', string-join($scapi:TRANSACTION-TYPE/enumeration/@value, ', ')))
        )
    
    let $newId                  := decorlib:getNextAvailableIdP($decor, $decorlib:OBJECTTYPE-TRANSACTION, decorlib:getDefaultBaseIdsP($decor, $decorlib:OBJECTTYPE-TRANSACTION)/@id)
    let $newEffectiveDate       := format-date(current-date(), '[Y0001]-[M01]-[D01]') || 'T00:00:00'
    (:let $tridnext               := decorlib:getNextAvailableIdP($decor, $decorlib:OBJECTTYPE-TRANSACTION, decorlib:getDefaultBaseIdsP($decor, $decorlib:OBJECTTYPE-TRANSACTION)/@id):)
    
    (: guarantee defaultLanguage. Preserve any incoming other languages. Guarantee name. Leave desc optional :)
    let $data                   := if ($sourceTransaction) then $sourceTransaction else $data
    let $newTransaction         :=
        if ($data) then (
            <transaction id="{$newId/@id}" effectiveDate="{$newEffectiveDate}" statusCode="draft">
            {
                attribute type {if ($data/transaction) then 'group' else $data/@type},
                $data/@model[string-length() gt 0],
                if ($data/@label[string-length() gt 0]) then attribute label {$data/@label || '-' || substring($newEffectiveDate, 1, 10)} else ()
                (: don't copy expirationDate, officialReleaseDate, canonicalUri :)
                ,
                attribute lastModifiedDate {$newEffectiveDate}
            }
            {
                scapi:handleTransactionElements($data, $defaultLanguage)
            }
            {
                for $trc at $j in $data/transaction
                let $trid  := $newId/@base || '.' || xs:integer($newId/@max) + 1 + $j
                return
                    <transaction id="{$trid}" effectiveDate="{$newEffectiveDate}" statusCode="draft">
                    {
                        attribute type {if ($trc/transaction) then 'group' else ($trc/@type[not(. = 'group')], 'stationary')[1]},
                        $trc/@model[string-length() gt 0],
                        if ($trc/@label[string-length() gt 0]) then attribute label {$trc/@label || '-' || substring($newEffectiveDate, 1, 10)} else ()
                        ,
                        attribute lastModifiedDate {$newEffectiveDate}
                    }
                    {
                        scapi:handleTransactionElements($trc, $defaultLanguage)
                    }
                    </transaction>
            }
            </transaction>
        )
        else (
            let $trgroupid  := $newId/@id
            let $tr1id      := $newId/@base || '.' || xs:integer($newId/@next) + 1
            return
            <transaction id="{$trgroupid}" effectiveDate="{$newEffectiveDate}" statusCode="draft" type="group" lastModifiedDate="{$newEffectiveDate}">
                <name language="{$defaultLanguage}"/>
                <transaction id="{$tr1id}" effectiveDate="{$newEffectiveDate}" statusCode="draft" type="stationary" lastModifiedDate="{$newEffectiveDate}">
                    <name language="{$defaultLanguage}"/>
                    <actors/>
                </transaction>
            </transaction>
        )
        
    let $insert     :=
        if ($insertTransaction) then
            switch ($insertMode)
            case 'following' return update insert $newTransaction following $insertTransaction[last()]
            case 'preceding' return update insert $newTransaction preceding $insertTransaction[1]
            default          return update insert $newTransaction into $insertTransaction[1]
        else
        if ($storedScenario[transaction]) then 
            update insert $newTransaction following $storedScenario/transaction[last()]
        else (
            update insert $newTransaction into $storedScenario
        )

    return
        $newTransaction
};

(:~ Central logic for patching an existing transaction

@param $authmap         - required. Map derived from token
@param $id              - required. DECOR transaction/@id to update
@param $effectiveDate   - required. DECOR transaction/@effectiveDate to update
@param $data            - required. DECOR transaction xml element containing everything that should be in the updated transaction
@return concept object as xml with json:array set on elements
:)
declare function scapi:patchTransaction($authmap as map(*), $id as xs:string, $effectiveDate as xs:string, $data as element(parameters)) as element(transaction) {

    let $storedTransaction      := utillib:getTransaction($id, $effectiveDate)
    let $decor                  := $storedTransaction/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix

    let $lock                   := decorlib:getLocks($authmap, $id, $effectiveDate, ())
    let $lock                   := if ($lock) then $lock else decorlib:setLock($authmap, $id, $effectiveDate, false())
    
    let $check                  :=
        if (empty($storedTransaction)) then 
            error($errors:BAD_REQUEST, 'Transaction with id ''' || $id || ''' and effectiveDate ''' || $effectiveDate || ''' does not exist')
        else
        if (count($storedTransaction) = 1) then () else (
            error($errors:SERVER_ERROR, 'Transaction with id ''' || $id || ''' and effectiveDate ''' || $effectiveDate || ''' occurs ' || count($storedTransaction) || ' times. Inform your database administrator.')
        )
    let $check                  :=
        if ($storedTransaction[@statusCode = $scapi:STATUSCODES-FINAL]) then
            if ($data/parameter[@path = '/statusCode'][@op = 'replace'][not(@value = $scapi:STATUSCODES-FINAL)]) then () else 
            if ($data[count(parameter) = 1]/parameter[@path = '/statusCode']) then () else (
                error($errors:BAD_REQUEST, 'Transaction  with id ''' || $id || ''' and effectiveDate ''' || $effectiveDate || ''' cannot be patched while it has one of status: ' || string-join($scapi:STATUSCODES-FINAL, ', '))
            )
        else ()
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-SCENARIOS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify scenarios/transactions in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if ($lock) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have a lock for this transaction (anymore). Get a lock first.'))
        )
    let $check                  :=
        if ($data[parameter]) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall be an array of ''parameter''.')
        )
    let $unsupportedops         := $data/parameter[not(@op = ('add', 'replace', 'remove'))]
    let $check                  :=
        if ($unsupportedops) then
            error($errors:BAD_REQUEST, 'Submitted parameters shall have supported ''op'' value. Found ''' || string-join(distinct-values($unsupportedops/@op), ''', ''') || ''', expected ''add'', ''replace'', or ''remove''') 
        else ()
    let $checkn                 := (
        if ($data/parameter[@path = '/name']) then
            if (count($storedTransaction/name) - count($data/parameter[@op = 'remove'][@path = '/name']) + count($data/parameter[@op = ('add', 'replace')][@path = '/name']) ge 1) then () else (
                'A transaction SHALL have at least one name. You cannot remove every name.'
            )
        else ()
        (: https://art-decor.atlassian.net/browse/AD30-451 :)
        (:,
        if ($data/parameter[@path = '/desc']) then
            if (count($storedTransaction/desc) - count($data/parameter[@op = 'remove'][@path = '/desc']) + count($data/parameter[@op = ('add', 'replace')][@path = '/desc']) ge 1) then () else (
                'A transaction SHALL have at least one description. You cannot remove every desc.'
            )
        else ():)
    )
    
    let $check                  :=
        for $param in $data/parameter
        let $op         := $param/@op
        let $path       := $param/@path
        let $value      := $param/@value
        let $pathpart   := substring-after($path, '/')
        return
            switch ($path)
            case '/statusCode' return (
                if ($op = 'remove') then
                    'Parameter path ''' || $path || ''' does not support ' || $op
                else 
                if (utillib:isStatusChangeAllowable($storedTransaction, $value)) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Supported are: ' || string-join(map:get($utillib:itemstatusmap, string($storedTransaction/@statusCode)), ', ')
                )
            )
            case '/expirationDate'
            case '/officialReleaseDate' return (
                if ($op = 'remove' or ($op = 'replace' and string($value) = '')) then () else 
                if ($value castable as xs:dateTime) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL be yyyy-mm-ddThh:mm:ss.'
                )
            )
            case '/canonicalUri'
            case '/model'
            case '/label'
            case '/versionLabel' return (
                if ($op = 'remove') then () else
                (:if ($op = 'remove' or ($op = 'replace' and string($value) = '')) then () else:) 
                if ($value[not(. = '')]) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL NOT be empty.'
                )
            )
            case '/type' return (
                if ($op = 'remove') then 
                    'Parameter path ''' || $path || ''' does not support ' || $op
                else 
                if ($value = $scapi:TRANSACTION-TYPE/enumeration/@value) then 
                    switch ($value)
                    case 'group' return 
                        if ($storedTransaction[transaction] | $storedTransaction[empty(representingTemplate)]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Type has value ''group'' but the target transaction is not a group.'
                        )
                    default return 
                        if ($storedTransaction[not(@type = 'group')]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Type has value ''' || $value || ''' but the target transaction is a group.'
                        )
                else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || '''. Type has unsupported value ' || $value || '. SHALL be one of: ' || string-join($scapi:TRANSACTION-TYPE/enumeration/@value, ', ')
                )
            )
            case '/name' 
            case '/desc' 
            case '/trigger'
            case '/condition'
            case '/dependencies'
            case '/copyright' return (
                if ($param[count(value/*[name() = $pathpart]) = 1]) then (
                    if ($param/value/*[name() = $pathpart][@inherited]) then 
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Contents SHALL NOT be marked ''inherited''.'
                    else (),
                    if ($param/value/*[name() = $pathpart][matches(@language, '[a-z]{2}-[A-Z]{2}')]) then 
                        if ($param[@op = 'remove'] | $param/value/*[name() = $pathpart][.//text()[not(normalize-space() = '')]]) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A ' || $pathpart || ' SHALL have contents.'
                        )
                    else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A ' || $pathpart || ' SHALL have a language with pattern ll-CC.'
                    )
                )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/*[name() = $pathpart]) 
                )
            )
            case '/publishingAuthority' return (
                if ($param[count(value/publishingAuthority) = 1]) then (
                    if ($param/value/publishingAuthority[@inherited]) then 
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Contents SHALL NOT be marked ''inherited''.'
                    else (),
                    if ($param/value/publishingAuthority[@id]) then
                        if ($param/value/publishingAuthority[utillib:isOid(@id)]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || ''' publishingAuthority.id SHALL be an OID is present. Found ' || string-join($param/value/publishingAuthority/@id, ', ')
                        )
                    else (),
                    if ($param/value/publishingAuthority[@name[not(. = '')]]) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || ''' publishingAuthority.name SHALL be a non empty string.'
                    ),
                    let $unsupportedtypes := $param/value/publishingAuthority/addrLine/@type[not(. = $scapi:ADDRLINE-TYPE/enumeration/@value)]
                    return
                        if ($unsupportedtypes) then
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Publishing authority has unsupported addrLine.type value(s) ' || string-join($unsupportedops, ', ') || '. SHALL be one of: ' || string-join($scapi:ADDRLINE-TYPE/enumeration/@value, ', ')
                        else ()
                ) else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' SHALL have a single publishingAuthority under value. Found ' || count($param/value/publishingAuthority)
                )
            )
            case '/property' return (
                if ($param[count(value/property) = 1]) then
                    if ($param/value/property/@name) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || '''. Property.name SHALL be a non empty string.'
                    )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/property) 
                ),
                if ($param/value/property[@datatype]) then 
                    if ($param/value/property[@datatype = $scapi:VALUEDOMAIN-TYPE/enumeration/@value]) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || ''' with type ''' || string-join($param/value/property/@datatype, ' ') || '''. Supported are: ' || string-join($scapi:VALUEDOMAIN-TYPE/enumeration/@value, ', ')
                    )
                else ()
            )
            case '/actors' return (
                if ($op = 'remove') then
                    if ($storedTransaction[@type = 'group']) then () else (
                        'Parameter path ''' || $path || ''' does not support ' || $op || '. A transaction of type other than group SHALL have actors containing everything that needs to be present after patching the transaction.' 
                    )
                else 
                if ($storedTransaction[@type = 'group']) then 
                    'Parameter path ''' || $path || ''' does not support ' || $op || '. A transaction of type group SHALL NOT have actors.' 
                else 
                if ($param[count(value/actors) = 1]) then
                    for $actor in $param/value/actors/actor
                    return
                        if ($actor[@id = $decor/scenarios/actors/actor/@id]) then
                            if ($actor[@role = $scapi:ACTOR-ROLE/enumeration/@value]) then () else (
                                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Actor has unsupported role ''' || $actor/@role || '''. Supported are: ' || string-join($scapi:ACTOR-ROLE/enumeration/@value, ', ')
                            )
                        else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Actor with id ''' || $actor/@id || ''' does not exist in the project yet.' 
                        )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one actors under value. Found ' || count($param/value/actors) 
                )
            )
            case '/representingTemplate' return (
                if ($op != 'replace') then (
                    'Parameter path ''' || $path || ''' does not support ' || $op || '. A patch on the entire transaction representingTemplate can only replace.' 
                )
                else 
                if ($storedTransaction[@type = 'group']) then 
                    'Parameter path ''' || $path || ''' does not support ' || $op || '. A transaction of type group SHALL NOT have representingTemplate.' 
                else 
                if ($param[count(value/representingTemplate) = 1]) then (
                    if ($param/value/representingTemplate[@sourceDataset[not(. = '')]]) then () else (
                        'Parameter path ''' || $path || ''' does not support ' || $op || '. A transaction dataset reference SHALL have a sourceDataset.'
                    )
                ) else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one representingTemplate under value. Found ' || count($param/value/representingTemplate) 
                )
            )
            case '/representingTemplate/dataset' return (
                if ($op = 'remove') then ()
                    (:if ($storedTransaction[@type = 'group']) then () else (
                        'Parameter path ''' || $path || ''' does not support ' || $op || '. A transaction of type other than group SHALL have representingTemplate.' 
                    ):)
                else 
                if ($storedTransaction[@type = 'group']) then 
                    'Parameter path ''' || $path || ''' does not support ' || $op || '. A transaction of type group SHALL NOT have representingTemplate.' 
                else 
                if ($param[count(value/representingTemplate) = 1]) then (
                    if ($param/value/representingTemplate[@sourceDataset[not(. = '')]]) then () else (
                        'Parameter path ''' || $path || ''' does not support ' || $op || '. A transaction dataset reference SHALL have a sourceDataset.'
                    )
                ) else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one representingTemplate under value. Found ' || count($param/value/representingTemplate) 
                )
            )
            case '/representingTemplate/template' return (
                if ($op = 'remove') then ()
                    (:if ($storedTransaction[@type = 'group']) then () else (
                        'Parameter path ''' || $path || ''' does not support ' || $op || '. A transaction of type other than group SHALL have representingTemplate.' 
                    ):)
                else 
                if ($storedTransaction[@type = 'group']) then 
                    'Parameter path ''' || $path || ''' does not support ' || $op || '. A transaction of type group SHALL NOT have representingTemplate.' 
                else 
                if ($param[count(value/representingTemplate) = 1]) then (
                    if ($param/value/representingTemplate[@ref[not(. = '')]]) then () else (
                        'Parameter path ''' || $path || ''' does not support ' || $op || '. A transaction template reference SHALL have a ref.'
                    )
                ) else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one representingTemplate under value. Found ' || count($param/value/representingTemplate) 
                )
            )
            case '/representingTemplate/questionnaire' return (
                if ($op = 'remove') then ()
                    (:if ($storedTransaction[@type = 'group']) then () else (
                        'Parameter path ''' || $path || ''' does not support ' || $op || '. A transaction of type other than group SHALL have representingTemplate.' 
                    ):)
                else 
                if ($storedTransaction[@type = 'group']) then 
                    'Parameter path ''' || $path || ''' does not support ' || $op || '. A transaction of type group SHALL NOT have representingTemplate.' 
                else 
                if ($param[count(value/representingTemplate) = 1]) then (
                    if ($param/value/representingTemplate[@representingQuestionnaire[not(. = '')]]) then () else (
                        'Parameter path ''' || $path || ''' does not support ' || $op || '. A transaction template reference SHALL have a representingQuestionnaire.'
                    )
                ) else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one representingTemplate under value. Found ' || count($param/value/representingTemplate) 
                )
            )
            default return (
                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Path value not supported'
            )
     let $check                 :=
        if (empty($checkn) and empty($check)) then () else (
            error($errors:BAD_REQUEST,  string-join (($checkn, $check), ' '))
        )
    
    let $intention              := if ($storedTransaction[@statusCode = 'final']) then 'patch' else 'version'
    let $update                 := histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-TRANSACTION, $projectPrefix, $intention, $storedTransaction)
    
    let $update                 :=
        for $param in $data/parameter
        return
            switch ($param/@path)
            case '/statusCode'
            case '/type' return (
                let $attname  := substring-after($param/@path, '/')
                let $new      := attribute {$attname} {$param/@value}
                let $stored   := $storedTransaction/@*[name() = $attname]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedTransaction
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/expirationDate'
            case '/officialReleaseDate'
            case '/canonicalUri'
            case '/model'
            case '/label'
            case '/versionLabel' return (
                let $attname  := substring-after($param/@path, '/')
                let $op       := if ($param[string-length(@value) = 0]) then 'remove' else $param/@op
                let $new      := attribute {$attname} {$param/@value}
                let $stored   := $storedTransaction/@*[name() = $attname]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedTransaction
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/name' 
            case '/desc'
            case '/trigger'
            case '/condition' 
            case '/dependencies' return (
                (: only one per language :)
                let $elmname  := substring-after($param/@path, '/')
                let $new      := utillib:prepareFreeFormMarkupWithLanguageForUpdate($param/value/*[name() = $elmname])
                let $stored   := $storedTransaction/*[name() = $elmname][@language = $param/value/*[name() = $elmname]/@language]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedTransaction
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/publishingAuthority' return (
                (: only one possible :)
                let $new      := utillib:preparePublishingAuthorityForUpdate($param/value/publishingAuthority)
                let $stored   := $storedTransaction/publishingAuthority
                
                return
                switch ($param/@op)
                case ('add') return update insert $new into $storedTransaction
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedTransaction
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/property' return (
                (: multiple possible per language :)
                let $new      := utillib:prepareConceptPropertyForUpdate($param/value/property)
                let $stored   := $storedTransaction/property[@name = $new/@name][lower-case(normalize-space(string-join(.//text(), ''))) = lower-case(normalize-space(string-join($new//text(), '')))]
                
                return
                switch ($param/@op)
                case ('add') return update insert $new into $storedTransaction
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedTransaction
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/copyright' return (
                (: only one per language :)
                let $new      := utillib:prepareFreeFormMarkupWithLanguageForUpdate($param/value/copyright)
                let $stored   := $storedTransaction/copyright[@language = $param/value/copyright/@language]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedTransaction
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/actors' return (
                let $new      := scapi:handleTransactionActors($param/value/actors)
                let $stored   := $storedTransaction/actors
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedTransaction
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/representingTemplate' return (
                let $stored := $storedTransaction/representingTemplate
                let $new    := $param/value/representingTemplate
                (: op can only be replace :)
                return
                if ($stored and $new) then update replace $stored with $new else ()
            )
            case '/representingTemplate/dataset' return (
                let $dsid   := $param/value/representingTemplate/@sourceDataset
                let $dsed   := $param/value/representingTemplate/@sourceDatasetFlexibility[. castable as xs:dateTime or . = 'dynamic']
                let $stored := $storedTransaction/representingTemplate
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return (
                    if ($stored) then (
                        (: if we're changing datasets then best delete concepts from the old one :)
                        if ($stored[@sourceDataset = $dsid][@sourceDatasetFlexibility = $dsed]) then () else update delete $stored/node(),
                        if ($stored[@sourceDataset]) then update value $stored/@sourceDataset with $dsid else update insert $dsid into $stored,
                        if ($dsed) then 
                            if ($stored[@sourceDatasetFlexibility]) then update value $stored/@sourceDatasetFlexibility with $dsed else update insert $dsed into $stored
                        else (
                            update delete $stored/@sourceDatasetFlexibility
                        )
                    )
                    else (
                        update insert <representingTemplate>{$dsid, $dsed}</representingTemplate> into $storedTransaction
                    )
                )
                case ('remove') return (
                    update delete $stored/@sourceDataset,
                    update delete $stored/@sourceDatasetFlexibility,
                    (: if we're deleting the source dataset then best delete any concepts :)
                    update delete $stored/concept
                )
                default return ( (: unknown op :) )
            )
            case '/representingTemplate/template' return (
                let $tmid   := $param/value/representingTemplate/@ref
                let $tmed   := $param/value/representingTemplate/@flexibility[. castable as xs:dateTime or . = 'dynamic']
                let $stored := $storedTransaction/representingTemplate
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return (
                    if ($stored) then (
                        if ($stored[@ref]) then update value $stored/@ref with $tmid else update insert $tmid into $stored,
                        if ($tmed) then 
                            if ($stored[@flexibility]) then update value $stored/@flexibility with $tmed else update insert $tmed into $stored
                        else (
                            update delete $stored/@flexibility
                        )
                    )
                    else (
                        update insert <representingTemplate>{$tmid, $tmed}</representingTemplate> into $storedTransaction
                    )
                )
                case ('remove') return (
                    update delete $stored/@ref,
                    update delete $stored/@flexibility
                )
                default return ( (: unknown op :) )
            )
            case '/representingTemplate/questionnaire' return (
                let $qid   := $param/value/representingTemplate/@representingQuestionnaire
                let $qed   := $param/value/representingTemplate/@representingQuestionnaireFlexibility[. castable as xs:dateTime or . = 'dynamic']
                let $stored := $storedTransaction/representingTemplate
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return (
                    if ($stored) then (
                        if ($stored[@representingQuestionnaire]) then update value $stored/@representingQuestionnaire with $qid else update insert $qid into $stored,
                        if ($qed) then 
                            if ($stored[@representingQuestionnaireFlexibility]) then update value $stored/@representingQuestionnaireFlexibility with $qed else update insert $qed into $stored
                        else (
                            update delete $stored/@representingQuestionnaireFlexibility
                        )
                    )
                    else (
                        update insert <representingTemplate>{$qid, $qed}</representingTemplate> into $storedTransaction
                    )
                )
                case ('remove') return (
                    update delete $stored/@representingQuestionnaire,
                    update delete $stored/@representingQuestionnaireFlexibility
                )
                default return ( (: unknown op :) )
            )
            default return ( (: unknown path :) )
    
    (: after all the updates, the XML Schema order is likely off. Restore order :)
    let $preparedTransaction    := scapi:handleTransaction($storedTransaction)
    
    let $update                 := update replace $storedTransaction with $preparedTransaction
    let $update                 := update delete $lock
    
    return
        element {name($preparedTransaction)} {
            $preparedTransaction/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($preparedTransaction/*)
        }
};

(:~ Central logic for deleting a transaction. If the item does not exist, then no error is given.

@param $id              - required. DECOR transaction/@id to delete
@param $effectiveDate   - optional. DECOR transaction/@effectiveDate to delete
@return nothing
:)
declare function scapi:deleteTransaction($authmap as map(*)?, $id as xs:string, $effectiveDate as xs:string?) {
    let $storedTransaction      := utillib:getTransaction($id, $effectiveDate, (), ())
    let $decor                  := $storedTransaction/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if ($authmap?groups = 'dba') then () else (
            error($errors:FORBIDDEN, 'You need to be dba for this feature')
        )    
    let $update                 := update delete $storedTransaction
    
    return
        ()
};

(:~ Update transaction statusCode, versionLabel, expirationDate and/or officialReleaseDate

@param $authmap required. Map derived from token
@param $id required. DECOR transaction/@id to update
@param $effectiveDate required. DECOR transaction/@effectiveDate to update
@param $recurse optional as xs:boolean. Default: false. Allows recursion into child particles to apply the same updates
@param $listOnly optional as xs:boolean. Default: false. Allows to test what will happen before actually applying it
@param $newStatusCode optional as xs:string. Default: empty. If empty, does not update the statusCode
@param $newVersionLabel optional as xs:string. Default: empty. If empty, does not update the versionLabel
@param $newExpirationDate optional as xs:string. Default: empty. If empty, does not update the expirationDate 
@param $newOfficialReleaseDate optional as xs:string. Default: empty. If empty, does not update the officialReleaseDate 
@return list object with success and/or error elements or error
:)
declare function scapi:putTransactionStatus($request as map(*)) {

    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $recurse                        := $request?parameters?recurse = true()
    let $list                           := $request?parameters?list = true()
    let $statusCode                     := $request?parameters?statusCode
    let $versionLabel                   := $request?parameters?versionLabel
    let $expirationDate                 := $request?parameters?expirationDate
    let $officialReleaseDate            := $request?parameters?officialReleaseDate
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    
    return
        scapi:setTransactionStatus($authmap, $id, $effectiveDate, $recurse, $list, $statusCode, $versionLabel, $expirationDate, $officialReleaseDate)
};

(:~ Central logic for updating an existing transaction representingTemplate

@param $authmap         - required. Map derived from token
@param $id              - required. DECOR scenario/@id to update
@param $effectiveDate   - required. DECOR scenario/@effectiveDate to update
@param $data            - required. DECOR scenario xml element containing everything that should be in the updated scenario
@return concept object as xml with json:array set on elements
:)
declare function scapi:putRepresentingTemplate($authmap as map(*), $id as xs:string, $effectiveDate as xs:string, $data as element(dataset)?, $cloneFromTrxId as xs:string?, $cloneFromTrxEd as xs:string?, $cloneFromCptId as xs:string?, $cloneFromCptEd as xs:string?, $deletelock as xs:boolean) {

    let $storedTransaction            := utillib:getTransaction($id, $effectiveDate, (), ())
    let $storedRepresentingTemplate   := $storedTransaction/representingTemplate
    let $decor                        := $storedTransaction/ancestor::decor
    let $projectPrefix                := $decor/project/@prefix
    let $defaultLanguage              := $decor/project/@defaultLanguage
    
    let $check                        :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-SCENARIOS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify scenarios in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    
    let $locks                        := decorlib:getLocks($authmap, $storedTransaction, true())
    let $locks                        := if ($locks) then $locks else decorlib:setLock($authmap, $storedTransaction, false())
    
    let $check                  :=
        if ($locks) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have a lock for this transaction (anymore). Get a lock first.'))
        )
    
    let $data                   := 
        if ($data) then $data else if (empty($cloneFromTrxId)) then () else (
            let $mergeDataset       := utillib:getDatasetTree((), (), $cloneFromTrxId, $cloneFromTrxEd, true())
            return
                scapi:mergeDatasets($storedTransaction, $mergeDataset, $cloneFromCptId, $cloneFromCptEd)
        )

    let $check                  :=
        if (empty($storedTransaction)) then 
            error($errors:BAD_REQUEST, 'Transaction with id ''' || $id || ''' and effectiveDate ''' || $effectiveDate || ''' does not exist')
        else
        if ($storedTransaction[@type = 'group'] | $storedTransaction[transaction]) then
            error($errors:BAD_REQUEST, 'Transaction with id ''' || $id || ''' and effectiveDate ''' || $effectiveDate || ''' is a group. Cannot add or update a representingTemplate on a transaction group.')
        else
        if ($storedTransaction/ancestor-or-self::*[@statusCode = $scapi:STATUSCODES-FINAL]) then
            error($errors:BAD_REQUEST, 'Transaction with id ''' || $id || ''' and effectiveDate ''' || $effectiveDate || ''' cannot be updated while it or one of its parents has one of status: ' || string-join($scapi:STATUSCODES-FINAL, ', '))
        else
        (:if (empty($storedRepresentingTemplate)) then 
            error($errors:BAD_REQUEST, 'Transaction with id ''' || $id || ''' and effectiveDate ''' || $effectiveDate || ''' exists but does not have a representingTemplate to update.')
        else:)
        if (count($storedTransaction) != 1) then
            error($errors:SERVER_ERROR, 'Transaction with id ''' || $id || ''' and effectiveDate ''' || $effectiveDate || ''' occurs ' || count($storedTransaction) || ' times. Inform your database administrator.')
        else ()
    
    let $coconstraint           := $data//(concept | condition)[@minimumMultiplicity[not(matches(., '^(\?|0|([1-9]\d*))?$'))] | @maximumMultiplicity[not(matches(., '^(\?|\*|0|([1-9]\d*))?$'))]]
    let $check                  :=
        if ($coconstraint) then
            error($errors:BAD_REQUEST, concat('A concept or condition minimumMultiplicity SHALL be empty, ? or numeric, and maximumMultiplicity SHALL be empty, ?, * or numeric. Found: ', string-join(
                for $c in $coconstraint
                let $concept := $c/ancestor-or-self::concept[1]
                return
                    '''' || $concept/name[1] || ''' id ''' || tokenize($concept/(@ref | @id), '\.')[last()] || ''' ' || string-join((($c/@minimumMultiplicity[not(. = '')], '?')[1], '..', ($c/@maximumMultiplicity[not(. = '')], '?')[1], $c/@conformance), '')
            , ', ')))
        else ()
    
    let $coconstraint           := $data//(concept | condition)[@conformance[. = 'M'] | @isMandatory[. = 'true']][@minimumMultiplicity[. = '0'] | @maximumMultiplicity[. = '0'] | @conformance[not(. = ('', 'R', 'M'))]]
    let $check                  :=
        if ($coconstraint) then
            error($errors:BAD_REQUEST, concat('A mandatory concept or condition SHALL NOT have minimum or maximum cardinality 0 or conformance other than R(equired). Found: ', string-join(
                for $c in $coconstraint
                let $concept := $c/ancestor-or-self::concept[1]
                return
                    '''' || $concept/name[1] || ''' id ''' || tokenize($concept/(@ref | @id), '\.')[last()] || ''' ' || string-join((concat(($c/@minimumMultiplicity[not(. = '')], '?')[1], '..', ($c/@maximumMultiplicity[not(. = '')], '?')[1]), $c/@conformance, if ($c[not($c/@conformance)][@isMandatory = 'true']) then 'M' else ()), ' ')
            , ', ')))
        else ()
        
    let $coconstraint           := $data//(concept | condition)[@conformance = 'NP'][@minimumMultiplicity[. != '0'] | @maximumMultiplicity[. != '0']]
    let $check                  :=
        if ($coconstraint) then
            error($errors:BAD_REQUEST, concat('A concept or condition with conformance N(ot)P(resent) SHALL NOT have minimum or maximum cardinality other than 0. Found: ', string-join(
                for $c in $coconstraint
                let $concept := $c/ancestor-or-self::concept[1]
                return
                    '''' || $concept/name[1] || ''' id ''' || tokenize($concept/(@ref | @id), '\.')[last()] || ''' ' || string-join((($c/@minimumMultiplicity[not(. = '')], '?')[1], '..', ($c/@maximumMultiplicity[not(. = '')], '?')[1], $c/@conformance), '')
            , ', ')))
        else ()
        
    let $coconstraint           := $data//concept[@conformance = 'C'][empty(condition) and empty(enableWhen)]
    let $check                  :=
        if ($coconstraint) then
            error($errors:BAD_REQUEST, concat('A conditional concept SHALL have one or more conditions / enableWhen. Found: ', string-join(
                for $c in $coconstraint
                let $concept := $c/ancestor-or-self::concept[1]
                return
                    '''' || $concept/name[1] || ''' id ''' || tokenize($concept/(@ref | @id), '\.')[last()] || ''' ' || string-join((($c/@minimumMultiplicity[not(. = '')], '?')[1], '..', ($c/@maximumMultiplicity[not(. = '')], '?')[1], $c/@conformance), '')
            , ', ')))
        else ()
    let $coconstraint           := $data//concept[not(@conformance = 'C')][condition]
    let $check                  :=
        if ($coconstraint) then
            error($errors:BAD_REQUEST, concat('A concept with conditions SHALL have conformance C. Found: ', string-join(
                for $c in $coconstraint
                let $concept := $c/ancestor-or-self::concept[1]
                return
                    '''' || $concept/name[1] || ''' id ''' || tokenize($concept/(@ref | @id), '\.')[last()] || ''' ' || string-join((($c/@minimumMultiplicity[not(. = '')], '?')[1], '..', ($c/@maximumMultiplicity[not(. = '')], '?')[1], $c/@conformance), '')
            , ', ')))
        else ()
    let $coconstraint           := $data//condition[@conformance = 'C']
    let $check                  :=
        if ($coconstraint) then
            error($errors:BAD_REQUEST, concat('A concept condition SHALL NOT have conformance C. Found: ', string-join(
                for $c in $coconstraint
                let $concept := $c/ancestor-or-self::concept[1]
                return
                    '''' || $concept/name[1] || ''' id ''' || tokenize($concept/(@ref | @id), '\.')[last()] || ''' ' || string-join((($c/@minimumMultiplicity[not(. = '')], '?')[1], '..', ($c/@maximumMultiplicity[not(. = '')], '?')[1], $c/@conformance), '')
            , ', ')))
        else ()
    
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-SCENARIOS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify scenarios in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if (empty($cloneFromCptId) or empty($storedRepresentingTemplate)) then () else 
        if ($data[@sourceDataset = $storedRepresentingTemplate/@sourceDataset][@sourceDatasetFlexibility = $storedRepresentingTemplate/@sourceDatasetFlexibility]) then () else (
            error($errors:BAD_REQUEST, 'Input SHALL match sourceDataset and sourceDatasetFlexibility of the representingTemplate to update. Found: ''' || $data/@sourceDataset || '''/''' || $data/@sourceDatasetFlexibility || ''', expected ''' || $storedRepresentingTemplate/@sourceDataset || '''/''' || $storedRepresentingTemplate/@sourceDatasetFlexibility || '''. Use patch on the transaction to update the dataset if necessary.')
        )

    let $preparedRepresentingTemplate   := scapi:prepareRepresentingTemplate($data, $defaultLanguage)
    
    let $checkv                 :=
        for $question in $preparedRepresentingTemplate//enableWhen/@question
        let $conceptId          := ($question/ancestor::concept/(@id | @ref))[1]
        let $conceptName        := ($data//concept[(@id | @ref) = $conceptId]/name)[1]
        return
            if ($conceptId = $question) then 
                'Concept ''' || $conceptId || ''' ' ||  $conceptName || ' is enabled based on itself.'
            else
            if ($preparedRepresentingTemplate/concept[(@id | @ref) = $question]) then () else (
                'Concept ''' || $conceptId || ''' ' ||  $conceptName || ' is enabled on a concept ''' || $question || ''' that is not part of the transaction.'
            )
    let $check                  :=
        if (empty($checkv)) then () else (
            error($errors:BAD_REQUEST, 'Transaction concepts with enableWhen SHALL depend on one or more other concepts that are also present in the transaction. ' || normalize-space(string-join($checkv, ' ')))
        )
    
    let $r                      := validation:jaxv-report($preparedRepresentingTemplate, $setlib:docDecorSchema)
    let $check                  := 
        if ($r/status='invalid') then
            error($errors:BAD_REQUEST, 'Transaction representingTemplate contents are not schema compliant: ' || serialize($r))
            (:error($errors:BAD_REQUEST, 'Transaction representingTemplate contents are not schema compliant: ' || serialize($param/value/item) || ' ----- ' || serialize($data) || ' ----- ' || serialize($r)):)
        else ()
    (: save history:)
    let $intention                      :=
        if ($storedTransaction[@statusCode='final']) then 'patch' else 'version'
    let $history                        :=
        histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-TRANSACTION, $projectPrefix, $intention, $storedTransaction)
    
    let $updates                        := 
        if ($storedRepresentingTemplate) then 
            update replace $storedRepresentingTemplate with $preparedRepresentingTemplate
        else (
            update insert $preparedRepresentingTemplate into $storedTransaction
        )
    let $delete                         := if ($deletelock) then update delete $locks else ()
    
    return ()
};

(: ****************** private functions ****************** :)

declare %private function scapi:xml2fhirjson($node as node()*) as node()* {
    for $e in $node
    return
        element {$e/name()} {
            $e/(@* except @value),
            $e/@value/string(),
            scapi:xml2fhirjson($e/*)
        }
};

declare %private function scapi:prepareTransactionForUpdate($transaction as element(), $storedScenario as element()?, $baseIdTransaction as xs:string) as element() {
    let $storedTransaction  := 
        if ($transaction[string-length(@effectiveDate) gt 0]) then 
            $storedScenario//transaction[@id = $transaction/@id][@effectiveDate = $transaction/@effectiveDate]
        else (
            $storedScenario//transaction[@id = $transaction/@id]
        )
    let $editedTransaction  := if ($transaction[edit]) then $transaction else $storedTransaction
    let $defaultLanguage    := ($storedScenario/ancestor::decor/project/@defaultLanguage, $storedScenario//@language, 'en-US')[1]
    
    let $transactionType    :=
        if ($editedTransaction[transaction]) then attribute type {'group'} else
        if ($editedTransaction/@type[not(. = 'group')]) then $editedTransaction/@type else
        if ($storedTransaction/@type[not(. = 'group')]) then $storedTransaction/@type else (
            attribute type {'stationary'}
        )
    return
    <transaction>
    {
        if ($storedTransaction/@id) then 
            $storedTransaction/@id 
        else
        if ($transaction/@id) then 
            $transaction/@id
        else (
            let $tridnext   := data($storedScenario/ancestor::scenarios[1]/@nextTransactionLeaf)
            let $update     := update value $storedScenario/ancestor::scenarios[1]/@nextTransactionLeaf with xs:integer($tridnext) + 1
            return
                attribute id {$baseIdTransaction || '.' || $tridnext}
        ),
        if ($storedTransaction[@effectiveDate castable as xs:dateTime]) then
            $storedTransaction/@effectiveDate
        else
        if ($transaction/@effectiveDate[. castable as xs:dateTime]) then
            $transaction/@effectiveDate
        else
        if ($storedTransaction/ancestor::*[@effectiveDate castable as xs:dateTime]) then
            ($storedTransaction/ancestor::*/@effectiveDate[. castable as xs:dateTime])[last()]
        else (
            attribute effectiveDate {substring(string(current-dateTime()),1,19)}
        )
        ,
        if ($storedTransaction/@statusCode) then 
            $storedTransaction/@statusCode
        else 
        if ($transaction/@statusCode) then 
            $transaction/@statusCode
        else (
            attribute statusCode {'draft'}
        )
    }
    {
        $editedTransaction/@versionLabel[not(.='')],
        $editedTransaction/@expirationDate[not(.='')],
        $editedTransaction/@officialReleaseDate[not(.='')],
        $transactionType,
        $editedTransaction/@label[not(.='')],
        $editedTransaction/@model[not(.='')],
        $editedTransaction/@canonicalUri[not(.='')],
        $editedTransaction/@lastModifiedDate[not(.='')]
        ,
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($editedTransaction/name)
        ,
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($editedTransaction/desc)
        ,
        for $trigger in $editedTransaction/trigger[string-length()>0]
        let $trigger    := utillib:parseNode($trigger)
        return
            <trigger>{$trigger/@*[not(.='')], $trigger/node()}</trigger>
        ,
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($editedTransaction/condition)
        ,
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($editedTransaction/dependencies)
        ,
        (: new ... 2021-05-21 :)
        (:want at least a @name on publishingAuthority for it to persist:)
        for $authority in $editedTransaction/publishingAuthority[@name[string-length()>0]]
        let $n := $authority/@name
        group by $n
        return
            utillib:preparePublishingAuthorityForUpdate($authority[1])
        ,
        (: new ... 2021-05-21 :)
        for $node in $editedTransaction/property[string-length()>0]
        return
            utillib:parseNode($node)
        ,
        (: new ... 2021-05-21 :)
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($editedTransaction/copyright)
        ,
        if ($transactionType = 'group') then
            for $t in $transaction/transaction
            return
                scapi:prepareTransactionForUpdate($t, $storedScenario, $baseIdTransaction)
        else (
            <actors>
            {
                for $actor in $editedTransaction/actors/actor[not(string(@id) = '')][not(string(@role) = '')]
                return
                    <actor>{$actor/@id, $actor/@role}</actor>
            }
            </actors>
            ,
            if ($editedTransaction/representingTemplate/(@ref|@sourceDataset|@representingQuestionnaire)[string-length()>0]) then (
                <representingTemplate>
                {
                    (:KH/AH 2013-02-21 @displayName is deprecated, so don't save that... Just save known valid attributes:)
                    (:AH 2015-04-26 @sourceDatasetFlexibility new:)
                    (:AH 2023-08-22 @representingQuestionnaire(Flexibilty) new:)
                    $editedTransaction/representingTemplate/@sourceDataset[string-length() gt 0],
                    $editedTransaction/representingTemplate/@sourceDatasetFlexibility[string-length() gt 0],
                    $editedTransaction/representingTemplate/@ref[string-length() gt 0],
                    $editedTransaction/representingTemplate/@flexibility[string-length() gt 0],
                    $editedTransaction/representingTemplate/@representingQuestionnaire[string-length() gt 0],
                    $editedTransaction/representingTemplate/@representingQuestionnaireFlexibility[string-length() gt 0],
                    (:merge with stored transaction concepts as these would not be part of a save in the 
                    scenario editor, but would come from the transaction editor. suppose that you edit a 
                    scenario before refreshing the scenario editor, you might actually overwrite what you 
                    have done in the transaction editor. :)
                    if ($storedTransaction/representingTemplate[@sourceDataset = $editedTransaction/representingTemplate/@sourceDataset]) then (
                        let $dataset    := utillib:getDataset($storedTransaction/representingTemplate/@sourceDataset[string-length() gt 0], $storedTransaction/representingTemplate/@sourceDatasetFlexibility[string-length() gt 0])
                        
                        for $concept in $storedTransaction/representingTemplate[@sourceDataset[string-length() gt 0]]/concept[not(@absent = 'true')]
                        return
                            scapi:prepareRepresentingTemplateConcept($concept, $dataset, $defaultLanguage)
                    )
                    else ()
                }
                </representingTemplate>
            ) else ()
        )
    }
    </transaction>
};

declare %private function scapi:prepareRepresentingTemplate($representingTemplate as element(), $defaultLanguage as xs:string) as element(representingTemplate) {
    <representingTemplate>
    {
        let $dsid     := ($representingTemplate[self::representingTemplate]/@sourceDataset,                         $representingTemplate[self::dataset]/@id)[not(. = '')][1]
        let $dsed     := ($representingTemplate[self::representingTemplate]/@sourceDatasetFlexibility,              $representingTemplate[self::dataset]/@effectiveDate)[not(. = '')][1]
        let $tmid     := ($representingTemplate[self::representingTemplate]/@ref,                                   $representingTemplate[self::dataset]/@templateId)[not(. = '')][1]
        let $tmed     := ($representingTemplate[self::representingTemplate]/@flexibility,                           $representingTemplate[self::dataset]/@templateEffectiveDate)[not(. = '')][1]
        let $qid      := ($representingTemplate[self::representingTemplate]/@representingQuestionnaire,             $representingTemplate[self::dataset]/@questionnaireId)[not(. = '')][1]
        let $qed      := ($representingTemplate[self::representingTemplate]/@representingQuestionnaireFlexibility,  $representingTemplate[self::dataset]/@questionnaireEffectiveDate)[not(. = '')][1]
        let $dataset  := if ($dsid) then utillib:getDataset($dsid, $dsed) else ()
        
        let $check    := 
            if ($dsid and empty($dataset)) then
                error($errors:BAD_REQUEST, 'RepresentingTemplate refers to non-existent dataset id ''' || $dsid || ''' effectiveDate ''' || $dsed)
            else ()
        
        (:KH/AH 2013-02-21 @displayName is deprecated, so don't save that... Just save known valid attributes:)
        (:AH 2015-04-26 @sourceDatasetFlexibility new:)
        (:AH 2023-08-22 @representingQuestionnaire(Flexibilty) new:)
        return (
            if ($dsid) then (
                attribute sourceDataset {$dsid},
                if ($dsed) then attribute sourceDatasetFlexibility {$dsed} else ()
            ) else ()
            ,
            if ($tmid) then (
                attribute ref {$tmid},
                if ($tmed) then attribute flexibility {$tmed} else ()
            ) else ()
            ,
            if ($qid) then (
                attribute representingQuestionnaire {$qid},
                if ($qed) then attribute representingQuestionnaireFlexibility {$qed} else ()
            ) else ()
            ,
            for $concept in $representingTemplate/concept[not(@absent = 'true')]
            return
                scapi:prepareRepresentingTemplateConcept($concept, $dataset, $defaultLanguage)
        )
    }
    </representingTemplate>
};

declare %private function scapi:prepareRepresentingTemplateConcept($concept as element(concept), $dataset as element(dataset)?, $defaultLanguage as xs:string) as item()* {
    let $deid               := ($concept/@ref, $concept/@id)[not(. = '')][1]
    let $datasetConcept     := $dataset//concept[@id = $deid]
    let $originalConcept    := utillib:getOriginalForConcept($datasetConcept[1])
    let $deed               := ($datasetConcept/@effectiveDate, $concept/@flexibility, $concept/@effectiveDate)[not(. = '')][1]

    return (
        <concept>
        {
            attribute ref {$deid}
            ,
            if ($deed) then attribute flexibility {$deed} else ()
            ,
            switch ($concept/@conformance)
            case 'NP' return (
                attribute minimumMultiplicity {0}, 
                attribute maximumMultiplicity {0}, 
                attribute conformance {'NP'}
            )
            case 'C' return  (
                $concept/@conformance
            )
            default return   (
                $concept/@minimumMultiplicity[matches(., '^(\?|0|([1-9]\d*))$')], 
                $concept/@maximumMultiplicity[matches(., '^(\?|\*|0|([1-9]\d*))$')], 
                $concept/@conformance[. = ('R', 'C', 'NP')]
            )
            ,
            if ($concept[@conformance = 'M']) then (
                attribute isMandatory {'true'}
            ) 
            else $concept/@isMandatory
            ,
            $concept/@enableBehavior
        }
        {
            comment {concat($originalConcept/@type, ' ', tokenize($deid,'\.')[last()], ' :: ', $originalConcept/name[1])}
        }
        {
            for $node in $concept/context[string-length()>0]
            return
                utillib:parseNode($node)
        }
        {
            if ($concept[@conformance = 'C']) then (
                for $node in $concept/condition
                return
                    scapi:prepareRepresentingTemplateConceptCondition($node, $defaultLanguage)
                ,
                $concept/enableWhen
                ) else ()
            ,
            for $node in $concept/terminologyAssociation[@conceptId]
            return
                scapi:prepareRepresentingTemplateConceptTerminologyAssociation($node)
            ,
            for $node in $concept/identifierAssociation[@conceptId]
            return
                scapi:prepareRepresentingTemplateConceptIdentifierAssociation($node)
        }
        </concept>
        ,
        for $childconcept in $concept/concept[not(@absent = 'true')]
        return
            scapi:prepareRepresentingTemplateConcept($childconcept, $dataset, $defaultLanguage)
    )
};

declare %private function scapi:prepareRepresentingTemplateConceptCondition($condition as element(condition), $defaultLanguage as xs:string) as element() {
    <condition>
    {
        switch ($condition/@conformance)
        case 'NP' return (
            attribute minimumMultiplicity {0}, 
            attribute maximumMultiplicity {0}, 
            attribute conformance {'NP'}
        )
        case 'C' return  (
            (:$condition/@conformance:) (: not logically possible :)
        )
        default return   (
            $condition/@minimumMultiplicity[matches(., '^(\?|0|([1-9]\d*))$')], 
            $condition/@maximumMultiplicity[matches(., '^(\?|\*|0|([1-9]\d*))$')], 
            $condition/@conformance[. = ('R', 'C', 'NP')]
        )
        ,
        if ($condition[@conformance = 'M']) then (
            attribute isMandatory {'true'}
        ) 
        else $condition/@isMandatory
        ,
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($condition/desc)
        ,
        (: back in 'the old days' we did not have a potential multi lingual desc element under condition but just text :)
        if ($condition[*] | $condition[string-length() = 0]) then () else (
            <desc language="{$defaultLanguage}">{data($condition)}</desc>
        )
    }
    </condition>
};

declare %private function scapi:prepareRepresentingTemplateConceptTerminologyAssociation($association as element(terminologyAssociation)) as element() {
    <terminologyAssociation>
    {
        $association/@conceptId[not(. = '')],
        $association/@conceptFlexibility[not(. = '')],
        
        (: a coded concept :)
        $association/@code[not(. = '')],
        $association/@codeSystem[not(. = '')],
        $association/@codeSystemName[not(. = '')],
        $association/@displayName[not(. = '')],
        $association/@equivalence[not(. = '')],
        
        (: a value set and flexibility :)
        $association/@valueSet[not(. = '')],
        $association/@flexibility[not(. = '')],
        $association/@strength[not(. = '')],
        
        (: version handling :)
        $association/@effectiveDate[not(. = '')],
        $association/@expirationDate[not(. = '')],
        $association/@officialReleaseDate[not(. = '')],
        $association/@versionLabel[not(. = '')]
    }
    </terminologyAssociation>
};

declare %private function scapi:prepareRepresentingTemplateConceptIdentifierAssociation($association as element(identifierAssociation)) as element() {
    <identifierAssociation>
    {
        $association/@conceptId[not(. = '')],
        $association/@conceptFlexibility[not(. = '')],
        $association/@ref[not(. = '')],
        
        (: version handling :)
        $association/@effectiveDate[not(. = '')],
        $association/@expirationDate[not(. = '')],
        $association/@officialReleaseDate[not(. = '')],
        $association/@versionLabel[not(. = '')]
    }
    </identifierAssociation>
};

declare %private function scapi:handleTransactionElements($data as element(transaction), $defaultLanguage as xs:string) as element()* {
    (: name :)
    utillib:prepareFreeFormMarkupWithLanguageForUpdate($data/name),
    (: desc :)
    utillib:prepareFreeFormMarkupWithLanguageForUpdate($data/desc),
    (: trigger :)
    for $node in $data/trigger[string-length() gt 0]
    let $node     := if ($node[*]) then $node else utillib:parseNode($node)
    return 
        element {name($node)} {$node/@*[not(.='')], $node/node()}
    ,
    (: condition :)
    utillib:prepareFreeFormMarkupWithLanguageForUpdate($data/condition),
    (: dependencies :)
    utillib:prepareFreeFormMarkupWithLanguageForUpdate($data/dependencies),
    (: new ... 2021-05-21 :)
    (:want at least a @name on publishingAuthority for it to persist:)
    for $authority in $data/publishingAuthority[@name[string-length()>0]]
    let $n := $authority/@name
    group by $n
    return
        utillib:preparePublishingAuthorityForUpdate($authority[1])
    ,
    (: new ... 2021-05-21 :)
    for $node in $data/property[string-length()>0]
    let $node     := if ($node[*]) then $node else utillib:parseNode($node)
    return 
        element {name($node)} {$node/@*[not(.='')], $node/node()}
    ,
    (: new ... 2021-05-21 :)
    utillib:prepareFreeFormMarkupWithLanguageForUpdate($data/copyright)
    ,
    if ($data[@type = 'group'] | $data[transaction]) then () else (
        <actors>
        {
            for $actor in $data/actors/actor[string-length(@id) gt 0]
            return
                <actor>{$actor/@id, $actor/@role}</actor>
        }
        </actors>
        ,
        if ($data[representingTemplate]) then
            <representingTemplate>
            {
                $data/representingTemplate/@sourceDataset[string-length() gt 0],
                $data/representingTemplate/@sourceDatasetFlexibility[string-length() gt 0],
                $data/representingTemplate/@ref[string-length() gt 0],
                $data/representingTemplate/@flexibility[string-length() gt 0],
                $data/representingTemplate/@representingQuestionnaire[string-length() gt 0],
                $data/representingTemplate/@representingQuestionnaireFlexibility[string-length() gt 0]
                ,
                let $dsid   := $data/representingTemplate/@sourceDataset[string-length() gt 0]
                let $dsed   := $data/representingTemplate/@sourceDatasetFlexibility[string-length() gt 0]
                
                return
                if ($dsid) then (
                    let $dataset    := utillib:getDataset($dsid, $dsed)
                    
                    for $concept in $data/representingTemplate/concept[not(@absent = 'true')]
                    return
                        scapi:prepareRepresentingTemplateConcept($concept, $dataset, $defaultLanguage)
                ) else ()
            }
            </representingTemplate>
        else ()
    )
};

declare %private function scapi:mergeDatasets($savedTransaction as element(transaction), $mergeDataset as element(dataset), $conceptId as xs:string?, $conceptEffectiveDate as xs:string?) as element(dataset) {
    if (empty($conceptId)) then (
        (: get full thing :)
        element {name($mergeDataset)} {
            $mergeDataset/(@* except (@transactionId | @transactionEffectiveDate | @templateId | @templateEffectiveDate)),
            attribute transactionId {$savedTransaction/@id},
            attribute transactionEffectiveDate {$savedTransaction/@effectiveDate},
            if ($savedTransaction[representingTemplate/@ref]) then (
                attribute templateId {$savedTransaction/representingTemplate/@ref},
                if ($savedTransaction[representingTemplate/@flexibility]) then
                    attribute templateEffectiveDate {$savedTransaction/representingTemplate/@flexibility}
                else ()
            ) else (),
            $mergeDataset/node()
        }
    )
    else (
        let $savedDataset   := utillib:getDatasetTree((), (), $savedTransaction/@id, $savedTransaction/@effectiveDate, true())
        return
        element {name($mergeDataset)} {
            $mergeDataset/(@* except (@transactionId | @transactionEffectiveDate | @templateId | @templateEffectiveDate)),
            attribute transactionId {$savedTransaction/@id},
            attribute transactionEffectiveDate {$savedTransaction/@effectiveDate},
            if ($savedTransaction[representingTemplate/@ref]) then (
                attribute templateId {$savedTransaction/representingTemplate/@ref},
                if ($savedTransaction[representingTemplate/@flexibility]) then
                    attribute templateEffectiveDate {$savedTransaction/representingTemplate/@flexibility}
                else ()
            ) else (),
            for $concept in $savedDataset/node()
            return
                scapi:mergeConcepts($concept, $mergeDataset//concept[@id = $conceptId][@effectiveDate = $conceptEffectiveDate])
        }
    )
};

declare %private function scapi:mergeConcepts($concept as item(), $mergeConcept as element(concept)) as item() {
    if ($concept[@id = $mergeConcept/@id][@effectiveDate = $mergeConcept/@effectiveDate]) then
        $mergeConcept
    else
    if ($concept/descendant::concept[@id = $mergeConcept/@id][@effectiveDate = $mergeConcept/@effectiveDate]) then (
        element concept {
            $concept/(@* except @absent),
            for $subconcept in $concept/node()
            return
                scapi:mergeConcepts($subconcept, $mergeConcept)
        }
    )
    else ($concept)
};


(:
    Smart clone a scenario transaction representingTemplate based on a new dataset version

    Example from PRSB
    Personalised Care and Support Plan
    ----------------------------------
    Personalised Care and Support Plan has a previous scenario 2.16.840.1.113883.3.1937.777.28.4.208/2021-01-08T14:21:57
    based on an older sourceDataset.
    It shall be cloned as a new transaction but no longer based on the old dataset but on a new dataset that is a natural version
    of the old dataset. The new dataset is dataset 2.16.840.1.113883.3.1937.777.28.1.223/2024-01-24T12:52:29.

    Example from AKTIN Notaufnahmeregister
    Notaufnahmeregister has a previous scenario 2.16.840.1.113883.2.6.60.3.4.33/2019-12-05T08:17:05
    based on an older sourceDataset.
    It shall be cloned as a new transaction but no longer based on the old dataset but on a new dataset that is a natural version
    of the old dataset. The new dataset is dataset 2.16.840.1.113883.2.6.60.3.1.9/2023-11-20T00:00:00.

    So to date (Feb 2024), the call is as follows:
    let $oldscenario := utillib:getTransaction($oldscid, $oldsced, (), ())
    let $clone := local:smartUpdateRepresentingTemplate($oldscenario/representingTemplate, $newdsid, $newdsed, 'en-US')

    The (intermediate) result is returned as
    <result check="{$check}" failed="{count($reptempwithduplicates/fail)}" concepts="{count($reptemp/concept)}">
        check=number of concepts checked, fail=number of unrelated concepts where mapping failed, concepts=number of concepts in new clone
        <rawclone>...the raw sceanrio clone for inspection (debug)</rawclone>
        <representingTemplate sourceDataset="{$targetDatasetId}" sourceDatasetFlexibility="{$targetDatasetFlexibility}">
            ... the list of concepts from the old sceanrio now referencing the new dataset, 
            with all properties copied such as conditions, constarints, etc....
        </representingTemplate>
    </result>
:)

(:~ Central logic for clone a scenario transaction representingTemplate based on a new dataset version

@param $authmap                 - required. Map derived from token
@param $id                      - required. DECOR transaction/@id to update the contained representingTemplate
@param $effectiveDate           - required. DECOR transaction/@effectiveDate to update
@param $newDatasetId            - required. DECOR scenario xml element containing everything that should be in the updated scenario
@param $newDatasetEffectiveDate - required. DECOR scenario xml element containing everything that should be in the updated scenario
@return an intermediate representingTemplate element that might be checked by the user and then be used for an update of the existing scenario transaction representingTemplate.
:)
declare function scapi:smartCloneScenarioRepresentingTemplate($request as map(*)) {
    
    (: let $authmap              := $request?user :)
    let $id                      := $request?parameters?id[string-length() gt 0]
    let $effectiveDate           := $request?parameters?effectiveDate[string-length() gt 0]
    let $newDatasetId            := $request?parameters?newDatasetId[string-length() gt 0]
    let $newDatasetEffectiveDate := $request?parameters?newDatasetEffectiveDate[string-length() gt 0]

    let $storedScenario          := utillib:getTransaction($id, $effectiveDate, (), ())
    
    let $decor                   := $storedScenario/ancestor::decor
    let $projectPrefix           := $decor/project/@prefix
    let $defaultLanguage         := ($storedScenario/ancestor::decor/project/@defaultLanguage, $storedScenario//@language, 'en-US')[1]

    let $check                  :=
        if (empty($storedScenario)) then 
            error($errors:BAD_REQUEST, 'Scenario with id ''' || $id || ''' and effectiveDate ''' || $effectiveDate || ''' does not exist')
        else
        if (count($storedScenario) gt 1) then
            error($errors:SERVER_ERROR, 'Scenario with id ''' || $id || ''' and effectiveDate ''' || $effectiveDate || ''' occurs ' || count($storedScenario) || ' times. Inform your database administrator.')
        else
        if ($storedScenario[@statusCode = $scapi:STATUSCODES-FINAL]) then
            error($errors:BAD_REQUEST, 'Scenario with id ''' || $id || ''' and effectiveDate ''' || $effectiveDate || ''' cannot be updated while it one of status: ' || string-join($scapi:STATUSCODES-FINAL, ', '))
        else ()
    (:
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-SCENARIOS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify scenarios in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    :)
    
    (: check the new dataset :)
    (:
    let $check                  :=
        if ($data[@id = $id] | $data[empty(@id)]) then () else (
            error($errors:BAD_REQUEST, concat('Scenario SHALL have the same id as the scenario id ''', $id, ''' used for updating. Found in request body: ', ($data/@id, 'null')[1]))
        )
    let $check                  :=
        if ($data[@effectiveDate = $effectiveDate] | $data[empty(@effectiveDate)]) then () else (
            error($errors:BAD_REQUEST, concat('Scenario SHALL have the same effectiveDate as the scenario effectiveDate ''', $effectiveDate, ''' used for updating. Found in request body: ', ($data/@effectiveDate, 'null')[1]))
        )
    let $check                  :=
        if ($data[@statusCode = $storedScenario/@statusCode] | $data[empty(@statusCode)]) then () else (
            error($errors:BAD_REQUEST, concat('Scenario SHALL have the same statusCode as the scenario statusCode ''', $storedScenario/@statusCode, ''' used for updating. Found in request body: ', ($data/@statusCode, 'null')[1]))
        )
    :)
               
    let $result := scapi:smartCloneRepresentingTemplate($storedScenario/representingTemplate, $newDatasetId, $newDatasetEffectiveDate, $defaultLanguage)
    
    return $result
};

(:~ Private function for clone a scenario transaction representingTemplate based on a new dataset version

@param $representingTemplate     - required. DECOR transaction to get the updated representingTemplate based on the contained representingTemplate
@param $targetDatasetId          - required. DECOR new target dataset/@id to use as the new base
@param $targetDatasetFlexibility - required. DECOR new target dataset/@effectiveDate to use as the new base
@param $defaultLanguage          - required. DECOR project default language
@return a representingTemplate element that might be checked by the user and then be used for an update of the existing scenario transaction representingTemplate.
:)  
declare %private function scapi:smartCloneRepresentingTemplate(
    $representingTemplate as element(representingTemplate),
    $targetDatasetId as xs:string,
    $targetDatasetFlexibility as xs:string,
    $defaultLanguage as xs:string) as element() {
    
    let $targetDataset  := if ($targetDatasetId) then utillib:getDataset($targetDatasetId, $targetDatasetFlexibility) else ()
    let $check          :=
        if ($targetDatasetId and empty($targetDataset)) then
            error($errors:BAD_REQUEST, 'RepresentingTemplate refers to non-existent new version of a dataset, id ''' || $targetDatasetId || ''' effectiveDate ''' || $targetDatasetFlexibility)
        else ()

    let $sourceDataset  := utillib:getDataset($representingTemplate/@sourceDataset, $representingTemplate/@sourceDatasetFlexibility)
    let $checks         :=
            if (empty($sourceDataset)) then
                'RepresentingTemplate refers to non-existent old version of a dataset, id ''' || $representingTemplate/@sourceDataset || ''' effectiveDate ''' || $representingTemplate/@sourceDatasetFlexibility
            else 'ok'
       
    let $reptempwithduplicates :=
        <representingTemplate>
        {
            for $concept in $representingTemplate/concept
            let $oref            := $concept/@ref
            let $oflx            := $concept/@flexibility
            let $conceptInTarget := $targetDataset//concept[inherit[@ref = $oref]]
            let $originalConcept := utillib:getConcept($oref, $oflx, (), ())
            let $sourceConcept   := 
                if ($originalConcept) then
                    <sourceConcept>
                    {
                        $originalConcept/(@*, name, desc)
                    }
                    </sourceConcept>
                else ()
            return
                if (empty($conceptInTarget))
                then 
                    element failed {
                        $concept/@*,
                        $concept/node(),
                        $sourceConcept
                    }
                else if (empty($conceptInTarget[@statusCode=$scapi:STATUSCODES-NONDEPRECS]))
                then 
                    element invalid {
                        $concept/@*,
                        $concept/node(),
                        $sourceConcept
                    }
                else (
                    for $cit in $conceptInTarget[@statusCode=$scapi:STATUSCODES-NONDEPRECS]
                    return
                        element concept {
                            $concept/(@* except (@ref|@flexibility)),
                            attribute ref { $cit/@id },
                            attribute flexibility { $cit/@effectiveDate },
                            $concept/node(),
                            $sourceConcept
                        },
                    for $xc in $conceptInTarget/ancestor::concept[@statusCode=$scapi:STATUSCODES-NONDEPRECS]
                        let $xcid := $xc/@id
                        let $xced := $xc/@effectiveDate
                        return
                            <concept ref="{$xcid}" flexibility="{$xced}" minimumMultiplicity="0" maximumMultiplicity="*" conformance="R" added2fix="true"/>
                )
        }
        </representingTemplate>
        
    let $reptemp :=
        <representingTemplate>
        {
            for $concept in $reptempwithduplicates/node()[name() = 'concept']
            let $cid       := $concept/@ref
            let $ced       := $concept/@flexibility
            let $cided     := concat($cid, $ced)
            group by $cided
            return 
                if ($concept[not(@added)]) then $concept[not(@added)][1] else $concept[@added][1]
        }
        </representingTemplate>
    
    let $result :=
        <result
            conceptsTransferred="{count($reptemp/concept)}"
            added2fix="{count($reptempwithduplicates/concept[@added2fix='true'])}"
            failed2Map="{count($reptempwithduplicates/failed)}"
            invalidStatus="{count($reptempwithduplicates/invalid)}"
            totalConceptsInSource="{count($representingTemplate/concept)}"
        >
        {
            <nomatches count="{count($reptempwithduplicates/failed)}">
            {
                utillib:addJsonArrayToElements($reptempwithduplicates/failed)
            }
            </nomatches>,
            <representingTemplate sourceDataset="{$targetDataset/@id}" sourceDatasetFlexibility="{$targetDataset/@effectiveDate}">
            {
                $representingTemplate/(@* except (@sourceDataset|@sourceDatasetFlexibility))
            }
            {
                for $concept in $reptemp/node()[name() = 'concept'][not(@absent = 'true')]
                let $prsc := scapi:prepareRepresentingTemplateConcept($concept, $targetDataset, $defaultLanguage)
                return
                    <concept>
                    {
                        $prsc/@*,
                        for $node in $prsc/context[string-length()>0]
                        return
                            utillib:serializeNode($node),
                        $prsc/(* except context)
                    }
                    </concept>
                    
            }
            </representingTemplate>
        }
        </result>
    
    return $result
 
};
