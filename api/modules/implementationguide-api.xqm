xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:~ Project API allows read, create, update, delete of Implementation Guides :)
module namespace igapi              = "http://art-decor.org/ns/api/implementationguide";

import module namespace roaster     = "http://e-editiones.org/roaster";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

import module namespace utillib     = "http://art-decor.org/ns/api/util" at "library/util-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "library/settings-lib.xqm";
import module namespace decorlib    = "http://art-decor.org/ns/api/decor" at "library/decor-lib.xqm";

import module namespace histlib     = "http://art-decor.org/ns/api/history" at "library/history-lib.xqm";

import module namespace comp        = "http://art-decor.org/ns/art-decor-compile" at "../../art/api/api-decor-compile.xqm";

declare namespace xmldb             = "http://exist-db.org/xquery/xmldb";
declare namespace util              = "http://exist-db.org/xquery/util";
declare namespace http              = "http://expath.org/ns/http-client";
declare namespace json              = "http://www.json.org";
declare namespace output            = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace sch               = "http://purl.oclc.org/dsdl/schematron";
declare namespace sm                = "http://exist-db.org/xquery/securitymanager";
declare namespace svrl              = "http://purl.oclc.org/dsdl/svrl";
declare namespace xs                = "http://www.w3.org/2001/XMLSchema";
declare namespace hl7               = "urn:hl7-org:v3";
declare namespace xsi               = "http://www.w3.org/2001/XMLSchema-instance";

(:~ All functions support their own override, but this is the fallback for the maximum number of results returned on a search :)
declare %private variable $igapi:maxResults                             := 50;

(:declare %private variable $igapi:ITEM-STATUS                            := utillib:getDecorTypes()/ItemStatusCodeLifeCycle;:)
declare %private variable $igapi:STATUSCODES-FINAL                      := ('final', 'pending', 'rejected', 'cancelled', 'deprecated');
(:declare %private variable $igapi:PROJECT-COPYRIGHT-TYPE                 := utillib:getDecorTypes()/CopyrightType;:)
(:declare %private variable $igapi:DECOR-OBJECT-TYPES                     := utillib:getDecorTypes()/DecorObjectType;:)
declare %private variable $igapi:ADDRLINE-TYPE                          := utillib:getDecorTypes()/AddressLineType;
declare %private variable $igapi:SUPPORTED-STANDARD-TYPES               := utillib:getDecorTypes()/SupportedStandardTypes;

(: https://developer.mozilla.org/en-US/docs/Web/Media/Formats/Image_types :)
(:declare %private variable $igapi:acceptableMediaTypes                   := ('image/apng', 'image/avif', 'image/gif', 'image/jpeg', 
                                                                            'image/png', 'image/svg+xml', 'image/webp');:)
declare %private variable $igapi:acceptableMediaTypeMap                 :=
    map {
        "image/apng": "apng", 
        "image/avif": "avif", 
        "image/gif": "gif", 
        "image/jpeg": ["jpg", "jpeg", "jfif", "pjpeg", "pjp"], 
        "image/png": "png", 
        "image/svg+xml": "svg", 
        "image/webp": "webp"
    };

(:~ Retrieves implementation guide list

    @param $bearer-token            - optional. provides authorization for the user. The token, if available, should be on the X-Auth-Token HTTP Header
    @param $project                 - required. limits scope to this project only
    @param $release                 - optional. date the project was published
    @param $language                - optional. string parameter to return certain information with. Defaults for project default language
    @return list with zero to many implementation guides
    @since 2022-08-02
:)
declare function igapi:getImplementationGuideList($request as map(*)) {

    let $authmap                := $request?user
    let $projectPrefixOrId      := $request?parameters?project
    let $projectVersion         := $request?parameters?release[string-length() gt 0]
    let $projectLanguage        := $request?parameters?language[string-length() gt 0]
    
    let $check                  :=
        if (empty($projectPrefixOrId)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project (getImplementationGuideList)')
        else ()
    
    let $decor                  := 
        if (utillib:isOid($projectPrefixOrId)) then 
            utillib:getDecorById($projectPrefixOrId, $projectVersion, $projectLanguage) 
        else (
            utillib:getDecorByPrefix($projectPrefixOrId, $projectVersion, $projectLanguage)
        )
    let $projectId              := $decor/project/@id
    
    let $results                :=
        if (count($decor) = 1) then
            if (empty($projectVersion)) then 
                collection($setlib:strDecorDataIG)/implementationGuide[@projectId = $projectId] 
            else (
                collection(util:collection-name($decor))/implementationGuide[@projectId = $projectId]
            )
        else ()
    
    let $count          := count($results)
    let $max            := $count
    let $allcnt         := $count
    
    (: Rewrite all results as if they were all the project, and mark where they came from :)
    return
        <list artifact="IG" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements(
                for $object in $results
                let $compilationDate      := collection(util:collection-name($object))/decor/@versionDate
                let $compilationRequests  := collection($setlib:strDecorScheduledTasks)/compile-request[@implementationguideId = $object/@id][@implementationguideEffectiveDate = $object/@effectiveDate]
                return
                    <implementationGuide>
                    {
                        $object/(@* except compilationDate),
                        if ($compilationDate) then attribute compilationDate {$compilationDate} else (),
                        $object/title,
                        (:could be multiple:)
                        for $r in $compilationRequests order by $r/@on return $r
                    }
                    </implementationGuide>
            )
        }
        </list>
};

(:~ Retrieves implementation guide

    @param $bearer-token            - optional. provides authorization for the user. The token, if available, should be on the X-Auth-Token HTTP Header
    @param $project                 - optional. limits scope to this project only
    @param $release                 - optional. date the project was published
    @param $language                - optional. string parameter to return certain information with. Defaults for project default language
    @param $id                      - required parameter denoting the id of the implementationGuide
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the implementationGuide. If not given assumes latest version for id
    @return implementationGuide
    @since 2020-08-11
:)
declare function igapi:getImplementationGuideLatest($request as map(*)) {
    igapi:getImplementationGuide($request)
};

(:~ Retrieves implementation guide

    @param $bearer-token            - optional. provides authorization for the user. The token, if available, should be on the X-Auth-Token HTTP Header
    @param $project                 - optional. limits scope to this project only
    @return list with zero to many implementation guides
    @since 2022-08-02
:)
declare function igapi:getImplementationGuide($request as map(*)) {

    let $authmap                := $request?user
    let $projectPrefixOrId      := $request?parameters?project
    let $projectVersion         := $request?parameters?release[string-length() gt 0]
    let $projectLanguage        := $request?parameters?language[string-length() gt 0]
    let $id                     := $request?parameters?id[string-length() gt 0]
    let $effectiveDate          := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    
    let $decor                  := 
        if (empty($projectPrefixOrId)) then () else
        if (utillib:isOid($projectPrefixOrId)) then 
            utillib:getDecorById($projectPrefixOrId, $projectVersion, $projectLanguage) 
        else (
            utillib:getDecorByPrefix($projectPrefixOrId, $projectVersion, $projectLanguage)
        )
    let $projectId              := $decor/project/@id
    
    let $results                := igapi:getImplementationGuide($decor, $id, $effectiveDate)
    
    (: Rewrite all results as if they were all the project, and mark where they came from :)
    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, "Found multiple implementation guides for '" || string-join(($id, $effectiveDate), ' / ') || "'. Expected 0..1. Alert your administrator as this should not be possible.")
        )
        else (
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
        )
};

(:~ Retrieves DECOR implementation guide response history based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id
    @param $effectiveDate           - optional parameter denoting the effectiveDate. If not given assumes latest version for id
    @return list
    @since 2022-08-16
:)
declare function igapi:getImplementationGuideHistory($request as map(*)) {
    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $projectPrefix                  := ()
    let $results                        := histlib:ListHistory($authmap, $decorlib:OBJECTTYPE-IMPLEMENTATIONGUIDE, (), $id, $effectiveDate, 0)
    
    return
        <list artifact="{$decorlib:OBJECTTYPE-IMPLEMENTATIONGUIDE}" current="{count($results)}" total="{count($results)}" all="{count($results)}" lastModifiedDate="{current-dateTime()}">
        {
            utillib:addJsonArrayToElements($results)
        }
        </list>
};

(:~ Retrieves implementation guide

    @param $bearer-token            - optional. provides authorization for the user. The token, if available, should be on the X-Auth-Token HTTP Header
    @param $project                 - optional. limits scope to this project only
    @return list with zero to many implementation guides
    @since 2022-08-02
:)
declare function igapi:getImplementationGuideMetadata($request as map(*)) {

    let $implementationguide    := igapi:getImplementationGuide($request)
    let $results                :=
        if ($implementationguide instance of element(implementationGuide)) then 
            element {name($implementationguide)} {
                $implementationguide/@*,
                $implementationguide/(node() except definition),
                (: we return just a count of issues and leave it up to the calling party to get those if required :)
                <issueAssociation count="{count($setlib:colDecorData//issue/object[@id = $implementationguide/@id][@effectiveDate = $implementationguide/@effectiveDate])}"/>
            }
        else ()
    
    (: Rewrite all results as if they were all the project, and mark where they came from :)
    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, "Found multiple implementation guides '" || string-join($implementationguide/(@id, @effectiveDate), ' / ') || "'. Expected 0..1. Alert your administrator as this should not be possible.")
        )
        else (
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
        )
};

(:~ Retrieves implementation guide page tree for an IG

    @param $bearer-token            - optional. provides authorization for the user. The token, if available, should be on the X-Auth-Token HTTP Header
    @param $project                 - required. limits scope to this project only
    @param $id                      - required parameter denoting the id of the implementationGuide
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the implementationGuide. If not given assumes latest version for id
    @return list with zero to many implementation guide resources
    @since 2022-08-02
:)
declare function igapi:getImplementationGuidePageTree($request as map(*)) {

    let $implementationguide    := igapi:getImplementationGuide($request)
    let $results                := 
        if ($implementationguide instance of element(implementationGuide)) then 
            igapi:buildImplementationGuidePageTree($implementationguide/definition/page[1], true()) 
        else ()
    
    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, "Found multiple implementation guide index pages for '" || string-join($implementationguide/(@id, @effectiveDate), ' / ') || "'. Expected 0..1. Alert your administrator as this should not be possible.")
        )
        else (
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
        )
};

declare %private function igapi:buildImplementationGuidePageTree($pages as element(page)*, $mark-index-page as xs:boolean) as element(page)* {
    for $page in $pages
    return
        <page>
        {
            $page/@id, 
            $page/@lastModifiedDate,
            if ($mark-index-page) then attribute { 'indexpage' } { 'true' } else (),
            for $title in $page/title
            return
                utillib:serializeNode($title)
            ,
            igapi:buildImplementationGuidePageTree($page/page, false())
        }
        </page>
};

(:~ Retrieves implementation guide page for an IG

    @param $bearer-token            - optional. provides authorization for the user. The token, if available, should be on the X-Auth-Token HTTP Header
    @param $project                 - required. limits scope to this project only
    @param $id                      - required parameter denoting the id of the implementationGuide
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the implementationGuide. If not given assumes latest version for id
    @return page object
    @since 2022-08-02
:)
declare function igapi:getImplementationGuidePage($request as map(*)) {

    let $pageId                 := $request?parameters?pageId[string-length() gt 0]
    
    let $check                  :=
        if (count($pageId) = 0) then
            error($errors:SERVER_ERROR, 'Parameter pageId is required')
        else
        if (count($pageId) gt 1) then 
            error($errors:SERVER_ERROR, 'Parameter pageId SHALL be used only once')
        else ()
    
    let $implementationguide    := igapi:getImplementationGuide($request)
    let $results                := 
        if ($implementationguide instance of element(implementationGuide)) then 
            $implementationguide/definition//page[@id = $pageId]
        else ()
    
    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, "Found multiple implementation guide index pages for '" || string-join($implementationguide/(@id, @effectiveDate), ' / ') || "'. Expected 0..1. Alert your administrator as this should not be possible.")
        )
        else (
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/(* except page))
                }
        )
};

(:~ Deletes implementation guide page for an IG

    @param $bearer-token            - optional. provides authorization for the user. The token, if available, should be on the X-Auth-Token HTTP Header
    @param $project                 - required. limits scope to this project only
    @param $id                      - required parameter denoting the id of the implementationGuide
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the implementationGuide. If not given assumes latest version for id
    @return nothing
    @since 2022-08-02
:)
declare function igapi:deleteImplementationGuidePage($request as map(*)) {

    let $authmap                := $request?user
    let $projectPrefixOrId      := $request?parameters?project
    let $projectVersion         := ()
    let $projectLanguage        := ()
    let $id                     := $request?parameters?id[string-length() gt 0]
    let $effectiveDate          := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $pageId                 := $request?parameters?pageId[string-length() gt 0]
    
    let $check                  :=
        if (empty($id) or empty($effectiveDate) or empty($pageId)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter id and/or effectiveDate, and/or pageId')
        else ()
    
    let $decor                  := 
        if (empty($projectPrefixOrId)) then () else
        if (utillib:isOid($projectPrefixOrId)) then 
            utillib:getDecorById($projectPrefixOrId, $projectVersion, $projectLanguage) 
        else (
            utillib:getDecorByPrefix($projectPrefixOrId, $projectVersion, $projectLanguage)
        )
    let $projectId              := $decor/project/@id
    let $implementationguide    := igapi:getImplementationGuide($decor, $id, $effectiveDate)
    
    let $check                  :=
        if (count($implementationguide) = 1) then () else (
            error($errors:BAD_REQUEST, 'Implementation guide ' || $id || ' / ' || $effectiveDate || ' does not exist. Found: ' || count($implementationguide))
        )
    
    let $results                := update delete $implementationguide/definition//page[@id = $pageId]
    
    return
        roaster:response(204, ())
};

(:~ Retrieves implementation guide resource list for a 'live IG' i.e. NOT a published IG

    @param $bearer-token            - optional. provides authorization for the user. The token, if available, should be on the X-Auth-Token HTTP Header
    @param $project                 - required. limits scope to this project only
    @param $id                      - required parameter denoting the id of the implementationGuide
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the implementationGuide. If not given assumes latest version for id
    @return list with zero to many implementation guide resources
    @since 2022-08-02
:)
declare function igapi:getImplementationGuideResourceList($request as map(*)) {

    let $implementationguide    := igapi:getImplementationGuide($request)
    let $results                :=
        if ($implementationguide instance of element(implementationGuide)) then
            igapi:buildImplementationGuideResourceList($implementationguide/definition/resource)
        else (
            igapi:buildImplementationGuideResourceList(())
        )
    
    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, 'Found multiple implementation guides "' || string-join($implementationguide/(@id, @effectiveDate), ' / ') || '". Expected 0..1. Alert your administrator as this should not be possible.')
        )
        else (
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/(* except page))
                }
        )
};

declare %private function igapi:buildImplementationGuideResourceList($resources as element(resource)*) as element(list) {
    let $count                  := count($resources)
    let $max                    := $count
    let $allcnt                 := $count
    
    return
        <list artifact="ALL" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements($resources)
        }
        </list>
};

(:~ Update DECOR implementation guide parts. Does not touch any concepts. Expect array of parameter objects, each containing RFC 6902 compliant contents.

{ "op": "[add|remove|replace]", "path": "[/|/displayName|/priority|/type]", "value": "[string|object]" }

where

* op - add (object associations, tracking, event) or remove (object associations only) or replace (displayName, priority, type, tracking, assignment)

* path - / (object, tracking, assignment) or /displayName or /priority or /type

* value - string when the path is not /. object when path is /
@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $id                       - required. the id for the issue to update 
@param $request-body             - required. json body containing array of parameter objects each containing RFC 6902 compliant contents
@return implementation guide
@since 2020-05-03
@see http://tools.ietf.org/html/rfc6902
:)
declare function igapi:patchImplementationGuide($request as map(*)) {

    let $authmap                        := $request?user
    let $igid                           := $request?parameters?id
    let $iged                           := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $projectVersion                 := ()
    let $projectLanguage                := ()
    let $data                           := utillib:getBodyAsXml($request?body, 'parameters', ())
    
    let $returnmode                     := ($request?parameters?returnmode[string-length() gt 0], 'ig')[1]
    let $pageIds                        := distinct-values($data/parameter[@path = '/definition/page']/value/page/@id)
    
    let $check                  :=
        if ($data) then () else (
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        )
    let $check                  :=
        switch ($returnmode)
        case 'page'       return (
            (: return page :)
            if (count($pageIds) = 1) then () else (
                error($errors:BAD_REQUEST, 'Parameter returnmode page SHALL be about patching one page and one page only. Found: ' || count($pageIds))
            )
        )
        case 'ig'         (: return full implementation guide as-is :)
        case 'metadata'   (: return implementation guide metadata which is all except the definition :)
        case 'pages'      (: return page tree :)
        case 'resources'  (: return resource list :)
        return ()
        default return (
            error($errors:BAD_REQUEST, 'Parameter returnmode SHALL be one of ig (default), metadata, pages, or resources')
        )
    
    let $return                 := igapi:patchImplementationGuide($authmap, string($igid), $iged, $data)
    let $results                := igapi:getImplementationGuide($request)
    let $results                := 
        switch ($returnmode)
        case 'ig'         return $results
        case 'metadata'   return for $ig in $results return element {name($ig)} {$ig/@*, $ig/(node() except definition)}
        case 'page'       return for $ig in $results return $ig/definition//page[@id = $pageIds]
        case 'pages'      return for $ig in $results return igapi:buildImplementationGuidePageTree($ig/definition/page, false())
        case 'resources'  return for $ig in $results return igapi:buildImplementationGuideResourceList($ig/definition/resource)
        default return (
            error($errors:BAD_REQUEST, 'Parameter returnmode SHALL be one of ig (default), metadata, pages, or resources')
        )
    
    for $result in $results
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

(:~ Central logic for patching an existing implementation guide

@param $authmap         - required. Map derived from token
@param $id              - required. DECOR implementationGuide/@id to update
@param $effectiveDate   - required. DECOR implementationGuide/@effectiveDate to update
@param $data            - required. DECOR xml element parameter elements each containing RFC 6902 compliant contents
@return implementationGuide object as xml with json:array set on elements
:)
declare function igapi:patchImplementationGuide($authmap as map(*), $id as xs:string, $effectiveDate as xs:string, $data as element(parameters)) as element(implementationGuide) {

    let $storedImplementationGuide  := igapi:getImplementationGuide((), $id, $effectiveDate)
    
    let $decor                  := utillib:getDecorById($storedImplementationGuide/@projectId)
    let $projectPrefix          := $decor/project/@prefix
    
    let $lock                   := decorlib:getLocks($authmap, $id, $effectiveDate, ())
    let $lock                   := if ($lock) then $lock else decorlib:setLock($authmap, $id, $effectiveDate, false())
    
    let $check                  :=
        if ($storedImplementationGuide) then () else (
            error($errors:BAD_REQUEST, 'Implementation guide with id ' || $id || ' and effectiveDate ' || $effectiveDate || ' does not exist')
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-SCENARIOS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify implementation guides in project ', $projectPrefix, '. You have to be an active (scenario) author in the project.'))
        )
    let $check                  :=
        if ($storedImplementationGuide[@statusCode = $igapi:STATUSCODES-FINAL]) then
            if ($data/parameter[@path = '/statusCode'][@op = 'replace'][not(@value = $igapi:STATUSCODES-FINAL)]) then () else
            if ($data[count(parameter) = 1]/parameter[@path = '/statusCode']) then () else (
                error($errors:BAD_REQUEST, concat('Implementation guide cannot be patched while it has one of status: ', string-join($igapi:STATUSCODES-FINAL, ', '), if ($storedImplementationGuide/@statusCode = 'pending') then 'You should switch back to status draft to edit.' else ()))
            )
        else ()
    let $check                  :=
        if ($lock) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have a lock for this implementation guide (anymore). Get a lock first.'))
        )
    let $check                  :=
        if ($data[parameter]) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall be an array of ''parameter''.')
        )
    let $unsupportedops         := $data/parameter[not(@op = ('add', 'replace', 'remove'))]
    let $check                  :=
        if ($unsupportedops) then
            error($errors:BAD_REQUEST, 'Submitted parameters shall have supported ''op'' value. Found ''' || string-join(distinct-values($unsupportedops/@op), ''', ''') || ''', expected ''add'', ''replace'', or ''remove''') 
        else ()
    let $checkn                 := (
        if ($data/parameter[@path = '/title']) then
            if (count($storedImplementationGuide/title) - count($data/parameter[@op = 'remove'][@path = '/title']) + count($data/parameter[@op = ('add', 'replace')][@path = '/title']) ge 1) then () else (
                'A implementation guide SHALL have at least one title. You cannot remove every title.'
            )
        else (),
        if ($data/parameter[@path = '/desc']) then
            if (count($storedImplementationGuide/desc) - count($data/parameter[@op = 'remove'][@path = '/desc']) + count($data/parameter[@op = ('add', 'replace')][@path = '/desc']) ge 1) then () else (
                'A implementation guide SHALL have at least one description. You cannot remove every desc.'
            )
        else ()
    )
    
    let $check                  :=
        for $param in $data/parameter
        let $op         := $param/@op
        let $path       := $param/@path
        let $value      := $param/@value
        let $pathpart   := substring-after($path, '/')
        return
            switch ($path)
            case '/statusCode' return (
                if ($op = 'remove') then
                    'Parameter path ''' || $path || ''' does not support ' || $op
                else 
                if (utillib:isStatusChangeAllowable($storedImplementationGuide, $value)) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Supported are: ' || string-join(map:get($utillib:itemstatusmap, string($storedImplementationGuide/@statusCode)), ', ')
                )
            )
            case '/name' return (
                if ($op = 'remove') then
                    'Parameter path ''' || $path || ''' does not support ' || $op
                else 
                if (string-length($value) gt 0) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with empty value.'
                )
            )
            case '/expirationDate'
            case '/officialReleaseDate'
            return (
                if ($op = 'remove') then () else 
                if ($value castable as xs:dateTime) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL be yyyy-mm-ddThh:mm:ss.'
                )
            )
            case '/canonicalUri'
            case '/versionLabel' return (
                if ($op = 'remove') then () else 
                if ($value[not(. = '')]) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL NOT be empty.'
                )
            )
            case '/experimental' return (
                if ($op = 'remove') then () else 
                if ($value[. castable as xs:boolean]) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL be boolean.'
                )
            )
            case '/standardVersion' return (
                if ($op = 'remove') then () else
                if ($igapi:SUPPORTED-STANDARD-TYPES/enumeration[@value = $value]) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Supported are: ' || string-join($igapi:SUPPORTED-STANDARD-TYPES/enumeration/@value, ', ')
                )
            )
            case '/title' 
            case '/desc'
            case '/copyright' return (
                if ($param[count(value/*[name() = $pathpart]) = 1]) then (
                    if ($param/value/*[name() = $pathpart][@inherited]) then 
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Contents SHALL NOT be marked ''inherited''.'
                    else (),
                    if ($param/value/*[name() = $pathpart][matches(@language, '[a-z]{2}-[A-Z]{2}')]) then 
                        if ($param[@op = 'remove'] | $param/value/*[name() = $pathpart][.//text()[not(normalize-space() = '')]]) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $path || ' not supported. A ' || $pathpart || ' SHALL have contents.'
                        )
                    else (
                        'Parameter ' || $param/@op || ' of path ' || $path || ' not supported. A ' || $pathpart || ' SHALL have a language with pattern ll-CC.'
                    )
                )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $path || ' not supported. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/*[name() = $pathpart]) 
                )
            )
            case '/publishingAuthority' return (
                if ($param[count(value/publishingAuthority) = 1]) then (
                    if ($param/value/publishingAuthority[@inherited]) then 
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Contents SHALL NOT be marked ''inherited''.'
                    else (),
                    if ($param/value/publishingAuthority[@id]) then
                        if ($param/value/publishingAuthority[utillib:isOid(@id)]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || ''' publishingAuthority.id SHALL be an OID is present. Found ' || string-join($param/value/publishingAuthority/@id, ', ')
                        )
                    else (),
                    if ($param/value/publishingAuthority[@name[not(. = '')]]) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || ''' publishingAuthority.name SHALL be a non empty string.'
                    ),
                    let $unsupportedtypes := $param/value/publishingAuthority/addrLine/@type[not(. = $igapi:ADDRLINE-TYPE/enumeration/@value)]
                    return
                        if ($unsupportedtypes) then
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Publishing authority has unsupported addrLine.type value(s) ' || string-join($unsupportedops, ', ') || '. SHALL be one of: ' || string-join($igapi:ADDRLINE-TYPE/enumeration/@value, ', ') || '.'
                        else ()
                ) else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' SHALL have a single publishingAuthority under value. Found ' || count($param/value/publishingAuthority)
                )
            )
            case '/definition/resource' return (
                if (count($param/value/resource) = 1) then (
                    let $node   := igapi:handleImplementationGuideDefinitionResource($param/value/resource, false())
                    let $xsdValidation  := validation:jaxv-report($node, doc($setlib:strDecorSchemaIG))
                    let $xsdError       := 
                        if ($xsdValidation/status = 'valid') then () else (
                            'Implementation guide definition resource is invalid: ' || string-join(for $msg in $xsdValidation/message return (data($msg) || ' (' || $msg/@line || ',' || $msg/@column || ')'), '. ') || serialize($node)
                        )
                    return $xsdError
                )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $path || ' not supported. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/*[name() = $pathpart]) || '.'
                )
            )
            case '/definition/page' return (
                if (count($param/value/page) = 1) then (
                    let $storedPage := $storedImplementationGuide//page[@id = $param/value/page/@id]
                    return
                    if ($storedPage) then
                        if (string($storedPage/@lastModifiedDate) = string($param/value/page/@lastModifiedDate)) then () else (
                            'Parameter not allowed for ''' || $path || '''. Page ' || string-join($param/value/page/@id, ', ') || ' SHALL have the same last modified date as the page-to-be-updated. You might otherwise inadvertently overwrite changes someone did before you. Expected ''' || string($storedPage/@lastModifiedDate) || ''', found: ''' || string($param/value/page/@lastModifiedDate) || '''.'
                        )
                    else (
                        'Parameter not allowed for ''' || $path || '''. Page ' || string-join($param/value/page/@id, ', ') || ' SHALL exist. Add is a patch using path ''/definition/pages'' + op ''replace'' with the page tree including the new page at its intended location.'
                    )
                    ,
                    let $node           := igapi:handleImplementationGuideDefinitionPage($param/value/page, false())
                    let $xsdValidation  := validation:jaxv-report($node, doc($setlib:strDecorSchemaIG))
                    let $xsdError       := 
                        if ($xsdValidation/status = 'valid') then () else (
                            'Implementation guide definition page is invalid: ' || string-join(for $msg in $xsdValidation/message return (data($msg) || ' (' || $msg/@line || ',' || $msg/@column || ')'), '. ') || serialize($node)
                        )
                    return $xsdError
                )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $path || ' not supported. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/*[name() = $pathpart]) || '.'
                )
            )
            case '/definition/pages' return (
                if ($op = 'remove') then
                    'Parameter ' || $op || ' not allowed for ''' || $path || '''. Deleting pages is a patch using path ''/definition/page'' + op ''remove''.'
                else
                if (count($param/value/page) = 1) then (
                    let $newIds         := $param/value//page/@id
                    let $missingPageIds := for $p in $storedImplementationGuide/definition//page/@id return if ($newIds[. = $p]) then () else $p
                    
                    return
                        if (empty($missingPageIds)) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $path || ' not supported. Input SHALL cover all pages. Getting the page tree is /implementationguide/{id}/{effectiveDate}/pages. Deleting pages is a patch using path ''/definition/page'' + op ''remove''. Missing page ids: ' || string-join($missingPageIds, ', ') || '.'
                        )
                    ,
                    let $node           := igapi:mergePageTreeWithStoredPages($param/value/page, $storedImplementationGuide)
                    let $xsdValidation  := validation:jaxv-report($node, doc($setlib:strDecorSchemaIG))
                    let $xsdError       := 
                        if ($xsdValidation/status = 'valid') then () else (
                            'Implementation guide definition page is invalid: ' || string-join(for $msg in $xsdValidation/message return (data($msg) || ' (' || $msg/@line || ',' || $msg/@column || ')'), '. ') || serialize($node)
                        )
                    return $xsdError
                )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $path || ' not supported. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/*[name() = $pathpart]) || '.'
                )
            )
            default return (
                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Path value not supported.'
            )
     let $check                 :=
        if (empty($checkn) and empty($check)) then () else (
            error($errors:BAD_REQUEST,  string-join (($checkn, $check), ' '))
        )
    
    let $intention              := if ($storedImplementationGuide[@statusCode = 'final']) then 'patch' else 'version'
    let $update                 := histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-IMPLEMENTATIONGUIDE, $projectPrefix, $intention, $storedImplementationGuide)
    
    let $update                 :=
        for $param in $data/parameter
        return
            switch ($param/@path)
            case '/name'
            case '/statusCode'
            case '/canonicalUri'
            case '/expirationDate'
            case '/officialReleaseDate'
            case '/versionLabel'
            case '/experimental'
            case '/standardVersion' return (
                let $attname  := substring-after($param/@path, '/')
                let $new      := attribute {$attname} {$param/@value}
                let $stored   := $storedImplementationGuide/@*[name() = $attname]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedImplementationGuide
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/title' 
            case '/desc'
            case '/copyright' return (
                (: only one per language :)
                let $elmname  := substring-after($param/@path, '/')
                let $new      := utillib:prepareFreeFormMarkupWithLanguageForUpdate($param/value/*[name() = $elmname])
                let $stored   := $storedImplementationGuide/*[name() = $elmname][@language = $param/value/*[name() = $elmname]/@language]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedImplementationGuide
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/publishingAuthority' return (
                (: only one possible :)
                let $new      := utillib:preparePublishingAuthorityForUpdate($param/value/publishingAuthority)
                let $stored   := $storedImplementationGuide/publishingAuthority
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedImplementationGuide
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/definition/resource' return (
                let $new      := igapi:handleImplementationGuideDefinitionResource($param/value/resource, true())
                let $stored   := $storedImplementationGuide//resource[@id = $new/@id]
                let $stored   := if ($new[@effectiveDate]) then $stored[@effectiveDate = $new/@effectiveDate] else $stored
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return 
                    if ($stored) then update replace $stored with $new else 
                    if ($storedImplementationGuide/definition) then
                        if ($storedImplementationGuide/definition[page]) then
                            update insert $new preceding $storedImplementationGuide/definition/page
                        else (
                            update insert $new into $storedImplementationGuide/definition
                        )
                    else (
                        update insert <definition>{$new}</definition> into $storedImplementationGuide
                    )
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/definition/page' return (
                let $stored   := $storedImplementationGuide//page[@id = $param/value/page/@id]
                let $new      := <page>{$param/value/page/@id, attribute lastModifiedDate {substring(string(current-dateTime()), 1, 19)}, $param/value/page/title, $param/value/page/content, $stored/page}</page>
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return 
                    if ($stored) then update replace $stored with $new else (
                        if ($storedImplementationGuide/definition) then
                            update insert $new into $storedImplementationGuide/definition
                        else (
                            update insert <definition>{$new}</definition> into $storedImplementationGuide
                        )
                    )
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/definition/pages' return (
                let $new      := igapi:mergePageTreeWithStoredPages($param/value/page, $storedImplementationGuide)
                let $stored   := $storedImplementationGuide/definition/page
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return 
                    if ($stored) then update replace $stored with $new else (
                        if ($storedImplementationGuide/definition) then
                            update insert $new into $storedImplementationGuide/definition
                        else (
                            update insert <definition>{$new}</definition> into $storedImplementationGuide
                        )
                    )
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            default return ( (: unknown path :) )
    
    (: after all the updates, the XML Schema order is likely off. Restore order :)
    let $preparedImplementationGuide  := igapi:handleImplementationGuide($storedImplementationGuide)
    
    let $update                 := xmldb:store(util:collection-name($storedImplementationGuide), util:document-name($storedImplementationGuide), $preparedImplementationGuide)
    let $update                 := update delete $lock
    
    return
        $preparedImplementationGuide
};

(:~ Merges the page tree with id and title and optionally content in case of new pages, with the stored pages by getting the contents from them if available :)
declare %private function igapi:mergePageTreeWithStoredPages($pages as element(page)*, $storedImplementationGuide as element(implementationGuide)) as element(page)* {
    for $page in $pages
    let $storedPage := $storedImplementationGuide//page[@id = $page/@id]
    return
        <page>
        {
            if ($storedPage) then (
                $page/@id,
                attribute lastModifiedDate {($page/@lastModifiedDate, substring(string(current-dateTime()), 1, 19))[1]},
                $page/title,
                $storedPage/content
            )
            else ( 
                attribute id {util:uuid()},
                attribute lastModifiedDate {substring(string(current-dateTime()), 1, 19)},
                $page/title,
                $page/content
            )
            ,
            igapi:mergePageTreeWithStoredPages($page/page, $storedImplementationGuide)
        }
        </page>
};

(: ====================  COMPILATION  ===================== :)
(:~ Create DECOR implementation guide

    @param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
    @param $project                  - required. path part which may be its prefix or its oid
    @param $request-body             - required. json body containing the updated release/version
    @return implementation guide list
    @since 2022-08-02
:)
declare function igapi:postImplementationGuide($request as map(*)) {

    let $authmap                := $request?user
    let $projectPrefixOrId      := $request?parameters?project[string-length() gt 0]
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                  :=
        if (empty($projectPrefixOrId)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project (postImplementationGuide)')
        else ()
    let $check                  :=
        if (empty($request?body)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $data                   := utillib:getBodyAsXml($request?body, 'decor-parameters', ()) 
    
    let $forceRecompile         := true()
    
    let $decor                  := if (utillib:isOid($projectPrefixOrId)) then utillib:getDecorById($projectPrefixOrId) else utillib:getDecorByPrefix($projectPrefixOrId)
    let $projectId              := $decor/project/@id
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $projectPrefixOrId, ''', does not exist.'))
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-PROJECT)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to create a runtime version of project ', $projectPrefixOrId, '. You have to be an active (scenario) author in the project.'))
        )
        
    let $defaultBaseId          := decorlib:getDefaultBaseIdsP($decor, $decorlib:OBJECTTYPE-IMPLEMENTATIONGUIDE)
    let $check                  :=
        if (empty($defaultBaseId)) then
            error($errors:BAD_REQUEST, 'Project with prefix or id ''' || $projectPrefixOrId || ''', is missing a default base id for implementation guides (type ' || $decorlib:OBJECTTYPE-IMPLEMENTATIONGUIDE || '). Please add this first. Suggested oid branch is .29.')
        else ()
    let $newId                  := decorlib:getNextAvailableIdP($decor, $decorlib:OBJECTTYPE-IMPLEMENTATIONGUIDE, $defaultBaseId/@id)/@id
    
    let $now                    := substring(xs:string(current-dateTime()), 1, 19)
    let $stamp                  := util:uuid()
    let $author                 := $decor/project/author[@username = $authmap?name]
    let $author                 := if (empty($author)) then $authmap?name else $author
    
    let $check                  :=
        if (count($projectPrefix) = 1) then () else (
            error($errors:BAD_REQUEST, 'Project parameter ' || $projectPrefixOrId || ' SHALL refer to exactly one project. Found: ' || count($decor))
        )
    
    let $targetDir              := xmldb:create-collection($setlib:strDecorDataIG, replace($projectPrefix, '-$', ''))
    let $targetDir              := xmldb:create-collection($targetDir, 'ig-' || replace($now, '\D', ''))
    let $igname                 := concat($projectPrefix, replace($now, '\D', ''), '-implementationguide')
    let $igcontent              :=
        <implementationGuide id="{$newId}" effectiveDate="{$now}" name="{$igname}" statusCode="draft" projectId="{$projectId}">
        {
            for $node in $decor/project/name
            return
                <title>{$node/@language, 'Implementation guide', data($node)}</title>
        }
            <definition>
                <page id="{util:uuid()}" lastModifiedDate="{$now}">
                {
                    for $node in $decor/project/name
                    return
                        <title>{$node/@language, 'Index page'}</title>
                    ,
                    for $node in $decor/project/name
                    return
                        <content>{$node/@language, 'Index page'}</content>
                }
                </page>
            </definition>
        </implementationGuide> 
    
    let $filterset              := comp:prepareFilterSet($data/filters)
    let $filters                := 
        if (comp:isCompilationFilterActive($filterset)) then 
            comp:getFinalCompilationFilters($decor, comp:getCompilationFilters($decor, $filterset, ()))
        else ()
    
    let $compile-request        :=  
        <compile-request uuid="{$stamp}" for="{$projectPrefix}" on="{$now}" as="{$stamp}" by="{$author}" 
            implementationguideId="{$igcontent/@id}" implementationguideEffectiveDate="{$igcontent/@effectiveDate}" 
            forcecompile="true" progress="Added to process queue ..." progress-percentage="0">
        {
            if ($filters) then 
                attribute filterlabel {($filters/@label, $filters/@id)[1]} |
                attribute transactions {$filters/transaction/concat(@ref, '--', @flexibility)}
            else (),
            $filters
        }
        </compile-request>
    
    let $reqname                := concat($projectPrefix, replace($now, '\D', ''), '-request')
    
    let $store                  := xmldb:store($targetDir, $igname || '.xml', $igcontent)
    let $store                  := xmldb:store($targetDir, $reqname || '.xml', $compile-request)
    let $write                  := xmldb:copy-resource($targetDir, $reqname || '.xml', $setlib:strDecorScheduledTasks, $reqname || '.xml')
    let $tt                     := sm:chmod($write, 'rw-rw----')
     
    return
        igapi:getImplementationGuideList($request)
};

(:~ Recompile DECOR project for implementation guide

    @param $bearer-token            - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
    @param $project                 - required. path part which may be its prefix or its oid
    @param $request-body            - required. json body containing the updated release/version
    @param $id                      - required parameter denoting the id of the implementationGuide
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the implementationGuide. If not given assumes latest version for id
    @return implementation guide list
    @since 2022-08-02
:)
declare function igapi:postImplementationGuideRefresh($request as map(*)) {

    let $authmap                := $request?user
    let $projectPrefixOrId      := $request?parameters?project[string-length() gt 0]
    let $id                     := $request?parameters?id[string-length() gt 0]
    let $effectiveDate          := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }

    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    
    let $data                   := utillib:getBodyAsXml($request?body, 'decor-parameters', ()) 
    let $forceRecompile         := true()
    
    let $implementationguide    := igapi:getImplementationGuide((), $id, $effectiveDate)
    
    let $check                  :=
        if (count($implementationguide) = 1) then () else (
            error($errors:BAD_REQUEST, 'Implementation guide ' || $id || ' / ' || $effectiveDate || ' does not exist.')
        )
    
    let $projectPrefixOrId      :=  if (empty($projectPrefixOrId)) then $implementationguide/@projectId else $projectPrefixOrId 
    let $decor                  := 
        if (utillib:isOid($projectPrefixOrId)) then 
            utillib:getDecorById($projectPrefixOrId) 
        else (
            utillib:getDecorByPrefix($projectPrefixOrId)
        )
    let $projectId              := $decor/project/@id
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if ($decor) then () else (
            error($errors:BAD_REQUEST, 'Project with prefix or id ''' || $projectPrefixOrId || ''' does not exist.')
        )
    let $check                  :=
        if (count($implementationguide[@projectId = $projectId]) = 1) then () else (
            error($errors:BAD_REQUEST, 'Implementation guide ' || $id || ' / ' || $effectiveDate || ' does not exist.')
        )
    let $check                  :=
        if (count($projectPrefix) = 1) then () else (
            error($errors:BAD_REQUEST, 'Project parameter ' || $projectPrefixOrId || ' SHALL refer to exactly one project. Found: ' || count($decor))
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-SCENARIOS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to refresh implementation guide resources for ', $projectPrefixOrId, '. You have to be an active (scenario) author in the project.'))
        )
    
    let $now                    := substring(xs:string(current-dateTime()), 1, 19)
    let $stamp                  := util:uuid()
    let $author                 := $decor/project/author[@username = $authmap?name]
    let $author                 := if (empty($author)) then $authmap?name else $author
    
    let $targetDir              := util:collection-name($implementationguide)
    let $currentRequest         := collection($targetDir)/compile-request 
    
    let $filterset              := if ($data) then comp:prepareFilterSet($data/filters) else $currentRequest/filters
    let $filters                := 
        if (comp:isCompilationFilterActive($filterset)) then 
            comp:getFinalCompilationFilters($decor, comp:getCompilationFilters($decor, $filterset, ()))
        else ()
    
    let $compile-request        :=  
        <compile-request uuid="{$stamp}" for="{$projectPrefix}" on="{$now}" as="{$stamp}" by="{$author}" 
            implementationguideId="{$implementationguide/@id}" implementationguideEffectiveDate="{$implementationguide/@effectiveDate}" 
            forcecompile="true" progress="Added to process queue ..." progress-percentage="0">
        {
            if ($filters) then 
                attribute filterlabel {($filters/@label, $filters/@id)[1]} |
                attribute transactions {$filters/transaction/concat(@ref, '--', @flexibility)}
            else (),
            $filters
        }
        </compile-request>
    
    let $delete                 := if ($currentRequest) then xmldb:remove(util:collection-name($currentRequest), util:document-name($currentRequest)) else ()
    
    let $reqname                := $projectPrefix || replace($now, '\D', '') || '-request.xml'
    
    let $store                  := xmldb:store($targetDir, $reqname, $compile-request)
    let $write                  := xmldb:copy-resource($targetDir, $reqname, $setlib:strDecorScheduledTasks, $reqname)
    let $tt                     := sm:chmod($write, 'rw-rw----')
     
    return
        igapi:getImplementationGuideList(map { "parameters": map { "project": $implementationguide/@projectId } })
};

(: ====================  COMPILATION  ===================== :)

declare function igapi:getImplementationGuide($decor as element(decor)*, $id as xs:string?, $effectiveDate as xs:string?) as element(implementationGuide)* {
    let $results                :=
        if (count($decor) = 1) then
            if (empty($decor/@versionDate)) then 
                collection($setlib:strDecorDataIG)/implementationGuide[@projectId = $decor/project/@id][@id = $id]
            else (
                collection(util:collection-name($decor))/implementationGuide[@projectId = $decor/project/@id][@id = $id]
            )
        else (
            collection($setlib:strDecorDataIG)/implementationGuide[@id = $id]
        )
    return
        if ($effectiveDate castable as xs:dateTime) then 
            $results[@effectiveDate = string(max($results/xs:dateTime(@effectiveDate)))]
        else (
            $results[@effectiveDate = $effectiveDate]
        )        
};

declare %private function igapi:handleImplementationGuide($implementationGuide as element(implementationGuide)) as element(implementationGuide) {
    element {name($implementationGuide)} {
        $implementationGuide/@id[not(. = '')],
        $implementationGuide/@effectiveDate[not(. = '')],
        $implementationGuide/@name[not(. = '')],
        $implementationGuide/@statusCode,
        $implementationGuide/@expirationDate[. castable as xs:dateTime],
        $implementationGuide/@officialReleaseDate[. castable as xs:dateTime],
        attribute lastModifiedDate {substring(string(current-dateTime()), 1, 19)},
        $implementationGuide/@versionLabel[not(. = '')],
        $implementationGuide/@canonicalUri[not(. = '')],
        $implementationGuide/@experimental[not(. = '')],
        $implementationGuide/@projectId[not(. = '')],
        $implementationGuide/@standardVersion[not(. = '')],
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($implementationGuide/title),
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($implementationGuide/desc),
        utillib:preparePublishingAuthorityForUpdate($implementationGuide/publishingAuthority),
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($implementationGuide/copyright),
        igapi:handleImplementationGuideDefinition($implementationGuide/definition[1])
    }
};
declare %private function igapi:handleImplementationGuideDefinition($definition as element(definition)?) as element(definition) {
    element {name($definition)} {
        igapi:handleImplementationGuideDefinitionResource($definition/resource, false()),
        igapi:handleImplementationGuideDefinitionPage($definition/page, false())
    }
};
declare %private function igapi:handleImplementationGuideDefinitionResource($resources as element(resource)*, $setLastModified as xs:boolean) as element(resource)* {
    for $resource in $resources
    return
        element {name($resource)} {
            $resource/@generated,
            $resource/@artefact[not(. = '')],
            $resource/@id[not(. = '')],
            $resource/@effectiveDate[not(. = '')],
            (:$resource/@name[not(. = '')],
            $resource/@displayName[not(. = '')],:)
            $resource/@statusCode,
            $resource/@expirationDate[. castable as xs:dateTime],
            $resource/@officialReleaseDate[. castable as xs:dateTime],
            if ($setLastModified) then
                attribute lastModifiedDate {substring(string(current-dateTime()), 1, 19)}
            else (
                $resource/@lastModifiedDate[. castable as xs:dateTime]
            ),
            $resource/@versionLabel[not(. = '')],
            $resource/@canonicalUri[not(. = '')],
            $resource/@experimental[. = ('true', 'false')],
            $resource/@type[not(. = '')],
            $resource/@model[not(. = '')],
            $resource/@label[not(. = '')],
            utillib:prepareFreeFormMarkupWithLanguageForUpdate($resource/name),
            utillib:prepareFreeFormMarkupWithLanguageForUpdate($resource/desc),
            for $example in $resource/example[not(@canonicalUri = '')]
            return
                <example>{$example/@canonicalUri}</example>
        }
};
declare %private function igapi:handleImplementationGuideDefinitionPage($resources as element(page)*, $setLastModified as xs:boolean) as element(page)* {
    for $resource in $resources
    return
        element {name($resource)} {
            $resource/@id[not(. = '')],
            if ($setLastModified) then
                attribute lastModifiedDate {substring(string(current-dateTime()), 1, 19)}
            else (
                $resource/@lastModifiedDate[. castable as xs:dateTime]
            ),
            utillib:prepareFreeFormMarkupWithLanguageForUpdate($resource/title),
            utillib:prepareFreeFormMarkupWithLanguageForUpdate($resource/content),
            igapi:handleImplementationGuideDefinitionPage($resource/page, $setLastModified)
        }
};