xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:~ Dataset concept API allows read, create, update on DECOR concepts in DECOR datasets :)
module namespace deapi              = "http://art-decor.org/ns/api/concept";

import module namespace roaster     = "http://e-editiones.org/roaster";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

import module namespace utillib     = "http://art-decor.org/ns/api/util" at "library/util-lib.xqm";
import module namespace utilhtml    = "http://art-decor.org/ns/api/util-html" at "library/util-html-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "library/settings-lib.xqm";
import module namespace decorlib    = "http://art-decor.org/ns/api/decor" at "library/decor-lib.xqm";
import module namespace histlib     = "http://art-decor.org/ns/api/history" at "library/history-lib.xqm";
import module namespace serverapi   = "http://art-decor.org/ns/api/server" at "server-api.xqm";
import module namespace vsapi       = "http://art-decor.org/ns/api/valueset" at "valueset-api.xqm";
import module namespace tmapi       = "http://art-decor.org/ns/api/template" at "template-api.xqm";
(:import module namespace userapi   = "http://art-decor.org/ns/api/user" at "user-api.xqm";:)

declare namespace json              = "http://www.json.org";

(:~ All functions support their own override, but this is the fallback for the maximum number of results returned on a search :)
declare %private variable $deapi:maxResults           := 50;
declare %private variable $deapi:STATUSCODES-FINAL    := ('final', 'pending', 'rejected', 'cancelled', 'deprecated');
declare %private variable $deapi:STATUSCODES-SKIPS    := ('rejected', 'cancelled');
declare %private variable $deapi:CONCEPT-TYPE         := utillib:getDecorTypes()/DataSetConceptType;
declare %private variable $deapi:RELATIONSHIP-TYPE    := utillib:getDecorTypes()/RelationshipTypes;
declare %private variable $deapi:ITEM-STATUS          := utillib:getDecorTypes()/ItemStatusCodeLifeCycle;
declare %private variable $deapi:VOCAB-TYPE           := utillib:getDecorTypes()/VocabType;
declare %private variable $deapi:VALUEDOMAIN-TYPE     := utillib:getDecorTypes()/DataSetValueType;
declare %private variable $deapi:VALUEDOMAIN-PROPS    := utillib:getDecorTypes()/DataSetValueProperty/attribute;
declare %private variable $deapi:EXAMPLE-TYPE         := utillib:getDecorTypes()/ExampleType;
declare %private variable $deapi:CONFORMANCE-TYPE     := utillib:getDecorTypes()/ConformanceType;
declare %private variable $deapi:CODINGSTRENGTH-TYPE  := utillib:getDecorTypes()/CodingStrengthType;
declare %private variable $deapi:EQUIVALENCY-TYPE     := utillib:getDecorTypes()/EquivalencyType;
 
(:~ local debug 0 or 1 - or 2,... later :)
declare %private variable $deapi:DEBUG := 0;

(:~ Retrieves latest DECOR concept based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                          - required parameter denoting the id of the concept
    @param $effectiveDate               - optional parameter denoting the effectiveDate of the concept. If not given assumes latest version for id
    @param $transactionId               - required parameter denoting the id of the transaction that the concept is in
    @param $transactionEffectiveDate    - optional parameter denoting the effectiveDate of the transaction. If not given assumes latest version for id
    @param $projectVersion              - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @param $projectLanguage             - optional parameter to select from a specific compiled language
    @param $treeonly                    - optional boolean parameter to get the tree structure only if true
    @param $fullTree                    - optional boolean parameter relevant if $treeonly = 'true' to include absent concepts
    @return as-is or as compiled as JSON
    @since 2020-05-03
:)
declare function deapi:getLatestConcept($request as map(*)) {
    deapi:getConcept($request)
};

(:~ Retrieves exact DECOR concept based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
@param $id                          - required parameter denoting the id of the concept
@param $effectiveDate               - required parameter denoting the effectiveDate of the concept. If not given assumes latest version for id
@param $transactionId               - required parameter denoting the id of the transaction that the concept is in
@param $transactionEffectiveDate    - optional parameter denoting the effectiveDate of the transaction. If not given assumes latest version for id
@param $projectVersion              - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
@param $projectLanguage             - optional parameter to select from a specific compiled language
@param $treeonly                    - optional boolean parameter to get the tree structure only if true
@param $fullTree                    - optional boolean parameter relevant if $treeonly = 'true' to include absent concepts
@param $associations                - optional boolean parameter relevant if $treeonly = 'false' to include associations: terminologyAssociation, identifierAssociation
@return as-is or as compiled as JSON
@since 2020-05-03
:)
declare function deapi:getConcept($request as map(*)) {

    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $transactionId                  := $request?parameters?transactionId
    let $transactionEffectiveDate       := $request?parameters?transactionEffectiveDate[string-length() gt 0]
    let $projectVersion                 := $request?parameters?release
    let $projectLanguage                := $request?parameters?language
    let $treeOnly                       := $request?parameters?treeonly = true()
    let $fullTree                       := $request?parameters?fulltree = true()
    let $associationMode                := $request?parameters?associations = true()
    
    let $propertiesMap                  := 
        map:merge((
            for $s in $request?parameters?conceptproperty[string-length() gt 0]
            for $item in tokenize(lower-case($s),'\s') 
            return map:entry($item, true())
        ))
    
    let $results            := deapi:getConcept($authmap, $id, $effectiveDate, $transactionId, $transactionEffectiveDate, $projectVersion, $projectLanguage, $treeOnly, $fullTree, $associationMode, $propertiesMap)

    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple concepts for id '", $id, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        )
        else (
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
        )

};

(:~ Retrieves DECOR dataset concept for publication based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the concept
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the concept. If not given assumes latest version for id
    @param $datasetOrtransactionId  - required parameter denoting the id of the dataset or transaction that the concept is in
    @param datasetOrtransactionE    - optional parameter denoting the effectiveDate of the dataset or transaction. If not given assumes latest version for id
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @param $projectLanguage         - optional parameter to select from a specific compiled language
    @param $format                  - optional. overrides the accept-header for frontend purposes
    @param $download                - optional as xs:boolean. Default: false. 
    @return as-is or as compiled as JSON
    @since 2024-11-08
:)

declare function deapi:getConceptExtract($request as map(*)) {

    let $datasetOrTransactionId         := $request?parameters?transactionId[not(. = '')]
    let $datasetOrTransactionEd         := $request?parameters?transactionEffectiveDate[string-length() gt 0]

    let $projectVersion                 := $request?parameters?release[not(. = '')]
    let $projectLanguage                := $request?parameters?language[not(. = '')]
    let $communityName                  := $request?parameters?community[not(. = '')]
    let $format                         := $request?parameters?format[not(. = '')]
    let $download                       := $request?parameters?download=true()
    
    let $id                             := $request?parameters?id[not(. = '')]
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }

    (: parameter format overrides accept-header as mime-type for using the api in the browser :)
    
    (: 
        this call is used in backend and frontend (browser) - default behaviour mime-types:
        - backend:   default mime-type is json - response payload is json - if no accept type is given
        - frontend:  default mime-type is xml - response payload is xml - if no format is given
    :)
    
    let $acceptTypes                    := roaster:accepted-content-types()
    let $acceptedType                   := ($acceptTypes[. = ('application/xml', 'application/json')],'application/json')[1]
    
    let $format                         := if ($format) then $format else tokenize($acceptedType, '/')[2]

    (: 
       overwrite format in the backend is not always possible because of middleware behaviour
       - if in backend the format is xml and accept is not application/xml, roaster always gives a json payload 
    :) 
    let $check                          :=
        if ($format = 'xml' and not($acceptedType = 'application/xml')) then 
            error($errors:BAD_REQUEST, 'In case of format parameter is xml the accept header should be application/xml')
        else ()   

    let $dataset                        := 
        if ($datasetOrTransactionId) then 
            utillib:getDataset($datasetOrTransactionId, $datasetOrTransactionEd, $projectVersion) | 
            utillib:getTransaction($datasetOrTransactionId, $datasetOrTransactionEd, $projectVersion)
        else utillib:getConcept($id, $effectiveDate, $projectVersion)/ancestor::dataset
    
    let $results                        := 
        if (empty($dataset)) then () else 
 
            let $datasetExtract         := 
                if ($datasetOrTransactionId) then 
                    utillib:getDatasetExtract($dataset, $datasetOrTransactionId, $datasetOrTransactionEd, $id, $effectiveDate,$projectVersion, $projectLanguage, $communityName)
                else utillib:getDatasetExtract($dataset, $id, $effectiveDate, $id, $effectiveDate,$projectVersion, $projectLanguage, $communityName)
            let $latestVersion          := utillib:getDataset($dataset/@id, ())/@effectiveDate = $dataset/@effectiveDate
            let $transactions           := $dataset/ancestor::decor//transaction[representingTemplate[@sourceDataset = $datasetExtract/@id][@sourceDatasetFlexibility = $datasetExtract/@effectiveDate]]
            let $transactions           := 
                if ($latestVersion) then 
                    $transactions | $dataset/ancestor::decor//transaction[representingTemplate[@sourceDataset = $datasetExtract/@id][not(@sourceDatasetFlexibility castable as xs:dateTime)]] 
                else $transactions
                
            return
                utillib:mergeDatasetWithUsage($datasetExtract, $transactions, $dataset/ancestor::decor/project/@prefix)    
        
    let $filePrefix                     := if (exists($results/@transactionId)) then 'TR_' else 'DS_'
    let $filename                       := $filePrefix || $results/@shortName || '_(download_' || substring(fn:string(current-dateTime()),1,19) || ').' || $format
    
    let $results                        :=
        for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/node(), $format)
                }
    
    (: in this case the json format overrides header application/xml and works with a text header :)
    let $results                        :=
        if ($format = 'json' and not($acceptedType = 'application/json')) then
            fn:serialize($results, map{"method": $format , "indent": true()})
            else $results     
           
    let $r-header                       := 
        (response:set-header('Content-Type', 'application/' || $format || '; charset=utf-8'),
        if ($download) then response:set-header('Content-Disposition', 'attachment; filename='|| $filename) else ())
    
    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple concepts for id '", $id, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        )
        else $results        

};

(:~ Retrieves DECOR dataset concept view based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the concept
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the concept. If not given assumes latest version for id
    @param $datasetOrtransactionId  - required parameter denoting the id of the dataset or transaction that the concept is in
    @param datasetOrtransactionE    - optional parameter denoting the effectiveDate of the dataset or transaction. If not given assumes latest version for id
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @param $projectLanguage         - optional parameter to select from a specific compiled language
    @param $ui-language             - optional parameter to select from a specific language for the ui
    @param $hidecolumns             - optional parameter to hide columns in the view based on (hex) number
    @param $format                  - optional. if not given it is html
    @param $download                - optional as xs:boolean. Default: false. 
    @return as-is 
    @since 2024-09-24
:)

declare function deapi:getConceptView($request as map(*)) {

    let $datasetOrTransactionId         := $request?parameters?transactionId[not(. = '')]
    let $datasetOrTransactionEd         := $request?parameters?transactionEffectiveDate[string-length() gt 0]

    let $projectVersion                 := $request?parameters?release[not(. = '')]
    let $projectLanguage                := $request?parameters?language[not(. = '')]
    let $communityName                  := $request?parameters?community[not(. = '')]
    let $ui-lang                        := $request?parameters?ui-language[not(. = '')]
    let $hidecolumns                    := $request?parameters?hidecolumns[not(. = '')]
    let $format                         := $request?parameters?format[not(. = '')]
    let $download                       := $request?parameters?download=true()
    
    let $id                             := $request?parameters?id[not(. = '')]
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }

    let $format                         :=
        if (not($format = 'list')) then 'html' else 'list'
    
    let $dataset                        := 
        if ($datasetOrTransactionId) then 
            utillib:getDataset($datasetOrTransactionId, $datasetOrTransactionEd, $projectVersion) | 
            utillib:getTransaction($datasetOrTransactionId, $datasetOrTransactionEd, $projectVersion)
        else utillib:getConcept($id, $effectiveDate, $projectVersion)/ancestor::dataset

    let $decor                          := $dataset/ancestor::decor[1]

    let $results                        := if (empty($dataset)) then () 
        else if ($datasetOrTransactionId) then 
            utillib:getDatasetExtract($dataset, $datasetOrTransactionId, $datasetOrTransactionEd, $id, $effectiveDate,$projectVersion, $projectLanguage, $communityName)
        else utillib:getDatasetExtract($dataset, $id, $effectiveDate, $id, $effectiveDate,$projectVersion, $projectLanguage, $communityName)

    let $filePrefix                     := if (exists($results/@transactionId)) then 'TR_' else 'DS_'
    let $filename                       := $filePrefix || $results/@shortName || '_(download_' || substring(fn:string(current-dateTime()),1,19) || ').html' 
    let $r-header                       := 
        (response:set-header('Content-Type', 'text/html; charset=utf-8'),
        if ($download) then response:set-header('Content-Disposition', 'attachment; filename='|| $filename) else ())
    
    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, concat("Found multiple datasets for id '", $id, "'. Expected 0..1. Alert your administrator as this should not be possible."))
        )
        else 
        
        (: prepare for Html :)
        let $referenceUrl               := $decor/project/reference[@url castable as xs:anyURI]/@url
        let $projectPrefix              := $decor/project/@prefix
                    
        return 
            if ($format = 'list') 
                then utilhtml:convertTransactionOrDataset2SimpleHtml($results, $projectLanguage, $ui-lang, $hidecolumns, true(), $projectVersion, $referenceUrl, $projectPrefix, false(), $download) 
                else utilhtml:convertTransactionOrDataset2Html($results, $projectLanguage, $ui-lang, $hidecolumns, true(), true(), true(), $projectVersion, $referenceUrl, $projectPrefix, (), false(), $filename, $download)
};

(:~ Creates a valueSet/@ref if one does not exist yet. Creates required BBR links in the project if required to reach that reference.

@return 'ref-and-bbr' - The terminology association points to value set that did not exist yet, so a value set reference was created. A building block repository link with url and ident was also created to make this reference work.
         'ref' - The terminology association points to value set that did not exist yet, so a value set was created. The building block repository link required to make this reference work already existed so it was not created.
:)
declare %private function deapi:addValueSetRef($decor as element(), $association as element()) as xs:string? {
    if ($decor//valueSet[(@id|@ref|@name)=$association/@valueSet]) then () else (
        let $bbrurl             := serverapi:getServerURLServices()
        let $valueSets          := ($setlib:colDecorData//valueSet[(@id|@ref|@name)=$association/@valueSet] | $setlib:colDecorCache//valueSet[(@id|@ref|@name)=$association/@valueSet])
        
        let $valueSets          := 
            for $valueSet in $valueSets
            order by $valueSet/@effectiveDate descending
            return $valueSet
        
        (: build ref element from latest version  we can find :)
        let $valueSetRef        := 
            <valueSet ref="{$valueSets[1]/(@id|@ref)}" name="{$valueSets[1]/@name}" displayName="{$valueSets[1]/(@displayName, @name)[1]}"/>
        return
            if (empty($valueSets)) then (
                let $dummy1 := 
                    if ($decor/terminology/conceptAssociation) then
                        update insert $valueSetRef preceding $decor/terminology/conceptAssociation[1]
                    else (
                        update insert $valueSetRef into $decor/terminology
                    )
                let $dummy2 := 
                    for $repo in $valueSets
                    let $url      := ($repo/ancestor::cacheme/@bbrurl, $bbrurl)[1]
                    let $ident    := $repo/ancestor::decor/project/@prefix
                    group by $url, $ident
                    return
                        if ($decor/project[@url = $url][@ident=$ident][empty(@format)] | $decor/project[@url = $url][@ident=$ident][@format='decor']) then false() else (
                            let $bbrelm := <buildingBlockRepository url="{$bbrurl}" ident="{$ident}"/>
                            let $dummy3 := update insert $bbrelm following $decor/project/(author|reference|restURI|defaultElementNamespace|contact)[last()]
                            
                            return true()
                        )
                        
                return if ($dummy2[. = true()]) then 'ref-and-bbr' else 'ref'
            ) 
            else ()
    )
};

(:~ Retrieves exact DECOR concept usage based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
@param $id                          - required parameter denoting the id of the concept
@param $effectiveDate               - required parameter denoting the effectiveDate of the concept. If not given assumes latest version for id
@param $transactionId               - required parameter denoting the id of the transaction that the concept is in
@param $transactionEffectiveDate    - optional parameter denoting the effectiveDate of the transaction. If not given assumes latest version for id
@param $projectVersion              - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
@param $projectLanguage             - optional parameter to select from a specific compiled language
@return as-is or as compiled as JSON
@since 2020-05-03
:)
declare function deapi:getConceptUsage($request as map(*)) {

    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $transactionId                  := $request?parameters?transactionId
    let $transactionEffectiveDate       := $request?parameters?transactionEffectiveDate[string-length() gt 0]
    let $projectVersion                 := $request?parameters?release
    let $projectLanguage                := $request?parameters?language
    
    return
        deapi:getConceptUsage($authmap, $id, $effectiveDate, $transactionId, $transactionEffectiveDate, $projectVersion, $projectLanguage)
};

(:~ Retrieves DECOR dataset concept history based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id
    @param $effectiveDate           - optional parameter denoting the effectiveDate. If not given assumes latest version for id
    @return list
    @since 2022-08-16
:)
declare function deapi:getConceptHistory($request as map(*)) {
    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $projectPrefix                  := ()
    let $results                        := histlib:ListHistory($authmap, $decorlib:OBJECTTYPE-DATASETCONCEPT, (), $id, $effectiveDate, 0)
    
    return
        <list artifact="{$decorlib:OBJECTTYPE-DATASETCONCEPT}" current="{count($results)}" total="{count($results)}" all="{count($results)}" lastModifiedDate="{current-dateTime()}">
        {
            utillib:addJsonArrayToElements($results)
        }
        </list>
};

(:~ Retrieves an existing dataset concept for edit. This means the user needs to be an editing author of the project, and setting a lock

@param $datasetId               - required. dataset id to insert concept into.
@param $datasetEffectiveDate    - required. dataset effectiveDate to insert concept into.
@param $conceptBaseId           - optional. baseId to create the new concept id out of. Defaults to defaultBaseId for type DE if omitted
@param $conceptType             - required. 'item' or 'group'
@param $insertMode              - required. 'into' or 'preceding' or 'following'. For preceding and for following, $insertRef is required
@param $insertRef               - optional. concept/@id reference for insert. Inserts as new concept in dataset if empty
@param $insertFlexibility       - optional. concept/@effectiveDate reference for insert. Only relevant when two versions of the same concept are in the same dataset which is logically highly unlikely
:)
declare function deapi:getConceptForEdit($request as map(*)) {

    let $authmap                        := $request?user
    let $conceptId                      := $request?parameters?id
    let $conceptEffectiveDate           := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $targetId                       := $request?parameters?targetId
    let $targetEffectiveDate            := $request?parameters?targetEffectiveDate
    let $targetType                     := $request?parameters?targetType
    let $generateConceptListIds         := $request?parameters?generateConceptListIds
    let $breakLock                      := $request?parameters?breakLock = true()
    let $associationMode                := $request?parameters?associations = true()
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    
    return
        deapi:getConceptForEdit($authmap, $conceptId, $conceptEffectiveDate, $targetId, $targetEffectiveDate, $targetType, $generateConceptListIds, $breakLock, $associationMode)
};

(: deapi:getConceptList returns a list of zero or more concepts. Results are always sorted by the match length. Shortest match first and largest match last.
@param $search                  - optional. Sequence of terms to look for. Returns every object if empty.
@param $prefix                  - required. Required DECOR project prefix to search in.
@param $datasetId               - optional. Returns a list concepts connected to a dataset with chosen id. This parameter works in conjunction with $datasetEffectiveDate.
@param $datasetEffectiveDate    - optional. Returns a list concepts connected to an object with this effectiveDate. This parameter works in conjunction with $datasetId.
@param $type                    - optional. Returns all types if empty. Or can choose type: 'item'or 'group'. 
@param $status                  - optional. Returns a list of  most recent status code ("new","draft","pending","final","rejected","cancelled","deprecated") values that results should have. Returns all if empty.
@param $max                     - optional. Maximum number of results to return, defaults to 50.
@param $originalOnly            - required. Returns original concepts (that do not inherit) if true, otherwise returns every hit including concepts that inherit from matching concepts
@param $localConceptsOnly       - required. Returns only concepts within the given prefix
@param $searchPrefix            - optional. Allows selection of results from this project only
:)
declare function deapi:getConceptList($request as map(*)) {
    
    let $searchTerms            := 
        array:flatten(
            for $s in $request?parameters?search[string-length() gt 0]
            return
                tokenize(lower-case($s),'\s')
        )
    
    let $check                  :=
        if (empty($searchTerms)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter search or it is empty')
        else ()
    
    let $projectPrefix          := $request?parameters?prefix
    let $datasetId              := $request?parameters?datasetId[not(. = '')]
    let $datasetEffectiveDate   := $request?parameters?datasetEffectiveDate[not(. = '')]
    let $conceptId              := $request?parameters?conceptId[not(. = '')]
    let $conceptEffectiveDate   := $request?parameters?conceptEffectiveDate[not(. = '')]
    let $conceptType            := $request?parameters?type
    let $statusCodes            := $request?parameters?status
    let $searchPrefix           := () (: $request?parameters?searchPrefix :)
    
    (:mostly interesting when we're called from the similar concepts context in the dataset form when we're creating a new concept:)
    (:we inherit only from original concepts:)
    let $originalOnly           := $request?parameters?originalonly = true()
    
    (:we want to distinguish between local searches and searches that include the current project:)
    let $localConceptsOnly      := $request?parameters?localconceptsonly = true()
    
    let $maxResults             := $request?parameters?max
    let $maxResults             := 
        if ($maxResults castable as xs:integer and xs:integer($maxResults)>0) then (xs:integer($maxResults)) else ()
    
    let $results                :=
        deapi:searchConcept($projectPrefix, $searchTerms, $maxResults, (), (), $datasetId, $datasetEffectiveDate, $conceptId, $conceptEffectiveDate, $conceptType, $statusCodes, $originalOnly, $localConceptsOnly, $searchPrefix)

    for $result in $results
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

(:~ Creates new dataset concept with an empty name in each of the project languages

@param $datasetId               - required. dataset id to insert concept into.
@param $datasetEffectiveDate    - required. dataset effectiveDate to insert concept into.
@param $conceptBaseId           - optional. baseId to create the new concept id out of. Defaults to defaultBaseId for type DE if omitted
@param $conceptType             - required. 'item' or 'group'
@param $insertMode              - required. 'into' or 'preceding' or 'following'. For preceding and for following, $insertRef is required
@param $insertRef               - optional. concept/@id reference for insert. Inserts as new concept in dataset if empty
@param $insertFlexibility       - optional. concept/@effectiveDate reference for insert. Only relevant when two versions of the same concept are in the same dataset which is logically highly unlikely
:)
declare function deapi:postConcept($request as map(*)) {

    let $authmap                        := $request?user
    let $datasetId                      := $request?parameters?id
    let $datasetEffectiveDate           := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $conceptBaseId                  := $request?parameters?baseId
    let $conceptType                    := $request?parameters?conceptType
    let $insertMode                     := $request?parameters?insertMode
    let $insertRef                      := $request?parameters?insertRef
    let $insertFlexibility              := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?insertFlexibility)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?insertFlexibility[string-length() gt 0]
        }
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    
    return
        deapi:createConcept($authmap, $datasetId, $datasetEffectiveDate, $conceptBaseId, $conceptType, $insertMode, $insertRef, $insertFlexibility)
};

(:~ Update concept statusCode, versionLabel, expirationDate and/or officialReleaseDate

@param $authmap required. Map derived from token
@param $id required. DECOR concept/@id to update
@param $effectiveDate required. DECOR concept/@effectiveDate to update
@param $recurse optional as xs:boolean. Default: false. Allows recursion into child particles to apply the same updates
@param $listOnly optional as xs:boolean. Default: false. Allows to test what will happen before actually applying it
@param $newStatusCode optional as xs:string. Default: empty. If empty, does not update the statusCode
@param $newVersionLabel optional as xs:string. Default: empty. If empty, does not update the versionLabel
@param $newExpirationDate optional as xs:string. Default: empty. If empty, does not update the expirationDate 
@param $newOfficialReleaseDate optional as xs:string. Default: empty. If empty, does not update the officialReleaseDate 
@return list object with success and/or error elements or error
:)
declare function deapi:putConceptStatus($request as map(*)) {

    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $recurse                        := $request?parameters?recurse = true()
    let $list                           := $request?parameters?list = true()
    let $statusCode                     := $request?parameters?statusCode
    let $versionLabel                   := $request?parameters?versionLabel
    let $expirationDate                 := $request?parameters?expirationDate
    let $officialReleaseDate            := $request?parameters?officialReleaseDate
    let $associationMode                := $request?parameters?associations = true()
    
    let $check                          :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()

    let $result                         :=
        deapi:setConceptStatus($authmap, $id, $effectiveDate, $recurse, $list, $statusCode, $versionLabel, $expirationDate, $officialReleaseDate)
    
    return
       deapi:getConcept($request)
     
};

(:~ Update dataset concept

@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $id                       - required. the id for the concept to update 
@param $request-body             - required. body containing new concept structure
@return concept structure
@since 2020-05-03
:)
declare function deapi:putConcept($request as map(*)) {

    let $authmap                        := $request?user
    let $deid                           := $request?parameters?id
    let $deed                           := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $trid                           := $request?parameters?transactionId
    let $tred                           := $request?parameters?transactionEffectiveDate
    let $data                           := utillib:getBodyAsXml($request?body, 'concept', ())
    let $associationMode                := $request?parameters?associations = true()
    (:let $s                              := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data):) 
    
    let $check                          :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                          :=
        if (empty($data)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $return                         := 
        if (empty($trid)) then 
            deapi:putConcept($authmap, string($deid), $deed, $data)
        else
            deapi:putTransactionConcept($authmap, string($deid), $deed, $trid, $tred, $data)
    
    return (
        (:roaster:response(200, $return):)
        deapi:getConcept($request)
    )

};

(:~ Update DECOR concept parts. Expect array of parameter objects, each containing RFC 6902 compliant contents. Note: RestXQ does not do PATCH (yet), but that would be the preferred option.

{ "op": "[add|remove|replace]", "path": "[/|/displayName|/priority|/type]", "value": "[string|object]" }

where

* op - add (object associations, tracking, event) or remove (object associations only) or replace (displayName, priority, type, tracking, assignment)

* path - / (object, tracking, assignment) or /displayName or /priority or /type

* value - string when the path is not /. object when path is /
@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $id                       - required. the id for the concept to update 
@param $request-body             - required. json body containing array of parameter objects each containing RFC 6902 compliant contents
@return concept structure
@since 2020-05-03
@see http://tools.ietf.org/html/rfc6902
:)
declare function deapi:patchConcept($request as map(*)) {

    let $authmap                        := $request?user
    let $deid                           := $request?parameters?id
    let $deed                           := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $data                           := utillib:getBodyAsXml($request?body, 'parameters', ())
    let $associationMode                := $request?parameters?associations = true()
    
    (:let $s                      := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data) :)
    
    let $check                  :=
        if ($data) then () else (
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        )
    
    let $return                 := deapi:patchConcept($authmap, string($deid), $deed, $data)
    return (
        (:roaster:response(200, $return):)
        deapi:getConcept($request)
    )

};

declare function deapi:searchConcept($projectPrefix as xs:string?, $searchTerms as xs:string+, $maxResults as xs:integer?, $version as xs:string?, $language as xs:string?, $datasetOrTransactionId as xs:string?, $datasetOrTransactionEffectiveDate as xs:string?, $conceptId as xs:string?, $conceptEffectiveDate as xs:string?, $conceptType as xs:string?, $statusCodes as xs:string*, $originalConceptsOnly as xs:boolean, $localConceptsOnly as xs:boolean, $searchPrefix as xs:string*) as element(result) {
    
    let $contextConcept     := if (empty($conceptId)) then () else utillib:getConcept($conceptId, $conceptEffectiveDate)
    
    let $queryOnId          := if (count($searchTerms)=1 and matches($searchTerms[1],'^\d+(\.\d+)*$')) then true() else false()
    
    let $resultsOnId        := 
        if ($queryOnId) then (
            if ($projectPrefix = '*') then
                if (empty($conceptType)) then
                    $setlib:colDecorData//concept[ends-with(@id, $searchTerms[1])][ancestor::decor[not(@private='true')]][ancestor::datasets][not(ancestor::conceptList | ancestor::history)]
                else (
                    $setlib:colDecorData//concept[ends-with(@id, $searchTerms[1])][ancestor::decor[not(@private='true')]][ancestor::datasets][not(ancestor::conceptList | ancestor::history)][@type = $conceptType]
                )
            else
            if (empty($projectPrefix)) then
                if (empty($conceptType)) then
                    $setlib:colDecorData//concept[ends-with(@id, $searchTerms[1])][ancestor::decor[not(@private='true')][@repository = 'true']][ancestor::datasets][not(ancestor::conceptList | ancestor::history)]
                else (
                    $setlib:colDecorData//concept[ends-with(@id, $searchTerms[1])][ancestor::decor[not(@private='true')][@repository = 'true']][ancestor::datasets][not(ancestor::conceptList | ancestor::history)][@type = $conceptType]
                )
            else
            if (empty($version)) then
                if (empty($conceptType)) then
                    $setlib:colDecorData//concept[ends-with(@id, $searchTerms[1])][ancestor::decor/project[@prefix=$projectPrefix]][ancestor::datasets][not(ancestor::conceptList | ancestor::history)]
                else (
                    $setlib:colDecorData//concept[ends-with(@id, $searchTerms[1])][ancestor::decor/project[@prefix=$projectPrefix]][ancestor::datasets][not(ancestor::conceptList | ancestor::history)][@type = $conceptType]
                )
            else (
                if ($setlib:colDecorVersion/decor[@versionDate = $version][@language = $language]/project[@prefix=$projectPrefix]) then
                    if (empty($conceptType)) then
                        $setlib:colDecorVersion//concept[ends-with(@id, $searchTerms[1])][ancestor::decor[@versionDate = $version][@language = $language]/project[@prefix=$projectPrefix]][ancestor::datasets][not(ancestor::conceptList | ancestor::history)]
                    else (
                        $setlib:colDecorVersion//concept[ends-with(@id, $searchTerms[1])][ancestor::decor[@versionDate = $version][@language = $language]/project[@prefix=$projectPrefix]][ancestor::datasets][not(ancestor::conceptList | ancestor::history)][@type = $conceptType]
                    )
                else (
                    if (empty($conceptType)) then
                        utillib:getDecorByPrefix($projectPrefix, $version, $language)//concept[ends-with(@id, $searchTerms[1])][ancestor::datasets]
                    else (
                        utillib:getDecorByPrefix($projectPrefix, $version, $language)//concept[ends-with(@id, $searchTerms[1])][ancestor::datasets][@type = $conceptType]
                    )
                )
            )
        ) else ()
        
    let $resultsOnName      :=
        if ($queryOnId) then () else (
            let $luceneQuery    := utillib:getSimpleLuceneQuery($searchTerms)
            let $luceneOptions  := utillib:getSimpleLuceneOptions()
            return
            
            if ($projectPrefix = '*') then
                if (empty($conceptType)) then
                    ($setlib:colDecorData//concept[ft:query(name,$luceneQuery)][ancestor::decor[not(@private='true')]][ancestor::datasets][not(ancestor::conceptList | ancestor::history)] |
                     $setlib:colDecorData//concept[ft:query(synonym,$luceneQuery)][ancestor::decor[not(@private='true')]][ancestor::datasets][not(ancestor::conceptList | ancestor::history)])
                else (
                    ($setlib:colDecorData//concept[ft:query(name,$luceneQuery)][ancestor::decor[not(@private='true')]][ancestor::datasets][not(ancestor::conceptList | ancestor::history)][@type = $conceptType] |
                     $setlib:colDecorData//concept[ft:query(synonym,$luceneQuery)][ancestor::decor[not(@private='true')]][ancestor::datasets][not(ancestor::conceptList | ancestor::history)][@type = $conceptType])
                )
            else
            if (empty($projectPrefix)) then
                if (empty($conceptType)) then
                    ($setlib:colDecorData//concept[ft:query(name,$luceneQuery)][ancestor::decor[not(@private='true')][@repository = 'true']][ancestor::datasets][not(ancestor::conceptList | ancestor::history)] |
                     $setlib:colDecorData//concept[ft:query(synonym,$luceneQuery)][ancestor::decor[not(@private='true')][@repository = 'true']][ancestor::datasets][not(ancestor::conceptList | ancestor::history)])
                else (
                    ($setlib:colDecorData//concept[ft:query(name,$luceneQuery)][ancestor::decor[not(@private='true')][@repository = 'true']][ancestor::datasets][not(ancestor::conceptList | ancestor::history)][@type = $conceptType] |
                     $setlib:colDecorData//concept[ft:query(synonym,$luceneQuery)][ancestor::decor[not(@private='true')][@repository = 'true']][ancestor::datasets][not(ancestor::conceptList | ancestor::history)][@type = $conceptType])
                )
            else
            if (empty($version)) then
                if (empty($conceptType)) then
                    ($setlib:colDecorData//concept[ft:query(name,$luceneQuery)][ancestor::decor[not(@private='true')][@repository = 'true'] | ancestor::decor/project[@prefix = $projectPrefix]][ancestor::datasets][not(ancestor::conceptList | ancestor::history)] |
                     $setlib:colDecorData//concept[ft:query(synonym,$luceneQuery)][ancestor::decor[not(@private='true')][@repository = 'true'] | ancestor::decor/project[@prefix = $projectPrefix]][ancestor::datasets][not(ancestor::conceptList | ancestor::history)])
                else (
                    ($setlib:colDecorData//concept[ft:query(name,$luceneQuery)][ancestor::decor[not(@private='true')][@repository = 'true'] | ancestor::decor/project[@prefix = $projectPrefix]][ancestor::datasets][not(ancestor::conceptList | ancestor::history)][@type = $conceptType] |
                     $setlib:colDecorData//concept[ft:query(synonym,$luceneQuery)][ancestor::decor[not(@private='true')][@repository = 'true'] | ancestor::decor/project[@prefix = $projectPrefix]][ancestor::datasets][not(ancestor::conceptList | ancestor::history)][@type = $conceptType])
                )
            else (
                if ($setlib:colDecorVersion/decor[@versionDate = $version][@language = $language]/project[@prefix=$projectPrefix]) then
                    if (empty($conceptType)) then
                        ($setlib:colDecorVersion//concept[ft:query(name,$luceneQuery)][ancestor::decor[@versionDate = $version][@language = $language]/project[@prefix = $projectPrefix]][ancestor::datasets][not(ancestor::conceptList | ancestor::history)] |
                         $setlib:colDecorVersion//concept[ft:query(synonym,$luceneQuery)][ancestor::decor[@versionDate = $version][@language = $language]/project[@prefix = $projectPrefix]][ancestor::datasets][not(ancestor::conceptList | ancestor::history)])
                    else (
                        ($setlib:colDecorVersion//concept[ft:query(name,$luceneQuery)][ancestor::decor[@versionDate = $version][@language = $language]/project[@prefix = $projectPrefix]][ancestor::datasets][not(ancestor::conceptList | ancestor::history)][@type = $conceptType] |
                         $setlib:colDecorVersion//concept[ft:query(synonym,$luceneQuery)][ancestor::decor[@versionDate = $version][@language = $language]/project[@prefix = $projectPrefix]][ancestor::datasets][not(ancestor::conceptList | ancestor::history)][@type = $conceptType])
                    )
                else (
                    if (empty($conceptType)) then
                        (utillib:getDecorByPrefix($projectPrefix, $version, $language)//concept[ft:query(name,$luceneQuery)][ancestor::datasets] |
                         utillib:getDecorByPrefix($projectPrefix, $version, $language)//concept[ft:query(synonym,$luceneQuery)][ancestor::datasets])
                    else (
                        (utillib:getDecorByPrefix($projectPrefix, $version, $language)//concept[ft:query(name,$luceneQuery)][ancestor::datasets][@type = $conceptType] |
                         utillib:getDecorByPrefix($projectPrefix, $version, $language)//concept[ft:query(synonym,$luceneQuery)][ancestor::datasets][@type = $conceptType])
                    )
                )
            )
        )
    
    let $allResults         := 
        if ($originalConceptsOnly) then (
            $resultsOnName[not(inherit | contains)] | $resultsOnId[not(inherit | contains)]
        ) else
        if ($queryOnId) then (
            $resultsOnName | $resultsOnId
        )
        else
        if (empty($version)) then (
            for $concept in ($resultsOnName | $resultsOnId) 
            return deapi:getConceptsThatInheritFromConcept($concept, ())
        )
        else (
            (: everything should already match in compiled projects. No need to re-look-up :)
            $resultsOnName | $resultsOnId
        )
    
    let $transaction        := if ($datasetOrTransactionId) then (utillib:getTransaction($datasetOrTransactionId, $datasetOrTransactionEffectiveDate, $version, $language)) else ()
    let $resultsFiltered    := 
        if ($transaction) then (
            $allResults[@id = $transaction//concept/@ref]
        )
        else 
        if ($datasetOrTransactionId) then (
            if ($datasetOrTransactionEffectiveDate castable as xs:dateTime) then (
                $allResults[ancestor::dataset[@id = $datasetOrTransactionId][@effectiveDate = $datasetOrTransactionEffectiveDate]] |
                $allResults[@datasetId = $datasetOrTransactionId][@datasetEffectiveDate = $datasetOrTransactionEffectiveDate]
            ) else (
                $allResults[ancestor::dataset[@id = $datasetOrTransactionId]] |
                $allResults[@datasetId = $datasetOrTransactionId]
            )
        ) else ($allResults)
    let $resultsFiltered    := 
        if ($localConceptsOnly) then $resultsFiltered[(ancestor::decor/project/@prefix | @ident) = $projectPrefix] else $resultsFiltered
    let $resultsFiltered    :=
        if (empty($statusCodes)) then $resultsFiltered else $resultsFiltered[@statusCode = $statusCodes]
    let $resultsFiltered    :=
        if (empty($searchPrefix)) then $resultsFiltered else $resultsFiltered[(ancestor::decor/project/@prefix | @ident) = $searchPrefix]
    
    (:deduplicate and sort by shortest match first:)
    let $resultsFiltered    :=
        for $concepts in $resultsFiltered
        let $conceptId      := concat($concepts/@id, $concepts/@effectiveDate)
        group by $conceptId
        order by string-length(($concepts/name/text())[1])
        return $concepts[1]
    
    let $resultsFiltered    :=
        for $concepts in $resultsFiltered
        let $conceptLength  := string-length(($concepts/name/text())[1])
        group by $conceptLength 
        order by $conceptLength
        return (
            for $concept in $concepts[ancestor::decor/project[@prefix=$projectPrefix] | @ident[.=$projectPrefix]]
            order by $concept/ancestor::dataset/@id, $concept/@datasetId
            return $concept
            ,
            for $concept in $concepts[not(ancestor::decor/project[@prefix=$projectPrefix] | @ident[.=$projectPrefix])]
            order by $concept/ancestor::dataset/@id, $concept/@datasetId
            return $concept
        )
    
    let $count              := count($resultsFiltered)
    let $maxResults         := if ($maxResults) then $maxResults else $count
    
    return
        <list artifact="DE" current="{if ($count le $maxResults) then $count else $maxResults}" total="{$count}" all="{$count}" lastModifiedDate="{current-dateTime()}" q="{if (request:exists()) then request:get-query-string() else ()}">
        {
            for $concepts in subsequence($resultsFiltered,1,$maxResults)
            let $ancestorProject    := $concepts[1]/ancestor::decor
            let $displayName        := $concepts[1]/name[1]
            order by string-length($displayName)
            return
                element {$concepts[1]/local-name()}
                {
                    attribute uuid {util:uuid()},
                    $concepts[1]/(@* except (@uuid|@conceptId|@datasetId|@datasetEffectiveDate|@datasetStatusCode|@datasetVersionLabel|@datasetExpirationDate|@ident|@repository|@private|@experimental)),
                    attribute conceptId {$concepts[1]/@id},
                    if ($ancestorProject) then (
                        attribute datasetId             {$concepts[1]/ancestor::dataset/@id},
                        attribute datasetEffectiveDate  {$concepts[1]/ancestor::dataset/@effectiveDate},
                        attribute datasetStatusCode     {$concepts[1]/ancestor::dataset/@statusCode},
                        if ($concepts[1]/ancestor::dataset/@versionLabel) then   attribute datasetVersionLabel {$concepts[1]/ancestor::dataset/@versionLabel} else (),
                        if ($concepts[1]/ancestor::dataset/@expirationDate) then attribute datasetExpirationDate {$concepts[1]/ancestor::dataset/@expirationDate} else (),
                        attribute ident                 {$ancestorProject/project/@prefix},
                        attribute repository            {$ancestorProject/@repository='true'},
                        attribute private               {$ancestorProject/@private='true'},
                        attribute experimental          {$ancestorProject/@experimental='true'}
                    )
                    else (
                        $concepts[1]/@datasetId,
                        $concepts[1]/@datasetEffectiveDate,
                        $concepts[1]/@datasetStatusCode,
                        $concepts[1]/@datasetVersionLabel,
                        $concepts[1]/@datasetExpirationDate,
                        $concepts[1]/@ident,
                        $concepts[1]/@repository,
                        $concepts[1]/@private,
                        $concepts[1]/@expiremental
                    ),
                    (: mark search result as being one of the parents or self of context concept :)
                    if ($contextConcept) then 
                        attribute contextParent {exists($contextConcept/ancestor-or-self::concept[@id = $concepts[1]/@id][@effectiveDate = $concepts[1]/@effectiveDate])}
                    else (),
                    $concepts[1]/name,
                    $concepts[1]/inherit,
                    $concepts[1]/contains,
                    if ($ancestorProject) then (
                        for $name in $concepts[1]/ancestor::dataset/name
                        return
                            <datasetName>{$name/@*, $name/node()}</datasetName>
                        ,
                        for $name in $concepts[1]/ancestor::decor/project/name
                        return
                            <projectName>{$name/@*, $name/node()}</projectName>
                    )
                    else (
                        $concepts[1]/datasetName,
                        $concepts[1]/projectName
                    ),
                    
                    let $dbconcept  := $setlib:colDecorData//concept[@id = $concepts[1]/@id][not(ancestor::history)]
                    let $dbconcept  := $dbconcept[@effectiveDate = $concepts[1]/@effectiveDate]
                    let $languages  := $dbconcept/ancestor::decor/project/name/@language
                    
                    for $language in $languages
                    return
                    <path language="{$language}">
                    {
                        for $ancestor in $dbconcept/ancestor::concept
                        let $originalConcept    := utillib:getOriginalForConcept($ancestor)
                        let $conceptName        := if ($originalConcept/name[@language=$language]) then $originalConcept/name[@language=$language] else ($originalConcept/name[1])
                        return
                            concat(($conceptName/text())[1], ' / ')
                    }
                    </path>
                }
        }
        </list>
};

(:~ Internal helper function that recursively finds concepts inheriting from the current concept :)
declare %private function deapi:getConceptsThatInheritFromConcept($concept as element(concept), $results as element(concept)*) as item()* {
    (:
        When you search on id, it might find inherited concepts. 
        This does not happen for search by name as inherited concepts do not have a name
    :)
    let $concept        :=
        if ($concept[not(name)]) then (
            let $originalConcept    := utillib:getOriginalForConcept($concept)
            return
            element {$concept/name()} {
                if ($concept[@ref]) then (
                    $concept/(@ref|@flexibility),
                    $originalConcept/@statusCode
                ) else (
                    $concept/(@id|@effectiveDate|@statusCode)
                ),
                attribute type                  {$originalConcept/@type},
                attribute datasetId             {$concept/ancestor::dataset/@id},
                attribute datasetEffectiveDate  {$concept/ancestor::dataset/@effectiveDate},
                attribute datasetStatusCode     {$concept/ancestor::dataset/@statusCode},
                if ($concept/ancestor::dataset/@versionLabel) then   attribute datasetVersionLabel {$concept/ancestor::dataset/@versionLabel} else (),
                if ($concept/ancestor::dataset/@expirationDate) then attribute datasetExpirationDate {$concept/ancestor::dataset/@expirationDate} else (),
                attribute ident                 {$concept/ancestor::decor/project/@prefix},
                attribute repository            {$concept/ancestor::decor/@repository='true'},
                attribute private               {$concept/ancestor::decor/@private='true'},
                attribute experimental          {$concept/ancestor::decor/project/@experimental='true'},
                $originalConcept/name,
                $concept/inherit,
                $concept/contains,
                for $name in $concept/ancestor::dataset/name
                return
                    <datasetName>{$name/@*, $name/node()}</datasetName>
                ,
                for $name in $concept/ancestor::decor/project/name
                return
                    <projectName>{$name/@*, $name/node()}</projectName>
                
            }
        ) else ($concept)
    let $inheritingConcepts := $setlib:colDecorData//inherit[@ref = $concept/@id][not(ancestor::history)]
    let $inheritingConcepts := $inheritingConcepts[@effectiveDate=$concept/@effectiveDate]/parent::concept
    
    let $containingConcepts := $setlib:colDecorData//contains[@ref = $concept/@id][not(ancestor::history)]
    let $containingConcepts := $containingConcepts[@flexibility = $concept/@effectiveDate]/parent::concept
    
    let $currentResult  :=
        for $currentResultConcept in ($inheritingConcepts | $containingConcepts)
        let $currentResultConceptWithName :=
            element {$currentResultConcept/name()} {
                attribute id                    {$currentResultConcept/@id}, 
                attribute effectiveDate         {$currentResultConcept/@effectiveDate},
                attribute statusCode            {$currentResultConcept/@statusCode},
                attribute type                  {$concept/@type},
                attribute datasetId             {$currentResultConcept/ancestor::dataset/@id},
                attribute datasetEffectiveDate  {$currentResultConcept/ancestor::dataset/@effectiveDate},
                attribute datasetStatusCode     {$concept/ancestor::dataset/@statusCode},
                if ($currentResultConcept/ancestor::dataset/@versionLabel) then   attribute datasetVersionLabel {$currentResultConcept/ancestor::dataset/@versionLabel} else (),
                if ($currentResultConcept/ancestor::dataset/@expirationDate) then attribute datasetExpirationDate {$currentResultConcept/ancestor::dataset/@expirationDate} else (),
                attribute ident                 {$currentResultConcept/ancestor::decor/project/@prefix},
                attribute repository            {$currentResultConcept/ancestor::decor/@repository='true'},
                attribute private               {$currentResultConcept/ancestor::decor/@private='true'},
                attribute experimental          {$currentResultConcept/ancestor::decor/project/@experimental='true'},
                $concept/name,
                $currentResultConcept/inherit | $currentResultConcept/contains,
                for $name in $currentResultConcept/ancestor::dataset/name
                return
                    <datasetName>{$name/@*, $name/node()}</datasetName>,
                for $name in $currentResultConcept/ancestor::decor/project/name
                return
                    <projectName>{$name/@*, $name/node()}</projectName>
            }
        return
            deapi:getConceptsThatInheritFromConcept($currentResultConceptWithName,$results)
    
    return $concept | $results | $currentResult
};

(:~ Retrieve DECOR concept from a transaction or dataset :)
declare function deapi:getConcept($authmap as map(*)?, $id as xs:string, $effectiveDate as xs:string?, $transactionId as xs:string?, $transactionEffectiveDate as xs:string?, $projectVersion as xs:string?, $projectLanguage as xs:string?, $treeonly as xs:boolean?, $fullTree as xs:boolean?, $associationMode as xs:boolean?, $propertiesMap as map(*)?) {
    let $id                         := $id[not(. = '')]
    let $effectiveDate              := $effectiveDate[not(. = '')]
    let $projectVersion             := $projectVersion[not(. = '')]
    let $projectLanguage            := $projectLanguage[not(. = '')]
    let $treeonly                   := $treeonly = true()
    let $fullTree                   := $fullTree = true()
    let $associationMode            := $associationMode = true()
    let $transactionId              := $transactionId[not(. = '')]
    let $transactionEffectiveDate   := $transactionEffectiveDate[not(. = '')]
    
    let $result                     :=
        if ($treeonly) then 
            deapi:getConceptTree($id, $effectiveDate, $transactionId[1], $transactionEffectiveDate[1], $fullTree, $propertiesMap)
        else
        if (empty($transactionId)) then
            utillib:conceptExpanded($authmap, utillib:getConcept($id, $effectiveDate, $projectVersion, $projectLanguage), $associationMode)
        else (
            utillib:conceptExpanded($authmap, utillib:getTransactionConcept($id, $effectiveDate, $transactionId, $transactionEffectiveDate, $projectVersion, $projectLanguage), $associationMode)
        )
    
    return
        $result
};

(:~ Retrieve DECOR concept from a transaction or dataset :)
declare function deapi:getConceptUsage($authmap as map(*)?, $id as xs:string, $effectiveDate as xs:string?, $transactionId as xs:string?, $transactionEffectiveDate as xs:string?, $projectVersion as xs:string?, $projectLanguage as xs:string?) {
    let $id                         := $id[not(. = '')]
    let $effectiveDate              := $effectiveDate[not(. = '')]
    let $transactionId              := $transactionId[not(. = '')]
    let $transactionEffectiveDate   := $transactionEffectiveDate[not(. = '')]
    let $projectVersion             := $projectVersion[not(. = '')]
    let $projectLanguage            := $projectLanguage[not(. = '')]
    
    let $concept                    :=
        if (empty($transactionId)) then
            utillib:getConcept($id, $effectiveDate, $projectVersion, $projectLanguage)
        else (
            utillib:getTransactionConcept($id, $effectiveDate, $transactionId, $transactionEffectiveDate, $projectVersion, $projectLanguage)
        )
    
    let $results                    := utillib:getConceptAssociations($concept, utillib:getOriginalForConcept($concept), true(), 'normal', true())/(* except terminologyAssociation)
    
    let $count                      := count($results)
    let $max                        := $count
    let $allcnt                     := $count
    
    return
        <list artifact="USAGE" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements(subsequence($results, 1, $max))
        }
        </list>
};

declare function deapi:getConceptForEdit($authmap as map(*), $id as xs:string, $effectiveDate as xs:string?, $targetId as xs:string?, $targetEffectiveDate as xs:string?, $targetType as xs:string?, $generateConceptListIds as xs:boolean?, $breakLock as xs:boolean?, $associationMode as xs:boolean?) {
    let $id                     := $id[not(. = '')]
    let $effectiveDate          := $effectiveDate[not(. = '')]
    let $targetId               := $targetId[not(. = '')]
    let $targetEffectiveDate    := $targetEffectiveDate[not(. = '')]
    let $targetType             := $targetType[not(. = '')]
    
    let $concept                := utillib:getConcept($id, $effectiveDate, (), ())
    let $decor                  := $concept/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if ($concept) then () else (
            error($errors:BAD_REQUEST, 'Concept with id ' || $id || ' and effectiveDate ' || $effectiveDate || ', does not exist. Cannot edit non-existent concept.')
        )
    let $check                  :=
        if ($concept/@statusCode = $deapi:STATUSCODES-FINAL) then
            error($errors:BAD_REQUEST, concat('Concept cannot be edited in status ', $concept/@statusCode, '. ', if ($concept/@statusCode = 'pending') then 'You should switch back to status draft to edit.' else ()))
        else ()
    let $lock                   := decorlib:getLocks($authmap, $concept/@id, $concept/@effectiveDate, ())
    let $lock                   := if ($lock) then $lock else decorlib:setLock($authmap, $concept/@id, $concept/@effectiveDate, $breakLock)
    
    let $check                  :=
        if ($lock) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have a lock for this concept (anymore). Get a lock first.'))
        )
    let $targetConcept          := 
        if ($targetType = 'to-designcopy') then if ($concept[contains]) then utillib:getConcept($concept/contains/@ref, $concept/contains/@flexibility, (), ()) else () else
        if ($targetType = 'to-copy')       then if ($concept[inherit]) then utillib:getConcept($concept/inherit/@ref, $concept/inherit/@effectiveDate, (), ()) else 
                                                if ($concept[contains]) then utillib:getConcept($concept/contains/@ref, $concept/contains/@flexibility, (), ()) else ()  else
        if (empty($targetId)) then () else utillib:getConcept($targetId, $targetEffectiveDate, (), ())
    
    let $check                  :=
        if ($targetConcept) then
            if ($targetConcept[@id = $concept/@id][@effectiveDate = $concept/@effectiveDate]) then
                error($errors:BAD_REQUEST, 'Concept SHALL NOT be the same as the target concept.')
            else ()
        else ()
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-DATASETS)) then () else (
            error($errors:FORBIDDEN, 'User ' || $authmap?name || ' does not have sufficient permissions to edit dataset concepts project ' || $projectPrefix || '. You have to be an active author in the project.')
        )
    let $check                  :=
        switch ($targetType)
        (: new inherit :)
        case 'designcopy' return (
            if (empty($targetConcept)) then
                error($errors:BAD_REQUEST, 'Parameter targetType ''' || $targetType || ''' SHALL have a targetId/targetEffectiveDate to an existing concept. Otherwise we cannot know what to target.')
            else
            if ($concept/ancestor-or-self::concept[@id = $targetConcept/@id][@effectiveDate = $targetConcept/@effectiveDate]) then
                error($errors:BAD_REQUEST, 'Target concept for inherit id=''' || $targetConcept/@id || ''' effectiveDate=''' || $targetConcept/@effectiveDate || ''' SHALL NOT point to one of the parents of the source concept (circular reference).')
            else ()
        )
        (: new contains :)
        case 'containment' return (
            if (empty($targetConcept)) then
                error($errors:BAD_REQUEST, 'Parameter targetType ''' || $targetType || ''' SHALL have a targetId/targetEffectiveDate to an existing concept. Otherwise we cannot know what to target.')
            else ()
        )
        (: from contains to inherit :)
        case "to-designcopy" return (
            if (empty($concept/contains/@ref)) then
                error($errors:BAD_REQUEST, 'Parameter targetType ''' || $targetType || ''' SHALL be used on a concept that has a containment. Otherwise we cannot convert anything.')
            else
            if (empty($targetConcept)) then
                error($errors:BAD_REQUEST, 'Parameter targetType ''' || $targetType || ''' SHALL be used on a concept that has a containment we can find. Otherwise we cannot convert anything.')
            else ()
            (:if ($concept/ancestor-or-self::concept[@id = $targetConcept/@id][@effectiveDate = $targetConcept/@effectiveDate]) then
                error($errors:BAD_REQUEST, 'Target concept for containment to inherit id=''' || $targetConcept/@id || ''' effectiveDate=''' || $targetConcept/@effectiveDate || ''' SHALL NOT point to one of the parents of the source concept (circular reference).')
            else ():)
        )
        (: from inherit to relationship :)
        case "to-copy" return (
            if (empty($concept/inherit/@ref)) then
                error($errors:BAD_REQUEST, 'Parameter targetType ''' || $targetType || ''' SHALL be used on a concept that has an inherit/is a design copy. Otherwise we cannot convert anything.')
            else
            if (empty($targetConcept)) then
                error($errors:BAD_REQUEST, 'Parameter targetType ''' || $targetType || ''' SHALL be used on a concept that has an inherit/is a design copy we can find. Otherwise we cannot convert anything.')
            else ()
        )
        default return (
            if (empty($targetConcept)) then () else 
            if (empty($targetType)) then (
                error($errors:BAD_REQUEST, 'Parameter targetType SHALL NOT be empty when a target concept is requested. Expected one of "designcopy", "containment"')
            )
            else (
                error($errors:BAD_REQUEST, 'Parameter targetType ''' || $targetType || ''' is not supported. Expected one of "designcopy", "containment", "to-designcopy", or "to-copy"')
            )
        )
    
    let $userDisplayName        := ($decor/author[@username = $authmap?name], $authmap?name)[1]
    let $language               := $decor/project/@defaultLanguage
    
    let $level                  := 0
    let $baseId                 := concat(string-join(tokenize($concept/@id,'\.')[position()!=last()],'.'),'.')
    
    let $debug := if($deapi:DEBUG > 0) then util:log('INFO', 'getConceptForEdit: update for type ' || $targetType) else ()
    let $debug := if($deapi:DEBUG > 0) then util:log('INFO', 'getConceptForEdit: concept status ' || $concept/@statusCode) else ()
    let $debug := if($deapi:DEBUG > 0) then util:log('INFO', 'getConceptForEdit: target status ' || $targetConcept/@statusCode) else ()

    (: delete our temp counters for concepts and conceptLists :)
    let $delete                 := update delete $concept/ancestor::datasets/@maxcounter
    let $delete                 := update delete $concept/ancestor::datasets/@maxcounter-cl

    (: 
        KH 20241009: handle concepts but skip cancelled or rejected = $deapi:STATUSCODES-SKIPS concepts
        Also dsapi:createDataset en dsapi:inheritConcept handles these skips for other calls
    :)
    let $response               := 
        if ($targetConcept/@statusCode = $deapi:STATUSCODES-SKIPS)
        then () (: skip :)
        else deapi:getConceptForEdit($authmap, $userDisplayName, $decor, $concept, $targetConcept, $targetType, $generateConceptListIds, $breakLock, $level, $baseId, $projectPrefix, $language, $associationMode)
    (: delete our temp counters for concepts and conceptLists :)
    let $delete                 := update delete $concept/ancestor::datasets/@maxcounter
    let $delete                 := update delete $concept/ancestor::datasets/@maxcounter-cl
    
    let $update                 :=
        if ($targetConcept and $targetType = ('designcopy', 'containment', 'to-designcopy')) then (
            (: storing the result one more time ensures that the top level concept is written correctly too. 
                without this we will return more info than we have saved in the project. :)
            if ($response[concept]) then (
                (: first delete the stuff that was saved during deapi:getConceptForEdit() :)
                let $delete         := update delete $concept/concept[@statusCode = 'new'][inherit | contains]
                (: now resave using the intended structure but respect existing concepts from before the inherit. This way we can still cancel :)
                let $insert         := 
                    if ($concept[concept | history]) then 
                        update insert $response/concept preceding ($concept/concept | $concept/history)[1]
                    else (
                        update insert $response/concept into $concept
                    )
                
                let $storedConcept  := utillib:getConcept($response/@id, $response/@effectiveDate)
                (:let $write := xmldb:store($setlib:strDecorTemp, 's1.xml', $storedConcept):)
                let $debug := if($deapi:DEBUG > 0) then util:log('INFO', 'getConceptForEdit: before cleanup ' || count($storedConcept) ) else ()
                let $cleanConcept := deapi:cleanupAfterCopyForEdit($storedConcept)
                (:let $write := xmldb:store($setlib:strDecorTemp, 's2.xml', $cleanConcept):)
                let $debug := if($deapi:DEBUG > 0) then util:log('INFO', 'getConceptForEdit: after cleanup ' || count($cleanConcept) ) else ()
                
                (:
                let $delete         := 
                    for $c in $storedConcept//concept[@statusCode = 'new'][inherit | contains]
                    let $debug := if($deapi:DEBUG > 0) then util:log('INFO', 'getConceptForEdit: updt delete for concept 1 ' || $c/@id) else ()
                    return (
                        (:
                            PROBLEM: THIS CASCADE of individual update delete have enormous costs on large databases
                            PRSB took 20+ minutes to de-conatin or inherit an item of just 270 nodes, alone 10 minutes
                            for the part below. Rewriting it to use a recursive function and then replace the
                            whole concept tree in one update replace action works within a second.
                        :)
                        update delete $c/(@navkey | @type | @iddisplay | @refdisplay | @*[. = '']), 
                        update delete $c/(* except (concept|inherit|contains)),
                        update delete $c/inherit/(@* except (@ref | @effectiveDate)),
                        update delete $c/contains/(@* except (@ref | @flexibility))
                    )
                 :)
                 let $replace := update replace $storedConcept with $cleanConcept
                 
                 let $debug := if($deapi:DEBUG > 0) then util:log('INFO', 'getConceptForEdit: after update replace') else ()
                 
                return ()
            ) else ()
        ) else ()
            
    return
        if (empty($response)) then () else 
        element {name($response)} {
            $response/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($response/*)
        }
};

declare %private function deapi:cleanupAfterCopyForEdit ($c as element()*) as element()* {

let $r :=
    for $ccc in $c
    return
        if ($ccc[@statusCode = 'new'][inherit | contains])
        then
            <concept>
            {
                $ccc/(@* except (@navkey | @type | @iddisplay | @refdisplay | @*[. = ''])),
                for $ii in $ccc/inherit
                return <inherit>{$ii/@ref, $ii/@effectiveDate}</inherit>,
                for $jj in $ccc/contains
                return <contains>{$jj/@ref, $jj/@flexibility}</contains>,
                deapi:cleanupAfterCopyForEdit($ccc/concept)
            }
            </concept>
         else
            (: simply copy node, special handle if a concept with sub concepts :)
            element { $ccc/name() }
            {
                $ccc/@*,
                $ccc/(* except concept),
                deapi:cleanupAfterCopyForEdit($ccc/concept)
            }
            
return $r

};

(:~ Adds properties of the referredConcept to the inherit | contains | relationship element. It also adds properties of the originalConcept to the inherit | contains | relationship, if the the referredConcept is not an originalConcept :)
declare %private function deapi:expandedInheritContainsRelationship($in as element(), $referred as element()?, $original as element()?, $language as xs:string?) as element() {
    element{name($in)}
    {
        $in/@type, $in/@ref, $in/@effectiveDate, $in/@flexibility,
        $referred/ancestor::decor/project/@prefix,
        attribute datasetId {$referred/ancestor-or-self::dataset/@id},
        attribute datasetEffectiveDate {$referred/ancestor-or-self::dataset/@effectiveDate},
        attribute datasetStatusCode {$referred/ancestor-or-self::dataset/@statusCode},
        attribute iType {$original/@type}, 
        attribute iStatusCode {$referred/@statusCode}, 
        attribute iEffectiveDate {$referred/@effectiveDate},
        if ($referred/@expirationDate) then attribute iExpirationDate {$referred/@expirationDate} else (),
        if ($referred/@versionLabel) then attribute iVersionLabel {$referred/@versionLabel} else (),
        attribute refdisplay {utillib:getNameForOID($in/@ref, $language, $referred/ancestor::decor)},
        if ($referred[@id = $original/@id][@effectiveDate = $original/@effectiveDate]) then () else (
            attribute originalId {$original/@id}, 
            attribute originalEffectiveDate {$original/@effectiveDate}, 
            attribute originalStatusCode {$original/@statusCode}, 
            if ($original[@expirationDate]) then attribute originalExpirationDate {$original/@expirationDate} else (),
            if ($original[@versionLabel]) then attribute originalVersionLabel {$original/@versionLabel} else (),
            attribute originalPrefix {$original/ancestor::decor/project/@prefix}
        )
        ,
        if (name($in) = 'relationship') then (
            let $n      := $original/name[@language=$language]
            
            return if ($n) then $n else <name language="{$language}">{$original/name[1]/node()}</name>
        )
        else ()
    }
};

declare %private function deapi:getConceptForEdit($authmap as map(*), $userDisplayName as xs:string, $decor as element(decor), $concept as element(concept), $targetConcept as element(concept)?, $targetType as xs:string?, $generateConceptListIds as xs:boolean?, $breakLock as xs:boolean?, $level as xs:integer, $baseId as xs:string, $projectPrefix as xs:string, $language as xs:string, $associationMode as xs:boolean?) as element(concept) {
    let $projectLanguages       := $decor/project/name/@language
    
    let $editMode               := if ($concept/@statusCode=('new','draft','pending')) then 'edit' else if ($concept/@statusCode='final') then 'move' else ()
    let $deInherit              := $targetType = 'to-copy'
    
    (:usually you inherit from the original, but if you inherit from something that inherits, then these two will differ
        originalConcept has the type and the associations. The rest comes from the inheritConcept
    :)
    let $inheritConcept         := if ($concept[inherit]) then utillib:getConcept($concept/inherit/@ref, $concept/inherit/@effectiveDate) else ()
    let $containConcept         := if ($concept[contains]) then utillib:getConcept($concept/contains/@ref, $concept/contains/@flexibility) else ()
    let $originalConcept        := if (empty($targetConcept)) then utillib:getOriginalForConcept($concept) else utillib:getOriginalForConcept($targetConcept)
    
    let $copyFromConcept        := if ($targetConcept | $concept[inherit | contains]) then $originalConcept else $concept
    (:let $inheritedAssociations  := if ($originalConcept or $generateConceptListIds) then utillib:getConceptAssociations($concept) else ():)
    
    let $datasets               := $decor/datasets
    let $setmaxcounter          := if ($datasets[@maxcounter]) then () else update insert attribute maxcounter {()} into $datasets
    let $setmaxcounter          := if ($datasets[@maxcounter-cl]) then () else update insert attribute maxcounter-cl {()} into $datasets
    
    let $baseIdConceptList      := concat($concept/@id,'.',replace($concept/@effectiveDate,'\D',''))
    let $iddisplay              := utillib:getNameForOID($concept/@id, $language, $decor)
    
    (:
    containment   - create new contains            - use associations from B (target)
    Input:  <concept id="A"> ??? </concept>
          + <target  id="B"> ??? </target>
    Output: <concept id="A"> <contains ref="B"/> </concept>
    
    designcopy    - create new inherit             - use associations from B (target)
    Input:  <concept id="A"> ??? </concept>
          + <target  id="B"> ??? </target>
    Output: <concept id="A"> <inherit ref="B"/> </concept>
    
    to-designcopy - convert contains to inherit    - use associations from A
    Input:  <concept id="A"> <contains ref="B"/> </concept>
          + <target  id="B"> ??? </target>
    Output: <concept id="A"> <inherit ref="B"/> </concept>
    
    to-copy       - convert inherit to new concept - use associations from A
    Input:  <concept id="A"> <inherit ref="B"/> </concept>
          + <target  id="B"> ??? </target>
    Output: <concept id="A"> ??? </concept>
    
    "default"     - leave concept as-is            - use associations from A
    Input:  <concept id="A"> ??? </concept>
          + <target  id="B"> ??? </target>
    Output: <concept id="A"> ??? </concept>
    :)
    
    let $debug                  :=
        if($deapi:DEBUG > 0) then util:log('INFO', 'getConceptForEdit: orig ' || $originalConcept/@id || ' ' || $originalConcept/@statusCode) else ()

    let $associations           :=
        if ($associationMode or $targetType = ('containment','designcopy','to-designcopy','to-copy')) then 
            switch ($targetType)
            case 'containment' 
            case 'designcopy' return utillib:getConceptAssociations($targetConcept, $originalConcept, false(), 'normal', true())
            default           return utillib:getConceptAssociations($concept, $originalConcept, false(), 'normal', true())
        else ()

    let $debug                  :=
        if($deapi:DEBUG > 0) then util:log('INFO', 'getConceptForEdit: to ... -> ' || $concept//@id || ' ' || $concept/@statusCode) else ()
    
    return
        <concept>
        {
            $concept/@id,
            $concept/@effectiveDate,
            $concept/@statusCode,
            $originalConcept/@type,
            $concept/@expirationDate,
            $concept/@officialReleaseDate,
            $concept/@versionLabel,
            $concept/@canonicalUri,
            $concept/@lastModifiedDate,
            if (string-length($iddisplay) = 0) then () else (
                attribute iddisplay {$iddisplay}
            )
        }
        {
            (:  When an item/group is new, we need a pseudo move so upon save we move to either the location it
                already is, or where the user moved it after creation with potential other new items trailing it.
            :)
            if ($concept/@statusCode='new') then (
                <move move="true"/>
            ) else ()
        }
            <edit mode="{$editMode}" deinherit="{$deInherit}"/>
        {
            decorlib:getLocks($authmap, $concept/@id, $concept/@effectiveDate, ())
        }
        {
            ()(:if ($concept[inherit | contains] and $deInherit) then
                (\: create identifier/terminology association for concept if present for inherited concept:\)
                for $association in $inheritedAssociations[@conceptId = $originalConcept/@id]
                return
                    element {name($association)}
                    {
                        attribute conceptId {$concept/@id},
                        attribute conceptFlexibility {$concept/@effectiveDate},
                        attribute effectiveDate {format-dateTime(current-dateTime(), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')},
                        $association/(@*[string-length()>0] except (@conceptId | @conceptFlexibility | @effectiveDate))
                    }
            else ():)
        }
        {
            if ($targetType = ('containment','designcopy')) then 
                $associations/*[@conceptId = ($targetConcept/@id | $targetConcept/contains/@ref | $targetConcept/inherit/@ref)]
            else
            if ($deInherit) then
                (: create identifier/terminology association for concept if present for inherited concept:)
                for $association in $associations/*[@conceptId = ($targetConcept/@id | $targetConcept/contains/@ref | $targetConcept/inherit/@ref)]
                return
                    element {name($association)}
                    {
                        attribute conceptId {$concept/@id},
                        attribute conceptFlexibility {$concept/@effectiveDate},
                        attribute effectiveDate {format-dateTime(current-dateTime(), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')},
                        $association/(@*[string-length()>0] except (@conceptId | @conceptFlexibility | @effectiveDate))
                    }
            else (
                $associations/*[@conceptId = ($concept/@id | $concept/contains/@ref | $concept/inherit/@ref)]
            )
            ,
            (: we return just a count of issues and leave it up to the calling party to get those if required :)
            <issueAssociation count="{count($setlib:colDecorData//issue/object[@id = $concept/@id][@effectiveDate = $concept/@effectiveDate])}"/>       
        }
        {
            if ($targetType = 'containment') then (
                deapi:expandedInheritContainsRelationship(<contains ref="{$targetConcept/@id}" flexibility="{$targetConcept/@effectiveDate}"/>, $targetConcept, $originalConcept, $targetConcept/ancestor::decor/project/@defaultLanguage)
                (:,
                utillib:getConceptAssociations($targetConcept):)
            )
            else
            if ($targetType = 'to-designcopy') then (
                (: for the top level to-designcopy concept we simply want to convert to inherit as request, but we do not want to recurse into circular reference so we 're-contain' those. :)
                if ($concept/ancestor-or-self::concept[@id = $targetConcept/@id] and $level gt 0) then
                    deapi:expandedInheritContainsRelationship(<contains ref="{$targetConcept/@id}" flexibility="{$targetConcept/@effectiveDate}" />, $targetConcept, $originalConcept, $targetConcept/ancestor::decor/project/@defaultLanguage)
                else (
                    deapi:expandedInheritContainsRelationship(<inherit ref="{$targetConcept/@id}" effectiveDate="{$targetConcept/@effectiveDate}"/>, $targetConcept, $originalConcept, $targetConcept/ancestor::decor/project/@defaultLanguage)
                )
                (:,
                utillib:getConceptAssociations($targetConcept):)
            )
            else
            if ($targetType = 'designcopy') then (
                deapi:expandedInheritContainsRelationship(<inherit ref="{$targetConcept/@id}" effectiveDate="{$targetConcept/@effectiveDate}"/>, $targetConcept, $originalConcept, $targetConcept/ancestor::decor/project/@defaultLanguage)
                ,
                for $e in $containConcept/contains | $containConcept/inherit
                return
                    deapi:expandedInheritContainsRelationship($e, $targetConcept, $originalConcept, $targetConcept/ancestor::decor/project/@defaultLanguage)
            )
            else
            if ($targetType = 'to-copy') then (
                for $e in $inheritConcept/contains | $inheritConcept/inherit
                return
                    deapi:expandedInheritContainsRelationship($e, $targetConcept, $originalConcept, $targetConcept/ancestor::decor/project/@defaultLanguage)
            )
            else (
                if ($concept[inherit]) then
                    deapi:expandedInheritContainsRelationship($concept/inherit, $inheritConcept, $originalConcept, $language)
                else ()
                ,
                if ($concept[contains]) then
                    deapi:expandedInheritContainsRelationship($concept/contains, $containConcept, $originalConcept, $language)
                else ()
            )
        }
        {
            for $lang in distinct-values(($projectLanguages, $language, $copyFromConcept/name/@language))
            let $node   := $copyFromConcept/name[@language=$lang][1]
            return 
                if ($node) then utillib:serializeNode($node) else ()
            ,
            for $node in $copyFromConcept/synonym
            return
                utillib:serializeNode($node)
            ,
            for $lang in distinct-values(($projectLanguages, $language, $copyFromConcept/desc/@language))
            let $node   := $copyFromConcept/desc[@language=$lang][1]
            return 
                if ($node) then utillib:serializeNode($node) else ()
            ,
            for $lang in distinct-values(($projectLanguages, $language, $copyFromConcept/source/@language))
            let $node   := $copyFromConcept/source[@language=$lang][1]
            return 
                if ($node) then utillib:serializeNode($node) else ()
            ,
            for $lang in distinct-values(($projectLanguages, $language, $copyFromConcept/rationale/@language))
            let $node   := $copyFromConcept/rationale[@language=$lang][1]
            return 
                if ($node) then utillib:serializeNode($node) else ()
            ,
            if ($concept[inherit | contains]) then
                for $node in $originalConcept/comment
                let $serializedNode := utillib:serializeNode($node)
                return
                    if ($deInherit) then
                        $serializedNode
                    else (
                        <inheritedComment>{$serializedNode/@*, $serializedNode/node()}</inheritedComment>
                    )
            else ()
            ,
            for $node in $concept/comment
            return
                utillib:serializeNode($node)
            ,
            (:new since 2015-04-21:)
            for $property in $copyFromConcept/property
            return
                utillib:serializeNode($property)
        }
        {
            (:new since 2015-04-21:)
            if ($concept[inherit | contains] and $targetType = 'to-copy') then (
                let $referredConcept    := $inheritConcept | $containConcept

                return
                deapi:expandedInheritContainsRelationship(
                    <relationship type="SPEC" ref="{$concept/inherit/@ref | $concept/contains/@ref}" flexibility="{$concept/inherit/@effectiveDate | $concept/contains/@flexibility}"/>,
                    $referredConcept, $originalConcept, $language
                )
            ) else ()
            ,
            for $relationship in $copyFromConcept/relationship
            let $referredConcept            := utillib:getConcept($relationship/@ref, $relationship/@flexibility)
            let $originalReferredConcept    := utillib:getOriginalForConcept($referredConcept)[1]
            return
                deapi:expandedInheritContainsRelationship($relationship, $referredConcept, $originalReferredConcept, $language)
        }
        {
            for $lang in distinct-values(($projectLanguages, $language, $copyFromConcept/operationalization/@language))
            let $node   := $copyFromConcept/operationalization[@language=$lang][1]
            return 
                if ($node) then utillib:serializeNode($node) else ()
        }
        {
            if ((not($concept[inherit | contains]) or $deInherit) and $copyFromConcept[@type='item'][not(valueDomain)]) then
                <valueDomain type="count">
                    <conceptList id="{concat($baseIdConceptList,'.0')}"/>
                </valueDomain>
            else ()
            ,
            let $multipleConceptLists   := count($copyFromConcept[@type='item'][empty(valueDomain/conceptList)] | $copyFromConcept[@type='item']/valueDomain/conceptList) gt 1
            
            for $valueDomain at $vdpos in $copyFromConcept[@type='item']/valueDomain
            return
            <valueDomain type="{$valueDomain/@type}">
            {
                for $property in $valueDomain/property[@*[string-length() gt 0]]
                return
                    <property>{$property/@*[not(. = '')]}</property>
            }
            {
                (: valueDomain/conceptList and valueDomain/conceptList/concept id logic:
                    - parent::concept/@id . parent::concept/@effectiveDate is base
                        [the effectiveDate makes versioned concepts possible. without this timestamp, the 
                            conceptList ids from different concept versions would be the same]
                    - conceptList/@id and conceptList/concept/@id are max new item
                    - for historic reasons first conceptList/@id is '0' (instead of '1')
                :)
                (:
                    Please note that the same numbering logic is applied when adding new conceptList/concepts in the dataset form!
                :)
                let $allConceptListConcepts := ($copyFromConcept/valueDomain/conceptList[@id] | $copyFromConcept/valueDomain/conceptList[@id]/concept[@id])
                return
                if (empty($valueDomain/conceptList)) then (
                    (: This valueDomain does not have a conceptList (yet), maybe due to its type, add a pseudo conceptList :)
                    let $nextclid       := 
                        if ($multipleConceptLists) then (
                            let $newCurrentId       := 
                                if ($datasets[@maxcounter-cl castable as xs:integer]) then $datasets/@maxcounter-cl + 1 else (
                                    decorlib:getNextAvailableIdP($decor, $decorlib:OBJECTTYPE-DATASETCONCEPTLIST, $baseIdConceptList)/@next
                                )
                            let $setmaxcounter      := update value $datasets/@maxcounter-cl with $newCurrentId
                            
                            return concat($baseIdConceptList,'.',$newCurrentId)
                        )
                        else (
                            concat($baseIdConceptList,'.0')
                        )
                    
                    return
                    <conceptList id="{$nextclid}"/>
                )
                else (
                    (: 
                        This valueDomain has conceptList[@id] or conceptList[@ref]. 
                        - conceptList[@id]
                            - Normal case is to copy @id and concepts
                            - Deinherit case generates new ids
                        - conceptList[@ref]
                            - Normal case is to copy @ref and concepts from original
                            - Deinherit case generates new ids
                    :)
                    for $conceptList at $clpos in $valueDomain/conceptList
                    let $doNewIds       := ($deInherit and $generateConceptListIds)
                    let $nextclid       := 
                        if ($doNewIds) then
                            if ($multipleConceptLists) then (
                                let $newCurrentId       := 
                                    if ($datasets[@maxcounter-cl castable as xs:integer]) then $datasets/@maxcounter-cl + 1 else (
                                        decorlib:getNextAvailableIdP($decor, $decorlib:OBJECTTYPE-DATASETCONCEPTLIST, $baseIdConceptList)/@next
                                    )
                                let $setmaxcounter      := update value $datasets/@maxcounter-cl with $newCurrentId
                                
                                return concat($baseIdConceptList,'.',$newCurrentId)
                            )
                            else (
                                concat($baseIdConceptList,'.0')
                            )
                        else ()
                    (: could be @ref :)
                    let $originalConceptList    := utillib:getOriginalConceptList($conceptList)
                    return (
                        <conceptList>
                        {
                            if ($doNewIds) then (
                                attribute id {$nextclid},
                                attribute oldid {$conceptList/(@id|@ref)},
                                (: create terminology association for concept if present for inherited concept:)
                                for $terminologyAssociation in $associations/*[@conceptId = ($conceptList/@id | $conceptList/@ref | $originalConceptList/@id)]
                                return
                                    <terminologyAssociation conceptId="{$nextclid}" effectiveDate="{format-dateTime(current-dateTime(), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')}">
                                    {
                                        $terminologyAssociation/(@*[not(. = '')] except (@conceptId | @effectiveDate))
                                    }
                                    </terminologyAssociation>
                            ) 
                            else 
                            if ($deInherit) then (
                                attribute ref {$conceptList/(@id|@ref)},
                                $associations/*[@conceptId = $conceptList/(@ref | @id)]
                            )
                            else (
                                $conceptList/(@id|@ref),
                                $associations/*[@conceptId = $conceptList/(@ref | @id)]
                            )
                        }
                        {
                            for $conceptListConcept at $clcpos in $originalConceptList/concept
                            let $newclcid := concat($nextclid,'.',$clcpos)
                            
                            return
                                <concept>
                                {
                                    if ($doNewIds) then attribute id {$newclcid} else ($conceptListConcept/(@id|@ref))
                                    ,
                                    if ($valueDomain[@type = 'ordinal'] | $conceptListConcept/@ordinal) then 
                                        attribute ordinal {$conceptListConcept/@ordinal}
                                    else (),
                                    attribute exception {$conceptListConcept/@exception='true'},
                                    $conceptListConcept/@level[not(. = '')],
                                    $conceptListConcept/@type[not(. = '')]
                                }
                                {
                                    if ($doNewIds) then
                                        (: create terminology association for concept if present for inherited concept:)
                                        for $terminologyAssociation in $associations/*[@conceptId = $conceptListConcept/@id]
                                        return
                                            <terminologyAssociation conceptId="{$newclcid}" effectiveDate="{format-dateTime(current-dateTime(), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')}">
                                            {
                                                $terminologyAssociation/(@*[not(. = '')] except (@conceptId | @effectiveDate))
                                            }
                                            </terminologyAssociation>
                                    else (
                                        $associations/*[@conceptId = $conceptListConcept/@id]
                                    )
                                    ,
                                    for $lang in distinct-values(($projectLanguages, $language, $conceptListConcept/name/@language))
                                    let $node   := $conceptListConcept/name[@language=$lang][1]
                                    return 
                                        if ($node) then utillib:serializeNode($node) else ()
                                    ,
                                    for $node in $conceptListConcept/synonym
                                    return
                                        utillib:serializeNode($node)
                                    ,
                                    for $lang in distinct-values(($projectLanguages, $language, $copyFromConcept/desc/@language))
                                    let $node   := $conceptListConcept/desc[@language=$lang][1]
                                    return 
                                        if ($node) then utillib:serializeNode($node) else ()
                                }
                                </concept>
                        }
                        </conceptList>
                    )
                )
            }
            {
                for $ex in $valueDomain/example
                return
                    <example type="{($ex/@type, 'neutral')[not(. = '')][1]}">
                    {
                        $ex/@caption[not(. = '')],
                        $ex/node()
                    }
                    </example>
                ,
                $valueDomain/(* except (property|conceptList|example))
            }
            </valueDomain>
        }
        {
            let $subConcepts        := 
                if ($targetConcept[contains] or $targetType = ('containment', 'to-copy')) then () else ( 
                    $targetConcept/concept
                )
            (: 
                KH 20241009: handle all subconcepts but skip cancelled or rejected = $deapi:STATUSCODES-SKIPS concepts
                Also dsapi:createDataset en dsapi:inheritConcept handles these skips for other calls
            :)
            for $subConcept in $subConcepts[not(@statusCode = $deapi:STATUSCODES-SKIPS)]
            let $newCurrentId       := 
                if ($datasets[@maxcounter castable as xs:integer]) then $datasets/@maxcounter + 1 else (
                    max($datasets//concept[matches(@id,concat($baseId,'\d+$'))]/xs:integer(tokenize(@id,'\.')[last()]))+1
                )
            let $newCurrentId       := if ($newCurrentId) then $newCurrentId else (1)
            let $newId              := concat($baseId, $newCurrentId)
            let $newEffectiveDate   := format-dateTime(current-dateTime(), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')
            let $newLock            := 
                <lock type="{$decorlib:OBJECTTYPE-DATASETCONCEPT}" ref="{$newId}" effectiveDate="{$newEffectiveDate}" 
                      user="{$authmap?name}" userName="{$userDisplayName}" since="{current-dateTime()}" prefix="{$projectPrefix}"/>
            let $insertLock         := update insert $newLock into $setlib:docDecorLocks/decorLocks
            let $newConcept         :=
                <concept id="{$newId}" type="{$subConcept/@type}" statusCode="new" effectiveDate="{$newEffectiveDate}" lastModifiedDate="{$newEffectiveDate}">
                    <edit mode="edit"/>
                    {$newLock}
                    <inherit ref="{$subConcept/@id}" effectiveDate="{$subConcept/@effectiveDate}"/>
                </concept>
            (: inserting it as we go ensures the right id count :)
            let $insertNewConcept   := update insert $newConcept into $concept
            let $setmaxcounter      := update value $datasets/@maxcounter with $newCurrentId
            return
                deapi:getConceptForEdit($authmap, $userDisplayName, $decor, $concept/concept[@id = $newId], $subConcept, $targetType, $generateConceptListIds, $breakLock, $level + 1, $baseId, $projectPrefix, $language, $associationMode)
        }
        </concept>
};

declare function deapi:getConceptTree($conceptId as xs:string?, $conceptEffectiveDate as xs:string?, $transactionId as xs:string?, $transactionEffectiveDate as xs:string?, $fullTree as xs:boolean?, $propertiesMap as map(*)?) {
let $conceptId                  := $conceptId[not(. = '')]
let $conceptEffectiveDate       := $conceptEffectiveDate[not(. = '')]
let $transactionId              := $transactionId[not(. = '')]
let $transactionEffectiveDate   := $transactionEffectiveDate[not(. = '')]
let $fullTree                   := $fullTree = true()

let $tmp                        := utillib:getConceptTree($conceptId, $conceptEffectiveDate, $transactionId[1], $transactionEffectiveDate[1], $fullTree, $propertiesMap)

return
    deapi:copy2vtree($tmp)
};

declare function deapi:createConcept($authmap as map(*), $datasetId as xs:string, $datasetEffectiveDate as xs:string, $conceptBaseId as xs:string?, $conceptType as xs:string, $insertMode as xs:string, $insertRef as xs:string?, $insertFlexibility as xs:string?) {

    (: check if dataset not final or deprecated ? (security):)
    let $dataset                := utillib:getDataset($datasetId, $datasetEffectiveDate)
    let $decor                  := $dataset/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix
    
    let $storedConcept          := 
        if ($insertFlexibility castable as xs:dateTime) then
            $dataset//concept[@id = $insertRef][@effectiveDate = $insertFlexibility][not(ancestor::history)]
        else (
            $dataset//concept[@id = $insertRef]
        )
    
    let $check                  :=
        if ($dataset) then () else (
            error($errors:BAD_REQUEST, 'Dataset with id ' || $datasetId || ' and effectiveDate ' || $datasetEffectiveDate || ' , does not exist. Cannot create in non-existent dataset.')
        )
    let $check                  :=
        if ($storedConcept/ancestor-or-self::concept/@statusCode = $deapi:STATUSCODES-FINAL) then
            error($errors:BAD_REQUEST, concat('Concept cannot be added while it or any of its parents or the dataset itself have one of status: ', string-join($deapi:STATUSCODES-FINAL, ', ')))
        else ()
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-DATASETS)) then () else (
            error($errors:FORBIDDEN, 'User ' || $authmap?name || ' does not have sufficient permissions to add concepts to a dataset in project ' || $projectPrefix || '. You have to be an active author in the project.')
        )
    let $check                  :=
        if ($conceptType = $deapi:CONCEPT-TYPE/enumeration/@value) then () else (
            error($errors:BAD_REQUEST, 'Concept type ' || $conceptType || ' SHALL be one of ' || string-join($deapi:CONCEPT-TYPE/enumeration/@value, ', '))
        )
    let $check                  :=
        switch ($insertMode)
        case 'into' 
        case 'following'
        case 'preceding' return ()
        default return (
            error($errors:BAD_REQUEST, 'Parameter insertMode ' || $insertMode || ' SHALL be one of ''into'', ''following'', or ''preceding''')
        )
    let $check                  :=
        if (empty($insertRef)) then 
            if ($insertMode = 'into') then () else (
                error($errors:BAD_REQUEST, 'Parameter insertRef SHALL have a value when insertMode is ' || $insertMode || '. Otherwise we don''t know where to insert')
            )
        else ()
    let $check                  :=
        if (empty($conceptBaseId)) then () else (
            let $conceptBaseIds         := decorlib:getBaseIdsP($decor, $decorlib:OBJECTTYPE-DATASETCONCEPT)
            return
                if ($conceptBaseIds[@id = $conceptBaseId]) then () else (
                    error($errors:BAD_REQUEST, 'Parameter conceptBaseId SHALL have a value that is declared as baseId with type DE (dataset concepts) in the project. Allowable are ' || string-join($conceptBaseIds/@id, ', '))
                )
        )

    (: Note: not protected from missing defaultBaseId for given type ... :)
    let $baseId           := if (string-length($conceptBaseId)=0) then decorlib:getDefaultBaseIdsP($decor, $decorlib:OBJECTTYPE-DATASETCONCEPT)/@id else ($conceptBaseId)
    let $newId            := decorlib:getNextAvailableIdP($decor, $decorlib:OBJECTTYPE-DATASETCONCEPT, $baseId)/@id
    let $newEffectiveDate := format-dateTime(current-dateTime(), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')
    
    let $concept          :=
        <concept id="{$newId}" type="{$conceptType}" statusCode="new" effectiveDate="{$newEffectiveDate}" lastModifiedDate="{$newEffectiveDate}">
        {
            for $language in distinct-values($decor/project/name/@language)
            return
                <name language="{$language}"/>
            ,
            if ($conceptType = 'item') then <valueDomain type="{$deapi:VALUEDOMAIN-TYPE/enumeration/@value[not(. = 'code')][1]}"/> else ()
        }
        </concept>
    let $insert           :=
        if ($storedConcept) then
            if ($insertMode='following') then
                update insert $concept following $storedConcept[last()]
            else
            if ($insertMode='preceding') then
                update insert $concept preceding $storedConcept[1]
            else
            if ($storedConcept[history | concept]) then
                update insert $concept preceding ($storedConcept/history | $storedConcept/concept)[1]
            else (
                update insert $concept into $storedConcept
            )
        else
        if ($dataset[concept]) then 
            update insert $concept following $dataset/concept[last()]
        else (
            update insert $concept into $dataset
        )

    let $lock       := decorlib:setLock($authmap, $concept/@id, $concept/@effectiveDate, false())

return
    element {name($concept)} {
        $concept/@*,
        namespace {"json"} {"http://www.json.org"},
        utillib:addJsonArrayToElements($concept/*)
    }
};

(:~ Central logic for updating an existing dataset concept

@param $authmap         - required. Map derived from token
@param $id              - required. DECOR concept/@id to update
@param $effectiveDate   - required. DECOR concept/@effectiveDate to update
@param $data            - required. DECOR concept xml element containing everything that should be in the updated concept
@return concept object as xml with json:array set on elements
:)
declare function deapi:putConcept($authmap as map(*), $id as xs:string, $effectiveDate as xs:string, $data as element(concept)) as element(concept) {

    let $storedConcept          := utillib:getConcept($id, $effectiveDate)
    let $decor                  := $storedConcept/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix

    let $lock                   := decorlib:getLocks($authmap, $id, $effectiveDate, ())
    let $lock                   := if ($lock) then $lock else decorlib:setLock($authmap, $id, $effectiveDate, false())
    
    let $check                  :=
        if ($storedConcept) then () else (
            error($errors:BAD_REQUEST, 'Concept with id ' || $id || ' and effectiveDate ' || $effectiveDate || ' does not exist')
        )
    let $check                  :=
        if ($storedConcept/@statusCode = $deapi:STATUSCODES-FINAL) then
            error($errors:BAD_REQUEST, concat('Concept cannot be edited in status ', $storedConcept/@statusCode, '. ', if ($storedConcept/@statusCode = 'pending') then 'You should switch back to status draft to edit.' else ()))
        else ()
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-DATASETS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify datasets in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if ($lock) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have a lock for this concept (anymore). Get a lock first.'))
        )
    let $check                  :=
        if ($data[@id = $id] | $data[empty(@id)]) then () else (
            error($errors:BAD_REQUEST, concat('Concept SHALL have the same id as the concept id ''', $id, ''' used for updating. Found in request body: ', ($data/@id, 'null')[1]))
        )
    let $check                  :=
        if ($data[@effectiveDate = $effectiveDate] | $data[empty(@effectiveDate)]) then () else (
            error($errors:BAD_REQUEST, concat('Concept SHALL have the same effectiveDate as the concept effectiveDate ''', $effectiveDate, ''' used for updating. Found in request body: ', ($data/@effectiveDate, 'null')[1]))
        )
    let $check                  :=
        if ($data[@statusCode = $storedConcept/@statusCode] | $data[empty(@statusCode)]) then () else (
            error($errors:BAD_REQUEST, concat('Concept SHALL have the same statusCode as the concept statusCode ''', $storedConcept/@statusCode, ''' used for updating. Found in request body: ', ($data/@statusCode, 'null')[1]))
        )
    let $check                  :=
        for $n in $data/descendant-or-self::concept[not(name | inherit | contains)]
        return
            error($errors:BAD_REQUEST, 'Concept and conceptList.concept SHALL have at least one name if it does not inherit or contains')
    let $check                  :=
        if ($data//conceptList/concept[inherit | contains]) then 
            error($errors:BAD_REQUEST, 'ConceptList.concept SHALL NOT have inherit or contains')
        else ()
    let $checkVal               := $data/descendant-or-self::concept[@type = $deapi:CONCEPT-TYPE/enumeration/@value][not(ancestor::conceptList)]
    let $check                  :=
        if ($checkVal) then () else (
            error($errors:BAD_REQUEST, 'Concept type ' || $data/@type || ' SHALL be one of ' || string-join($deapi:CONCEPT-TYPE/enumeration/@value, ', '))
        )
    let $check                  :=
        if ($data[not(@type = 'item')] | $data[@type = 'item'][inherit | contains | valueDomain]) then () else (
            error($errors:BAD_REQUEST, 'Concept type ' || $data/@type || ' SHALL have an inherit, a contains or valueDomain.')
        )
    let $check                  :=
        for $n in $data//inherit[not(@ref = $storedConcept//inherit/@ref)]
        return
            if ($storedConcept/ancestor-or-self::concept[@id = $n/@ref][@effectiveDate = $n/@effectiveDate]) then
                error($errors:BAD_REQUEST, 'Concept ' || local-name($n) || ' SHALL NOT point to one of its parents (circular reference). Found: ref=''' || $n/@ref || ''' effectiveDate=''' || $n/@effectiveDate || '''')
            else
            if ($n[utillib:isOid(@ref)][@effectiveDate castable as xs:dateTime]) then
                if (utillib:getConcept($n/@ref, $n/@effectiveDate)) then () else (
                    error($errors:BAD_REQUEST, 'Concept ' || local-name($n) || ' SHALL point to an existing concept. Found: ref=''' || $n/@ref || ''' effectiveDate=''' || $n/@effectiveDate || '''')
                )
            else (
                error($errors:BAD_REQUEST, 'Concept ' || local-name($n) || ' SHALL have both ref as oid and effectiveDate as dateTime. Found: ref=''' || $n/@ref || ''' effectiveDate=''' || $n/@effectiveDate || '''')
            )
    let $check                  :=
        for $n in $data//contains[not(@ref = $storedConcept//contains/@ref)]
        return
            if ($n[utillib:isOid(@ref)][@flexibility castable as xs:dateTime]) then
                if (utillib:getConcept($n/@ref, $n/@flexibility)) then () else (
                    error($errors:BAD_REQUEST, 'Concept ' || local-name($n) || ' SHALL point to an existing concept. Found: ref=''' || $n/@ref || ''' flexibility=''' || $n/@flexibility || '''')
                )
            else (
                error($errors:BAD_REQUEST, 'Concept ' || local-name($n) || ' SHALL have both ref as oid and flexibility as dateTime. Found: ref=''' || $n/@ref || ''' flexibility=''' || $n/@flexibility || '''')
            )
    let $checkVal               := $data//valueDomain[not(ancestor::concept[1][inherit | contains])][not(@type = $deapi:VALUEDOMAIN-TYPE/enumeration/@value)]/@type
    let $check                  :=
        if (empty($checkVal)) then () else (
            error($errors:BAD_REQUEST, 'Concept valueDomain type ''' || string-join(distinct-values($checkVal), ''', ''') || ''' SHALL be one of ' || string-join($deapi:VALUEDOMAIN-TYPE/enumeration/@value, ', '))
        )
    let $checkVal               := $data//valueDomain[not(ancestor::concept[1][inherit | contains])]/example[not(@type = $deapi:EXAMPLE-TYPE/enumeration/@value)]/@type
    let $check                  :=
        if (empty($checkVal)) then () else (
            error($errors:BAD_REQUEST, 'Concept valueDomain example type ''' || string-join(distinct-values($checkVal), ''', ''') || ''' SHALL be one of ' || string-join($deapi:EXAMPLE-TYPE/enumeration/@value, ', '))
        )
    let $check                  :=
        if (empty($data//valueDomain[not(ancestor::concept[1][inherit | contains])][@type = 'code'][not(conceptList[@id | @ref])])) then () else (
            error($errors:BAD_REQUEST, 'Concept with coded valueDomain SHALL have a conceptList with an id or ref')
        )
    let $check                  :=
        for $clid in $data//valueDomain[not(ancestor::concept[1][inherit | contains])]/conceptList[not(@ref = $storedConcept//valueDomain/conceptList/@ref)]/@ref
        return
            if (utillib:getConceptList($clid, $projectPrefix)) then () else (
                error($errors:BAD_REQUEST, 'Concept valueDomain conceptList with ref ' || $clid || ' SHALL refer to a conceptList in this project ' || $projectPrefix)
            )
    let $check                  :=
        for $clid in $data//valueDomain[not(ancestor::concept[1][inherit | contains])]/conceptList[not(@id = $storedConcept//valueDomain/conceptList/@id)]/@id
        return
            if (utillib:getConceptList($clid, ())) then
                error($errors:BAD_REQUEST, 'Concept valueDomain conceptList with new id ' || $clid ||' SHALL NOT already exist anywhere')
            else ()
    let $check                  :=
        for $clid in $data//valueDomain[not(ancestor::concept[1][inherit | contains])]/conceptList/concept[not(@id = $storedConcept//valueDomain/conceptList/concept/@id)]/@id
        return
            if (utillib:getConceptListConcept($clid, ())) then
                error($errors:BAD_REQUEST, 'Concept valueDomain conceptList concept with new id ' || $clid ||' SHALL NOT already exist anywhere')
            else ()
    let $check                  :=
        for $clid in $storedConcept//valueDomain/conceptList[not(@id = $data//valueDomain/conceptList/@id)]/@id
        let $referred           := utillib:getConceptListRef($clid, $projectPrefix)
        return
            if ($referred) then
                error($errors:BAD_REQUEST, 'Concept uses a new valueDomain conceptList id ' || $clid ||' but this is still being referred to by ' || count($referred) || ' concepts. Referring concepts: ' || string-join($referred/concat('concept ', @id, ' / ', @effectiveDate), ', '))
            else ()
    let $checkVal               := $data//conceptList/concept/@id
    let $check                  :=
        if (count($checkVal) = count(distinct-values($checkVal))) then () else (
            error($errors:BAD_REQUEST, 'Concept valueDomain conceptList concept ids SHALL be unique. Duplicate concept ids: ' || string-join(distinct-values($checkVal[count(index-of($checkVal, .)) gt 1]), ', '))
        )
    
    let $preparedConcept        :=
        if ($data[valueDomain | @type[. = 'item']]) then
            deapi:prepareItemForUpdate($data, $storedConcept)
        else 
        if ($data[concept | @type[. = 'group']]) then
            deapi:prepareGroupForUpdate($data, $storedConcept)
        else (
            (: already handled above :)
        )
    
    let $intention              := if ($storedConcept[@statusCode = 'final']) then 'patch' else 'version'
    let $update                 := histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-DATASETCONCEPT, $projectPrefix, $intention, $storedConcept)
    let $update                 := update replace $storedConcept with $preparedConcept
    let $update                 := update delete $lock
    
    return
        element {name($preparedConcept)} {
            $preparedConcept/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($preparedConcept/*)
        }
};

(:~ Central logic for patching an existing transaction concept

@param $authmap         - required. Map derived from token
@param $id              - required. DECOR concept/@id to update
@param $effectiveDate   - required. DECOR concept/@effectiveDate to update
@param $transactionId   - required. DECOR transaction/@id this concept is in
@param $transactionEffectiveDate   - required. DECOR transaction/@effectiveDate this concept is in
@param $data            - required. DECOR concept xml element containing everything that should be in the updated concept
@return concept object as xml with json:array set on elements
:)
declare function deapi:putTransactionConcept($authmap as map(*), $id as xs:string, $effectiveDate as xs:string, $transactionId as xs:string, $transactionEffectiveDate as xs:string, $data as element(concept)) as element(concept) {
    let $storedConcept          := utillib:getTransactionConcept($id, $effectiveDate, $transactionId, $transactionEffectiveDate)
    let $decor                  := $storedConcept/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix
    let $lock                   := decorlib:getLocks($authmap, ($storedConcept/ancestor::transaction)[1]/@id, ($storedConcept/ancestor::transaction)[1]/@effectiveDate, ())
    let $lock                   := if ($lock) then $lock else decorlib:setLock($authmap, $id, $effectiveDate, false())
    
    let $check                  :=
        if ($storedConcept) then () else (
            error($errors:BAD_REQUEST, 'Transaction concept with id ' || $id || ' and effectiveDate ' || $effectiveDate || ' does not exist in transaction with id ' || ($storedConcept/ancestor::transaction)[1]/@id || ' and effectiveDate ' || ($storedConcept/ancestor::transaction)[1]/@effectiveDate)
        )
    let $check                  :=
        if ($storedConcept/ancestor-or-self::concept/@statusCode = $deapi:STATUSCODES-FINAL) then
            error($errors:BAD_REQUEST, concat('Concept cannot be edited while it or any of its parents have one of status: ', string-join($deapi:STATUSCODES-FINAL, ', ')))
        else ()
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-SCENARIOS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify scenarios/transactions in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if ($lock) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have a lock for this concept (anymore). Get a lock first.'))
        )
    let $check                  :=
        if ($data[@id = $id]) then () else (
            error($errors:BAD_REQUEST, concat('Concept SHALL have the same id as the concept id ''', $id, ''' used for updating. Found in request body: ', ($data/@id, 'null')[1]))
        )
    let $check                  :=
        if ($data[@effectiveDate = $effectiveDate]) then () else (
            error($errors:BAD_REQUEST, concat('Concept SHALL have the same effectiveDate as the concept id ''', $effectiveDate, ''' used for updating. Found in request body: ', ($data/@effectiveDate, 'null')[1]))
        )
    let $check                  :=
        if ($data[@minimumMultiplicity | @maximumMultiplicity | @conformance]) then () else (
            error($errors:BAD_REQUEST, 'Concept SHALL have one of minimumMultiplicity | maximumMultiplicity | conformance')
        )
    let $check                  :=
        if ($data//@conformance[not(. = ($deapi:CONFORMANCE-TYPE/enumeration/@value, 'M'))]) then
            error($errors:BAD_REQUEST, 'Concept conformance ' || string-join($data//@conformance[not(. = $deapi:CONFORMANCE-TYPE/enumeration/@value)], ' and ') || ' SHALL be one of ' || string-join($deapi:CONFORMANCE-TYPE/enumeration/@value, ', '))
        else ()
    let $check                  :=
        switch ($data/@conformance)
        case 'C' return (
            if ($data[@minimumMultiplicity | @maximumMultiplicity | @isMandatory]) then
                error($errors:BAD_REQUEST, 'Concept SHALL NOT have conformance C(onditional) and specify generic minimumMultiplicity, maximumMultiplicity or isMandatory')
            else (),
            if ($data[condition]) then () else (
                error($errors:BAD_REQUEST, 'Concept with conformance C(onditional) SHALL have condition elements specifying the conditions')
            ),
            if ($data/condition[@conformance = 'C']) then
                error($errors:BAD_REQUEST, 'Concept condition SHALL NOT have conformance C(onditional)')
            else (),
            if ($data/condition[not(desc)]) then
                error($errors:BAD_REQUEST, 'Concept condition SHALL have a description outlining the circumstances')
            else (),
            if ($data/condition[not(count(desc) = count(distinct-values(desc/@language)))]) then
                error($errors:BAD_REQUEST, 'Concept condition descriptions SHALL each have a unique language')
            else ()
        )
        case 'NP' return (
            if ($data[condition | @minimumMultiplicity[not(. = '0')] | @maximumMultiplicity[not(. = '0')] | @isMandatory[not(. = 'false')]]) then
                error($errors:BAD_REQUEST, 'Concept with conformance Not Present (NP) SHALL NOT have conditions or any of minimumMultiplicity | maximumMultiplicity | isMandatory')
            else ()
        )
        default return (
            if ($data[condition]) then
                error($errors:BAD_REQUEST, 'Concept SHALL NOT have condition elements unless it has conformance C(onditional)')
            else ()
        )
    let $check                  :=
        if ($data[count(context) = count(distinct-values(context/@language))]) then () else (
            error($errors:BAD_REQUEST, 'Concept contexts SHALL each have a unique language')
        )
    let $check                  :=
        if ($data//@minimumMultiplicity[not(. castable as xs:integer or . = '?')]) then
            error($errors:BAD_REQUEST, 'Concept minimumMultiplicity SHALL be an integer. Found: ' || string-join($data//@minimumMultiplicity[not(. castable as xs:integer or . = '?')], ' and '))
        else ()
    let $check                  :=
        if ($data//@maximumMultiplicity[not(. castable as xs:integer or . = ('*', '?'))]) then
            error($errors:BAD_REQUEST, 'Concept maximumMultiplicity SHALL be an integer or '*'. Found: ' || string-join($data//@maximumMultiplicity[not(. castable as xs:integer or . = ('*', '?'))], ' and '))
        else ()
    let $minmax                 := $data/descendant-or-self::*[@minimumMultiplicity castable as xs:integer][@maximumMultiplicity castable as xs:integer][xs:integer(@minimumMultiplicity) gt xs:integer(@maximumMultiplicity)]
    let $check                  :=
        if ($minmax) then
            error($errors:BAD_REQUEST, 'Concept minimumMultiplicity SHALL be &lt;= maximumMultiplicity. Found: ' || string-join($minmax/concat(@minimumMultiplicity, '..', @maximumMultiplicity), ' and '))
        else ()
    let $check                  := 
        for $man in $data/descendant-or-self::*[@isMandatory = 'true'] | $data/descendant-or-self::*[@conformance = 'M']
        return
            if ($man[condition] | $man[empty(@minimumMultiplicity)] | $man[@minimumMultiplicity = '0']) then
                error($errors:BAD_REQUEST, 'Concept that is mandatory SHALL NOT have conditions and SHALL have minimumMultiplicity != 0')
            else ()
    (: terminologyAssociation :)
    let $check                  :=
        for $assoc in $data/terminologyAssociation
        return (
            if ($assoc[@conceptId]) then () else (
                error($errors:BAD_REQUEST, 'Concept terminologyAssociation SHALL have a conceptId')
            ),
            if ($assoc[@conceptFlexibility]) then
                if ($assoc/@conceptFlexibility[. castable as xs:dateTime]) then () else (
                    error($errors:BAD_REQUEST, 'Concept terminologyAssociation conceptFlexibility SHALL be yyyy-mm-ddThh:mm:ss. Found: ' || $assoc/@conceptFlexibility)
                )
            else (),
            if ($assoc[@valueSet][@conceptFlexibility]) then 
                error($errors:BAD_REQUEST, 'Concept terminologyAssociation with a valueSet SHALL NOT have conceptFlexibility as valueDomain.conceptList does not have an effectiveDate to point to')
            else (),
            if ($assoc[@valueSet][@code | @codeSystem | @codeSystemName | @codeSystemVersion]) then
                error($errors:BAD_REQUEST, 'Concept terminologyAssociation SHALL NOT have a valueSet and one of code | codeSystem | codeSystemName | codeSystemVersion')
            else (),
            if ($assoc[@valueSet][@flexibility]) then () else if ($assoc[@flexibility]) then 
                error($errors:BAD_REQUEST, 'Concept terminologyAssociation SHALL NOT have a flexibility without valueSet')
            else (),
            if ($assoc[@flexibility]) then
                if ($assoc/@flexibility[. castable as xs:dateTime] | $assoc/@flexibility[. = 'dynamic']) then () else (
                    error($errors:BAD_REQUEST, 'Concept terminologyAssociation flexibility SHALL be yyyy-mm-ddThh:mm:ss or be dynamic. Found: ' || $assoc/@flexibility)
                )
            else (),
            if ($assoc[@effectiveDate]) then
                if ($assoc/@effectiveDate[. castable as xs:dateTime]) then () else (
                    error($errors:BAD_REQUEST, 'Concept terminologyAssociation effectiveDate SHALL be yyyy-mm-ddThh:mm:ss. Found: ' || $assoc/@effectiveDate)
                )
            else ()
        )
    (: identifierAssociation :)
    let $check                  :=
        for $assoc in $data/identifierAssociation
        return (
            if ($assoc[@conceptId]) then () else (
                error($errors:BAD_REQUEST, 'Concept identifierAssociation SHALL have a conceptId')
            ),
            if ($assoc[@conceptFlexibility]) then
                if ($assoc/@conceptFlexibility[. castable as xs:dateTime]) then () else (
                    error($errors:BAD_REQUEST, 'Concept identifierAssociation conceptFlexibility SHALL be yyyy-mm-ddThh:mm:ss. Found: ' || $assoc/@conceptFlexibility)
                )
            else (),
            (:if ($assoc[@ref]) then 
                error($errors:BAD_REQUEST, 'Concept identifierAssociation SHALL have ref')
            else (),:)
            if ($assoc[@effectiveDate]) then
                if ($assoc/@effectiveDate[. castable as xs:dateTime]) then () else (
                    error($errors:BAD_REQUEST, 'Concept identifierAssociation effectiveDate SHALL be yyyy-mm-ddThh:mm:ss. Found: ' || $assoc/@effectiveDate)
                )
            else ()
        )
    
    let $preparedConcept        := deapi:prepareTransactionItemForUpdate($data, $storedConcept)
    
    let $intention              := if ($storedConcept/ancestor::transaction[1][@statusCode = 'final']) then 'patch' else 'version'
    let $update                 := histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-TRANSACTION, $projectPrefix, $intention, $storedConcept/ancestor::transaction[1])
    let $update                 := update replace $storedConcept with $preparedConcept
    let $update                 := update delete $lock
    
    return
        element {name($preparedConcept)} {
            $preparedConcept/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($preparedConcept/*)
        }
};

(:~ Central logic for patching an existing dataset concept

@param $authmap         - required. Map derived from token
@param $id              - required. DECOR concept/@id to update
@param $effectiveDate   - required. DECOR concept/@effectiveDate to update
@param $data            - required. DECOR concept xml element containing everything that should be in the updated concept
@return concept object as xml with json:array set on elements
:)
declare function deapi:patchConcept($authmap as map(*), $id as xs:string, $effectiveDate as xs:string, $data as element(parameters)) as element(concept) {

    let $storedConcept          := utillib:getConcept($id, $effectiveDate)
    let $decor                  := $storedConcept/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix

    let $lock                   := decorlib:getLocks($authmap, $id, $effectiveDate, ())
    let $lock                   := if ($lock) then $lock else decorlib:setLock($authmap, $id, $effectiveDate, false())
    
    let $check                  :=
        if ($storedConcept) then () else (
            error($errors:BAD_REQUEST, 'Concept with id ' || $id || ' and effectiveDate ' || $effectiveDate || ' does not exist')
        )
    let $check                  :=
        if ($storedConcept/ancestor::concept/@statusCode = $deapi:STATUSCODES-FINAL) then
            error($errors:BAD_REQUEST, concat('Concept patched be added while any of its parents has one of status: ', string-join($deapi:STATUSCODES-FINAL, ', ')))
        else 
        if ($storedConcept[@statusCode = $deapi:STATUSCODES-FINAL]) then
            if ($data/parameter[@path = '/statusCode'][@op = 'replace'][not(@value = $deapi:STATUSCODES-FINAL)]) then () else 
            if ($data[count(parameter) = 1]/parameter[@path = '/statusCode']) then () else (
                error($errors:BAD_REQUEST, concat('Concept cannot be patched while it has one of status: ', string-join($deapi:STATUSCODES-FINAL, ', ')))
            )
        else ()
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-DATASETS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify datasets in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if ($lock) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have a lock for this concept (anymore). Get a lock first.'))
        )
    let $check                  :=
        if ($data[parameter]) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall be an array of ''parameter''.')
        )
    let $unsupportedops         := $data/parameter[not(@op = ('add', 'replace', 'remove'))]
    let $check                  :=
        if ($unsupportedops) then
            error($errors:BAD_REQUEST, 'Submitted parameters shall have supported ''op'' value. Found ''' || string-join(distinct-values($unsupportedops/@op), ''', ''') || ''', expected ''add'', ''replace'', or ''remove''') 
        else ()
    let $allowableInheritPaths  := ('/statusCode', '/canonicalUri', '/expirationDate', '/officialReleaseDate', '/versionLabel', '/comment')
    let $checkn                 :=
        if ($storedConcept[inherit | contains]) then
            if (count($data/parameter/@path) = count($data/parameter[@path = $allowableInheritPaths])) then () else (
                'A concept that inherits or contains, does not allow patching of ' || string-join($data/parameter[not(@path = $allowableInheritPaths)], ', ')
            )
        else (
            if (count($storedConcept/name) - count($data/parameter[@op = 'remove'][@path = '/name']) + count($data/parameter[@op = 'add'][@path = '/name']) ge 1) then () else (
                'A concept SHALL have at least one name. You cannot remove every name.'
            )
            (: https://art-decor.atlassian.net/browse/AD30-451 :)
            (:,
            if (count($storedConcept/desc) - count($data/parameter[@op = 'remove'][@path = '/desc']) + count($data/parameter[@op = 'add'][@path = '/desc']) ge 1) then () else (
                'A concept SHALL have at least one description. You cannot remove every desc.'
            ),:)
            ,
            let $currentConceptType   := utillib:getOriginalForConcept($storedConcept)/@type
            let $newConceptType       := ($data/parameter[@path = '/type'][not(@op = 'remove')]/@value)[last()]
            return
            if (($newConceptType, $currentConceptType)[1] = 'group') then
                if (($data/parameter[starts-with(@path, '/valueDomain')][last()])[not(@op = 'remove')]) then
                    'You are adding/replacing a valueDomain and switching to type group. A concept group SHALL NOT have a valueDomain.'
                else
                if (count($storedConcept/valueDomain) - count($data/parameter[@op = 'remove'][@path = '/valueDomain']) + count($data/parameter[not(@op = 'remove')][@path = '/valueDomain']) lt 1) then () else (
                    if ($currentConceptType = 'group') then
                        'The stored concept is a group but has a valueDomain that you are not removing. A concept group SHALL NOT have a valueDomain.'
                    else (
                        'You are switching to concept type group and the current concept has a valueDomain that you are not removing. A concept group SHALL NOT have a valueDomain.'
                    )
                )
            else (
                if ($storedConcept[concept]) then
                    'A concept item SHALL NOT have child concepts. You cannot switch the type to ''item'' while the group is non-empty'
                else (),
                if (count($storedConcept/valueDomain) - count($data/parameter[@op = 'remove'][@path = '/valueDomain']) + count($data/parameter[not(@op = 'remove')][@path = '/valueDomain']) ge 1) then () else (
                    'A concept SHALL have a valueDomain. You cannot remove every valueDomain.'
                )
            )
        )
    let $check                  :=
        for $param in $data/parameter
        let $op         := $param/@op
        let $path       := $param/@path
        let $value      := $param/@value
        let $pathpart   := substring-after($path, '/')
        return
            switch ($path)
            case '/statusCode' return (
                if ($op = 'remove') then
                    'Parameter path ''' || $path || ''' does not support ' || $op
                else 
                if (utillib:isStatusChangeAllowable($storedConcept, $value)) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Supported are: ' || string-join(map:get($utillib:itemstatusmap, string($storedConcept/@statusCode)), ', ')
                )
            )
            case '/type' return (
                if ($op = 'remove') then
                    'Parameter path ''' || $path || ''' does not support ' || $op
                else 
                if ($value = $deapi:CONCEPT-TYPE/enumeration/@value) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Supported are: ' || string-join($deapi:CONCEPT-TYPE/enumeration/@value, ', ')
                )
            )
            case '/expirationDate'
            case '/officialReleaseDate' return (
                if ($op = 'remove') then () else 
                if ($value castable as xs:dateTime) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL be yyyy-mm-ddThh:mm:ss.'
                )
            )
            case '/canonicalUri'
            case '/versionLabel' return (
                if ($op = 'remove') then () else 
                if ($value[not(. = '')]) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL NOT be empty.'
                )
            )
            case '/name' 
            case '/synonym'
            case '/desc' 
            case '/source'
            case '/operationalization' 
            case '/rationale' 
            case '/comment' return (
                if ($param[count(value/*[name() = $pathpart]) = 1]) then
                    if ($param/value/*[name() = $pathpart][matches(@language, '[a-z]{2}-[A-Z]{2}')]) then 
                        if ($param[@op = 'remove'] | $param/value/*[name() = $pathpart][.//text()[not(normalize-space() = '')]]) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A ' || $pathpart || ' SHALL have contents.'
                        )
                    else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A ' || $pathpart || ' SHALL have a language with pattern ll-CC.'
                    )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/*[name() = $pathpart]) 
                )
            )
            case '/property' return (
                if ($param[count(value/property) = 1]) then
                    if ($param/value/property/@name[not(. = '')]) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || '''. Property.name SHALL be a non empty string.'
                    )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one property under value. Found ' || count($param/value/property) 
                ),
                if ($param/value/property[@datatype]) then 
                    if ($param/value/property[@datatype = $deapi:VALUEDOMAIN-TYPE/enumeration/@value]) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || ''' with type ''' || string-join($param/value/property/@datatype, ' ') || '''. Supported are: ' || string-join($deapi:VALUEDOMAIN-TYPE/enumeration/@value, ', ')
                    )
                else ()
            )
            case '/relationship' return (
                if ($param/value/relationship[@type = $deapi:RELATIONSHIP-TYPE/enumeration/@value]) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with type ''' || string-join($param/value/relationship/@type, ' ') || '''. Supported are: ' || string-join($deapi:RELATIONSHIP-TYPE/enumeration/@value, ', ')
                ),
                if ($param/value/relationship[@ref[utillib:isOid(.)]]) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with ref ''' || string-join($param/value/relationship/@ref, ' ') || '''. Relationship/ref SHALL be an oid.'
                ),
                if ($param/value/relationship[empty(@flexibility) or @flexibility castable as xs:dateTime or @flexibility = 'dynamic']) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with flexibility ''' || string-join($param/value/relationship/@flexibility, ' ') || '''. Relationship/flexibility SHALL be omitted, yyyy-MM-DDThh:mm:ss or ''dynamic''.'
                )
            )
            case '/contains'
            case '/inherit' return (
                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Use /concept/{id}/{effectiveDate}/$inherit to manipulate inherit and contains'
            )
            case '/valueDomain' return (
                if ($op = 'remove') then () else (
                    if ($op = 'replace') then
                        if (count($storedConcept/valueDomain) le 1 or count($storedConcept/valueDomain[@type = $param/value/valueDomain/@type]) le 1) then () else (
                            'Parameter ' || $op || ' ambiguous for ''' || $path || '''. Multiple valueDomains exist. Don''t know what to replace.'
                        )
                    else (),
                    if ($op = 'add') then
                        if ($storedConcept/valueDomain) then
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. There already is a valueDomain and multiple are not allowed.'
                        else ()
                    else (),
                    if ($param/value/valueDomain/@type = $deapi:VALUEDOMAIN-TYPE/enumeration/@value) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || ''' with valueDomain type ''' || string-join($param/value/valueDomain/@type, ' ') || '''. Supported are: ' || string-join($deapi:VALUEDOMAIN-TYPE/enumeration/@value, ', ')
                    ),
                    if (count($param/value/valueDomain/example/@type) = count($param/value/valueDomain/example[@type = $deapi:EXAMPLE-TYPE/enumeration/@value])) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || ''' with valueDomain example type ''' || string-join($param/value/valueDomain/example[not(@type = $deapi:EXAMPLE-TYPE)]/@type, ' or ') || '''. Supported are: ' || string-join($deapi:EXAMPLE-TYPE/enumeration/@value, ', ')
                    ),
                    if ($param/value/valueDomain/property[@*[not(name() = $deapi:VALUEDOMAIN-PROPS/@name)]]) then 
                        'Parameter ' || $op || ' not allowed for ''' || $path || '''. Property SHALL NOT have unsupported values. Found: ' || string-join($param/value/valueDomain/property/@*[not(name() = $deapi:VALUEDOMAIN-PROPS/@name)]/name(), ', ')
                    else (),
                    if ($param/value/valueDomain[@type = 'code']) then (
                        if ($param/value/valueDomain[@type = 'code']/conceptList[@id | @ref]) then () else (
                            if ($storedConcept/@statusCode = 'new') then (
                                (: no concept list on a new coded concept, add one later, see AD30-1453 :)
                            ) else (
                                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Coded valueDomain SHALL have a conceptList.'
                        )
                        ),
                        (: if there are no conceptList/@ids assume fine, if they are the same as the current conceptList/@ids, assume fine, else check unique :)
                        for $clid in $param/value/valueDomain/conceptList[not(@id = $storedConcept/valueDomain/conceptList/@id)]/@id
                        return
                            if (utillib:isOid($clid) and empty(utillib:getConceptList($clid, ()))) then () else (
                                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Concept valueDomain conceptList SHALL have a unique oid. Recommended pattern: [concept id].[concept effectiveDate as yyyyMMddhhmmss].0. Found: ' || $clid
                            )
                        ,
                        (: if there are no conceptList/concept/@ids assume fine, if they are the same as the current conceptList/@ids, assume fine, else check unique :)
                        for $clid in $param/value/valueDomain/conceptList/concept[not(@id = $storedConcept/valueDomain/conceptList/concept/@id)]/@id
                        return
                            if (utillib:isOid($clid) and empty($setlib:colDecorData//@id[. = $clid] | $setlib:colDecorData//@codeSystem[. = $clid])) then () else (
                                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Concept valueDomain conceptList concept SHALL have a unique oid. Recommended pattern: [concept id].[concept effectiveDate as yyyyMMddhhmmss].[n]. Found: ' || $clid
                            )
                    )
                    else (),
                    if ($param/value/valueDomain[count(conceptList) le 1]) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || '''. Concept valueDomain SHALL NOT have more than 1 conceptList'
                    ),
                    let $checkVal               := $param/value/valueDomain/conceptList/concept/@id
                    return
                        if (count($checkVal) = count(distinct-values($checkVal))) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Concept valueDomain SHALL HAVE unique ids on all its concepts. Duplicate(s) found: ' || string-join(distinct-values($checkVal[count(index-of($checkVal, .)) gt 1]), ', ')
                        )
                )
            )
            default return (
                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Path value not supported'
            )
     let $check                 :=
        if (empty($checkn) and empty($check)) then () else (
            error($errors:BAD_REQUEST, string-join (($checkn, $check), ' '))
        )
    
    let $intention              := if ($storedConcept[@statusCode = 'final']) then 'patch' else 'version'
    let $update                 := histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-DATASETCONCEPT, $projectPrefix, $intention, $storedConcept)
    
    let $update                 :=
        for $param in $data/parameter
        return
            switch ($param/@path)
            case '/statusCode'
            case '/type' return (
                let $attname  := substring-after($param/@path, '/')
                let $new      := attribute {$attname} {$param/@value}
                let $stored   := $storedConcept/@*[name() = $attname]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedConcept
                (:case ('remove') return update delete $stored:)
                default return ( (: unknown op :) )
            )
            case '/expirationDate'
            case '/officialReleaseDate'
            case '/canonicalUri'
            case '/versionLabel' return (
                let $attname  := substring-after($param/@path, '/')
                let $new      := attribute {$attname} {$param/@value}
                let $stored   := $storedConcept/@*[name() = $attname]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedConcept
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/name' 
            case '/desc' 
            case '/source'
            case '/operationalization' 
            case '/rationale' return (
                (: only one per language :)
                let $elmname  := substring-after($param/@path, '/')
                let $new      := utillib:prepareFreeFormMarkupWithLanguageForUpdate($param/value/*[name() = $elmname])
                let $stored   := $storedConcept/*[name() = $elmname][@language = $param/value/*[name() = $elmname]/@language]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedConcept
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/synonym'
            case '/comment' return ( 
                (: multiple possible per language :)
                let $elmname  := substring-after($param/@path, '/')
                let $new      := utillib:prepareFreeFormMarkupWithLanguageForUpdate($param/value/*[name() = $elmname])
                let $stored   := $storedConcept/*[name() = $elmname][@language = $param/value/*[name() = $elmname]/@language][lower-case(normalize-space(string-join(.//text(), ''))) = lower-case(normalize-space(string-join($new//text(), '')))]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedConcept
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            (:case '/inherit' return (
            ):)
            (:case '/contains' return (
            ):)
            case '/relationship' return (
                let $new      := utillib:prepareDatasetRelationshipForUpdate($param/value/relationship)
                let $stored   := $storedConcept/relationship[@ref = $new/@ref]
                let $stored   := if ($new/@flexibility) then $stored[@flexibility = $new/@flexibility] else $stored
                
                return
                switch ($param/@op)
                case ('add') return update insert $new into $storedConcept
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedConcept
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/property' return (
                (: multiple possible per language :)
                let $elmname  := substring-after($param/@path, '/')
                let $new      := utillib:prepareConceptPropertyForUpdate($param/value/property)
                let $stored   := $storedConcept/property[@name = $new/@name][lower-case(normalize-space(string-join(.//text(), ''))) = lower-case(normalize-space(string-join($new//text(), '')))]
                
                return
                switch ($param/@op)
                case ('add') return update insert $new into $storedConcept
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedConcept
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/valueDomain' return (
                (: multiple possible :)
                let $elmname  := substring-after($param/@path, '/')
                
                (: build a temp valueDomain element, if needed with additions :)
                let $checkedValueDomain      :=
                    if ( ($param/value/valueDomain[@type = ('code', 'ordinal')] and not($param/value/valueDomain/conceptList[@id | @ref]))
                          and $storedConcept/@statusCode = 'new') then (
                        (: no concept list on a new coded concept, add one now, see AD30-1453 :)
                        <valueDomain>
                        {
                            $param/value/valueDomain/@*,
                            $param/value/valueDomain/node(),
                            <conceptList id="{concat($storedConcept/@id, '.', replace($storedConcept/@effectiveDate,'\D', ''), '.0')}"/>
                        }
                        </valueDomain>
                    ) 
                    else $param/value/valueDomain
                    
                let $new      := deapi:handleConceptValueDomain($checkedValueDomain)
                let $stored   := ($storedConcept/valueDomain[@type = $new/@type] | $storedConcept/valueDomain)[1]
                
                return
                switch ($param/@op)
                case ('add') return update insert $new into $storedConcept
                case ('replace') return if ($stored) then update replace $stored with $new else ()
                case ('remove') return update delete $storedConcept/valueDomain[@type = $new/@type][count(property) = count($new/property)][count(example) = count($new/example)][empty(conceptList | $new/conceptList) or conceptList/(@id | @ref) = $new/conceptList/(@id | @ref)]
                default return ( (: unknown op :) )
            )
            default return ( (: unknown path :) )
    
    (: after all the updates, the XML Schema order is likely off. Restore order :)
    let $originalConcept        := utillib:getOriginalForConcept($storedConcept)
    let $preparedConcept        :=
        if ($storedConcept[@type[. = 'group'] | concept] | $originalConcept[@type = 'group']) then
            deapi:prepareGroupForUpdate($storedConcept, $storedConcept)
        else (
            deapi:prepareItemForUpdate($storedConcept, $storedConcept)
        )
    
    let $update                 := update replace $storedConcept with $preparedConcept
    let $update                 := update delete $lock
    
    return
        element {name($preparedConcept)} {
            $preparedConcept/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($preparedConcept/*)
        }
};

(:~ Central logic for patching an existing dataset concept statusCode, versionLabel, expirationDate and/or officialReleaseDate

@param $authmap required. Map derived from token
@param $id required. DECOR concept/@id to update
@param $effectiveDate required. DECOR concept/@effectiveDate to update
@param $recurse optional as xs:boolean. Default: false. Allows recursion into child particles to apply the same updates
@param $listOnly optional as xs:boolean. Default: false. Allows to test what will happen before actually applying it
@param $newStatusCode optional as xs:string. Default: empty. If empty, does not update the statusCode
@param $newVersionLabel optional as xs:string. Default: empty. If empty, does not update the versionLabel
@param $newExpirationDate optional as xs:string. Default: empty. If empty, does not update the expirationDate 
@param $newOfficialReleaseDate optional as xs:string. Default: empty. If empty, does not update the officialReleaseDate 
@return list object with success and/or error elements or error
:)
declare function deapi:setConceptStatus($authmap as map(*), $id as xs:string, $effectiveDate as xs:string?, $recurse as xs:boolean, $listOnly as xs:boolean, $newStatusCode as xs:string?, $newVersionLabel as xs:string?, $newExpirationDate as xs:string?, $newOfficialReleaseDate as xs:string?) as element(list) {
    (:get object for reference:)
    let $object                 := utillib:getConcept($id, $effectiveDate)
    let $decor                  := $object/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if (count($object) = 1) then () else
        if ($object) then
            error($errors:SERVER_ERROR, 'Concept id ''' || $id || ''' effectiveDate ''' || $effectiveDate || ''' cannot be updated because multiple ' || count($object) || ' were found in: ' || string-join(distinct-values($object/ancestor::decor/project/@prefix), ' / ') || '. Please inform your database administrator.')
        else (
            error($errors:BAD_REQUEST, 'Concept id ''' || $id || ''' effectiveDate ''' || $effectiveDate || ''' cannot be updated because it cannot be found.')
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-DATASETS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify datasets in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    (: https://art-decor.atlassian.net/browse/AD30-259 :)
    let $check                  :=
        if ($object[@statusCode = 'cancelled'] and $object[not(@statusCode = $newStatusCode)]) then
            if ($object/ancestor::dataset[@statusCode = 'draft']) then () else (
                error($errors:BAD_REQUEST, 'Status transition not allowed from ' || $object/@statusCode || ' to ''' || $newStatusCode || ''', because the dataset is not in status draft but in status ''' || $object/ancestor::dataset/@statusCode || '''')
            )
        else ()
    
    let $testUpdate             :=
        utillib:applyStatusProperties($authmap, $object, $object/@statusCode, $newStatusCode, $newVersionLabel, $newExpirationDate, $newOfficialReleaseDate, $recurse, true(), $projectPrefix)
    let $results                :=
        if ($testUpdate[self::error]) then $testUpdate else
        if ($listOnly) then $testUpdate else (
            utillib:applyStatusProperties($authmap, $object, $object/@statusCode, $newStatusCode, $newVersionLabel, $newExpirationDate, $newOfficialReleaseDate, $recurse, false(), $projectPrefix)
        )
    return
    if ($results[self::error] and not($listOnly)) then
        error($errors:BAD_REQUEST, string-join(
            for $e in $results[self::error]
            return
                $e/@itemname || ' id ''' || $e/@id || ''' effectiveDate ''' || $e/@effectiveDate || ''' cannot be updated: ' || data($e)
            , ' ')
        )
    else (
        <list artifact="STATUS" current="{count($results)}" total="{count($results)}" all="{count($results)}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            (: max 2 arrays: one with error and one with success :)
            utillib:addJsonArrayToElements($results)
        }
        </list>
    )
};

declare %private function deapi:copy2vtree($node as node()*) as node()* {
    for $e in $node
    return
        <concept>
        {
            $e/@*,
            $e/name,
            deapi:copy2vtree($e/concept)
        }
        </concept>
};

declare function deapi:prepareItemForUpdate($concept as element(),$storedItem as element()?) as element() {
let $status     := if ($concept[@statusCode=('new','')] | $concept[empty(@statusCode)]) then 'draft' else ($concept/@statusCode)

return
    <concept id="{$concept/@id}" effectiveDate="{$concept/@effectiveDate}" statusCode="{$status}">
    {
        if ($concept[inherit | contains]) then () else (
            attribute type {'item'}
            (:$concept/@type:)
        )
        ,
        $concept/@versionLabel[string-length() gt 0],
        $concept/@expirationDate[string-length() gt 0],
        $concept/@officialReleaseDate[string-length() gt 0],
        $concept/@canonicalUri[string-length() gt 0],
        attribute lastModifiedDate {substring(string(current-dateTime()), 1, 19)}
        ,
        if ($concept[inherit]) then (
            <inherit>{$concept/inherit/@ref, $concept/inherit/@effectiveDate}</inherit>
            ,
            for $node in $concept/comment
            return 
                utillib:prepareFreeFormMarkupWithLanguageForUpdate($node)
        )
        else
        if ($concept[contains]) then (
            <contains>{$concept/contains/@ref, $concept/contains/@flexibility}</contains>
            ,
            for $node in $concept/comment
            return 
                utillib:prepareFreeFormMarkupWithLanguageForUpdate($node)
        )
        else (
            utillib:prepareFreeFormMarkupWithLanguageForUpdate($concept/name)
            ,
            for $node in $concept/synonym
            return 
                utillib:prepareFreeFormMarkupWithLanguageForUpdate($node)
            ,
            utillib:prepareFreeFormMarkupWithLanguageForUpdate($concept/desc)
            ,
            utillib:prepareFreeFormMarkupWithLanguageForUpdate($concept/source)
            ,
            utillib:prepareFreeFormMarkupWithLanguageForUpdate($concept/rationale)
            ,
            for $node in $concept/comment
            return 
                utillib:prepareFreeFormMarkupWithLanguageForUpdate($node)
            ,
            (:new since 2015-04-21:)
            utillib:prepareConceptPropertyForUpdate($concept/property)
            ,
            (:new since 2015-04-21:)
            utillib:prepareDatasetRelationshipForUpdate($concept/relationship)
            ,
            utillib:prepareFreeFormMarkupWithLanguageForUpdate($concept/operationalization)
            ,
            for $valueDomain in $concept/valueDomain
            return
                deapi:handleConceptValueDomain($valueDomain)
        )
   }
   </concept>
};
declare function deapi:prepareTransactionItemForUpdate($concept as element(),$storedItem as element()?) as element() {
    <concept ref="{$concept/@id}" flexibility="{$concept/@effectiveDate}">
    {
        if ($concept[@conformance = ('C', 'NP')]) then () else (
            (: keep empty min/max. this happens for incomplete transactions where not all is known yet :)
            $concept/@minimumMultiplicity,
            $concept/@maximumMultiplicity
        )
        ,
        if ($concept[@conformance = 'M']) then (
            attribute isMandatory {'true'}
        )
        else (
            $concept/@conformance[not(.='')]
        ),
        if ($concept[@conformance = ('C', 'NP')]) then () else (
            $concept/@isMandatory[. = 'true']
        )
    }
    {
        $concept/context[exists(node())][.//text()[not(normalize-space() = '')]]
    }
    {
        if ($concept[@conformance = 'C']) then (
            for $condition in $concept/condition
            return
            <condition>
            {
                if ($condition/@conformance='NP') then () else (
                    (: keep empty min/max. this happens for incomplete transactions where not all is known yet :)
                    $condition/@minimumMultiplicity,
                    $condition/@maximumMultiplicity
                )
                ,
                if ($condition/@conformance='M') then (
                    attribute isMandatory {'true'}
                ) else (
                    $condition/@conformance[not(.='')]
                ),
                if ($condition[@conformance = ('C', 'NP')]) then () else (
                    $condition/@isMandatory[. = 'true']
                )
                ,
                if ($condition[desc]) then
                    utillib:prepareFreeFormMarkupWithLanguageForUpdate($condition/desc)
                else 
                if ($condition[text()[not(. = '')]]) then () else (
                    <desc language="{$storedItem/ancestor::decor/project/@defaultLanguage}">{$condition/text()}</desc>
                )
            }
            </condition>
        )
        else ()
    }
    {
        (: prepare them while allowing for 'empty' associations, that don't bind to anything, signalling deletion in the context of this transaction :)
        for $node in $concept/terminologyAssociation
        return
            utillib:prepareTerminologyAssociationForUpdate($node, true())
        ,
        for $node in $concept/identifierAssociation
        return
            utillib:prepareIdentifierAssociationForUpdate($node, true())
    }
    </concept>
};

declare function deapi:prepareGroupForUpdate($concept as element(),$storedGroup as element()?) as element() {
let $status     := if ($concept[@statusCode=('new','')] | $concept[empty(@statusCode)]) then 'draft' else ($concept/@statusCode)

return
    <concept id="{$concept/@id}" effectiveDate="{$concept/@effectiveDate}" statusCode="{$status}">
    {
        if ($concept[inherit | contains]) then () else (
            attribute type {'group'}
            (:$concept/@type:)
        )
        ,
        $concept/@versionLabel[string-length() gt 0],
        $concept/@expirationDate[string-length() gt 0],
        $concept/@officialReleaseDate[string-length() gt 0],
        $concept/@canonicalUri[string-length() gt 0],
        attribute lastModifiedDate {substring(string(current-dateTime()), 1, 19)}
        ,
        if ($concept[inherit]) then (
            <inherit>{$concept/inherit/@ref, $concept/inherit/@effectiveDate}</inherit>
            ,
            for $node in $concept/comment
            return 
                utillib:prepareFreeFormMarkupWithLanguageForUpdate($node)
        )
        else
        if ($concept[contains]) then (
            <contains>{$concept/contains/@ref, $concept/contains/@flexibility}</contains>
            ,
            for $node in $concept/comment
            return 
                utillib:prepareFreeFormMarkupWithLanguageForUpdate($node)
        )
        else (
            utillib:prepareFreeFormMarkupWithLanguageForUpdate($concept/name)
            ,
            for $node in $concept/synonym
            return 
                utillib:prepareFreeFormMarkupWithLanguageForUpdate($node)
            ,
            utillib:prepareFreeFormMarkupWithLanguageForUpdate($concept/desc)
            ,
            utillib:prepareFreeFormMarkupWithLanguageForUpdate($concept/source)
            ,
            utillib:prepareFreeFormMarkupWithLanguageForUpdate($concept/rationale)
            ,
            for $node in $concept/comment
            return 
                utillib:prepareFreeFormMarkupWithLanguageForUpdate($node)
            ,
            (:new since 2015-04-21:)
            utillib:prepareConceptPropertyForUpdate($concept/property)
            ,
            (:new since 2015-04-21:)
            utillib:prepareDatasetRelationshipForUpdate($concept/relationship)
            ,
            utillib:prepareFreeFormMarkupWithLanguageForUpdate($concept/operationalization)
        )
    }
    {
        if ($concept[contains]) then () 
        else
        if ($concept[inherit][concept]) then (
            (: when you select edit for a group, only the group level comes to the client. If you hit save 
            (e.g. after adding a comment somewhere) you would delete the stored children. So we check if the thing
            coming in has children of its own. When it does, it must have been for a new inherit in which case we 
            purposely want to overwrite the stored version of the group :)
            for $child in $concept/concept
            return 
                if ($child[concept]) 
                then deapi:prepareGroupForUpdate($child, utillib:getConcept($child/@id, $child/@effectiveDate)) 
                else deapi:prepareItemForUpdate($child, utillib:getConcept($child/@id, $child/@effectiveDate))
        ) 
        else (
            (: just get whatever was stored before under this group :)
            $storedGroup/concept
        )
    }
    </concept>
};

declare function deapi:handleConceptValueDomain($in as element(valueDomain)?) as element(valueDomain)? {
    if ($in[@type = $deapi:VALUEDOMAIN-TYPE/enumeration/@value]) then
        element {name($in)} {
            $in/@type,
            for $n in $in/property
            let $prop   :=
                <property>
                {
                    if ($in/@type=('count')) then (
                        $n/(@minInclude, @maxInclude, @default, @fixed)[not(. = '')]
                    )
                    else if ($in/@type=('decimal')) then (
                        $n/(@minInclude, @maxInclude, @fractionDigits, @default, @fixed)[not(. = '')]
                    )
                    else if ($in/@type=('duration','quantity')) then (
                        $n/(@minInclude, @maxInclude, @fractionDigits, @default, @fixed, @unit, @currency)[not(. = '')]
                    )
                    else if ($in/@type=('date','datetime','time')) then (
                        $n/(@timeStampPrecision, @default, @fixed)[not(. = '')]
                    )
                    else if ($in/@type=('code','score','ordinal','boolean')) then (
                        $n/(@fractionDigits, @default, @fixed)[not(. = '')]
                    )
                    else if ($in/@type=('string','text','identifier','blob')) then (
                        $n/(@minLength, @maxLength, @default, @fixed)[not(. = '')]
                    ) 
                    else ()
                }
                </property>
            return
                $prop[@*]
            ,
            for $n in $in[@type = ('code', 'ordinal')]/conceptList[utillib:isOid(@id) or utillib:isOid(@ref)]
            return
                <conceptList>
                {
                    $n/@id | $n/@ref,
                    for $sn in $n/concept[utillib:isOid(@id)]
                    return
                        <concept>
                        {
                            $sn/@id,
                            $sn/@exception[. = ('true', 'false')],
                            $sn/@type[. = $deapi:VOCAB-TYPE/enumeration/@value],
                            $sn/@level[. castable as xs:integer],
                            $sn/@ordinal[not(. = '')],
                            utillib:prepareFreeFormMarkupWithLanguageForUpdate($sn/name)
                            ,
                            (: don't deduplicate by language so we do for loop here :)
                            for $ssn in $sn/synonym
                            return
                                utillib:prepareFreeFormMarkupWithLanguageForUpdate($ssn)
                            ,
                            utillib:prepareFreeFormMarkupWithLanguageForUpdate($sn/desc)
                        }
                        </concept>
                }
                </conceptList>
            ,
            for $n in $in/example[exists(node())][.//text()[not(normalize-space() = '')]]
            return
                <example>
                {
                    $n/@type[. = $deapi:EXAMPLE-TYPE/enumeration/@value],
                    $n/@caption[not(. = '')],
                    $n/node()
                }
                </example>
        }
    else ()
};

(:~ Retrieves DECOR concept maps based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
@param $id                          - required parameter denoting the id of the concept
@param $effectiveDate               - required parameter denoting the effectiveDate of the concept. If not given assumes latest version for id
@param $transactionId               - required parameter denoting the id of the transaction that the concept is in
@param $transactionEffectiveDate    - optional parameter denoting the effectiveDate of the transaction. If not given assumes latest version for id
@param $projectVersion              - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
@param $projectLanguage             - optional parameter to select from a specific compiled language
@param $associations                - optional boolean parameter relevant if $treeonly = 'false' to include associations: terminologyAssociation, identifierAssociation
@return as-is or as compiled as JSON
@since 2020-05-03
:)
declare function deapi:getConceptAssociationList($request as map(*)) {

    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $transactionId                  := $request?parameters?transactionId
    let $transactionEffectiveDate       := $request?parameters?transactionEffectiveDate[string-length() gt 0]
    let $projectVersion                 := $request?parameters?release
    let $projectLanguage                := $request?parameters?language
    let $associationMode                := $request?parameters?mode
    
    let $result                         := deapi:getConceptAssociationList($id, $effectiveDate, $transactionId, $transactionEffectiveDate, $projectVersion, $projectLanguage, $associationMode)

    return
        <list artifact="MP" current="{count($result/*)}" total="{count($result/*)}" all="{count($result/*)}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements($result/*)
        }
        </list>
};

(:~ Update DECOR dataset or transaction concept. Expect array of parameter objects, each containing RFC 6902 compliant contents. Note: RestXQ does not do PATCH (yet), but that would be the preferred option.

{ "op": "[add|remove|replace]", "path": "/", "value": "[terminologyAssociation|identifierAssociation]" }

where
* op - add (object associations, tracking, event) or remove (object associations only) or replace (displayName, priority, type, tracking, assignment)
* path - / 
* value - terminologyAssociation|identifierAssociation object
@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $id                       - required. the id for the issue to update 
@param $request-body             - required. json body containing array of parameter objects each containing RFC 6902 compliant contents
@return issue structure including generated meta data
@since 2020-05-03
@see http://tools.ietf.org/html/rfc6902
:)
declare function deapi:patchConceptAssociation($request as map(*)) {

    let $authmap                        := $request?user
    let $deid                           := $request?parameters?id
    let $deed                           := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $transactionId                  := $request?parameters?transactionId
    let $transactionEffectiveDate       := $request?parameters?transactionEffectiveDate[string-length() gt 0]
    let $data                           := utillib:getBodyAsXml($request?body, 'parameters', ())
    
    (:let $s                      := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data) :)
    
    let $check                  :=
        if ($data) then () else (
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        )
    
    let $result                         := deapi:patchConceptAssociation($authmap, string($deid), $deed, $transactionId, $transactionEffectiveDate, $data)
    return (
        roaster:response(200, 
            <list artifact="MP" current="{count($result/*)}" total="{count($result/*)}" all="{count($result/*)}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
            {
                $result/(@* except (@artifact | @current | @total | @all)),
                utillib:addJsonArrayToElements($result/*)
            }
            </list>)
    )

};

(:~ Retrieve DECOR concept map list from a transaction or dataset :)
declare function deapi:getConceptAssociationList($id as xs:string, $effectiveDate as xs:string?, $transactionId as xs:string?, $transactionEffectiveDate as xs:string?, $projectVersion as xs:string?, $projectLanguage as xs:string?, $associationMode as xs:string?) {
    let $id                         := $id[not(. = '')]
    let $effectiveDate              := $effectiveDate[not(. = '')]
    let $projectVersion             := $projectVersion[not(. = '')]
    let $projectLanguage            := $projectLanguage[not(. = '')]
    let $transactionId              := $transactionId[not(. = '')]
    let $transactionEffectiveDate   := $transactionEffectiveDate[not(. = '')]
    let $associationMode            := if ($associationMode) then $associationMode else if (empty($transactionId)) then 'normal' else 'all'
    
    let $storedConcept              :=
        if (empty($id)) then 
            () 
        else 
        if (empty($transactionId)) then 
            utillib:getConcept($id, $effectiveDate, $projectVersion, $projectLanguage) 
        else (
            utillib:getTransactionConcept($id, $effectiveDate, $transactionId, $transactionEffectiveDate, $projectVersion, $projectLanguage)
        )
    
    let $datasetConcept             := 
        if ($storedConcept) then
            if (empty($transactionId)) then
                $storedConcept
            else (
                let $dsid   := $storedConcept/ancestor::representingTemplate/@sourceDataset
                let $dsed   := $storedConcept/ancestor::representingTemplate/@sourceDatasetFlexibility
                
                return
                utillib:getDatasetConcept($dsid, $dsed, $storedConcept/@ref, $storedConcept/@flexibility, $projectVersion, $projectLanguage)
            )
        else ()
    let $originalConcept            := if ($datasetConcept) then utillib:getOriginalForConcept($datasetConcept) else ()
    
    return
        if ($storedConcept) then
            utillib:getConceptAssociations($storedConcept, $originalConcept, false(), $associationMode, true())
        else ()
};

(:~ Central logic for patching an existing dataset or transaction concept map

@param $authmap         - required. Map derived from token
@param $id              - required. DECOR concept/@id to update
@param $effectiveDate   - required. DECOR concept/@effectiveDate to update
@param $data            - required. DECOR concept xml element containing everything that should be in the updated concept
@return concept object as xml with json:array set on elements
:)
declare function deapi:patchConceptAssociation($authmap as map(*), $id as xs:string, $effectiveDate as xs:string, $transactionId as xs:string?, $transactionEffectiveDate as xs:string?, $data as element(parameters)) {

    let $associationMode            := if ($transactionId) then 'all' else 'normal'
    let $projectVersion             := ()
    let $projectLanguage            := ()
    let $id                         := $id[not(. = '')]
    let $effectiveDate              := $effectiveDate[not(. = '')]
    let $transactionId              := $transactionId[not(. = '')]
    let $transactionEffectiveDate   := $transactionEffectiveDate[not(. = '')]
    
    let $storedConcept              :=
        if (empty($id)) then 
            () 
        else 
        if (empty($transactionId)) then 
            utillib:getConcept($id, $effectiveDate, $projectVersion, $projectLanguage) 
        else (
            utillib:getTransactionConcept($id, $effectiveDate, $transactionId, $transactionEffectiveDate, $projectVersion, $projectLanguage)
        )
    
    let $check                  :=
        if ($storedConcept) then () else
        if ($transactionId) then 
            error($errors:BAD_REQUEST, 'Concept with id ' || $id || ' and effectiveDate ' || $effectiveDate || ' does not exist in transaction with id ' || $transactionId || ' and effectiveDate ' || $transactionEffectiveDate)
        else (
            error($errors:BAD_REQUEST, 'Concept with id ' || $id || ' and effectiveDate ' || $effectiveDate || ' does not exist')
        )
    
    let $decor                      := $storedConcept/ancestor::decor
    let $projectPrefix              := $decor/project/@prefix
    
    let $check                  :=
        if (empty($transactionId)) then
            if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-DATASETS)) then () else (
                error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify datasets in project ', $projectPrefix, '. You have to be an active author in the project.'))
            )
        else (
            if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-SCENARIOS)) then () else (
                error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify scenarios in project ', $projectPrefix, '. You have to be an active author in the project.'))
            )
        )
    
    let $datasetConcept                 := 
        if ($storedConcept[ancestor::dataset]) then $storedConcept else
        if ($storedConcept[@id]) then (utillib:getConcept($storedConcept/@id, $storedConcept/@effectiveDate)) else
        if ($storedConcept[@ref]) then (utillib:getConcept($storedConcept/@ref, $storedConcept/@flexibility[. castable as xs:dateTime])) else ()
    let $originalConcept                := if ($datasetConcept) then utillib:getOriginalForConcept($datasetConcept) else ()
    let $originalConceptLists           :=
        for $ref in $originalConcept/valueDomain/conceptList
        return
            utillib:getOriginalConceptList($ref)
    
    let $associationIds         :=
        $storedConcept/(@id | @ref | valueDomain/conceptList/@id | valueDomain/conceptList/@ref | valueDomain/conceptList/concept/@id) |
        $datasetConcept/(@id | @ref | valueDomain/conceptList/@id | valueDomain/conceptList/@ref | valueDomain/conceptList/concept/@id) |
        $originalConcept/(@id | @ref | valueDomain/conceptList/@id | valueDomain/conceptList/@ref | valueDomain/conceptList/concept/@id) |
        $originalConceptLists/(@id | concept/@id)
    
    let $check                  :=
        if ($data[parameter]) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall be an array of ''parameter''.')
        )
    let $unsupportedops         := $data/parameter[not(@op = ('add', 'replace', 'remove'))]
    let $check                  :=
        if ($unsupportedops) then
            error($errors:BAD_REQUEST, 'Submitted parameters shall have supported ''op'' value. Found ''' || string-join(distinct-values($unsupportedops/@op), ''', ''') || ''', expected ''add'', ''replace'', or ''remove''') 
        else ()
    
    (: There are three types of associations;
    1. DatasetConcepts. Expect concept/@id=@conceptId, optionally concept/@effectiveDate=@conceptFlexibility, @code+@codeSystem+@displayName + optionally @equivalence
    2. Concept value domain conceptlist. Expect concept/@id=@conceptId, No @conceptFlexibility, @ref + optionally @flexibility and/or @strength
    3. Concept value domain conceptlist concept. Expect concept/@id=@conceptId, No @conceptFlexibility, @code+@codeSystem+@displayName + optionally @equivalence
    :)
    let $check                  :=
        for $param in $data/parameter
        let $op         := $param/@op
        let $path       := $param/@path
        let $elmname    := substring-after($param/@path, '/')
        let $value      := $param/value/*[name() = $elmname]
        return
            switch ($path)
            case '/identifierAssociation' (:fall through :)
            case '/terminologyAssociation' return (
                if (count($value) = 1) then (
                    if ($value[@conceptId = $id]) then (
                        if ($value[@conceptFlexibility = $effectiveDate]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Association to the concept itself SHALL have a matching effectiveDate ' || $effectiveDate || '. Found  conceptFlexibility "' || $value/@conceptFlexibility || '"'
                        ),
                        if ($param[@valueSet | @flexibility]) then 
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Association to a concept SHALL not bind to a value set. Found: "' || string-join(($value/@valueSet, $value/@flexibility), ' - ') || '"'
                        else ()
                    )
                    else
                    if ($value[@conceptId = $originalConceptLists/@id]) then (
                        if ($param[@conceptFlexibility]) then 
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Association to a conceptList SHALL not have conceptFlexibility. Found: "' || $value/@conceptFlexibility || '"'
                        else (),
                        if ($param[@code | @codeSystem | @displayName]) then 
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Association to a conceptList SHALL not bind to a single code. Found: "' || string-join(($value/@code, $value/@codeSystem, $value/@displayName), ' - ') || '"'
                        else ()
                    )
                    else
                    if ($value[@conceptId = $originalConceptLists/concept/@id]) then (
                        if ($param[@conceptFlexibility]) then 
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Association to a conceptList.concept SHALL not have conceptFlexibility. Found: "' || $value/@conceptFlexibility || '"'
                        else (),
                        if ($param[@valueSet | @flexibility]) then 
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Association to a conceptList.concept SHALL not bind to a value set. Found: "' || string-join(($value/@valueSet, $value/@flexibility), ' - ') || '"'
                        else ()
                    )
                    else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || '''. Association with conceptId "' || $value/@conceptId || '" SHALL be to the target concept (' || $id || '), or one of its conceptLists, or one of the concepts in such conceptList.'
                    ),
                    if ($value[@strength]) then
                        if ($value[@strength = $deapi:CODINGSTRENGTH-TYPE/enumeration/@value]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Association strength "' || $value/@strength || '" not supported. Supported are: ', string-join($deapi:CODINGSTRENGTH-TYPE/enumeration/@value, ' ')
                        )
                    else (),
                    if ($value[@equivalence]) then 
                        if ($value[@equivalence = $deapi:EQUIVALENCY-TYPE/enumeration/@value]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Association equivalence "' || $value/@equivalence || '" not supported. Supported are: ', string-join($deapi:EQUIVALENCY-TYPE/enumeration/@value, ' ')
                        )
                    else (),
                    if ($op = 'remove') then () else
                    if ($value[@conceptId]) then
                        if ($value[@valueSet]) then (
                            if ($value[@code | @codeSystem | @displayName | @ref]) then
                                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Association with valueSet SHALL NOT have code or codeSystem or displayName or ref.'
                            else (),
                            let $valueSet                   := 
                                if ($value[@valueSet]) then 
                                    vsapi:getValueSet(map { "parameters": map { "id": $value/@valueSet, "effectiveDate": $value/@flexibility } }) 
                                else ()
                            return
                            if (empty($valueSet)) then 
                                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Association with valueSet SHALL point to an existing value set. Could not find: ' || $param/@valueSet
                            else ()
                        )
                        else 
                        if ($value[@code]) then (
                            if ($value[@valueSet | @flexibility | @ref]) then
                                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Association with a code SHALL NOT have valueSet or flexibility or ref.'
                            else 
                            if ($value[@codeSystem][@displayName]) then () else (
                                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Association with a code SHALL have codeSystem and displayName.'
                            ),
                            if ($value[matches(@code, '^\S+')]) then () else (
                                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Association with a code SHALL NOT contain whitespace. Hint for SNOMED CT expressions: use concept ids only, e.g. 363679005:260686004=312250003,405813007=76752008'
                            ),
                            if ($value[utillib:isOid(@codeSystem)]) then () else (
                                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Association codeSystem SHALL be a valid oid. Found: "' || $value/@codeSystem || '"'
                            )
                        )
                        else
                        if ($value[@ref]) then (
                            if ($value[@valueSet | @flexibility | @code | @codeSystem | @displayName]) then
                                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Association with a ref SHALL NOT have both valueSet or flexibility or code or codeSystem or displayName.'
                            else (),
                            if ($value[utillib:isOid(@ref)]) then () else (
                                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Association ref SHALL be a valid oid. Found: "' || $value/@ref || '"'
                            )
                        )
                        else 
                        if ($transactionId) then (
                            (: this signals override of anything in the dataset :)
                        ) 
                        else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Association SHALL have at least valueSet, code or ref'
                        )
                    else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || '''. Association SHALL have conceptId.'
                    )
                ) else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || '''. value SHALL contain a single ' || $elmname || ' element/object. Found: ' || count($value) 
                )
            )
            default return (
                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Path value not supported'
            )
     let $check                 :=
        if (empty($check)) then () else (
            error($errors:BAD_REQUEST,  string-join ($check, ' '))
        )
    
    let $update                 :=
        for $param in $data/parameter
        return
            switch ($param/@path)
            case '/terminologyAssociation' return (
                (: only one per language :)
                let $elmname  := substring-after($param/@path, '/')
                let $new      := utillib:prepareTerminologyAssociationForUpdate($param/value/*[name() = $elmname], not(empty($transactionId)))
                let $valueSet := 
                    if ($new[@valueSet]) then 
                        vsapi:getValueSet(map { "parameters": map { "id": $new/@valueSet, "effectiveDate": $new/@flexibility } }) 
                    else ()
                (: if the transaction concept does not have any listings yet for this id, we copy what the dataset concept has first so we can work from there :)
                let $add      :=
                    if ($storedConcept[ancestor::transaction][empty(*[@conceptId = $new/@conceptId])]) then (
                        let $datasetConceptAssociations := 
                            for $assoc in utillib:getConceptAssociations($datasetConcept)
                            return
                                if ($assoc[self::terminologyAssociation]) then
                                    utillib:prepareTerminologyAssociationForUpdate($assoc, not(empty($transactionId)))
                                else
                                if ($assoc[self::identifierAssociation]) then
                                    utillib:prepareIdentifierAssociationForUpdate($assoc, not(empty($transactionId)))
                                else (
                                    $assoc
                                )
                        
                        return
                            if ($datasetConceptAssociations[@conceptId = $new/@conceptId]) then update insert $datasetConceptAssociations[@conceptId = $new/@conceptId] into $storedConcept else () 
                    ) else ()
                (: should not have multiple connections to the same valueSet (regardless of version), so delete those first in case we're updating strength :)
                (: should not have multiple connections to the same code/codeSystem (regardless of version), so delete those first in case we're updating equivalence :)
                let $delete   := 
                    if ($new[@valueSet]) then 
                        if ($transactionId) then (
                            update delete $storedConcept/terminologyAssociation[@conceptId = $new/@conceptId][empty(@valueSet | @code)],
                            update delete $storedConcept/terminologyAssociation[@conceptId = $new/@conceptId][@valueSet = $new/@valueSet]
                        )
                        else (
                            update delete $decor/terminology/terminologyAssociation[@conceptId = $new/@conceptId][@valueSet = $new/@valueSet]
                        )
                    else
                    if ($new[@code]) then
                        if ($transactionId) then (
                            update delete $storedConcept/terminologyAssociation[@conceptId = $new/@conceptId][empty(@valueSet | @code)],
                            update delete $storedConcept/terminologyAssociation[@conceptId = $new/@conceptId][@code = $new/@code][@codeSystem = $new/@codeSystem]
                        )
                        else (
                            update delete $decor/terminology/terminologyAssociation[@conceptId = $new/@conceptId][@code = $new/@code][@codeSystem = $new/@codeSystem]
                        )
                    else
                    if ($transactionId) then
                        update delete $storedConcept/terminologyAssociation[@conceptId = $new/@conceptId]
                    else ()
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return (
                    let $updateTerminology          :=
                        if ($decor/terminology) then () else (
                            update insert <terminology/> following $decor/ids
                        )
                    let $updateAssociation          :=
                        if ($transactionId) then (
                            update insert $new into $storedConcept
                        )
                        else
                        if ($decor/terminology/terminologyAssociation) then (
                            update insert $new following $decor/terminology/terminologyAssociation[last()]
                        )
                        else 
                        if ($decor/terminology/*) then (
                            update insert $new preceding $decor/terminology/*[1]
                        )
                        else (
                            update insert $new into $decor/terminology
                        )
                    let $updateAssociation          :=
                        if (empty($valueSet)) then () else (
                            deapi:addValueSetRef($decor, $valueSet/@ident, $valueSet/@url, $valueSet/@id, $valueSet/@name, ($valueSet/@displayName, $valueSet/@name)[1])
                        )
                    
                    return ()
                )
                case ('remove') return (
                    (: if we removed the last terminologyAssociation from a transaction concept then we need to 
                    add a marker that makes the 'non-binding' explicit, otherwise the dataset bindings would be reinstated :)
                    if ($transactionId) then
                        if ($new[empty(@code | @codeSystem | @valueSet)]) then ((: we were asked to remove an empty terminologyAssociation. this was already done before this line, so nothing left to do :)) else
                        if ($storedConcept[empty(terminologyAssociation[@conceptId = $new/@conceptId])]) then (
                            let $new    := <terminologyAssociation>{$new/@conceptId | $new/@conceptFlexibility | $new/@effectiveDate | $new/@lastModifiedDate | $new/@expirationDate | $new/@officialReleaseDate}</terminologyAssociation>
                            return
                            update insert utillib:prepareTerminologyAssociationForUpdate($new, true()) into $storedConcept
                        ) else ()
                    else ()
                )
                default return ( (: unknown op :) )
            )
            case '/identifierAssociation' return (
                (: only one per language :)
                let $elmname  := substring-after($param/@path, '/')
                let $new      := utillib:prepareIdentifierAssociationForUpdate($param/value/*[name() = $elmname], not(empty($transactionId)))
                (: if the transaction concept does not have any listings yet for this id, we copy what the dataset concept has first so we can work from there :)
                let $add      :=
                    if ($storedConcept[ancestor::transaction][empty(*[@conceptId = $new/@conceptId])]) then (
                        let $datasetConceptAssociations := 
                            for $assoc in utillib:getConceptAssociations($datasetConcept)
                            return
                                if ($assoc[self::terminologyAssociation]) then
                                    utillib:prepareTerminologyAssociationForUpdate($assoc, not(empty($transactionId)))
                                else
                                if ($assoc[self::identifierAssociation]) then
                                    utillib:prepareIdentifierAssociationForUpdate($assoc, not(empty($transactionId)))
                                else (
                                    $assoc
                                )
                        
                        return
                            if ($datasetConceptAssociations) then update insert $datasetConceptAssociations[@conceptId = $new/@conceptId] into $storedConcept else () 
                    ) else ()
                let $delete   := 
                    if ($new[@ref]) then 
                        if ($transactionId) then (
                            update delete $storedConcept/identifierAssociation[@conceptId = $new/@conceptId][empty(@ref)],
                            update delete $storedConcept/identifierAssociation[@conceptId = $new/@conceptId][@ref = $new/@ref]
                        )
                        else (
                            update delete $decor/ids/identifierAssociation[@conceptId = $new/@conceptId][@ref = $new/@ref]
                        )
                    else
                    if ($transactionId) then
                        update delete $storedConcept/identifierAssociation[@conceptId = $new/@conceptId]
                    else ()
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return (
                    if ($transactionId) then (
                        update insert $new into $storedConcept
                    )
                    else
                    if ($decor/ids/*) then (
                        update insert $new following $decor/ids/*[last()]
                    )
                    else (
                        update insert $new into $decor/ids
                    )
                )
                case ('remove') return (
                    (: if we removed the last identifierAssociation from a transaction concept then we need to 
                    add a marker that makes the 'non-binding' explicit, otherwise the dataset bindings would be reinstated :)
                    if ($transactionId) then
                        if ($new[empty(@ref)]) then ((: we were asked to remove an empty identifierAssociation. this was already done before this line, so nothing left to do :)) else
                        if ($storedConcept[empty(identifierAssociation[@conceptId = $new/@conceptId])]) then (
                            let $new    := <identifierAssociation>{$new/@conceptId | $new/@conceptFlexibility | $new/@effectiveDate | $new/@lastModifiedDate | $new/@expirationDate | $new/@officialReleaseDate}</identifierAssociation>
                            return
                            update insert utillib:prepareIdentifierAssociationForUpdate($new, true()) into $storedConcept
                        ) else ()
                    else ()
                )
                default return ( (: unknown op :) )
            )
            default return ( (: unknown path :) )
    
    return
        deapi:getConceptAssociationList($id, $effectiveDate, $transactionId, $transactionEffectiveDate, $projectVersion, $projectLanguage, $associationMode)
};

declare %private function deapi:addValueSetRef($decor as element(), $repoPrefix as xs:string, $repoUrl as xs:string, $valueSetId as xs:string, $valueSetName as xs:string, $valueSetDisplayName as xs:string) as item()* {
    
    let $valueSetRefElm     := <valueSet ref="{$valueSetId}" name="{$valueSetName}" displayName="{$valueSetDisplayName}"/>
    let $buildingBlockElm   := <buildingBlockRepository url="{$repoUrl}" ident="{$repoPrefix}"/>
    
    let $addValueSetRef      :=
        if ($decor//valueSet[@id = $valueSetId] | $decor//valueSet[@ref = $valueSetId]) then () else (
            let $dummy1 := update insert $valueSetRefElm following $decor/terminology/*[last()]
            let $dummy2 := 
                if ($decor/project/buildingBlockRepository[@url=$buildingBlockElm/@url][@ident=$buildingBlockElm/@ident][empty(@format)] |
                    $decor/project/buildingBlockRepository[@url=$buildingBlockElm/@url][@ident=$buildingBlockElm/@ident][@format='decor']) then ('false') else (
                    update insert $buildingBlockElm following $decor/project/(author|reference|restURI|defaultElementNamespace|contact)[last()]
                )
            
            return 
                if ($dummy2='false') then 'ref' else 'ref-and-bbr'
        )
    
    return ()
};