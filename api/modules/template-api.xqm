xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:~ Template API allows read, create, update of DECOR templates :)
module namespace tmapi              = "http://art-decor.org/ns/api/template";

import module namespace roaster     = "http://e-editiones.org/roaster";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

import module namespace utillib     = "http://art-decor.org/ns/api/util" at "library/util-lib.xqm";
import module namespace utilhtml    = "http://art-decor.org/ns/api/util-html" at "library/util-html-lib.xqm";
import module namespace utilsvg     = "http://art-decor.org/ns/api/util-svg" at "library/util-svg-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "library/settings-lib.xqm";
import module namespace decorlib    = "http://art-decor.org/ns/api/decor" at "library/decor-lib.xqm";
import module namespace histlib     = "http://art-decor.org/ns/api/history" at "library/history-lib.xqm";
import module namespace ggapi       = "http://art-decor.org/ns/api/governancegroups" at "governancegroups-api.xqm";
import module namespace vsapi       = "http://art-decor.org/ns/api/valueset" at "valueset-api.xqm";
import module namespace tm          = "http://art-decor.org/ns/decor/template" at "../../art/api/api-decor-template.xqm";
import module namespace tcsapi      = "http://art-decor.org/ns/api/terminology/codesystem" at "terminology-codesystem-api.xqm";

declare namespace json      = "http://www.json.org";
declare namespace rest      = "http://exquery.org/ns/restxq";
declare namespace resterr   = "http://exquery.org/ns/restxq/error";
declare namespace http      = "http://expath.org/ns/http-client";
declare namespace output    = "http://www.w3.org/2010/xslt-xquery-serialization";

(:relevant for template list. If treetype is 'limited' and param id or name is valued then a list is built for just this id or name.:)
declare variable $tmapi:TREETYPELIMITED                     := 'limited';
(:relevant for template list. If treetype is 'marked' and param id or name is valued then a full list is built where every template hanging off this template is marked as such.:)
declare variable $tmapi:TREETYPEMARKED                      := 'marked';
(:relevant for template list. If treetype is 'marked' and param id or name is valued then a limited list is built containing only templates hanging off this template.:)
declare variable $tmapi:TREETYPELIMITEDMARKED               := 'limitedmarked';

declare %private variable $tmapi:STATUSCODES-FINAL          := ('active', 'cancelled', 'pending', 'review', 'rejected', 'retired');
declare %private variable $tmapi:ADDRLINE-TYPE              := utillib:getDecorTypes()/AddressLineType;
declare %private variable $tmapi:TEMPLATE-TYPES             := utillib:getDecorTypes()/TemplateTypes;
declare %private variable $tmapi:TEMPLATE-FORMATS           := utillib:getDecorTypes()/TemplateFormats;
declare %private variable $tmapi:RELATIONSHIP-TYPES         := utillib:getDecorTypes()/RelationshipTypes;
declare %private variable $tmapi:EXAMPLE-TYPES              := utillib:getDecorTypes()/ExampleType;

(:~ Retrieves latest DECOR template based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the template
    @param $projectPrefix           - optional. limits scope to this project only
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @return as-is or as compiled as JSON
    @since 2020-05-03
:)
declare function tmapi:getLatestTemplate($request as map(*)) {
    tmapi:getTemplate($request)
};

(:~ Retrieves latest DECOR template based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
@param $id                      - required parameter denoting the id of the template
@param $effectiveDate           - optional parameter denoting the effectiveDate of the template. If not given assumes latest version for id
@param $projectPrefix           - optional. limits scope to this project only
@param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
@return as-is or as compiled as JSON
@since 2020-05-03
:)
declare function tmapi:getTemplate($request as map(*)) {
    let $projectPrefix      := $request?parameters?prefix[not(. = '')]
    let $projectVersion     := $request?parameters?release[not(. = '')]
    let $projectLanguage    := $request?parameters?language[not(. = '')]
    let $id                 := $request?parameters?id[not(. = '')]
    let $effectiveDate      := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }

    let $results            := tmapi:getTemplate($id, ($effectiveDate, 'dynamic')[1], $projectPrefix, $projectVersion, $projectLanguage)
    
    (: if multiple copies are found, prefer copies from this server of other copies. :)
    let $results            :=
        if (count($results) gt 1) then (
            let $t := $results[@id][@url = $utillib:strDecorServicesURL] | $results[@id][empty(@url)]
            let $t := if ($t) then $t else $results[@id]
            return
                if ($t) then $t else $results
        )
        else (
            $results
        )
    
    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else
        if (count($results) gt 1) then (
            error($errors:SERVER_ERROR, 'Found multiple templates for id ''' || $id || '''. Expected 0..1. Alert your administrator as this should not be possible. Found in: ' || string-join($results/concat(@ident, ' / ', @url), ', '))
            (:<list xmlns:json="http://www.json.org">
            {
                utillib:addJsonArrayToElements($results)
            }
            </list>:)
        )
        else (
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
        )
};

(:~ Retrieves DECOR template as a diagram based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the dataset
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the dataset. If not given assumes latest version for id
    @param $projectPrefix           - optional. limits scope to this project only
    @param $projectLanguage         - optional parameter to select from a specific compiled language
    @param $format                  - optional. overrides the accept-header for frontend purposes, default svg if not calculated
    @return as-is 
    @since 2023-12-21
:)
declare function tmapi:getTemplateDiagram($request as map(*)) {

    let $project                := $request?parameters?project[not(. = '')]
    let $projectLanguage        := $request?parameters?language[not(. = '')]
    (: no format given: default format is 'svg' :)
    let $format                 := $request?parameters?format[not(. = '')]
    
    let $id                     := $request?parameters?id[not(. = '')]
    let $effectiveDate          := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
       
    (: no format given: default format is 'svg' :)
    let $format                 :=
        if (empty($format)) then 'svg' else $format
           
    (: 
       overwrite format in the backend is not always possible because of middleware behaviour
       - if in backend the format is xml and accept is not application/xml, roaster always gives a json payload 
    :) 
    let $check                  :=
        if ($format = ('xml', 'hlist') and not((roaster:accepted-content-types()[. = ('image/svg+xml', 'application/xml')])[1] = 'application/xml')) then
            error($errors:BAD_REQUEST, 'In case of format parameter is xml or hlist the accept header should be application/xml')
        else ()   
    
    (: no laguage given: default default language is art-language :)
    let $projectLanguage        :=
        if (empty($projectLanguage)) then $setlib:strArtLanguage else $projectLanguage
    
    let $projectLanguage        :=
        if ($projectLanguage = (utillib:getArtLanguages())) then $projectLanguage else 'en-US'
    
    let $projectPrefix                  := 
        if (empty($project)) then () else utillib:getDecor($project, (), $projectLanguage)/project/@prefix 

    let $templates              :=
        (: Return zero or more extracted templates  :)
        if (empty($projectPrefix)) then 
            tmapi:getTemplateById($id, $effectiveDate)[@id]
        else 
            tmapi:getTemplateById($id, $effectiveDate, $projectPrefix, (), $projectLanguage)[@id]
    
    (: first the template scan used for the html (hgraph and wiki) payload and xml hlist :)
    let $templateScan           :=
        if (count($templates)=1) then tmapi:templateScan($templates, $projectPrefix, $projectLanguage, 'template', 1, 1, ()) else () 
    
    (: templateChain for svg and xml representation :)
    let $templateChain          :=
        if(empty($templateScan)) then () else tmapi:chainCopy2(tmapi:chainCopy1(<tree>{$templateScan}</tree>, -1), 1)
    
    let $results          := 
        if (empty($templateScan)) then ()
        else if ($format = ('xml', 'hlist')) then (
            let $xml      :=  
                if ($format = 'xml') then (
                    <x project="{$projectPrefix}">
                         {
                             $templateChain,
                             <max>{max($templateChain//@len)}</max>
                         }
                    </x>
                )    
                else $templateScan
           return
           for $x in $xml
                return
                element {name($x)} {
                    $x/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($x/node(), $format)
                }
        )
        else if ($format=('fshlogicalmodel')) then (
            response:set-header('Content-Type','text/html; charset=utf-8'),
            tmapi:apply-fsh-stylesheet(($templateScan ! (.)), $format)
        )
        else if ($format=('hgraph', 'hgraphwiki', 'wikilist', 'transclusionwikilist')) then (
            response:set-header('Content-Type','text/html; charset=utf-8'),
            tmapi:apply-hgraph-stylesheet(($templateScan ! (.)), $format)
        )
        else (
            response:set-header('Content-Type','image/svg+xml'),
            response:set-header('X-Robots-Tag', 'noindex'), 
            utilsvg:convertTemplate2Svg($templateChain)
        ) 

    return
        if (empty($results)) then (
            roaster:response(404, ())
        )
        else $results
};

(:~ Retrieves DECOR template for publication based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the template
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the template. If not given assumes latest version for id
    @param $projectPrefix           - required. limits scope to this project only
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @param $language                - optional
    @param $tree                    - optional
    @return as-is or compiled as JSON
    @since 2023-11-17
:)
declare function tmapi:getTemplateExtract($request as map(*)) {

    let $project            := $request?parameters?project[not(. = '')]
    let $projectVersion     := $request?parameters?release[not(. = '')]
    let $projectLanguage    := $request?parameters?language[not(. = '')]
    let $tree               := $request?parameters?tree[not(. = '')]
    let $id                 := $request?parameters?id[not(. = '')]
    let $effectiveDate      := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    
    let $acceptTypes                    := roaster:accepted-content-types()
    let $acceptedType                   := ($acceptTypes[. = ('application/xml', 'application/json')],'application/json')[1]
    
    let $format                         := tokenize($acceptedType, '/')[2]
     
    let $projectPrefix                  := utillib:getDecor($project, $projectVersion, $projectLanguage)/project/@prefix 
    
    let $check                  :=
        if (empty($projectPrefix)) then
            error($errors:BAD_REQUEST, 'Project ' || $project || ' not found.')
        else ()
    
    let $results            := 
        if (not($tree = 'expand')) 
            then tmapi:getTemplateExtract($projectPrefix, $projectVersion, $projectLanguage, $id, $effectiveDate)
            else tmapi:getExpandedTemplateExtract($projectPrefix, $projectVersion, $projectLanguage, $id, $effectiveDate)
   
    let $results            :=  
        if ($tree = ('wrap', 'expand')) then $results else ($results/template/template)[1] 

    return
        if (empty($results/*)) then (
            roaster:response(404, ())
        )
        else (
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/node(), $format)
                } 
        )
};

(:~ Retrieves DECOR template view based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the template
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the template. If not given assumes latest version for id
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @param $projectLanguage         - optional parameter to select from a specific compiled language, also ui-language
    @param $inline                  - optional parameter to omit HTML header info. Useful for inclusion of HTML in other pages.
    @param $collapsable             - optional parameter the valueset is collapable.
    @return as-is 
    @since 2024-10-14
:)

declare function tmapi:getTemplateView($request as map(*)) {

    let $project                        := $request?parameters?project[not(. = '')]
    let $projectVersion                 := $request?parameters?release[not(. = '')]
    let $projectLanguage                := $request?parameters?language[not(. = '')]
    let $inline                         := $request?parameters?inline = true()
    let $collapsable                    := not($request?parameters?collapsable = false())
    
    let $id                             := $request?parameters?id[not(. = '')]
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    
    let $decor                          := utillib:getDecor($project, $projectVersion, $projectLanguage)
    
    let $check                  :=
        if (empty($decor/project/@prefix)) then
            error($errors:BAD_REQUEST, 'Project ' || $project || ' not found.')
        else ()    
     
    let $results                        := tmapi:getExpandedTemplateExtract($decor/project/@prefix, $projectVersion, $projectLanguage, $id, $effectiveDate)            
                
    let $r-header                       := response:set-header('Content-Type', 'text/html; charset=utf-8')
    
    return
        if (empty($results/*)) then (
            roaster:response(404, ())
        )

        else 
        
        (: prepare for Html :)
        let $language                       := if ($decor/project/name[@language = $projectLanguage]) then $projectLanguage else $decor/project/@defaultLanguage
        let $header                         := if ($inline) then false() else true()        
        
        return utilhtml:convertObject2Html($results, $language, $header, $collapsable, $projectVersion, $decor)

};


(:~ Returns a list of zero or more templates
    
    @param $projectPrefix    - optional. determines search scope. null is full server, pfx- limits scope to this project only
    @param $projectVersion   - optional. if empty defaults to current version. if valued then the template will come explicitly from that archived project version which is expected to be a compiled version
    @param $id               - optional. Identifier of the template to retrieve
    @param $name             - optional. Name of the template to retrieve (valueSet/@name)
    @param $effectiveDate    - optional. null gets all versions, 'dynamic' gets the newest version based on id or name, yyyy-mm-ddThh:mm:ss gets this specific version
    @param $treetype         - optional. Default $tmapi:TREETYPELIMITEDMARKED
    @param $resolve          - optional. Boolean. Default true if governanceGroupId is empty. If true retrieves all versions for references, otherwise just returns the references.
    @return Zero value sets in case no matches are found, one if only one exists or if a specific version was requested, or more if more versions exist and no specific version was requested
    @since 2013-06-14
:)
declare function tmapi:getTemplateList($request as map(*)) {
    let $governanceGroupId      := $request?parameters?governanceGroupId[not(. = '')]
    let $projectPrefix          := $request?parameters?prefix[not(. = '')]
    let $projectVersion         := $request?parameters?release[not(. = '')]
    let $projectLanguage        := $request?parameters?language[not(. = '')]
    let $searchTerms            := 
        array:flatten(
            for $s in $request?parameters?search[string-length() gt 0]
            return
                tokenize(lower-case($s),'\s')
        )
    let $id                     := $request?parameters?id[not(. = '')]
    let $nm                     := $request?parameters?name[not(. = '')]
    let $ed                     := $request?parameters?effectiveDate[not(. = '')]
    let $treetype               := $request?parameters?treetype
    let $resolve                :=  if (empty($governanceGroupId)) then not($request?parameters?resolve = false()) else $request?parameters?resolve = true()
    
    let $check                  :=
        if (count($governanceGroupId) + count($projectPrefix) = 0) then 
            error($errors:BAD_REQUEST, 'Request SHALL have either parameter governanceGroupId or prefix')
        else 
        if (count($governanceGroupId) + count($projectPrefix) gt 1) then 
            error($errors:BAD_REQUEST, 'Request SHALL have exactly one parameter governanceGroupId or prefix, not both or multiple')
        else
        if (count($governanceGroupId) gt 0 and not(empty($projectVersion))) then
            error($errors:BAD_REQUEST, 'Request SHALL NOT have both governanceGroupId and release of a single project')
        else
        if (count($governanceGroupId) gt 0 and not(empty($searchTerms))) then
            error($errors:BAD_REQUEST, 'Search only supported in a project')
        else ()

    let $results                :=
        tmapi:getTemplateList($governanceGroupId, $projectPrefix, $projectVersion, $projectLanguage, $searchTerms, $id, $ed, $treetype, $resolve)
        (:if (count($searchTerms) gt 0) then
            tmapi:searchTemplate($projectPrefix, $projectVersion, $projectLanguage, $searchTerms)
        else (
            tmapi:getTemplateList($governanceGroupId, $projectPrefix, $projectVersion, $projectLanguage, $searchTerms, $id, $ed, $treetype, $resolve)
        ):)
    
    let $countTN                := count($results/*/template/version)
    
    return
        <list artifact="TM" current="{$countTN}" total="{$countTN}" all="{$countTN}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements($results/*)
        }
        </list>
};

(:~ Returns a list of zero or more templateAssociation
    
    @param $projectPrefix    - required. determines search scope. limits scope to this project only
    @param $projectVersion   - optional. if empty defaults to current version. if valued then the template will come explicitly from that archived project version which is expected to be a compiled version
    @param $projectLanguage  - optional. works only in conjunction with $projectVersion. if empty defaults to *. if valued then the template will come explicitly from the compilation in that language. * is the "all languages" compilation
    @param $id               - optional. Identifier of the template to retrieve templateAssociations for
    @param $effectiveDate    - optional. null gets all versions, 'dynamic' gets the newest version based on id or name, yyyy-mm-ddThh:mm:ss gets this specific version
    @return list object with zero or more templateAssociation objects
    @since 2022-02-02
:)
declare function tmapi:getTemplateAssociationList($request as map(*)) {
    let $projectPrefix          := $request?parameters?prefix[not(. = '')]
    let $projectVersion         := $request?parameters?release[not(. = '')]
    let $projectLanguage        := $request?parameters?language[not(. = '')]
    let $id                     := $request?parameters?id[not(. = '')]
    let $ed                     := $request?parameters?effectiveDate[not(. = '')]
    
    let $check                  :=
        if (empty($projectPrefix)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have parameter prefix')
        else () 
        
    let $results                := tmapi:getTemplateAssociationList($projectPrefix, $projectVersion, $projectLanguage, $id, $ed)
    
    return
        $results
};

(:~ Deletes a template reference based on $id (oid) and project id or prefix
    @param $id                      - required parameter denoting the id of the template
    @param $project                 - required. limits scope to this project only
:)
declare function tmapi:deleteTemplate($request as map(*)) {
    
    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0][not(. = '*')]
    let $id                     := $request?parameters?id[string-length() gt 0]
    
    let $check                  :=
        if (empty($project) or empty($id)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have both parameter project and id')
        else ()
    
    let $decor                  := 
        if (utillib:isOid($project)) then
            utillib:getDecorById($project)
        else (
            utillib:getDecorByPrefix($project)
        )
    
    let $check                  :=
        if (empty($decor)) then
            error($errors:BAD_REQUEST, 'Project ' || $project || ' not found.')
        else ()
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-RULES)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify templates in project ', $project[1], '. You have to be an active author in the project.'))
        )
    
    let $delete                 := update delete $decor/rules/template[@ref = $id]
    let $delete                 := update delete $decor/rules/templateAssociation[@templateId = $id][empty(*)]
    
    return
        roaster:response(204, ())
};

(:~ Returns template datatypes
    
    @param $format           - optional. defaults to hl7v3xml1
    @return datatypes or http 404 if not found
    @since 2022-02-07
:)
declare function tmapi:getTemplateDatatypes($request as map(*)) {
    let $format                 := ($request?parameters?format[not(. = '')], 'hl7v3xml1')[1]
        
    let $decorDatatypes         := $setlib:colDecorCore/supportedDataTypes[@type = $format]

    let $results                :=
        if (empty($decorDatatypes)) then () else (
            <supportedDataTypes format="{$decorDatatypes/@type}">
            {
                $decorDatatypes/(@* except (@type | @xsi:*))
            }
            {
                let $allTypes   := $decorDatatypes//dataType | $decorDatatypes//flavor | $decorDatatypes//atomicDataType
                
                (: assume that if there is a definition for the dataType at root level,  then that is complete, whereas a subtyped dataType does not need to be:)
                for $typeDef in $allTypes
                let $typeName    := $typeDef/@name 
                group by $typeName
                order by $typeName
                return
                    <type>
                    {
                        $typeDef[1]/@name, 
                        $typeDef[1]/@type, 
                        $typeDef[1]/@hasStrength, 
                        if ($typeDef[1]/self::flavor) then attribute isFlavor {'true'} else (),
                        if ($typeDef[1][self::atomicDataType]) then attribute type {'simpletype'} else (),
                        $typeDef[1]/@realm 
                    }
                        <item name="{$typeName}"/>
                    {
                        for $subtype in distinct-values(($typeDef[1]//dataType | $typeDef[1]//flavor | $typeDef[1]//atomicDataType)/@name)
                        order by $subtype
                        return
                        <item name="{$subtype}"/>
                    }
                    {
                        $typeDef[1]/desc,
                        $typeDef[1]/attribute,
                        $typeDef[1]/element
                    }
                    </type>
            }
            </supportedDataTypes>
        )

    return
        if (empty($results)) then
            roaster:response(404, ())
        else (
            for $result in $results
            return
                element {name($result)} {
                    $result/@*,
                    namespace {"json"} {"http://www.json.org"},
                    utillib:addJsonArrayToElements($result/*)
                }
        )
};

declare function tmapi:getTemplate($id as xs:string, $effectiveDate as xs:string?, $projectPrefix as xs:string?, $projectVersion as xs:string?, $projectLanguage as xs:string?) as element(template)* {
    let $id                     := $id[not(. = '')]
    let $effectiveDate          := $effectiveDate[not(. = '')]
    let $projectPrefix          := $projectPrefix[not(. = '')]
    let $projectVersion         := $projectVersion[not(. = '')]
    
    let $templates              :=
        if (empty($projectPrefix)) then 
            tmapi:getExpandedTemplateById($id, $effectiveDate, false())
        else (
            tmapi:getExpandedTemplateById($id, $effectiveDate, $projectPrefix, $projectVersion, $projectLanguage, false())
        )
    let $results                := 
        for $template in $templates
        return
            tmapi:prepareTemplateForAPI($template)

    return
        $results
};

declare function tmapi:prepareTemplateForAPI($template as element(template)) as element(template) {
    let $ns-attributes  := 
        if ($template/@*[contains(name(), 'dummy')]) then () else ( 
            $template/../@*[contains(name(), 'dummy')]
        )
    return 
        <template>
        {
            $template/@*,
            if ($template/@ident) then () else (
                $template/../@ident,
                if ($template/@url) then () else $template/../@url
            ),
            $ns-attributes,
            for $ns-prefix at $i in in-scope-prefixes($template)
            let $ns-uri := namespace-uri-for-prefix($ns-prefix, $template)
            return
                (:hl7:dummy-default="urn:hl7-org:v3":)
                <ns uri="{$ns-uri}" prefix="{$ns-prefix}" default="{exists($template/@*[name() = $ns-prefix || ':dummy-default'])}"/>
            ,
            (: we return just a count of issues and leave it up to the calling party to get those if required :)
            <issueAssociation count="{count($setlib:colDecorData//issue/object[@id = $template/@id][@effectiveDate = $template/@effectiveDate] | $setlib:colDecorData//issue/object[@id = $template/@ref])}"/>
            ,
            (: we include all static association here that belong to our template. Note that there could be staticAssociation element under include. Those would come from a different template than our with potentially overlapping elementIds
               We do not want to include those because that would lead to false associations where a concept connected to a different, included template is assoicated to someting in the calling template.
            :)
            <staticAssociations>
            {
                for $node in $template//staticAssociations/*
                return
                    <concept>{$node/@*, $node/node()}</concept>
            }
            </staticAssociations>
            ,
            $template/desc
            ,
            tmapi:collapseTemplateClassifications($template/classification)
            ,
            for $e in $template/(* except (ns|issueAssociation|staticAssociations|desc|classification|
                                                       element|attribute|include|choice|defineVariable|let|assert|report|property|item))
            return
                if ($e/self::example) then
                    <example>
                    {
                        if ($e/@type) then $e/@type else attribute type {'neutral'},
                        $e/@caption,
                        $e/node()
                    }
                    </example>
                else (
                    $e
                )
            ,
            tmapi:prepareTemplateBodyForAPI($template/(element|attribute|include|choice|defineVariable|let|assert|report|property|item))
        }
        </template>
};

declare %private function tmapi:prepareTemplateBodyForAPI($node as node()*) {
    for $e at $pos in $node
    let $elname := name($e)
    return
        <items>
        {
            attribute { 'is' } { $elname },
            attribute { 'order' } { $pos },
            if ($e/self::example) then (
                if ($e/@type) then $e/@type else attribute type {'neutral'},
                $e/@caption,
                utillib:serializeNode($e)/node()
            )
            else
            if ($e/self::assert | $e/self::report) then (
              $e/(@* except (@is | @order)),
              if ($e/@role) then () else attribute role {'error'},
              utillib:serializeNode($e)/node()
            )
            else (
                $e/(@* except (@is | @order))
                ,
                for $elm in $e/(node() except (ns|issueAssociation|classification|element|attribute|include|choice|defineVariable|let|assert|report|property|item|example))
                return
                    if ($elm/self::staticAssociations) then
                        <staticAssociations>
                        {
                            for $assoc in $elm/*
                            return
                                <concept>{$assoc/@*, $assoc/node()}</concept>
                        }
                        </staticAssociations>
                    else (
                        $elm
                    ) 
                ,
                tmapi:prepareTemplateBodyForAPI($e/(element|attribute|include|choice|defineVariable|let|assert|report|property|item|example))
            )
        }
        </items>   
};

declare %private function tmapi:collapseTemplateClassifications($classification as element(classification)*) {
    <classification>
    {
        ($classification/@format)[1]
        ,
        if ($classification/@type) then
            for $type in $classification/@type
            return
                <type>{data($type)}</type>
        else (
            <type>notype</type>
        ),
        for $tag in $classification/tag
        group by $tag
        return
            $tag
        ,
        for $property in $classification/property
        group by $property
        return
            $property
    }
    </classification>
};

(:~ Retrieves a new template in edit mode to be saved later, or does initial save of a template
@param $projectPrefix           - required. determines search scope. null is full server, pfx- limits scope to this project only
@param $sourceId                - optional. parameter denoting the id of a template to use as a basis for creating the new template
@param $sourceEffectiveDate     - optional. parameter denoting the effectiveDate of a template to use as a basis for creating the new template
@param $targetDate              - optional. If true invokes effectiveDate of the new template as [DATE]T00:00:00. If false invokes date + time [DATE]T[TIME] 
@param $keepIds                 - optional. Only relevant if source template is specified. If true, the new template will keep the same id and only update the effectiveDate
@param $lock                    - optional. relevant when saving a new template. does not set a lock if false and returns in read mode, sets a lock if true and leaves in edit mode, otherwise
@param $request-body            - optional. json body containing new template structure
@return template
@since 2020-05-03
:)
declare function tmapi:postTemplate($request as map(*)) {
    let $authmap                        := $request?user
    let $projectPrefix                  := $request?parameters?prefix[not(. = '')]
    let $sourceId                       := $request?parameters?sourceId
    let $sourceEffectiveDate            := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?sourceEffectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?sourceEffectiveDate[string-length() gt 0]
        }
    let $refOnly                        := $request?parameters?refOnly = true()
    let $targetDate                     := $request?parameters?targetDate = true()
    let $keepIds                        := $request?parameters?keepIds = true()
    let $lock                           := $request?parameters?lock = true()
    let $data                           := utillib:getBodyAsXml($request?body, 'template', ())
    
    let $check                          := 
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                          :=
        if (empty($projectPrefix)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have prefix')
        else ()
    let $check                          := 
        if ($refOnly and empty($sourceId)) then
            error($errors:BAD_REQUEST, 'Request SHALL have source id if refOnly = true')
        else ()
    let $check                          := 
        if ($keepIds and empty($sourceId)) then
            error($errors:BAD_REQUEST, 'Request SHALL have source id if keepIds = true')
        else ()
    let $data                           := 
        if ($refOnly) then 
            <template ref="{$sourceId}" flexibility="{$sourceEffectiveDate}"/>
        else 
        if (empty($data)) then
            (:error($errors:SERVER_ERROR, 'Retrieval of new template not yet implemented'):)
            tmapi:getTemplateForEdit($authmap, $projectPrefix, 'new', $sourceId, $sourceEffectiveDate, $keepIds, $targetDate, false())
        else (
            $data
        )
    let $return                         := tmapi:postTemplate($authmap, $projectPrefix, $data, $keepIds, $lock)
            
    return
        if (empty($data)) then 
            roaster:response(200, $return)
        else (
            roaster:response(201, $return)
        )
};

declare function tmapi:postTemplateExample($request as map(*)) {
    let $authmap                        := $request?user
    let $projectPrefix                  := $request?parameters?prefix[not(. = '')]
    let $tmid                           := $request?parameters?id[not(. = '')]
    let $tmed                           := $request?parameters?effectiveDate[not(. = '')]
    let $release                        := $request?parameters?release[not(. = '')]
    let $language                       := $request?parameters?language[not(. = '')]
    let $elid                           := $request?parameters?elementId[not(. = '')]
    let $doSerialized                   := $request?parameters?serialized = true()
    (: tmapi:prepareTemplateForUpdate takes care of eliminating unselected items :)
    let $doSelectedOnly                 := false()
    let $doRecursive                    := $request?parameters?recursive = true()
    
    let $data                           := utillib:getBodyAsXml($request?body, 'template', ())
    let $projectPrefix                  := ($data/@projectPrefix, $projectPrefix)[1]
    let $check                          :=
        if ($data) then
            if (empty($tmid)) then () else (
                error($errors:BAD_REQUEST, 'Request SHALL NOT have both request body and parameter id')
            )
        else 
        if (empty($tmid)) then 
            error($errors:BAD_REQUEST, 'Request SHALL at least have a request body or a parameter id')
        else ()
    let $check                  :=
        if (empty($projectPrefix)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have prefix')
        else ()
    (: start processing ... :)
    let $template           := if ($data) then $data else (tmapi:getTemplateById($tmid, $tmed, $projectPrefix, $release, $language)[@id])[1]
    let $prefix             := ($projectPrefix, $template/ancestor::decor/project/@prefix)[1]
    let $template           := if ($template) then tmapi:prepareTemplateForUpdate($template) else ()
    let $format             := ($template/classification/@format, 'hl7v3xml1')[not(. ='')][1]
    
    let $check              :=
       if ($template) then 
           if (empty($elid)) then () else if ($template//*[@id = $elid]) then () else (
               error($errors:BAD_REQUEST,concat('Argument elementId ', $elid, ' does not exist in template with id ', $template/@id, ' effectiveDate ', $template/@effectiveDate))
           )
       else (
           error($errors:BAD_REQUEST,concat('Argument id ', $tmid, ' effectiveDate ', $tmed, ' did not lead to a template and no template provided.'))
       )
    
    let $templateChain      := 
        if ($template and $doRecursive) then (
            let $decor              :=
                if (utillib:isOid($projectPrefix)) then utillib:getDecorById($projectPrefix) else utillib:getDecorByPrefix($projectPrefix)
            let $decors             := utillib:getBuildingBlockRepositories($decor, (), $utillib:strDecorServicesURL)
            return
                $template | tmapi:getTemplateChain($template, $decors, map:merge(map:entry(concat($template/@id, $template/@effectiveDate), '')))
       )
       else (
           $template,
           for $elm in $template//element[@contains] | $template//include
           return
               (tmapi:getTemplateById($elm/@contains | $elm/@ref, ($elm/@flexibility, 'dynamic')[1], $prefix, $release, $language)[@id])[1]
       )
    let $templateChain      :=
        for $t in $templateChain
        let $ideff  := concat($t/@id, $t/@effectiveDate)
        group by $ideff
        order by $t[1]/@id
        return $t[1]
    
    let $vocabChain         :=
        for $v in $templateChain//vocabulary[@valueSet]
        let $vsid         := $v/@valueSet
        let $vsed         := $v/@flexibility[. castable as xs:dateTime]
        group by $vsid, $vsed
        return
            vsapi:getValueSetByRef($vsid, ($vsed, 'dynamic')[1], $projectPrefix, $release, $language)
    let $vocabChain         :=
        for $vs in $vocabChain
        return
        try {
            let $expansion    := tcsapi:getValueSetExpansionSet($vs, map { "maxResults": 20, "expand": true(), "debug": true() })
            return
            <valueSet>
            {
                $vs/@*, 
                if ($vs/@ident) then () else attribute ident {($vs/ancestor::decor/project/@prefix)[1]},
                if ($vs/@url) then () else attribute url {($vs[1]/ancestor::cacheme/@bbrurl, $utillib:strDecorServicesURL)[1]},
                $vs/(* except (completeCodeSystem | conceptList))
            }
                <conceptList>{$expansion/expansion/*}</conceptList>
            </valueSet>
        }
        catch * {
            $vs
        }
    
    let $decor              := utillib:getDecorByPrefix($prefix, $release, $language)
    let $decorPackage       :=
       <decor>
           {$decor/@*}
           <project>{$decor/project/@*, $decor/project/defaultElementNamespace}</project>
           <terminology>{$vocabChain}</terminology>
           <rules>{$templateChain}</rules>
       </decor>
    
    (:let $s  := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $decorPackage):)
    
    let $xsltParameters :=
       <parameters>
           <param name="tmid"                      value="{($template/@id, $tmid)[1]}"/>
           <param name="tmed"                      value="{($template/@effectiveDate, $tmed)[1]}"/>
           <param name="elid"                      value="{$elid}"/>
           <param name="doSelectedOnly"            value="{$doSelectedOnly}"/>
           <param name="doRecursive"               value="{$doRecursive}"/>
           <param name="logLevel"                  value="'OFF'"/>
       </parameters>
    
    (:let $xslt           := xs:anyURI('https://assets.art-decor.org/ADAR/rv/Template2Example.xsl'):)
       (:if ($dodev) then 
           xs:anyURI('https://art-decor.org/ADAR-dev/rv/Template2Example.xsl')
       else (
           (\:xs:anyURI(concat('xmldb:exist://', $utillib:strDecorCore, '/Template2Example.xsl')):\)          
       ):)
    let $xslt               := xs:anyURI('xmldb:exist://' || $setlib:strDecorCore || '/Template2Example.xsl')
    
    let $dopackage          := false()
    (:we need a root element, it's the way it is...:)
    let $tpath              := replace($template/context/@path,'[/\[].*','')
    let $path               := if (string-length($tpath)>0) then $tpath else ('art:placeholder')
    let $element            := <element name="{$path}" format="{$format}" xmlns:art="urn:art-decor:example" selected="">{$template/(attribute|element|include|choice)}</element>
    let $example            := if ($dopackage) then () else if ($decor) then transform:transform($decorPackage, $xslt, $xsltParameters) else ()
    
    (:if we did not need our pseudo root-element, just leave it off:)
    let $example            := if ($example[count(@*)>0 or count(*)>1]) then $example else $example/node()
    
    let $return                         := 
       if ($dopackage) then $decorPackage else (
           <example caption="" type="neutral" xmlns:json="http://www.json.org">
           {
               if ($doSerialized) then
                   fn:serialize($example,
                       <output:serialization-parameters xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization">
                           <output:method>xml</output:method>
                           <output:encoding>UTF-8</output:encoding>
                       </output:serialization-parameters>
                   )
               else (
                   $example
                   (:,
                   if ($dodev) then $decorPackage else ():)
               )
           }
           </example>
       )
    return
            roaster:response(200, $return)
};

(:~ Retrieves a template in edit mode
@param $id                      - required parameter denoting the id of the template
@param $effectiveDate           - optional parameter denoting the effectiveDate of the template. If not given assumes latest version for id
@return template
@since 2020-05-03
:)
declare function tmapi:getTemplateForEdit($request as map(*)) {
    let $authmap                        := $request?user
    let $id                             := $request?parameters?id[not(. = '')]
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $breakLock                      := $request?parameters?breakLock = true()
    
    let $check                          :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    
    let $return                         := tmapi:getTemplateForEdit($authmap, (), 'edit', $id, $effectiveDate, true(), true(), $breakLock)
    return (
        roaster:response(200, $return)
    )
};

declare function tmapi:getTemplateForEdit($authmap as map(*), $projectPrefix as xs:string?, $mode as xs:string, $id as xs:string?, $effectiveDate as xs:string?, $keepIds as xs:boolean, $targetDate as xs:boolean, $breakLock as xs:boolean) as element(template) {
    
    let $template               := 
        if (empty($id)) then () else 
        if (empty($projectPrefix)) then 
            $setlib:colDecorData//template[@id = $id][@effectiveDate = $effectiveDate] 
        else (
            tmapi:getPrototype($id, $effectiveDate, $projectPrefix) 
        )
    let $decor                  := if (empty($projectPrefix)) then $template/ancestor::decor else utillib:getDecorByPrefix($projectPrefix)
    let $projectPrefix          := $decor/project/@prefix
    let $language               := $decor/project/@defaultLanguage
    
    let $lock                   := 
        if (empty($template) or not($mode = 'edit')) then () else decorlib:getLocks($authmap, $id, $effectiveDate, ())
    let $lock                   := 
        if (empty($template) or not($mode = 'edit')) then () else if ($lock) then $lock else decorlib:setLock($authmap, $id, $effectiveDate, $breakLock)
    
    let $check                  :=
        if ($template) then () else if (empty($id)) then () else (
            error($errors:BAD_REQUEST, 'Template with id ''' || $id || ''' and effectiveDate ''' || $effectiveDate || ''' not found in the context of project ' || $projectPrefix)
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-RULES)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify templates in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if ($mode = 'edit') then (
            if ($lock) then () else (
                error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have a lock for this template (anymore). Get a lock first.'))
            ),
            if ($template[@statusCode = $tmapi:STATUSCODES-FINAL]) then
                error($errors:BAD_REQUEST, concat('Template cannot be updated while it has one of status: ', string-join($tmapi:STATUSCODES-FINAL, ', '), if ($template/@statusCode = 'pending') then 'You should switch back to status draft to edit.' else ()))
            else ()
        )
        else ()
    
    let $templateAssociations   := if (empty($id)) then () else $decor//templateAssociation[@templateId = $id][@effectiveDate=$effectiveDate]
    let $valueSetList           := $decor/terminology/valueSet
    let $templateList           := $decor/rules/template

    (: TODO: why do we only pick SPEC? Could it not be "anything"? All it does it supply you with potential elements and attributes to activate :)
    let $specialization         := $template/relationship[@type='SPEC'][@template][1]
    (:get prototype for editor with normalized attributes:)
    let $prototype              := if ($specialization) then (tmapi:getPrototype($specialization/@template, $specialization/@flexibility, $projectPrefix)) else ()
    
    let $useBaseId              := decorlib:getDefaultBaseIds($projectPrefix, $decorlib:OBJECTTYPE-TEMPLATE)[1]/@id
        
    let $namespaces-attrs       := 
        for $ns at $i in utillib:getDecorNamespaces($decor) 
        return 
            attribute {QName($ns/@uri,concat($ns/@prefix,':dummy-', if ($ns/@default = 'true') then 'default' else $i))} {$ns/@uri}
    
    let $templateName           := attribute name {($template/@name, utillib:getNameForOID($template/@id, (), $decor))[not(.='')][1]}
    let $templateDisplayName    := attribute displayName {($template/@displayName, $templateName)[not(. = '')][1]}
    let $result                 := 
        <template 
            projectPrefix="{$projectPrefix}" 
            baseId="{$useBaseId}">
        {
            attribute originalId {if ($mode = 'edit' or $keepIds) then $template/@id else ()},
            attribute id {if ($mode = 'edit' or $keepIds) then $template/@id else ()},
            $templateName,
            $templateDisplayName,
            attribute effectiveDate {
                if ($mode = 'edit') then $template/@effectiveDate else 
                if ($targetDate) then substring(string(current-date()), 1, 10) || 'T00:00:00' else substring(string(current-dateTime()), 1, 19)
            },
            attribute statusCode {if ($mode='edit') then $template/@statusCode else 'draft'},
            attribute versionLabel {$template/@versionLabel},
            attribute isClosed {$template/@isClosed},
            attribute expirationDate {$template/@expirationDate},
            attribute officialReleaseDate {$template/@officialReleaseDate},
            attribute canonicalUri {if ($mode='edit' or $keepIds) then $template/@canonicalUri else ()}
        }
        {
            $namespaces-attrs
        }
            <edit mode="{$mode}"/>
        {
            $lock/*
        }
        {
            if ($template/desc) then 
                for $desc in $template/desc
                return
                    utillib:serializeNode($desc)
            else (
                <desc language="{$language}"/>
            )
        }
        {
            $template/classification
        }
        {
            if ($mode = 'edit') then () else (
                if ($template[@id]) then (
                    <relationship type="{if ($mode = 'new') then 'SPEC' else upper-case($mode)}" template="{$template/@id}" selected="template" flexibility="{$template/@effectiveDate}">
                    {
                        for $attr in $template/(@id | @name | @displayName | @effectiveDate | @expirationDate | @statusCode | @versionLabel)
                        return
                            attribute {concat('tm', local-name($attr))} {$attr}
                        ,
                        attribute url {$utillib:strDecorServicesURL},
                        attribute ident {$projectPrefix},
                        attribute linkedartefactmissing {'false'}
                    }
                    </relationship>
                ) else ()
            )
            ,
            for $relationship in $template/relationship
            let $addAttribute := if ($relationship/@template) then 'model' else 'template'
            let $artefact     := 
                if (empty($relationship/@template)) then () else (
                    (tmapi:getTemplateById($relationship/@template, $relationship/@flexibility, $decor, (), ())[@id][@effectiveDate])[1]
                )
            let $tmpurl     := ($artefact/@url, $artefact/ancestor::cacheme/@bbrurl, $utillib:strDecorServicesURL)[1]
            let $tmpident   := ($artefact/@ident, $artefact/ancestor::decor/project/@prefix)[1]
            return
            <relationship>
            {
                attribute type {$relationship/@type},
                if ($relationship/@template) then (
                    attribute template {$relationship/@template},
                    attribute model {''},
                    attribute flexibility {$relationship/@flexibility},
                    attribute selected {'template'},
                    if (empty($artefact)) then () else (
                        for $attr in $artefact/(@id | @name | @displayName | @effectiveDate | @expirationDate | @statusCode | @versionLabel)
                        return
                            attribute {concat('tm', local-name($attr))} {$attr}
                        ,
                        attribute url {$tmpurl},
                        attribute ident {$tmpident}
                    ),
                    attribute linkedartefactmissing {empty($artefact)}
                )
                else (
                    attribute template {''},
                    attribute model {$relationship/@model},
                    attribute flexibility {$relationship/@flexibility},
                    attribute selected {'model'}
                )
            }
            </relationship>
        }
        {
            for $node in $template/publishingAuthority
            return
                <publishingAuthority>
                {
                    $node/(@* except @selected),
                    attribute selected {''},
                    $node/addrLine
                }
                </publishingAuthority>
        }
        {
            for $node in $template/endorsingAuthority
            return
                <endorsingAuthority>
                {
                    $node/(@* except @selected),
                    attribute selected {''},
                    $node/addrLine
                }
                </endorsingAuthority>
        }
        {
            for $node in $template/purpose
            return
                utillib:serializeNode($node)
        }
        {
            for $node in $template/copyright
            return
                utillib:serializeNode($node)
        }
            <context>
            {
                if ($template/context/@id) then (
                    $template/context/@id,
                    attribute path {''},
                    attribute selected {'id'}
                )
                else (
                    attribute id {''},
                    attribute path {$template/context/@path},
                    attribute selected {'path'}
                )
            }
            </context>
            {
                if ($template/item) then
                <item label="{$template/item/@label}">
                {
                    for $desc in $template/item/desc
                    return
                        utillib:serializeNode($desc)
                }
                </item>
                else ()
            }
        {
            if ($template/example) then
                (: if multiple root nodes exist, it is never going to be valid xml in an editor, so we wrap it under one :)
                for $example in $template/example
                return
                    <example type="{$example/@type}" caption="{$example/@caption}">
                    {
                        if (count($example/*) gt 1) then (
                            utillib:serializeNode(<art:placeholder xmlns:art="urn:art-decor:example">{$example/node()}</art:placeholder>)
                        )
                        else (
                            utillib:serializeNode($example)/node()
                        )
                    }
                    </example>
            else (
                <example type="neutral" caption=""/>
            )
        }
        {
            if (exists($specialization) and exists($prototype)) then (
                (: if template is empty, check prototype for relevant parts :)
                if ($template/(attribute|element|choice|include|let|defineVariable|assert|report|constraint)) then (
                    for $item in $template/(attribute|element|choice|include|let|defineVariable|assert|report|constraint)
                    return
                        tmapi:mergePrototypeTemplateForEdit($projectPrefix,$item,$prototype,$language,$valueSetList,$templateList, $mode)
                ) else (
                    for $item in $prototype/(attribute|element|choice|include|let|defineVariable|assert|report|constraint)
                    return
                        tmapi:recurseItemForEdit($projectPrefix,$item,$language,$valueSetList,$templateList,false(), $mode)
                )
            )
            else (
                for $item in $template/(attribute|element|choice|include|let|defineVariable|assert|report|constraint)
                return
                    tmapi:recurseItemForEdit($projectPrefix,$item,$language,$valueSetList,$templateList,true(), $mode)
            )
        }
        {
            (: by adding them here, they stay in the clients XML until he closes the form :)
            if ($templateAssociations) then tmapi:createStaticAssociationElement($templateAssociations, $language) else ()
        }
        </template>
    
    let $result                 := tmapi:prepareTemplateForAPI($result)
    
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

(: keep it simple for performance. if the requested valueSet is in the project terminology by ref, don't look further :)
declare %private function tmapi:isValueSetInScope($projectPrefix as xs:string, $ref as attribute()?,$flexibility as attribute()?,$valueSetList as element()*) as attribute()* {
    if (string-length($ref)=0) then ()
    else (
        let $vsElms := $valueSetList[(@id|@name|@ref)=$ref]
        let $vsEff  := if ($flexibility castable as xs:dateTime) then $flexibility else (max($vsElms[@effectiveDate]/xs:dateTime(@effectiveDate)))
       
        return
            if ($vsElms[@effectiveDate=$vsEff]) then (
                attribute vsid {$vsElms[@effectiveDate=$vsEff][1]/(@id|@ref)},
                attribute vsname {$vsElms[@effectiveDate=$vsEff][1]/@name},
                attribute vsdisplayName {$vsElms[@effectiveDate=$vsEff][1]/@displayName}
            )
            else if ($vsElms[@ref]) then (
                attribute vsid {$vsElms[1]/(@id|@ref)},
                attribute vsname {$vsElms[1]/@name},
                attribute vsdisplayName {$vsElms[1]/@displayName}
            )
            else (
                attribute linkedartefactmissing {'true'}
            )
    )
};

(: keep it simple for performance. if the requested template is in the project rules by ref, don't look further :)
declare %private function tmapi:isTemplateInScope($projectPrefix as xs:string, $ref as attribute()?,$flexibility as attribute()?,$templateList as element()*) as attribute()* {
    if (string-length($ref)=0) then ()
    else (
        let $templates  := (tmapi:getTemplateByRef($ref, ($flexibility, 'dynamic')[1], $projectPrefix, (), ())[@id])[1]
        
        return
            if ($templates) then (
                attribute tmid {$templates/@id},
                attribute tmdisplayName {if ($templates/@displayName) then $templates/@displayName else $templates/@name}
            ) else (
                attribute linkedartefactmissing {'true'}
            )
    )
};

declare %private function tmapi:getDatatype($datatype as xs:string?, $classification-format as xs:string?) as xs:string? {
    let $classification-format  := if (empty($classification-format)) then 'hl7v3xml1' else $classification-format
    let $datatypes              := $setlib:colDecorCore//supportedDataTypes[@type = $classification-format]
    let $datatypeName           := if (contains($datatype,':')) then substring-after($datatype,':') else ($datatype)
    let $flavor                 := $datatypes//flavor[@name=$datatypeName]
    return
        if ($flavor) then ($flavor/ancestor::dataType[1]/@name) else ($datatype)
};

(: re-write attribute strength CNE is required and CWE is extensible :)
declare %private function tmapi:rewriteStrength($os as xs:string?) as xs:string? {
    let $r := if ($os = 'CNE') then 'required' else if ($os = 'CWE') then 'extensible' else $os
    return $r
};

declare %private function tmapi:recurseItemForEdit($projectPrefix as xs:string, $item as element(),$language as xs:string, $valueSetList as element()*, $templateList as element()*, $selected as xs:boolean, $mode as xs:string) as element()* {
if ($item/name()='attribute') then
    for $att in tmapi:normalizeAttributes($item)
    return
    <attribute>
    {
        $att/@name,
        $att/@value,
        attribute conformance {
            if ($att[@prohibited='true']) then 'NP' else
            if ($att[@isOptional='true']) then 'O' else
                'R'
        }
        ,
        $att/@datatype,
        $att/@id
        ,
        if ($att/@strength) then (
            attribute strength {tmapi:rewriteStrength($att/@strength)}
        ) else ()
        ,
        if ($att/@datatype) then (
            attribute originalType {tmapi:getDatatype($att/@datatype,$item/ancestor::template/classification/@format)},
            attribute originalStrength {tmapi:rewriteStrength($att/@strength)}
        ) else ()
        ,
        attribute originalConformance {
            if ($selected and $mode = 'new') then 
                if ($att[@prohibited='true']) then 'NP' else
                if ($att[@isOptional='true']) then 'O' else
                    'R'
            else ('O')
        }
        ,
        if ($selected) then (attribute selected {''}) else ()
        ,
        for $desc in $att/desc
        return
        utillib:serializeNode($desc)
        ,
        for $subitem in $att/(* except desc)
        return
            if ($subitem[name()='vocabulary']) then
                <vocabulary>
                {
                    $subitem/(@valueSet|@flexibility|@code|@codeSystem|@codeSystemName|@codeSystemVersion|@displayName|@domain)[not(.='')],
                    tmapi:isValueSetInScope($projectPrefix, $subitem/@valueSet,$subitem/@flexibility, $valueSetList),
                    $subitem/*
                }
                </vocabulary>
            else (
                $subitem
            )
    }
    </attribute>
else if ($item/name()='element') then
    <element>
    {
        $item/(@* except (@strength|@tmid|@tmname|@tmdisplayName|@linkedartefactmissing)),
        tmapi:isTemplateInScope($projectPrefix, $item/@contains, $item/@flexibility, $templateList)
        ,
        if ($item/@strength) then (
            attribute strength {tmapi:rewriteStrength($item/@strength)}
        ) else ()
        ,
        if ($item/@datatype) then (
            attribute originalType {tmapi:getDatatype($item/@datatype,$item/ancestor::template/classification/@format)},
            attribute originalStrength {tmapi:rewriteStrength($item/@strength)}
        )
        else ()
        ,
        if ($item/@minimumMultiplicity) then () else (
            attribute minimumMultiplicity {''}
        )
        ,
        if ($item/@maximumMultiplicity) then () else (
            attribute maximumMultiplicity {''}
        )
        ,
        attribute originalMin {'0'},
        attribute originalMax {'*'}
        ,
        if ($item/@conformance) then () else (
            let $conformance := 
                if ($item[@isMandatory='true']) then 'R' else
                if ($item[@minimumMultiplicity castable as xs:integer]/@minimumMultiplicity > 0) then 'R' else ('O')
            return
            attribute conformance {$conformance}
        )
        ,
        if ($item/@isMandatory) then () else (
            attribute isMandatory {'false'}
        )
        ,
        if ($item/@strength) then () else (
            attribute strength {''}
        )
        ,
        if ($selected) then (attribute selected {''}) else ()
        ,
        for $desc in $item/desc
        return
            utillib:serializeNode($desc)
        ,
        for $vocabulary in $item/vocabulary
        return
            <vocabulary>
            {
                $vocabulary/(@valueSet|@flexibility|@code|@codeSystem|@codeSystemName|@codeSystemVersion|@displayName|@domain)[not(.='')],
                tmapi:isValueSetInScope($projectPrefix, $vocabulary/@valueSet,$vocabulary/@flexibility,$valueSetList),
                $vocabulary/*
            }
            </vocabulary>
        ,
        $item/property,
        $item/item[1],
        $item/text,
        for $example in $item/example
        return
            <example type="{$example/@type}" caption="{$example/@caption}">
            {
                if (count($example/*) gt 1) then (
                    utillib:serializeNode(<art:placeholder xmlns:art="urn:art-decor:example">{$example/node()}</art:placeholder>)
                )
                else (
                    utillib:serializeNode($example)/node()
                )
            }
            </example>
        ,
        for $subItem in $item/(attribute|element|choice|include|let|defineVariable|assert|report|constraint)
        return
            tmapi:recurseItemForEdit($projectPrefix, $subItem,$language,$valueSetList,$templateList,$selected, $mode)
    }
    </element>
else if ($item/name()='choice') then
    <choice>
    {
        $item/@*,
        if ($item/@minimumMultiplicity) then () else (
            attribute minimumMultiplicity {''}
        )
        ,
        if ($item/@maximumMultiplicity) then () else (
            attribute maximumMultiplicity {''}
        )
        ,
        attribute originalMin {'0'}
        ,
        attribute originalMax {'*'}
        ,
        if ($selected) then (attribute selected {''}) else ()
        ,
        for $desc in $item/desc
        return
        utillib:serializeNode($desc)
        ,
        $item/item[1],
        for $subItem in $item/(element|choice|include|constraint)
        return
        tmapi:recurseItemForEdit($projectPrefix, $subItem,$language,$valueSetList,$templateList,$selected, $mode)
    }
    </choice>
else if ($item/name()='include') then
    <include>
    {
        $item/(@* except (@tmid|@tmname|@tmdisplayName|@linkedartefactmissing)),
        tmapi:isTemplateInScope($projectPrefix, $item/@ref, $item/@flexibility, $templateList)
        ,
        if ($item/@minimumMultiplicity) then () else (
            attribute minimumMultiplicity {''}
        )
        ,
        if ($item/@maximumMultiplicity) then () else (
            attribute maximumMultiplicity {''}
        )
        ,
        attribute originalMin {'0'}
        ,
        attribute originalMax {'*'}
        ,
        if ($item/@conformance) then () else (
            let $conformance := if ($item/@minimumMultiplicity='1') then 'R' else ('O')
            return
            attribute conformance {$conformance}
        )
        ,
        if ($item/@isMandatory) then () else (
            attribute isMandatory {'false'}
        )
        ,
        if ($item/@flexibility) then () else (
            attribute flexibility {'dynamic'}
        )
        ,
        if ($selected) then (attribute selected {''}) else ()
        ,
        for $desc in $item/desc
        return
        utillib:serializeNode($desc)
        ,
        $item/item[1],
        for $example in $item/example
        return
            <example type="{$example/@type}" caption="{$example/@caption}">
            {
                if (count($example/*) gt 1) then (
                    utillib:serializeNode(<art:placeholder xmlns:art="urn:art-decor:example">{$example/node()}</art:placeholder>)
                )
                else (
                    utillib:serializeNode($example)/node()
                )
            }
            </example>
        ,
        for $subItem in $item/constraint
        return
            tmapi:recurseItemForEdit($projectPrefix, $subItem, $language, $valueSetList, $templateList, $selected, $mode)
    }
    </include>
else if ($item/name()='let') then
    <let>
    {
        if ($selected) then (attribute selected {''}) else (),
        attribute name {$item/@name},
        attribute value {$item/@value},
        $item/node()
    }
    </let>
else if ($item/name()=('assert','report')) then
    element { name($item) } 
    {
        if ($selected) then (attribute selected {''}) else (),
        attribute role {$item/@role},
        attribute test {$item/@test},
        attribute see {$item/@see},
        attribute flag {$item/@flag},
        utillib:serializeNode($item)/node()
    }
else if ($item/name()='constraint') then
    <constraint>{if ($selected) then (attribute selected {''}) else (),$item/(@* except @selected),utillib:serializeNode($item)/node()}</constraint>
else()
};

declare %private function tmapi:index-of-node( $nodes as node()* , $nodeToFind as node() )  as xs:integer* {
    for $seq in (1 to count($nodes))
    return $seq[$nodes[$seq] is $nodeToFind]
};

declare %private function tmapi:index-node-in-set( $nodes as node()*, $nodeToFind as node() ) as element()* {
    let $n      :=
        if ($nodeToFind[@name]/name()='attribute') then
            $nodes[name()=$nodeToFind/name()][@name = $nodeToFind/@name]
        else
        if ($nodeToFind[@contains]/name()='element') then
            $nodes[name()=$nodeToFind/name()][@name = $nodeToFind/@name][@contains = $nodeToFind/@contains]
        else
        if ($nodeToFind[@name = ('hl7:templateId', 'cda:templateId')][attribute[@name = 'root'][@value]]/name()='element') then
            $nodes[name()=$nodeToFind/name()][@name = $nodeToFind/@name][attribute[@name = 'root'][@value = $nodeToFind/attribute[@name = 'root']/@value]]
        else
        if ($nodeToFind/name()='element') then
            $nodes[name()=$nodeToFind/name()][@name = $nodeToFind/@name]
        else
        if ($nodeToFind/name()='choice') then
            if ($nodeToFind[element]) then
                $nodes[self::choice][element/@name = $nodeToFind/element/@name]
            else
            if ($nodeToFind[include]) then
                $nodes[self::choice][include/@ref = $nodeToFind/include/@ref]
            else
            if ($nodeToFind[choice]) then
                $nodes[self::choice][choice]
            else
            if ($nodeToFind[@minimumMultiplicity][@maximumMultiplicity]) then
                $nodes[self::choice][@minimumMultiplicity = $nodeToFind/@minimumMultiplicity][@maximumMultiplicity = $nodeToFind/@maximumMultiplicity]
            else
            if ($nodeToFind[@minimumMultiplicity]) then
                $nodes[self::choice][@minimumMultiplicity = $nodeToFind/@minimumMultiplicity]
            else
            if ($nodeToFind[@maximumMultiplicity]) then
                $nodes[self::choice][@maximumMultiplicity = $nodeToFind/@maximumMultiplicity]
            else (
                $nodes[self::choice]
            )
        else
        if ($nodeToFind/name()='include') then
            $nodes[name()=$nodeToFind/name()][@ref = $nodeToFind/@ref]
        else
        if ($nodeToFind/name()='let') then
            $nodes[name()=$nodeToFind/name()][@name = $nodeToFind/@name]
        else
        if ($nodeToFind/name()='defineVariable') then
            $nodes[name()=$nodeToFind/name()][@name = $nodeToFind/@name]
        else
        if ($nodeToFind/name()='assert') then
            $nodes[name()=$nodeToFind/name()][@test = $nodeToFind/@test]
        else
        if ($nodeToFind/name()='report') then
            $nodes[name()=$nodeToFind/name()][@test = $nodeToFind/@test]
        else
        if ($nodeToFind/name()='text') then 
            $nodes[name()=$nodeToFind/name()][. = $nodeToFind]
        else
        if ($nodeToFind/name()='constraint') then
            $nodes[name()=$nodeToFind/name()][@language = $nodeToFind/@language]
        else
        if ($nodeToFind/name()='example') then
            $nodes[name()=$nodeToFind/name()][@language = $nodeToFind/@language][count(preceding-sibling::example) + 1]
        else (
            $nodes[name()=$nodeToFind/name()][string-join(for $att in (@* except (@isOptional|@prohibited|@datatype|@id)) order by name($att) return $att,'')=$nodeToFind/string-join(for $att in (@* except (@isOptional|@prohibited|@datatype|@id)) order by name($att) return $att,'')]
        )
    let $n      :=
        if ($n[2]) then 
            if ($n[@id = $nodeToFind/@id]) then 
                $n[@id = $nodeToFind/@id] 
            (:else 
            if ($n[deep-equal(self::node(), $nodeToFind)]) then 
                $n[deep-equal(self::node(), $nodeToFind)]:) 
            else $n
        else $n
        
    return $n
};

(: Returns paths like this
    element[@name='hl7:informEvent'][empty(@contains)][@id='2.16.840.1.113883.2.4.3.111.3.7.9.16'][1]
:)
declare %private function tmapi:path-to-node ( $nodes as node()* )  as xs:string* { 
    string-join(
        for $item in $nodes/ancestor-or-self::*[not(descendant-or-self::template)]
        return (
            string-join(
                ($item/name(),
                if ($item[@name]/name()='attribute') then (
                    '[@name=''',replace($item/@name,'''',''''''),''']'
                )
                else
                if ($item[@name][@contains]/name()='element') then (
                    '[@name=''',replace($item/@name,'''',''''''),''']',
                    '[@contains=''',replace($item/@contains,'''',''''''),''']'
                )
                else
                if ($item[@name = ('hl7:templateId', 'cda:templateId')][attribute[@name = 'root'][@value]]/name()='element') then (
                    '[@name=''',replace($item/@name,'''',''''''),''']','[attribute[@name = ''root''][@value = ''',$item/attribute[@name = 'root']/@value,''']]',
                    '[',count($item/preceding-sibling::element[@name = $item/@name][attribute[@name = 'root'][@value]]) + 1,']'
                )
                else
                if ($item[@name][empty(@contains)]/name()='element') then (
                    '[@name=''',replace($item/@name,'''',''''''),''']','[empty(@contains)]',
                    '[',count($item/preceding-sibling::element[@name = $item/@name][empty(@contains)]) + 1,']'
                )
                else
                if ($item/name()='choice') then (
                    if ($item[element]) then (
                        '[element/@name=(',for $n in $item/element/@name return concat('''',replace($n,'''',''''''),''''),')]'
                    ) else
                    if ($item[include]) then (
                        '[include/@ref=(',for $n in $item/include/@ref return concat('''',replace($n,'''',''''''),''''),')]'
                    ) else
                    if ($item[choice]) then (
                        '[choice]'
                    ) else
                    if ($item[@minimumMultiplicity][@maximumMultiplicity]) then (
                        '[@minimumMultiplicity = ',$item/@minimumMultiplicity,']','[@maximumMultiplicity = ',$item/@maximumMultiplicity,']'
                    ) else
                    if ($item[@minimumMultiplicity]) then (
                        '[@minimumMultiplicity = ',$item/@minimumMultiplicity,']'
                    ) else
                    if ($item[@maximumMultiplicity]) then (
                        '[@maximumMultiplicity = ',$item/@maximumMultiplicity,']'
                    ) else (
                        ''
                    )
                )
                else
                if ($item/name()='include') then (
                    '[@ref=''',replace($item/@ref,'''',''''''),''']'
                )
                else
                if ($item/name()='let') then (
                    '[@name=''',replace($item/@name,'''',''''''),''']'
                )
                else
                if ($item/name()='defineVariable') then (
                    '[@name=''',replace($item/@name,'''',''''''),''']'
                )
                else
                if ($item/name()='assert') then (
                    '[@test=''',replace($item/@test,'''',''''''),''']'
                )
                else
                if ($item/name()='report') then (
                    '[@test=''',replace($item/@test,'''',''''''),''']'
                )
                else
                if ($item/name()='constraint') then (
                    '[@language=''',replace($item/@language,'''',''''''),''']'
                )
                else
                if ($item/name()='example') then (
                    '[',count(preceding-sibling::example) + 1,']'
                )
                else
                if ($item/name()='text') then (
                    '[. = ''',replace(., '''', ''''''),''']'
                )
                else
                if ($item[@name]) then (
                    '[@name=''',replace($item/@name,'''',''''''),''']'
                )
                else
                if ($item[@code][@codeSystem]) then (
                    '[@code=''',replace($item/@code,'''',''''''),''']',
                    '[@codeSystem=''',replace($item/@codeSystem,'''',''''''),''']'
                )
                else
                if ($item[@valueSet]) then (
                    '[@valueSet=''',replace($item/@valueSet,'''',''''''),''']'
                )
                else
                if ($item[@language]) then (
                    '[@language=''',replace($item/@language,'''',''''''),''']'
                )
                else (
                    let $n  := $item/(@* except (@isOptional|@prohibited|@datatype|@id))[1]
                    return (
                        if ($n) then concat('[@',name($n),'=''',replace($n,'''',''''''),''']') else ()
                    )
                )
            ), '')
        )
    , '/')
};

declare %private function tmapi:mergePrototypeTemplateForEdit($projectPrefix as xs:string, $item as element(), $prototype as element(), $language as xs:string, $valueSetList as element()*, $templateList as element()*, $mode as xs:string) as element()* {
(: get corresponding node in prototype :)
let $node           := util:eval-inline($prototype,tmapi:path-to-node($item))
let $node           := if ($node[@id = $item/@id]) then $node[@id = $item/@id][1] else $node[1]
(: get the reversed sequence for previous siblings of the corresponding node in the prototype template, if any :)
let $precedingNodes := reverse($node/preceding-sibling::*)

let $results        := (
    (: if there are no preceding nodes in template, get all preceding nodes from prototype
        else get preceding nodes up to node with same name as preceding template node
    :)
    if (count($item/preceding-sibling::*)=0 and count($node/preceding-sibling::*)>0) then (
        for $precedingNode in $node/preceding-sibling::*
        return
            tmapi:recurseItemForEdit($projectPrefix,$precedingNode,$language,$valueSetList,$templateList,false(), $mode)
    )
    else (
        (: check if there are preceding nodes in prototype that are not in the template
            in order not get retrieve every single node preceding the current one, we first 
            try to find a previous matching node, anything between the previous and the 
            current item should be merged into the current template in this context 
        :)
        let $indexNode  := 
            for $precedingItem in $item/preceding-sibling::*
            let $n      := tmapi:index-node-in-set($precedingNodes, $precedingItem)
            return
                if (count($n) le 1) then ($n) else (
                    error(QName('http://art-decor.org/ns/error', 'MultipleIndexNodes'), concat('Unexpected error. Found multiple index nodes so we do not know what to merge here: ', tmapi:path-to-node($precedingItem), ', index nodes: ', string-join(for $i in $n return concat('''&lt;',$i/name(),string-join(for $att in $i/@* return concat(' ',$att/name(),'=''',data($att),''''),''),'&gt;'''),' ')))
                )
        
        let $index      := if ($indexNode) then tmapi:index-of-node($precedingNodes,$indexNode[last()]) else (count($precedingNodes))
        
        for $n in reverse($precedingNodes)
        let $lin := tmapi:index-of-node($precedingNodes,$n) lt $index and 
                    $n[ self::attribute|
                        self::element|
                        self::choice|
                        self::include|
                        self::let|
                        self::defineVariable|
                        self::assert|
                        self::report|
                        self::constraint ]
        return
            if ($lin) then tmapi:recurseItemForEdit($projectPrefix, $n, $language, $valueSetList, $templateList, false(), $mode) else ()
    )
    ,
    if ($item/name()='attribute') then
        for $att in tmapi:normalizeAttributes($item)
        return
        <attribute selected="{if ($node) then 'original' else ''}">
        {
            $att/@name,
            $att/@value,
            attribute conformance {
                if ($att[@prohibited='true']) then 'NP' else
                if ($att[@isOptional='true']) then 'O' else
                    'R'
            }
            ,
            $att/@datatype,
            $att/@id
            ,
            if ($node/@datatype) then (
                attribute originalType {tmapi:getDatatype($node/@datatype,$node/ancestor::template/classification/@format)},
                attribute originalStrength {tmapi:rewriteStrength($node/@strength)}
            ) else 
            if ($att/@datatype) then (
                attribute originalType {tmapi:getDatatype($att/@datatype,$att/ancestor::template/classification/@format)},
                attribute originalStrength {tmapi:rewriteStrength($att/@strength)}
            )
            else ()
            ,
            attribute originalConformance {
                if ($node/@prohibited='true') then 'NP' else 
                if ($node/@isOptional='true') then 'O' else 
                if ($node) then 'R' else 
                    'O'
            }
            ,
            for $desc in $att/desc
            return
            utillib:serializeNode($desc)
            ,
            for $subitem in $att/(* except desc)
            return
                if ($subitem[name()='vocabulary']) then
                    <vocabulary>
                    {
                        $subitem/(@valueSet|@flexibility|@code|@codeSystem|@codeSystemName|@codeSystemVersion|@displayName|@domain)[not(.='')],
                        tmapi:isValueSetInScope($projectPrefix, $subitem/@valueSet,$subitem/@flexibility,$valueSetList),
                        $subitem/*
                    }
                    </vocabulary>
                else (
                    $subitem
                )
        }
        </attribute>
    else if ($item/name()='element') then
        <element selected="{if ($node) then 'original' else ''}">
        {
            $item/(@* except (@strength|@tmid|@tmname|@tmdisplayName|@linkedartefactmissing)),
            tmapi:isTemplateInScope($projectPrefix, $item/@contains, $item/@flexibility, $templateList)
            ,
            if ($item/@strength) then attribute strength {tmapi:rewriteStrength($item/@strength)} else ()
            ,
            if ($node/@datatype) then (
                attribute originalType {tmapi:getDatatype($node/@datatype,$node/ancestor::template/classification/@format)},
                attribute originalStrength {tmapi:rewriteStrength($node/@strength)}
            ) else 
            if ($item/@datatype) then (
                attribute originalType {tmapi:getDatatype($item/@datatype,$item/ancestor::template/classification/@format)},
                attribute originalStrength {tmapi:rewriteStrength($item/@strength)}
            )
            else ()
            ,
            if ($item/@minimumMultiplicity) then () else (
                attribute minimumMultiplicity {''}
            )
            ,
            if ($item/@maximumMultiplicity) then () else (
                attribute maximumMultiplicity {''}
            )
            ,
            if ($node/@minimumMultiplicity) then
                attribute originalMin {$node/@minimumMultiplicity}
            else (
                attribute originalMin {'0'}
            )
            ,
            if ($node/@maximumMultiplicity) then
                attribute originalMax {$node/@maximumMultiplicity}
            else (
                attribute originalMax {'*'}
            )
            ,
            if ($item/@conformance) then () else (
                attribute conformance {'O'}
            )
            ,
            if ($item/@isMandatory) then () else (
                attribute isMandatory {'false'}
            )
            ,
            if ($item/@strength) then () else (
                attribute strength {''}
            )
            ,
            for $desc in $item/desc
            return
                utillib:serializeNode($desc)
            ,
            for $vocabulary in $item/vocabulary
            return
                <vocabulary>
                {
                    $vocabulary/(@valueSet|@flexibility|@code|@codeSystem|@codeSystemName|@codeSystemVersion|@displayName|@domain)[not(.='')],
                    tmapi:isValueSetInScope($projectPrefix, $vocabulary/@valueSet,$vocabulary/@flexibility,$valueSetList),
                    $vocabulary/*
                }
                </vocabulary>
            ,
            $item/property,
            $item/item[1],
            $item/text,
            for $example in $item/example
            return
                <example type="{$example/@type}" caption="{$example/@caption}">
            {
                if (count($example/*) gt 1) then (
                    utillib:serializeNode(<art:placeholder xmlns:art="urn:art-decor:example">{$example/node()}</art:placeholder>)
                )
                else (
                    utillib:serializeNode($example)/node()
                )
            }
            </example>
            ,
            for $subItem in $item/(attribute|element|choice|include|constraint|let|defineVariable|assert|report)
            return
                tmapi:mergePrototypeTemplateForEdit($projectPrefix, $subItem, $prototype, $language, $valueSetList, $templateList, $mode)
        }
        </element>
    else if ($item/name()='choice') then
        <choice selected="{if ($node) then 'original' else ''}">
        {
            $item/@*,
            if (not($item/@minimumMultiplicity)) then
                attribute minimumMultiplicity {''}
            else ()
            ,
            if (not($item/@maximumMultiplicity)) then
                attribute maximumMultiplicity {''}
            else ()
            ,
            if ($node/@minimumMultiplicity) then
                attribute originalMin {$node/@minimumMultiplicity}
            else (attribute originalMin {'0'})
            ,
            if ($node/@maximumMultiplicity) then
                attribute originalMax {$node/@maximumMultiplicity}
            else (attribute originalMax {'*'})
            ,
            for $desc in $item/desc
            return
            utillib:serializeNode($desc)
            ,
            $item/item[1],
            for $subItem in $item/(element|choice|include|constraint)
            return
            tmapi:mergePrototypeTemplateForEdit($projectPrefix, $subItem, $prototype, $language, $valueSetList, $templateList, $mode)
        }
        </choice>
    else if ($item/name()='include') then
        <include selected="{if ($node) then 'original' else ''}">
        {
            $item/(@* except (@tmid|@tmname|@tmdisplayName|@linkedartefactmissing)),
            tmapi:isTemplateInScope($projectPrefix, $item/@ref, $item/@flexibility, $templateList)
            ,
            if (not($item/@minimumMultiplicity)) then
                attribute minimumMultiplicity {''}
            else ()
            ,
            if (not($item/@maximumMultiplicity)) then
                attribute maximumMultiplicity {''}
            else ()
            ,
            if ($node/@minimumMultiplicity) then
                attribute originalMin {$node/@minimumMultiplicity}
            else (
                attribute originalMin {'0'}
            )
            ,
            if ($node/@maximumMultiplicity) then
                attribute originalMax {$node/@maximumMultiplicity}
            else (
                attribute originalMax {'*'}
            )
            ,
            if (not($item/@conformance)) then
                attribute conformance {'O'}
            else ()
            ,
            if (not($item/@isMandatory)) then
                attribute isMandatory {'false'}
            else ()
            ,
            for $desc in $item/desc
            return
            utillib:serializeNode($desc)
            ,
            $item/item[1]
            ,
            for $example in $item/example
            return
                <example type="{$example/@type}" caption="{$example/@caption}">
                {
                    if (count($example/*) gt 1) then (
                        utillib:serializeNode(<art:placeholder xmlns:art="urn:art-decor:example">{$example/node()}</art:placeholder>)
                    )
                    else (
                        utillib:serializeNode($example)/node()
                    )
                }
                </example>
            ,
            for $subItem in $item/constraint
            return
                tmapi:mergePrototypeTemplateForEdit($projectPrefix, $subItem, $prototype, $language, $valueSetList, $templateList, $mode)
        }
        </include>
    else if ($item/name()='let') then
        <let selected="{if ($node) then 'original' else ''}">
        {
            attribute name {$item/@name},
            attribute value {$item/@value},
            $item/node()
        }
        </let>
    else if ($item/name()='defineVariable') then
        <defineVariable selected="{if ($node) then 'original' else ''}">
        {
            attribute name {$item/@name},
            attribute value {$item/@value},
            $item/node()
        }
        </defineVariable>
    else if ($item/name()=('assert','report')) then
        element { name($item) }
        {
            attribute selected {if ($node) then 'original' else ''},
            attribute role {$item/@role},
            attribute test {$item/@test},
            attribute see {$item/@see},
            attribute flag {$item/@flag},
            utillib:serializeNode($item)/node()
        }
    else if ($item/name()='constraint') then
        <constraint selected="{if ($node) then 'original' else ''}">
        {
            $item/(@* except @selected),
            utillib:serializeNode($item)/node()
        }
        </constraint>
    else (),
    (:
       if parent is not 'template' check if there are prototype nodes after the last template node
    :)
    if ($item/following-sibling::*) then () else 
    if ($node) then (
        (: our own current template had a match in the prototype :)
        for $n in ( $node/following-sibling::attribute|
                    $node/following-sibling::element|
                    $node/following-sibling::choice|
                    $node/following-sibling::include|
                    $node/following-sibling::let|
                    $node/following-sibling::defineVariable|
                    $node/following-sibling::assert|
                    $node/following-sibling::report|
                    $node/following-sibling::constraint)
        return
            tmapi:recurseItemForEdit($projectPrefix, $n, $language, $valueSetList, $templateList, false(), $mode)
    )
    else (
        (: since our current node did not match, find any preceding node from our template that matched in the prototype :)
        let $precedingNodes :=
            for $n in ( $item/preceding-sibling::attribute|
                        $item/preceding-sibling::element|
                        $item/preceding-sibling::choice|
                        $item/preceding-sibling::include|
                        $item/preceding-sibling::let|
                        $item/preceding-sibling::defineVariable|
                        $item/preceding-sibling::assert|
                        $item/preceding-sibling::report|
                        $item/preceding-sibling::constraint)
            (: get corresponding node in prototype :)
            let $nodeInPrototype    := util:eval-inline($prototype,tmapi:path-to-node($n))
            let $nodeInPrototype    := if ($nodeInPrototype[@id = $n/@id]) then $nodeInPrototype[@id = $n/@id][1] else $nodeInPrototype[1]
            return
                $nodeInPrototype
        
        (: add all nodes from the prototype template following the last match that we had, to the end of the current template :)
        for $n in ( $precedingNodes[last()]/following-sibling::attribute|
                    $precedingNodes[last()]/following-sibling::element|
                    $precedingNodes[last()]/following-sibling::choice|
                    $precedingNodes[last()]/following-sibling::include|
                    $precedingNodes[last()]/following-sibling::let|
                    $precedingNodes[last()]/following-sibling::defineVariable|
                    $precedingNodes[last()]/following-sibling::assert|
                    $precedingNodes[last()]/following-sibling::report|
                    $precedingNodes[last()]/following-sibling::constraint)
         return
            tmapi:recurseItemForEdit($projectPrefix, $n, $language, $valueSetList, $templateList, false(), $mode)
    )
)

for $result in $results
return
    if ($result/attribute) then (
        element {name($result)} {
            $result/@*,
            $result/attribute[last()] | $result/attribute[last()]/preceding-sibling::* except $result/element,
            $result/attribute[last()]/preceding-sibling::element | $result/attribute[last()]/following-sibling::node()
        }
    )
    else (
        $result
    )
};

(:get prototype for editor with normalized attributes:)
declare %private function tmapi:getPrototype($id as xs:string?, $flexibility as xs:string?, $projectPrefix as xs:string?) as element(template)? {
    if (empty($id)) then
        ()
    else
    if (empty($projectPrefix)) then
        (tmapi:getTemplateById($id, ($flexibility, 'dynamic')[1])[@id][@effectiveDate])[1]
    else (
        (tmapi:getTemplateById($id, ($flexibility, 'dynamic')[1], $projectPrefix, (), ())[@id][@effectiveDate])[1]
    )
};

(:~ Update DECOR template

@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $id                       - required. the id for the template to update
@param $effectiveDate            - required. the effectiveDate for the template to update
@param $request-body             - required. json body containing new template structure
@return template structure 
@since 2020-05-03
:)
declare function tmapi:putTemplate($request as map(*)) {

    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $data                           := utillib:getBodyAsXml($request?body, 'template', ())
    let $deletelock                     := $request?parameters?deletelock = true()
    
    (:let $s                              := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data):) 
    
    let $check                          :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    let $check                          :=
        if (empty($data)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        else ()
    
    let $return                         := tmapi:putTemplate($authmap, $id, $effectiveDate, $data, $deletelock)
    return (
        roaster:response(200, $return)
    )

};

(:~ Update DECOR template parts. Expect array of parameter objects, each containing RFC 6902 compliant contents. Note: RestXQ does not do PATCH (yet), but that would be the preferred option.

{ "op": "[add|remove|replace]", "path": "e.g. [/statusCode|/expirationDate|/officialReleaseDate|/canonicalUri|/versionLabel|/name|/displayName|/experimental|/desc|/publishingAuthority|/copyright|/completeCodeSystem|/conceptList]", "value": "[string|object]" }

where

* op - add & replace (statusCode, expirationDate, officialReleaseDate, canonicalUri, versionLabel, name, displayName, experimental, desc, publishingAuthority, copyright, completeCodeSystem, conceptList) or remove (desc, publishingAuthority, copyright, completeCodeSystem, conceptList)

* path - see above

* value - string when the path is not /. object when path is /
@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $id                       - required. the id for the issue to update 
@param $request-body             - required. json body containing array of parameter objects each containing RFC 6902 compliant contents
@return template structure
@since 2020-05-03
@see http://tools.ietf.org/html/rfc6902
:)
declare function tmapi:patchTemplate($request as map(*)) {

    let $authmap                := $request?user
    let $id                     := $request?parameters?id
    let $effectiveDate          := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $data                   := utillib:getBodyAsXml($request?body, 'parameters', ())
    
    (:let $s                      := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data) :)
    
    let $check                  :=
        if ($data) then () else (
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        )
    
    let $return                 := tmapi:patchTemplate($authmap, string($id), string($effectiveDate), $data)
    return (
        roaster:response(200, $return)
    )

};

(:~ Update DECOR templateAssociation parts. Expect array of parameter objects, each containing RFC 6902 compliant contents. Note: RestXQ does not do PATCH (yet), but that would be the preferred option.

{ "op": "[add|remove|replace]", "path": "e.g. [/concept]", "value": "[object]" }

where

* op - add, replace or remove concept

* path - see above

* value - concept object
@param $bearer-token             - required. provides authorization for the user. The token should be on the X-Auth-Token HTTP Header
@param $id                       - required. the id for the issue to update 
@param $request-body             - required. json body containing array of parameter objects each containing RFC 6902 compliant contents
@return template structure
@since 2020-05-03
@see http://tools.ietf.org/html/rfc6902
:)
declare function tmapi:patchTemplateAssociation($request as map(*)) {

    let $authmap                := $request?user
    let $project                := $request?parameters?project
    let $id                     := $request?parameters?id
    let $effectiveDate          := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    
    let $check                  :=
        if (empty($project)) then 
            error($errors:BAD_REQUEST, 'Request SHALL have parameter project')
        else () 
    
    let $data                   := utillib:getBodyAsXml($request?body, 'parameters', ())
    
    (:let $s                      := xmldb:store('/db/apps/decor/tmp', 'ttt.xml', $data) :)
    
    let $check                  :=
        if ($data) then () else (
            error($errors:BAD_REQUEST, 'Request SHALL have data')
        )
    
    let $return                 := tmapi:patchTemplateAssociation($authmap, $id, $effectiveDate, $project, $data)
    
    return (
        roaster:response(200, $return)
    )

};

(:~ Update template statusCode, versionLabel, expirationDate and/or officialReleaseDate

@param $authmap required. Map derived from token
@param $id required. DECOR concept/@id to update
@param $effectiveDate required. DECOR concept/@effectiveDate to update
@param $recurse optional as xs:boolean. Default: false. Allows recursion into child particles to apply the same updates
@param $listOnly optional as xs:boolean. Default: false. Allows to test what will happen before actually applying it
@param $newStatusCode optional as xs:string. Default: empty. If empty, does not update the statusCode
@param $newVersionLabel optional as xs:string. Default: empty. If empty, does not update the versionLabel
@param $newExpirationDate optional as xs:string. Default: empty. If empty, does not update the expirationDate 
@param $newOfficialReleaseDate optional as xs:string. Default: empty. If empty, does not update the officialReleaseDate 
@return list object with success and/or error elements or error
:)
declare function tmapi:putTemplateStatus($request as map(*)) {

    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $recurse                        := $request?parameters?recurse = true()
    let $list                           := $request?parameters?list = true()
    let $statusCode                     := $request?parameters?statusCode
    let $versionLabel                   := $request?parameters?versionLabel
    let $expirationDate                 := $request?parameters?expirationDate
    let $officialReleaseDate            := $request?parameters?officialReleaseDate
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    
    return
        tmapi:setTemplateStatus($authmap, $id, $effectiveDate, $recurse, $list, $statusCode, $versionLabel, $expirationDate, $officialReleaseDate)
};

(:~ Central logic for creating an initial template

@param $authmap             - required. Map derived from token
@param $request-body        - required. json body containing new template structure
@param $lock                - required. true = create lock on result. false = do not create a lock on result
@return template structure
:)
declare function tmapi:postTemplate($authmap as map(*), $projectPrefix as xs:string, $data as element(), $keepIds as xs:boolean, $lock as xs:boolean) as element(template) {

    let $storedTemplate                 := 
        if ($data/@id[not(. = '')]) then 
            $setlib:colDecorData//template[@id = $data/@id][@effectiveDate = $data/@effectiveDate] |
            $setlib:colDecorCache//template[@id = $data/@id][@effectiveDate = $data/@effectiveDate]
        else ()
    let $originalTemplate               :=
        if ($data/@ref[not(. = '')]) then
            tmapi:getPrototype($data/@ref, if ($data/@flexibility[not(. = '')]) then $data/@flexibility else 'dynamic', $projectPrefix)
        else () 
    let $decor                          := utillib:getDecorByPrefix($projectPrefix)
    
    let $check                          :=
        if ($storedTemplate) then 
            error($errors:BAD_REQUEST, 'Template with id ''' || ($data/@id)[1] || ''' and effectiveDate ''' || $data/@effectiveDate || ''' already exists in project ' || string-join($storedTemplate/ancestor::decor/project/@prefix, ', '))
        else ()
    let $check                          :=
        if ($data/@ref[not(. = '')]) then 
            if ($originalTemplate) then () else (
                error($errors:BAD_REQUEST, 'Template with id ''' || ($data/@ref)[1] || ''' and effectiveDate ''' || $data/@flexibility || ''' not found in project ' || $projectPrefix)
            )
        else ()
    let $check                          :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-RULES)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to add templates in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    
    (:relevant only for mode=new and when a concept is connected somewhere, but doesn't hurt to generate for all cases:)
    let $newTemplateElementId           := decorlib:getNextAvailableIdP($decor, $decorlib:OBJECTTYPE-TEMPLATEELEMENT, ())[1]/@id
    
    let $preparedTemplate               := 
        if ($originalTemplate) then
            <template ref="{$data/@ref}" name="{$originalTemplate/@name}" displayName="{($originalTemplate/@displayName, $originalTemplate/@name)[not(. = '')][1]}"/>
        else
        if ($keepIds and $data/@id) then
            tmapi:prepareIncomingTemplateForUpdate($decor, $data, $storedTemplate, 'version', $newTemplateElementId)
        else (
            tmapi:prepareIncomingTemplateForUpdate($decor, $data, $storedTemplate, 'new', $newTemplateElementId)
        )
    (: Note: this step also rolls up multiple templateAssociation elements into one. This is unlikely to occur unless manually. :)
    let $storedTemplateAssociations     := $decor//templateAssociation[@templateId = $data/@id][@effectiveDate = $data/@effectiveDate]
    let $preparedTemplateAssociation    := if ($preparedTemplate[@id]) then tmapi:prepareTemplateAssociationForUpdate($storedTemplateAssociations, $preparedTemplate, $data, $newTemplateElementId, 'new') else ()
    
    (:let $intention              := if ($storedTemplate[@statusCode = ('active', 'final')]) then 'patch' else 'version'
    let $update                 := histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-TEMPLATE, $projectPrefix, $intention, $storedTemplate):)
    
    (: now update the template :)
    let $storedReference                := if ($preparedTemplate[@id]) then () else $decor/rules/template[@ref = $preparedTemplate/@ref]
    let $update                         := if ($storedReference) then update replace $storedReference with $preparedTemplate else update insert $preparedTemplate into $decor/rules
    let $addids                         := if ($preparedTemplate[@id]) then tmapi:addTemplateElementAndAttributeIds($decor, $preparedTemplate/@id, $preparedTemplate/@effectiveDate) else ()
    let $storedTemplate                 := if ($preparedTemplate[@id]) then $decor/rules/template[@id = $preparedTemplate/@id][@effectiveDate = $preparedTemplate/@effectiveDate] else $decor/rules/template[@ref = $preparedTemplate/@ref]
    let $delete                         := update delete $storedTemplate//@json:array
    
    (: now rewrite any templateAssociations. We don't want broken links into elements or attributes that do not exist anymore :)
    let $update                         := 
        if ($storedTemplateAssociations) then (
            update delete $storedTemplateAssociations,
            update insert $preparedTemplateAssociation preceding $storedTemplate
        )
        else
        if ($preparedTemplateAssociation) then (
            update insert text {'&#x0a;        '} preceding $storedTemplate,
            update insert comment {concat(' ',$preparedTemplate/@displayName,' ')} preceding $storedTemplate,
            update insert $preparedTemplateAssociation preceding $storedTemplate
        ) else ()
    
    let $newlock                        := if ($lock) then decorlib:setLock($authmap, $preparedTemplate/@id, $preparedTemplate/@effectiveDate, false()) else ()
    
    let $result                         := 
        if ($lock) then
            tmapi:getTemplateForEdit($authmap, $projectPrefix, 'edit', $preparedTemplate/@id, $preparedTemplate/@effectiveDate, true(), true(), false())
        else
        if ($preparedTemplate[@id]) then
            tmapi:getTemplate($preparedTemplate/@id, $preparedTemplate/@effectiveDate, $projectPrefix, (), ())
        else (
            tmapi:getTemplate($data/@ref, $data/@flexibility, $projectPrefix, (), ())
        )
        
    return
        element {name($result[@id][1])} {
            $result[@id][1]/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result[@id][1]/*)
        }
};

(:~ Central logic for updating an existing template

@param $authmap                 - required. Map derived from token
@param $id                       - required. the id for the template to update
@param $effectiveDate            - required. the effectiveDate for the template to update
@param $request-body             - required. json body containing new template structure
@return template structure
:)
declare function tmapi:putTemplate($authmap as map(*), $id as xs:string, $effectiveDate as xs:string, $data as element(), $deletelock as xs:boolean) as element(template) {

    let $storedTemplate         := $setlib:colDecorData//template[@id = $data/@originalId][@effectiveDate = $effectiveDate] 
    let $decor                  := $storedTemplate/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix
    
    let $lock                   := decorlib:getLocks($authmap, $id, $effectiveDate, ())
    let $lock                   := if ($lock) then $lock else decorlib:setLock($authmap, $id, $effectiveDate, false())
    
    let $check                  :=
        if ($storedTemplate) then () else (
            error($errors:BAD_REQUEST, 'Template with id ''' || ($data/@originalId, $id)[1] || ''' and effectiveDate ''' || $effectiveDate || ''' does not exist')
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-RULES)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify templates in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if ($lock) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have a lock for this template (anymore). Get a lock first.'))
        )
    let $check                  :=
        if ($storedTemplate[@statusCode = $tmapi:STATUSCODES-FINAL]) then
            error($errors:BAD_REQUEST, concat('Template cannot be updated while it has one of status: ', string-join($tmapi:STATUSCODES-FINAL, ', '), if ($storedTemplate/@statusCode = 'pending') then 'You should switch back to status draft to edit.' else ()))
        else ()
    let $check                  :=
        if ($data[@originalId = $id] | $data[empty(@originalId)][@id = $id]) then () else (
            error($errors:BAD_REQUEST, concat('Submitted data shall have no template id or the same template id as the template id ''', $id, ''' used for updating. Found in request body: ', ($data/@originalId, $data/@id, 'null')[1]))
        )
    let $check                  :=
        if ($data[@effectiveDate = $effectiveDate] | $data[empty(@effectiveDate)]) then () else (
            error($errors:BAD_REQUEST, concat('Submitted data shall have no template effectiveDate or the same template effectiveDate as the template effectiveDate ''', $effectiveDate, ''' used for updating. Found in request body: ', ($data/@effectiveDate, 'null')[1]))
        )
    
    (:relevant only for mode=new and when a concept is connected somewhere, but doesn't hurt to generate for all cases:)
    let $newTemplateElementId   := decorlib:getNextAvailableIdP($decor, $decorlib:OBJECTTYPE-TEMPLATEELEMENT, ())[1]/@id
    
    let $preparedTemplate       := tmapi:prepareIncomingTemplateForUpdate($decor, $data, $storedTemplate, 'edit', $newTemplateElementId)
    (: Note: this step also rolls up multiple templateAssociation elements into one. This is unlikely to occur unless manually. :)
    let $storedTemplateAssociations     := $decor//templateAssociation[@templateId = $data/@originalId][@effectiveDate = $data/@effectiveDate]
    let $preparedTemplateAssociation    := tmapi:prepareTemplateAssociationForUpdate($storedTemplateAssociations, $preparedTemplate, $data, $newTemplateElementId, 'edit')
    
    let $intention              := if ($storedTemplate[@statusCode = ('active', 'final')]) then 'patch' else 'version'
    let $update                 := histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-TEMPLATE, $projectPrefix, $intention, $storedTemplate)
    
    (: now update the template :)
    let $update                 := update replace $storedTemplate with $preparedTemplate
    let $addids                 := tmapi:addTemplateElementAndAttributeIds($decor, $preparedTemplate/@id, ($preparedTemplate/@effectiveDate)[1])
    let $delete                 := update delete $storedTemplate//@json:array
    
    (: now rewrite any templateAssociations. We don't want broken links into elements or attributes that do not exist anymore :)
    let $update                 := 
        if ($storedTemplateAssociations) then (
            update delete $storedTemplateAssociations,
            update insert $preparedTemplateAssociation preceding $storedTemplate
        )
        else (
            update insert text {'&#x0a;        '} preceding $storedTemplate,
            update insert comment {concat(' ',$preparedTemplate/@displayName,' ')} preceding $storedTemplate,
            update insert $preparedTemplateAssociation preceding $storedTemplate
        )
    
    let $deleteLock             := if ($deletelock) then update delete $lock else ()
    let $result                 := 
        if ($deletelock) then
            tmapi:getTemplate($preparedTemplate/@id, $preparedTemplate/@effectiveDate, $projectPrefix, (), ())
        else (
            tmapi:getTemplateForEdit($authmap, $projectPrefix, 'edit', $preparedTemplate/@id, $preparedTemplate/@effectiveDate, true(), true(), false())
        )
    
    return
        element {name($result[@id][1])} {
            $result[@id][1]/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result[@id][1]/*)
        }
};

(:~ Retrieves DECOR template history based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id
    @param $effectiveDate           - optional parameter denoting the effectiveDate. If not given assumes latest version for id
    @return as-is or as compiled as JSON
    @since 2020-05-03
:)
declare function tmapi:getTemplateHistory($request as map(*)) {
    let $authmap                        := $request?user
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    let $projectPrefix                  := ()
    let $results                        := histlib:ListHistory($authmap, $decorlib:OBJECTTYPE-TEMPLATE, (), $id, $effectiveDate, 0)
    
    return
        <list artifact="{$decorlib:OBJECTTYPE-TEMPLATE}" current="{count($results)}" total="{count($results)}" all="{count($results)}" lastModifiedDate="{current-dateTime()}">
        {
            utillib:addJsonArrayToElements($results)
        }
        </list>
};

(:~ Retrieves DECOR template usage based on $id (oid) and optionally $effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
    @param $id                      - required parameter denoting the id of the template
    @param $effectiveDate           - optional parameter denoting the effectiveDate of the template. If not given assumes latest version for id
    @param $projectPrefix           - optional. limits scope to this project only
    @param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
    @return as-is or as compiled as JSON
    @since 2021-12-16
:)
declare function tmapi:getTemplateUsage($request as map(*)) {
    
    let $projectPrefix                  := $request?parameters?prefix
    (:let $projectVersion                 := $request?parameters?release
    let $projectLanguage                := $request?parameters?language:)
    let $id                             := $request?parameters?id
    let $effectiveDate                  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    
    let $results                        := tmapi:getDependenciesAndUsage($projectPrefix, (), (), $id, $effectiveDate)
    (:let $mostRecent                     := tmapi:getTemplate($id, $effectiveDate, $projectPrefix, $projectVersion, $projectLanguage)/@effectiveDate
    let $isMostRecent                   := $mostRecent = $effectiveDate or empty($effectiveDate)
    
    let $allTransactions                := utillib:getTransactionsByTemplate($id, $effectiveDate)
    
    let $results                        := (
        for $item in $allTransactions
        let $trid := $item/@id
        let $tred := $item/@effectiveDate
        group by $trid, $tred
        return
            utillib:doTransactionAssociation($ds, $item[1])
    ):)
    
    let $count                          := count($results)
    let $max                            := $count
    let $allcnt                         := $count
    
    return
        <list artifact="USAGE" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements($results)
        }
        </list>
};

(: ======== ADD TEMPLATE ELEMENT/ATTRIBUTE IDS ============ :)
declare function tmapi:postAddTemplateIds($request as map(*)) {
    
    let $authmap                := $request?user
    let $project                := $request?parameters?project[string-length() gt 0]
    let $templateId             := $request?parameters?id
    let $templateEffectiveDate  := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?effectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?effectiveDate[string-length() gt 0]
        }
    
    let $check                  :=
        if (empty($project)) then
            error($errors:BAD_REQUEST, 'You are missing required parameter project')
        else ()
    
    let $decorProjects          := if (utillib:isOid($project)) then utillib:getDecorById($project) else utillib:getDecorByPrefix($project)
    
    let $check                  :=
        if ($decorProjects) then () else (
            error($errors:BAD_REQUEST, concat('Project with prefix or id ''', $project, ''', does not exist.'))
        )
    
    let $update                 := 
        for $decor in $decorProjects
        let $check                  :=
            if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-RULES)) then () else (
                error($errors:FORBIDDEN, 'User ' || $authmap?name || ' does not have sufficient permissions to edit templates in project ' || $decor/project/@prefix || '. You have to be an active author in the project.')
            )
        return tmapi:addTemplateElementAndAttributeIds($decor, $templateId, $templateEffectiveDate)
    
    return
        roaster:response(200, ())
};
(: ======== ADD TEMPLATE ELEMENT/ATTRIBUTE IDS ============ :)

(:~ Central logic for patching an existing template

@param $authmap         - required. Map derived from token
@param $id              - required. DECOR template/@id to update
@param $effectiveDate   - required. DECOR template/@effectiveDate to update
@param $data            - required. DECOR xml element parameter elements each containing RFC 6902 compliant contents
@return template object
:)
declare function tmapi:patchTemplate($authmap as map(*), $id as xs:string, $effectiveDate as xs:string, $data as element(parameters)) as element(template) {

    let $storedTemplate         := $setlib:colDecorData//template[@id = $id][@effectiveDate = $effectiveDate]
    let $decor                  := $storedTemplate/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix

    let $lock                   := decorlib:getLocks($authmap, $id, $effectiveDate, ())
    let $lock                   := if ($lock) then $lock else decorlib:setLock($authmap, $id, $effectiveDate, false())
    
    let $check                  :=
        if ($storedTemplate) then () else (
            error($errors:BAD_REQUEST, 'Template with id ' || $id || ' and effectiveDate ' || $effectiveDate || ''' does not exist')
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-TERMINOLOGY)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify templates in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    let $check                  :=
        if ($storedTemplate[@statusCode = $tmapi:STATUSCODES-FINAL]) then
            if ($data/parameter[@path = '/statusCode'][@op = 'replace'][not(@value = $tmapi:STATUSCODES-FINAL)]) then () else
            if ($data[count(parameter) = 1]/parameter[@path = '/statusCode']) then () else (
                error($errors:BAD_REQUEST, concat('Template cannot be patched while it has one of status: ', string-join($tmapi:STATUSCODES-FINAL, ', '), if ($storedTemplate/@statusCode = 'pending') then 'You should switch back to status draft to edit.' else ()))
            )
        else ()
    let $check                  :=
        if ($lock) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have a lock for this template (anymore). Get a lock first.'))
        )
    let $check                  :=
        if ($data[parameter]) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall be an array of ''parameter''.')
        )
    let $unsupportedops         := $data/parameter[not(@op = ('add', 'replace', 'remove'))]
    let $check                  :=
        if ($unsupportedops) then
            error($errors:BAD_REQUEST, 'Submitted parameters shall have supported ''op'' value. Found ''' || string-join(distinct-values($unsupportedops/@op), ''', ''') || ''', expected ''add'', ''replace'', or ''remove''') 
        else ()
    
    let $check                  :=
        for $param in $data/parameter
        let $op         := $param/@op
        let $path       := $param/@path
        let $value      := $param/@value
        let $pathpart   := substring-after($path, '/')
        return
            switch ($path)
            case '/statusCode' return (
                if ($op = 'remove') then
                    'Parameter path ''' || $path || ''' does not support ' || $op
                else 
                if (utillib:isStatusChangeAllowable($storedTemplate, $value)) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Supported are: ' || string-join(map:get($utillib:templatestatusmap, string($storedTemplate/@statusCode)), ', ')
                )
            )
            case '/expirationDate'
            case '/officialReleaseDate' return (
                if ($op = 'remove') then () else 
                if ($value castable as xs:dateTime) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL be yyyy-mm-ddThh:mm:ss.'
                )
            )
            case '/canonicalUri'
            case '/versionLabel' return (
                if ($op = 'remove') then () else 
                if ($value[not(. = '')]) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL NOT be empty.'
                )
            )
            case '/name' 
            case '/displayName' return (
                if ($op = 'remove') then
                    'Parameter path ''' || $path || ''' does not support ' || $op
                else 
                if ($value[not(. = '')]) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL NOT be empty.'
                )
            )
            case '/isClosed' return (
                if ($op = 'remove') then () else 
                if ($value[. = ('true', 'false')]) then () else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' with value ''' || $value || '''. Value SHALL be true or false.'
                )
            )
            case '/desc' 
            case '/copyright'
            case '/purpose' return (
                if ($param[count(value/*[name() = $pathpart]) = 1]) then (
                    if ($param/value/*[name() = $pathpart][@inherited]) then 
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Contents SHALL NOT be marked ''inherited''.'
                    else (),
                    if ($param/value/*[name() = $pathpart][matches(@language, '[a-z]{2}-[A-Z]{2}')]) then
                        if ($param[@op = 'remove'] | $param/value/*[name() = $pathpart][.//text()[not(normalize-space() = '')]]) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A ' || $pathpart || ' SHALL have contents.'
                        )
                    else (
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. A ' || $pathpart || ' SHALL have a language with pattern ll-CC.'
                    )
                )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/*[name() = $pathpart]) 
                )
            )
            case '/classification' return (
                if ($param[count(value/classification) = 1]) then 
                    if ($op = 'remove') then () else (
                        if ($param/value/classification[@format]) then
                            if ($param/value/classification[@format = $tmapi:TEMPLATE-FORMATS/enumeration/@value]) then () else (
                                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Classification has unsupported .format value ' || string-join($param/value/classification/@format, ', ') || '. SHALL be one of: ' || string-join($tmapi:TEMPLATE-FORMATS/enumeration/@value, ', ')
                            )
                        else (),
                        if ($param/value/classification[@type]) then
                            if ($param/value/classification[@type = $tmapi:TEMPLATE-TYPES/enumeration/@value]) then () else (
                                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Classification has unsupported .type value ' || string-join($param/value/classification/@type, ', ') || '. SHALL be one of: ' || string-join($tmapi:TEMPLATE-TYPES/enumeration/@value, ', ')
                            )
                        else ()
                    )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/*[name() = $pathpart]) 
                )
            )
            case '/relationship' return (
                if ($param[count(value/relationship) = 1]) then 
                    if ($op = 'remove') then () else (
                        if ($param/value/relationship[@type = $tmapi:RELATIONSHIP-TYPES/enumeration/@value]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Relationship has unsupported .type value ' || string-join($param/value/relationship/@type, ', ') || '. SHALL be one of: ' || string-join($tmapi:RELATIONSHIP-TYPES/enumeration/@value, ', ')
                        ),
                        if ($param/value/relationship[count(@template | @model) = 1]) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Relationship SHALL have exactly one of template or model property'
                        )
                    )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/*[name() = $pathpart]) 
                )
            )
            case '/context' return (
                if ($param[count(value/context) = 1]) then 
                    if ($op = 'remove') then () else (
                        if ($param/value/context[count(@id | @path) = 1]) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Context SHALL have exactly one of id or path property'
                        ),
                        if ($param/value/context[empty(@id)] | $param/value/context[@id = ('*', '**')]) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Context SHALL be one of * or **'
                        )
                    )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/*[name() = $pathpart]) 
                )
            )
            case '/item' return (
                if ($param[count(value/item) = 1]) then 
                    if ($op = 'remove') then () else (
                        if ($param/value/item[@label]) then () else (
                            'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Item SHALL have label property'
                        )
                    )
                else (
                    'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Input SHALL have exactly one ' || $pathpart || ' under value. Found ' || count($param/value/*[name() = $pathpart]) 
                )
            )
            case '/publishingAuthority' return (
                if ($param[count(value/publishingAuthority) = 1]) then (
                    if ($param/value/publishingAuthority[@inherited]) then 
                        'Parameter ' || $param/@op || ' of path ' || $param/@path || ' not supported. Contents SHALL NOT be marked ''inherited''.'
                    else (),
                    if ($param/value/publishingAuthority[@id]) then
                        if ($param/value/publishingAuthority[utillib:isOid(@id)]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || ''' publishingAuthority.id SHALL be an OID is present. Found ' || string-join($param/value/publishingAuthority/@id, ', ')
                        )
                    else (),
                    if ($param/value/publishingAuthority[@name[not(. = '')]]) then () else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || ''' publishingAuthority.name SHALL be a non empty string.'
                    ),
                    let $unsupportedtypes := $param/value/publishingAuthority/addrLine/@type[not(. = $tmapi:ADDRLINE-TYPE/enumeration/@value)]
                    return
                        if ($unsupportedtypes) then
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. Publishing authority has unsupported addrLine.type value(s) ' || string-join($unsupportedops, ', ') || '. SHALL be one of: ' || string-join($tmapi:ADDRLINE-TYPE/enumeration/@value, ', ')
                        else ()
                ) else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' SHALL have a single publishingAuthority under value. Found ' || count($param/value/publishingAuthority)
                )
            )
            case '/example' return (
                let $unsupportedtypes := $param/value/example/@type[not(. = $tmapi:EXAMPLE-TYPES/enumeration/@value)]
                return
                    if ($unsupportedtypes) then
                        'Parameter ' || $op || ' not allowed for ''' || $path || '''. Example has unsupported type value(s) ' || string-join($unsupportedops, ', ') || '. SHALL be one of: ' || string-join($tmapi:EXAMPLE-TYPES/enumeration/@value, ', ')
                    else ()
            )
            default return (
                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Path value not supported'
            )
     let $check                 :=
        if (empty($check)) then () else (
            error($errors:BAD_REQUEST,  string-join ($check, ' '))
        )
    
    let $intention              := if ($storedTemplate[@statusCode = 'final']) then 'patch' else 'version'
    let $update                 := histlib:AddHistory($authmap?name, $decorlib:OBJECTTYPE-TEMPLATE, $projectPrefix, $intention, $storedTemplate)
    
    let $update                 :=
        for $param in $data/parameter
        return
            switch ($param/@path)
            case '/statusCode'
            case '/expirationDate'
            case '/officialReleaseDate'
            case '/canonicalUri'
            case '/versionLabel'
            case '/name'
            case '/displayName'
            case '/isClosed' return (
                let $attname  := substring-after($param/@path, '/')
                let $new      := attribute {$attname} {$param/@value}
                let $stored   := $storedTemplate/@*[name() = $attname]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedTemplate
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/desc' return (
                (: only one per language :)
                let $elmname  := substring-after($param/@path, '/')
                let $new      := utillib:prepareFreeFormMarkupWithLanguageForUpdate($param/value/desc)
                let $stored   := $storedTemplate/*[name() = $elmname][@language = $param/value/desc/@language]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedTemplate
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/publishingAuthority' return (
                (: only one possible :)
                let $new      := utillib:preparePublishingAuthorityForUpdate($param/value/publishingAuthority)
                let $stored   := $storedTemplate/publishingAuthority
                
                return
                switch ($param/@op)
                case 'add' (: fall through :)
                case 'replace' return if ($stored) then update replace $stored with $new else update insert $new into $storedTemplate
                case 'remove' return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/purpose' return (
                (: only one possible per language :)
                let $elmname  := substring-after($param/@path, '/')
                let $new      := utillib:prepareFreeFormMarkupWithLanguageForUpdate($param/value/purpose)
                let $stored   := $storedTemplate/*[name() = $elmname][@language = $param/value/purpose/@language]
                
                return
                switch ($param/@op)
                case 'add' (: fall through :)
                case 'replace' return if ($stored) then update replace $stored with $new else update insert $new into $storedTemplate
                case 'remove' return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/copyright' return (
                (: only one per language :)
                let $new      := utillib:prepareFreeFormMarkupWithLanguageForUpdate($param/value/copyright)
                let $stored   := $storedTemplate/copyright[@language = $param/value/copyright/@language]
                
                return
                switch ($param/@op)
                case ('add') (: fall through :)
                case ('replace') return if ($stored) then update replace $stored with $new else update insert $new into $storedTemplate
                case ('remove') return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/classification' return (
                (: only one per type or format :)
                let $new      := tmapi:prepareClassificationForUpdate($param/value/classification)
                let $stored   := $storedTemplate/classification
                
                return
                switch ($param/@op)
                case 'add' (: fall through :)
                case 'replace' return if ($stored) then (update insert $new preceding $stored[1], update delete $stored) else (update insert $new into $storedTemplate)
                case 'remove' return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/relationship' return (
                (: only one per template or model, insert before others :)
                let $new      := tmapi:prepareRelationshipForUpdate($param/value/relationship)
                let $stored   := $storedTemplate/relationship[@template = $new/@template] | $storedTemplate/relationship[@model = $new/@model]
                
                return
                switch ($param/@op)
                case 'add' (: fall through :)
                case 'replace' return if ($stored) then (update insert $new preceding $stored[1], update delete $stored) else (update insert $new into $storedTemplate)
                case 'remove' return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/context' return (
                (: only one :)
                let $new      := tmapi:prepareContextForUpdate($param/value/context)
                let $stored   := $storedTemplate/context
                
                return
                switch ($param/@op)
                case 'add' (: fall through :)
                case 'replace' return if ($stored) then update replace $stored with $new else update insert $new into $storedTemplate
                case 'remove' return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/item' return (
                (: only one :)
                let $new      := tmapi:prepareItemForUpdate($param/value/item)
                let $stored   := $storedTemplate/item[1]
                
                return
                switch ($param/@op)
                case 'add' (: fall through :)
                case 'replace' return if ($stored) then update replace $stored with $new else update insert $new into $storedTemplate
                case 'remove' return update delete $stored
                default return ( (: unknown op :) )
            )
            case '/example' return (
                (: only one :)
                let $new      := 
                    for $example in $param/value/example
                    return
                        <example>
                        {
                            $example/@type[not(. = '')],
                            $example/@caption[not(. = '')],
                            $example/node()
                        }
                        </example>
                let $stored   := update delete $storedTemplate/example
                
                return
                switch ($param/@op)
                case 'add' (: fall through :)
                case 'replace' return if ($new) then update insert $new into $storedTemplate else ()
                case 'remove' return ()
                default return ( (: unknown op :) )
            )
            default return ( (: unknown path :) )
    
    (: after all the updates, the XML Schema order is likely off. Restore order :)
    let $preparedTemplate       := tmapi:prepareTemplateForUpdate($storedTemplate)
    
    let $update                 := update replace $storedTemplate with $preparedTemplate
    let $update                 := update delete $lock
    
    let $result                 := tmapi:getTemplate($preparedTemplate/@id, $preparedTemplate/@effectiveDate, $projectPrefix, (), ())[@id]
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

(:~ Central logic for patching an existing templateAssociation

@param $authmap         - required. Map derived from token
@param $id              - required. DECOR template/@id to update
@param $effectiveDate   - required. DECOR template/@effectiveDate to update
@param $data            - required. DECOR xml element parameter elements each containing RFC 6902 compliant contents
@return templateAssociation object
:)
declare function tmapi:patchTemplateAssociation($authmap as map(*), $id as xs:string, $effectiveDate as xs:string, $decorOrPrefix as item(), $data as element(parameters)) as element(template) {
    
    let $decor                  := utillib:getDecor($decorOrPrefix, (), ())

    let $check                  :=
        if (empty($decor)) then
            error($errors:BAD_REQUEST, 'Project with prefix or id ' || $decorOrPrefix || ' not found.')
        else ()
        
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-TERMINOLOGY)) then () else (
            error($errors:FORBIDDEN, 'User ' || $authmap?name || ' does not have sufficient permissions to modify templates in project ' || $decor/project/@prefix || '. You have to be an active author in the project.')
        )
    
    let $storedTemplate         := tmapi:getTemplateById($id, $effectiveDate, $decorOrPrefix, (), ())
    
    let $check                  :=
        if (count($storedTemplate) gt 1) then
            error($errors:SERVER_ERROR, 'Cannot patch template with id "' || $id || '" effectiveDate "' || $effectiveDate || '" project "' || ($decor/project/@prefix)[1] || '", because there are ' || count($storedTemplate) || ' matching templates. Please inform your administrator.')
        else ()
    
    let $updateTemplate         := $decor//template[@id = $id][@effectiveDate = $effectiveDate] | $decor//template[@ref = $id]
    
    let $check                  :=
        if ($storedTemplate) then () else (
            error($errors:BAD_REQUEST, 'Template with id ' || $id || ' and effectiveDate ' || $effectiveDate || ''' does not exist')
        )
 
    let $check                  :=
        if ($data[parameter]) then () else (
            error($errors:BAD_REQUEST, 'Submitted data shall be an array of ''parameter''.')
        )
    let $unsupportedops         := $data/parameter[not(@op = ('add', 'replace', 'remove'))]
    let $check                  :=
        if ($unsupportedops) then
            error($errors:BAD_REQUEST, 'Submitted parameters shall have supported ''op'' value. Found ''' || string-join(distinct-values($unsupportedops/@op), ''', ''') || ''', expected ''add'', ''replace'', or ''remove''') 
        else ()
    
    let $check                  :=
        for $param in $data/parameter
        let $op         := $param/@op
        let $path       := $param/@path
        let $value      := $param/@value
        let $pathpart   := substring-after($path, '/')
        return
            switch ($path)
            case '/concept' return (
                if ($param[count(value/concept) = 1]) then (
                    if ($op = 'remove') then () else 
                    if ($param/value/concept[@ref][@effectiveDate][@elementId]) then (
                        let $datasetConcept := utillib:getConcept($param/value/concept/@ref, $param/value/concept/@effectiveDate)
                        return
                        if (empty($datasetConcept)) then
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. TemplateAssociation concept id ''' || $param/value/concept/@ref || ''' effectiveDate ''' || $param/value/concept/@effectiveDate || ''' not found.'
                        else 
                        if ($datasetConcept/ancestor::decor/project[not(@prefix = $decor/project/@prefix)]) then 
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. TemplateAssociation concept id ''' || $param/value/concept/@ref || ''' effectiveDate ''' || $param/value/concept/@effectiveDate || ''' SHALL be in the same project. Project indicated: ' || $decor/project/@prefix || ', concept in ' || ($datasetConcept/ancestor::decor/project/@prefix) 
                        else (),
                        if ($storedTemplate//attribute[@id = $param/value/concept/@elementId] | $storedTemplate//element[@id = $param/value/concept/@elementId]) then () else (
                            'Parameter ' || $op || ' not allowed for ''' || $path || '''. TemplateAssociation attribute or element id ''' || $param/value/concept/@elementId || ''' not found.'
                        )
                    ) else (
                        'Parameter ' || $op || ' not allowed for ''' || $path || '''. A templateAssociation concept SHALL have a ref, effectiveDate and elementId.'
                    )
                ) else (
                    'Parameter ' || $op || ' not allowed for ''' || $path || ''' SHALL have a single concept under value. Found ' || count($param/value/concept)
                )  
            )
              
            default return (
                'Parameter ' || $op || ' not allowed for ''' || $path || '''. Path value not supported'
            )
     let $check                 :=
        if (empty($check)) then () else (
            error($errors:BAD_REQUEST,  string-join ($check, ' '))
        )
    
    let $update                 :=
        for $param in $data/parameter
        return
            switch ($param/@path)
            case '/concept' return (
                let $paramValue := $param/value/concept
                let $preparedTemplateAssociation := <templateAssociation templateId="{$storedTemplate/@id}" effectiveDate="{$storedTemplate/@effectiveDate}"/>
                    let $new      := 
                    <concept>
                    {
                        $paramValue/@ref,
                        $paramValue/@effectiveDate,
                        ($paramValue/@elementId, $paramValue/@elementPath)[1]  
                    }
                    </concept>
                let $storedTemplateAssociation := $decor//templateAssociation[@templateId = $storedTemplate/@id][@effectiveDate = $storedTemplate/@effectiveDate] 
                let $storedTemplateAssociation := 
                    if ($storedTemplateAssociation) then ($storedTemplateAssociation) else (
                        let $update := update insert text {'&#x0a;        '} preceding $updateTemplate
                        let $update := update insert comment {concat(' ',$storedTemplate/@displayName,' ')} preceding $updateTemplate
                        let $update := update insert $preparedTemplateAssociation preceding $updateTemplate
                        
                        return
                        $decor//templateAssociation[@templateId = $storedTemplate/@id][@effectiveDate = $storedTemplate/@effectiveDate]
                    )
                let $stored                    := $storedTemplateAssociation/concept[@ref = $paramValue/@ref][@effectiveDate = $paramValue/@effectiveDate]
                let $stored                    := 
                    if ($paramValue/@elementId) then ($stored[@elementId = $paramValue/@elementId]) else 
                    if ($paramValue/@elementPath) then ($stored[@elementPath = $paramValue/@elementPath]) else ($stored)
                
                return
                switch ($param/@op)
                case 'add' (: fall through :)
                case 'replace' return if ($stored) then update replace $stored with $new else update insert $new into $storedTemplateAssociation[1]
                case 'remove' return update delete $stored
                default return ( (: unknown op :) )
            )
            default return ( (: unknown path :) )
    
    let $result                 := $decor//templateAssociation[@templateId = $storedTemplate/@id][@effectiveDate = $storedTemplate/@effectiveDate]
    
    return
        element {name($result)} {
            $result/@*,
            namespace {"json"} {"http://www.json.org"},
            utillib:addJsonArrayToElements($result/*)
        }
};

(:~ Central logic for patching an existing transaction statusCode, versionLabel, expirationDate and/or officialReleaseDate

@param $authmap required. Map derived from token
@param $id required. DECOR concept/@id to update
@param $effectiveDate required. DECOR concept/@effectiveDate to update
@param $recurse optional as xs:boolean. Default: false. Allows recursion into child particles to apply the same updates
@param $listOnly optional as xs:boolean. Default: false. Allows to test what will happen before actually applying it
@param $newStatusCode optional as xs:string. Default: empty. If empty, does not update the statusCode
@param $newVersionLabel optional as xs:string. Default: empty. If empty, does not update the versionLabel
@param $newExpirationDate optional as xs:string. Default: empty. If empty, does not update the expirationDate 
@param $newOfficialReleaseDate optional as xs:string. Default: empty. If empty, does not update the officialReleaseDate 
@return list object with success and/or error elements or error
:)
declare function tmapi:setTemplateStatus($authmap as map(*), $id as xs:string, $effectiveDate as xs:string?, $recurse as xs:boolean, $listOnly as xs:boolean, $newStatusCode as xs:string?, $newVersionLabel as xs:string?, $newExpirationDate as xs:string?, $newOfficialReleaseDate as xs:string?) as element(list) {
    (:get object for reference:)
    let $object                 := 
        if ($effectiveDate castable as xs:dateTime) then 
            $setlib:colDecorData//template[@id = $id][@effectiveDate = $effectiveDate]
        else (
            let $set := $setlib:colDecorData//template[@id = $id]
            return
                $set[@effectiveDate = max($set/xs:dateTime(@effectiveDate))]
        )
    let $decor                  := $object/ancestor::decor
    let $projectPrefix          := $decor/project/@prefix
    
    let $check                  :=
        if (count($object) = 1) then () else
        if ($object) then
            error($errors:SERVER_ERROR, 'Template id ''' || $id || ''' effectiveDate ''' || $effectiveDate || ''' cannot be updated because multiple ' || count($object) || ' were found in: ' || string-join(distinct-values($object/ancestor::decor/project/@prefix), ' / ') || '. Please inform your database administrator.')
        else (
            error($errors:BAD_REQUEST, 'Template id ''' || $id || ''' effectiveDate ''' || $effectiveDate || ''' cannot be updated because it cannot be found.')
        )
    let $check                  :=
        if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-SCENARIOS)) then () else (
            error($errors:FORBIDDEN, concat('User ', $authmap?name, ' does not have sufficient permissions to modify scenarios/transactions in project ', $projectPrefix, '. You have to be an active author in the project.'))
        )
    
    let $templateChain          :=
        if ($recurse) then (
            let $templatechain          := tmapi:getTemplateList((), $projectPrefix, (), (), (), $object/@id, $object/@effectiveDate, $tmapi:TREETYPELIMITEDMARKED, false())
            
            for $t in $templatechain//template
            return 
                $decor//template[@id = $t/@id][@effectiveDate = $t/@effectiveDate]
        ) 
        else (
            $object
        )
    let $testUpdate             :=
        for $object in $templateChain
        return
            utillib:applyStatusProperties($authmap, $object, $object/@statusCode, $newStatusCode, $newVersionLabel, $newExpirationDate, $newOfficialReleaseDate, $recurse, true(), $projectPrefix)
    let $results                :=
        if ($testUpdate[self::error]) then $testUpdate else
        if ($listOnly) then $testUpdate else (
            for $object in $templateChain
            return
                utillib:applyStatusProperties($authmap, $object, $object/@statusCode, $newStatusCode, $newVersionLabel, $newExpirationDate, $newOfficialReleaseDate, $recurse, false(), $projectPrefix)
        )
    return
    if ($results[self::error] and not($listOnly)) then
        error($errors:BAD_REQUEST, string-join(
            for $e in $results[self::error]
            return
                $e/@itemname || ' id ''' || $e/@id || ''' effectiveDate ''' || $e/@effectiveDate || ''' cannot be updated: ' || data($e)
            , ' ')
        )
    else (
        <list artifact="STATUS" current="{count($results)}" total="{count($results)}" all="{count($results)}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            (: max 2 arrays: one with error and one with success :)
            utillib:addJsonArrayToElements($results)
        }
        </list>
    )
};

(:~ Returns a list of zero or more templates
   
   @param $projectPrefix    - optional. determines search scope. null is full server, pfx- limits scope to this project only
   @param $projectVersion   - optional. if empty defaults to current version. if valued then the template will come explicitly from that archived project version which is expected to be a compiled version
   @param $id               - optional. Identifier of the template to retrieve
   @param $name             - optional. Name of the template to retrieve (valueSet/@name)
   @param $flexibility      - optional. null gets all versions, 'dynamic' gets the newest version based on id or name, yyyy-mm-ddThh:mm:ss gets this specific version
   @param $treetype         - optional. Default $tmapi:TREETYPELIMITEDMARKED
   @param $resolve          - optional. Boolean. Default true. If true and $doV2 is false, returns valueSetList.valueSet* where every valueSet has @id|@ref and a descending by 
   effectiveDate list of matching valueSets. If false and $doV2 is false, returns valueSetList.valueSet* containing only the latest version of the valueSet (or ref if no versions 
   exist in $projectPrefix)<br/>If true and $doV2 is true, returns valueSetList.valueSet* where every valueSet has @id|@ref and a descending by effectiveDate list of matching 
   valueSets. If false and $doV2 is false, returns valueSetList.valueSet* containing only the latest version of the valueSet (or ref if no versions exist in $projectPrefix)
   @return Zero value sets in case no matches are found, one if only one exists or if a specific version was requested, or more if more versions exist and no specific version was requested
   @since 2013-06-14
:)
declare function tmapi:getTemplateList($governanceGroupId as xs:string?, $projectPrefix as xs:string, $projectVersion as xs:string?, $projectLanguage as xs:string?, $searchTerms as xs:string*, $objectid as xs:string?, $objected as xs:string?, $treetype as xs:string*, $resolve as xs:boolean) {

    let $governanceGroupId      := $governanceGroupId[not(. = '')]
    let $projectPrefix          := $projectPrefix[not(. = '')]
    let $projectVersion         := $projectVersion[not(. = '')]
    let $projectLanguage        := $projectLanguage[not(. = '')]
    
    let $objectid               := $objectid[not(. = '')]
    let $objected               := $objected[not(. = '')]
    
    (: $tmapi:TREETYPELIMITED       If treetype is 'limited' and param id is valued then a list is built for just this id.:)
    (: $tmapi:TREETYPEMARKED        If treetype is 'marked' and param id is valued then a full list is built where every template 
                                    hanging off this template is marked as such.:)
    (: $tmapi:TREETYPELIMITEDMARKED If treetype is 'marked' and param id is valued then a limited list is built containing only 
                                    templates hanging off this template.:)

    let $treetype               :=
        if (empty($objectid)) then ($tmapi:TREETYPELIMITED) else (
            ($treetype[. = ($tmapi:TREETYPELIMITED, $tmapi:TREETYPEMARKED, $tmapi:TREETYPELIMITEDMARKED)], $tmapi:TREETYPELIMITED)[1]
        )

    let $decor                  := 
        if ($governanceGroupId) then
            for $projectId in ggapi:getLinkedProjects($governanceGroupId)/@ref
            return
                utillib:getDecorById($projectId)
        else
        if (utillib:isOid($projectPrefix)) then
            utillib:getDecorById($projectPrefix, $projectVersion, $projectLanguage)
        else (
            utillib:getDecorByPrefix($projectPrefix, $projectVersion, $projectLanguage)
        )
    let $decors                 := utillib:getBuildingBlockRepositories($decor, (), $utillib:strDecorServicesURL)
    
    let $results                := 
        if (empty($searchTerms)) then (
            $decor//template
        )
        else (
            let $luceneQuery                := utillib:getSimpleLuceneQuery($searchTerms, 'wildcard')
            let $luceneOptions              := utillib:getSimpleLuceneOptions()

            return 
                ($decors//template[@id = $searchTerms] |
                $decors//template[ends-with(@id, $searchTerms[1])] |
                $decors//template[@id][ft:query(@name | @displayName, $luceneQuery, $luceneOptions)])
        )
    
    let $objectsByRef           :=
        if ($resolve and empty($projectVersion)) then (
            for $tm in $results[@ref]
            let $tmref := $tm/@ref
            group by $tmref
            return
                $tm[1] | $decors//template[@id = $tmref]
        )
        else (
            for $tm in $results[@ref]
            let $tmref := $tm/@ref
            group by $tmref
            return
                $tm[1]
        )
        
    let $results                := $results[@id] | $objectsByRef
    
    let $starttemplate          :=
        if (empty($objectid)) then 
            ()
        else
        if ($objected castable as xs:dateTime) then
            $results[@id = $objectid][@effectiveDate = $objected]
        else (
            $results[@id = $objectid][@effectiveDate = string(max($results[@effectiveDate]/xs:dateTime(@effectiveDate)))]
        )
    
    let $templateChain          := 
        if ($starttemplate) then 
            $starttemplate[1] | tmapi:getTemplateChain($starttemplate, $decors, map:merge(map:entry(concat($starttemplate/@id, $starttemplate/@effectiveDate), ''))) 
        else ()
    let $templateChainArr   := $templateChain/concat(@id,@effectiveDate)
    
    let $templateSet        :=
        if ($treetype=($tmapi:TREETYPELIMITED, $tmapi:TREETYPEMARKED)) then (
            $results
        )
        else (
            $results[@ref = $templateChain/@id] | $results[concat(@id, @effectiveDate) = $templateChainArr]
        )
    (:let $decorRepresentingTemplates := $decorRules/ancestor::decor[project/@prefix=$prefix]/scenarios//representingTemplate[@ref]:)
    
    let $result             :=
        for $projectTemplatesById in $templateSet
        let $idref                  := $projectTemplatesById/@id | $projectTemplatesById/@ref
        group by $idref
        return (
            let $representingTemplates          := $decor//representingTemplate[@ref = $idref]
            let $newestTemplateEffectiveDate    := string(max($projectTemplatesById/xs:dateTime(@effectiveDate)))
            let $newestTemplate                 := $projectTemplatesById[@effectiveDate = $newestTemplateEffectiveDate]
            let $templateSet                    :=
                for $template in $projectTemplatesById
                let $currentTemplateEffectiveDate   := $template/@effectiveDate
                let $lookingForNewest               := $template[@effectiveDate = $newestTemplateEffectiveDate]
                group by $currentTemplateEffectiveDate
                order by $template[1]/@effectiveDate descending
                return (
                    <version uuid="{util:uuid()}">
                    {
                        $template[1]/(@* except (@displayName|@ident|@url)),
                        attribute displayName {
                            if ($newestTemplate and $template[@ref]) then (
                                ($newestTemplate/@displayName, $newestTemplate/@name)[1]
                            )
                            else (
                                ($template/@displayName, $template/@name)[1]
                            )
                        },
                        attribute url {($template[1]/ancestor-or-self::*/@url, $template[1]/ancestor::cacheme/@bbrurl, $utillib:strDecorServicesURL)[1]},
                        attribute ident {($template[1]/ancestor::decor/project/@prefix)[1]}
                        ,
                        if (empty($searchTerms)) then () else $template[1]/desc,
                        <classification>
                        {
                            ($template[1]/classification/@format)[1]
                            ,
                            if ($template[1]/classification/@type) then
                                for $type in $template[1]/classification/@type
                                return
                                    <type>{$type}</type>
                            else (
                                <type>notype</type>
                            ),
                            for $tag in $template[1]/classification/tag
                            group by $tag
                            return
                                $tag
                            ,
                            for $property in $template[1]/classification/property
                            group by $property
                            return
                                $property
                        }
                        </classification>
                    }
                    {
                        for $rtemp in $representingTemplates[@flexibility = $currentTemplateEffectiveDate]
                        return
                            <representingTemplate>{$rtemp/@ref, $rtemp/@flexibility}</representingTemplate>
                        ,
                        if ($lookingForNewest) then
                            for $rtemp in $representingTemplates[not(@flexibility castable as xs:dateTime)]
                            return
                                <representingTemplate>{$rtemp/@ref}</representingTemplate>
                        else ()
                    }
                    {
                        if ($starttemplate) then
                            if ($treetype = $tmapi:TREETYPELIMITEDMARKED) then (
                                <ref type="template">{$starttemplate[1]/(@id | @name | @displayName | @effectiveDate)}</ref>
                            ) else
                            if ($template[@effectiveDate = $templateChain[@id=$template/@id]/@effectiveDate]) then
                                <ref type="template">{$starttemplate[1]/(@id | @name | @displayName | @effectiveDate)}</ref>
                            else ()
                        else ()
                    }
                    </version>
                )
            
            return
            <template uuid="{util:uuid()}">
            {
                ($projectTemplatesById/@ref, $projectTemplatesById/@id)[1],
                ($templateSet[@id], $templateSet)[1]/(@* except (@uuid | @id | @ref | @class)),
                attribute class {($templateSet[@id][1]/classification/type/@type, 'notype')[1]}
                
            }
            {
                $templateSet
            }
            </template>
        )
    
    let $startDisplayName   := if ($starttemplate/@displayName) then $starttemplate/@displayName else $starttemplate/@name
    let $schemaTypes        := utillib:getDecorTypes()//TemplateTypes/enumeration
    let $classified         := true()
    return    
        <return>
        {
            if (empty($objectid)) then () else attribute id {$objectid},
            if (empty($objected)) then () else attribute effectiveDate {$objected},
            if (empty($objectid)) then () else attribute treetype {$treetype},
            attribute starttemplate {exists($starttemplate)},
            attribute prefix {$decor/project/@prefix}
        }
        {
            if ($classified) then (
                let $schemaTypes        := utillib:getDecorTypes()//TemplateTypes/enumeration
                
                (: this is the code for a classification based hierarchical tree view :)
                (:get templates with and without classification:)
                for $templateSets in $result
                let $class  := $templateSets/@class
                group by $class
                order by count($schemaTypes[@value = $class]/preceding-sibling::enumeration)
                return
                <category uuid="{util:uuid()}" type="{$class}">
                {
                    for $label in $schemaTypes[@value = $class]/label
                    return
                        <name language="{$label/@language}">{$label/text()}</name>
                }
                {
                    for $templateSet in $templateSets
                    order by lower-case(($templateSet/@displayName, $templateSet/@name)[1])
                    return
                        $templateSet
                }
                </category>
            )
            else (
                for $r in $result
                order by count($r//representingTemplate)=0, lower-case(($r/template[1]/@displayName, $r/template[1]/@name)[1])
                return $r
            )
        }
            <!--<x>{$results}</x>-->
        </return>
};

(:~ Returns a list of zero or more templateAssociation objects
   
   @param $projectPrefix    - optional. determines search scope. null is full server, pfx- limits scope to this project only
   @param $projectVersion   - optional. if empty defaults to current version. if valued then the template will come explicitly from that archived project version which is expected to be a compiled version
   @param $id               - optional. Identifier of the template to retrieve
   @param $effectiveDate    - optional. null gets all versions, yyyy-mm-ddThh:mm:ss gets this specific version
   @param $treetype         - optional. Default $tmapi:TREETYPELIMITEDMARKED
   @param $resolve          - optional. Boolean. Default true. If true and $doV2 is false, returns valueSetList.valueSet* where every valueSet has @id|@ref and a descending by 
   effectiveDate list of matching valueSets. If false and $doV2 is false, returns valueSetList.valueSet* containing only the latest version of the valueSet (or ref if no versions 
   exist in $projectPrefix)<br/>If true and $doV2 is true, returns valueSetList.valueSet* where every valueSet has @id|@ref and a descending by effectiveDate list of matching 
   valueSets. If false and $doV2 is false, returns valueSetList.valueSet* containing only the latest version of the valueSet (or ref if no versions exist in $projectPrefix)
   @return Zero value sets in case no matches are found, one if only one exists or if a specific version was requested, or more if more versions exist and no specific version was requested
   @since 2013-06-14
:)
declare %private function tmapi:getTemplateAssociationList($projectPrefix as xs:string, $projectVersion as xs:string?, $projectLanguage as xs:string?, $objectid as xs:string?, $objected as xs:string?) as element(list) {
    let $objectid               := $objectid[not(. = '')]
    let $objected               := $objected[not(. = '')]
    let $projectPrefix          := $projectPrefix[not(. = '')]
    let $projectVersion         := $projectVersion[not(. = '')]
    
    let $decor                  :=
        if (utillib:isOid($projectPrefix)) then 
            utillib:getDecorById($projectPrefix, $projectVersion, $projectLanguage) 
        else (
            utillib:getDecorByPrefix($projectPrefix, $projectVersion, $projectLanguage)
        )
    
    let $check                  :=
        if (count($decor) = 1) then () else
        if (count($decor) = 0) then
            if (empty($projectVersion)) then
                error($errors:BAD_REQUEST, 'Project with prefix ' || $projectPrefix || ' not found')
            else (
                error($errors:BAD_REQUEST, 'Project with prefix ' || $projectPrefix || ' release ' || $projectVersion || ' language ''' || $projectLanguage || ''' not found')
            )
        else (
            if (empty($projectVersion)) then
                error($errors:SERVER_ERROR, 'Found ' || count($decor) || ' instances of project with prefix ' || $projectPrefix || '. Expected 0..1. Alert your administrator as this should not be possible.')
            else (
                error($errors:SERVER_ERROR, 'Found ' || count($decor) || ' instances of project with prefix ' || $projectPrefix || ' release ' || $projectVersion || ' language ''' || $projectLanguage || '''. Expected 0..1. Alert your administrator as this should not be possible.')
            )
        )
    
    let $results                :=
        if (empty($objectid)) then
            $decor//templateAssociation
        else
        if (empty($objected)) then
            $decor//templateAssociation[@templateId = $objectid]
        else (
            $decor//templateAssociation[@templateId = $objectid][@effectiveDate = $objected]
        )
    
    return
        <list artifact="ASSOCIATION" current="{count($results)}" total="{count($results)}" all="{count($decor//templateAssociation)}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements($results)
        }
        </list>
};

(:~ Creates a list object with templates

@param $projectPrefix           - required. limits scope to this project only
@param $projectVersion          - optional parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss
@param $projectLanguage         - optional parameter to select from a specific compiled language
@param $searchTerms             - optional array of string. Searches for datasets by name or ending with id
@return all live repository/non-private templates as JSON, all data sets for the given $projectPrefix or nothing if not found
@since 2020-05-03
:)
declare function tmapi:searchTemplate($decorOrPrefix as item(), $decorRelease as xs:string?, $decorLanguage as xs:string?, $searchTerms as xs:string*) {

    let $decor          := tmapi:getDecorByPrefix($decorOrPrefix, $decorRelease, $decorLanguage)
    let $decors         := if (empty($decorRelease)) then utillib:getBuildingBlockRepositories($decor, (), $utillib:strDecorServicesURL) else $decor
    
    let $luceneQuery    := utillib:getSimpleLuceneQuery($searchTerms)
    let $luceneOptions  := utillib:getSimpleLuceneOptions()
            
    let $results        := 
        if (empty($searchTerms)) then
            $decors//template
        else
        if (count($searchTerms)=1 and matches($searchTerms[1],'^\d+(\.\d+)*$')) then
            $decors//template[ends-with(@id, $searchTerms[1])] | $decor//template[ends-with(@ref, $searchTerms[1])]
        else (
            $decors//template[ft:query(@name | @displayName, $luceneQuery, $luceneOptions)]
        )
    return
        $results[@id]
};

(:~ Copied from art/api/api-decor-template.xqm. Retrieves the ART-DECOR 3 compatible list of usages for as template
* Transaction associations
* Template associations inbound and outbound
:)
declare function tmapi:getDependenciesAndUsage($projectPrefix as xs:string?, $projectVersion as xs:string?, $language as xs:string?, $id as xs:string, $effectiveDate as xs:string?) {

let $templates      := 
    if (empty($projectPrefix)) then 
        tmapi:getTemplateById($id, ())[@id]
    else (
        tmapi:getTemplateById($id, (), $projectPrefix, $projectVersion, $language)[@id]
    )
let $latest         := string(max($templates/xs:dateTime(@effectiveDate)))
let $template       := 
    if ($effectiveDate castable as xs:dateTime) then   
        $templates[@effectiveDate = $effectiveDate][1]
    else (
        $templates[@effectiveDate = $latest][1]
    )

let $projectPrefix  := if (empty($projectPrefix)) then $template/ancestor::decor/project/@prefix else $projectPrefix
let $decor          := $template/ancestor::decor
let $isLatestTm     := $latest = $template/@effectiveDate

let $result                 :=
    if ($template) then (
        (: get dependencies in THIS project :)
        (: get dependencies in other projects than this one :)
        tmapi:getDependendies($template, $template/@id, $latest, 1, $setlib:colDecorData/decor | $setlib:colDecorCache/decor),
        (: get uses :)
        tmapi:templateUses($template, $template/@id, 1, $decor)
    ) else ()

(: uniquify result re/ type of ref element, type, id and effectiveDate, sort project ref's first :)
let $templateReferences     :=
    for $d in $result
    let $id := concat(name($d), $d/@type, $d/@id, $d/@effectiveDate)
    group by $id
    order by (not($d[1]/@prefix=$projectPrefix))
    return
        $d[1]

(: calculate transaction references for template itself and and any templates that refer to it :)
let $t_templates            := $template | $templateReferences[self::ref]
let $transactionReferences  := ($setlib:colDecorData//representingTemplate[@ref = $t_templates/@id] | 
                                $setlib:colDecorCache//representingTemplate[@ref = $t_templates/@id])

(: calculate what the latest version is for templates that are being called dynamically. no use for static bindings. :)
let $tmmap                  := 
    map:merge(
        for $ref in $transactionReferences
        let $tmid           := $ref/@ref
        group by $tmid
        return
            if ($ref/@flexibility[. castable as xs:dateTime]) then () else (
                map:entry($tmid, tmapi:getTemplateById($tmid, 'dynamic')[@id]/@effectiveDate)
            )
    )

(: transaction bindings count for static matches and dynamic matches :)
let $transactionReferences  :=
    for $ref in $transactionReferences
    let $match  := 
        if ($ref[@flexibility castable as xs:dateTime]) then
            $t_templates[@id = $ref/@ref][@effectiveDate = $ref/@flexibility]
        else (
            $t_templates[@effectiveDate = map:get($tmmap, @id)]
        )
    return
        if (empty($match)) then () else (
            utillib:doTransactionAssociation($match[1], $ref/ancestor::transaction[1])
        )

return (
    $transactionReferences,
    $templateReferences
)
};

declare function tmapi:getDependendies($version as element(), $self as xs:string?, $latest as xs:string?, $level as xs:int, $decor as element()*) as element()* {
    
    let $ref            := $version/@id/string()
    let $flexibility    := $version/@effectiveDate/string()
    let $lvtextinclude  := if ($level=1) then 'include' else 'dependency'
    let $lvtextcontains := if ($level=1) then 'contains' else 'dependency'

    let $dep :=
        if ($level > 5) then
            (: too deeply nested, raise error and give up :)
            ()
        else if (($ref = $self) and ($level > 1)) then
            (: ref to myself in a level greater than 1, give up, you are done :)
            ()
        else (
            (: all include statements anywhere with @ref and @flexibility :)
            (: get all referenced templates pointed to by @ref and @flexibility :)
            for $chain in $decor//include[@ref=$ref][@flexibility=$flexibility]/ancestor::template
            let $bbrurl     := $chain/ancestor::cacheme/@bbrurl
            let $bbrident   := ($chain/@ident, $chain/ancestor::cacheme/@bbrident, $chain/ancestor::decor/project/@prefix)[1] 
            return (
                <ref type="{$lvtextinclude}" prefix="{$bbrident}" flexibility="{$flexibility}">
                {
                    $chain/(@* except (@type|@prefix|@flexibility)),
                    if ($chain/@ident) then ()  else if ($bbrident) then attribute ident {$bbrident} else (),
                    if ($chain/@url) then () else if ($bbrurl) then attribute url {$bbrurl} else ()
                }
                </ref>,
                tmapi:getDependendies($chain, $self, $latest, $level+1, $decor)
            )
            ,
            (: all elements anywhere with @contains and @flexibility :)
            (: get all referenced templates pointed to by @contains and @flexibility :)
            for $chain in $decor//element[@contains=$ref][@flexibility=$flexibility]/ancestor::template
            let $bbrurl     := $chain/ancestor::cacheme/@bbrurl
            let $bbrident   := ($chain/ancestor::cacheme/@bbrident, $chain/ancestor::decor/project/@prefix)[1] 
            return (
                <ref type="{$lvtextcontains}" prefix="{$bbrident}" flexibility="{$flexibility}">
                {
                    $chain/(@* except (@type|@prefix|@flexibility)),
                    if ($chain/@ident) then ()  else if ($bbrident) then attribute ident {$bbrident} else (),
                    if ($chain/@url) then () else if ($bbrurl) then attribute url {$bbrurl} else ()
                }
                </ref>,
                tmapi:getDependendies($chain, $self, $latest, $level+1, $decor)
            )
            ,
            (: all 
                   include statements anywhere with @ref but no @flexibility 
                   or elements with @contains but no @flexibility 
                   BUT ONLY in case $flexibility = $latest, i.e. dynamic binding 
            :)
            if ($flexibility = $latest) then (
                (: get all referenced templates pointed to by @ref :)
                let $ttis   := 
                    for $i in $decor//include[@ref = $ref]
                    let $rf := $i/@ref
                    let $fl := if ($i[@flexibility]) then $i/@flexibility else 'dynamic'
                    group by $rf, $fl
                    return if ($fl = 'dynamic') then $i else ()
                let $ttis   := $ttis/ancestor::template
                
                for $chain in $ttis
                let $bbrurl     := $chain/ancestor::cacheme/@bbrurl
                let $bbrident   := ($chain/ancestor::cacheme/@bbrident, $chain/ancestor::decor/project/@prefix)[1] 
                return (
                    <ref type="{$lvtextinclude}" prefix="{$bbrident}" flexibility="dynamic">
                    {
                        $chain/(@* except (@type|@prefix|@flexibility)),
                        if ($chain/@ident) then ()  else if ($bbrident) then attribute ident {$bbrident} else (),
                        if ($chain/@url) then () else if ($bbrurl) then attribute url {$bbrurl} else ()
                    }
                    </ref>,
                    tmapi:getDependendies($chain, $self, $latest, $level+1, $decor)
                ),
                
                (: get all referenced templates pointed to by @contains :)
                let $ttis   := 
                    for $i in $decor//element[@contains = $ref]
                    let $rf := $i/@contains
                    let $fl := if ($i[@flexibility]) then $i/@flexibility else 'dynamic'
                    group by $rf, $fl
                    return if ($fl = 'dynamic') then $i else ()
                let $ttis   := $ttis/ancestor::template
                
                for $chain in $ttis
                let $bbrurl     := $chain/ancestor::cacheme/@bbrurl
                let $bbrident   := ($chain/ancestor::cacheme/@bbrident, $chain/ancestor::decor/project/@prefix)[1] 
                return (
                    <ref type="{$lvtextcontains}" prefix="{$bbrident}" flexibility="dynamic">
                    {
                        $chain/(@* except (@type|@prefix|@flexibility)),
                        if ($chain/@ident) then ()  else if ($bbrident) then attribute ident {$bbrident} else (),
                        if ($chain/@url) then () else if ($bbrurl) then attribute url {$bbrurl} else ()
                    }
                    </ref>,
                    tmapi:getDependendies($chain, $self, $latest, $level+1, $decor)
                )
            )
            else ()
        )
            
     return $dep
     
};

declare function tmapi:templateUses($version as element(), $self as xs:string?, $level as xs:int, $decor as element(decor)) as element(uses)* {
    if ($level > 7) then
        (: too deeply nested, raise error and give up :)
        ()
    else if (($version/@id = $self) and ($level > 1)) then
        (: ref to myself in a level greater than 1, give up, you are done :)
        ()
    else (
        for $lc in ($version//element[@contains] | $version//include[@ref])
        let $xid            := if ($lc/name() = 'element') then $lc/@contains else $lc/@ref
        let $flex           := ($lc/@flexibility, 'dynamic')[1]
        let $xtype          := if ($lc/name() = 'element') then 'contains' else 'include'
        let $templ          := tmapi:getTemplateByRef($xid, $flex, $decor, $decor/@versionDate, $decor/@language)[@id]
        
        return
            if ($templ) then
                for $x in $templ
                
                return
                    <uses type="{$xtype}" prefix="{$decor/project/@prefix}" flexibility="{$flex}">
                        {
                         $x/(@* except (@type|@prefix|@flexibility|@*[contains(name(), 'dummy-')])),
                         if($x/@url) then () else attribute url {($x[1]/ancestor::cacheme/@bbrurl, $utillib:strDecorServicesURL)[1]},
                         if($x/@ident) then () else attribute ident {($x/ancestor::decor/project/@prefix)[1]}
                        }
                    </uses>
            else (
                    <uses type="{$xtype}" prefix="{$decor/project/@prefix}" id="{$xid}" name="" displayName="" effectiveDate="{$flex}"/>
            )
    )
};

(:~ Rewrite an incoming template back into db format

@param $decor - decor project to update results into
@param $inputTemplate - incoming raw template
@param $storedTemplate - template in db to update (if any)
@param $mode - 'new' (completely new), 'edit' (edit of existing, same id/effectiveDate), 'version' (clone under same id, new effectiveDate), 'adapt' (clone under new id and effectiveDate)
:)
declare %private function tmapi:prepareIncomingTemplateForUpdate($decor as element(decor), $inputTemplate as element(template), $storedTemplate as element(template)?, $mode as xs:string, $newTemplateElementId as xs:string?) as element(template) {

    let $originalId             := ($inputTemplate/@originalId, $inputTemplate/@id)[1]
    let $templateBaseIds        := decorlib:getBaseIdsP($decor, $decorlib:OBJECTTYPE-TEMPLATE)
    
    (:if a baseId is present use it, else use default template baseId:)
    let $templateRoot           :=
        if ($inputTemplate/@baseId) then
            if ($templateBaseIds[@id = $inputTemplate/@baseId]) then $inputTemplate/@baseId else (
                error($errors:BAD_REQUEST, 'Submitted data SHALL specify a known baseId in project ' || $decor/project/@prefix || '. BaseId ''' || $inputTemplate/@baseId || ''' not found')
            )
        else (
            decorlib:getDefaultBaseIdsP($decor, $decorlib:OBJECTTYPE-TEMPLATE)[1]/@id
        )
    
    (:generate new id if mode is 'new' or 'adapt', else use existing id:)
    let $newTemplateId              := 
        if ($mode=('new','adapt')) then
            decorlib:getNextAvailableIdP($decor, $decorlib:OBJECTTYPE-TEMPLATE, $templateRoot)[1]/@id
        else (
            $inputTemplate/@id
        )
    
    let $templateName               := attribute name {($inputTemplate/@name, utillib:getNameForOID($newTemplateId, (), $decor))[not(.='')][1]}
    let $templateDisplayName        := attribute displayName {($inputTemplate/@displayName, $templateName)[not(.='')][1]}

    (:keep effectiveDate for mode is 'edit', else generate new:)
    let $templateEffectiveTime  := 
        if ($mode = 'edit' and $storedTemplate[@effectiveDate castable as xs:dateTime]) then
            $storedTemplate/@effectiveDate
        else
        if ($inputTemplate[@effectiveDate castable as xs:dateTime][not(@effectiveDate = $storedTemplate/@effectiveDate)]) then 
            $inputTemplate/@effectiveDate
        else (
            attribute effectiveDate {format-dateTime(current-dateTime(), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')}
        )
    (:set draft effectiveDate for mode is 'edit', else generate new:)
    let $templateStatusCode     := 
        if ($inputTemplate/@statusCode[. = ('','new')]) then
            attribute statusCode {'draft'}
        else (
            $inputTemplate/@statusCode
        )
    
    return (
        <template>
        {
            $newTemplateId,
            $templateName,
            $templateDisplayName,
            $templateEffectiveTime,
            $templateStatusCode,
            $inputTemplate/@versionLabel[not(. = '')],
            $inputTemplate/@expirationDate[. castable as xs:dateTime],
            $inputTemplate/@officialReleaseDate[. castable as xs:dateTime],
            $inputTemplate/@canonicalUri[not(. = '')],
            $inputTemplate/@isClosed[. = ('true', 'false')],
            attribute lastModifiedDate {substring(string(current-dateTime()), 1, 19)}
            ,
            utillib:prepareFreeFormMarkupWithLanguageForUpdate($inputTemplate/desc)
            ,
            tmapi:prepareClassificationForUpdate($inputTemplate/classification)
            ,
            tmapi:prepareRelationshipForUpdate($inputTemplate/relationship)
            ,
            tmapi:prepareContextForUpdate($inputTemplate/context[1])
            ,
            tmapi:prepareItemForUpdate($inputTemplate/(item | items[@is = 'item'])[1])
            ,
            utillib:preparePublishingAuthorityForUpdate($inputTemplate/publishingAuthority[empty(@inherited)][@name[not(. = '')]][1])
            (:,
            utillib:preparePublishingAuthorityForUpdate($inputTemplate/endorsingAuthority[empty(@inherited)][@name[not(. = '')]][1])
            :)
            ,
            for $node in $inputTemplate/purpose
            return
                utillib:prepareFreeFormMarkupWithLanguageForUpdate($node)
            ,
            utillib:prepareFreeFormMarkupWithLanguageForUpdate($inputTemplate/copyright[empty(@inherited)])
            (:,
            utillib:prepareObjectHistoryForUpdate($inputTemplate/revisionHistory):)
            ,
            utillib:prepareExampleForUpdate($inputTemplate/example)
            ,
            (: not yet necessary for patch as we don't patch these ... :)
            for $attribute in $inputTemplate/(attribute|items[@is = 'attribute'])
            return
                tmapi:processAttribute($attribute, $newTemplateId, $newTemplateElementId, true())
            ,
            for $item in $inputTemplate/(element|include|choice|let|assert|report|defineVariable|constraint|items[@is = ('element','include','choice','let','assert','report','defineVariable','constraint')])
            return
                tmapi:processElement($item, $newTemplateId, $newTemplateElementId, true())
        }
        </template>
    )
};

(:~ Collect all relevant templateAssociations into one element :)
declare function tmapi:prepareTemplateAssociationForUpdate($storedTemplateAssociations as element(templateAssociation)*, $preparedTemplate as element(template), $inputTemplate as element(template)?, $newTemplateElementId as xs:string, $mode as xs:string) as element(templateAssociation) {
    <templateAssociation templateId="{$preparedTemplate/@id}" effectiveDate="{$preparedTemplate/@effectiveDate}">
    {
        (:  keep all concepts associated with element/@id | attribute/@id in the prepared template :)
        for $association in ($storedTemplateAssociations/concept[@elementId = ($preparedTemplate//element/@id | $preparedTemplate//attribute/@id)] |
                             $inputTemplate/staticAssociations/*[@elementId = ($preparedTemplate//element/@id | $preparedTemplate//attribute/@id)] | 
                             $storedTemplateAssociations/concept[@elementPath] | $inputTemplate/staticAssociations/*[@elementPath])
        let $deid   := $association/@ref
        let $deed   := $association/@effectiveDate
        let $elid   := $association/@elementId
        let $elpt   := $association/@elementPath
        group by $deid, $deed, $elid, $elpt
        return
            <concept>{$deid, $deed, $elid, $elpt}</concept>
        ,
        (:if mode is 'new' and conceptId is present, create new templateAssociation for concept:)
        if ($mode = 'new' and $inputTemplate[@conceptId]//@concept) then
            <concept ref="{$inputTemplate/@conceptId}" effectiveDate="{$inputTemplate/@conceptEffectiveDate}" elementId="{$newTemplateElementId}"/>
        else ()
    }
    </templateAssociation>
};

declare function tmapi:prepareTemplateForUpdate($editedTemplate as element(template)) as element(template) {
    <template>
    {
        $editedTemplate/@id,
        $editedTemplate/@name,
        $editedTemplate/@displayName,
        $editedTemplate/@effectiveDate,
        $editedTemplate/@statusCode,
        $editedTemplate/@isClosed[not(.='')],
        $editedTemplate/@expirationDate[not(.='')],
        $editedTemplate/@officialReleaseDate[not(.='')],
        $editedTemplate/@versionLabel[not(.='')],
        $editedTemplate/@canonicalUri[not(.='')],
        attribute lastModifiedDate {substring(string(current-dateTime()), 1, 19)}
        ,
        utillib:prepareFreeFormMarkupWithLanguageForUpdate($editedTemplate/desc)
        ,
        tmapi:prepareClassificationForUpdate($editedTemplate/classification)
        ,
        tmapi:prepareRelationshipForUpdate($editedTemplate/relationship)
        ,
        utillib:preparePublishingAuthorityForUpdate($editedTemplate/publishingAuthority[empty(@inherited)][@name[not(. = '')]][1])
        (:,
        utillib:preparePublishingAuthorityForUpdate($editedTemplate/endorsingAuthority[empty(@inherited)][@name[not(. = '')]][1])
        :)
        ,
        for $node in $editedTemplate/purpose
        return
            utillib:prepareFreeFormMarkupWithLanguageForUpdate($node)
        ,
        for $node in $editedTemplate/copyright
        return
            utillib:prepareFreeFormMarkupWithLanguageForUpdate($node)
        (:,
        utillib:prepareObjectHistoryForUpdate($editedTemplate/revisionHistory):)
        ,
        tmapi:prepareContextForUpdate($editedTemplate/context[1])
        ,
        tmapi:prepareItemForUpdate($editedTemplate/item[1])
        ,
        utillib:prepareExampleForUpdate($editedTemplate/example)
        ,
        (: not yet necessary for patch as we don't patch these ... :)
        for $attribute in $editedTemplate/(attribute|items[@is = 'attribute'])
        return
            (:$attribute :)tmapi:processAttribute($attribute, '', '', false())
        ,
        for $item in $editedTemplate/(element|include|choice|let|assert|report|defineVariable|constraint|items[@is = ('element','include','choice','let','assert','report','defineVariable','constraint')])
        return
            (:$item :)tmapi:processElement($item, '', '', false())
    }
    </template>
};

(: Input
      <classification format="hl7v3xml1">
        <type>clinicalstatementlevel</type>
        <type>cdaentrylevel</type>
        <tag>tag1</tag>
        <tag>tag2</tag>
        <property>property1</property>
        <property>property2</property>
      </classification>
   Output
      <classification type="clinicalstatementlevel" format="hl7v3xml1">
        <tag>tag1</tag>
        <tag>tag2</tag>
        <property>property1</property>
        <property>property2</property>
      </classification>
      <classification type="cdaentrylevel">
        <tag>tag1</tag>
        <tag>tag2</tag>
        <property>property1</property>
        <property>property2</property>
      </classification>
:)
declare function tmapi:prepareClassificationForUpdate($nodes as element(classification)*) as element(classification)* {
    let $classbytype  :=
        for $type at $i in distinct-values(($nodes/@type, $nodes/type))
        return
            <classification type="{$type}"/>
    
    return
        if ($classbytype) then (
            <classification>
            {
                $classbytype[1]/@type,
                ($nodes/@format)[1],
                for $tag in $nodes/tag
                group by $tag
                return
                    <tag>{utillib:parseNode($tag[1])/(node() except item)}</tag>
                ,
                for $property in $nodes/property
                group by $property
                return
                    <property>{utillib:parseNode($property[1])/(node() except item)}</property>
            }
            </classification>
            ,
            $classbytype[position() gt 1]
        )
        else 
        if ($nodes[@format | tag | property]) then (
            <classification>
            {
                $nodes[1]/@format,
                for $tag in $nodes/tag
                group by $tag
                return
                    <tag>{utillib:parseNode($tag[1])/(node() except item)}</tag>
                ,
                for $property in $nodes/property
                group by $property
                return
                    <property>{utillib:parseNode($property[1])/(node() except item)}</property>
            }
            </classification>
        )
        else (
        )
};

declare function tmapi:prepareRelationshipForUpdate($nodes as element(relationship)*) as element(relationship)* {
    for $node in $nodes
    return
        element {name($node)} {
            $node/@type,
            ($node/@template, $node/@model)[1],
            $node/@flexibility[. = 'dynamic'] | $node/@flexibility[. castable as xs:dateTime]
        }
};

declare function tmapi:prepareContextForUpdate($nodes as element(context)*) as element(context)* {
    for $node in $nodes
    let $att  :=
        switch ($node/@selected)
        case 'id'     return $node/@id[not(. = '')]
        case 'path'   return $node/@path[not(. = '')]
        default       return ($node/@id, $node/@path)[not(. = '')][1] 
    return
        if ($att) then <context>{$att}</context> else ()
};

declare function tmapi:prepareItemForUpdate($nodes as element()*) as element(item)* {
    for $node in $nodes[self::items[@is = 'item'] | self::item][string-length(@label) gt 0][not(@original='false')]
    return
        <item>
        {
            $node/@label,
            utillib:prepareFreeFormMarkupWithLanguageForUpdate($node/desc)
        }
        </item>
};

declare %private function tmapi:processElement($element as element(), $templateId as xs:string, $elementId as xs:string, $selectedOnly as xs:boolean) as element()? {
    if (not($selectedOnly) or $element/@selected) then (
        let $elmName    := if (name($element) = 'items') then $element/@is else name($element)
        return
        switch ($elmName)
        case 'element' return
            <element>
            {
                (: attributes :)
                $element/@name[not(.='')],
                if ($element[@contains[not(.='')]]) then (
                    $element/@contains,
                    $element/@flexibility[. castable as xs:dateTime or . = 'dynamic']
                ) else ()
                ,
                if ($element/@datatype[not(.='')]) then (
                    $element/@datatype,
                    $element/@strength[not(.='')]
                ) else ()
                ,
                $element/@minimumMultiplicity[. = '0'] | $element/@minimumMultiplicity[. castable as xs:positiveInteger],
                $element/@maximumMultiplicity[. = '0'] | $element/@maximumMultiplicity[. castable as xs:positiveInteger] | $element/@maximumMultiplicity[. = '*'],
                $element/@conformance[. = ('R', 'C', 'NP')],
                $element/@isMandatory[. = 'true'],
                $element/@isClosed[.= ('true', 'false')],
                if ($element/@concept and $elementId[not(. = '')]) then
                   attribute id {$elementId}
                else(
                    $element/@id[not(.='')]
                )
                ,
                (: this part is in the schema, but not implemented (yet) :)
                $element/@include[not(.='')],
                $element/@useWhere[not(.='')],
                $element/@displayName[not(.='')],
                $element/@statusCode[not(.='')],
                $element/@versionLabel[not(.='')],
                $element/@effectiveDate[. castable as xs:dateTime],
                $element/@expirationDate[. castable as xs:dateTime],
                $element/@officialReleaseDate[. castable as xs:dateTime]
            }
            {
                (: elements :)
                utillib:prepareFreeFormMarkupWithLanguageForUpdate($element/desc)
                ,
                for $item in $element/(item | items[@is = 'item'])[@label[not(. = '')]][not(@original='false')]
                return
                    <item>
                    {
                        $item/@label,
                        utillib:prepareFreeFormMarkupWithLanguageForUpdate($item/desc)
                    }
                    </item>
                ,
                utillib:prepareExampleForUpdate($element/(example | items[@is = 'example']))
                ,
                tmapi:prepareVocabulary($element/vocabulary)
                ,
                tmapi:prepareElementProperty($element/(property | items[@is = 'property']))
                ,
                $element/text
            }
            {
                for $attribute in $element/(attribute|items[@is = 'attribute'])
                return 
                    tmapi:processAttribute($attribute, $templateId, $elementId, $selectedOnly)
                ,
                for $item in $element/(element|include|choice|let|assert|report|defineVariable|constraint|items[@is = ('element','include','choice','let','assert','report','defineVariable','constraint')])
                return
                    tmapi:processElement($item, $templateId, $elementId, $selectedOnly)
            }
            </element>
        
        case 'choice' return
            <choice>
            {
                (: attributes :)
                $element/@minimumMultiplicity[. = '0'] | $element/@minimumMultiplicity[. castable as xs:positiveInteger],
                $element/@maximumMultiplicity[. = '0'] | $element/@maximumMultiplicity[. castable as xs:positiveInteger] | $element/@maximumMultiplicity[. = '*']
            }
            {
                (: elements :)
                utillib:prepareFreeFormMarkupWithLanguageForUpdate($element/desc)
                ,
                for $item in $element/(item | items[@is = 'item'])[@label[not(. = '')]][not(@original='false')]
                return
                    <item>
                    {
                        $item/@label,
                        utillib:prepareFreeFormMarkupWithLanguageForUpdate($item/desc)
                    }
                    </item>
                ,
                for $item in $element/(element|include|constraint|items[@is = ('element','include','constraint')])
                return
                    tmapi:processElement($item, $templateId, $elementId, $selectedOnly)
            }
            </choice>
        
        case 'include' return
            <include>
            {
                (: attributes :)
                if ($element[@ref[not(. = '')]]) then (
                    $element/@ref,
                    $element/@flexibility[. castable as xs:dateTime] | $element/@flexibility[. = 'dynamic']
                ) else ()
                ,
                $element/@minimumMultiplicity[. = '0'] | $element/@minimumMultiplicity[. castable as xs:positiveInteger],
                $element/@maximumMultiplicity[. = '0'] | $element/@maximumMultiplicity[. castable as xs:positiveInteger] | $element/@maximumMultiplicity[. = '*'],
                $element/@conformance[. = ('R', 'C', 'NP')],
                $element/@isMandatory[. = 'true'],
                (: this part is in the schema, but not implemented (yet) :)
                $element/@scenario[not(.='')],
                $element/@effectiveDate[. castable as xs:dateTime]
           }
           {
                (: elements :)
                for $node in $element/desc
                return
                    utillib:prepareFreeFormMarkupWithLanguageForUpdate($node)
                ,
                for $item in $element/(item | items[@is = 'item'])[@label[not(. = '')]][not(@original='false')]
                return
                    <item>
                    {
                        $item/@label,
                        utillib:prepareFreeFormMarkupWithLanguageForUpdate($item/desc)
                    }
                    </item>
                ,
                utillib:prepareExampleForUpdate($element/(example | items[@is = 'example']))
                ,
                utillib:prepareFreeFormMarkupWithLanguageForUpdate($element/constraint)
            }
            </include>
        
        case 'let' return
            <let>
            {
                $element/@name,
                $element/@value,
                $element/node()
            }
            </let>
        
        case 'assert' return
            <assert>
            {
                $element/@role[not(.='')],
                $element/@test[not(.='')],
                $element/@flag[not(.='')],
                $element/@see[not(.='')],
                let $parsed := utillib:parseNode($element)
                
                return
                    if ($parsed[count(*) = 1][*:p | *:div][normalize-space(string-join(text(), '')) = '']) then 
                        $parsed/*/node() 
                    else (
                        $parsed/node()
                    )
            }
            </assert>
        
        case 'report' return
            <report>
            {
                $element/@role[not(.='')],
                $element/@test[not(.='')],
                $element/@flag[not(.='')],
                $element/@see[not(.='')],
                let $parsed := utillib:parseNode($element)
                
                return
                    if ($parsed[count(*) = 1][*:p | *:div][normalize-space(string-join(text(), '')) = '']) then 
                        $parsed/*/node() 
                    else (
                        $parsed/node()
                    )
            }
            </report>
        
        case 'defineVariable' return
            <defineVariable>
            {
                $element/@name[not(.='')],
                $element/@path[not(.='')],
                $element/node()
            }
            </defineVariable>
            
        case 'constraint' return
            <constraint>
            {
                $element/@language[not(.='')],
                $element/@lastTranslated[not(.='')],
                $element/@mimeType[not(.='')],
                utillib:parseNode($element)/node()
            }
            </constraint>
        
        default return ()
    
    )
    else ()
};

declare %private function tmapi:processAttribute($attribute as element(), $templateId as xs:string, $elementId as xs:string, $selectedOnly as xs:boolean) as element()? {
    if (not($selectedOnly) or $attribute/@selected) then (
        <attribute>
        {
            $attribute/(@name|@classCode|@contextConductionInd|@contextControlCode|
                        @determinerCode|@extension|@independentInd|@institutionSpecified|
                        @inversionInd|@mediaType|@moodCode|@negationInd|
                        @nullFlavor|@operator|@qualifier|@representation|
                        @root|@typeCode|@unit|@use)[not(.='')]
            ,
            if ($attribute[@name = 'root'][matches(@value,'[a-zA-Z]')]/parent::element/@name[matches(., '^([^:]+:)?templateId')]) then
                if ($templateId[not(. = '')]) then 
                    attribute value {$templateId}
                else ()
            else (
                $attribute/@value[not(. = '')]
            )
            ,
            if ($attribute/@datatype[not(.='')]) then (
                $attribute/@datatype,
                $attribute/@strength[not(.='')]
            ) else ()
            ,
            if ($attribute/@prohibited = 'true' or $attribute/@conf = 'prohibited' or $attribute/@conformance = 'NP') then
                attribute prohibited {'true'}
            else 
            if ($attribute/@isOptional = 'true' or $attribute/@conf = 'isOptional' or $attribute/@conformance = 'O') then
                attribute isOptional {'true'}
            else ()
            ,
            if ($attribute/@concept and $elementId[not(. = '')]) then
                attribute id {$elementId}
            else(
                $attribute/@id[not(.='')]
            )
            ,
            utillib:prepareFreeFormMarkupWithLanguageForUpdate($attribute/desc)
            ,
            for $item in $attribute/(item | items[@is = 'item'])[@label[not(. = '')]][not(@original='false')]
            return
                <item>
                {
                    $item/@label,
                    utillib:prepareFreeFormMarkupWithLanguageForUpdate($item/desc)
                }
                </item>
            ,
            tmapi:prepareVocabulary($attribute/vocabulary)
        }
        </attribute>
    )
    else ()
};

(:~ Process property element(s). Incoming property element may be named 'property' or items[@is='property']. If it contains any of the supported attributes with a value, 
    a property element is created with those attributes. Empty attributes and any other attributes than the supported attributes are ignored (like @is and @order). Supported 
    attributes with illegal contents raise an error :)
declare %private function tmapi:prepareElementProperty($properties as element()*) as element(property)* {
    for $property in $properties[self::property | self::items[@is = 'property']][(@minInclude | @maxInclude | @minLength | @maxLength | @value | @fractionDigits | @unit | @currency)[not(. = '')]]
    return
        <property>
        {
            for $att in ($property/@minInclude, $property/@maxInclude)[not(. = '')]
            return
                if ($att castable as xs:decimal) then $att else (
                    error($errors:BAD_REQUEST, 'Property ' || name($att) || ' SHALL be a valid integer or decimal (with decimal point, not comma). Found ''' || $att || '''')
                )
            ,
            for $att in ($property/@minLength, $property/@maxLength)[not(. = '')]
            return
                if ($att castable as xs:positiveInteger) then $att else (
                    error($errors:BAD_REQUEST, 'Property ' || name($att) || ' SHALL be a valid positive integer. Found ''' || $att || '''')
                )
            ,
            $property/@value[not(. = '')]
            ,
            for $att in $property/@fractionDigits[not(. = '')]
            return
                if (matches($att, '^\d{1,4}[!\.]?$')) then $att else (
                    error($errors:BAD_REQUEST, 'Property ' || name($att) || ' SHALL match pattern of 1-4 digits followed by ! or . (period). Found ''' || $att || '''')
                )
            ,
            $property/@unit[not(. = '')],
            $property/@currency[not( . = '')]
        }
        </property>
};

declare %private function tmapi:prepareVocabulary($vocabularies as element(vocabulary)*) as element()* {
    for $vocabulary in $vocabularies[(@valueSet | @code | @codeSystem | @codeSystemName | @codeSystemVersion | @displayName | @domain)[not(. = '')]]
    return
        <vocabulary>
        {
            if ($vocabulary/@valueSet[not(. = '')]) then (
                if ($vocabulary/@valueSet[utillib:isOid(.)]) then $vocabulary/@valueSet else (
                    error($errors:BAD_REQUEST, 'Vocabulary attribute valueSet SHALL be a valid OID. Found ''' || $vocabulary/@valueSet || '''')
                )
                ,
                if ($vocabulary/@flexibility[not(. = '')]) then 
                    if ($vocabulary/@flexibility[. castable as xs:dateTime] | $vocabulary/@flexibility[. = 'dynamic']) then $vocabulary/@flexibility else (
                        error($errors:BAD_REQUEST, 'Vocabulary attribute flexibility SHALL be a valid dateTime or ''dynamic''. Found ''' || $vocabulary/@flexibility || '''')
                    )
                else ()
            ) 
            else (
                if ($vocabulary/@code[not(. = '')]) then
                    if ($vocabulary/@code[matches(., '\s')]) then 
                        error($errors:BAD_REQUEST, 'Vocabulary attribute code SHALL NOT contain whitespace. Found ''' || $vocabulary/@code || '''')
                    else (
                        $vocabulary/@code
                    )
                else ()
                ,
                if ($vocabulary/@codeSystem[not(. = '')]) then
                    if ($vocabulary/@codeSystem[utillib:isOid(.)]) then $vocabulary/@codeSystem else (
                        error($errors:BAD_REQUEST, 'Vocabulary attribute codeSystem SHALL be a valid OID. Found ''' || $vocabulary/@codeSystem || '''')
                    )
                else ()
                ,
                $vocabulary/@codeSystemName[not(. = '')],
                $vocabulary/@codeSystemVersion[not(. = '')],
                $vocabulary/@displayName[not(. = '')]
            )
            ,
            $vocabulary/@domain[not(. = '')]
        }
        </vocabulary>
};

declare %private function tmapi:getTemplateHistoryTEST($projectPrefix as xs:string*, $id as xs:string*, $effectiveDate as xs:string*) {
let $result := ()
    (: shall retrun a list of history items of the artefact identified by id and effectiveDate, returns these items
    <history
        type="TM"
        id="{$h/@artefactId}"
        effectiveDate="{$h/@artefactEffectiveDate}"
        statusCode="{$h/@artefactStatusCode}"
        performer="{data($h/@author)}"
        intention="{$h/@intention}"
        when="{format-dateTime($h/@date,'[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]','en',(),())}"/>
    :)
return
    $result
};

(:~ Return zero or more templates as-is

@param $ref          - required. Identifier or name of the template to retrieve
@param $flexibility  - optional. null gets all versions, yyyy-mm-ddThh:mm:ss gets this specific version, anything that doesn't cast to xs:dateTime gets latest version
@return Matching templates
@since 2023-09-10
:)
declare function tmapi:getTemplateByRef($ref as xs:string, $flexibility as xs:string?) as element(template)* {
    if (utillib:isOid($ref)) then
        tmapi:getTemplateById($ref, $flexibility)
    else (
        tmapi:getTemplateByName($ref, $flexibility)
    )
};

(:~ Return zero or more templates as-is

@param $ref           - required. Identifier or nameof the template to retrieve
@param $flexibility   - optional. null gets all versions, yyyy-mm-ddThh:mm:ss gets this specific version, anything that doesn't cast to xs:dateTime gets latest version
@param $decorOrPrefix - required. determines search scope. pfx- limits scope to this project only
@param $decorRelease  - optional. if empty defaults to current version. if valued then the template will come explicitly from that archived project version which is expected to be a compiled version
@param $decorLanguage - optional. Language compilation. Defaults to project/@defaultLanguage.
@return Matching templates
@since 2023-09-10
:)
declare function tmapi:getTemplateByRef($ref as xs:string, $flexibility as xs:string?, $decorOrPrefix as item(), $decorRelease as xs:string?, $decorLanguage as xs:string?) as element(template)* {
    if (utillib:isOid($ref)) then
        tmapi:getTemplateById($ref, $flexibility, $decorOrPrefix, $decorRelease, $decorLanguage)
    else (
        tmapi:getTemplateByName($ref, $flexibility, $decorOrPrefix, $decorRelease, $decorLanguage)
    )
};

(:~ Return zero or more templates as-is

@param $id           - required. Identifier of the template to retrieve
@param $flexibility  - optional. null gets all versions, yyyy-mm-ddThh:mm:ss gets this specific version, anything that doesn't cast to xs:dateTime gets latest version
@return Matching templates
@since 2023-09-10
:)
declare function tmapi:getTemplateById($id as xs:string, $flexibility as xs:string?) as element(template)* {
    if ($flexibility castable as xs:dateTime) then
        $setlib:colDecorData//template[@id = $id][@effectiveDate = $flexibility] | $setlib:colDecorCache//template[@id = $id][@effectiveDate = $flexibility]
    else
    if ($flexibility) then (
        let $templates := $setlib:colDecorData//template[@id = $id] | $setlib:colDecorCache//template[@id = $id]
        return
            $templates[@effectiveDate = string(max($templates/xs:dateTime(@effectiveDate)))]
    )
    else (
        $setlib:colDecorData//template[@id = $id] | $setlib:colDecorCache//template[@id = $id]
    )
};

(:~ Return zero or more templates as-is

@param $id            - required. Identifier of the template to retrieve
@param $flexibility   - optional. null gets all versions, yyyy-mm-ddThh:mm:ss gets this specific version, anything that doesn't cast to xs:dateTime gets latest version
@param $decorOrPrefix - required. determines search scope. pfx- limits scope to this project only
@param $decorRelease  - optional. if empty defaults to current version. if valued then the template will come explicitly from that archived project version which is expected to be a compiled version
@param $decorLanguage - optional. Language compilation. Defaults to project/@defaultLanguage.
@return Matching templates
@since 2023-09-10
:)
declare function tmapi:getTemplateById($id as xs:string, $flexibility as xs:string?, $decorOrPrefix as item(), $decorRelease as xs:string?, $decorLanguage as xs:string?) as element(template)* {
let $decor          := tmapi:getDecorByPrefix($decorOrPrefix, $decorRelease, $decorLanguage)
let $decors         := if (empty($decorRelease)) then utillib:getBuildingBlockRepositories($decor, (), $utillib:strDecorServicesURL) else $decor

return
    if ($flexibility castable as xs:dateTime) then
        $decors//template[@id = $id][@effectiveDate = $flexibility]
    else
    if ($flexibility) then (
        let $templates := $decors//template[@id = $id]
        return
            $templates[@effectiveDate = string(max($templates/xs:dateTime(@effectiveDate)))]
    )
    else (
        $decors//template[@id = $id]
    )
};

(:~ Return zero or more templates as-is

@param $name         - required. Identifier of the template to retrieve
@param $flexibility  - optional. null gets all versions, yyyy-mm-ddThh:mm:ss gets this specific version, anything that doesn't cast to xs:dateTime gets latest version
@return Matching templates
@since 2023-09-10
:)
declare function tmapi:getTemplateByName($name as xs:string, $flexibility as xs:string?) as element(template)* {
    if ($flexibility castable as xs:dateTime) then
        $setlib:colDecorData//template[@name = $name][@effectiveDate = $flexibility] | $setlib:colDecorCache//template[@name = $name][@effectiveDate = $flexibility]
    else
    if ($flexibility) then (
        let $templates := $setlib:colDecorData//template[@name = $name] | $setlib:colDecorCache//template[@name = $name]
        return
            $templates[@effectiveDate = string(max($templates/xs:dateTime(@effectiveDate)))]
    )
    else (
        $setlib:colDecorData//template[@name = $name] | $setlib:colDecorCache//template[@name = $name]
    )
};

(:~ Return zero or more templates as-is

@param $name          - required. Identifier of the template to retrieve
@param $flexibility   - optional. null gets all versions, yyyy-mm-ddThh:mm:ss gets this specific version, anything that doesn't cast to xs:dateTime gets latest version
@param $decorOrPrefix - required. determines search scope. pfx- limits scope to this project only
@param $decorRelease  - optional. if empty defaults to current version. if valued then the template will come explicitly from that archived project version which is expected to be a compiled version
@param $decorLanguage - optional. Language compilation. Defaults to project/@defaultLanguage.
@return Matching templates
@since 2023-09-10
:)
declare function tmapi:getTemplateByName($name as xs:string, $flexibility as xs:string?, $decorOrPrefix as item(), $decorRelease as xs:string?, $decorLanguage as xs:string?) as element(template)* {
let $decor          := tmapi:getDecorByPrefix($decorOrPrefix, $decorRelease, $decorLanguage)
let $decors         := if (empty($decorRelease)) then utillib:getBuildingBlockRepositories($decor, (), $utillib:strDecorServicesURL) else $decor

return
    if ($flexibility castable as xs:dateTime) then
        $decors//template[@name = $name][@effectiveDate = $flexibility]
    else
    if ($flexibility) then (
        let $templates := $decors//template[@name = $name]
        return
            $templates[@effectiveDate = string(max($templates/xs:dateTime(@effectiveDate)))]
    )
    else (
        $decors//template[@name = $name]
    )
};

(:~ Return zero or more expanded templates

@param $ref          - required. Identifier or name of the template to retrieve
@param $flexibility  - optional. null gets all versions, yyyy-mm-ddThh:mm:ss gets this specific version, anything that doesn't cast to xs:dateTime gets latest version
@return Matching templates
@since 2023-09-10
:)
declare function tmapi:getExpandedTemplateByRef($ref as xs:string, $flexibility as xs:string?, $serialize as xs:boolean) as element(template)* {
    if (utillib:isOid($ref)) then
        tmapi:getExpandedTemplateById($ref, $flexibility, $serialize)
    else (
        tmapi:getExpandedTemplateByName($ref, $flexibility, $serialize)
    )
};

(:~ Return zero or more expanded templates

@param $ref           - required. Identifier or nameof the template to retrieve
@param $flexibility   - optional. null gets all versions, yyyy-mm-ddThh:mm:ss gets this specific version, anything that doesn't cast to xs:dateTime gets latest version
@param $decorOrPrefix - required. determines search scope. pfx- limits scope to this project only
@param $decorRelease  - optional. if empty defaults to current version. if valued then the template will come explicitly from that archived project version which is expected to be a compiled version
@param $decorLanguage - optional. Language compilation. Defaults to project/@defaultLanguage.
@return Matching templates
@since 2023-09-10
:)
declare function tmapi:getExpandedTemplateByRef($ref as xs:string, $flexibility as xs:string?, $decorOrPrefix as item(), $decorRelease as xs:string?, $decorLanguage as xs:string?, $serialize as xs:boolean) as element(template)* {
    if (utillib:isOid($ref)) then
        tmapi:getExpandedTemplateById($ref, $flexibility, $decorOrPrefix, $decorRelease, $decorLanguage, $serialize)
    else (
        tmapi:getExpandedTemplateByName($ref, $flexibility, $decorOrPrefix, $decorRelease, $decorLanguage, $serialize)
    )
};

(:~ Return zero or more expanded templates

@param $id            - required. Identifier of the template to retrieve
@param $flexibility   - optional. null gets all versions, yyyy-mm-ddThh:mm:ss gets this specific version, anything that doesn't cast to xs:dateTime gets latest version
@param $serialize     - normally false. Determines serialization of xhtml nodes
@return Matching templates
@since 2023-09-10

:)
declare function tmapi:getExpandedTemplateById ($id as xs:string, $flexibility as xs:string?, $serialize as xs:boolean) as element(template)* {
    for $template in tmapi:getTemplateById($id, $flexibility)
    let $prefix     := $template/ancestor::decor/project/@prefix
    let $url        := ($template/ancestor::cacheme/@bbrurl, $utillib:strDecorServicesURL)[1]
    group by $url, $prefix
    return
        tmapi:getExpandedTemplate($template, $template[1]/ancestor::decor, (), (), $serialize)
};

(:~ Return zero or more expanded templates

Inside the path /return/template[@id or @ref][@ident] is 1..* template element. This may is the expanded template (@id or @ref) with additions:
- when the template originates from a different project than parent::template/@ident, the origin is added using template/@url and template/@ident.
- the item element is added at every relevant level where the template itself doesn't explicitly define it
- the include element will have the extra attributes tmid, tmname, tmdisplayName containing the id/name/displayName of the referred template (unless it cannot be found)
- the include element will have an extra attribute linkedartefactmissing='true' when the referred template cannot be found
- the include element will contain the template it refers (unless it cannot be found)
- the element[@contains] element will have the extra attributes tmid, tmname, tmdisplayName containing the id/name/displayName of the contained template (unless it cannot be found)
- the element[@contains] element will have an extra attribute linkedartefactmissing='true' when the contained template cannot be found
- the vocabulary[@valueSet] element will have the extra attributes vsid, vsname, vsdisplayName containing the id/name/displayName of the referred value set (unless it cannot be found)
- the vocabulary[@valueSet] element will have an extra attribute linkedartefactmissing='true' when the referred value set cannot be found
- at the bottom of the template there may be extra elements:
  - If this is a template that is used in a transaction: &lt;representingTemplate ref="..." flexibility="..." model="..." sourceDataset="..." type="stationary" schematron="vacc-vacccda2"/>
  - For every include[@ref] / element[@contains]        : &lt;ref type="contains|include" id="..." name="..." displayName="..." effectiveDate="...."/>
  - For every project template that calls this template : &lt;ref type="dependency" id="..." name="..." displayName="..." effectiveDate="...."/>
    &lt;staticAssociations>
        
        - For every template association:
        &lt;origconcept datasetId="..." datasetEffectiveDate="..." ref="..." effectiveDate="..." elementId="..." path="...">
            &lt;concept id=".." effectiveDate="...">
                &lt;name language="nl-NL">...</name>
                &lt;desc language="nl-NL">...</desc>
            &lt;/concept>
        &lt;/origconcept>
        
    &lt;/staticAssociations>

@param $id            - required. Identifier of the template to retrieve
@param $flexibility   - optional. null gets all versions, yyyy-mm-ddThh:mm:ss gets this specific version, anything that doesn't cast to xs:dateTime gets latest version
@param $decorOrPrefix - required. determines search scope. pfx- limits scope to this project only
@param $decorRelease  - optional. if empty defaults to current version. if valued then the template will come explicitly from that archived project version which is expected to be a compiled version
@param $decorLanguage - optional. Language compilation. Defaults to project/@defaultLanguage
@param $serialize     - normally false. Determines serialization of xhtml nodes
@return Matching templates
@since 2014-06-20
:)
declare function tmapi:getExpandedTemplateById ($id as xs:string, $flexibility as xs:string?, $decorOrPrefix as item(), $decorRelease as xs:string?, $decorLanguage as xs:string?, $serialize as xs:boolean) as element(template)* {
    for $template in tmapi:getTemplateById($id, $flexibility, $decorOrPrefix, $decorRelease, $decorLanguage)
    let $prefix     := $template/ancestor::decor/project/@prefix
    let $url        := ($template/ancestor::cacheme/@bbrurl, $utillib:strDecorServicesURL)[1]
    group by $url, $prefix
    return
        tmapi:getExpandedTemplate($template, $prefix, (), (), $serialize)
};

(:~ Return zero or more expanded templates

@param $id            - required. Identifier of the template to retrieve
@param $flexibility   - optional. null gets all versions, yyyy-mm-ddThh:mm:ss gets this specific version, anything that doesn't cast to xs:dateTime gets latest version
@param $serialize     - normally false. Determines serialization of xhtml nodes
@return Matching templates
@since 2023-09-10

:)
declare function tmapi:getExpandedTemplateByName($name as xs:string, $flexibility as xs:string?, $serialize as xs:boolean) as element(template)* {
    for $template in tmapi:getTemplateByName($id, $flexibility)
    let $prefix     := $template/ancestor::decor/project/@prefix
    let $url        := ($template/ancestor::cacheme/@bbrurl, $utillib:strDecorServicesURL)[1]
    group by $url, $prefix
    return
        tmapi:getExpandedTemplate($template, $template[1]/ancestor::decor, (), (), $serialize)
};

(:~ Return zero or more expanded templates

Inside the path /return/template[@id or @ref][@ident] is 1..* template element. This may is the expanded template (@id or @ref) with additions:
- when the template originates from a different project than parent::template/@ident, the origin is added using template/@url and template/@ident.
- the item element is added at every relevant level where the template itself doesn't explicitly define it
- the include element will have the extra attributes tmid, tmname, tmdisplayName containing the id/name/displayName of the referred template (unless it cannot be found)
- the include element will have an extra attribute linkedartefactmissing='true' when the referred template cannot be found
- the include element will contain the template it refers (unless it cannot be found)
- the element[@contains] element will have the extra attributes tmid, tmname, tmdisplayName containing the id/name/displayName of the contained template (unless it cannot be found)
- the element[@contains] element will have an extra attribute linkedartefactmissing='true' when the contained template cannot be found
- the vocabulary[@valueSet] element will have the extra attributes vsid, vsname, vsdisplayName containing the id/name/displayName of the referred value set (unless it cannot be found)
- the vocabulary[@valueSet] element will have an extra attribute linkedartefactmissing='true' when the referred value set cannot be found
- at the bottom of the template there may be extra elements:
  - If this is a template that is used in a transaction: &lt;representingTemplate ref="..." flexibility="..." model="..." sourceDataset="..." type="stationary" schematron="vacc-vacccda2"/>
  - For every include[@ref] / element[@contains]        : &lt;ref type="contains|include" id="..." name="..." displayName="..." effectiveDate="...."/>
  - For every project template that calls this template : &lt;ref type="dependency" id="..." name="..." displayName="..." effectiveDate="...."/>
    &lt;staticAssociations>
        
        - For every template association:
        &lt;origconcept datasetId="..." datasetEffectiveDate="..." ref="..." effectiveDate="..." elementId="..." path="...">
            &lt;concept id=".." effectiveDate="...">
                &lt;name language="nl-NL">...</name>
                &lt;desc language="nl-NL">...</desc>
            &lt;/concept>
        &lt;/origconcept>
        
    &lt;/staticAssociations>

@param $name          - required. Name of the template to retrieve
@param $flexibility   - optional. null gets all versions, yyyy-mm-ddThh:mm:ss gets this specific version, anything that doesn't cast to xs:dateTime gets latest version
@param $decorOrPrefix - required. determines search scope. pfx- limits scope to this project only
@param $decorRelease  - optional. if empty defaults to current version. if valued then the template will come explicitly from that archived project version which is expected to be a compiled version
@param $decorLanguage - optional. Language compilation. Defaults to project/@defaultLanguage
@param $serialize     - normally false. Determines serialization of xhtml nodes
@return Matching templates
@since 2014-06-20
:)
declare function tmapi:getExpandedTemplateByName($name as xs:string, $flexibility as xs:string?, $decorOrPrefix as item(), $decorRelease as xs:string?, $decorLanguage as xs:string?, $serialize as xs:boolean) as element(template)* {
    for $template in tmapi:getTemplateByName($name, $flexibility, $decorOrPrefix, $decorRelease, $decorLanguage)
    let $prefix     := $template/ancestor::decor/project/@prefix
    let $url        := ($template/ancestor::cacheme/@bbrurl, $utillib:strDecorServicesURL)[1]
    group by $url, $prefix
    return
        tmapi:getExpandedTemplate($template, $prefix, (), (), $serialize)
};

(:~ Expands a template with @id by recursively resolving all includes. Use tmapi:getExpandedTemplateById to resolve a template/@ref first. 
                            
@param $template      - required. The template element with content to expand
@param $decorOrPrefix - required. The origin of template. pfx- limits scope this project only
@param $decorRelease  - optional. if empty defaults to current version. if valued then the template will come explicitly from that archived project version which is expected to be a compiled version
@param $decorLanguage - optional. Language compilation. Defaults to project/@defaultLanguage
@param $serialize     - normally false. Determines serialization of xhtml nodes
@return The expanded template
@since 2013-06-14
:)
declare function tmapi:getExpandedTemplate($template as element(), $decorOrPrefix as item(), $decorRelease as xs:string?, $decorLanguage as xs:string?, $serialize as xs:boolean) as element() {
(: all rules and terminologies of this project for later use :)
let $decor                          := tmapi:getDecorByPrefix($decorOrPrefix, $decorRelease, $decorLanguage)[1]
let $decor                          := if ($decor) then $decor else ($setlib:colDecorCache//decor[project/@prefix = $decorOrPrefix])[1]
let $prefix                         := $decor/project/@prefix
let $language                       := ($decor/@language, $decor/project/@defaultLanguage)[1]

(: all transactions where this template is ref'ed :)
let $decorRepresentingTemplates     := $setlib:colDecorData//representingTemplate[@ref = $template/@id]
let $newestTemplateEffectiveDate    := tmapi:getTemplateById($template/(@id | @ref), 'dynamic')/@effectiveDate
let $currentTemplateIsNewest        := exists(
                                           $template[@effectiveDate = $newestTemplateEffectiveDate] |
                                           $template[@ref][empty(@flexibility)] |
                                           $template[@ref][@flexibility = 'dynamic'] |
                                           $template[@ref][@flexibility = $newestTemplateEffectiveDate]
                                       )

return
    <template>
    {
        $template/@id,
        $template/@name,
        $template/@displayName,
        $template/@effectiveDate,
        $template/@statusCode,
        $template/@versionLabel,
        $template/@expirationDate,
        $template/@officialReleaseDate,
        $template/@canonicalUri,
        attribute isClosed {$template/@isClosed = 'true'},
        $template/@lastModifiedDate,
        if ($template/@url) then $template/@url else if ($template/ancestor::*/@bbrurl) then attribute url {$template/ancestor::*/@bbrur} else (),
        attribute ident {($template/@ident, $template/ancestor::decor/project/@prefix)[1]},
        for $ns at $i in utillib:getDecorNamespaces($template/ancestor::decor) 
        return 
            attribute {QName($ns/@uri,concat($ns/@prefix,':dummy-', if ($ns/@default = 'true') then 'default' else $i))} {$ns/@uri}
    }
    {
        if ($serialize) then 
            utillib:serializeNodes($template/desc, $decor/project/name/@language)
        else ($template/desc)
    }
    {
        let $theitem := if ($template/item) then $template/item[1] else <item original="false" label="{$template/@name}"/>
        
        for $node in $template/(* except desc)
        return
            tmapi:copyNodes($node, $theitem, 1, $decor, $serialize)
            (:tm:copyNodes($node, $theitem, 1, $decor[1]/rules, $decor[1]/terminology, $language, $serialize):)
    }
    {
        (: get where the template is a representing template :)
        for $rtemp in $decorRepresentingTemplates
        let $rtempFlexibility   := 
            if ($rtemp[@flexibility castable as xs:dateTime]) then (
                (:starts with 4 digits, explicit dateTime:)
                string($rtemp/@flexibility)
            ) else (
                (:empty or dynamic:)
                $newestTemplateEffectiveDate
            )
        let $rtempFlexValue     :=
            if (string-length($rtemp/@flexibility)>0) then (
                $rtemp/@flexibility
            ) else (
                'dynamic'
            )
        let $rtempTransaction   := $rtemp/parent::transaction
        let $ident              := $rtemp/ancestor::decor/project/@prefix
        return
        if ($rtempFlexibility = $template/@effectiveDate) then
            <representingTemplate ref="{$rtemp/@ref}" flexibility="{$rtempFlexValue}" schematron="{$prefix}{$rtempTransaction/@label}" transactionId="{$rtempTransaction/@id}" transactionEffectiveDate="{$rtempTransaction/@effectiveDate}">
            {
                $rtemp/@sourceDataset,
                $rtemp/@sourceDatasetFlexibility,
                $rtempTransaction/@type,
                $rtempTransaction/@model,
                $rtempTransaction/@statusCode,
                $rtempTransaction/@versionLabel,
                attribute ident {$ident},
                $rtempTransaction/name
            }
            </representingTemplate>
        else()
    }
    {
        tmapi:createStaticAssociationElement($decor/rules/templateAssociation[@templateId = $template/@id][@effectiveDate = $template/@effectiveDate], $language)
    }
    </template>
};

declare %private function tmapi:getTemplateExtract($projectPrefix as xs:string*, $projectVersion as xs:string*, $projectLanguage as xs:string?, $id as xs:string, $effectiveDate as xs:string?) as element(template)* {
    let $id                     := $id[not(. = '')]
    let $effectiveDate          := ($effectiveDate[not(. = '')], 'dynamic')[1]
    let $projectPrefix          := $projectPrefix[not(. = '')][1]
    let $projectVersion         := $projectVersion[not(. = '')][1]
    let $projectLanguage        := $projectLanguage[not(. = '')][1]

    (: retrieve all templates for the input $decor project(s) 
        - when compiled version  - as is - 
        - when live version - getting all (cached) decor projects in scope  
    :)
    
    let $allTemplates              := tmapi:getTemplateById($id, $effectiveDate, $projectPrefix, $projectVersion, $projectLanguage) | utillib:getDecor($projectPrefix, $projectVersion, $projectLanguage)//template[@ref = $id]
           
    return
    
    <return>
    {
        
        for $templates in $allTemplates
        let $id := $templates/@id | $templates/@ref
        group by $id
        return (
            <template id="{$id}" ident="{$projectPrefix}">
            {
                for $template in $templates
                let $tmed   := $template/@effectiveDate
                group by $tmed
                order by xs:dateTime($template[1]/@effectiveDate) descending
                return 
                    <template>
                    {
                        $template[1]/(@* except @*[contains(name(),'dummy-')]),
                        if ($template/@url) then $template/@url else if ($template/ancestor::*/@bbrurl) then attribute url {$template/ancestor::*/@bbrur} else (),
                        attribute ident {($template/@ident, $template/ancestor::decor/project/@prefix)[1]},
                        if ($template[1]/../@*[contains(name(),'dummy-')]) then
                            $template[1]/../@*[contains(name(),'dummy-')]
                        else
                        if ($template[1]/ancestor::decor) then (
                            (: <ns uri="urn:hl7-org:v3" prefix="hl7" default="{$ns-default='hl7'}" readonly="true"/> :)
                            for $ns at $i in utillib:getDecorNamespaces($templates[1]/ancestor::decor)
                            return 
                                attribute {QName($ns/@uri,concat($ns/@prefix,':dummy-', if ($ns/@default = 'true') then 'default' else $i))} {$ns/@uri}
                        )
                        else (
                            for $ns-prefix at $i in in-scope-prefixes($template[1])[not(.=('xml'))]
                            let $ns-uri := namespace-uri-for-prefix($ns-prefix, $template[1])
                            order by $ns-prefix
                            return
                                attribute {QName($ns-uri,concat($ns-prefix,':dummy-',$i))} {$ns-uri}
                        )
                        ,
                        $template[1]/node()
                    }
                    </template>
            }
            </template>
        )
    }
    </return>

};    

declare %private function tmapi:getExpandedTemplateExtract($projectPrefix as xs:string*, $projectVersion as xs:string*, $projectLanguage as xs:string?, $id as xs:string, $effectiveDate as xs:string?) as element(template)* {
    let $id                     := $id[not(. = '')]
    let $effectiveDate          := ($effectiveDate[not(. = '')], 'dynamic')[1]
    let $projectPrefix          := $projectPrefix[not(. = '')][1]
    let $projectVersion         := $projectVersion[not(. = '')][1]
    let $projectLanguage        := $projectLanguage[not(. = '')][1]

    (: retrieve all templates for the input $decor project(s) 
        - when compiled version  - as is - 
        - when live version - getting all (cached) decor projects in scope  
    :)

    let $allTemplates           := tmapi:getTemplateById($id, $effectiveDate, $projectPrefix, $projectVersion, $projectLanguage) | utillib:getDecor($projectPrefix, $projectVersion, $projectLanguage)//template[@ref = $id]
    
    let $expndTemplates        :=
        for $templates in $allTemplates
            return
            if ($templates[@ref]) then $templates
            else tmapi:getExpandedTemplate($templates, $projectPrefix, $projectVersion, $projectLanguage, false())

    return 
    <result>
    {
        
       for $templates in $expndTemplates
       let $id := $templates/@id | $templates/@ref
       group by $id 
       return
       <template id="{$id}" ident="{$projectPrefix}">
       
       {
           for $template in $templates
           return
               <template>
               {
                   $templates/@*,
                   $template/*,
                   for $ur in tmapi:getDependenciesAndUsage($projectPrefix, (), (), $template/@id, $template/@effectiveDate)
                   let $if := $ur/@id || $ur/@effectiveDate
                   group by $if
                   order by replace(replace(concat(($ur/@id)[1], '.'), '\.', '.0000000000'), '.0*([0-9]{9,})', '.$1'), $if
                   for $u in $ur
                   return
                   if (name($u) = 'transactionAssociation') then () else $u
                   
               }
               </template>
       }
       </template>
   }
   </result>
};  

declare function tmapi:createStaticAssociationElement($templateAssociations as element(templateAssociation)*, $language as xs:string?) as element(staticAssociations) {
    <staticAssociations>
    {
        for $association in $templateAssociations/concept
        let $concept            := utillib:getConcept($association/@ref, $association/@effectiveDate)
        let $dataset            := $concept/ancestor::dataset
        let $originalConcept    := utillib:getOriginalForConcept($concept)
        (: 2018-12-19 Disabled for performance reasons :)
        let $refdisplay         := if (1=1) then attribute refdisplay {utillib:getNameForOID($association/@ref, $language, $concept/ancestor::decor)} else ()
        let $path               := 
            for $c in $concept/ancestor::concept
            let $originalConceptName    := utillib:getOriginalForConcept($c)
            return 
                if ($originalConceptName/name[@language=$language]) then $originalConceptName/name[@language=$language] else ($originalConceptName/name[1])
        return
            <origconcept>
            {
                $association/@ref,
                $association/@effectiveDate,
                $refdisplay,
                attribute conceptStatusCode {$concept/@statusCode},
                if ($concept/@expirationDate) then attribute conceptExpirationDate {$concept/@expirationDate} else (),
                if ($concept/@versionLabel) then attribute conceptVersionLabel {$concept/@versionLabel} else (),
                attribute datasetId {$dataset/@id},
                attribute datasetEffectiveDate {$dataset/@effectiveDate},
                attribute datasetStatusCode {$dataset/@statusCode},
                if ($dataset/@expirationDate) then attribute datasetExpirationDate {$dataset/@expirationDate} else (),
                if ($dataset/@versionLabel) then attribute datasetVersionLabel {$dataset/@versionLabel} else (),
                attribute datasetName {$dataset/name[1]},
                attribute ident {$concept/ancestor::decor/project/@prefix},
                $association/@elementId,
                attribute templateId {$association/../@templateId},
                attribute templateEffectiveDate {$association/../@effectiveDate},
                $association/@elementPath,
                attribute path {string-join($path,' / ')}
                ,
                $originalConcept/name,
                $originalConcept/desc,
                for $name in $dataset/name
                return
                    <datasetName>{$name/@*, $name/node()}</datasetName>
                ,
                for $name in $concept/ancestor::decor/project/name
                return
                    <projectName>{$name/@*, $name/node()}</projectName>
            }
            </origconcept>
    }
    </staticAssociations>
};
(:
:   Rewrite all shorthands to the same name/value format to ease processing
:   Remove @isOptional if @probited='true'. Explicitly set @isOptional otherwise. (default value for @isOptional is 'false')
:)
declare function tmapi:normalizeAttributes($attributes as element(attribute)*) as element(attribute)* {
    let $mapping    :=
        <attributes>
            <attribute id="1" name="classCode" datatype="cs"/>
            <attribute id="2" name="contextConductionInd" datatype="bl"/>
            <attribute id="3" name="contextControlCode" datatype="cs"/>
            <attribute id="4" name="determinerCode" datatype="cs"/>
            <attribute id="5" name="extension" datatype="st"/>
            <attribute id="6" name="independentInd" datatype="bl"/>
            <attribute id="7" name="institutionSpecified" datatype="bl"/>
            <attribute id="8" name="inversionInd" datatype="bl"/>
            <attribute id="9" name="mediaType" datatype="st"/>
            <attribute id="10" name="moodCode" datatype="cs"/>
            <attribute id="11" name="negationInd" datatype="bl"/>
            <attribute id="12" name="nullFlavor" datatype="cs"/>
            <attribute id="13" name="operator" datatype="cs"/>
            <attribute id="14" name="qualifier" datatype="set_cs"/>
            <attribute id="15" name="representation" datatype="cs"/>
            <attribute id="16" name="root" datatype="uid"/>
            <attribute id="17" name="typeCode" datatype="cs"/>
            <attribute id="18" name="unit" datatype="cs"/>
            <attribute id="19" name="use" datatype="set_cs"/>
        </attributes>
    
    for $attribute in $attributes
    for $att  at $i in $attribute/(@name|@classCode|@contextConductionInd|@contextControlCode|@determinerCode|
                                         @extension|@independentInd|@institutionSpecified|@inversionInd|
                                         @mediaType|@moodCode|@negationInd|@nullFlavor|
                                         @operator|@qualifier|@representation|@root|
                                         @typeCode|@unit|@use)
    let $anme := if ($att[name()='name']) then $att/string() else ($att/name())
    let $aval := if ($att[name()='name']) then $attribute/@value/string() else ($att/string())
    let $adt  := 
        if ($attribute/@datatype[not(.='')]) 
        then ($attribute/@datatype) 
        else if ($mapping/attribute/@name=$anme) 
        then (attribute datatype {$mapping/attribute[@name=$anme]/@datatype}) 
        else ()
    return
        <attribute name="{$anme}">
        {
            if ($adt=('bl','cs','set_cs') and contains($aval,'|')) then (
                (:boolean can only be true|false so no need to re-specify, cs/set_cs should be done in vocabulary:)
            ) else if (string-length($aval)>0) then (
                attribute value {$aval}
            ) else ()
            ,
            if ($attribute/@prohibited='true') then
                attribute prohibited {'true'}
            else (
                attribute isOptional {$attribute/@isOptional='true'}
            )
            ,
            $adt
            ,
            (:could lead to duplicates...:)
            if ($i=1) then ($attribute/@id) else ()
            ,
            $attribute/@selected
            ,
            $attribute/(desc|item|comment()),
            if ($adt=('cs','set_cs')) then (
                if (contains($aval,'|')) then (
                    for $v in tokenize($aval,'\|')
                    return <vocabulary code="{normalize-space($v)}"/>
                ) else ()
                ,
                $attribute/vocabulary
            ) else (
                $attribute/constraint
            )
        }
        </attribute>
};

declare function tmapi:copyNodes($tnode as element(), $item as element(), $nesting as xs:integer, $decor as element(decor), $serialize as xs:boolean) as element()* {
    let $elmname    := name($tnode)
    let $theitem    := if ($tnode/item) then $tnode/item[1] else <item original="false" label="{$item/@label}">{$item/desc}</item>
    let $language   := ($decor/@language, $decor/project/@defaultLanguage)[1]
    let $tnodedecor := ($tnode/ancestor::decor, $decor)[1]
    return
        if ($nesting > 30) then (
            (: too deeply nested, raise error and give up :)
            element templateerror {
                attribute {'type'} {'nesting'}(:,
                $tnode:)
            }
        ) else 
        if ($elmname='include') then (
            (:let $artefact := if ($tnode/@ref) then tmapi:artefactMissing('template', $tnode/@ref, $tnode/@flexibility, $tnode/ancestor::decor) else ():)
            let $artefact := 
                if ($tnode/@ref) then 
                   (: (tmapi:getTemplateById($tnode/@ref, $tnode/@flexibility, $tnodedecor, $tnodedecor/@versionDate, $tnodedecor/@language))[@id][1] :)
                   tmapi:getTemplateByRef($tnode/@ref, if ($tnode[@flexibility]) then $tnode/@flexibility else ('dynamic'), $decor/rules/ancestor::decor/project/@prefix, (), ())[@id][1]
                else ()
            let $tmpurl     := ($artefact/@url, $artefact/ancestor::cacheme/@bbrurl, $utillib:strDecorServicesURL)[1]
            let $tmpident   := ($artefact/@ident, $artefact/ancestor::decor/project/@prefix)[1]
            return
            element include {
                $tnode/(@* except (@tmid|@tmname|@tmdisplayName|@tmeffectiveDate|@tmexpirationDate|@tmstatusCode|@tmversionLabel|@linkedartefactmissing|@url|@ident)),
                if ($tnode/@ref) then (
                    for $att in $artefact/(@id, @name, @displayName, @effectiveDate, @expirationDate, @statusCode, @versionLabel)
                    return
                        attribute {'tm' || name($att)} {$att}
                    ,
                    if ($tmpurl) then attribute url {$tmpurl} else (),
                    if ($tmpident) then attribute ident {$tmpident} else (),
                    attribute linkedartefactmissing {empty($artefact)}
                ) else ()
                ,
                $tnode/text(),
                if ($serialize) then 
                    for $desc in $tnode/desc
                    return
                        utillib:serializeNode($desc)
                else ($tnode/desc)
                ,
                $theitem,
                for $s in $tnode/(* except (desc|item))
                return
                tmapi:copyNodes($s, $theitem, $nesting+1, $decor, $serialize)
                ,
                let $recentcardconf := tmapi:cardconfs1element ($artefact, $tnode/@minimumMultiplicity, $tnode/@maximumMultiplicity, $tnode/@isMandatory, $tnode/@conformance)
                let $theitem        := if ($artefact/item) then $artefact/item[1] else <item original="false" label="{$artefact/@name}"/>
                for $t in $recentcardconf
                return
                tmapi:copyNodes($t, $theitem, $nesting+1, $decor, $serialize),
                if (empty($artefact)) then () else (
                    tmapi:createStaticAssociationElement($decor/rules/templateAssociation[@templateId = $artefact/@id][@effectiveDate = $artefact/@effectiveDate], $language)
                )
            }
        ) else 
        if ($elmname=('desc','constraint','purpose','copyright', 'report', 'assert')) then (
            if ($serialize) then utillib:serializeNode($tnode) else ($tnode)
        ) else 
        if ($elmname=('example')) then (
            let $tnode := <example type="{($tnode/@type, 'neutral')[1]}">{$tnode/@caption, $tnode/node()}</example>
            return 
                if ($serialize) then utillib:serializeNode($tnode) else ($tnode)
        ) else 
        if ($elmname='relationship') then (
           (: let $artefact := if ($tnode/@template) then tmapi:artefactMissing('template', $tnode/@template, $tnode/@flexibility, $tnodedecor) else ():)
           let $artefact := 
                if ($tnode/@template) then 
                   tmapi:getTemplateByRef($tnode/@template, if ($tnode[@flexibility]) then $tnode/@flexibility else ('dynamic'), $decor/rules/ancestor::decor/project/@prefix, (), ())[@id][1]
                else ()
            let $tmpurl     := ($artefact/@url, $artefact/ancestor::cacheme/@bbrurl, $utillib:strDecorServicesURL)[1]
            let $tmpident   := ($artefact/@ident, $artefact/ancestor::decor/project/@prefix)[1]    
            return
            element relationship {
                $tnode/(@* except (@tmid|@tmname|@tmdisplayName|@tmeffectiveDate|@tmexpirationDate|@tmstatusCode|@tmversionLabel|@linkedartefactmissing|@url|@ident)),
                if ($tnode/@template) then (
                    attribute {'tmid'} {$artefact/@id},
                    attribute {'tmname'} {$artefact/@name},
                    attribute {'tmdisplayName'} {$artefact/@displayName},
                    if ($artefact/@effectiveDate) then attribute {'tmeffectiveDate'} {$artefact/@effectiveDate} else (),
                    if ($artefact/@expirationDate) then attribute {'tmexpirationDate'} {$artefact/@expirationDate} else (),
                    if ($artefact/@statusCode) then attribute {'tmstatusCode'} {$artefact/@statusCode} else (),
                    if ($artefact/@versionLabel) then attribute {'tmversionLabel'} {$artefact/@versionLabel} else (),
                    if ($tmpurl) then attribute url {$tmpurl} else (),
                    if ($tmpident) then attribute ident {$tmpident} else (),
                    attribute linkedartefactmissing {empty($artefact)}
                ) else ()
                ,
                $tnode/*
            }
        ) else 
        if ($elmname='vocabulary') then (
            let $artefact := tmapi:artefactMissing('valueSet', $tnode/@valueSet, $tnode/@flexibility, $tnodedecor)
            return
            element vocabulary {
                $tnode/(@* except (@vsid|@vsname|@vsdisplayName|@vseffectiveDate|@vsexpirationDate|@vsstatusCode|@vsversionLabel|@linkedartefactmissing|@url|@ident)),
                if ($tnode/@valueSet) then (
                    attribute {'vsid'} {$artefact/@id},
                    attribute {'vsname'} {$artefact/@name},
                    attribute {'vsdisplayName'} {if ($artefact[@displayName]) then $artefact/@displayName else $artefact/@name},
                    if ($artefact/@effectiveDate) then attribute {'vseffectiveDate'} {$artefact/@effectiveDate} else (),
                    if ($artefact/@expirationDate) then attribute {'vsexpirationDate'} {$artefact/@expirationDate} else (),
                    if ($artefact/@statusCode) then attribute {'vsstatusCode'} {$artefact/@statusCode} else (),
                    if ($artefact/@versionLabel) then attribute {'vsversionLabel'} {$artefact/@versionLabel} else (),
                    $artefact/@url,
                    $artefact/@ident,
                    attribute {'linkedartefactmissing'} {$artefact/@missing}
                ) else (),
                $tnode/*
            }
        ) else 
        if ($elmname='attribute') then (
            for $s in tmapi:normalizeAttributes($tnode)
            return
            element attribute {
                $s/@*,
                for $t in $s/* 
                return tmapi:copyNodes($t, $theitem, $nesting+1, $decor, $serialize)
            }
        ) else 
        if ($elmname=('let','defineVariable','classification','publishingAuthority','context','text')) then (
            $tnode
        ) else (
            let $artefact := if ($tnode/@contains) then tmapi:artefactMissing('template', $tnode/@contains, $tnode/@flexibility, $tnodedecor) else ()
            return
            element {$elmname} {
                $tnode/(@* except (@tmid|@tmname|@tmdisplayName|@tmeffectiveDate|@tmstatusCode|@linkedartefactmissing)),
                if ($tnode/@contains) then (
                    attribute {'tmid'} {$artefact/@id},
                    attribute {'tmname'} {$artefact/@name},
                    attribute {'tmdisplayName'} {$artefact/@displayName},
                    if ($artefact/@effectiveDate) then attribute {'tmeffectiveDate'} {$artefact/@effectiveDate} else (),
                    if ($artefact/@expirationDate) then attribute {'tmexpirationDate'} {$artefact/@expirationDate} else (),
                    if ($artefact/@statusCode) then attribute {'tmstatusCode'} {$artefact/@statusCode} else (),
                    if ($artefact/@versionLabel) then attribute {'tmversionLabel'} {$artefact/@versionLabel} else (),
                    $artefact/@url,
                    $artefact/@ident,
                    attribute {'linkedartefactmissing'} {$artefact/@missing}
                ) else (),
                (:$tnode/text(),:)
                if ($serialize) then 
                    for $desc in $tnode/desc
                    return
                        utillib:serializeNode($desc)
                else ($tnode/desc)
                ,
                if ($elmname = ('template', 'item')) then () else $theitem,
                for $s in $tnode/(* except (desc|item))
                return
                tmapi:copyNodes($s, $theitem, $nesting+1, $decor, $serialize)
            }
        )
};

(:
    Accepts 1 or more templates, although typically only 1 (TODO: change $e to element(template)? instead of element()* ?), and will use parameters 
    $minimumMultiplicity, $maximumMultiplicity, $isMandatory and $conformance to override all top level element of those templates.
    
    This is applicable to template content that is called via <include/> where the card/conf of the include overrides corresponding properties of the 
    top level template element it is calling.
    
    Overrides are applicable to <element/>, <include/> and <choice/>. If any of the card/conf parameters has no value then the value (if any) of the 
    top level element applies.
:)
declare function tmapi:cardconfs1element($e as element()*, $minimumMultiplicity as xs:string?, $maximumMultiplicity as xs:string?, $isMandatory as xs:string?, $conformance as xs:string? ) as element()* {
    for $child in $e/(element|include|choice|attribute|assert|report|let|defineVariable|constraint)
    let $minimumMultiplicity := if (string-length($minimumMultiplicity)=0) then ($child/@minimumMultiplicity) else ($minimumMultiplicity)
    let $maximumMultiplicity := if (string-length($maximumMultiplicity)=0) then ($child/@maximumMultiplicity) else ($maximumMultiplicity)
    let $isMandatory := if (string-length($isMandatory)=0) then ($child/@isMandatory) else ($isMandatory)
    let $conformance := if (string-length($conformance)=0) then ($child/@conformance) else ($conformance)
    return
        if ($child/self::element | $child/self::include) then (
            element {$child/name()} {
                $child/(@* except (@minimumMultiplicity|@maximumMultiplicity|@isMandatory|@conformance)),
                if (string-length($minimumMultiplicity)>0) then attribute minimumMultiplicity {$minimumMultiplicity} else (),
                if (string-length($maximumMultiplicity)>0) then attribute maximumMultiplicity {$maximumMultiplicity} else (),
                if (string-length($isMandatory)>0) then attribute isMandatory {$isMandatory} else (),
                if (string-length($conformance)>0) then attribute conformance {$conformance} else (),
                $child/node()
            }
        ) else if ($child/self::choice) then (
            element {$child/name()} {
                $child/(@* except (@minimumMultiplicity|@maximumMultiplicity|@isMandatory|@conformance)),
                if (string-length($minimumMultiplicity)>0) then attribute minimumMultiplicity {$minimumMultiplicity} else (),
                if (string-length($maximumMultiplicity)>0) then attribute maximumMultiplicity {$maximumMultiplicity} else (),
                $child/node()
            } 
        ) else (
            $child
        )
};

declare function tmapi:artefactMissing($what as xs:string, $ref as xs:string?, $flexibility as xs:string?, $decor as element(decor)) as element()? {
    let $ref            := $ref[string-length() gt 0]
    let $flexibility    := if ($flexibility castable as xs:dateTime) then $flexibility else 'dynamic'
    return
    (: returns <artefact missing="true"... if artefact cannot be found in decor project :)
    if (empty($decor)) then
        <artefact missing="false" id="{$ref}" name="" displayName="{$what}" effectiveDate="{$flexibility}" statusCode="oops"/>
    else 
    if (empty($ref)) then 
        <artefact missing="false" id="" name="" displayName="" effectiveDate="" statusCode=""/>
    else (
        switch ($what)
        case 'template' return (
            let $tmp        := (tmapi:getTemplateByRef($ref, $flexibility, $decor, $decor/@versionDate, $decor/@language)[@id])
            let $tmpurl     := ($tmp/@url, $tmp/ancestor::cacheme/@bbrurl, $utillib:strDecorServicesURL)[1]
            let $tmpident   := ($tmp/@ident, $tmp/ancestor::decor/project/@prefix)[1]
            return
                <artefact missing="{empty($tmp)}" id="{$tmp[1]/(@id|@ref)}">
                {
                    $tmp[1]/(@* except (@id|@ref|@url|@ident)),
                    if ($tmpurl) then attribute url {$tmpurl} else (),
                    if ($tmpident) then attribute ident {$tmpident} else ()
                }
                </artefact>
        )
        case 'valueSet' return (
            (: find out effectiveDate for this value set :)
            let $tmp        := vsapi:getValueSetByRef($ref, $flexibility, $decor, (), ())
            
            return
                <artefact missing="{empty($tmp)}" id="{$tmp[1]/(@id|@ref)}">{$tmp[1]/(@* except (@id|@ref))}</artefact>
        )
        default return ()
    )
};

(:recursive function to get all templates hanging off from the current template. has circular reference protection. Does not return the start templates :)
declare function tmapi:getTemplateChain($startTemplates as element(template)*, $decorsInScope as element(decor)*, $resultsmap as map(*)) as element(template)* {
    for $ref in ($startTemplates//element/@contains | $startTemplates//include/@ref)
    let $flexibility    := if ($ref/../@flexibility) then $ref/../@flexibility else ('dynamic')
    group by $ref, $flexibility
    return (
        let $template       := 
            if ($flexibility castable as xs:dateTime) then 
                $decorsInScope//template[@id = $ref][@effectiveDate = $flexibility]
            else (
                let $t      := $decorsInScope//template[@id = $ref]
                return
                    $t[@effectiveDate = string(max($t/xs:dateTime(@effectiveDate)))]
            )
        let $newkey         := concat($template[1]/@id, $template[1]/@effectiveDate)
        return
        if ($template) then
            if (map:contains($resultsmap, $newkey)) then () else (
                let $newmap         := map:merge(($resultsmap, map:merge(map:entry($newkey, ''))))
                
                return $template | tmapi:getTemplateChain($template, $decorsInScope, $newmap)
            )
        else ()
    )
};

declare %private function tmapi:getDecorByPrefix($decorOrPrefix as item(), $decorVersion as xs:string?, $language as xs:string?) as element(decor)* {
    typeswitch ($decorOrPrefix)
    case element(decor) return $decorOrPrefix
    default return
        if (utillib:isOid($decorOrPrefix)) then
            utillib:getDecorById($decorOrPrefix, $decorVersion, $language)
        else (
            utillib:getDecorByPrefix($decorOrPrefix, $decorVersion, $language)
        )
};

declare %private function tmapi:templateScan($t as element()?, $projectPrefix as xs:string?, $projectLanguage as xs:string, $by as xs:string, $indent as xs:int, $ylevel as xs:int, $templatesSoFar as xs:string*) as element()* {
(: get the templates, elements and include chain :)
    let $contains       :=   
        switch($projectLanguage)
            case "en-US" return "contains"
            case "de-DE" return "enthält"
            case "nl-NL" return "bevat"
            default return "contains"
 
    return        
    if (empty($t)) then (
    ) else 
    if ($t[self::template]) then (
        <template>
        {
            $t/(@id | @name | @displayName),
            attribute effectiveDate {format-dateTime($t/@effectiveDate, '[Y0001]&#8209;[M01]&#8209;[D01]')},
            attribute by {$by},
            attribute len {
                max((string-length($t/@id),string-length($t/@name))) + 
                string-length($t/@effectiveDate) + 
                string-length($contains) + 
                1
            }
        }
        {
             attribute classification {$t/classification/@type/string()}
        }
        {
            for $lc at $step in ($t//element[@contains] | $t//include[@ref])
            let $xid    := if ($lc[self::element]) then $lc/@contains else $lc/@ref
            let $lcby   := if ($lc[self::element]) then $contains else $lc/name()
            let $xflx   := if ($lc[@flexibility]) then $lc/@flexibility else 'dynamic'
            let $min    := $lc/@minimumMultiplicity
            let $max    := $lc/@maximumMultiplicity
            let $lct    := if (empty($projectPrefix)) then tmapi:getTemplateByRef($xid, $xflx)[@id][1] 
                else tmapi:getTemplateByRef($xid, $xflx, $projectPrefix, (), $projectLanguage)[@id][1]
            return 
                if ($templatesSoFar[.=$lct/concat(@id,@effectiveDate)]) then (
                    <template recurse="true">
                    {
                        $lct/(@id | @name | @displayName),
                        if ($min) then attribute minimumMultiplicity { $min } else (),
                        if ($max) then attribute maximumMultiplicity { $max } else (),
                        attribute effectiveDate {format-dateTime($lct/@effectiveDate, '[Y0001]&#8209;[M01]&#8209;[D01]')},
                        attribute by {$by},
                        attribute len {
                            max((string-length($lct/@id),string-length($lct/@name))) + 
                            string-length($lct/@effectiveDate) + 
                            string-length($contains) + 
                            1
                        }
                    }
                    </template>
                ) else (
                    tmapi:templateScan ($lct, $projectPrefix, $projectLanguage, $lcby, $indent + 1, $ylevel + $step, ($templatesSoFar, $lct/concat(@id,@effectiveDate)))
                )
        }
        </template>
    ) else 
    if ($t[self::element][@contains]) then (
        <element>
        {
            $t/@contains,
            $t/@flexibility,
            $t/@minimumMultiplicity,
            $t/@maximumMultiplicity,
            attribute by {$by},
            attribute len { string-length($t/@contains) + string-length($t/@flexibility) }
        }
        {
            for $lc at $step in ($t//element[@contains] | $t//include[@ref])
            let $xid    := if ($lc[self::element]) then $lc/@contains else $lc/@ref
            let $lcby   := if ($lc[self::element]) then $contains else $lc/name()
            let $xflx   := if ($lc[@flexibility]) then $lc/@flexibility else 'dynamic'
            let $min    := $lc/@minimumMultiplicity
            let $max    := $lc/@maximumMultiplicity
            let $lct    := if (empty($projectPrefix)) then tmapi:getTemplateByRef($xid, $xflx)[@id] 
                else tmapi:getTemplateByRef($xid, $xflx, $projectPrefix, (), $projectLanguage)[@id] 
            return 
                if ($templatesSoFar[.=$lct/concat(@id,@effectiveDate)]) then (
                    <template recurse="true">
                    {
                        $lct/(@id | @name | @displayName),
                        if ($min) then attribute minimumMultiplicity { $min } else (),
                        if ($max) then attribute maximumMultiplicity { $max } else (),
                        attribute effectiveDate {format-dateTime($lct/@effectiveDate, '[Y0001]&#8209;[M01]&#8209;[D01]')},
                        attribute by {$by},
                        attribute len {
                            max((string-length($lct/@id),string-length($lct/@name))) + 
                            string-length($lct/@effectiveDate) + 
                            string-length($contains) + 
                            1
                        }
                    }
                    </template>
                ) else (
                    tmapi:templateScan ($lct, $projectPrefix, $projectLanguage, $lcby, $indent + 1, $ylevel + $step, ($templatesSoFar, $lct/concat(@id,@effectiveDate)))
                )
        }
        </element>
    ) else 
    if ($t[self::include][@ref]) then (
        <include>
        {
            $t/@ref,
            $t/@flexibility,
            $t/@minimumMultiplicity,
            $t/@maximumMultiplicity,
            attribute by {$by},
            attribute len { string-length($t/@ref) + string-length($t/@flexibility) }
        }
        {
            for $lc at $step in ($t//element[@contains] | $t//include[@ref])
            let $xid    := if ($lc[self::element]) then $lc/@contains else $lc/@ref
            let $lcby   := if ($lc[self::element]) then $contains else $lc/name()
            let $xflx   := if ($lc[@flexibility]) then $lc/@flexibility else 'dynamic'
            let $min    := $lc/@minimumMultiplicity
            let $max    := $lc/@maximumMultiplicity
            let $lct    := if (empty($projectPrefix)) then tmapi:getTemplateByRef($xid, $xflx)[@id]
                else tmapi:getTemplateByRef($xid, $xflx, $projectPrefix, (), $projectLanguage)[@id] 
            return 
                if ($templatesSoFar[.=$lct/concat(@id,@effectiveDate)]) then (
                    <template recurse="true">
                    {
                        $lct/(@id | @name | @displayName),
                        if ($min) then attribute minimumMultiplicity { $min } else (),
                        if ($max) then attribute maximumMultiplicity { $max } else (),
                        attribute effectiveDate {format-dateTime($lct/@effectiveDate, '[Y0001]&#8209;[M01]&#8209;[D01]')},
                        attribute by {$by},
                        attribute len {
                            max((string-length($lct/@id),string-length($lct/@name))) + 
                            string-length($lct/@effectiveDate) + 
                            string-length($contains) + 
                            1
                        }
                    }
                    </template>
                ) else (
                    tmapi:templateScan ($lct, $projectPrefix, $projectLanguage, $lcby, $indent + 1, $ylevel + $step, ($templatesSoFar, $lct/concat(@id,@effectiveDate)))
                )
        }
        </include>
    ) else ()
};

declare %private function tmapi:chainCopy1 ($t as element(), $indent as xs:int) as element() {
    element {$t/name()} {
        $t/@*,
        attribute indent {$indent},
        for $i at $step in $t/*
        return tmapi:chainCopy1($i, $indent+1)
    }
};

declare %private function tmapi:chainCopy2 ($t as element(), $pos as xs:int) as element() {
    element {$t/name()} {
        $t/@*,
        attribute pos {$pos},
        for $i at $step in $t//*
        let $oname := $i/name()
        return element {$oname} {
            $i/@*,
            attribute pos {$step},
            attribute connector {
                if ($t//*[($step - 1)]/@indent = $t//*[$step]/@indent) then 2 
                else if ($t//*[($step - 1)]/@indent < $t//*[$step]/@indent) then 1
                else if ($t//*[($step - 1)]/@indent > $t//*[$step]/@indent) then 4 + 1
                else 0
            }
        }
    }
};

(:
    hgraph format returns the hierarchical graph of the template chain including classification
    hgraphwiki is the same as hgraph but the OIDs of the templates are in Mediawiki link style [[1.2.3...]]
    transclusionwikilist returns the list of templates in Mediawiki transclusion style, sorted by classification
    wikilist returns the list of templates in Mediawiki list style, sorted by id
    ==========
:)
declare %private function tmapi:apply-hgraph-stylesheet($input as node(), $format as xs:string) {
let $xsltParameters :=
    <parameters>
        <param name="outputformat"      value="{$format}"/>
        <param name="coretableonly"     value="false"/>
    </parameters>

let $xslt := 
    xs:anyURI(concat('xmldb:exist://', $setlib:strDecorCore, '/Template2list8hgraph.xsl'))

return 
    transform:transform($input, $xslt, $xsltParameters)
};

(:
    FHIR shorthand converter
    fshlogicalmodel format returns the hierarchical graph of the template chain including classification
    to be rendered as a logical model
    ==========
:)
declare %private function tmapi:apply-fsh-stylesheet($input as node(), $format as xs:string) {
let $xsltParameters :=
    <parameters>
        <param name="outputformat"      value="{$format}"/>
        <param name="title"             value="{string-join($input/@*, ',')}"/>
    </parameters>

let $xslt := 
    xs:anyURI(concat('xmldb:exist://', $setlib:strDecorCore, '/Template2fsh.xsl'))

return 
    $input(:transform:transform($input, $xslt, $xsltParameters):)
};


(: ===== WRITE FUNCTIONS ===== :)

(:  Adds project unique ids to template elements and attributes where they do not exist yet, 
:   based on the defaultBaseId for type EL in the project
:   
:   @param $decor               - required. The <decor/> element for the project the template is in
:   @param $id                  - optional. Id of the template. Matches template/@id. If empty does all templates.
:   @param $effectiveDate       - optional. Considered only if $id is not empty. null does all versions, yyyy-mm-ddThh:mm:ss gets this specific version
:   @return nothing or error
:   @since 2016-11-30

:)
declare function tmapi:addTemplateElementAndAttributeIds($decor as element(decor), $id as xs:string?, $effectiveDate as xs:string?) {
let $template               :=
    if (string-length($id)>0 and string-length($effectiveDate)>0) then
        $decor//template[@id=$id][@effectiveDate=$effectiveDate]
    else if (string-length($id)>0) then
        $decor//template[@id=$id]
    else (
        $decor//template[@id]
    )
let $defaultElementBaseId   := decorlib:getDefaultBaseIdsP($decor, $decorlib:OBJECTTYPE-TEMPLATEELEMENT)/@id
let $elementBaseId          := 
    if ($defaultElementBaseId) then
        if (ends-with($defaultElementBaseId,'.')) 
        then $defaultElementBaseId[1]/string()
        else concat($defaultElementBaseId[1],'.') 
    else ()
let $existingIds            := $decor//element[matches(@id,concat('^',$elementBaseId,'\d+$'))] | $decor//attribute[matches(@id,concat('^',$elementBaseId,'\d+$'))]
let $elementIncr            :=
    if ($existingIds) then
        max($existingIds/xs:integer(tokenize(@id,'\.')[last()]))
    else (
        0
    )
(: 2014-12-16 AH Add support for ids on attributes without id and with no more than 1 attribute defined. Excludes things like:
    <attribute classCode="OBS" moodCode="EVN"/>
    Adding an id on this type of attribute would be ambiguous
:)
let $update                 :=
    if ($elementBaseId) then
        for $element at $pos in ($template//element[empty(@id)] | 
                                 $template//attribute[empty(@id)][count(@name|@classCode|@contextConductionInd|@contextControlCode|
                                                                      @determinerCode|@extension|@independentInd|@institutionSpecified|
                                                                      @inversionInd|@mediaType|@moodCode|@negationInd|
                                                                      @nullFlavor|@operator|@qualifier|@representation|
                                                                      @root|@typeCode|@unit|@use)=1])
        let $i  := $elementIncr + $pos
        let $u  := update insert attribute id {concat($elementBaseId, $i)} into $element
        return $i
    else ()

return ()
};
