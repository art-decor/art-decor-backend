xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

(:~ News API allow retrieval, creation, updating of news items. For example about new features, upcoming maintenance windows etc. :)
module namespace lockapi            = "http://art-decor.org/ns/api/lock";

import module namespace roaster     = "http://e-editiones.org/roaster";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "library/settings-lib.xqm";
import module namespace utillib     = "http://art-decor.org/ns/api/util" at "library/util-lib.xqm";
import module namespace decorlib    = "http://art-decor.org/ns/api/decor" at "library/decor-lib.xqm";

declare namespace json      = "http://www.json.org";

(:~ Retrieves decor locks

@param $username                - optional. Retrieves locks for this username only
@param $prefix                  - optional. Retrieves locks for this project (prefix) only

@param $projectId               - optional. Retrieves locks for community connected to this projectId only
@param $objectId                - optional. Retrieves locks for this object only. If community this shall be the @name. If other it shall be the @id
@param $objectEffectiveDate     - optional. Retrieves locks for this object only. If community this shall be empty. If other it shall be the @effectiveDate
:)
declare function lockapi:getLocks($request as map(*)) {

    let $authmap                := $request?user
    let $username               := $request?parameters?username
    let $projectPrefix          := $request?parameters?prefix[string-length() gt 0]
    
    let $projectId              := $request?parameters?projectId[string-length() gt 0]
    let $objectId               := $request?parameters?objectId[string-length() gt 0]
    let $objectEffectiveDate    := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?objectEffectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?objectEffectiveDate[string-length() gt 0]
        }
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    
    let $locks                  := decorlib:getLocks((), $objectId, $objectEffectiveDate, $projectId)
    
    let $locks                  := if (empty($username)) then $locks else $locks[@user = $username]
    let $locks                  := if (empty($projectPrefix)) then $locks else $locks[@prefix = $projectPrefix]
    
    let $locks                  := 
        for $locksByUser in $locks
        let $user := $locksByUser/@user
        group by $user
        return
            <userLocks user="{$user}" userName="{$locksByUser[1]/@userName}">
            {
                for $lock in $locksByUser
                order by $lock/@type, $lock/@since
                return $lock
            }
            </userLocks>

    let $count      := count($locks)
    let $max        := $count
    let $allcnt     := $count
    
    return
        <list artifact="LOCKS" current="{if ($count le $max) then $count else $max}" total="{$count}" all="{$allcnt}" lastModifiedDate="{current-dateTime()}" xmlns:json="http://www.json.org">
        {
            utillib:addJsonArrayToElements($locks)
        }
        </list>
};

(:~ Sets a decor lock

@param $projectId               - optional. Sets lock for community connected to this projectId only
@param $objectId                - optional. Sets lock for this object only. If community this shall be the @name. If other it shall be the @id
@param $objectEffectiveDate     - optional. Sets locks for this object only. If community this shall be empty. If other it shall be the @effectiveDate
:)
declare function lockapi:postLock($request as map(*)) {

    let $authmap                := $request?user
    let $username               := $request?parameters?username
    let $projectPrefix          := $request?parameters?prefix[string-length() gt 0]
    
    let $projectId              := $request?parameters?projectId[string-length() gt 0]
    let $objectId               := $request?parameters?objectId[string-length() gt 0]
    let $objectEffectiveDate    := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?objectEffectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?objectEffectiveDate[string-length() gt 0]
        }
    
    let $breakLock              := $request?parameters?breakLock = true()
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    
    let $lock                   := decorlib:setLock($authmap, $projectId, $objectId, $objectEffectiveDate, $breakLock)
    
    return
        if (empty($lock)) then
            error($errors:SERVER_ERROR, 'Failed to create the lock as requested')
        else (
            roaster:response(201, $lock)
        )
};

(:~ Deletes decor locks

@param $username                - optional. Deletes locks for this username only (shall be authenticated as dba or have to be decor-admin in project with $projectPrefix)
@param $prefix                  - optional. Deletes locks for this project (prefix) only

@param $projectId               - optional. Retrieves locks for community connected to this projectId only
@param $objectId                - optional. Retrieves locks for this object only. If community this shall be the @name. If other it shall be the @id
@param $objectEffectiveDate     - optional. Retrieves locks for this object only. If community this shall be empty. If other it shall be the @effectiveDate
:)
declare function lockapi:deleteLocks($request as map(*)) {

    let $authmap                := $request?user
    let $username               := $request?parameters?username
    let $projectPrefix          := $request?parameters?prefix[string-length() gt 0]
    
    let $projectId              := $request?parameters?projectId[string-length() gt 0]
    let $objectId               := $request?parameters?objectId[string-length() gt 0]
    let $objectEffectiveDate    := 
        try {
            xmldb:decode-uri(xs:anyURI(string($request?parameters?objectEffectiveDate)))[string-length() gt 0]
        }
        catch * {
            $request?parameters?objectEffectiveDate[string-length() gt 0]
        }
    
    let $check                  :=
        if (empty($authmap)) then 
            error($errors:UNAUTHORIZED, 'You need to authenticate first')
        else ()
    
    let $locks                  := decorlib:getLocks((), $objectId, $objectEffectiveDate, $projectId)
    
    let $locks                  := if (empty($username)) then $locks else $locks[@user = $username]
    let $locks                  := if (empty($projectPrefix)) then $locks else $locks[@prefix = $projectPrefix]
    
    let $check                  :=
        if ($authmap?groups = 'dba') then () else (
            for $prefix in distinct-values($locks[not(@user = $authmap?name)]/@prefix)
            group by $prefix
            return
                if (decorlib:getActiveAuthors($prefix) = $authmap?name and $authmap?groups = ('decor', 'editor', 'dba')) then () else (
                    error($errors:FORBIDDEN, 'You SHALL be dba or author for the project that these locks are for, if the lock you are deleting is not one of your own.')
                )
        )        
    let $delete               := update delete $locks

    return
        roaster:response(204, ())
};