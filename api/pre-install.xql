xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace sm      = "http://exist-db.org/xquery/securitymanager";
import module namespace xmldb   = "http://exist-db.org/xquery/xmldb";
import module namespace repo    = "http://exist-db.org/xquery/repo";

declare namespace util           = "http://exist-db.org/xquery/util";

(: The following external variables are set by the repo:deploy function :)

(: file path pointing to the exist installation directory :)
(:declare variable $home external;:)
(: path to the directory containing the unpacked .xar package :)
(:declare variable $dir external;:)
(: the target collection into which the app is deployed :)
(:declare variable $target external;:)
(:install path for art (/db, /db/apps), no trailing slash :)
declare variable $root := repo:get-root();

(: helper function for creating database groups required for Art webapplication :)
declare %private function local:addUsersAndGroups() {
    if (sm:group-exists('decor')) then () else (
        sm:create-group('decor','admin','Group for general access to DECOR files')
    )
    ,
    if (sm:group-exists('terminology')) then () else (
        sm:create-group('terminology','admin','General terminology group')
    )
    ,
    if (sm:get-group-members('decor') = 'admin') then () else sm:add-group-member('decor', 'admin')
    ,
    if (sm:get-group-members('terminology') = 'admin') then () else sm:add-group-member('terminology', 'admin')
    ,
    if (sm:user-exists('api-user')) then (
        if (sm:get-group-members('decor') = 'api-user') then () else sm:add-group-member('decor', 'api-user'),
        if (sm:get-group-members('terminology') = 'api-user') then () else sm:add-group-member('terminology', 'api-user')
    )
    else (
        sm:create-account('api-user', util:uuid(), 'decor', ('decor', 'terminology'))
    )
    ,
    if (sm:user-exists('api-admin')) then (
        if (sm:get-group-members('dba') = 'api-admin') then () else sm:add-group-member('dba', 'api-admin'),
        if (sm:get-group-members('decor') = 'api-admin') then () else sm:add-group-member('decor', 'api-admin'),
        if (sm:get-group-members('terminology') = 'api-admin') then () else sm:add-group-member('terminology', 'api-admin')
    ) 
    else (
        sm:create-account('api-admin', util:uuid(), 'decor', ('dba', 'decor', 'terminology'))
    )
};

declare %private function local:addIndexConfig() {
    let $apiConf            := 
        <collection xmlns="http://exist-db.org/collection-config/1.0">
            <triggers><!--<trigger class="org.exist.extensions.exquery.restxq.impl.RestXqTrigger"/>--></triggers>
        </collection>
    
    (:== indexes ==:)
    (: /db/apps/api collection :)
    let $index-file := concat('/db/system/config', $root, 'api/collection.xconf')
    let $index-coll := xmldb:create-collection(concat('/db/system/config', $root), 'api')
    
    let $api-index-create             :=
        if (doc-available($index-file)) then () else (
            xmldb:store($index-coll,'collection.xconf', $apiConf),
            xmldb:reindex(substring-after($index-coll, '/db/system/config'))
        )
    
    (:let $triggerConf        :=
        <collection xmlns="http://exist-db.org/collection-config/1.0">
          <triggers>
            <!-- create update copy move delete -->
            <trigger event="create,update,copy,move,delete" class="org.exist.collections.triggers.XQueryTrigger">
              <parameter name="url" value="xmldb:exist://{$root}api/modules/library/trigger.xql"/>
            </trigger>
          </triggers>
        </collection>:)
    (: /db/apps/decor/trigger collection :)
    (:let $index-file := concat('/db/system/config', $root, 'decor/trigger/collection.xconf')
    let $index-coll := xmldb:create-collection(concat('/db/system/config', $root), 'decor/trigger')
    
    let $trigger-index-create         := 
        if (doc-available($index-file)) then () else (
            xmldb:store($index-coll,'collection.xconf', $triggerConf),
            xmldb:reindex(substring-after($index-coll, '/db/system/config'))
        ):)
    
    return ()
};

(:
experr:EXPATH00 Failed to install dependency from 
http://exist-db.org/exist/apps/public-repo/find?name=http%3A%2F%2Fexist-db.org%2Fapps%2Fshared&processor=6.2.0&version=: http://exist-db.org/exist/apps/public-repo/find?name=http%3A%2F%2Fexist-db.org%2Fapps%2Fshared&processor=6.2.0&version= [at line 110, column 17, source: String/-2322663942770752158]
In function:
	local:installExternalDependencies() [120:16:String/-2322663942770752158]
Severity

:)

(:~ installs packages needed for this package that live on the eXist-db public repo :)
declare %private function local:installExternalDependencies() {
    let $publicrepourl    := 'https://exist-db.org/exist/apps/public-repo'

    let $dependencies     := (
        (:<dependency package="http://exist-db.org/apps/shared"/> |:)
        <dependency package="http://existsolutions.com/ns/jwt" semver-min="1.0.0"/> |
        <dependency package="http://e-editiones.org/roaster" semver-min="1.0.0"/>
    )

    let $install          :=
        for $pkg in $dependencies
        let $pkguri     := $pkg/@package
        let $pkgvrs     := $pkg/@semver-min
        (: opportunistic approach: any version will do. returns: <status xmlns="http://exist-db.org/xquery/repo" result="ok" target="/db/system/repo/roaster-1.0.0"/> :)
        let $installed  := 
            if (repo:list()[. = $pkguri]) then <status result="ok"/> else (
                repo:install-and-deploy($pkg/@package, $publicrepourl || '/find')
            )
        return
            if ($installed/@result = 'ok') then () else (
                error(xs:QName('repo:DEPENDENCYMISSING'), 'This package failed to install ' || $pkguri || ' version ' || $pkgvrs || '. Install manually if necessary from package repository: ' || $publicrepourl)
            )
    
    return ()
};

let $update := local:installExternalDependencies()
let $update := local:addUsersAndGroups()
let $update := local:addIndexConfig()

return ()
