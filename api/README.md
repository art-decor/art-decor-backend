# NOTES

## eXist-db 5.3 dependency

This API has a dependency on eXist-db 5.3 because this is the first version of eXist-db that implements HTTP PATCH support

## eXist-db Scheduler dependency

Some tasks take longer than others. Compilation of projects - for project checks, runtime environments, instance-validation - is one of them. These processes write a simple request file in decor/scheduled-tasks and count on the [eXist-db Scheduler](https://exist-db.org/exist/apps/doc/scheduler.xml) to pick them up.

The file `etc/conf.xml` is expected to have these two lines in the element `/exist/scheduler`:
    
```xml
<!-- Run every 15 seconds to scan/process compilation requests -->
<job type="user" name="scheduled-compile" xquery="/db/apps/api/modules/library/scheduled-compile.xql" period="15000" unschedule-on-exception="false"/>
```
