xquery version "3.1";
(:
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
declare namespace sm            = "http://exist-db.org/xquery/securitymanager";
declare namespace xmldb         = "http://exist-db.org/xquery/xmldb";
declare namespace repo          = "http://exist-db.org/xquery/repo";
declare namespace cfg           = "http://exist-db.org/collection-config/1.0";
(: The following external variables are set by the repo:deploy function :)

(: file path pointing to the exist installation directory :)
declare variable $home external;
(: path to the directory containing the unpacked .xar package :)
declare variable $dir external;
(: the target collection into which the app is deployed :)
declare variable $target external;
(:install path for art (/db, /db/apps), no trailing slash :)
declare variable $root := repo:get-root();

(:
:   Helper function with recursion for adpfix:setDecorPermissions()
:)
declare %private function local:setPermissions($path as xs:string, $collusrown as xs:string?, $collgrpown as xs:string?, $collmode as xs:string, $resusrown as xs:string?, $resgrpown as xs:string?, $resmode as xs:string) {
    if (string-length($collusrown) = 0) then () else sm:chown(xs:anyURI($path),$collusrown),
    if (string-length($collgrpown) = 0) then () else sm:chgrp(xs:anyURI($path),$collgrpown),
    sm:chmod(xs:anyURI($path),$collmode),
    sm:clear-acl(xs:anyURI($path)),
    for $res in xmldb:get-child-resources(xs:anyURI($path))
    return (
        if (empty($resusrown)) then () else sm:chown(xs:anyURI(concat($path,'/',$res)),$resusrown),
        if (empty($resgrpown)) then () else sm:chgrp(xs:anyURI(concat($path,'/',$res)),$resgrpown),
        sm:chmod(xs:anyURI(concat($path,'/',$res)),$resmode),
        sm:clear-acl(xs:anyURI(concat($path,'/',$res)))
    )
    ,
    for $collection in xmldb:get-child-collections($path)
    return
        local:setPermissions(concat($path,'/',$collection), $collusrown, $collgrpown, $collmode, $resusrown, $resgrpown, $resmode)
};

(: helper function for creating top level database collection and index definitions required for Art webapplication :)
declare %private function local:createTopCollections() {
    let $conf               :=
        <collection xmlns="http://exist-db.org/collection-config/1.0">
            <index xmlns:xforms="http://www.w3.org/2002/xforms" xmlns:f="http://hl7.org/fhir" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fr="http://orbeon.org/oxf/xml/form-runner">
                <fulltext default="none" attributes="false"/>
                <!--<lucene>
                    <analyzer class="org.apache.lucene.analysis.standard.StandardAnalyzer">
                        <param name="stopwords" type="org.apache.lucene.analysis.util.CharArraySet"/>
                    </analyzer>
                    <text qname="@value"/>
                    <text qname="f:text"/>
                </lucene>-->
                <!-- eXist-db 2.2 -->
                <!--<range>
                    <create qname="@id" type="xs:string"/>
                    <create qname="@type" type="xs:string"/>
                    <create qname="@value" type="xs:string"/>
                    <create qname="@version" type="xs:string"/>
                </range>-->
                <!-- eXist-db 2.1 -->
                <create qname="@id" type="xs:string"/>
                <create qname="@type" type="xs:string"/>
                <create qname="@value" type="xs:string"/>
                <create qname="@version" type="xs:string"/>
                <create qname="@fhirVersion" type="xs:string"/>
            </index>
        </collection>
    
    let $root-coll          := xmldb:create-collection($root, 'fhir-data/3.0')
    let $perm               := local:setPermissions($root-coll, 'admin', 'decor', 'rwxrwsrwx', (), 'decor', 'rw-rw-r--')
    let $move-old-data      :=
        for $coll in ('_audit','_metadata','_snapshot','history','resources')
        return
            if (xmldb:collection-available(concat($root, 'fhir-data/', $coll))) then (
                xmldb:move(concat($root, 'fhir-data/', $coll), concat($root, 'fhir-data/1.0/'))
            ) else ()
    
    let $delete-old-indexes :=
        for $coll in ('fhir-data/_audit','fhir-data/_metadata','fhir-data/_snapshot','fhir-data/history','fhir-data/resources')
        return
            if (xmldb:collection-available(concat('/db/system/config',$root, $coll))) then (
                xmldb:remove(concat('/db/system/config',$root, $coll))
            ) else ()
    
    (:/db/apps collections:)
    let $move-old-app       :=
        if (xmldb:collection-available(concat($root, '/fhir-data/stu3'))) then
            xmldb:rename(concat(repo:get-root(), '/fhir-data/stu3'), '3.0')
        else ()
    
    for $coll in ('fhir-data/3.0/_audit','fhir-data/3.0/_metadata','fhir-data/3.0/_snapshot','fhir-data/3.0/history','fhir-data/3.0/resources')
    let $root-coll  := xmldb:create-collection($root, $coll)
    let $perm       := local:setPermissions($root-coll, 'admin', 'decor', 'rwxrwsrwx', (), 'decor', 'rw-rw-r--')
    
    (:== indexes ==:)
    let $index-coll := xmldb:create-collection(concat('/db/system/config',$root), $coll)
    let $index-file := concat($index-coll,'/collection.xconf')
    return (
        if (doc-available($index-file)) then () else (
            xmldb:store($index-coll,'collection.xconf',$conf),
            xmldb:reindex($root-coll)
        )
    )
};

sm:set-user-primary-group('admin','decor'),
local:createTopCollections()
