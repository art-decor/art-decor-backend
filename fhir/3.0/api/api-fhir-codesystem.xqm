xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

module namespace adfhircs           = "http://art-decor.org/ns/fhir/3.0/codesystem";
import module namespace adfhir      = "http://art-decor.org/ns/fhir/3.0" at "api-fhir.xqm";
import module namespace getf        = "http://art-decor.org/ns/fhir-settings" at "fhir-settings.xqm";
import module namespace cs          = "http://art-decor.org/ns/decor/codesystem" at "../../../art/api/api-decor-codesystem.xqm";
import module namespace utillib     = "http://art-decor.org/ns/api/util" at "../../../api/modules/library/util-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "../../../api/modules/library/settings-lib.xqm";
import module namespace markdown    = "http://art-decor.org/ns/api/markdown" at "../../../api/modules/library/markdown-lib.xqm";
declare namespace f                 = "http://hl7.org/fhir";
declare namespace error             = "http://art-decor.org/ns/fhir/error";

declare %private variable $adfhircs:type            := 'CodeSystem';

declare function adfhircs:convertDecorCodeSystem2FHIRCodeSystem($codeSystem as element(codeSystem)) as element() {
let $uuid               := util:uuid()
let $projectprefix      := if ($codeSystem/parent::*/@ident) then $codeSystem/parent::*/@ident else request:get-attribute('request.projectprefix')[string-length()>0]
let $projectversion     := request:get-attribute('request.projectversion')[string-length()>0]

(: FIXME: when the request contains a list of urls we would need the one that matches this codeSystem ... :)
let $url                := if (request:exists()) then request:get-parameter('url',())[string-length()>0][not(contains(., ','))] else ()

(:  this value has to match system uris in ValueSet. However: if we were called using parameter url, and the calling 
    function determined this codeSystem, then we have to assume that is the correct canonical :)
let $csuri              :=  
    if (empty($url)) then
        utillib:getCanonicalUriForOID('CodeSystem', $codeSystem, $projectprefix, $setlib:strKeyFHIRSTU3)
    else 
        $url
let $decorTypes         := utillib:getDecorTypes()
let $desigType          := $decorTypes//*:DesignationType
let $vocabType          := $decorTypes//*:VocabType
let $structureVersion   := <profile value="http://hl7.org/fhir/{$getf:strFhirVersionShort}/StructureDefinition/CodeSystem"/>
let $language           := if ($codeSystem/@language) then $codeSystem/@language else if ($codeSystem/*:desc[@language = 'en-US']) then 'en-US' else ($codeSystem/*:desc/@language)[1]

let $ordinals           := $codeSystem/*:conceptList/*:codedConcept/@ordinal
(: Terminology Capabilities and ordinal extension (https://chat.fhir.org/#narrow/stream/179202-terminology/topic/Terminology.20Capabilities.20and.20ordinal.20extension) :)
let $ordinalPropCode    := 'ordinal-value'
(: code | Coding | string | integer | boolean | dateTime
    DECOR only allows single values. Coding and code would not be distinguishable from string. Hence string is our default
    Note: STU3 does not support decimal.
:)
let $ordinalType        :=
    if (count($ordinals[. castable as xs:integer]) = count($ordinals)) then 'integer' else
    if (count($ordinals[. castable as xs:boolean]) = count($ordinals)) then 'boolean' else
    if (count($ordinals[. castable as xs:dateTime]) = count($ordinals)) then 'dateTime' else
        'string'

let $inactiveStatusCodes        := ('deprecated', 'retired', 'inactive', 'terminated', 'cancelled')
let $doParentChildProperties    := count(distinct-values($codeSystem/*:conceptList/*:codedConcept/@level)) gt 1

let $resourceId                 := replace(tokenize($csuri, '/')[last()], 'urn:oid:', '')
let $resourceId                 := if (matches($resourceId, '^[A-Za-z0-9\-\.]{1,64}$')) then $resourceId else $codeSystem/@id

(:FHIR id may not have colons. This affects using @effectiveDate as part of the Resource.id
    Remove any non-digit from the effectiveDate before adding it to the URI, e.g. 20161026123456
:)
return
    <CodeSystem xmlns="http://hl7.org/fhir">
        <id value="{$resourceId}"/>
    {
        if ($codeSystem/*:conceptList/*:codedConcept) then 
            <meta>
                <profile value="http://hl7.org/fhir/StructureDefinition/shareablecodesystem"/>
                {comment {fn:serialize($structureVersion)}}
            </meta>
        else ()
    }
    {
        if (empty($language)) then () else (
            <language value="{$language}"/>
        )
    }
    {
        (: Any valueSet in ART-DECOR is effective from effectiveDate to expirationDate :)
        if ($codeSystem[@effectiveDate castable as xs:dateTime] | $codeSystem[@expirationDate castable as xs:dateTime]) then (
            <extension url="http://hl7.org/fhir/StructureDefinition/resource-effectivePeriod">
                <valuePeriod>
                {
                    if ($codeSystem[@effectiveDate castable as xs:dateTime]) then 
                        <start value="{adjust-dateTime-to-timezone(xs:dateTime($codeSystem/@effectiveDate))}"/>
                    else (),
                    if ($codeSystem[@expirationDate castable as xs:dateTime]) then
                        <end value="{adjust-dateTime-to-timezone(xs:dateTime($codeSystem/@expirationDate))}"/>
                    else ()
                }
                </valuePeriod>
            </extension>
        ) else ()
    }
        <url value="{$csuri}"/>
        <identifier>
            <use value="official"/>
            <system value="urn:ietf:rfc:3986"/>
            <value value="urn:oid:{$codeSystem/@id}"/>
        </identifier>
        <version value="{let $semver := adfhir:getSemverString($codeSystem/@versionLabel) return if (empty($semver)) then $codeSystem/@effectiveDate else $semver}"/>
        <name value="{adfhir:validResourceName($codeSystem/@name)}"/>
    {
        if ($codeSystem/@displayName) then <title value="{$codeSystem/@displayName}"/> else ()
    }
        <status value="{if ($codeSystem/*:conceptList/*:codedConcept) then adfhircs:decorStatus2fhirStatus($codeSystem/@statusCode) else 'unknown'}"/>
    {
        if ($codeSystem/*:conceptList/*:codedConcept) then
            <experimental value="{$codeSystem/@experimental = 'true'}"/>
        else ()
    }
    {
        if ($codeSystem[*:publishingAuthority/@name]) then (
            <publisher value="{string-join($codeSystem/*:publishingAuthority/@name,', ')}"/>
            ,
            for $publisher in $codeSystem/*:publishingAuthority[*:addrLine/@type = ('phone', 'email', 'fax', 'uri')]
            return
            <contact>
                <name value="{$publisher/@name}"/>
            {
                for $addrLine in $publisher/*:addrLine[@type = ('phone', 'email', 'fax', 'uri')]
                let $system     := if ($addrLine/@type = 'uri') then 'url' else $addrLine/@type
                return
                <telecom>
                    <system value="{$system}"/>
                    <value value="{$addrLine}"/>
                </telecom>
            }
            </contact>
        )
        else (
            (: required for http://hl7.org/fhir/shareablevalueset.html:)
            <publisher value="ART-DECOR"/>
        )
    }
    {
        let $desc   := ($codeSystem/*:desc[@language = 'en-US'][.//text()], $codeSystem/*:desc[.//text()])[1]
        (: required for http://hl7.org/fhir/shareablecodesystem.html:)
        let $desc   := if (empty($desc)) then ($codeSystem/@displayName, $codeSystem/@name, '-')[1] else markdown:html2markdown(utillib:parseNode($desc)/node())
        return
            <description value="{$desc}"/>
    }
    {
        if ($codeSystem[*:purpose//text()]) then <purpose value="{utillib:serializeNode($codeSystem/*:purpose[.//text()][1])/node()}"/> else (),
        if ($codeSystem[*:copyright//text()]) then <copyright value="{utillib:serializeNode($codeSystem/*:copyright[.//text()][1])/node()}"/> else ()
    }
    {
        if ($codeSystem/*:conceptList/*:codedConcept) then
            <caseSensitive value="{$codeSystem/@caseSensitive = 'true'}"/>
        else ()
    }
        <content value="{if ($codeSystem/*:conceptList/*:codedConcept) then 'complete' else 'not-present'}"/>
        <count value="{count($codeSystem/*:conceptList/*:codedConcept)}"/>
    {
        if ($ordinals) then
            <property>
                <code value="{$ordinalPropCode}"/>
                <uri value="http://hl7.org/fhir/StructureDefinition/codesystem-ordinalValue"/>
                <description value="A numeric value that allows the comparison (less than, greater than) or other numerical manipulation of a concept (e.g. Adding up components of a score). Scores are usually a whole number, but occasionally decimals are encountered in scores."/>
                <type value="{$ordinalType}"/>
            </property>
        else (),
        (:http://hl7.org/fhir/concept-properties 
        status	       Status	       R5   A code that indicates the status of the concept. Typical values are active, experimental, deprecated, and retired
        inactive	     Inactive	     STU3 True if the concept is not considered active - e.g. not a valid concept any more. Property type is boolean, default value is false. Note that the status property may also be used to indicate that a concept is inactive
        deprecated	   Deprecated	   STU3 The date at which a concept was deprecated. Concepts that are deprecated but not inactive can still be used, but their use is discouraged, and they should be expected to be made inactive in a future release. Property type is dateTime. Note that the status property may also be used to indicate that a concept is deprecated
        effectiveDate	effectiveDate	R5   The date at which the concept was status was last changed
        notSelectable	Not Selectable STU3 The concept is not intended to be chosen by the user - only intended to be used as a selector for other concepts. Note, though, that the interpretation of this is highly contextual; all concepts are selectable in some context. Property type is boolean
        parent	       Parent	       STU3 The concept identified in this property is a parent of the concept on which it is a property. The property type will be 'code'. The meaning of 'parent' is defined by the hierarchyMeaning attribute
        child	        Child	        STU3 The concept identified in this property is a child of the concept on which it is a property. The property type will be 'code'. The meaning of 'child' is defined by the hierarchyMeaning attribute
        partOf	       Part Of	      R5   The concept identified in this property (by it's code) contains this concept as a component (i.e.. a part-of relationship rather than a subsumption relationship such as elbow is part-of arm
        synonym	      Synonym	      R5   This property contains an alternative code that may be used to identify this concept instead of the primary code
        comment	      Comment	      R5   A strng that provides additional detail pertinent to the use or understanding of the concept
        :)
        if ($codeSystem/*:conceptList/*:codedConcept/@statusCode) then
            <property>
                <code value="status"/>
                <uri value="http://hl7.org/fhir/concept-properties"/>
                <description value="A code that indicates the status of the concept. Values found in this version of the code system are: {string-join(distinct-values($codeSystem/*:conceptList/*:codedConcept/@statusCode), ', ')}"/>
                <type value="code"/>
            </property>
        else (),
        if ($codeSystem/*:conceptList/*:codedConcept[@expirationDate]) then
            <property>
                <code value="deprecated"/>
                <uri value="http://hl7.org/fhir/concept-properties"/>
                <description value="The date at which a concept was deprecated. Concepts that are deprecated but not inactive can still be used, but their use is discouraged, and they should be expected to be made inactive in a future release. Property type is dateTime. Note that the status property may also be used to indicate that a concept is deprecated"/>
                <type value="dateTime"/>
            </property>
        else (),
        if ($codeSystem/*:conceptList/*:codedConcept[@effectiveDate | @expirationDate | @officialReleaseDate]) then
            <property>
                <code value="effectiveDate"/>
                <uri value="http://hl7.org/fhir/concept-properties"/>
                <description value="The date at which the concept was status was last changed. This is calculated based on the highest of 'creation date', 'expiration date', and 'official release date'"/>
                <type value="dateTime"/>
            </property>
        else (),
        if ($codeSystem/*:conceptList/*:codedConcept[@statusCode[. = $inactiveStatusCodes] | @type[. = 'D']]) then
            <property>
                <code value="inactive"/>
                <uri value="http://hl7.org/fhir/concept-properties"/>
                <description value="The date at which a concept was deprecated. Concepts that are deprecated but not inactive can still be used, but their use is discouraged, and they should be expected to be made inactive in a future release. Property type is dateTime. Note that the status property may also be used to indicate that a concept is deprecated"/>
                <type value="boolean"/>
            </property>
        else (),
        if ($codeSystem/*:conceptList/*:codedConcept[@type = 'A']) then
            <property>
                <code value="notSelectable"/>
                <uri value="http://hl7.org/fhir/concept-properties"/>
                <description value="The concept is not intended to be chosen by the user - only intended to be used as a selector for other concepts. Note, though, that the interpretation of this is highly contextual; all concepts are selectable in some context. Property type is boolean"/>
                <type value="boolean"/>
            </property>
        else (),
        if ($doParentChildProperties) then (
            <property>
                <code value="parent"/>
                <uri value="http://hl7.org/fhir/concept-properties"/>
                <description value="The concept identified in this property is a parent of the concept on which it is a property. The property type will be 'code'. The meaning of 'parent' is defined by the hierarchyMeaning attribute"/>
                <type value="code"/>
            </property>
            ,
            <property>
                <code value="child"/>
                <uri value="http://hl7.org/fhir/concept-properties"/>
                <description value="The concept identified in this property is a child of the concept on which it is a property. The property type will be 'code'. The meaning of 'child' is defined by the hierarchyMeaning attribute"/>
                <type value="code"/>
            </property>
        ) else ()
    }
    {
        for $concept in $codeSystem/*:conceptList/*:codedConcept
        let $displayName    := $concept/*:designation[@language = $language][@type = 'fsn']
        let $displayName    := if (empty($displayName)) then $concept/*:designation[@language = $language][@type = 'preferred'] else $displayName
        let $displayName    := if (empty($displayName)) then $concept/*:designation[@language = $language] else $displayName
        let $displayName    := if (empty($displayName)) then $concept/*:designation else $displayName
        let $parentCode     := 
            if ($doParentChildProperties and $concept[@level castable as xs:integer]/xs:integer(@level) gt 0) then 
                ($concept/preceding-sibling::*:codedConcept[@level castable as xs:integer][xs:integer(@level) = ($concept/xs:integer(@level) - 1)])[1]
            else ()
        let $childCodes     :=
            if ($doParentChildProperties) then
                for $n in $concept/following-sibling::*:codedConcept[@level castable as xs:integer][xs:integer(@level) = ($concept/xs:integer(@level) + 1)]
                return
                    if ($n/preceding-sibling::*:codedConcept[@level castable as xs:integer][xs:integer(@level) = $concept/xs:integer(@level)][1]/@code = $concept/@code) then
                        $n
                    else ()
            else ()
        return
            <concept>
            {
                if ($concept[@ordinal]) then (
                    <extension url="http://hl7.org/fhir/StructureDefinition/codesystem-ordinalValue">
                        <valueDecimal value="{$concept/@ordinal}"/>
                    </extension>
                ) else ()
            }
                <code value="{$concept/@code}"/>
            {   
                if (empty($displayName)) then () else <display value="{$displayName[1]/@displayName}"/> 
            }
            {
                if ($concept/*:desc) then (
                    <definition>
                    {
                        if ($concept/*:desc[@language = 'en-US']) then 
                            attribute value {replace(string-join($concept/*:desc[@language = 'en-US'], '\n'), '\s+$', '')}
                        else (
                            attribute value {replace(string-join($concept/*:desc[1], '\n'), '\s+$', '')}
                        )
                    }
                    </definition>
                ) else ()
            }
            {
                for $designation in $concept/*:designation except $displayName
                let $typedisplay    := $desigType/*[@value=$designation/@type]/*:label[@language='en-US']
                return
                <designation>
                    <language value="{$designation/@language}"/>
                {
                    adfhircs:decorDesignationType2FhirUse($designation/@type, $typedisplay)
                }
                    <value value="{$designation/@displayName}"/>
                </designation>
            }
            {
                if ($concept/@ordinal) then
                    <property>
                        <code value="{$ordinalPropCode}"/>
                    {
                        element {concat('value', upper-case(substring($ordinalType, 1, 1)), substring($ordinalType, 2))} {
                            attribute value {$concept/@ordinal}
                        }
                    }
                    </property>
                else (),
                if ($concept/@statusCode) then
                    <property>
                        <code value="status"/>
                        <valueCode value="{$concept/@statusCode}"/>
                    </property>
                else (),
                if ($concept/@expirationDate) then
                    <property>
                        <code value="deprecated"/>
                        <valueDateTime value="{if (ends-with($concept/@expirationDate, 'T00:00:00')) then substring-before($concept/@expirationDate, 'T00:00:00') else adjust-dateTime-to-timezone(xs:dateTime($concept/@expirationDate))}"/>
                    </property>
                else (),
                if ($concept[@effectiveDate | @expirationDate | @officialReleaseDate]) then (
                    let $d  := max(for $dd in $concept/(@effectiveDate | @expirationDate | @officialReleaseDate) return xs:dateTime($dd))
                    return
                    <property>
                        <code value="effectiveDate"/>
                        <valueDateTime value="{if (ends-with($d, 'T00:00:00')) then substring-before($d, 'T00:00:00') else adjust-dateTime-to-timezone(xs:dateTime($d))}"/>
                    </property>
                )
                else (),
                if ($concept[@statusCode[. = $inactiveStatusCodes] | @type[. = 'D']]) then
                    <property>
                        <code value="inactive"/>
                        <valueBoolean value="true"/>
                    </property>
                else (),
                if ($concept[@type = 'A']) then
                    <property>
                        <code value="notSelectable"/>
                        <valueBoolean value="true"/>
                    </property>
                else (),
                for $n in $parentCode
                return
                    <property>
                        <code value="parent"/>
                        <valueCode value="{$n/@code}"/>
                    </property>
                ,
                for $n in $childCodes
                return
                    <property>
                        <code value="child"/>
                        <valueCode value="{$n/@code}"/>
                    </property>
            }
            </concept>
    }
    </CodeSystem>
};

declare function adfhircs:decorDesignationType2FhirUse($type as item()?, $typedisplay as element()?) as element()? {
<use xmlns="http://hl7.org/fhir">
{
    switch ($type)
    case 'fsn'          return (<system value="http://snomed.info/sct"/>, <code value="900000000000003001"/>, if ($typedisplay) then <display value="Fully specified name"/> else ())     (: FHIR core :)
    default             return (<system value="http://snomed.info/sct"/>, <code value="900000000000013009"/>, if ($typedisplay) then <display value="Synonym"/> else ())
    (:case 'preferred'    return (<system value="http://snomed.info/sct"/>, <code value="900000000000013009"/>, if ($typedisplay) then <display value="Synonym"/> else ()) (\: extension would be 900000000000548007 :\)
    case 'synonym'      return (<system value="http://snomed.info/sct"/>, <code value="900000000000013009"/>, if ($typedisplay) then <display value="Synonym"/> else ()) (\: FHIR core :\)
    case 'abbreviation' return (<system value="http://snomed.info/sct"/>, <code value="900000000000013009"/>, if ($typedisplay) then <display value="Synonym"/> else ()) (\: extension would be 398223008 :\)
    default             return (<system value="https://assets.art-decor.org/ADAR/rv/DECOR.xsd#DesignationType"/>, <code value="{$type}"/>, if ($typedisplay) then <display value="{$typedisplay}"/> else ()):)
}
</use>
};

(:~ http://hl7.org/fhir/STU3/valueset-publication-status.html :)
declare function adfhircs:decorStatus2fhirStatus($status as xs:string?) as xs:string? {
    switch ($status)
    case 'new'          return 'draft'
    case 'draft'        return 'draft'
    case 'pending'      return 'draft'
    case 'final'        return 'active'
    case 'cancelled'    return 'retired'
    case 'rejected'     return 'retired'
    case 'deprecated'   return 'retired'
    default             return 'unknown'
};

(:~ http://hl7.org/fhir/STU3/valueset-publication-status.html :)
declare function adfhircs:fhirStatus2decorStatus($status as xs:string?) as xs:string* {
    switch ($status)
    case 'draft'        return 'draft'
    case 'active'       return 'final'
    case 'retired'      return 'deprecated'
    default             return ()
};
