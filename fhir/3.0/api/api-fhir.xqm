xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

module namespace adfhir             = "http://art-decor.org/ns/fhir/3.0";
import module namespace getf        = "http://art-decor.org/ns/fhir-settings" at "fhir-settings.xqm";
declare namespace validation        = "http://exist-db.org/xquery/validation";
declare namespace transform         = "http://exist-db.org/xquery/transform";
declare namespace xs                = "http://www.w3.org/2001/XMLSchema";
declare namespace svrl              = "http://purl.oclc.org/dsdl/svrl";
declare namespace f                 = "http://hl7.org/fhir";
declare namespace error             = "http://art-decor.org/ns/fhir/error";
declare namespace get               = "http://art-decor.org/ns/art-decor-settings";
declare namespace snomed            = "http://art-decor.org/ns/terminology/snomed";
(:declare namespace fhir-context      = "java:ca.uhn.fhir.context.FhirContext";
declare namespace fhir-parser       = "java:ca.uhn.fhir.parser.IParser";:)

declare %private variable $adfhir:baseUri               := 'http://localhost:8877/';
(:declare %private variable $adfhir:hapiFhirContext       := fhir-context:new();
declare %private variable $adfhir:hapiFhirJsonParser    := fhir-context:newJsonParser($adfhir:hapiFhirContext);
declare %private variable $adfhir:hapiFhirXmlParser     := fhir-context:newXmlParser($adfhir:hapiFhirContext);:)
declare %private variable $adfhir:_supportedResources   := ('Bundle','CapabilityStatement','NamingSystem','StructureDefinition','ValueSet','CodeSystem');
declare %private variable $adfhir:_supportedOperations  := ($getf:PARAM_METADATA, $getf:PARAM_HISTORY, $getf:PARAM_META, $getf:PARAM_SEARCH);

(:Determine response format based on _format parameter or Accept header
    See: http://hl7.org/fhir/http.html (§2.1.0.6 Content Types and encodings)
    
    And http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.1
        Accept: text/plain; q=0.5, text/html,
                text/x-dvi; q=0.8, text/x-c
                
    From Saxon command line we get:
        Accept: text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2

  If try to determine exactly 1 format, but if all else fails return all suggested alternatives. 
  When presented with a wildcard like */* or , default to application/xml+fhir
:)
declare function adfhir:getResponseFormats() as xs:string* {
let $_response-format           := request:get-parameter($getf:PARAM_FORMAT, ())[not(. = '')][1]
let $_response-format           := 
    if (empty($_response-format)) then 
        for $type in tokenize(tokenize(head((request:get-header("accept"), request:get-header("Accept"))), ';')[1], "\s*,\s*")
        return
            normalize-space($type)[not(. = '')]
    else (
        $_response-format
    )
return
    if ($_response-format = $getf:CT_FHIR_XML)
    then $getf:CT_FHIR_XML
    else
    if ($_response-format = $getf:CT_FHIR_JSON)
    then $getf:CT_FHIR_JSON
    else
    if (empty($_response-format))
    then $getf:CT_FHIR_XML
    else
    if ($_response-format = ('xml','text/xml','application/xml','application/xml+fhir','application/fhir+xml','*/xml','*/*','*','application/*','text/*')) 
    then $getf:CT_FHIR_XML
    else
    if ($_response-format = ('json','text/json','application/json','application/json+fhir','application/fhir+json','*/json'))
    then $getf:CT_FHIR_JSON
    else (
        (:error(xs:QName('f:UnsupportedFormat'), 'Requested mime-type not supported. Found: ' || string-join($_response-format, ',') || '. Expected one of: ' || string-join(($getf:CT_FHIR_XML, $getf:CT_FHIR_JSON), ', ') || '.'):)
        $_response-format
    )
};
(:~ Return file extension for use on Content-Disposition header. Example .xml :)
declare function adfhir:getResponseExtension($response-format as xs:string*) as xs:string {
    if ($response-format = $getf:CT_FHIR_XML)
    then '.xml'
    else
    if ($response-format = $getf:CT_FHIR_JSON)
    then '.json'
    else (
         '.txt'
    )
};

(:~ Note: FIXME application/xml+json as only option until we know how to call HAPI-FHIR.:)
declare function adfhir:handleResponseFormat($data as item()?, $_response-format as xs:string*) as item()* {
    if ($data) then
        switch ($_response-format)
        case $getf:CT_FHIR_XML return $data
        case $getf:CT_FHIR_JSON return adfhir:xml2json($data/descendant-or-self::f:*[1])
        default return (
            $data (:oops... not (FHIR) XML?:)
        )
    else ()
};

(:~ FIXME Hack alert: when you do not write to db first, then eXist applies xml escaping to <> in json. Gave up after several 
    hours of unsuccessfully trying to come up with a valid reproduction package to report a bug on
:)
declare function adfhir:xml2json($data as element()?) as xs:string? {
    let $d  := 
    if ($data) then (
        let $d := xml-to-json(transform:transform($data, doc($getf:strFhirXml2JsonXsl), ()))
        let $f  := util:uuid() || '.json'
        let $s  := xmldb:store('/db/apps/decor/tmp', $f, $d)
        let $d  := 
        try {
            if (request:get-attribute('response.cors.allow-origin')[. = '']) then () else response:set-header('Access-Control-Allow-Origin', request:get-attribute('response.cors.allow-origin')),
            if (request:get-attribute('response.cors.allow-methods')[. = '']) then () else response:set-header('Access-Control-Allow-Methods', request:get-attribute('response.cors.allow-methods')),
            if (request:get-attribute('response.cors.allow-headers')[. = '']) then () else response:set-header('Access-Control-Allow-Headers', request:get-attribute('response.cors.allow-headers')),
            if (request:get-attribute('response.cors.expose-headers')[. = '']) then () else response:set-header('Access-Control-Expose-Headers', request:get-attribute('response.cors.expose-headers')),
            response:stream-binary(util:binary-doc($s), $getf:CT_FHIR_JSON)
        }
        catch * {()}
        let $s  := xmldb:remove('/db/apps/decor/tmp', $f)
        return $d
    )
    else ()
    return ()
};
declare function adfhir:json2xml($data as item()) as element()? {
    typeswitch ($data)
    case document-node()    return transform:transform($data, doc($getf:strFhirJson2XmlXsl), ())
    case element()          return transform:transform($data, doc($getf:strFhirJson2XmlXsl), ())
    case xs:string          return transform:transform(json-to-xml($data), doc($getf:strFhirJson2XmlXsl), ())
    default return error(xs:QName('adfhir:UnknownInput'), 'Internal processing error of input.')
};

declare function adfhir:supportedResources() as xs:string* {
    $adfhir:_supportedResources
};

declare function adfhir:supportedOperations() as xs:string* {
    $adfhir:_supportedOperations
};

declare function adfhir:getResource($type as xs:string?,$_id as xs:string?,$_version as xs:string?) as element()* {
let $coll               :=
    if ($type='SecurityEvent') 
    then $getf:colFhirAudit
    else if (string-length($_id)>0 and string-length($_version)>0) 
    then ($getf:colFhirResources | $getf:colFhirHistory)
    else (
        $getf:colFhirResources
    )
return
    if (string-length($type)>0 and string-length($_id)>0 and string-length($_version)>0) then (
        $coll/entry[@id=$_id][@version=$_version][@type=$type]
    )
    else if (string-length($type)>0 and string-length($_id)>0) then (
        $coll/entry[@id=$_id][@type=$type]
    )
    else if (string-length($_id)>0 and string-length($_version)>0) then (
        (:this is funky: any resource type that matches id + version...:)
        $coll/entry[@id=$_id][@version=$_version]
    )
    else if (string-length($_id)>0) then (
        (:this is funky: any resource type that matches id...:)
        $coll/entry[@id=$_id]
    )
    else if (string-length($type)>0) then (
        $coll/entry[@type=$type]
    )
    else (
        $coll/entry
    )
};

declare function adfhir:getResourceSnapshot($type as xs:string?,$_id as xs:string?, $_offset as xs:integer?, $_count as xs:integer?) as element()* {
let $serverurl          := $getf:strFhirServices
let $coll               := collection($getf:strFhirSnapshot)
let $_offset            := if (empty($_offset)) then 1 else $_offset

let $entries            :=
    if (string-length($type)>0 and string-length($_id)>0) then
        $coll//entry[@id=$_id][@type=$type]
    else if (string-length($type)>0) then
        $coll//entry[@type=$type]
    else (
        $coll//entry
    )

for $entry in $entries
let $_feedid            := $entry/@id
let $matches            := $entry/*/f:Bundle/f:entry[f:search/f:mode[@value = 'match']]
let $_totalcount        := count($matches)
let $_resultcount       := if (empty($_count) or ($_totalcount - $_offset) < $_count) then ($_totalcount - $_offset + 1) else ($_count)
let $_nextoffset        := if (empty($_count) or ($_totalcount - $_offset) < $_count) then () else ($_offset + $_count)
let $_lastoffset        := if (empty($_count) or $_count = 0) then () else (floor($_totalcount div $_count) * $_count)
return
<entry>
{
    $entry/@*, $entry/meta
}
    <body>
        <Bundle xmlns="http://hl7.org/fhir">
            {$entry/*/f:Bundle/f:id}
            {comment {'<meta><profile value="http://hl7.org/fhir/' || $getf:strFhirVersionShort || '/StructureDefinition/Bundle"/></meta>'}}
            {$entry/*/f:Bundle/f:type}
            {$entry/*/f:Bundle/f:base}
            <total value="{$_resultcount}"/>
            <link>
                <relation value="{$serverurl}Bundle/{$_feedid}?_format=application/xml+fhir&amp;search-offset={$_offset}&amp;_count={$_count}"/>
                <url value="self"/>
            </link>
            {if ($_totalcount <= $_resultcount) then () else (
            <link>
                <relation value="{$serverurl}Bundle/{$_feedid}?_format=application/xml+fhir&amp;search-offset=1&amp;_count={$_count}"/>
                <url value="first"/>
            </link>)}
            {if ($_totalcount <= $_resultcount or empty($_nextoffset)) then () else (
            <link>
                <relation value="{$serverurl}Bundle/{$_feedid}?_format=application/xml+fhir&amp;search-offset={$_nextoffset}&amp;_count={$_count}"/>
                <url value="next"/>
            </link>)}
            {if ($_totalcount <= $_resultcount or empty($_lastoffset)) then () else (
            <link>
                <relation value="{$serverurl}Bundle/{$_feedid}?_format=application/xml+fhir&amp;search-offset={$_lastoffset}&amp;_count={$_count}"/>
                <url value="last"/>
            </link>
            )}
            {
                for $entry in subsequence($matches,$_offset,$_resultcount)
                return
                    if ($entry[adfhir:reference]) then (
                        let $stype      := $entry/adfhir:reference/@type
                        let $sid        := $entry/adfhir:reference/@id
                        let $sversion   := $entry/adfhir:reference/@version
                        let $entry      := adfhir:getResource($stype,$sid,$sversion)
                        return
                        if ($entry[@deleted]) then (
                            <entry>
                                <deleted>
                                    <type value="{$entry[1]/@type}"/>
                                    <resourceId value="{$entry[1]/@id}"/>
                                    <versionId value="{$entry[1]/@version}"/>
                                    <instant value="{$entry[1]/@lastupdated}"/>
                                </deleted>
                            </entry>
                        )
                        else (
                            (:body element has no namespace but we're in fhir namespace context:)
                            <entry>
                                <resource>{$entry[1]/*:body[1]/node()}</resource>
                            </entry>
                        )
                    )
                    else ($entry)
                ,
                $entry/*/f:Bundle/(f:entry except $matches)
            }
        </Bundle>
    </body>
</entry>
};

declare function adfhir:getResourceByIdentifier($type as xs:string?,$_identifier as xs:string?) as element()* {
let $coll               := $getf:colFhirResources

return
    if (string-length($_identifier)>0) then
        $coll//entry[@type=$type][body/f:*/f:identifier/@value=$_identifier]
    else if (string-length($type)>0) then
        $coll//entry[@type=$type]
    else (
        $coll//entry
    )
};

declare function adfhir:getValueSetBySystem($_system as xs:string) as element()* {
let $coll               := $getf:colFhirResources
let $type               := 'ValueSet'

return
    if ($_system='') then
        $coll//entry[@type=$type]
    else (
        $coll//entry[@type=$type][body/f:*/f:define/f:system/@value=$_system]
    )
};

(:
    TODO: support $sort
:)
declare function adfhir:saveBundle($entries as element(entry)*, $bundleType as xs:string, $count as xs:integer?, $sort as xs:string*) as element() {
let $resourceName       := request:get-attribute('request.resource')[string-length()>0]
let $serverurl          := $getf:strFhirServices
let $_now               := current-dateTime()
let $_feedid            := util:uuid()
let $_resultcount       := if ($bundleType = $getf:BUNDLE_SEARCHSET) then count($entries/*:body/*[1][local-name() = $resourceName]) else count($entries)
let $_bundlecount       := if (not(empty($count)) and $_resultcount > $count) then $count else $_resultcount

let $bundle             :=
<Bundle xmlns="http://hl7.org/fhir">
    <id value="{$_feedid}"/>
    {comment {'<meta><profile value="http://hl7.org/fhir/' || $getf:strFhirVersionShort || '/StructureDefinition/Bundle"/></meta>'}}
    <type value="{$bundleType}"/>
    <base value="{$serverurl}"/>
    <total value="{$_resultcount}"/>
    <link>
        <relation value="{$serverurl}Bundle/{$_feedid}?_format=application/xml+fhir&amp;search-offset=1&amp;_count={$count}"/>
        <url value="self"/>
    </link>
    {if (empty($count) or $_resultcount <= $count) then () else (
    <link>
        <relation value="{$serverurl}Bundle/{$_feedid}?_format=application/xml+fhir&amp;search-offset=1&amp;_count={$count}"/>
        <url value="first"/>
    </link>,
    <link>
        <relation value="{$serverurl}Bundle/{$_feedid}?_format=application/xml+fhir&amp;search-offset={$count}&amp;_count={$count}"/>
        <url value="next"/>
    </link>,
    <link>
        <relation value="{$serverurl}Bundle/{$_feedid}?_format=application/xml+fhir&amp;search-offset={$_resultcount - $count}&amp;_count={$count}"/>
        <url value="last"/>
    </link>
    )}
    {
        (:  Entry in the bundle - will have deleted or resource status only when resource deleted or resource (but not both) :)
        for $entry in $entries
        let $currentResourceName    := $entry/*:body/*[1]/local-name()
        return
        <entry>
        {
            if ($entry[@deleted]) then (
                <deleted>
                    <type value="{$entry/@type}"/>
                    <resourceId value="{$entry/@id}"/>
                    <versionId value="{$entry/@version}"/>
                    <instant value="{$entry/@lastupdated}"/>
                </deleted>
            )
            else (
                (:body element has no namespace but we're in fhir namespace context:)
                <resource>{$entry/*:body/node()}</resource>
                ,
                if ($bundleType = $getf:BUNDLE_SEARCHSET) then 
                    switch ($currentResourceName)
                    case 'OperationOutcome' return <search><mode value="outcome"/></search>
                    case $resourceName      return <search><mode value="match"/></search>
                    default                 return <search><mode value="include"/></search>
                else ()
            )
        }
        </entry>
    }
</Bundle>

let $wrapper            :=
    <entry type="{$bundle/local-name()}" id="{$_feedid}" version="1" original-mime-type="{$getf:CT_FHIR_XML}" created="{$_now}" lastupdated="{$_now}" author="{(sm:id()//sm:real/sm:username/text())[1]}" fhirVersion="{$getf:strFhirVersion}">
        <meta/>
        <body>{$bundle}</body>
    </entry>

(:all resources names are uuid. Do we need protection against resource name collisions?:)
let $storeName          := concat(util:uuid(),'.xml')
let $storeNewCurrent    := xmldb:store($getf:strFhirSnapshot,$storeName,$wrapper)

return
    <result c="true" location="{concat($getf:strFhirServices,$wrapper/@type,'/',$wrapper/@id,'/_history/',$wrapper/@version)}">{$wrapper/@*}</result>
};

(:
    TODO: support $count, $since, $sort
:)
declare function adfhir:saveResponseBundle($results as element(result)*, $bundleType as xs:string, $count as xs:integer?, $sort as xs:string*) as element() {
let $serverurl          := $getf:strFhirServices
let $_now               := current-dateTime()
let $_feedid            := util:uuid()
let $_resultcount       := count($results)
let $_bundlecount       := if (not(empty($count)) and $_resultcount > $count) then $count else $_resultcount
let $selflocation       := concat($serverurl,'Bundle/',$_feedid,'?_format=application/xml+fhir&amp;search-offset=1&amp;_count=',$count)

let $bundle             :=
    <Bundle xmlns="http://hl7.org/fhir">
        <id value="{$_feedid}"/>
        {comment {'<meta><profile value="http://hl7.org/fhir/' || $getf:strFhirVersionShort || '/StructureDefinition/Bundle"/></meta>'}}
        <type value="{$bundleType}"/>
        <base value="{$serverurl}"/>
        <total value="{$_resultcount}"/>
        <link>
            <relation value="{$selflocation}"/>
            <url value="self"/>
        </link>
        {if (empty($count) or $_resultcount <= $count) then () else (
        <link>
            <relation value="{$serverurl}Bundle/{$_feedid}?_format=application/xml+fhir&amp;search-offset=1&amp;_count={$count}"/>
            <url value="first"/>
        </link>,
        <link>
            <relation value="{$serverurl}Bundle/{$_feedid}?_format=application/xml+fhir&amp;search-offset={$count}&amp;_count={$count}"/>
            <url value="next"/>
        </link>,
        <link>
            <relation value="{$serverurl}Bundle/{$_feedid}?_format=application/xml+fhir&amp;search-offset={$_resultcount - $count}&amp;_count={$count}"/>
            <url value="last"/>
        </link>
        )}
    {
        (:  Result in the bundle - will have deleted or resource status only when resource deleted or resource (but not both) :)
        for $result in $results
        return
            <entry xmlns="http://hl7.org/fhir">
                <adfhir:reference>{$result/@*}</adfhir:reference>
            </entry>
    }
    </Bundle>

let $wrapper            :=
    <entry type="{$bundle/local-name()}" id="{$_feedid}" version="1" original-mime-type="{$getf:CT_FHIR_XML}" created="{$_now}" lastupdated="{$_now}" author="{(sm:id()//sm:real/sm:username/text())[1]}" fhirVersion="{$getf:strFhirVersion}">
        <meta/>
        <body>{$bundle}</body>
    </entry>

(:all resources names are uuid. Do we need protection against resource name collisions?:)
let $storeName          := concat(util:uuid(),'.xml')
let $storeNewCurrent    := xmldb:store($getf:strFhirSnapshot,$storeName,$wrapper)

return
    <result c="true" location="{$selflocation}">{$wrapper/@*}</result>
};

(: SecurityEvent resources can only be created as new and cannot be updated afterwards :)
declare function adfhir:createSecurityEvent($resource as element(f:SecurityEvent)) {
let $_id                := $resource/f:id/@value
let $_version           := 1
let $mimeTypeReceived   := 'application/xml+fhir'
let $_now               := current-dateTime()

let $wrapper            :=
    <entry type="{$resource/local-name()}" id="{$_id}" version="{$_version}" original-mime-type="{$mimeTypeReceived}" created="{$_now}" lastupdated="{$_now}" author="{(sm:id()//sm:real/sm:username/text())[1]}" fhirVersion="{$getf:strFhirVersion}">
        <meta/>
        <body>{$resource}</body>
    </entry>

(:all resources names are uuid. Do we need protection against resource name collisions?:)
let $storeName          := concat(util:uuid(),'.xml')
let $storeNewCurrent    := xmldb:store($getf:strFhirAudit,$storeName,$wrapper)

return ()
};

(: Saving does PUT (update) and POST (create)
    creates new id as uuid based on max @id in $getf:strFhirResources for @type=$resource/local-name() e.g. ValueSet
    creates new version as xs:integer based on max current version of this resource
:)
declare function adfhir:saveResource($resource as element(),$_id as xs:string?,$if-matches as xs:string?,$mimeTypeReceived as xs:string?) as element(result) {
let $_type              := $resource/local-name()
let $_namespace         := $resource/namespace-uri()
let $_id                := if (empty($_id)) then (util:uuid()) else ($_id)
let $_currents          := adfhir:getResource($_type, $_id, ())

let $check              :=
    if ($if-matches[string-length()>0] and not($_currents[@version=$if-matches])) then
        error(QName('http://art-decor.org/ns/fhir/error','ResourceNotCurrent'),concat('Resource id ''', $_currents/@id, ''' version ''', $_currents/@version, ''' did not match header ''', $getf:HEADER_IF_MATCH, ''': ', $if-matches))
    else if (exists($_currents[@deleted])) then
        error(QName('http://art-decor.org/ns/fhir/error','ResourceDeleted'),'Cannot update deleted resource')
    else ()

(:should only be one...:)
let $_version           := if (empty($_currents)) then (1) else (max($_currents/xs:integer(@version))+1)
let $_now               := current-dateTime()
let $_creationdate      := if (not($_currents[@created])) then $_now else ($_currents/@created)[1]

let $wrapper            :=
    <entry type="{$_type}" id="{$_id}" version="{$_version}" original-mime-type="{$mimeTypeReceived}" created="{$_creationdate}" lastupdated="{$_now}" author="{(sm:id()//sm:real/sm:username/text())[1]}" fhirVersion="{$getf:strFhirVersion}">
        <meta/>
        <body>
        {
            (:STU3. Need to have id inside resource:)
            (: 0..1 Metadata about the resource :)
            (: 0..1 Version specific identifier :)
            (: 0..1 When the resource version last changed :)
            element {QName($resource/namespace-uri(),$resource/local-name())} {
                <id value="{$_id}"/>
                ,
                <meta>
                    <versionId value="{$_version}"/>
                    <lastUpdated value="{$_now}"/>
                    {$resource/f:meta/(* except (f:versionId|f:lastUpdated))}
                </meta>
                ,
                for $n in $resource/(node() except (f:id|f:meta))
                return adfhir:copyIntoNamespace($n)
            }
        }
        </body>
    </entry>

let $moveOldCurrent     := adfhir:moveToHistory($_currents)

let $storeName          := concat(util:uuid(),'.xml')
let $storeNewCurrent    := xmldb:store($getf:strFhirResources,$storeName,$wrapper)

return
    <result c="{empty($_currents)}" location="{concat($getf:strFhirServices,$_type,'/',$_id,'/_history/',$_version)}" type="{$_type}" id="{$_id}" version="{$_version}" lastupdated="{$_now}"/>
};

(:~ This strips off the namespace prefix (if any) before saving so you don't get mixed mode 
    elements. At least the sprinkler tool can't handle that and it gives more readable output 
    anyway 
:)
declare function adfhir:copyIntoNamespace($n as node()) as node() {
    if ($n instance of element()) then
        element {QName($n/namespace-uri(),$n/local-name())} {
            for $subat in $n/@*
            return attribute {$subat/local-name()} {$subat}
            ,
            for $subn in $n/node()
            return adfhir:copyIntoNamespace($subn)
        }
    else ($n)
};

(: Saving does PUT (update) and POST (create)
    creates new id as uuid based on max @id in $getf:strFhirResources for @type=$resource/local-name() e.g. ValueSet
    creates new version as xs:integer based on max current version of this resource
:)
declare function adfhir:saveResourceBundle($resourceBundle as element(),$mimeTypeReceived as xs:string?) as element(result)* {
(:if we are bulk loading official HL7 FHIR tables, then skip if-matches and assume that HL7 content is latest
    If HL7 says delete, then delete, if HL7 says create/update, just do it.:)
let $skipIfMatches  := $resourceBundle/f:base/@value='http://hl7.org/fhir'

for $entry in $resourceBundle/f:entry
let $status     :=
    if ($entry/f:deleted) then
        'delete'
    else if ($entry/f:status) then
        $entry/f:status/@value
    else ()
let $resource   := $entry/f:resource/f:*
return
    if ($status='delete') then (
        if ($skipIfMatches) then 
            adfhir:deleteResource($entry/f:deleted/f:type/@value, $entry/f:deleted/f:resourceId/@value, ())
        else (
            adfhir:deleteResource($entry/f:deleted/f:type/@value, $entry/f:deleted/f:resourceId/@value, $entry/f:deleted/f:versionId/@value)
        )
    )
    else (
        if ($skipIfMatches) then
            adfhir:saveResource($resource, $resource/f:id/@value, (), $mimeTypeReceived)
        else (
            adfhir:saveResource($resource, $resource/f:id/@value, $resource/f:meta/f:versionId/@value, $mimeTypeReceived)
        )
    )
};

(: Deleting moves current to history and creates new current empty under same id marked @deleted="" :)
declare function adfhir:deleteResource($_type as xs:string,$_id as xs:string,$if-matches as xs:string?) as element(result) {
let $storedResources    := collection($getf:strFhirResources)//entry[@type=$_type][@id=$_id]
let $_now               := current-dateTime()

return
    if (not(empty($if-matches)) and not($storedResources/@version=$if-matches)) then
        error(QName('http://art-decor.org/ns/fhir/error','ResourceNotCurrent'),concat('Resource version ''', $storedResources/@version, ''' did not match header ''', $getf:HEADER_IF_MATCH, ''': ', $if-matches))
    else if (empty($storedResources)) then
        error(QName('http://art-decor.org/ns/fhir/error','ResourceNotFound'),concat('Resource not found. type=',$_type,' id=',$_id))
    else if (count($storedResources)>1) then
        error(QName('http://art-decor.org/ns/fhir/error','ResourceAmbiguous'),concat('Resource occurred multiple times. Deletion ambiguous. type=',$_type,' id=',$_id))
    else if ($storedResources[@deleted]) then (
        <result d="" location="{concat($getf:strFhirServices,$_type,'/',$_id,'/_history/',$storedResources[1]/@version)}" type="{$_type}" id="{$_id}" version="{$storedResources[1]/@version}" lastupdated="{$storedResources[1]/@lastupdated}"/>
    ) else (
        let $moveOldCurrent     := adfhir:moveToHistory($storedResources)
        let $storeName          := concat(util:uuid(),'.xml')
        let $storeNewCurrent    := xmldb:store($getf:strFhirResources,$storeName,$storedResources)
        let $_currentContents   := doc($storeNewCurrent)/*
        let $_newVersion        :=
            if ($_currentContents/@version castable as xs:integer) then
                (xs:integer($_currentContents/@version) + 1)
            else (1)
        
        let $updateVersion      := update value $_currentContents/@version with $_newVersion
        let $updateDate         := update value $_currentContents/@lastupdated with current-dateTime()
        let $updateDeleted      := update insert attribute deleted {} into $_currentContents
        let $updateContents     := update delete $_currentContents/body/node()
        
        return
            <result d="true" location="{concat($getf:strFhirServices,$_type,'/',$_id,'/_history/',$_newVersion)}" type="{$_type}" id="{$_id}" version="{$_newVersion}" lastupdated="{$_now}"/>
    )
};

(:
GET [base]/_meta                                        get a list of all profiles, security labels, and 
                                                        tags used in the system
GET [base]/[type]/_meta                                 get a list of all profiles, security labels, and 
                                                        tags used for the nominated resource type
GET [base]/[type]/[id]/_meta                            get a list of all profiles, security labels, and 
                                                        tags affixed to the nominated resource. This is 
                                                        the same information included when reading the 
                                                        resource, but may have different security clearance
GET [base]/[type]/[id]/_history/[vid]/_meta             get a list of all profiles, security labels, and 
                                                        tags affixed to the nominated version of the 
                                                        resource. This is the same information included when 
                                                        reading the given version of the resource, but may 
                                                        have different security clearance
:)
declare function adfhir:getResourceMeta($_type as xs:string?,$_id as xs:string?,$_version as xs:string?) as element(f:meta) {
let $storedMeta         := adfhir:getResource($_type, $_id, $_version)[empty(@deleted)]/body/f:*/f:meta

return
    <f:meta>
    {
        for $t in $storedMeta/f:profile
        let $val := $t/@value
        group by $val
        return $t[1]
        ,
        for $t in $storedMeta/f:security
        let $sys := $t/f:system/@value
        let $val := $t/f:code/@value
        group by $sys, $val
        return $t[1]
        ,
        for $t in $storedMeta/f:tag
        let $sys := $t/f:system/@value
        let $val := $t/f:code/@value
        group by $sys, $val
        return $t[1]
    }
    </f:meta>
};

(:
POST [base]/[type]/[id]/_meta                           Affix the provided profiles, security labels, and 
                                                        tags in the list to the nominated resource
POST [base]/[type]/[id]/_history/[vid]/_meta            Affix the provided profiles, security labels, and 
                                                        tags in the list to the nominated version of the 
                                                        resource
:)
declare function adfhir:updateResourceMeta($metaElement as element(f:meta),$_type as xs:string,$_id as xs:string,$_version as xs:string?) as element(result) {
let $storedResources    := adfhir:getResource($_type, $_id, $_version)
let $_now               := current-dateTime()

let $if-matches         := adfhir:getIfMatches()
return
    if (not(empty($if-matches)) and not($storedResources/@version=$if-matches)) then
        error(QName('http://art-decor.org/ns/fhir/error','ResourceNotCurrent'),concat('Resource version ''', $storedResources/@version, ''' did not match header ''', $getf:HEADER_IF_MATCH, ''': ', $if-matches))
    else if (empty($storedResources)) then
        error(QName('http://art-decor.org/ns/fhir/error','ResourceNotFound'),concat('Resource not found. type=',$_type,' id=',$_id,''))
    else if (count($storedResources)>1) then
        error(QName('http://art-decor.org/ns/fhir/error','ResourceAmbiguous'),concat('Resource occurred multiple times. Update ambiguous. type=',$_type,' id=',$_id))
    else if (exists(adfhir:getResource($_type, $_id, ())[@deleted])) then
        error(QName('http://art-decor.org/ns/fhir/error','ResourceDeleted'),'Cannot update deleted resource')
    else (
        let $newmeta    :=
            <f:meta>
            {
                $storedResources/body/f:*/f:meta/(f:versionId|f:lastUpdated)
                ,
                $storedResources/body/f:*/f:meta/f:profile
                ,
                for $t in $metaElement/f:profile
                let $e   := $storedResources/body/f:*/f:meta/f:profile[@value=$t/@value]
                let $val := $t/@value
                group by $val
                return if (empty($e)) then $t[1] else ()
                ,
                $storedResources/body/f:*/f:meta/f:security
                ,
                for $t in $metaElement/f:security
                let $e   := $storedResources/body/f:*/f:meta/f:security[f:system/@value=$t/f:system/@value][f:code/@value=$t/f:code/@value]
                let $sys := $t/f:system/@value
                let $val := $t/f:code/@value
                group by $sys, $val
                return if (empty($e)) then $t[1] else ()
                ,
                $storedResources/body/f:*/f:meta/f:tag
                ,
                for $t in $metaElement/f:tag
                let $e   := $storedResources/body/f:*/f:meta/f:tag[f:system/@value=$t/f:tag/@value][f:code/@value=$t/f:code/@value]
                let $sys := $t/f:system/@value
                let $val := $t/f:code/@value
                group by $sys, $val
                return if (empty($e)) then $t[1] else ()
            }
            </f:meta>
        let $update     := 
            if (deep-equal($storedResources/body/f:*/f:meta, $newmeta)) then () else (
                update value $storedResources/@lastupdated with $_now
                ,
                update value $storedResources/f:*/f:meta/f:lastUpdated/@value with $_now
            )
        let $update     :=
            if ($storedResources/body/f:*/f:meta) then
                update replace $storedResources/body/f:*/f:meta with $newmeta
            else if ($storedResources/body/f:*/f:id) then
                update insert $newmeta following $storedResources/body/f:*/f:id
            else (
                update insert $newmeta preceding $storedResources/body/f:*/node()[1]
            )
        
        return
            <result location="{concat($getf:strFhirServices,$_type,'/',$_id,'/_history/',$storedResources/@version)}" type="{$_type}" id="{$_id}" version="{$storedResources/@version}" lastupdated="{$storedResources/@lastupdated}"/>
    )
};

(:
POST [base]/[type]/[id]/_meta/_delete                   Remove all profiles, security labels, and tags in 
                                                        the provided list from the meta for the nominated 
                                                        resource
POST [base]/[type]/[id]/_history/[vid]/_meta/_delete    Remove all profiles, security labels, and tags in 
                                                        the provided list from the list of tags for the 
                                                        nominated version of the resource:)
declare function adfhir:deleteResourceMeta($metaElement as element(f:meta),$_type as xs:string,$_id as xs:string,$_version as xs:string?) as element(result) {
let $storedResources    := adfhir:getResource($_type, $_id, $_version)
let $_now               := current-dateTime()

let $if-matches         := adfhir:getIfMatches()
return
    if (not(empty($if-matches)) and not($storedResources/@version=$if-matches)) then
        error(QName('http://art-decor.org/ns/fhir/error','ResourceNotCurrent'),concat('Resource version ''', $storedResources/@version, ''' did not match header ''', $getf:HEADER_IF_MATCH, ''': ', $if-matches))
    else if (empty($storedResources)) then
        error(QName('http://art-decor.org/ns/fhir/error','ResourceNotFound'),concat('Resource not found. type=',$_type,' id=',$_id,' version=',$_version))
    else if (count($storedResources)>1) then
        error(QName('http://art-decor.org/ns/fhir/error','ResourceAmbiguous'),concat('Resource occurred multiple times. Update ambiguous. type=',$_type,' id=',$_id,' version=',$_version))
    else if (exists(adfhir:getResource($_type, $_id, ())[@deleted])) then
        error(QName('http://art-decor.org/ns/fhir/error','ResourceDeleted'),'Cannot update deleted resource')
    else (
        let $update1    := 
            for $t in $metaElement/f:profile
            let $storedmeta := $storedResources/body/f:*/f:meta/f:profile[@value=$t/@value]
            let $u          := update delete $storedmeta
            return exists($storedmeta)
        let $update2    := 
            for $t in $metaElement/f:security
            let $storedmeta := $storedResources/body/f:*/f:meta/f:security[f:system/@value=$t/f:system/@value][f:code/@value=$t/f:code/@value]
            let $u          := update delete $storedmeta
            return exists($storedmeta)
        let $update3    := 
            for $t in $metaElement/f:tag
            let $storedmeta := $storedResources/body/f:*/f:meta/f:tag[f:system/@value=$t/f:system/@value][f:code/@value=$t/f:code/@value]
            let $u          := update delete $storedmeta
            return exists($storedmeta)
        let $update     := 
            if (($update1,$update2,$update3)=true()) then (
                update value $storedResources/@lastupdated with $_now
                ,
                update value $storedResources/f:*/f:meta/f:lastUpdated/@value with $_now
            ) else ()
        
        return
            <result location="{concat($getf:strFhirServices,$_type,'/',$_id,'/_history/',$storedResources/@version)}" type="{$_type}" id="{$_id}" version="{$storedResources/@version}" lastupdated="{$storedResources/@lastupdated}"/>
    )
};

(: Move to 
all resources names are uuid. Do we need protection against resource name collisions?:)
declare %private function adfhir:moveToHistory($storedResources as element()*) {
let $move   :=
    for $storedResource in $storedResources
    let $pathCurrent    := util:collection-name($storedResource)
    let $nameCurrent    := util:document-name($storedResource)
    return 
        if ($pathCurrent = $getf:strFhirHistory) then () else (
            let $nameHistory    :=
                (:if (xmldb:get-child-resources($getf:colFhirHistory)[.=$nameCurrent])
                then concat(util:uuid(),'.xml')
                else :)$nameCurrent
            return
            xmldb:move($pathCurrent, $getf:strFhirHistory, $nameHistory)
        )

return ()
};

(: **** SEARCH START **** :)
(: Note: heavy performance penalty on searching by date if baseDatatype in anything but instant :)
(: TODO: test getSearchForNumber :)
(: TODO: reference / composite / quantity
    reference	A reference to another resource.
    composite	A composite search parameter that combines a search on two values together.
    quantity	A search parameter that searches on a quantity.:)

(: number   Search parameter SHALL be a number (a whole number, or a decimal).
:)
declare function adfhir:getSearchByNumber($paramVal as xs:string*, $basePath as xs:string, $baseDatatype as xs:string) as xs:string* {
let $basePath := if ($baseDatatype=$getf:DT_CODEABLECONCEPT) then concat($basePath,'/f:coding') else ($basePath)

return
    if (empty($paramVal)) then ()
    else (
        for $p in $paramVal
        let $expr   :=
            (:XPath 3.0 Alert: tokenize on every comma, not preceded 
            by a backslash, without matching that char itself:)
            for $token in tokenize($p,'(!?:\\),')
            let $p1             := substring($token,1,1)
            let $p2             := substring($token,1,2)
            let $pcomp          := if ($p2=('&lt;=','>=','!=')) then $p2 else if ($p1=('&lt;','>')) then $p1 else ()
            let $prest          := if (empty($pcomp)) then $token else (substring($token,string-length($pcomp)+1))
            let $pcomp          := if (empty($pcomp)) then '=' else ($pcomp)
            return
                concat('xs:decimal(@value)',$pcomp,'xs:decimal(',$prest,')')
        return
            concat('[',$basePath,'[',string-join($expr,' or '),']]')
    )
};

(: date     Search parameter is on a date/time. The date format is the standard 
            XML format, though other formats may be supported.

Date comparisons are tricky. Both operands may be incomplete dates, with or without timezone and both could be BC (Before Christ)

If both dates are castable as dateTime(), and for the saved resource that means datatype Instant, then comparison is between dates, but 
if either (potentially) is not a dateTime(), then the comparison will switch to numeric where input operand is trimmed/zero-padded to 
the length of the parameter.

So comparison will be (op = operator)
    xs:dateTime(input) op xs:dateTime(param)
or
    2015 op 2015
    201500 op 201501
    etc.
:)
declare function adfhir:getSearchByDate($paramVal as xs:string*, $basePath as xs:string, $baseDatatype as xs:string) as xs:string* {
let $basePath       := if ($baseDatatype=$getf:DT_CODEABLECONCEPT) then concat($basePath,'/f:coding') else ($basePath)

return
    if (empty($paramVal)) then ()
    else (
        for $p in $paramVal
        let $expr   :=
            (:XPath 3.0 Alert: tokenize on every comma, not preceded 
            by a backslash, without matching that char itself:)
            for $token in tokenize($p,'(!?:\\),')
            let $p1             := substring($token,1,1)
            let $p2             := substring($token,1,2)
            let $pcomp          := if ($p2=('&lt;=','>=','!=')) then $p2 else if ($p1=('&lt;','>')) then $p1 else ()
            let $pdate          := if (empty($pcomp)) then $token else (substring($token,string-length($pcomp)+1))
            let $pcomp          := if (empty($pcomp)) then '=' else ($pcomp)
            let $pdatenotz      := if ($pdate castable as xs:dateTime) then 
                                        if (starts-with($pdate,'-')) then 
                                            concat('-',substring(format-dateTime(xs:dateTime($pdate),'[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]','en',(),()),1,19))
                                        else
                                            substring(format-dateTime(xs:dateTime($pdate),'[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]','en',(),()),1,19)
                                   else if (starts-with($pdate,'-')) then (
                                        substring($pdate,1,20)
                                   )
                                   else (
                                        substring($pdate,1,19)
                                   )
            let $pdatenum        := replace($pdatenotz,'(\d)[^\d]','$1')
            let $pdatelength    := if (starts-with($pdatenum,'-')) then string-length(substring($pdatenum,2)) else string-length($pdatenum)
            
            return
                if ($baseDatatype=$getf:DT_INSTANT and $pdate castable as xs:dateTime) then (
                    concat('xs:dateTime(@value)',$pcomp,'xs:dateTime(''',$pdate,''')')
                ) else (
                    concat('number(substring(concat(replace(@value,''(\d)[^\d]'',''$1''),''00000000000000''),1,',$pdatelength,')) ', $pcomp, ' number(',$pdatenum,')')
                )
        return
            concat('[',$basePath,'[',string-join($expr,' or '),']]')
    )
};

(: string   Search parameter is a simple string, like a name part. Search is 
            case-insensitive and accent-insensitive. May match just the start 
            of a string. String parameters may contain spaces.:)
declare function adfhir:searchByString($resources as element()*, $paramVal as xs:string*, $basePath as xs:string) as element()* {
if (empty($paramVal)) then ($resources) else (
    let $r  :=
        util:eval(concat(
            '$resources[',
            $basePath,
            '[ft:query(.,adfhir:getSimpleLuceneQuery(tokenize(normalize-space($paramVal[1]),''\s'')))]]'
        ))
    return
    adfhir:searchByString($r, $paramVal[position()>1], $basePath)
)
};

(: string:exact   (the match needs to be exact, no partial matches, case 
            sensitive and accent-sensitive), instead of the default behavior, 
            which is that the search does partial matches. It is at the discretion 
            of the server whether to do a left-partial search
:)
declare function adfhir:getSearchByStringExact($paramVal as xs:string*, $basePath as xs:string, $baseDatatype as xs:string, $not as xs:boolean) as xs:string* {
let $basePath    := if ($baseDatatype=$getf:DT_CODEABLECONCEPT) then concat($basePath,'/f:coding') else ($basePath)

return
    if (empty($paramVal)) then () else (
        for $p in $paramVal
        let $expr   :=
            (:XPath 3.0 Alert: tokenize on every comma, not preceded 
            by a backslash, without matching that char itself:)
            for $token in tokenize($p,'(!?:\\),')
            let $t  := replace($token,'''','''''')
            return 
                concat('''',$t,'''')
        return
            if ($not) then 
                concat('[not(',$basePath,'/@value=(',string-join($expr,','),'))]')
            else (
                concat('[',$basePath,'/@value=(',string-join($expr,','),')]')
            )
    )
};

(: token/token:not    Search parameter on a coded element or identifier. May be used to 
            search through the text, displayname, code and code/codesystem (for 
            codes) and label, system and key (for identifier). Its value is either 
            a string or a pair of namespace and value, separated by a "|", 
            depending on the modifier used.
:)
declare function adfhir:getSearchByToken($paramVal as xs:string*, $basePath as xs:string, $baseDatatype as xs:string, $not as xs:boolean) as xs:string* {
let $basePath := if ($baseDatatype=$getf:DT_CODEABLECONCEPT) then concat($basePath,'/f:coding') else ($basePath)

return
    if (empty($paramVal)) then ()
    else if ($baseDatatype=($getf:DT_CODING,$getf:DT_CODEABLECONCEPT)) then (
        for $p in $paramVal 
        let $expr   :=
            (:XPath 3.0 Alert: tokenize on every comma, not preceded 
            by a backslash, without matching that char itself:)
            for $token in tokenize($p,'(!?:\\),')
            let $syscode    := tokenize($token,'\|')
            let $system     := if (count($syscode)=1) then () else (replace($syscode[1],'''',''''''))
            let $code       := replace($syscode[last()],'''','''''')
            return
                if (empty($system)) 
                then (concat('(f:code[@value=''',$code,'''])'))
                else if ($system='')
                then (concat('(not(f:system) and f:code[@value=''',$code,'''])'))
                else (concat('(f:system[@value=''',$system,'''] and f:code[@value=''',$code,'''])'))
        return 
            if ($not) then 
                concat('[not(',$basePath,'[',string-join($expr,' or '),'])]')
            else (
                concat('[',$basePath,'[',string-join($expr,' or '),']]')
            )
    )
    else if ($baseDatatype=$getf:DT_PRIMITVES) then (
        adfhir:getSearchByStringExact($paramVal, $basePath, $baseDatatype, $not)
    )
    else (
        error(QName('http://art-decor.org/ns/fhir/error', 'UnknownDatatype'),concat('Datatype ',$baseDatatype,' for element unknown. Cannot do token search.'))
    )
};

(: uri/uri:above/uri:below  The uri parameter refers to an element which is URI (RFC 3986). 
    Matches are precise (e.g. case, accent, and escape) sensitive, and the entire URI must 
    match. The modifier :above or :below can be used to indicate that the match is based on 
    left. For example:
    
    GET [base]/ValueSet?url=http://acme.org/fhir/ValueSet/123
    GET [base]/ValueSet?url:below=http://acme.org/fhir/
    
    The first is a request to find any value set with the exact url "http://acme.org/fhir/ValueSet/123". 
    The second search will return any value sets that have a URL that starts with "http://acme.org/fhir/". 
    The converse - the search for any value set above a given specific URL - may be useful for searching 
    name systems, but it is generally less useful than the :below search.
:)
declare function adfhir:getSearchByURI($paramVal as xs:string*, $basePath as xs:string, $baseDatatype as xs:string, $above as xs:boolean, $below as xs:boolean) as xs:string* {
    () (:TODO:)
};

(: :missing E.g. gender:missing=true (or false). Searching for "gender:missing=true" 
            will return all the resources that don't have any value for the gender 
            parameter (which usually equates to not having the relevant element in the 
            resource). Searching for "gender:missing=false" will return all the resources 
            that have a value for the "gender" parameter.
:)
declare function adfhir:getSearchForMissing($paramVal as xs:string*, $basePath as xs:string) as xs:string* {
if (empty($paramVal)) then () else (
    if ($paramVal='false') then
        concat('[',$basePath,']')
    else if ($paramVal='true') then
        concat('[not(',$basePath,')]')
    else ()
)
};


(: string:exact   (the match needs to be exact, no partial matches, case 
            sensitive and accent-sensitive), instead of the default behavior, 
            which is that the search does partial matches. It is at the discretion 
            of the server whether to do a left-partial search
:)
declare function adfhir:getSearchDecorByStringExact($paramVal as xs:string*, $basePath as xs:string, $baseDatatype as xs:string, $not as xs:boolean) as xs:string* {
    if (empty($paramVal)) then () else (
        for $p in $paramVal
        let $expr   :=
            (:XPath 3.0 Alert: tokenize on every comma, not preceded 
            by a backslash, without matching that char itself:)
            for $token in tokenize($p,'(!?:\\),')
            let $t  := replace($token,'''','''''')
            return 
                concat('''',$t,'''')
        return
            if ($not) then 
                concat('[not(',$basePath,'=(',string-join($expr,','),'))]')
            else (
                concat('[',$basePath,'=(',string-join($expr,','),')]')
            )
    )
};

(: token/token:not    Search parameter on a coded element or identifier. May be used to 
            search through the text, displayname, code and code/codesystem (for 
            codes) and label, system and key (for identifier). Its value is either 
            a string or a pair of namespace and value, separated by a "|", 
            depending on the modifier used.
:)
declare function adfhir:getSearchDecorByToken($paramVal as xs:string*, $basePath as xs:string, $baseDatatype as xs:string, $not as xs:boolean) as xs:string* {
    if (empty($paramVal)) then ()
    else if ($baseDatatype=($getf:DT_CODING,$getf:DT_CODEABLECONCEPT)) then (
        for $p in $paramVal 
        let $expr   :=
            (:XPath 3.0 Alert: tokenize on every comma, not preceded 
            by a backslash, without matching that char itself:)
            for $token in tokenize($p,'(!?:\\),')
            let $syscode    := tokenize($token,'\|')
            let $system     := if (count($syscode)=1) then () else (replace($syscode[1],'''',''''''))
            let $code       := replace($syscode[last()],'''','''''')
            return
                if (empty($system)) 
                then (concat('(.[@code=''',$code,'''])'))
                else if ($system='')
                then (concat('(not(f:system) and f:code[@value=''',$code,'''])'))
                else (concat('(.[@code=''',$code,'''][@codeSystem=''',$system,'''])'))
        return 
            if ($not) then 
                concat('[not(',$basePath,'[',string-join($expr,' or '),'])]')
            else (
                concat('[',$basePath,'[',string-join($expr,' or '),']]')
            )
    )
    else if ($baseDatatype=$getf:DT_PRIMITVES) then (
        adfhir:getSearchDecorByStringExact($paramVal, $basePath, $baseDatatype, $not)
    )
    else (
        error(QName('http://art-decor.org/ns/fhir/error', 'UnknownDatatype'),concat('Datatype ',$baseDatatype,' for element unknown. Cannot do token search.'))
    )
};

(: :missing E.g. gender:missing=true (or false). Searching for "gender:missing=true" 
            will return all the resources that don't have any value for the gender 
            parameter (which usually equates to not having the relevant element in the 
            resource). Searching for "gender:missing=false" will return all the resources 
            that have a value for the "gender" parameter.
:)
declare function adfhir:getSearchDecorForMissing($paramVal as xs:string*, $basePath as xs:string) as xs:string* {
if (empty($paramVal)) then () else (
    if ($paramVal='false') then
        concat('[',$basePath,']')
    else if ($paramVal='true') then
        concat('[not(',$basePath,')]')
    else ()
)
};

(:~ A normal version number MUST take the form X.Y.Z where X, Y, and Z are non-negative integers, and MUST NOT contain 
    leading zeroes. X is the major version, Y is the minor version, and Z is the patch version. Each element MUST 
    increase numerically. For instance: 1.9.0 -> 1.10.0 -> 1.11.0.
    https://semver.org :)
declare function adfhir:isSemverString($versionlabel as xs:string?) as xs:boolean {
    if (empty($versionlabel)) then false() else matches($versionlabel, '^\d+\.\d+\.\d+(-[A-Za-z\d+\-])?$')
};

declare function adfhir:getSemverString($versionlabel as xs:string?) as xs:string? {
    if (adfhir:isSemverString($versionlabel)) then ($versionlabel) else
    if (adfhir:isSemverString(concat($versionlabel, '.0'))) then (concat($versionlabel, '.0')) else
    if (adfhir:isSemverString(substring($versionlabel, 2))) then substring($versionlabel, 2) else
    if (adfhir:isSemverString(concat(substring($versionlabel, 2), '.0'))) then concat(substring($versionlabel, 2), '.0')
    else ()
};

(: **** SEARCH END **** :)

(:  <OperationOutcome xmlns="http://hl7.org/fhir">
        <issue>                             <!-- 1..* A single issue associated with the action -->
            <severity value="[code]"/>      <!-- 1..1 fatal | error | warning | information -->
            <type>                          <!-- 0..1 Coding Error or warning code --></type>
            <details value="[string]"/>     <!-- 0..1 Additional description of the issue -->
            <location value="[string]"/>    <!-- 0..* XPath of element(s) related to issue -->
        </issue>
    </OperationOutcome>
:)
declare function adfhir:operationOutCome($severity as xs:string, $type as element(type)?, $details as xs:string?, $location as xs:string?) as element() {
    adfhir:operationOutCome($severity, $type, (), $details, (), $location)
};
declare function adfhir:operationOutCome($severity as xs:string, $type as element(type)?, $detailcode as xs:string?, $details as xs:string?, $diagnostics as xs:string?, $location as xs:string?) as element() {
    <OperationOutcome xmlns="http://hl7.org/fhir">
        {comment {'<meta><profile value="http://hl7.org/fhir/' || $getf:strFhirVersionShort || '/StructureDefinition/OperationOutcome"/></meta>'}}
        <issue xmlns="http://hl7.org/fhir">
            <severity value="{$severity}"/>
            {$type}
            {if (empty($details) and empty($detailcode)) then () else (
                <details>
                {
                    if (empty($detailcode)) then
                        <text value="{$details}"/>
                    else (
                        <coding>
                            <system value="http://hl7.org/fhir/operation-outcome"/>
                            <code value="{$detailcode}"/>
                            <display value="{$details}"/>
                        </coding>
                    )
                }
                </details>
            )}
            {if (empty($diagnostics)) then () else (<diagnostics value="{$diagnostics}"/>)}
            {if (empty($location)) then () else (<location value="{$location}"/>)}
        </issue>
    </OperationOutcome>
};
declare function adfhir:operationOutCome($issues as element(issue)+) as element() {
    <OperationOutcome xmlns="http://hl7.org/fhir">
        {comment {'<meta><profile value="http://hl7.org/fhir/' || $getf:strFhirVersionShort || '/StructureDefinition/OperationOutcome"/></meta>'}}
    {
        for $issue in $issues
        return
        <issue xmlns="http://hl7.org/fhir">
            <severity value="{$issue/@role}"/>
            <details><text value="{$issue/*:description}"/></details>
            <location value="{string-join($issue/*:location/@*,':')}"/>
        </issue>
    }
    </OperationOutcome>
};

(:
    URL patterns
    read                    GET     [base]/[type]/[id]                              {?_format=[mime-type]}
    vread                   GET     [base]/[type]/[id]/_history/[vid]               {?_format=[mime-type]}
    update                  PUT     [base]/[type]/[id]                              {?_format=[mime-type]}
    delete                  DELETE  [base]/[type]/[id]                              Deletion means creating a new version marked as deleted. This effectively deletes the resource by that id
    create                  POST    [base]/[type]                                   {?_format=[mime-type]}
    search          
                all resource types
                            GET     [base]?[parameters]                             {&_format=[mime-type]}
                single resource type
                            GET     [base]/[type]?[parameters]                      {&_format=[mime-type]}
                            GET     [base]/[type]/_search?[parameters]              {&_format=[mime-type]}
                compartment
                            GET     [base]/[compartment]/[id]/?[parameters]         {&_format=[mime-type]}
                            GET     [base]/[compartment]/[id]/[type]?[parameters]   {&_format=[mime-type]}
    validate                POST    [base]/[type]/_validate{/[id]}
    capabilities            GET     [base]/metadata                                 {?_format=[mime-type]}
                            OPTIONS [base]                                          {?_format=[mime-type]}
    history                 GET     [base]/[type]/[id]/_history                     {?[parameters]&_format=[mime-type]}
                            GET     [base]/[type]/_history                          {?[parameters]&_format=[mime-type]}
                            GET     [base]/_history                                 {?[parameters]&_format=[mime-type]}
                    
    === UNSUPPORTED IN ART-DECOR ===
    transaction             POST    [base]                                          {?_format=[mime-type]}
    tag operations          GET     [base]/_tags                                    get a list of all tags
                            GET     [base]/[type]/_tags                             get a list of all tags used for the nominated resource type
                            GET     [base]/[type]/[id]/_tags                        get a list of all tags affixed to the nominated resource. This duplicates the HTTP header entries
                            GET     [base]/[type]/[id]/_history/[vid]/_tags         get a list of all tags affixed to the nominated version of the resource. This duplicates the HTTP header entries
                            POST    [base]/[type]/[id]/_tags                        Affix tags in the list to the nominated resource
                            POST    [base]/[type]/[id]/_history/[vid]/_tags         Affix tags in the list to the nominated version of the resource
                            POST    [base]/[type]/[id]/_tags/_delete                Remove all tags in the provided list from the list of tags for the nominated resource
                            POST    [base]/[type]/[id]/_history/[vid]/_tags/_delete Remove tags in the provided list from the list of tags for the nominated version of the resource
:)
declare function adfhir:getResourceFromPath($path as xs:string?) as xs:string? {
    let $tok    := tokenize($path,'/')
    return 
    if (empty($path) or $path=('/','')) then
        ()
    else if (starts-with($tok[2],'_') or $tok[2]='metadata') then
        ()
    else (
        $tok[2]
    )
};

(:
Instance Level Interactions	
read            Read the current state of the resource
vread           Read the state of a specific version of the resource
update          Update an existing resource by its id (or create it if it is new)
delete          Delete a resource
history         Retrieve the update history for a particular resource
Type Level Interactions
create          Create a new resource with a server assigned id
search          Search the resource type based on some filter criteria
history         Retrieve the update history for a particular resource type
Whole System Interactions
capabilities    Get a capabilities statement for the system
transaction     Update, create or delete a set of resources as a single transaction
history         Retrieve the update history for all resources
search          Search across all resource types based on some filter criteria

Include these codes as defined in http://hl7.org/fhir/restful-interaction
Code                    Display
read                    read                Read the current state of the resource
vread                   vread               Read the state of a specific version of the resource
update                  update              Update an existing resource by its id (or create it if it is new)
delete                  delete              Delete a resource
validate                validate            Check that the content would be acceptable as an update
create                  create              Create a new resource with a server assigned id
history-instance        history-instance    Retrieve the update history for a particular resource
history-type            history-type        Retrieve the update history for a all resources of a particular type
history-system          history-system      Retrieve the update history for all resources on a system
search-type             search-type         Search all resources of the specified type based on some filter criteria
search-system           search-system       Search all resources based on some filter criteria
transaction             transaction         Update, create or delete a set of resources as a single transaction

capabilities            capabilities        Read a capability statement for the server
create-meta             create-meta         Add meta to existing resource
read-meta               read-meta           Read the meta from current state of the resource
vread-meta              vread-meta          Read the meta from state of a specific version of the resource
delete-meta             delete-meta         Delete meta from an existing resource by its id (or create meta if it is new)

:)
declare function adfhir:getRequestParts($method as xs:string, $path as xs:string, $query as xs:string?, $hasQueryString as xs:boolean) as element(parts) {
    (:initialize what we are populating hereafter:)
    let $decorPrefix        := ()
    let $decorVersion       := ()
    let $resourceName       := ()
    let $resourceId         := ()
    let $resourceVersion    := ()
    (: Max: _history _meta _delete :)
    let $operation1         := ()
    let $operation2         := ()
    let $operation3         := ()
    let $compartment        := ()
    let $logicalOperation   := ()
    let $crudOperation      := ()
    
    let $tok                := tokenize($path,'/')
    
    (:start populating:)
    
    (: options:
        'public'                -- resource from anywhere
        'demo1-'                -- resource from demo1-
        'demo1-20160101123456'  -- resource from demo1- release 2016-01-01T12:34:56
        'hl7org'                -- NamingSystem from specific registry
    :)
    let $projectCompartment := $tok[2]
    let $decorVersion       := 
        if ($projectCompartment = 'public') then () else 
        if (matches($projectCompartment, '[^\d]\d{14}$')) then (replace($projectCompartment, '.+(\d{14})$', '$1')) else
        if (matches($projectCompartment, 'development$')) then (replace($projectCompartment, '.+(development)$', '$1'))
        else ('')
    let $decorPrefix        := if ($projectCompartment = 'public') then () else if ($decorVersion = '') then $projectCompartment else (replace($projectCompartment, $decorVersion, ''))
    let $decorVersion       := if (matches($decorVersion, '^\d{14}$')) then (replace($decorVersion, '(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})', '$1-$2-$3T$4:$5:$6')) else ($decorVersion[string-length() gt 0])
    let $resourceName       := 
        if (string-length($tok[3])=0) then () else (
            if ($tok[3]=$getf:PARAM_METADATA) then ('CapabilityStatement') else if (starts-with($tok[3],('_','$'))) then () else ($tok[3])
        )
    (:resourceId is reevaluated after populating logicalOperation:)
    let $resourceId         := 
        if (string-length($resourceName)=0) then () else (
            if (starts-with($tok[4],('_','$'))) then () else if (string-length($tok[4]) = 0) then () else ($tok[4])
        )
    let $resourceVersion    :=
        if (empty($resourceId) or not($tok[5]=$getf:PARAM_HISTORY)) then () else (
            if (starts-with($tok[6],('_','$'))) then () else ($tok[6])
        )
    let $operation1         := if (  $tok[3]=$getf:PARAM_METADATA  ) then $getf:PARAM_METADATA  else ($tok[starts-with(.,('_','$'))])[1]
    let $operation2         := if ($operation1=$getf:PARAM_METADATA) then ()                    else ($tok[starts-with(.,('_','$'))])[2]
    let $operation3         := if ($operation1=$getf:PARAM_METADATA) then ()                    else ($tok[starts-with(.,('_','$'))])[3]
    let $operations         := ($operation1,$operation2,$operation3)
    let $compartment        := 
        if (empty($resourceId) or starts-with($tok[5],('_','$'))) then () else (
            $tok[5]
        )
    
    let $logicalOperation   :=
        
        (: OPTIONS :)
        if ($method=$getf:HEADER_METHOD_OPTIONS)
        then $getf:RFINT_CAPABILITIES
        
        (: GET (that works as OPTIONS would) :)
        else if ($method=$getf:HEADER_METHOD_GET and $operation1=($getf:PARAM_METADATA,$getf:PARAM__METADATA))
        then $getf:RFINT_CAPABILITIES
        
        (: GET (not FHIR, just main index page) :)
        else if ($method=$getf:HEADER_METHOD_GET and $path=('/','') and not($hasQueryString))
        then $getf:RFINT_INDEX
        
        (: GET vread-meta, read-meta, vread, history-instance, history-type, history-system, search-type, search-system, read :)
        else if ($method=$getf:HEADER_METHOD_GET)
        then (
            if ($operations=$getf:PARAM_META and not(empty($resourceVersion)))
            then $getf:RFINT_VREAD_META
            else if ($operations=$getf:PARAM_META)
            then $getf:RFINT_READ_META
            else if ($operations=$getf:PARAM_HISTORY and not(empty($resourceVersion)))
            then $getf:RFINT_VREAD
            else if ($operations=$getf:PARAM_HISTORY and not(empty($resourceId)))
            then $getf:RFINT_HISTORY_INSTANCE
            else if ($operations=$getf:PARAM_HISTORY and not(empty($resourceName)))
            then $getf:RFINT_HISTORY_TYPE
            else if ($operations=$getf:PARAM_HISTORY)
            then $getf:RFINT_HISTORY_SYSTEM
            else if ($operations=$getf:PARAM_SEARCH or $hasQueryString(: or starts-with($operation1,'$'):))
            then (
                if (empty($resourceName))
                then $getf:RFINT_SEARCH_SYSTEM
                else $getf:RFINT_SEARCH_TYPE
            )
            else if (not(empty($resourceName)))
            then (
                if (empty($resourceId))
                then $getf:RFINT_SEARCH_TYPE
                else $getf:RFINT_READ
            )
            else ($getf:RFINT_READ)
        )
        
        (: POST search-type, search-system, delete-meta, create-meta, create :)
        else if ($method=$getf:HEADER_METHOD_POST)
        then (
            if ($operations=$getf:PARAM_SEARCH or 
                starts-with($operation1,'$') or 
                starts-with($operation2,'$') or 
                starts-with($operation3,'$'))
            then (
                if (empty($resourceName))
                then $getf:RFINT_SEARCH_SYSTEM
                else $getf:RFINT_SEARCH_TYPE
            )
            else if ($operations=$getf:PARAM_META and $operations=$getf:PARAM_DELETE)
            then $getf:RFINT_DELETE_META
            else if ($operations=$getf:PARAM_META)
            then $getf:RFINT_CREATE_META
            else if (not(empty($resourceName)))
            then $getf:RFINT_CREATE
            else $getf:RFINT_TRANSACTION
        )
        
        (: PUT update :)
        else if ($method=$getf:HEADER_METHOD_PUT)
        then (
            if (not(empty($resourceName)))
            then $getf:RFINT_UPDATE
            else ()
        )
        
        (: DELETE delete :)
        else if ($method=$getf:HEADER_METHOD_DELETE)
        then (
            if (not(empty($resourceId)))
            then $getf:RFINT_DELETE
            else ()
        )
        else ()
    
    let $crudOperation      :=
        if ($logicalOperation=$getf:RFINT_DELETE) 
        then 'D'
        else if ($logicalOperation=($getf:RFINT_UPDATE,$getf:RFINT_CREATE_META,$getf:RFINT_UPDATE_META, $getf:RFINT_DELETE_META))
        then 'U'
        else if ($logicalOperation=($getf:RFINT_CREATE,$getf:RFINT_TRANSACTION)) 
        then 'C'
        else 'R'
    
    let $resourceId         := if ($logicalOperation=$getf:RFINT_CAPABILITIES) then ($getf:strCapabilityStatementId) else ($resourceId)
    
    let $check              := 
        if (empty($logicalOperation)) then 
            error(QName('http://art-decor.org/ns/fhir/error','InvalidRequestException'),concat('Could not determine the logical operation from the request method/path: ',$method, ' ', string-join(($path,$query),'?')))
        else ()
    
    return
        <parts>
        {
            if (empty($decorPrefix)     ) then () else (attribute prefix    {$decorPrefix}),
            if (empty($decorVersion)    ) then () else (attribute version   {$decorVersion}),
            if (empty($resourceName)    ) then () else (attribute rname     {$resourceName}),
            if (empty($resourceId)      ) then () else (attribute rid       {$resourceId}),
            if (empty($resourceVersion) ) then () else (attribute rvers     {$resourceVersion}),
            if (empty($operation1)      ) then () else (attribute oper1     {$operation1}),
            if (empty($operation2)      ) then () else (attribute oper2     {$operation2}),
            if (empty($operation3)      ) then () else (attribute oper3     {$operation3}),
            if (empty($compartment)     ) then () else (attribute comp      {$compartment}),
            if (empty($logicalOperation)) then () else (attribute logop     {$logicalOperation}),
            if (empty($crudOperation)   ) then () else (attribute crudop    {$crudOperation})
        }
        </parts>
};

(:declare function adfhir:getIdFromPath($path as xs:string?) as xs:string? {
    let $res    := adfhir:getResourceFromPath($path)
    let $tok    := tokenize($path,'/')
    return 
    if (empty($res)) then
        ()
    else (
        let $idpos  := index-of($tok,$res)[1] + 1
        return
        if (starts-with($tok[$idpos],'_') or $tok[$idpos]='metadata') then
            ()
        else (
            $tok[$idpos]
        )
    )
};

declare function adfhir:getVersionFromPath($path as xs:string?) as xs:string? {
    let $res    := adfhir:getResourceFromPath($path)
    let $oper   := '_history'
    let $tok    := tokenize($path,'/')
    return 
    if (empty($res) or not($tok[.='_history'])) then
        ()
    else (
        let $vspos  := index-of($tok,$oper)[1] + 1
        return
        $tok[$vspos]
    )
};:)

declare function adfhir:canGzipResponse() as xs:boolean {
    if (request:exists()) then 
        tokenize(request:get-header($getf:HEADER_ACCEPT_ENCODING),'\s*,\s*')=$getf:ENCODING_GZIP
    else 
        false()
};

declare function adfhir:canAcceptFhirXml() as xs:boolean {
    if (request:exists()) then
        tokenize(request:get-header($getf:HEADER_ACCEPT),'\s*,\s*')=$getf:CT_FHIR_XML
    else (
        true()
    )
};

declare function adfhir:canAcceptFhirJson() as xs:boolean {
    if (request:exists()) then
        tokenize(request:get-header($getf:HEADER_ACCEPT),'\s*,\s*')=$getf:CT_FHIR_JSON
    else (
        true()
    )
};

declare function adfhir:getIfMatches() as xs:string? {
    if (request:exists()) then
        request:get-header($getf:HEADER_IF_MATCH)[string-length()>0]
    else ()
};

(:~
:   Returns lucene config xml for a sequence of strings. The search will find yield results that match all terms+trailing wildcard in the sequence
:   Example output:
:   <query>
:       <bool>
:           <wildcard occur="must">term1*</wildcard>
:           <wildcard occur="must">term2*</wildcard>
:       </bool>
:   </query>
:
:   @param $searchTerms required sequence of terms to look for
:   @return lucene config
:   @since 2014-06-06
:)
declare function adfhir:getSimpleLuceneQuery($searchTerms as xs:string+) as element() {
    adfhir:getSimpleLuceneQuery($searchTerms, 'wildcard')
};

(:~
:   http://exist-db.org/exist/apps/doc/lucene.xml?q=lucene&field=all&id=D2.2.4.28#D2.2.4.28
:)
declare function adfhir:getSimpleLuceneQuery($searchTerms as xs:string+, $searchType as xs:string) as element() {
    <query>
        <bool>
        {
            for $term in $searchTerms
            return
                if ($searchType='fuzzy') then
                    <fuzzy occur="must">{concat($term,'~')}</fuzzy>
                
                else if ($searchType='fuzzy-not') then
                    <fuzzy occur="not">{concat($term,'~')}</fuzzy>
                    
                else if ($searchType='regex') then
                    <regex occur="must">{$term}</regex>
                    
                else if ($searchType='regex-not') then
                    <regex occur="not">{$term}</regex>
                    
                else if ($searchType='phrase') then
                    <phrase occur="must">{$term}</phrase>
                    
                else if ($searchType='phrase-not') then
                    <phrase occur="not">{$term}</phrase>
                    
                else if ($searchType='term') then
                    <term occur="must">{$term}</term>
                    
                else if ($searchType='term-not') then
                    <term occur="not">{$term}</term>
                    
                else if ($searchType='wildcard') then
                    <wildcard occur="must">{concat($term,'*')}</wildcard>
                    
                else if ($searchType='wildcard-not') then
                    <wildcard occur="not">{concat($term,'*')}</wildcard>
                    
                else ()
        }
        </bool>
    </query>
};

(:~
:   Returns lucene options xml that instruct filter-rewrite=yes
:)
declare function adfhir:getSimpleLuceneOptions() as element() {
    <options>
        <default-operator>and</default-operator>
        <phrase-slop>0</phrase-slop>
        <leading-wildcard>no</leading-wildcard>
        <filter-rewrite>yes</filter-rewrite>
    </options>
};

(: **** VALIDATION **** :)
declare function adfhir:validate($resource as element()) as element(issue)* {
(:    let $schemaFile     := adfhir:getSchemaForResource($resource)
    let $schematronFile := adfhir:getSchematronForResource($resource):)
    
    (: validate xsd :)
    let $schemaFile := concat($getf:strFhirXsdSch,'/fhir-single.xsd')
    
    let $schemaFile :=
        if (doc-available($schemaFile)) then $schemaFile
        else (error(QName('http://art-decor.org/ns/error', 'SchemaFileMissing'), concat('XML Schema file missing: ',$schemaFile)))
    
    let $schemaReport   := validation:jaxv-report($resource, doc($schemaFile))
    let $xsdIssues      := 
        for $schemaIssue in $schemaReport//*[@level='Warning' or @level='Error']
        return
            <issue type="schema" role="{lower-case($schemaIssue/@level)}" count="{if ($schemaIssue/@repeat) then $schemaIssue/@repeat else ('1')}">
                <description>{$schemaIssue/text()}</description>
                <location line="{$schemaIssue/@line}" column="{$schemaIssue/@column}"/>
            </issue>
            
    (: validate schematron :)
    let $schematronFile := concat($getf:strFhirXsdSch,'/fhir-invariants.xsl')
    
    let $schematronFile :=
        if (doc-available($schematronFile)) then $schematronFile
        else (error(QName('http://art-decor.org/ns/error', 'SchematronFileMissing'), concat('XML Schematron file missing: ',$schematronFile)))
        
    let $schResult      := transform:transform($resource, doc($schematronFile), ())
    let $schIssues      :=
        for $schematronIssue  in $schResult//svrl:failed-assert | $schResult//svrl:successful-report
        let $role   := if ($schematronIssue/@role) then $schematronIssue/@role else ('error')
        return
            <issue type="schematron" role="{$role}" count="1">
                {$schematronIssue/(@* except (@type|@role|@id|@location))}
                <description>{$schematronIssue/svrl:text/text()}</description>
                <location path="{$schematronIssue/@location}"/>
            </issue>
    
    return
        ($xsdIssues|$schIssues)
};
declare function adfhir:getSchemaForResource($resource as element()) as xs:string {
    let $f := concat($getf:strFhirXsdSch,'/',lower-case($resource/local-name()),'.xsd')
    
    return
    if (doc-available($f)) then 
        $f
    else (
        error(QName('http://art-decor.org/ns/error', 'SchemaFileMissing'), concat('XML Schema file missing: ',$f))
    )
};
declare function adfhir:getSchematronForResource($resource as element()) as xs:string {
    let $f := concat($getf:strFhirXsdSch,'/',lower-case($resource/local-name()),'.xsl')
    
    return
    if (doc-available($f)) then 
        $f
    else (
        error(QName('http://art-decor.org/ns/error', 'SchematronFileMissing'), concat('XML Schematron file missing: ',$f))
    )
};

declare function adfhir:getRequestBody() as item()* {
    let $resource       := 
        if (request:is-multipart-content()) then (
            let $file   := request:get-uploaded-file-data('file')
            return if (empty($file)) then () else (util:base64-decode($file))
        )
        else ( 
            request:get-data()
        )

    let $resource       :=
        if ($resource instance of element()) then (
            (:exactly what we want:)
            $resource
        )
        else if ($resource instance of document-node()) then (
            $resource/*
        )
        else (
            (:Hack alert: upload fails when content has UTF-8 Byte Order Marker. 
            the UTF-8 representation of the BOM is the byte sequence 0xEF,0xBB,0xBF:)
            if (empty($resource))
            then ()
            else if (string-to-codepoints(substring($resource,1,1))=65279) 
            then (fn:parse-xml(substring($resource,2))/f:*)
            else (fn:parse-xml($resource)/f:*)
        )
    
    return $resource
};

(:https://www.w3.org/Protocols/rfc2616/rfc2616-sec3.html#sec3.3.1
    HTTP-date is required for headers like Last-Modified
    
    Valid examples:
      Sun, 06 Nov 1994 08:49:37 GMT  ; RFC 822, updated by RFC 1123
      Sunday, 06-Nov-94 08:49:37 GMT ; RFC 850, obsoleted by RFC 1036
      Sun Nov  6 08:49:37 1994       ; ANSI C's asctime() format
:)
declare function adfhir:dateTime2httpDate($dateTime as xs:string?) as xs:string {
    let $parsedDateTime   := 
        if ($dateTime castable as xs:dateTime) then 
            xs:dateTime($dateTime) 
        else if (concat($dateTime,'T00:00:00') castable as xs:dateTime) then 
            xs:dateTime(concat($dateTime,'T00:00:00'))
        else (
            current-dateTime()
        )
    return
        format-dateTime(adjust-dateTime-to-timezone($parsedDateTime, xs:dayTimeDuration('PT0H')), '[F], [D01] [MNn] [Y] [H01]:[m01]:[s01] GMT', 'en', (), ())
};
(:FHIR id may not have colons. This affects using @effectiveDate as part of the ValueSet.id
    Reset the colons if omitted so 2016-10-26T123456 is reset to 2016-10-26T12:34:56
    Decided to support every variation so even 20161026123456 is supported
:)
declare function adfhir:hl7TS2dateTime($dateTime as xs:string?) as xs:string? {
    if (matches($dateTime, '^(\d{4})-?(\d{2})-?(\d{2})T?(\d{2}):?(\d{2}):?(\d{2})$')) then 
        replace($dateTime, '^(\d{4})-?(\d{2})-?(\d{2})T?(\d{2}):?(\d{2}):?(\d{2})$','$1-$2-$3T$4:$5:$6') 
    else $dateTime
};

(: **** UTILITY **** :)
(:~ Return SNOMED CT system uri if SNOMED CT is installed :)
declare function adfhir:getSnomedUri() as xs:string? {
    try {
        util:import-module(xs:anyURI("http://art-decor.org/ns/terminology/snomed"), "snomed", xs:anyURI(concat(repo:get-root(), "/terminology/snomed/api/api-snomed.xqm")))
        ,
        let $function   := function-lookup(xs:QName("snomed:searchDescription"), 4)
        return
        if (exists($function('sno cli ver', $snomed:maxResults, '', ''))) then
            $getf:CS_SNOMEDCT
        else ()
    } catch * {()}
};
(:~ Return LOINC system uri if LOINC is installed :)
declare function adfhir:getLoincUri() as xs:string? {
    try {
        util:import-module(xs:anyURI("http://art-decor.org/ns/terminology/loinc"), "loinc", xs:anyURI(concat(repo:get-root(), "/terminology/loinc/api/api-loinc.xqm")))
        ,
        let $function   := function-lookup(xs:QName("loinc:getVersionInfo"), 0)
        return
        if (exists($function())) then
            $getf:CS_LOINC
        else ()
    } catch * {()}
};
(:<description conceptId="138875005" fullName="SNOMED CT Concept (SNOMED RT+CTV3)" type="syn">SNOMED Clinical Terms version: 20180731 [R] (July 2018 Release)</description>:)
(:~ Return ClaML system uris for every ClaML based terminology that is installed :)
declare function adfhir:getClamlUris() as xs:string* {
    try {
        util:import-module(xs:anyURI("http://art-decor.org/ns/terminology/claml"), "claml", xs:anyURI(concat(repo:get-root(), "terminology/claml/api/api-claml.xqm"))),
        util:import-module(xs:anyURI("http://art-decor.org/ns/art"), "utillib", xs:anyURI(concat(repo:get-root(), "/api/modules/library/util-lib.xqm"))),
        util:import-module(xs:anyURI("http://art-decor.org/ns/art-decor-settings"), "setlib", xs:anyURI(concat(repo:get-root(), "/api/modules/library/settings-lib.xqm")))
        ,
        let $function       := function-lookup(xs:QName("claml:getClaMLIndex"), 0)
        let $artfunction    := function-lookup(xs:QName("utillib:getCanonicalUriForOID"), 5)
        let $index          := $function()
        
        for $classificationId in $index//classification/@id
        return 
            $artfunction('CodeSystem', $classificationId, (), (), $get:strKeyFHIRSTU3)
    } catch * {()}
};
(:~ strip all html tags out of a string :)
declare function adfhir:stripHTMLtags($origs as xs:string) as xs:string* {
(: algorithm:
    replace the tags with spaces,
    replace HTML entities with spaces
    reduce any duplicate spaces into single spaces, 
    trim away leading and trailing spaces
:)
    let $noh1 := replace($origs, '/<[^>]*>/g', ' ')
    let $noh2 := replace($noh1, '/&#38;([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});/ig', ' ')
    return normalize-space(replace($noh2, '/\s{2,}/g', ' '))
};
(:~ Returns a name which is an acceptable FHIR .name which matches ('[A-Z]([A-Za-z0-9_]){0,254}')
    Most common diacritics are replaced
    
    Note: an implementation of this function exists in XSL too in the ADA package shortName.xsl. Changes here should be reflected there too and vice versa.
    
    Input:  xs:string, example: "Underscored Lowercase ë"
    Output: xs:string, example: "underscored_lowercase_e"
    
    @author Marc de Graauw, Alexander Henket
    @since 2013
:)
declare function adfhir:shortName($name as xs:string?) as xs:string? {
    let $shortname := 
        if ($name) then (
            (: add some readability to CamelCased names. E.g. MedicationStatement to Medication_Statement. :)
            let $r0 := replace($name, '([a-z])([A-Z])', '$1_$2')
            
            (: find matching alternatives for <=? smaller(equal) and >=? greater(equal) :)
            let $r1 := replace($r0, '<\s*=', 'le')
            let $r2 := replace($r1, '<', 'lt')
            let $r3 := replace($r2, '>\s*=', 'ge')
            let $r4 := replace($r3, '>', 'gt')
            
            (: find matching alternatives for more or less common diacriticals, replace single spaces with _ , replace ? with q (same name occurs quite often twice, with and without '?') :)
            let $r5 := translate(normalize-space($r4),' ÀÁÃÄÅÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝŸÇÑàáãäåèéêëìíîïòóôõöùúûüýÿç€ßñ?','_AAAAAEEEEIIIIOOOOOUUUUYYCNaaaaaeeeeiiiiooooouuuuyycEsnq')
            (: ditch anything that's not alpha numerical :)
            let $r6 := replace($r5,'[^A-Za-z\d]','')
            (: make sure we start with a capital :)
            let $r7 := 
                if (matches(substring($r6, 1, 1), '[A-Za-z]')) then 
                    upper-case(substring($r6, 1, 1)) || substring($r6, 2)
                else (
                    'A' || $r6
                )
            return $r7
        ) else ()
    
    return if (matches($shortname, '^[A-Z]([A-Za-z0-9_]){1,254}$')) then $shortname else ()
};
(:~ Return valid .name according to inv-0 pattern name.matches('[A-Z]([A-Za-z0-9_]){0,254}') :)
declare function adfhir:validResourceName($nameString as xs:string?) as xs:string? {
    if (string-length($nameString) = 0)                       then $nameString else
    if (matches($nameString, '^[A-Z]([A-Za-z0-9_]){0,254}$')) then $nameString else (
        (: guarantee characters :)
        (: find matching alternatives for <=? smaller(equal) and >=? greater(equal) :)
        let $r1 := replace($nameString, '<\s*=', 'le')
        let $r2 := replace($r1, '<', 'lt')
        let $r3 := replace($r2, '>\s*=', 'ge')
        let $r4 := replace($r3, '>', 'gt')
        
        (: find matching alternatives for more or less common diacriticals, replace single spaces with _ , replace ? with q (same name occurs quite often twice, with and without '?') :)
        let $r5 := translate(normalize-space($r4),' ÀÁÃÄÅÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝŸÇàáãäåèéêëìíîïòóôõöùúûüýÿç€ßñ?-','_AAAAAEEEEIIIIOOOOOUUUUYYCaaaaaeeeeiiiiooooouuuuyycEsnq_')
        
        (: ditch anything that's not alpha numerical or underscore :)
        let $r6 := replace($r5,'[^A-Za-z\d_]','')
        
        (: guarantee initial A-Z :)
        let $firstChar  := substring($r6, 1, 1)
        let $r7         := if (matches($firstChar, '[A-Za-z]')) then concat(upper-case($firstChar), substring($r6, 2)) else concat('A', $r6)
        
        (: guarantee length :)
        let $r8 := substring($r7, 1, 254)
        
        return $r8
    )
};
(:~ HL7 V3 TS yyyyMMddhhmmss+-ZZzz to ISO 8601 yyyy-mm-ddThh:mm:ss :)
declare function adfhir:dateTimeFromTimestamp($ts as xs:string?) as xs:string {
    if (string-length($ts)=0) then (
        string(current-dateTime())
    )
    else (
        let $year       := if (substring($ts,1,1)='-') then substring($ts,1,5) else substring($ts,1,4)
        let $year       := $year[string-length()>=4]
        let $month      := if (substring($ts,1,1)='-') then substring($ts,6,2) else substring($ts,5,2)
        let $month      := $month[$year][string-length()=2]
        let $day        := if (substring($ts,1,1)='-') then substring($ts,8,2) else substring($ts,7,2)
        let $day        := $day[$month][string-length()=2]
        let $hour       := if (substring($ts,1,1)='-') then substring($ts,10,2) else substring($ts,9,2)
        let $hour       := $hour[$day][string-length()=2]
        let $minute     := if (substring($ts,1,1)='-') then substring($ts,12,2) else substring($ts,11,2)
        let $minute     := $minute[$hour][string-length()=2]
        let $second     := if (substring($ts,1,1)='-') then substring($ts,14,2) else substring($ts,13,2)
        let $second     := $second[$minute][string-length()=2]
        let $subsecond  := replace($ts,'^\d{14}(\.\d+)?.*$','$1')
        let $subsecond  := $subsecond[string-length()>0]
        let $timezone   := replace($ts,'^-?\d+(([+\-]\d{2})(\d{2}))?$','$2:$3')
        let $timezone   := $timezone[string-length()>1]
        
        let $date       := string-join(($year,$month,$day),'-')
        let $time       := string-join(($hour,$minute,$second),':')
        let $time       := string-join(($time,$timezone),'')
        
        return
            if ($date and $time) then string-join(($date,$time),'T') else $date
    )
};