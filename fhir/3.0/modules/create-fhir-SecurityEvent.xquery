xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adfhir              = "http://art-decor.org/ns/fhir/3.0" at "../api/api-fhir.xqm";
import module namespace getf                = "http://art-decor.org/ns/fhir-settings" at "../api/fhir-settings.xqm";
declare namespace f                         = "http://hl7.org/fhir";

let $save-security              := if (request:exists()) then request:get-attribute('securityevent.create')[string-length()>0] else ()

(: This is our log timestamp :)
let $now                        := current-dateTime()
let $_response-format           := if (request:exists()) then request:get-attribute('response.format') else ($getf:CT_FHIR_XML)

(: The source IP/host. Note that due to rewriting in Orbeon/Tomcat this might be useless data (localhost) :)
let $httpRequestSource          := if (request:exists()) then request:get-attribute('request.source.address') else ()
(: The path that was in the original request :)
let $httpRequestPath            := if (request:exists()) then request:get-attribute('request.path')[string-length()>0] else ()
(:  C    Create      R   Read/View/Print     U   Update                 D   Delete        
    E   Execute Perform a system or application function such as log-on, program execution or use of an object's method, or perform a query/search operation.
:)
let $httpRequestType            := if (request:exists()) then request:get-attribute('request.type') else ()
(:read, vread, update, create, history-instance, capabilities etc.:)
let $fhirRequestType            := if (request:exists()) then request:get-attribute('request.subtype') else ()

(:  2xx Success     4xx Client failure      5xx Server failure :)
let $httpResponseCode           := if (request:exists()) then xs:integer(request:get-attribute('response.http-status')) else (200)
(:  May need to affix new/updated headers to client response. Example: Location and Content-Type. Expect:
    <header key="..." value="..."/>
:)
let $httpResponseHeaders        := if (request:exists()) then request:get-attribute('response.headers') else ()

(:  0   Success     4   Minor failure       8   Serious failure       12  Major failure (i.e. the system died).
    We would never have 12 as this xquery would not be triggered.
:)
let $outcomeCode                := 
    if ($httpResponseCode >= 500) then 8 else if ($httpResponseCode >= 400) then 4 else if ($httpResponseCode >= 200) then 0 else (12)
let $outcomeDisplay             := 
    if ($outcomeCode=8) then 'Serious failure' else if ($outcomeCode=4) then 'Minor failure' else if ($outcomeCode=0) then 'Success' else ('Major failure')
let $id                         := count(xmldb:get-child-resources($getf:strFhirAudit))+1

let $SecurityEvent              :=
    <SecurityEvent xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://hl7.org/fhir">
        <id value="{$id}"/>
        <meta>
            <profile value="http://hl7.org/fhir/{$getf:strFhirVersionShort}/StructureDefinition/SecurityEvent"/>
        </meta>
        <text>
            <status value="generated"/>
            <div xmlns="http://www.w3.org/1999/xhtml">
                <p>
                    <b>Generated Narrative with Details</b>
                </p>
                <h3>Events</h3>
                <table class="grid">
                    <tr>
                        <td>
                            <b>Type</b>
                        </td>
                        <td>
                            <b>Subtype</b>
                        </td>
                        <td>
                            <b>Action</b>
                        </td>
                        <td>
                            <b>DateTime</b>
                        </td>
                        <td>
                            <b>Outcome</b>
                        </td>
                        <td>
                            <b>OutcomeDesc</b>
                        </td>
                    </tr>
                    <tr>
                        <td>RESTful Operation <span style="background: LightGoldenRodYellow;">(Details : {{http://hl7.org/fhir/security-event-type code &#39;rest&#39; = &#39;RESTful Operation&#39;, given as &#39;Restful Operation&#39;}})</span></td>
                        {
                            if (string-length($fhirRequestType)>0 and not($fhirRequestType='not-yet-implemented')) then
                                <td>{$fhirRequestType} <span style="background: LightGoldenRodYellow;">(Details : {{http://hl7.org/fhir/restful-interaction code &#39;{$fhirRequestType}&#39; = &#39;{$fhirRequestType}&#39;, given as &#39;{$fhirRequestType}&#39;}})</span></td>
                            else ()
                        }
                        <td>{$httpRequestType}</td>
                        <td>{format-dateTime($now, '[Y0001]-[M01]-[D01] [H01]:[m01]:[s01]', (), (), ())}</td>
                        <td>{$outcomeCode}</td>
                        <td>{$outcomeDisplay}</td>
                    </tr>
                </table>
                <blockquote>
                    <p>
                        <b>participant</b>
                    </p>
                    <p>
                        <b>userId</b>: {(sm:id()//sm:real/sm:username/text())[1]}
                    </p>
                    <p>
                        <b>requestor</b>: true
                    </p>
                </blockquote>
                <h3>Sources</h3>
                <table class="grid">
                    <tr>
                        <!--td>
                            <b>Site</b>
                        </td-->
                        <td colapn="3">
                            <b>Identifier</b>
                        </td>
                        <!--td>
                            <b>Type</b>
                        </td-->
                    </tr>
                    <tr>
                        <!--td>Cloud</td-->
                        <td colspan="3">{$httpRequestSource}</td>
                        <!--td>Web Server (Details: http://hl7.org/fhir/security-source-type code 3 = &#39;Web Server&#39;, stated as &#39;Web Server&#39;)</td-->
                    </tr>
                </table>
                <blockquote>
                    <p>
                        <b>object</b>
                    </p>
                    <p>
                        <b>reference</b>: <a href="{$httpRequestPath}">{$httpRequestPath}</a>
                    </p>
                    <!--p>
                        <b>type</b>: _2
                    </p>
                    <p>
                        <b>lifecycle</b>: _6
                    </p-->
                </blockquote>
            </div>
        </text>
        <event>
            <type>
                <coding>
                    <system value="http://hl7.org/fhir/security-event-type"/>
                    <code value="rest"/>
                    <display value="RESTful Operation"/>
                </coding>
            </type>
            {
                if (string-length($fhirRequestType)>0 and not($fhirRequestType='not-yet-implemented')) then
                    <subtype>
                        <coding>
                            <system value="http://hl7.org/fhir/restful-interaction"/>
                            <code value="{$fhirRequestType}"/>
                            <display value="{$fhirRequestType}"/>
                        </coding>
                    </subtype>
                else ()
            }
            <action value="{$httpRequestType}"/>
            <dateTime value="{current-dateTime()}"/>
            <outcome value="{$outcomeCode}"/>
        </event>
        <participant>
            <userId value="{(sm:id()//sm:real/sm:username/text())[1]}"/>
            <!--name value="{aduser:getDisplayName()}"/-->
            <requestor value="true"/>
        </participant>
        <source>
            <!--site value="Cloud"/-->
            <identifier value="{$httpRequestSource}"/>
            <!--type>
                <system value="http://hl7.org/fhir/security-source-type"/>
                <code value="3"/>
                <display value="Web Server"/>
            </type-->
        </source>
        {
            if ($httpRequestPath) then (
                <object>
                    <reference>
                        <reference value="{$httpRequestPath}"/>
                    </reference>
                    <!--type value="2"/-->
                    <!--lifecycle value="6"/-->
                </object>
            ) else ()
        }
    </SecurityEvent>

let $add-event                  := 
    if ($save-security = $getf:SECURITY_SAVE_NONE) then () else (
        if ($save-security = $getf:SECURITY_SAVE_ALWAYS or $outcomeCode != 0) then adfhir:createSecurityEvent($SecurityEvent) else ()
    )

let $data                       := request:get-data()
let $data                       := if (empty($data)) then request:get-attribute('response.contents') else ($data)

return (
    if (response:exists()) then (
        response:set-status-code($httpResponseCode),
        response:set-header('Content-Type', head($_response-format) || $getf:HEADER_SUFFIX_CT_UTF_8),
        for $httpResponseHeader in $httpResponseHeaders
        return 
            response:set-header($httpResponseHeader/@name, $httpResponseHeader/@value)
        ,
        response:set-header('X-Response-Format',string-join($_response-format,'; ')),
        adfhir:handleResponseFormat($data, $_response-format)
    ) else ()
)