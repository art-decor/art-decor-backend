xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adfhir      = "http://art-decor.org/ns/fhir/3.0" at "../api/api-fhir.xqm";
declare namespace f                 = "http://hl7.org/fhir";

let $_request-method        := request:get-method()
let $_request-path          := request:get-attribute('request.path')
let $_request-query         := request:get-query-string()
let $_request-has-query     := request:get-attribute('request.hasquerystring')
let $_request-body          :=
    if (request:is-multipart-content()) 
    then (
        let $file   := request:get-uploaded-file-data('file')
        return if (empty($file)) then () else (util:base64-decode($file))
    )
    else (
        request:get-data()
    )

return
<echo>
    <method>{$_request-method}</method>
    <path>{$_request-path}</path>
    <query>{$_request-query}</query>
    <context>{request:get-context-path()}</context>
    {
        for $cn in request:get-cookie-names()
        return <cookie name="{$cn}">{request:get-cookie-value($cn)}</cookie>
    }
    <path-info>{request:get-path-info()}</path-info>
    <uri effective="{request:get-effective-uri()}" uri="{request:get-uri()}" url="{request:get-url()}" scheme="{request:get-scheme()}"/>
    <local server-name="{request:get-server-name()}" server-port="{request:get-server-port()}" hostname="{request:get-hostname()}"/>
    <remote addr="{request:get-remote-addr()}" host="{request:get-remote-host()}" port="{request:get-remote-port()}"/>
    <headers>
    {
        for $header in request:get-header-names()
        return <header name="{$header}">{request:get-header($header)}</header>
    }
    </headers>
    <data multipart="{request:is-multipart-content()}">
    {
        if (request:is-multipart-content()) then (
            attribute name {request:get-uploaded-file-name('file')},
            attribute size {request:get-uploaded-file-size('file')}
        ) else()
        ,
        if ($_request-body instance of document-node()) then
            $_request-body/*
        else (
            $_request-body
        )
    }
    </data>
    {
        if (empty($_request-body) or $_request-body instance of element() or $_request-body instance of document-node()) then () else (
        <parsed-data>
        {
            try {
                (:Hack alert: upload fails when content has UTF-8 Byte Order Marker. 
                the UTF-8 representation of the BOM is the byte sequence 0xEF,0xBB,0xBF:)
                if (string-to-codepoints(substring($_request-body,1,1))=65279) 
                then (fn:parse-xml(substring($_request-body,2)))
                else (fn:parse-xml($_request-body))
            }
            catch * {
                <error>{concat('ERROR ',$err:code,'. Could not parse incoming data: ',$err:description,' module: ',$err:module,' (',$err:line-number,' ',$err:column-number,')')}</error>
                ,
                $err:value
            }
        }
        </parsed-data>
        )
    }
    {adfhir:getRequestParts($_request-method, $_request-path, $_request-query, $_request-has-query)}
</echo>