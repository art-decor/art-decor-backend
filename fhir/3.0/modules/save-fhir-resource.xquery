xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adfhir      = "http://art-decor.org/ns/fhir/3.0" at "../api/api-fhir.xqm";
import module namespace getf        = "http://art-decor.org/ns/fhir-settings" at "../api/fhir-settings.xqm";
declare namespace f                 = "http://hl7.org/fhir";
declare namespace error             = "http://art-decor.org/ns/fhir/error";

(:
    TODO:
    - Find out a strategy for id and version
    - Convert JSON > XML if necessary
    
    - Single resources versus bundled/feed resources (done)
    - Minimal validation (done)
    - Permissions (done)
    - Return HTTP Location header with final location (done)
    - Return correct HTTP statuses (done)
    - Return OperationOutcome if necessary (done)
    - Move current version(s) to history and create request-data as new current. (done)
:)
let $result             :=
    try {
        let $id             := request:get-attribute('request.id')
        let $count          := request:get-parameter('_count',())[. castable as xs:integer]
        let $since          := request:get-parameter('_since',())[. castable as xs:dateTime]
        let $offset         := 0
        let $sort           := request:get-parameter('_sort',())[string-length()>0]
        let $resource       := adfhir:getRequestBody()
        let $isBundle       := $resource instance of element(f:Bundle)
        let $isElement      := $resource instance of element()
        let $content-type   := 
            if (request:is-multipart-content()) then (
                if ($resource instance of element()) then
                    concat('application/xml',if ($resource/namespace-uri()='http://hl7.org/fhir') then '+fhir' else ())
                else (
                    (:json?:)
                )
            )
            else (
                request:get-header('Content-Type')[string-length()>0]
            )
        let $validation     :=  if ($isElement) then adfhir:validate($resource) else ()
        let $update         :=
            if ($validation[@role=('error','warn','warning','fatal')])
            then error(QName('http://art-decor.org/ns/fhir/error','ResourceInvalid'),'ResourceInvalid',$validation)
            else if ($isBundle and $resource[f:entry/f:search])
            then error(QName('http://art-decor.org/ns/fhir/error','ResourceNotSupported'),'Bundle contains conditional processing, but that is currently unsupported.')
            else if ($isBundle)
            then adfhir:saveResourceBundle($resource,$content-type)
            else if ($isElement) 
            then adfhir:saveResource($resource,$id,adfhir:getIfMatches(),$content-type)
            else if (empty($resource)) 
            then error(QName('http://art-decor.org/ns/fhir/error','ResourceEmpty'),'Resource to save contained no data')
            else error(QName('http://art-decor.org/ns/fhir/error','ResourceNotXml'),'Resource to save was not (FHIR) XML.')
        
        let $headers        := if (count($update)=1) then (
                                    <header name="Location" value="{$update/@location}"/> |
                                    <header name="ETag" value="W/&quot;{$update/@version}&quot;"/> |
                                    <header name="Last-Modified" value="{adfhir:dateTime2httpDate($update[1]/@lastupdated)}"/>
                               ) else (
                                    <header name="Last-Modified" value="{adfhir:dateTime2httpDate($update[1]/@lastupdated)}"/>
                               )
        let $r              := request:set-attribute('response.headers',$headers)
        let $r              := request:set-attribute('response.http-status',if ($update/@c='true') then 201 else 200)
        
        let $results        := 
            if ($isBundle) then 
                adfhir:saveResponseBundle($update,$getf:BUNDLE_TRANSACTION_RESPONSE, $count, $sort) 
            else ()
        
        return
            if ($results[@location]) then (
                (:adfhir:getResourceSnapshot($results/@type,$results/@id,$offset,$count):)
                let $r              := request:set-attribute('response.http-status','303')
                let $headers        := (request:get-attribute('response.headers') | 
                                        <header name="Location" value="{($results/@location)[1]}"/>)
                let $r              := request:set-attribute('response.headers',$headers)
                
                return ()
            ) else ()
    }
    catch error:ResourceEmpty {
        let $desc   := concat('ERROR ',$err:code,'. Could not create/update resource. ',$err:description)
        let $r      := request:set-attribute('response.http-status',400)
        return adfhir:operationOutCome('error',(),$desc,())
    }
    catch error:ResourceDeleted {
        let $desc   := concat('ERROR ',$err:code,'. Could not update resource. ',$err:description)
        let $r      := request:set-attribute('response.http-status',409)
        return adfhir:operationOutCome('error',(),$desc,())
    }
    catch error:ResourceNotCurrent {
        let $desc   := concat('ERROR ',$err:code,'. Could not update resource. ',$err:description)
        let $r      := request:set-attribute('response.http-status',412)
        return adfhir:operationOutCome('error',(),$desc,())
    }
    catch error:ResourceNotSupported {
        let $desc   := concat('ERROR ',$err:code,'. Could not process resource. ',$err:description)
        let $r      := request:set-attribute('response.http-status',412)
        return adfhir:operationOutCome('error',(),$desc,())
    }
    catch error:ResourceNotXml {
        let $desc   := concat('ERROR ',$err:code,'. Could not process resource. ',$err:description)
        let $r      := request:set-attribute('response.http-status',415)
        return adfhir:operationOutCome('error',(),$desc,())
    }
    catch error:ResourceInvalid {
        let $desc   := concat('ERROR ',$err:code,'. Could not create/update resource. ',$err:description)
        let $r      := request:set-attribute('response.http-status',400)
        return adfhir:operationOutCome($err:value)
    }
    catch * {
        let $desc   := concat('ERROR ',$err:code,'. Could not save resource. ',$err:description,' module: ',$err:module,' (',$err:line-number,' ',$err:column-number,').')
        let $r      := request:set-attribute('response.http-status',500)
        return adfhir:operationOutCome('error',(),$desc,())
    }

let $r      := if (empty($result)) then () else request:set-attribute('response.contents', $result)
return $result