xquery version "3.1";
import module namespace getf                = "http://art-decor.org/ns/fhir-settings" at "xmldb:exist:///db/apps/fhir/api/fhir-settings.xqm";

let $fhirBaseURL    := 'http://hl7.org/fhir/STU3/'

let $resourceList   := xmldb:get-child-resources($getf:strFhirXsdSch)[ends-with(.,'.sch')][not(.='fhir-invariants.sch')]

return
<x count="{count($resourceList)}">
{
for $resource in $resourceList
let $profileRes     := replace($resource,'.sch','.profile.xml')
let $profileUpd     := 
    try {
        xmldb:store($getf:strFhirXsdSch, $profileRes,doc(concat($fhirBaseURL, $profileRes)))
    }
    catch * {
        concat('Problem getting profile ',$profileRes,' ',$err:code,' ',$err:description)
    }
order by $resource
return
    <resource name="{$resource}">{$profileUpd}</resource>
}
</x>