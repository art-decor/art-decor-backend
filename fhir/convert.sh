# Converts FHIR schemas into an XSL that does FHIR XML to JSON

export wd=`dirname $0`
export toolCommand=/Applications/Oxygen\ XML\ Editor\ 22.1/flattenSchema.sh
export saxon=/Users/ahenket/Development/lib/SaxonPE9-7-0-20J/saxon9pe.jar

if [ ! -e "$toolCommand" ]; then
    echo ERROR The Oxygen tool \'${toolCommand}\' does not exist!
    exit
fi

#echo "FHIR DSTU2"
#java -jar "$saxon" -s:"${wd}/1.0/resources/schemas/fhir-single.xsd" -xsl:"schema2mapxsl.xsl" -o:"${wd}/1.0/resources/stylesheets/fhir-xml2json.xsl"
#java -jar "$saxon" -s:"${wd}/1.0/resources/schemas/fhir-single.xsd" -xsl:"schema2xmlxsl.xsl" -o:"${wd}/1.0/resources/stylesheets/fhir-json2xml.xsl"

#echo "FHIR STU3"
#java -jar "$saxon" -s:"${wd}/3.0/resources/schemas/fhir-single.xsd" -xsl:"schema2mapxsl.xsl" -o:"${wd}/3.0/resources/stylesheets/fhir-xml2json.xsl"
#java -jar "$saxon" -s:"${wd}/3.0/resources/schemas/fhir-single.xsd" -xsl:"schema2xmlxsl.xsl" -o:"${wd}/3.0/resources/stylesheets/fhir-json2xml.xsl"

#echo "FHIR R4"
#java -jar "$saxon" -s:"${wd}/4.0/resources/schemas/fhir-single.xsd" -xsl:"schema2mapxsl.xsl" -o:"${wd}/4.0/resources/stylesheets/fhir-xml2json.xsl"
#java -jar "$saxon" -s:"${wd}/4.0/resources/schemas/fhir-single.xsd" -xsl:"schema2xmlxsl.xsl" -o:"${wd}/4.0/resources/stylesheets/fhir-json2xml.xsl"

#echo "FHIR R4B"
#fhirVersion=4.3
#java -jar "$saxon" -s:"${wd}/${fhirVersion}/resources/schemas/fhir-single.xsd" -xsl:"schema2mapxsl.xsl" -o:"${wd}/${fhirVersion}/resources/stylesheets/fhir-xml2json.xsl"
#java -jar "$saxon" -s:"${wd}/${fhirVersion}/resources/schemas/fhir-single.xsd" -xsl:"schema2xmlxsl.xsl" -o:"${wd}/${fhirVersion}/resources/stylesheets/fhir-json2xml.xsl"

echo "FHIR R5"
fhirVersion=5.0
java -jar "$saxon" -s:"${wd}/${fhirVersion}/resources/schemas/fhir-single.xsd" -xsl:"schema2mapxsl.xsl" -o:"${wd}/${fhirVersion}/resources/stylesheets/fhir-xml2json.xsl"
java -jar "$saxon" -s:"${wd}/${fhirVersion}/resources/schemas/fhir-single.xsd" -xsl:"schema2xmlxsl.xsl" -o:"${wd}/${fhirVersion}/resources/stylesheets/fhir-json2xml.xsl"
