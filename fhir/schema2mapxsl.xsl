<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:nf="urn:nictiz:local-functions"
    exclude-result-prefixes="#all"
    version="3.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> May 10, 2020</xd:p>
            <xd:p><xd:b>Author:</xd:b> ahenket</xd:p>
            <xd:p>Takes in FHIR XSD fhir-single.xsd and constructs a stylesheet that will take in a FHIR XML instance and produce an expath map (http://www.w3.org/2005/xpath-functions) that may be converted into a valid FHIR JSON instance</xd:p>
            <xd:p>Known 'issue': does not handle xml comment() as array of fhir_comments, and basically ignores xml comment()</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:output indent="yes"/>
    
    <xsl:param name="fhir-version" select="'R4'"/>
    <xsl:variable name="schemadir" select="concat('http://hl7.org/fhir/', $fhir-version)"/>
    <xsl:param name="fhir-base" select="doc(concat($schemadir, '/fhir-base.xsd'))/xs:schema"/>
    
    <xsl:variable name="toolName" select="'schema2mapxsl.xsl'"/>
    <xsl:variable name="toolVersion" select="'1.0.0'"/>
    <xsl:variable name="functionPrefix">
        <xsl:choose>
            <xsl:when test="$fhir-version = 'DSTU2'">f2</xsl:when>
            <xsl:when test="$fhir-version = 'STU3'">f3</xsl:when>
            <xsl:when test="$fhir-version = 'R4'">f4</xsl:when>
            <xsl:when test="$fhir-version = 'R5'">f5</xsl:when>
            <xsl:otherwise>
                <xsl:message terminate="yes">Unsupported FHIR version. Supported values for xsl param fhir-version are DSTU2, STU3, R4, R5</xsl:message>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="this" select="/xs:schema"/>
    <xsl:variable name="ResourceType-list" select="$this/xs:complexType[@name = 'ResourceContainer']/xs:choice/xs:element/(@ref | @name)" as="item()+"/>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="/xs:schema">
        <xsl:element name="xsl:stylesheet">
            <xsl:namespace name="xs">http://www.w3.org/2001/XMLSchema</xsl:namespace>
            <xsl:namespace name="xd">http://www.oxygenxml.com/ns/doc/xsl</xsl:namespace>
            <xsl:namespace name="fn">http://www.w3.org/2005/xpath-functions</xsl:namespace>
            <xsl:namespace name="f">http://hl7.org/fhir</xsl:namespace>
            <xsl:namespace name="xh">http://www.w3.org/1999/xhtml</xsl:namespace>
            <xsl:namespace name="xhtml">http://www.w3.org/1999/xhtml</xsl:namespace>
            <xsl:attribute name="exclude-result-prefixes">#all</xsl:attribute>
            <xsl:attribute name="version">3.0</xsl:attribute>
            <xd:doc scope="stylesheet">
                <xd:desc>
                    <xd:p><xd:b>Generated:</xd:b><xsl:text> </xsl:text><xsl:value-of select="current-dateTime()"/></xd:p>
                    <xd:p><xd:b>By:</xd:b><xsl:text> </xsl:text><xsl:value-of select="$toolName"/><xsl:text> version </xsl:text><xsl:value-of select="$toolVersion"/></xd:p>
                  <xd:p>Takes in FHIR XSD fhir-single.xsd and constructs a stylesheet that will take in a FHIR XML instance and produce an expath map (http://www.w3.org/2005/xpath-functions) that may be converted into a valid FHIR JSON instance</xd:p>
                  <xd:p>Known 'issue': does not handle xml comment() as array of fhir_comments, and basically ignores xml comment()</xd:p>
                </xd:desc>
            </xd:doc>
            <xsl:element name="xsl:output">
                <xsl:attribute name="indent">yes</xsl:attribute>
                <xsl:attribute name="method">xml</xsl:attribute>
                <xsl:attribute name="media-type">application/xml</xsl:attribute>
                <xsl:attribute name="omit-xml-declaration">yes</xsl:attribute>
            </xsl:element>
            
            <xd:doc>
                <xd:desc>Process FHIR instances and error on anything else</xd:desc>
            </xd:doc>
            <xsl:element name="xsl:template">
                <xsl:attribute name="match">/</xsl:attribute>
                
                <xsl:element name="xsl:choose">
                    <xsl:for-each select="$ResourceType-list">
                        <xsl:element name="xsl:when">
                            <xsl:attribute name="test">
                                <xsl:text>f:</xsl:text>
                                <xsl:value-of select="."/>
                            </xsl:attribute>
                            <xsl:element name="map" namespace="http://www.w3.org/2005/xpath-functions">
                                <xsl:element name="xsl:apply-templates">
                                    <xsl:attribute name="mode" select="."/>
                                </xsl:element>
                            </xsl:element>
                        </xsl:element>
                    </xsl:for-each>
                    <xsl:element name="xsl:otherwise">
                        <xsl:element name="xsl:message">
                            <xsl:attribute name="terminate">yes</xsl:attribute>
                            <xsl:text>Unknown resourceType or not in FHIR namespace: {</xsl:text>
                            <xsl:element name="xsl:value-of">
                                <xsl:attribute name="select">*[1]/namespace-uri()</xsl:attribute>
                            </xsl:element>
                            <xsl:text>}:</xsl:text>
                            <xsl:element name="xsl:value-of">
                                <xsl:attribute name="select">*[1]/local-name()</xsl:attribute>
                            </xsl:element>
                        </xsl:element>
                    </xsl:element>
                </xsl:element>
            </xsl:element>
            
            <xsl:apply-templates select="xs:complexType except xs:include"/>
        </xsl:element>
    </xsl:template>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="xs:complexType">
        <xsl:call-template name="doAnnotation"/>
        <xsl:element name="xsl:template">
            <xsl:attribute name="match">*</xsl:attribute>
            <xsl:attribute name="mode" select="@name"/>
            
            <xsl:if test="parent::xs:schema and @name = $ResourceType-list">
                <string key="resourceType" xmlns="http://www.w3.org/2005/xpath-functions">
                    <xsl:value-of select="@name"/>
                </string>
            </xsl:if>
            
            <xsl:apply-templates select=".//xs:attribute"/>
            
            <xsl:apply-templates select="xs:* except (xs:annotation | xs:attribute)"/>
        </xsl:element>
    </xsl:template>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="xs:sequence/xs:element | xs:choice/xs:element">
        <xsl:variable name="theName" select="@ref | @name" as="attribute()?"/>
        <xsl:variable name="theType" as="xs:string">
            <xsl:choose>
                <xsl:when test="@type">
                    <xsl:value-of select="@type"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="/xs:schema/*[@name = current()/@ref]/@type"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <!-- primitives have their @id and extensions in a sibling array -->
        <xsl:variable name="isPrimitive" select="nf:isPrimitive($theType)" as="xs:boolean"/>
        <!--  -->
        <xsl:variable name="doArray" select="@maxOccurs = 'unbounded'" as="xs:boolean"/>
        
        <xsl:comment>
            <xsl:value-of select="$theName"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="(@minOccurs, '1')[1]"/>
            <xsl:text>..</xsl:text>
            <xsl:value-of select="(if (@maxOccurs = 'unbounded') then '*' else @maxOccurs, '1')[1]"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="$theType"/>
            <xsl:if test="$isPrimitive">
                <xsl:text> (json type: </xsl:text>
                <xsl:value-of select="nf:getJsonType($theType)"/>
                <xsl:text>)</xsl:text>
            </xsl:if>
        </xsl:comment>
        <xsl:element name="xsl:if">
            <xsl:attribute name="test">
                <xsl:if test="not(contains($theName, ':'))">f:</xsl:if>
                <xsl:value-of select="$theName"/>
            </xsl:attribute>
            <xsl:choose>
                <xsl:when test="$theName = 'xhtml:div'">
                    <string key="div" xmlns="http://www.w3.org/2005/xpath-functions">
                        <xsl:element name="xsl:value-of">
                            <xsl:attribute name="select">fn:serialize(<xsl:value-of select="$theName"/>)</xsl:attribute>
                        </xsl:element>
                    </string>
                </xsl:when>
                <xsl:when test="$doArray">
                    <xsl:choose>
                        <xsl:when test="$isPrimitive">
                            <!-- do element first -->
                            <array key="{$theName}" xmlns="http://www.w3.org/2005/xpath-functions">
                                <xsl:element name="xsl:for-each">
                                    <xsl:attribute name="select" select="concat('f:', $theName)"/>
                                    <xsl:element name="xsl:choose">
                                        <xsl:element name="xsl:when">
                                            <xsl:attribute name="test">@value</xsl:attribute>

                                            <xsl:element name="{nf:getJsonType($theType)}" namespace="http://www.w3.org/2005/xpath-functions">
                                                <xsl:element name="xsl:value-of">
                                                    <xsl:attribute name="select">@value</xsl:attribute>
                                                </xsl:element>
                                            </xsl:element>

                                        </xsl:element>
                                        <xsl:element name="xsl:otherwise">
                                            <null/>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:element>
                            </array>
                            <!-- do id and extensions -->
                            <xsl:element name="xsl:if">
                                <xsl:attribute name="test">f:<xsl:value-of select="$theName"/>[@id | f:extension]</xsl:attribute>

                                <array key="_{$theName}" xmlns="http://www.w3.org/2005/xpath-functions">
                                    <xsl:element name="xsl:for-each">
                                        <xsl:attribute name="select" select="concat('f:', $theName)"/>
                                        <xsl:element name="xsl:choose">
                                            <xsl:element name="xsl:when">
                                                <xsl:attribute name="test">.[@id | f:extension]</xsl:attribute>
                                                <map>
                                                    <xsl:element name="xsl:apply-templates">
                                                        <xsl:attribute name="select">.</xsl:attribute>
                                                        <xsl:attribute name="mode">Element</xsl:attribute>
                                                    </xsl:element>
                                                </map>
                                            </xsl:element>
                                            <xsl:element name="xsl:otherwise">
                                                <null/>
                                            </xsl:element>
                                        </xsl:element>
                                    </xsl:element>
                                </array>
                            </xsl:element>
                        </xsl:when>
                        <xsl:otherwise>
                            <!-- do the complex element -->
                            <array key="{$theName}" xmlns="http://www.w3.org/2005/xpath-functions">
                                <xsl:element name="xsl:for-each">
                                    <xsl:attribute name="select" select="concat('f:', $theName)"/>
                                    <map>
                                        <xsl:element name="xsl:apply-templates">
                                            <xsl:attribute name="select">.</xsl:attribute>
                                            <xsl:attribute name="mode">
                                                <xsl:value-of select="$theType"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                    </map>
                                </xsl:element>
                            </array>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <xsl:when test="$isPrimitive">
                            <!-- do element first -->
                            <xsl:element name="xsl:if">
                                <xsl:attribute name="test">f:<xsl:value-of select="$theName"/>/@value</xsl:attribute>
                                
                                <xsl:element name="{nf:getJsonType($theType)}" namespace="http://www.w3.org/2005/xpath-functions">
                                    <xsl:attribute name="key" select="$theName"/>
                                    <xsl:element name="xsl:value-of">
                                        <xsl:attribute name="select">f:<xsl:value-of select="$theName"/>/@value</xsl:attribute>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>
                            <!-- do id and extensions -->
                            <xsl:element name="xsl:if">
                                <xsl:attribute name="test">f:<xsl:value-of select="$theName"/>[@id | f:extension]</xsl:attribute>

                                <map key="_{$theName}" xmlns="http://www.w3.org/2005/xpath-functions">
                                    <xsl:element name="xsl:apply-templates">
                                        <xsl:attribute name="select">f:<xsl:value-of select="$theName"/></xsl:attribute>
                                        <xsl:attribute name="mode">Element</xsl:attribute>
                                    </xsl:element>
                                </map>
                            </xsl:element>
                        </xsl:when>
                        <xsl:when test="$theName = $ResourceType-list">
                            <xsl:element name="xsl:apply-templates">
                                <xsl:attribute name="select">f:<xsl:value-of select="$theName"/></xsl:attribute>
                                <xsl:attribute name="mode">
                                    <xsl:value-of select="$theType"/>
                                </xsl:attribute>
                            </xsl:element>
                        </xsl:when>
                        <xsl:otherwise>
                            <!-- do the element -->
                            <map key="{$theName}" xmlns="http://www.w3.org/2005/xpath-functions">
                                <xsl:element name="xsl:apply-templates">
                                    <xsl:attribute name="select">f:<xsl:value-of select="$theName"/></xsl:attribute>
                                    <xsl:attribute name="mode">
                                        <xsl:value-of select="$theType"/>
                                    </xsl:attribute>
                                </xsl:element>
                            </map>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:element>
    </xsl:template>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="xs:complexContent">
        <xsl:apply-templates select="* except (xs:annotation | xs:attribute)"/>
    </xsl:template>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="xs:extension">
        <xsl:variable name="base" select="@base"/>
        <xsl:element name="xsl:apply-templates">
            <xsl:attribute name="select">.</xsl:attribute>
            <xsl:attribute name="mode" select="$base"/>
        </xsl:element>
        <xsl:apply-templates select="* except (xs:annotation | xs:attribute)"/>
    </xsl:template>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="xs:restriction">
        <xsl:apply-templates select="xs:sequence"/>
    </xsl:template>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="xs:attribute">
        <xsl:element name="xsl:if">
            <xsl:attribute name="test">@<xsl:value-of select="@name"/></xsl:attribute>
            <xsl:element name="{nf:getJsonType(@type)}" namespace="http://www.w3.org/2005/xpath-functions">
                <xsl:attribute name="key" select="@name"/>
                <xsl:element name="xsl:value-of">
                    <xsl:attribute name="select">@<xsl:value-of select="@name"/></xsl:attribute>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:template>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="xs:include"/>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="xs:simpleType"/>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="xs:choice">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="xs:annotation"/>
    
    <xd:doc>
        <xd:desc/>
        <xd:param name="doParam"/>
    </xd:doc>
    <xsl:template name="doAnnotation">
        <xsl:param name="doParam" select="true()" as="xs:boolean"/>
        <xd:doc>
            <xd:desc>
                <xsl:for-each select="xs:annotation/xs:documentation">
                    <xd:p>
                        <xsl:copy-of select="node()" copy-namespaces="no"/>
                    </xd:p>
                </xsl:for-each>
            </xd:desc>
            <!--<xsl:if test="$doParam">
                <xd:param name="key">Optional string value for the @key attribute</xd:param>
            </xsl:if>-->
        </xd:doc>
    </xsl:template>
    
    <xd:doc>
        <xd:desc/>
        <xd:param name="type"/>
    </xd:doc>
    <xsl:template name="getElementType" as="xs:string">
        <xsl:param name="type" select="@name" as="xs:string"/>
        
        <xsl:variable name="complexType" select="$this/xs:complexType[@name = $type]" as="element()?"/>
        <xsl:variable name="complexTypeElementType" select="$complexType/xs:complexContent/xs:extension[@base = 'Element'][empty(xs:sequence/*)][count(xs:attribute) = 1]/xs:attribute[@name = 'value']/@type"/>
        <xsl:variable name="simpleTypeType" select="$this/xs:simpleType[@name = $complexTypeElementType]/xs:restriction/tokenize(@base, '-')[1]" as="xs:string?"/>
        
        <xsl:value-of select="replace(($simpleTypeType, tokenize($complexTypeElementType, '-')[1], $type)[1], '^xs:', '')"/>
    </xsl:template>
    
    <xd:doc>
        <xd:desc/>
        <xd:param name="type"/>
    </xd:doc>
    <xsl:function name="nf:getJsonType" as="xs:string?">
        <xsl:param name="type" as="xs:string?"/>
        
        <xsl:choose>
            <xsl:when test="$type = 'integer'">number</xsl:when>
            <xsl:when test="$type = 'integer-primitive'">number</xsl:when>
            <xsl:when test="$type = 'unsignedInt'">number</xsl:when>
            <xsl:when test="$type = 'unsignedInt-primitive'">number</xsl:when>
            <xsl:when test="$type = 'positiveInt'">number</xsl:when>
            <xsl:when test="$type = 'positiveInt-primitive'">number</xsl:when>
            <xsl:when test="$type = 'decimal'">number</xsl:when>
            <xsl:when test="$type = 'decimal-primitive'">number</xsl:when>
            <xsl:when test="$type = 'count'">number</xsl:when>
            <xsl:when test="$type = 'count-primitive'">number</xsl:when>
            <xsl:when test="$type = 'boolean'">boolean</xsl:when>
            <xsl:when test="$type = 'boolean-primitive'">boolean</xsl:when>
            <xsl:when test="$type = 'base64Binary'">string</xsl:when>
            <xsl:when test="$type = 'base64Binary-primitive'">string</xsl:when>
            <xsl:when test="$type = 'instant'">string</xsl:when>
            <xsl:when test="$type = 'instant-primitive'">string</xsl:when>
            <xsl:when test="$this/xs:simpleType[@name = $type]">string</xsl:when>
            <xsl:when test="$this/xs:complexType[@name = $type]/xs:complexContent/xs:extension[@base = 'Element'][empty(xs:sequence/*)][count(xs:attribute) = 1]/xs:attribute[@name = 'value']">string</xsl:when>
        </xsl:choose>
    </xsl:function>
    
    <xd:doc>
        <xd:desc/>
        <xd:param name="type"/>
    </xd:doc>
    <xsl:function name="nf:isPrimitive" as="xs:boolean">
        <xsl:param name="type" as="xs:string?"/>
        
        <xsl:choose>
            <xsl:when test="matches($type, '^[a-z]')">
                <xsl:value-of select="true()"/>
            </xsl:when>
            <xsl:when test="$this/xs:simpleType[@name = $type]">
                <xsl:value-of select="true()"/>
            </xsl:when>
            <xsl:when test="$this/xs:complexType[@name = $type]/xs:complexContent/xs:extension[@base = 'Element'][empty(xs:sequence/*)][count(xs:attribute) = 1]/xs:attribute[@name = 'value']">
                <xsl:value-of select="true()"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="false()"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
</xsl:stylesheet>