xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adfhir      = "http://art-decor.org/ns/fhir/1.0" at "../api/api-fhir.xqm";
declare namespace f                 = "http://hl7.org/fhir";
declare namespace error             = "http://art-decor.org/ns/fhir/error";

(:
    TODO:
    - Permissions
    - Return HTTP Location header with final location
    - Return correct HTTP statuses
    - Return OperationOutcome if necessary
:)
let $type       := request:get-attribute('request.resource')
let $id         := request:get-attribute('request.id')
let $version    := request:get-attribute('request.version')
let $result             :=
    try {
        let $resource       := 
            if (request:is-multipart-content()) then (
                let $filecontent    := request:get-uploaded-file-data('file')
                let $filecontent    := if (empty($filecontent)) then () else (util:base64-decode($filecontent))
                (:Hack alert: upload fails when content has UTF-8 Byte Order Marker. 
                the UTF-8 representation of the BOM is the byte sequence 0xEF,0xBB,0xBF:)
                return 
                    if (empty($filecontent))
                    then ()
                    else if (string-to-codepoints(substring($filecontent,1,1))=65279) 
                    then (fn:parse-xml(substring($filecontent,2))/f:*)
                    else (fn:parse-xml($filecontent)/f:*)
            )
            else ( 
                request:get-data()
            )
        let $update             :=
            if ($resource instance of element(f:meta)) 
            then adfhir:deleteResourceMeta($resource,$type,$id,$version)
            else if (empty($resource)) 
            then error(QName('http://art-decor.org/ns/fhir/error','ResourceEmpty'),'Deletion requires a (FHIR) XML meta element in the request.')
            else error(QName('http://art-decor.org/ns/fhir/error','ResourceNotXml'),'Deletion requires a (FHIR) XML meta element in the request.')
        
        (:these headers are only useful when operating on an instance:)
        let $headers            := 
            if (empty($id)) then () else (
                <header name="Location" value="{$update/@location}"/> |
                <header name="ETag" value="W/&quot;{$update/@version}&quot;"/> |
                <header name="Last-Modified" value="{$update/@lastupdated}"/>
            )
        let $r                  := request:set-attribute('response.headers',$headers)
        let $r                  := request:set-attribute('response.http-status',204)
        return ()
    }
    catch error:ResourceNotFound {
        let $desc   := concat('ERROR ',$err:code,'. Could not delete. ',$err:description)
        let $r      := request:set-attribute('response.http-status',404)
        return adfhir:operationOutCome('error',(),$desc,())
    }
    catch error:ResourceNotCurrent {
        let $desc   := concat('ERROR ',$err:code,'. Could not delete. ',$err:description)
        let $r      := request:set-attribute('response.http-status',412)
        return adfhir:operationOutCome('error',(),$desc,())
    }
    catch * {
        let $desc   := concat('ERROR ',$err:code,'. Could not delete. ',$err:description,' module: ',$err:module,' (',$err:line-number,' ',$err:column-number,')')
        let $r      := request:set-attribute('response.http-status',500)
        return adfhir:operationOutCome('error',(),$desc,())
    }

let $r                          := if (empty($result)) then () else request:set-attribute('response.contents', $result)

return $result