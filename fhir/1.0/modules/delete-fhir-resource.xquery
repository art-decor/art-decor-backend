xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adfhir      = "http://art-decor.org/ns/fhir/1.0" at "../api/api-fhir.xqm";
declare namespace f                 = "http://hl7.org/fhir";
declare namespace error             = "http://art-decor.org/ns/fhir/error";

let $type       := request:get-attribute('request.resource')
let $id         := request:get-attribute('request.id')

(:
    TODO:
    - Permissions
    - Return HTTP Location header with final location
    - Return correct HTTP statuses
    - Return OperationOutcome if necessary
:)
let $result             :=
    try {
        let $update             := adfhir:deleteResource($type,$id,adfhir:getIfMatches())
        let $headers            := <header name="Location" value="{$update/@location}"/> |
                                   <header name="ETag" value="W/&quot;{$update/@version}&quot;"/> |
                                   <header name="Last-Modified" value="{adfhir:dateTime2httpDate($update/@lastupdated)}"/>
        let $r                  := request:set-attribute('response.headers',$headers)
        let $r                  := request:set-attribute('response.http-status',204)
        return ()
    }
    catch error:ResourceNotFound {
        let $desc   := concat('ERROR ',$err:code,'. Could not delete. ',$err:description)
        let $r      := request:set-attribute('response.http-status',404)
        return adfhir:operationOutCome('error',(),$desc,())
    }
    catch error:ResourceNotCurrent {
        let $desc   := concat('ERROR ',$err:code,'. Could not delete. ',$err:description)
        let $r      := request:set-attribute('response.http-status',412)
        return adfhir:operationOutCome('error',(),$desc,())
    }
    catch * {
        let $desc   := concat('ERROR ',$err:code,'. Could not delete. ',$err:description,' module: ',$err:module,' (',$err:line-number,' ',$err:column-number,')')
        let $r      := request:set-attribute('response.http-status',500)
        return adfhir:operationOutCome('error',(),$desc,())
    }

let $r                          := if (empty($result)) then () else request:set-attribute('response.contents', $result)

return $result