xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adfhir      = "http://art-decor.org/ns/fhir/1.0" at "../api/api-fhir.xqm";
import module namespace getf        = "http://art-decor.org/ns/fhir-settings" at "api/fhir-settings.xqm";
declare namespace f                 = "http://hl7.org/fhir";

let $resourceName       := request:get-attribute('request.resource')
let $id                 := request:get-attribute('request.id')
let $version            := request:get-attribute('request.version')
let $result             := 
    try {
        let $headers    := <header name="Content-Type" value="application/xml+fhir; charset=utf-8"/>
        let $r          := request:set-attribute('response.headers',$headers)
        
        return
        adfhir:getResourceMeta($resourceName,$id,$version)
    }
    catch * {
        let $desc   := concat('ERROR ',$err:code,'. Could not retrieve meta. ',$err:description,' module: ',$err:module,' (',$err:line-number,' ',$err:column-number,')')
        let $r      := request:set-attribute('response.http-status',500)
        
        return adfhir:operationOutCome('error',(),$desc,())
    }

let $r                  := if (empty($result)) then () else request:set-attribute('response.contents', $result)
(:return single result. either single ValueSet wrapped in an entry/body, or a Bundle we've created:)
return
    $result