xquery version "3.1";
import module namespace adfhir      = "http://art-decor.org/ns/fhir/1.0" at "api/api-fhir.xqm";
import module namespace getf        = "http://art-decor.org/ns/fhir-settings" at "api/fhir-settings.xqm";
(:http://demo.exist-db.org/exist/apps/doc/urlrewrite.xml#D2.2.5:)
declare variable $exist:path external;
declare variable $exist:resource external;
declare variable $exist:controller external;

(:~
 : A list of regular expressions to check which external hosts are
 : allowed to access this ART-DECOR API instance. The check is done
 : against the Origin header sent by the browser.
 :)
declare variable $origin-whitelist  := ("(?:https?://localhost:.*|https?://127.0.0.1:.*)");
declare variable $allowOrigin       := local:allowOriginDynamic(request:get-header("Origin"));
declare variable $allowMethods      := 'GET, POST, DELETE, PUT, PATCH, OPTIONS';
declare variable $allowHeaders      := 'Content-Type, api_key, Authorization';
declare variable $exposeHeaders     := 'pb-start, pb-total';

declare function local:allowOriginDynamic($origin as xs:string?) {
    let $origin := replace($origin, "^(\w+://[^/]+).*$", "$1")
    return
        if (local:checkOriginWhitelist($origin-whitelist, $origin)) then
            $origin
        else
            "*"
};

declare function local:checkOriginWhitelist($regexes, $origin) {
    if (empty($regexes)) then
        false()
    else if (matches($origin, head($regexes))) then
        true()
    else
        local:checkOriginWhitelist(tail($regexes), $origin)
};

let $_debug                     := request:get-header('X-Request-Echo')='true' or request:get-parameter('_debug','false')='true'
(:let $_debug                     := true():)
let $_supportedResources        := adfhir:supportedResources()
let $_request-method            := request:get-method()
let $_request-path              := replace(util:unescape-uri($exist:path,'UTF-8'),'//','/')
let $_request-query             := request:get-query-string()
let $_request-hasquery          := count(request:get-parameter-names()[not(. = ($getf:PARAM_FORMAT, $getf:PARAM_COUNT, $getf:PARAM_SORT, $getf:PARAM_SORT_ASC, $getf:PARAM_SORT_DESC, $getf:PARAM_DEBUG, 'fhirLinkItemStyle'))]) > 0
let $_response-format           := adfhir:getResponseFormats()
let $_response-offset           := request:get-parameter('search-offset',())[. castable as xs:integer]
let $_response-count            := request:get-parameter('_count',())[. castable as xs:integer]

let $corsAttributes             := request:set-attribute('response.cors.allow-origin', $allowOrigin)
let $corsAttributes             := request:set-attribute('response.cors.allow-methods', $allowMethods)
let $corsAttributes             := request:set-attribute('response.cors.allow-headers', $allowHeaders)
let $corsAttributes             := request:set-attribute('response.cors.expose-headers', $exposeHeaders)

let $defaultRequestAttributes   := request:set-attribute('request.hasquerystring', $_request-hasquery)
let $defaultRequestAttributes   := request:set-attribute('response.format', $_response-format)
let $defaultRequestAttributes   := request:set-attribute('response.offset', $_response-offset)
let $defaultRequestAttributes   := request:set-attribute('response.count', $_response-count)
(: This setting influences if any security event is written to the db. Default 'onerror'. Other options: none, always :)
let $defaultRequestAttributes   := request:set-attribute('securityevent.create', $getf:SECURITY_SAVE_ONERROR)

(:CRUD = Create, Read, Update, Delete:)
let $_request-crud              := 
    if ($_request-method=('POST')) then (
        if (contains($_request-path,'_meta'))   then    'U'
        else 
        if (contains($_request-path,'_search')) then    'R' 
        else                                            'C'
    )
    else if ($_request-method=('GET','OPTIONS'))
    then 'R'
    else if ($_request-method=('PUT'))
    then 'U'
    else if ($_request-method=('DELETE'))
    then 'D'
    else ()
let $_request-parts         := <parts crudop="{$_request-crud}"/>

return
try {
   let $_request-parts          := adfhir:getRequestParts($_request-method, $_request-path, $_request-query, $_request-hasquery)
   let $_request-projectprefix  := $_request-parts/@prefix/string()
   let $_request-projectversion := $_request-parts/@version/string()
   let $_request-resource       := $_request-parts/@rname/string()
   let $_request-id             := $_request-parts/@rid/string()
   let $_request-version        := $_request-parts/@rvers/string()
   
   (:read, vread, update, create, history-instance, conformance etc.:)
   let $_request-logical       := $_request-parts/@logop/string()
   
   let $defaultRequestAttributes   := (
           request:set-attribute('request.parts',$_request-parts),
           request:set-attribute('response.http-status',$getf:STATUS_HTTP_200_OK),
           request:set-attribute('request.type',$_request-parts/@crudop/string()),
           request:set-attribute('request.subtype',$_request-parts/@logop/string()),
           request:set-attribute('request.path',$_request-path),
           request:set-attribute('request.source.address',request:get-remote-host()),
           
           request:set-attribute('request.projectprefix', $_request-projectprefix),
           request:set-attribute('request.projectversion', $_request-projectversion),
           request:set-attribute('request.resource', $_request-resource),
           request:set-attribute('request.id', $_request-id),
           request:set-attribute('request.version', $_request-version)
       )
    
    return
    if ($_debug) then (
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/modules/get-request-echo.xquery">
                <set-header name="Access-Control-Allow-Origin" value="{$allowOrigin}"/>
                { if ($allowOrigin = "*") then () else <set-header name="Access-Control-Allow-Credentials" value="true"/> }
                <set-header name="Access-Control-Allow-Methods" value="{$allowMethods}"/>
                <set-header name="Access-Control-Allow-Headers" value="{$allowHeaders}"/>
                <set-header name="Access-Control-Expose-Headers" value="{$exposeHeaders}"/>
                <set-header name="Cache-Control" value="no-cache"/>
            </forward>
        </dispatch>
    )
    else if (not($_response-format=($getf:CT_FHIR_XML, $getf:CT_FHIR_JSON))) then (
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/modules/create-fhir-OperationOutcome.xquery">
                <set-attribute name="operationoutcome.severity" value="error"/>
                <set-attribute name="operationoutcome.text" value="{concat('Unsupported response format ''',$_response-format,'''. This server supports ',$getf:CT_FHIR_XML, ' or ', $getf:CT_FHIR_JSON)}"/>
            </forward>
            <view>
                <forward url="{$exist:controller}/modules/create-fhir-SecurityEvent.xquery">
                    <clear-attribute name="operationoutcome.severity"/>
                    <clear-attribute name="operationoutcome.text"/>
                    <set-attribute name="response.http-status" value="{$getf:STATUS_HTTP_406_NOT_ACCEPTABLE}"/>
                    <set-attribute name="response.format" value="{$getf:CT_FHIR_XML}"/>
                    <set-header name="Access-Control-Allow-Origin" value="{$allowOrigin}"/>
                    { if ($allowOrigin = "*") then () else <set-header name="Access-Control-Allow-Credentials" value="true"/> }
                    <set-header name="Access-Control-Allow-Methods" value="{$allowMethods}"/>
                    <set-header name="Access-Control-Allow-Headers" value="{$allowHeaders}"/>
                    <set-header name="Access-Control-Expose-Headers" value="{$exposeHeaders}"/>
                    <set-header name="Cache-Control" value="no-cache"/>
                </forward>
            </view>
        </dispatch>
    )
    else if ($_request-logical=$getf:RFINT_INDEX) then (
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/xforms/index.xhtml"/>
            <view>
                <forward url="{$exist:controller}/modules/create-fhir-SecurityEvent.xquery">
                    <set-header name="Access-Control-Allow-Origin" value="{$allowOrigin}"/>
                    { if ($allowOrigin = "*") then () else <set-header name="Access-Control-Allow-Credentials" value="true"/> }
                    <set-header name="Access-Control-Allow-Methods" value="{$allowMethods}"/>
                    <set-header name="Access-Control-Allow-Headers" value="{$allowHeaders}"/>
                    <set-header name="Access-Control-Expose-Headers" value="{$exposeHeaders}"/>
                    <set-header name="Cache-Control" value="no-cache"/>
                </forward>
            </view>
        </dispatch>
    )
    else if (not($_request-resource = $_supportedResources) and $_supportedResources[lower-case(.)=lower-case($_request-resource)]) then (
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/modules/create-fhir-OperationOutcome.xquery">
                <set-attribute name="operationoutcome.severity" value="error"/>
                <set-attribute name="operationoutcome.text" value="{concat('Wrong casing of collection name, try ''',$_supportedResources[lower-case(.)=lower-case($_request-resource)],''' instead.')}"/>
            </forward>
            <view>
                <forward url="{$exist:controller}/modules/create-fhir-SecurityEvent.xquery">
                    <clear-attribute name="operationoutcome.severity"/>
                    <clear-attribute name="operationoutcome.text"/>
                    <set-attribute name="response.http-status" value="{$getf:STATUS_HTTP_404_NOT_FOUND}"/>
                    <set-header name="Access-Control-Allow-Origin" value="{$allowOrigin}"/>
                    { if ($allowOrigin = "*") then () else <set-header name="Access-Control-Allow-Credentials" value="true"/> }
                    <set-header name="Access-Control-Allow-Methods" value="{$allowMethods}"/>
                    <set-header name="Access-Control-Allow-Headers" value="{$allowHeaders}"/>
                    <set-header name="Access-Control-Expose-Headers" value="{$exposeHeaders}"/>
                    <set-header name="Cache-Control" value="no-cache"/>
                </forward>
            </view>
        </dispatch>
    )
    else if (not($_request-resource = $_supportedResources)) then (
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/modules/create-fhir-OperationOutcome.xquery">
                <set-attribute name="operationoutcome.severity" value="error"/>
                <set-attribute name="operationoutcome.text" value="{concat('This server does not support this resource &quot;',$_request-resource,'&quot;, try any of ''',string-join($_supportedResources,', '),''' instead.')}"/>
            </forward>
            <view>
                <forward url="{$exist:controller}/modules/create-fhir-SecurityEvent.xquery">
                    <clear-attribute name="operationoutcome.severity"/>
                    <clear-attribute name="operationoutcome.text"/>
                    <set-attribute name="response.http-status" value="{$getf:STATUS_HTTP_404_NOT_FOUND}"/>
                    <set-header name="Access-Control-Allow-Origin" value="{$allowOrigin}"/>
                    { if ($allowOrigin = "*") then () else <set-header name="Access-Control-Allow-Credentials" value="true"/> }
                    <set-header name="Access-Control-Allow-Methods" value="{$allowMethods}"/>
                    <set-header name="Access-Control-Allow-Headers" value="{$allowHeaders}"/>
                    <set-header name="Access-Control-Expose-Headers" value="{$exposeHeaders}"/>
                    <set-header name="Cache-Control" value="no-cache"/>
                </forward>
            </view>
        </dispatch>
    )
    (: **** RESOURCE START **** :)
    (: **** SEARCH-SYSTEM START **** :)
    else if (empty($_request-resource) and $_request-logical=($getf:RFINT_READ,$getf:RFINT_SEARCH_SYSTEM)) then (
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/modules/get-fhir-resource.xquery"/>
            <view>
                <forward url="{$exist:controller}/modules/create-fhir-SecurityEvent.xquery">
                    <set-header name="Access-Control-Allow-Origin" value="{$allowOrigin}"/>
                    { if ($allowOrigin = "*") then () else <set-header name="Access-Control-Allow-Credentials" value="true"/> }
                    <set-header name="Access-Control-Allow-Methods" value="{$allowMethods}"/>
                    <set-header name="Access-Control-Allow-Headers" value="{$allowHeaders}"/>
                    <set-header name="Access-Control-Expose-Headers" value="{$exposeHeaders}"/>
                    <set-header name="Cache-Control" value="no-cache"/>
                </forward>
            </view>
        </dispatch>
    )
    (: **** SEARCH-SYSTEM END **** :)
    (: **** READ/VREAD/SEARCH-TYPE / PAGE REQUEST START **** :)
    else if ($_request-logical=($getf:RFINT_READ,$getf:RFINT_VREAD,$getf:RFINT_SEARCH_TYPE,$getf:RFINT_CONFORMANCE)) then (
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/modules/get-fhir-resource.xquery"/>
            <view>
                <forward url="{$exist:controller}/modules/create-fhir-SecurityEvent.xquery">
                    <set-header name="Access-Control-Allow-Origin" value="{$allowOrigin}"/>
                    { if ($allowOrigin = "*") then () else <set-header name="Access-Control-Allow-Credentials" value="true"/> }
                    <set-header name="Access-Control-Allow-Methods" value="{$allowMethods}"/>
                    <set-header name="Access-Control-Allow-Headers" value="{$allowHeaders}"/>
                    <set-header name="Access-Control-Expose-Headers" value="{$exposeHeaders}"/>
                    <set-header name="Cache-Control" value="no-cache"/>
                </forward>
            </view>
        </dispatch>
    )
    (: **** READ/VREAD/SEARCH / PAGE REQUEST END **** :)
    (: **** CREATE/UPDATE START **** :)
    (:else if ($_request-logical=($getf:RFINT_CREATE,$getf:RFINT_UPDATE)) then (
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/modules/save-fhir-resource.xquery"/>
            <view>
                <forward url="{$exist:controller}/modules/create-fhir-SecurityEvent.xquery">
                    <set-header name="Access-Control-Allow-Origin" value="{$allowOrigin}"/>
                    { if ($allowOrigin = "*") then () else <set-header name="Access-Control-Allow-Credentials" value="true"/> }
                    <set-header name="Access-Control-Allow-Methods" value="{$allowMethods}"/>
                    <set-header name="Access-Control-Allow-Headers" value="{$allowHeaders}"/>
                    <set-header name="Access-Control-Expose-Headers" value="{$exposeHeaders}"/>
                    <set-header name="Cache-Control" value="no-cache"/>
                </forward>
            </view>
        </dispatch>
    ):)
    (: **** CREATE/UPDATE END **** :)
    (: **** DELETE START **** :)
    (:else if ($_request-logical=($getf:RFINT_DELETE)) then (
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/modules/delete-fhir-resource.xquery"/>
            <view>
                <forward url="{$exist:controller}/modules/create-fhir-SecurityEvent.xquery">
                    <set-header name="Access-Control-Allow-Origin" value="{$allowOrigin}"/>
                    { if ($allowOrigin = "*") then () else <set-header name="Access-Control-Allow-Credentials" value="true"/> }
                    <set-header name="Access-Control-Allow-Methods" value="{$allowMethods}"/>
                    <set-header name="Access-Control-Allow-Headers" value="{$allowHeaders}"/>
                    <set-header name="Access-Control-Expose-Headers" value="{$exposeHeaders}"/>
                    <set-header name="Cache-Control" value="no-cache"/>
                </forward>
            </view>
        </dispatch>
    ):)
    (: **** DELETE END **** :)
    (: **** RESOURCE END **** :)
    
    (: **** META START **** :)
    (: **** READ/VREAD START **** :)
    (:else if ($_request-logical=($getf:RFINT_READ_META,$getf:RFINT_VREAD_META)) then (
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/modules/get-fhir-meta.xquery"/>
            <view>
                <forward url="{$exist:controller}/modules/create-fhir-SecurityEvent.xquery">
                    <set-header name="Access-Control-Allow-Origin" value="{$allowOrigin}"/>
                    { if ($allowOrigin = "*") then () else <set-header name="Access-Control-Allow-Credentials" value="true"/> }
                    <set-header name="Access-Control-Allow-Methods" value="{$allowMethods}"/>
                    <set-header name="Access-Control-Allow-Headers" value="{$allowHeaders}"/>
                    <set-header name="Access-Control-Expose-Headers" value="{$exposeHeaders}"/>
                    <set-header name="Cache-Control" value="no-cache"/>
                </forward>
            </view>
        </dispatch>
    ):)
    (: **** READ/VREAD END **** :)
    (: **** CREATE/UPDATE START **** :)
    (:else if ($_request-logical=($getf:RFINT_CREATE_META,$getf:RFINT_UPDATE_META)) then (
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/modules/save-fhir-meta.xquery"/>
            <view>
                <forward url="{$exist:controller}/modules/create-fhir-SecurityEvent.xquery">
                    <set-header name="Access-Control-Allow-Origin" value="{$allowOrigin}"/>
                    { if ($allowOrigin = "*") then () else <set-header name="Access-Control-Allow-Credentials" value="true"/> }
                    <set-header name="Access-Control-Allow-Methods" value="{$allowMethods}"/>
                    <set-header name="Access-Control-Allow-Headers" value="{$allowHeaders}"/>
                    <set-header name="Access-Control-Expose-Headers" value="{$exposeHeaders}"/>
                    <set-header name="Cache-Control" value="no-cache"/>
                </forward>
            </view>
        </dispatch>
    ):)
    (: **** CREATE/UPDATE END **** :)
    (: **** DELETE START **** :)
    (:else if ($_request-logical=($getf:RFINT_DELETE_META)) then (
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/modules/delete-fhir-meta.xquery"/>
            <view>
                <forward url="{$exist:controller}/modules/create-fhir-SecurityEvent.xquery">
                    <set-header name="Access-Control-Allow-Origin" value="{$allowOrigin}"/>
                    { if ($allowOrigin = "*") then () else <set-header name="Access-Control-Allow-Credentials" value="true"/> }
                    <set-header name="Access-Control-Allow-Methods" value="{$allowMethods}"/>
                    <set-header name="Access-Control-Allow-Headers" value="{$allowHeaders}"/>
                    <set-header name="Access-Control-Expose-Headers" value="{$exposeHeaders}"/>
                    <set-header name="Cache-Control" value="no-cache"/>
                </forward>
            </view>
        </dispatch>
    ):)
    (: **** DELETE END **** :)
    (: **** META END **** :)
    
    (: **** FALLBACK if nothing matches **** :)
    else (
        (:Method Not Allowed - everything else is error:)
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/modules/create-fhir-OperationOutcome.xquery">
                <set-attribute name="operationoutcome.severity" value="error"/>
                <set-attribute name="operationoutcome.text" value="{concat('Unsupported method ''',request:get-method(),''' for this path ''',$exist:path,''' and these parameters ''',
                    string-join(
                        for $n in request:get-parameter-names() 
                        for $v in request:get-parameter($n,()) 
                        return concat($n,'=',encode-for-uri($v))
                    ,'&amp;amp;'),'''')}"/>
            </forward>
            <view>
                <forward url="{$exist:controller}/modules/create-fhir-SecurityEvent.xquery">
                    <clear-attribute name="operationoutcome.severity"/>
                    <clear-attribute name="operationoutcome.text"/>
                    <set-attribute name="response.http-status" value="{$getf:STATUS_HTTP_405_METHOD_NOT_ALLOWED}"/>
                    <set-header name="Access-Control-Allow-Origin" value="{$allowOrigin}"/>
                    { if ($allowOrigin = "*") then () else <set-header name="Access-Control-Allow-Credentials" value="true"/> }
                    <set-header name="Access-Control-Allow-Methods" value="{$allowMethods}"/>
                    <set-header name="Access-Control-Allow-Headers" value="{$allowHeaders}"/>
                    <set-header name="Access-Control-Expose-Headers" value="{$exposeHeaders}"/>
                    <set-header name="Cache-Control" value="no-cache"/>
                </forward>
            </view>
        </dispatch>
    )
}
catch * {
    let $defaultRequestAttributes   := (
            request:set-attribute('response.http-status',$getf:STATUS_HTTP_500_INTERNAL_ERROR),
            request:set-attribute('request.type',$_request-parts/@crudop/string()),
            request:set-attribute('request.subtype',$_request-parts/@logop/string()),
            request:set-attribute('request.path',$_request-path),
            request:set-attribute('request.source.address',request:get-remote-host())
        )

    return
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/modules/create-fhir-OperationOutcome.xquery">
            <set-attribute name="operationoutcome.severity" value="error"/>
            <set-attribute name="operationoutcome.text" value="{concat('Could not process ''',request:get-method(),''' for: ',string-join(($exist:path,$_request-query),'?'),'. ', $err:description,' module: ',$err:module,' (',$err:line-number,' ',$err:column-number,')')}"/>
        </forward>
        <view>
            <forward url="{$exist:controller}/modules/create-fhir-SecurityEvent.xquery">
                <clear-attribute name="operationoutcome.severity"/>
                <clear-attribute name="operationoutcome.text"/>
                <set-header name="Access-Control-Allow-Origin" value="{$allowOrigin}"/>
                { if ($allowOrigin = "*") then () else <set-header name="Access-Control-Allow-Credentials" value="true"/> }
                <set-header name="Access-Control-Allow-Methods" value="{$allowMethods}"/>
                <set-header name="Access-Control-Allow-Headers" value="{$allowHeaders}"/>
                <set-header name="Access-Control-Expose-Headers" value="{$exposeHeaders}"/>
                <set-header name="Cache-Control" value="no-cache"/>
            </forward>
        </view>
    </dispatch>
}
