xquery version "3.1";

declare variable $exist:controller external;
declare variable $exist:path external;
declare variable $exist:resource external;
declare variable $exist:root external;

(:~
 : A list of regular expressions to check which external hosts are
 : allowed to access this ART-DECOR API instance. The check is done
 : against the Origin header sent by the browser.
 :)
declare variable $origin-whitelist  := ("(?:https?://localhost:.*|https?://127.0.0.1:.*)");
declare variable $allowOrigin       := local:allowOriginDynamic(request:get-header("Origin"));

declare function local:allowOriginDynamic($origin as xs:string?) {
    let $origin := replace($origin, "^(\w+://[^/]+).*$", "$1")
    return
        if (local:checkOriginWhitelist($origin-whitelist, $origin)) then
            $origin
        else
            "*"
};

declare function local:checkOriginWhitelist($regexes, $origin) {
    if (empty($regexes)) then
        false()
    else if (matches($origin, head($regexes))) then
        true()
    else
        local:checkOriginWhitelist(tail($regexes), $origin)
};

let $var    := request:set-attribute("exist-path", $exist:path)

return
<dispatch xmlns="http://exist.sourceforge.net/NS/exist">
    <forward url="{$exist:controller}/helper.xq">
        <set-header name="Access-Control-Allow-Origin" value="{$allowOrigin}"/>
        { if ($allowOrigin = "*") then () else <set-header name="Access-Control-Allow-Credentials" value="true"/> }
        <set-header name="Access-Control-Allow-Methods" value="GET, POST, DELETE, PUT, PATCH, OPTIONS"/>
        <set-header name="Access-Control-Allow-Headers" value="Content-Type, api_key, Authorization"/>
        <set-header name="Access-Control-Expose-Headers" value="pb-start, pb-total"/>
        <set-header name="Cache-Control" value="no-cache"/>
    </forward>
</dispatch>