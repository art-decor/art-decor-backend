xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

module namespace adfhirsd           = "http://art-decor.org/ns/fhir/1.0/structuredefinition";
import module namespace adfhir      = "http://art-decor.org/ns/fhir/1.0" at "api-fhir.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../../art/api/api-server-settings.xqm";
import module namespace getf        = "http://art-decor.org/ns/fhir-settings" at "fhir-settings.xqm";
import module namespace utillib     = "http://art-decor.org/ns/api/util" at "../../../api/modules/library/util-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "../../../api/modules/library/settings-lib.xqm";
import module namespace vs          = "http://art-decor.org/ns/decor/valueset" at "../../../art/api/api-decor-valueset.xqm";
import module namespace templ       = "http://art-decor.org/ns/decor/template" at "../../../art/api/api-decor-template.xqm";
declare namespace f                 = "http://hl7.org/fhir";
declare namespace error             = "http://art-decor.org/ns/fhir/error";

declare %private variable $adfhirsd:type                := 'StructureDefinition';
declare %private variable $adfhirsd:inactiveStatusCodes := ('cancelled','rejected','deprecated');

declare function adfhirsd:convertDecorDataset2FHIRStructureDefinition($datasetOrTransaction as element()) as element() {
let $isTransaction  := exists($datasetOrTransaction/@transactionId)
let $decorId        := if ($isTransaction) then $datasetOrTransaction/@transactionId else $datasetOrTransaction/@id
let $decorEffectiveDate 
                    := if ($isTransaction) then $datasetOrTransaction/@transactionEffectiveDate else $datasetOrTransaction/@effectiveDate
let $decorStatusCode
                    := if ($isTransaction) then $datasetOrTransaction/@transactionStatusCode else $datasetOrTransaction/@statusCode
let $decorExpirationDate 
                    := if ($isTransaction) then $datasetOrTransaction/@transactionExpirationDate else $datasetOrTransaction/@expirationDate
let $decorVersionLabel
                    := if ($isTransaction) then $datasetOrTransaction/@transactionVersionLabel else $datasetOrTransaction/@versionLabel
let $fhirId         := concat($decorId,'--',replace($decorEffectiveDate,'[^\d]',''))
let $maps           := 
    for $codeSystem in $datasetOrTransaction//@codeSystem
    let $csid := $codeSystem
    let $csed := $codeSystem/../@codeSystemVersion
    group by $csid, $csed
    return 
        map:entry($csid || $csed, utillib:getCanonicalUriForOID('CodeSystem', $csid, $csed, $datasetOrTransaction/@prefix, $setlib:strKeyFHIRDSTU2))
let $oidfhirmap     := map:merge($maps)
let $language       := if ($datasetOrTransaction/*:name[@language = 'en-US']) then 'en-US' else data($datasetOrTransaction/*:name[1]/@language)

let $name           := 
    if ($datasetOrTransaction/*:name[@language = $language][node()]) then 
        $datasetOrTransaction/*:name[@language = $language]
    else 
        $datasetOrTransaction/*:name[node()][1]

let $shortName      := adfhir:shortName($name)
let $rootPath       := if ($datasetOrTransaction/*:concept[2]) then $shortName else ()

let $rootElem       := 
    if ($datasetOrTransaction/*:concept[2]) then (
        <element id="{$fhirId}" xmlns="http://hl7.org/fhir">
            <path value="{$rootPath}"/>
            <short value="{replace($name, '(^\s+)|(\s+$)', '')}">
            {
                for $node in $datasetOrTransaction/*:name[not(@language = $language)][text()[string-length()>0]]
                return
                    <extension url="http://hl7.org/fhir/StructureDefinition/translation">
                        <extension url="lang">
                            <valueString value="{$node/@language}"/>
                        </extension>
                        <extension url="content">
                            <valueString value="{$node}"/>
                        </extension>
                    </extension>
            }
            </short>
        {
            if ($datasetOrTransaction[*:desc[node()]]) then 
                <definition value="{utillib:serializeNode($datasetOrTransaction/*:desc[node()][1])/text()}">
                {
                    for $node in $datasetOrTransaction/*:desc[not(@language = $language)][text()[string-length()>0]]
                    return
                        <extension url="http://hl7.org/fhir/StructureDefinition/translation">
                            <extension url="lang">
                                <valueString value="{$node/@language}"/>
                            </extension>
                            <extension url="content">
                                <valueString value="{$node}"/>
                            </extension>
                        </extension>
                }
                </definition> 
            else <definition value="-"/>
        }
            <min value="1"/>
            <max value="1"/>
            <mustSupport value="true"/>
        </element>
    ) else ()

let $prefix         := ($datasetOrTransaction/@prefix | $datasetOrTransaction/ancestor::decor/project/@prefix)[1]

(:FHIR id may not have colons. This affects using @effectiveDate as part of the Resource.id
    Remove any non-digit from the effectiveDate before adding it to the URI, e.g. 20161026123456
:)
return
(:<StructureDefinition xmlns="http://hl7.org/fhir" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:schemaLocation="http://hl7.org/fhir http://hl7.org/fhir/structuredefinition.xsd">:)
<StructureDefinition xmlns="http://hl7.org/fhir">
    <id value="{$fhirId}"/>
    <meta>
        <profile value="http://hl7.org/fhir/{$getf:strFhirVersionShort}/StructureDefinition/StructureDefinition"/>
    </meta>
    <language value="{$language}"/>
{
    (: Any object in ART-DECOR is effective from effectiveDate to expirationDate :)
    if ($datasetOrTransaction[@effectiveDate castable as xs:dateTime] | $datasetOrTransaction[@expirationDate castable as xs:dateTime]) then (
        <extension url="http://hl7.org/fhir/StructureDefinition/resource-effectivePeriod">
            <valuePeriod>
            {
                if ($datasetOrTransaction[@effectiveDate castable as xs:dateTime]) then 
                    <start value="{adjust-dateTime-to-timezone(xs:dateTime($decorEffectiveDate))}"/>
                else (),
                if ($datasetOrTransaction[@expirationDate castable as xs:dateTime]) then
                    <end value="{adjust-dateTime-to-timezone(xs:dateTime($decorExpirationDate))}"/>
                else ()
            }
            </valuePeriod>
        </extension>
    ) else ()
}
{
    let $projectprefix      := request:get-attribute('request.projectprefix')[string-length()>0]
    let $projectversion     := request:get-attribute('request.projectversion')[string-length()>0]
    return
    comment {
        let $str := concat($getf:strFhirServices, $getf:strFhirVersionShort, '/', string-join(($projectprefix, replace($projectversion, '[^\d]', '')), ''), '/', string-join(($prefix, replace($datasetOrTransaction/@versionDate, '[^\d]', '')), ''), '/StructureDefinition/', $fhirId)
        return concat(' ', replace(replace($str, '--', '-\\-'), '//', '/'),' ')
    }
}
    <url value="{concat($getf:strFhirServices,'StructureDefinition/', $fhirId)}"/>
    <identifier>
        <use value="official"/>
        <system value="urn:ietf:rfc:3986"/>
        <value value="urn:oid:{$decorId}"/>
    </identifier>
    <version value="{let $semver := adfhir:getSemverString($decorVersionLabel) return if (empty($semver)) then $decorEffectiveDate else $semver}"/>
    <name value="{$name}">
    {
        for $node in $datasetOrTransaction/*:name[not(@language = $language)][text()[string-length()>0]]
        return
            <extension url="http://hl7.org/fhir/StructureDefinition/translation">
                <extension url="lang">
                    <valueString value="{$node/@language}"/>
                </extension>
                <extension url="content">
                    <valueString value="{$node}"/>
                </extension>
            </extension>
    }
    </name>
    <status value="{adfhirsd:decorStatus2fhirStatus($decorStatusCode)}"/>
    {
        if ($datasetOrTransaction[*:desc[@language = $language][node()]]) then 
            <description value="{utillib:serializeNode($datasetOrTransaction/*:desc[@language = $language][1])/text()}">
            {
                for $node in $datasetOrTransaction/*:desc[not(@language = $language)][text()[string-length()>0]]
                return
                    <extension url="http://hl7.org/fhir/StructureDefinition/translation">
                        <extension url="lang">
                            <valueString value="{$node/@language}"/>
                        </extension>
                        <extension url="content">
                            <valueString value="{$node}"/>
                        </extension>
                    </extension>
            }
            </description>
        else ()
    }
    <fhirVersion value="{$getf:strFhirVersion}"/>
    <kind value="logical"/>
    <abstract value="true"/>
    <snapshot>
    {
        $rootElem,
        if ($isTransaction) then 
            for $concept in $datasetOrTransaction/*:concept
            return
                adfhirsd:convertDecorConcept2FHIRElementDefinition($concept, $rootPath, $prefix, $language, $isTransaction)
        else (
            for $concept in $datasetOrTransaction/*:concept[not(@statusCode = $adfhirsd:inactiveStatusCodes)]
            return
                adfhirsd:convertDecorConcept2FHIRElementDefinition($concept, $rootPath, $prefix, $language, $isTransaction)
        )
    }
    </snapshot>
    {
        ()(:$datasetOrTransaction:)
    }
</StructureDefinition>
};

declare function adfhirsd:convertDecorConcept2FHIRElementDefinition($concept as element(concept), $parentPath as xs:string?, $prefix as xs:string?, $language as xs:string, $isTransaction as xs:boolean) as element()* {
    let $originalConcept    := utillib:getOriginalForConcept($concept)
    let $name               := 
        if ($originalConcept/*:name[@language = $language]) then 
            $originalConcept/*:name[@language = $language]
        else 
            $originalConcept/*:name[node()][1]
    
    let $shortName          := adfhir:shortName($name)
    let $shortName          := if (empty($parentPath)) then $shortName else lower-case(substring($shortName, 1, 1)) || substring($shortName, 2)
    let $id                 := 
        if ($concept[@id]) then
            $concept/concat(@id,'--',replace(@effectiveDate,'[^\d]',''))
        else
            $concept/concat(@ref,'--',replace(@flexibility,'[^\d]',''))
    let $path               := string-join(($parentPath,$shortName),'.')
    let $assocs             := 
        if ($concept[@shortName]) then
            $concept/*:terminologyAssociation | 
            $concept/*:valueDomain//*:terminologyAssociation | 
            $concept/*:valueSet//*:terminologyAssociation
        else
            utillib:getConceptAssociations($concept, $originalConcept, false())/*
    (: utillib:getGetEnhancedValueset does the heavy lifting, but could produce an empty valueSet element, so check that before relying on it. :)
    let $valueSets          := 
        if ($concept[*:valueSet[@id | @ref]]) then
            $concept/*:valueSet
        else (
            for $assoc in $assocs[@valueSet]
            return vs:getValueSetById($assoc/@valueSet, if ($assoc/@flexibility) then $assoc/@flexibility else 'dynamic', false())//*:valueSet[@id]
        )
    
    return (
        <element id="{$id}" xmlns="http://hl7.org/fhir">
        {
            let $valueDomain        := if ($originalConcept/*:valueDomain[2]) then () else $originalConcept/*:valueDomain[1]
            let $dataType           := adfhirsd:decorValueDomainType2FHIRtype($valueDomain/@type)
            let $properties         := $valueDomain/*:property[@unit]
            
            return
                if ($dataType = ('Quantity', 'Duration', 'Count')) then 
                    for $property in $properties
                    let $isUcumUnit := $getf:CS_UCUMCOMMONUNITS[@unit = $property/@unit][@message = 'OK']
                    return
                    <extension url="http://hl7.org/fhir/StructureDefinition/elementdefinition-allowedUnits">
                        <valueCodeableConcept>
                        {
                            if ($isUcumUnit) then
                                <coding>
                                    <system value="http://unitsofmeasure.org/" />
                                    <code value="{$property/@unit}" />
                                </coding>
                            else (
                                <text value="{$property/@unit}"/>
                            )
                        }
                        </valueCodeableConcept>
                    </extension>
                else ()
        }
            <path value="{$path}"/>
        {
            (: http://hl7.org/fhir/R4/structuredefinition.html#invs sdf-9 disallows this on base :)
            if (contains($path, '.')) then 
                for $assoc in $assocs[@conceptId=$concept/@id][@code][empty(@expirationDate)] | $assocs[@conceptId=$originalConcept/@id][@code][empty(@expirationDate)]
                return
                    <code>
                        <system value="{utillib:getCanonicalUriForOID('CodeSystem', $assoc/@codeSystem, $assoc/@codeSystemVersion, $prefix, $setlib:strKeyFHIRDSTU2)}"/>
                        <code value="{$assoc/@code}"/>
                    {
                        if ($assoc/@displayName) then
                            <display value="{$assoc/@displayName}"/>
                        else ()
                    }
                    </code>
            else ()
        }
            <short value="{$name}">
            {
                for $node in $originalConcept/*:name[not(@language = $language)][text()[string-length()>0]]
                return
                    <extension url="http://hl7.org/fhir/StructureDefinition/translation">
                        <extension url="lang">
                            <valueString value="{$node/@language}"/>
                        </extension>
                        <extension url="content">
                            <valueString value="{$node}"/>
                        </extension>
                    </extension>
            }
            </short>
        {
            if ($originalConcept[*:desc[@language = $language][node()]]) then 
                <definition value="{utillib:serializeNode($originalConcept/*:desc[@language = $language][1])/text()}">
                {
                    for $node in $originalConcept/*:desc[not(@language = $language)][text()[string-length()>0]]
                    return
                        <extension url="http://hl7.org/fhir/StructureDefinition/translation">
                            <extension url="lang">
                                <valueString value="{$node/@language}"/>
                            </extension>
                            <extension url="content">
                                <valueString value="{$node}"/>
                            </extension>
                        </extension>
                }
                </definition>
            else (
                (: The following will be invalid in the StructureDefinition, but shows the value in the dataset is empty. :)
                <definition value="-"/>
            )
            ,
            if ($originalConcept[*:comment[@language = $language][node()]]) then 
                <comments value="{utillib:serializeNode($originalConcept/*:comment[@language = $language][1])/text()}">
                {
                    for $node in $originalConcept/*:comment[not(@language = $language)][text()[string-length()>0]]
                    return
                        <extension url="http://hl7.org/fhir/StructureDefinition/translation">
                            <extension url="lang">
                                <valueString value="{$node/@language}"/>
                            </extension>
                            <extension url="content">
                                <valueString value="{$node}"/>
                            </extension>
                        </extension>
                }
                </comments>
            else ()
            ,
            if ($originalConcept[*:rationale[@language = $language][node()]]) then 
                <requirements value="{utillib:serializeNode($originalConcept/*:rationale[@language = $language][1])/text()}">
                {
                    for $node in $originalConcept/*:rationale[not(@language = $language)][text()[string-length()>0]]
                    return
                        <extension url="http://hl7.org/fhir/StructureDefinition/translation">
                            <extension url="lang">
                                <valueString value="{$node/@language}"/>
                            </extension>
                            <extension url="content">
                                <valueString value="{$node}"/>
                            </extension>
                        </extension>
                }
                </requirements>
            else ()
            ,
            for $synonym in $originalConcept/*:synonym[text()]
            return
                <alias value="{$synonym}"/>
        }
            <min value="{utillib:getMinimumMultiplicity($concept)}"/>
            <max value="{utillib:getMaximumMultiplicity($concept)}"/>
        {
            if (empty($parentPath)) then ( (: sdf-15: The first element in a snapshot has no type :) ) else
            if ($concept[*:contains]) then
                <type>
                    <code value="Reference"/>
                    <profile value="{concat($getf:strFhirServices,'StructureDefinition/', $concept/*:contains/@ref, '--', replace($concept/*:contains/@flexibility,'[^\d]',''))}"/>
                </type>
            else
            if ($originalConcept[*:concept]) then
                <type>
                    <code value="BackboneElement"/>
                </type>
            else (
                for $valueDomain in $originalConcept/*:valueDomain
                return
                <type>
                    <code value="{adfhirsd:decorValueDomainType2FHIRtype($valueDomain/@type)}"/>
                </type>
            )
        }
        {
            let $valueDomain        := if ($originalConcept/*:valueDomain[2]) then () else $originalConcept/*:valueDomain[1]
            let $dataType           := adfhirsd:decorValueDomainType2FHIRtype($valueDomain/@type)
            let $properties         := if ($valueDomain/*:property[2]) then () else $valueDomain/*:property[1]
            
            return (
                if ($properties/@defaultValue) then (
                    <defaultValue value="{$properties/@defaultValue}"/>
                ) else ()
                ,
                if ($properties/@fixedValue) then (
                    <fixed value="{$properties/@fixedValue}"/>
                ) else ()
                ,
                if ($valueDomain/*:example[@type != 'error']) then (
                    adfhirsd:decorExample2FHIRexample($valueDomain/*:example[@type != 'error'][1], $dataType, $properties)
                ) else ()
                ,
                if ($properties[@minInclude | @maxInclude]) then (
                    adfhirsd:decorProperties2FHIRminmaxValue($properties[@minInclude | @maxInclude], $dataType)
                ) else ()
                ,
                if ($properties[@maxLength]) then (
                    <maxLength value="{$properties/@maxLength}"/>
                ) else ()
            )
        }
        {
            if ($concept[@isMandatory='true'] | $concept[@conformance='R']) then 
                <mustSupport value="true"/>
            else ()
        }
        {
            if ($valueSets[2]) then () else (
                for $valueSet in $valueSets
                return
                    <binding>
                        <strength value="required"/>
                        <valueSetReference>
                            <reference value="{adserver:getServerURLFhirCanonicalBase()}ValueSet/{$valueSet/@id}{$getf:PARAMDECOR_ID_VERSION_SEPCHARS}{$valueSet/replace(@effectiveDate,'[^\d]','')}"/>
                            <display value="{if ($valueSet/@displayName) then ($valueSet/@displayName) else ($valueSet/@name)}"/>
                        </valueSetReference>
                    </binding>
            )
        }
        {   ()
            (: This is not how mappings work so deactivated until we have a better solution :)
            (:if ($originalConcept[*:source]) then (
                <mapping>
                    <identity value="{generate-id($originalConcept/*:source[1])}"/>
                    <map value="{$originalConcept/*:source[1]}"/>
                </mapping>
            ) else ():)
        }
        </element>
        ,
        if ($valueSets[2]) then (
            (: In DSTU2 there is a 64 character limitation on @id. This limitation was lifted later. :)
            (: The definition is just to satisfy the StructureDefinition. :)
            <element id="{$id}.c" xmlns="http://hl7.org/fhir">
                <path value="{$path}.c" />
                <slicing>
                    <discriminator value="system"/>
                    <rules value="open" />
                </slicing>
                <definition value="-"/>
                <min value="1" />
                <max value="1"/>
            </element>
            ,
            for $valueSet at $i in $valueSets
            let $valueSetName   := $valueSet/@name
            return
            <element id="{$id}.{$i}" xmlns="http://hl7.org/fhir">
                <path value="{$path}.coding"/>
                <short value="{$valueSetName}"/>
            {
                if ($valueSet[*:desc[@language = $language][node()]]) then 
                    <definition value="{utillib:serializeNode($valueSet/*:desc[@language = $language][1])/text()}">
                    {
                        for $node in $valueSet/*:desc[not(@language = $language)][text()[string-length()>0]]
                        return
                            <extension url="http://hl7.org/fhir/StructureDefinition/translation">
                                <extension url="lang">
                                    <valueString value="{$node/@language}"/>
                                </extension>
                                <extension url="content">
                                    <valueString value="{$node}"/>
                                </extension>
                            </extension>
                    }
                    </definition>
                else (
                    (: The following is just to satisfy the StructureDefinition, and shows the description in the valueSet is empty. :)
                    <definition value="-"/>
                )
            }
                <min value="0" />
                <max value="1" />
                <binding>
                    <strength value="required"/>
                    <description value="{$valueSetName}"/>
                    <valueSetReference>
                        <reference value="{adserver:getServerURLFhirCanonicalBase()}ValueSet/{$valueSet/@id}{$getf:PARAMDECOR_ID_VERSION_SEPCHARS}{$valueSet/replace(@effectiveDate,'[^\d]','')}"/>
                        <display value="{if ($valueSet/@displayName) then ($valueSet/@displayName) else ($valueSet/@name)}"/>
                    </valueSetReference>
                </binding>
            </element>
        ) else ()
        ,
        if ($isTransaction) then (
            for $subconcept in $concept/*:concept
            return 
                adfhirsd:convertDecorConcept2FHIRElementDefinition($subconcept, $path, $prefix, $language, $isTransaction)
        )
        else (
            for $subconcept in $concept/*:concept
            return (
                if ($subconcept[@statusCode = $adfhirsd:inactiveStatusCodes]) then () else
                    adfhirsd:convertDecorConcept2FHIRElementDefinition($subconcept, $path, $prefix, $language, $isTransaction)
            )
        )
    )
};

declare function adfhirsd:decorExample2FHIRexample($examples as item()?, $fhirType as xs:string?, $properties as element()*) as element()? {
    let $exampleElementName     := 
        switch ($fhirType)
        case 'Age'              return 'exampleQuantity'
        case 'Distance'         return 'exampleQuantity'
        case 'SimpleQuantity'   return 'exampleQuantity'
        case 'Duration'         return 'exampleQuantity'
        case 'Count'            return 'exampleQuantity'
        case 'Quantity'         return 'exampleQuantity'
        default                 return concat('example',upper-case(substring($fhirType, 1, 1)), substring($fhirType, 2))
    
    for $example at $i in $examples
    let $value  := normalize-space($example)
    return
        switch ($fhirType)
        case 'CodeableConcept' return
            element {QName('http://hl7.org/fhir',$exampleElementName)} {
                <text value="{$value}"/>
            }
        case 'Identifier' return
            element {QName('http://hl7.org/fhir',$exampleElementName)} {
                <value value="{$value}"/>
            }
        case 'string' return
            element {QName('http://hl7.org/fhir',$exampleElementName)} {
                attribute value {$value}
            }
        case 'date' return (
            let $valid  :=
                if ($value castable as xs:date) then $value else
                if (matches($value, '^\d{2}/\d{2}/\d{4}')) then (
                    (: US style mm/dd/yyyy :)
                    concat(substring($value, 7, 4), '-', substring($value, 1, 2), '-', substring($value, 4, 2))[. castable as xs:date]
                )
                else
                if (matches($value, '^\d{2}.\d{2}.\d{4}')) then (
                    (: Rest of the world style dd-mm-yyyy :)
                    concat(substring($value, 7, 4), '-', substring($value, 4, 2), '-', substring($value, 1, 2))[. castable as xs:date]
                ) else ()
                
            return
            if (string-length($valid) > 0) then (
                element {QName('http://hl7.org/fhir',$exampleElementName)} {
                    attribute value {$valid}
                }
            ) else ()
        )
        case 'dateTime' return (
            let $valid  :=
                if ($value castable as xs:dateTime) then $value else
                if ($value castable as xs:date) then $value else
                if (matches($value, '^\d{2}/\d{2}/\d{4}')) then (
                    (: US style mm/dd/yyyy :)
                    let $d        := concat(substring($value, 7, 4), '-', substring($value, 1, 2), '-', substring($value, 4, 2))
                    let $t        := replace(normalize-space(substring($value, 11)), '[^\d:Z+\-]', '')
                    let $t        := if (matches($t, '^\d{2}(:\d{2})?$')) then substring($t || ':00:00', 1, 8) else $t
                    let $dt       := $d || 'T' || $t
                    
                    return
                    if ($dt castable as xs:dateTime) then
                        if (empty(timezone-from-time(xs:time($t)))) then
                            string(adjust-dateTime-to-timezone(xs:dateTime($dt)))
                        else (
                            $dt
                        )
                    else 
                    if ($d castable as xs:date) then $d else ()
                )
                else
                if (matches($value, '^\d{2}.\d{2}.\d{4}')) then (
                    (: Rest of the world style dd-mm-yyyy :)
                    let $d        := concat(substring($value, 7, 4), '-', substring($value, 1, 2), '-', substring($value, 4, 2))
                    let $t        := replace(normalize-space(substring($value, 11)), '[^\d:Z+\-]', '')
                    let $t        := if (matches($t, '^\d{2}(:\d{2})?$')) then substring($t || ':00:00', 1, 8) else $t
                    let $dt       := $d || 'T' || $t
                    
                    return
                    if ($dt castable as xs:dateTime) then
                        if (empty(timezone-from-time(xs:time($t)))) then
                            string(adjust-dateTime-to-timezone(xs:dateTime($dt)))
                        else (
                            $dt
                        )
                    else 
                    if ($d castable as xs:date) then $d else ()
                ) else ()
            return
            if (string-length($valid) = 0) then () else (
                element {QName('http://hl7.org/fhir',$exampleElementName)} {
                    attribute value {$valid}
                }
            )
        )
        case 'Count' return (
            if ($value castable as xs:decimal) then (
                element {QName('http://hl7.org/fhir',$exampleElementName)} {
                    <value value="{$value}"/>
                }
            ) else ()
        )
        case 'decimal' return (
            if ($value castable as xs:decimal) then (
                element {QName('http://hl7.org/fhir',$exampleElementName)} {
                    <value value="{$value}"/>
                }
            ) else ()
        )
        case 'boolean' return (
            let $value  :=
                if ($value castable as xs:boolean) then xs:boolean($value) else 
                if (lower-case($value) = ('yes','ja','oui','si')) then true() else
                if (lower-case($value) = ('no','nee','non','no','nein')) then false() else (true())
            return
                element {QName('http://hl7.org/fhir',$exampleElementName)} {
                    attribute value {$value}
                }
        )
        case 'base64Binary' return ()
        default return (
            if ($fhirType = ('Age','Duration','Quantity','SimpleQuantity')) then (
                let $valuepart  := replace($value,'^([\d,\.]+).*','$1')
                (: fix decimals with comma (non US) if possible/necessary :)
                let $valuepart  := 
                    if ($valuepart castable as xs:decimal) then 
                        $valuepart 
                    else
                    if (replace($valuepart, ',', '.') castable as xs:decimal) then 
                        replace($valuepart, ',', '.')
                    else (
                        (: cannot be fixed. someone needs to fix this in the dataset example :)
                        $valuepart
                    )
                (: unit is required :)
                let $unitpart   := (normalize-space(substring-after($value, $valuepart)), $properties/@unit, '1')[string-length() > 0][1]
                return
                 element {QName('http://hl7.org/fhir',$exampleElementName)} {
                    <value value="{$valuepart}"/>,
                    if ($unitpart[string-length()=0]) then () else <unit value="{$unitpart}"/>
                 }
            )
            else ()
        )
};

declare function adfhirsd:decorValueDomainType2FHIRtype($decorType as item()?) as xs:string {
    switch ($decorType)
    case 'count'        return 'Count'      (:could have chosen integer of decimal. not sure why to pick what :)
    case 'code'         return 'CodeableConcept'
    case 'ordinal'      return 'CodeableConcept'
    case 'identifier'   return 'Identifier'
    case 'string'       return 'string'
    case 'text'         return 'string'
    case 'date'         return 'date'       (:need work with properties:)
    case 'datetime'     return 'dateTime'   (:need work with properties:)
    case 'complex'      return 'string'     (:this is a problem. this is lazy dataset behavior that is unimplementable:)
    case 'quantity'     return 'Quantity'
    case 'duration'     return 'Duration'
    case 'boolean'      return 'boolean'
    case 'blob'         return 'base64Binary'
    case 'decimal'      return 'decimal'
    default             return 'string'
};

declare function adfhirsd:decorProperties2FHIRminmaxValue($properties as item(), $fhirType as xs:string?) as element()* {
    let $minValue               := if (normalize-space($properties/@minInclude) castable as xs:decimal) then normalize-space($properties/@minInclude) else ()
    let $maxValue               := if (normalize-space($properties/@maxInclude) castable as xs:decimal) then normalize-space($properties/@maxInclude) else ()
    let $minElementName         := 
        switch ($fhirType)
        case 'Age'              return 'minValueQuantity'
        case 'Distance'         return 'minValueQuantity'
        case 'SimpleQuantity'   return 'minValueQuantity'
        case 'Duration'         return 'minValueQuantity'
        case 'Count'            return 'minValueQuantity'
        case 'Quantity'         return 'minValueQuantity'
        default                 return concat('minValue',upper-case(substring($fhirType, 1, 1)), substring($fhirType, 2))
    let $maxElementName         := 
        switch ($fhirType)
        case 'Age'              return 'maxValueQuantity'
        case 'Distance'         return 'maxValueQuantity'
        case 'SimpleQuantity'   return 'maxValueQuantity'
        case 'Duration'         return 'maxValueQuantity'
        case 'Count'            return 'maxValueQuantity'
        case 'Quantity'         return 'maxValueQuantity'
        default                 return concat('maxValue',upper-case(substring($fhirType, 1, 1)), substring($fhirType, 2))
    
    return
        if ($fhirType = 'Count') then (
            if (empty($minValue)) then () else (
                element {QName('http://hl7.org/fhir',$minElementName)} {
                    <value value="{$minValue}"/>
                }
            )
            ,
            if (empty($maxValue)) then () else (
                element {QName('http://hl7.org/fhir',$maxElementName)} {
                    <value value="{$maxValue}"/>
                }
            )
        ) else
        if ($fhirType = ('Age','Duration','Quantity','SimpleQuantity')) then (
            let $unit   :=  ($properties/@unit)[1]
            
            return (
                if (empty($minValue)) then () else (
                    element {QName('http://hl7.org/fhir',$minElementName)} {
                        <value value="{$minValue}"/>,
                        if (empty($unit)) then () else <unit value="{$unit}"/>
                    }
                )
                ,
                if (empty($maxValue)) then () else (
                    element {QName('http://hl7.org/fhir',$maxElementName)} {
                        <value value="{$maxValue}"/>,
                        if (empty($unit)) then () else <unit value="{$unit}"/>
                    }
                )
            )
        ) else
        if ($fhirType = 'decimal') then (
            if (empty($minValue)) then () else (
                element {QName('http://hl7.org/fhir',$minElementName)} {
                    attribute value {$minValue}
                }
            )
            ,
            if (empty($maxValue)) then () else (
                element {QName('http://hl7.org/fhir',$maxElementName)} {
                    attribute value {$maxValue}
                }
            )
        )
        else ()
};

(:  Legal in FHIR:          draft | active | retired
    Legal in Template ITS:  draft | active | retired | new | rejected | cancelled | pending | review
:)
declare function adfhirsd:decorStatus2fhirStatus($status as xs:string?) as xs:string? {
    switch ($status)
    case 'new'          return 'draft'
    case 'draft'        return 'draft'
    case 'pending'      return 'draft'
    case 'active'       return 'active'
    case 'final'        return 'active' (: not technically a template status, but an item status :)
    case 'review'       return 'draft'
    case 'cancelled'    return 'retired'
    case 'rejected'     return 'retired'
    case 'deprecated'   return 'retired'
    default             return 'draft'
};
declare function adfhirsd:fhirStatus2decorStatus($status as xs:string?) as xs:string* {
    switch ($status)
    case 'draft'        return 'draft'
    case 'active'       return 'final'
    case 'retired'      return 'deprecated'
    default             return ()
};

declare function adfhirsd:decorBindingStrength2fhirBindingStrength($strength as xs:string?) as xs:string {
    switch ($strength)
    case 'CNE'          return 'required'
    case 'CWE'          return 'extensible'
    case 'required'     return 'required'
    case 'extensible'   return 'extensible'
    case 'preferred'    return 'preferred'
    case 'example'      return 'example'
    default             return 'required'
};
declare function adfhirsd:fhirBindingStrength2decorBindingStrength($strength as xs:string?) as xs:string {
    switch ($strength)
    case 'required'     return 'required'
    case 'extensible'   return 'extensible'
    case 'preferred'    return 'preferred'
    case 'example'      return 'example'
    default             return 'required'
};

(:~ Produces a FHIR Bundle containing StructureDefinitions that express HL7 Version 3 artifacts. The StructureDefinition expression 
:   is (sort of) on par with what Template ITS as used in ART-DECOR can do. This is a experimental capability of the FHIR StructureDefinition
:
:   Example call:
:   let $projectPrefix  := if (request:exists()) then request:get-parameter('prefix','demo1-') else ()
:   let $id             := if (request:exists()) then request:get-parameter('id',())[string-length()>0][1] else ()
:   let $effectiveDate  := if (request:exists()) then request:get-parameter('effectiveDate',())[string-length()>0][1] else ()

:   let $templates      := 
:       $setlib:colDecorData//template[@statusCode=('draft','active','retired','pending','review')][ancestor::decor/project/@prefix=$projectPrefix]
:   
:   return
        adfhirsd:convertDecorTemplate2FHIRStructureDefinition($templates)
:)
declare function adfhirsd:convertDecorTemplate2FHIRStructureDefinition($templates as element(template)*) as element(f:StructureDefinition)* {

    let $comment    :=
        comment {'This is an experimental service for conversion of compliant Template ITS templates to FHIR 
        StructureDefinitions. You can current do one template at a time. Parameters:
        id              - required. Template id
        effectiveDate   - optional. Template effectiveDate (yyyy-mm-ddThh:mm:ss). Does latest version if omitted
        
        Handles: desc | constraint | element | assert | include | choice | vocabulary | property
        
        TODO: slicing, multiple value set bindings, inline value sets (repetitions of vocabulary with code/codeSystem), 
              languages (when multiple desc | constraints exist), other namespaced elements/attributes 
              (extension: http://hl7.org/fhir/StructureDefinition/elementdefinition-namespace with valueUri)
              
        Part of the TODOs need resolution in Template ITS and parts need resolution in FHIR. These are brought forward there'}

    (:<Bundle xmlns="http://hl7.org/fhir">
    {
        $comment
    }
        <id value="{util:uuid()}"/>
        <type value="collection"/>
    {
        for $t in $templates[*:classification/@format='hl7v3xml1'] | $templates[not(*:classification/@format)]
        let $projectPrefix  := $t/ancestor::*:decor/*:project/@prefix
        let $template       := if ($t[@ref]) then (templ:getTemplateById($t/@ref, 'dynamic', $projectPrefix, ())//*:template/*:template[@id]) else $t
        let $tmid           := $template/@id
        let $tmed           := $template/@effectiveDate
        let $fhirId         := concat($tmid,'--',replace($tmed,'[^\d]',''))
        return
        <entry>
            <fullUrl value="{concat($getf:strFhirServices,'StructureDefinition/', $fhirId)}"/>
            <resource>
            </resource>
            <!--{$template}-->
        </entry>
    }
    </Bundle>:)
    for $t in $templates[*:classification/@format='hl7v3xml1'] | $templates[not(*:classification/@format)]
    let $projectPrefix  := $t/ancestor::*:decor/*:project/@prefix
    let $template       := if ($t[@ref]) then (templ:getTemplateById($t/@ref, 'dynamic', $projectPrefix, ())//*:template/*:template[@id]) else $t
    let $tmid           := $template/@id
    let $tmed           := $template/@effectiveDate
    let $fhirId         := concat($tmid,'--',replace($tmed,'[^\d]',''))
    return
    <StructureDefinition xmlns="http://hl7.org/fhir">
    {
        $comment
    }
        <id value="{$fhirId}"/>
        <meta>
            <profile value="http://hl7.org/fhir/{$getf:strFhirVersionShort}/StructureDefinition/StructureDefinition"/>
        </meta>
        <extension url="http://hl7.org/fhir/StructureDefinition/elementdefinition-namespace">
            <valueUri value="urn:hl7-org:v3"/>
        </extension>
    {
        (: Any object in ART-DECOR is effective from effectiveDate to expirationDate :)
        if ($template[@effectiveDate castable as xs:dateTime] | $template[@expirationDate castable as xs:dateTime]) then (
            <extension url="http://hl7.org/fhir/StructureDefinition/resource-effectivePeriod">
                <valuePeriod>
                {
                    if ($template[@effectiveDate castable as xs:dateTime]) then 
                        <start value="{adjust-dateTime-to-timezone(xs:dateTime($template/@effectiveDate))}"/>
                    else (),
                    if ($template[@expirationDate castable as xs:dateTime]) then
                        <end value="{adjust-dateTime-to-timezone(xs:dateTime($template/@expirationDate))}"/>
                    else ()
                }
                </valuePeriod>
            </extension>
        ) else ()
    }
        <fullUrl value="{if ($t[@canonicalUri]) then $t/@canonicalUri else concat($getf:strFhirServices,'StructureDefinition/', $fhirId)}"/>
        <identifier>
            <use value="official"/>
            <system value="urn:ietf:rfc:3986"/>
            <value value="urn:oid:{$tmid}"/>
        </identifier>
        <version value="{let $semver := adfhir:getSemverString($t/@versionLabel) return if (empty($semver)) then $tmed else $semver}"/>
        <name value="{$template/@name}"/>
        <title value="{$template/@displayName}"/>
        <status value="{adfhirsd:decorStatus2fhirStatus($template/@statusCode)}"/>
        <experimental value="false"/>
        <date value="{$template/@effectiveDate}Z"/>
    {
        let $copyright  := ($t/ancestor::*:decor/*:project/*:copyright[empty(@type)] | $t/ancestor::*:decor/*:project/*:copyright[@type = 'author'])[1]
        return
            if ($copyright) then (
                <publisher value="{$copyright/@by}"/>
                ,
                for $publisher in $copyright[*:addrLine/@type = ('phone', 'email', 'fax', 'uri')]
                return
                <contact>
                    <name value="{$publisher/@name}"/>
                {
                    for $addrLine in $publisher/*:addrLine[@type = ('phone', 'email', 'fax', 'uri')]
                    let $system     := if ($addrLine/@type = 'uri') then 'other' else $addrLine/@type
                    return
                    <telecom>
                        <system value="{$system}"/>
                        <value value="{$addrLine}"/>
                    </telecom>
                }
                </contact>
            )
            else (
                <publisher value="ART-DECOR"/>
            )
    }
        {
            if ($template[*:desc//text() | *:constraint//text()]) then
                <description value="{$template/*:desc[1]/node() | $template/*:constraint//text()}"/>
            else ()
        }
        <kind value="logical"/>
        <abstract value="false"/>
        <differential>
        {
            adfhirsd:template2fhirElement($template, $projectPrefix, (), ())
        }
        </differential>
    </StructureDefinition>
};

declare function adfhirsd:templateDatatype2fhirDatatype($itemType as xs:string?, $itemName as xs:string?, $dataType as item()?, $parentDecorDatatype as xs:string?, $dotpath as xs:string?) as xs:string? {
    if (string-length($dataType)=0 and string-length($parentDecorDatatype)=0) then (
        let $cdaElementParts    := ('.authenticator', '.author', '.authorization', '.component', '.componentOf', 
                                    '.consumable', '.custodian', '.dataEnterer', '.documentationOf', 
                                    '.encounterParticipant', '.entry', '.entryRelationship', '.informant', 
                                    '.informationRecipient', '.inFulfillmentOf', '.legalAuthenticator', '.location', 
                                    '.participant', '.performer', '.precondition', '.product', '.recordTarget', 
                                    '.reference', '.referenceRange', '.relatedDocument', '.responsibleParty', 
                                    '.specimen', '.subject')
        let $finalPart          := tokenize($dotpath, '\.')[last()]
        let $finalDotPart       := concat('.', $finalPart)
        
        return
            if ($cdaElementParts[. = $finalDotPart]) then 
                'Element'
            else
            if ($itemName = 'templateId') then
                'http://hl7.org/fhir/cda/StructureDefinition/II'
            else
            if ($itemName = 'realmCode') then
                'http://hl7.org/fhir/cda/StructureDefinition/CS'
            else
            if ($itemName = 'typeId') then
                'http://hl7.org/fhir/cda/StructureDefinition/II'
            else
            if ($itemName = 'manufacturedMaterial') then
                'http://hl7.org/fhir/cda/StructureDefinition/Material'
            else
            if ($itemName = 'scopingEntity') then
                'http://hl7.org/fhir/cda/StructureDefinition/Entity'
            else
            if ($itemName = ('associatedPerson', 'assignedPerson')) then
                'http://hl7.org/fhir/cda/StructureDefinition/Person'
            else
            if ($itemName = ('asOrganizationPartOf', 'receivedOrganization', 'representedOrganization', 'representedCustodianOrganization', 'scopingOrganization', 'wholeOrganization')) then
                'http://hl7.org/fhir/cda/StructureDefinition/Organization'
            else (
                (:
                    <path value="ClinicalDocument.dataEnterer.assignedEntity"/>
                    <type><code value="http://hl7.org/fhir/cda/StructureDefinition/AssignedEntity"/></type>
                :)
                concat('http://hl7.org/fhir/cda/StructureDefinition/', upper-case(substring($finalPart, 1, 1)), substring($finalPart, 2))
            )
    )
    else
    if (string-length($dataType)=0) then (
        switch ($itemName)
        case 'alignment'                    return 'code'(:'cs':)
        case 'assigningAuthorityName'       return 'string'(:'st':)
        case 'code'                         return 'code'(:'cs':)
        case 'codeSystem'                   return 'string'(:'oid':)
        case 'codeSystemName'               return 'string'(:'st':)
        case 'codeSystemVersion'            return 'string'(:'st':)
        case 'displayName'                  return 'string'(:'st':)
        case 'mediaType'                    return 'code'(:'cs':)
        case 'language'                     return 'code'(:'cs':)
        case 'compression'                  return 'code'(:'cs':)
        case 'integrityCheck'               return 'base64Binary'(:'bin':)
        case 'integrityCheckAlgorithm'      return 'code'(:'cs':)
        case 'currency'                     return 'code'(:'cs':)
        case 'displayable'                  return 'boolean'(:'bl':)
        case 'inclusive'                    return 'boolean'(:'bl':)
        case 'inverted'                     return 'boolean'(:'bn':)
        case 'partType'                     return 'code'(:'cs':)
        case 'classCode'                    return 'code'(:'cs':)
        case 'contextConductionInd'         return 'boolean'(:'bl':)
        case 'contextControlCode'           return 'code'(:'cs':)
        case 'determinerCode'               return 'code'(:'cs':)
        case 'extension'                    return 'string'(:'st':)
        case 'independentInd'               return 'boolean'(:'bl':)
        case 'institutionSpecified'         return 'boolean'(:'bl':)
        case 'inversionInd'                 return 'boolean'(:'bl':)
        case 'mediaType'                    return 'string'(:'st':)
        case 'moodCode'                     return 'code'(:'cs':)
        case 'negationInd'                  return 'boolean'(:'bl':)
        case 'nullFlavor'                   return 'code'(:'cs':)
        case 'operator'                     return 'code'(:'cs':)
        case 'qualifier'                    return 'code'(:'set_cs':)
        case 'representation'               return 'code'(:'cs':)
        case 'root'                         return 'string'(:'uid':)
        case 'typeCode'                     return 'code'(:'cs':)
        case 'unit'                         return 'code'(:'cs':)
        case 'use'                          return 'code'(:'set_cs':)
        case 'xsi:type'                     return 'code'(:'cs':)
        case 'xsi:nil'                      return 'boolean'(:'bl':)
        case 'width'                        return 'http://hl7.org/fhir/cda/StructureDefinition/PQ'
        case 'translation'                  return 'http://hl7.org/fhir/cda/StructureDefinition/PQR'
        case 'low'                          return (
            switch ($parentDecorDatatype)
            case 'IVL_MO' return 'http://hl7.org/fhir/cda/StructureDefinition/MO'
            case 'IVL_PQ' return 'http://hl7.org/fhir/cda/StructureDefinition/PQ'
            case 'IVL_TS' return 'http://hl7.org/fhir/cda/StructureDefinition/TS'
            case 'IVL_INT' return 'http://hl7.org/fhir/cda/StructureDefinition/INT'
            case 'IVL_REAL' return 'http://hl7.org/fhir/cda/StructureDefinition/REAL'
            default return
                error(QName('http://art-decor.org/ns/error', 'MissingDatatype'), 
                concat('Particle without datatype under particle with datatype "', $parentDecorDatatype, '". Name "',$itemName,'". Path ', $dotpath))
        )
        case 'high'                         return (
            switch ($parentDecorDatatype)
            case 'IVL_MO' return 'http://hl7.org/fhir/cda/StructureDefinition/MO'
            case 'IVL_PQ' return 'http://hl7.org/fhir/cda/StructureDefinition/PQ'
            case 'IVL_TS' return 'http://hl7.org/fhir/cda/StructureDefinition/TS'
            case 'IVL_INT' return 'http://hl7.org/fhir/cda/StructureDefinition/INT'
            case 'IVL_REAL' return 'http://hl7.org/fhir/cda/StructureDefinition/REAL'
            default return
                error(QName('http://art-decor.org/ns/error', 'MissingDatatype'), 
                concat('Particle without datatype under particle with datatype "', $parentDecorDatatype, '". Name "',$itemName,'". Path ', $dotpath))
        )
        case 'center'                       return (
            switch ($parentDecorDatatype)
            case 'IVL_MO' return 'http://hl7.org/fhir/cda/StructureDefinition/MO'
            case 'IVL_PQ' return 'http://hl7.org/fhir/cda/StructureDefinition/PQ'
            case 'IVL_TS' return 'http://hl7.org/fhir/cda/StructureDefinition/TS'
            case 'IVL_INT' return 'http://hl7.org/fhir/cda/StructureDefinition/INT'
            case 'IVL_REAL' return 'http://hl7.org/fhir/cda/StructureDefinition/REAL'
            default return
                error(QName('http://art-decor.org/ns/error', 'MissingDatatype'), 
                concat('Particle without datatype under particle with datatype "', $parentDecorDatatype, '". Name "',$itemName,'". Path ', $dotpath))
        )
        case 'value'                        return (
            if ($itemType = 'element') then
                error(QName('http://art-decor.org/ns/error', 'MissingDatatype'), 
                    concat('Particle without datatype under particle with datatype "', $parentDecorDatatype, '". Name "',$itemName,'". Path ', $dotpath))
            else
            if ($itemType = 'attribute') then
                switch ($parentDecorDatatype)
                case 'IVL_MO' return 'decimal'
                case 'MO' return 'decimal'
                case 'IVL_PQ' return 'decimal'
                case 'PQ' return 'decimal'
                case 'IVL_TS' return 'dateTime'
                case 'TS' return 'dateTime'
                case 'IVL_INT' return 'integer'
                case 'INT' return 'integer'
                case 'IVL_REAL' return 'decimal'
                case 'REAL' return 'decimal'
                case 'TEL' return 'string'
                default return
                    error(QName('http://art-decor.org/ns/error', 'MissingDatatype'), 
                    concat('Particle without datatype under particle with datatype "', $parentDecorDatatype, '". Name "',$itemName,'". Path ', $dotpath))
            else ()
        )
        default return 
            error(QName('http://art-decor.org/ns/error', 'MissingDatatype'), 
                concat('Particle without datatype under particle with datatype "', $parentDecorDatatype, '". Name "',$itemName,'". Path ', $dotpath))
    )
    else (
        let $noprefix       :=
            if (contains($dataType,':')) then (
                substring-after($dataType,':')
            ) else ($dataType)
        let $noflavor       := tokenize($noprefix,'\.')[1]
        
        return
            switch ($noflavor)
            case 'bin'                  return 'base64Binary'
            case 'bl'                   return 'boolean'
            case 'bn'                   return 'boolean'
            case 'cs'                   return 'code'
            case 'int'                  return 'integer'
            case 'oid'                  return 'string'
            case 'real'                 return 'decimal'
            case 'ruid'                 return 'string'
            case 'set_cs'               return 'code'
            case 'st'                   return 'string'
            case 'ts'                   return 'dateTime'
            case 'uid'                  return 'string'
            case 'url'                  return 'uri'
            case 'uuid'                 return 'string'
            case 'SD'                   return 'xhtml'
            case 'ON'                   return 'http://hl7.org/fhir/cda/StructureDefinition/EN'
            case 'PN'                   return 'http://hl7.org/fhir/cda/StructureDefinition/EN'
            case 'TN'                   return 'http://hl7.org/fhir/cda/StructureDefinition/EN'
            case 'ADXP'                 return 'http://hl7.org/fhir/cda/StructureDefinition/ST'
            (:case 'ENXP'                 return 'http://hl7.org/fhir/cda/StructureDefinition/ST':)
            default return concat('http://hl7.org/fhir/cda/StructureDefinition/', $noflavor)
    )
};
declare function adfhirsd:templateCleanName($name as xs:string) as xs:string {
    let $noprefix       :=
        if (contains($name,'hl7:') or contains($name,'cda:')) then (
            substring-after($name,':')
        ) else ($name)
    let $nopredicate    :=
        if (contains($noprefix,'[')) then (
            substring-before($noprefix,'[')
        ) else ($noprefix)
    return $nopredicate
};
declare function adfhirsd:template2fhirElement($template as element(), $projectPrefix as xs:string, $dotpath as xs:string?, $parentDecorDatatype as xs:string?) as element()* {
    let $containedTemplate  := if ($template[@contains]) then (templ:getTemplateById($template/@contains, $template/@flexibility, $projectPrefix)//*:template/*:template[@id]) else ()
    
    return (
        for $item in templ:normalizeAttributes($template/*:attribute | $containedTemplate/*:attribute)
        return
            adfhirsd:templateAttribute2fhirElement($item, $projectPrefix, $dotpath, $parentDecorDatatype)
        ,
        for $item in $template/*:element | $template/*:include | $template/*:choice | $containedTemplate/*:element | $containedTemplate/*:include | $containedTemplate/*:choice
        return
            if ($item[local-name()='element']) then 
                adfhirsd:templateElement2fhirElement($item, $projectPrefix, $dotpath, $parentDecorDatatype)
            else
            if ($item[local-name()='include']) then
                adfhirsd:templateInclude2fhirElement($item, $projectPrefix, $dotpath, $parentDecorDatatype)
            else
            if ($item[local-name()='choice']) then
                adfhirsd:templateChoice2fhirElement($item, $projectPrefix, $dotpath, $parentDecorDatatype)
            else
                ()
    )
};
(:<property minInclude="" maxInclude="" minLength="" maxLength="" currency="" fractionDigits="" unit="" value=""/>:)
declare function adfhirsd:templateAttribute2fhirElement($att as element(), $projectPrefix as xs:string, $dotpath as xs:string?, $parentDecorDatatype as xs:string?) as element()+ {
    <element xmlns="http://hl7.org/fhir">
        <path value="{string-join(($dotpath,$att/@name),'.')}"/>
        <representation value="xmlAttr"/>
    {
        if ($att[*:item]) then
            <label value="{$att/*:item/@label}"/>
        else ()
    }
    {
        if ($att[*:desc//text() | *:constraint//text()]) then
            <definition value="{$att/*:desc[1]/node() | $att/*:constraint/node()}"/>
        else ()
    }
        <min value="{utillib:getMinimumMultiplicity($att)}"/>
        <max value="{utillib:getMaximumMultiplicity($att)}"/>
        <type>
            <code value="{adfhirsd:templateDatatype2fhirDatatype(local-name($att), adfhirsd:templateCleanName($att/@name), $att/@datatype, $parentDecorDatatype, $dotpath)}"/>
        </type>
    {
        (:.. defaultValue[x]	Σ I	0..1	*	Specified value it missing from instance:)
        ()
    }
    {
        (:... meaningWhenMissing	Σ I	0..1	markdown	Implicit meaning when this element is missing:)
        ()
    }
    {
        (:... fixed[x]	Σ I	0..1	*	Value must be exactly this:)
        if ($att[@value]) then
            <fixedString value="{$att/@value}"/>
        else (),
        for $code in $att/*:vocabulary[@code]/@code
        return
            <fixedString value="{$code}"/>
    }
    {
        (:... pattern[x]	Σ I	0..1	*	Value must have at least these property values:)
        ()
    }
    {
        (:... example[x]	Σ	0..1	*	Example value: [as defined for type]:)
        ()
    }
    {
        (:... minValue[x]	Σ	0..1	*	Minimum Allowed Value (for some types):)
        if ($att[*:property/@minInclude]) then
            <minValueString value="{$att/*:property/@minInclude}"/>
        else ()
    }
    {
        (:... maxValue[x]	Σ	0..1	*	Maximum Allowed Value (for some types):)
        if ($att[*:property/@maxInclude]) then
            <maxValueString value="{$att/*:property/@maxInclude}"/>
        else ()
    }
    {
        (:... maxLength	Σ	0..1	integer	Max length for strings:)
        if ($att[*:property/@maxLength]) then
            <maxLength value="{$att/*:property/@maxLength}"/>
        else ()
    }
    {
        adfhirsd:templateVocabulary2fhirBinding($att/*:vocabulary[@valueSet], $projectPrefix, adfhirsd:decorBindingStrength2fhirBindingStrength($att/@strength))
    }
    </element>
};
declare function adfhirsd:templateElement2fhirElement($elm as element(), $projectPrefix as xs:string, $dotpath as xs:string?, $parentDecorDatatype as xs:string?) as element()+ {
    let $lastelmname    := tokenize($dotpath, '\.')[last()]
    let $cleanname      := adfhirsd:templateCleanName($elm/@name)
    let $context        := string-join(($dotpath, $cleanname),'.')
    return (
        <element xmlns="http://hl7.org/fhir">
            <path value="{$context}"/>
        {
            if ($elm/*:attribute[@name = 'xsi:type'][not(@isOptional = 'false')][not(@prohibited = 'true')]) then
                <representation value="typeAttr"/>
            else 
            if ($cleanname = 'value' and $lastelmname = ('observation', 'observationRange', 'criterion')) then
                <representation value="typeAttr"/>
            else
            if ($cleanname = 'effectiveTime' and $lastelmname = ('substanceAdministration', 'supply')) then
                <representation value="typeAttr"/>
            else
            if ($cleanname = 'text' and $lastelmname = 'section') then
                <representation value="cdaText"/>
            else ()
        }
        {
            if ($elm[*:item]) then
                <label value="{$elm/*:item/@label}"/>
            else ()
        }
        {
            if ($elm[*:desc//text() | *:constraint//text()]) then
                <definition value="{$elm/*:desc[1]/node() | $elm/*:constraint/node()}"/>
            else ()
        }
            <min value="{utillib:getMinimumMultiplicity($elm)}"/>
            <max value="{utillib:getMaximumMultiplicity($elm)}"/>
        {
            if (empty($dotpath) and not(contains($context, '.'))) then () else (
                <type>
                    <code value="{adfhirsd:templateDatatype2fhirDatatype(local-name($elm), adfhirsd:templateCleanName($elm/@name), $elm/@datatype, $parentDecorDatatype, $context)}"/>
                </type>
            )
        }
        {
            for $assert in $elm/*:assert
            return
                <condition value="{generate-id($assert)}"/>
            ,
            for $assert in $elm/*:assert
            return
            <constraint>
                <!--extension url="http://hl7.org/fhir/StructureDefinition/structuredefinition-expression">
                    <valueString value="" />
                </extension-->
                <key value="{generate-id($assert)}" />
                <severity value="{if ($assert[@role=('hint','warning')]) then 'warning' else 'error'}" />
                <human value="{$assert/node()}"/>
                <xpath value="{$assert/@test}"/>
            </constraint>
        }
        {
            if ($elm[@isMandatory='true'] | $elm[@conformance='R']) then 
                <mustSupport value="true"/>
            else ()
        }
        {
            adfhirsd:templateVocabulary2fhirBinding($elm/*:vocabulary[@valueSet], $projectPrefix, adfhirsd:decorBindingStrength2fhirBindingStrength($elm/@strength))
        }
        </element>
        ,
        for $vocabulary in $elm/*:vocabulary[@code | @codeSystem | @displayName]/@code |
                           $elm/*:vocabulary[@code | @codeSystem | @displayName]/@codeSystem |
                           $elm/*:vocabulary[@code | @codeSystem | @displayName]/@codeSystemName |
                           $elm/*:vocabulary[@code | @codeSystem | @displayName]/@codeSystemVersion |
                           $elm/*:vocabulary[@code | @codeSystem | @displayName]/@displayName
        return
        <element xmlns="http://hl7.org/fhir">
            <path value="{string-join(($context,local-name($vocabulary)),'.')}"/>
            <representation value="xmlAttr"/>
            <fixedString value="{$vocabulary}"/>
        </element>
        ,
        adfhirsd:template2fhirElement($elm, $projectPrefix, $context, if ($elm/@datatype) then $elm/@datatype else $parentDecorDatatype)
    )
};
declare function adfhirsd:templateInclude2fhirElement($elm as element(), $projectPrefix as xs:string, $dotpath as xs:string?, $parentDecorDatatype as xs:string?) as element()* {
    let $template   := templ:getTemplateByRef($elm/@ref, if ($elm/@flexibility) then $elm/@flexibility else 'dynamic', $projectPrefix)//*:template/*:template[@id]
    
    return adfhirsd:template2fhirElement($template, $projectPrefix, $dotpath, $parentDecorDatatype)
};
declare function adfhirsd:templateChoice2fhirElement($elm as element(), $projectPrefix as xs:string, $dotpath as xs:string?, $parentDecorDatatype as xs:string?) as element()* {
    for $item in $elm/*
    return 
        adfhirsd:template2fhirElement($item, $projectPrefix, $dotpath, $parentDecorDatatype)
};

declare function adfhirsd:templateVocabulary2fhirBinding($vocabularies as element()*, $projectPrefix as xs:string, $strength as xs:string?) as element(f:binding)* {
    for $vocabulary in $vocabularies[@valueSet] | $vocabularies[self::*:valueSet]
    let $valueSet   := if ($vocabulary[self::*:valueSet]) then $vocabulary else (vs:getValueSetByRef($vocabulary/@valueSet, string($vocabulary/@flexibility), $projectPrefix, false())//*:valueSet[@id])[1]
    return
    <binding xmlns="http://hl7.org/fhir">
        <strength value="{$strength}"/>
        <valueSetReference>
            <reference value="{utillib:getCanonicalUriForOID('ValueSet', $valueSet, $projectPrefix, $setlib:strKeyFHIRSTU3)}"/>
            <display value="{($valueSet/@displayName, $valueSet/@name)[1]}"/>
        </valueSetReference>
    </binding>
};