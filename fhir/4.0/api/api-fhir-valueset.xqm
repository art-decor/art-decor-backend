xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

module namespace adfhirvs           = "http://art-decor.org/ns/fhir/4.0/valueset";
import module namespace adfhir      = "http://art-decor.org/ns/fhir/4.0" at "api-fhir.xqm";
import module namespace getf        = "http://art-decor.org/ns/fhir-settings" at "fhir-settings.xqm";
import module namespace cs          = "http://art-decor.org/ns/decor/codesystem" at "../../../art/api/api-decor-codesystem.xqm";
import module namespace vsapi       = "http://art-decor.org/ns/api/valueset" at "../../../api/modules/valueset-api.xqm";
import module namespace adloinc     = "http://art-decor.org/ns/terminology/loinc" at "../../../terminology/loinc/api/api-loinc.xqm";
import module namespace adsnomed    = "http://art-decor.org/ns/terminology/snomed" at "../../../terminology/loinc/api/api-snomed.xqm";
import module namespace utillib     = "http://art-decor.org/ns/api/util" at "../../../api/modules/library/util-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "../../../api/modules/library/settings-lib.xqm";
import module namespace markdown    = "http://art-decor.org/ns/api/markdown" at "../../../api/modules/library/markdown-lib.xqm";
import module namespace decorlib    = "http://art-decor.org/ns/api/decor" at "../../../api/modules/library/decor-lib.xqm";
import module namespace utilfhir    = "http://art-decor.org/ns/fhir-util" at "../api/fhir-util.xqm";
declare namespace f                 = "http://hl7.org/fhir";
declare namespace error             = "http://art-decor.org/ns/fhir/error";
declare namespace svrl              = "http://purl.oclc.org/dsdl/svrl";

declare %private variable $adfhirvs:type            := 'ValueSet';

declare function adfhirvs:convertDecorExpandedValueSet2FHIRValueSet($valueSetExpansionSet as element(valueSetExpansionSet)) as element(f:ValueSet) {
let $valueSet           := $valueSetExpansionSet/valueSet
let $expansion          := $valueSetExpansionSet/expansion
let $uuid               := if ($valueSetExpansionSet[@id]) then $valueSetExpansionSet/@id else util:uuid()
let $timestamp          := if ($expansion[@timestamp]) then $expansion/@timestamp else current-dateTime()
let $projectPrefix      := if ($valueSet/@ident) then $valueSet/@ident else request:get-attribute('request.projectprefix')[string-length()>0]
let $projectversion     := request:get-attribute('request.projectversion')[string-length()>0]

let $csmaps             := 
  for $codeSystem in $valueSet//@codeSystem
  let $csid := $codeSystem
  let $csed := $codeSystem/../@codeSystemVersion
  group by $csid, $csed
  return 
      map:entry($csid || $csed, utillib:getCanonicalUriForOID('CodeSystem', $csid, $csed, $projectPrefix, $setlib:strKeyFHIRR4))

let $vsmap              := map:entry($valueSet/@id, utillib:getCanonicalUriForOID('ValueSet', $valueSet, $projectPrefix, $setlib:strKeyFHIRR4))

let $oidfhirmap         := map:merge(($csmaps, $vsmap))
let $decorTypes         := utillib:getDecorTypes()
(:let $desigType          := $decorTypes//*:DesignationType:)
let $vocabType          := $decorTypes//*:VocabType
let $structureVersion   := <profile value="http://hl7.org/fhir/{$getf:strFhirVersionShort}/StructureDefinition/ValueSet"/>

let $codeSystems        :=
    for $cs in $valueSet/*:completeCodeSystem | $valueSet/*:conceptList/*:include[@codeSystem][empty(@code)]
    return (cs:getCodeSystemById($cs/@codeSystem, $cs/@codeSystemVersion, $projectPrefix, $projectversion)//*:codeSystem[@id])[1]

let $error              :=
    if (empty($expansion/concept)) then
        error(QName('http://art-decor.org/ns/fhir/error','UnsupportedRequest'),concat('This server does not support expansion of intensional statements found in this ValueSet.', ''))
    else ()

(:FHIR id may not have colons. This affects using @effectiveDate as part of the Resource.id
    Remove any non-digit from the effectiveDate before adding it to the URI, e.g. 20161026123456
:)
return
    <ValueSet xmlns="http://hl7.org/fhir">
        <meta>
        {
            if ($valueSet/@lastModifiedDate) then
                <lastUpdated value="{$valueSet/@lastModifiedDate || 'Z'}"/>
            else (),
            if ($projectPrefix) then 
                <source value="{$utillib:strFhirCanonicalBaseURL || $getf:strFhirVersionShort || '/' || $projectPrefix}"/>
            else ()
        }
            <profile value="http://hl7.org/fhir/StructureDefinition/shareablevalueset"/>
        {
            comment {fn:serialize($structureVersion)}
        }
            <tag><system value="http://hl7.org/fhir/FHIR-version"/><code value="{$getf:strFhirVersion}"/></tag>
        </meta>
    {
        (: Any object in ART-DECOR is effective from effectiveDate to expirationDate :)
        if ($valueSet[@effectiveDate castable as xs:dateTime] | $valueSet[@expirationDate castable as xs:dateTime]) then (
            <extension url="http://hl7.org/fhir/StructureDefinition/resource-effectivePeriod">
                <valuePeriod>
                {
                    if ($valueSet[@effectiveDate castable as xs:dateTime]) then 
                        <start value="{adjust-dateTime-to-timezone(xs:dateTime($valueSet/@effectiveDate))}"/>
                    else (),
                    if ($valueSet[@expirationDate castable as xs:dateTime]) then
                        <end value="{adjust-dateTime-to-timezone(xs:dateTime($valueSet/@expirationDate))}"/>
                    else ()
                }
                </valuePeriod>
            </extension>
        ) else ()
    }
        <url value="{map:get($oidfhirmap, $valueSet/@id)}"/>
        <identifier>
            <use value="official"/>
            <system value="urn:ietf:rfc:3986"/>
            <value value="urn:oid:{$valueSet/@id}"/>
        </identifier>
        <version value="{let $semver := adfhir:getSemverString($valueSet/@versionLabel) return if (empty($semver)) then $valueSet/@effectiveDate else $semver}"/>
        <name value="{adfhir:validResourceName($valueSet/@name)}"/>
    {
        if ($valueSet/@displayName) then <title value="{$valueSet/@displayName}"/> else ()
    }
        <status value="{adfhirvs:decorStatus2fhirStatus($valueSet/@statusCode)}"/>
        <experimental value="{$valueSet/@experimental = 'true'}"/>
    {
        if ($valueSet[*:publishingAuthority/@name]) then (
            <publisher value="{string-join($valueSet/*:publishingAuthority/@name,', ')}"/>
            ,
            for $publisher in $valueSet/*:publishingAuthority[*:addrLine/@type = ('phone', 'email', 'fax', 'uri')]
            return
            <contact>
                <name value="{$publisher/@name}"/>
            {
                for $addrLine in $publisher/*:addrLine[@type = ('phone', 'email', 'fax', 'uri')]
                let $system     := if ($addrLine/@type = 'uri') then 'url' else $addrLine/@type
                return
                <telecom>
                    <system value="{$system}"/>
                    <value value="{$addrLine}"/>
                </telecom>
            }
            </contact>
        )
        else (
            (: required for http://hl7.org/fhir/shareablevalueset.html:)
            <publisher value="ART-DECOR"/>
        )
    }
    {
        let $desc   := ($valueSet/*:desc[@language = 'en-US'][.//text()], $valueSet/*:desc[.//text()])[1]
        (: required for http://hl7.org/fhir/shareablevalueset.html:)
        let $desc   := if (empty($desc)) then ($valueSet/@displayName, $valueSet/@name, '-')[1] else markdown:html2markdown(utillib:parseNode($desc)/node())
        return
            <description value="{$desc}"/>
    }
        <immutable value="false"/>
    {
        if ($valueSet[*:purpose//text()]) then <purpose value="{utillib:serializeNode($valueSet/*:purpose[.//text()][1])/node()}"/> else (),
        if ($valueSet[*:copyright//text()]) then <copyright value="{utillib:serializeNode($valueSet/*:copyright[.//text()][1])/node()}"/> else ()
    }
    {
        adfhirvs:convertDecorValueSet2FHIRValueSet($valueSet)/*:compose
        
        (:<concept code="P" codeSystem="2.16.840.1.113883.3.1937.99.62.3.5.1" displayName="Patient" level="0" type="L">
            <designation lang="en-US" use="pref" statusCode="active" count="1" length="7">Patient</designation>
            <designation lang="nl-NL" use="pref" statusCode="active" count="1" length="7">Patiënt</designation>
        </concept>:)
    }
        <expansion>
            <identifier value="urn:uuid:{$uuid}"/>
            <timestamp value="{$timestamp}"/>
        {
            <total value="{if ($expansion/@total) then $expansion/@total else count($expansion/concept)}"/>
            ,
            for $concept in $expansion/*:concept
            let $code           := $concept/@code
            let $csid           := $concept/@codeSystem
            let $csed           := $concept/@codeSystemVersion
            let $displayName    := if ($concept/@displayName[not(. = '')]) then $concept/@displayName else $concept/*:designation[1]/@displayName
            let $designations   := if ($concept/@displayName[not(. = '')]) then $concept/*:designation else $concept/*:designation[position() gt 1]
            return
                <contains>
                    <system value="{map:get($oidfhirmap, $csid || $csed)}"/>
                {
                    if ($concept[@type = 'A'] | 
                        $concept[@abstract = 'true'] | 
                        $concept/*:property[@code = 'notSelectable']/*:valueBoolean[@value = 'true']) then 
                        <abstract value="true"/> 
                    else ()
                }
                {
                    if ($concept[@type = 'D'] | 
                        $concept[@statusCode = ('inactive', 'deprecated', 'retired', 'cancelled', 'rejected')]) then 
                        <inactive value="true"/> 
                    else ()
                }
                    <code value="{$code}"/>
                    <display value="{replace($displayName, '(^\s+)|(\s+$)', '')}"/>
                {
                    for $designation in $designations
                    return
                    <designation>
                        <language value="{$designation/@lang}"/>
                    {
                        (:https://www.hl7.org/fhir/valueset-designation-use.html:)
                        if ($designation/@use) then 
                            switch ($designation/@use)
                            case 'fsn'      return 
                                <use>
                                    <system value="http://snomed.info/sct"/>
                                    <code value="900000000000003001"/>
                                    <display value="Fully specified name"/>
                                </use>
                            default         return 
                                <use>
                                    <system value="http://snomed.info/sct"/>
                                    <code value="900000000000013009"/>
                                    <display value="Synonym"/>
                                </use>
                        else ()
                   }
                        <value value="{$designation}"/>
                    </designation>
                }
                </contains>
        }
        </expansion>
    </ValueSet>
};

declare function adfhirvs:convertDecorValueSet2FHIRValueSet($valueSet as element(valueSet)) as element() {
    (: default behavior: live resolution of included value sets, so the compiledProject parameter is empty :)
    adfhirvs:convertDecorValueSet2FHIRValueSet($valueSet, ())
};

declare function adfhirvs:convertDecorValueSet2FHIRValueSet($valueSet as element(valueSet), $compiledProject as element()?) as element() {
let $language               := ($valueSet/ancestor::*:decor/*:project/@defaultLanguage, $valueSet/*[@language = 'en-US']/@language, $valueSet/*/@language, 'en-US')[not(. = '*')][1]
(: 
    convert a DECOR value set to a FHIR value set
    default behavior: live resolution of included value sets, get projectPrefix from request attibutes
    if compiledProject parameter is submitted it is expected to contain all referenced artifatcs of the project
    all included value sets are resolved from within the compiled project and the projectPrefix is from the compiled project
:)
let $projectPrefix          := 
    if ($valueSet/@ident) then 
        $valueSet/@ident
    else 
    if (empty($compiledProject)) then 
        request:get-attribute('request.projectprefix')[string-length()>0]
    else (
        $compiledProject/*:project/@prefix
    )
let $projectversion         := request:get-attribute('request.projectversion')[string-length()>0]

let $uuid                   := util:uuid()

let $csmaps                 := 
  for $codeSystem in $valueSet//@codeSystem
  let $csid := $codeSystem
  let $csed := $codeSystem/../@codeSystemVersion
  group by $csid, $csed
  return 
      map:entry($csid || $csed, utillib:getCanonicalUriForOID('CodeSystem', $csid, $csed, $projectPrefix, $setlib:strKeyFHIRR4))

let $vsmap                  := map:entry($valueSet/@id, utillib:getCanonicalUriForOID('ValueSet', $valueSet, $projectPrefix, $setlib:strKeyFHIRR4))

let $oidfhirmap             := map:merge(($csmaps, $vsmap))
let $decorTypes             := utillib:getDecorTypes()
(:let $desigType            := $decorTypes//*:DesignationType:)
let $vocabType              := $decorTypes//*:VocabType
let $structureVersion       := <profile value="http://hl7.org/fhir/{$getf:strFhirVersionShort}/StructureDefinition/ValueSet"/>

let $publisher              := ($valueSet/*:publishingAuthority)[1]
let $publisher              := 
    if ($publisher) then $publisher else (
        $valueSet/ancestor::*:decor/*:project/*:copyright[@type = 'author'],
        $valueSet/ancestor::*:decor/*:project/*:copyright[empty(@type)]
    )[1]

(: skip abstract concepts. FHIR ValueSet does not support those :)
(: https://jira.hl7.org/browse/FHIR-17277 :)
let $conceptList            := 
    for $c in ($valueSet/*:conceptList/*:concept | $valueSet/*:conceptList/*:exception)[not(@type = 'A')]
    return $c
            
(:FHIR id may not have colons. This affects using @effectiveDate as part of the Resource.id
    Remove any non-digit from the effectiveDate before adding it to the URI, e.g. 20161026123456
:)
return
    <ValueSet xmlns="http://hl7.org/fhir">
        <id value="{concat($valueSet/@id,$getf:PARAMDECOR_ID_VERSION_SEPCHARS,encode-for-uri(replace($valueSet/@effectiveDate,'[^\d]','')))}"/>
        <meta>
        {
            if ($valueSet/@lastModifiedDate) then
                <lastUpdated value="{$valueSet/@lastModifiedDate || 'Z'}"/>
            else (),
            if ($projectPrefix) then 
                <source value="{$utillib:strFhirCanonicalBaseURL || $getf:strFhirVersionShort || '/' || $projectPrefix}"/>
            else ()
        }
            <profile value="http://hl7.org/fhir/StructureDefinition/shareablevalueset"/>
        {
            comment {fn:serialize($structureVersion)}
        }
            <tag><system value="http://hl7.org/fhir/FHIR-version"/><code value="{$getf:strFhirVersion}"/></tag>
        </meta>
    {
        (: Any object in ART-DECOR is effective from effectiveDate to expirationDate :)
        if ($valueSet[@effectiveDate castable as xs:dateTime] | $valueSet[@expirationDate castable as xs:dateTime]) then (
            <extension url="http://hl7.org/fhir/StructureDefinition/resource-effectivePeriod">
                <valuePeriod>
                {
                    if ($valueSet[@effectiveDate castable as xs:dateTime]) then 
                        <start value="{adjust-dateTime-to-timezone(xs:dateTime($valueSet/@effectiveDate))}"/>
                    else (),
                    if ($valueSet[@expirationDate castable as xs:dateTime]) then
                        <end value="{adjust-dateTime-to-timezone(xs:dateTime($valueSet/@expirationDate))}"/>
                    else ()
                }
                </valuePeriod>
            </extension>
        ) else ()
    }
        <url value="{map:get($oidfhirmap, $valueSet/@id)}"/>
        <identifier>
            <use value="official"/>
            <system value="urn:ietf:rfc:3986"/>
            <value value="urn:oid:{$valueSet/@id}"/>
        </identifier>
        <version value="{let $semver := adfhir:getSemverString($valueSet/@versionLabel) return if (empty($semver)) then $valueSet/@effectiveDate else $semver}"/>
        <name value="{adfhir:validResourceName($valueSet/@name)}"/>
    {
        if ($valueSet/@displayName) then <title value="{$valueSet/@displayName}"/> else ()
    }
        <status value="{adfhirvs:decorStatus2fhirStatus($valueSet/@statusCode)}"/>
        <experimental value="{$valueSet/@experimental = 'true'}"/>
    {
        (: publisher 0..1 :)
        (: contact 0..* :)
        (: required for http://hl7.org/fhir/shareablevalueset.html:)
        if ($publisher) then (
            adfhir:decorPublishingAuthority2fhirPublisher($publisher),
            adfhir:decorPublishingAuthority2fhirContact($publisher)
        )
        else (
            <publisher value="ART-DECOR"/>
        )
    }
    {
        let $desc   := ($valueSet/*:desc[@language = 'en-US'][.//text()], $valueSet/*:desc[.//text()])[1]
        (: required for http://hl7.org/fhir/shareablevalueset.html:)
        let $desc   := if (empty($desc)) then ($valueSet/@displayName, $valueSet/@name, '-')[1] else markdown:html2markdown(utillib:parseNode($desc)/node())
        return
            <description value="{$desc}"/>
    }
        <immutable value="false"/>
    {
        adfhir:decorPurpose2fhirPurpose($valueSet/*:purpose, $language),
        adfhir:decorCopyright2fhirCopyright($valueSet/*:copyright, $language)
    }
        <compose>
        {
            (: note that this boolean signifies expanding inactive codes. we do not formally know if the Deprecated concept
               in our ValueSet definition is actually deprecated in the CodeSystem or just in the ValueSet. Hence we cannot say 
               for sure that inactive=true. However ... deprecation in a value set is more likely to occur based on deprecation in
               the CodeSystem than just the ValueSet and for all sense and purposes: the implementer probably would not care 
               about the diferrence, so we signal inactive=true ... if only because the deprecated extension does not support
               expand as context so we can only mark inactive=true there and the distinction is gone then anyway
            :)
            if ($conceptList[@type = 'D']) then
                <inactive value="true"/>
            else ()
        }
        {
            for $completeCodeSystem in $valueSet/*:completeCodeSystem | $valueSet/*:conceptList/*:include[@codeSystem][empty(@code)]
            let $filter         := 
                for $f in $completeCodeSystem/*:filter
                let $operation := if ($f/@op='equal') then '=' else $f/@op
                return
                    <filter>
                        <property value="{$f/@property}"/>
                        <op value="{$operation}"/>
                        <value value="{$f/@value}"/>
                    </filter>
            return
                <include>
                {
                    <system value="{map:get($oidfhirmap, $completeCodeSystem/@codeSystem || $completeCodeSystem/@codeSystemVersion)}"/>,
                    $filter
                }
                </include>
        }
        {
            for $concepts in $conceptList
            let $codeSystem     := $concepts/@codeSystem
            group by $codeSystem
            return
                <include>
                {
                    if ($concepts[1]/@codeSystem) then 
                        <system value="{map:get($oidfhirmap, $concepts[1]/@codeSystem || $concepts[1]/@codeSystemVersion)}"/>
                    else ()
                }
                {
                    for $concept in $concepts
                    return
                        <concept>
                        {
                            if ($concept[@ordinal]) then (
                                <extension url="http://hl7.org/fhir/StructureDefinition/ordinalValue">
                                    <valueDecimal value="{$concept/@ordinal}"/>
                                </extension>
                            ) else ()
                        }
                        {
                            (: See https://jira.hl7.org/browse/FHIR-17276
                                We do not know why something is D in a valueSet, so we cannot positively state it is inactive in the CodeSystem
                                This property is now conveyed in the valueset-deprecated extension.
                            :)
                            if ($concept[@type = 'D']) then (
                                <extension url="http://hl7.org/fhir/StructureDefinition/valueset-deprecated">
                                    <valueBoolean value="true"/>
                                </extension>
                            ) else ()
                        }
                        {
                            (: Note that there are two extensions that might be relevant:
                                http://hl7.org/fhir/StructureDefinition/valueset-concept-comments
                                http://hl7.org/fhir/StructureDefinition/valueset-concept-definition
                                Chose of comments because the concept/desc is more notational in nature.
                            :)
                            if ($concept/*:desc) then (
                                <extension url="http://hl7.org/fhir/StructureDefinition/valueset-concept-comments">
                                {
                                    if ($concept/*:desc[@language = 'en-US']) then 
                                        <valueString value="{replace(string-join($concept/*:desc[@language = 'en-US'], ''), '(^\s+)|(\s+$)', '')}"/>
                                    else (
                                        <valueString value="{replace(string-join($concept/*:desc[1], ''), '(^\s+)|(\s+$)', '')}"/>
                                    )
                                }
                                </extension>
                            ) else ()
                        }
                            <code value="{$concept/@code}"/>
                            <display value="{replace($concept/@displayName, '(^\s+)|(\s+$)', '')}"/>
                        {
                            for $designation in $concept/*:designation
                            return
                            <designation>
                                <language value="{$designation/@language}"/>
                            {
                                (:https://www.hl7.org/fhir/valueset-designation-use.html:)
                                if ($designation/@type) then
                                    switch ($designation/@type)
                                    case 'fsn'      return 
                                        <use>
                                            <system value="http://snomed.info/sct"/>
                                            <code value="900000000000003001"/>
                                            <display value="Fully specified name"/>
                                        </use>
                                    default         return 
                                        <use>
                                            <system value="http://snomed.info/sct"/>
                                            <code value="900000000000013009"/>
                                            <display value="Synonym"/>
                                        </use>
                                else ()
                            }
                                <value value="{replace($designation/@displayName, '(^\s+)|(\s+$)', '')}"/>
                            </designation>
                        }
                        </concept>
                }
                </include>
        }
        {
            for $include in $valueSet/*:conceptList/*:include[@ref]
            let $vsed   := if ($include/@flexibility castable as xs:dateTime) then $include/@flexibility else ('dynamic')
            (: 
                get the included value set from anywhere ...
                ... unless there is a compiled canned project available (submitted as parameter)
            :)
            let $vs     := 
                if (empty($compiledProject))
                then vsapi:getValueSetById($include/@ref, $vsed, $projectPrefix, $projectversion, ())[@id][1]
                else ($compiledProject//valueSet[@id=$include/@ref][@effectiveDate=$vsed])[1]
            return
                if (empty($vs)) then ()
                else
                <include>
                    <valueSet value="{utillib:getCanonicalUriForOID('ValueSet', $vs, $projectPrefix, $setlib:strKeyFHIRR4)}"/>
                </include>
        }
        {
            (: NOTE: The property concept is appropriate to SNOMED CT, but not for other systems. Unfortunately there is no other hint in the DECOR format... :)
            for $include in $valueSet/*:conceptList/*:include[@op]
            let $operation      := if ($include/@op='equal') then '=' else $include/@op
            let $csid           := $include/@codeSystem
            let $csed           := $include/@codeSystemVersion
            let $code           := $include/@code
            (:let $displayName    := $include/@displayName:)
            return
                <include>
                    <system value="{map:get($oidfhirmap, $csid || $csed)}"/>
                    <filter>
                        <property value="concept"/>
                        <op value="{$operation}"/>
                        <value value="{$code}"/>
                    </filter>
                </include>
        }
        {
            (: NOTE: The property concept is appropriate to SNOMED CT, but not for other systems. Unfortunately there is no other hint in the DECOR format... :)
            for $exclude in $valueSet/*:conceptList/*:exclude[@op]
            let $operation      := if ($exclude/@op='equal') then '=' else $exclude/@op
            let $csid           := $exclude/@codeSystem
            let $csed           := $exclude/@codeSystemVersion
            let $code           := $exclude/@code
            (:let $displayName    := $include/@displayName:)
            return
                <exclude>
                    <system value="{map:get($oidfhirmap, $csid || $csed)}"/>
                    <filter>
                        <property value="concept"/>
                        <op value="{$operation}"/>
                        <value value="{$code}"/>
                    </filter>
                </exclude>
        }
        </compose>
    </ValueSet>
};

declare function adfhirvs:getExpandedValueSetById($_id as xs:string, $_version as xs:string?) as element(f:ValueSet)* {
    let $entries    := adfhir:getResource($adfhirvs:type,$_id, $_version)
    
    for $entry in $entries[empty(@deleted)]
    return adfhirvs:getExpandedValueSet($entry/*/f:ValueSet)
};

declare function adfhirvs:getExpandedValueSetByUri($uri as xs:anyURI) as element(f:ValueSet)? {
    () (:TODO. See Gforge #5554 / #5555:)
};

declare function adfhirvs:getExpandedValueSet($valueSet as element(f:ValueSet)) as element(f:ValueSet) {
    adfhirvs:getExpandedValueSet($valueSet, ())
};

declare function adfhirvs:getExpandedValueSet($valueSet as element(f:ValueSet), $filter as xs:string*) as element(f:ValueSet) {
    <ValueSet xmlns="http://hl7.org/fhir" xmlns:f="http://hl7.org/fhir">
    {
        $valueSet/(f:* except (f:define|f:expansion|f:compose))
        ,
        if ($valueSet[f:expansion]) then (
            $valueSet/f:expansion
        ) else if ($valueSet[f:define|f:compose]) then (
            <expansion>
                <identifier>
                    <system value="{$getf:strFhirServices}"/>
                    <value value="{util:uuid()}"/>
                </identifier>
                <timestamp value="{current-dateTime()}"/>
            {
                for $define in $valueSet/f:define
                let $system             := $define/f:system
                let $version            := $define/f:version
                let $caseInSensitive    := $define/f:caseSensitive/@value='false'
                return
                    if ($define[not(*)]) then (
                        <contains>
                            <system value="{$system}"/>
                        </contains>
                    ) else (
                        for $concept in $define/f:concept
                        return adfhirvs:getContainsForConceptDefinition($concept, $system, $version, $caseInSensitive)
                    )
            }
            {
                for $compose in $valueSet/f:compose[f:include|f:exclude]
                return adfhirvs:getContainsForCompose($compose, $filter)
            }
            </expansion>
        ) else ()
    }
    </ValueSet>
};

(:
    <include>
        <system value="http://loinc.org"/>
        <version value="2.36"/>
        <!--   for LOINC, we simply include the listed codes - no subsumption in LOINC   -->
        <!--   these were selected by hand   -->
        <concept>
            <code value="14647-2"/>
        </concept>
        <concept>
            <code value="2093-3"/>
        </concept>
        <concept>
            <code value="35200-5"/>
        </concept>
        <concept>
            <code value="9342-7"/>
        </concept>
    </include>
    ,
    <include>
        <system value="http://hl7.org/fhir/v3/RoleCode"/>
        <filter>
            <property value="concept"/>
            <op value="is-a"/>
            <value value="PRN"/>
        </filter>
    </include>
    ,
    <include>
        <system value="http://loinc.org"/>
        <filter>
            <property value="SCALE_TYP"/>
            <op value="="/>
            <value value="DOC"/>
        </filter>
    </include>
    ,
    <include>
        <system value="http://snomed.info/sct"/>
        <filter>
            <!--  SNOMED CT - selected children of SCTID:91723000 "anatomical structure"  -->
            <property value="concept"/>
            <op value="is-a"/>
            <value value="91723000"/>
        </filter>
    </include>
    ,
    <include>
        <system value="http://loinc.org"/>
        <filter>
            <property value="ORDER_OBS"/>
            <op value="="/>
            <value value="Order"/>
        </filter>
    </include>
    ,
    <include>
        <system value="http://loinc.org"/>
        <filter>
            <!-- All LOINC codes that are methods. 
            These will all have codes starting with "LP" -->
            <property value="Type"/>
            <op value="="/>
            <value value="Method"/>
        </filter>
    </include>
    ,
    <exclude>
        <system value="http://hl7.org/fhir/nutrition-order-status"/>
        <concept>
            <code value="cancelled"/>
        </concept>
    </exclude>
:)
declare %private function adfhirvs:getContainsForCompose($compose as element(f:compose), $filter as xs:string*) as element(f:contains)* {
let $valueSets  :=
    for $import in $compose/f:import
    return adfhirvs:getExpandedValueSetByUri($import/@value)//f:expansion/f:contains

let $includes   :=
    for $include in $compose/f:include
    return adfhirvs:handleIncludeExclude($include)
    
let $excludes   :=
    for $exclude in $compose/f:exclude
    return adfhirvs:handleIncludeExclude($exclude)

for $concept in ($valueSets | $includes)
return
    if ($excludes[f:system/@value=$concept/f:system/@value][f:code/@value=$concept/f:code/@value]) then ()
    else if (empty($filter)) then (
        $concept
    )
    else (
        $concept[contains(f:code/@value, $filter[1])]
    )
};

(:
Code        Definition
=           The specified property of the code equals the provided value.
is-a        The specified property of the code has an is-a relationship with the provided value.
is-not-a    The specified property of the code does not have an is-a relationship with the provided value.
regex       The specified property of the code matches the regex specified in the provided value.
in          The specified property of the code is in the set of codes or concepts specified in the provided value (comma separated list).
not in      The specified property of the code is not in the set of codes or concepts specified in the provided value (comma separated list).

<f:filter>
    <f:property value="concept"/>
    <f:op value="is-a"/>
    <f:value value="TWIN"/>
</f:filter>
:)
declare function adfhirvs:handleIncludeExclude($includeExclude as element(f:include)) as element(f:include)* {
    let $system         := $includeExclude/f:system
    let $define         := adfhir:getValueSetBySystem($system/@value)//f:define
    
    return
    if ($system/@value=('http://loinc.org','urn:oid:2.16.840.1.113883.6.1')) then (
        let $version            := $includeExclude/f:version
        let $loincversion       := <version xmlns="http://hl7.org/fhir" value="{adloinc:getVersionInfo()/@version}"/>
        let $caseInSensitive    := false()
        let $concepts           := adfhirvs:handleLoincFilter($adloinc:colLoincDb/concept,$includeExclude/f:filter)
        
        for $concept in ($concepts | $includeExclude/f:concept)
        return adfhirvs:getContainsForLoincConcept($concept,$system,$loincversion,$caseInSensitive)
    )
    else if ($system/@value=('http://snomed.info/sct','urn:oid:2.16.840.1.113883.6.96')) then (
        let $version            := $includeExclude/f:version
        let $snomedversion      := <version xmlns="http://hl7.org/fhir" value="{adsnomed:searchDescription('sno cli ver', 1, (), ())/*[1]/text()}"/>
        (:let $snomedversion      := <version xmlns="http://hl7.org/fhir" value=""/>:)
        let $caseInSensitive    := false()
        let $concepts           := adfhirvs:handleSnomedFilter((),$includeExclude/f:filter)
        
        for $concept in ($concepts | $includeExclude/f:concept)
        let $code := $concept/f:code/@value
        group by $code
        return adfhirvs:getContainsForSnomedConcept($concept[1],$system,$snomedversion,$caseInSensitive)
    )
    else if ($define) then (
        let $version            := $define/f:version
        let $caseInSensitive    := $define/f:caseSensitive/@value='false'
        let $filters            := $includeExclude/f:filter
        let $concepts           := if ($includeExclude[f:filter]) then (adfhirvs:handleFhirFilter($define/f:concept, $includeExclude/f:filter)) else ()
        
        for $concept in ($concepts | $includeExclude/f:concept)
        return adfhirvs:getContainsForConceptDefinition($concept,$system,$version,$caseInSensitive)
    )
    else if ($includeExclude[not(f:filter)]) then (
        let $version            := $includeExclude/f:version
        let $caseInSensitive    := $includeExclude/f:caseSensitive/@value='false'
        
        for $concept in $includeExclude/f:concept
        return adfhirvs:getContainsForConceptDefinition($concept,$system,$version,$caseInSensitive)
    ) else (
        let $systemUri  := $includeExclude/f:system/@value
        let $systemOid  := ()
        let $conceptSet := ()
        
        return ()
    )
};

declare %private function adfhirvs:handleFhirFilter($concepts as element(f:concept)*, $filters as element(f:filter)*) as element(f:concept)* {
let $filter         := $filters[1]
let $op             := $filter/f:op/@value
let $col            := $filter/f:property/@value
let $val            := $filter/f:value/@value
let $opt            := adfhir:getSimpleLuceneOptions()

let $results        := 
    if ($op = '=') then (
        let $search := adfhir:getSimpleLuceneQuery($val,'term')
        
        for $c in $concepts//f:code[ft:query(@value,$search,$opt)]/parent::f:concept
        return
            <concept xmlns="http://hl7.org/fhir">{$c/@*, $c/(* except f:concept)}</concept>
    )
    else if ($op = 'regex') then (
        let $search := adfhir:getSimpleLuceneQuery($val,'regex')
        
        for $c in $concepts//f:code[ft:query(@value,$search,$opt)]/parent::f:concept
        return
            <concept xmlns="http://hl7.org/fhir">{$c/@*, $c/(* except f:concept)}</concept>
    )
    else if ($op = 'is-a') then (
        let $search := adfhir:getSimpleLuceneQuery($val,'term')
        
        return $concepts//f:code[ft:query(@value,$search,$opt)]/parent::f:concept
    )
    else if ($op = 'is-not-a') then (
        let $search := adfhir:getSimpleLuceneQuery($val,'term-not')
        
        return $concepts//f:code[ft:query(@value,$search,$opt)]/parent::f:concept
    )
    else if ($op = 'in') then (
        let $search := adfhir:getSimpleLuceneQuery(tokenize($val,','),'term')
        
        for $c in $concepts//f:code[ft:query(@value,$search,$opt)]/parent::f:concept
        return
            <concept xmlns="http://hl7.org/fhir">{$c/@*, $c/(* except f:concept)}</concept>
    )
    else if ($op = ('not in','not-in')) then (
        let $search := adfhir:getSimpleLuceneQuery(tokenize($val,','),'term-not')
        
        for $c in $concepts//f:code[ft:query(@value,$search,$opt)]/parent::f:concept
        return
            <concept xmlns="http://hl7.org/fhir">{$c/@*, $c/(* except f:concept)}</concept>
    )
    else (
        $concepts
    )

return
    if (exists($filters[2])) then (
        adfhirvs:handleFhirFilter($results, subsequence($filters,2))
    )
    else (
        $results
    )
};

declare %private function adfhirvs:handleLoincFilter($concepts as element(concept)*, $filters as element(f:filter)*) as element(concept)* {
let $filter         := $filters[1]
let $op             := $filter/f:op/@value
let $col            := $filter/f:property/@value
let $val            := $filter/f:value/@value
let $opt            := adfhir:getSimpleLuceneOptions()
let $search         :=
    if ($op = '=') then (
        adfhir:getSimpleLuceneQuery($val,'term')
    ) else if ($op = 'regex') then (
        adfhir:getSimpleLuceneQuery($val,'regex')
    ) else (
        (:should we error? we likely got "is-a" "is-not-a" "not-in"/"not in" , which is not supported for LOINC:)
    )
let $results        :=
    if (empty($filter)) then (
        (:potentially returns all of loinc:)
        $concepts
    )
    else if (empty($search)) then (
        (:... got unsupported filter operation ...:)
    )
    else if ($col='LOINC_NUM') then (
        $concepts[ft:query(@loinc_num,$search, $opt)]
    )
    else if ($col='COMPONENT') then (
        $concepts/*:component[ft:query(.,$search, $opt)]/parent::*:concept
    )
    else if ($col='PROPERTY') then (
        $concepts/*:property[ft:query(.,$search, $opt)]/parent::*:concept
    )
    else if ($col='TIME_ASPCT') then (
        $concepts/*:timing[ft:query(.,$search, $opt)]/parent::*:concept
    )
    else if ($col='SYSTEM') then (
        $concepts/*:system[ft:query(.,$search, $opt)]/parent::*:concept
    )
    else if ($col='SCALE_TYP') then (
        $concepts/*:scale[ft:query(.,$search, $opt)]/parent::*:concept
    )
    else if ($col='METHOD_TYP') then (
        $concepts/*:method[ft:query(.,$search, $opt)]/parent::*:concept
    )
    else if ($col='EXAMPLE_UCUM_UNITS') then (
        $concepts/*:exUCUMunits[ft:query(.,$search, $opt)]/parent::*:concept
    )
    else if ($col='EXAMPLE_UNITS') then (
        $concepts/*:exUnits[ft:query(.,$search, $opt)]/parent::*:concept
    )
    else if ($col='CLASS') then (
        $concepts/*:class[ft:query(.,$search, $opt)]/parent::*:concept
    )
    else if ($col='LONG_COMMON_NAME') then (
        $concepts/*:longName[ft:query(.,$search, $opt)]/parent::*:concept
    )
    else if ($col='SHORTNAME') then (
        $concepts/*:shortName[ft:query(.,$search, $opt)]/parent::*:concept
    )
    else if ($col='ORDER_OBS') then (
        $concepts/*:orderObs[ft:query(.,$search, $opt)]/parent::*:concept
    )
    else (
        $concepts/*[@name=$col][ft:query(.,$search, $opt)]/parent::*:concept
    )

return
    if (exists($filters[2])) then (
        adfhirvs:handleLoincFilter($results, subsequence($filters,2))
    )
    else (
        $results
    )
};

(:~
Description         Select a set of concepts based on subsumption testing
Property Name       concept
Operations Allowed  is-a
Values Allowed      [concept id]
Comments            Includes all concept ids that have a transitive is-a relationship with the concept Id provided as the value

Description         Select a set of concepts based on their membership of a SNOMED CT reference set
Property Name       concept
Operations Allowed  in
Values Allowed      [concept id]

Description         Select a set of concepts based on a formal expression statement
Property Name       expression
Operations Allowed  =
Values Allowed      [expression]
Comments            The result of the filter is the result of executing the given SNOMED CT expression. note: the 
                    query statement is under current development
:)
declare %private function adfhirvs:handleSnomedFilter($concepts as element(concept)*, $filters as element(f:filter)*) as element(concept)* {
let $filter         := $filters[1]
let $op             := $filter/f:op/@value
let $col            := $filter/f:property/@value
let $val            := $filter/f:value/@value
    
let $results        :=
    if (empty($filter)) then (
        (:potentially returns all of snomed:)
        $concepts
    )
    else if ($op = 'is-a') then (
        adfhirvs:getConceptHierarchy(adsnomed:getRawConcept($val), ())
    ) else if ($op = 'in') then (
        (:adsnomed:getRefsetMembersByStatus($val,'active')/concept:)
    ) else if ($op = '=') then (
        (:TODO:)
    ) else (
        (:should we error? we likely got "is-a" "is-not-a" "not-in"/"not in" , which is not supported for LOINC:)
    )

return
    if (exists($filters[2])) then (
        adfhirvs:handleSnomedFilter($results, subsequence($filters,2))
    )
    else (
        $results
    )
};

declare %private function adfhirvs:getContainsForConceptDefinition($concept as element(), $system as element(f:system), $version as element(f:version)?, $caseInSensitive as xs:boolean) as element(f:contains)? {
    <contains xmlns="http://hl7.org/fhir">
        <system value="{$system/@value}">
        {
            adfhirvs:getCaseSensitiveExtension($caseInSensitive)
        }
        </system>
        {
            $concept/f:abstract,
            $version,
            $concept/f:code,
            $concept/f:display,
            for $subconcept in $concept/f:concept
            return adfhirvs:getContainsForConceptDefinition($subconcept, $system, $version, $caseInSensitive)
        }
    </contains>

};

declare %private function adfhirvs:getContainsForLoincConcept($concept as element(), $system as element(f:system), $version as element(f:version)?, $caseInSensitive as xs:boolean) as element(f:contains)? {
    <contains xmlns="http://hl7.org/fhir">
        <system value="{$system/@value}">
        {
            adfhirvs:getCaseSensitiveExtension($caseInSensitive)
        }
        </system>
        {$version}
        <code value="{$concept/@loinc_num | $concept/f:code/@value}"/>
        <display value="{replace(($concept/*:longName | $concept/f:display/@value)[1], '(^\s+)|(\s+$)', '')}"/>
        {
            for $subconcept in $concept/f:concept
            return adfhirvs:getContainsForLoincConcept($subconcept, $system, $version, $caseInSensitive)
        }
    </contains>

};

declare %private function adfhirvs:snomedAsFhirConcept($concept as element(concept)) as element(f:concept) {
    <concept xmlns="http://hl7.org/fhir">
        <code value="{$concept/@conceptId}"/>
        <display value="{$concept/*:desc[@type='pref'][@active='1'][1]}"/>
    </concept>
};
declare %private function adfhirvs:getConceptHierarchy($concept as element(concept)?, $results as element()*) as element(concept)* {
    let $descendants    := $concept/dest[@active][not(@sourceId=$results/f:code/@value)]
    return
    if ($descendants) then (
        for $dest in $descendants
        let $subconcept         := adsnomed:getRawConcept($dest/@sourceId)
        return adfhirvs:getConceptHierarchy($subconcept, ($results | adfhirvs:snomedAsFhirConcept($concept)))
    )
    else if ($concept) then (
        $results | adfhirvs:snomedAsFhirConcept($concept)
    )
    else (
        $results
    )
};

declare %private function adfhirvs:getContainsForSnomedConcept($concept as element(), $system as element(f:system), $version as element(f:version)?, $caseInSensitive as xs:boolean) as element(f:contains)? {
    <contains xmlns="http://hl7.org/fhir">
        <system value="{$system/@value}">
        {
            adfhirvs:getCaseSensitiveExtension($caseInSensitive)
        }
        </system>
        {$version}
        {$concept/f:code}
        {$concept/f:display}
        {
            for $subconcept in $concept/*:concept
            return adfhirvs:getContainsForSnomedConcept($subconcept, $system, $version, $caseInSensitive)
        }
    </contains>

};

declare %private function adfhirvs:getCaseSensitiveExtension($caseInSensitive as xs:boolean) as element(f:extension)? {
    if ($caseInSensitive) then (
        <extension url="http://example.org/fhir/StructureDefinition/codeSystemCaseSensitive/_history/1"  xmlns="http://hl7.org/fhir">
            <valueBoolean value="false"/>
        </extension>
    ) else ()
};

(:~ http://hl7.org/fhir/R4/valueset-publication-status.html :)
declare function adfhirvs:decorStatus2fhirStatus($status as xs:string?) as xs:string? {
    switch ($status)
    case 'new'          return 'draft'
    case 'draft'        return 'draft'
    case 'pending'      return 'draft'
    case 'final'        return 'active'
    case 'cancelled'    return 'retired'
    case 'rejected'     return 'retired'
    case 'deprecated'   return 'retired'
    default             return 'draft'
};

(:~ http://hl7.org/fhir/R4/valueset-publication-status.html :)
declare function adfhirvs:fhirStatus2decorStatus($status as xs:string?) as xs:string? {
    switch ($status)
    case 'draft'        return 'draft'
    case 'active'       return 'final'
    case 'retired'      return 'deprecated'
    default             return ()
};

(: Check ID of valueSet for update :)
declare function adfhirvs:checkIdValueSet($id as xs:string, $valueSet as element(f:ValueSet)*) {
    adfhirvs:checkIdValueSet($id, $valueSet, ())

};
declare function adfhirvs:checkIdValueSet($id as xs:string, $valueSet as element(f:ValueSet)*, $bodyId as xs:string?) {
    
    (: check id :)
    let $check                  :=
        if ($bodyId) then (
            if ($bodyId = $id) then () else (
                let $detailcode         := 'MSG_INVALID_ID'
                let $details            := 'The ValueSet request url id ' || $id || ' shall match the ValueSet request body id ' || $bodyId || '.'
                return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details)
            )
        ) else () 
    
    let $fhirVersion            := 
                $valueSet/f:meta/f:tag[f:system/@value = 'http://hl7.org/fhir/FHIR-version']/f:code/@value
        
    return
    (: check OK ValueSet exactly occurs one time with matching occurence of fhirVersion :)
    switch (count($valueSet))
        case 0 return (
            let $detailcode         := 'MSG_NO_EXIST'
            let $details            := 'The ValueSet request url id ' || $id || ' does not exist.'
            return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details)
        )
        case 1 return (
            if (doc-available(util:collection-name($valueSet) || '/'|| $id || '.xml')) then 
                let $detailcode         := 'MSG_DELETED_ID'
                let $details            := 'The ValueSet request url id ' || $id || ' is already removed from the project.' 
                return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details) 
            else 
            if (empty($fhirVersion)) then
                let $detailcode         := 'MSG_VERSION_AWARE_CONFLICT'
                let $details            := 'The requested ValueSet with id ' || $id || ' is missing a version tag and cannot be determined to be compatible with this FHIR Server version ' || $getf:strFhirVersion || '. Please inform your server administrator of this issue.'
                return error(QName('http://hl7.org/fhir/ValueSet/operation-outcome', $detailcode), $details)
            else
            if ($fhirVersion = ($getf:strFhirVersionShort, $getf:strFhirVersion)) then () 
            else 
                let $detailcode         := 'MSG_VERSION_AWARE_CONFLICT'
                let $details            := 'The requested ValueSet with id ' || $id || ' is not compatible with this FHIR Server version ' || $getf:strFhirVersion || '. Please retrieve using FHIR Server version ' || string-join($fhirVersion, ', or ')
                return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details) 
        )
        default return (
            let $detailcode         := 'MSG_DUPLICATE_ID'
            let $details            := 'The ValueSet request url id ' || $id || ' occurred ' || count($valueSet) || ' times. Please inform your system administrator.'
            return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details)
        ) 
};
declare function adfhirvs:checkDecorValueSet($authmap as map(*), $projectPrefix as xs:string?, $valueSet as element(f:ValueSet)?) as element(decor) {

    (: check projectprefix and permissions :)
    let $storedDecor                    := if ($valueSet) then collection(util:collection-name($valueSet)||'/../')/decor else ()
    let $pathDecor                      := if ($projectPrefix) then utillib:getDecor($projectPrefix, (), ()) else ()
    
    let $check                          :=
        if (empty($pathDecor) or empty($storedDecor)) then () else 
        if ($storedDecor/project/@prefix = $pathDecor/project/@prefix) then () else (
            let $detailcode         := 'MSG_RESOURCE_MISMATCH'
            let $details            := 'This resource is part of ' || $storedDecor/project/@prefix || ', but requested to save in ' || $pathDecor/project/@prefix || '. Please use the project prefix ' || $storedDecor/project/@prefix || ' or ''public''.'
            return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details)
        )
    
    let $decor                          := ($storedDecor, $pathDecor)[1]
    let $check                          := 
        if ($decor) then (
            if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-RULES)) then () else (
                let $detailcode         := 'MSG_RESOURCE_NOT_ALLOWED'
                let $details            := 'User ' || $authmap?name || ' does not have sufficient permissions to modify ValueSets in project ' || $decor/project/@prefix || '. You have to be an active author in the project.'
                return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details)
            )
        ) 
        else (
            let $detailcode             := 'MSG_PARAM_INVALID'
            let $details                := 'Project with prefix ' || $decor/project/@prefix || ' does not exist'
            return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details)
        )
       
    return $decor
};

declare function adfhirvs:saveValueSet($body as element(f:ValueSet), $decor as element(), $id as xs:string?, $projectPrefix as xs:string?) as element(f:ValueSet) {

    let $id                     := if (empty($id)) then (util:uuid()) else ($id)
    
    let $resource            :=
        <ValueSet xmlns="http://hl7.org/fhir">
            <id value="{$id}"/>
            {utilfhir:createMeta($getf:strFhirVersion, $projectPrefix)}
            {$body/(node() except (f:id | f:meta))}
        </ValueSet>    
  
    (: check result is scheme and schematron compliant :)
    let $validation       :=  adfhir:validate($resource)
    let $check            :=
        if ($validation[@role = ('error', 'warn', 'warning', 'fatal')]) then (
            let $detailcode         := 'MSG_BAD_SYNTAX'
            let $details            := 'The ValueSet to save has structural and/or business rule related issues'
            return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details)
        ) else ()
    
    let $entry            := 
        <entry xmlns="http://hl7.org/fhir">
            <fullUrl value="urn:uuid:{$id}"/>
            <resource>
                {$resource}
            </resource>
        </entry>
 
    let $storeResource              := utilfhir:storeBundleInDecor($getf:strFhirVersionShort, $decor, $resource)
    let $currentVS                  := $storeResource//f:ValueSet[f:id[@value = $resource/*:id/@value]]
    let $save                       := if ($currentVS) then (update replace $currentVS with $resource ) else (update insert $entry into $storeResource)
 
    return ($resource)
};

declare function adfhirvs:saveValueSetToDecor($fhirValueSet as element(f:ValueSet), $decor as element(), $id as xs:string?, $projectPrefix as xs:string?) as element() {
 
    (: Check if valueset already exists :)
    let $exists := 
         if ($decor//valueSet[@name=$fhirValueSet/f:name/@value]) then
            let $detailcode         := 'MSG_DUPLICATE_ID'
            let $details            := 'The ValueSet already exists in the project'
            return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details)
         else ()
        
    (: check if incoming value set is schema and schematron compliant :)
    let $validation       :=  adfhir:validate($fhirValueSet)
    let $check            :=
        if ($validation[@role = ('error', 'warn', 'warning', 'fatal')]) then (
            let $detailcode         := 'MSG_BAD_SYNTAX'
            let $details            := 'The ValueSet to save has structural and/or business rule related issues'
            return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details)
        ) else ()

   let $language := $decor/project/@defaultLanguage
   let $vsId := if ($fhirValueSet/f:identifier[f:system/@value = 'urn:ietf:rfc:3986']) then
                  $fhirValueSet/f:identifier[f:system/@value = 'urn:ietf:rfc:3986']/substring-after(f:value/@value, 'urn:oid:')
                else error(QName('http://art-decor.org/ns/fhir/error','MissingData'),concat('OID for value set missing.', ''))
                  
   let $vsEffectiveDate := format-dateTime(xs:dateTime(concat($fhirValueSet/f:date/@value, 'T00:00:00')), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')
   let $fhirVsStatus2decorVsStatus := map {
      'draft': 'draft',
      'active': 'final',
      'retired': 'deprecated'
   }
   let $decorStatus := if ($fhirVsStatus2decorVsStatus($fhirValueSet/f:status/@value)) then
      $fhirVsStatus2decorVsStatus($fhirValueSet/f:status/@value)
   else
      'draft'
   
   let $fhirContactSystem2decorAddrLineType := map {
      'phone': 'phone',
      'fax': 'fax',
      'email': 'email',
      'url': 'uri'
   }
   
   let $conceptList :=
      for $include in $fhirValueSet/f:compose/f:include
      let $codeSystemOid := 
            if ($decor//id[property[@name='HL7-FHIR-System-URI-Preferred']=$include/f:system/@value]) then
               $decor//id[property[@name='HL7-FHIR-System-URI-Preferred']=$include/f:system/@value]/@root
            else if ($fhirValueSet/f:compose/f:include/f:extension[@url='http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid']) then
               let $systemOid := $fhirValueSet/f:compose/f:include/f:extension/substring-after(f:valueOid/@value, 'urn:oid:')
               let $ifRef :=  <id root="{$systemOid}">
                                 {
                                    for $lang in distinct-values($decor/project/name/@language)
                                    return
                                    <designation language="{$lang}" type="preferred" displayName="{$fhirValueSet/f:name/@value}">{$fhirValueSet/f:name/@value/string()}</designation>
                                 }
                                 <property name="HL7-FHIR-System-URI-Preferred">{$include/f:system/@value/string()}</property>
                              </id>
               let $idInsert :=  if ($decor//ids/identifierAssociation) then
                                    update insert $ifRef preceding $decor//ids/identifierAssociation[1]
                                 else
                                    update insert $ifRef into $decor//ids
               return
               $systemOid
            else error(QName('http://art-decor.org/ns/fhir/error','MissingData'),concat('OID for code system cannot be found.', ''))
      let $codeSystemVersion := $include/f:version/@value
      for $concept in $include/f:concept
      let $conceptType :=
            if ($concept/f:abstract/@value='true') then 'A'
            else if ($concept/f:concept) then 'S'
            else 'L'
      let $ancestors := 
            for $aConcept in $concept/f:property[f:code/@value='subsumedBy']
            return
            (
            <id code="{$aConcept/f:valueCode/@value}" distance="1"/>
            ,
            adfhirvs:resolveAncestors($fhirValueSet, $aConcept, 2)
            )
            
      let $level :=
            if ($concept/f:property[f:code/@value='subsumedBy']) then
               max($ancestors/xs:integer(@distance))
            else '0'
      return
      <concept code="{$concept/f:code/@value}" codeSystem="{$codeSystemOid}" displayName="{$concept/f:display/@value}" level="{$level}" type="{$conceptType}">
      {
         if ($codeSystemVersion) then
            attribute codeSystemVersion {$codeSystemVersion}
         else()
         ,
         if ($concept/f:extension[@url='http://hl7.org/fhir/Profile/tools-extensions#definition']) then
            <desc language="{$language}">
               {$concept/f:extension[@url='http://hl7.org/fhir/Profile/tools-extensions#definition']/@value/string()}
            </desc>
         else ()
      }
      </concept>
 
      let $decorValueSet :=
          <valueSet
            id="{$vsId}"
            effectiveDate="{$vsEffectiveDate}"
            name="{$fhirValueSet/f:name/@value}"
            displayName="{$fhirValueSet/f:name/@value}"
            statusCode="{$decorStatus}"
            canonicalUri="{$fhirValueSet/f:url/@value}">
            {
            if ($fhirValueSet/f:version) then
               attribute versionLabel {$fhirValueSet/f:version/@value}
            else()
            ,
            if ($fhirValueSet/f:experimental/@value) then
               attribute experimental {$fhirValueSet/f:experimental/@value}
            else ()
            }
            <desc
               language="{$language}">{$fhirValueSet/f:description/@value/string()}</desc>
            {
            if ($fhirValueSet/f:publisher) then
               <publishingAuthority name="{$fhirValueSet/f:publisher/@value}">
               {
                  for $contact in $fhirValueSet/f:contact
                     for $telecom in $contact/f:telecom
                     return
                     <addrLine type="{$fhirContactSystem2decorAddrLineType($telecom/f:system/@value)}">{$telecom/f:value/@value/string()}</addrLine>
               }   
               </publishingAuthority>
            else()
            ,
            if ($fhirValueSet/f:copyright) then
               <copyright>{$fhirValueSet/f:copyright/@value/string()}</copyright>
            else ()
            }
            <conceptList>{$conceptList}</conceptList>
         
         </valueSet>
    
    
    let $validationDECOR       :=  adfhirvs:validateDecorValueSet($decorValueSet)
    let $check            :=
        if ($validationDECOR[@role = ('error', 'warn', 'warning', 'fatal')]) then (
            let $detailcode         := 'MSG_BAD_SYNTAX'
            let $details            := 'The DECOR ValueSet to save has structural and/or business rule related issues'
            return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details)
        ) else ()
        
    
    let $valueSetStore :=
         if ($decor/terminology/conceptMap) then
            update insert $decorValueSet preceding $decor/terminology/conceptMap[1]
         else
          update insert $decorValueSet into $decor/terminology
    
    return
      <response>
         <id>{$vsId}</id>
      </response>
};
declare %private function adfhirvs:resolveAncestors($valueSet as element(f:ValueSet), $property as element(), $distance as xs:integer) as element()* {
   let $aConcept := $valueSet//f:concept[f:code/@value=$property/f:valueCode/@value]
   for $ancestor in $aConcept/f:property[f:code/@value='subsumedBy']
   return
      (
      <id code="{$ancestor/f:valueCode/@value}" distance="{$distance}"/>
      ,
      adfhirvs:resolveAncestors($valueSet, $ancestor, $distance + 1)
      )
   
};


(: **** VALIDATION **** :)
declare %private function adfhirvs:validateDecorValueSet($resource as element()) as element(issue)* {
    
    (: validate xsd :)
    let $schemaFile := concat($setlib:strDecorCore,'/DECOR.xsd')
    
    let $schemaFile :=
        if (doc-available($schemaFile)) then $schemaFile
        else (error(QName('http://art-decor.org/ns/error', 'SchemaFileMissing'), concat('XML Schema file missing: ',$schemaFile)))
    
    let $schemaReport   := validation:jaxv-report($resource, doc($schemaFile))
    let $xsdIssues      := 
        for $schemaIssue in $schemaReport//*[@level='Warning' or @level='Error']
        return
            <issue type="schema" role="{lower-case($schemaIssue/@level)}" count="{if ($schemaIssue/@repeat) then $schemaIssue/@repeat else ('1')}">
                <description>{$schemaIssue/text()}</description>
                <location line="{$schemaIssue/@line}" column="{$schemaIssue/@column}"/>
            </issue>
            
    (: validate schematron :)
    let $schematronFile := concat($setlib:strDecorCore,'/DECOR.sch_svrl.xsl')
    
    let $schematronFile :=
        if (doc-available($schematronFile)) then $schematronFile
        else (error(QName('http://art-decor.org/ns/error', 'SchematronFileMissing'), concat('XML Schematron file missing: ',$schematronFile)))
        
    let $schResult      := transform:transform($resource, doc($schematronFile), ())
    let $schIssues      :=
        for $schematronIssue  in $schResult//svrl:failed-assert | $schResult//svrl:successful-report
        let $role   := if ($schematronIssue/@role) then $schematronIssue/@role else ('error')
        return
            <issue type="schematron" role="{$role}" count="1">
                {$schematronIssue/(@* except (@type|@role|@id|@location))}
                <description>{$schematronIssue/svrl:text/text()}</description>
                <location path="{$schematronIssue/@location}"/>
            </issue>
    
    return
        ($xsdIssues|$schIssues)
};
