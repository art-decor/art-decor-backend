xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
module namespace adfhirq           = "http://art-decor.org/ns/fhir/4.0/questionnaire";

import module namespace adfhir      = "http://art-decor.org/ns/fhir/4.0" at "api-fhir.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../../art/api/api-server-settings.xqm";
import module namespace getf        = "http://art-decor.org/ns/fhir-settings" at "fhir-settings.xqm";
import module namespace utillib     = "http://art-decor.org/ns/api/util" at "../../../api/modules/library/util-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "../../../api/modules/library/settings-lib.xqm";
import module namespace vs          = "http://art-decor.org/ns/decor/valueset" at "../../../art/api/api-decor-valueset.xqm";
import module namespace markdown    = "http://art-decor.org/ns/api/markdown" at "../../../api/modules/library/markdown-lib.xqm";

declare namespace f                 = "http://hl7.org/fhir";
declare namespace error             = "http://art-decor.org/ns/fhir/error";
declare namespace json              = "http://www.json.org";
declare namespace xhtml             = "http://www.w3.org/1999/xhtml";

declare %private variable $adfhirq:type                 := 'Questionnaire';
declare %private variable $adfhirq:inactiveStatusCodes  := ('cancelled','rejected','deprecated');
declare %private variable $adfhirq:UCUMCODES            := if (doc-available(concat($setlib:strDecorCore, '/DECOR-ucum.xml'))) then doc(concat($setlib:strDecorCore, '/DECOR-ucum.xml'))/* else ();

declare function adfhirq:convertDecorDatasetOrTransaction2FHIRQuestionnaire($id as xs:string, $effectiveDate as xs:string?, $version as xs:string?, $language as xs:string?, $fhirLinkItemStyle as xs:string?) as element(f:Questionnaire)? {
    let $transactionOrDataset   := 
        if (string-length($id) > 0) then
            utillib:getDataset($id, $effectiveDate, $version, $language) | utillib:getTransaction($id, $effectiveDate, $version, $language)
        else ()
    return
        adfhirq:convertDecorDatasetOrTransaction2FHIRQuestionnaire($transactionOrDataset, $version, $language, $fhirLinkItemStyle)
};
declare function adfhirq:convertDecorDatasetOrTransaction2FHIRQuestionnaire($transactionOrDataset as element()?, $version as xs:string?, $language as xs:string?, $fhirLinkItemStyle as xs:string?) as element(f:Questionnaire)? {
    let $language               := if (empty($language) or ($language = '*')) then $transactionOrDataset/ancestor::decor/project/@defaultLanguage else ($language)
    (: fix for de-CH :)
    let $language               := if ($language='de-CH') then 'de-DE' else $language
    
    let $fullDatasetTree        := 
        if ($version castable as xs:dateTime) then (
            let $datasets   := 
                if ($transactionOrDataset[self::dataset]) then ($transactionOrDataset) else (
                    if ($language = '*') then
                        $setlib:colDecorVersion//transactionDatasets[@versionDate = $version]//dataset[@transactionId = $transactionOrDataset/@id][@transactionEffectiveDate = $transactionOrDataset/@effectiveDate]
                    else (
                        $setlib:colDecorVersion//transactionDatasets[@versionDate = $version][@language = $language]//dataset[@transactionId = $transactionOrDataset/@id][@transactionEffectiveDate = $transactionOrDataset/@effectiveDate]
                    )
                )
            
            return $datasets[1]
        ) else if ($transactionOrDataset) then (
            utillib:getFullDatasetTree($transactionOrDataset, (), (), $language, (), false(), ())
        ) else ()
        
    let $result                 := 
        if ($fullDatasetTree) then (
            (:let $xsl            := doc('../resources/stylesheets/tr2quest.xsl')
            return transform:transform($fullDatasetTree, $xsl, ()):)
            let $oidMap           := 
                for $cs in $fullDatasetTree//@codeSystem
                let $csid := $cs
                let $csed := $cs/../@codeSystemVersion
                group by $csid, $csed
                return map:entry($csid || $csed, utillib:getCanonicalUriForOID('CodeSystem', $csid, $csed, $transactionOrDataset/ancestor::decor, $setlib:strKeyCanonicalUriPrefdR4))
            return
            adfhirq:decorTransaction2Questionnaire($fullDatasetTree,
                map { 
                    "fhirLinkItemStyle": $fhirLinkItemStyle, 
                    "fhirCanonicalBase": adserver:getServerURLFhirCanonicalBase(), 
                    "publisher": ($transactionOrDataset/publishingAuthority/@name, utillib:inheritPublishingAuthority($transactionOrDataset)/@name)[1], 
                    "language":  ($language, $transactionOrDataset/ancestor::decor/project/@defaultLanguage)[1],
                    "oids": map:merge($oidMap)
                }
            )
        )
        else ()
    return $result
};

declare function adfhirq:convertDecorQuestionnaire2FHIRQuestionnaire($q as element(questionnaire), $params as map(*)?) as element(f:Questionnaire) {
    let $language               := ($params?language, $q/ancestor::*:decor/*:project/@defaultLanguage, $q/*[@language = 'en-US']/@language, $q/*/@language, 'en-US')[not(. = '*')][1]
    let $projectPrefix          := ($params?projectPrefix, $q/ancestor::decor/project/@prefix)[1]
    let $projectVersion         := ($params?projectVersion, $q/ancestor::decor/@versionDate)[1]
    let $fhirCanonicalBase      := $params?fhirCanonicalBase
    let $publisher              := ($q/*:publishingAuthority, $params?publisher, utillib:inheritPublishingAuthority($q))[1]
    let $copyright              := if ($q/*:copyright) then $q/*:copyright else utillib:inheritCopyright($q)
    let $fhirId                 := concat($q/@id, '--', replace($q/@effectiveDate,'\D',''))
    let $oidMap                 := map:merge(
        for $cs in $q//*[empty(@canonicalUri)][@codeSystem]
        let $csid := $cs/@codeSystem
        let $csed := $cs/@codeSystemVersion
        group by $csid, $csed
        return map:entry($csid || $csed, utillib:getCanonicalUriForOID('CodeSystem', $csid, $csed, $q/ancestor::decor, $setlib:strKeyCanonicalUriPrefdR4))
    )
    return
    <Questionnaire xmlns="http://hl7.org/fhir">
        <id value="{$fhirId}"/>
        <meta>
        {
            if ($q/@lastModifiedDate) then
                <lastUpdated value="{$q/@lastModifiedDate || 'Z'}"/>
            else (),
            if ($projectPrefix) then 
                <source value="{$fhirCanonicalBase || $getf:strFhirVersionShort || '/' || $projectPrefix}"/>
            else ()
        }
            <tag><system value="http://hl7.org/fhir/FHIR-version"/><code value="{$getf:strFhirVersion}"/></tag>
        </meta>
        <language value="{$language}"/>
    {
        (: let ART-DECOR act as a ValueSet expansion server if the Questionnaire has value set association but nowhere to get it :)
        <extension url="http://hl7.org/fhir/uv/sdc/StructureDefinition/sdc-questionnaire-preferredTerminologyServer">
            <valueUrl value="{adserver:getServerURLFhirServices() || $getf:strFhirVersionShort || '/public'}"/>
        </extension>
    }
    {
        adfhir:item2fhirNamespace($q/*:contained[*/*:id/concat('#', @value) = $q/*:item//@ref])
    }
        <url value="{utillib:getCanonicalUri('Questionnaire', $q)}"/>
        <identifier>
            <system value="urn:ietf:rfc:3986"/>
            <value value="urn:oid:{$q/@id}"/>
        </identifier>
    {
        if ($q/@versionLabel) then <version value="{$q/@versionLabel}"/> else (),
        adfhir:decorName2fhirName($q/*:name, $language),
        adfhir:decorName2fhirTitle($q/*:name, $language),
        (: derivedFrom - could come from relationship to another Questionnaire? :)
        adfhirq:decorStatusCode2fhirQuestionnaireStatus($q/@statusCode),
        <experimental value="{$q/@experimental = 'true'}"/>,
        adfhirq:decorSubjectType2fhirSubjectType($q/*:subjectType),
        <date value="{$q/@lastModifiedDate || 'Z'}"/>,
        adfhir:decorPublishingAuthority2fhirPublisher($publisher),
        adfhir:decorPublishingAuthority2fhirContact($publisher),
        adfhir:decorDesc2fhirDescription($q/*:desc, $language),
        (: useContext - not supported :)
        adfhir:decorJurisdiction2fhirJurisdiction($q/*:jurisdiction, $oidMap),
        adfhir:decorPurpose2fhirPurpose($q/*:purpose, $language),
        adfhir:decorCopyright2fhirCopyright($copyright, $language),
        if ($q/@officialReleaseDate) then
            <approvalDate value="{substring-before($q/@officialReleaseDate, 'T')}"/>
        else (),
        (: lastReviewDate - not supported :)
        <effectivePeriod>
            <start value="{$q/@effectiveDate || 'Z'}"/>
        {
            if ($q/@expirationDate) then
                <end value="{$q/@expirationDate || 'Z'}"/>
            else ()
        }
        </effectivePeriod>,
        adfhirq:decorCode2fhirCode($q/*:code, $oidMap),
        adfhirq:decorItem2fhirItem($q/*:item, $language, $oidMap, $params)
    }
    </Questionnaire>
};

declare %private function adfhirq:decorTransaction2Questionnaire($transaction as element(dataset), $params as map(*)?) as element(f:Questionnaire) {
    let $language               := ($params?language, $transaction/*:name[@language = 'en-US']/@language, $transaction/*:name/@language)[1]
    let $projectPrefix          := ($params?projectPrefix, $transaction/ancestor::decor/project/@prefix)[1]
    let $fhirLinkItemStyle      := $params?fhirLinkItemStyle
    let $fhirCanonicalBase      := $params?fhirCanonicalBase
    let $publisher              := $params?publisher
    let $transactionId          := $transaction/@transactionId
    let $fhirId                 := 
        if ($transaction/@transactionId) then 
            concat($transaction/@transactionId,'--',replace($transaction/@transactionEffectiveDate,'\D','')) 
        else (
            concat($transaction/@id,'--',replace($transaction/@effectiveDate,'\D',''))
        )
    let $targets                := if (map:keys($params) = 'targets') then map:get($params, 'targets') else $transaction/*:concept[not(@conformance = 'NP')]/@id
    
    let $check                  :=
        if (empty($targets)) then
            error(xs:QName('adfhirq:NOCONCEPTS'), 'ERROR: no targets for this transaction')
        else ()

    return
    <Questionnaire xmlns="http://hl7.org/fhir">
        <id value="{$fhirId}"/>
        <meta>
        {
            if ($transaction/@lastModifiedDate) then
                <lastUpdated value="{$transaction/@lastModifiedDate || 'Z'}"/>
            else (),
            if ($projectPrefix) then 
                <source value="{$fhirCanonicalBase || $getf:strFhirVersionShort || '/' || $projectPrefix}"/>
            else ()
        }
            <tag><system value="http://hl7.org/fhir/FHIR-version"/><code value="{$getf:strFhirVersion}"/></tag>
        </meta>
        <language value="{$transaction/*:name[1]/@language}"/>
    {
        (: let ART-DECOR act as a ValueSet expansion server if the Questionnaire has value set association but nowhere to get it :)
        <extension url="http://hl7.org/fhir/uv/sdc/StructureDefinition/sdc-questionnaire-preferredTerminologyServer">
            <valueUrl value="{adserver:getServerURLFhirServices() || $getf:strFhirVersionShort || '/public'}"/>
        </extension>
    }
        <text>
            <status value="generated"/>
            <div xmlns="http://www.w3.org/1999/xhtml">Questionnaire for {data($transaction/*:name[1])}</div>
        </text>
        <url value="{utillib:getCanonicalUri('Questionnaire', $transaction)}"/>
        {
            if ($transaction/@versionLabel) then 
                <version value="{$transaction/@versionLabel}"/> 
            else (),
            adfhir:decorName2fhirName($transaction/*:name, $language),
            adfhir:decorName2fhirTitle($transaction/*:name, $language)
        }
        <status value="draft"/>
        <subjectType value="Patient"/>
        <date value="{substring(string(current-date()), 1, 10)}"/>
    {
        if (empty($publisher)) then () else <publisher value="{$publisher}"/>
    }
    {
        for $c in $transaction/*:concept[@id = $targets] return adfhirq:decorConcept2QuestionnaireItem($c, $params)
    }
    </Questionnaire>
};

declare function adfhirq:decorSubjectType2fhirSubjectType($in as element(subjectType)?) as element(f:subjectType)* {
    for $s in $in
    return
        <subjectType xmlns="http://hl7.org/fhir" value="{$s/@code}"/>
};
declare function adfhirq:decorCode2fhirCode($in as element(code)*, $oidMap as map(*)?) as element(f:code)* {
    for $j in $in[@code][@codeSystem | @canonicalUri]
    let $system   := 
        if ($j/@canonicalUri) then 
            $j/@canonicalUri 
        else 
        if ($j/@codeSystem) then
            map:get($oidMap, $j/@codeSystem || $j/@codeSystemVersion)
        else ()
    return
    <code xmlns="http://hl7.org/fhir">
        <system value="{$system}"/>
        <code value="{$j/@code}"/>
    {
        if ($j[@displayName]) then
            <display value="{replace($j/@displayName, '(^\s+)|(\s+$)', '')}"/>
        else ()
    }
    </code>
};
declare function adfhirq:decorItem2fhirItem($in as element(item)*, $language as xs:string, $oidMap as map(*)?, $params as map(*)?) as element(f:item)* {
    let $projectPrefix          := ($params?projectPrefix, $in/ancestor::decor/project/@prefix)[1]
    let $projectVersion         := ($params?projectVersion, $in/ancestor::decor/@versionDate)[string-length() gt 0][1]
    
    for $i in $in
    (: weird shizzle ... if we do it deeper down, it does not resolve the value sets. leaving as-is for now :)
    let $answerValueSets        :=
        for $answerValueSet in $i/*:answerValueSet[@ref | @canonicalUri]
        return 
            if ($answerValueSet/@canonicalUri[not(. = '')]) then
                <answerValueSet value="{$answerValueSet/@canonicalUri}" xmlns="http://hl7.org/fhir"/>
            else (
                let $vsid := $answerValueSet/@ref
                let $vsed := ($answerValueSet/@flexibility, 'dynamic')[1]
                let $vs   := vs:getValueSetById($vsid, $vsed, $projectPrefix, (), false())/descendant-or-self::valueSet[@id]
                return
                    if (empty($vs)) then (
                        error(xs:QName('adfhirq:VALUESETMISSING'), 'Questionnaire "' || $i/ancestor::*:questionnaire/*:name[1] || '" item.linkId (' || $i/@linkId || ') answerValueSet ''' || $vsid || ''' cannot be found.')
                    ) else (
                        <answerValueSet value="{utillib:getCanonicalUri('ValueSet', $vs[1])}" xmlns="http://hl7.org/fhir"/>
                    )
            )
    return
        <item xmlns="http://hl7.org/fhir">
        {
            if ($i/*:designNote) then
                <extension url="http://hl7.org/fhir/StructureDefinition/designNote">
                    <valueMarkdown value="{($i/*:designNote[@language = $language], $i/*:designNote)[1]}"/>
                </extension>
            else ()
            ,
            (:Note that LHCForms has a Fiddle using this extension url but HL7 doesn't know this one ... so not using yet
                        https://jsfiddle.net/lforms/83of00cj/27/
            <extension url="http://hl7.org/fhir/StructureDefinition/terminology-server">:)
            for $t in $i/*:terminologyServer
            return (
                <extension url="http://hl7.org/fhir/uv/sdc/StructureDefinition/sdc-questionnaire-preferredTerminologyServer">
                    <valueUrl value="{$t/@ref}"/>
                </extension>
            )
            ,
            (: let ART-DECOR act as a ValueSet expansion server if the Questionnaire has value set association but nowhere to get it :)
            if (empty($i/*:terminologyServer) and $answerValueSets) then
                <extension url="http://hl7.org/fhir/uv/sdc/StructureDefinition/sdc-questionnaire-preferredTerminologyServer">
                    <valueUrl value="{adserver:getServerURLFhirServices() || $getf:strFhirVersionShort || '/public'}"/>
                </extension>
            else ()
            ,
            for $u in $i/*:unitOption[@code]
            let $system   := 
                if ($u/@canonicalUri) then 
                    $u/@canonicalUri 
                else 
                if ($u/@codeSystem) then
                    map:get($oidMap, $u/@codeSystem || $u/@codeSystemVersion)
                else ()
            return
                <extension url="http://hl7.org/fhir/StructureDefinition/questionnaire-unitOption">
                    <valueCoding>
                        <system value="{$system}"/>
                        <code value="{$u/@code}"/>
                    {
                        if ($u[@displayName]) then
                            <display value="{replace($u/@displayName, '(^\s+)|(\s+$)', '')}"/>
                        else ()
                    }
                    </valueCoding>
                </extension>
            ,
            for $u in $i/*:itemControl
            let $system   := 
                if ($u/@canonicalUri) then 
                    $u/@canonicalUri 
                else 
                if ($u/@codeSystem) then
                    map:get($oidMap, $u/@codeSystem || $u/@codeSystemVersion)
                else ()
            return
                <extension url="http://hl7.org/fhir/StructureDefinition/questionnaire-itemControl">
                    <valueCodeableConcept>
                    {
                        if ($u[@code]) then
                            <coding>
                                <system value="{$system}"/>
                                <code value="{$u/@code}"/>
                            {
                                if ($u[@displayName]) then
                                    <display value="{replace($u/@displayName, '(^\s+)|(\s+$)', '')}"/>
                                else ()
                            }
                            </coding>
                        else (
                            <text value="{replace($u/@displayName, '(^\s+)|(\s+$)', '')}"/>
                        )
                    }
                    </valueCodeableConcept>
                </extension>
            ,
            if (empty($i/@minLength)) then () else (
                <extension url="http://hl7.org/fhir/StructureDefinition/minLength">
                    <valueInteger value="{$i/@minLength}"/>
                </extension>
            )
            ,
            if ($i/@minValue) then
                <extension url="http://hl7.org/fhir/StructureDefinition/minValue">
                {
                    typeswitch ($i/@minValue)
                    case xs:date return <valueDate value="{$i/@minValue}"/>
                    case xs:dateTime return <valueDateTime value="{$i/@minValue}"/>
                    case xs:time return <valueTime value="{$i/@minValue}"/>
                    default return
                        if (matches($i/@minValue, '\.')) then
                            <valueDecimal value="{$i/@minValue}"/>
                        else (
                            <valueInteger value="{$i/@minValue}"/>
                        )
                }   
                </extension>
            else ()
            ,
            if ($i/@maxValue) then
                <extension url="http://hl7.org/fhir/StructureDefinition/maxValue">
                {
                    typeswitch ($i/@maxValue)
                    case xs:date return <valueDate value="{$i/@maxValue}"/>
                    case xs:dateTime return <valueDateTime value="{$i/@maxValue}"/>
                    case xs:time return <valueTime value="{$i/@maxValue}"/>
                    default return
                        if (matches($i/@maxValue, '\.')) then
                            <valueDecimal value="{$i/@maxValue}"/>
                        else (
                            <valueInteger value="{$i/@maxValue}"/>
                        )
                }   
                </extension>
            else ()
            ,
            if (empty($i/@maxDecimalPlaces)) then () else (
                <extension url="http://hl7.org/fhir/StructureDefinition/maxDecimalPlaces">
                    <valueInteger value="{$i/@maxDecimalPlaces}"/>
                </extension>
            )
            ,
            for $iec in $i/*:itemExtractionContext
            return
                <extension url="http://hl7.org/fhir/uv/sdc/StructureDefinition/sdc-questionnaire-itemExtractionContext">
                {
                    if ($iec/@code) then
                        <valueCode value="{$iec/@code}"/>
                    else
                    if ($iec/*:expression) then
                        <valueExpression>
                        {
                            for $att in $iec/*:expression/(@description, @name, @language, @expression, @reference)
                            return
                                element {name($att)} {attribute value {$att}}
                        }
                        </valueExpression>
                    else ()
                }   
                </extension>
            ,
            if (empty($i/@hiddenItem)) then () else (
                <extension url="http://hl7.org/fhir/StructureDefinition/questionnaire-hidden">
                    <valueBoolean value="{$i/@hiddenItem}"/>
                </extension>
            )
        }
        {
            if ($i/@linkId) then <linkId value="{$i/@linkId}"/> else (),
            if ($i/@definition) then <definition value="{$i/@definition}"/> else ()
            ,
            for $u in $i/*:code
            let $system   := 
                if ($u/@canonicalUri) then 
                    $u/@canonicalUri 
                else 
                if ($u/@codeSystem) then
                    map:get($oidMap, $u/@codeSystem || $u/@codeSystemVersion)
                else ()
            return
                <code>
                    <system value="{$system}"/>
                    <code value="{$u/@code}"/>
                {
                    if ($u[@displayName]) then
                        <display value="{replace($u/@displayName, '(^\s+)|(\s+$)', '')}"/>
                    else ()
                }
                </code>
            ,
            if ($i/@prefix) then <prefix value="{$i/@prefix}"/> else ()
            ,
            let $default    := ($i/*:text[@language = $language], $i/*:text)[1]
            return
                if ($default) then 
                    <text xmlns="http://hl7.org/fhir" value="{markdown:unHTML($default)}">
                    {
                        (: handle different languages :)
                        for $n in $i/*:text[not(@language = $default/@language)]
                        return
                            <extension url="http://hl7.org/fhir/StructureDefinition/translation">
                                <extension url="lang">
                                    <valueCode value="{$n/@language}"/>
                                </extension>
                                <extension url="content">
                                    <valueString value="{$n}"/>
                                    <!-- valueMarkdown ?! -->
                                </extension>
                            </extension>
                    }
                    {
                        (: handle xhtml - if present - as markdown rep :)
                        if ($default[*]) then
                            <extension url="http://hl7.org/fhir/StructureDefinition/rendering-markdown">
                                <valueMarkdown value="{markdown:html2markdown(utillib:parseNode($default)/node())}"/>
                            </extension>
                        else ()
                    }
                    {
                        (: handle how the specified element should be rendered when displayed. :)
                        if (empty($i/@renderingStyle)) then () else (
                            <extension url="http://hl7.org/fhir/StructureDefinition/rendering-style">
                                <valueString value="{$i/@renderingStyle}"/>
                            </extension>
                        )
                    }
                    </text>
                else ()
            ,
            if ($i/@type) then <type value="{$i/@type}"/> else (),
            adfhirq:decorItemEnableWhen2fhirItemEnableWhen($i/*:enableWhen, $oidMap),
            if ($i/@enableBehavior) then <enableBehavior value="{$i/@enableBehavior}"/> else (),
            (:questionnaire/item of type display SHALL NOT have @repeats or @required (FHIR invariant que-6):)
            (:questionnaire/item of type display SHALL NOT have @readOnly (FHIR invariant que-9).:)
            for $att in $i/(@required[not($i/@type = 'display')], @repeats[not($i/@type = 'display')], @readOnly[not($i/@type = 'display')], @maxLength)
            return
                element {name($att)} {attribute value {$att}}
        }
        {
            $answerValueSets
        }
        {
            for $ao in $i/*:answerOption
            let $option             := ($ao/(*:valueInteger|*:valueDate|*:valueTime|*:valueString|*:valueCoding))[1]
            let $system             := 
                if ($option/@canonicalUri) then 
                    $option/@canonicalUri 
                else 
                if ($option/@codeSystem) then
                    map:get($oidMap, $option/@codeSystem || $option/@codeSystemVersion)
                else ()
            let $ordinal            := $ao/@ordinal
            let $initialSelected    := $ao/@initialSelected = 'true'
            let $valueString        := ($ao/*:valueString[@language = $language], $ao/*:valueString)[1]
            return
                <answerOption>
                {
                    if (empty($ordinal)) then () else (
                        <extension url="http://hl7.org/fhir/StructureDefinition/ordinalValue">
                            <valueDecimal value="{$ordinal}"/>
                        </extension>
                    ),
                    if ($option[local-name() = ('valueInteger', 'valueDate', 'valueTime')]) then
                        element {name($option)} {attribute value {$option/@value}}
                    else if ($option[local-name() = 'valueString']) then
                        element { 'valueString' } {attribute value {$valueString/@value}}                        
                    else (
                        <valueCoding>
                            <system value="{$system}"/>
                            <code value="{$option/@code}"/>
                        {
                            if ($option[@displayName]) then
                                <display value="{replace($option/@displayName, '(^\s+)|(\s+$)', '')}"/>
                            else ()
                        }
                        </valueCoding>
                    ),
                    if ($initialSelected) then <initialSelected value="{$initialSelected}"/> else ()
                }
                </answerOption>
        }
        {
            for $initial in $i/*:initial/*
            return
                switch (local-name($initial))
                case 'valueBoolean' 
                case 'valueDecimal' 
                case 'valueInteger' 
                case 'valueDate'
                case 'valueDateTime' 
                case 'valueTime'
                case 'valueString' 
                case 'valueUri' return <initial>{$initial}</initial>
                case 'valueCoding' return
                    <initial>
                      <valueCoding>
                      {
                        if ($initial/@codeSystem) then <system value="{map:get($oidMap, $initial/@codeSystem || $initial/@codeSystemVersion)}"/> else (),
                        if ($initial/@codeSystemVersion) then <version value="{$initial/@codeSystemVersion}"/> else (),
                        if ($initial/@code) then <code value="{$initial/@code}"/> else (),
                        if ($initial/@displayName) then <display value="{$initial/@displayName}"/> else ()
                      }
                      </valueCoding>
                    </initial>
                case 'valueQuantity' return
                    <initial>
                      <valueQuantity>
                      {
                        if ($initial/@value) then <value>{$initial/@value}</value> else (),
                        if ($initial/@comparator) then <comparator value="{$initial/@comparator}"/> else (),
                        if ($initial/@displayName) then <unit value="{$initial/@displayName}"/> else (),
                        if ($initial/@codeSystem) then <system value="{map:get($oidMap, $initial/@codeSystem || $initial/@codeSystemVersion)}"/> else (),
                        if ($initial/@code) then <code value="{$initial/@code}"/> else ()
                      }
                      </valueQuantity>
                    </initial>
                (: valueAttachment and valueReference not supported (yet) in DECOR.xsd:)
                default return ()
        }
        {
            let $helptext    := ($i/*:operationalization[@language = $language], $i/*:operationalization)[1]
            return
                if ($helptext) then
                    <item>
                        <extension url="http://hl7.org/fhir/StructureDefinition/questionnaire-itemControl">
                            <valueCodeableConcept>
                                <coding>
                                    <system value="http://hl7.org/fhir/questionnaire-item-control"/>
                                    <code value="help"/>
                                    <display value="Help-Button"/>
                                </coding>
                                <text value="Help-Button"/>
                            </valueCodeableConcept>
                        </extension>
                        <linkId value="{$i/@linkId}-help"/>
                        <text value="{markdown:unHTML($helptext)}">
                        {
                            if ($helptext[*]) then
                                <extension url="http://hl7.org/fhir/StructureDefinition/rendering-markdown">
                                    <valueMarkdown value="{markdown:html2markdown(utillib:parseNode($helptext)/node())}"/>
                                </extension>
                            else ()
                        }
                        </text>
                        <type value="display"/>
                    </item>
                else ()
        }
        {
            adfhirq:decorItem2fhirItem($i/*:item, $language, $oidMap, $params)
        }
        </item>
};
declare function adfhirq:decorItemEnableWhen2fhirItemEnableWhen($items as element(enableWhen)*, $oidMap as map(*)?) as element(enableWhen)* {
    for $ao in $items
    let $option             := ($ao/(*:answerBoolean | *:answerDecimal | *:answerInteger | *:answerDate | *:answerDateTime | *:answerTime | *:answerString | *:answerCoding | *:answerQuantity))[1]
    let $system             := 
        if ($option/@canonicalUri) then 
            $option/@canonicalUri 
        else 
        if ($option/@codeSystem) then
            map:get($oidMap, $option/@codeSystem || $option/@codeSystemVersion)
        else ()
    return
        <enableWhen xmlns="http://hl7.org/fhir">
        {
            $ao/@id
        }
        {
            if ($ao/@question) then 
                if ($ao/ancestor::*:item) then 
                    <question value="{$ao/@question}"/>
                else
                if (utillib:isOid(substring-before($ao/@question, '--'))) then
                    <question value="{substring-before($ao/@question, '--')}"/>
                else (
                    (: we just hope it leads us to something :)
                    <question value="{$ao/@question}"/>
                )
            else ()
        }
        {
            if ($ao/@operator) then <operator value="{$ao/@operator}"/> else ()
        }
        {
            if ($option[local-name() = 'answerCoding']) then (
                <answerCoding>
                    <system value="{$system}"/>
                    <code value="{$option/@code}"/>
                {
                    if ($option[@displayName]) then
                        <display value="{replace($option/@displayName, '(^\s+)|(\s+$)', '')}"/>
                    else ()
                }
                </answerCoding>
            )
            else
            if ($option[local-name() = 'answerQuantity']) then (
                <answerQuantity>
                {
                    if ($option/@value) then <value value="{$option/@value}"/> else (),
                    if ($option/@comparator) then <comparator value="{$option/@comparator}"/> else (),
                    if ($option/@unit) then <unit value="{$option/@unit}"/> else (),
                    (: assume UCUM if not provided otherwise :)
                    if ($option/@code) then <system value="{($system, 'http://unitsofmeasure.org')[1]}"/> else (),
                    if ($option/@code) then <code value="{$option/@code}"/> else ()
                }
                </answerQuantity>
            )
            else (
                element {local-name($option)} {attribute value {$option/@value}}
            )
        }
        </enableWhen>
};

declare %private function adfhirq:decorConcept2QuestionnaireItem($concept as element(concept), $params as map(*)?) as element(f:item)? {
    let $language               := map:get($params, 'language')
    (: we cannot support this properly because enableWhen.question depends on this. If we want to support this here then we should also update enableWhen.quertion logic to match whatever we do here  :)
    let $fhirLinkItemStyle      := 'oid' (:map:get($params, 'fhirLinkItemStyle'):)
    let $fhirCanonicalBase      := map:get($params, 'fhirCanonicalBase')
    let $publisher              := map:get($params, 'publisher')
    let $oidMap                 := map:get($params, 'oids')
    let $targetId               := 
        if ($fhirLinkItemStyle='idDisplay') 
        then $concept/@iddisplay 
        else if ($fhirLinkItemStyle='oid')
        then $concept/@id 
        else $concept/concat(@id, '--', replace(@effectiveDate, '\D', ''))
    let $conceptId              := $concept/@id
    
    let $conceptDescription     := $concept/*:desc[.//text()[not(normalize-space() = '')]]
    let $conceptDescriptionText := replace(replace(string-join($conceptDescription[1], '\n'), '&#13;?&#10;\s+' , '&#10;'), '^\s+|\s+$', '')
    let $conceptDescriptionHtml := if ($conceptDescription) then replace(replace(utillib:serializeNode($conceptDescription[1]), '&#13;?&#10;\s+' , '&#10;'), '^\s+|\s+$', '') else ()
    let $definitionUri          := 
        $fhirCanonicalBase || 'StructureDefinition/' || $concept/ancestor::*:dataset/@transactionId || '--' || replace($concept/ancestor::*:dataset/@transactionEffectiveDate, '\D', '') || 
                                                 '#' || $concept/@id                                || '--' || replace($concept/@effectiveDate, '\D', '')

    let $itemText               := ($concept/*:operationalization, $concept/*:name)[1]
    (:let $itemText               :=
        if ($itemText[count(*) = 1][*:div][normalize-space(string-join(text(), '')) = '']) then
            markdown:html2markdown($itemText/*:div/node())
        else (
            markdown:html2markdown($itemText/node())
        ):)
    (:let $itemText               :=
        if ($itemText[count(*) = 1][*:div][normalize-space(string-join(text(), '')) = '']) then
            utillib:serializeNode($itemText/*:div)
        else (
            utillib:serializeNode($itemText)
        ):)
    let $itemText               :=
        if ($itemText[count(*) = 1][*:div][normalize-space(string-join(text(), '')) = '']) then
            $itemText/*:div
        else (
            $itemText/node()
        )
    
    let $defaultValue           := ($concept/*:valueDomain/*:property/@default)[1]
    (: In addition, the following extensions MUST be supported: minValue, maxValue, minLength, maxDecimalPlaces, unit :)
    let $minInclude             := min($concept/*:valueDomain/*:property[@minInclude castable as xs:decimal]/xs:decimal(@minInclude))
    let $maxInclude             := max($concept/*:valueDomain/*:property[@maxInclude castable as xs:decimal]/xs:decimal(@maxInclude))
    let $minLength              := min($concept/*:valueDomain/*:property[@minLength castable as xs:integer]/xs:integer(@minLength))
    let $maxDecimalPlaces       := 
        max(
            for $t in $concept/*:valueDomain/*:property/@fractionDigits
            let $md   := replace($t, '[^\d]', '')[not(. = '')]
            return
                if ($md castable as xs:integer) then xs:integer($md) else ()
        )
    return
    switch ($concept/@type)
    case 'group' return (
        <item xmlns="http://hl7.org/fhir">
            <linkId value="{$targetId}"/>
        {
            if ($conceptDescription) then
                (: its not a DEFINITION....
                  <definition value="{markdown:html2markdown(utillib:parseNode($conceptDescription[1])/node())}"/>
                  <definition value="{$definitionUri}"/>
                :)
                ()
            else ()
        }
            <text value="{replace(string-join($itemText, ''), '^\s+|\s+$', '')}">
            {
                (: handle how the specified element should be rendered when displayed :)
                if (empty($concept/@renderingStyle)) then () else (
                    <extension url="http://hl7.org/fhir/StructureDefinition/rendering-style">
                        <valueString value="{$concept/@renderingStyle}"/>
                    </extension>
                )
            }
            </text>
        {
            for $assoc in $concept/*:terminologyAssociation[@conceptId = $conceptId]
            return
                <code>
                    <system value="{map:get($oidMap, $assoc/@codeSystem || $assoc/@codeSystemVersion)}"/>
                    <code value="{$assoc/@code}"/>
                {
                    if (empty($assoc/@displayName)) then () else ( 
                        <display value="{$assoc/@displayName}"/>
                    )
                }
                </code>
        }
            <type value="group"/>
        {
            adfhirq:decorItemEnableWhen2fhirItemEnableWhen($concept/*:enableWhen, $oidMap),
            if ($concept/@enableBehavior) then <enableBehavior value="{$concept/@enableBehavior}"/> else ()
        }
            <required value="{if ($concept[@conformance = 'NP'] | $concept[@maximumMultiplicity = '0']) then false() else exists($concept/@minimumMultiplicity[not(. = '0')])}"/>
            <repeats value="{if ($concept[@conformance = 'NP'] | $concept[@maximumMultiplicity = ('0', '1')]) then false() else true()}"/>
        {
            if ($conceptDescription) then
                (: Note: datatype string does not officially allow xhtml and will trigger a datatype error, however ... it is what the core extension is defined with
                https://chat.fhir.org/#narrow/stream/179166-implementers/topic/Questionnaire.20extension.20rendering-xhtml.20with.20datatype.20string :)
                <item>
                    <extension url="http://hl7.org/fhir/StructureDefinition/questionnaire-itemControl">
                        <valueCodeableConcept>
                            <coding>
                                <system value="http://hl7.org/fhir/questionnaire-item-control"/>
                                <code value="help"/>
                                <display value="Help-Button"/>
                            </coding>
                            <text value="Help-Button"/>
                        </valueCodeableConcept>
                    </extension>
                    <linkId value="{$targetId}-help"/>
                    <text value="{$conceptDescriptionText}">
                        <extension url="http://hl7.org/fhir/StructureDefinition/rendering-xhtml">
                            <valueString value="{$conceptDescriptionHtml}"/>
                            <!-- valueMarkdown ?! -->
                        </extension>
                    </text>
                    <type value="display"/>
                </item>
            else ()
        }
        {
            for $c in $concept/*:concept[not(@conformance = 'NP')] return adfhirq:decorConcept2QuestionnaireItem($c, $params)
        }
        </item>
    )
    case 'item' return (
        <item xmlns="http://hl7.org/fhir">
        {
            if (empty($minInclude)) then () else (
                <extension url="http://hl7.org/fhir/StructureDefinition/minValue">
                {
                    if ($minInclude castable as xs:integer) then
                        <valueInteger value="{xs:integer($minInclude)}"/>
                    else (
                        <valueDecimal value="{$minInclude}"/>
                    )
                }
                </extension>
            )
        }
        {
            if (empty($maxInclude)) then () else (
                <extension url="http://hl7.org/fhir/StructureDefinition/maxValue">
                {
                    if ($maxInclude castable as xs:integer) then
                        <valueInteger value="{xs:integer($maxInclude)}"/>
                    else (
                        <valueDecimal value="{$maxInclude}"/>
                    )
                }
                </extension>
            )
        }
        {
            if (empty($minLength)) then () else (
                <extension url="http://hl7.org/fhir/StructureDefinition/minLength">
                    <valueInteger value="{xs:integer($minLength)}"/>
                </extension>
            )
        }
        {
            if ($concept/*:valueDomain/@type = 'quantity') then
                for $unit in $concept/*:valueDomain/*:property/@unit
                let $unit     := normalize-space($unit)
                let $ucumUnit := $adfhirq:UCUMCODES/*:ucum[@unit = $unit]
                return
                    <extension url="http://hl7.org/fhir/StructureDefinition/questionnaire-unitOption">
                        <valueCoding>
                        {
                            if (empty($ucumUnit)) then () else (
                                <system value="http://unitsofmeasure.org"/>
                            )
                        }
                            <code value="{$unit}"/>
                            <display value="{($ucumUnit/@displayName[not(. = '')], $unit)[1]}"/>
                        </valueCoding>
                    </extension>
            else ()
        }
        {
            if (empty($maxDecimalPlaces)) then () else (
                <extension url="http://hl7.org/fhir/StructureDefinition/maxDecimalPlaces">
                    <valueInteger value="{$maxDecimalPlaces}"/>
                </extension>
            )
        }
            <linkId value="{$targetId}"/>
        {
            if ($conceptDescription) then
                (:<definition value="{markdown:html2markdown(utillib:parseNode($conceptDescription[1])/node())}"/>:)
                (:<definition value="{$definitionUri}"/>:)
                ()
            else ()
        }
        {
            for $assoc in $concept/*:terminologyAssociation[@conceptId = $conceptId]
            return
                <code>
                    <system value="{map:get($oidMap, $assoc/@codeSystem || $assoc/@codeSystemVersion)}"/>
                    <code value="{$assoc/@code}"/>
                {
                    if (empty($assoc/@displayName)) then () else ( 
                        <display value="{$assoc/@displayName}"/>
                    )
                }
                </code>
        }
            <text value="{replace(string-join($itemText, ''), '^\s+|\s+$', '')}">
            {
                if ($itemText[*]) then
                    <extension url="http://hl7.org/fhir/StructureDefinition/rendering-markdown">
                        <valueMarkdown value="{serialize($itemText)}"/>
                    </extension>
                else ()
            }
            {
                (:Identifies how the specified element should be rendered when displayed.:)
                if (empty($concept/@renderingStyle)) then () else (
                    <extension url="http://hl7.org/fhir/StructureDefinition/rendering-style">
                        <valueString value="{$concept/@renderingStyle}"/>
                    </extension>
                )
            }
            </text>
        {
            let $itemType     := adfhirq:decor2questionnaireType($concept)
            
            return (
                <type value="{$itemType}"/>,
                if ($itemType = 'display') then () else (
                    <required value="{if ($concept[@conformance = 'NP'] | $concept[@maximumMultiplicity = '0']) then false() else exists($concept/@minimumMultiplicity[not(. = '0')])}"/>,
                    <repeats value="{if ($concept[@conformance = 'NP'] | $concept[@maximumMultiplicity = ('0', '1')]) then false() else true()}"/>
                )
            )
        }
        {
            adfhirq:decorItemEnableWhen2fhirItemEnableWhen($concept/*:enableWhen, $oidMap),
            if ($concept/@enableBehavior) then <enableBehavior value="{$concept/@enableBehavior}"/> else ()
        }
        {
            if ($concept/*:valueDomain/*:property/@fixed='true') then 
                <readOnly value="true"/>
            else ()
        }
        {
            if ($concept/*:valueDomain/*:property[@maxLength castable as xs:integer]) then
                <maxLength value="{max($concept/*:valueDomain/*:property[@maxLength castable as xs:integer]/xs:integer(@maxLength))}"/>
            else ()
        }
        {
            for $vs in $concept/*:valueSet[*:completeCodeSystem | *:conceptList/*:include | *:conceptList/*:exclude]
            return
                <answerValueSet value="{
                  if ($vs[@canonicalUri]) then $vs/@canonicalUri else (
                      concat(adserver:getServerURLFhirCanonicalBase(),'ValueSet/',$vs/@id, '--', replace($vs/@effectiveDate,'[^\d]',''))
                  )
                }"/>
        }
        {
            let $codedConcepts  :=
                if ($concept/*:valueDomain/*:conceptList/*:concept) then (
                    for $c in $concept/*:valueDomain/*:conceptList/*:concept
                    let $associations := $concept/*:terminologyAssociation[@conceptId = $c/@id]
                    return
                        for $ta in $associations order by $ta/@codeSystem return $ta
                )
                else ()
            let $codedConcepts   :=
                if ($codedConcepts) then $codedConcepts else (
                    $concept/*:valueSet[empty(*:completeCodeSystem | *:conceptList/*:include | *:conceptList/*:exclude)]/*:conceptList/(*:concept | *:exception)
                )
            for $c in $codedConcepts
            return
                <answerOption>
                {
                    (: Ordinal Value: A numeric value that allows the comparison (less than, greater than) or other numerical manipulation of a concept 
                        (e.g. Adding up components of a score). Scores are usually a whole number, but occasionally decimals are encountered in scores. :)
                    if ($c/@ordinal) then 
                        <extension url="http://hl7.org/fhir/StructureDefinition/ordinalValue">
                            <valueDecimal value="{$c/@ordinal}"/>
                        </extension>
                    else ()
                }
                    <valueCoding>
                        <system value="{map:get($oidMap, $c/@codeSystem || $c/@codeSystemVersion)}"/>
                        <code value="{$c/@code}"/>
                        <display value="{$c/@displayName}"/>
                    </valueCoding>
                {
                    if ($c/@code = $defaultValue) then
                        <initialSelected value="true"/>
                    else ()
                }
                </answerOption>
        }
        {
            if ($defaultValue) then 
                (: For code, Questionnaire expect Coding, DECOR default is just a simple string :)
                if ($concept/*:valueDomain/@type != 'code') then
                    <initial>
                    {
                        element {adfhirq:decor2questionnaireAnswerType($concept)} {
                            attribute value {$defaultValue}
                        }
                    }
                    </initial>
                else ()
            else ()
        }
        {
            (: Note: datatype string does not officially allow xhtml and will trigger a datatype error, however ... it is what the core extension is defined with
                https://chat.fhir.org/#narrow/stream/179166-implementers/topic/Questionnaire.20extension.20rendering-xhtml.20with.20datatype.20string :)
            if ($conceptDescription) then
                <item>
                    <extension url="http://hl7.org/fhir/StructureDefinition/questionnaire-itemControl">
                        <valueCodeableConcept>
                            <coding>
                                <system value="http://hl7.org/fhir/questionnaire-item-control"/>
                                <code value="help"/>
                                <display value="Help-Button"/>
                            </coding>
                            <text value="Help-Button"/>
                        </valueCodeableConcept>
                    </extension>
                    <linkId value="{$targetId}-help"/>
                    <text value="{$conceptDescriptionText}">
                        <extension url="http://hl7.org/fhir/StructureDefinition/rendering-xhtml">
                            <valueString value="{$conceptDescriptionHtml}"/>
                        </extension>
                    </text>
                    <type value="display"/>
                </item>
            else ()
        }
        </item>
    )
    default return (
        error(xs:QName('adfhirq:UNKNOWNCONCEPTTYPE'), concat('ERROR: Unknown type of concept "', $concept/@type, '" expected item or group'))
    )
};

declare %private function adfhirq:decor2questionnaireType($concept as element(concept)) as xs:string {
    let $valueDomainType    := $concept/valueDomain/@type
    let $conceptItemType    := $concept/property[@name = 'adfhirq::ItemType']
    let $conceptItem        := $conceptItemType/normalize-space(string-join(.//text()))
    return
    (: allow override of all other options by explicitly setting the questionnaire item type value.
        https://hl7.org/fhir/R4/valueset-item-type.html
    :)
    if ($conceptItemType) then 
        switch (lower-case($conceptItem))
        case 'group'
        case 'display'
        case 'question'
        case 'boolean'
        case 'decimal'
        case 'integer'
        case 'date'
        case 'time'
        case 'string'
        case 'text'
        case 'url'
        case 'choice'
        case 'open-choice'
        case 'attachment'
        case 'reference'
        case 'quantity' return $conceptItem
        case 'datetime' return 'dateTime'
        default return error(xs:QName('adfhirq:IllegalItemType'), concat('Concept id=', $concept/@id, ' effectiveDate=', $concept/@effectiveDate, ' ', $concept/name[1], ' defines unknown property adfhirq::ItemType value: ''', $conceptItem, ''''))
    else
    (: Input param is a concept, since we need to look at group :)
    if ($concept/@type = 'group') then 'group' else
    if ($valueDomainType = 'boolean') then $valueDomainType else
    if ($valueDomainType = 'date') then $valueDomainType else
    if ($valueDomainType = 'decimal') then $valueDomainType else
    if ($valueDomainType = 'quantity') then $valueDomainType else
    if ($valueDomainType = 'text') then $valueDomainType else
    if ($valueDomainType = 'time') then $valueDomainType else
    if ($valueDomainType = 'count') then 'integer' else
    if ($valueDomainType = 'datetime') then 'dateTime' else
    if ($valueDomainType = 'duration') then 'quantity' else
    if ($valueDomainType = 'currency') then 'quantity' else
    if ($valueDomainType = 'identifier') then 'string' else
    if ($valueDomainType = 'ordinal') then 'choice' else
    if ($valueDomainType = 'blob') then 'attachment' else
    if ($valueDomainType = 'code') then (
        if ($concept/*:valueSet) then
            if ($concept/*:terminologyAssociation[@strength[not(. = 'required')]]) then
                'open-choice'
            else
            if ($concept/*:valueSet[*:completeCodeSystem | *:conceptList/*:include | *:conceptList/*:exclude]) then
                'open-choice'
            else
            if ($concept/*:valueSet/*:conceptList/*[@code = 'OTH'][@codeSystem = '2.16.840.1.113883.65.1008']) then
                'open-choice'
            else (
                'choice'
            )
        else (
            'string'
        )
    )
    else (
        'string'
    )
};
declare %private function adfhirq:decor2questionnaireAnswerType($concept as element(concept)) as xs:string {
    (: Input param is a concept, since we need to look at group :)
    switch ($concept/valueDomain/@type)
    case 'count' return 'valueInteger'
    case 'date' return 'valueDate'
    case 'time'  return 'valueTime'
    case 'code' return 'valueCoding'
    case 'blob' return 'valueReference'
    default return 'valueString'
};
declare %private function adfhirq:decorStatusCode2fhirQuestionnaireStatus($status as xs:string) as element(f:status) {
    switch ($status)
    case 'new'
    case 'draft'
    case 'pending' return <status xmlns="http://hl7.org/fhir" value="draft"/>
    case 'final' return <status xmlns="http://hl7.org/fhir" value="active"/>
    case 'rejected'
    case 'cancelled'
    case 'deprecated' return <status xmlns="http://hl7.org/fhir" value="retired"/>
    default return <status xmlns="http://hl7.org/fhir" value="draft"/>
};
declare function adfhirq:fhirQuestionnaireStatusToDecorStatus($status as xs:string) as xs:string {
    switch ($status)
    case 'active' return 'final'
    case 'retired' return 'deprecated'
    default return 'draft'
};