xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
module namespace adfix          = "http://art-decor.org/ns/fhir-permissions";
import module namespace adfhir  = "http://art-decor.org/ns/fhir/4.0" at "api-fhir.xqm";
import module namespace getf    = "http://art-decor.org/ns/fhir-settings" at "fhir-settings.xqm";
declare namespace sm            = "http://exist-db.org/xquery/securitymanager";

declare namespace   f = "http://hl7.org/fhir";

(:  Mode            Octal
 :  rw-r--r--   ==  0644
 :  rw-rw-r--   ==  0664
 :  rwxr-xr--   ==  0754
 :  rwxr-xr-x   ==  0755
 :  rwxrwxr-x   ==  0775
 :  rwxrwxrwx   ==  0777
 :)

declare function adfix:setPermissions() {
    let $u  := adfix:checkIfUserDba()
    let $u  := adfix:setQueryPermissions()
    let $u  := adfix:setFhirDataPermissions()
    let $u  := adfix:refreshCapabilityStatement(())
    let $u  := adfix:cleanupData()
    return ()
};

declare function adfix:refreshCapabilityStatement($source as xs:string?) as xs:boolean {
    let $u                      := adfix:checkIfUserDba()
    let $source                 := if (empty($source)) then concat(repo:get-root(), '/fhir/', $getf:strFhirVersionShort, '/install-data/fhir-data') else $source
    let $capabilitystatement    := 
        if (xmldb:collection-available($source)) then collection($source)//f:CapabilityStatement else
        if (doc-available($source)) then doc($source)//f:CapabilityStatement else ()
    
    let $save                   := 
        if ($capabilitystatement) then (
            let $doc    := util:eval(fn:serialize($capabilitystatement,
                    <output:serialization-parameters xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization">
                        <output:method>xml</output:method>
                    </output:serialization-parameters>
                ))
            
            let $update := adfhir:saveResource($doc, $getf:strCapabilityStatementId, (), $getf:CT_FHIR_XML)
            
            return true()
        )
        else false()
    
    return $save
};

(:
:   Call to fix any potential permissions problems in the paths:
:       /db/apps/fhir-data/*
:   Dependency: $getf:strFhirData, $getf:strFhirAudit, $getf:strFhirHistory, $getf:strFhirResources
:)
declare function adfix:setFhirDataPermissions() {
    adfix:checkIfUserDba(),
    sm:chmod(xs:anyURI(string-join(tokenize($getf:strFhirData, '/')[position() != last()], '/')),'rwxrwsr-x'),
    sm:chown(xs:anyURI($getf:strFhirData),'admin:decor'),
    sm:chmod(xs:anyURI($getf:strFhirData),'rwxrwsr-x'),
    sm:clear-acl(xs:anyURI($getf:strFhirData)),
    
    adfix:setPermissions($getf:strFhirAudit, 'admin', 'decor', 'rwxrwsr-x', (), 'decor', 'rw-rw-r--'),
    adfix:setPermissions($getf:strFhirMetadata, 'admin', 'decor', 'rwxrwsr-x', (), 'decor', 'rw-rw-r--'),
    adfix:setPermissions($getf:strFhirSnapshot, 'admin', 'decor', 'rwxrwsr-x', (), 'decor', 'rw-rw-r--'),
    adfix:setPermissions($getf:strFhirHistory, 'admin', 'decor', 'rwxrwsr-x', (), 'decor', 'rw-rw-r--'),
    adfix:setPermissions($getf:strFhirResources, 'admin', 'decor', 'rwxrwsr-x', (), 'decor', 'rw-rw-r--')
};

(:~
:   Call to fix any potential permissions problems in the path /db/apps/fhir/modules
:   Dependency: $getf:strFhir
:)
declare %private function adfix:setQueryPermissions() {
    for $query in xmldb:get-child-resources(xs:anyURI(concat($getf:strFhir,'/modules')))
    let $perm    :=
        switch($query)
        case 'save-ValueSet-resource.xquery'
        case 'save-QuestionnaireResponse-resource.xquery' 
        case 'delete-QuestionnaireResponse-resource.xquery' 
        return (
            sm:chown(xs:anyURI(concat($getf:strFhir,'/modules/',$query)), 'api-user:decor'),
            sm:chmod(xs:anyURI(concat($getf:strFhir,'/modules/',$query)), 'rwsr-sr-x'),
            sm:clear-acl(xs:anyURI(concat($getf:strFhir,'/modules/',$query)))
        )
        default return (
            sm:chown(xs:anyURI(concat($getf:strFhir,'/modules/',$query)), 'admin:decor'),
            sm:chmod(xs:anyURI(concat($getf:strFhir,'/modules/',$query)), 'rwxr-sr-x'),
            sm:clear-acl(xs:anyURI(concat($getf:strFhir,'/modules/',$query)))
        )
    return()

};

(:~
:   Helper function with recursion for adpfix:setDecorPermissions()
:)
declare function adfix:setPermissions($path as xs:string, $collusrown as xs:string?, $collgrpown as xs:string?, $collmode as xs:string, $resusrown as xs:string?, $resgrpown as xs:string?, $resmode as xs:string) {
    if (string-length($collusrown) = 0) then () else sm:chown(xs:anyURI($path),$collusrown),
    if (string-length($collgrpown) = 0) then () else sm:chgrp(xs:anyURI($path),$collgrpown),
    sm:chmod(xs:anyURI($path),$collmode),
    sm:clear-acl(xs:anyURI($path)),
    for $res in xmldb:get-child-resources(xs:anyURI($path))
    return (
        if (empty($resusrown)) then () else sm:chown(xs:anyURI(concat($path,'/',$res)),$resusrown),
        if (empty($resgrpown)) then () else sm:chgrp(xs:anyURI(concat($path,'/',$res)),$resgrpown),
        sm:chmod(xs:anyURI(concat($path,'/',$res)),$resmode),
        sm:clear-acl(xs:anyURI(concat($path,'/',$res)))
    )
    ,
    for $collection in xmldb:get-child-collections($path)
    return
        adfix:setPermissions(concat($path,'/',$collection), $collusrown, $collgrpown, $collmode, $resusrown, $resgrpown, $resmode)
};

(:~
:   Cleanup data older than a week from audit and snapshot (query results)
:)
declare function adfix:cleanupData() {
    let $oneweekago := current-dateTime() - xs:dayTimeDuration('P7D')
    let $cleanup    :=
        for $f in xmldb:get-child-resources(xs:anyURI($getf:strFhirAudit))
        return
            if (xmldb:last-modified($getf:strFhirAudit, $f) lt $oneweekago) then xmldb:remove($getf:strFhirAudit, $f) else ()
    let $cleanup    :=
        for $f in xmldb:get-child-resources(xs:anyURI($getf:strFhirSnapshot))
        return
            if (xmldb:last-modified($getf:strFhirSnapshot, $f) lt $oneweekago) then xmldb:remove($getf:strFhirSnapshot, $f) else ()
            
    return ()
};

declare %private function adfix:checkIfUserDba() {
    if (sm:is-dba((sm:id()//sm:real/sm:username/text())[1])) then () else (
        error(QName('http://art-decor.org/ns/fhir-permissions', 'NotAllowed'), concat('Only dba user can use this module. ',(sm:id()//sm:real/sm:username/text())[1]))
    )
};
