xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

module namespace adfhirig           = "http://art-decor.org/ns/fhir/4.0/implementationguide";

import module namespace adfhir      = "http://art-decor.org/ns/fhir/4.0" at "api-fhir.xqm";
import module namespace getf        = "http://art-decor.org/ns/fhir-settings" at "fhir-settings.xqm";
import module namespace utillib     = "http://art-decor.org/ns/api/util" at "../../../api/modules/library/util-lib.xqm";
(:import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "../../../api/modules/library/settings-lib.xqm";:)
import module namespace comp        = "http://art-decor.org/ns/art-decor-compile" at "../../../art/api/api-decor-compile.xqm";
import module namespace adfhirsd    = "http://art-decor.org/ns/fhir/4.0/structuredefinition" at "../api/api-fhir-structuredefinition.xqm";
import module namespace adfhirvs    = "http://art-decor.org/ns/fhir/4.0/valueset" at "../api/api-fhir-valueset.xqm";
import module namespace markdown    = "http://art-decor.org/ns/api/markdown" at "../../../api/modules/library/markdown-lib.xqm";
declare namespace f                 = "http://hl7.org/fhir";
declare namespace error             = "http://art-decor.org/ns/fhir/error";

declare %private variable $adfhirig:type            := 'ImplementationGuide';

declare function adfhirig:compileIGresources() as element() {
(: 
 : function to compile the artifacts for an IG
 : 
 : artifacts stored in a subdirectory 'reosurces' of the IG folder
 : - in the original decor format,
 :   prefixed tmp- (template) and vs- (value set) with id and effective time as file name,
 :   e.g. tmp-2.16.840.1.113883.10.22.4.9--20170302000000.xml
 :   - these may be rendered through the normal DECOR2HTML process
 : - in FHIR format, converted from the original decor format artifacts,
 :   prefixed fsd- (structure definition) and fvs- (value set) with id and effective time as file name,
 :   e.g. fvs-2.16.840.1.113883.11.21.1--20150429000000.xml
 :   - these may be rendered through a FHIR IG for CDA process
 : 
 : In addition all artifacts as zipped into a {uuid}.zip file,
 : e.g. 4746b078-e019-40a0-86b0-78e019b0a0eb.zip in the subdirectory 'reosurces'
 : 
 : A list.xml file gets 
 : - all information about the count of the artifacts
 : - the collection path to the zip file
 : - the list of all templates and value sets in original format as template and valueSet
 : - the (hopefully) succesfully converted FHIR artifacts as structuredefinition and valueSet
 : 
 : <resources artifacts="2" zip=".../resources/4746b078-e019-40a0-86b0-78e019b0a0eb.zip">
 :  <template id="..." name="tmp-...xml" statusCode="pending" count="1" file="..." path="..."/>
 :  <fsd id="..." name="fsd-...xml" statusCode="pending" count="1" file="..." path="..."/>
 :  <valueSet id="..." name="vs-...xml" statusCode="final" count="1" file="..." path="..."/>
 :  <fvs id="..." name="fvs-...xml" statusCode="final" count="1" file="..." path="..."/>
 : ...
 : </resources>
 : 
 : 
 : 
 :)
 

(: request parameters :)
let $igCollection := request:get-attribute('ig.collection')

(: home of the IG to process :)
let $homie := $igCollection
let $homie := '/db/apps/decor/implementationguides/hl7ips/ig-20230719172642' (:***TEST***:)
let $homie := '/db/apps/decor/implementationguides/hl7ips/ig-20240414195126' (:***TEST***:)

(:  use IG, see its resource references :)
let $ig := (collection($homie)//implementationGuide)[1]
let $igid := $ig/@id

(: make temp collection :)
let $resourcecollection := xmldb:create-collection($homie, "resources")

(: compile project :)
let $request        := collection($homie)//compile-request[1]
let $projectPrefix  := $request/@for[not(. = '*')]
let $decor          := if (empty($projectPrefix)) then () else utillib:getDecorByPrefix($projectPrefix)
let $development    := $request/@development = 'true'
let $timeStamp      := if ($development) then 'development' else $request/@on
let $filters        := $request/filters

let $templates      := $ig//definition/resource[@artefact='TM']    (: all templates referenced in definition :)
let $valuesets      := $ig//definition/resource[@artefact='VS']    (: all value sets referenced in definition :)
(: this is the index page and all others as subpages :)
let $pagetree       := $ig//definition/page
(: the tree of IG pages with references to files :)
let $processedpages := adfhirig:processPages($pagetree)

let $resultsphase1  :=
    <result>
    {
        (:
            store tree of pages as a separate file, although also contained in the original DECOR IG
        :)
        let $pfna := 'all-preprocessed-pages.xml'
        let $stored := 
          if (empty($pagetree))
          then 
              concat('+++ No text pages found in IG ', $igid)
          else (
              (: store original decor format :)
              xmldb:store($resourcecollection, $pfna, $processedpages)
          )
        return
            <pages count="{count($processedpages/@count)}" file="{$pfna}" path="{$stored}"/>,
            
        (: 
            store all pages separately as html files,
            suffixed dp- for original pre-processed ART-DECOR pages and 
            suffxied fp- for pre-processed FHIR IG ready pages
        :)
        let $dsinglepages :=
            for $p in $processedpages//dcontent
            let $dpname := $p/@name
            let $dtitle := $p/@title
            let $dbody := $p/content
            let $dcontent :=
                serialize(
                    $dbody/node(),
                    map {"method": "xml", "indent": true() }
                )
            let $dcontent := concat('<div xmlns="http://www.w3.org/1999/xhtml">', $dcontent, '</div>')
            let $stored := xmldb:store($resourcecollection, $dpname, $dcontent)
            return
                <page file="{$dpname}" path="{$stored}"/>
        let $fsinglepages :=
            for $p in $processedpages//fcontent
            let $fpname := $p/@of
            let $ftitle := string($p/page/title/@value)
            let $fbody := $p/page/content
            let $fcontent :=
                serialize(
                    $fbody/node(),
                    map {"method": "xml", "indent": true() }
                )
            let $fcontent := concat('<div xmlns="http://www.w3.org/1999/xhtml">', $fcontent, '</div>')
            let $stored := xmldb:store($resourcecollection, $fpname, $fcontent)
            return
                <page file="{$fpname}" path="{$stored}"/>
        
        return ($dsinglepages, $fsinglepages)

    }
    </result>
    
(:  create compiled project for further processing :)
let $filters        := comp:getCompilationFilters($decor, (), ())
let $filters        := comp:getFinalCompilationFilters($decor, $filters)
let $project        := comp:compileDecor($decor, '*', $timeStamp, $filters, false(), false())
(: store compiled project :)
let $cpna           := 'compiled.xml'
let $stored         := xmldb:store($resourcecollection, $cpna, $project)

let $resultsphase2        :=
    <result>
    {
        (: all of before :)
        $resultsphase1/*,

        (: 
            all templates referenced in resources
            -------------
        :)
        for $t in $templates (:***TEST predicate [@id='2.16.840.1.113883.10.22.4.9'] ***:)
        let $id := $t/@id
        let $me := $t/@effectiveDate
        (:let $me := max($t/xs:dateTime(@effectiveDate)):)
        (:  make up the file names :)
        let $dfna := concat('tmp-', $id, '--', replace($me,'[^\d]',''), '.xml')
        let $ffna := concat('fsd-', $id, '--', replace($me,'[^\d]',''), '.xml')
        let $sa := $t/@statusCode
        (: get artifact with certain status codes only :)
        let $et := ($project//template[@id=$id][@effectiveDate=$me][@statusCode=('draft','active','pending','review')])[1]
        let $dn := $et/@displayName
        (:  conversion to FHIR :)
        let $re :=
            if (count($et) eq 1)
            then
                try {
                    adfhirsd:convertDecorTemplate2FHIRStructureDefinition($et)
                   
                } catch * {
                    <errors>
                        <error>Caught error {$err:code}: {$err:description}</error>
                    </errors>
                }
            else
                <errors>
                    <error>Error: unexpected cardinality of artifact {$id/text()} - {count($et)} found, 1 expected</error>
                </errors>
        (: 
            build result:
            1. if artifact exist, store its original...
        :)
        let $stored := 
            if (empty($et))
            then 
                concat('+++ No artifact found with id ', $id, ' as of ', $me) 
            else (
                xmldb:store($resourcecollection, $dfna, $et[1])
            )
        (:
            2. store converted format if successfully converted or give back the caught error message
        :)
        let $storef := 
            if (empty($re) or $re/error)
            then
                concat('+++ Conversion failed for artifact with id ', $id, ' as of ', $me, ' - ', $re/node()) 
            else (
                xmldb:store($resourcecollection, $ffna, $re)
            )
        return
            (
                <template id="{$t/@id}" displayName="{$dn}" statusCode="{$sa}" count="{count($et)}" file="{$dfna}">
                {
                    if ($stored[starts-with(., '+++ ')]) 
                    then attribute error {$stored}
                    else attribute path {$stored}
                }
                </template>,
                <fsd id="{$t/@id}" displayName="{$dn}" statusCode="{$sa}" count="{count($re)}" file="{$ffna}">
                {
                    if ($storef[starts-with(., '+++ ')]) 
                    then attribute error {$storef}
                    else attribute path {$storef}
                }
                </fsd>
            )   
        ,
        (:
            all value sets referenced in resources
            --------------
        :)
        for $t in $valuesets (:***TEST predicate [@id='2.16.840.1.113883.11.21.1'] ***:)
        let $id := $t/@id
        let $me := $t/@effectiveDate
        (:  make up the file names :)
        let $dfna := concat('vs-', $id, '--', replace($me,'[^\d]',''), '.xml')
        let $ffna := concat('fvs-', $id, '--', replace($me,'[^\d]',''), '.xml')
        let $sa := $t/@statusCode
        (: get artifacts with certain status codes only :)
        let $et := ($project//valueSet[@id=$id][@effectiveDate=$me][@statusCode=('draft','final','pending','review')])[1]
        let $dn := $et/@displayName
        (:  conversion to FHIR :)
        let $re :=
            if (count($et) eq 1)
            then
                try {
                    adfhirvs:convertDecorValueSet2FHIRValueSet($et, $project)
                } catch * {
                    <errors>
                        <error>Caught error {$err:code}: {$err:description}</error>
                    </errors>
                }
            else 
                <errors>
                    <error>Error: unexpected cardinality of artifact {$id/text()} - {count($et)} found, 1 expected</error>
                </errors>
        (: 
            build result:
            1. if artifact exist, store its original...
        :)
        let $stored := 
            if (empty($et))
            then
                concat('+++ No artifact found with id ', $id, ' as of ', $me) 
            else (
                xmldb:store($resourcecollection, $dfna, $et[1])
            )
        (:
            2. store converted format if successfully converted or give back the caught error message
        :)
        let $storef := 
            if (empty($re) or $re/error)
            then
                concat('+++ Conversion failed for artifact with id ', $id, ' as of ', $me, ' - ', $re/node())  
            else (
              (: store converted FHIR format :)
              xmldb:store($resourcecollection, $ffna, $re)
            )
        return
            (
                <valueSet id="{$t/@id}" displayName="{$dn}" statusCode="{$sa}" count="{count($et)}" file="{$dfna}">
                {
                    if ($stored[starts-with(., '+++ ')]) 
                    then attribute error {$stored}
                    else attribute path {$stored}
                }
                </valueSet>,
                <fvs id="{$t/@id}" displayName="{$dn}" statusCode="{$sa}" count="{count($re)}" file="{$ffna}">
                {
                    if ($storef[starts-with(., '+++ ')]) 
                    then attribute error {$storef}
                    else attribute path {$storef}
                }
                </fvs>
            )
            
    }
    </result>
    
    
let $resultsphase3  :=
    <result>
    {
        (: all of before :)
        $resultsphase1/*,
        $resultsphase2/*,

        (:
            store the original DECOR IG
        :)
        let $igna   := 'decor-implementationguide.xml'
        let $stored := 
          if (empty($ig))
          then 
              concat('+++ No implementation guide found with ID ', $igid)
          else (
              (: store original decor format :)
              xmldb:store($resourcecollection, $igna, $ig)
          )
        return
            <implementationguide type="DECOR" file="{$igna}" path="{$stored}"/>
        ,
        (:
            create the FHIR IG (if requested) with special parameters
        :)
        let $figna  := 'fhir-implementationguide.xml'
        let $fpara  := ()
        let $fig    := adfhirig:createFhirIg($ig, $processedpages, $fpara)
        let $stored := 
          if (empty($fig))
          then 
              concat('+++ No FHIR implementation guide created based on IG ID ', $igid)
          else (
              (: store original decor format :)
              xmldb:store($resourcecollection, $figna, $fig)
          )
        return
            <implementationguide type="FHIR" file="{$figna}" path="{$stored}"/>
    }
    </result>
    
    (: construct zip-bound <entry> elements for the documents in the resource collection :)
    let $entries := collection($resourcecollection) !
        <entry name="{util:document-name(.)}" type="xml" method="store">
        {
            serialize(., map { "method": "xml" })
        }
        </entry>
    
    (: compress the entries, store later to db :)
    let $zc := compression:zip($entries, false())
    
    (: remove entries, only list and zip will remain :)
    let $dele := collection($resourcecollection) ! xmldb:remove($resourcecollection, util:document-name(.))
    
    (: converted artifact summary zip file :)
    let $uuid := util:uuid()
    let $zipfile := concat($uuid, '.zip')

    (: store the zip :)
    let $sz := xmldb:store($resourcecollection, $zipfile, $zc)
    
    (: create the list file, store to db :)
    let $list := 
        <resources>
        {
            attribute implementationguide { $igid },
            attribute artifacts { count($resultsphase3/*) },
            attribute errors { count($resultsphase3//*/@error) },
            attribute zip { $sz },
            $resultsphase3/*
        }
        </resources>
    (: store list file :)
    let $sl := xmldb:store($resourcecollection, concat('list-', $uuid, '.xml'), $list)
    
    (: finally return results that are also in list-xxxxxxx.xml :)
    return
        $list
};

(:~ Process IG pages

    Use the original DECOR IG pages such as
    
    <page id="6e55fc85-13fc-403a-95fc-8513fc803af9" lastModifiedDate="2023-07-19T17:26:42">
        <title language="en-US" lastTranslated="2023-07-19T17:27:04">Index page</title>
        <content language="en-US" lastTranslated="2023-07-19T17:27:04">
            <p>This document contains: Standard for Trial Use <strong>International Patient Summary</strong> (2023)</p>
        </content>
    </page>
    
    The function param $page is already a list of all DECOR //pages without hierarchy.
        
    Return two kinds of pages preprocessed:
    
    1. element page/dcontent
    DECOR IG pages as in the original but with replaced image src tags
    ie. resolve all <img src="/exist/apps/api/project/hl7ips-/blob/5d64cc6f-3d09-47f2-a4cc-6f3d0997f238"/>
    as local references to the image, eg 5d64cc6f-3d09-47f2-a4cc-6f3d0997f238.png
    
    2. element page/fcontent
    FHIR pages with replaced image src tags (see above) and with external references (in fhir namespace)
    <page> 
      <name value="patient-example.html"/> 
      <title value="Example Patient Page"/> 
      <generation value="html"/> 
      <page> 
        <name value="list.html"/> 
        <title value="Value Set Page"/> 
        <generation value="html"/> 
      </page> 
    </page>
    
    There is always a wrapping element <pages> and subsequently all page content as
    <dcontent id="pagename">...html content...</dcontent> or <fcontent id="pagename">...html content...</fcontent>
    
:)
declare %private function adfhirig:processPages($pagetree as node()) as element() {

(: handle root page and all subsequent pages as skeletton :)
let $tmp := (:adfhirig:process1Page($pagetree):)
    <pagelist>
    {
        <page>
        {
            $pagetree/@*,
            $pagetree/(* except page)
        }
        </page>,
        for $p in $pagetree//page
        return 
            <page>
            {
                $p/@*,
                $p/(* except page)
            }
            </page>
    }
    </pagelist>

(:
    get content separately, do some replacements
:)

let $dcontent :=
    for $c in $tmp//page
    let $pagename := concat('dp-', $c/@id, '.xml')
    return
        <dcontent name="{$pagename}" title="{$c/title[1]}">
        {
            if (empty($c/content[1])) then <no-content/> else adfhirig:copyXHTMLAndReplaceIMG($c/content[1])
        }
        </dcontent>

let $fcontent :=
    for $c in $tmp//page
    let $pagename := concat('fp-', $c/@id, '.xml')
    return
        <fcontent of="{$pagename}">
            <page>
            {
                <name value="{$pagename}"/>,
                <title value="{$c/title[1]}"/>,
                <generation value="html"/>,
                if (empty($c/content[1])) then <no-content/> else adfhirig:copyXHTMLAndShiftHEADs(adfhirig:copyXHTMLAndReplaceIMG($c/content[1]))
            }
            </page>
        </fcontent>

(: wrap all pages :)
let $pages := 
    <pages count="{count($tmp//page)}">
    {
        $tmp,  (: only for now for debug purposes :)
        $dcontent,
        $fcontent
    }
    </pages>
    
return $pages

};

(:~ This function returns all page elements (without content) recursively processed as one node set (list)
:)
declare %private function adfhirig:process1Page($page as node()) as element() {

(: handle page :)
let $pageL :=
    for $p in $page//page
    return 
        <page>
        {
            $p/@*
        }
        </page>
    
return $pageL
    
};

(:~ This function copies all XHTML elements and replaces certain elements / attributes
:)
declare %private function adfhirig:copyXHTMLAndReplaceIMG($n as node()*) as node()* {
   let $xsl :=
      <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xhtml="http://www.w3.org/1999/xhtml">
      
        <xsl:output method="xml" omit-xml-declaration="yes"/>
            
        <!-- 
            replace the img blob constructs from API-callable hrefs to local hrefs
            example as in ig pages and what the extract result here should be
              <img src="/exist/apps/api/project/hl7ips-/blob/f784fd13-e44c-4a1c-84fd-13e44cfa1cc3" alt="ART-DECOR Blob Image f784fd13-e44c-4a1c-84fd-13e44cfa1cc3"/>
              becomes
              <img src="/exist/apps/api/project/hl7ips-/blob/f784fd13-e44c-4a1c-84fd-13e44cfa1cc3" alt="ART-DECOR Blob Image f784fd13-e44c-4a1c-84fd-13e44cfa1cc3"/>
                          
        -->
        <xsl:template match="img[starts-with(@src, '/exist/apps/api/project/')][contains(@src, '/blob/')]">
          <xsl:variable name="srcrep" select="concat(tokenize(@src, '/')[last()], '.png')"/>
          <xsl:element name="img">
            <xsl:attribute name="src">
              <xsl:value-of select="$srcrep"/>
            </xsl:attribute>
            <xsl:attribute name="alt">
              <xsl:value-of select="$srcrep"/>
            </xsl:attribute>
            <xsl:text> </xsl:text>
          </xsl:element>
        </xsl:template>
      
        <xsl:template match="*|@*">
          <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:apply-templates/>
          </xsl:copy>
        </xsl:template>
      
        <xsl:template match="processing-instruction()|comment()">
          <xsl:copy>.</xsl:copy>
        </xsl:template>
      
      </xsl:stylesheet>
      
      (: add a wrapping div and out without the div :)
      return (transform:transform(<div>{$n}</div>, $xsl, ())/*)
};

(:~ This function shifts all HEAD elements H1, H2 etc to 2 levels down (h1->h3, h2->h4 etc) for the FHIR IG page input
:)
declare %private function adfhirig:copyXHTMLAndShiftHEADs($n as node()*) as node()* {
   let $xsl :=
      <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xhtml="http://www.w3.org/1999/xhtml">
      
        <xsl:output method="xml" omit-xml-declaration="yes"/>
        
        <xsl:template match="H1|h1">
            <h3>
            {
                <xsl:apply-templates/>
            }
            </h3>
        </xsl:template>
        <xsl:template match="H2|h2">
            <h4>
            {
                <xsl:apply-templates/>
            }
            </h4>
        </xsl:template>
        <xsl:template match="H3|h3">
            <h5>
            {
                <xsl:apply-templates/>
            }
            </h5>
        </xsl:template>
        <xsl:template match="H4|h4">
            <h6>
            {
                <xsl:apply-templates/>
            }
            </h6>
        </xsl:template>
      
        <xsl:template match="*|@*">
          <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:apply-templates/>
          </xsl:copy>
        </xsl:template>
      
        <xsl:template match="processing-instruction()|comment()">
          <xsl:copy>.</xsl:copy>
        </xsl:template>
      
      </xsl:stylesheet>
      
      (: add a wrapping div and out without the div :)
      return (transform:transform(<div>{$n}</div>, $xsl, ())/*)
};


declare %private function adfhirig:createFhirIg($decorig as node(), $fhirpages as node(), $parameters as node()?) as element() {

(:
<implementationGuide
  id="2.16.840.1.113883.3.1937.777.13.29.1"
  effectiveDate="2023-07-19T17:26:42"
  name="hl7ips-20230719172642-implementationguide"
  statusCode="draft"
  lastModifiedDate="2023-08-23T10:32:04"
  versionLabel="2023"
  projectId="2.16.840.1.113883.3.1937.777.13">
    <title language="en-US" lastTranslated="2023-07-19T17:27:04">International Patient Summary IPS</title>

:)

let $result :=
    <ImplementationGuide xmlns="http://hl7.org/fhir">
        <id value="{$decorig/@name}"/>
        <url value="{$decorig/@id}"/>
        <version value="{$decorig/@versionLabel}"/>
        <name value="{$decorig/@name}"/>
        <title value="{$decorig/title/text()}"/>
        <status value="{$decorig/@statusCode}"/>
        
        <publisher value="Health Level Seven"/>
        
        <contact>
            <name value="HL7 International - International Patient Summary"/>
            <telecom>
                <system value="url"/>
                <value value="http://www.hl7.org/Special/committees/structure"/>
            </telecom>
        </contact>
        
        <packageId value="hl7.cda.uv.ips"/>
        
        <fhirVersion value="5.0.0"/>
        
        <!--
        <dependsOn id="cda">
            <uri value="http://hl7.org/fhir/cda/ImplementationGuide/hl7.fhir.cda"/>
            <packageId value="hl7.fhir.cda"/>
            <version value="dev"/>
        </dependsOn>
        -->
        
        <definition>
            <grouping id="document">
                <name value="Document Templates"/>
                <description value="Document-level templates ..."/>
            </grouping>
            <grouping id="header">
                <name value="Header Templates"/>
                <description value="Header-level Templates ..."/>
            </grouping>
            <grouping id="section">
                <name value="Section Templates"/>
                <description value="Section-level Templates ..."/>
            </grouping>
            <grouping id="entry">
                <name value="Entry Templates"/>
                <description value="Entry-level Templates"/>
            </grouping>
            <grouping id="other">
                <name value="Other Templates"/>
                <description value="Other templates ..."/>
            </grouping>
            <resource>
                <reference>
                    <reference value="StructureDefinition/2.16.840.1.113883.10.22.1.1--20200714160821"/>
                    <display value="HL7-IPS"/>
                </reference>
                <name value="HL7-IPS"/>
                <exampleBoolean value="false"/>
                <groupingId value="document"/>
            </resource>
            {
                $fhirpages/page
            }
            <parameter>
                <code>
                   <system value="http://hl7.org/fhir/tools/CodeSystem/ig-parameters"/>
                   <code value="releaselabel"/>
                </code>
                <value value="CI Build"/>
            </parameter>
            <parameter>
                <code>
                  <system value="http://hl7.org/fhir/tools/CodeSystem/ig-parameters"/>
                  <code value="copyrightyear"/>
                </code>
                <value value="2024+"/>
            </parameter>
        </definition>
    </ImplementationGuide>

return $result

};