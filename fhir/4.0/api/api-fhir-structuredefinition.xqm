xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

module namespace adfhirsd           = "http://art-decor.org/ns/fhir/4.0/structuredefinition";
import module namespace adfhir      = "http://art-decor.org/ns/fhir/4.0" at "api-fhir.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../../art/api/api-server-settings.xqm";
import module namespace getf        = "http://art-decor.org/ns/fhir-settings" at "fhir-settings.xqm";
import module namespace utillib     = "http://art-decor.org/ns/api/util" at "../../../api/modules/library/util-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "../../../api/modules/library/settings-lib.xqm";
import module namespace vs          = "http://art-decor.org/ns/decor/valueset" at "../../../art/api/api-decor-valueset.xqm";
import module namespace templ       = "http://art-decor.org/ns/decor/template" at "../../../art/api/api-decor-template.xqm";
import module namespace markdown    = "http://art-decor.org/ns/api/markdown" at "../../../api/modules/library/markdown-lib.xqm";
declare namespace f                 = "http://hl7.org/fhir";
declare namespace error             = "http://art-decor.org/ns/fhir/error";
declare namespace hl7               = "urn:hl7-org:v3";

declare %private variable $adfhirsd:type                := 'StructureDefinition';
declare %private variable $adfhirsd:inactiveStatusCodes := ('cancelled','rejected','deprecated');

declare %private variable $adfhirsd:sdprefix            := 'http://hl7.org/cda/stds/core/StructureDefinition/';

(:elements shall be xs:NCName. adfhir:shortName was designed to create those, but does too much sometimes such as 
  putting everything in lower-case and placing an underscore before every capital that is not the initial letter.
  So we test the name as-is first to be an NCName before falling back to the shortName. Note that 
  $name castable as xs:NCName did not work for some reason. Hence the less readable but equivalent regular expression :)
declare %private variable $adfhirsd:ncnamePattern       := '^[\i-[:]][\c-[:]]*$';
declare %private variable $adfhirsd:ucum                := if (doc-available(concat($setlib:strDecorCore, '/DECOR-ucum.xml'))) then doc(concat($setlib:strDecorCore, '/DECOR-ucum.xml'))/*/ucum else ();

declare function adfhirsd:convertDecorDataset2FHIRStructureDefinition($datasetOrTransaction as element()) as element() {
let $isTransaction  := exists($datasetOrTransaction/@transactionId)
let $decorId        := if ($isTransaction) then $datasetOrTransaction/@transactionId else $datasetOrTransaction/@id
let $decorEffectiveDate 
                    := if ($isTransaction) then $datasetOrTransaction/@transactionEffectiveDate else $datasetOrTransaction/@effectiveDate
let $decorStatusCode
                    := if ($isTransaction) then $datasetOrTransaction/@transactionStatusCode else $datasetOrTransaction/@statusCode
let $decorExpirationDate 
                    := if ($isTransaction) then $datasetOrTransaction/@transactionExpirationDate else $datasetOrTransaction/@expirationDate
let $decorVersionLabel
                    := if ($isTransaction) then $datasetOrTransaction/@transactionVersionLabel else $datasetOrTransaction/@versionLabel
let $fhirId         := concat($decorId,'--',replace($decorEffectiveDate,'[^\d]',''))
let $maps           := 
    for $codeSystem in $datasetOrTransaction//@codeSystem 
    let $csid := $codeSystem
    let $csed := $codeSystem/../@codeSystemVersion
    group by $csid, $csed
    return 
        map:entry($csid || $csed, utillib:getCanonicalUriForOID('CodeSystem', $csid, $csed, $datasetOrTransaction/@prefix, $setlib:strKeyFHIRR4))
let $oidfhirmap     := map:merge($maps)
let $language       := if ($datasetOrTransaction/*:name[@language = 'en-US']) then 'en-US' else data($datasetOrTransaction/*:name[1]/@language)
let $dataset        := if ($isTransaction) then utillib:getDataset($datasetOrTransaction/@id, $datasetOrTransaction/@effectiveDate, $datasetOrTransaction/@versionDate, '*') else ()

let $name           := 
    if ($isTransaction and $datasetOrTransaction/*:name[@language = $language][node()][. = 'Registration']) then
        ($dataset/*:name[@language = $language], $dataset/*:name)[node()][1]
    else (
        ($datasetOrTransaction/*:name[@language = $language], $datasetOrTransaction/*:name)[.//text()[string-length()>0]][1]
    )
let $description    := 
    if ($isTransaction and $datasetOrTransaction/*:desc[@language = $language][node()][. = '-']) then
        ($dataset/*:desc[@language = $language], $dataset/*:desc)[node()][1]
    else (
        ($datasetOrTransaction/*:desc[@language = $language], $datasetOrTransaction/*:desc)[.//text()[string-length()>0]][1]
    )

(: fullDatasetTree contains shortName based on project default language, while we want a language that matches the $language which may be different :)
let $shortName      := 
    (:if (matches($name, $adfhirsd:ncnamePattern)) then $name else:) 
    (:if ($datasetOrTransaction[@shortName]) then $datasetOrTransaction/@shortName else adfhir:shortName($name):)
    adfhir:shortName($name)
let $rootPath       := if ($datasetOrTransaction/*:concept[2]) then $shortName else ()

let $rootElem       := 
    if ($datasetOrTransaction/*:concept[2]) then (
        <element id="{$fhirId}" xmlns="http://hl7.org/fhir">
            <path value="{$rootPath}"/>
            <short value="{replace($name, '(^\s+)|(\s+$)', '')}">
            {
                for $node in $datasetOrTransaction/*:name[not(@language = $language)][.//text()[string-length()>0]]
                return
                    <extension url="http://hl7.org/fhir/StructureDefinition/translation">
                        <extension url="lang">
                            <valueCode value="{$node/@language}"/>
                        </extension>
                        <extension url="content">
                            <valueMarkdown value="{$node}"/>
                        </extension>
                    </extension>
            }
            </short>
        {
                (: KH 20210212: was <definition value="{utillib:serializeNode($datasetOrTransaction/*:desc[node()][1])/text()}"> :)
                (: AH 20210212: was <definition value="{adfhir:stripHTMLtags($datasetOrTransaction/*:desc[node()][1])}">:)
                let $desc   := ($datasetOrTransaction/*:desc[@language = 'en-US'][.//text()], $datasetOrTransaction/*:desc[.//text()])[1]
                (: required for http://hl7.org/fhir/shareablestructuredefinition.html:)
                let $desc   := if (empty($desc)) then '-' else markdown:html2markdown(utillib:parseNode($desc)/node())
                return
                    <definition value="{$desc}"> 
                    {
                        for $node in $datasetOrTransaction/*:desc[not(@language = $language)][.//text()[string-length()>0]]
                        return
                            <extension url="http://hl7.org/fhir/StructureDefinition/translation">
                                <extension url="lang">
                                    <valueCode value="{$node/@language}"/>
                                </extension>
                                <extension url="content">
                                    <valueMarkdown value="{markdown:html2markdown(utillib:parseNode($node)/node())}"/>
                                </extension>
                            </extension>
                    }
                    </definition>
        }
            {
            (: KH 20240218 only transactions have cardinalities, see https://art-decor.atlassian.net/browse/AD30-1442 :)
            (:if ($isTransaction) then (
                <min value="1"/>,
                <max value="1"/>
            ) else ():)
            <min value="1"/>,
            <max value="1"/>
        }
            <base>
                <path value="{$rootPath}"/>
                <min value="1"/>
                <max value="1"/>
            </base>
            {
            (: KH 20240218 only transactions have conformance rules :)
            if ($isTransaction) then (
                <mustSupport value="true"/>
            ) else ()
        }
        </element>
    ) else ()
let $rootType       :=
    if ($rootElem) then (
        $rootPath
    ) 
    else (
        let $concept            :=
            if ($isTransaction) then $datasetOrTransaction/*:concept[1] else (
                $datasetOrTransaction/*:concept[not(@statusCode = $adfhirsd:inactiveStatusCodes)][1]
            )
        let $originalConcept    := utillib:getOriginalForConcept($concept)
        let $name               := 
            if ($originalConcept/*:name[@language = $language]) then 
                $originalConcept/*:name[@language = $language]
            else 
                $originalConcept/*:name[node()][1]
        return
            (: fullDatasetTree contains shortName based on project default language, while we want a language that matches the $language which may be different :)
            (:if (matches($name, $adfhirsd:ncnamePattern)) then $name else:) 
            (:if ($concept[@shortName]) then ($concept/@shortName) else adfhir:shortName($name):)
            adfhir:shortName($name)
    )
let $prefix         := ($datasetOrTransaction/@prefix | $datasetOrTransaction/ancestor::decor/project/@prefix)[1]

let $snapshot       :=
    <snapshot xmlns="http://hl7.org/fhir">
    {
        $rootElem,
        if ($isTransaction) then 
            for $concept in $datasetOrTransaction/*:concept
            return
                adfhirsd:convertDecorConcept2FHIRElementDefinition($concept, $rootPath, $prefix, $language, $isTransaction, $oidfhirmap)
        else (
            for $concept in $datasetOrTransaction/*:concept[not(@statusCode = $adfhirsd:inactiveStatusCodes)]
            return
                adfhirsd:convertDecorConcept2FHIRElementDefinition($concept, $rootPath, $prefix, $language, $isTransaction, $oidfhirmap)
        )
    }
    </snapshot>

(:FHIR id may not have colons. This affects using @effectiveDate as part of the Resource.id
    Remove any non-digit from the effectiveDate before adding it to the URI, e.g. 20161026123456
:)
return
(:<StructureDefinition xmlns="http://hl7.org/fhir" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:schemaLocation="http://hl7.org/fhir http://hl7.org/fhir/structuredefinition.xsd">:)
<StructureDefinition xmlns="http://hl7.org/fhir">
    <id value="{$fhirId}"/>
    { ()
    (:<meta>
        <profile value="http://hl7.org/fhir/{$getf:strFhirVersionShort}/StructureDefinition/StructureDefinition"/>
    </meta>:)
    }
    <language value="{$language}"/>
{
    (: Any object in ART-DECOR is effective from effectiveDate to expirationDate :)
    if ($datasetOrTransaction[@effectiveDate castable as xs:dateTime] | $datasetOrTransaction[@expirationDate castable as xs:dateTime]) then (
        <extension url="http://hl7.org/fhir/StructureDefinition/resource-effectivePeriod">
            <valuePeriod>
            {
                if ($datasetOrTransaction[@effectiveDate castable as xs:dateTime]) then 
                    <start value="{adjust-dateTime-to-timezone(xs:dateTime($decorEffectiveDate))}"/>
                else (),
                if ($datasetOrTransaction[@expirationDate castable as xs:dateTime]) then
                    <end value="{adjust-dateTime-to-timezone(xs:dateTime($decorExpirationDate))}"/>
                else ()
            }
            </valuePeriod>
        </extension>
    ) else ()
}
{
    let $projectprefix      := request:get-attribute('request.projectprefix')[string-length()>0]
    let $projectversion     := request:get-attribute('request.projectversion')[string-length()>0]
    return
    comment {
        let $str := concat($getf:strFhirServices, $getf:strFhirVersionShort, '/', string-join(($projectprefix, replace($projectversion, '[^\d]', '')), ''), '/', string-join(($prefix, replace($datasetOrTransaction/@versionDate, '[^\d]', '')), ''), '/StructureDefinition/', $fhirId)
        return concat(' ', replace(replace($str, '--', '-\\-'), '//', '/'),' ')
    }
}
    <url value="{
        if ($datasetOrTransaction[@transactionCanonicalUri]) then $datasetOrTransaction/@transactionCanonicalUri else 
        if ($datasetOrTransaction[@canonicalUri]) then $datasetOrTransaction/@canonicalUri
        else concat($getf:strFhirServices,'StructureDefinition/', $fhirId)}"/>
    <identifier>
        <use value="official"/>
        <system value="urn:ietf:rfc:3986"/>
        <value value="urn:oid:{$decorId}"/>
    </identifier>
    <version value="{let $semver := adfhir:getSemverString($decorVersionLabel) return if (empty($semver)) then $decorEffectiveDate else $semver}"/>
    <name value="{adfhir:validResourceName($name)}">
    {
        for $node in $name/../*:name[not(@language = $language)][.//text()[string-length()>0]]
        return
            <extension url="http://hl7.org/fhir/StructureDefinition/translation">
                <extension url="lang">
                    <valueCode value="{$node/@language}"/>
                </extension>
                <extension url="content">
                    <valueMarkdown value="{$node}"/>
                </extension>
            </extension>
    }
    </name>
    <status value="{adfhirsd:decorStatus2fhirStatus($decorStatusCode)}"/>
    {
        if ($datasetOrTransaction/*:property[@name = 'DCM::ModelerList']) then 
            <publisher value="{$datasetOrTransaction/*:property[@name = 'DCM::ModelerList']}"/>
        else ()
    }
    {
        if ($description) then 
            <description value="{markdown:html2markdown(utillib:parseNode($description)/node())}">
            {
                for $node in $description/../*:desc[not(@language = $language)][.//text()[string-length()>0]]
                return
                    <extension url="http://hl7.org/fhir/StructureDefinition/translation">
                        <extension url="lang">
                            <valueCode value="{$node/@language}"/>
                        </extension>
                        <extension url="content">
                            <valueMarkdown value="{markdown:html2markdown(utillib:parseNode($node)/node())}"/>
                        </extension>
                    </extension>
            }
            </description>
        else ()
    }
    <fhirVersion value="{$getf:strFhirVersion}"/>
    <kind value="logical"/>
    <abstract value="true"/>
    <type value="{$rootType}"/>
    <baseDefinition value="http://hl7.org/cda/stds/core/StructureDefinition/Element"/>
    <derivation value="specialization"/>
    {
        $snapshot
    }
    <differential>
    {
        $snapshot/node()
    }
    </differential>
</StructureDefinition>
};

declare function adfhirsd:convertDecorConcept2FHIRElementDefinition($concept as element(concept), $parentPath as xs:string?, $prefix as xs:string?, $language as xs:string, $isTransaction as xs:boolean, $oidfhirmap as map(*)) as element()* {
    let $originalConcept    := utillib:getOriginalForConcept($concept)
    
    let $name               := 
        if ($originalConcept/*:name[@language = $language]) then 
            $originalConcept/*:name[@language = $language][1]
        else 
            $originalConcept/*:name[node()][1]
    
    (: fullDatasetTree contains shortName based on project default language, while we want a language that matches the $language which may be different :)
    (:let $shortName          := (\:if (matches($name, $adfhirsd:ncnamePattern)) then $name else:\) if ($concept[@shortName]) then $concept/@shortName else adfhir:shortName($name):)
    let $shortName          := adfhir:shortName($name)
    let $shortName          := if (empty($parentPath)) then $shortName else lower-case(substring($shortName, 1, 1)) || substring($shortName, 2)
    let $id                 := 
        if ($concept[@id]) then
            $concept/concat(@id,'--',replace(@effectiveDate,'[^\d]',''))
        else
            $concept/concat(@ref,'--',replace(@flexibility,'[^\d]',''))
    let $path               := string-join(($parentPath,$shortName),'.')
    let $minCard            := utillib:getMinimumMultiplicity($concept)
    let $maxCard            := utillib:getMaximumMultiplicity($concept)
    let $isRequired         := $concept[@conformance='R']
    let $isMandatory        := $concept[@isMandatory='true']
    
    let $conditionText      := 'One concept must be selected'
    let $condition          :=
        for $conditions at $i in $concept/../*:concept/*:condition[*:desc[starts-with(., $conditionText)]]
        let $grp    := $conditions/*:desc[starts-with(., $conditionText)]
        group by $grp
        return
            <condition value="{($conditions[1]/../@iddisplay, 'cond')[1]}-{$i[1]}" xmlns="http://hl7.org/fhir"/>
    let $constraints        :=
        for $conditions at $i in $concept/*:concept/*:condition[*:desc[starts-with(., $conditionText)]]
        let $grp    := $conditions/*:desc[starts-with(., $conditionText)]
        group by $grp
        return (
            let $choiceBox  := 
                for $choiceBoxConcept in $conditions/ancestor::*:concept[1]
                let $actualConceptName          := 
                    if ($choiceBoxConcept/*:name[@language = $language]) then 
                        $choiceBoxConcept/*:name[@language = $language][1]
                    else 
                        $choiceBoxConcept/*:name[node()][1]
                (: fullDatasetTree contains shortName based on project default language, while we want a language that matches the $language which may be different :)
                let $actualShortName            :=
                    (:if (matches($actualConceptName, $adfhirsd:ncnamePattern)) then $actualConceptName else :)
                    (:if ($choiceBoxConcept[@shortName]) then $choiceBoxConcept/@shortName else adfhir:shortName($actualConceptName):)
                    adfhir:shortName($actualConceptName)
                order by $actualShortName
                return
                    $actualShortName
                    
            return
            <constraint xmlns="http://hl7.org/fhir">
                <key value="{($conditions[1]/../../@iddisplay, 'cond')[1]}-{$i[1]}"/>
                <severity value="error"/>
            {
                let $humanDesc  :=
                    if ($conditions[1]/*:desc[@language = $language]) then 
                        $conditions[1]/*:desc[@language = $language][1]
                    else 
                        $conditions[1]/*:desc[node()][1]
                return
                if ($humanDesc) then 
                    <human value="{utillib:serializeNode($humanDesc)//text()}">
                    {
                        for $node in $conditions[1]/*:desc[not(@language = $humanDesc/@language)][.//text()[string-length() gt 0]]
                        return
                            <extension url="http://hl7.org/fhir/StructureDefinition/translation">
                                <extension url="lang">
                                    <valueCode value="{$node/@language}"/>
                                </extension>
                                <extension url="content">
                                    <valueMarkdown value="{markdown:html2markdown(utillib:parseNode($node)/node())}"/>
                                </extension>
                            </extension>
                    }
                    </human>
                else (
                    (: The following will be invalid in the StructureDefinition, but shows the value in the dataset is empty. :)
                    <human value="-"/>
                )
            }
                <expression value="{$path}.where({string-join($choiceBox, ' or ')}).count() = 1"/>
            </constraint>
            )
    
    let $concept            :=
        if ($concept[count(*:concept) = 1]/*:concept/*:contains) then (
            let $actualConceptName          := 
                if ($concept/*:concept/*:name[@language = $language]) then 
                    $concept/*:concept/*:name[@language = $language][1]
                else 
                    $concept/*:concept/*:name[node()][1]
            (: fullDatasetTree contains shortName based on project default language, while we want a language that matches the $language which may be different :)
            let $actualShortName            := 
                (:if (matches($actualConceptName, $adfhirsd:ncnamePattern)) then $actualConceptName else:)
                (:if ($concept/*:concept[@shortName]) then $concept/*:concept/@shortName else adfhir:shortName($actualConceptName):)
                adfhir:shortName($actualConceptName)
            
            return
                if ($shortName = $actualShortName) then $concept/*:concept else $concept
        )
        else ($concept)
    
    (: presence of @shortName is an indicator of fullDatasetTree instead of dataset :)
    let $assocs             := 
        if ($concept[@shortName]) then
            $concept/*:terminologyAssociation | 
            $concept/*:valueDomain//*:terminologyAssociation
        else
            utillib:getConceptAssociations($concept, $originalConcept, false())/*
    (: utillib:getGetEnhancedValueset does the heavy lifting, but could produce an empty valueSet element, so check that before relying on it. :)
    let $valueSets          := 
        if ($concept[*:valueSet[@id | @ref]]) then
            $concept/*:valueSet
        else (
            for $assoc in $assocs[@valueSet]
            return vs:getValueSetById($assoc/@valueSet, if ($assoc/@flexibility) then $assoc/@flexibility else 'dynamic', false())//*:valueSet[@id]
        )
    
    let $valueDomain        := if ($originalConcept/*:valueDomain[2]) then () else $originalConcept/*:valueDomain[1]
    let $dataType           := adfhirsd:decorValueDomainType2FHIRtype($valueDomain/@type)
    let $properties         := $valueDomain/*:property[@unit]
    return (
        <element id="{$id}" xmlns="http://hl7.org/fhir">
        {
            if ($dataType = ('Quantity', 'Duration', 'Count') and count($properties) = 1) then 
                (: elementdefinition-allowedUnits has a required binding to UCUM so if @unit is not a UCUM unit, then alas, no extension :)
                for $property in $properties
                let $isUcumUnit := $getf:CS_UCUMCOMMONUNITS[@unit = $property/@unit][@message = 'OK']
                return
                    if ($isUcumUnit) then
                        <extension url="http://hl7.org/fhir/StructureDefinition/elementdefinition-allowedUnits">
                            <valueCodeableConcept>
                                <coding>
                                    <system value="http://unitsofmeasure.org/" />
                                    <code value="{$property/@unit}" />
                                </coding>
                            </valueCodeableConcept>
                        </extension>
                    else ()
            else ()
        }
            <path value="{$path}"/>
        {
            (: http://hl7.org/fhir/R4/structuredefinition.html#invs sdf-9 disallows this on base :)
            if (contains($path, '.')) then 
                for $assoc in $assocs[@conceptId=$concept/@id][@code][empty(@expirationDate)] | $assocs[@conceptId=$originalConcept/@id][@code][empty(@expirationDate)]
                return
                    <code>
                        <system value="{utillib:getCanonicalUriForOID('CodeSystem', $assoc/@codeSystem, $assoc/@codeSystemVersion, $prefix, $setlib:strKeyFHIRR4)}"/>
                        <code value="{$assoc/@code}"/>
                    {
                        if ($assoc/@displayName) then
                            <display value="{$assoc/@displayName}"/>
                        else ()
                    }
                    </code>
            else ()
        }
            <short value="{replace($name, '(^\s+)|(\s+$)', '')}">
            {
                for $node in $originalConcept/*:name[not(@language = $language)][.//text()[string-length()>0]]
                return
                    <extension url="http://hl7.org/fhir/StructureDefinition/translation">
                        <extension url="lang">
                            <valueCode value="{$node/@language}"/>
                        </extension>
                        <extension url="content">
                            <valueMarkdown value="{$node}"/>
                        </extension>
                    </extension>
            }
            </short>
        {
            if ($originalConcept[*:desc[@language = $language][node()]]) then 
                (: KH 20210212: was before <definition value="{utillib:serializeNode($originalConcept/*:desc[@language = $language][1])/text()}" but did not strip HTML :)
                (: AH 20210212: was <definition value="{adfhir:stripHTMLtags($originalConcept/*:desc[@language = $language][1])}">:)
                <definition value="{markdown:html2markdown(utillib:parseNode(($originalConcept/*:desc[@language = $language])[1])/node())}">
                {
                    for $node in $originalConcept/*:desc[not(@language = $language)][.//text()[string-length()>0]]
                    return
                        <extension url="http://hl7.org/fhir/StructureDefinition/translation">
                            <extension url="lang">
                                <valueCode value="{$node/@language}"/>
                            </extension>
                            <extension url="content">
                                <valueMarkdown value="{markdown:html2markdown(utillib:parseNode($node)/node())}"/>
                            </extension>
                        </extension>
                }
                </definition>
            else (
                (: The following will be invalid in the StructureDefinition, but shows the value in the dataset is empty. :)
                <definition value="-"/>
            )
            ,
            let $unitComment                := 
                if ($dataType = ('Quantity', 'Duration', 'Count') and count($properties) gt 1) then (
                    switch ($language)
                    case 'nl-NL' return <comment language="{$language}">{'Toegestane eenheden: ' || string-join($properties/@unit, ', ')}</comment>
                    case 'de-DE' return <comment language="{$language}">{'Zulässige Einheiten: ' || string-join($properties/@unit, ', ')}</comment>
                    default      return <comment language="en-US">{'Allowable units: '     || string-join($properties/@unit, ', ')}</comment>
                ) else ()
            let $unconvertableConditions    := $concept[not(*:condition/*:desc[starts-with(., $conditionText)])]/*:condition[*:desc]
            return
            if ($originalConcept[*:comment[@language = $language][node()]] | $unconvertableConditions | $unitComment) then 
                <comment value="{
                    replace(string-join((
                        data($unitComment)
                        , 
                        if ($originalConcept/*:comment[@language = $language]) then
                            utillib:serializeNode($originalConcept/*:comment[@language = $language][1])/text()
                        else ()
                        , 
                        string-join(
                            for $cond in $unconvertableConditions
                            let $conf   := if ($cond[@isMandatory = 'true']) then 'M' else $cond/@conformance
                            let $desc   := if ($cond/*:desc[@language = $language]) then $cond/*:desc[@language = $language][1] else $cond/*:desc[1]
                            return (
                                concat('* ', utillib:getMinimumMultiplicity($cond), '..', utillib:getMaximumMultiplicity($cond), $conf, ' ', utillib:serializeNode($desc)/text())
                            )
                        , '&#xA;')
                    ), '&#xA;'), '\s$' , '')
                }">
                {
                    for $node in $originalConcept/*:comment[@language != $language][.//text()[string-length()>0]]
                    return
                        <extension url="http://hl7.org/fhir/StructureDefinition/translation">
                            <extension url="lang">
                                <valueCode value="{$node/@language}"/>
                            </extension>
                            <extension url="content">
                                <valueMarkdown value="{markdown:html2markdown(utillib:parseNode($node)/node())}"/>
                            </extension>
                        </extension>
                }
                </comment>
            else ()
            ,
            if ($originalConcept[*:rationale[@language = $language][node()]]) then
                (: KH 20210212: was <requirements value="{utillib:serializeNode($originalConcept/*:rationale[@language = $language][1])/text()}"> :)
                (: AH 20210212: was <requirements value="{adfhir:stripHTMLtags($originalConcept/*:rationale[@language = $language][1])}">:)
                <requirements value="{markdown:html2markdown(utillib:parseNode($originalConcept/*:rationale[@language = $language][1])/node())}">
                {
                    for $node in $originalConcept/*:rationale[not(@language = $language)][.//text()[string-length()>0]]
                    return
                        <extension url="http://hl7.org/fhir/StructureDefinition/translation">
                            <extension url="lang">
                                <valueCode value="{$node/@language}"/>
                            </extension>
                            <extension url="content">
                                <valueMarkdown value="{markdown:html2markdown(utillib:parseNode($node)/node())}"/>
                            </extension>
                        </extension>
                }
                </requirements>
            else ()
            ,
            for $synonym in $originalConcept/*:synonym[text()]
            return
                <alias value="{$synonym}"/>
        }
        {
            (: KH 20240218 only transactions have cardinalities, see https://art-decor.atlassian.net/browse/AD30-1442 :)
            (:if ($isTransaction) then (
                <min value="{$minCard}"/>,
                <max value="{$maxCard}"/>
            ) else ():)
            <min value="{$minCard}"/>,
            <max value="{$maxCard}"/>
        }
            <base>
                <path value="{$path}"/>
                <min value="{$minCard}"/>
                <max value="{$maxCard}"/>
            </base>
        {
            if (empty($parentPath)) then ( (: sdf-15: The first element in a snapshot has no type :) ) else
            if ($concept[*:contains]) then
                <type>
                    <code value="Reference"/>
                    <profile value="{concat($getf:strFhirServices,'StructureDefinition/', $concept/*:contains/@ref, '--', replace($concept/*:contains/@flexibility,'[^\d]',''))}"/>
                </type>
            else
            if ($originalConcept[*:concept]) then
                <type>
                    <code value="BackboneElement"/>
                </type>
            else (
                for $valueDomain in $originalConcept/*:valueDomain
                return
                <type>
                    <code value="{adfhirsd:decorValueDomainType2FHIRtype($valueDomain/@type)}"/>
                </type>
            )
        }
        {
            let $valueDomain        := if ($originalConcept/*:valueDomain[2]) then () else $originalConcept/*:valueDomain[1]
            let $dataType           := adfhirsd:decorValueDomainType2FHIRtype($valueDomain/@type)
            let $properties         := if ($valueDomain/*:property[2]) then () else $valueDomain/*:property[1]
            
            return (
                adfhirsd:decorDefaultFixed2FHIRDefaultFixed($properties/@default, $dataType, $properties, $oidfhirmap),
                adfhirsd:decorDefaultFixed2FHIRDefaultFixed($properties/@fixed, $dataType, $properties, $oidfhirmap),
                adfhirsd:decorExample2FHIRexample($concept, $valueDomain/*:example, $dataType, $properties, $oidfhirmap)
                ,
                if ($properties[@minInclude | @maxInclude]) then (
                    adfhirsd:decorProperties2FHIRminmaxValue($properties[@minInclude | @maxInclude], $dataType)
                ) else ()
                ,
                if ($properties[@maxLength castable as xs:integer]) then (
                    <maxLength value="{max($properties[@maxLength castable as xs:integer]/xs:integer(@maxLength))}"/>
                ) else ()
            )
        }
        {
            $condition,
            $constraints
        }
        {
            (: KH 20240218 only transactions have conformance rules :)
            if ($isTransaction and ($isMandatory | $isRequired)) then 
                <mustSupport value="true"/>
            else ()
        }
        {
            if ($valueSets[2]) then () else (
                for $valueSet in $valueSets
                let $strength   := adfhirsd:decorBindingStrength2fhirBindingStrength($assocs[@valueSet = $valueSet/@id][1]/@strength)
                return
                    adfhirsd:templateVocabulary2fhirBinding($valueSet, $prefix, $strength)
            )
        }
        {   ()
            (: This is not how mappings work so deactivated until we have a better solution :)
            (:if ($originalConcept[*:source]) then (
                <mapping>
                    <identity value="{generate-id($originalConcept/*:source[1])}"/>
                    <map value="{$originalConcept/*:source[1]}"/>
                </mapping>
            ) else ():)
        }
        </element>
        ,
        if ($valueSets[2]) then (
            <element id="{$id}.coding" xmlns="http://hl7.org/fhir">
                <path value="{$path}.coding" />
                <slicing>
                    <discriminator>
                        <type value="value"/>
                        <path value="system"/>
                    </discriminator>
                    <rules value="open" />
                </slicing>
                <definition value="-"/>
                <min value="1" />
                <max value="1"/>
                <base>
                    <path value="CodeableConcept.coding"/>
                    <min value="0"/>
                    <max value="*"/>
                </base>
            </element>
            ,
            for $valueSet in $valueSets
            let $valueSetName   := $valueSet/@name
            let $strength       := adfhirsd:decorBindingStrength2fhirBindingStrength($assocs[@valueSet = $valueSet/@id][1]/@strength)
            return
            <element id="{$id}.coding:{$valueSetName}" xmlns="http://hl7.org/fhir">
                <path value="{$path}.coding"/>
                <sliceName value="{$valueSetName}"/>
            {
                if ($valueSet[*:desc[@language = $language][node()]]) then 
                    (: KH 20210212: was <definition value="{utillib:serializeNode($valueSet/*:desc[@language = $language][1])/text()}"> :)
                    (: AH 20210212: was <definition value="{adfhir:stripHTMLtags($valueSet/*:desc[@language = $language][1])}">:)
                    <definition value="{markdown:html2markdown(utillib:parseNode($valueSet/*:desc[@language = $language][1])/node())}">
                    {
                        for $node in $valueSet/*:desc[not(@language = $language)][.//text()[string-length()>0]]
                        return
                            <extension url="http://hl7.org/fhir/StructureDefinition/translation">
                                <extension url="lang">
                                    <valueCode value="{$node/@language}"/>
                                </extension>
                                <extension url="content">
                                    <valueMarkdown value="{markdown:html2markdown(utillib:parseNode($node)/node())}"/>
                                </extension>
                            </extension>
                    }
                    </definition>
                else (
                    (: The following is just to satisfy the StructureDefinition, and shows the description in the valueSet is empty. :)
                    <definition value="-"/>
                )
            }
                <min value="0"/>
                <max value="1"/>
                <base>
                    <path value="CodeableConcept.coding"/>
                    <min value="0"/>
                    <max value="*"/>
                </base>
            {
                adfhirsd:templateVocabulary2fhirBinding($valueSet, $prefix, $strength)
            }
            </element>
        ) else ()
        ,
        if ($isTransaction) then (
            for $subconcept in $concept/*:concept
            return 
                adfhirsd:convertDecorConcept2FHIRElementDefinition($subconcept, $path, $prefix, $language, $isTransaction, $oidfhirmap)
        )
        else (
            for $subconcept in $concept/*:concept
            return (
                if ($subconcept[@statusCode = $adfhirsd:inactiveStatusCodes]) then () else
                    adfhirsd:convertDecorConcept2FHIRElementDefinition($subconcept, $path, $prefix, $language, $isTransaction, $oidfhirmap)
            )
        )
    )
};

declare function adfhirsd:decorDefaultFixed2FHIRDefaultFixed($defaultFixed as attribute()?, $fhirType as xs:string?, $properties as element()*, $oidfhirmap as map(*)) as element()* {
    let $baseElementName        :=
        if (empty($defaultFixed)) then () else (
            switch (name($defaultFixed))
            case 'default' return 'defaultValue'
            case 'fixed' return 'fixed'
            default return
                error(xs:QName('adfhirsd:INTERNAL_ERROR'), concat('adfhirsd:decorDefaultFixed2FHIRDefaultFixed called attribute of type ', name($defaultFixed), ' we only support default or fixed.'))
        )
    let $elementName     := 
        if (empty($defaultFixed)) then () else (
            switch ($fhirType)
            case 'Age'              return concat($baseElementName, 'Quantity')
            case 'Distance'         return concat($baseElementName, 'Quantity')
            case 'SimpleQuantity'   return concat($baseElementName, 'Quantity')
            case 'Duration'         return concat($baseElementName, 'Quantity')
            case 'Count'            return concat($baseElementName, 'Quantity')
            case 'Quantity'         return concat($baseElementName, 'Quantity')
            default                 return concat($baseElementName,upper-case(substring($fhirType, 1, 1)), substring($fhirType, 2))
        )
    let $value  := normalize-space($defaultFixed)
    
    return
    if (string-length($value) = 0) then () else (
        switch ($fhirType)
        case 'CodeableConcept' return (
            let $concept    := $defaultFixed/ancestor::valueDomain/conceptList/concept[name = $defaultFixed]
            let $assocs     :=
                if (count($concept) = 1) then $defaultFixed/ancestor::concept[1]/terminologyAssociation[@conceptId = $concept/@id] else (
                    $defaultFixed/ancestor::concept[1]/terminologyAssociation[@displayName = $defaultFixed]
                )
            return
                element {QName('http://hl7.org/fhir',$elementName)} {
                    if (count($assocs) = 1) then
                        <coding>
                            <system value="{map:get($oidfhirmap, $assocs/@codeSystem || $assocs/@codeSystemVersion)}"/>
                            <code value="{$assocs/@code}"/>
                        {
                            if ($assocs/@displayName) then <display value="{$assocs/@displayName}"/> else ()
                        }
                        </coding>
                    else (),
                    <text value="{$value}"/>
                }
        )
        case 'Identifier' return (
            let $assocs   := $defaultFixed/ancestor::concept[1]/identifierAssociation
            return
                element {QName('http://hl7.org/fhir',$elementName)} {
                    if (count($assocs) = 1) then
                        <system value="{utillib:getCanonicalUriForOID('NamingSystem', $assocs/@ref, (), $defaultFixed/ancestor::dataset/@prefix, $setlib:strKeyFHIRR4)}"/>
                    else ()
                    ,
                    <value value="{$value}"/>
                }
        )
        case 'string' return
                element {QName('http://hl7.org/fhir',$elementName)} {
                    attribute value {$value}
                }
        case 'markdown' return
                element {QName('http://hl7.org/fhir',$elementName)} {
                    attribute value {markdown:html2markdown($defaultFixed/node())}
                }
        case 'date' return (
            let $valid  :=
                if ($value castable as xs:date) then $value else
                if (matches($value, '^\d{2}/\d{2}/\d{4}')) then (
                    (: US style mm/dd/yyyy :)
                    concat(substring($value, 7, 4), '-', substring($value, 1, 2), '-', substring($value, 4, 2))[. castable as xs:date]
                )
                else
                if (matches($value, '^\d{2}.\d{2}.\d{4}')) then (
                    (: Rest of the world style dd-mm-yyyy :)
                    concat(substring($value, 7, 4), '-', substring($value, 4, 2), '-', substring($value, 1, 2))[. castable as xs:date]
                ) else ()
                
            return
            if (string-length($valid) > 0) then (
                element {QName('http://hl7.org/fhir',$elementName)} {
                    attribute value {$valid}
                }
            ) else ()
        )
        case 'dateTime' return (
            let $valid  :=
                if ($value castable as xs:dateTime) then $value else
                if ($value castable as xs:date) then $value else
                if (matches($value, '^\d{2}/\d{2}/\d{4}')) then (
                    (: US style mm/dd/yyyy :)
                    let $d        := concat(substring($value, 7, 4), '-', substring($value, 1, 2), '-', substring($value, 4, 2))
                    let $t        := replace(normalize-space(substring($value, 11)), '[^\d:Z+\-]', '')
                    let $t        := if (matches($t, '^\d{2}(:\d{2})?$')) then substring($t || ':00:00', 1, 8) else $t
                    let $dt       := $d || 'T' || $t
                    
                    return
                    if ($dt castable as xs:dateTime) then
                        if (empty(timezone-from-time(xs:time($t)))) then
                            string(adjust-dateTime-to-timezone(xs:dateTime($dt)))
                        else (
                            $dt
                        )
                    else 
                    if ($d castable as xs:date) then $d else ()
                )
                else
                if (matches($value, '^\d{2}.\d{2}.\d{4}')) then (
                    (: Rest of the world style dd-mm-yyyy :)
                    let $d        := concat(substring($value, 7, 4), '-', substring($value, 1, 2), '-', substring($value, 4, 2))
                    let $t        := replace(normalize-space(substring($value, 11)), '[^\d:Z+\-]', '')
                    let $t        := if (matches($t, '^\d{2}(:\d{2})?$')) then substring($t || ':00:00', 1, 8) else $t
                    let $dt       := $d || 'T' || $t
                    
                    return
                    if ($dt castable as xs:dateTime) then
                        if (empty(timezone-from-time(xs:time($t)))) then
                            string(adjust-dateTime-to-timezone(xs:dateTime($dt)))
                        else (
                            $dt
                        )
                    else 
                    if ($d castable as xs:date) then $d else ()
                ) else ()
            return
            if (string-length($valid) = 0) then () else (
                element {QName('http://hl7.org/fhir',$elementName)} {
                    attribute value {$valid}
                }
            )
        )
        case 'time' return (
            let $valid  := if ($value castable as xs:time) then $value else ()
            return
            if (string-length($valid) = 0) then () else (
                    element {QName('http://hl7.org/fhir',$elementName)} {
                        attribute value {$valid}
                    }
            )
        )
        case 'decimal' return (
            let $value  := if ($value castable as xs:decimal) then $value else replace($value, ',', '.')
            return
            if ($value castable as xs:decimal) then (
                    element {QName('http://hl7.org/fhir',$elementName)} {
                        <value value="{$value}"/>
                    }
            ) else ()
        )
        case 'boolean' return (
            let $value  :=
                if ($value castable as xs:boolean) then xs:boolean($value) else 
                if (lower-case($value) = ('yes','ja','oui','si')) then true() else
                if (lower-case($value) = ('no','nee','non','nein')) then false() else (true())
            return
                element {QName('http://hl7.org/fhir',$elementName)} {
                    attribute value {$value}
                }
        )
        case 'base64Binary' return ()
        default return (
            if ($fhirType = ('Age','Count','Duration','Quantity','SimpleQuantity')) then (
                let $valuepart  := replace($value,'^([\d,\.]+).*','$1')
                (: fix decimals with comma (non US) if possible/necessary :)
                let $valuepart  := 
                    if ($valuepart castable as xs:decimal) then 
                        $valuepart 
                    else
                    if (replace($valuepart, ',', '.') castable as xs:decimal) then 
                        replace($valuepart, ',', '.')
                    else (
                        (: cannot be fixed. someone needs to fix this in the dataset example :)
                        $valuepart
                    )
                (: unit is required :)
                let $unitpart   := (normalize-space(substring-after($value, $valuepart)), $properties/@unit, '1')[string-length() > 0][1]
                return
                if ($valuepart castable as xs:decimal) then (
                        element {QName('http://hl7.org/fhir',$elementName)} {
                            <value value="{$valuepart}"/>,
                            if (empty($unitpart) or $unitpart = '1') then () else <unit value="{$unitpart}"/>,
                            if ($unitpart = '1' or $adfhirsd:ucum[@unit = $unitpart]) then (
                                <system value="http://unitsofmeasure.org"/>,
                                <code value="{$unitpart}"/>
                            ) else ()
                        }
                ) else ()
            )
            else ()
        )
    )
};

declare function adfhirsd:decorExample2FHIRexample($concept as element(concept), $examples as element(example)*, $fhirType as xs:string?, $properties as element()*, $oidfhirmap as map(*)) as element()* {
    let $exampleElementName     := 
        switch ($fhirType)
        case 'Age'              return 'valueQuantity'
        case 'Distance'         return 'valueQuantity'
        case 'SimpleQuantity'   return 'valueQuantity'
        case 'Duration'         return 'valueQuantity'
        case 'Count'            return 'valueQuantity'
        case 'Quantity'         return 'valueQuantity'
        default                 return concat('value',upper-case(substring($fhirType, 1, 1)), substring($fhirType, 2))
    
    for $example at $i in $examples[not(@type = 'error')]
    let $value  := normalize-space(string-join($example//text(), ''))
    return
        switch ($fhirType)
        case 'CodeableConcept' return (
            let $exampleConcept     := $example/ancestor::valueDomain/conceptList/concept[name = $value]
            let $assocs             :=
                if (count($concept) = 1) then 
                    $concept/terminologyAssociation[@conceptId = $concept/@id] 
                else (
                    $concept/terminologyAssociation[@displayName = $value]
                )
            let $assocs             := 
                if ($assocs) then $assocs else (
                    $concept/valueSet/conceptList/*[@displayName = $value]
                )
            return
            <example xmlns="http://hl7.org/fhir">
                <label value="example {$i}"/>
            {
                element {QName('http://hl7.org/fhir',$exampleElementName)} {
                    for $ass in $assocs[@code][@codeSystem]
                    return
                        <coding>
                            <system value="{map:get($oidfhirmap, $ass/@codeSystem || $ass/@codeSystemVersion)}"/>
                            <code value="{$ass/@code}"/>
                        {
                            if ($ass/@displayName) then <display value="{$ass/@displayName}"/> else ()
                        }
                        </coding>
                    ,
                    <text value="{$value}"/>
                }
            }
            </example>
        )
        case 'Identifier' return (
            let $assocs   := $example/ancestor::concept[1]/identifierAssociation
            return
            <example xmlns="http://hl7.org/fhir">
                <label value="example {$i}"/>
            {
                element {QName('http://hl7.org/fhir',$exampleElementName)} {
                    if (count($assocs) = 1) then
                        <system value="{utillib:getCanonicalUriForOID('NamingSystem', $assocs/@ref, (), $example/ancestor::dataset/@prefix, $setlib:strKeyFHIRR4)}"/>
                    else ()
                    ,
                    <value value="{$value}"/>
                }
            }
            </example>
        )
        case 'string' return
            <example xmlns="http://hl7.org/fhir">
                <label value="example {$i}"/>
            {
                element {QName('http://hl7.org/fhir',$exampleElementName)} {
                    attribute value {$value}
                }
            }
            </example>
        case 'markdown' return
            <example xmlns="http://hl7.org/fhir">
                <label value="example {$i}"/>
            {
                element {QName('http://hl7.org/fhir',$exampleElementName)} {
                    attribute value {markdown:html2markdown($example/node())}
                }
            }
            </example>
        case 'date' return (
            let $valid  :=
                if ($value castable as xs:date) then $value else
                if (matches($value, '^\d{2}/\d{2}/\d{4}')) then (
                    (: US style mm/dd/yyyy :)
                    concat(substring($value, 7, 4), '-', substring($value, 1, 2), '-', substring($value, 4, 2))[. castable as xs:date]
                )
                else
                if (matches($value, '^\d{2}.\d{2}.\d{4}')) then (
                    (: Rest of the world style dd-mm-yyyy :)
                    concat(substring($value, 7, 4), '-', substring($value, 4, 2), '-', substring($value, 1, 2))[. castable as xs:date]
                ) else ()
                
            return
            if (string-length($valid) > 0) then (
                <example xmlns="http://hl7.org/fhir">
                    <label value="example {$i}"/>
                {
                    element {QName('http://hl7.org/fhir',$exampleElementName)} {
                        attribute value {$valid}
                    }
                }
                </example>
            ) else ()
        )
        case 'dateTime' return (
            let $valid  :=
                if ($value castable as xs:dateTime) then $value else
                if ($value castable as xs:date) then $value else
                if (matches($value, '^\d{2}/\d{2}/\d{4}')) then (
                    (: US style mm/dd/yyyy :)
                    let $d        := concat(substring($value, 7, 4), '-', substring($value, 1, 2), '-', substring($value, 4, 2))
                    let $t        := replace(normalize-space(substring($value, 11)), '[^\d:Z+\-]', '')
                    let $t        := if (matches($t, '^\d{2}(:\d{2})?$')) then substring($t || ':00:00', 1, 8) else $t
                    let $dt       := $d || 'T' || $t
                    
                    return
                    if ($dt castable as xs:dateTime) then
                        if (empty(timezone-from-time(xs:time($t)))) then
                            string(adjust-dateTime-to-timezone(xs:dateTime($dt)))
                        else (
                            $dt
                        )
                    else 
                    if ($d castable as xs:date) then $d else ()
                )
                else
                if (matches($value, '^\d{2}.\d{2}.\d{4}')) then (
                    (: Rest of the world style dd-mm-yyyy :)
                    let $d        := concat(substring($value, 7, 4), '-', substring($value, 1, 2), '-', substring($value, 4, 2))
                    let $t        := replace(normalize-space(substring($value, 11)), '[^\d:Z+\-]', '')
                    let $t        := if (matches($t, '^\d{2}(:\d{2})?$')) then substring($t || ':00:00', 1, 8) else $t
                    let $dt       := $d || 'T' || $t
                    
                    return
                    if ($dt castable as xs:dateTime) then
                        if (empty(timezone-from-time(xs:time($t)))) then
                            string(adjust-dateTime-to-timezone(xs:dateTime($dt)))
                        else (
                            $dt
                        )
                    else 
                    if ($d castable as xs:date) then $d else ()
                ) else ()
            return
            if (string-length($valid) = 0) then () else (
                <example xmlns="http://hl7.org/fhir">
                    <label value="example {$i}"/>
                {
                    element {QName('http://hl7.org/fhir',$exampleElementName)} {
                        attribute value {$valid}
                    }
                }
                </example>
            )
        )
        case 'time' return (
            let $valid  := if ($value castable as xs:time) then $value else ()
            return
            if (string-length($valid) = 0) then () else (
                <example xmlns="http://hl7.org/fhir">
                    <label value="example {$i}"/>
                {
                    element {QName('http://hl7.org/fhir',$exampleElementName)} {
                        attribute value {$valid}
                    }
                }
                </example>
            )
        )
        case 'decimal' return (
            let $value  := if ($value castable as xs:decimal) then $value else replace($value, ',', '.')
            return
            if ($value castable as xs:decimal) then (
                <example xmlns="http://hl7.org/fhir">
                    <label value="example {$i}"/>
                {
                    element {QName('http://hl7.org/fhir',$exampleElementName)} {
                        <value value="{$value}"/>
                    }
                }
                </example>
            ) else ()
        )
        case 'boolean' return (
            let $value  :=
                if ($value castable as xs:boolean) then xs:boolean($value) else 
                if (lower-case($value) = ('yes','ja','oui','si')) then true() else
                if (lower-case($value) = ('no','nee','non','nein')) then false() else (true())
            return
            <example xmlns="http://hl7.org/fhir">
                <label value="example {$i}"/>
            {
                element {QName('http://hl7.org/fhir',$exampleElementName)} {
                    attribute value {$value}
                }
            }
            </example>
        )
        case 'base64Binary' return ()
        default return (
            if ($fhirType = ('Age','Count','Duration','Quantity','SimpleQuantity')) then (
                let $valuepart  := replace($value,'^([\d,\.]+).*','$1')
                (: fix decimals with comma (non US) if possible/necessary :)
                let $valuepart  := 
                    if ($valuepart castable as xs:decimal) then 
                        $valuepart 
                    else
                    if (replace($valuepart, ',', '.') castable as xs:decimal) then 
                        replace($valuepart, ',', '.')
                    else (
                        (: cannot be fixed. someone needs to fix this in the dataset example :)
                        $valuepart
                    )
                (: unit is required :)
                let $unitpart   := (normalize-space(substring-after($value, $valuepart)), $properties/@unit, '1')[string-length() > 0][1]
                return
                if ($valuepart castable as xs:decimal) then (
                    <example xmlns="http://hl7.org/fhir">
                        <label value="example {$i} of {$fhirType}"/>
                    {
                        element {QName('http://hl7.org/fhir',$exampleElementName)} {
                            <value value="{$valuepart}"/>,
                            if (empty($unitpart) or $unitpart = '1') then () else <unit value="{$unitpart}"/>,
                            if ($unitpart = '1' or $adfhirsd:ucum[@unit = $unitpart]) then (
                                <system value="http://unitsofmeasure.org"/>,
                                <code value="{$unitpart}"/>
                            ) else ()
                        }
                    }
                    </example>
                ) else ()
            )
            else ()
        )
};

declare function adfhirsd:decorValueDomainType2FHIRtype($decorType as item()?) as xs:string {
    switch ($decorType)
    case 'count'        return 'Count'      (:could have chosen integer of decimal. not sure why to pick what :)
    case 'code'         return 'CodeableConcept'
    case 'ordinal'      return 'CodeableConcept'
    case 'identifier'   return 'Identifier'
    case 'string'       return 'string'
    case 'text'         return 'markdown'
    case 'date'         return 'date'       (:need work with properties:)
    case 'datetime'     return 'dateTime'   (:need work with properties:)
    case 'time'         return 'time'       (:need work with properties:)
    case 'complex'      return 'string'     (:this is a problem. this is lazy dataset behavior that is unimplementable:)
    case 'quantity'     return 'Quantity'
    case 'duration'     return 'Duration'
    case 'boolean'      return 'boolean'
    case 'blob'         return 'base64Binary'
    case 'decimal'      return 'decimal'
    default             return 'string'
};

declare function adfhirsd:decorProperties2FHIRminmaxValue($properties as item(), $fhirType as xs:string?) as element()* {
    let $minValue               := if (normalize-space($properties/@minInclude) castable as xs:decimal) then normalize-space($properties/@minInclude) else ()
    let $maxValue               := if (normalize-space($properties/@maxInclude) castable as xs:decimal) then normalize-space($properties/@maxInclude) else ()
    let $minElementName         := 
        switch ($fhirType)
        case 'Age'              return 'minValueQuantity'
        case 'Distance'         return 'minValueQuantity'
        case 'SimpleQuantity'   return 'minValueQuantity'
        case 'Duration'         return 'minValueQuantity'
        case 'Count'            return 'minValueQuantity'
        case 'Quantity'         return 'minValueQuantity'
        default                 return concat('minValue',upper-case(substring($fhirType, 1, 1)), substring($fhirType, 2))
    let $maxElementName         := 
        switch ($fhirType)
        case 'Age'              return 'maxValueQuantity'
        case 'Distance'         return 'maxValueQuantity'
        case 'SimpleQuantity'   return 'maxValueQuantity'
        case 'Duration'         return 'maxValueQuantity'
        case 'Count'            return 'maxValueQuantity'
        case 'Quantity'         return 'maxValueQuantity'
        default                 return concat('maxValue',upper-case(substring($fhirType, 1, 1)), substring($fhirType, 2))
    
    return
        if ($fhirType = 'Count') then (
            if (empty($minValue)) then () else (
                element {QName('http://hl7.org/fhir',$minElementName)} {
                    <value value="{$minValue}"/>
                }
            )
            ,
            if (empty($maxValue)) then () else (
                element {QName('http://hl7.org/fhir',$maxElementName)} {
                    <value value="{$maxValue}"/>
                }
            )
        ) else
        if ($fhirType = ('Age','Duration','Quantity','SimpleQuantity')) then (
            let $unit   :=  ($properties/@unit)[1]
            
            return (
                if (empty($minValue)) then () else (
                    element {QName('http://hl7.org/fhir',$minElementName)} {
                        <value value="{$minValue}"/>,
                        if (empty($unit)) then () else <unit value="{$unit}"/>
                    }
                )
                ,
                if (empty($maxValue)) then () else (
                    element {QName('http://hl7.org/fhir',$maxElementName)} {
                        <value value="{$maxValue}"/>,
                        if (empty($unit)) then () else <unit value="{$unit}"/>
                    }
                )
            )
        ) else
        if ($fhirType = 'decimal') then (
            if (empty($minValue)) then () else (
                element {QName('http://hl7.org/fhir',$minElementName)} {
                    attribute value {$minValue}
                }
            )
            ,
            if (empty($maxValue)) then () else (
                element {QName('http://hl7.org/fhir',$maxElementName)} {
                    attribute value {$maxValue}
                }
            )
        )
        else ()
};

(:  Legal in FHIR:          draft | active | retired
    Legal in Template ITS:  draft | active | retired | new | rejected | cancelled | pending | review
:)
declare function adfhirsd:decorStatus2fhirStatus($status as xs:string?) as xs:string? {
    switch ($status)
    case 'new'          return 'draft'
    case 'draft'        return 'draft'
    case 'pending'      return 'draft'
    case 'active'       return 'active'
    case 'final'        return 'active' (: not technically a template status, but an item status :)
    case 'review'       return 'draft'
    case 'cancelled'    return 'retired'
    case 'rejected'     return 'retired'
    case 'deprecated'   return 'retired'
    default             return 'draft'
};

declare function adfhirsd:fhirStatus2decorStatus($status as xs:string?) as xs:string* {
    switch ($status)
    case 'draft'        return 'draft'
    case 'active'       return 'final'
    case 'retired'      return 'deprecated'
    default             return ()
};

declare function adfhirsd:decorBindingStrength2fhirBindingStrength($strength as xs:string?) as xs:string {
    switch ($strength)
    case 'CNE'          return 'required'
    case 'CWE'          return 'extensible'
    case 'required'     return 'required'
    case 'extensible'   return 'extensible'
    case 'preferred'    return 'preferred'
    case 'example'      return 'example'
    default             return 'required'
};
declare function adfhirsd:fhirBindingStrength2decorBindingStrength($strength as xs:string?) as xs:string {
    switch ($strength)
    case 'required'     return 'required'
    case 'extensible'   return 'extensible'
    case 'preferred'    return 'preferred'
    case 'example'      return 'example'
    default             return 'required'
};

(:~ Produces a FHIR Bundle containing StructureDefinitions that express HL7 Version 3 artifacts. The StructureDefinition expression 
:   is (sort of) on par with what Template ITS as used in ART-DECOR can do. This is a experimental capability of the FHIR StructureDefinition
:
:   Example call:
:   let $projectPrefix  := if (request:exists()) then request:get-parameter('prefix','demo1-') else ()
:   let $id             := if (request:exists()) then request:get-parameter('id',())[string-length()>0][1] else ()
:   let $effectiveDate  := if (request:exists()) then request:get-parameter('effectiveDate',())[string-length()>0][1] else ()

:   let $templates      := 
:       $setlib:colDecorData//template[@statusCode=('draft','active','retired','pending','review')][ancestor::decor/project/@prefix=$projectPrefix]
:   
:   return
        adfhirsd:convertDecorTemplate2FHIRStructureDefinition($templates)
:)
declare function adfhirsd:convertDecorTemplate2FHIRStructureDefinition($templates as element(template)*) as element(f:StructureDefinition)* {

    let $comment    :=
        comment {'This is an experimental service for conversion of compliant Template ITS templates to FHIR 
        StructureDefinitions. You can current do one template at a time. Parameters:
        id              - required. Template id
        effectiveDate   - optional. Template effectiveDate (yyyy-mm-ddThh:mm:ss). Does latest version if omitted
        
        Handles: desc | constraint | element | assert | include | choice | vocabulary | property
        
        TODO: slicing, multiple value set bindings, inline value sets (repetitions of vocabulary with code/codeSystem), 
              languages (when multiple desc | constraints exist), other namespaced elements/attributes 
              (extension: http://hl7.org/fhir/StructureDefinition/elementdefinition-namespace with valueUri)
              
        Part of the TODOs need resolution in Template ITS and parts need resolution in FHIR. These are brought forward there.
        '}

    (:<Bundle xmlns="http://hl7.org/fhir">
    {
        $comment
    }
        <id value="{util:uuid()}"/>
        <type value="collection"/>
    {
        for $t in $templates[*:classification/@format='hl7v3xml1'] | $templates[not(*:classification/@format)]
        let $projectPrefix  := $t/ancestor::*:decor/*:project/@prefix
        let $template       := if ($t[@ref]) then (templ:getTemplateById($t/@ref, 'dynamic', $projectPrefix, ())//*:template/*:template[@id]) else $t
        let $tmid           := $template/@id
        let $tmed           := $template/@effectiveDate
        let $fhirId         := concat($tmid,'--',replace($tmed,'[^\d]',''))
        return
        <entry>
            <fullUrl value="{concat($getf:strFhirServices,'StructureDefinition/', $fhirId)}"/>
            <resource>
            </resource>
            <!--{$template}-->
        </entry>
    }
    </Bundle>:)
    for $t in $templates[*:classification/@format='hl7v3xml1'] | $templates[not(*:classification/@format)]
    let $decor            := $t/ancestor::*:decor
    let $project          := $decor/*:project
    let $projectPrefix    := $project/@prefix
    let $template         := if ($t[@ref]) then (templ:getTemplateById($t/@ref, 'dynamic', $projectPrefix, ())//*:template/*:template[@id]) else $t
    let $tmid             := $template/@id
    let $tmed             := $template/@effectiveDate
    let $fhirId           := concat($tmid,'--',replace($tmed,'[^\d]',''))
    (: create a list of all namespaces including the deault namespace and store it for later use :)
    let $defaulnsprefix   := substring-before($project/defaultElementNamespace/@ns,':')
    let $namespaces       := 
        <namespaces>
        {
            for $ns-prefix in in-scope-prefixes($decor)
            let $ns-uri := namespace-uri-for-prefix($ns-prefix, $decor)
            return
                <ns ns="{$ns-prefix}" uri="{$ns-uri}">
                {
                    if ($ns-prefix = $defaulnsprefix) then attribute { 'default' } { 'true' } else ()
                }
                </ns>
        }
        </namespaces>
    return
    <StructureDefinition xmlns="http://hl7.org/fhir">
    {
        $comment
    }
        <id value="{$fhirId}"/>
        <meta>
            <profile value="http://hl7.org/fhir/{$getf:strFhirVersionShort}/StructureDefinition/StructureDefinition"/>
        </meta>
        <extension url="http://hl7.org/fhir/StructureDefinition/elementdefinition-namespace">
            <valueUri value="urn:hl7-org:v3"/>
        </extension>
    {
        (: Any object in ART-DECOR is effective from effectiveDate to expirationDate :)
        if ($template[@effectiveDate castable as xs:dateTime] | $template[@expirationDate castable as xs:dateTime]) then (
            <extension url="http://hl7.org/fhir/StructureDefinition/resource-effectivePeriod">
                <valuePeriod>
                {
                    if ($template[@effectiveDate castable as xs:dateTime]) then 
                        <start value="{adjust-dateTime-to-timezone(xs:dateTime($template/@effectiveDate))}"/>
                    else (),
                    if ($template[@expirationDate castable as xs:dateTime]) then
                        <end value="{adjust-dateTime-to-timezone(xs:dateTime($template/@expirationDate))}"/>
                    else ()
                }
                </valuePeriod>
            </extension>
        ) else ()
    }
        <url value="{if ($t[@canonicalUri]) then $t/@canonicalUri else concat($getf:strFhirServices,'StructureDefinition/', $fhirId)}"/>
        <identifier>
            <use value="official"/>
            <system value="urn:ietf:rfc:3986"/>
            <value value="urn:oid:{$tmid}"/>
        </identifier>
        <version value="{let $semver := adfhir:getSemverString($t/@versionLabel) return if (empty($semver)) then $tmed else $semver}"/>
        <name value="{adfhir:validResourceName($template/@name)}"/>
        <title value="{$template/@displayName}"/>
        <status value="{adfhirsd:decorStatus2fhirStatus($template/@statusCode)}"/>
        <experimental value="false"/>
        <date value="{$template/@effectiveDate}Z"/>
    {
        let $copyright  := ($t/ancestor::*:decor/*:project/*:copyright[empty(@type)] | $t/ancestor::*:decor/*:project/*:copyright[@type = 'author'])[1]
        return
            if ($copyright) then (
                <publisher value="{$copyright/@by}"/>
                ,
                for $publisher in $copyright[*:addrLine/@type = ('phone', 'email', 'fax', 'uri')]
                return
                <contact>
                    <name value="{$publisher/@name}"/>
                {
                    for $addrLine in $publisher/*:addrLine[@type = ('phone', 'email', 'fax', 'uri')]
                    let $system     := if ($addrLine/@type = 'uri') then 'other' else $addrLine/@type
                    return
                    <telecom>
                        <system value="{$system}"/>
                        <value value="{$addrLine}"/>
                    </telecom>
                }
                </contact>
            )
            else (
                <publisher value="ART-DECOR Expert Group"/>
            )
    }
        {
            (:
            if ($template[*:desc//text() | *:constraint//text()]) then
                <description value="{$template/*:desc[1]/node() | $template/*:constraint//text()}"/>
            else ()
            :)
            let $description := markdown:html2markdown($template/*:desc[1]/node())
            return
            if ($description) then
                <description value="{$description}{$template/*:constraint//text()}">
                {
            (:
                    for $node in $template[*:desc]/../*:desc[not(@language = $language)][.//text()[string-length()>0]]
                    return
                        <extension url="http://hl7.org/fhir/StructureDefinition/translation">
                            <extension url="lang">
                                <valueCode value="{$node/@language}"/>
                            </extension>
                            <extension url="content">
                                <valueMarkdown value="{markdown:html2markdown(utillib:parseNode($node)/node())}"/>
                            </extension>
                        </extension>
                    :)
                }
                </description>
                else ()
        }
        <kind value="logical"/>
        <abstract value="false"/>
        {
            let $tel1name := if (string-length($template/*:element[1]/@name) > 0) then adfhirsd:templateCleanName($template/*:element[1]/@name) else 'UNKNOWN'
            let $btype := 'http://hl7.org/cda/stds/core/StructureDefinition/' || adfhirsd:template1Element2fhirBaseType($tel1name)
            let $bdef := $btype
            return (
                <type value="{$btype}"/>,
                <baseDefinition value="{$bdef}"/>
            )
        }
        <derivation value="constraint"/>
        <differential>
        {
            adfhirsd:template2fhirElement($template, (), false(), $projectPrefix, (), (), $namespaces)
        }
        </differential>
    </StructureDefinition>
};

declare function adfhirsd:determineSlicing($item as element()?, $cleansiblings as element()*) as element()? {
    let $elmname := name($item)
    let $cleanitemname := if ($elmname = 'element') then adfhirsd:templateCleanName($item/@name) else ''
    let $xkey := generate-id($item)
    let $firstkey := generate-id($cleansiblings[1])
    let $slice :=
         if ($cleanitemname = 'templateId' and $item/@minimumMultiplicity = '1' and $item/@maximumMultiplicity = '1' and $item/@isMandatory='true' and $item/attribute[@name='root'][@value])
         then
            if ($xkey = $firstkey) 
            then <slicing pos="1" name="templateId" type="path" discriminators="root extension"/> 
            else 
                for $ci at $pos in $cleansiblings
                return
                    if (generate-id($ci) = $xkey)
                    then <sliceparticle pos="{$pos}" name="templateId" type="path"/>
                    else ()
         else
            if ($cleanitemname = 'component' and $item[@contains])
            then
               if ($xkey = $firstkey) 
               then <slicing pos="1" name="component" type="profile" discriminators="profile"/> 
               else 
                   for $ci at $pos in $cleansiblings
                   return
                       if (generate-id($ci) = $xkey)
                       then <sliceparticle pos="{$pos}" name="component" type="profile"/>
                       else ()
         else
            if ($cleanitemname = 'entry' and $item[@contains])
            then
               if ($xkey = $firstkey) 
               then <slicing pos="1" name="entry" type="profile" discriminators="profile"/> 
               else 
                   for $ci at $pos in $cleansiblings
                   return
                       if (generate-id($ci) = $xkey)
                       then <sliceparticle pos="{$pos}" name="entry" type="profile"/>
                       else ()
         else
            if ($cleanitemname = 'qualifier')
            then
               if ($xkey = $firstkey) 
               then <slicing pos="1" name="qualifier" type="path" discriminators="name"/> 
               else 
                   for $ci at $pos in $cleansiblings
                   return
                       if (generate-id($ci) = $xkey)
                       then <sliceparticle pos="{$pos}" name="qualifier" type="path"/>
                       else ()
         else
            if ($cleanitemname = 'translation')
            then
               if ($xkey = $firstkey) 
               then <slicing pos="1" name="translation" type="path" discriminators="code"/> 
               else 
                   for $ci at $pos in $cleansiblings
                   return
                       if (generate-id($ci) = $xkey)
                       then <sliceparticle pos="{$pos}" name="translation" type="path"/>
                       else ()
         else
            if ($cleanitemname = 'participant')
            then
               if ($xkey = $firstkey) 
               then <slicing pos="1" name="participant" type="path" discriminators="typeCode"/> 
               else 
                   for $ci at $pos in $cleansiblings
                   return
                       if (generate-id($ci) = $xkey)
                       then <sliceparticle pos="{$pos}" name="participant" type="path"/>
                       else ()
         else
            if ($cleanitemname = 'entryRelationship' and $item[@contains])
            then
               if ($xkey = $firstkey) 
               then <slicing pos="1" name="entryRelationship" type="profile" discriminators="profile"/> 
               else 
                   for $ci at $pos in $cleansiblings
                   return
                       if (generate-id($ci) = $xkey)
                       then <sliceparticle pos="{$pos}" name="entryRelationship" type="profile"/>
                       else ()
        else ()
    return
        $slice
    
};

declare function adfhirsd:template1Element2fhirBaseType($rname as xs:string?) as xs:string? {
    let $noprefix       :=
            if (contains($rname,':')) then (
                substring-after($rname,':')
            ) else ($rname)
    let $nopredicate    := tokenize($noprefix,'\[')[1]
    return
        switch ($nopredicate)
        case 'act'                     return 'Act'
        case 'author'                  return 'Author'
        case 'ClinicalDocument'        return 'ClinicalDocument'
        case 'code'                    return 'Code'
        case 'custodian'               return 'Custodian'
        case 'documentationOf'         return 'DocumentationOf'
        case 'encounter'               return 'Encounter'
        case 'id'                      return 'Id'
        case 'legalAuthenticator'      return 'LegalAuthenticator'
        case 'manufacturedMaterial'    return 'ManufacturedMaterial'
        case 'manufacturedProduct'     return 'ManufacturedProduct'
        case 'name'                    return 'Name'
        case 'observation'             return 'Observation'
        case 'observationMedia'        return 'ObservationMedia'
        case 'organizer'               return 'Organizer'
        case 'participant'             return 'Participant'
        case 'procedure'               return 'Procedure'
        case 'recordTarget'            return 'RecordTarget'
        case 'relatedDocument'         return 'RelatedDocument'
        case 'section'                 return 'Section'
        case 'streetAddressLine'       return 'StreetAddressLine'
        case 'substanceAdministration' return 'SubstanceAdministration'
        case 'supply'                  return 'Supply'
        case 'templateId'              return 'TemplateId'
        default                        return 'unknown'
};

declare function adfhirsd:templateDatatype2fhirDatatype($itemType as xs:string?, $itemName as xs:string?, $dataType as item()?, $parentDecorDatatype as xs:string?, $dotpath as xs:string?) as xs:string? {
    if (string-length($dataType)=0 and string-length($parentDecorDatatype)=0) then (
        let $cdaElementParts    := ('.authenticator', '.author', '.authorization', '.component', '.componentOf', 
                                    '.consumable', '.custodian', '.dataEnterer', '.documentationOf', 
                                    '.encounterParticipant', '.entry', '.entryRelationship', '.informant', 
                                    '.informationRecipient', '.inFulfillmentOf', '.legalAuthenticator', '.location', 
                                    '.participant', '.performer', '.precondition', '.product', '.recordTarget', 
                                    '.reference', '.referenceRange', '.relatedDocument', '.responsibleParty', 
                                    '.specimen', '.subject')
        let $finalPart          := tokenize($dotpath, '\.')[last()]
        let $finalDotPart       := concat('.', $finalPart)
        
        return
            if ($cdaElementParts[. = $finalDotPart]) then 
                'Element'
            else
            if ($itemName = 'templateId') then
                $adfhirsd:sdprefix || 'II'
            else
            if ($itemName = 'realmCode') then
                $adfhirsd:sdprefix || 'CS'
            else
            if ($itemName = 'typeId') then
                $adfhirsd:sdprefix || 'II'
            else
            if ($itemName = 'manufacturedMaterial') then
                $adfhirsd:sdprefix || 'Material'
            else
            if ($itemName = 'playingDevice') then
                $adfhirsd:sdprefix || 'Device'
            else
            if ($itemName = 'scopingEntity') then
                $adfhirsd:sdprefix || 'Entity'
            else
            if ($itemName = ('associatedPerson', 'assignedPerson')) then
                $adfhirsd:sdprefix || 'Person'
            else
            if ($itemName = ('receivedOrganization', 'representedOrganization', 'representedCustodianOrganization', 'scopingOrganization', 'wholeOrganization')) then
                $adfhirsd:sdprefix || 'Organization'
            else
            if ($itemName = ('asOrganizationPartOf')) then
                $adfhirsd:sdprefix || 'OrganizationPartOf'
            else (
                (:
                    <path value="ClinicalDocument.dataEnterer.assignedEntity"/>
                    <type><code value="http://hl7.org/fhir/cda/StructureDefinition/AssignedEntity"/></type>
                :)
                concat($adfhirsd:sdprefix, upper-case(substring($finalPart, 1, 1)), substring($finalPart, 2))
            )
    )
    else
    if (string-length($dataType)=0) then (
        switch ($itemName)
        case 'alignment'                    return 'code'(:'cs':)
        case 'assigningAuthorityName'       return 'string'(:'st':)
        case 'code'                         return 'code'(:'cs':)
        case 'codeSystem'                   return 'string'(:'oid':)
        case 'codeSystemName'               return 'string'(:'st':)
        case 'codeSystemVersion'            return 'string'(:'st':)
        case 'displayName'                  return 'string'(:'st':)
        case 'mediaType'                    return 'code'(:'cs':)
        case 'language'                     return 'code'(:'cs':)
        case 'compression'                  return 'code'(:'cs':)
        case 'integrityCheck'               return 'base64Binary'(:'bin':)
        case 'integrityCheckAlgorithm'      return 'code'(:'cs':)
        case 'currency'                     return 'code'(:'cs':)
        case 'displayable'                  return 'boolean'(:'bl':)
        case 'inclusive'                    return 'boolean'(:'bl':)
        case 'inverted'                     return 'boolean'(:'bn':)
        case 'partType'                     return 'code'(:'cs':)
        case 'classCode'                    return 'code'(:'cs':)
        case 'contextConductionInd'         return 'boolean'(:'bl':)
        case 'contextControlCode'           return 'code'(:'cs':)
        case 'determinerCode'               return 'code'(:'cs':)
        case 'extension'                    return 'string'(:'st':)
        case 'independentInd'               return 'boolean'(:'bl':)
        case 'institutionSpecified'         return 'boolean'(:'bl':)
        case 'inversionInd'                 return 'boolean'(:'bl':)
        case 'mediaType'                    return 'string'(:'st':)
        case 'moodCode'                     return 'code'(:'cs':)
        case 'negationInd'                  return 'boolean'(:'bl':)
        case 'nullFlavor'                   return 'code'(:'cs':)
        case 'operator'                     return 'code'(:'cs':)
        case 'qualifier'                    return 'code'(:'set_cs':)
        case 'representation'               return 'code'(:'cs':)
        case 'root'                         return 'string'(:'uid':)
        case 'typeCode'                     return 'code'(:'cs':)
        case 'unit'                         return 'code'(:'cs':)
        case 'use'                          return 'code'(:'set_cs':)
        case 'xsi:type'                     return 'code'(:'cs':)
        case 'type'                         return 'code'(:'cs':)
        case 'xsi:nil'                      return 'boolean'(:'bl':)
        case 'width'                        return $adfhirsd:sdprefix || 'PQ'
        case 'translation'                  return $adfhirsd:sdprefix || 'PQR'
        case 'low'                          return (
            switch ($parentDecorDatatype)
            case 'IVL_MO' return $adfhirsd:sdprefix || 'MO'
            case 'IVL_PQ' return $adfhirsd:sdprefix || 'PQ'
            case 'IVL_TS' return $adfhirsd:sdprefix || 'TS'
            case 'IVL_INT' return $adfhirsd:sdprefix || 'INT'
            case 'IVL_REAL' return $adfhirsd:sdprefix || 'REAL'
            default return
                error(QName('http://art-decor.org/ns/error', 'MissingDatatype'), 
                concat('Particle "low" without datatype under particle with datatype "', $parentDecorDatatype, '". Name "',$itemName,'". Path ', $dotpath))
        )
        case 'high'                         return (
            switch ($parentDecorDatatype)
            case 'IVL_MO' return $adfhirsd:sdprefix || 'MO'
            case 'IVL_PQ' return $adfhirsd:sdprefix || 'PQ'
            case 'IVL_TS' return $adfhirsd:sdprefix || 'TS'
            case 'IVL_INT' return $adfhirsd:sdprefix || 'INT'
            case 'IVL_REAL' return $adfhirsd:sdprefix || 'REAL'
            default return
                error(QName('http://art-decor.org/ns/error', 'MissingDatatype'), 
                concat('Particle "high" without datatype under particle with datatype "', $parentDecorDatatype, '". Name "',$itemName,'". Path ', $dotpath))
        )
        case 'center'                       return (
            switch ($parentDecorDatatype)
            case 'IVL_MO' return $adfhirsd:sdprefix || 'MO'
            case 'IVL_PQ' return $adfhirsd:sdprefix || 'PQ'
            case 'IVL_TS' return $adfhirsd:sdprefix || 'TS'
            case 'IVL_INT' return $adfhirsd:sdprefix || 'INT'
            case 'IVL_REAL' return $adfhirsd:sdprefix || 'REAL'
            default return
                error(QName('http://art-decor.org/ns/error', 'MissingDatatype'), 
                concat('Particle "center" without datatype under particle with datatype "', $parentDecorDatatype, '". Name "',$itemName,'". Path ', $dotpath))
        )
        case 'value'                        return (
            if ($itemType = 'element') then
                error(QName('http://art-decor.org/ns/error', 'MissingDatatype'), 
                    concat('Particle element without datatype under particle with datatype "', $parentDecorDatatype, '". Name "',$itemName,'". Path ', $dotpath))
            else
            if ($itemType = 'attribute') then
                switch ($parentDecorDatatype)
                case 'IVL_MO' return 'decimal'
                case 'MO' return 'decimal'
                case 'IVL_PQ' return 'decimal'
                case 'PQ' return 'decimal'
                case 'IVL_TS' return 'dateTime'
                case 'TS' return 'dateTime'
                case 'IVL_INT' return 'integer'
                case 'INT' return 'integer'
                case 'IVL_REAL' return 'decimal'
                case 'REAL' return 'decimal'
                case 'TEL' return 'string'
                default return
                    error(QName('http://art-decor.org/ns/error', 'MissingDatatype'), 
                    concat('Particle attribute without datatype under particle with datatype "', $parentDecorDatatype, '". Name "',$itemName,'". Path ', $dotpath))
            else ()
        )
        default return 
            error(QName('http://art-decor.org/ns/error', 'MissingDatatype'), 
                concat('Particle without datatype under particle with datatype "', $parentDecorDatatype, '". Name "',$itemName,'". Path ', $dotpath))
    )
    else (
        let $noprefix       :=
            if (contains($dataType,':')) then (
                substring-after($dataType,':')
            ) else ($dataType)
        let $noflavor       := tokenize($noprefix,'\.')[1]
        
        return
            switch ($noflavor)
            case 'bin'                  return 'base64Binary'
            case 'bl'                   return 'boolean'
            case 'bn'                   return 'boolean'
            case 'cs'                   return 'code'
            case 'int'                  return 'integer'
            case 'oid'                  return 'string'
            case 'real'                 return 'decimal'
            case 'ruid'                 return 'string'
            case 'set_cs'               return 'code'
            case 'st'                   return 'string'
            case 'ts'                   return 'dateTime'
            case 'uid'                  return 'string'
            case 'url'                  return 'url'
            case 'uuid'                 return 'string'
            case 'SD'                   return 'xhtml'
            case 'IVL_TS'               return $adfhirsd:sdprefix || 'IVL-TS'
            case 'ON'                   return $adfhirsd:sdprefix || 'ON'
            case 'PN'                   return $adfhirsd:sdprefix || 'PN'
            case 'TN'                   return $adfhirsd:sdprefix || 'EN'
            case 'ADXP'                 return $adfhirsd:sdprefix || 'ST'
            (:case 'ENXP'                 return $adfhirsd:sdprefix || 'ST':)
            default return concat($adfhirsd:sdprefix, $noflavor)
    )
};

(:~ Returns the element name defined without the namespace prefix and without predicates 
:)
declare function adfhirsd:templateCleanName($name as xs:string) as xs:string {
    let $noprefix       :=
        if (contains($name,':')) then (
            substring-after($name,':')
        ) else ($name)
    let $nopredicate    :=
        if (contains($noprefix,'[')) then (
            substring-before($noprefix,'[')
        ) else ($noprefix)
    return $nopredicate
};

declare function adfhirsd:template2fhirElement($template as element(), $previous-sliceintro as element()?, $previous-inslice as xs:boolean, $projectPrefix as xs:string, $dotpath as xs:string?, $parentDecorDatatype as xs:string?, $namespaces as element()?) as element()* {

    let $containedTemplate  := ()

    return (
    
        (: start with possible attribues :)
        for $item in templ:normalizeAttributes($template/*:attribute)
        return
            adfhirsd:templateAttribute2fhirElement($item, $previous-sliceintro, $previous-inslice, $projectPrefix, $dotpath, $parentDecorDatatype, $namespaces)
        ,
        (: process elements, includes and choices, watch out for slicing :)
        for $item in $template/*:element | $template/*:include | $template/*:choice
        let $cleanitemname := if (name($item) = 'element') then adfhirsd:templateCleanName($item/@name) else ''
        let $cleansiblings := $template/*:element[adfhirsd:templateCleanName(@name) = $cleanitemname]
        let $sliceintro := if ($previous-sliceintro) then $previous-sliceintro else adfhirsd:determineSlicing($item, $cleansiblings)
        let $inslice := if (empty($sliceintro)) then false() else true()
        return
            if ($item[local-name()='element']) then
                adfhirsd:templateElement2fhirElement($item, $sliceintro, $inslice, $projectPrefix, $dotpath, $parentDecorDatatype, $namespaces)
            else
            if ($item[local-name()='include']) then
                adfhirsd:templateInclude2fhirElement($item, $projectPrefix, $dotpath, $parentDecorDatatype, $namespaces)
            else
            if ($item[local-name()='choice']) then
                adfhirsd:templateChoice2fhirElement($item, $projectPrefix, $dotpath, $parentDecorDatatype, $namespaces)
            else
                ()
    
    )

};
(:<property minInclude="" maxInclude="" minLength="" maxLength="" currency="" fractionDigits="" unit="" value=""/>:)

declare function adfhirsd:templateAttribute2fhirElement($att as element(), $sliceintro as element()?, $inslice as xs:boolean, $projectPrefix as xs:string, $dotpath as xs:string?, $parentDecorDatatype as xs:string?, $namespaces as element()?) as element()+ {
    
    let $slicename      := adfhirsd:getSliceName($sliceintro)
    let $dotpath        := if (string-length($dotpath) = 0) then '' else concat(upper-case(substring($dotpath, 1, 1)), substring($dotpath, 2))
    
    let $foreignnamespaceprefix := if (contains($att/@name,':')) then substring-before($att/@name,':') else ''
    let $foreignnamespacesuffix := if (contains($att/@name,':')) then substring-after($att/@name,':') else ''
    let $attname                := if (string-length($foreignnamespacesuffix) > 0) then $foreignnamespacesuffix else $att/@name
    let $foreignnamespace       := $namespaces/ns[@ns=$foreignnamespaceprefix][not(@default)]
    
    return
        <element id="{concat(string-join($dotpath,'.'), if ($inslice=false()) then '.' else concat(':', $slicename, '.'), $attname)}" xmlns="http://hl7.org/fhir">
        {
            (: process possible namepsaces :)
            if (not(empty($foreignnamespace))) then
            (
                <extension url="http://hl7.org/fhir/StructureDefinition/elementdefinition-namespace">
                    <valueUri value="{$foreignnamespace/@uri}"/>
                </extension>,
                <extension url="http://hl7.org/fhir/StructureDefinition/elementdefinition-xml-name">
                     <valueString value="{$foreignnamespace/@ns}"/>
                </extension>
            ) else ()
        }
        {
            <path value="{string-join(($dotpath, $attname),'.')}"/>,
            <representation value="xmlAttr"/>
        }
        {
            if ($att[*:item]) then
                <label value="{$att/*:item/@label}"/>
            else ()
        }
        {
            if ($att[*:desc//text() | *:constraint//text()]) then
                <definition value="{$att/*:desc[1]/node() | $att/*:constraint/node()}"/>
            else ()
        }
            <min value="{utillib:getMinimumMultiplicity($att)}"/>
            <max value="{utillib:getMaximumMultiplicity($att)}"/>
            <type>
                <code value="{adfhirsd:templateDatatype2fhirDatatype(local-name($att), adfhirsd:templateCleanName($att/@name), $att/@datatype, $parentDecorDatatype, $dotpath)}"/>
            </type>
        {
            (:.. defaultValue[x]	Σ I	0..1	*	Specified value it missing from instance:)
            ()
        }
        {
            (:... meaningWhenMissing	Σ I	0..1	markdown	Implicit meaning when this element is missing:)
            ()
        }
        {
            (:... fixed[x]	Σ I	0..1	*	Value must be exactly this:)
            if ($att[@value]) then
                <fixedString value="{$att/@value}"/>
            else (),
            for $code in $att/*:vocabulary[@code]/@code
            return
                <patternString value="{$code}"/>
        }
        {
            (:... pattern[x]	Σ I	0..1	*	Value must have at least these property values:)
            ()
        }
        {
            (:... example[x]	Σ	0..1	*	Example value: [as defined for type]:)
            ()
        }
        {
            (:... minValue[x]	Σ	0..1	*	Minimum Allowed Value (for some types):)
            if ($att[*:property/@minInclude]) then
                <minValueString value="{$att/*:property/@minInclude}"/>
            else ()
        }
        {
            (:... maxValue[x]	Σ	0..1	*	Maximum Allowed Value (for some types):)
            if ($att[*:property/@maxInclude]) then
                <maxValueString value="{$att/*:property/@maxInclude}"/>
            else ()
        }
        {
            (:... maxLength	Σ	0..1	integer	Max length for strings:)
            if ($att[*:property/@maxLength]) then
                <maxLength value="{$att/*:property/@maxLength}"/>
            else ()
        }
        {
            adfhirsd:templateVocabulary2fhirBinding($att/*:vocabulary[@valueSet], $projectPrefix, adfhirsd:decorBindingStrength2fhirBindingStrength($att/@strength))
        }
        </element>
};

declare function adfhirsd:getSliceName($s as element()?) as xs:string {
    (: make slice names for maybe later use: primary, secondary, tertiary, quaternary, quinary, senary, septenary, octonary, nonary, denary :)
    let $sna := ['primary', 'secondary', 'tertiary', 'quaterary', 'quintary', 'sextary',
                 'septimary', 'octavary', 'nonary', 'decimary', 'undecimary',
                 'duodecimary', 'tridecimary', 'tetradecimary', 'pentadecimary']
    return
        if (empty($s)) 
        then ''
        else
            switch ($s/@name)
            case 'component'
            case 'entry'
                case 'entryRelationship'
                return concat($s/@name, '-', $s/@pos)
            default
                return
                    if ($s/@pos > 0 and $s/@pos <= 15)
            then array:get($sna, $s/@pos)
            else concat('slice-', $s/@pos)
};

declare function adfhirsd:templateElement2fhirElement($elm as element(), $sliceintro as element()?, $inslice as xs:boolean, $projectPrefix as xs:string, $dotpath as xs:string?, $parentDecorDatatype as xs:string?, $namespaces as element()?) as element()+ {

    (: get last element name in dot path :)
    let $lastelmname    := tokenize($dotpath, '\.')[last()]
    
    (: process elment, watch out for foreign namespaces in the original name and watch out for slicing in the target name :)
    let $cleanname      := adfhirsd:templateCleanName($elm/@name)
    let $context        := string-join(($dotpath, $cleanname),'.')
    let $context        := upper-case(substring($context, 1, 1)) || substring($context, 2)

    let $foreignnamespaceprefix := if (contains($elm/@name,':')) then substring-before($elm/@name,':') else ''
    let $foreignnamespace       := $namespaces/ns[@ns=$foreignnamespaceprefix][not(@default)]

    let $slicetop       := 
        if ($sliceintro/name()='slicing') then
            <element id="{$context}" xmlns="http://hl7.org/fhir">
                <path value="{$context}"/>
                <slicing>
                {
                    for $s in tokenize($sliceintro/@discriminators, ' ')
                    return
                        <discriminator>
                            <type value="value"/>
                            <path value="{$s}"/>
                        </discriminator>,
                    <rules value="open"/>
                }
                </slicing>
                <min value="1"/>
           </element>
         else ()
         
    let $slicename      := adfhirsd:getSliceName($sliceintro)
            
    let $containedTemplate  := if ($elm[@contains]) then (templ:getTemplateById($elm/@contains, $elm/@flexibility, $projectPrefix)//*:template/*:template[@id]) else ()
    let $containedTemplateDYN := $containedTemplate[1]
    let $containedTemplateId  := 
        if ($containedTemplateDYN/@canonicalUri) 
        then $containedTemplateDYN/@canonicalUri
        else concat('http://localhost:8877/fhir/StructureDefinition/', $containedTemplateDYN/@id, '--', replace($containedTemplateDYN/@effectiveDate,'[^\d]',''))
    let $containedTemplateConstraint :=  concat($containedTemplateDYN/@displayName, ' ', $containedTemplateDYN/@id, ' (', substring($containedTemplateDYN/@effectiveDate, 1, 10), ')')
 
    let $fhirbasetype   := if ($containedTemplateDYN/*:element[1]/@name)
        then adfhirsd:template1Element2fhirBaseType(adfhirsd:templateCleanName($containedTemplateDYN/*:element[1]/@name))
        else 'Element'
    let $fhirbasetypeLC := lower-case(substring($fhirbasetype, 1, 1)) || substring($fhirbasetype, 2)
    
    let $slicebottom    :=
        if ($inslice and $fhirbasetype != 'Element') then
            <element id="{$context}{if ($inslice=false()) then () else concat(':', $slicename, '.', $fhirbasetypeLC)}" xmlns="http://hl7.org/fhir">
            {
                <path value="{concat($context, '.', $fhirbasetypeLC)}"/>,
                <type>
                    <code value="{concat($adfhirsd:sdprefix, $fhirbasetype)}"/>
                    <profile value="{$containedTemplateId}"/>
                </type>,
                <min value="1"/>,
                <max value="1"/>
            }
            {
                if ($elm[@isMandatory='true'] | $elm[@conformance='R']) then 
                    <mustSupport value="true"/>
                else ()
            }
            </element>
        else ()

    return (
        $slicetop,
        <element id="{$context}{if ($inslice=false()) then () else concat(':', $slicename)}" xmlns="http://hl7.org/fhir">
        {
            (: process possible namepsaces :)
            if (not(empty($foreignnamespace))) then
            (
                <extension url="http://hl7.org/fhir/StructureDefinition/elementdefinition-namespace">
                    <valueUri value="{$foreignnamespace/@uri}"/>
                </extension>,
                <extension url="http://hl7.org/fhir/StructureDefinition/elementdefinition-xml-name">
                     <valueString value="{$foreignnamespace/@ns}"/>
                </extension>
            ) else ()
        }
        {
            <path value="{$context}"/>
        }
        {
            if ($elm/*:attribute[@name = 'xsi:type'][not(@isOptional = 'false')][not(@prohibited = 'true')]) then
                <representation value="typeAttr"/>
            else 
            if ($cleanname = 'value' and $lastelmname = ('observation', 'observationRange', 'criterion')) then
                <representation value="typeAttr"/>
            else
            if ($cleanname = 'effectiveTime' and $lastelmname = ('substanceAdministration', 'supply')) then
                <representation value="typeAttr"/>
            else
            if ($cleanname = 'text' and $lastelmname = 'section') then
                <representation value="cdaText"/>
            else ()
        }
        {
            if ($elm[*:item]) then
                <label value="{$elm/*:item/@label}"/>
            else ()
        }
        {
            let $definition := markdown:html2markdown($elm/*:desc[1]/node())
            let $definition := if ($elm/*:constraint/node()) then concat ($definition, markdown:html2markdown($elm/*:constraint/node())) else $definition
            return
                if (string-length($definition)>0) then
                    <definition value="{$definition}"/>
            else ()
        }
        {
             <min value="{utillib:getMinimumMultiplicity($elm)}"/>,
             <max value="{utillib:getMaximumMultiplicity($elm)}"/>
        }
        {
            if ($inslice=false()) then () else <sliceName value="{$slicename}"/>
        }
        {
            if (empty($dotpath) and not(contains($context, '.'))) then () else (
                if ($sliceintro/@type='path') then (
                    <type>
                        <code value="{adfhirsd:templateDatatype2fhirDatatype(local-name($elm), 
                            adfhirsd:templateCleanName($elm/@name), $elm/@datatype, $parentDecorDatatype, $context)}"/>
                    </type>
                ) else if ($sliceintro/@type='profile' and $sliceintro/@name=('entry', 'component', 'entryRelationship')) then (
                    <constraint>
                        <key value="{$slicename}"/>
                        <human value="{concat('contains ', $containedTemplateConstraint)}"/>
                    </constraint>
                ) else ()
                )
        }
        {
            for $assert in $elm/*:assert
            return
                <condition value="{generate-id($assert)}"/>
            ,
            for $assert in $elm/*:assert
            return
            <constraint>
                <!--extension url="http://hl7.org/fhir/StructureDefinition/structuredefinition-expression">
                    <valueString value="" />
                </extension-->
                <key value="{generate-id($assert)}" />
                <severity value="{if ($assert[@role=('hint','warning')]) then 'warning' else 'error'}" />
                <human value="{$assert/node()}"/>
                <xpath value="{$assert/@test}"/>
            </constraint>
        }
        {
            if ($elm[@isMandatory='true'] | $elm[@conformance='R']) then 
                <mustSupport value="true"/>
            else ()
        }
        {
            adfhirsd:templateVocabulary2fhirBinding($elm/*:vocabulary[@valueSet], $projectPrefix, adfhirsd:decorBindingStrength2fhirBindingStrength($elm/@strength))
        }
        </element>
        ,
        $slicebottom
        ,
        for $vocabulary in $elm/*:vocabulary[@code | @codeSystem | @displayName]/@code |
                           $elm/*:vocabulary[@code | @codeSystem | @displayName]/@codeSystem |
                           $elm/*:vocabulary[@code | @codeSystem | @displayName]/@codeSystemName |
                           $elm/*:vocabulary[@code | @codeSystem | @displayName]/@codeSystemVersion |
                           $elm/*:vocabulary[@code | @codeSystem | @displayName]/@displayName
        return
        <element id="{string-join(($context,local-name($vocabulary)),'.')}" xmlns="http://hl7.org/fhir">
            <path value="{string-join(($context,local-name($vocabulary)),'.')}"/>
            <representation value="xmlAttr"/>
            <fixedString value="{$vocabulary}"/>
        </element>
        ,
        adfhirsd:template2fhirElement($elm, $sliceintro, $inslice, $projectPrefix, $context, if ($elm/@datatype) then $elm/@datatype else $parentDecorDatatype, $namespaces)
    )
};

declare function adfhirsd:templateInclude2fhirElement($elm as element(), $projectPrefix as xs:string, $dotpath as xs:string?, $parentDecorDatatype as xs:string?, $namespaces as element()?) as element()* {
    let $template   := templ:getTemplateByRef($elm/@ref, if ($elm/@flexibility) then $elm/@flexibility else 'dynamic', $projectPrefix)//*:template/*:template[@id]
    
    return adfhirsd:template2fhirElement($template, (), false(), $projectPrefix, $dotpath, $parentDecorDatatype, $namespaces)
};

declare function adfhirsd:templateChoice2fhirElement($elm as element(), $projectPrefix as xs:string, $dotpath as xs:string?, $parentDecorDatatype as xs:string?, $namespaces as element()?) as element()* {
    for $item in $elm/*
    return 
        adfhirsd:template2fhirElement($item, (), false(), $projectPrefix, $dotpath, $parentDecorDatatype, $namespaces)
};

declare function adfhirsd:templateVocabulary2fhirBinding($vocabularies as element()*, $projectPrefix as xs:string, $strength as xs:string?) as element(f:binding)* {
    for $vocabulary in $vocabularies[@valueSet] | $vocabularies[local-name()='valueSet']
    let $valueSet   := if ($vocabulary[local-name()='valueSet']) then $vocabulary else (vs:getValueSetByRef($vocabulary/@valueSet, string($vocabulary/@flexibility), $projectPrefix, false())//*:valueSet[@id])[1]
    let $canonical  := utillib:getCanonicalUriForOID('ValueSet', $valueSet, $projectPrefix, $setlib:strKeyFHIRR4)
    return
    <binding xmlns="http://hl7.org/fhir">
        <strength value="{$strength}"/>
        <description value="{($valueSet/@displayName, $valueSet/@name)[1]}"/>
        <valueSet value="{$canonical}"/>
    </binding>
};

(:~ Convert a FHIR Structure Definition into a Template based on the Template ITS + FHIR extension (the "ART-DECOR FHIR Profile")
 :
 :  Functions to convert a single FHIR Structure Definition as f:StructureDefinition of kind primitive-type | complex-type | resource
 :  into the Template ITS + FHIR extension "ART-DECOR FHIR Profile"
:)
declare function adfhirsd:convertFHIRStructureDefinition2TemplateAsProfile($fraw as element(f:StructureDefinition)) as element(template) {

    (: convert only of type resource :)
    let $f   := if ($fraw/f:kind/@value = ('primitive-type', 'complex-type', 'resource')) then $fraw else ()
    
    let $allelem :=
        for $e at $i in $f/f:differential/f:element
        let $eix := tokenize($e/f:path/@value, '\.')
        return
            <element xmlns="http://hl7.org/fhir">
            {
                $e/@*,
                attribute { 'level' } { count($eix) },
                attribute { 'last' } { ($eix)[last()] },
                attribute { 'position' } { $i },
                $e/node()
            }
            </element>
    let $template :=
        <template>
        {
            attribute { 'id' }            { '1.2.3.4.5.6.7' },
            attribute { 'name' }          { $f/f:name/@value },
            attribute { 'displayName' }   { $f/f:name/@value },
            attribute { 'effectiveDate' } { $f/substring(f:date/@value, 1, 19) },
            attribute { 'statusCode' }    { $f/f:status/@value },
            attribute { 'versionLabel' }  { $f/f:version/@value },
            attribute { 'canonicalUri' }  { $f/f:url/@value },
            <desc language="en-US">
            {
                $f/f:description/@value
            }
            </desc>,
            <classification type="fhirprofile" format="fhirxml"/>,
            if ($f/f:purpose) then 
                <purpose language="en-US">
                {
                    $f/f:purpose/@value
                }
                </purpose>
            else (),
            <fhirmeta>
            {
                attribute { 'fhirVersion' }    { $f/f:fhirVersion/@value },
                attribute { 'kind' }           { $f/f:kind/@value },
                attribute { 'abstract' }       { $f/f:abstract/@value },
                attribute { 'type' }           { $f/f:type/@value },
                attribute { 'baseDefinition' } { $f/f:baseDefinition/@value },
                attribute { 'derivation' }     { $f/f:derivation/@value },
                for $m in $f/f:mapping
                return
                    <mapping>
                    {
                        attribute { 'identity' } { $m/f:identity/@value },
                        attribute { 'uri' }      { $m/f:uri/@value },
                        attribute { 'name' }     { $m/f:name/@value }
                    }
                    </mapping>
            }
            </fhirmeta>,
            local:processFHIRelements($allelem)
        }
        </template>
        
    return if (empty($f)) then () else $template
};

declare function local:processFHIRelements ($elms as element()*) as node()* {
    if (empty($elms)) then ()
    else
        let $lvl := $elms[1]/@level,
        $until := (
            for $el at $p in $elms
            where $p gt 1 and $el/@level le $lvl
            return $p
            ,
            count($elms) + 1
        )[1]
        return (
            element
            {
                if (ends-with($elms[1]/@last, '[x]')) then 'choice' else 'element'
            }
            {
                attribute { 'name' } { $elms[1]/@last },
                local:doRemainderFHIRElement($elms[1]),
                local:processFHIRelements(subsequence($elms, 2, $until - 2))
            },
            local:processFHIRelements(subsequence($elms, $until))
        )
};

declare function local:doRemainderFHIRElement($felm as element()) as node()* {
    let $rets1 := (
        (: process items that are attributes in ITS :)
        if ($felm/f:min) then attribute { 'minimumMultiplicity' } { $felm/f:min/@value } else (),
        if ($felm/f:max) then attribute { 'maximumMultiplicity' } { $felm/f:max/@value } else (),
        (: process items that are elements in (extended) ITS :)
        <desc language="en-US">
        {
            $felm/f:short/@value
        }
        </desc>
    )
    let $fe :=
        <fhir>
        {
            attribute { 'fid' } { $felm/@id },
            (: process items as attributes :)
            for $fat in (
                $felm/f:mustSupport | 
                $felm/f:isModifier | 
                $felm/f:isSummary | 
                $felm/f:definition |
                $felm/f:comment | 
                $felm/f:requirements
            ) return attribute { name($fat) } { $fat/@value },
            (: process items as elements :)
            for $ex in $felm/f:type
            return
                <type>
                {
                    $ex/f:code/@value,
                    for $tp in $ex/f:targetProfile
                    return <targetProfile ref="{$tp/@value}"/>
                }
                </type>,
            for $bnd in $felm/f:binding
            let $vsurl      := $bnd/f:valueSet/@value
            let $vsurlov    := substring-before(concat($vsurl, '|'), '|')
            let $vsurlvers  := substring-after($vsurl, '|')
            let $vs         := adfhir:getRawValueSetDefinition($vsurlov, $vsurlvers)
            let $title      := $vs/f:title/@value
            let $name       := $vs/f:name/@value
            let $oid        := $vs/f:identifier[f:system/@value='urn:ietf:rfc:3986']/f:value/@value
            let $valueSet   := if (string-length($oid) > 0) then substring-after($oid, 'urn:oid:') else $vsurl
            let $vname := 
                if ($bnd/f:extension[@url='http://hl7.org/fhir/StructureDefinition/elementdefinition-bindingName']) then
                    $bnd/f:extension[@url='http://hl7.org/fhir/StructureDefinition/elementdefinition-bindingName']/f:valueString//@value
                else $name
            return
                <binding>
                {
                    attribute { 'displayName' } { if (string-length($title) > 0) then $title else $vname },
                    attribute { 'strength' } { $bnd/f:strength/@value },
                    attribute { 'description' } { $bnd/f:description/@value },
                    if (string-length($valueSet) > 0) then attribute { 'valueSet' } { $valueSet } else (),
                    if (string-length($vsurl) > 0) then attribute { 'canonicalUri' } { $vsurl } else ()
                }
                </binding>,
            for $ex in $felm/f:mapping
            return
                <mapping>
                {
                    attribute { 'identity' } { $ex/f:identity/@value },
                    attribute { 'map' }      { $ex/f:map/@value }
                }
                </mapping>,
            for $ex in $felm/f:constraint
            return
                <constraint>
                {
                    $ex/node()
                }
                </constraint>
        }
        </fhir>
    (: process artificial choice elements if this is a polymorphic element :)
    let $rets2 :=
        if (count($felm/f:type)>1) then (
            let $cname := replace($felm/@last, '\[x\]', '')
            for $ty in $felm/f:type
            let $cvalue := concat(upper-case(substring($ty/f:code/@value, 1, 1)), substring($ty/f:code/@value, 2))
            return
                <element name="{concat($cname, $cvalue)}">
                    <fhir>
                    {
                        $ty/f:code/@value,
                        for $tp in $ty/f:targetProfile
                        return <targetProfile ref="{$tp/@value}"/>
                    }
                    </fhir>
                </element>
        ) else ()
    
    return (
        $rets1,
        if (count($fe/@*) + count($fe/node()) > 0) then $fe else (),
        $rets2
        )
};