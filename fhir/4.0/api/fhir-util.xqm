xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

(: this module should not be dependent of fhirversion and a part of the future FHIR-code package! :)

module namespace utilfhir           = "http://art-decor.org/ns/fhir-util";

import module namespace getf        = "http://art-decor.org/ns/fhir-settings" at "../api/fhir-settings.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "../../../api/modules/library/settings-lib.xqm";
import module namespace utillib     = "http://art-decor.org/ns/api/util" at "../../../api/modules/library/util-lib.xqm";

declare namespace f                 = "http://hl7.org/fhir";
declare namespace error             = "http://art-decor.org/ns/fhir/error";

(: generic functions for fhir :)

declare function utilfhir:getRequestContentType() as xs:string* {

    let $contentType           := 
        for $type in tokenize(tokenize(head((request:get-header("content-type"), request:get-header("Content-Type"))), ';')[1], "\s*,\s*")
        return
            normalize-space($type)[not(. = '')]
    return
        if ($contentType = ('xml','text/xml','application/xml','application/xml+fhir','application/fhir+xml','*/xml','*/*','*','application/*','text/*')) 
            then($getf:CT_FHIR_XML)
            (: The content-type application/json is not in list because of corrupted body data :)
            else if ($contentType = ('json','text/json', 'application/json+fhir','application/fhir+json','*/json'))
                then($getf:CT_FHIR_JSON)
                else (
                    let $detailcode         := 'MSG_CANT_PARSE_CONTENT'
                    let $details            := 'Requested mime-type not supported. Found: ' || string-join($contentType, ',') || '. Expected one of: ' || string-join(($getf:CT_FHIR_XML, $getf:CT_FHIR_JSON), ', ') || '.'
                    return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details)
                )
};

declare function utilfhir:getRequestBody($contentType as xs:string?, $body as item()?) as item()* {

(: returns the requestbody in XML-format :)

    (:Hack alert: upload fails when content has UTF-8 Byte Order Marker, the UTF-8 representation of the BOM is the byte sequence 0xEF,0xBB,0xBF:)
    let $body               := if (string-to-codepoints(substring($body,1,1))=65279) then (substring($body,2)) else $body
    let $body               := if ($contentType = $getf:CT_FHIR_JSON) 
        then (transform:transform(json-to-xml($body), doc($getf:strFhirJson2XmlXsl), ()))
        else if ($body instance of element()) then $body
            else if ($body instance of document-node()) then ($body/*)
                else if (empty($body)) then () else (fn:parse-xml($body)/f:*)
 
    return $body
};

declare function utilfhir:createMeta($fhirVersion as xs:string?, $projectPrefix as xs:string?) as element(f:meta) {

(: generic metadat for every resource entry in FHIR on behalf of DECOR project :)

    let $meta           :=
        <meta xmlns="http://hl7.org/fhir">
            <lastUpdated value="{current-dateTime()}"/>
            {
                if ($projectPrefix) then 
                    <source value="{$getf:strFhirServices || $fhirVersion || '/' || $projectPrefix}"/>
                else ()
            }
            <tag>
                <system value="http://hl7.org/fhir/FHIR-version"/>
                <code value="{$fhirVersion}"/>
            </tag>
        </meta>
    
    return $meta
};

declare function utilfhir:storeBundleInDecor($fhirVersionShort as xs:string, $decor as element(), $resource as element()) as element(f:Bundle) {

(: returns the bundle for the resource to be saved in, if no bundle is avalable a new bundle is created :)

    let $storeName          := 'Bundle-' || $fhirVersionShort || '-' || $resource/local-name() ||'.xml'
    let $storeDecor         := util:collection-name($decor)
    let $storeColl          := if (xmldb:collection-available($storeDecor || '/prototypes')) 
        then ($storeDecor || '/prototypes/') 
        else xmldb:create-collection($storeDecor, 'prototypes')
           

    let $storeResource      := $storeColl || $storeName
    
    let $bundle             :=
        if (doc-available($storeResource)) 
            then doc($storeResource)/f:Bundle 
            else (doc(xmldb:store($storeColl, $storeName, document {
                 processing-instruction xml-model {'href="../../../../../fhir/' || $fhirVersionShort || '/resources/schemas/fhir-invariants.sch" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"'},
                 <Bundle xmlns="http://hl7.org/fhir" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://hl7.org/fhir ../../../../../fhir/{$fhirVersionShort}/resources/schemas/fhir-single.xsd">
                     <type value="collection"/>
                 </Bundle>}))/f:Bundle
             )
            
    return ($bundle)
};

declare function utilfhir:deleteFhirEntry($fhirVersionShort as xs:string, $decor as element(), $entry as element(f:entry)) {

    let $id                 := $entry//*:id/@value
    let $storeName          := $id || '.xml'
    
    let $strHistory         := 'history/' || $fhirVersionShort
    let $storeDecor         := util:collection-name($decor)
    
    let $storeHistoryFhir   := if (xmldb:collection-available($storeDecor || '/' || $strHistory)) 
        then ($storeDecor || '/' || $strHistory || '/') 
        else xmldb:create-collection($storeDecor, $strHistory)

    let $save           := xmldb:store($storeHistoryFhir, $storeName, $entry)
    let $delete         := update delete $entry    
    
    return ()
};

declare function utilfhir:storeResourceInFhir($fhirResources as xs:string, $fhirHistory as xs:string, $id as xs:string, $resource as element()) as xs:string {

(: not used; escape function to store a fhir resource :)

    let $storeName          := $id || '.xml'
    let $storeResource      := $fhirResources || '/' || $storeName
    let $move               :=
        if (doc-available($storeResource)) then xmldb:move($fhirResources, $fhirHistory, $storeName) else()
    let $save               := xmldb:store($fhirResources, $storeName, $resource)
  
    return ($storeResource)
};

declare function utilfhir:createSortRoutine($varResource as xs:string, $_sort as xs:string*) as xs:string {

    (: dynamic sort routine for multiple order by :)
    (: example 'for $r in $resources order by $r/descendant-or-self::*[local-name()='authored'][1]/@value descending return $r' 
               'for $r in $resources order by $r/descendant-or-self::*[local-name()='status'][1]/@value ascending, $r/descendant-or-self::*[local-name()='authored'][1]/@value descending return $r'
                for $r in $resources order by $r/descendant-or-self::*[local-name()='status'][1]/@value ascending $r/descendant-or-self::*[local-name()='authored'][1]/@value descending return $r
    :)
    
    (: TODO: sorting on 'status' int't working :)
    
    let $sort              := tokenize($_sort, ',')
    
    let $orderBy           := ()
    
    let $orderBy           :=
        for $s in $sort
            let $orderBy       := if (not(substring($s, 1, 1) = '-')) 
                then $orderBy || '$r/descendant-or-self::*[local-name()=''' || $s || '''][1]/@value ascending'  
                else $orderBy || '$r/descendant-or-self::*[local-name()=''' || substring($s, 2) || '''][1]/@value descending'
            return $orderBy
    
    return concat('for $r in ',$varResource ,' order by ', string-join(data($orderBy), ","), ' return $r')
};

declare function utilfhir:wrapResourcesInBundle($resources as element()*, $_offset as xs:integer?, $_count as xs:integer?, $_sort as xs:string*) as element(f:Bundle) {

    let $resourceName       := request:get-attribute('request.resource')[string-length()>0]
    let $serverurl          := $getf:strFhirServices
    let $_now               := current-dateTime()
    let $_feedid            := util:uuid()
    
    let $_totalcount        := count($resources)
    let $_resultcount       := if (empty($_count) or ($_totalcount - $_offset) < $_count) then ($_totalcount - $_offset + 1) else ($_count)
    let $_nextoffset        := if (empty($_count) or ($_totalcount - $_offset) < $_count) then () else ($_offset + $_count)
    let $_lastoffset        := if (empty($_count) or $_count = 0) then () else (floor($_totalcount div $_count) * $_count)

    let $resources             :=
    if ($_sort) then
        let $sortRoutine:= utilfhir:createSortRoutine('$resources', $_sort) 
        return util:eval($sortRoutine)
    else $resources

    return
    <Bundle xmlns="http://hl7.org/fhir">
        <id value="{$_feedid}"/>
        {comment {'<meta><profile value="http://hl7.org/fhir/' || $getf:strFhirVersionShort || '/StructureDefinition/Bundle"/></meta>'}}
        <type value="{$getf:BUNDLE_SEARCHSET}"/>
        <base value="{$serverurl}"/>
        <total value="{$_resultcount}"/>
        <link>
            <relation value="{$serverurl}Bundle/{$_feedid}?_format=application/xml+fhir&amp;search-offset={$_offset}&amp;_count={$_count}"/>
            <url value="self"/>
        </link>
        {if ($_totalcount <= $_resultcount) then () else (
        <link>
            <relation value="{$serverurl}Bundle/{$_feedid}?_format=application/xml+fhir&amp;search-offset=1&amp;_count={$_count}"/>
            <url value="first"/>
        </link>)}
        {if ($_totalcount <= $_resultcount or empty($_nextoffset)) then () else (
        <link>
            <relation value="{$serverurl}Bundle/{$_feedid}?_format=application/xml+fhir&amp;search-offset={$_nextoffset}&amp;_count={$_count}"/>
            <url value="next"/>
        </link>)}
        {if ($_totalcount <= $_resultcount or empty($_lastoffset)) then () else (
        <link>
            <relation value="{$serverurl}Bundle/{$_feedid}?_format=application/xml+fhir&amp;search-offset={$_lastoffset}&amp;_count={$_count}"/>
            <url value="last"/>
        </link>
        )}
        {
            (:  Entry in the bundle - will have deleted or resource status only when resource deleted or resource (but not both) :)
            for $r in subsequence($resources,$_offset,$_resultcount)
            return
            (:body element has no namespace but we're in fhir namespace context:)
            <entry>
                <resource>{$r}</resource>
                <search><mode value="match"/></search>
            </entry>
        }
    </Bundle>

};