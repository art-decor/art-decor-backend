xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

module namespace getf               = "http://art-decor.org/ns/fhir-settings";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../../art/api/api-server-settings.xqm";
declare namespace repo              = "http://exist-db.org/xquery/repo";

declare variable $getf:root                                     := repo:get-root();

(:~ String variable with the FHIR Base version we claim conformance to :)
declare variable $getf:strFhirVersion                           := '4.0.1';
declare variable $getf:strFhirVersionShort                      := '4.0';
declare variable $getf:strFhirVersionFull                       := 'v4.0.1 (R4 Oct 30, 2019)';

(:~ String variable with the FHIR Base URL :)
(:declare variable $getf:strFhirServices  := concat(string-join(tokenize(adserver:getServerURLArt(),'/')[not(position()=(last()-1,last()))],'/'),'/fhir/');:)
declare variable $getf:strFhirServices                          := adserver:getServerURLFhirCanonicalBase();

(:~ String variable with everything under fhir :)
declare variable $getf:strFhir                                  := concat($getf:root,'fhir/', $getf:strFhirVersionShort);

(:~ String variable of release of FHIR schemas/schematrons :)
declare variable $getf:strFhirXsdSch                            := concat($getf:strFhir,'/resources/schemas');

(:~ String variable for conversion of FHIR XML to JSON :)
declare variable $getf:strFhirXml2JsonXsl                       := $getf:strFhir || '/resources/stylesheets/fhir-xml2json.xsl';

(:~ String variable for conversion of FHIR JSON to XML :)
declare variable $getf:strFhirJson2XmlXsl                       := $getf:strFhir || '/resources/stylesheets/fhir-json2xml.xsl';

(:~ String variable with everything under fhir-data
    This collection should only contain child collections and no direct eXist-db resources
:)
declare variable $getf:strFhirData                              := concat($getf:root,'fhir-data/', $getf:strFhirVersionShort);
(: Collection variable with everything under fhir-data :)
(:declare variable $getf:colFhirData      := collection($getf:strFhirData);:)

(:~ String variable with everything under fhir-data/_audit
    Audit contains FHIR SecurityEvents
:)
declare variable $getf:strFhirAudit                             := concat($getf:strFhirData,'/_audit');
(:~ Collection variable with everything under fhir-data/_audit :)
declare variable $getf:colFhirAudit                             := collection($getf:strFhirAudit);

(:~ String variable with everything under fhir-data/_metadata
    Metadata contains the FHIR CapabilityStatement for this server (TODO)
:)
declare variable $getf:strFhirMetadata                          := concat($getf:strFhirData,'/_metadata');
(:~ Collection variable with everything under fhir-data/_audit :)
declare variable $getf:colFhirMetadata                          := collection($getf:strFhirMetadata);

(:~ String variable with everything under fhir-data/_snapshot
    Snapshots are saved query results that may be used for caching and paging
:)
declare variable $getf:strFhirSnapshot                          := concat($getf:strFhirData,'/_snapshot');
(:~ Collection variable with everything under fhir-data/snapshot :)
declare variable $getf:colFhirSnapshot                          := collection($getf:strFhirSnapshot);

(:~ String variable with everything under fhir-data/resources
    Resources contains the current version of resources
:)
declare variable $getf:strFhirResources                         := concat($getf:strFhirData,'/resources');
(:~ Collection variable with everything under fhir-data/resources :)
declare variable $getf:colFhirResources                         := collection($getf:strFhirResources);

(:~ String variable with everything under fhir-data/definitions
    Bundles with definitions iof the current FHIR version
:)
declare variable $getf:strFhirDefinitions                       := concat($getf:strFhirData,'/definitions');
(:~ Collection variable with everything under fhir-data/resources :)
declare variable $getf:colFhirDefinitions                       := collection($getf:strFhirDefinitions);

(:~ String variable with everything under fhir-data/history
    History contains any non-current version of resources
:)
declare variable $getf:strFhirHistory                           := concat($getf:strFhirData,'/history');
(:~ Collection variable with everything under fhir-data/history :)
declare variable $getf:colFhirHistory                           := collection($getf:strFhirHistory);

(:~ id of the server CapabilityStatement resource, regardless of any others that might exist :)
declare variable $getf:strCapabilityStatementId                 := '1';

(:https://github.com/jamesagnew/hapi-fhir/blob/master/hapi-fhir-base/src/main/java/ca/uhn/fhir/rest/server/Constants.java:)
declare variable $getf:CT_ATOM_XML                              := "application/atom+xml";
declare variable $getf:CT_FHIR_JSON                             := "application/fhir+json";

declare variable $getf:CT_FHIR_XML                              := "application/fhir+xml";
declare variable $getf:CT_HTML                                  := "text/html";
declare variable $getf:CT_JSON                                  := "application/json";
declare variable $getf:CT_OCTET_STREAM                          := "application/octet-stream";
declare variable $getf:CT_TEXT                                  := "text/plain";
declare variable $getf:CT_XML                                   := "application/xml";
declare variable $getf:ENCODING_GZIP                            := "gzip";
declare variable $getf:FORMAT_JSON                              := "json";
declare variable $getf:FORMAT_XML                               := "xml";
declare variable $getf:HEADER_ACCEPT                            := "Accept";
declare variable $getf:HEADER_ACCEPT_ENCODING                   := "Accept-Encoding";
declare variable $getf:HEADER_AUTHORIZATION                     := "Authorization";
declare variable $getf:HEADER_AUTHORIZATION_VALPREFIX_BASIC     := "Basic ";
declare variable $getf:HEADER_AUTHORIZATION_VALPREFIX_BEARER    := "Bearer ";
declare variable $getf:HEADER_CATEGORY                          := "Category";
declare variable $getf:HEADER_CATEGORY_LC                       := lower-case($getf:HEADER_CATEGORY);
declare variable $getf:HEADER_CONTENT_DISPOSITION               := "Content-Disposition";
declare variable $getf:HEADER_CONTENT_ENCODING                  := "Content-Encoding";
declare variable $getf:HEADER_CONTENT_LOCATION                  := "Content-Location";
declare variable $getf:HEADER_CONTENT_LOCATION_LC               := lower-case($getf:HEADER_CONTENT_LOCATION);
declare variable $getf:HEADER_CONTENT_TYPE                      := "Content-Type";
declare variable $getf:HEADER_COOKIE                            := "Cookie";
declare variable $getf:HEADER_CORS_ALLOW_METHODS                := "Access-Control-Allow-Methods";
declare variable $getf:HEADER_CORS_ALLOW_ORIGIN                 := "Access-Control-Allow-Origin";
declare variable $getf:HEADER_CORS_EXPOSE_HEADERS               := "Access-Control-Expose-Headers";
declare variable $getf:HEADER_ETAG                              := "ETag";
declare variable $getf:HEADER_ETAG_LC                           := lower-case($getf:HEADER_ETAG);
declare variable $getf:HEADER_IF_NONE_MATCH                     := "If-None-Match";
declare variable $getf:HEADER_IF_NONE_MATCH_LC                  := lower-case($getf:HEADER_IF_NONE_MATCH);
declare variable $getf:HEADER_IF_MATCH                          := "If-Match";
declare variable $getf:HEADER_IF_MATCH_LC                       := lower-case($getf:HEADER_IF_MATCH);
declare variable $getf:HEADER_LAST_MODIFIED                     := "Last-Modified";
declare variable $getf:HEADER_LAST_MODIFIED_LOWERCASE           := lower-case($getf:HEADER_LAST_MODIFIED);
declare variable $getf:HEADER_LOCATION                          := "Location";
declare variable $getf:HEADER_LOCATION_LC                       := lower-case($getf:HEADER_LOCATION);
declare variable $getf:HEADER_SUFFIX_CT_UTF_8                   := "; charset=UTF-8";
declare variable $getf:HEADER_METHOD_GET                        := 'GET';
declare variable $getf:HEADER_METHOD_POST                       := 'POST';
declare variable $getf:HEADER_METHOD_PUT                        := 'PUT';
declare variable $getf:HEADER_METHOD_DELETE                     := 'DELETE';
declare variable $getf:HEADER_METHOD_OPTIONS                    := 'OPTIONS';
declare variable $getf:HEADER_METHOD_PATCH                      := 'PATCH';
declare variable $getf:HEADERVALUE_CORS_ALLOW_METHODS_ALL       := ('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS');
declare variable $getf:OPENSEARCH_NS_OLDER                      := "http://purl.org/atompub/tombstones/1.0";
declare variable $getf:PARAM_COUNT                              := "_count";
declare variable $getf:PARAM_DELETE                             := "_delete";
declare variable $getf:PARAM_DEBUG                              := "_debug";
declare variable $getf:PARAM_FORMAT                             := "_format";
declare variable $getf:PARAM_HISTORY                            := "_history";
declare variable $getf:PARAM_INCLUDE                            := "_include";
declare variable $getf:PARAM_META                               := "_meta";
declare variable $getf:PARAM_METADATA                           := "metadata";
declare variable $getf:PARAM__METADATA                          := "_metadata";
declare variable $getf:PARAM_NARRATIVE                          := "_narrative";
declare variable $getf:PARAM_PAGINGACTION                       := "_getpages";
declare variable $getf:PARAM_PAGINGOFFSET                       := "_getpagesoffset";
declare variable $getf:PARAM_PRETTY                             := "_pretty";
declare variable $getf:PARAM_PRETTY_VALUE_TRUE                  := "true";
declare variable $getf:PARAM_QUERY                              := "_query";
declare variable $getf:PARAM_SEARCH                             := "_search";
declare variable $getf:PARAM_SINCE                              := "_since";
declare variable $getf:PARAM_SORT                               := "_sort";
declare variable $getf:PARAM_SORT_ASC                           := "_sort:asc";
declare variable $getf:PARAM_SORT_DESC                          := "_sort:desc";
declare variable $getf:PARAM_TAGS                               := "_tags";
declare variable $getf:PARAM_VALIDATE                           := "_validate";
declare variable $getf:PARAMQUALIFIER_MISSING                   := ":missing";
declare variable $getf:PARAMQUALIFIER_STRING_EXACT              := ":exact";
declare variable $getf:PARAMQUALIFIER_TOKEN_TEXT                := ":text";
declare variable $getf:PARAMDECOR_ID_VERSION_SEPCHARS           := "--";
declare variable $getf:STATUS_HTTP_200_OK                       := 200;
declare variable $getf:STATUS_HTTP_201_CREATED                  := 201;
declare variable $getf:STATUS_HTTP_204_NO_CONTENT               := 204;
declare variable $getf:STATUS_HTTP_304_NOT_MODIFIED             := 304;
declare variable $getf:STATUS_HTTP_400_BAD_REQUEST              := 400;
declare variable $getf:STATUS_HTTP_401_CLIENT_UNAUTHORIZED      := 401;
declare variable $getf:STATUS_HTTP_404_NOT_FOUND                := 404;
declare variable $getf:STATUS_HTTP_405_METHOD_NOT_ALLOWED       := 405;
declare variable $getf:STATUS_HTTP_406_NOT_ACCEPTABLE           := 406;
declare variable $getf:STATUS_HTTP_409_CONFLICT                 := 409;
declare variable $getf:STATUS_HTTP_410_GONE                     := 410;
declare variable $getf:STATUS_HTTP_412_PRECONDITION_FAILED      := 412;
declare variable $getf:STATUS_HTTP_415_UNSUPPORTED_MEDIA_TYPE   := 415;
declare variable $getf:STATUS_HTTP_422_UNPROCESSABLE_ENTITY     := 422;
declare variable $getf:STATUS_HTTP_500_INTERNAL_ERROR           := 500;
declare variable $getf:STATUS_HTTP_501_NOT_IMPLEMENTED          := 501;
declare variable $getf:URL_TOKEN_HISTORY                        := "_history";

(:
Include these codes as defined in http://hl7.org/fhir/restful-interaction
Code                    Display
read                    read                Read the current state of the resource
vread                   vread               Read the state of a specific version of the resource
update                  update              Update an existing resource by its id (or create it if it is new)
delete                  delete              Delete a resource
validate                validate            Check that the content would be acceptable as an update
create                  create              Create a new resource with a server assigned id
history-instance        history-instance    Retrieve the update history for a particular resource
history-type            history-type        Retrieve the update history for a all resources of a particular type
history-system          history-system      Retrieve the update history for all resources on a system
search-type             search-type         Search all resources of the specified type based on some filter criteria
search-system           search-system       Search all resources based on some filter criteria
transaction             transaction         Update, create or delete a set of resources as a single transaction

index                   index               Retrieve the index html page
capabilities            capabilities        Read a capability statement for the server
read-meta               read-meta           Read the meta from current state of the resource
vread-meta              vread-meta          Read the meta from state of a specific version of the resource
update-meta             update-meta         Update meta from an existing resource by its id (or create meta if it is new)
validate-meta           validate-meta       Check that the meta content would be acceptable as an update
create-meta             create-meta         Add meta to existing resource
transaction-meta        transaction-meta    Update, create or delete meta on a set of resources as a single transaction
:)

declare variable $getf:RFINT_INDEX                              := 'index';
declare variable $getf:RFINT_READ                               := 'read';
declare variable $getf:RFINT_VREAD                              := 'vread';
declare variable $getf:RFINT_UPDATE                             := 'update';
declare variable $getf:RFINT_DELETE                             := 'delete';
declare variable $getf:RFINT_VALIDATE                           := 'validate';
declare variable $getf:RFINT_CREATE                             := 'create';
declare variable $getf:RFINT_HISTORY_INSTANCE                   := 'history-instance';
declare variable $getf:RFINT_HISTORY_TYPE                       := 'history-type';
declare variable $getf:RFINT_HISTORY_SYSTEM                     := 'history-system';
declare variable $getf:RFINT_SEARCH_TYPE                        := 'search-type';
declare variable $getf:RFINT_SEARCH_SYSTEM                      := 'search-system';
declare variable $getf:RFINT_TRANSACTION                        := 'transaction';

declare variable $getf:RFINT_CAPABILITIES                       := 'capabilities';
declare variable $getf:RFINT_CREATE_META                        := 'create-meta';
declare variable $getf:RFINT_DELETE_META                        := 'delete-meta';
declare variable $getf:RFINT_READ_META                          := 'read-meta';
declare variable $getf:RFINT_TRANSACTION_META                   := 'transaction-meta';
declare variable $getf:RFINT_UPDATE_META                        := 'update-meta';
declare variable $getf:RFINT_VALIDATE_META                      := 'validate-meta';
declare variable $getf:RFINT_VREAD_META                         := 'vread-meta';

(: **** DATATYPES START **** :)
(: **** PRIMITIVE TYPES **** :)

(:boolean	xs:boolean	Values can be either true or false (0 and 1 are not valid values):)
declare variable $getf:DT_BOOLEAN                               := 'boolean';
(:integer	xs:int	A signed 32-bit integer (for larger values, use decimal):)
declare variable $getf:DT_INTEGER                               := 'integer';
(:decimal	xs:decimal	A rational number. Note: for implementations, do not use an IEEE type floating point type. Instead use something that works like a true decimal, with inbuilt precision (e.g. Java BigDecimal). Decimals may not use exponents:)
declare variable $getf:DT_DECIMAL                               := 'decimal';
(:base64Binary	xs:base64Binary	A stream of bytes, base64 encoded (RFC 4648):)
declare variable $getf:DT_BASE64BINARY                          := 'base64Binary';
(:instant	xs:dateTime	An instant in time - known at least to the second and always includes a time zone. Note: This type is for system times, not human times (see date and dateTime below).:)
declare variable $getf:DT_INSTANT                               := 'instant';
(:string	xs:string	A sequence of Unicode characters. Note that strings SHALL NOT exceed 1MB in size:)
declare variable $getf:DT_STRING                                := 'string';
(:uri	xs:anyURI	A Uniform Resource Identifier Reference. It can be absolute or relative, and may have an optional fragment identifier (RFC 3986):)
declare variable $getf:DT_URI                                   := 'uri';
(:date	union of xs:date, xs:gYearMonth, xs:gYear	A date, or partial date (e.g. just year or year + month) as used in human communication. There is no time zone. Dates SHALL be valid dates.:) 
declare variable $getf:DT_DATE                                  := 'date';
(:dateTime	union of xs:dateTime, xs:date, xs:gYearMonth, xs:gYear	A date, date-time or partial date (e.g. just year or year + month) as used in human communication. If hours and minutes are specified, a time zone SHALL be populated. Seconds may be provided but may also be ignored. Dates SHALL be valid dates. The time "24:00" is not allowed:)
declare variable $getf:DT_DATETIME                              := 'dateTime';
(:time	xs:time	A time during the day, with no date specified (can be converted to a Duration since midnight). Seconds may be provided but may also be ignored. The time "24:00" is not allowed, and neither is a time zone:)
declare variable $getf:DT_TIME                                  := 'time';

(: ---- Simple Restrictions ---- :)

(:code	string	Indicates that the value is taken from a set of controlled strings defined elsewhere (see Using codes for further discussion). Technically, a code is restricted to string which has at least one character and no leading or trailing whitespace, and where there is no whitespace other than single spaces in the contents:) 
declare variable $getf:DT_CODE                                  := 'code';
(:oid	uri	An OID represented as a URI (RFC 3001): urn:oid:1.2.3.4.5:)
declare variable $getf:DT_OID                                   := 'oid';
(:uuid	uri	A UUID, represented as a URI (RFC 4122): urn:uuid:a5afddf4-e880-459b-876e-e4591b0acc11. Note the RFC comments about case: UUID values SHALL be represented in lower case, but systems SHOULD interpret them case insensitively:)
declare variable $getf:DT_UUID                                  := 'uuid';
(:id	string	Any combination of upper or lower case ASCII letters ('A'..'Z', and 'a'..'z', numerals ('0'..'9'), '-' and '.', with a length limit of 64 characters. (This might be an integer, an un-prefixed OID, UUID or any other identifier pattern that meets these constraints.) Ids are case sensitive. UUIDs SHALL be sent using lowercase letters.:)
declare variable $getf:DT_ID                                    := 'id';

(: **** ALL PRIMITIVE TYPES **** :)
declare variable $getf:DT_PRIMITVES                             := ($getf:DT_BOOLEAN,$getf:DT_INTEGER,$getf:DT_DECIMAL,$getf:DT_BASE64BINARY,
                                                                    $getf:DT_INSTANT,$getf:DT_STRING,$getf:DT_URI,$getf:DT_DATE,$getf:DT_DATETIME,
                                                                    $getf:DT_TIME,$getf:DT_CODE,$getf:DT_OID,$getf:DT_UUID,$getf:DT_ID);
(: **** COMPLEX TYPES **** :)
declare variable $getf:DT_ATTACHMENT                            := 'Attachment';
declare variable $getf:DT_CODING                                := 'Coding';
declare variable $getf:DT_CODEABLECONCEPT                       := 'CodeableConcept';
declare variable $getf:DT_QUANTITY                              := 'Quantity';
declare variable $getf:DT_RANGE                                 := 'Range';
declare variable $getf:DT_RATIO                                 := 'Ratio';
declare variable $getf:DT_PERIOD                                := 'Period';
declare variable $getf:DT_SAMPLEDDATA                           := 'SampledData';
declare variable $getf:DT_IDENTIFIER                            := 'Identifier';
declare variable $getf:DT_HUMANNAME                             := 'HumanName';
declare variable $getf:DT_ADDRESS                               := 'Address';
declare variable $getf:DT_CONTACTPOINT                          := 'ContactPoint';
declare variable $getf:DT_TIMING                                := 'Timing';
(: **** DATATYPES END **** :)

(: maximum nmuber of results in a bundle :)
declare variable $getf:BUNDLE_MAX_RESULTS                       := 50;

(: **** BUNDLE TYPES START **** :)
declare variable $getf:BUNDLE_COLLECTION                        := 'collection';
declare variable $getf:BUNDLE_DOCUMENT                          := 'document';
declare variable $getf:BUNDLE_HISTORY                           := 'history';
declare variable $getf:BUNDLE_MESSAGE                           := 'message';
declare variable $getf:BUNDLE_TRANSACTION                       := 'transaction';
declare variable $getf:BUNDLE_TRANSACTION_RESPONSE              := 'transaction-response';
declare variable $getf:BUNDLE_SEARCHSET                         := 'searchset';
(: **** BUNDLE TYPES END **** :)

(: **** OPERATIONOUTCOME_SEVERITY START **** :)
declare variable $getf:OPERATIONOUTCOME_SEVERITY_FATAL          := 'fatal';
declare variable $getf:OPERATIONOUTCOME_SEVERITY_ERROR          := 'error';
declare variable $getf:OPERATIONOUTCOME_SEVERITY_WARNING        := 'warning';
declare variable $getf:OPERATIONOUTCOME_SEVERITY_INFORMATION    := 'information';
(: **** OPERATIONOUTCOME_SEVERITY END **** :)

(: **** CONFIGURATION ITEMS FOR SAVING SECURITY EVENTS START **** :)
declare variable $getf:SECURITY_SAVE_NONE                       := 'none';
declare variable $getf:SECURITY_SAVE_ONERROR                    := 'onerror';
declare variable $getf:SECURITY_SAVE_ALWAYS                     := 'always';
(: **** CONFIGURATION ITEMS FOR SAVING SECURITY EVENTS END **** :)

(: **** FIXED COMMON CODESYSTEMS START **** :)
declare variable $getf:CS_LOINC                                 := 'http://loinc.org';
declare variable $getf:CS_SNOMEDCT                              := 'http://snomed.info/sct';
declare variable $getf:CS_UCUM                                  := 'http://unitsofmeasure.org';
declare variable $getf:CS_UCUMCOMMONUNITS                       := doc(repo:get-root() || '/decor/core/DECOR-ucum.xml')/ucums/ucum;
(: **** FIXED COMMON CODESYSTEMS END **** :)