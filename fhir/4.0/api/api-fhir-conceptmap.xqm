xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

module namespace adfhircm           = "http://art-decor.org/ns/fhir/4.0/conceptmap";
import module namespace adfhir      = "http://art-decor.org/ns/fhir/4.0" at "api-fhir.xqm";
import module namespace getf        = "http://art-decor.org/ns/fhir-settings" at "fhir-settings.xqm";
(:import module namespace cs          = "http://art-decor.org/ns/decor/codesystem" at "../../../art/api/api-decor-codesystem.xqm";
import module namespace vs          = "http://art-decor.org/ns/decor/valueset" at "../../../art/api/api-decor-valueset.xqm";
import module namespace adloinc     = "http://art-decor.org/ns/terminology/loinc" at "../../../terminology/loinc/api/api-loinc.xqm";
import module namespace adsnomed    = "http://art-decor.org/ns/terminology/snomed" at "../../../terminology/loinc/api/api-snomed.xqm";:)
import module namespace utillib     = "http://art-decor.org/ns/api/util" at "../../../api/modules/library/util-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "../../../api/modules/library/settings-lib.xqm";
import module namespace markdown    = "http://art-decor.org/ns/api/markdown" at "../../../api/modules/library/markdown-lib.xqm";
declare namespace f                 = "http://hl7.org/fhir";
declare namespace error             = "http://art-decor.org/ns/fhir/error";

declare %private variable $adfhircm:type            := 'ConceptMap';

declare function adfhircm:convertDecorConceptMap2FHIRConceptMap($conceptMap as element(conceptMap), $params as map(*)?) as element() {
    let $language               := ($params?language, $conceptMap/ancestor::*:decor/*:project/@defaultLanguage, $conceptMap/*[@language = 'en-US']/@language, $conceptMap/*/@language, 'en-US')[not(. = '*')][1]
    let $projectPrefix          := ($params?projectPrefix, $conceptMap/ancestor::decor/project/@prefix)[1]
    let $fhirCanonicalBase      := $params?fhirCanonicalBase
    let $publisher              := ($conceptMap/*:publishingAuthority, $params?publisher)[1]
    let $publisher              := 
        if ($publisher) then $publisher else (
            $conceptMap/ancestor::*:decor/*:project/*:copyright[@type = 'author'],
            $conceptMap/ancestor::*:decor/*:project/*:copyright[empty(@type)]
        )[1]
    
    let $uuid                   := util:uuid()
    let $fhirId                 := concat($conceptMap/@id, '--', replace($conceptMap/@effectiveDate,'\D',''))
    let $structureVersion       := <profile value="http://hl7.org/fhir/{$getf:strFhirVersionShort}/StructureDefinition/{$adfhircm:type}"/>
    let $projectversion         := if (request:exists()) then request:get-attribute('request.projectversion')[string-length()>0] else ()
    
    let $oidMap                 := map:merge(
        for $cs in $conceptMap//*[empty(@canonicalUri)][utillib:isOid(@codeSystem)]
        let $csid := $cs/@codeSystem
        let $csed := $cs/@codeSystemVersion
        group by $csid, $csed
        return map:entry($csid || $csed, utillib:getCanonicalUriForOID('CodeSystem', $csid, $csed, $projectPrefix, $setlib:strKeyCanonicalUriPrefdR4))
    )
    let $sourceScopeUri     := 
        if ($conceptMap/sourceScope/@canonicalUri)      then $conceptMap/sourceScope/@canonicalUri else
        if ($conceptMap/sourceScope/@ref[utillib:isOid(.)]) then 
            utillib:getCanonicalUriForOID('ValueSet', $conceptMap/sourceScope/@ref, $conceptMap/sourceScope/@flexibility, $projectPrefix, $setlib:strKeyCanonicalUriPrefdR4) 
        else (
            $conceptMap/sourceScope/@ref
        )
    let $targetScopeUri     := 
        if ($conceptMap/targetScope/@canonicalUri)      then $conceptMap/targetScope/@canonicalUri else
        if ($conceptMap/targetScope/@ref[utillib:isOid(.)]) then 
            utillib:getCanonicalUriForOID('ValueSet', $conceptMap/targetScope/@ref, $conceptMap/targetScope/@flexibility, $projectPrefix, $setlib:strKeyCanonicalUriPrefdR4) 
        else (
            $conceptMap/targetScope/@ref
        )
    (:FHIR id may not have colons. This affects using @effectiveDate as part of the Resource.id
        Remove any non-digit from the effectiveDate before adding it to the URI, e.g. 20161026123456
    :)
    return
    <ConceptMap xmlns="http://hl7.org/fhir">
        <id value="{$fhirId}"/>
    {
        if ($conceptMap/@lastModifiedDate or $projectPrefix) then
            <meta>
            {
                if ($conceptMap/@lastModifiedDate) then
                    <lastUpdated value="{$conceptMap/@lastModifiedDate || 'Z'}"/>
                else (),
                if ($projectPrefix) then 
                    <source value="{$fhirCanonicalBase || $getf:strFhirVersionShort || '/' || $projectPrefix}"/>
                else ()
            }
            {comment {fn:serialize($structureVersion)}}
            </meta>
        else (),
        <language value="{$language}"/>
    }
    {
        (: url 0..1 :)
        if ($conceptMap/@canonicalUri) then <url value="{$conceptMap/@canonicalUri}"/> else (),
        (: identifier 0..1 :)
        <identifier>
            <system value="urn:ietf:rfc:3986"/>
            <value value="urn:oid:{$conceptMap/@id}"/>
        </identifier>,
        (: version 0..1 :)
        if ($conceptMap/@versionLabel) then <version value="{$conceptMap/@versionLabel}"/> else (),
        (: name 0..1 :)
        adfhir:decorName2fhirName($conceptMap/@displayName, $language),
        (: title 0..1 :)
        adfhir:decorName2fhirTitle($conceptMap/@displayName, $language),
        (: status 0..1 :)
        <status value="{adfhircm:decorStatus2fhirStatus($conceptMap/@statusCode)}"/>,
        (: experimental 0..1 :)
        <experimental value="{$conceptMap/@experimental = 'true'}"/>,
        (: date 0..1 :)
        <date value="{$conceptMap/@effectiveDate || 'Z'}"/>,
        (: publisher 0..1 :)
        (: contact 0..* :)
        (: required for http://hl7.org/fhir/shareablevalueset.html:)
        if ($publisher) then (
            adfhir:decorPublishingAuthority2fhirPublisher($publisher),
            adfhir:decorPublishingAuthority2fhirContact($publisher)
        )
        else (
            <publisher value="ART-DECOR Expert Group"/>
        ),
        (: description 0..1 :)
        adfhir:decorDesc2fhirDescription($conceptMap/*:desc, $language),
        (: useContext 0..*  - not supported :)
        (: jurisdiction 0..* :)
        adfhir:decorJurisdiction2fhirJurisdiction($conceptMap/*:jurisdiction, $oidMap),
        (: purpose 0..1 :)
        adfhir:decorPurpose2fhirPurpose($conceptMap/*:purpose, $language),
        (: copyright 0..1 :)
        adfhir:decorCopyright2fhirCopyright($conceptMap/*:copyright, $language),
        (: source[uri | canonical] 0..1 :)
        adfhircm:decorSourceScope2fhirSourceScope($conceptMap/*:sourceScope, $fhirCanonicalBase || $getf:strFhirVersionShort || '/' || $projectPrefix),
        (: target[uri | canonical] 0..1 :)
        adfhircm:decorTargetScope2fhirTargetScope($conceptMap/*:targetScope, $fhirCanonicalBase || $getf:strFhirVersionShort || '/' || $projectPrefix),
        (: group 0..* :)
        adfhircm:decorGroup2fhirGroup($conceptMap/*:group, $language)
    }
    </ConceptMap>
};

declare function adfhircm:decorSourceScope2fhirSourceScope($in as element(sourceScope)?, $fhirCanonicalBase as xs:string?) as element()? {
    if ($in/@canonicalUri) then
        <sourceCanonical xmlns="http://hl7.org/fhir" value="{$in/@canonicalUri}"/>
    else 
    if ($in[utillib:isOid(@ref)]) then (
        let $vs     := $setlib:colDecorData//valueSet[@id = $in/@ref] | $setlib:colDecorCache//valueSet[@id = $in/@ref]
        let $vs     := 
            if ($in[@flexibility castable as xs:dateTime]) then $vs[@effectiveDate = $in/@flexibility] else (
                $vs[@effectiveDate = string(max($vs/xs:dateTime(@effectiveDate)))]
            )
        return
            if ($vs/@canonicalUri) then
                <sourceCanonical xmlns="http://hl7.org/fhir" value="{($vs/@canonicalUri)[1]}"/>
            else
            if ($fhirCanonicalBase) then
                <sourceUri xmlns="http://hl7.org/fhir" value="{$fhirCanonicalBase || '/ValueSet/' || string-join(($in/@ref, replace($in/@flexibility, '\D', '')[not(. = '')]), $getf:PARAMDECOR_ID_VERSION_SEPCHARS)}"/>
            else ()
    )
    else (
        <sourceCanonical xmlns="http://hl7.org/fhir" value="{$in/@ref}"/>
    )
};
declare function adfhircm:decorTargetScope2fhirTargetScope($in as element(targetScope)?, $fhirCanonicalBase as xs:string?) as element()? {
    if ($in/@canonicalUri) then
        <targetCanonical xmlns="http://hl7.org/fhir" value="{$in/@canonicalUri}"/>
    else
    if ($in[utillib:isOid(@ref)]) then (
        let $vs     := $setlib:colDecorData//valueSet[@id = $in/@ref] | $setlib:colDecorCache//valueSet[@id = $in/@ref]
        let $vs     := 
            if ($in[@flexibility castable as xs:dateTime]) then $vs[@effectiveDate = $in/@flexibility] else (
                $vs[@effectiveDate = string(max($vs/xs:dateTime(@effectiveDate)))]
            )
        return
            if ($vs/@canonicalUri) then
                <targetCanonical xmlns="http://hl7.org/fhir" value="{($vs/@canonicalUri)[1]}"/>
            else
            if ($fhirCanonicalBase) then
                <targetUri xmlns="http://hl7.org/fhir" value="{$fhirCanonicalBase || '/ValueSet/' || string-join(($in/@ref, replace($in/@flexibility, '\D', '')[not(. = '')]), $getf:PARAMDECOR_ID_VERSION_SEPCHARS)}"/>
            else ()
    )
    else (
        <targetCanonical xmlns="http://hl7.org/fhir" value="{$in/@ref}"/>
    )
};
declare function adfhircm:decorGroup2fhirGroup($groups as element(group)*, $language as xs:string) as element(f:group)* {
    for $group in $groups
    return
        <group xmlns="http://hl7.org/fhir">
        {
            if ($group/*:source/@codeSystem) then (
                if ($group/*:source[utillib:isOid(@codeSystem)]) then 
                    <source value="{utillib:getCanonicalUriForOID('CodeSystem', $group/*:source/@codeSystem, $group/*:source/@codeSystemVersion, $group/ancestor::decor, $setlib:strKeyCanonicalUriPrefdR4)}"/>
                else (
                    <source value="{$group/*:source/@codeSystem}"/>
                ),
                if ($group/*:source/@codeSystemVersion) then <sourceVersion value="{$group/*:source/@codeSystemVersion}"/> else () 
            ) else ()
        }
        {
            if ($group/*:target/@codeSystem) then (
                if ($group/*:target[utillib:isOid(@codeSystem)]) then 
                    <target value="{utillib:getCanonicalUriForOID('CodeSystem', $group/*:target/@codeSystem, $group/*:target/@codeSystemVersion, $group/ancestor::decor, $setlib:strKeyCanonicalUriPrefdR4)}"/>
                else (
                    <target value="{$group/*:target/@codeSystem}"/>
                ),
                if ($group/*:target/@codeSystemVersion) then <targetVersion value="{$group/*:target/@codeSystemVersion}"/> else () 
            ) else ()
        }
        {
            adfhircm:decorGroupElement2fhirGroupElement($group/*:element, $language)
        }
        </group>
};
declare function adfhircm:decorGroupElement2fhirGroupElement($in as element(element)*, $language as xs:string) as element(f:element)* {
    for $element in $in
    return
        <element xmlns="http://hl7.org/fhir">
        {
            if ($element/@code) then <code value="{$element/@code}"/> else (),
            if ($element/@displayName) then <display value="{$element/@displayName}"/> else (),
            for $target in $element/*:target
            return
                <target>
                {
                    if ($target/@code) then <code value="{$target/@code}"/> else (),
                    if ($target/@displayName) then <display value="{$target/@displayName}"/> else (),
                    if ($target/@relationship) then <equivalence value="{$target/@relationship}"/> else (),
                    adfhircm:decorComment2fhirComment($target/*:comment, $language)
                }
                </target>
        }
        </element>
};
declare function adfhircm:decorComment2fhirComment($in as element(comment)*, $language as xs:string) as element(f:comment)? {
    let $default    := ($in[@language = $language], $in)[1]
    return
        if ($default) then 
            <comment xmlns="http://hl7.org/fhir" value="{$default}">
            {
                for $n in $in[@language][not(@language = $default/@language)]
                return
                    <extension url="http://hl7.org/fhir/StructureDefinition/translation">
                        <extension url="lang">
                            <valueCode value="{$n/@language}"/>
                        </extension>
                        <extension url="content">
                            <valueString value="{$n}"/>
                        </extension>
                    </extension>
            }
            </comment>
        else ()
};
(:~ http://hl7.org/fhir/R4/valueset-publication-status.html :)
declare function adfhircm:decorStatus2fhirStatus($status as xs:string?) as xs:string? {
    switch ($status)
    case 'new'          return 'draft'
    case 'draft'        return 'draft'
    case 'pending'      return 'draft'
    case 'final'        return 'active'
    case 'cancelled'    return 'retired'
    case 'rejected'     return 'retired'
    case 'deprecated'   return 'retired'
    default             return 'draft'
};

(:~ http://hl7.org/fhir/R4/valueset-publication-status.html :)
declare function adfhircm:fhirStatus2decorStatus($status as xs:string?) as xs:string? {
    switch ($status)
    case 'draft'        return 'draft'
    case 'active'       return 'final'
    case 'retired'      return 'deprecated'
    default             return ()
};
