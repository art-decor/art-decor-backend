xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

module namespace adfhirqr           = "http://art-decor.org/ns/fhir/4.0/questionnaireresponse";

import module namespace adfhir      = "http://art-decor.org/ns/fhir/4.0" at "api-fhir.xqm";
import module namespace getf        = "http://art-decor.org/ns/fhir-settings" at "../api/fhir-settings.xqm";
import module namespace utilfhir    = "http://art-decor.org/ns/fhir-util" at "../api/fhir-util.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "../../../api/modules/library/settings-lib.xqm";
import module namespace utillib     = "http://art-decor.org/ns/api/util" at "../../../api/modules/library/util-lib.xqm";
import module namespace decorlib    = "http://art-decor.org/ns/api/decor" at "../../../api/modules/library/decor-lib.xqm";

declare namespace f                 = "http://hl7.org/fhir";

(:~ Checks various aspects of the request id versus QuestionnaireResponse resources in the database.

- There SHALL be exactly one QuestionnaireResponse for the given $id
- The given $id SHALL match the given payload QuestionnaireResponse.id
- The QuestionnaireResponse SHALL be active, i.e. not inside $id.xml
- The QuestionnaireResponse SHALL have a .meta.tag containing the FHIR version
- The QuestionnaireResponse.meta.tag with the FHIR version SHALL match the current FHIR server version

@param $id 1..1 - contains the id string search parameter we used to find the $qr with
@param $qr 0..* - contains the QuestionnaireResponse element we found based on the $id
@return error() or empty
:)

declare function adfhirqr:checkIdQuestionnaireResponse($id as xs:string, $qr as element(f:QuestionnaireResponse)*) {
    adfhirqr:checkIdQuestionnaireResponse($id, $qr, ())

};

declare function adfhirqr:checkIdQuestionnaireResponse($id as xs:string, $qr as element(f:QuestionnaireResponse)*, $bodyId as xs:string?) {
    
    (: check id :)
    let $check                  :=
        if ($bodyId) then (
            if ($bodyId = $id) then () else (
                let $detailcode         := 'MSG_INVALID_ID'
                let $details            := 'The QuestionnaireResponse request url id ' || $id || ' shall match the QuestionnaireResponse request body id ' || $bodyId || '.'
                return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details)
            )
        ) else () 
    
    let $fhirVersion            := 
                $qr/f:meta/f:tag[f:system/@value = 'http://hl7.org/fhir/FHIR-version']/f:code/@value
        
    return
    (: check OK QuestionnaireResponse exactly occurs one time with matching occurence of fhirVersion :)
    switch (count($qr))
        case 0 return (
            let $detailcode         := 'MSG_NO_EXIST'
            let $details            := 'The QuestionnaireResponse request url id ' || $id || ' does not exist.'
            return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details)
        )
        case 1 return (
            if (doc-available(util:collection-name($qr) || '/'|| $id || '.xml')) then 
                let $detailcode         := 'MSG_DELETED_ID'
                let $details            := 'The QuestionnaireResponse request url id ' || $id || ' is already removed from the project.' 
                return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details) 
            else 
            if (empty($fhirVersion)) then
                let $detailcode         := 'MSG_VERSION_AWARE_CONFLICT'
                let $details            := 'The requested QuestionnaireResponse with id ' || $id || ' is missing a version tag and cannot be determined to be compatible with this FHIR Server version ' || $getf:strFhirVersion || '. Please inform your server administrator of this issue.'
                return error(QName('http://hl7.org/fhir/ValueSet/operation-outcome', $detailcode), $details)
            else
            if ($fhirVersion = ($getf:strFhirVersionShort, $getf:strFhirVersion)) then () 
            else 
                let $detailcode         := 'MSG_VERSION_AWARE_CONFLICT'
                let $details            := 'The requested QuestionnaireResponse with id ' || $id || ' is not compatible with this FHIR Server version ' || $getf:strFhirVersion || '. Please retrieve using FHIR Server version ' || string-join($fhirVersion, ', or ')
                return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details) 
        )
        default return (
            let $detailcode         := 'MSG_DUPLICATE_ID'
            let $details            := 'The QuestionnaireResponse request url id ' || $id || ' occurred ' || count($qr) || ' times. Please inform your system administrator.'
            return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details)
        )
        
};

declare function adfhirqr:checkDecorQuestionnaireResponse($authmap as map(*), $projectPrefix as xs:string?, $qr as element(f:QuestionnaireResponse)?) as element(decor) {

    (: check projectprefix and permissions :)
    let $storedDecor                    := if ($qr) then collection(util:collection-name($qr)||'/../')/decor else ()
    let $pathDecor                      := if ($projectPrefix) then utillib:getDecor($projectPrefix, (), ()) else ()
    
    let $check                          :=
        if (empty($pathDecor) or empty($storedDecor)) then () else 
        if ($storedDecor/project/@prefix = $pathDecor/project/@prefix) then () else (
            let $detailcode         := 'MSG_RESOURCE_MISMATCH'
            let $details            := 'This resource is part of ' || $storedDecor/project/@prefix || ', but requested to save in ' || $pathDecor/project/@prefix || '. Please use the project prefix ' || $storedDecor/project/@prefix || ' or ''public''.'
            return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details)
        )
    
    let $decor                          := ($storedDecor, $pathDecor)[1]
    let $check                          := 
        if ($decor) then (
            if (decorlib:authorCanEditP($authmap, $decor, $decorlib:SECTION-RULES)) then () else (
                let $detailcode         := 'MSG_RESOURCE_NOT_ALLOWED'
                let $details            := 'User ' || $authmap?name || ' does not have sufficient permissions to modify questionnaireresponses in project ' || $decor/project/@prefix || '. You have to be an active author in the project.'
                return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details)
            )
        ) 
        else (
            let $detailcode             := 'MSG_PARAM_INVALID'
            let $details                := 'Project with prefix ' || $decor/project/@prefix || ' does not exist'
            return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details)
        )
       
    return $decor
};

declare function adfhirqr:checkBodyQuestionnaireResponse($body as element(f:QuestionnaireResponse)) {
        
    (: check resource - body :)
     let $check              := 
        if ($body/*:questionnaire) then (
            let $q                    := $setlib:colDecorData//questionnaire[@canonicalUri = $body/*:questionnaire/@value]
            let $mismatchingLinkIds   := if ($q) then $body//f:linkId[not(@value = $q//@linkId)] else ()
            let $mismatchingDatatypes := 
                if ($q) then 
                    for $answer in $body//f:answer/f:*
                    let $qrdt         := lower-case(substring-after(local-name($answer), 'value'))
                    let $qdt          := $q//item[@linkId = $answer/ancestor::f:item[1]/f:linkId/@value]/lower-case(@type)
                    return
                        if ($qrdt = $qdt) then () else if ($qrdt = 'coding' and $qdt = ('choice', 'open-choice')) then () else <x>{$answer/ancestor::f:item[1]/f:linkId/@value || ': ' || $qrdt || ' != ' || $qdt}</x>  (:$answer/ancestor::f:item[1]:)
                else ()
            return 
                if (empty($q)) then (
                    let $detailcode         := 'MSG_RESOURCE_MISMATCH'
                    let $details            := 'The QuestionnaireResponse points to a Questionnaire ' || $body/*:questionnaire/@value || ' that cannot be found. Please refer to a questionnaire known to this server.'
                    return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details)
                )
                else
                if ($mismatchingLinkIds) then (
                    let $detailcode         := 'MSG_RESOURCE_MISMATCH'
                    let $details            := 'The QuestionnaireResponse has one or more mismatching item.linkId values compared to the Questionnaire ' || $body/*:questionnaire/@value || '. Found: ''' || string-join($mismatchingLinkIds/@value, ''', ''') || ''''
                    return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details)
                )
                else
                if ($mismatchingDatatypes) then (
                    let $detailcode         := 'MSG_RESOURCE_MISMATCH'
                    let $details            := 'The QuestionnaireResponse points to a Questionnaire ' || $body/*:questionnaire/@value || ' that specifies one or more different answer types for one or more questions in this response.' || ' Found: ''' || string-join(distinct-values($mismatchingDatatypes(:/f:linkId/@value:)), ''', ''') || ''''
                    return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details)
                )
                else ()
        ) 
        else (
            let $detailcode         := 'MSG_RESOURCE_MISMATCH'
            let $details            := 'The QuestionnaireResponse.questionnaire is missing but required. Please refer to a questionnaire known to this server.'
            return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details)
        )
    return ()
    
};

declare function adfhirqr:saveQuestionnaireResponse($body as element(f:QuestionnaireResponse), $decor as element(), $id as xs:string?, $projectPrefix as xs:string?) as element(f:QuestionnaireResponse) {

    let $id                     := if (empty($id)) then (util:uuid()) else ($id)
    
    let $resource            :=
        <QuestionnaireResponse xmlns="http://hl7.org/fhir">
            <id value="{$id}"/>
            {utilfhir:createMeta($getf:strFhirVersion, $projectPrefix)}
            {$body/(node() except (f:id | f:meta))}
        </QuestionnaireResponse>    
  
    (: check result is scheme and schematron compliant :)
    let $validation       :=  adfhir:validate($resource)
    let $check            :=
        if ($validation[@role = ('error', 'warn', 'warning', 'fatal')]) then (
            let $detailcode         := 'MSG_BAD_SYNTAX'
            let $details            := 'The QuestionnaireResponse to save has structural and/or business rule related issues'
            return error(QName('http://art-decor.org/ns/fhir/error', $detailcode), $details)
        ) else ()
    
    let $entry            := 
        <entry xmlns="http://hl7.org/fhir">
            <fullUrl value="urn:uuid:{$id}"/>
            <resource>
                {$resource}
            </resource>
        </entry>
 
    let $storeResource              := utilfhir:storeBundleInDecor($getf:strFhirVersionShort, $decor, $resource)
    let $currentQR                  := $storeResource//f:QuestionnaireResponse[f:id[@value = $resource/*:id/@value]]
    let $save                       := if ($currentQR) then (update replace $currentQR with $resource ) else (update insert $entry into $storeResource)
 
    return ($resource)
};

declare function adfhirqr:getQuestionnaireResponses($doSearch as xs:boolean, $id as xs:string?, $expr as xs:string?) as element(f:QuestionnaireResponse)* {

    let $projectPrefix     := request:get-attribute('request.projectprefix')[string-length() gt 0]
    (: get all in scope QuestionnaireResponses :)
    let $questresponses    := 
        if ($doSearch) then (
            if (empty($projectPrefix)) then 
                $setlib:colDecorData//f:QuestionnaireResponse 
            else (
                collection(util:collection-name(utillib:getDecor($projectPrefix, (), ())))//f:QuestionnaireResponse
            )
        ) 
        else (
             $setlib:colDecorData//f:QuestionnaireResponse
        )
    (: get all QuestionnaireResponses that have the right version if we are searching. For read with id 
       we want to be able to help the user when the id is not under the requested version of FHIR. 
       Normally in meta/tag. Could determine by parsing the document-name :)
    let $questresponses     :=
        if ($doSearch or empty($id)) then
            $questresponses[f:meta[f:tag[f:system/@value = 'http://hl7.org/fhir/FHIR-version'][f:code/@value = ($getf:strFhirVersionShort, $getf:strFhirVersion)]]]
            (:| $questresponses[tokenize(util:document-name(.), '-')[2] = $getf:strFhirVersionShort]:)
        else (
            $questresponses
        )
    
    (: id :)
    let $questresponses     :=
        if (empty($id)) then ($questresponses) else (
            $questresponses[f:id[@value = $id]]
        )
    
    (: _id :)
    let $questresponses     :=
        if (count(request:get-parameter('_id',())[string-length() gt 0]) = 0) then ($questresponses) else (
            for $p in request:get-parameter('_id',())[string-length() gt 0]
            return
                $questresponses[f:id/@value = $p]
        )
    (: author :)
    let $questresponses     :=
        if (count(request:get-parameter('author',())[string-length() gt 0]) = 0) then ($questresponses) else (
            for $p in request:get-parameter('author',())[string-length() gt 0]
            return
                $questresponses[f:author/f:reference/@value = $p]
        )
    (: based-on :)
    let $questresponses     :=
        if (count(request:get-parameter('based-on',())[string-length() gt 0]) = 0) then ($questresponses) else (
            for $p in request:get-parameter('based-on',())[string-length() gt 0]
            return
                $questresponses[f:basedOn/f:reference/@value = $p]
        )
    (: encounter :)
    let $questresponses     :=
        if (count(request:get-parameter('encounter',())[string-length() gt 0]) = 0) then ($questresponses) else (
            for $p in request:get-parameter('encounter',())[string-length() gt 0]
            return
                $questresponses[f:encounter/f:reference/@value = $p]
        )
    (: item-subject :)
    let $questresponses     :=
        if (count(request:get-parameter('item-subject',())[string-length() gt 0]) = 0) then ($questresponses) else (
            for $p in request:get-parameter('item-subject',())[string-length() gt 0]
            return
                $questresponses[.//f:item[f:extension/@url = 'http://hl7.org/fhir/StructureDefinition/questionnaireresponse-isSubject']/f:answer/f:valueReference/@value = $p]
        )
    (: part-of :)
    let $questresponses     :=
        if (count(request:get-parameter('part-of',())[string-length() gt 0]) = 0) then ($questresponses) else (
            for $p in request:get-parameter('part-of',())[string-length() gt 0]
            return
                $questresponses[f:partOf/f:reference/@value = $p]
        )
    (: patient :)
    let $questresponses     :=
        if (count(request:get-parameter('patient',())[string-length() gt 0]) = 0) then ($questresponses) else (
            for $p in request:get-parameter('patient',())[string-length() gt 0]
            return
                $questresponses[f:subject/f:reference/contains(@value, 'Patient/')]
        )
    (: source :)
    let $questresponses     :=
        if (count(request:get-parameter('source',())[string-length() gt 0]) = 0) then ($questresponses) else (
            for $p in request:get-parameter('source',())[string-length() gt 0]
            return
                $questresponses[f:source/f:reference/@value = $p]
        )
    (: subject :)
    let $questresponses     :=
        if (count(request:get-parameter('subject',())[string-length() gt 0]) = 0) then ($questresponses) else (
            for $p in request:get-parameter('subject',())[string-length() gt 0]
            return
                $questresponses[f:subject/f:reference/@value = $p]
        )

    return if (string-length($expr) = 0) then $questresponses else util:eval(concat('$questresponses', $expr))
        
};

declare function adfhirqr:getSearchExpr() as xs:string {

    (: expr-authored :)
    let $expr-authored      :=
        for $p in request:get-parameter('authored',())[string-length() gt 0]
        let $mod      := substring($p, 1, 2)
        let $val      := substring($p, 3, 19)
        let $valflat  := replace($val, '\D', '')
        let $valflen  := string-length($valflat)
        return
            switch ($mod)
            case 'eq' return '[f:authored/starts-with(@value, ''' || $val || ''')]'
            case 'lt'
            case 'eb' return '[f:authored[number(substring(replace(@value, ''\D'', ''''), 1, ' || $valflen || ')) lt number(''' || $valflat || ''')]]'
            case 'le' return '[f:authored[number(substring(replace(@value, ''\D'', ''''), 1, ' || $valflen || ')) le number(''' || $valflat || ''')]]'
            case 'gt'
            case 'sa' return '[f:authored[number(substring(replace(@value, ''\D'', ''''), 1, ' || $valflen || ')) gt number(''' || $valflat || ''')]]'
            case 'ge' return '[f:authored[number(substring(replace(@value, ''\D'', ''''), 1, ' || $valflen || ')) ge number(''' || $valflat || ''')]]'
            default return (
                error(QName('http://art-decor.org/ns/fhir/error','UnsupportedParameterValue'),concat('Parameter authored with prefix ', $mod, ' not supported'))
            )
    (: identifier :)
    let $expr-identifier    :=
        for $p in request:get-parameter('identifier',())[string-length() gt 0]
        return
            concat('[@id = (''', string-join(
                for $sp in tokenize($p, ',')
                let $c  := if (matches($sp, '\|')) then replace(substring-after($sp, '|'), '\{|\}', '') else replace($sp, '\{|\}', '')
                return
                    replace(replace($c, 'urn:oid:', ''), '''', '''''')
                , ''', '''),
            ''')]')
    (: status :)
    let $expr-status        :=
        for $p in request:get-parameter('status',())[string-length() gt 0]
        return
            concat('[f:status/@value = (''', string-join(
                for $sp in tokenize($p, ',')
                let $c  := if (matches($sp, '\|')) then replace(substring-after($sp, '|'), '\{|\}', '') else replace($sp, '\{|\}', '')
                return
                    replace($c, '''', '''''')
                , ''', '''),
            ''')]')
    (: questionnaire :)
    let $expr-quest          :=
        for $p in request:get-parameter('questionnaire',())[string-length() gt 0]
        return
            concat('[f:questionnaire/@value = (''', string-join(
                for $sp in tokenize($p, ',')
                let $c  := if (matches($sp, '\|')) then replace(substring-after($sp, '|'), '\{|\}', '') else replace($sp, '\{|\}', '')
                return
                    replace($c, '''', '''''')
                , ''', '''),
            ''')]')
            
    return string-join(($expr-authored, $expr-identifier, $expr-status, $expr-quest), '')


};

