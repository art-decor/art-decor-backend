xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

module namespace adfhirns           = "http://art-decor.org/ns/fhir/4.0/namingsystem";
import module namespace adfhir      = "http://art-decor.org/ns/fhir/4.0" at "api-fhir.xqm";
import module namespace getf        = "http://art-decor.org/ns/fhir-settings" at "fhir-settings.xqm";
(:import module namespace utillib     = "http://art-decor.org/ns/api/util" at "../../../api/modules/library/util-lib.xqm";:)
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "../../../api/modules/library/settings-lib.xqm";
declare namespace f                 = "http://hl7.org/fhir";
declare namespace error             = "http://art-decor.org/ns/fhir/error";

declare %private variable $adfhirns:type            := 'NamingSystem';

declare function adfhirns:convertDecorOIDEntry2FHIRNamingSystem($oid as element(oid)) as element() {
    let $dotNotation    := $oid/@oid
    let $uri            := $oid/*:property[@type = $setlib:strKeyCanonicalUriPrefdR4]/@value
    let $uri            := if (empty($uri)) then $oid/*:property[@type = $setlib:strKeyCanonicalUriPrefd]/@value else $uri
    let $mnemonic       := $oid/*:property[@type = $setlib:strKeyHL7v2Table0396CodePrefd]/@value
    let $registryname   := $oid/ancestor::*:oidList/@name
    let $id             := string-join(($dotNotation,$registryname),'--')
    let $status         := adfhirns:decorStatus2fhirStatus($oid/@status)
    let $creationDate   := adfhir:dateTimeFromTimestamp($oid/@creationDate)
    let $name           := $oid/*:name[1]
    let $name           := ($name, 'Unknown name')[1]
    let $description    := $oid/*:desc[1]
    let $publisher      := $oid/ancestor::*:oidList/@publisher
    let $kind           := adfhirns:categoryToKind($oid/*:property[@type='Oid_Type']/@value) 
    return
    <NamingSystem xmlns="http://hl7.org/fhir">
        <id value="{$id}"/>
        {comment {'<meta><profile value="http://hl7.org/fhir/' || $getf:strFhirVersionShort || '/StructureDefinition/NamingSystem"/></meta>'}}
        <name value="{adfhir:validResourceName($name)}"/>
        <status value="{$status}"/>
        <kind value="{$kind}"/>
        <publisher value="{if (empty($publisher)) then 'Unknown publisher' else $publisher}"/>
        <date value="{$creationDate}"/>
        <description value="{if (empty($description)) then 'Unknown description' else $description}"/>
        {
            if ($uri) then 
                <uniqueId>
                    <type value="uri"/>
                    <value value="{$uri}"/>
                    <preferred value="true"/>
                </uniqueId>
            else ()
        }
        <uniqueId>
            <type value="oid"/>
            <value value="{$dotNotation}"/>
            {
                if ($uri) then 
                    <preferred value="false"/>
                else ()
            }
        </uniqueId>
        {
            if ($mnemonic) then
                <uniqueId>
                    <type value="other"/>
                    <value value="{$mnemonic}"/>
                    <preferred value="false"/>
                    <comment value="HL7 V2 Table 0396"/>
                </uniqueId>
            else ()
        }
    </NamingSystem>
};

declare function adfhirns:decorStatus2fhirStatus($status as xs:string?) as xs:string {
    switch ($status)
    case 'pending'      return 'draft'
    case 'completed'    return 'active'
    case 'retired'      return 'retired'
    case 'deprecated'   return 'retired'
    default             return 'unknown'
};
declare function adfhirns:fhirStatus2decorStatus($status as xs:string?) as xs:string {
    switch ($status)
    case 'draft'        return 'draft'
    case 'active'       return 'completed'
    case 'retired'      return 'retired'
    default             return 'unknown'
};

declare %private function adfhirns:categoryToKind($category as xs:string?) as xs:string {
    switch (lower-case($category))
    (:1 - OID for an HL7 Internal Object:)
    case '1' return 'root'
    (:2 - OID for an HL7 Body or Group:)
    case '2' return 'root'
    (:3 - Root to be a Registration Authority:)
    case '3' return 'root'
    (:4 - OID for a Registered Namespace:)
    case '4' return 'identifier'
    (:5 - OID for an HL7 Internal Code System:)
    case '5' return 'codesystem'
    (:6 - OID for an External Code System:)
    case '6' return 'codesystem'
    (:7 - OID for an HL7 Document:)
    case '7' return 'root'
    (:8 - OID for an HL7 Document Artifact:)
    case '8' return 'root'
    (:9 - OID for an HL7 Conformance Profile:)
    case '9' return 'identifier'
    (:10 - OID for an HL7 Template:)
    case '10' return 'root'
    (:11 - OID for an HL7 Internal Value Set:)
    case '11' return 'root'
    (:12 - OID for a Version 2.x Table:)
    case '12' return 'root'
    (:13 - OID for an External Value Set:)
    case '13' return 'root'
    (:14 - branch node subtype:)
    case '14' return 'root'
    (:15 - Defined external codesets:)
    case '15' return 'root'
    (:17 - Other Type OID:)
    case '17' return 'root'
    (:18 - OID for a Version 2.x Coding System:)
    case '18' return 'codesystem'
    (:19 - OID for a published HL7 Example:)
    case '19' return 'root'
    case 'codesystem'
    case 'identifier' return lower-case($category)
    default return 'root'
};
