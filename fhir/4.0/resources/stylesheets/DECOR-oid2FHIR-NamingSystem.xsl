<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:local="urn:art-decor:org"
  exclude-result-prefixes="xs"
  version="3.0">
  
  <xsl:output omit-xml-declaration="yes"/>
  <xsl:param name="strKeyCanonicalUriPrefd">HL7-FHIR-System-URI-Preferred-R4</xsl:param>
  <xsl:param name="strKeyHL7v2Table0396CodePrefd">HL7-V2-Table-0396-Code</xsl:param>
  <xsl:param name="strFhirVersionShort">4.0</xsl:param>
  
  <!-- convertDecorOIDEntry2FHIRNamingSystem -->
  <xsl:template match="oid">
    <xsl:variable name="dotNotation" select="dotNotation/@value"/>
    <xsl:variable name="uri" select="additionalProperty[attribute[@value = $strKeyCanonicalUriPrefd]]/value/@value"/>
    <xsl:variable name="mnemonic" select="additionalProperty[attribute[@value = $strKeyHL7v2Table0396CodePrefd]]/value/@value"/>
    <xsl:variable name="registryname" select="ancestor::myoidregistry/@name"/>
    <xsl:variable name="id" select="string-join(($dotNotation, $registryname), '--')"/>
    <xsl:variable name="status" select="local:decorStatus2fhirStatus(status/@code)"/>
    <xsl:variable name="creationDate" select="local:dateTimeFromTimestamp(creationDate/@value)"/>
    <xsl:variable name="name" select="(description[1]/thumbnail/@value, description[1]/@value, 'Unknown name')[1]"/>
    <xsl:variable name="description" select="description[1]/@value"/>
    <xsl:variable name="publisher" select="ancestor::registry/hostingOrganization/name/part/@value"/>
    <xsl:variable name="kind" select="local:categoryToKind(additionalProperty[attribute[@value = 'Oid_Type']]/value/@value)"/>
    <NamingSystem xmlns="http://hl7.org/fhir">
      <id value="{$id}"/>
      <xsl:comment><meta><profile value="http://hl7.org/fhir/{$strFhirVersionShort}/StructureDefinition/NamingSystem"/></meta></xsl:comment>
      <name value="{local:validResourceName($name)}"/>
      <status value="{$status}"/>
      <kind value="{$kind}"/>
      <publisher value="{if (empty($publisher)) then 'Unknown publisher' else $publisher}"/>
      <date value="{$creationDate}"/>
      <description value="{if (empty($description)) then 'Unknown description' else $description}"/>
      <xsl:if test="$uri">
        <uniqueId>
          <type value="uri"/>
          <value value="{$uri}"/>
          <preferred value="true"/>
        </uniqueId>
      </xsl:if>
      <uniqueId>
        <type value="oid"/>
        <value value="{$dotNotation}"/>
        <xsl:if test="$uri">
          <preferred value="false"/>
        </xsl:if>
      </uniqueId>
      <xsl:if test="$mnemonic">
        <uniqueId>
          <type value="other"/>
          <value value="{$mnemonic}"/>
          <preferred value="false"/>
          <comment value="HL7 V2 Table 0396"/>
        </uniqueId>
      </xsl:if>
    </NamingSystem>
  </xsl:template>
  
  <xsl:function name="local:categoryToKind" as="xs:string">
    <xsl:param name="category" as="xs:string"/>
    <xsl:choose>
      <!--1 - OID for an HL7 Internal Object-->
      <xsl:when test="$category = 1">root</xsl:when>
      <!--2 - OID for an HL7 Body or Group-->
      <xsl:when test="$category = 2">root</xsl:when>
      <!--3 - Root to be a Registration Authority-->
      <xsl:when test="$category = 3">root</xsl:when>
      <!--4 - OID for a Registered Namespace-->
      <xsl:when test="$category = 4">identifier</xsl:when>
      <!--5 - OID for an HL7 Internal Code System-->
      <xsl:when test="$category = 5">codesystem</xsl:when>
      <!--6 - OID for an External Code System-->
      <xsl:when test="$category = 6">codesystem</xsl:when>
      <!--7 - OID for an HL7 Document-->
      <xsl:when test="$category = 7">root</xsl:when>
      <!--8 - OID for an HL7 Document Artifact-->
      <xsl:when test="$category = 8">root</xsl:when>
      <!--9 - OID for an HL7 Conformance Profile-->
      <xsl:when test="$category = 9">identifier</xsl:when>
      <!--10 - OID for an HL7 Template-->
      <xsl:when test="$category = 10">root</xsl:when>
      <!--11 - OID for an HL7 Internal Value Set-->
      <xsl:when test="$category = 11">root</xsl:when>
      <!--12 - OID for a Version 2.x Table-->
      <xsl:when test="$category = 12">root</xsl:when>
      <!--13 - OID for an External Value Set-->
      <xsl:when test="$category = 13">root</xsl:when>
      <!--14 - branch node subtype-->
      <xsl:when test="$category = 14">root</xsl:when>
      <!--15 - Defined external codesets-->
      <xsl:when test="$category = 15">root</xsl:when>
      <!--17 - Other Type OID-->
      <xsl:when test="$category = 17">root</xsl:when>
      <!--18 - OID for a Version 2.x Coding System-->
      <xsl:when test="$category = 18">codesystem</xsl:when>
      <!--19 - OID for a published HL7 Example-->
      <xsl:when test="$category = 19">root</xsl:when>
      <xsl:when test="lower-case($category) = 'codesystem'">codesystem</xsl:when>
      <xsl:when test="lower-case($category) = 'identifier'">identifier</xsl:when>
      <xsl:otherwise>root</xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <xsl:function name="local:decorStatus2fhirStatus" as="xs:string">
    <xsl:param name="status" as="xs:string"/>
    <xsl:choose>
      <xsl:when test="$status = 'pending'">draft</xsl:when>
      <xsl:when test="$status = 'completed'">active</xsl:when>
      <xsl:when test="$status = 'retired'">retired</xsl:when>
      <xsl:when test="$status = 'deprecated'">retired</xsl:when>
      <xsl:otherwise>unknown</xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <xsl:function name="local:fhirStatus2decorStatus" as="xs:string">
    <xsl:param name="status" as="xs:string"/>
    <xsl:choose>
      <xsl:when test="$status = 'draft'">draft</xsl:when>
      <xsl:when test="$status = 'active'">completed</xsl:when>
      <xsl:when test="$status = 'retired'">retired</xsl:when>
      <xsl:otherwise>unknown</xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <!-- Return valid .name according to inv-0 pattern name.matches('[A-Z]([A-Za-z0-9_]){0,254}') -->
  <xsl:function name="local:validResourceName" as="xs:string?">
    <xsl:param name="nameString" as="xs:string?"/>
    
    <xsl:choose>
      <xsl:when test="string-length($nameString) = 0"/>
      <xsl:when test="matches($nameString, '^[A-Z]([A-Za-z0-9_]){0,254}$')"><xsl:value-of select="$nameString"/></xsl:when>
      <xsl:otherwise>
        <!-- guarantee characters -->
        <!-- find matching alternatives for <=? smaller(equal) and >=? greater(equal) -->
        <xsl:variable name="r1" select="replace($nameString, '&lt;\s*=', 'le')"/>
        <xsl:variable name="r2" select="replace($r1, '&lt;', 'lt')"/>
        <xsl:variable name="r3" select="replace($r2, '&gt;\s*=', 'ge')"/>
        <xsl:variable name="r4" select="replace($r3, '&gt;', 'gt')"/>
        
        <!-- find matching alternatives for more or less common diacriticals, replace single spaces with _ , replace ? with q (same name occurs quite often twice, with and without '?') -->
        <xsl:variable name="r5" select="translate(normalize-space($r4),' ÀÁÃÄÅÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝŸÇàáãäåèéêëìíîïòóôõöùúûüýÿç€ßñ?-','_AAAAAEEEEIIIIOOOOOUUUUYYCaaaaaeeeeiiiiooooouuuuyycEsnq_')"/>
        
        <!-- ditch anything that's not alpha numerical or underscore -->
        <xsl:variable name="r6" select="replace($r5,'[^A-Za-z\d_]','')"/>
        
        <!-- guarantee initial A-Z -->
        <xsl:variable name="firstChar" select="substring($r6, 1, 1)"/>
        <xsl:variable name="r7" select="if (matches($firstChar, '[A-Za-z]')) then concat(upper-case($firstChar), substring($r6, 2)) else concat('A', $r6)"/>
        
        <!-- guarantee length -->
        <xsl:variable name="r8" select="substring($r7, 1, 254)"/>
        
        <xsl:value-of select="$r8"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <!-- HL7 V3 TS yyyyMMddhhmmss+-ZZzz to ISO 8601 yyyy-mm-ddThh:mm:ss -->
  <xsl:function name="local:dateTimeFromTimestamp" as="xs:string">
    <xsl:param name="ts" as="xs:string?"/>
    
    <xsl:choose>
      <xsl:when test="string-length($ts) = 0"><xsl:value-of select="string(current-dateTime())"/></xsl:when>
      <xsl:otherwise>
        <xsl:variable name="year" select="if (substring($ts,1,1)='-') then substring($ts,1,5) else substring($ts,1,4)"/>
        <xsl:variable name="month" select="if (substring($ts,1,1)='-') then substring($ts,6,2) else substring($ts,5,2)"/>
        <xsl:variable name="day" select="if (substring($ts,1,1)='-') then substring($ts,8,2) else substring($ts,7,2)"/>
        <xsl:variable name="hour" select="if (substring($ts,1,1)='-') then substring($ts,10,2) else substring($ts,9,2)"/>
        <xsl:variable name="minute" select="if (substring($ts,1,1)='-') then substring($ts,12,2) else substring($ts,11,2)"/>
        <xsl:variable name="second" select="if (substring($ts,1,1)='-') then substring($ts,14,2) else substring($ts,13,2)"/>
        <!--<xsl:variable name="subsecond" select="replace($ts,'^\d{14}(\.\d+)?.*$','$1')"/>
        <xsl:variable name="subsecond" select="$subsecond[string-length()>0]"/>-->
        <xsl:variable name="timezone" select="replace($ts,'^-?\d+(([+\-]\d{2})(\d{2}))?$','$2:$3')"/>
        <xsl:variable name="timezone" select="$timezone[string-length() gt 1]"/>
        
        <xsl:variable name="date" select="string-join(($year[string-length() ge 4], $month[string-length() = 2], $day[string-length() = 2]),'-')"/>
        <xsl:variable name="timenotz" select="string-join(($hour[string-length() = 2], $minute[$hour[string-length() = 2]][string-length() = 2], $second[$minute[string-length() = 2]][string-length() = 2]),':')"/>
        <xsl:variable name="time" select="string-join(($timenotz, $timezone),'')"/>
        
        <xsl:value-of select="if ($date and $time) then string-join(($date,$time),'T') else $date"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
</xsl:stylesheet>