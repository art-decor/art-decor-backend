xquery version "3.1";
(:
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace adfix       = "http://art-decor.org/ns/fhir-permissions" at "api/api-permissions.xqm";
import module namespace getf        = "http://art-decor.org/ns/fhir-settings" at "api/fhir-settings.xqm";

declare namespace xmldb             = "http://exist-db.org/xquery/xmldb";
declare namespace sm                = "http://exist-db.org/xquery/securitymanager";
declare namespace repo              = "http://exist-db.org/xquery/repo";
declare namespace f                 = "http://hl7.org/fhir";

(: The following external variables are set by the repo:deploy function :)

(:  home="/Applications/eXist-db" 
    dir="/Applications/eXist-db/webapp/WEB-INF/data/expathrepo/ART-1.1.37/." 
    target="/db/apps/art" 
    root="/db/apps/"
:)

(: file path pointing to the exist installation directory :)
declare variable $home external;
(: path to the directory containing the unpacked .xar package :)
declare variable $dir external;
(: the target collection into which the app is deployed :)
declare variable $target external;
(:install path for art (/db, /db/apps), no trailing slash :)
declare variable $root := repo:get-root();

declare %private function local:copy($source as xs:string, $target as xs:string, $base as xs:boolean) {
let $dirName       := tokenize($source,'/')[last()]
let $targetDirName := if ($base) then $target else (concat($target,'/',$dirName))
let $createDir     := 
    if (xmldb:collection-available($targetDirName)) then () else (
        xmldb:create-collection(string-join(tokenize($targetDirName,'/')[not(position()=last())],'/'),tokenize($targetDirName,'/')[last()])
    )
let $copyResources :=
    for $r in xmldb:get-child-resources($source)
    return
        if ($r='Conformance.xml') then () else
        if ($r='CapabilityStatement.xml') then () else
        if (xmldb:get-child-resources($targetDirName)[. = $r]) then () else (
            xmldb:copy-resource($source, $r, $targetDirName, $r)
        )
let $copyCollections :=
    for $c in xmldb:get-child-collections($source)
    return
        local:copy(concat($source,'/',$c),$targetDirName, false())

return ()
};

(: check if message collection exists, if not then create and set permissions :)
xmldb:copy-resource(concat($target,'/install-data'), 'controller.xql', '../..', 'controller.xql'),
xmldb:copy-resource(concat($target,'/install-data'), 'helper.xq', '../..', 'helper.xq'),
sm:chmod(xs:anyURI(concat($target,'/../controller.xql')), 'rwxrwxr-x'),
sm:chmod(xs:anyURI(concat($target,'/../helper.xq')), 'rwxrwxr-x'),
local:copy(concat($target,'/install-data/fhir-data'),$getf:strFhirResources, true()),
local:copy(concat($target,'/install-data/definitions'),$getf:strFhirDefinitions, true()),
adfix:setPermissions()