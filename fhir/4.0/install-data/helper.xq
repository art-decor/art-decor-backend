xquery version "3.1";
import module namespace adserver       = "http://art-decor.org/ns/art-decor-server" at "../art/api/api-server-settings.xqm";
declare variable $exist-path    := request:get-attribute('exist-path');

declare %private function local:doOperationOutcome($urlFormat as xs:string?, $acceptHeader as xs:string?, $errorText as xs:string*) {
    if (contains($urlFormat, 'json') or contains($acceptHeader, 'json')) then (
        response:set-header('Content-Type', 'application/fhir+json'),
        serialize(
            <fn:map xmlns:fn="http://www.w3.org/2005/xpath-functions">
                <fn:string key="resourceType">OperationOutcome</fn:string>
                <fn:array key="issue">
                    <fn:map>
                        <fn:string key="severity">error</fn:string>
                        <fn:string key="code">code-invalid</fn:string>
                        <fn:map key="details">
                            <fn:string key="text">{$errorText}</fn:string>
                        </fn:map>
                    </fn:map>
                </fn:array>
            </fn:map>, map { "method": "json"}
        )
    )
    else (
        response:set-header('Content-Type', 'application/fhir+xml'),
        <OperationOutcome xmlns="http://hl7.org/fhir">
            <issue>
                <severity value="fatal"/>
                <code value="exception"/>
                <details>
                    <text value="{$errorText}"/>
                </details>
            </issue>
        </OperationOutcome>
    )
};

let $supported-packages                 := sort(xmldb:get-child-collections(concat(repo:get-root(), '/fhir')))
let $defaultFhirVersion                 := try { adserver:getServerFhirDefaultVersion() } catch * {()}
(:  Input   Accept: application/fhir+json;fhir-version=X
    Output  X
    
    X could be a specific version, 'any version' (*), and a comma separated list of those. first in list has higher priority than last
:)
let $urlFormat                          := (request:get-parameter('_format', ())[not(. = '')])[1]
let $acceptHeader                       := request:get-header('Accept')
let $requestedFhirVersions              := 
    for $v in tokenize(replace(normalize-space(tokenize($acceptHeader, ';')[matches(., 'fhirVersion=')][1]), '^fhirVersion=(.*)$', '$1'), ',')
    return if (string-length($v) gt 0) then normalize-space($v) else ()
    
let $requestedAndSupportedFhirVersions  := (
    if (empty($requestedFhirVersions)) then $defaultFhirVersion else ( 
        for $v in $requestedFhirVersions return 
        if ($v = $supported-packages) then $v else if ($v = '*') then $defaultFhirVersion else ()
    )
)
let $versionsOperation                 := starts-with($exist-path, '/$versions')

return
    (:if (1=1) then <path versionsOperation="{$versionsOperation}" supportedPackages="{$supported-packages}" requestedFhirVersions="{$requestedFhirVersions}" requestedAndSupportedFhirVersions="{$requestedAndSupportedFhirVersions}">{$exist-path}</path> else:)
    if ($versionsOperation) then (
        if (contains($urlFormat, 'json') or contains($acceptHeader, 'application/fhir+json') or contains($acceptHeader, 'application/json+fhir')) then (
            response:set-header('Content-Type', 'application/fhir+json'),
            serialize(
            <fn:map xmlns:fn="http://www.w3.org/2005/xpath-functions">
                <fn:string key="resourceType">Parameters</fn:string>
                <fn:array key="parameter">
                {
                    for $v in $supported-packages
                    return
                        <fn:map>
                            <fn:string key="name">version</fn:string>
                            <fn:string key="valueString">{$v}</fn:string>
                        </fn:map>
                }
                </fn:array>
            </fn:map>, map { "method": "json"})
        )
        else
        if (contains($urlFormat, 'xml') or contains($acceptHeader, 'application/fhir+xml') or contains($acceptHeader, 'application/xml+fhir')) then (
            response:set-header('Content-Type', 'application/fhir+xml'),
            <Parameters xmlns="http://hl7.org/fhir">
            {
                for $v in $supported-packages
                return
                <parameter>
                    <name value="version"/>
                    <valueString value="{$v}"/>
                </parameter>
            }
            </Parameters>
        )
        else
        if (contains($acceptHeader, 'json')) then (
            response:set-header('Content-Type', 'application/json'),
            serialize(<fn:map xmlns:fn="http://www.w3.org/2005/xpath-functions">
                <fn:array key="versions">
                {
                    for $v in $supported-packages
                    return
                        <fn:string key="valueString">{$v}</fn:string>
                }
                </fn:array>
            </fn:map>, map { "method": "json"})
        )
        else (
            <versions>{for $v in $supported-packages return <version>{$v}</version>}</versions>
        )
    )
    else
    (: we used to have a /fhir/dstu2/ and a /fhir/stu3 endpoint. we now have /fhir/1.0/, fhir/3.0/, fhir/4.0/ endpoint. reroute that first :)
    if (matches($exist-path, '^/dstu2/public') and $supported-packages[. = '1.0']) then (
        response:redirect-to(xs:anyURI(string-join((concat(adserver:getServerURLFhirServices(), replace($exist-path, '/dstu2/', '')), request:get-query-string()), '?')))
    )
    else
    if (matches($exist-path, '^/stu3/public') and $supported-packages[. = '3.0']) then (
        response:redirect-to(xs:anyURI(string-join((concat(adserver:getServerURLFhirServices(), replace($exist-path, '/stu3/', '')), request:get-query-string()), '?')))
    )
    else
    (: if we get /<...>/public then it must be because the requested FHIR endpint is not supported, otherwise you would be sent to that controller :)
    if (matches($exist-path, '^/[^/]+/public')) then (
        response:set-status-code(501), (: not implemented :)
        let $errorText  := 'There is no HL7® FHIR® service installed on this endpoint that matches the version(s) you requested (' || 
                            replace($exist-path, '^/([^/]+).*$', '$1') || 
                           '). Please contact the administrator for more information. The following HL7® FHIR® service endpoints are available: ' ||
                            string-join(for $endpoint in $supported-packages return concat($endpoint, '/public/'), ' ')
        return
            local:doOperationOutcome($urlFormat, $acceptHeader, $errorText)
    )
    else
    (: in the beginning we only had a /fhir/ endpoint. we now have a versioned endpoint. Reroute there if possible, 
       or suggest alternative endpoints. If supported-packages is empty then no FHIR server lives here.
    :)
    if ($supported-packages[. = $requestedAndSupportedFhirVersions]) then (
        (: if we get a path like /public/.. or /demo1-/..., then all we need to do is insert the right version for rerouting.
           otherwise we need to add /public to the path too.
           
           Note that this assumes that all FHIR resources start with a capital, and all ART-DECOR prefixes and registry names start with anything else
        :)
        if (matches($exist-path, '^/[^A-Z]')) then 
            (:<seeyou1 at="{xs:anyURI(string-join((concat(adserver:getServerURLFhirServices(), $requestedAndSupportedFhirVersions[1], $exist-path), request:get-query-string()), '?'))}"/>:)
            response:redirect-to(xs:anyURI(string-join((concat(adserver:getServerURLFhirServices(), $requestedAndSupportedFhirVersions[1], $exist-path), request:get-query-string()), '?')))
        else (
            (:<seeyou2 at="{xs:anyURI(string-join((concat(adserver:getServerURLFhirServices(), $requestedAndSupportedFhirVersions[1], '/public', $exist-path), request:get-query-string()), '?'))}"/>:)
            response:redirect-to(xs:anyURI(string-join((concat(adserver:getServerURLFhirServices(), $requestedAndSupportedFhirVersions[1], '/public', $exist-path), request:get-query-string()), '?')))
        )
    )
    else
    if (empty($supported-packages)) then (
        response:set-status-code(501), (: not implemented :)
        let $errorText  := 'There is no HL7® FHIR® service installed on this endpoint. Please contact the administrator for more information.'
        return
            local:doOperationOutcome($urlFormat, $acceptHeader, $errorText)
    )
    else
    if (string-length($defaultFhirVersion) = 0) then (
        response:set-status-code(500),
        let $errorText  := 'The default endpoint has not been configured. Please ask your server administrator to set this up. The following HL7® FHIR® service endpoints are available: ' ||
                            string-join(for $endpoint in $supported-packages return concat($endpoint, '/public/'), ' ')
        return (
            local:doOperationOutcome($urlFormat, $acceptHeader, $errorText)
        )
    )
    else 
    if (empty($requestedFhirVersions)) then (
        response:set-status-code(500),
        let $errorText  := 'The default endpoint ' || $defaultFhirVersion || ' does not exist. Please ask your server administrator to set this up. The following HL7® FHIR® service endpoints are available: ' ||
                            string-join(for $endpoint in $supported-packages return concat($endpoint, '/public/'), ' ')
        return
            local:doOperationOutcome($urlFormat, $acceptHeader, $errorText)
    )
    else (
        response:set-status-code(501), (: not implemented :)
        let $errorText  := 'There is no HL7® FHIR® service installed on this endpoint that matches the version(s) you requested (' || $requestedFhirVersions || '). Please contact the administrator for more information. The following HL7® FHIR® service endpoints are available: ' ||
                            string-join(for $endpoint in $supported-packages return concat($endpoint, '/public/'), ' ')
        return
            local:doOperationOutcome($urlFormat, $acceptHeader, $errorText)
    )