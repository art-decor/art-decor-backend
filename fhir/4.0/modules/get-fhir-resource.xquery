xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adfhir      = "http://art-decor.org/ns/fhir/4.0" at "../api/api-fhir.xqm";
import module namespace adfhircm    = "http://art-decor.org/ns/fhir/4.0/conceptmap" at "../api/api-fhir-conceptmap.xqm";
import module namespace adfhirns    = "http://art-decor.org/ns/fhir/4.0/namingsystem" at "../api/api-fhir-namingsystem.xqm";
import module namespace adfhirsd    = "http://art-decor.org/ns/fhir/4.0/structuredefinition" at "../api/api-fhir-structuredefinition.xqm";
import module namespace adfhircs    = "http://art-decor.org/ns/fhir/4.0/codesystem" at "../api/api-fhir-codesystem.xqm";
import module namespace adfhirvs    = "http://art-decor.org/ns/fhir/4.0/valueset" at "../api/api-fhir-valueset.xqm";
import module namespace adfhirq     = "http://art-decor.org/ns/fhir/4.0/questionnaire" at "../api/api-fhir-questionnaire.xqm";
import module namespace getf        = "http://art-decor.org/ns/fhir-settings" at "../api/fhir-settings.xqm";
import module namespace utillib     = "http://art-decor.org/ns/api/util" at "../../../api/modules/library/util-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "../../../api/modules/library/settings-lib.xqm";
import module namespace cs          = "http://art-decor.org/ns/decor/codesystem" at "../../../art/api/api-decor-codesystem.xqm";
import module namespace vs          = "http://art-decor.org/ns/decor/valueset" at "../../../art/api/api-decor-valueset.xqm";
import module namespace templ       = "http://art-decor.org/ns/decor/template" at "../../../art/api/api-decor-template.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../../art/api/api-server-settings.xqm";

declare namespace http              = "http://expath.org/ns/http-client";
declare namespace f                 = "http://hl7.org/fhir";

declare %private function local:fhirStatus2decorStatus($status as xs:string?) as xs:string {
    switch ($status)
    case 'draft'        return 'draft'
    case 'active'       return 'final'
    case 'retired'      return 'deprecated'
    default             return 'unknown'
};

declare %private function local:convertDecorOIDEntry2DecorCodeSystem($oid as element(oid), $uri as xs:string?, $language as xs:string) as element() {
    let $dotNotation    := $oid/dotNotation/@value
    let $uri            := if (empty($uri)) then $oid/additionalProperty[attribute[@value = $setlib:strKeyCanonicalUriPrefdR4]]/value/@value else $uri
    let $uri            := if (empty($uri)) then $oid/additionalProperty[attribute[@value = $setlib:strKeyCanonicalUriPrefd]]/value/@value else $uri
    let $status         := 
        switch ($oid/status/@code)
        case 'pending'      return 'pending'
        case 'completed'    return 'final'
        case 'retired'      return 'deprecated'
        case 'deprecated'   return 'deprecated'
        default             return 'unknown'
    let $creationDate   := adfhir:dateTimeFromTimestamp($oid/creationDate/@value)
    let $name           := $oid/description[1]/thumbnail/@value
    let $name           := if (empty($name)) then $oid/description[1]/@value else $name
    let $name           := if (empty($name)) then 'Unknown name' else $name
    let $description    := $oid/description[1]/@value
    let $publisher      := $oid/ancestor::registry/hostingOrganization/name/part/@value
    return
    <codeSystem>
    {
        attribute id {$dotNotation},
        attribute name {adfhir:validResourceName($name)},
        attribute displayName {$name},
        attribute effectiveDate {substring($creationDate, 1, 19)},
        attribute statusCode {$status},
        if (empty($uri)) then () else attribute canonicalUri {$uri} 
    }
        <desc language="{$language}">{data($description)}</desc>
        <publishingAuthority name="{if ($publisher) then data($publisher) else 'ART-DECOR'}"/>
    </codeSystem>
};

let $requestParts       := request:get-attribute('request.parts')
let $doSearch           := request:get-attribute('request.subtype')=($getf:RFINT_SEARCH_TYPE, $getf:RFINT_SEARCH_SYSTEM)
let $doMetadata         := request:get-attribute('request.subtype')=$getf:RFINT_CAPABILITIES
let $doOperation        := $requestParts/(@oper1|@oper2|@oper3)[starts-with(.,'$')][1]/string()
let $resourcePath       := request:get-attribute('request.path')
let $projectprefix      := request:get-attribute('request.projectprefix')[string-length()>0]
let $projectversion     := request:get-attribute('request.projectversion')[string-length()>0]
let $resourceName       := request:get-attribute('request.resource')[string-length()>0]
let $basePath           := if (empty($resourceName)) then ('body/f:*') else (concat('body/f:',$resourceName))
let $id                 := request:get-attribute('request.id')[string-length()>0]
let $version            := request:get-attribute('request.version')[string-length()>0]
let $offset             := request:get-attribute('response.offset')
let $offset             := 
    if ($offset castable as xs:integer) then 
        if (xs:integer($offset) gt 0) then xs:integer($offset) else 1
    else ()
let $count              := request:get-attribute('response.count')
let $count              := 
    if ($count castable as xs:integer) then 
        if (xs:integer($count) gt 0) then xs:integer($count) else 0
    else 50
let $since              := request:get-parameter('_since',())[. castable as xs:dateTime]
let $sort               := request:get-parameter('_sort',())[string-length()>0]
let $_now               := current-dateTime()
let $response-extension := adfhir:getResponseExtension(request:get-attribute('response.format'))

let $result             := 
    try {
        (: first we try to get stuff by id :)
        let $resources  :=
            switch ($resourceName)
            case 'Bundle'       return (
                adfhir:getResourceSnapshot($resourceName, $id, $offset, $count)/descendant-or-self::f:*[1]
            )
            case 'StructureDefinition' return (
                let $url                := request:get-parameter('url',())[string-length()>0][1]
                let $language           := request:get-parameter('language','*')[string-length()>0][1]
                let $sdid               := if (matches($id, concat('^\d+(\.\d+)*',$getf:PARAMDECOR_ID_VERSION_SEPCHARS))) then substring-before($id,$getf:PARAMDECOR_ID_VERSION_SEPCHARS) else $id
                let $sded               := if (matches($id, concat('^\d+(\.\d+)*',$getf:PARAMDECOR_ID_VERSION_SEPCHARS))) then substring-after($id, $getf:PARAMDECOR_ID_VERSION_SEPCHARS) else 'dynamic'
                (: FHIR id may not have colons. This affects using @effectiveDate as part of the StructureDefinition.id
                   Reset the colons if omitted so 2016-10-26T123456 is reset to 2016-10-26T12:34:56
                   Decided to support every variation so even 20161026123456 is supported
                :)
                let $sded               := adfhir:hl7TS2dateTime($sded)
                
                let $conceptid          := ()
                let $concepted          := ()
                let $object             := 
                    if (empty($sdid)) then () else (
                        if (empty($projectprefix)) then (
                            let $objects    :=
                                $setlib:colDecorData//dataset[@id = $sdid] | $setlib:colDecorData//transaction[@id = $sdid] | $setlib:colDecorData//concept[@id = $sdid] |
                                $setlib:colDecorCache//dataset[@id = $sdid] | $setlib:colDecorCache//transaction[@id = $sdid] | $setlib:colDecorCache//concept[@id = $sdid]
                            return
                                if ($sded castable as xs:dateTime) then $objects[@effectiveDate = $sded] else (
                                    $objects[@effectiveDate = max($objects/xs:dateTime(@effectiveDate))]
                                )
                        )
                        else (
                            utillib:getDataset($sdid, $sded, $projectversion, $language) | 
                            utillib:getTransaction($sdid, $sded, $projectversion, $language) | 
                            utillib:getConcept($sdid, $sded, $projectversion, $language)
                        )
                    )[1]
                let $sdid               := if ($object[local-name()='concept']) then $object/ancestor::*:dataset/@id else if ($object[@id]) then $object/@id else ($sdid)
                let $sded               := if ($object[local-name()='concept']) then $object/ancestor::*:dataset/@effectiveDate else if ($object[@effectiveDate]) then $object/@effectiveDate else ($sded)
                let $conceptid          := if ($object[local-name()='concept']) then $object/@id else $conceptid
                let $concepted          := if ($object[local-name()='concept']) then $object/@effectiveDate else $concepted
                
                return
                    if (empty($url)) then (
                        if ($object) then (
                            let $fullDatasetTree        := 
                                if ($projectversion castable as xs:dateTime) then (
                                    let $datasets   := 
                                        if ($object[local-name()='dataset']) then ($object) else 
                                        if ($object[local-name()='concept']) then ($object) else (
                                            if ($language = '*') then
                                                $setlib:colDecorVersion//transactionDatasets[@versionDate = $projectversion]//dataset[@transactionId = $object/@id][@transactionEffectiveDate = $object/@effectiveDate]
                                            else (
                                                $setlib:colDecorVersion//transactionDatasets[@versionDate = $projectversion][@language = $language]//dataset[@transactionId = $object/@id][@transactionEffectiveDate = $object/@effectiveDate]
                                            )
                                        )
                                 
                                    let $datasets   :=
                                        if ($object[local-name()='concept']) then (
                                            for $concept in $datasets
                                            let $dataset    := $concept/ancestor::dataset
                                            return
                                                element {$dataset/name()} {
                                                    $dataset/@*,
                                                    $dataset/(* except (concept|history)),
                                                    $concept
                                                }
                                        )
                                        else ($datasets)
                                 
                                    return $datasets[1]
                                ) else 
                                if ($object) then (
                                    utillib:getFullDatasetTree($object, $conceptid, $concepted, $language, (), false(), ())
                                ) else ()
                            
                            return
                                adfhirsd:convertDecorDataset2FHIRStructureDefinition($fullDatasetTree) (:<x v="{$projectversion}"><o>{$object}</o><f>{$fullDatasetTree}</f></x>:)
                        ) else ()
                    ) else (
                        adfhir:getResource($resourceName,$sdid,$sded)/body/f:StructureDefinition[f:url[@value=$url]]
                    )
            )
            default             return adfhir:getResource($resourceName, $id, $version)
         
        let $headers    := ()
        (:let $headers    := 
            if (request:get-parameter($getf:PARAM_FORMAT,())[.=('text/xml','application/xml')]) then
                <header name="Content-Type" value="{request:get-parameter($getf:PARAM_FORMAT,())[1]}{$getf:HEADER_SUFFIX_CT_UTF_8}"/>
            else (
                <header name="Content-Type" value="{$getf:CT_FHIR_XML}{$getf:HEADER_SUFFIX_CT_UTF_8}"/>
            )
        let $r          := request:set-attribute('response.headers',$headers):)
        
        return
        if ($offset castable as xs:integer) then (
            if (count($resources)=1) then (
                let $hdrs   := $headers |
                               <header name="Last-Modified" value="{adfhir:dateTime2httpDate($resources[1]/@lastupdated)}"/>
                let $r      := request:set-attribute('response.headers', $hdrs)
                let $r      := request:set-attribute('response.http-status',200)
                
                return $resources/descendant-or-self::f:*[1]
            )
            else if (empty($resources)) then (
               let $r      := request:set-attribute('response.http-status',404)
               return ()
            )
            else (
                error(QName('http://art-decor.org/ns/fhir/error','ResourceAmbiguous'), concat('Resource occurred multiple times. Read is ambiguous. type=',$resourceName,' _id=',$id))
            )
        ) else 
        if ($resourceName = 'StructureDefinition' and $doOperation = '$template-its') then (
            (: example call HTTP POST for CDA document level template from demo5- 
            curl -L --post302 -H "Content-Type: application/xml" -o template-its-output.xml -d @template-its-params.xml http://localhost:8877/fhir/StructureDefinition/\$template-its
            with these file contents:
            <Parameters xmlns="http://hl7.org/fhir">
                <parameter>
                    <name value="templateId"/>
                    <valueString value="2.16.840.1.113883.3.1937.99.60.5.4.101"/>
                </parameter>
                <!--<parameter>
                    <name value="templateEffectiveDate"/>
                    <valueString value="2014-07-08T00:00:00"/>
                </parameter>-->
            </Parameters>
            :)
            let $parameters     := request:get-data()/f:Parameters
            let $paramid        := $parameters/f:parameter[f:name/@value='templateId']/f:valueString/@value
            let $paramed        := $parameters/f:parameter[f:name/@value='templateEffectiveDate']/f:valueString/@value
            let $unsupportedParameterNames  := request:get-parameter-names()
            let $requiredParameters         := ('templateId')
            let $operationOutcomeEntry      := 
                if (count($unsupportedParameterNames) = 0) then () else (
                    let $operationOutcome   :=
                        adfhir:operationOutCome($getf:OPERATIONOUTCOME_SEVERITY_WARNING,(),'MSG_PARAM_UNKNOWN', concat('Unsupported parameters "',string-join($unsupportedParameterNames,', '),'"'), 'None of these were used in compiling the result. Please send any parameters in the payload.',())
                    return
                    <entry type="OperationOutcome" id="{$operationOutcome/f:id/@value}" version="1" original-mime-type="{$getf:CT_FHIR_XML}" created="{$_now}" lastupdated="{$_now}" fhirVersion="{$getf:strFhirVersion}">
                        <body>{$operationOutcome}</body>
                    </entry>
                )
            let $operationOutcomeEntry := 
                if (empty($paramid)) then (
                    let $operationOutcome   :=
                        adfhir:operationOutCome($getf:OPERATIONOUTCOME_SEVERITY_WARNING,(),concat('Insufficient parameters. Required parameters are "',string-join($requiredParameters,', '),'".'),())
                    return
                    $operationOutcomeEntry | 
                    <entry type="OperationOutcome" id="{$operationOutcome/f:id/@value}" version="1" original-mime-type="{$getf:CT_FHIR_XML}" created="{$_now}" lastupdated="{$_now}" fhirVersion="{$getf:strFhirVersion}">
                        <body>{$operationOutcome}</body>
                    </entry>
                ) else ($operationOutcomeEntry)
            
            let $objects        := 
                if (empty($paramid)) then () else (
                    if (empty($projectprefix)) then (
                        (: check if this is a transaction :)
                        let $objects    := utillib:getTransaction($paramid, $paramed)
                        let $templateId := if ($objects) then $objects/representingTemplate/@ref else $paramid
                        let $templateEd := if ($objects) then $objects/representingTemplate/@flexibility else $paramed
                        
                        let $objects    := $setlib:colDecorCache//template[@id = $templateId] | $setlib:colDecorData//template[@id = $templateId]
                        return
                            if ($templateEd castable as xs:dateTime) then $objects[@effectiveDate = $templateEd] else (
                                $objects[@effectiveDate = max($objects/xs:dateTime(@effectiveDate))][1]
                            )
                    ) 
                    else (
                        (: check if this is a transaction :)
                        let $objects    := utillib:getTransaction($paramid, $paramed)
                        let $templateId := if ($objects) then $objects/representingTemplate/@ref else $paramid
                        let $templateEd := if ($objects) then $objects/representingTemplate/@flexibility else $paramed
                        return 
                            templ:getTemplateById($templateId, $templateEd, $projectprefix, $projectversion)/template[@id]
                    )
                )
            let $strucdefs  := adfhirsd:convertDecorTemplate2FHIRStructureDefinition($objects)
            
            let $operationOutcomeEntry := 
                if (empty($objects)) then (
                    let $operationOutcome   :=
                        adfhir:operationOutCome($getf:OPERATIONOUTCOME_SEVERITY_WARNING,(),concat('Could not find template with id ',string-join($paramid, ' '),if (empty($paramed)) then () else concat(', effectiveDate ', string-join($paramed, ' ')),if (empty($projectprefix)) then () else concat(', prefix ', string-join($projectprefix, ' '),' version ', string-join($projectversion, ' ')),'.'),())
                    return
                    $operationOutcomeEntry | 
                    <entry type="OperationOutcome" id="{$operationOutcome/f:id/@value}" version="1" original-mime-type="{$getf:CT_FHIR_XML}" created="{$_now}" lastupdated="{$_now}" fhirVersion="{$getf:strFhirVersion}">
                        <body>{$operationOutcome}</body>
                    </entry>
                ) else ($operationOutcomeEntry)
            
            let $subs       := (
                $operationOutcomeEntry, 
                for $sub in $strucdefs
                return
                    <entry type="{local-name($sub)}" id="{$sub/*:id/@value}" version="1" original-mime-type="{$getf:CT_FHIR_XML}" created="{$_now}" lastupdated="{$_now}" fhirVersion="{$getf:strFhirVersion}">
                        <body>{$sub}</body>
                    </entry>
            )
            
            let $hdrs       := $headers(: | <header name="X-Search-Expression" value="{$expr}"/>:) | <header name="{$getf:HEADER_CONTENT_DISPOSITION}" value="attachment; filename=Bundle-{$resourceName}{$response-extension}"/>
            let $r          := request:set-attribute('response.headers', $hdrs)
            let $r          := request:set-attribute('response.http-status',200)
            
            let $results    := adfhir:saveBundle($subs, $getf:BUNDLE_COLLECTION, $count, $sort)
            
            return
                adfhir:getResourceSnapshot($results/@type, $results/@id, $offset, $count)/descendant-or-self::f:*[1]
            
            (:return $sub/descendant-or-self::f:Bundle[1]:)
        ) else
        if ($resourceName = 'StructureDefinition' and $doOperation = '$mappingbundle') then (
            let $parameters     := request:get-data()/f:Parameters
            let $paramprefix    := $parameters/f:parameter[f:name/@value='prefix']/f:valueString/@value
            let $publicationurl := $parameters/f:parameter[f:name/@value='publicationurl']/f:valueString/@value
            let $unsupportedParameterNames
                                := request:get-parameter-names()
            let $operationOutcomeEntry
                                := if (count($unsupportedParameterNames) = 0) then () else (
                    let $operationOutcome   :=
                        adfhir:operationOutCome($getf:OPERATIONOUTCOME_SEVERITY_WARNING,(),'MSG_PARAM_UNKNOWN', concat('Unsupported parameters "',string-join($unsupportedParameterNames,', '),'"'), 'None of these were used in compiling the result. Please send any parameters in the payload.',())
                    return
                    <entry type="OperationOutcome" id="{$operationOutcome/f:id/@value}" version="1" original-mime-type="{$getf:CT_FHIR_XML}" created="{$_now}" lastupdated="{$_now}" fhirVersion="{$getf:strFhirVersion}">
                        <body>{$operationOutcome}</body>
                    </entry>
                )
            
            let $requestHeaders     := 
                <http:request method="GET" href="{$publicationurl}">
                    <http:header name="Accept" value="{$getf:CT_FHIR_XML}"/>
                    <http:header name="Content-Type" value="text/xml"/>
                    <http:header name="Cache-Control" value="no-cache"/>
                    <http:header name="Max-Forwards" value="1"/>
                </http:request>
            let $server-response    := http:send-request($requestHeaders)
            let $strucdefs          := $server-response//f:StructureDefinition
            let $allassocs          := utillib:getDecorByPrefix($paramprefix)//structuredefinition
                            
            let $sub                :=
                <entry type="Bundle" id="" version="1" original-mime-type="{$getf:CT_FHIR_XML}" created="{$_now}" lastupdated="{$_now}" fhirVersion="{$getf:strFhirVersion}">
                    <body>
                        <Bundle xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://hl7.org/fhir http://hl7.org/fhir/bundle.xsd" xmlns="http://hl7.org/fhir">
                            <meta>
                                <profile value="http://hl7.org/fhir/{$getf:strFhirVersionShort}/StructureDefinition/Bundle"/>
                            </meta>
                            <type value="collection"/>
                            {
                                let $strucdefs  :=
                                    for $strucdef at $i in $strucdefs
                                    let $canonicalurl       := $strucdef/f:url/@value
                                    let $basemodel          := collection($getf:strFhirXsdSch)//f:StructureDefinition[f:url/@value = $strucdef/f:base/@value]
                                    let $strucassocs        := $allassocs[@url = $canonicalurl]
                                    return (
                                        <!-- The focus resource -->,
                                        <entry id="focusResource{$i}">
                                            <fullUrl value="urn:focusResource:{$i}"/>
                                            <resource>{$strucdef}</resource>
                                        </entry>,
                                        <!-- The base resource -->,
                                        if ($basemodel) then (
                                            <entry id="baseResource{$i}">
                                                <fullUrl value="urn:baseResource:{$i}"/>
                                                <resource>{$basemodel}</resource>
                                            </entry>
                                        ) else ()
                                        ,
                                        comment { ' mapping Resource ' }
                                        ,
                                        if ($strucassocs[*:concept]) then (
                                            <entry id="mappingResource{$i}"  xmlns="http://hl7.org/fhir">
                                                <resource>
                                                    <Basic>
                                                        <meta>
                                                            <profile value="http://hl7.org/fhir/{$getf:strFhirVersionShort}/StructureDefinition/Basic"/>
                                                        </meta>
                                                        <extension url="sourceUriArtDecor">
                                                            <valueString value="{adserver:getServerURLArt()}"/>
                                                        </extension>
                                                        <extension url="sourceUriDecorServices">
                                                            <valueString value="{adserver:getServerURLServices()}"/>
                                                        </extension>
                                                        <extension url="sourceUriDecorServices">
                                                            <valueString value="{adserver:getServerURLFhirCanonicalBase()}"/>
                                                        </extension>
                                                        {
                                                            for $concept in $strucassocs/*:concept
                                                            let $conceptId      := $concept/@ref
                                                            let $conceptEd      := $concept/@effectiveDate
                                                            let $elementId      := $concept/@elementId
                                                            let $elementPath    := $concept/@elementPath
                                                            let $theConcept     := utillib:getConcept($conceptId, $conceptEd)
                                                            let $conceptEd      := $theConcept/@effectiveDate
                                                            let $theDataset     := $theConcept/ancestor::*:dataset
                                                            let $datasetId      := $theDataset/@id
                                                            let $datasetEd      := $theDataset/@effectiveDate
                                                            return
                                                                <extension url="mapping">
                                                                    <extension url="datasetId">
                                                                        <valueString value="{$datasetId}"/>
                                                                    </extension>
                                                                    <extension url="datasetEffectiveDate">
                                                                        <valueString value="{$datasetEd}"/>
                                                                    </extension>
                                                                    {
                                                                        for $name in $theDataset/*:name
                                                                        return
                                                                            <extension url="datasetName">
                                                                                <extension url="language">
                                                                                    <valueString value="{$name/@language}"/>
                                                                                </extension>
                                                                                <extension url="displayName">
                                                                                    <valueString value="{$name/node()}"/>
                                                                                </extension>
                                                                            </extension>
                                                                    }
                                                                    <extension url="conceptId">
                                                                        <valueString value="{$conceptId}"/>
                                                                    </extension>
                                                                    <!--<extension url="conceptIdDisplay">
                                                                        <valueString value="{utillib:getNameForOID($conceptId, (), ())}"/>
                                                                    </extension>-->
                                                                    <extension url="conceptEffectiveDate">
                                                                        <valueString value="{$conceptEd}"/>
                                                                    </extension>
                                                                    {
                                                                        for $name in $theConcept/*:name
                                                                        return
                                                                            <extension url="conceptName">
                                                                                <extension url="language">
                                                                                    <valueString value="{$name/@language}"/>
                                                                                </extension>
                                                                                <extension url="displayName">
                                                                                    <valueString value="{$name/node()}"/>
                                                                                </extension>
                                                                            </extension>
                                                                    }
                                                                    {
                                                                        if ($elementId[string-length()>0]) then
                                                                            <extension url="elementId">
                                                                                <valueString value="{$elementId}"/>
                                                                            </extension>
                                                                        else if ($elementPath[string-length()>0]) then
                                                                            <extension url="elementPath">
                                                                                <valueString value="{$elementPath}"/>
                                                                            </extension>
                                                                        else ()
                                                                    }
                                                                </extension>
                                                        }
                                                        <code>
                                                            <text value="ART-DECOR Dataset associations with a FHIR StructureDefinition"/>
                                                        </code>
                                                    </Basic>
                                                </resource>
                                            </entry>
                                        ) else ()
                                    )
                                 
                                 return (
                                    <total value="{count($operationOutcomeEntry | $strucdefs)}"/>
                                    ,
                                    $strucdefs
                                    ,
                                    if ($operationOutcomeEntry) then
                                        <entry id="focusResource">
                                            <fullUrl value="urn:focusResource"/>
                                            <resource>{$operationOutcomeEntry/body/*}</resource>
                                        </entry>
                                    else ()
                                )
                            }
                        </Bundle>
                    </body>
                </entry>
            
            let $hdrs       := $headers(: | <header name="X-Search-Expression" value="{$expr}"/>:) | <header name="{$getf:HEADER_CONTENT_DISPOSITION}" value="attachment; filename=Bundle-{$resourceName}{$response-extension}"/>
            let $r          := request:set-attribute('response.headers', $hdrs)
            let $r          := request:set-attribute('response.http-status',200)
            
            return $sub/descendant-or-self::f:Bundle[1]
        ) else
        (:else if ($resourceName='ValueSet' and $doOperation='$batch') then (
            let $r      := request:set-attribute('response.http-status',200)
            
            return adfhirvs:getExpandedValueSet($resources//f:ValueSet)
        )
        else if ($doOperation='$validate') then (
            let $r      := request:set-attribute('response.http-status',200)
            
            return adfhirvs:getExpandedValueSet($resources//f:ValueSet)
        )
        else if ($resourceName='ValueSet' and $doOperation='$validate-code') then (
            let $r      := request:set-attribute('response.http-status',200)
            
            return adfhirvs:getExpandedValueSet($resources//f:ValueSet)
        )
        else if ($resourceName='ValueSet' and $doOperation='$lookup') then (
            let $r      := request:set-attribute('response.http-status',200)
            
            return adfhirvs:getExpandedValueSet($resources//f:ValueSet)
        ):)
        if (starts-with($doOperation,'$')) then (
            error(QName('http://art-decor.org/ns/fhir/error','UnsupportedOperation'),concat('The operation ',$doOperation,' is not supported for resource ',$resourceName))
        )
        else if (count($resources)=1 and $doMetadata and $resources[empty(@deleted)]) then (
            let $hdrs   := $headers |
                           <header name="Location" value="{concat($getf:strFhirServices,$resourceName,'/',$resources[1]/encode-for-uri(@id),'/_history/',$resources[1]/encode-for-uri(@version))}"/> |
                           <header name="ETag" value="W/&quot;{$resources[1]/@version}&quot;"/> |
                           <header name="Last-Modified" value="{adfhir:dateTime2httpDate($resources[1]/@lastupdated)}"/> |
                           <header name="{$getf:HEADER_CONTENT_DISPOSITION}" value="attachment; filename={$resourceName}-{$resources[1]/@id}{$response-extension}"/>
            let $r      := request:set-attribute('response.headers', $hdrs)
            let $r      := request:set-attribute('response.http-status',200)
            
            return $resources[1]/body/f:*
        ) else 
        if ($doSearch and $resourceName = 'StructureDefinition') then (
            let $supportedParameterNames
                            := ('url', 'publicationurl')
            let $unsupportedParameterNames
                            := request:get-parameter-names()[not(. = $supportedParameterNames)]
            let $operationOutcomeEntry
                            := if (count($unsupportedParameterNames) = 0) then () else (
                    let $operationOutcome   :=
                        adfhir:operationOutCome($getf:OPERATIONOUTCOME_SEVERITY_WARNING,(),'MSG_PARAM_UNKNOWN', concat('Unsupported parameters "',string-join($unsupportedParameterNames,', '),'"'), 'None of these were used in compiling the result. Please send any parameters in the payload.',())
                    return
                    <entry type="OperationOutcome" id="{$operationOutcome/f:id/@value}" version="1" original-mime-type="{$getf:CT_FHIR_XML}" created="{$_now}" lastupdated="{$_now}" fhirVersion="{$getf:strFhirVersion}">
                        <body>{$operationOutcome}</body>
                    </entry>
                )
            
            let $url                := request:get-parameter('url',())[string-length()>0][1]
            let $puburl             := request:get-parameter('publicationurl',())[string-length()>0][1]
            let $puburl             := if ($puburl castable as xs:anyURI) then $puburl else if ($url castable as xs:anyURI) then $url else ()
            let $requestHeaders     := 
                <http:request method="GET" href="{$puburl}">
                    <http:header name="Accept" value="{$getf:CT_FHIR_XML}"/>
                    <http:header name="Content-Type" value="text/xml"/>
                    <http:header name="Cache-Control" value="no-cache"/>
                    <http:header name="Max-Forwards" value="1"/>
                </http:request>
            
            (: if we have it, great. if not, check the publication location :)
            let $results        :=  
                if ($resources) then ($resources) else (
                    if (empty($puburl)) then () else (
                        let $x  := http:send-request($requestHeaders)//f:*[1]
                        let $u  := 
                            if ($x[self::f:StructureDefinition]) then (
                                adfhir:saveResource($x,(), (), $getf:CT_FHIR_XML)
                            )
                            else ()
                        
                        return $x
                    )
                )
            let $server-status  := 
                if ($results[1][@status]) then $results[1]/xs:integer(@status) else 
                if ($results) then (200) else (404)
            let $hdrs           := $headers(: | <header name="X-Search-Expression" value="{$expr}"/>:) | <header name="{$getf:HEADER_CONTENT_DISPOSITION}" value="attachment; filename={$resourceName}{$response-extension}"/>
            let $r              := request:set-attribute('response.headers', $hdrs)
            let $r              := request:set-attribute('response.http-status',200)
            
            return
                $results
        ) else
        (:if ($doSearch) then (
            let $expr       := string-join(('$resources',local:searchDefaultParams($basePath),local:buildSearch($resourceName,$basePath)),'')
            let $hdrs       := $headers | <header name="X-Search-Expression" value="{$expr}"/> | <header name="{$getf:HEADER_CONTENT_DISPOSITION}" value="attachment; filename=Bundle-searchset-{$resourceName}{$response-extension}"/>
            let $r          := request:set-attribute('response.headers', $hdrs)
            
            let $sub        := util:eval($expr)
            let $sub        := local:doStringSearches($sub, $resourceName, $basePath)
            
            let $r          := request:set-attribute('response.http-status',200)
            
            let $results    := adfhir:saveBundle($sub, $getf:BUNDLE_SEARCHSET, $count, $sort)
            
            return
                adfhir:getResourceSnapshot($results/@type,$results/@id,$offset,$count)/descendant-or-self::f:*[1]
        ) else:) 
        if (empty($id)) then (
            let $r          := request:set-attribute('response.http-status',200)
            
            let $results    := adfhir:saveBundle($resources, $getf:BUNDLE_SEARCHSET, $count, $sort)
            
            return
                adfhir:getResourceSnapshot($results/@type,$results/@id,$offset,$count)/descendant-or-self::f:*[1]
        )
        else 
        if (count($resources) = 1) then (
            if ($resources[@deleted]) then (
                let $desc   := 'ERROR error:ResourceDeleted. Resource was deleted'
                let $r      := request:set-attribute('response.http-status',410)
                
                return adfhir:operationOutCome('error',(),$desc,())
            )
            else (
                let $resourceId         := ($resources/@id, $resources/descendant-or-self::f:*[1]/f:id/@value)[1]
                let $resourceVersion    := ($resources/@version)[1]
                let $hdrs   := $headers |
                                <header name="Location" value="{concat($getf:strFhirServices,$resourceName,'/',encode-for-uri($resourceId), if ($resourceVersion) then concat('/_history/',encode-for-uri($resourceVersion)) else ())}"/> |
                                <header name="ETag" value="W/&quot;{$resources[1]/@version}&quot;"/> |
                                <header name="Last-Modified" value="{adfhir:dateTime2httpDate($resources[1]/@lastupdated)}"/> |
                                <header name="{$getf:HEADER_CONTENT_DISPOSITION}" value="attachment; filename={string-join(($resourceName, $resources/descendant-or-self::f:*[1]/f:name[1]/@value, $resourceId), '-')}{$response-extension}"/>
                let $r      := request:set-attribute('response.headers', $hdrs)
                let $r      := request:set-attribute('response.http-status',200)
                
                return $resources/descendant-or-self::f:*[1]
            )
        )
        else  
        if (empty($resources)) then (
            let $r      := request:set-attribute('response.http-status',404)
            return ()
        )
        else (
            error(QName('http://art-decor.org/ns/fhir/error','ResourceAmbiguous'),concat('Resource occurred multiple (',count($resources),') times. Read is ambiguous. type=',$resourceName,' _id=',$id))
        )
    }
    (: Occurs when util:eval() fails. This might be due our own programming errors, but let's not go there :-) :)
    catch java:org.exist.xquery.StaticXQueryException {
        let $desc   := concat('ERROR ',$err:code,'. Could not retrieve result(s) using ',string-join(($resourcePath,request:get-query-string()),'?'),'''. Please check your parameters.')
        let $r      := request:set-attribute('response.http-status',400)
        
        return adfhir:operationOutCome('error',(),$desc,())
    }
    catch * {
        let $desc   := concat('ERROR ',$err:code,'. Could not retrieve result(s) using ',string-join(($resourcePath,request:get-query-string()),'?'),'''. ',$err:description,' module: ',$err:module,' (',$err:line-number,' ',$err:column-number,').')
        let $r      := request:set-attribute('response.http-status',500)
        
        return adfhir:operationOutCome('error',(),$desc,())
    }

let $r      := if (empty($result)) then () else request:set-attribute('response.contents', $result)

(:return single result. either single Resource, or a Bundle we've created:)
return
    $result