xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adfix       = "http://art-decor.org/ns/fhir-permissions" at "../api/api-permissions.xqm";

let $type   := if (request:exists()) then request:get-parameter('type', 'all')[string-length() gt 0][1] else ('capabilitystatement')

return
switch ($type)
case ('capabilitystatement') return adfix:refreshCapabilityStatement(())
case ('conformance') return adfix:refreshCapabilityStatement(())
case ('cleanup') return adfix:cleanupData()
default return adfix:setPermissions()