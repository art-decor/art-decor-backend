xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adfhir      = "http://art-decor.org/ns/fhir/4.0" at "../api/api-fhir.xqm";
import module namespace adfhirvs    = "http://art-decor.org/ns/fhir/4.0/valueset" at "../api/api-fhir-valueset.xqm";
import module namespace getf        = "http://art-decor.org/ns/fhir-settings" at "../api/fhir-settings.xqm";
import module namespace utillib     = "http://art-decor.org/ns/api/util" at "../../../api/modules/library/util-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "../../../api/modules/library/settings-lib.xqm";
import module namespace vsapi       = "http://art-decor.org/ns/api/valueset" at "../../../api/modules/valueset-api.xqm";
import module namespace tcsapi      = "http://art-decor.org/ns/api/terminology/codesystem" at "../../../api/modules/terminology-codesystem-api.xqm";

declare namespace http              = "http://expath.org/ns/http-client";
declare namespace f                 = "http://hl7.org/fhir";

let $requestParts       := request:get-attribute('request.parts')
let $doSearch           := request:get-attribute('request.subtype')=($getf:RFINT_SEARCH_TYPE, $getf:RFINT_SEARCH_SYSTEM)
let $doMetadata         := request:get-attribute('request.subtype')=$getf:RFINT_CAPABILITIES
let $doOperation        := $requestParts/(@oper1|@oper2|@oper3)[starts-with(.,'$')][1]/string()
let $resourcePath       := request:get-attribute('request.path')
let $projectprefix      := request:get-attribute('request.projectprefix')[string-length() gt 0]
let $projectversion     := request:get-attribute('request.projectversion')[string-length() gt 0]
let $resourceName       := request:get-attribute('request.resource')[string-length() gt 0]
let $basePath           := if (empty($resourceName)) then ('body/f:*') else (concat('body/f:',$resourceName))
let $id                 := request:get-attribute('request.id')[string-length() gt 0]
let $version            := request:get-attribute('request.version')[string-length() gt 0]
let $offset             := request:get-attribute('response.offset')
let $offset             := 
    if ($offset castable as xs:integer) then 
        if (xs:integer($offset) gt 0) then xs:integer($offset) else 1
    else ()
let $count              := request:get-attribute('response.count')
let $count              := 
    if ($count castable as xs:integer) then 
        if (xs:integer($count) gt 0) then xs:integer($count) else 0
    else 50
let $since              := request:get-parameter('_since',())[. castable as xs:dateTime]
let $sort               := request:get-parameter('_sort',())[string-length() gt 0]
let $_now               := current-dateTime()
let $response-extension := adfhir:getResponseExtension(request:get-attribute('response.format'))
let $headers            := ()
let $fhirLinkItemStyle  := request:get-parameter('fhirLinkItemStyle', 'idDisplay')

let $result             := 
    try {
        let $language   := request:get-parameter('language','*')[string-length() gt 0][1]
        let $supportedParameterNames    := ('_id', 'url', 'identifier', 'version', 'publisher', 'name', 'description', '_count', '_sort', '_format')
        let $unsupportedParameterNames  := request:get-parameter-names()[not(. = $supportedParameterNames)]
        let $operationOutcomeEntry      := if ($doSearch and count($unsupportedParameterNames) gt 0) then (
                let $operationOutcome   :=
                    adfhir:operationOutCome($getf:OPERATIONOUTCOME_SEVERITY_WARNING,(),'MSG_PARAM_UNKNOWN', concat('Unsupported search parameters "',string-join($unsupportedParameterNames,', '),'"'), concat('Supported parameters: "',string-join($supportedParameterNames,', '),'"'),())
                return
                <entry type="OperationOutcome" id="{$operationOutcome/f:id/@value}" version="1" original-mime-type="{$getf:CT_FHIR_XML}" created="{$_now}" lastupdated="{$_now}" fhirVersion="{$getf:strFhirVersion}">
                    <body>{$operationOutcome}</body>
                </entry>
            ) else ()
        
        let $objects      := 
            if (empty($projectprefix) or empty($projectversion)) then 
                $setlib:colDecorData//valueSet | $setlib:colDecorCache//valueSet 
            else (
                let $decor := utillib:getDecorByPrefix($projectprefix, $projectversion, $language)
                return
                    utillib:getBuildingBlockRepositories($decor, (), $utillib:strDecorServicesURL)//valueSet
            )
        
        (: id :)
        let $objects            :=
            if (empty($id)) then ($objects) else (
                let $searchId       := substring-before($id,$getf:PARAMDECOR_ID_VERSION_SEPCHARS)[not(. = '')]
                let $searchEff      := substring-after($id, $getf:PARAMDECOR_ID_VERSION_SEPCHARS)[not(. = '')]
                (: FHIR id may not have colons. This affects using @effectiveDate as part of the StructureDefinition.id
                   Reset the colons if omitted so 2016-10-26T123456 is reset to 2016-10-26T12:34:56
                   Decided to support every variation so even 20161026123456 is supported
                :)
                let $searchEff      := adfhir:hl7TS2dateTime($searchEff)
                return
                    if (empty($searchId)) then (
                        let $o    := $objects[@id = $id]
                        return
                            $o[@effectiveDate = string(max($o/xs:dateTime(@effectiveDate)))]
                    ) else 
                    if (empty($searchEff)) then (
                        let $o    := $objects[@id = $searchId]
                        return
                            $o[@effectiveDate = string(max($o/xs:dateTime(@effectiveDate)))]
                    )
                    else (
                        $objects[@id = $searchId][@effectiveDate = $searchEff]
                    )
            )
        
        (: _id :)
        let $objects      :=
            if (count(request:get-parameter('_id',())[string-length() gt 0]) = 0) then ($objects) else (
                for $p in request:get-parameter('_id',())[string-length() gt 0]
                let $searchId       := substring-before($p,$getf:PARAMDECOR_ID_VERSION_SEPCHARS)[not(. = '')]
                let $searchEff      := substring-after($p, $getf:PARAMDECOR_ID_VERSION_SEPCHARS)[not(. = '')]
                (: FHIR id may not have colons. This affects using @effectiveDate as part of the StructureDefinition.id
                   Reset the colons if omitted so 2016-10-26T123456 is reset to 2016-10-26T12:34:56
                   Decided to support every variation so even 20161026123456 is supported
                :)
                let $searchEff      := adfhir:hl7TS2dateTime($searchEff)
                return
                    if (empty($searchId)) then $objects[@id = $p] else 
                    if (empty($searchEff)) then $objects[@id = $searchId] else (
                        $objects[@id = $searchId][@effectiveDate = $searchEff]
                    )
            )
        
        (: identifier :)
        let $objects      :=
            if (count(request:get-parameter('identifier',())[string-length()>0]) = 0) then ($objects) else (
                $objects[@id = request:get-parameter('identifier',())[string-length() gt 0]]
            )
        
        (: version :)
        let $objects      :=
            if (count(request:get-parameter('version',())[string-length()>0]) = 0) then ($objects) else (
                $objects[@effectiveDate = request:get-parameter('version',())[string-length() gt 0]]
            )
        
        (: url :)
        (: a ValueSet url could be "this servers default url"
           1. concat($getf:strFhirServices,'ValueSet/',$valueSet/@id,$getf:PARAMDECOR_ID_VERSION_SEPCHARS,encode-for-uri(replace($valueSet/@effectiveDate,'[^\d]','')))
              or
           2. some url that was assigned through a URI property of type "HL7-FHIR-System-URI-Preferred"
           
           The first URL actually would contain the version that FHIR also allows you to attach through the pipe character. So the ValueSet URL could contain
           and possibly even contradict the version trailing the pipe symbol. Prefer a version trailing the pipe, fallback to URL parsing.
        :)
        let $objects      :=
            if (count(request:get-parameter('url',())[string-length() gt 0]) = 0) then ($objects) else (
                for $p in request:get-parameter('url',())[string-length() gt 0]
                let $vsurl      := tokenize($p, '\|')[1]
                let $version    := tokenize($p, '\|')[2]
                let $vss        := $objects[@canonicalUri = $vsurl]
                return
                    if (empty($version) and count($vss) gt 1) then $vss[@effectiveDate = max($vss/xs:dateTime(@effectiveDate))] else 
                    if ($vss) then $vss else (
                        let $vsided := substring-after($vsurl, 'ValueSet/')
                        let $vsid   := 
                            if (contains($vsided, $getf:PARAMDECOR_ID_VERSION_SEPCHARS)) then substring-before($vsided, $getf:PARAMDECOR_ID_VERSION_SEPCHARS) else $vsided
                        let $vsid   := $vsid[utillib:isOid(.)]
                        let $vsed   := 
                            if (contains($vsided, $getf:PARAMDECOR_ID_VERSION_SEPCHARS)) then substring-after($vsided, $getf:PARAMDECOR_ID_VERSION_SEPCHARS) else ()
                        let $vsed   := if (empty($vsid)) then () else adfhir:hl7TS2dateTime($vsed)
                        let $vss    :=
                            if (empty($vsid)) then () else
                            if (empty($vsed)) then $objects[@id = $vsid] else (
                                $objects[@canonicalUri = $vsurl] | $objects[@id = $vsid][@effectiveDate = $vsed]
                            )
                        return
                            if (empty($vsed) and count($vss) gt 1) then $vss[@effectiveDate = max($vss/xs:dateTime(@effectiveDate))] else $vss
                    )
            )
        
        (: lucene - name :)
        let $objects      :=
            if (count(request:get-parameter('name',())[string-length() gt 0]) = 0) then ($objects) else (
                let $luceneQry  := adfhir:getSimpleLuceneQuery(tokenize(normalize-space(request:get-parameter('name',())[string-length() gt 0][1]),'[\s\-]'), 'wildcard')
                return
                $objects[ft:query(@name | @displayName, $luceneQry)]
            )
        (: lucene - description :)
        let $objects      :=
            if (count(request:get-parameter('description',())[string-length() gt 0]) = 0) then ($objects) else (
                let $luceneQry  := adfhir:getSimpleLuceneQuery(tokenize(normalize-space(request:get-parameter('description',())[string-length() gt 0][1]),'[\s\-]'), 'wildcard')
                return
                $objects[ft:query(desc, $luceneQry)]
            )
        (: publisher :)
        let $objects      :=
            if (count(request:get-parameter('publisher',())[string-length() gt 0]) = 0) then ($objects) else (
                let $decors := for $prefix in request:get-parameter('publisher',())[string-length() gt 0] return utillib:getDecorByPrefix($prefix)
                return
                $objects[ancestor::decor/project/@prefix=request:get-parameter('publisher',())[string-length() gt 0]] |
                $objects[@id = $decors//valueSet/@ref]
            )
        (: project :)
        let $objects      :=
            if (empty($projectprefix)) then $objects else if (not(empty($projectversion))) then $objects else (
                let $decorProject   := utillib:getDecorByPrefix($projectprefix)
                let $vsids          := $decorProject//valueSet/@id | $decorProject//valueSet/@ref
                
                return
                $objects[@id = $vsids] 
            )
        let $objects      :=
            for $valueSet in $objects[@id]
            let $id     := $valueSet/@id
            let $ed     := $valueSet/@effectiveDate
            group by $id, $ed
            return $valueSet[1]
        
        let $check      :=
            if ($doOperation = '$expand') then (
                switch (count($objects))
                case 0 return (
                    error(QName('http://art-decor.org/ns/fhir/error','UnsupportedRequest'), 'ValueSet to expand not found. This server does not support expansion of value sets supplied in a request. Please use this operation on readily saved value sets.')
                )
                case 1 return ()
                default return (
                    error(QName('http://art-decor.org/ns/fhir/error','UnsupportedRequest'), 'This server does not support expansion of more than one ValueSet at a time. Found: ' || count($objects) || '. doSearch: ' || $doSearch || '.')
                )
            )
            else ()
        
        let $subs       := (
            $operationOutcomeEntry,
            if (count($objects) le $count) then () else (
                let $operationOutcome   :=
                    adfhir:operationOutCome($getf:OPERATIONOUTCOME_SEVERITY_WARNING,(),concat('More results than maximum of ',$count,' found: "',count($objects),'". Only the first ',$count,' are returned.'),())
                return
                <entry type="OperationOutcome" id="{$operationOutcome/f:id/@value}" version="1" original-mime-type="{$getf:CT_FHIR_XML}" created="{$_now}" lastupdated="{$_now}" fhirVersion="{$getf:strFhirVersion}">
                    <body>{$operationOutcome}</body>
                </entry>
            )
            ,
            (:let $vss            := if ($count gt 0) then subsequence($objects[@id], 1, $count) else ():)
            for $valueSet in $objects[@id]
            let $lang           := $valueSet/ancestor::decor/project/@defaultLanguage
            let $rawValueSet    := vsapi:getRawValueSet($valueSet, (), $lang, true())
            return
                <entry type="ValueSet" id="{$id}" version="1" original-mime-type="{$getf:CT_FHIR_XML}" created="{$_now}" lastupdated="{$_now}" fhirVersion="{$getf:strFhirVersion}">
                    <body>
                    {
                        if ($doOperation = '$expand') then (
                            let $params         :=
                                map {
                                    "maxResults": 1000,
                                    "expand": true(),
                                    "debug": false(),
                                    "searchString": normalize-space(lower-case(request:get-parameter('filter',())))[string-length() gt 0]
                                }
                            return 
                                adfhirvs:convertDecorExpandedValueSet2FHIRValueSet(tcsapi:getValueSetExpansionSet($rawValueSet, $params))
                        )
                        else (
                            adfhirvs:convertDecorValueSet2FHIRValueSet($rawValueSet)
                        )
                    }
                    </body>
                </entry>
        )
        
        let $hdrs               := 
            if ($doSearch) then
                $headers (:| <header name="X-Search-Expression" value="{$expr}"/>:) | <header name="{$getf:HEADER_CONTENT_DISPOSITION}" value="attachment; filename=Bundle-searchset-{$resourceName}{$response-extension}"/>
            else ()
        let $r                  := request:set-attribute('response.headers', $hdrs)
                
        let $r              := 
            if ($doSearch) then 200
            else (
                switch (count($objects))
                case 0 return 404
                case 1 return 200
                default return (
                    error(QName('http://art-decor.org/ns/fhir/error','ResourceAmbiguous'), concat('Resource occurred ',count($objects),' times. Read is ambiguous. type=',$resourceName,' _id=',$id))
                )
            )
        
        let $r              := request:set-attribute('response.http-status', $r)
        
        return
            if ($doSearch) then (
                let $results        := adfhir:saveBundle($subs, $getf:BUNDLE_SEARCHSET, $count, $sort)
                return
                    adfhir:getResourceSnapshot($results/@type, $results/@id, $offset, $count)/descendant-or-self::f:*[1]
            )
            else (
                $subs/descendant-or-self::f:*[1]
            )
    }
    (: Occurs when util:eval() fails. This might be due our own programming errors, but let's not go there :-) :)
    catch java:org.exist.xquery.StaticXQueryException {
        let $desc   := $err:code || '. Could not retrieve result(s) using ' || string-join(($resourcePath,request:get-query-string()), '?') || '''. Please check your parameters.'
        let $r      := request:set-attribute('response.http-status', $getf:STATUS_HTTP_400_BAD_REQUEST)
        
        return adfhir:operationOutCome('error', (), $desc, ())
    }
    catch * {
        let $desc   := $err:code || ' ' || $err:description || '. URL: ' || string-join(($resourcePath,request:get-query-string()), '?') 
        let $r      := request:set-attribute('response.http-status', $getf:STATUS_HTTP_500_INTERNAL_ERROR)
        
        return adfhir:operationOutCome('error', (), $desc, ())
    }

let $r                  := if (empty($result)) then () else request:set-attribute('response.contents', $result)

(:return single result. either single Resource, or a Bundle we've created:)
return
    $result