xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adfhir      = "http://art-decor.org/ns/fhir/4.0" at "../api/api-fhir.xqm";
import module namespace adfhircs    = "http://art-decor.org/ns/fhir/4.0/codesystem" at "../api/api-fhir-codesystem.xqm";
import module namespace getf        = "http://art-decor.org/ns/fhir-settings" at "../api/fhir-settings.xqm";
import module namespace utillib     = "http://art-decor.org/ns/api/util" at "../../../api/modules/library/util-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "../../../api/modules/library/settings-lib.xqm";

declare namespace http              = "http://expath.org/ns/http-client";
declare namespace f                 = "http://hl7.org/fhir";

let $requestParts       := request:get-attribute('request.parts')
let $doSearch           := request:get-attribute('request.subtype')=($getf:RFINT_SEARCH_TYPE, $getf:RFINT_SEARCH_SYSTEM)
let $doMetadata         := request:get-attribute('request.subtype')=$getf:RFINT_CAPABILITIES
let $doOperation        := $requestParts/(@oper1|@oper2|@oper3)[starts-with(.,'$')][1]/string()
let $resourcePath       := request:get-attribute('request.path')
let $projectprefix      := request:get-attribute('request.projectprefix')[string-length() gt 0]
let $projectversion     := request:get-attribute('request.projectversion')[string-length() gt 0]
let $resourceName       := request:get-attribute('request.resource')[string-length() gt 0]
let $basePath           := if (empty($resourceName)) then ('body/f:*') else (concat('body/f:',$resourceName))
let $id                 := request:get-attribute('request.id')[string-length() gt 0]
let $version            := request:get-attribute('request.version')[string-length() gt 0]
let $offset             := request:get-attribute('response.offset')
let $offset             := 
    if ($offset castable as xs:integer) then 
        if (xs:integer($offset) gt 0) then xs:integer($offset) else 1
    else ()
let $count              := request:get-attribute('response.count')
let $count              := 
    if ($count castable as xs:integer) then 
        if (xs:integer($count) gt 0) then xs:integer($count) else 0
    else 50
let $since              := request:get-parameter('_since',())[. castable as xs:dateTime]
let $sort               := request:get-parameter('_sort',())[string-length() gt 0]
let $_now               := current-dateTime()
let $response-extension := adfhir:getResponseExtension(request:get-attribute('response.format'))
let $headers            := ()
let $fhirLinkItemStyle  := request:get-parameter('fhirLinkItemStyle', 'idDisplay')

let $result             := 
    try {
        let $language   := request:get-parameter('language','*')[string-length() gt 0][1]
        let $supportedParameterNames    
                        := ('_id', 'url', 'identifier', 'version', 'publisher', 'name', 'description', '_count', '_sort', '_format')
        let $unsupportedParameterNames
                        := request:get-parameter-names()[not(. = $supportedParameterNames)]
        let $operationOutcomeEntry
                        := if (not($doSearch) or count($unsupportedParameterNames) = 0) then () else (
                let $operationOutcome   :=
                    adfhir:operationOutCome($getf:OPERATIONOUTCOME_SEVERITY_WARNING,(),'MSG_PARAM_UNKNOWN', concat('Unsupported search parameters "',string-join($unsupportedParameterNames,', '),'"'), concat('Supported parameters: "',string-join($supportedParameterNames,', '),'"'),())
                return
                <entry type="OperationOutcome" id="{$operationOutcome/f:id/@value}" version="1" original-mime-type="{$getf:CT_FHIR_XML}" created="{$_now}" lastupdated="{$_now}" fhirVersion="{$getf:strFhirVersion}">
                    <body>{$operationOutcome}</body>
                </entry>
            )
        
        (: normally FHIR Questionnaire comes from a questionnaire, but we can create it out of a dataset or transaction too :)
        let $objects            := 
            if (empty($projectprefix)) then 
                $setlib:colDecorData//codeSystem | $setlib:colDecorCache//codeSystem
            else (
                let $decor := utillib:getDecorByPrefix($projectprefix, $projectversion, $language)
                return
                    utillib:getBuildingBlockRepositories($decor, (), $utillib:strDecorServicesURL)//codeSystem
            )
        
        (: id :)
        let $objects            :=
            if (empty($id)) then ($objects) else (
                let $searchId       := substring-before($id,$getf:PARAMDECOR_ID_VERSION_SEPCHARS)[not(. = '')]
                let $searchEff      := substring-after($id, $getf:PARAMDECOR_ID_VERSION_SEPCHARS)[not(. = '')]
                (: FHIR id may not have colons. This affects using @effectiveDate as part of the StructureDefinition.id
                   Reset the colons if omitted so 2016-10-26T123456 is reset to 2016-10-26T12:34:56
                   Decided to support every variation so even 20161026123456 is supported
                :)
                let $searchEff      := adfhir:hl7TS2dateTime($searchEff)
                return
                    if (empty($searchId)) then (
                        let $o    := $objects[@id = $id]
                        return
                            $o[@effectiveDate = string(max($o/xs:dateTime(@effectiveDate)))]
                    ) else 
                    if (empty($searchEff)) then (
                        let $o    := $objects[@id = $searchId]
                        return
                            $o[@effectiveDate = string(max($o/xs:dateTime(@effectiveDate)))]
                    )
                    else (
                        $objects[@id = $searchId][@effectiveDate = $searchEff]
                    )
            )
        
        (: _id :)
        let $objects            :=
            if (count(request:get-parameter('_id',())[string-length() gt 0]) = 0) then ($objects) else (
                for $p in request:get-parameter('_id',())[string-length() gt 0]
                let $searchId       := substring-before($p,$getf:PARAMDECOR_ID_VERSION_SEPCHARS)[not(. = '')]
                let $searchEff      := substring-after($p, $getf:PARAMDECOR_ID_VERSION_SEPCHARS)[not(. = '')]
                (: FHIR id may not have colons. This affects using @effectiveDate as part of the StructureDefinition.id
                   Reset the colons if omitted so 2016-10-26T123456 is reset to 2016-10-26T12:34:56
                   Decided to support every variation so even 20161026123456 is supported
                :)
                let $searchEff      := adfhir:hl7TS2dateTime($searchEff)
                return
                    if (empty($searchId)) then $objects[@id = $p] else 
                    if (empty($searchEff)) then $objects[@id = $searchId] else (
                        $objects[@id = $searchId][@effectiveDate = $searchEff]
                    )
            )
            
        let $objects            :=
            if (empty($objects)) then
                if (empty($id) and count(request:get-parameter('_id',())[string-length() gt 0]) = 0) then $objects else (
                    for $p in ($id, request:get-parameter('_id',())[string-length() gt 0])
                    let $searchId       := (substring-before($p,$getf:PARAMDECOR_ID_VERSION_SEPCHARS)[not(. = '')], $p)[1]
                    group by $searchId
                    return
                        if (utillib:isOid($searchId[1])) then (
                            let $n                  := utillib:getNameForOID($searchId[1], (), ())
                            let $oids               := collection($setlib:strOidsData)//oid[dotNotation[@value = $searchId[1]]]
                            let $oids               :=
                                if ($oids[2]) then
                                    if ($oids[registrationAuthority/code[@code = 'PRI']]) then 
                                        $oids[registrationAuthority/code[@code = 'PRI']][1]
                                    else
                                    if ($oids[registrationAuthority/code[@code = 'SEC']]) then 
                                        $oids[registrationAuthority/code[@code = 'SEC']][1]
                                    else (
                                        $oids[1]
                                    )
                                else (
                                    $oids
                                )
                            let $language           := 'en-US'
                            let $isCodeSystem       := $oids/additionalProperty[attribute/@value="Oid_Type"][value/@value = ('5', '6', 'codesystem')]
                            let $isCodeSystem       := if ($isCodeSystem) then $isCodeSystem else $setlib:colDecorData//@codeSystem[. = $searchId[1]]
                            return
                                if ($isCodeSystem) then
                                    if ($oids) then (
                                        adfhircs:convertDecorOIDEntry2DecorCodeSystem($oids[1], (), $language)
                                    )
                                    else (
                                        <codeSystem>
                                        {
                                            attribute id {$searchId[1]},
                                            attribute name {adfhir:shortName($n)},
                                            attribute displayName {$n},
                                            attribute effectiveDate {substring(string(current-dateTime()), 1, 19)},
                                            attribute statusCode {'new'},
                                            <desc language="{$language}">{data($n)}</desc>
                                        }
                                        </codeSystem>
                                        
                                    )
                                else ()
                        ) else ()
                )
            else (
                $objects
            )
       
        (: url :)
        (: a CodeSystem url could be "this servers default url"
           1. concat($getf:strFhirServices,'CodeSystem/',$codeSystem/@id,$getf:PARAMDECOR_ID_VERSION_SEPCHARS,encode-for-uri(replace($codeSystem/@effectiveDate,'[^\d]','')))
              or
           2. some url that was assigned through a URI property of type "HL7-FHIR-System-URI-Preferred"
           
           The first URL actually would contain the version that FHIR also allows you to attach through the pipe character. So the ValueSet URL could contain
           and possibly even contradict the version trailing the pipe symbol. Prefer a version trailing the pipe, fallback to URL parsing.
        :)
        let $objects            :=
            if (count(request:get-parameter('url',())[string-length() gt 0]) = 0) then ($objects) else (
                for $p in request:get-parameter('url',())[string-length() gt 0]
                let $csurl      := tokenize($p, '\|')[1]
                let $version    := tokenize($p, '\|')[2]
                let $csid       := 
                    if (starts-with($csurl, concat($getf:strFhirServices,'CodeSystem/'))) then (
                        let $csided := substring-after($csurl, concat($getf:strFhirServices,'CodeSystem/'))
                        return
                            substring-before($csided, $getf:PARAMDECOR_ID_VERSION_SEPCHARS)
                    )
                    else (
                        utillib:getOIDForCanonicalUri($csurl, (), $projectprefix, $setlib:strKeyFHIRR4)
                    )
                let $csed       := 
                    if (empty($version)) then 
                        if (starts-with($csurl, concat($getf:strFhirServices,'CodeSystem/'))) then (
                            let $csided := substring-after($csurl, concat($getf:strFhirServices,'CodeSystem/'))
                            return
                                adfhir:hl7TS2dateTime(substring-after($csided, $getf:PARAMDECOR_ID_VERSION_SEPCHARS))
                        )
                        else ()
                    else (adfhir:hl7TS2dateTime($version))
                let $byId       := 
                    if (empty($csid)) then () else 
                    if (empty($csed)) then (
                        let $tempcs    := $objects[@canonicalUri = $csurl] | $objects[@id = $csid]
                        return
                            if ($tempcs) then $tempcs else (
                                let $oids               := collection($setlib:strOidsData)//oid[dotNotation[@value = $csid]]
                                let $oids               :=
                                    if ($oids[2]) then
                                        if ($oids[registrationAuthority/code[@code = 'PRI']]) then 
                                            $oids[registrationAuthority/code[@code = 'PRI']][1]
                                        else
                                        if ($oids[registrationAuthority/code[@code = 'SEC']]) then 
                                            $oids[registrationAuthority/code[@code = 'SEC']][1]
                                        else (
                                            $oids[1]
                                        )
                                    else (
                                        $oids
                                    )
                                let $language           := 'en-US'
                                
                                return
                                    if ($oids) then (
                                        adfhircs:convertDecorOIDEntry2DecorCodeSystem($oids[1], $csurl, $language)
                                    )
                                    else (
                                        let $n                  := utillib:getNameForOID($csid, (), ())
                                        return
                                        <codeSystem>
                                        {
                                            attribute id {$csid},
                                            attribute name {adfhir:shortName($n)},
                                            attribute displayName {$n},
                                            attribute effectiveDate {substring(string(current-dateTime()), 1, 19)},
                                            attribute statusCode {'new'},
                                            <desc language="{$language}">{data($n)}</desc>,
                                            <publishingAuthority name="ART-DECOR">
                                                <addrLine type="uri">https://art-decor.org</addrLine>
                                            </publishingAuthority>
                                        }
                                        </codeSystem>
                                        
                                    )
                            )
                    )
                    else (
                        $objects[@id = $csid][@effectiveDate = $csed]
                    )
                return
                    if (empty($version) and count($byId) gt 1) then $byId[@effectiveDate = max($byId/xs:dateTime(@effectiveDate))] else $byId
            )
        
        (: identifier :)
        let $objects            :=
            if (count(request:get-parameter('identifier',())[string-length()>0]) = 0) then ($objects) else (
                $objects[@id=request:get-parameter('identifier',())[string-length()>0]]
            )
        (: version :)
        let $objects            :=
            if (count(request:get-parameter('version',())[string-length()>0]) = 0) then ($objects) else (
                $objects[@effectiveDate=request:get-parameter('version',())[string-length()>0]]
            )
        (: lucene - name :)
        let $objects            :=
            if (count(request:get-parameter('name',())[string-length()>0]) = 0) then ($objects) else (
                let $luceneQry  := adfhir:getSimpleLuceneQuery(tokenize(normalize-space(request:get-parameter('name',())[string-length()>0][1]),'[\s\-]'), 'wildcard')
                return
                $objects[ft:query(@name | @displayName, $luceneQry)]
            )
        (: lucene - description :)
        let $objects            :=
            if (count(request:get-parameter('description',())[string-length()>0]) = 0) then ($objects) else (
                let $luceneQry  := adfhir:getSimpleLuceneQuery(tokenize(normalize-space(request:get-parameter('description',())[string-length()>0][1]),'[\s\-]'), 'wildcard')
                return
                $objects[ft:query(desc, $luceneQry)]
            )
        (: publisher :)
        let $objects            :=
            if (count(request:get-parameter('publisher',())[string-length()>0]) = 0) then ($objects) else (
                let $decors := for $prefix in request:get-parameter('publisher',())[string-length() gt 0] return utillib:getDecor($prefix, (), ())
                return
                $objects[ancestor::decor/project/@prefix=request:get-parameter('publisher',())[string-length() gt 0]] |
                $objects[@id = $decors//codeSystem/@ref]
            )
        
        let $objects            :=
            for $codeSystem in $objects[@id]
            let $id     := $codeSystem/@id
            let $ed     := $codeSystem/@effectiveDate
            group by $id, $ed
            return $codeSystem[1]
        
        (:let $expr               := string-join(($expr-identifier, $expr-jurisdiction, $expr-name, $expr-status, $expr-source, $expr-source-system, $expr-source-code, $expr-target, $expr-target-system, $expr-target-code, $expr-url, $expr-version), '')
        let $objects            := if (string-length($expr) = 0) then $objects else util:eval(concat('$objects', $expr)):)
                
        let $hdrs               := 
            if ($doSearch) then
                $headers(: | <header name="X-Search-Expression" value="{$expr}"/>:) | <header name="{$getf:HEADER_CONTENT_DISPOSITION}" value="attachment; filename=Bundle-searchset-{$resourceName}{$response-extension}"/>
            else ()
        let $r                  := request:set-attribute('response.headers', $hdrs)
        
        let $subs               := (
            $operationOutcomeEntry,
            for $sub in subsequence($objects, 1, $count)
            return
                <entry type="CodeSystem" id="{$id}" version="1" original-mime-type="{$getf:CT_FHIR_XML}" created="{$_now}" lastupdated="{$_now}" fhirVersion="{$getf:strFhirVersion}">
                    <body>{
                        adfhircs:convertDecorCodeSystem2FHIRCodeSystem($sub,
                            map:merge((
                                map:entry("fhirCanonicalBase", $utillib:strFhirCanonicalBaseURL),
                                map:entry("publisher", ($sub/publishingAuthority, $sub/ancestor::decor/project/copyright[empty(@type) or @type='author'])[1]), 
                                map:entry("language",  ($language, $sub/ancestor::decor/project/@defaultLanguage)[1]),
                                map:entry("projectPrefix", ($projectprefix, $sub/ancestor::decor/project/@prefix)[1]),
                                map:entry("projectVersion", ($projectversion, $sub/ancestor::decor/@versionDate)[1])
                            ))
                        )
                    }</body>
                </entry>
        )
        
        let $r              := 
            if ($doSearch) then 200
            else (
                switch (count($objects))
                case 0 return 404
                case 1 return 200
                default return (
                    error(QName('http://art-decor.org/ns/fhir/error','ResourceAmbiguous'),concat('Resource occurred ',count($objects),' times. Read is ambiguous. type=',$resourceName,' _id=',$id))
                )
            )
        
        let $r              := request:set-attribute('response.http-status', $r)
        
        return
            if ($doSearch) then (
                let $results        := adfhir:saveBundle($subs, $getf:BUNDLE_SEARCHSET, $count, $sort)
                return
                    adfhir:getResourceSnapshot($results/@type, $results/@id, $offset, $count)/descendant-or-self::f:*[1]
            )
            else (
                $subs/descendant-or-self::f:*[1]
            )
    }
    (: Occurs when util:eval() fails. This might be due our own programming errors, but let's not go there :-) :)
    catch java:org.exist.xquery.StaticXQueryException {
        let $desc   := $err:code || '. Could not retrieve result(s) using ' || string-join(($resourcePath,request:get-query-string()), '?') || '''. Please check your parameters.'
        let $r      := request:set-attribute('response.http-status', $getf:STATUS_HTTP_400_BAD_REQUEST)
        
        return adfhir:operationOutCome('error', (), $desc, ())
    }
    catch * {
        let $desc   := $err:code || ' ' || $err:description || '. URL: ' || string-join(($resourcePath,request:get-query-string()), '?') 
        let $r      := request:set-attribute('response.http-status', $getf:STATUS_HTTP_500_INTERNAL_ERROR)
        
        return adfhir:operationOutCome('error', (), $desc, ())
    }

let $r                  := if (empty($result)) then () else request:set-attribute('response.contents', $result)

(:return single result. either single Resource, or a Bundle we've created:)
return
    $result