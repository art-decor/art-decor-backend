xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adfhir              = "http://art-decor.org/ns/fhir/4.0" at "../api/api-fhir.xqm";
import module namespace get                 = "http://art-decor.org/ns/fhir-settings" at "../api/fhir-settings.xqm";

let $headers                    := (request:get-attribute('response.headers') |
                                    <header name="Content-Type" value="application/xml+fhir; charset=utf-8"/>)
let $r                          := request:set-attribute('response.headers',$headers)

let $result                     := adfhir:operationOutCome(request:get-attribute('operationoutcome.severity'),(),request:get-attribute('operationoutcome.text'),())

let $r                          := if (empty($result)) then () else request:set-attribute('response.contents', $result)

return 
    $result