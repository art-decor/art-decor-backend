xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adfhir      = "http://art-decor.org/ns/fhir/4.0" at "../api/api-fhir.xqm";
import module namespace getf        = "http://art-decor.org/ns/fhir-settings" at "../api/fhir-settings.xqm";
import module namespace adfhirqr    = "http://art-decor.org/ns/fhir/4.0/questionnaireresponse" at "../api/api-fhir-questionnaireresponse.xqm";
import module namespace utilfhir    = "http://art-decor.org/ns/fhir-util" at "../api/fhir-util.xqm";

declare namespace http              = "http://expath.org/ns/http-client";
declare namespace f                 = "http://hl7.org/fhir";
declare namespace error             = "http://art-decor.org/ns/fhir/error";

let $doSearch           := request:get-attribute('request.subtype')=($getf:RFINT_SEARCH_TYPE, $getf:RFINT_SEARCH_SYSTEM)
let $resourcePath       := request:get-attribute('request.path')
let $resourceName       := request:get-attribute('request.resource')[string-length() gt 0]
let $basePath           := if (empty($resourceName)) then ('body/f:*') else (concat('body/f:',$resourceName))
let $id                 := request:get-attribute('request.id')[string-length() gt 0]
let $offset             := request:get-attribute('response.offset')
let $offset             := 
    if ($offset castable as xs:integer) then 
        if (xs:integer($offset) gt 0) then xs:integer($offset) else 1
    else 1
let $count              := request:get-attribute('response.count')
let $count              := 
    if ($count castable as xs:integer) then 
        if (xs:integer($count) gt 0) then xs:integer($count) else 0
    else 50
let $sort               := request:get-parameter('_sort',())[string-length() gt 0]
let $_now               := current-dateTime()
let $response-extension := adfhir:getResponseExtension(request:get-attribute('response.format'))

let $errdesc            := '. Could not retrieve resource(s) using ' || string-join(($resourcePath,request:get-query-string()), '?') || '''. '

let $result             := 
    try {
        let $language   := request:get-parameter('language','*')[string-length() gt 0][1]
        let $supportedParameterNames
                        := ('_id', 'author', 'authored', 'based-on', 'encounter', 'identifier', 'item-subject', 'part-of', 'patient', 'questionnaire', 'source', 'status', 'subject', '_count', '_sort', '_format')
        let $unsupportedParameterNames
                        := request:get-parameter-names()[not(. = $supportedParameterNames)]
        let $operationOutcomeEntry
                        :=
            if (not($doSearch) or count($unsupportedParameterNames) = 0) then () else (
                let $operationOutcome   :=
                    adfhir:operationOutCome($getf:OPERATIONOUTCOME_SEVERITY_WARNING,(),'MSG_PARAM_UNKNOWN', concat('Unsupported search parameters "',string-join($unsupportedParameterNames,', '),'"'), concat('Supported parameters: "',string-join($supportedParameterNames,', '),'"'),())
                return
                <entry type="OperationOutcome" id="{$operationOutcome/f:id/@value}" version="1" original-mime-type="{$getf:CT_FHIR_XML}" created="{$_now}" lastupdated="{$_now}" fhirVersion="{$getf:strFhirVersion}">
                    <body>{$operationOutcome}</body>
                </entry>
            )
        
        (: questionnaireresponse handling:)
        let $expr               := adfhirqr:getSearchExpr()
        let $questresponses     := adfhirqr:getQuestionnaireResponses($doSearch, $id, $expr)
        
        (: check id :)
        let $check              := if ($doSearch) then () else 
            adfhirqr:checkIdQuestionnaireResponse($id, $questresponses)
 
        (: create response :)
        let $headers            := 
            <header name="Location" value="{$resourcePath}"/> 
        let $headers            := 
            if ($doSearch) then
                $headers | <header name="X-Search-Expression" value="{$expr}"/> | <header name="{$getf:HEADER_CONTENT_DISPOSITION}" value="attachment; filename=Bundle-searchset-{$resourceName}{$response-extension}"/>
            else $headers

        let $r                  := request:set-attribute('response.headers', $headers)
        let $r                  := request:set-attribute('response.http-status', 200)
        
        let $result             :=
            if ($doSearch)  
                then utilfhir:wrapResourcesInBundle($questresponses, $offset, $count, $sort) 
                else for $qr in $questresponses return $qr
                
        return $result
         
     }
     
    (: Occurs when util:eval() fails. This might be due our own programming errors, but let's not go there :-) :)
        catch java:org.exist.xquery.StaticXQueryException {
        let $desc   := $err:code || $errdesc || $err:description
        let $r      := request:set-attribute('response.http-status', 400)
        
        return adfhir:operationOutCome('error', (), $desc, ())
    }
    (: throwing expected errors from check functions :) 
    catch error:MSG_VERSION_AWARE_CONFLICT {
        let $desc   := $err:code || $errdesc || $err:description
        let $r      := request:set-attribute('response.http-status',400)
        
        return adfhir:operationOutCome('error',(),$desc,())
    }
    catch error:MSG_DUPLICATE_ID {
        let $desc   := $err:code || $errdesc || $err:description
        let $r      := request:set-attribute('response.http-status',400)
        
        return adfhir:operationOutCome('error',(),$desc,())
    }
    catch error:MSG_NO_EXIST {
        let $desc   := $err:code || $errdesc || $err:description
        let $r      := request:set-attribute('response.http-status',404)
        
        return adfhir:operationOutCome('error',(),$desc,())
    }
    catch error:MSG_DELETED_ID {
        let $desc   := $err:code || $errdesc || $err:description
        let $r      := request:set-attribute('response.http-status',410)
        
        return adfhir:operationOutCome('error',(),$desc,())
    }
    catch * {
        let $desc   := $err:code || $errdesc || $err:description || 'module: ' || $err:module || ', ' || $err:line-number || ', '  || $err:column-number || '.'
        let $r      := request:set-attribute('response.http-status',500)
        return adfhir:operationOutCome('error',(),$desc,())
    }

let $r                  := if (empty($result)) then () else request:set-attribute('response.contents', $result)

(:return single result. either single Resource, or a Bundle we've created:)
return
    $result