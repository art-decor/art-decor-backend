xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adfhir      = "http://art-decor.org/ns/fhir/4.0" at "../api/api-fhir.xqm";
import module namespace adfhirns    = "http://art-decor.org/ns/fhir/4.0/namingsystem" at "../api/api-fhir-namingsystem.xqm";
import module namespace getf        = "http://art-decor.org/ns/fhir-settings" at "../api/fhir-settings.xqm";
import module namespace utillib     = "http://art-decor.org/ns/api/util" at "../../../api/modules/library/util-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "../../../api/modules/library/settings-lib.xqm";

declare namespace http              = "http://expath.org/ns/http-client";
declare namespace f                 = "http://hl7.org/fhir";

let $requestParts       := request:get-attribute('request.parts')
let $doSearch           := request:get-attribute('request.subtype')=($getf:RFINT_SEARCH_TYPE, $getf:RFINT_SEARCH_SYSTEM)
let $doMetadata         := request:get-attribute('request.subtype')=$getf:RFINT_CAPABILITIES
let $doOperation        := $requestParts/(@oper1|@oper2|@oper3)[starts-with(.,'$')][1]/string()
let $resourcePath       := request:get-attribute('request.path')
let $projectprefix      := request:get-attribute('request.projectprefix')[string-length() gt 0]
let $projectversion     := request:get-attribute('request.projectversion')[string-length() gt 0]
let $resourceName       := request:get-attribute('request.resource')[string-length() gt 0]
let $basePath           := if (empty($resourceName)) then ('body/f:*') else (concat('body/f:',$resourceName))
let $id                 := request:get-attribute('request.id')[string-length() gt 0]
let $version            := request:get-attribute('request.version')[string-length() gt 0]
let $offset             := request:get-attribute('response.offset')
let $offset             := 
    if ($offset castable as xs:integer) then 
        if (xs:integer($offset) gt 0) then xs:integer($offset) else 1
    else ()
let $count              := request:get-attribute('response.count')
let $count              := 
    if ($count castable as xs:integer) then 
        if (xs:integer($count) gt 0) then xs:integer($count) else 0
    else 50
let $since              := request:get-parameter('_since',())[. castable as xs:dateTime]
let $sort               := request:get-parameter('_sort',())[string-length() gt 0]
let $_now               := current-dateTime()
let $response-extension := adfhir:getResponseExtension(request:get-attribute('response.format'))
let $headers            := ()
let $fhirLinkItemStyle  := request:get-parameter('fhirLinkItemStyle', 'idDisplay')

let $result             := 
    try {
        let $language   := request:get-parameter('language','*')[string-length() gt 0][1]
        let $supportedParameterNames
                        := ('_id', 'id-type', 'value', 'value:missing', 'publisher', 'description', '_count', '_sort', '_format')
        let $unsupportedParameterNames
                        := request:get-parameter-names()[not(. = $supportedParameterNames)]
        let $operationOutcomeEntry
                        := if (not($doSearch) or count($unsupportedParameterNames) = 0) then () else (
                let $operationOutcome   :=
                    adfhir:operationOutCome($getf:OPERATIONOUTCOME_SEVERITY_WARNING,(),'MSG_PARAM_UNKNOWN', concat('Unsupported search parameters "',string-join($unsupportedParameterNames,', '),'"'), concat('Supported parameters: "',string-join($supportedParameterNames,', '),'"'),())
                return
                <entry type="OperationOutcome" id="{$operationOutcome/f:id/@value}" version="1" original-mime-type="{$getf:CT_FHIR_XML}" created="{$_now}" lastupdated="{$_now}" fhirVersion="{$getf:strFhirVersion}">
                    <body>{$operationOutcome}</body>
                </entry>
            )
        
        let $objects            := collection($setlib:strOidsData)//oid
        
        (: id :)
        let $objects            :=
            if (empty($id)) then ($objects) else (
                let $searchOid          := if (matches($id, concat('^\d+(\.\d+)*',$getf:PARAMDECOR_ID_VERSION_SEPCHARS))) then substring-before($id,$getf:PARAMDECOR_ID_VERSION_SEPCHARS) else $id
                let $searchRegistry     := if (matches($id, concat('^\d+(\.\d+)*',$getf:PARAMDECOR_ID_VERSION_SEPCHARS))) then substring-after($id, $getf:PARAMDECOR_ID_VERSION_SEPCHARS) else ($projectprefix)
                let $results            :=
                    if (empty($searchOid)) then 
                        if (empty($searchRegistry)) then
                            $objects
                        else (
                            $objects[ancestor::oidList[@name = $searchRegistry]]
                        )
                    else (
                        if (empty($searchRegistry)) then
                            $objects[@oid = $searchOid]
                        else (
                            $objects[@oid = $searchOid][ancestor::oidList[@name = $searchRegistry]]
                        )
                    )
                return
                    if ($results[2]) then (
                        let $r  := $results[@registrationAuthorityCode = 'PRI']
                        let $r  := if ($r) then $r else ($results[@registrationAuthorityCode = 'SEC'])
                        return 
                            if ($r) then $r[1] else $results[1]
                    )
                    else (
                        $results
                    )
            )
        
        (: _id :)
        let $objects    :=
            if (count(request:get-parameter('_id',())[string-length() gt 0]) = 0) then ($objects) else (
                for $p in request:get-parameter('_id',())[string-length() gt 0]
                let $searchOid          := if (matches($id, concat('^\d+(\.\d+)*',$getf:PARAMDECOR_ID_VERSION_SEPCHARS))) then substring-before($id,$getf:PARAMDECOR_ID_VERSION_SEPCHARS) else $id
                let $searchRegistry     := if (matches($id, concat('^\d+(\.\d+)*',$getf:PARAMDECOR_ID_VERSION_SEPCHARS))) then substring-after($id, $getf:PARAMDECOR_ID_VERSION_SEPCHARS) else ($projectprefix)
                return
                    if (empty($searchOid)) then 
                        $objects[@oid = $p] 
                    else 
                    if (empty($searchRegistry)) then
                        $objects[@oid = $searchOid]
                    else (
                        $objects[@oid = $searchOid][ancestor::oidList[@name = $searchRegistry]]
                    )
            )
        
        (: identifier :)
        let $id-type            := request:get-parameter('id-type',())[string-length() gt 0]
        let $id-value           := request:get-parameter('value',())[string-length() gt 0]
        let $id-value-missing   := request:get-parameter('value:missing',())[string-length() gt 0]
        let $objects    :=
            if (empty($id-value)) then (
                if ($id-value-missing = 'true') then (
                    if ($id-type = 'uri') then (
                        $objects[not(property[@value = $setlib:strKeyCanonicalUriPrefd])]
                    ) else ( (:other id-type :)
                        $objects
                    )
                ) else 
                if ($id-value-missing = 'false') then (
                    if ($id-type = 'uri') then (
                        $objects[property[@value = $setlib:strKeyCanonicalUriPrefd]]
                    ) else ( (:other id-type :)
                        $objects
                    )
                ) else (
                    $objects
                )
            ) else (
                if ($id-type = 'oid') then (
                    $objects[property[@value = $id-value]]
                ) else 
                if ($id-type = 'uri') then (
                    $objects[property[@value = $id-value][@value = $setlib:strKeyCanonicalUriPrefd]]
                ) else 
                ( (:unknown id-type :)
                    $objects[@oid = $id-value] |
                    $objects[property[@value = $id-value][@value = $setlib:strKeyCanonicalUriPrefd]]
                )
            )
        (: publisher :)
        let $objects    :=
            if (count(request:get-parameter('publisher',())[string-length() gt 0]) = 0) then ($objects[ancestor::oidList]) else (
                $objects[ancestor::oidList[@name = request:get-parameter('publisher',())[string-length() gt 0]]]
            )
        (: lucene - description :)
        let $objects    :=
            if (count(request:get-parameter('description',())[string-length() gt 0]) = 0) then ($objects) else (
                let $luceneQry  := adfhir:getSimpleLuceneQuery(tokenize(normalize-space(request:get-parameter('description',())[string-length() gt 0][1]),'[\s\-]'), 'wildcard')
                return
                    $objects[ft:query(desc | @symbolicName, $luceneQry)]
            )
                
        let $hdrs               := 
            if ($doSearch) then
                $headers | (:<header name="X-Search-Expression" value="{$expr}"/> |:) <header name="{$getf:HEADER_CONTENT_DISPOSITION}" value="attachment; filename=Bundle-searchset-{$resourceName}{$response-extension}"/>
            else ()
        let $r                  := request:set-attribute('response.headers', $hdrs)
        
        let $subs               := (
            $operationOutcomeEntry,
            for $sub in subsequence($objects, 1, $count)
            return
                <entry type="NamingSystem" id="{$id}" version="1" original-mime-type="{$getf:CT_FHIR_XML}" created="{$_now}" lastupdated="{$_now}" fhirVersion="{$getf:strFhirVersion}">
                    <body>{adfhirns:convertDecorOIDEntry2FHIRNamingSystem($sub)}</body>
                </entry>
        )
                
        let $r              := 
            if ($doSearch) then 200
            else (
                switch (count($objects))
                case 0 return 404
                case 1 return 200
                default return (
                    error(QName('http://art-decor.org/ns/fhir/error','ResourceAmbiguous'),concat('Resource occurred ',count($objects),' times. Read is ambiguous. type=',$resourceName,' _id=',$id))
                )
            )
        
        let $r              := request:set-attribute('response.http-status', $r)
        
        return
            if ($doSearch) then (
                let $results        := adfhir:saveBundle($subs, $getf:BUNDLE_SEARCHSET, $count, $sort)
                return
                    adfhir:getResourceSnapshot($results/@type, $results/@id, $offset, $count)/descendant-or-self::f:*[1]
            )
            else (
                $subs/descendant-or-self::f:*[1]
            )
    }
    (: Occurs when util:eval() fails. This might be due our own programming errors, but let's not go there :-) :)
    catch java:org.exist.xquery.StaticXQueryException {
        let $desc   := $err:code || '. Could not retrieve result(s) using ' || string-join(($resourcePath,request:get-query-string()), '?') || '''. Please check your parameters.'
        let $r      := request:set-attribute('response.http-status', $getf:STATUS_HTTP_400_BAD_REQUEST)
        
        return adfhir:operationOutCome('error', (), $desc, ())
    }
    catch * {
        let $desc   := $err:code || ' ' || $err:description || '. URL: ' || string-join(($resourcePath,request:get-query-string()), '?') 
        let $r      := request:set-attribute('response.http-status', $getf:STATUS_HTTP_500_INTERNAL_ERROR)
        
        return adfhir:operationOutCome('error', (), $desc, ())
    }

let $r                  := if (empty($result)) then () else request:set-attribute('response.contents', $result)

(:return single result. either single Resource, or a Bundle we've created:)
return
    $result