xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adfhir      = "http://art-decor.org/ns/fhir/4.0" at "../api/api-fhir.xqm";
import module namespace getf        = "http://art-decor.org/ns/fhir-settings" at "../api/fhir-settings.xqm";
import module namespace adfhirqr    = "http://art-decor.org/ns/fhir/4.0/questionnaireresponse" at "../api/api-fhir-questionnaireresponse.xqm";
import module namespace utilfhir    = "http://art-decor.org/ns/fhir-util" at "../api/fhir-util.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "../../../api/modules/library/settings-lib.xqm";

import module namespace jwt-auth    = "http://art-decor.org/ns/api/auth" at "../../../api/modules/library/jwt-auth.xqm";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

declare namespace http              = "http://expath.org/ns/http-client";
declare namespace f                 = "http://hl7.org/fhir";
declare namespace error             = "http://art-decor.org/ns/fhir/error";

let $type       := request:get-attribute('request.resource')
let $id         := request:get-attribute('request.id')


let $errdesc                := '. Could not delete resource using ' || string-join((request:get-attribute('request.path'),request:get-query-string()), '?') || '''. '

let $result             :=
    try {
        (: first check the authentication through a JWT Token :)
        let $authmap                := jwt-auth:bearer-auth(map {})
        let $authmap                := 
            if (empty($authmap)) then 
                error($errors:UNAUTHORIZED, 'Access denied, please authorize with token in header')
           else ($authmap)
        
        (: request handling :)
        let $id                     := request:get-attribute('request.id')
        let $projectPrefix          := request:get-attribute('request.projectprefix')[string-length() gt 0]

        (: questionnaireresponse handling :)
        let $qr                     := $setlib:colDecorData//f:QuestionnaireResponse[f:id[@value = $id]][f:meta[f:tag[f:system/@value = 'http://hl7.org/fhir/FHIR-version'][f:code/@value = ($getf:strFhirVersionShort, $getf:strFhirVersion)]]]
        (: check instances :)
        let $check                  := adfhirqr:checkIdQuestionnaireResponse($id, $qr)
        (: check projectprefix and permissions :)
        let $decor                  := adfhirqr:checkDecorQuestionnaireResponse($authmap, $projectPrefix, $qr)
        (: delete questionnaireresponse :)
        let $delete                 := utilfhir:deleteFhirEntry($getf:strFhirVersionShort, $decor, $qr/ancestor::f:entry)

        (: create response :)
        let $headers            := 
            <header name="Location" value="{request:get-attribute('request.path')}"/> |
            <header name="Last-Modified" value="{adfhir:dateTime2httpDate(fn:string(current-dateTime()))}"/>

        let $r                  := request:set-attribute('response.headers',$headers)
        let $r                  := request:set-attribute('response.http-status',204)
        
        return ()
    }
    catch error:MSG_VERSION_AWARE_CONFLICT {
        let $desc   := $err:code || $errdesc || $err:description
        let $r      := request:set-attribute('response.http-status',400)
        return adfhir:operationOutCome('error',(),$desc,())
    }
    catch error:MSG_DUPLICATE_ID {
        let $desc   := $err:code || $errdesc || $err:description
        let $r      := request:set-attribute('response.http-status',400)
        return adfhir:operationOutCome('error',(),$desc,())
    }
    catch error:MSG_PARAM_INVALID {
        let $desc   := $err:code || $errdesc || $err:description
        let $r      := request:set-attribute('response.http-status',400)
        return adfhir:operationOutCome('error',(),$desc,())
    }
    catch error:MSG_RESOURCE_MISMATCH {
        let $desc   := $err:code || $errdesc || $err:description
        let $r      := request:set-attribute('response.http-status',400)
        return adfhir:operationOutCome('error',(),$desc,())
    }
    catch errors:UNAUTHORIZED_401 {
        let $desc   := $err:code || $errdesc || $err:description
        let $r      := request:set-attribute('response.http-status',401)
        return adfhir:operationOutCome('error',(),$desc,())
    }
    catch error:MSG_RESOURCE_NOT_ALLOWED {
        let $desc   := $err:code || $errdesc || $err:description
        let $r      := request:set-attribute('response.http-status',403)
        return adfhir:operationOutCome('error',(),$desc,())
    }
    catch error:MSG_NO_EXIST {
        let $desc   := $err:code || $errdesc || $err:description
        let $r      := request:set-attribute('response.http-status',404)
        return adfhir:operationOutCome('error',(),$desc,())
    }
    catch error:MSG_DELETED_ID {
        let $desc   := $err:code || $errdesc || $err:description
        let $r      := request:set-attribute('response.http-status',410)
        return adfhir:operationOutCome('error',(),$desc,())
    }
    catch * {
        let $desc   := $err:code || $errdesc || $err:description || '. module: ' || $err:module || ', ' || $err:line-number || ', '  || $err:column-number || '.'
        let $r      := request:set-attribute('response.http-status',500)
        return adfhir:operationOutCome('error',(),$desc,())
    }

let $r      := if (empty($result)) then () else request:set-attribute('response.contents', $result)
return $result