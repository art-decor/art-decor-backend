xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adfhir      = "http://art-decor.org/ns/fhir/4.0" at "../api/api-fhir.xqm";
import module namespace getf        = "http://art-decor.org/ns/fhir-settings" at "../api/fhir-settings.xqm";
import module namespace utillib     = "http://art-decor.org/ns/api/util" at "../../../api/modules/library/util-lib.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "../../../api/modules/library/settings-lib.xqm";
import module namespace adfhirq     = "http://art-decor.org/ns/fhir/4.0/questionnaire" at "../api/api-fhir-questionnaire.xqm";

declare namespace http              = "http://expath.org/ns/http-client";
declare namespace f                 = "http://hl7.org/fhir";
declare namespace error             = "http://art-decor.org/ns/fhir/error";

let $requestParts       := request:get-attribute('request.parts')
let $doSearch           := request:get-attribute('request.subtype')=($getf:RFINT_SEARCH_TYPE, $getf:RFINT_SEARCH_SYSTEM)
let $doMetadata         := request:get-attribute('request.subtype')=$getf:RFINT_CAPABILITIES
let $doOperation        := $requestParts/(@oper1|@oper2|@oper3)[starts-with(.,'$')][1]/string()
let $resourcePath       := request:get-attribute('request.path')
let $projectprefix      := request:get-attribute('request.projectprefix')[string-length() gt 0]
let $projectversion     := request:get-attribute('request.projectversion')[string-length() gt 0]
let $resourceName       := request:get-attribute('request.resource')[string-length() gt 0]
let $basePath           := if (empty($resourceName)) then ('body/f:*') else (concat('body/f:',$resourceName))
let $id                 := request:get-attribute('request.id')[string-length() gt 0]
let $version            := request:get-attribute('request.version')[string-length() gt 0]
let $offset             := request:get-attribute('response.offset')
let $offset             := 
    if ($offset castable as xs:integer) then 
        if (xs:integer($offset) gt 0) then xs:integer($offset) else 1
    else 1
let $count              := request:get-attribute('response.count')
let $count              := 
    if ($count castable as xs:integer) then 
        if (xs:integer($count) gt 0) then xs:integer($count) else 0
    else 50
let $since              := request:get-parameter('_since',())[. castable as xs:dateTime]
let $sort               := request:get-parameter('_sort',())[string-length() gt 0]
let $_now               := current-dateTime()
let $response-extension := adfhir:getResponseExtension(request:get-attribute('response.format'))
let $headers            := ()
let $fhirLinkItemStyle  := request:get-parameter('fhirLinkItemStyle', 'idDisplay')

let $result             := 
    try {
        let $language   := request:get-parameter('language','*')[string-length() gt 0][1]
        let $supportedParameterNames
                        := ('_id', 'code', 'description', 'identifier', 'jurisdiction', 'name', 'publisher', 'status', 'subjectType', 'title', 'url', 'version', '_count', '_sort', '_format')
        let $unsupportedParameterNames
                        := request:get-parameter-names()[not(. = $supportedParameterNames)]
        let $operationOutcomeEntry
                        :=
            if (not($doSearch) or count($unsupportedParameterNames) = 0) then () else (
                let $operationOutcome   :=
                    adfhir:operationOutCome($getf:OPERATIONOUTCOME_SEVERITY_WARNING,(),'MSG_PARAM_UNKNOWN', concat('Unsupported search parameters "',string-join($unsupportedParameterNames,', '),'"'), concat('Supported parameters: "',string-join($supportedParameterNames,', '),'"'),())
                return
                <entry type="OperationOutcome" id="{$operationOutcome/f:id/@value}" version="1" original-mime-type="{$getf:CT_FHIR_XML}" created="{$_now}" lastupdated="{$_now}" fhirVersion="{$getf:strFhirVersion}">
                    <body>{$operationOutcome}</body>
                </entry>
            )
        
        (: normally FHIR Questionnaire comes from a questionnaire, but we can create it out of a dataset or transaction too :)
        let $objects            :=
            if (empty($projectprefix)) then 
                $setlib:colDecorData//(questionnaire | transaction | dataset) | $setlib:colDecorCache//(questionnaire | transaction | dataset)
            else (
                let $decor := utillib:getDecorByPrefix($projectprefix, $projectversion, $language)
                return
                    utillib:getBuildingBlockRepositories($decor, (), $utillib:strDecorServicesURL)//(questionnaire | transaction | dataset)
            )
        
        (: id :)
        let $objects            :=
            if (empty($id)) then ($objects) else (
                let $searchId       := substring-before($id,$getf:PARAMDECOR_ID_VERSION_SEPCHARS)[not(. = '')]
                let $searchEff      := substring-after($id, $getf:PARAMDECOR_ID_VERSION_SEPCHARS)[not(. = '')]
                (: FHIR id may not have colons. This affects using @effectiveDate as part of the StructureDefinition.id
                   Reset the colons if omitted so 2016-10-26T123456 is reset to 2016-10-26T12:34:56
                   Decided to support every variation so even 20161026123456 is supported
                :)
                let $searchEff      := adfhir:hl7TS2dateTime($searchEff)
                return
                    if (empty($searchId)) then (
                        let $o    := $objects[@id = $id]
                        return
                            $o[@effectiveDate = string(max($o/xs:dateTime(@effectiveDate)))]
                    ) else 
                    if (empty($searchEff)) then (
                        let $o    := $objects[@id = $searchId]
                        return
                            $o[@effectiveDate = string(max($o/xs:dateTime(@effectiveDate)))]
                    )
                    else (
                        $objects[@id = $searchId][@effectiveDate = $searchEff]
                    )
            )
        (: _id :)
        let $objects            :=
            if (count(request:get-parameter('_id',())[string-length() gt 0]) = 0) then ($objects) else (
                for $p in request:get-parameter('_id',())[string-length() gt 0]
                let $searchId       := substring-before($p,$getf:PARAMDECOR_ID_VERSION_SEPCHARS)[not(. = '')]
                let $searchEff      := substring-after($p, $getf:PARAMDECOR_ID_VERSION_SEPCHARS)[not(. = '')]
                (: FHIR id may not have colons. This affects using @effectiveDate as part of the StructureDefinition.id
                   Reset the colons if omitted so 2016-10-26T123456 is reset to 2016-10-26T12:34:56
                   Decided to support every variation so even 20161026123456 is supported
                :)
                let $searchEff      := adfhir:hl7TS2dateTime($searchEff)
                return
                    if (empty($searchId)) then $objects[@id = $p] else 
                    if (empty($searchEff)) then $objects[@id = $searchId] else (
                        $objects[@id = $searchId][@effectiveDate = $searchEff]
                    )
            )
        (: lucene - description :)
        let $objects            :=
            if (count(request:get-parameter('description',())[string-length() gt 0]) = 0) then ($objects) else (
                let $luceneQry  := adfhir:getSimpleLuceneQuery(tokenize(lower-case(normalize-space(request:get-parameter('description',())[string-length() gt 0][1])),'[\s\-]'), 'wildcard')
                return
                    $objects[ft:query(desc, $luceneQry)]
            )
        (: lucene - publisher :)
        let $objects            :=
            if (count(request:get-parameter('publisher',())[string-length() gt 0]) = 0) then ($objects) else (
                let $luceneQry  := adfhir:getSimpleLuceneQuery(tokenize(lower-case(normalize-space(request:get-parameter('publisher',())[string-length() gt 0][1])),'[\s\-]'), 'wildcard')
                return
                    $objects[ft:query(publishingAuthority/@name, $luceneQry)]
            )
        (: lucene - title :)
        let $objects            :=
            if (count(request:get-parameter('title',())[string-length() gt 0]) = 0) then ($objects) else (
                let $luceneQry  := adfhir:getSimpleLuceneQuery(tokenize(lower-case(normalize-space(request:get-parameter('title',())[string-length() gt 0][1])),'[\s\-]'), 'wildcard')
                return
                    $objects[ft:query(name, $luceneQry)]
            )
        (: identifier :)
        let $expr-identifier    :=
            if (count(request:get-parameter('identifier',())[string-length() gt 0]) = 0) then () else (
                for $p in request:get-parameter('identifier',())[string-length() gt 0]
                return
                    concat('[@id = (''', string-join(
                        for $sp in tokenize($p, ',')
                        let $c  := if (matches($sp, '\|')) then replace(substring-after($sp, '|'), '\{|\}', '') else replace($sp, '\{|\}', '')
                        return
                            replace(replace($c, 'urn:oid:', ''), '''', '''''')
                        , ''', '''),
                    ''')]')
            )
        (: code :)
        let $expr-code          :=
            if (count(request:get-parameter('code',())[string-length() gt 0]) = 0) then () else (
                for $p in request:get-parameter('code',())[string-length() gt 0]
                return
                    concat('[code[', string-join(
                        for $sp in tokenize($p, ',')
                        let $c  := if (matches($sp, '\|')) then replace(substring-after($sp, '|'), '\{|\}', '') else replace($sp, '\{|\}', '')
                        let $cs := if (matches($sp, '\|')) then replace(substring-before($sp, '|'), '\{|\}', '') else ()
                        let $c  := if (string-length($c) gt 0) then replace($c, '''', '''''') else ()
                        let $cs := if (string-length($cs) gt 0) then replace($cs, '''', '''''') else ()
                        return
                            if ($c and $cs) then
                                '.[@code = ''' || $c || '''][@canonicalUri = ''' || $cs || ''']'
                            else
                            if ($c) then
                                '.[@code = ''' || $c || ''']'
                            else (
                                '.[@canonicalUri = ''' || $cs || ''']'
                            )
                        , ' | '),
                    ']]')
            )
        
        (: jurisdiction :)
        let $expr-jurisdiction  :=
            if (count(request:get-parameter('jurisdiction',())[string-length() gt 0]) = 0) then () else (
                for $p in request:get-parameter('jurisdiction',())[string-length() gt 0]
                return
                    concat('[jurisdiction[', string-join(
                        for $sp in tokenize($p, ',')
                        let $c  := if (matches($sp, '\|')) then replace(substring-after($sp, '|'), '\{|\}', '') else replace($sp, '\{|\}', '')
                        let $cs := if (matches($sp, '\|')) then replace(substring-before($sp, '|'), '\{|\}', '') else ()
                        let $c  := if (string-length($c) gt 0) then replace($c, '''', '''''') else ()
                        let $cs := if (string-length($cs) gt 0) then replace($cs, '''', '''''') else ()
                        return
                            if ($c and $cs) then
                                '.[@code = ''' || $c || '''][@canonicalUri = ''' || $cs || ''']'
                            else
                            if ($c) then
                                '.[@code = ''' || $c || ''']'
                            else (
                                '.[@canonicalUri = ''' || $cs || ''']'
                            )
                        , ' | '),
                    ']]')
            )
        (: name :)
        let $expr-name          :=
            if (count(request:get-parameter('name',())[string-length() gt 0]) = 0) then () else (
                for $p in request:get-parameter('name',())[string-length() gt 0]
                return
                    concat('[name[', string-join(
                        for $sp in tokenize($p, ',')
                        return
                            '.[adfhir:validResourceName(.) = ''' || replace($sp, '''', '''''') || ''']'
                        , ' | '),
                    ']]')
            )
        (: status :)
        let $expr-status        :=
            if (count(request:get-parameter('status',())[string-length() gt 0]) = 0) then () else (
                for $p in request:get-parameter('status',())[string-length() gt 0]
                return
                    concat('[@statusCode = (''', string-join(
                        for $sp in tokenize($p, ',')
                        let $c  := if (matches($sp, '\|')) then replace(substring-after($sp, '|'), '\{|\}', '') else replace($sp, '\{|\}', '')
                        let $c2 := adfhirq:fhirQuestionnaireStatusToDecorStatus($c)
                        return
                            replace($c2, '''', '''''')
                        , ''', '''),
                    ''')]')
            )
        (: subjectType :)
        let $expr-subjectType   :=
            if (count(request:get-parameter('subjectType',())[string-length() gt 0]) = 0) then () else (
                for $p in request:get-parameter('subjectType',())[string-length() gt 0]
                return
                    concat('[subjectType[', string-join(
                        for $sp in tokenize($p, ',')
                        let $c  := if (matches($sp, '\|')) then replace(substring-after($sp, '|'), '\{|\}', '') else replace($sp, '\{|\}', '')
                        let $cs := if (matches($sp, '\|')) then replace(substring-before($sp, '|'), '\{|\}', '') else ()
                        let $c  := if (string-length($c) gt 0) then replace($c, '''', '''''') else ()
                        let $cs := if (string-length($cs) gt 0) then replace($cs, '''', '''''') else ()
                        return
                            '.[@code = ''' || $c || ''']'
                        , ' | '),
                    ']]')
            )
        (: url - ... if this is a transaction then the canonicalUri on the transaction is irrelevant, 
        but unsure what it will be upon conversion as that gets its value if and when that happens :)
        let $expr-url           :=
            if (count(request:get-parameter('url',())[string-length() gt 0]) = 0) then () else (
                for $p in request:get-parameter('url',())[string-length() gt 0]
                return
                    concat('[@canonicalUri = (''', string-join(
                        for $sp in tokenize($p, ',')
                        let $c  := if (matches($sp, '\|')) then replace(substring-after($sp, '|'), '\{|\}', '') else replace($sp, '\{|\}', '')
                        return
                            replace($c, '''', '''''')
                        , ''', '''),
                    ''')]')
            )
        (: version :)
        let $expr-version       :=
            if (count(request:get-parameter('version',())[string-length() gt 0]) = 0) then () else (
                for $p in request:get-parameter('version',())[string-length() gt 0]
                return
                    concat('[@versionLabel = (''', string-join(
                        for $sp in tokenize($p, ',')
                        let $c  := if (matches($sp, '\|')) then replace(substring-after($sp, '|'), '\{|\}', '') else replace($sp, '\{|\}', '')
                        return
                            replace($c, '''', '''''')
                        , ''', '''),
                    ''')]')
            )
        
        let $expr               := string-join(($expr-identifier, $expr-code, $expr-jurisdiction, $expr-name, $expr-status, $expr-subjectType, $expr-url, $expr-version), '')
        let $objects            := if (string-length($expr) = 0) then $objects else util:eval(concat('$objects', $expr))
        
        let $hdrs               := 
            if ($doSearch) then
                $headers | <header name="X-Search-Expression" value="{$expr}"/> | <header name="{$getf:HEADER_CONTENT_DISPOSITION}" value="attachment; filename=Bundle-searchset-{$resourceName}{$response-extension}"/>
            else ()
        let $r                  := request:set-attribute('response.headers', $hdrs)
        
        let $subs               := (
            $operationOutcomeEntry,
            for $sub in subsequence($objects, 1, $count)
            return
                <entry type="Questionnaire" id="{$id}" version="1" original-mime-type="{$getf:CT_FHIR_XML}" created="{$_now}" lastupdated="{$_now}" fhirVersion="{$getf:strFhirVersion}">
                    <body>{
                    if ($sub[name() = 'questionnaire']) then
                        adfhirq:convertDecorQuestionnaire2FHIRQuestionnaire($sub,
                            map:merge((
                                map:entry("fhirCanonicalBase", $utillib:strFhirCanonicalBaseURL),
                                map:entry("publisher", ($sub/publishingAuthority, utillib:inheritPublishingAuthority($sub))[1]), 
                                map:entry("language",  ($language, $sub/ancestor::decor/project/@defaultLanguage)[1]),
                                map:entry("projectPrefix", ($projectprefix, $sub/ancestor::decor/project/@prefix)[1]),
                                map:entry("projectVersion", ($projectversion, $sub/ancestor::decor/@versionDate)[1])
                            ))
                        )
                    else (
                        adfhirq:convertDecorDatasetOrTransaction2FHIRQuestionnaire($sub, $projectversion, $language, $fhirLinkItemStyle)
                    )
                    }</body>
                </entry>
        )
                
        let $r              := 
            if ($doSearch) then 200
            else (
                switch (count($objects))
                case 0 return 404
                case 1 return 200
                default return (
                    error(QName('http://art-decor.org/ns/fhir/error','ResourceAmbiguous'),concat('Resource occurred ',count($objects),' times. Read is ambiguous. type=',$resourceName,' _id=',$id))
                )
            )
        
        let $r              := request:set-attribute('response.http-status', $r)
        
        return
            if ($doSearch) then (
                let $results        := adfhir:saveBundle($subs, $getf:BUNDLE_SEARCHSET, $count, $sort)
                return
                    adfhir:getResourceSnapshot($results/@type, $results/@id, $offset, $count)/descendant-or-self::f:*[1]
            )
            else (
                $subs/descendant-or-self::f:*[1]
            )
    }
    (: Occurs when util:eval() fails. This might be due our own programming errors, but let's not go there :-) :)
    catch java:org.exist.xquery.StaticXQueryException {
        let $desc   := $err:code || '. Could not retrieve result(s) using ' || string-join(($resourcePath,request:get-query-string()), '?') || '''. Please check your parameters.'
        let $r      := request:set-attribute('response.http-status', $getf:STATUS_HTTP_400_BAD_REQUEST)
        
        return adfhir:operationOutCome('error', (), $desc, ())
    }
    catch * {
        let $desc   := $err:code || ' ' || $err:description || '. URL: ' || string-join(($resourcePath,request:get-query-string()), '?') 
        let $r      := request:set-attribute('response.http-status', $getf:STATUS_HTTP_500_INTERNAL_ERROR)
        
        return adfhir:operationOutCome('error', (), $desc, ())
    }

let $r                  := if (empty($result)) then () else request:set-attribute('response.contents', $result)

(:return single result. either single Resource, or a Bundle we've created:)
return
    $result