<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:nf="urn:nictiz:local-functions"
    exclude-result-prefixes="#all"
    version="3.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> May 10, 2020</xd:p>
            <xd:p><xd:b>Author:</xd:b> ahenket</xd:p>
            <xd:p>Takes in FHIR XSD fhir-single.xsd and constructs a stylesheet that will take in an expath map (http://www.w3.org/2005/xpath-functions) as constructed by json-to-xml() and convert that to FHIR XML</xd:p>
            <xd:p>Known 'issue': does not handle array of fhir_comments as xml comment(), and basically ignores them</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:output indent="yes"/>
    
    <xsl:param name="fhir-version" select="'R4'"/>
    <xsl:variable name="schemadir" select="concat('http://hl7.org/fhir/', $fhir-version)"/>
    <xsl:param name="fhir-base" select="doc(concat($schemadir, '/fhir-base.xsd'))/xs:schema"/>
    
    <xsl:variable name="toolName" select="'schema2xmlxsl.xsl'"/>
    <xsl:variable name="toolVersion" select="'1.0.0'"/>
    <xsl:variable name="functionPrefix">
        <xsl:choose>
            <xsl:when test="$fhir-version = 'DSTU2'">f2</xsl:when>
            <xsl:when test="$fhir-version = 'STU3'">f3</xsl:when>
            <xsl:when test="$fhir-version = 'R4'">f4</xsl:when>
            <xsl:when test="$fhir-version = 'R5'">f5</xsl:when>
            <xsl:otherwise>
                <xsl:message terminate="yes">Unsupported FHIR version. Supported values for xsl param fhir-version are DSTU2, STU3, R4, R5</xsl:message>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="this" select="/xs:schema"/>
    <xsl:variable name="ResourceType-list" select="$this/xs:complexType[@name = 'ResourceContainer']/xs:choice/xs:element/(@ref | @name)" as="item()+"/>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="/xs:schema">
        <xsl:element name="xsl:stylesheet">
            <xsl:namespace name="xs">http://www.w3.org/2001/XMLSchema</xsl:namespace>
            <xsl:namespace name="xd">http://www.oxygenxml.com/ns/doc/xsl</xsl:namespace>
            <xsl:namespace name="fn">http://www.w3.org/2005/xpath-functions</xsl:namespace>
            <xsl:namespace name="f">http://hl7.org/fhir</xsl:namespace>
            <!--<xsl:namespace name="xh">http://www.w3.org/1999/xhtml</xsl:namespace>
            <xsl:namespace name="f2">http://art-decor.org/ns/fhir/1.0</xsl:namespace>
            <xsl:namespace name="f3">http://art-decor.org/ns/fhir/3.0</xsl:namespace>
            <xsl:namespace name="f4">http://art-decor.org/ns/fhir/4.0</xsl:namespace>
            <xsl:namespace name="f5">http://art-decor.org/ns/fhir/5.0</xsl:namespace>-->
            <xsl:attribute name="exclude-result-prefixes">#all</xsl:attribute>
            <xsl:attribute name="version">3.0</xsl:attribute>
            <xd:doc scope="stylesheet">
                <xd:desc>
                    <xd:p><xd:b>Generated:</xd:b><xsl:text> </xsl:text><xsl:value-of select="current-dateTime()"/></xd:p>
                    <xd:p><xd:b>By:</xd:b><xsl:text> </xsl:text><xsl:value-of select="$toolName"/><xsl:text> version </xsl:text><xsl:value-of select="$toolVersion"/></xd:p>
                    <xd:p>Takes in an expath map (http://www.w3.org/2005/xpath-functions) as constructed by json-to-xml() and convert that to FHIR XML</xd:p>
                    <xd:p>Known 'issue': does not handle array of fhir_comments as xml comment(), and basically ignores them</xd:p>
                </xd:desc>
            </xd:doc>
            <xsl:if test="empty(xs:complexType)">
                <xsl:element name="xsl:import">
                    <xsl:attribute name="href">fhir-base.xsl</xsl:attribute>
                </xsl:element>
            </xsl:if>
            <xsl:apply-templates select="xs:include"/>
            
            <xsl:element name="xsl:output">
                <xsl:attribute name="indent">yes</xsl:attribute>
                <xsl:attribute name="method">xml</xsl:attribute>
                <xsl:attribute name="media-type">application/xml</xsl:attribute>
                <xsl:attribute name="omit-xml-declaration">yes</xsl:attribute>
            </xsl:element>
            
            <xsl:element name="xsl:variable">
                <xsl:attribute name="name">theJSON</xsl:attribute>
                <xsl:attribute name="select">if (/fn:map) then . else fn:json-to-xml(.)</xsl:attribute>
                <xsl:attribute name="as">document-node()</xsl:attribute>
            </xsl:element>
            
            <xd:doc>
                <xd:desc/>
            </xd:doc>
            <xsl:element name="xsl:template">
                <xsl:attribute name="match">/</xsl:attribute>
                
                <xsl:element name="xsl:apply-templates">
                    <xsl:attribute name="select">$theJSON/fn:map</xsl:attribute>
                </xsl:element>
            </xsl:element>
            
            <xd:doc>
                <xd:desc/>
            </xd:doc>
            <xsl:element name="xsl:template">
                <xsl:attribute name="match">/fn:map</xsl:attribute>
                
                <xsl:element name="xsl:variable">
                    <xsl:attribute name="name">theName</xsl:attribute>
                    <xsl:attribute name="select">fn:string[@key = 'resourceType']</xsl:attribute>
                </xsl:element>
                <xsl:element name="xsl:choose">
                    <xsl:for-each select="$ResourceType-list">
                        <xsl:element name="xsl:when">
                            <xsl:attribute name="test">
                                <xsl:text>$theName = '</xsl:text>
                                <xsl:value-of select="."/>
                                <xsl:text>'</xsl:text>
                            </xsl:attribute>
                            <xsl:element name="xsl:apply-templates">
                                <xsl:attribute name="select">.</xsl:attribute>
                                <xsl:attribute name="mode" select="."/>
                                <xsl:element name="xsl:with-param">
                                    <xsl:attribute name="name">
                                        <xsl:text>key</xsl:text>
                                    </xsl:attribute>
                                    <xsl:attribute name="select">
                                        <xsl:text>$theName</xsl:text>
                                    </xsl:attribute>
                                </xsl:element>
                            </xsl:element>
                        </xsl:element>
                    </xsl:for-each>
                    <xsl:element name="xsl:otherwise">
                        <xsl:element name="xsl:message">
                            <xsl:attribute name="terminate">yes</xsl:attribute>
                            <xsl:text>Unknown resourceType </xsl:text>
                            <xsl:element name="xsl:value-of">
                                <xsl:attribute name="select">fn:string[@key = 'resourceType']</xsl:attribute>
                            </xsl:element>
                        </xsl:element>
                    </xsl:element>
                </xsl:element>
            </xsl:element>
            
            <xsl:apply-templates select="* except xs:include"/>
            
            <xd:doc>
                <xd:desc/>
            </xd:doc>
            <xsl:element name="xsl:template">
                <xsl:attribute name="match">fn:array[@key]</xsl:attribute>
                <xsl:attribute name="mode">#all</xsl:attribute>
                <xsl:attribute name="priority">-1</xsl:attribute>
                
                <xsl:element name="xsl:variable">
                    <xsl:attribute name="name"><xsl:text>theName</xsl:text></xsl:attribute>
                    <xsl:attribute name="select"><xsl:text>@key</xsl:text></xsl:attribute>
                </xsl:element>
                
                <xsl:element name="xsl:for-each">
                    <xsl:attribute name="select">*</xsl:attribute>
                    
                    <xsl:element name="xsl:variable">
                        <xsl:attribute name="name"><xsl:text>pos</xsl:text></xsl:attribute>
                        <xsl:attribute name="select"><xsl:text>position()</xsl:text></xsl:attribute>
                    </xsl:element>
                    
                    <xsl:element name="xsl:element">
                        <xsl:attribute name="name">{$theName}</xsl:attribute>
                        <xsl:attribute name="namespace">http://hl7.org/fhir</xsl:attribute>

                        <xsl:element name="xsl:if">
                            <xsl:attribute name="test">self::fn:string | self::fn:boolean | self::fn:number</xsl:attribute>

                            <xsl:element name="xsl:attribute">
                                <xsl:attribute name="name">value</xsl:attribute>
                                <xsl:attribute name="select">.</xsl:attribute>
                            </xsl:element>
                        </xsl:element>
                        <xsl:element name="xsl:if">
                            <xsl:attribute name="test">fn:string[$theName = ('extension', 'modifierExtension')][@key = 'url']</xsl:attribute>

                            <xsl:element name="xsl:attribute">
                                <xsl:attribute name="name">url</xsl:attribute>
                                <xsl:attribute name="select">fn:string[@key = 'url']</xsl:attribute>
                            </xsl:element>
                        </xsl:element>
                        
                        <xsl:element name="xsl:apply-templates">
                            <xsl:attribute name="select">*[@key = 'id'] | ../../fn:array[@key = concat('_', $theName)]/fn:map[$pos]/*[@key = 'id']</xsl:attribute>
                        </xsl:element>
                        <xsl:element name="xsl:apply-templates">
                            <xsl:attribute name="select">*[@key = 'extension'] | ../../fn:array[@key = concat('_', $theName)]/fn:map[$pos]/fn:array[@key = 'extension']</xsl:attribute>
                            <xsl:attribute name="mode">Extension</xsl:attribute>
                            <xsl:element name="xsl:with-param">
                                <xsl:attribute name="name">key</xsl:attribute>
                                <xsl:attribute name="select">extension</xsl:attribute>
                            </xsl:element>
                        </xsl:element>
                        <xsl:element name="xsl:apply-templates">
                            <xsl:attribute name="select">*[@key = 'modifierExtension'] | ../../fn:array[@key = concat('_', $theName)]/fn:map[$pos]/fn:array[@key = 'modifierExtension']</xsl:attribute>
                            <xsl:attribute name="mode">Extension</xsl:attribute>
                            <xsl:element name="xsl:with-param">
                                <xsl:attribute name="name">key</xsl:attribute>
                                <xsl:attribute name="select">'modifierExtension'</xsl:attribute>
                            </xsl:element>
                        </xsl:element>
                        <xsl:element name="xsl:apply-templates">
                            <xsl:attribute name="select">* except (*[starts-with(@key, '_')] | *[@key = ('id', 'url', 'extension', 'modifierExtension')])</xsl:attribute>
                            <xsl:attribute name="mode">#current</xsl:attribute>
                        </xsl:element>
                    </xsl:element>
                </xsl:element>
            </xsl:element>
            
            <xd:doc>
                <xd:desc/>
            </xd:doc>
            <xsl:element name="xsl:template">
                <xsl:attribute name="match">fn:string[@key] | fn:boolean[@key] | fn:number[@key] | fn:null[@key]</xsl:attribute>
                
                <xsl:element name="xsl:variable">
                    <xsl:attribute name="name">theName</xsl:attribute>
                    <xsl:attribute name="select">@key</xsl:attribute>
                </xsl:element>
                
                <xsl:element name="xsl:choose">
                    <xsl:element name="xsl:when">
                        <xsl:attribute name="test"><xsl:text>$theName = 'id' and (empty(../fn:string[@key = 'resourceType']) or starts-with(parent::fn:*/@key, '_'))</xsl:text></xsl:attribute>
                        
                        <xsl:element name="xsl:attribute">
                            <xsl:attribute name="name">id</xsl:attribute>
                            <xsl:attribute name="select">.</xsl:attribute>
                        </xsl:element>
                    </xsl:element>
                    <xsl:element name="xsl:when">
                        <xsl:attribute name="test"><xsl:text>$theName = 'div' and ../@key = 'text'</xsl:text></xsl:attribute>
                        
                        <!-- Bit of a waste but saves us the xsi namespace declaration -->
                        <!--<xsl:element name="xsl:variable">
                            <xsl:attribute name="name">r1</xsl:attribute>
                            <xsl:attribute name="select">replace(.,   '&amp;lt;', '&lt;')</xsl:attribute>
                        </xsl:element>
                        <xsl:element name="xsl:variable">
                            <xsl:attribute name="name">r2</xsl:attribute>
                            <xsl:attribute name="select">replace($r1, '&amp;gt;', '&gt;')</xsl:attribute>
                        </xsl:element>
                        <xsl:element name="xsl:variable">
                            <xsl:attribute name="name">r3</xsl:attribute>
                            <xsl:attribute name="select">replace($r2, '&amp;quot;', '&quot;')</xsl:attribute>
                        </xsl:element>
                        <xsl:element name="xsl:variable">
                            <xsl:attribute name="name">r4</xsl:attribute>
                            <xsl:attribute name="select">replace($r3, '&amp;apos;', '&apos;&apos;')</xsl:attribute>
                        </xsl:element>
                        <xsl:element name="xsl:variable">
                            <xsl:attribute name="name">r5</xsl:attribute>
                            <xsl:attribute name="select">replace($r4, '&amp;amp;', '&amp;')</xsl:attribute>
                        </xsl:element>-->
                        
                        
                        <xsl:element name="xsl:variable">
                            <xsl:attribute name="name">div</xsl:attribute>
                            <xsl:attribute name="select">fn:parse-xml(.)/*</xsl:attribute>
                            <xsl:attribute name="as">element()?</xsl:attribute>
                        </xsl:element>
                        <xsl:element name="xsl:if">
                            <xsl:attribute name="test"><xsl:text>$div</xsl:text></xsl:attribute>
                            
                            <xsl:element name="xsl:element">
                                <xsl:attribute name="name">{local-name($div)}</xsl:attribute>
                                <xsl:attribute name="namespace">{fn:namespace-uri($div)}</xsl:attribute>
                                <xsl:element name="xsl:copy-of">
                                    <xsl:attribute name="select">$div/node()</xsl:attribute>
                                </xsl:element>
                            </xsl:element>
                        </xsl:element>
                    </xsl:element>
                    <xsl:element name="xsl:otherwise">
                        <xsl:element name="xsl:variable">
                            <xsl:attribute name="name">extensions</xsl:attribute>
                            <xsl:attribute name="select">../fn:map[@key = concat('_', $theName)]/fn:*[not(@key = 'fhir_comments')]</xsl:attribute>
                        </xsl:element>
                        <xsl:element name="xsl:element">
                            <xsl:attribute name="name">{$theName}</xsl:attribute>
                            <xsl:attribute name="namespace">http://hl7.org/fhir</xsl:attribute>
                            <xsl:element name="xsl:if">
                                <xsl:attribute name="test">string-length(.) gt 0</xsl:attribute>
                                <xsl:element name="xsl:attribute">
                                    <xsl:attribute name="name">value</xsl:attribute>
                                    <xsl:attribute name="select">.</xsl:attribute>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="xsl:if">
                                <xsl:attribute name="test">$extensions[self::fn:string][@key = 'id']</xsl:attribute>
                                <xsl:element name="xsl:attribute">
                                    <xsl:attribute name="name">id</xsl:attribute>
                                    <xsl:attribute name="select">$extensions[self::fn:string][@key = 'id'][1]</xsl:attribute>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="xsl:apply-templates">
                                <xsl:attribute name="select"><xsl:text>$extensions[@key = 'extension']</xsl:text></xsl:attribute>
                                <xsl:attribute name="mode">Extension</xsl:attribute>
                                <xsl:element name="xsl:with-param">
                                    <xsl:attribute name="name">key</xsl:attribute>
                                    <xsl:attribute name="select">'extension'</xsl:attribute>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="xsl:apply-templates">
                                <xsl:attribute name="select"><xsl:text>$extensions[@key = 'modifierExtension']</xsl:text></xsl:attribute>
                                <xsl:attribute name="mode">Extension</xsl:attribute>
                                <xsl:element name="xsl:with-param">
                                    <xsl:attribute name="name">key</xsl:attribute>
                                    <xsl:attribute name="select">'modifierExtension'</xsl:attribute>
                                </xsl:element>
                            </xsl:element>
                        </xsl:element>
                    </xsl:element>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:template>
    
    <!--<xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="/xs:schema/xs:element">
        <xsl:call-template name="doAnnotation">
            <xsl:with-param name="doParam" select="false()"/>
        </xsl:call-template>
        <xsl:element name="xsl:template">
            <xsl:attribute name="match" select="concat('f:', @name)"/>
            <xsl:element name="xsl:copy-of">
                <xsl:attribute name="select" select="concat($functionPrefix, ':', @type, '(., (), true(), true())')"/>
            </xsl:element>
        </xsl:element>
    </xsl:template>-->
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="xs:sequence/xs:element | xs:choice/xs:element">
        <xsl:variable name="theName" select="@ref | @name" as="attribute()?"/>
        <xsl:variable name="theType" as="xs:string">
            <xsl:choose>
                <xsl:when test="@type">
                    <xsl:value-of select="@type"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="/xs:schema/*[@name = current()/@ref]/@type"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <!-- primitives have their @id and extensions in a sibling array -->
        <xsl:variable name="isPrimitive" select="nf:isPrimitive($theType)" as="xs:boolean"/>
        <!--  -->
        <xsl:variable name="doArray" select="@maxOccurs = 'unbounded'" as="xs:boolean"/>
        
        <xsl:comment>
            <xsl:value-of select="$theName"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="(@minOccurs, '1')[1]"/>
            <xsl:text>..</xsl:text>
            <xsl:value-of select="(if (@maxOccurs = 'unbounded') then '*' else @maxOccurs, '1')[1]"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="$theType"/>
        </xsl:comment>
        <xsl:choose>
            <xsl:when test="@ref = 'xhtml:div'">
                <xsl:element name="xsl:for-each">
                    <xsl:attribute name="select" select="'*[@key = ''div'']'"/>
                    
                    <!-- Bit of a waste but saves us the xsi namespace declaration -->
                    <!--<xsl:element name="xsl:variable">
                        <xsl:attribute name="name">r1</xsl:attribute>
                        <xsl:attribute name="select">replace(.,   '&amp;lt;', '&lt;')</xsl:attribute>
                    </xsl:element>
                    <xsl:element name="xsl:variable">
                        <xsl:attribute name="name">r2</xsl:attribute>
                        <xsl:attribute name="select">replace($r1, '&amp;gt;', '&gt;')</xsl:attribute>
                    </xsl:element>
                    <xsl:element name="xsl:variable">
                        <xsl:attribute name="name">r3</xsl:attribute>
                        <xsl:attribute name="select">replace($r2, '&amp;quot;', '&quot;')</xsl:attribute>
                    </xsl:element>
                    <xsl:element name="xsl:variable">
                        <xsl:attribute name="name">r4</xsl:attribute>
                        <xsl:attribute name="select">replace($r3, '&amp;apos;', '&apos;&apos;')</xsl:attribute>
                    </xsl:element>
                    <xsl:element name="xsl:variable">
                        <xsl:attribute name="name">r5</xsl:attribute>
                        <xsl:attribute name="select">replace($r4, '&amp;amp;', '&amp;')</xsl:attribute>
                    </xsl:element>-->
                    
                    <xsl:element name="xsl:variable">
                        <xsl:attribute name="name">div</xsl:attribute>
                        <xsl:attribute name="select">fn:parse-xml(.)/*</xsl:attribute>
                        <xsl:attribute name="as">element()?</xsl:attribute>
                    </xsl:element>
                    <xsl:element name="xsl:if">
                        <xsl:attribute name="test"><xsl:text>$div</xsl:text></xsl:attribute>
                        
                        <xsl:element name="xsl:element">
                            <xsl:attribute name="name">{local-name($div)}</xsl:attribute>
                            <xsl:attribute name="namespace">{fn:namespace-uri($div)}</xsl:attribute>
                            <xsl:element name="xsl:copy-of">
                                <xsl:attribute name="select">$div/node()</xsl:attribute>
                            </xsl:element>
                        </xsl:element>
                    </xsl:element>
                </xsl:element>
            </xsl:when>
            <xsl:when test="ancestor::xs:complexType[@name = 'ResourceContainer']">
                <xsl:element name="xsl:apply-templates">
                    <xsl:attribute name="select" select="concat('.[*[@key = ''resourceType''][. = ''', $theName, ''']]')"/>
                    <xsl:attribute name="mode" select="$theType"/>
                    <xsl:element name="xsl:with-param">
                        <xsl:attribute name="name">key</xsl:attribute>
                        <xsl:attribute name="select">'<xsl:value-of select="$theName"/>'</xsl:attribute>
                    </xsl:element>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="xsl:apply-templates">
                    <xsl:attribute name="select" select="concat('*[@key = ''', $theName, ''']')"/>
                    <xsl:attribute name="mode" select="$theType"/>
                    <xsl:element name="xsl:with-param">
                        <xsl:attribute name="name">key</xsl:attribute>
                        <xsl:attribute name="select">'<xsl:value-of select="$theName"/>'</xsl:attribute>
                    </xsl:element>
                </xsl:element>
                <xsl:if test="$isPrimitive">
                    <xsl:element name="xsl:if">
                        <xsl:attribute name="test" select="concat('.[empty(*[@key = ''', $theName, '''])][*[@key = ''_', $theName, ''']]')"/>
                        <xsl:element name="xsl:variable">
                            <xsl:attribute name="name">theElement</xsl:attribute>
                            <xsl:attribute name="as">element()</xsl:attribute>
                            
                            <x>
                                <xsl:element name="string" namespace="http://www.w3.org/2005/xpath-functions">
                                    <xsl:attribute name="key" select="$theName"/>
                                </xsl:element>
                                <xsl:element name="xsl:copy-of">
                                    <xsl:attribute name="select" select="concat('fn:map[@key = ''_', $theName, ''']')"/>
                                </xsl:element>
                            </x>
                        </xsl:element>
                        <xsl:element name="xsl:apply-templates">
                            <xsl:attribute name="select">$theElement/fn:string</xsl:attribute>
                            <xsl:attribute name="mode" select="$theType"/>
                            <xsl:element name="xsl:with-param">
                                <xsl:attribute name="name">key</xsl:attribute>
                                <xsl:attribute name="select">'<xsl:value-of select="$theName"/>'</xsl:attribute>
                            </xsl:element>
                        </xsl:element>
                    </xsl:element>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="xs:complexType">
        <xsl:call-template name="doAnnotation"/>
        <xsl:element name="xsl:template">
            <xsl:attribute name="match">*</xsl:attribute>
            <xsl:attribute name="mode" select="@name"/>
            <!--<xsl:attribute name="as">element()*</xsl:attribute>-->
            <!-- key -->
            <xsl:element name="xsl:param">
                <xsl:attribute name="name">key</xsl:attribute>
                <xsl:attribute name="as">xs:string?</xsl:attribute>
            </xsl:element>
            
            <xsl:choose>
                <xsl:when test=".//xs:element">
                    <xsl:element name="xsl:choose">
                        <xsl:element name="xsl:when">
                            <xsl:attribute name="test">self::fn:array</xsl:attribute>
                            
                            <xsl:element name="xsl:apply-templates">
                                <xsl:attribute name="select">*</xsl:attribute>
                                <xsl:attribute name="mode">#current</xsl:attribute>
                                
                                <xsl:element name="xsl:with-param">
                                    <xsl:attribute name="name">key</xsl:attribute>
                                    <xsl:attribute name="select">@key</xsl:attribute>
                                </xsl:element>
                            </xsl:element>
                        </xsl:element>
                        <xsl:element name="xsl:otherwise">
                            <xsl:element name="xsl:variable">
                                <xsl:attribute name="name">nodes</xsl:attribute>
                                <!--<xsl:attribute name="as">element()*</xsl:attribute>-->
                                <xsl:apply-templates select="xs:complexContent | xs:sequence | xs:choice"/>
                            </xsl:element>
                            
                            
                            <xsl:element name="xsl:choose">
                                <xsl:element name="xsl:when">
                                    <xsl:attribute name="test"><xsl:text>$key</xsl:text></xsl:attribute>
                                    
                                    <xsl:element name="xsl:element">
                                        <xsl:attribute name="name">{$key}</xsl:attribute>
                                        <xsl:attribute name="namespace">http://hl7.org/fhir</xsl:attribute>
                                        
                                        <xsl:choose>
                                            <xsl:when test="@name = 'Element' or @name = $ResourceType-list or @name = 'ResourceContainer'">
                                                <xsl:apply-templates select="xs:complexContent/*/xs:attribute | xs:attribute"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:for-each-group select="xs:complexContent/*/xs:attribute | xs:attribute | //xs:complexType[@name = 'Element']//xs:attribute" group-by="@name">
                                                    <xsl:apply-templates select="current-group()[1]"/>
                                                </xsl:for-each-group>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                        
                                        <xsl:element name="xsl:copy-of">
                                            <xsl:attribute name="select" select="'$nodes'"/>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:element>
                                <xsl:element name="xsl:otherwise">
                                    <xsl:element name="xsl:copy-of">
                                        <xsl:attribute name="select" select="'$nodes'"/>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>
                        </xsl:element>
                    </xsl:element>
                </xsl:when>
                <xsl:when test="xs:complexContent/xs:extension[@base = 'Element']/xs:attribute[@name = 'value']">
                    <xsl:element name="xsl:apply-templates">
                        <xsl:attribute name="select">.</xsl:attribute>
                        
                        <xsl:element name="xsl:with-param">
                            <xsl:attribute name="name">key</xsl:attribute>
                            <xsl:attribute name="select"><xsl:text>$key</xsl:text></xsl:attribute>
                        </xsl:element>
                    </xsl:element>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:element name="xsl:apply-templates">
                        <xsl:attribute name="select">.</xsl:attribute>
                        <xsl:attribute name="mode" select="xs:complexContent/xs:extension/@base"/>
                        
                        <xsl:element name="xsl:with-param">
                            <xsl:attribute name="name">key</xsl:attribute>
                            <xsl:attribute name="select"><xsl:text>$key</xsl:text></xsl:attribute>
                        </xsl:element>
                    </xsl:element>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:element>
    </xsl:template>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="xs:complexContent">
        <xsl:apply-templates select="* except (xs:annotation | xs:attribute)"/>
    </xsl:template>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="xs:extension">
        <xsl:variable name="base" select="@base"/>
        <xsl:element name="xsl:apply-templates">
            <xsl:attribute name="select">.</xsl:attribute>
            <xsl:attribute name="mode" select="$base"/>
        </xsl:element>
        <xsl:apply-templates select="* except (xs:annotation | xs:attribute)"/>
    </xsl:template>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="xs:restriction">
        <xsl:apply-templates select="xs:sequence"/>
    </xsl:template>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="xs:attribute">
        <xsl:element name="xsl:if">
            <xsl:attribute name="test">*[@key = '<xsl:value-of select="@name"/>']</xsl:attribute>
            <xsl:element name="xsl:attribute">
                <xsl:attribute name="name" select="@name"/>
                <xsl:attribute name="select">*[@key = '<xsl:value-of select="@name"/>']</xsl:attribute>
            </xsl:element>
        </xsl:element>
    </xsl:template>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="xs:include[@schemaLocation = 'valueset.xsd' or @schemaLocation = 'codesystem.xsd']">
        <xsl:element name="xsl:import">
            <xsl:attribute name="href"><xsl:value-of select="concat(substring-before(@schemaLocation, '.xsd'), '.xsl')"/></xsl:attribute>
        </xsl:element>
    </xsl:template>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="xs:include"/>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="xs:simpleType"/>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="xs:choice">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="xs:annotation"/>
    
    <xd:doc>
        <xd:desc/>
        <xd:param name="doParam"/>
    </xd:doc>
    <xsl:template name="doAnnotation">
        <xsl:param name="doParam" select="true()" as="xs:boolean"/>
        <xd:doc>
            <xd:desc>
                <xsl:for-each select="xs:annotation/xs:documentation">
                    <xd:p>
                        <xsl:copy-of select="node()" copy-namespaces="no"/>
                    </xd:p>
                </xsl:for-each>
            </xd:desc>
            <xsl:if test="$doParam">
                <xd:param name="key">Optional string value for the @key attribute</xd:param>
            </xsl:if>
        </xd:doc>
    </xsl:template>
    
    <xd:doc>
        <xd:desc/>
        <xd:param name="type"/>
    </xd:doc>
    <xsl:template name="getElementType" as="xs:string">
        <xsl:param name="type" select="@name" as="xs:string"/>
        
        <xsl:variable name="complexType" select="$this/xs:complexType[@name = $type]" as="element()?"/>
        <xsl:variable name="complexTypeElementType" select="$complexType/xs:complexContent/xs:extension[@base = 'Element'][empty(xs:sequence/*)][count(xs:attribute) = 1]/xs:attribute[@name = 'value']/@type"/>
        <xsl:variable name="simpleTypeType" select="$this/xs:simpleType[@name = $complexTypeElementType]/xs:restriction/tokenize(@base, '-')[1]" as="xs:string?"/>
        
        <xsl:value-of select="replace(($simpleTypeType, tokenize($complexTypeElementType, '-')[1], $type)[1], '^xs:', '')"/>
    </xsl:template>
    
    <xd:doc>
        <xd:desc/>
        <xd:param name="type"/>
    </xd:doc>
    <xsl:template name="getElementName" as="xs:string">
        <xsl:param name="type" as="xs:string"/>
        
        <xsl:choose>
            <xsl:when test="$type = 'integer'">number</xsl:when>
            <xsl:when test="$type = 'unsignedInt'">number</xsl:when>
            <xsl:when test="$type = 'positiveInt'">number</xsl:when>
            <xsl:when test="$type = 'decimal'">number</xsl:when>
            <xsl:when test="$type = 'count'">number</xsl:when>
            <xsl:when test="$type = 'boolean'">boolean</xsl:when>
            <xsl:otherwise>string</xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xd:doc>
        <xd:desc/>
        <xd:param name="type"/>
    </xd:doc>
    <xsl:function name="nf:isPrimitive" as="xs:boolean">
        <xsl:param name="type" as="xs:string?"/>
        
        <xsl:choose>
            <xsl:when test="matches($type, '^[a-z]')">
                <xsl:value-of select="true()"/>
            </xsl:when>
            <xsl:when test="$this/xs:simpleType[@name = $type]">
                <xsl:value-of select="true()"/>
            </xsl:when>
            <xsl:when test="$this/xs:complexType[@name = $type]/xs:complexContent/xs:extension[@base = 'Element'][empty(xs:sequence/*)][count(xs:attribute) = 1]/xs:attribute[@name = 'value']">
                <xsl:value-of select="true()"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="false()"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
</xsl:stylesheet>