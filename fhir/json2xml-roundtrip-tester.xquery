xquery version "3.1";


let $startf             := xmldb:store('/db/apps/fhir', 'ts.xml', <x>{current-dateTime()}</x>, 'application/xml')
let $start              := xmldb:last-modified('/db/apps/fhir', 'ts.xml')
let $del                := xmldb:remove('/db/apps/fhir', 'ts.xml')
(:
Download these and upload to your database
http://hl7.org/fhir/DSTU2/examples.zip
http://hl7.org/fhir/STU3/examples.zip
http://hl7.org/fhir/R4/examples.zip

Export the examples-xml collections from the differetn versions and compare those to the originals.
:)

let $collections        :=
    (
        '/db/apps/fhir/1.0/resources/examples',
        '/db/apps/fhir/3.0/resources/examples',
        '/db/apps/fhir/4.0/resources/examples'
    )
    
let $json2xml           :=
    (
        '/db/apps/fhir/1.0/resources/stylesheets/fhir-json2xml.xsl',
        '/db/apps/fhir/3.0/resources/stylesheets/fhir-json2xml.xsl',
        '/db/apps/fhir/4.0/resources/stylesheets/fhir-json2xml.xsl'
    )
    
let $xml2json           :=
    (
        '/db/apps/fhir/1.0/resources/stylesheets/fhir-xml2json.xsl',
        '/db/apps/fhir/3.0/resources/stylesheets/fhir-xml2json.xsl',
        '/db/apps/fhir/4.0/resources/stylesheets/fhir-xml2json.xsl'
    )

let $xsd                :=
    (
        '/db/apps/fhir/1.0/resources/schemas/fhir-single.xsd',
        '/db/apps/fhir/3.0/resources/schemas/fhir-single.xsd',
        '/db/apps/fhir/4.0/resources/schemas/fhir-single.xsd'
    )

let $doJson             :=
    for $coll at $i in $collections
    (:let $outputxexp     := xmldb:create-collection(string-join(tokenize($coll, '/')[position() != last()], '/'), tokenize($coll, '/')[last()] || '-x2exp'):)
    let $outputjson     := xmldb:create-collection(string-join(tokenize($coll, '/')[position() != last()], '/'), tokenize($coll, '/')[last()] || '-json')
    (:let $outputjexp     := xmldb:create-collection(string-join(tokenize($coll, '/')[position() != last()], '/'), tokenize($coll, '/')[last()] || '-j2exp'):)
    let $outputxml      := xmldb:create-collection(string-join(tokenize($coll, '/')[position() != last()], '/'), tokenize($coll, '/')[last()] || '-xml')
    order by $coll
    return (
        for $res in xmldb:get-child-resources($coll)
        order by $res
        return
            try {
                let $jx := transform:transform(doc($coll || '/' || $res), doc($xml2json[$i]), ())
                (:let $jd := xmldb:store($outputxexp  ,  $res                           , $jx, 'application/xml'):)
                let $jd := xmldb:store($outputjson  , replace($res, '.xml$', '.json') , xml-to-json($jx)   , 'application/json')
                let $xj := <x>{json-doc($jd) => fn:serialize(map {'method':'json'})}</x> (:=> json-to-xml():)
                (:let $jd := xmldb:store($outputjexp  ,  $res                           , json-doc($jd) => fn:serialize(map {'method':'json'}) => json-to-xml(), 'application/xml'):)
                let $xj := transform:transform($jx                      , doc($json2xml[$i]), ())
                let $xd := xmldb:store($outputxml   ,  $res                           , $xj                , 'application/xml')
                return ()
                           
                
            }
            catch * {
                error(xs:QName($err:code), $err:description || $coll || '/' || $res)
            }
    )

let $startf             := xmldb:store('/db/apps/fhir', 'ts.xml', <x>{current-dateTime()}</x>, 'application/xml')
let $end                := xmldb:last-modified('/db/apps/fhir', 'ts.xml')
let $del                := xmldb:remove('/db/apps/fhir', 'ts.xml')

return <x><start>{$start}</start><ends>{$end}</ends></x>