xquery version "1.0";
(:
	Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
	see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace xmldb       = "http://exist-db.org/xquery/xmldb";
import module namespace sm          = "http://exist-db.org/xquery/securitymanager";
import module namespace repo        = "http://exist-db.org/xquery/repo";
import module namespace adpfix      = "http://art-decor.org/ns/art-decor-permissions" at "../../../art/api/api-permissions.xqm";

(: The following external variables are set by the repo:deploy function :)

(: file path pointing to the exist installation directory :)
declare variable $home external;
(: path to the directory containing the unpacked .xar package :)
declare variable $dir external;
(: the target collection into which the app is deployed :)
declare variable $target external;
(:install path for art (/db, /db/apps), no trailing slash :)
declare variable $root := repo:get-root();

(:
<restURI for="VS" format="XML">https://art-decor.org/decor/services/RetrieveValueSet?id=__ID__&amp;effectiveDate=__ED__&amp;language=__LANG__&amp;prefix=__PFX__&amp;format=xml</restURI>
<restURI for="FHIR" format="1.0">https://art-decor.org/fhir/1.0/</restURI>
:)
declare function local:updateRestURI() {
    let $strServerInfo  := '/db/apps/art-data/server-info.xml'
    let $docServerInfo  := if (doc-available($strServerInfo)) then doc($strServerInfo)/* else ()
    let $fhirUrl        := ($docServerInfo/url-fhir-services[string-length() gt 0])[1]
    let $decorUrl       := ($docServerInfo/url-art-decor-services[string-length() gt 0])[1]
    
    let $update         :=
        if ($decorUrl) then
            for $r in collection($target)/decor/project/restURI[not(@for = 'FHIR')]
            return
                update value $r with replace($r, 'https?://art-decor.org/decor/services/', $decorUrl)
        else ()
    let $update         :=
        if ($fhirUrl) then
            for $r in collection($target)/decor/project/restURI[@for = 'FHIR']
            return
                update value $r with replace($r, '^https?://art-decor.org/fhir/', $fhirUrl)
        else () 
    
    return ()
};

adpfix:setDecorPermissions(),
local:updateRestURI()
