function handleSelectSystemChange(selector) {
    var currentSystem = selector.options[selector.selectedIndex].value;
    
    var inputfield = document.getElementById('codeSystemSelector');
    inputfield.value = currentSystem;
};
function handleInputSystemChange(selector) {
    var currentSystem = selector.value;
    
    var dropdown = document.getElementById('dropdownSystems');
    var dropdownSelectedSystem = dropdown.options[dropdown.selectedIndex].value;
    
    if (dropdownSelectedSystem != currentSystem) {
        var opts = dropdown.options.length;
        for (var i=0; i<opts; i++){
            if (dropdown.options[i].value == currentSystem){
                dropdown.options[i].selected = true;
                break;
            }
        }
    }
};