var myUrl = new URLSearchParams(window.location.search)
var decor = {};
decor.strLanguage = myUrl.get('language');
decor.strId = myUrl.get('id');
decor.strEffectiveDate = myUrl.get('effectiveDate');
decor.strPrefix = myUrl.get('prefix');
decor.strVersion = myUrl.get('version');

decor.strFhirVersion = myUrl.get('fhirVersion');
if (decor.strFhirVersion) {} else { decor.strFhirVersion = '4.0' };

function getQuestionnaire() {
return new Promise(
    function(resolve, reject) {
        var request = new XMLHttpRequest();
        var strId;
        if (decor.strEffectiveDate) {
            //2.16.840.1.113883.3.1937.99.60.10.4.8--2023-02-23T00:00:00
            //2.16.840.1.113883.3.1937.99.60.10.4.8--20230223000000
            strId = decor.strId + '--' + decor.strEffectiveDate.replace(/\D/g, '')
        }
        else {
            strId = decor.strId
        }
        var strUrl;
        if (decor.strVersion) {
            strUrl = '../../fhir/' + decor.strFhirVersion + '/' + decor.strPrefix + decor.strVersion.replace(/\D/g, '') + '/Questionnaire/' + strId
        }
        else {
            strUrl = '../../fhir/' + decor.strFhirVersion + '/public/Questionnaire/' + strId
        }
        request.onload = function() {
            if (request.status == 200) {
                resolve(request.responseText);
            }
            else {
                reject('Error, got status: ' + request.status + '\nbody: ' + request.responseText + '\nfor query: ' + strUrl);
            }
            document.getElementById("loading").innerHTML = "";
        }
        document.getElementById("loading").innerHTML = "Loading...";
        request.open("GET", strUrl + '?_format=json', true);
        request.send();
    });
}

function initForm(json) {
    var fhirQ = JSON.parse(json) 
    // Convert FHIR Questionnaire to LForms format
    var lformsQ = LForms.FHIR.R4.SDC.convertQuestionnaireToLForms(fhirQ);
    
    // Turn off the top-level questions and controls (optional)
    lformsQ.templateOptions = {
        showFormHeader: false,
        hideFormControls: true,
        allowHTMLInInstructions: true,
        showCodingInstruction: true
    };
        
    // Add the form to the page
    LForms.Util.addFormToPage(lformsQ, formContainer);
}

function formError(error) {
    console.error(error);
    window.alert(error);
}

// Define the function for showing the QuestionnaireResponse
function showQR() {
    var qr = LForms.Util.getFormFHIRData('QuestionnaireResponse', 'R4');
    window.alert(JSON.stringify(qr, null, 2));
}

getQuestionnaire().then(initForm, formError)