var searchstring = '&id=2.16.840.1.113883.3.1937.99.62.3.1.1&effectiveDate=2012-05-30T11:32:36&language=nl-NL&version=2018-11-28T15:20:52'
var searchstring = '&id=2.16.840.1.113883.2.4.3.11.60.90.77.1.6&effectiveDate=2016-09-08T00:00:00&language=nl-NL&version=2018-11-29T11:31:05'
var decorSearch = {}

function getConcepts() {
    var searchstring = new URLSearchParams();
    var promise;
    decorSearch.search = $('#search').val();
    searchstring.append("search", decorSearch.search);
    searchstring.append("id", decorSearch.id);
    searchstring.append("effectiveDate", decorSearch.effectiveDate);
    searchstring.append("version", decorSearch.version);
    promise = $.getJSON('../modules/SearchConcept.xquery?' + searchstring.toString());
    $('#found').text("Searching: " + decorSearch.search + "...");

    promise.done(function(data) {
        var template = $('#conceptTemplate').html();
        var html = Mustache.to_html(template, data);
        $('#results').html(html);
        $('#found').text("Searched: " + data.search + " found: " + data.count);
        // Do this with .each
        // var text = $('#results').html();
        var regex = new RegExp(decorSearch.search, "gi");
        // var boldened = text.replace(regex, '<span style="background-color: lightgray">$&</span>')
        // $('#results').html(boldened);
        $(".desc").each(function(){
            var text = $(this).html();
            $(this).html(text.replace(regex, '<span style="background-color: lightgray">$&</span>'));
        });
    });
}

$(document).ready(function(){
    // Get the input field
    var inputField = document.getElementById("search");
    var inParams = new URLSearchParams(window.location.search);
    decorSearch.search = inParams.get('search');
    decorSearch.id = inParams.get('id');
    decorSearch.effectiveDate = inParams.get('effectiveDate');
    decorSearch.version = inParams.get('version');
    if (decorSearch.search != null) {
        $("#search").val(decorSearch.search);
        getConcepts();
    }

    // Execute a function when the user releases a key on the keyboard
    inputField.addEventListener("keyup", function(event) {
      // Number 13 is the "Enter" key on the keyboard
      if (event.keyCode === 13) {
        // Cancel the default action, if needed
        event.preventDefault();
        // Trigger the button element with a click
        document.getElementById("btnSubmit").click();
      }
    });
});
