
function selectChildren(div) {
    var divid = div.id;
    var isactive = div.getElementsByTagName("input")[0].checked;
    
    var divcontents = document.getElementById('content' + divid);
    var inputs = divcontents.getElementsByTagName("input");
    
    if (isactive) { divcontents.style.display = ''; } else  { divcontents.style.display = 'none'; }
    
    for (i = 0; i < inputs.length; i++) {
        if (! inputs[i].disabled) { inputs[i].checked = isactive };
    }
};