<!-- 
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
    
    This program is free software; you can redistribute it and/or modify it under the terms 
    of the GNU General Public License as published by the Free Software Foundation; 
    either version 3 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU General Public License for more details.
    
    See http://www.gnu.org/licenses/gpl.html
-->
<xsl:stylesheet xmlns:svg="http://www.w3.org/2000/svg" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="#all" version="2.0">
    <xsl:output indent="yes" omit-xml-declaration="yes"/>
    
    <xsl:param name="decorServices" select="//decor/@deeplinkprefixservices"/>
    <xsl:param name="projectPrefix" select="//decor/project/@prefix"/>
    <xsl:param name="projectVersion" select="//decor/@versionDate"/>
    <xsl:param name="projectLanguage" select="(//decor/@language, //decor/project/@defaultLanguage)[1]"/>
    <xsl:param name="projectLabel" select="(//decor/@versionLabel, //decor/project/(version|release)/@versionLabel)[1]"/>
    <xsl:param name="transactions" as="xs:string?"/>
    
    <!-- xs:string of id#effectiveDate for transactions to include -->
    <xsl:variable name="transactionLookup" select="tokenize($transactions, ',')" as="xs:string*"/>
    
    <xsl:template match="/">
        <xsl:variable name="transactionList" select="if (empty($transactionLookup)) then //transaction[representingTemplate/@sourceDataset] else //transaction[concat(@id, '#', @effectiveDate) = $transactionLookup]" as="element()*"/>
        <ada xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="{replace($decorServices, '/decor/services/?', '')}/ada/core/ada.xsd">
            <project prefix="{$projectPrefix}" language="{$projectLanguage}" versionDate="{$projectVersion}">
                <release baseUri="{$decorServices}RetrieveTransaction"/>
            </project>
            <applications>
                <application version="1">
                    <views>
                        <view id="1" type="index" target="xforms">
                            <name>
                                <xsl:value-of select="//decor/project/(name[@language = $projectLanguage], name)[1]"/>
                                <xsl:text> </xsl:text>
                                <xsl:value-of select="$projectLabel"/>
                                <xsl:text> Index</xsl:text>
                            </name>
                            <xsl:for-each select="2 to (count($transactionList) + 1)">
                                <indexOf ref="{.}"/>
                            </xsl:for-each>
                            <concepts include="all"/>
                        </view>
                        <xsl:for-each select="$transactionList">
                            <view id="{position() + 1}" type="crud" target="xforms" transactionId="{@id}" transactionEffectiveDate="{@effectiveDate}">
                                <name>
                                    <xsl:value-of select="(name[@language = $projectLanguage], name)[1]"/>
                                </name>
                                <concepts include="all"/>
                            </view>
                        </xsl:for-each>
                    </views>
                </application>
            </applications>
        </ada>
    </xsl:template>
</xsl:stylesheet>