<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fun="http://art-decor.org/fun" exclude-result-prefixes="xs" version="2.0">
    <xsl:function name="fun:shortName">
        <xsl:param name="name"/>
        <!-- find matching alternatives for more or less common diacriticals, replace single spaces with _ , replace ? with q (same name occurs quite often twice, with and without '?') -->
        <xsl:variable name="r1" select="translate(normalize-space(lower-case($name)),' àáãäåèéêëìíîïòóôõöùúûüýÿç€ßñ?','_aaaaaeeeeiiiiooooouuuuyycEsnq')"/>
        <!-- ditch anything that's not alpha numerical or underscore -->
        <xsl:variable name="r2" select="replace($r1,'[^a-zA-Z\d_]','')"/>
        <!-- make sure we do not start with a digit -->
        <xsl:value-of select="if (matches($r2,'^\d')) then concat('_',$r2) else $r2"/>
    </xsl:function>
    <xsl:function name="fun:longDate">
        <xsl:param name="shortDate"/>
        <xsl:value-of select="concat(substring($shortDate, 1, 4), '-', substring($shortDate, 5, 2), '-', substring($shortDate, 7, 2), 'T', substring($shortDate, 9, 2), ':', substring($shortDate, 11, 2), ':', substring($shortDate, 13, 2))"/>
    </xsl:function>
    <xsl:function name="fun:decor2questionnaireType">
        <!-- Input param is a concept, since we need to look at group -->
        <xsl:param name="concept"/>
        <xsl:variable name="decorType" select="$concept/valueDomain/@type"/>
        <xsl:choose>
            <xsl:when test="$concept/@type='group'">group</xsl:when>
            <xsl:when test="$decorType = ('boolean', 'date', 'decimal', 'quantity', 'string')">
                <xsl:value-of select="$decorType"/>
            </xsl:when>
            <xsl:when test="$decorType = 'text'">string</xsl:when>
            <xsl:when test="$decorType = 'code' and $concept/valueSet/conceptList/(concept | exception)">choice</xsl:when>
            <!-- For instance whole codesystem -->
            <xsl:when test="$decorType = 'code'">string</xsl:when>
            <xsl:when test="$decorType = 'count'">integer</xsl:when>
            <xsl:when test="$decorType = 'datetime'">dateTime</xsl:when>
            <xsl:when test="$decorType = 'quantity'">quantity</xsl:when>
            <xsl:when test="$decorType = 'duration'">decimal</xsl:when>
            <xsl:when test="$decorType = 'identifier'">string</xsl:when>
            <xsl:when test="$decorType = 'ordinal'">choice</xsl:when>
            <xsl:otherwise>string</xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    <xsl:function name="fun:decor2questionnaireAnswerType">
        <!-- Input param is a concept, since we need to look at group -->
        <xsl:param name="concept"/>
        <xsl:variable name="decorType" select="$concept/valueDomain/@type"/>
        <xsl:choose>
            <xsl:when test="$decorType = 'boolean'">valueBoolean</xsl:when>
            <xsl:when test="$decorType = 'date'">valueDate</xsl:when>
            <xsl:when test="$decorType = 'decimal'">valueDecimal</xsl:when>
            <xsl:when test="$decorType = 'quantity'">valueQuantity</xsl:when>
            <xsl:when test="$decorType = 'string'">valueString</xsl:when>
            <xsl:when test="$decorType = 'text'">valueString</xsl:when>
            <xsl:when test="$decorType = 'count'">valueInteger</xsl:when>
            <xsl:when test="$decorType = 'datetime'">valueDateTime</xsl:when>
            <xsl:when test="$decorType = 'duration'">valueDecimal</xsl:when>
            <xsl:when test="$decorType = 'identifier'">valueString</xsl:when>
            <xsl:otherwise/>
        </xsl:choose>
    </xsl:function>
    <xsl:function name="fun:fhir2questionnaireType">
        <!-- Questionnaire does not support all FHIR types, so we need to convert sometimes -->
        <xsl:param name="fhirType"/>
        <xsl:choose>
            <xsl:when test="$fhirType = 'Count'">Integer</xsl:when>
            <xsl:when test="$fhirType = 'Identifier'">String</xsl:when>
            <xsl:when test="$fhirType = 'CodeableConcept'">Coding</xsl:when>
            <xsl:when test="$fhirType = 'SimpleQuantity'">Quantity</xsl:when>
            <xsl:when test="$fhirType = 'Duration'">Quantity</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$fhirType"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <!-- 
    DECOR types not in FHIR observation:
    count, blob, complex, currency, date, decimal, identifier	
    
    FHIR observation types not in DECOR:
    Range, SampledData, Attachment, Time, Period
    -->
    <xsl:function name="fun:decor2observationType">
        <!-- Input param is a concept, since we need to look at group -->
        <xsl:param name="concept"/>
        <xsl:variable name="decorType" select="$concept/valueDomain/@type"/>
        <xsl:choose>
            <xsl:when test="$decorType = ('boolean', 'quantity', 'string')">
                <xsl:value-of select="$decorType"/>
            </xsl:when>
            <xsl:when test="$decorType = 'text'">string</xsl:when>
            <xsl:when test="$decorType = ('code', 'ordinal')">CodeableConcept</xsl:when>
            <xsl:when test="$decorType = 'datetime'">dateTime</xsl:when>
            <xsl:when test="$decorType = 'duration'">Duration</xsl:when>
            <xsl:when test="$decorType = 'ratio'">Ratio</xsl:when>
            <xsl:otherwise>string</xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    <xsl:function name="fun:getTargetById">
        <xsl:param name="map"/>
        <xsl:param name="id"/>
    </xsl:function>
    <xsl:function name="fun:codeSystemUrl">
        <xsl:param name="codeSystem"/>
        <xsl:choose>
            <xsl:when test="$codeSystem = '2.16.840.1.113883.6.1'">http://loinc.org</xsl:when>
            <xsl:when test="$codeSystem = '2.16.840.1.113883.6.8'">http://unitsofmeasure.org</xsl:when>
            <xsl:when test="$codeSystem = '2.16.840.1.113883.6.96'">http://snomed.info/sct</xsl:when>
            <xsl:when test="$codeSystem = '2.16.840.1.113883.6.256'">http://www.radlex.org</xsl:when>
            <xsl:when test="$codeSystem = '2.16.840.1.113883.5.1008'">http://hl7.org/fhir/v3/NullFlavor</xsl:when>
            <!-- Do all the others -->
            <xsl:otherwise>urn:oid:<xsl:value-of select="$codeSystem"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    <xsl:variable name="ucumDoc" select="if (doc-available('/db/apps/decor/core/DECOR-ucum.xml')) then doc('/db/apps/decor/core/DECOR-ucum.xml')/* else ()"/>
    <xsl:function name="fun:getUcum" as="element()*">
        <xsl:param name="in" as="xs:string?"/>
        <xsl:copy-of select="$ucumDoc/ucum[@unit = $in]"/>
    </xsl:function>
</xsl:stylesheet>