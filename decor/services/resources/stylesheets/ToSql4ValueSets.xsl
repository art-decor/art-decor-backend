<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    <xsl:output method="text" indent="no"/>
    <xsl:template match="/">
        <xsl:apply-templates select="//valueSet | //codeSystem"/>
    </xsl:template>
    <xsl:template match="valueSet[conceptList/concept]">
    --DROP TABLE <xsl:value-of select="translate(@name, '-', '_')"/> CASCADE;
    CREATE TABLE <xsl:value-of select="translate(@name, '-', '_')"/> (
            <xsl:value-of select="translate(@name, '-', '_')"/>_id int NOT NULL PRIMARY KEY,
            code varchar(255) NOT NULL,
            codeSystem varchar(255) NOT NULL,
            displayName varchar(255) NOT NULL
        );
        <xsl:for-each select="conceptList/concept">
    INSERT INTO <xsl:value-of select="translate(../../@name, '-', '_')"/> (<xsl:value-of select="translate(../../@name, '-', '_')"/>_id, code, codeSystem, displayName)
            VALUES (<xsl:value-of select="if (@localId) then @localId else position()"/>, '<xsl:value-of select="@code"/>', '<xsl:value-of select="@codeSystem"/>', '<xsl:value-of select="translate(@displayName, '''', '')"/>');
        </xsl:for-each>
    </xsl:template>
    <!--
        <codeSystem id="2.16.840.1.113883.3.1937.99.62.3.5.1" name="cs-measured-by" displayName="Measured by" effectiveDate="2018-07-25T15:22:56" statusCode="final">
            <conceptList>
                <codedConcept code="P" level="0" type="L" statusCode="active">
                    <designation displayName="Patiënt" language="nl-NL" type="preferred"/>
                    <designation displayName="Patient" language="en-US" type="preferred"/>
                </codedConcept>
            </conceptList>
        </codeSystem>
    -->
    <xsl:template match="codeSystem[conceptList/codedConcept]">
        <xsl:variable name="codeSystemId" select="@id"/>
        --DROP TABLE <xsl:value-of select="translate(@name, '-', '_')"/> CASCADE;
        CREATE TABLE <xsl:value-of select="translate(@name, '-', '_')"/> (
            <xsl:value-of select="translate(@name, '-', '_')"/>_id int NOT NULL PRIMARY KEY,
            code varchar(255) NOT NULL,
            codeSystem varchar(255) NOT NULL,
            displayName varchar(255) NOT NULL,
            statusCode varchar(255),
            effectiveDate varchar(255),
            expirationDate varchar(255),
            officialReleaseDate varchar(255),
        );
        <xsl:for-each select="conceptList/codedConcept">
            INSERT INTO <xsl:value-of select="translate(../../@name, '-', '_')"/> (<xsl:value-of select="translate(../../@name, '-', '_')"/>_id, code, codeSystem, displayName, statusCode, effectiveDate, expirationDate, officialReleaseDate)
            VALUES (<xsl:value-of select="if (@localId) then @localId else position()"/>, '<xsl:value-of select="@code"/>', '<xsl:value-of select="$codeSystemId"/>', '<xsl:value-of select="translate(designation[1]/@displayName, '''', '')"/>', '<xsl:value-of select="translate(@statusCode, '''', '')"/>', '<xsl:value-of select="translate(@effectiveDate, '''', '')"/>', '<xsl:value-of select="translate(@expirationDate, '''', '')"/>', '<xsl:value-of select="translate(@officialReleaseDate, '''', '')"/>');
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="@*|node()">
        <xsl:apply-templates select="@*|node()"/>
    </xsl:template>
</xsl:stylesheet>