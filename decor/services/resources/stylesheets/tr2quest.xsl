<xsl:stylesheet xmlns="http://hl7.org/fhir" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fun="http://art-decor.org/fun" version="2.0" exclude-result-prefixes="#all">
    <xsl:import href="fhir-common.xsl"/>
    <xsl:output method="xml" indent="yes"/>
    
    <!-- This is the base for canonical URI's -->
    <!-- This is where the generated resources reside, can be local disk for testing -->
    <xsl:variable name="params" as="element(params)">
        <params xmlns="" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../decor2fhir/schemas/params.xsd">
            <fhirIdStyle value="idDisplay"/>
            <fhirCanonicalBase value="https://art-decor.org/fhir/"/>
            <publisher value="ART-DECOR Expert Group"/>
            <language value="en-US"/>
        </params>
    </xsl:variable>
    <xsl:variable name="language" select="if ($params/language) then $params/language/@value/string() else 'nl-NL'"/>
    <xsl:variable name="fhirIdStyle" select="$params/fhirIdStyle/@value/string()"/>
    <xsl:variable name="fhirCanonicalBase" select="$params/fhirCanonicalBase/@value/string()"/>
    <xsl:variable name="publisher" select="$params/publisher/@value/string()"/>
    <xsl:key name="target" match="target" use="../source/@id"/>
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="dataset">
        <xsl:variable name="transactionId" select="@transactionId/string()"/>
        <xsl:variable name="targets" select="if ($params/targets) then $params/targets/id/text() else concept/@id/string()"/>
        <xsl:if test="count($targets) = 0">
            <xsl:message>ERROR: no targets for this transaction</xsl:message>
        </xsl:if>
        <Questionnaire xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <id value="{@transactionIddisplay}"/>
            <text>
                <status value="generated"/>
                <div xmlns="http://www.w3.org/1999/xhtml">Questionnaire for <xsl:value-of select="@shortName"/>
                </div>
            </text>
            <url value="{$fhirCanonicalBase}Questionnaire/Q_{@shortName}"/>
            <!-- TODO: distinct valuesets -->
            <!-- For now, choice is explicitly listed in option/code -->
            <!--<xsl:apply-templates select="//valueSet"/>-->
            <name value="{@shortName}"/>
            <title value="{name}"/>
            <status value="draft"/>
            <subjectType value="Patient"/>
            <date value="2016-04-14"/>
            <xsl:if test="$publisher">
                <publisher value="{$publisher}"/>
            </xsl:if>
            <xsl:variable name="datasetRoot" select="."/>
            <xsl:for-each select="$targets">
                <xsl:variable name="conceptId" select="."/>
                <xsl:apply-templates select="$datasetRoot//concept[@id=$conceptId]"/>
            </xsl:for-each>
        </Questionnaire>
    </xsl:template>
    <xsl:template match="concept[@type = 'group']">
        <xsl:variable name="id" select="@id"/>
        <!--<xsl:variable name="targetUrl" select="$map/key('target', $id)/@url"/>-->
        <xsl:variable name="targetId" select="if ($fhirIdStyle='idDisplay') then @iddisplay else concat(@id, '--', @effectiveDate)"/>
        <item>
            <linkId value="{$targetId}"/>
            <!-- We use definitions for primitive data types only -->
            <!--<definition value="{$targetUrl}"/>-->
            <text value="{name}"/>
            <type value="group"/>
            <xsl:choose>
                <xsl:when test="@minimumMultiplicity = '1'">
                    <required value="true"/>
                </xsl:when>
                <xsl:otherwise>
                    <required value="false"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
                <xsl:when test="@maximumMultiplicity != '1'">
                    <repeats value="true"/>
                </xsl:when>
                <xsl:otherwise>
                    <repeats value="false"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates/>
        </item>
    </xsl:template>
    <xsl:template match="concept[@type = 'item']">
        <xsl:variable name="id" select="@id"/>
        <!--<xsl:variable name="targetUrl" select="$map/key('target', $id)/@url"/>-->
        <xsl:variable name="targetId" select="if ($fhirIdStyle='idDisplay') then @iddisplay else concat(@id, '--', @effectiveDate)"/>
        <xsl:variable name="defaultValue" select="(valueDomain/property/@default)[1]"/>
        <!-- In addition, the following extensions MUST be supported: minValue, maxValue, minLength, maxDecimalPlaces, unit -->
        <xsl:variable name="minInclude" select="max(valueDomain/property[@minInclude castable as xs:decimal]/xs:decimal(@minInclude))" as="xs:decimal?"/>
        <xsl:variable name="maxInclude" select="max(valueDomain/property[@maxInclude castable as xs:decimal]/xs:decimal(@maxInclude))" as="xs:decimal?"/>
        <xsl:variable name="minLength" select="max(valueDomain/property[@minLength castable as xs:decimal]/xs:decimal(@minLength))" as="xs:decimal?"/>
        <xsl:variable name="maxDecimalPlaces" select="max(valueDomain/property[replace(@fractionDigits, '\D', '') castable as xs:integer]/xs:integer(replace(@fractionDigits, '\D', '')))" as="xs:integer?"/>
        <item>
            <xsl:if test="not(empty($minInclude))">
                <extension url="http://hl7.org/fhir/StructureDefinition/minValue">
                    <xsl:choose>
                        <xsl:when test="$minInclude castable as xs:integer">
                            <valueInteger value="{xs:integer($minInclude)}"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <valueDecimal value="{$minInclude}"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </extension>
            </xsl:if>
            <xsl:if test="not(empty($maxInclude))">
                <extension url="http://hl7.org/fhir/StructureDefinition/maxValue">
                    <xsl:choose>
                        <xsl:when test="$maxInclude castable as xs:integer">
                            <valueInteger value="{xs:integer($maxInclude)}"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <valueDecimal value="{$maxInclude}"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </extension>
            </xsl:if>
            <xsl:if test="not(empty($minLength))">
                <extension url="http://hl7.org/fhir/StructureDefinition/minLength">
                    <valueInteger value="{xs:integer($minLength)}"/>
                </extension>
            </xsl:if>
            <xsl:if test="valueDomain/@type='quantity'">
                <xsl:for-each select="valueDomain/property/@unit">
                    <xsl:variable name="ucumUnit" select="fun:getUcum(.)"/>
                    <extension url="http://hl7.org/fhir/StructureDefinition/questionnaire-unit">
                        <valueCoding>
                            <xsl:if test="$ucumUnit">
                                <system value="{fun:codeSystemUrl('2.16.840.1.113883.6.8')}"/>
                                <code value="{.}"/>
                            </xsl:if>
                            <display value="{($ucumUnit/@displayName[not(. = '')], .)[1]}"/>
                        </valueCoding>
                    </extension>
                </xsl:for-each>
            </xsl:if>
            <xsl:if test="not(empty($maxDecimalPlaces))">
                <extension url="http://hl7.org/fhir/StructureDefinition/maxDecimalPlaces">
                    <valueInteger value="{$maxDecimalPlaces}"/>
                </extension>
            </xsl:if>
            <!--Identifies how the specified element should be rendered when displayed.-->
            <xsl:if test="not(empty(@renderingStyle))">
                <extension url="http://hl7.org/fhir/StructureDefinition/rendering-style">
                    <valueString value="{@renderingStyle}"/>
                </extension>
            </xsl:if>
            <linkId value="{$targetId}"/>
            <xsl:for-each select="terminologyAssociation[@conceptId=$id]">
                <code>
                    <system value="{fun:codeSystemUrl(@codeSystem)}"/>
                    <code value="{@code}"/>
                    <xsl:if test="@displayName">
                        <display value="{@displayName}"/>
                    </xsl:if>
                </code>
            </xsl:for-each>
            <text value="{name}"/>
            <type value="{fun:decor2questionnaireType(.)}"/>
            <xsl:choose>
                <xsl:when test="@minimumMultiplicity = '1'">
                    <required value="true"/>
                </xsl:when>
                <xsl:otherwise>
                    <required value="false"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
                <xsl:when test="@maximumMultiplicity != '1'">
                    <repeats value="true"/>
                </xsl:when>
                <xsl:otherwise>
                    <repeats value="false"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:if test="valueDomain[1]/property[1]/@fixed='true'">
                <readOnly value="true"/>
            </xsl:if>
            <xsl:if test="valueDomain[1]/property[1]/@maxLength">
                <maxLength value="{valueDomain[1]/property[1]/@maxLength}"/>
            </xsl:if>
            <xsl:for-each select="valueSet[not(completeCodeSystem | conceptList/include | conceptList/exclude)]/conceptList/(concept | exception)">
                <answerOption>
                    <valueCoding>
                        <system value="{fun:codeSystemUrl(@codeSystem)}"/>
                        <code value="{@code}"/>
                        <display value="{@displayName}"/>
                    </valueCoding>
                    <xsl:if test="@code = $defaultValue">
                        <initialSelected value="true"/>
                    </xsl:if>
                </answerOption>
            </xsl:for-each>
            <xsl:if test="valueDomain[1]/property[1]/@default">
                <!-- For code, Questionnaire expect Coding, DECOR default is just a simple string -->
                <xsl:if test="valueDomain/@type != 'code'">
                    <xsl:variable name="default" select="valueDomain[1]/property[1]/@default/string()"/>
                    <xsl:variable name="answerType" select="fun:decor2questionnaireAnswerType(.)"/>
                    <initial>
                        <xsl:element name="{$answerType}">
                            <xsl:attribute name="value" select="$default"/>
                        </xsl:element>
                    </initial>
                </xsl:if>
            </xsl:if>
        </item>
    </xsl:template>
    <xsl:template match="@* | node()"/>
</xsl:stylesheet>