xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";
import module namespace decor       = "http://art-decor.org/ns/decor" at "../../../art/api/api-decor.xqm";
(:import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../../art/api/api-server-settings.xqm";:)
import module namespace ds          = "http://art-decor.org/ns/decor-services" at "decor-services.xqm";
import module namespace artjson     = "http://art-decor.org/ns/artjson" at "artjson.xqm";

declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "json";
declare option output:media-type "application/json";
declare option output:json-ignore-whitespace-text-nodes "yes";

declare variable $codeSystemSNOMED      := '2.16.840.1.113883.6.96';
declare variable $codeSystemLOINC       := '2.16.840.1.113883.6.1';
(:performance:)
declare variable $codeSystemNames       := <cs id="{$codeSystemSNOMED}" nm="SNOMED-CT"/>|<cs id="{$codeSystemLOINC}" nm="LOINC"/>;

(:declare %private function local:textQuery($dataset as node(), $search as xs:string, $language as xs:string) as node()*{
    let $options :=
        <options>
            <filter-rewrite>yes</filter-rewrite>
        </options>
    let $doc := doc('/db/apps/decor/data/projects/peri20/peri20-decor.xml')
    let $text := for $part in tokenize($search, ' ')
                 return concat($part, '*')
    let $textquery  := string-join($text, ' AND ')
    let $results :=
        if (not($textquery) or $textquery = '**') then $doc else $doc//concept[ft:query((name[@language=$language] | desc[@language=$language] | ./valueDomain//name[@language=$language] | ./valueDomain//desc[@language=$language]), $textquery, $options)]
    let $results :=
        $results//concept[@type | inherit][not(ancestor::history)]
    return
      for $concept in $results
      return
        <concept>{
          $concept/@*,
          $concept/(* except (concept, history))
        }</concept>
};:)

(: demo 1 :)
let $id                     := if (request:exists()) then request:get-parameter('id',())[string-length()>0][1] else '2.16.840.1.113883.3.1937.99.62.3.1.1'
let $effectiveDate          := if (request:exists()) then request:get-parameter('effectiveDate',()) else '2012-05-30T11:32:36'
let $version                := if (request:exists()) then request:get-parameter('version',()) else '2018-11-28T15:20:52'
(: peri20 :)
let $id                     := if (request:exists()) then request:get-parameter('id',())[string-length()>0][1] else '2.16.840.1.113883.2.4.3.11.60.90.77.1.5'
let $effectiveDate          := if (request:exists()) then request:get-parameter('effectiveDate',()) else '2013-09-10T00:00:00'
let $version                := if (request:exists()) then request:get-parameter('version',()) else '2018-11-29T11:31:05'

let $language               := if (request:exists()) then request:get-parameter('language',()) else 'nl-NL'
let $search                 := if (request:exists()) then request:get-parameter('search',()) else 'baring'
let $conceptId              := if (request:exists()) then request:get-parameter('conceptId',())[string-length()>0][1] else ()
let $conceptEffectiveDate   := if (request:exists()) then request:get-parameter('conceptEffectiveDate',())[string-length()>0] else ()
let $communityprefix        := if (request:exists()) then request:get-parameter('community',())[string-length()>0] else ()
let $showonlyactiveconcepts := if (request:exists()) then request:get-parameter('showonlyactiveconcepts', true()) else true()

(: if we have an id find dataset or transaction. if we do not have an id but we have a conceptId, then get that dataset :)
let $transactionOrDataset   := 
    if (string-length($id) > 0) then
        art:getDataset($id, $effectiveDate, $version, $language) | art:getTransaction($id, $effectiveDate, $version, $language)
    else if (string-length($conceptId) > 0) then
        art:getConcept($conceptId, $conceptEffectiveDate, $version, $language)/ancestor::dataset
    else ()

(:concepts either are or are not part of a transaction. there's no point in filtering based on status:)
let $showonlyactiveconcepts := if ($transactionOrDataset[self::transaction]) then false() else $showonlyactiveconcepts

let $project                := ($transactionOrDataset/ancestor::decor)[1]
let $projectId              := $project/project/@id
let $projectPrefix          := $project/project/@prefix

let $language               := if (empty($language)) then $project/project/@defaultLanguage else $language
(: fix for de-CH :)
let $language               := if ($language='de-CH') then 'de-DE' else $language

let $doSubConcept           := string-length($conceptId) > 0

let $fullDatasetTree        := 
    if ($version castable as xs:dateTime) then (
        let $datasets   := 
            if ($transactionOrDataset[self::dataset]) then ($transactionOrDataset) else (
                if ($language = '*') then
                    $get:colDecorVersion//transactionDatasets[@versionDate = $version]//dataset[@transactionId = $transactionOrDataset/@id][@transactionEffectiveDate = $transactionOrDataset/@effectiveDate]
                else (
                    $get:colDecorVersion//transactionDatasets[@versionDate = $version][@language = $language]//dataset[@transactionId = $transactionOrDataset/@id][@transactionEffectiveDate = $transactionOrDataset/@effectiveDate]
                )
            )
        
        let $datasets   :=
            if ($datasets and $doSubConcept) then (
                for $dataset in $datasets
                return
                    element {$dataset/name()} {
                        $dataset/@*,
                        $dataset/(* except (concept|history)),
                        $dataset//concept[@id = $conceptId]
                    }
            ) else ($datasets)
        
        return $datasets[1]
    ) else if ($transactionOrDataset) then (
        art:getFullDatasetTree($transactionOrDataset, $conceptId, $conceptEffectiveDate, $language, (), false(), ())
    ) else ()
    
let $community              := decor:getDecorCommunity($communityprefix, $projectId, true())

let $result                 := 
    if ($fullDatasetTree) then (
        let $xml        := ds:mergeDatasetTreeWithCommunity($fullDatasetTree,$language,$community)
        (:let $concepts   := local:textQuery($fullDatasetTree, $search, $language):)
        let $search     := fn:lower-case($search)
        let $concepts   := $xml//concept[not(ancestor::conceptList)][contains(fn:lower-case(name), $search) or contains(fn:lower-case(desc), $search) or contains((valueSet | valueDomain)/conceptList/concept/fn:lower-case(name), $search)]
        let $concepts   := $concepts[@statusCode=('new','draft','preview','final')]
        let $concepts   :=
            for $concept in $concepts
            let $weight := if (contains(fn:lower-case($concept/name), $search)) then 3 else if (contains(fn:lower-case($concept/desc), $search)) then 2 else 1
            order by $weight descending
            return 
                <concept>{
                    $concept/@*, 
                    attribute {'weight'} {$weight}, 
                    attribute {'ancestors'} {string-join($concept/(ancestor::*)[position()!=1]/@shortName, '/')}, 
                    if ($concept/@type = 'group') then element {'children'} {$concept/@shortName, for $child in $concept/concept return $child/name[1]} else (),
                    $concept/(* except concept)
                    }
                </concept>

        let $result :=
            element results {
                (: TODO: Full text search does not work with /releases - no Lucene index :)
                $concepts
                }
        return <result search="{$search}" count="{count($concepts)}">{artjson:fastNode2json($result)}</result>
    )
    else (
        if (request:exists()) then (
            response:set-status-code(404)
        ) else ()
    )
return $result