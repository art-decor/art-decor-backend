xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

(:
    Random String Generator

    This function returns a <random> element with a randomized string as element content
    in the format r-x where x is a UUID
:)

declare namespace util = "http://exist-db.org/xquery/util";

let $x := util:uuid()
return
    <random>r-{$x}</random>