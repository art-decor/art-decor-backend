xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace art         = "http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace i18n        = "http://art-decor.org/ns/decor/i18n" at "../../../art/api/api-decor-i18n.xqm";
import module namespace templ       = "http://art-decor.org/ns/decor/template" at "../../../art/api/api-decor-template.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../../art/api/api-server-settings.xqm";

declare namespace diff = "http://art-decor.org/ns/decor/diff";

declare copy-namespaces no-preserve, no-inherit;

declare option exist:serialize "method=xhtml indent=yes media-type=application/xhtml+html";

declare variable $artDeepLinkServices   := adserver:getServerURLServices();
declare variable $useLocalAssets        := if (request:exists()) then request:get-parameter('useLocalAssets','false') else 'false';
declare variable $download              := if (request:exists()) then request:get-parameter('download','false') else '';
(:When called with localAssets=true then we need relative local paths, else 
    we need our server services URL, but with matching scheme (http or https)
    If the scheme is https and servicesUrl is http this leads to a security 
    problem in some browsers
:)
declare variable $resourcePath          := if ($download='true') then ('https://assets.art-decor.org/ADAR/rv/assets') else if ($useLocalAssets = 'true') then ('../assets') else ('../core/assets');
declare variable $artServerUrl          := adserver:getServerURLArt();

(: optional parameter :)
declare variable $language              := if (request:exists() and string-length(string-join(request:get-parameter('language',''),''))>0) then request:get-parameter('language',$get:strArtLanguage)[1] else ($get:strArtLanguage);
declare variable $docMessages           := i18n:getMessagesDoc('decor/services');

declare variable $g_supportedTypes      := ('xxxx','releases');

(: temp settings :)
declare variable $comparewith           := doc(concat($get:strDecorHistory, '/pmp/decor-20150303142424.xml'));
declare variable $testdiff :=
<template displayName="Patientenbezogener Medikationsplan CDA document" effectiveDate="2014-10-20T00:00:00" id="1.2.276.0.76.10.1014" name="PersonalMedicationList" statusCode="draft" isClosed="false">
    <diff:insert attribute="isClosed"/>
    <desc language="de-DE">
        <diff:normalizedtext xmlns:diff="http://art-decor.org/ns/decor/diff">Patientenbezogener Medikationsplan als CDA Dokument</diff:normalizedtext>
    </desc>
    <classification type="cdadocumentlevel">
        <diff:delete tag="classification"/>
    </classification>
    <relationship flexibility="2005-09-07T00:00:00" template="2.16.840.1.113883.10.12.1" type="SPEC"/>
    <context path="/"/>
    <item label="docpmp"/>
    <element id="2.16.840.1.113883.2.60.4.9.15" minimumMultiplicity="1" name="hl7:ClinicalDocument" maximumMultiplicity="*">
        <diff:delete attribute="minimumMultiplicity"/>
        <diff:insert attribute="maximumMultiplicity"/>
        <include ref="1.2.276.0.76.10.90002" conformance="R" isMandatory="true" maximumMultiplicity="1" minimumMultiplicity="1">
            <diff:insert attribute="conformance"/>
            <diff:insert attribute="isMandatory"/>
            <diff:insert attribute="maximumMultiplicity"/>
            <diff:insert attribute="minimumMultiplicity"/>
        </include>
        <element datatype="II" id="2.16.840.1.113883.2.60.4.9.16" isMandatory="true" maximumMultiplicity="1" minimumMultiplicity="1" name="hl7:templateId">
            <example>
                <templateId root="1.2.276.0.76.10.1014"/>
            </example>
            <attribute isOptional="false" name="root" value="1.2.276.0.76.10.1014"/>
        </element>
        <include ref="1.2.276.0.76.10.90004" maximumMultiplicity="*" minimumMultiplicity="0">
            <diff:insert attribute="maximumMultiplicity"/>
            <diff:insert attribute="minimumMultiplicity"/>
        </include>
        <element datatype="CE" id="2.16.840.1.113883.2.60.4.9.17" isMandatory="true" maximumMultiplicity="1" minimumMultiplicity="1" name="hl7:code">
            <vocabulary code="X_PMR" codeSystem="2.16.840.1.113883.6.1">
                <diff:update attribute="code" from="???"/>
            </vocabulary>
        </element>
        <include maximumMultiplicity="1" minimumMultiplicity="0" ref="1.2.276.0.76.10.90005">
            <example>
                <title>
                    <diff:normalizedtext xmlns:diff="http://art-decor.org/ns/decor/diff">Medikationsplan 2014-12-09 12:34:56<diff:update from="Medikationsplan 2014-12-08 12:34:56"/></diff:normalizedtext>
                </title>
            </example>
        </include>
        <include ref="1.2.276.0.76.10.90003" minimumMultiplicity="1" conformance="R" isMandatory="true" maximumMultiplicity="1">
            <diff:update attribute="ref" from="1.2.276.0.76.10.90006"/>
            <diff:insert attribute="conformance"/>
            <diff:insert attribute="isMandatory"/>
            <diff:insert attribute="maximumMultiplicity"/>
        </include>
        <include ref="1.2.276.0.76.10.90007" maximumMultiplicity="*" minimumMultiplicity="0">
            <diff:insert attribute="maximumMultiplicity"/>
            <diff:insert attribute="minimumMultiplicity"/>
        </include>
        <include ref="1.2.276.0.76.10.90009" maximumMultiplicity="*" minimumMultiplicity="0">
            <diff:insert attribute="maximumMultiplicity"/>
            <diff:insert attribute="minimumMultiplicity"/>
        </include>
        <include isMandatory="true" maximumMultiplicity="1" minimumMultiplicity="1" ref="1.2.276.0.76.10.2028"/>
        <include isMandatory="true" maximumMultiplicity="1" minimumMultiplicity="1" ref="1.2.276.0.76.10.2029"/>
        <include conformance="R" maximumMultiplicity="1" minimumMultiplicity="0" ref="1.2.276.0.76.10.2031"/>
        <include isMandatory="true" maximumMultiplicity="1" minimumMultiplicity="1" ref="1.2.276.0.76.10.2030"/>
        <include maximumMultiplicity="*" minimumMultiplicity="0" ref="1.2.276.0.76.10.2011">
            <desc language="de-DE">
                <diff:normalizedtext xmlns:diff="http://art-decor.org/ns/decor/diff">Notfall-Kontakt / Auskunftsberechtigte Person</diff:normalizedtext>
            </desc>
        </include>
        <element id="2.16.840.1.113883.2.60.4.9.18" name="hl7:component" conformance="R" isMandatory="true" maximumMultiplicity="1" minimumMultiplicity="1">
            <diff:insert attribute="conformance"/>
            <diff:insert attribute="isMandatory"/>
            <diff:insert attribute="maximumMultiplicity"/>
            <diff:insert attribute="minimumMultiplicity"/>
            <attribute isOptional="true" name="typeCode" value="COMP"/>
            <attribute isOptional="true" name="contextConductionInd" value="true"/>
            <element id="2.16.840.1.113883.2.60.4.9.19" name="hl7:structuredBody" conformance="R" isMandatory="true" maximumMultiplicity="1" minimumMultiplicity="1">
                <diff:insert attribute="conformance"/>
                <diff:insert attribute="isMandatory"/>
                <diff:insert attribute="maximumMultiplicity"/>
                <diff:insert attribute="minimumMultiplicity"/>
                <attribute isOptional="true" name="classCode" value="DOCBODY"/>
                <attribute isOptional="true" name="moodCode" value="EVN"/>
                <element conformance="R" contains="1.2.276.0.76.10.3039" id="2.16.840.1.113883.2.60.4.9.20" maximumMultiplicity="1" minimumMultiplicity="0" name="hl7:component">
                    <desc language="de-DE">
                        <diff:normalizedtext xmlns:diff="http://art-decor.org/ns/decor/diff">Section: Clinical Information</diff:normalizedtext>
                    </desc>
                    <attribute isOptional="false" name="typeCode" value="COMP"/>
                    <attribute isOptional="false" name="contextConductionInd" value="true"/>
                </element>
            </element>
        </element>
        <include maximumMultiplicity="*" minimumMultiplicity="0" ref="1.2.276.0.76.10.90006">
            <diff:insert tag="include"/>
        </include>
    </element>
</template>
;

declare %private function local:c14n ($element as element()?) as element()? {
    
    let $xelm := if ($element/self::attribute) then templ:normalizeAttributes($element) else $element
    
    return
        element { local-name($xelm) } {
            for $att in $xelm/@*
            let $sortName := local-name($att)
            order by $sortName
            return
                attribute {local-name($att)} {$att},
            for $child in $xelm/(* | text())
            return
                if ($child instance of element())
                then local:c14n($child)
                else if ($child instance of text())
                then <diff:normalizedtext>{normalize-space($child)}</diff:normalizedtext>
                else $child
         }

};

declare %private function local:displaytree ($node as element()?) as element()? {
    let $x := 1
    
    return
        <ul>
        {
            for $e in $node[not(self::diff:*)]
            return (
                <li>
                {
                    (: handle diff tag :)
                    if ($e/diff:insert[@tag=name($e)]) then (
                        attribute {'class'} {'inserted'},
                        name($e)
                    ) else 
                    if ($e/diff:delete[@tag=name($e)]) then (
                        attribute {'class'} {'deleted'},
                        name($e)
                    ) else
                    if ($e/diff:update[@tag=name($e)]) then (
                        <div class="deleted" style="display:inline;">
                        {
                            name($e)
                        }
                        </div>,
                            ' ',
                        <div class="inserted" style="display:inline;">
                        {
                            name($e)
                        }
                        </div>
                    ) else name($e)                 
                    ,
                    for $a in $e/@*
                    return
                        <ul>
                        {
                            (: handle diff attribute :)
                            if ($e/diff:insert[@attribute=name($a)]) then (
                                attribute {'class'} {'inserted'},
                                concat('@', name($a), '=', $a/string())
                            ) else 
                            if ($e/diff:delete[@attribute=name($a)]) then (
                                attribute {'class'} {'deleted'},
                                concat('@', name($a), '=', $a/string())
                            ) else
                            if ($e/diff:update[@attribute=name($a)]) then (
                                <div class="deleted" style="display:inline;">
                                {
                                    concat('@', name($a), '=', $e/diff:update/@from)
                                }
                                </div>,
                                ' ',
                                <div class="inserted" style="display:inline;">
                                {
                                    concat('@', name($a), '=', $a/string())
                                }
                                </div>
                            ) else concat('@', name($a), '=', $a/string())
                        }
                        </ul>,
                    for $e2 in $e/*
                    return
                        if ($e2/self::diff:normalizedtext)
                        then (
                            <ul>
                            {
                            (: handle diff attribute :)
                            if ($e2/diff:insert) then (
                                attribute {'class'} {'inserted normalizedtext'},
                                $e2/text()
                            ) else 
                            if ($e2/diff:delete) then (
                                attribute {'class'} {'deleted normalizedtext'},
                                $e2/text()
                            ) else
                            if ($e2/diff:update[@from]) then (
                                ' ',
                                <div class="deleted normalizedtext" style="display:inline;">
                                {
                                     concat($e2/diff:update/@from, '')
                                }
                                </div>,
                                ' ',
                                <div class="inserted normalizedtext" style="display:inline;">
                                {
                                    $e2/text()
                                }
                                </div>
                            ) else 
                                <div class="normalizedtext" style="display:inline;">
                                {
                                    $e2/text()
                                }
                                </div>
                            }
                            </ul>
                        )
                        else local:displaytree ($e2)
                }
                </li>
            )
        }
        </ul>
};

declare %private function local:com ($p1 as element(), $p2 as element(), $language as xs:string) as element()? {

let $resultTR := 
    for $t1 in $p1//transaction[@id][not(@type='group')]
    let $id := $t1/@id
    let $effectiveDate := $t1/@effectiveDate
    let $t2 := $p2//transaction[@id=$id][@effectiveDate=$effectiveDate]
    return
        <transaction>
        {
            $t1/@*,
            attribute {'displayName'} {if (string-length(($t1/name[$language])[1])>0) then ($t1/name[$language])[1] else $t1/name[1]},
            if (empty($t2))
            then (
                <notinold id="{$id}" effectiveDate="{$effectiveDate}">
                    <new>{$t1}</new>
                </notinold>
            ) else if (deep-equal($t1,$t2))
            then (
                <equaloldnew/>
            ) else if (deep-equal($t1/(@* except @statusCode|*),$t2/(@* except @statusCode|*)))
            then (
                <equaloldnewdesign id="{$id}" effectiveDate="{$effectiveDate}" statusCode="{$t1/@statusCode}"/>
            ) else (
                <diffoldnew id="{$id}" effectiveDate="{$effectiveDate}">
                    <new>{$t1}</new>,
                    <old>{$t2}</old>
                </diffoldnew>
            )
        }
        </transaction>

let $resultTM := 
    for $t1 in $p1//template[@id]
    let $id := $t1/@id
    let $effectiveDate := $t1/@effectiveDate
    let $t2 := $p2//template[@id=$id][@effectiveDate=$effectiveDate]
    return
        <template>
        {
            $t1/@*,
            if (empty($t2))
            then (
                <notinold id="{$id}" effectiveDate="{$effectiveDate}">
                    <new>{$t1}</new>
                </notinold>
            ) else if (deep-equal($t1,$t2))
            then (
                <equaloldnew/>
            ) else if (deep-equal($t1/(@* except @statusCode|*),$t2/(@* except @statusCode|*)))
            then (
                <equaloldnewdesign id="{$id}" effectiveDate="{$effectiveDate}" statusCode="{$t1/@statusCode}"/>
            ) else (
                <diffoldnew id="{$id}" effectiveDate="{$effectiveDate}">
                    <new>{$t1}</new>,
                    <old>{$t2}</old>
                </diffoldnew>
            )
        }
        </template>

let $resultVS := 
    for $t1 in $p1//valueSet[@id]
    let $id := $t1/@id
    let $effectiveDate := $t1/@effectiveDate
    let $t2 := $p2//valueSet[@id=$id][@effectiveDate=$effectiveDate]
    return
        <valueSet>
        {
            $t1/@*,
            if (empty($t2))
            then (
                <notinold id="{$id}" effectiveDate="{$effectiveDate}">
                    <new>{$t1}</new>
                </notinold>
            ) else if (deep-equal($t1,$t2))
            then (
                <equaloldnew/>
            ) else if (deep-equal($t1/(@* except @statusCode|*),$t2/(@* except @statusCode|*)))
            then (
                <equaloldnewdesign id="{$id}" effectiveDate="{$effectiveDate}" statusCode="{$t1/@statusCode}"/>
            ) else (
                <diffoldnew id="{$id}" effectiveDate="{$effectiveDate}">
                    <new>{$t1}</new>,
                    <old>{$t2}</old>
                </diffoldnew>
            )
        }
        </valueSet>


return
    <comparison>
    {
        for $z in $resultTR | $resultTM | $resultVS
        let $zname := name($z)
        let $type := 
            if ($zname='template') then i18n:getMessage($docMessages,'Template',$language) else
            if ($zname='valueSet') then i18n:getMessage($docMessages,'ValueSet',$language) else
            if ($zname='transaction') then i18n:getMessage($docMessages,'Transaction',$language) else '?'
        return
            if ($z/equaloldnew) then () else (
                <artefact type="{$type}" id="{$z/@id}" displayName="{$z/@displayName}" effectiveDate="{replace($z/@effectiveDate, 'T00:00:00', '')}">
                {
                    attribute {'changes'} {
                        if (name($z/*)='diffoldnew') 
                        then i18n:getMessage($docMessages,'Changed',$language)
                        else  if (name($z/*)='equaloldnewdesign') 
                        then concat(' ', i18n:getMessage($docMessages,'Statuschanged',$language), ' ', string($z/@statusCode))
                        else i18n:getMessage($docMessages,'New',$language)
                    }
                }
                </artefact>
            )
     }
     </comparison>
};

(: required parameters :)
let $artefacttype        := if (request:exists()) then request:get-parameter('type',())[.=$g_supportedTypes] else ()
let $projectPrefix       := if (request:exists()) then request:get-parameter('prefix','') else ''
let $format              := if (request:exists()) then request:get-parameter('format','html') else 'html'

(: older and newer release version signature as parameters :)
let $older             := if (request:exists()) then request:get-parameter('older','') else ''
let $newer             := if (request:exists()) then request:get-parameter('newer','') else ''
let $oldersignature    := replace ($older, '[-:]', '')
let $newersignature    := replace ($newer, '[-:]', '')

(: get stuff :)
let $project     := collection($get:strDecorData)//decor[project/@prefix=$projectPrefix]
let $templates   := if ($project) then $project//template[@id='1.2.276.0.76.10.1014'] else ()

let $targetStr   := if ($project) then concat($get:strDecorVersion, '/', substring($projectPrefix, 1, string-length($projectPrefix) - 1)) else ()
let $targetDir   := if ($project) then collection($targetStr) else ()

let $result      := 
    if (($artefacttype='releases' and empty($project)) or 
        ($artefacttype='xxxx' and empty($templates)) or
        string-length($artefacttype)=0) then (
        
        let $queryString    := 
            concat( 'prefix ', if (string-length($projectPrefix)=0) then ' NULL' else $projectPrefix, ' /  ',
                    'type '  , if (string-length($artefacttype)=0)  then ' NULL' else $artefacttype)
        let $errorString    :=
            if ($artefacttype='xxxx') then
                concat(i18n:getMessage($docMessages,'errorRetrieveTemplateNoResults',$language,$queryString),' ', $projectPrefix)
            else if ($artefacttype='xxxx') then
                i18n:getMessage($docMessages,'errorRetrieveProjectNoResults',$language,$queryString)
            else (
                i18n:getMessage($docMessages,'errorNoResults',$language,$queryString)
            )
        
        return
        (: this is in error :)
        if ($format="xml") then (
            response:set-status-code(404),
            response:set-header('Content-Type','text/xml'),
            <error>{$errorString}</error>
        ) else (
            response:set-header('Content-Type','text/html'),
            <html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                    <title>{i18n:getMessage($docMessages,'Artefactdiff',$language)}</title>
                    <link href="{$resourcePath}/css/default.css" rel="stylesheet" type="text/css"/>
                    <link href="{$resourcePath}/css/diff.css" rel="stylesheet" type="text/css"/>
                </head>
                <body>
                    <h1>{i18n:getMessage($docMessages,'Artefactdiff',$language)}</h1>
                    <div class="content">
                        <span style="color: red;">{$errorString}</span>
                        <p>{i18n:getMessage($docMessages,'Usage',$language)}: <tt>.../CompareArtefacts?prefix=</tt><i>project-prefix</i>&amp;<tt>type=</tt><i>artefact-type</i></p>
                        <p style="padding-left: 20px;">
                        {
                            i18n:getMessage($docMessages,'whereXisY',$language,'project-prefix','project prefix')
                        }
                        </p>
                        <p style="padding-left: 20px;">
                        {
                            i18n:getMessage($docMessages,'and',$language),' ',
                            i18n:getMessage($docMessages,'whereXisoneofY',$language,'artefact-type',string-join($g_supportedTypes,' '))
                        }
                        </p>
                   </div>
               </body>
           </html>
        )
    )
    else if ($artefacttype = 'releases') then (
        (: 
            pre-set options for xml or html output later
        :)
        let $neweroptions :=
            <newer label="{i18n:getMessage($docMessages,'SelectNewerRelease',$language)}">
            {
                <option value="9999" selected="true" label="{i18n:getMessage($docMessages,'ActualRelease',$language)}"/>,
                for $r in $targetDir//decor[string-length(@versionDate)>0][string-length(@compilationDate)>0][not(@compilationDate='development')]
                let $versionSignature := replace($r/@versionDate, '[-:]', '')
                order by $r/@versionDate descending
                return
                    <option value="{$versionSignature}" 
                        label="{concat($r/@versionDate/string(), ' ', $r/project/release[1]/@by/string(), ' ', $r/project/release[1]/@versionLabel/string())}"/>
            }
            </newer>
        let $olderoptions :=
            <older label="{i18n:getMessage($docMessages,'SelectOlderRelease',$language)}">
            {
                for $r in $targetDir//decor[string-length(@versionDate)>0][string-length(@compilationDate)>0][not(@compilationDate='development')]
                let $maxd := max($targetDir//decor[not(@compilationDate='development')]/xs:dateTime(@versionDate))
                let $versionSignature := replace($r/@versionDate, '[-:]', '')
                order by $r/@versionDate descending
                return
                    <option value="{$versionSignature}" 
                        label="{concat($r/@versionDate/string(), ' ', $r/project/release[1]/@by/string(), ' ', $r/project/release[1]/@versionLabel/string())}">
                    {
                        if ($r/@versionDate=$maxd) then attribute {'selected'} {'true'} else ()
                    }
                    </option>
             }
             </older>
        let $newArtefact    :=
            if ($newersignature='9999')
            then collection($get:strDecorData)//decor[project/@prefix=$projectPrefix]
            else (
                try {
                    doc(concat($get:strDecorVersion, '/', substring($projectPrefix, 1, string-length($projectPrefix) - 1), 
                    '/version-', $newersignature, '/', $projectPrefix, $newersignature, '-decor.xml'))/decor
                } catch * {()}
            )
        let $oldArtefact    :=
            try {
                doc(concat($get:strDecorVersion, '/', substring($projectPrefix, 1, string-length($projectPrefix) - 1), 
                    '/version-', $oldersignature, '/', $projectPrefix, $oldersignature, '-decor.xml'))/decor
            } catch * {()}
        let $comparison :=
            if ($newArtefact and $oldArtefact) then
                local:com ($newArtefact, $oldArtefact, $language)
             else ()
        return
            if ($format="xml") then (
                response:set-status-code(200),
                response:set-header('Content-Type','text/xml'),
                <response>
                {
                    $neweroptions, $olderoptions
                }
                {
                    $comparison
                }
                </response>
            ) else (
                response:set-header('Content-Type','text/html'),
                <html>
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                        <title>{i18n:getMessage($docMessages,'Artefactdiff',$language)}</title>
                        <link href="{$resourcePath}/css/default.css" rel="stylesheet" type="text/css"/>
                        <link href="{$resourcePath}/css/diff.css" rel="stylesheet" type="text/css"/>
                    </head>
                    <body>
                        <h1>{i18n:getMessage($docMessages,'Artefactdiff',$language)}</h1>
                        <form name="diffform" method="GET">
                            <input name="prefix" value="{$projectPrefix}" type="hidden"> </input>
                            <input name="type" value="{$artefacttype}" type="hidden"> </input>
                            <input name="language" value="{$language}" type="hidden"> </input>
                            <div>{$comparison}</div>
                            <table width="100%">
                                <tbody>
                                    {
                                        (: no releases set so get the releases :)
                                        if (string-length($older)=0 or string-length($newer)=0) then (
                                            <tr>
                                                <td align="left" colspan="2" width="1%">
                                                    <h2>
                                                    {
                                                        i18n:getMessage($docMessages,'ReleaseDiffFor',$language,$projectPrefix)
                                                    }
                                                    </h2>
                                                </td>
                                            </tr>
                                            ,
                                            <tr>
                                                <td align="left" width="10%">{$neweroptions/@label/string()}</td>
                                                <td align="left">
                                                    <select name="newer">
                                                    {
                                                        for $o in $neweroptions/option
                                                        return
                                                            <option value="{$o/@value/string()}">
                                                            {
                                                                $o/@label/string()
                                                            }
                                                            </option>
                                                    }
                                                    </select>
                                                </td>
                                            </tr>
                                            ,
                                            <tr>
                                               <td align="left" width="10%">{$olderoptions/@label/string()}</td>
                                               <td align="left">
                                                   <select name="older">
                                                   {
                                                        for $o in $olderoptions/option
                                                        return
                                                            <option value="{$o/@value/string()}">
                                                            {
                                                                $o/@label/string()
                                                            }
                                                            </option>
                                                   }
                                                   </select>
                                               </td>
                                             </tr>
                                            ,
                                            <tr>
                                                <td colspan="2">
                                                <input type="submit" value="{i18n:getMessage($docMessages,'Compare',$language)}"> </input>
                                                </td>
                                            </tr>
                                        ) else (
                                            <div class="content">
                                            {
                                                <h2>
                                                {
                                                    i18n:getMessage($docMessages,'ReleaseDiffForOldNew',$language,$projectPrefix,$newersignature,$oldersignature)
                                                }
                                                </h2>,
                                                i18n:getMessage($docMessages,'NewerRelease',$language), 
                                                if ($newersignature='9999') then i18n:getMessage($docMessages,'ActualRelease',$language) else $newersignature, 
                                                <br/>,
                                                i18n:getMessage($docMessages,'OlderRelease',$language), $oldersignature,
                                                <p/>,
                                                <div style="border : 1px solid #C3C0B2; padding: 10px 10px 10px 10px; background-color: #eeeeee;">
                                                {
                                                    for $c in $comparison/artefact
                                                    return
                                                    <h3>
                                                    {
                                                        string($c/@type), ' ',
                                                        string($c/@id),
                                                        ' ',
                                                        <i>
                                                        {
                                                        string($c/@displayName/string())
                                                        }
                                                        </i>,
                                                        concat(' (', string($c/@effectiveDate/string()), ') '),
                                                        string($c/@changes/string())
                                                    }
                                                    </h3>
                                                }
                                                </div>
                                            }
                                            </div>,
                                            <p/>,
                                            <input type="submit" value="{i18n:getMessage($docMessages,'Retry',$language)}"> </input>
                                       )
                                    }
                                </tbody>
                            </table>
                        </form>
                    </body>
                </html>
            )
    )
    else if ($artefacttype = 'xxxx') then (
        response:set-header('Content-Type','text/html'),
        if (empty($templates)) then
            (response:set-status-code(404), <error>{i18n:getMessage($docMessages,'errorRetrieveTemplateNoResults',$language),' ', $projectPrefix}</error>)
        else (
            <html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                    <title>Templates Diff</title>
                    <link href="{$resourcePath}/css/default.css" rel="stylesheet" type="text/css"/>
                    <link href="{$resourcePath}/css/diff.css" rel="stylesheet" type="text/css"/>
                </head>
                <body>
                    <h1>Test</h1>
                    <div class="content" style="width:700px;">
                    {
                        local:displaytree ( $testdiff )
                    }
                    </div>
                    <h1>Templates</h1>
                    <div class="content">
                    <table border="1">
                    {
                        for $t in $templates
                        let $sortName   := lower-case($t/@displayName)
                        let $c14norig   := local:c14n($t)
                        let $thist      := $comparewith//template[@id=$t/@id and @effectiveDate=$t/@effectiveDate]
                        let $c14nhist   := if (empty($thist)) then <template/> else local:c14n($thist)
                        order by $sortName
                        return (
                            <tr>
                            {
                                <td width="500px" valign="top">
                                {
                                    <h2>{$t/@displayName/string()} orig</h2>,
                                    local:displaytree ( $c14norig )
                                }
                                </td>,
                                <td width="500px" valign="top">
                                {
                                    <h2>{$t/@displayName/string()} hist</h2>,
                                    local:displaytree ( $c14nhist )
                                }
                                </td>
                            }
                            </tr>
                            (:
                            ,
                            <tr>
                            {
                                <td width="1000px" valign="top">
                                {
                                    <pre>{fn:serialize($c14norig,())}</pre>,
                                    <pre>{fn:serialize($c14nhist,())}</pre>
                                }
                                </td>
                            }
                            </tr>
                            :)
                        )
                    }
                    </table>
                    </div>
                </body>
            </html>
        )
    ) else ()
    
return
    $result