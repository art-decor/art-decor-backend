xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../../art/api/api-server-settings.xqm";
import module namespace i18n        = "http://art-decor.org/ns/decor/i18n" at "../../../art/api/api-decor-i18n.xqm";

declare namespace hl7   = "urn:hl7-org:v3";
declare namespace xs    = "http://www.w3.org/2001/XMLSchema";

declare variable $artDeepLinkServices   := adserver:getServerURLServices();
declare variable $useLocalAssets        := if (request:exists()) then request:get-parameter('useLocalAssets','false') else 'false';
declare variable $download              := if (request:exists()) then request:get-parameter('download','false') else '';
(:When called with localAssets=true then we need relative local paths, else 
    we need our server services URL, but with matching scheme (http or https)
    If the scheme is https and servicesUrl is http this leads to a security 
    problem in some browsers
:)
declare variable $resourcePath          := if ($download='true') then ('https://assets.art-decor.org/ADAR/rv/assets') else if ($useLocalAssets = 'true') then ('../assets') else ('../core/assets');

declare variable $docMessages           := i18n:getMessagesDoc('decor/services');

let $xslFile := xs:anyURI(concat('xmldb:exist://',$get:root,'decor/services/resources/stylesheets/decor-transaction-group-2-svg.xsl'))

(: let $projectId := request:get-parameter('id','') :)
(: let $projectName := request:get-parameter('name','') :)
let $projectPrefix          := 
    if (request:exists()) then 
        if (request:get-parameter-names()[. = 'project']) then 
            request:get-parameter('project', ())
        else (
            request:get-parameter('prefix', ())
        )
    else ()
let $language      := request:get-parameter('language',$get:strArtLanguage)
let $xsltOptions   := 
    <parameters>
        <param name="doFunctionalView" value="true"/>
        <param name="doTechnicalView" value="false"/>
        <param name="language" value="{$language}"/>
        <param name="transactionGroupId" value="{request:get-parameter('id','')}"/>
        <param name="transactionGroupEffectiveDate" value="{request:get-parameter('effectiveDate','')}"/>
        <param name="inline" value="{request:get-parameter('inline','false')}"/>
        <param name="textFunctionalPerspective" value="{i18n:getMessage($docMessages,'textFunctionalPerspective',$language)}"/>
        <param name="textTechnicalPerspective" value="{i18n:getMessage($docMessages,'textTechnicalPerspective',$language)}"/>
    </parameters>

let $html := 
    if (string-length($projectPrefix)>0) then
        let $decorProject   := $get:colDecorData//decor[project/@prefix=$projectPrefix]
        return
        transform:transform($decorProject, $xslFile, $xsltOptions)
    else (
        <html>
            <head>
                <title>{i18n:getMessage($docMessages,'titleError',$language)}</title>
                <link href="{$resourcePath}/css/default.css" rel="stylesheet" type="text/css"/>
            </head>
            <body>
                <h1>{i18n:getMessage($docMessages,'errorRetrieveTransactionGroupDiagramNotEnoughParameters',$language)}</h1>
            </body>
        </html>
    )
return
    if ($html[local-name()='html']) then (
        response:set-header('Content-Type','text/html'),
        $html
    ) else (
        response:set-header('Content-Type','image/svg+xml'),
        if (empty($html)) then (<svg xmlns="http://www.w3.org/2000/svg" version="1.1" height="16px" width="16px" style="fill:#ffffff;fill-opacity:1;stroke:#000000;stroke-width:0"/>) else ($html)
    )