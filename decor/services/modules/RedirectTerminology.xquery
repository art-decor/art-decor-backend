xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../../art/api/api-server-settings.xqm";
import module namespace i18n        = "http://art-decor.org/ns/decor/i18n" at "../../../art/api/api-decor-i18n.xqm";

declare variable $useLocalAssets        := if (request:exists()) then request:get-parameter('useLocalAssets','false') else 'false';
declare variable $download              := if (request:exists()) then request:get-parameter('download','false') else '';
(:When called with localAssets=true then we need relative local paths, else 
    we need our server services URL, but with matching scheme (http or https)
    If the scheme is https and servicesUrl is http this leads to a security 
    problem in some browsers
:)
declare variable $resourcePath          := if ($download='true') then ('https://assets.art-decor.org/ADAR/rv/assets') else if ($useLocalAssets = 'true') then ('../assets') else ('../core/assets');

declare variable $artDeepLink           := adserver:getServerURLArt();
declare variable $decorServicesDeepLink := adserver:getServerURLServices();
declare variable $artDeepLinkTerminology:= if (contains($artDeepLink,'localhost')) then 'http://localhost:8877/terminology/' else concat($artDeepLink,'../terminology/');

declare variable $codeSystemSNOMED      := '2.16.840.1.113883.6.96';
declare variable $codeSystemLOINC       := '2.16.840.1.113883.6.1';
declare variable $codeSystemsCLAML      := collection($get:strTerminologyData)//ClaML/Identifier/@uid;

let $projectPrefix                      := if (request:exists()) then request:get-parameter('prefix',()) else ()
let $projectVersion                     := if (request:exists()) then request:get-parameter('version',()) else ()
let $language                           := if (request:exists()) then request:get-parameter('language',()) else ()
let $codeSystem                         := if (request:exists()) then request:get-parameter('codeSystem',()) else ()
let $conceptId                          := if (request:exists()) then request:get-parameter('conceptId',()) else ()

let $deeplinktocode := 
    if ($codeSystem=$codeSystemSNOMED) then
        (: Don't deeplink for SNOMED CT post-coordinated codes :)
        if (string(number($conceptId)) != 'NaN') then concat($artDeepLink,'snomed-ct?conceptId=',encode-for-uri($conceptId)) else ()
    else if ($codeSystem=$codeSystemLOINC) then
        concat($artDeepLink,'loinc?conceptId=',encode-for-uri($conceptId))
    else if ($codeSystem=$codeSystemsCLAML) then
        concat($artDeepLink,'claml?classificationId=',encode-for-uri($codeSystem),'&amp;conceptId=',encode-for-uri($conceptId))
    else if (empty($projectPrefix)) then () else (
        concat($decorServicesDeepLink,'RetrieveCode?format=html&amp;prefix=',$projectPrefix,'&amp;version=',$projectVersion,'&amp;codeSystem=',encode-for-uri($codeSystem),'&amp;code=',encode-for-uri($conceptId),'&amp;language=',$language)
    )

return
    if (empty($deeplinktocode)) then (
        response:set-status-code(404),response:set-header('Content-Type','text/html'),
        <html>
            <head>
                <title>HTTP 404 Not Found</title>
                <link href="{$resourcePath}/css/default.css" rel="stylesheet" type="text/css"></link>
            </head>
            <body>
                <h1>HTTP 404 Not Found</h1>
                <p>Could not find Terminology Browser based on system "{$codeSystem}" {if (empty($conceptId)) then () else concat(' and conceptId "',$conceptId,'"')}</p>
            </body>
        </html>
    )
    else 
        response:redirect-to(xs:anyURI($deeplinktocode))