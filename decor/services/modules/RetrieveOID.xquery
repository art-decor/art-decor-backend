xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../../art/api/api-server-settings.xqm";
import module namespace i18n        = "http://art-decor.org/ns/decor/i18n" at "../../../art/api/api-decor-i18n.xqm";

declare namespace f                 = "http://hl7.org/fhir";
declare namespace xforms            = "http://www.w3.org/2002/xforms";
declare namespace xsd               = "http://www.w3.org/2001/XMLSchema";
declare namespace sch               = "http://purl.oclc.org/dsdl/schematron";

declare variable $artDeepLinkServices   := adserver:getServerURLServices();
declare variable $useLocalAssets        := if (request:exists()) then request:get-parameter('useLocalAssets','false') else 'false';
declare variable $download              := if (request:exists()) then request:get-parameter('download','false') else '';
(:When called with localAssets=true then we need relative local paths, else 
    we need our server services URL, but with matching scheme (http or https)
    If the scheme is https and servicesUrl is http this leads to a security 
    problem in some browsers
:)
declare variable $resourcePath          := if ($download='true') then ('https://assets.art-decor.org/ADAR/rv/assets') else if ($useLocalAssets = 'true') then ('../assets') else ('../core/assets');

declare variable $strMessages           := 'decor/services';
(:declare variable $docMessages           := i18n:getMessagesDoc('decor/services');:)
declare variable $oidTypes            := doc(concat($get:strOidsCore, '/', $get:strISO13582schema));

(: TODO: media-type beter zetten en XML declaration zetten bij XML output :)
(:declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=yes";:)

let $installedFhirVersions  := adserver:getInstalledFhirServices()
let $pathinfo               := if (request:exists()) then request:get-parameter('pathinfo',()) = ('true', 'on') else (false())

let $format                 := 
    if (request:exists()) then 
        if (request:get-parameter-names()[.='format']) then
            request:get-parameter('format','html')
        else if (request:get-header('Content-Type')[matches(.,'application/xml') or matches(.,'/\*')]) then (
            'fhir'
        )
        else if (request:get-parameter('_format','html')[.='application/xml+fhir']) then (
            'fhir'
        ) 
        else (
            'html'
        )
    else ('html')

let $searchRegistry         := 
    if (request:get-parameter('prefix',())[not(.='')]) then
        request:get-parameter('prefix',())[not(.='')][1]
    else (
        request:get-parameter('registry',())[not(.='')][1]
    )
let $searchId               := request:get-parameter('id',())[string-length()>0][1]
let $searchName             := tokenize(lower-case(request:get-parameter('name',())[string-length()>0][1]),'\s')
let $searchStatus           := request:get-parameter('statusCode',())[string-length()>0][1]
let $language               := request:get-parameter('language',$get:strArtLanguage)[1]

let $maxResults             := 50
let $queryName              := 
    <query><bool>{
        for $term in $searchName return <wildcard occur="must">{concat($term,'*')}</wildcard>
    }</bool></query>
let $allRegistries      :=
    for $registry in collection($get:strOidsData)/myoidregistry
    return <registry name="{$registry/@name}" displayName="{$registry/registry/name/@value}"/>

let $resultOnRegistry   :=
    if (empty($searchRegistry)) then
        collection($get:strOidsData)/myoidregistry//oid
    else (
        collection($get:strOidsData)/myoidregistry[@name=$searchRegistry]//oid
    )

let $resultsOnId        := 
    if (empty($searchId)) then 
        $resultOnRegistry
    else (
        $resultOnRegistry/dotNotation[@value=$searchId]/ancestor::oid[1]
    )
    
let $resultsOnName      := 
    if (empty($searchName)) then 
        $resultsOnId
    else (
        $resultsOnId[description/@value[ft:query(.,$queryName)] | symbolicName/@value[ft:query(.,$queryName)]]
    )
    
let $resultsOnStatus      := 
    if (empty($searchStatus)) then 
        $resultsOnName
    else (
        $resultsOnName[status/@code=$searchStatus]
    )

return 
    if ($format='fhir') then (
        response:set-status-code(500),
        response:set-header('Content-Type','application/xml+fhir; charset=utf-8'),
        <OperationOutcome xmlns="http://hl7.org/fhir">
            <text>
                <status value="generated"/>
                <div xmlns="http://www.w3.org/1999/xhtml">
                    <p>Not supported anymore. Please use [base]/NamingSystem/[id] or [base]/NamingSystem?name=[string] or [base]/NamingSystem?status=[code]</p>
                </div>
            </text>
            <issue xmlns="http://hl7.org/fhir">
                <severity value="error"/>
                <code value="processing"/>
                <details>
                    <text value="Not supported anymore. Please use [base]/NamingSystem/[id] or [base]/NamingSystem?name=[string] or [base]/NamingSystem?status=[code]"/>
                </details>
            </issue>
        </OperationOutcome>
    )
    else
    if (empty($searchId) and empty($searchName)) then (
        response:set-status-code(404), 
        response:set-header('Content-Type','text/xml; charset=utf-8'), <error>{i18n:getMessage($strMessages,'errorRetrieveOIDNotEnoughParameters',$language),' ',if (request:exists()) then request:get-query-string() else()}</error>
    )
    else 
    (:if (empty($resultsOnStatus)) then (
        response:set-status-code(404),
        if ($format='fhir') then (
            response:set-header('Content-Type','application/xml+fhir; charset=utf-8'),
            response:set-header('Content-Disposition', concat('filename=NamingSystem_(download_',substring(string(current-dateTime()),1,19),').xml'))
        ) else (
            response:set-header('Content-Type','application/xml; charset=utf-8'), <error>{i18n:getMessage($strMessages,'errorRetrieveOIDNoResults',$language),' ',if (request:exists()) then request:get-query-string() else()}</error>
        )
    ):)
    if ($format = 'xml') then (
        if (empty($resultsOnStatus)) then (response:set-status-code(404)) else (),
        response:set-header('Content-Type','application/xml; charset=utf-8'),
        <result>{$resultsOnStatus}</result>
    ) 
    else if ($format = 'csv') then (
        if (empty($resultsOnStatus)) then (response:set-status-code(404)) else (),
        response:set-header('Content-Type','text/xml; charset=utf-8'),
        <warning>{i18n:getMessage($strMessages,'errorNotImplementedYet',$language)}</warning>
    )
    else (
        let $doResponsibleValidTime := exists($resultsOnStatus/responsibleAuthority/validTime[.//@value])
        let $thead  :=
            <thead>
                <tr>
                    <th>{i18n:getMessage($strMessages,'columnRealm',$language)}</th>
                    <th>{i18n:getMessage($strMessages,'columnCategory',$language)}</th>
                    <th>{i18n:getMessage($strMessages,'columnDescription',$language)}</th>
                    <th>{i18n:getMessage($strMessages,'columnResponsibleAuthority',$language)}</th>
                    <th>{i18n:getMessage($strMessages,'columnResponsibleAuthorityStatus',$language)}</th>
                    {if ($doResponsibleValidTime) then (<th>{i18n:getMessage($strMessages,'columnResponsibleAuthorityEffectiveTime',$language)}</th>) else ()}
                    <th>{i18n:getMessage($strMessages,'columnExtraProperties',$language)}</th>
                    <th>{i18n:getMessage($strMessages,'columnReference',$language)}</th>
                    <th>{i18n:getMessage($strMessages,'columnRegistrationAuthority',$language)}</th>
                </tr>
            </thead>
        return (
        if (empty($resultsOnStatus)) then (response:set-status-code(404)) else (),
        response:set-header('Content-Type','text/html; charset=utf-8'),
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                <title>OID {$searchId}</title>
                <link href="{$resourcePath}/decor.css" rel="stylesheet" type="text/css"/>
            </head>
            <body>
                <div class="content">
                {
                    if (empty($resultsOnStatus)) then (
                        <h1>OID {($searchId, '-')[1]}</h1>,
                        <p>
                        {
                            i18n:getMessage($strMessages,'goTo',$language)
                        }
                            <a href="OIDIndex{if ($language != '') then (concat('?language=',$language)) else ()}" alt="">index</a>
                        {
                            if (empty($searchId)) then () else (' - ', <a href="http://oid-info.com/get/{$searchId}">oid-info.com: {$searchId}</a>)
                        }
                        </p>
                        ,
                        <p>{i18n:getMessage($strMessages,'errorRetrieveOIDNoResults',$language),' ',if (request:exists()) then request:get-query-string() else()}</p>
                        ,
                        if (empty($searchId)) then () else
                            <table class="zebra-table">
                                <thead>
                                    <tr>
                                        <th>{i18n:getMessage($strMessages,'columnOID',$language)}</th>
                                        <th>{i18n:getMessage($strMessages,'columnDescription',$language)}</th>
                                    </tr>
                                </thead>
                            {
                                let $nodes  := tokenize($searchId, '\.')
                                
                                for $node at $i in $nodes
                                let $oid        := string-join($nodes[position() le $i], '.')
                                let $oidrecord  := collection($get:strOidsData)//oid[dotNotation[@value = $oid]]
                                let $name       := (
                                        $oidrecord/description[@language = $language]/thumbnail/@value,
                                        $oidrecord/description/thumbnail/@value,
                                        $oidrecord/description[@language = $language]/@value,
                                        $oidrecord/description/@value
                                    )[1]
                                let $iddisplay  := if ($name) then () else art:getNameForOID($oid, $language, ())[not(. = '')]
                                return
                                    <tr style="background-color:white" onMouseover="this.style.backgroundColor='lightblue';" onMouseout="this.style.backgroundColor='white';">
                                        <td style="padding-right: 2em;">
                                            <a href="RetrieveOID?id={$oid}&amp;language={$language}" alt="">{$oid}</a>
                                        </td>
                                        <td>
                                        {
                                            if ($name) then data($name) else if (empty($iddisplay)) then '?' else <i>{data($iddisplay)} <img src="/art-decor/img/help.png" title="Name hint as found outside of hosted OID Registries, e.g. in a DECOR project" style="display: inline; margin-left: 1em; width: 16px; height: 16px;"/></i>
                                        }
                                        </td>
                                    </tr>
                            }
                            </table>
                    ) else ()
                }
                {
                    for $oid in $resultsOnStatus
                    let $name   := 
                        if ($oid/description[@language=$language]/thumbnail/@value) then
                            $oid/description[@language=$language]/thumbnail/@value
                        else (
                            $oid/description[1]/thumbnail/@value
                        )
                    return (
                        <h1>OID {$oid/dotNotation/@value/string()}{if ($name) then concat(' - ',$name[1]) else ('')}</h1>,
                        <p>
                        {
                            i18n:getMessage($strMessages,'goTo',$language)
                        }
                            <a href="OIDIndex{if ($language != '') then (concat('?language=',$language)) else ()}" alt="">index</a>
                        {
                            ' - ', i18n:getMessage($strMessages,'displayAs',$language)
                        }
                        {
                            let $uri    := 'RetrieveOID?format=xml&amp;'
                            let $uri    := concat($uri, string-join(
                                for $pn in request:get-parameter-names()[not(.='format')]
                                for $pv in request:get-parameter($pn,())[string-length()>0]
                                return concat($pn,'=',encode-for-uri($pv))
                            ,'&amp;'))
                            return
                            <a href="{$uri}" alt="">XML (ISO 13582)</a>
                        }
                        {
                            ' - ', i18n:getMessage($strMessages,'displayAs',$language), 'FHIR NamingSystem'
                        }
                        {
                            for $fhirVersion in $installedFhirVersions
                            order by $fhirVersion
                            return (
                                ' ',
                                <a href="/fhir/{$fhirVersion}/{$oid/ancestor::myoidregistry/@name}/NamingSystem/{concat($oid/dotNotation/@value,'--',$oid/ancestor::myoidregistry/@name)}" alt="">{$fhirVersion}</a>
                            )
                        }
                        </p>
                        ,
                        <table>
                        <tr>
                            <th style="padding: 0 1em; text-align: right;">{i18n:getMessage($strMessages,'columnID',$language)}</th><td>{$oid/dotNotation/@value/string()}</td>
                            <th style="padding: 0 1em; text-align: right;">{i18n:getMessage($strMessages,'registrationStatus',$language)}</th><td>{$oid/status/@code/string()}</td>
                            <th style="padding: 0 1em; text-align: right;">{i18n:getMessage($strMessages,'titleOIDRegistry',$language)}</th><td>{$oid/parent::registry/name/@value/string()}</td>
                        </tr>
                        </table>,
                        <p/>,
                        <table class="values" id="codeList">
                            {$thead}
                            <tbody>
                                <tr>
                                    <td>{$oid/realm/@code/string()}</td>
                                    <td>{$oid/category/@code/string()}</td>
                                    <td>
                                    {
                                        let $defaultDescription := $oid/description[@language = $language]
                                        return (
                                            if ($defaultDescription) then <p>{data($defaultDescription/@value)}</p> else ()
                                            ,
                                            for $description in $oid/description
                                            return (
                                                if ($description[@value = $defaultDescription/@value]) then () else (<p>{concat($description/@language,': ',$description/@value)}</p>)
                                            )
                                        )
                                    }</td>
                                    <td>{$oid/responsibleAuthority/code/@code/string()} - {$oid/responsibleAuthority/scopingOrganization/name/part/@value/string()}</td>
                                    <td>{$oid/responsibleAuthority/statusCode/@code/string()}</td>
                                    {if ($doResponsibleValidTime) then (
                                        <td>
                                        {
                                            if ($oid/responsibleAuthority/validTime/low/@value) then
                                                i18n:getMessage($strMessages,'effectiveTimeFrom',$language,$oid/responsibleAuthority/validTime/low/@value)
                                            else ()
                                        }
                                        {
                                            if ($oid/responsibleAuthority/validTime/high/@value) then
                                                i18n:getMessage($strMessages,'effectiveTimeTo',$language,$oid/responsibleAuthority/validTime/high/@value)
                                            else ()
                                        }</td>
                                    ) else ()}
                                    <td>{
                                        for $additionalProperty in $oid/additionalProperty 
                                        return
                                            <p>{concat($additionalProperty/attribute/@value,'=',$additionalProperty/value/@value)}</p>
                                    }</td>
                                    {
                                        if ($oid/reference)
                                        then
                                            <td>
                                            {
                                                for $reference in $oid/reference
                                                let $reftypecode    := $reference/type/@code
                                                let $reftype        := $oidTypes//xsd:simpleType[@name="list_ReferenceTypeCodes"]//xsd:enumeration[@value="RPLC"]//xforms:label
                                                let $reftype        := ($reftype[@xml:lang=$language], $reftype[@xml:lang='en-US'], $reftype, $reftypecode)[1]
                                                let $refvalue       := $reference/ref/@value
                                                order by $reftypecode
                                                return
                                                    <div>
                                                    {
                                                        if (starts-with($refvalue, 'http')) then
                                                            <a href="{$refvalue}">{data($reftype)}</a>
                                                        else
                                                        if (art:isOid($refvalue)) then (
                                                            $reftype, <a href="?id={$refvalue}&amp;language={$language}&amp;format={$format}">{data($refvalue)}</a>
                                                        )
                                                        else (
                                                            string-join(($reftype, $refvalue), ' ')
                                                        )
                                                        ,
                                                        if ($reference/lastVisitedDate) then (i18n:getMessage($strMessages,'lastVisit',$language), $reference/lastVisitedDate/@value/string()) else ()
                                                    }
                                                    </div>
                                            }
                                            </td>
                                        else
                                            <td></td>
                                     }
                                    <td>{$oid/registrationAuthority/code/@code/string()} - {$oid/registrationAuthority/scopingOrganization/name/part/@value/string()}</td>
                                </tr>
                            </tbody>
                        </table>
                        ,
                        (: path to oid info :)
                        if ($pathinfo) then 
                        <table class="zebra-table" style="border-top: 2px solid lightgrey; margin-top: 2em; width: 100%">
                            <thead>
                                <tr>
                                    <th>{i18n:getMessage($strMessages,'columnOID',$language)}</th>
                                    <th>{i18n:getMessage($strMessages,'columnDescription',$language)}</th>
                                </tr>
                            </thead>
                        {
                            let $nodes  := tokenize($searchId, '\.')
                            
                            for $node at $i in $nodes
                            let $oid        := string-join($nodes[position() le $i], '.')
                            let $oidrecord  := collection($get:strOidsData)//oid[dotNotation[@value = $oid]]
                            let $name       := (
                                    $oidrecord/description[@language = $language]/thumbnail/@value,
                                    $oidrecord/description/thumbnail/@value,
                                    $oidrecord/description[@language = $language]/@value,
                                    $oidrecord/description/@value
                                )[1]
                            let $iddisplay  := if ($name) then () else art:getNameForOID($oid, $language, ())[not(. = '')]
                            return
                                <tr style="background-color:white" onMouseover="this.style.backgroundColor='lightblue';" onMouseout="this.style.backgroundColor='white';">
                                    <td style="padding-right: 2em;">
                                        <a href="RetrieveOID?id={$oid}&amp;language={$language}" alt="">{$oid}</a>
                                    </td>
                                    <td>
                                    {
                                        if ($name) then data($name) else if (empty($iddisplay)) then '?' else <i>{data($iddisplay)} <img src="/art-decor/img/help.png" title="Name hint as found outside of hosted OID Registries, e.g. in a DECOR project" style="display: inline; margin-left: 1em; width: 16px; height: 16px;"/></i>
                                    }
                                    </td>
                                </tr>
                        }
                        </table>
                        else ()
                    )
                }
                    <p/>
                {
                    if (empty($resultsOnStatus)) then () else
                        <table width="100%">
                            <tr class="desclabel"><td>{i18n:getMessage($strMessages,'OIDCategoryLegendaLine',$language)}</td></tr>
                            <tr class="desclabel"><td>{i18n:getMessage($strMessages,'OIDRoleTypeLegendaLine',$language)}</td></tr>
                        </table>
                }
                </div>
            </body>
        </html>
        )
    )
 
 (:
        <oid>
        <dotNotation value="1.0.3166.1.2.2"/>
        <category code="LNS"/>
        <status code="completed"/>
        <realm code="UV"/>
        <description>
            <text language="en-US" value="ISO 3166 2 alpha Landcodes" identifierName="ISO 3166 Alpha 2"/>
            <text language="nl-NL" value="ISO 3166 2 alpha Landcodes" identifierName="ISO 3166 Alpha 2"/>
        </description>
        <registrationAuthority>
            <code code="OBO"/>
            <scopingOrganization>
                <name>
                    <part value="Nictiz"/>
                </name>
            </scopingOrganization>
        </registrationAuthority>
        <responsibleAuthority>
            <code code="PRI"/>
            <statusCode code="completed"/>
            <validTime>
                <low value="20111228"/>
            </validTime>
            <scopingOrganization>
                <name>
                    <part value="ISO"/>
                </name>
            </scopingOrganization>
        </responsibleAuthority>
        <additionalProperty>
            <attribute value="purpose"/>
            <value value="codesystem"/>
        </additionalProperty>
        <reference>
            <ref value="http://www.iso.org/iso/english_country_names_and_code_elements"/>
            <type code="LINK"/>
            <lastVisitedDate value="20111228"/>
        </reference>
    </oid>
        :)