(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace comp        = "http://art-decor.org/ns/art-decor-compile" at "../../../art/api/api-decor-compile.xqm";
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace gg          = "http://art-decor.org/ns/decor/governancegroups" at "../../../art/api/api-decor-governancegroups.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../../art/api/api-server-settings.xqm";
import module namespace templ       = "http://art-decor.org/ns/decor/template" at "../../../art/api/api-decor-template.xqm";


declare variable $strDecorServicesURL := adserver:getServerURLServices();

(: don't serialize desc nocdes :)
declare variable $boolSerialize := false();


declare option exist:serialize "method=xml media-type=application/xhtml+xml indent=no encoding=UTF-8";
declare option exist:output-size-limit "3000000";


let $language           := if (request:exists() and string-length(request:get-parameter('language',())[1])>0) then request:get-parameter('language',())[1] else ()

let $id                 := if (request:exists() and string-length(request:get-parameter('id',())[1])>0) then request:get-parameter('id',())[1] else ()

let $isGovernanceGroup  := if (request:exists() and string-length(request:get-parameter('isGovernanceGroup','false')[1])>0) then request:get-parameter('isGovernanceGroup','false')[1] else ()
let $withexperimentals  := if (request:exists() and string-length(request:get-parameter('withexperimentals','false')[1])>0) then request:get-parameter('withexperimentals','false')[1] else ()

let $hashesOnly          := if (request:exists() and string-length(request:get-parameter('hashesOnly','false')[1])>0) then request:get-parameter('hashesOnly','false')[1] else ()

let $g                  := if ($isGovernanceGroup) then gg:getGovernanceGroups($id, false(), $language, true()) else ()

let $now                := format-dateTime(current-dateTime(), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')
 
let $tmp                :=
    <result>
    {
        if ($isGovernanceGroup='true') then (
            for $p in $g/group/project
            let $decorproject := $get:colDecorData//decor[project[@id=$p/@id][empty(@experimental) or @experimental='false' or @experimental=$withexperimentals]]
            let $language     := if (count($decorproject)=1 and empty($language)) then $decorproject/project/@defaultLanguage/string() else ($language)
            return
                if (count($decorproject/*) lt 2)
                then <emptydecor id="{$p/@id}"/>
                else 
                    if ($hashesOnly='true')
                    then <hash ident="{$decorproject/project/@prefix}" id="{$p/@id}" hash="{util:hash(serialize($decorproject), "md5")}"/>
                    else <member ident="{$decorproject/project/@prefix}" id="{$p/@id}"/> (: comp:compileDecor($decorproject, $language, $now, <filters filter="off"/>, false()) :)
        ) else (
            let $decorproject := $get:colDecorData//decor[project[@id=$id][empty(@experimental) or @experimental='false' or @experimental=$withexperimentals]]
            let $language     := if (count($decorproject)=1 and empty($language)) then $decorproject/project/@defaultLanguage/string() else ($language)
            return
                if (count($decorproject/*) lt 2)
                then <emptydecor id="{$id}"/>
                else 
                     if ($hashesOnly='true')
                     then <hash ident="{$decorproject/project/@prefix}" id="{$id}" hash="{util:hash(serialize($decorproject), "md5")}"/>
                     else comp:compileDecor($decorproject, $language, $now, <filters filter="off"/>, false())
        )
    }
    </result>

return
    <result compilationDate="{$now}">
    {
         if ($hashesOnly='true')
         then $tmp/*
         else (
            (: add all identifiers belonging to this governance group :)
            if ($isGovernanceGroup='true') then (
            for $gi in $tmp/member
            return <identOfGovernanceGroup ident="{$gi/@ident}" pid="{$gi/@id}"/>
            ) else (),
            (: one single dummy decor element + project inside as a wrapper for everything found, just to satify the conversion routines :)
            if ($tmp/emptydecor)
               then $tmp/*
               else
            if ($isGovernanceGroup='true')
               then ()
               else 
               <decor prefix="#all" deeplinkprefixservices="{$strDecorServicesURL}">
               {
                  
                   <project defaultLanguage="{$language}" prefix="{$tmp//decor/project/@prefix}"/>,
                   $tmp//decor/datasets,
                   $tmp//decor/scenarios,
                   $tmp//decor/ids,
                   $tmp//decor/terminology,
                   <rules>
                   {
                       (: simplyfy rules by adding template with same id and effectiveDate only once :)
                       for $t in $tmp//decor/rules/template[@id]
                       let $id := $t/@id
                       let $effectiveDate := $t/@effectiveDate
                       group by $ie := concat ($t/@id, $t/@effectiveDate)
                       return
                           for $r in ($tmp//decor/rules/template[@id=$id][@effectiveDate=$effectiveDate])[1]
                           let $referencedFrom := $r/ancestor::decor/project/@prefix
                           let $drp := $t//ancestor::decor/project/@prefix
                           let $template := 
                               if (string-length($drp)>0 and count($id)=1) then templ:getExpandedTemplateById($id, $effectiveDate, $drp, (), (), $boolSerialize)/project/template else $r (: templ:getExpandedTemplateById($id, $effectiveDate, $boolSerialize)/project/template :)
                           return 
                               <template>
                               {
                                   $template/@*,
                                   attribute {'referencedFrom'} {$referencedFrom},
                                   $template/*
                               }
                               </template>
                   }
                   {
                       $tmp//decor/rules/(* except template)
                   }
                   </rules>
               }
           </decor>
         )
    }
    </result>