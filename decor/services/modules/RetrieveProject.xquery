xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace comp        = "http://art-decor.org/ns/art-decor-compile" at "../../../art/api/api-decor-compile.xqm";
import module namespace i18n        = "http://art-decor.org/ns/decor/i18n" at "../../../art/api/api-decor-i18n.xqm";
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../../art/api/api-server-settings.xqm";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "xhtml";
declare option output:encoding "UTF-8";

declare variable $artDeepLinkServices   := adserver:getServerURLServices();
declare variable $useLocalAssets        := if (request:exists()) then request:get-parameter('useLocalAssets','false') else 'false';
declare variable $download              := if (request:exists()) then request:get-parameter('download','false') else ('false');
(:When called with localAssets=true then we need relative local paths, else 
    we need our server services URL, but with matching scheme (http or https)
    If the scheme is https and servicesUrl is http this leads to a security 
    problem in some browsers
:)
declare variable $resourcePath          := if ($download='true') then ('//art-decor.org/ADAR/rv/assets') else if ($useLocalAssets = 'true') then ('../assets') else ('../core/assets');
declare variable $strMessages           := 'decor/services';

declare variable $MODEVERBATIM          := 'verbatim';
declare variable $MODECOMPILED          := 'compiled';
declare variable $MODERUNTIME           := 'runtime';
declare variable $MODETEST              := 'test';
declare variable $MODECACHEMETA         := 'cachemeta';
declare variable $MODECACHE             := 'cache';
declare variable $MODEXPATHS            := 'xpaths';
declare variable $MODEADADEF            := 'ada-definition';

declare %private function local:addCacheAttributes($decorprojects as element()?) as element()? {
    if ($decorprojects) then (
        <decor>
        {
            $decorprojects/(@* except (@deeplinkprefix | @deeplinkprefixservices))
            ,
            if (string-length(adserver:getServerURLArt()) gt 0) then 
                attribute deeplinkprefix {adserver:getServerURLArt()}
            else (),
            if (string-length(adserver:getServerURLServices()) gt 0) then 
                attribute deeplinkprefixservices {adserver:getServerURLServices()}
            else (),
            $decorprojects/node()
        }
        </decor>
    )
    else ()
};

(: get all projects for drop-down population :)
declare %private function local:getAllProjects($projectId as xs:string*, $projectPrefix as xs:string*, $proj-lang as xs:string?) as element(project)* {
    for $project in (collection($get:strDecorData)//decor[not(@private='true')]/project |
                     collection($get:strDecorData)//decor/project[@id=$projectId] |
                     collection($get:strDecorData)//decor/project[@prefix=$projectPrefix])
    return
        <project>
        {
            $project/@id,
            $project/@prefix,
            $project/@defaultLanguage,
            attribute repository {$project/parent::decor/@repository = 'true'},
            attribute private {$project/parent::decor/@private = 'true'},
            attribute experimental {$project/@experimental = 'true'},
            attribute name {if ($project/name[@language = $proj-lang]) then $project/name[@language = $proj-lang][1] else $project/name[1]},
            attribute creationdate {xmldb:created(util:collection-name($project),util:document-name($project))},
            attribute modifieddate {xmldb:last-modified(util:collection-name($project),util:document-name($project))}
        }
        </project>
        
};

(: mode determines whether or not you want the project to be returned as-is or with resolved references
   'verbatim' gets the project as-is
   'cache' gets the project as-is, but adds decor/@deeplinkprefix and decor/@deeplinkprefixservices for linkback
   'compiled' tries to make this project self-contained by getting referenced content from other projects/repositories into the current project
   'runtime' acts as compiled but more limited to scenarios, terminology and templates which is enough for creating a runtime enviroment
   'test' retrieves the filters that would be applied when compiled or runtime
:)
let $format             := if (request:exists()) then request:get-parameter('format','html')[1] else ('html')
let $mode               := if (request:exists()) then request:get-parameter('mode',$MODEVERBATIM)[1] else ($MODEVERBATIM)
let $proj-lang          := if (request:exists()) then request:get-parameter('language',())[string-length()>0][1] else ()
(:interface language:)
let $ui-lang            := if (request:exists()) then request:get-parameter('ui',$get:strArtLanguage)[string-length()>0][1] else ($get:strArtLanguage)
let $projectId          := if (request:exists()) then request:get-parameter('id',())[string-length()>0][1] else ()
let $projectPrefix      := if (request:exists()) then request:get-parameter('prefix',())[string-length()>0][1] else ()
(:version is only relevant when we have 1 project to handle. No support for more than 1 version thereof:)
let $projectVersion     := if (request:exists()) then request:get-parameter('version',())[string-length()>0][1] else ()
(: if ignoreFilter true then don't get the project's filters and process them as if filters = off :)
let $filterId           := if (request:exists()) then request:get-parameter('filterId', ())[string-length() gt 0][1] else ()
let $ignoreFilter       := if (request:exists()) then request:get-parameter('ignoreFilter', 'false') else 'true'
let $ignoreFilter       := $ignoreFilter = 'true' or $ignoreFilter = 'on'
(: if forceRecompile is true, the cached version will be bypassed and a new compile will be generated. Useful for debug or if a connected project has changed while the focus project was stable :)
let $forceRecompile     := if (request:exists()) then request:get-parameter('force', 'false') else ('true')
let $forceRecompile     := if (empty($projectVersion)) then $forceRecompile = 'true' or $forceRecompile = 'on' else false()
(:let $format             := if ($projectPrefix and $mode=('compiled', 'verbatim')) then ('xml') else ($format):)
let $transactions       := if (request:exists()) then request:get-parameter('transaction', ())[string-length() gt 0] else ()

let $proj-lang          := if (empty($projectVersion)) then () else if (empty($proj-lang)) then '*' else ($proj-lang)

(: get filtered projects for results :)
let $decorprojects      := 
    if (string-length($projectId) gt 0) then (
        art:getDecorById($projectId, $projectVersion, $proj-lang)
    ) else
    if (string-length($projectPrefix) gt 0) then (
        art:getDecorByPrefix($projectPrefix, $projectVersion, $proj-lang)
    ) else (
        (: cannot get any projects :)
    )
let $projectPrefix      := if (count($decorprojects) = 1) then $decorprojects/project/@prefix   else $projectPrefix
let $projectId          := if (count($decorprojects) = 1) then $decorprojects/project/@id       else $projectId

let $versions           :=
    if ($format = 'html') then 
        if (count($decorprojects) = 1) then 
            $get:colDecorVersion/decor[project[@id = $decorprojects/project/@id]][@versionDate][@language]
        else (
            (:cannot process version without prefix so only live data by default:)
        )
    else ()
let $versions       := 
    if (string-length($projectPrefix) = 0) then (
        (:cannot process version without prefix so only live data by default:)
    )
    else (
        for $project in $versions
        let $versionLanguage    := $project/@language
        let $versionDate        := $project/@versionDate
        let $versionLabel       := if ($project[@versionLabel]) then $project/@versionLabel else ($project/project/release[@date = $versionDate]/@versionLabel | $project/project/version[@date = $versionDate]/@versionLabel)
        order by lower-case($versionDate) descending, $versionLanguage
        return
            <project prefix="{$projectPrefix}" versionDate="{$versionDate}" language="{$versionLanguage}">
            {
                if ($versionLabel[string-length()>0]) then attribute versionLabel {$versionLabel} else (),
                $project/project/@defaultLanguage,
                attribute modifieddate {xmldb:last-modified(util:collection-name($project),util:document-name($project))}
            }
            </project>
    )

let $now                := format-dateTime(current-dateTime(), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')
let $proj-lang          := 
    if ($decorprojects/project/name[@language = $proj-lang] or $proj-lang = '*') then (
        $proj-lang
    ) else 
    if ($decorprojects) then (
        $decorprojects/project/@defaultLanguage/string()[1]
    )
    else ($get:strArtLanguage)
let $filenameverbatim   := 
    if (count($decorprojects)=1 or count(distinct-values($decorprojects/project/@prefix))=1) then 
        if (empty($projectVersion)) then
            concat($projectPrefix,replace($now,'[:\-]',''),'-decor.xml')
        else (
            util:document-name($decorprojects[@language = $proj-lang])
        ) 
    else ()
let $filenamecompiled   := 
    if (count($decorprojects)=1 or count(distinct-values($decorprojects/project/@prefix))=1) then 
        if (empty($projectVersion)) then
            concat($projectPrefix,replace($now,'[:\-]',''),'-',if ($proj-lang = '*') then 'all' else $proj-lang[1],'-decor-compiled.xml')
        else (
            util:document-name($decorprojects[@language = $proj-lang])
        )
    else ()
let $filenamecached     := 
    if (count($decorprojects)=1 or count(distinct-values($decorprojects/project/@prefix))=1) then 
        if (empty($projectVersion)) then
            concat($projectPrefix,replace($now,'[:\-]',''),'-decor-cached.xml')
        else (
            concat($projectPrefix,replace($projectVersion,'[:\-]',''),'-',if ($proj-lang = '*') then 'all' else $proj-lang[1],'-decor-cached.xml')
        ) 
    else ()
    
let $filenameadadef     := 
    if (count($decorprojects)=1 or count(distinct-values($decorprojects/project/@prefix))=1) then 
        if (empty($projectVersion)) then
            concat($projectPrefix,replace($now,'[:\-]',''),'-ada.xml')
        else (
            concat($projectPrefix,replace($projectVersion,'[:\-]',''),'-ada.xml')
        ) 
    else ()
let $filterset          := 
    if (count($decorprojects) = 1) then 
        comp:getCompilationFilterSet($decorprojects)
     else ()
let $filters            :=
    if ($filterset) then
        if (empty($filterId)) then () else
        if ($mode = ($MODECOMPILED, $MODERUNTIME, $MODETEST)) then
            let $filters    := $filterset/filters[@id = $filterId]
            return
                if ($filters) then
                    <filters>
                    {
                        $filters/(@* except @filter),
                        attribute filter {'on'},
                        $filters/node()
                    }
                    </filters>
                else ()
        else ()
    else ()
return 
    if ($format='html') then (
        let $allProjects        := local:getAllProjects($projectId, $projectPrefix, $proj-lang)
        
        let $doVersions         := count(distinct-values($decorprojects/project/@prefix)) = 1
        let $doLanguage         := $doVersions
        let $doMode             := $doVersions
        let $doForceRecompile   := count($decorprojects[empty(@versionDate)]) = 1 and ($mode = $MODECOMPILED or $mode = $MODERUNTIME)
        let $doIgnoreFilter     := count($decorprojects[empty(@versionDate)]) = 1 and ($mode = $MODECOMPILED or $mode = $MODERUNTIME)
        let $doButton           := $doVersions
        return (
            (:response:set-status-code(404), <error>{i18n:getMessage($strMessages,'errorRetrieveProjectNoResults',$ui-lang),' ',if (request:exists()) then request:get-query-string() else()}</error>:)
            if (request:exists()) then response:set-header('Content-Type','text/html; charset=utf-8') else (),
            <html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                    <title>RetrieveProject</title>
                    <link href="{$resourcePath}/css/default.css" rel="stylesheet" type="text/css"/>
                    <script>
                        function getPrefix() {{
                            return document.getElementById('prefixSelector').options[document.getElementById('prefixSelector').selectedIndex].value;
                        }};
                        function getVersionLanguage() {{
                            return document.getElementById('versionSelector').options[document.getElementById('versionSelector').selectedIndex].value;
                        }}
                        function getVersion() {{
                            var versionLanguage = getVersionLanguage();
                            var spaceIndex      = versionLanguage.indexOf(' ');
                            var version;        var language;
                            if (spaceIndex == -1)
                                version = versionLanguage
                            else
                                version = versionLanguage.substr(0, spaceIndex)
                            
                            return version
                        }};
                        function getLanguage() {{
                            return document.getElementById('languageSelector').options[document.getElementById('languageSelector').selectedIndex].value;
                        }};
                        function getMode() {{
                            return document.getElementById('modeSelector').options[document.getElementById('modeSelector').selectedIndex].value;
                        }};
                        function getDownload() {{
                            return document.getElementById('downloadTrue').checked;
                        }};
                        function getForceRecompile() {{
                            return document.getElementById('forceRecompile').checked;
                        }};
                        function getTransactions() {{
                            return document.getElementById('transaction').checked;
                        }};
                        function handleModeChange(selector) {{
                            var currentMode = selector.options[selector.selectedIndex].value;
                            
                            var fr = document.getElementById('forceRecompile'); 
                            fr.checked = false;
                            if (currentMode == '{$MODERUNTIME}' || currentMode == '{$MODECOMPILED}' || currentMode == '{$MODETEST}') {{
                                fr.disabled = false;
                            }} else {{
                                fr.disabled = true;
                            }} 
                            
                            var ignf = document.getElementById('ignoreFilter'); 
                            if (ignf != null) {{
                                ignf.checked = true; 
                                if (currentMode == '{$MODERUNTIME}' || currentMode == '{$MODECOMPILED}' || currentMode == '{$MODETEST}') {{
                                    ignf.disabled = false;
                                }} else {{
                                    ignf.disabled = true;
                                }}
                            }}
                            
                            var trRow = document.getElementById('transactionsRow');
                            if (currentMode == '{$MODEADADEF}') {{
                                if (trRow != null) trRow.style.display = '';
                            }} else {{
                                if (trRow != null) trRow.style.display = 'none';
                            }}
                            trRow = document.getElementById('forceRecompileRow');
                            if (currentMode == '{$MODEADADEF}') {{
                                if (trRow != null) trRow.style.display = 'none';
                            }} else {{
                                if (trRow != null) trRow.style.display = '';
                            }}
                            var trRow = document.getElementById('filtersRow'); 
                            if (currentMode == '{$MODERUNTIME}' || currentMode == '{$MODECOMPILED}' || currentMode == '{$MODETEST}') {{
                                if (trRow != null) trRow.style.display = '';
                            }} else {{
                                if (trRow != null) trRow.style.display = 'none';
                            }}
                            trRow = document.getElementById('ignoreFilterRow');
                            if (currentMode == '{$MODEADADEF}') {{
                                if (trRow != null) trRow.style.display = 'none';
                            }} else {{
                                if (trRow != null) trRow.style.display = '';
                            }}
                        }};
                    </script>
                <script src="resources/scripts/retrieveproject.js" type="text/javascript"></script>
                </head>
                <body>
                    <h1>RetrieveProject</h1>
                    <div class="content">
                    <form name="input" action="RetrieveProject" method="get">
                        <table width="100%" border="0">
                            <!-- project -->
                            <tr style="vertical-align: top;">
                                <td width="50%">{i18n:getMessage($strMessages,'Project',$ui-lang)}:</td>
                                <td>
                                    <select id="prefixSelector" onchange="javascript:location.href=window.location.pathname+'?prefix='+getPrefix()+'&amp;format=html&amp;language='+getLanguage()+'&amp;ui={$ui-lang}'+'&amp;mode='+getMode()+'&amp;download='+getDownload()+'&amp;force='+getForceRecompile()" name="prefix" style="width: 300px;">
                                        <option value="">{i18n:getMessage($strMessages,'SelectProject',$ui-lang)}</option>
                                    {
                                        for $project in $allProjects
                                        order by $project/@repository, lower-case($project/@name)
                                        return
                                            <option value="{$project/@prefix}">
                                            {
                                                if ($project/@prefix=$projectPrefix) 
                                                then attribute {'selected'} {'true'} 
                                                else (),
                                                if ($project/@repository='true') 
                                                then '(BBR) '
                                                else (),
                                                $project/@name/string(),
                                                ' (',$project/@defaultLanguage/string(),')'
                                            }
                                            </option>
                                    }
                                    </select> (*)
                                </td>
                            </tr>
                            <!-- version -->
                            <tr style="background-color: #eee;">
                                <td style="text-align: right;">
                                {
                                    if ($projectVersion castable as xs:dateTime or empty($projectVersion)) then () else (
                                        <span style="margin-right: 1em;">{i18n:getMessage($strMessages,'columnDate',$ui-lang), format-dateTime(xs:dateTime($versions[@versionDate = $projectVersion][1]/@modifieddate), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')}</span>
                                    )
                                }
                                </td>
                                <td>
                                    <select id="versionSelector" onchange="javascript:location.href=window.location.pathname+'?prefix='+getPrefix()+'&amp;version='+getVersion()+'&amp;format=html&amp;language='+getLanguage()+'&amp;ui={$ui-lang}'+'&amp;mode='+getMode()+'&amp;download='+getDownload()+'&amp;force='+getForceRecompile()" name="version" style="width: 300px;">
                                    {
                                        ()(:if ($doVersions) then () else (attribute disabled {'true'}):) 
                                    }
                                        <option value="">--{i18n:getMessage($strMessages,'columnLiveVersion',$ui-lang)}--</option>
                                    {
                                        for $version in $versions
                                        let $versionDate    := $version/@versionDate
                                        group by $versionDate
                                        order by lower-case($version[1]/@versionDate) descending
                                        return
                                            <option value="{$versionDate}">
                                            {
                                                if ($version[1][@versionDate = $projectVersion]) 
                                                then attribute {'selected'} {'true'} 
                                                else (),
                                                $version[1]/@versionDate/string(),
                                                if ($version[1]/@versionLabel) then concat(' (',$version[1]/@versionLabel,')') else ()
                                            }
                                            </option>
                                    }
                                    </select> (optional)
                                </td>
                            </tr>
                            <!-- language -->
                            <tr style="vertical-align: top;">
                                <td>Language to retrieve the project in if you prefer not to retrieve in the default language:</td>
                                <td>
                                    <select name="language" id="languageSelector" style="width: 300px;">
                                    {
                                        ()(:if ($doLanguage) then () else (attribute disabled {'true'}):) 
                                    }
                                    {
                                        if ($projectVersion) then (
                                            for $version in $versions[@versionDate = $projectVersion]
                                            order by $version/@language descending
                                            return
                                                <option value="{$version/@language}">
                                                {
                                                    if ($version[@language = $proj-lang]) then attribute selected {'true'} else (),
                                                    if ($version[@language = '*']) then '--any language--' else 
                                                    if ($version[@language = $version/@defaultLanguage]) then concat('--default language (', $version/@language, ')--')
                                                    else data($version/@language)
                                                }
                                                </option>
                                        ) else (
                                            let $currentProject     := $decorprojects/project[@prefix = $projectPrefix]
                                            let $currentLanguages   := distinct-values($currentProject/name/@language)
                                            
                                            return (
                                                <option value="{$currentProject/@defaultLanguage}" selected="true">--default language ({data($currentProject/@defaultLanguage)})--</option>,
                                                if (count($currentLanguages) = 1) then () else <option value="*">--any language--</option>,
                                                for $version in distinct-values($currentLanguages)
                                                return
                                                    if ($currentProject[@defaultLanguage = $version]) then () else <option value="{$version}">{$version}</option>
                                            )
                                        )
                                    }
                                    </select> (optional)
                                </td>
                            </tr>
                            <!-- mode -->
                            <tr style="vertical-align: top; background-color: #eee;">
                                <td>Get project as-is (verbatim), with all references resolved (compiled), with all runtime references resolved (runtime), just retrieve filters (test), get (cachemeta){if ($projectVersion[not(. = '')]) then ', get transaction (xpaths)' else ()}, get <a href="https://www.art-decor.org/mediawiki/index.php?title=ADA_Documentation#ADA_Definitions" target="_new">ADA Definition</a> (ada-definition)<br/>cachemeta is info for ADRESH caching service can determine if it should refresh its cache:<br/>(Releases/versions are precompiled and can only be retrieved as-is)</td>
                                <td>
                                    <!-- Update forceRecompile disabled status onchange -->
                                    <!-- Update ignoreFilter enabled status onchange -->
                                    <select id="modeSelector" onchange="handleModeChange(this)" name="mode" style="width: 300px;">
                                    {
                                        ()(:if ($doMode) then () else (attribute disabled {'true'}):) 
                                    }
                                        <option value="{$MODEVERBATIM}">{ if ($mode = $MODEVERBATIM)  then attribute selected {'true'} else ()}{$MODEVERBATIM}</option>
                                        <option value="{$MODECOMPILED}">{ if ($mode = $MODECOMPILED)  then attribute selected {'true'} else ()}{$MODECOMPILED}</option>,
                                        <option value="{$MODERUNTIME}">{  if ($mode = $MODERUNTIME)   then attribute selected {'true'} else ()}{$MODERUNTIME}</option>,
                                        <option value="{$MODETEST}">{     if ($mode = $MODETEST)      then attribute selected {'true'} else ()}{$MODETEST}</option>
                                        <option value="{$MODECACHEMETA}">{if ($mode = $MODECACHEMETA) then attribute selected {'true'} else ()}{$MODECACHEMETA}</option>
                                    {   if ($projectVersion[not(. = '')]) then
                                        <option value="{$MODEXPATHS}">{   if ($mode = $MODEXPATHS)    then attribute selected {'true'} else ()}{$MODEXPATHS}</option>
                                        else ()
                                    }
                                    <option value="{$MODEADADEF}">{   if ($mode = $MODEADADEF)    then attribute selected {'true'} else ()}{$MODEADADEF}</option>
                                    </select> (*)
                                </td>
                            </tr>
                            <!-- transactions -->
                            <tr style="vertical-align: top; background-color: #eee; {if ($mode = $MODEADADEF) then () else (' display: none;')}" id="transactionsRow">
                                <td>Select specific transactions to include in the ADA definition. Leave empty for inclusion of all transactions.</td>
                                <td>
                                    <fieldset name="transaction" style="min-width: 300px; max-width: 500px;">
                                        <legend>Select transactions for inclusion</legend>
                                    {
                                        let $i18nScenario       := normalize-space(i18n:getMessage($strMessages, 'Scenario', $ui-lang))
                                        let $i18nTransactions   := normalize-space(i18n:getMessage($strMessages, 'Transactions', $ui-lang))
                                        let $i18nTransaction    := normalize-space(i18n:getMessage($strMessages, 'Transaction', $ui-lang))
                                        
                                        for $scenario in $decorprojects//scenarios/scenario
                                        return (
                                            <div>
                                                <div id="sc_{$scenario/@id}_{$scenario/@effectiveDate}" onclick="selectChildren(this)">
                                                    <input type="checkbox" value="">
                                                        {
                                                        if ($scenario//representingTemplate[@sourceDataset][concept]) then () else (
                                                            attribute disabled {"true"}
                                                        )
                                                    }
                                                        <span class="node-s{$scenario/@statusCode}" title="{
                                                            concat($i18nScenario, ': ', $scenario/@id, ' ', $scenario/@effectiveDate, '. ', $i18nTransactions, ': ', count($scenario/transactions)),
                                                            if ($scenario//representingTemplate[@sourceDataset]) then () else ('. No dataset(s) configured.')
                                                        }">
                                                        {
                                                            '&#160;SC: ',
                                                            data($scenario/(name[@language = $ui-lang], name)[1]), data($scenario/@versionLabel)
                                                        }
                                                        </span>
                                                    </input>
                                                </div>
                                                <div id="contentsc_{$scenario/@id}_{$scenario/@effectiveDate}" style="{if ($scenario//transaction[concat((@id | @ref), '#', @effectiveDate) = $transactions]) then () else 'display: none;'}">
                                                {
                                                    for $transactiongroup in $scenario/transaction
                                                    return (
                                                        <div id="tr_{$transactiongroup/@id}_{$transactiongroup/@effectiveDate}" onclick="selectChildren(this)">
                                                            <input type="checkbox" style="margin-left: 1em;" value="">
                                                                {
                                                                if ($transactiongroup//representingTemplate[@sourceDataset][concept]) then () else (
                                                                    attribute disabled {"true"}
                                                                )
                                                            }
                                                                <span class="node-s{$transactiongroup/@statusCode}" title="{
                                                                    concat($i18nTransaction, ': ', $transactiongroup/@id, ' ', $transactiongroup/@effectiveDate),
                                                                    if ($transactiongroup//representingTemplate[@sourceDataset]) then () else ('. No dataset(s) configured.')
                                                                }">
                                                                {
                                                                    '&#160;TR: ',
                                                                    data($transactiongroup/(name[@language = $ui-lang], name)[1]), data($transactiongroup/@versionLabel)
                                                                }
                                                                </span>
                                                            </input>
                                                        </div>
                                                        ,
                                                        <div id="contenttr_{$transactiongroup/@id}_{$transactiongroup/@effectiveDate}">
                                                        {
                                                            for $transaction in $transactiongroup/transaction
                                                            return (
                                                                <div>
                                                                    <input type="checkbox" style="margin-left: 2em;">
                                                                    {
                                                                        if ($transaction/representingTemplate[@sourceDataset][concept]) then (
                                                                            attribute name {"transaction"},
                                                                            attribute value {string-join(($transaction/(@id | @ref), $transaction/@effectiveDate), '#')}
                                                                        ) else (
                                                                            attribute value {()},
                                                                            attribute disabled {"true"}
                                                                        )
                                                                        ,
                                                                        <span class="node-s{$transaction/@statusCode}" title="{
                                                                            concat($i18nTransaction, ': ', $transaction/@id, ' ', $transaction/@effectiveDate), 
                                                                            if ($transaction/representingTemplate[@sourceDataset]) then () else ('. No dataset configured.')
                                                                        }">
                                                                        {
                                                                            '&#160;TR: '
                                                                            ,
                                                                            data($transaction/(name[@language = $ui-lang], name)[1]), data($transaction/@versionLabel)
                                                                        }
                                                                        </span>
                                                                    }
                                                                    </input>
                                                                </div>
                                                            )
                                                        }
                                                        </div>
                                                    )
                                                }
                                                </div>
                                            </div>
                                        )
                                    }
                                    </fieldset>
                                </td>
                            </tr>
                            <!-- forceRecompile -->
                            <tr style="vertical-align: top;{if ($mode = $MODEADADEF) then (' display: none;') else ()}" id="forceRecompileRow">
                                <td>Force recompile in mode '{$MODECOMPILED}' even if a valid previous compilation exists. Useful for testing or if the project itself didn't change, but one of it's connected projects did. Default is false.</td>
                                <td>
                                    <input id="forceRecompile" name="force" type="checkbox">
                                    {
                                        if ($doForceRecompile) then () else (attribute disabled {'true'}),
                                        if ($forceRecompile)   then attribute checked {'true'} else ()
                                    }
                                    </input> Force recompile (optional)
                                </td>
                            </tr>
                            <!-- filters -->
                            <tr style="vertical-align: top;{if (count($decorprojects) = 1 and $mode = ($MODECOMPILED, $MODERUNTIME, $MODETEST)) then () else (' display: none;')}" id="filtersRow">
                                <td>Use the following filter:</td>
                                <td>
                                    <select id="filterSelector" name="filterId" style="width: 300px;">
                                        <option value="">{ if (empty($filterId)) then attribute selected {'selected'} else ()}-- no filters, do all</option>
                                    {
                                        for $f in $filterset/filters
                                        let $label  := $f/@label
                                        order by $label
                                        return
                                            <option value="{$f/@id}">{ if ($filterId = $f/@id) then attribute selected {'selected'} else ()}{data($label)}</option>
                                    }
                                    </select>
                                </td>
                            </tr>
                            <!-- ignoreFilter -->
                            <!--<tr style="vertical-align: top;{if ($mode = $MODEADADEF) then (' display: none;') else ()}" id="ignoreFilterRow">
                                <td>Use current compile filter settings if any (false) or ignore any filter settings and do a full compile (true). Default is true.</td>
                                <td>
                                    <input id="ignoreFilter" name="ignoreFilter" type="checkbox">
                                    {
                                        if ($doIgnoreFilter) then () else (attribute disabled {'true'}),
                                        if ($ignoreFilter)   then attribute checked {'true'} else ()
                                    }
                                    </input> Ignore any filters (optional)
                                </td>
                            </tr>-->
                            <!-- download/show -->
                            <tr style="vertical-align: top; background-color: #eee;">
                                <td>Download to disk or show in browser:</td>
                                <td>
                                    <input id="downloadFalse" type="radio" name="download" value="false">{if ($download='false') then attribute checked {'false'} else ()} Show</input>
                                    <input id="downloadTrue" type="radio" name="download" value="true">{ if ($download='true')  then attribute checked {'true'} else () } Download</input>
                                </td>
                            </tr>
                            <tr style="vertical-align: top;">
                                <td><input name="format" type="hidden" value="xml"/></td>
                                <td><input type="submit" value="{i18n:getMessage($strMessages,'Send',$ui-lang)}" style="font-weight: bold; color: black;">
                                {
                                    if ($doButton) then () else attribute disabled {'true'}
                                }
                                </input></td>
                            </tr>
                        </table>
                    </form>
                    </div>
                </body>
            </html>
        )
    )
    else if ($format='xml' and $mode = $MODECACHEMETA) then (
        let $allProjects    := local:getAllProjects($projectId, $projectPrefix, $proj-lang)
        
        return (
            if (request:exists()) then response:set-header('Content-Type','text/xml; charset=utf-8') else (),
            if ($download='true') then (
                if (request:exists()) then response:set-header('Content-Disposition', concat('attachment; filename=',$filenamecached)) else ()
            ) else (),
            <return>
            {
                if (count($projectId) gt 0)     then $allProjects[@id=$projectId] else 
                if (count($projectPrefix) gt 0) then $allProjects[@prefix=$projectPrefix] else $allProjects
            }
            </return>
        )
    )
    else 
    if ($mode = $MODETEST) then (
        if (request:exists()) then response:set-header('Content-Type','text/xml; charset=utf-8') else (),
        if ($download='true') then (
            if (request:exists()) then response:set-header('Content-Disposition', concat('attachment; filename=',$filenamecompiled)) else (),
            processing-instruction {'xml-stylesheet'} {' type="text/xsl" href="https://assets.art-decor.org/ADAR/rv/DECOR2schematron.xsl"'}
        ) else (),
        comp:getFinalCompilationFilters($decorprojects, $filters)
    )
    else 
    if (count($decorprojects) = 0) then (
        if (request:exists()) then response:set-status-code(404) else (), 
        <error>{i18n:getMessage($strMessages,'errorRetrieveProjectNoSingleResult',$ui-lang),concat(' (',count($decorprojects),') '), if (request:exists()) then request:get-query-string() else()}</error>
    )
    else 
    if (count($decorprojects) gt 1 and empty($projectVersion)) then (
        if (request:exists()) then response:set-status-code(404) else (), 
        <error>{i18n:getMessage($strMessages,'errorRetrieveProjectNoSingleResult',$ui-lang),concat(' (',count($decorprojects),') '), if (request:exists()) then request:get-query-string() else()}</error>
    )
    else 
    if ($mode = ($MODECOMPILED, $MODERUNTIME, $MODETEST)) then (
        let $projectname    := $decorprojects/project/name[1]
        let $result         := 
            if ($decorprojects[@compilationDate]) then ($decorprojects[@language = $proj-lang][1]) else (
                (:comp:compileDecor($decorprojects, $proj-lang, $now, $filters, $mode=$MODETEST, $mode=$MODERUNTIME):)
                comp:getCompiledResult($decorprojects[1], $now, $proj-lang, comp:getFinalCompilationFilters($decorprojects, $filters), $mode=$MODETEST, $mode=$MODERUNTIME, $forceRecompile)
            )
        
        return
            if ($result/descendant-or-self::decor[1]) then (
                if (request:exists()) then response:set-header('Content-Type','text/xml; charset=utf-8') else (),
                if ($download = 'true') then (
                    if (request:exists()) then response:set-header('Content-Disposition', concat('attachment; filename=',$filenamecompiled)) else (),
                    processing-instruction {'xml-stylesheet'} {' type="text/xsl" href="https://assets.art-decor.org/ADAR/rv/DECOR2schematron.xsl"'}
                ) else (),
                if ($mode = $MODETEST) then () else (
                    processing-instruction {'xml-model'} {' href="https://assets.art-decor.org/ADAR/rv/DECOR.sch" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"'}
                ),
                $result/descendant-or-self::decor[1]
            )
            else (
                <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="nl" lang="nl">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                        <!--<meta http-equiv="refresh" content="30" />-->
                        <meta http-equiv="refresh" content="30; URL=?prefix={$projectPrefix}&amp;version={$projectVersion}&amp;mode={$mode}&amp;language={$proj-lang}&amp;filterId={$filterId}&amp;download={$download}&amp;force=false&amp;ui={$ui-lang}&amp;format=xml"/>
                        <meta name="robots" content="noindex, nofollow" />
                        <meta http-equiv="expires" content="0" />
                        <title>Compiling {$projectname} ....</title>
                    </head>
                    <body style="font-family: Verdana, Arial, sans-serif;">
                        <div>Compiling {data($projectname)} with language {data($proj-lang)} since {data($result/@compileDate)} (auto-refreshing every 30 seconds...)</div>
                    </body>
                 </html>
            )
    )
    else 
    if ($mode = $MODECACHE) then (
        if (request:exists()) then response:set-header('Content-Type','text/xml; charset=utf-8') else (),
        if ($download='true') then (
            if (request:exists()) then response:set-header('Content-Disposition', concat('attachment; filename=',$filenamecached)) else ()
        ) else (),
        if ($decorprojects[@compilationDate]) then local:addCacheAttributes($decorprojects[@language = $proj-lang][1]) else local:addCacheAttributes($decorprojects[1])
    )
    else 
    if ($format='xml' and $mode = $MODEXPATHS) then (
        let $decorproject   := $decorprojects[1]
        let $xpathdoc       := (collection(util:collection-name($decorproject))/transactionDatasets[@language = $decorproject/@language])[1]
        return (
            if (request:exists()) then response:set-header('Content-Type','text/xml; charset=utf-8') else (),
            if ($download='true') then (
                if (request:exists()) then response:set-header('Content-Disposition', concat('attachment; filename=',util:document-name($xpathdoc))) else ()
            ) else (),
            $xpathdoc
        )
    )
    else
    if ($format = 'xml' and $mode = $MODEADADEF) then (
        let $decorproject   := $decorprojects[1]
        let $xslt           := xs:anyURI(concat('xmldb:exist://',$get:strDecorServices, '/resources/stylesheets/decor2ada-definition.xsl'))
        let $params         :=
            <parameters>
                <param name="decorServices" value="{$artDeepLinkServices}"/>
                <param name="projectPrefix" value="{$projectPrefix}"/>
                <param name="projectVersion" value="{$projectVersion}"/>
                <param name="projectLanguage" value="{$proj-lang}"/>
                <!--<param name="projectLabel" value=""/>-->
                <param name="transactions" value="{string-join($transactions, ',')}"/>
            </parameters>
        return (
            if (request:exists()) then response:set-header('Content-Type','text/xml; charset=utf-8') else (),
            if ($download='true') then (
                if (request:exists()) then response:set-header('Content-Disposition', concat('attachment; filename=', $filenameadadef)) else ()
            ) else (),
            comment {
                concat('Generated at ', current-dateTime(), ' with URI:'),
                '&#10;',
                concat($artDeepLinkServices, 'RetrieveProject?', if (request:exists()) then request:get-query-string() else ()),
                '&#10;'
            },
            comment {
                'Documentation for ADA: https://www.art-decor.org/mediawiki/index.php?title=ADA_Documentation'
            },
            transform:transform($decorproject, $xslt, $params)
        )
    )
    (:verbatim is the default:)
    else (
        if (request:exists()) then response:set-header('Content-Type','text/xml; charset=utf-8') else (),
        if ($download='true') then (
            if (request:exists()) then response:set-header('Content-Disposition', concat('attachment; filename=',$filenameverbatim)) else (),
            processing-instruction {'xml-stylesheet'} {' type="text/xsl" href="https://assets.art-decor.org/ADAR/rv/DECOR2schematron.xsl"'}
        ) else (),
        processing-instruction {'xml-model'} {' href="https://assets.art-decor.org/ADAR/rv/DECOR.sch" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"'},
        if ($decorprojects[@compilationDate]) then $decorprojects[@language = $proj-lang][1] else $decorprojects[1]
    )