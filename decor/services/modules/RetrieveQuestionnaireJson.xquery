xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";
import module namespace decor       = "http://art-decor.org/ns/decor" at "../../../art/api/api-decor.xqm";
import module namespace quest       = "http://art-decor.org/ns/quest" at "questionnaire.xqm";

declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "json";
declare option output:media-type "application/json";
declare option output:json-ignore-whitespace-text-nodes "yes";

(: demo 1 :)
let $id                     := if (request:exists()) then request:get-parameter('id',())[string-length()>0][1] else '2.16.840.1.113883.3.1937.99.62.3.4.2'
let $effectiveDate          := if (request:exists()) then request:get-parameter('effectiveDate',()) else '2012-09-05T16:59:35'
let $version                := if (request:exists()) then request:get-parameter('version',()) else ()
let $language               := if (request:exists()) then request:get-parameter('language',()) else 'nl-NL'
let $fhirLinkItemStyle      := if (request:exists()) then request:get-parameter('fhirLinkItemStyle',()) else 'idDisplay'

let $questionnaire := quest:getQuestionnaireJson($id, $effectiveDate, $version, $language, $fhirLinkItemStyle)
return 
    if ($questionnaire) then ($questionnaire)
    else (
        if (request:exists()) then (
            response:set-status-code(404)
        ) else ()
    )
