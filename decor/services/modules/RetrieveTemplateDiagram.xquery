xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace tmapi       = "http://art-decor.org/ns/api/template" at "../../../api/modules/template-api.xqm";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

(:
    function takes a DECOR template as argument and returns
    the tempate as a set of different diagrams
    "svg", "xml", "hlist", "hgraph", "hgraphwiki", "wikilist", "transclusionwikilist"
     - xml-content: svg, xml and hlist
     - html-content: hgraph, several wiki formats
:)

let $language               := if (request:exists()) then request:get-parameter('language', ()) else ()
let $projectPrefix          := if (request:exists()) then if (request:get-parameter-names()[. = 'project']) then request:get-parameter('project', ()) else request:get-parameter('prefix', ()) else ()
let $templateId             := if (request:exists()) then request:get-parameter('id', ()) else ()
let $templateEffectiveDate  := if (request:exists()) then request:get-parameter('effectiveDate', 'dynamic') else ()
let $format                 := if (request:exists()) then request:get-parameter('format','svg') else ('svg')

return 
    if (empty($templateId)) then
        error($errors:BAD_REQUEST, 'Parameter templateId is required, without templateEffectiveDate you get the latest version')
        else (
        tmapi:getTemplateDiagram(map {
            "parameters": map {
                "language": $language,
                "project" : $projectPrefix,
                "format": $format,
                "id": $templateId,
                "effectiveDate": $templateEffectiveDate
                }
            }
        )
    )