xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../../art/api/api-server-settings.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";

let $projectPrefix      := if (request:exists()) then request:get-parameter('prefix',())[string-length()>0][1] else ('demo1-')
let $projectVersion     := if (request:exists()) then request:get-parameter('version',())[string-length()>0][1] else ('development')
let $projectLanguage    := if (request:exists()) then request:get-parameter('language',())[string-length()>0][1] else ()
(: request a specific logo to be returned. param is ignored when mode='list' :)
let $logoname           := if (request:exists()) then request:get-parameter('logo',())[string-length()>0][1] else ()
(: request a list of project logos to be returned :)
let $mode               := if (request:exists()) then request:get-parameter('mode',())[string-length()>0][1] else ('list')

let $decorProject   :=
    if (empty($projectPrefix)) then (
        error(QName('http://art-decor.org/ns/error','MissingParameter'),'Parameter ''prefix'' is required')
    ) else (
        art:getDecorByPrefix($projectPrefix, $projectVersion, $projectLanguage)/project
    )

return
    if ($decorProject) then
        if ($mode = 'list') then (
            let $logodir    := concat(util:collection-name($decorProject),'/',$decorProject/@prefix,'logos/')
            let $configured := $decorProject/reference/@logo | $decorProject/copyright/@logo
            let $notinuse   := xmldb:get-child-resources($logodir)[not(.=$configured)]
            return
            <project>
            {
                $decorProject/@id,
                $decorProject/@prefix,
                for $logo in $configured
                return
                    element {name($logo/..)} {$logo/../@by, $logo}
                ,
                if (empty($notinuse)) then () else (
                    comment {' These logos exist in the logos collection but are not used '},
                    for $logo in $notinuse
                    return
                        <otherlogo logo="{$logo}"/>
                )
            }
            </project>
        )
        else (
            let $logo           := if (empty($logoname)) then $decorProject/reference/@logo[string-length()>0] else $logoname
            let $logosrc        := 
                if (empty($logo)) then () else (
                    if (starts-with($logo,'http')) then ($logo) else (
                        (:assume the logo is in project-logos collection:)
                        concat(util:collection-name($decorProject),'/',$decorProject/@prefix,'logos/',$logo)
                    )
                )
            let $logosrc        :=
                if (starts-with($logoname,'http')) then ($logosrc) else 
                if (util:binary-doc-available($logosrc)) then ($logosrc) else (
                    (:  final fallback is server logo. note that we only reach this point if no specific logo was 
                        requested or configured or if the requested/configured logo does not exist. if the server-logo 
                        does not load then this server should be configured better anyway... :)
                    let $logoname   := adserver:getServerLogo()
                    return
                    if (starts-with($logoname,'http')) then ($logoname) else (
                        concat(adserver:getServerURLArt(),'img/',$logoname)
                    )
                )
            let $logoext        := tokenize($logosrc,'\.')[last()]
            let $logofilename   := tokenize($logosrc,'/')[last()]
            
            return
            if (response:exists()) then (
                if (starts-with($logosrc,'http')) then (
                    response:redirect-to(xs:anyURI($logosrc))
                ) else (
                    response:stream-binary(util:binary-doc($logosrc),concat('image/',$logoext),$logofilename)
                )
            )
            else (
                (: testing from eXide or oXygen:)
                <logo src="{$logosrc}" mediaType="{concat('image/',$logoext)}" name="{$logofilename}"/>
            )
        )
    else (
        if (response:exists()) then (
            response:set-status-code(404), response:set-header('Content-Type','text/html')
        ) else (),
        <html>
            <head><title>HTTP 404</title></head>
            <body>
                <h1>HTTP 404</h1>
                <p>
                {
                    if (empty($projectVersion)) then
                        concat('Project with prefix ',string-join($projectPrefix,' '),' not found.')
                    else
                        concat('Project with prefix ',string-join($projectPrefix,' '),' and version ',string-join($projectVersion,' '),' not found.')
                }
                </p>
            </body>
        </html>
    )