xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../../art/api/api-server-settings.xqm";
import module namespace iss         = "http://art-decor.org/ns/decor/issue" at "../../../art/api/api-decor-issue.xqm";
import module namespace i18n        = "http://art-decor.org/ns/decor/i18n" at "../../../art/api/api-decor-i18n.xqm";

(:declare option exist:serialize "method=xml media-type=text/xml omit-xml-declaration=no indent=no";:)
declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=no indent=no 
        doctype-public=-//W3C//DTD&#160;XHTML&#160;1.0&#160;Transitional//EN
        doctype-system=http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd";

declare variable $artDeepLinkServices   := adserver:getServerURLServices();
declare variable $useLocalAssets        := if (request:exists()) then request:get-parameter('useLocalAssets','false') else 'false';
declare variable $download              := if (request:exists()) then request:get-parameter('download','false') else '';
(:When called with localAssets=true then we need relative local paths, else 
    we need our server services URL, but with matching scheme (http or https)
    If the scheme is https and servicesUrl is http this leads to a security 
    problem in some browsers
:)
declare variable $resourcePath          := if ($download='true') then ('https://assets.art-decor.org/ADAR/rv/assets') else if ($useLocalAssets = 'true') then ('../assets') else ('../core/assets');

declare variable $docMessages           := i18n:getMessagesDoc('decor/services');
declare variable $strArtURL             := adserver:getServerURLArt();

(: TODO: media-type beter zetten en XML declaration zetten bij XML output :)
(:declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=yes";:)

let $projectPrefix      := if (request:exists()) then (request:get-parameter('prefix',())) else ()
let $projectVersion     := if (request:exists()) then (request:get-parameter('version',())) else ()
let $format             := if (request:exists() and string-length(request:get-parameter('format','')[1])>0) then request:get-parameter('format','html')[1] else ('html')
let $language           := if (request:exists()) then (request:get-parameter('language',$get:strArtLanguage)) else ()
let $searchString       := if (request:exists()) then (request:get-parameter('searchString',())[string-length()>0]) else ()
let $searchTerms        := tokenize(lower-case($searchString),'\s')
let $types              := 
    if (request:exists()) then (
        for $s in request:get-parameter('type',())[string-length()>0]
        return tokenize($s, '\s')
    ) else ()
let $types              := if (empty($types)) then ('RFC','INC','CLF') else $types
let $priorities         := 
    if (request:exists()) then (
        for $s in request:get-parameter('priority',())[string-length()>0]
        return tokenize($s, '\s')
    ) else ()
let $statuscodes        := 
    if (request:exists()) then (
        for $s in request:get-parameter('statusCode',())[string-length()>0]
        return tokenize($s, '\s')
    ) else ()
let $statuscodes        := if (empty($statuscodes)) then ('open','inprogress','feedback') else $statuscodes
let $lastassignedids    := 
    if (request:exists()) then (
        for $s in request:get-parameter('assignedTo',())[string-length()>0]
        return tokenize($s, '\s')
    ) else ()
let $labels             := 
    if (request:exists()) then (
        for $s in request:get-parameter('labels',())[string-length()>0]
        return tokenize($s, '\s')
    ) else ()
let $changedafter       := if (request:exists()) then (request:get-parameter('changedafter',())[string-length()>0]) else ()
let $sort               := if (request:exists()) then (request:get-parameter('sort',())[string-length()>0][1]) else ()
let $max                := if (request:exists()) then (request:get-parameter('max',$iss:maxResults)[string-length()>0][1]) else ($iss:maxResults)

let $max                := if ($max castable as xs:integer) then xs:integer($max) else (75)

let $htmlInline         := if (request:exists() and string-length(request:get-parameter('inline',())[1])>0) then request:get-parameter('inline',())[1] else ()
let $displayHeader      := if ($htmlInline='true') then false() else true()

let $switchDoTreeTable      := if (request:exists()) then request:get-parameter('collapsable','false')='true' else (false())

let $artdecordeeplinkprefix := adserver:getServerURLArt()

let $issueList          :=
    if (empty($projectPrefix)) then () else (
        iss:getIssueList($projectPrefix, $searchTerms, $types, $priorities, $statuscodes, $lastassignedids, $labels, $changedafter, $sort, $max)
    )
    
return
    if ($format = 'xml') then (
        response:set-status-code(200),
        response:set-header('Content-Type','text/xml; charset=utf-8'),
        $issueList
    )
    else (
        let $decor          := if ($projectPrefix) then art:getDecorByPrefix($projectPrefix) else ()
        let $projects       :=
            for $prefix in $projectPrefix
            return art:getDecorByPrefix($prefix)
        let $allProjects    := 
            for $project in (collection($get:strDecorData)//decor[not(@private='true')]/project |
                             collection($get:strDecorData)//decor/project[@prefix=$projectPrefix])
            return
                <project>
                {
                    $project/@id,
                    $project/@prefix,
                    $project/@defaultLanguage,
                    attribute repository {$project/parent::decor/@repository='true'},
                    attribute private {$project/parent::decor/@private='true'},
                    attribute experimental {$project/@experimental='true'},
                    attribute name {if ($project/name[@language=$language]) then $project/name[@language=$language][1] else $project/name[1]},
                    attribute creationdate {xmldb:created(util:collection-name($project),util:document-name($project))},
                    attribute modifieddate {xmldb:last-modified(util:collection-name($project),util:document-name($project))}
                }
                </project>
        let $decorTypes     := art:getDecorTypes()
        let $logo           := 
            if (count($projects)=1) then (
                concat('ProjectLogo?prefix=',$projects/project/@prefix,'&amp;version=',encode-for-uri($projectVersion[1]))
            ) else (
                let $server-logo    := adserver:getServerLogo()
                return if (starts-with($server-logo, 'http')) then $server-logo else concat('/art-decor/img/', $server-logo)
            )
        let $url          :=  if (count($projects)=1) then ($projects/project/@url) else ()
        
        return (
            response:set-status-code(200),
            response:set-header('Content-Type','text/html; charset=utf-8'),
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>{i18n:getMessage($docMessages,'titleIssueIndex',$language)}</title>
        <link href="{$resourcePath}/css/default.css" rel="stylesheet" type="text/css"/>
    </head>
    <body onload="javascript:document.getElementById('searchString').focus();">
        <a name="top"></a>
        <table width="100%">
            <tbody>
                <tr>
                    <td align="left">
                        <h1>{i18n:getMessage($docMessages,'titleIssueIndex',$language)}</h1>
                    </td>
                    <td align="right">
                    {if ($logo and $url) then 
                        <a href="{$url}">
                            <img src="{$logo}" alt="" title="{$url}" height="50px"/>
                        </a>
                     else if ($logo) then
                        <img src="{$logo}" alt="" height="50px"/>
                     else ()
                    }
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="text-align: right; vertical-align: top;">
                        <img src="/art-decor/img/flags/nl.png" onclick="location.href=window.location.pathname+'?language=nl-NL{string-join(for $p in request:get-parameter-names() return if ($p='language') then () else concat('&amp;',$p,'=',string-join(request:get-parameter($p,())[string-length()>0]),' '),'')}';" class="linked flag"/>
                        <img src="/art-decor/img/flags/de.png" onclick="location.href=window.location.pathname+'?language=de-DE{string-join(for $p in request:get-parameter-names() return if ($p='language') then () else concat('&amp;',$p,'=',string-join(request:get-parameter($p,())[string-length()>0]),' '),'')}';" class="linked flag"/>
                        <img src="/art-decor/img/flags/us.png" onclick="location.href=window.location.pathname+'?language=en-US{string-join(for $p in request:get-parameter-names() return if ($p='language') then () else concat('&amp;',$p,'=',string-join(request:get-parameter($p,())[string-length()>0]),' '),'')}';" class="linked flag"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <form name="input">
                            <input type="hidden" name="language" value="{$language}"/>
                            <table width="100%">
                                <!-- Project -->
                                <tr>
                                    <td>{i18n:getMessage($docMessages,'Project',$language)}</td>
                                    <td colspan="5">
                                        <select id="prefixSelector" name="prefix" onchange="javascript:location.href=window.location.pathname+'?prefix='+this.options[this.selectedIndex].value+'&amp;format=html&amp;language={$language}'">
                                        <option value="">{i18n:getMessage($docMessages,'SelectProject',$language)}</option>
                                        {
                                            for $project in $allProjects
                                            order by $project/@repository, lower-case($project/@name)
                                            return
                                                <option value="{$project/@prefix/string()}">
                                                {
                                                    if ($project/@prefix=$projectPrefix) 
                                                    then attribute {'selected'} {'true'} 
                                                    else (),
                                                    if ($project/@repository='true') 
                                                    then '(BBR) '
                                                    else (),
                                                    $project/@name/string()
                                                }
                                                </option>
                                        }
                                        </select>
                                    </td>
                                </tr>
                                <!-- Status -->
                                <tr>
                                    <td>{i18n:getMessage($docMessages,'columnStatus',$language)}</td>
                                    <td colspan="5">
                                    {
                                        for $t in $decorTypes/IssueStatusCodeLifeCycle/enumeration[not(@value = 'new')]
                                        return
                                            <input type="checkbox" name="statusCode" value="{$t/@value}" style="margin: 0 5px;">
                                            {
                                                if ($statuscodes[. = $t/@value]) then attribute checked {'checked'} else (), 
                                                ($t/label[@language=$language][1], $t/label[@language='en-US'])[1]/node()
                                            }
                                            </input>
                                    }
                                    </td>
                                </tr>
                                <!-- Type -->
                                <tr>
                                    <td>{i18n:getMessage($docMessages,'columnType',$language)}</td>
                                    <td colspan="5">
                                    {
                                        for $t in $decorTypes/IssueType/enumeration
                                        return
                                            <input type="checkbox" name="type" value="{$t/@value}" style="margin: 0 5px;">
                                            {
                                                if ($types[. = $t/@value]) then attribute checked {'checked'} else (), 
                                                if ($t/label[@language=$language]) then $t/label[@language=$language]/node() else $t/label[@language='en-US']/node()
                                            }
                                            </input>
                                    }
                                    </td>
                                </tr>
                                <!-- Priority -->
                                <tr>
                                    <td>{i18n:getMessage($docMessages,'columnPriority',$language)}</td>
                                    <td colspan="5">
                                    {
                                        for $t in $decorTypes/IssuePriority/enumeration
                                        return
                                            <input type="checkbox" name="priority" value="{$t/@value}" style="margin: 0 5px;">
                                            {
                                                if ($priorities[. = $t/@value]) then attribute checked {'checked'} else (), 
                                                if ($t/label[@language=$language]) then $t/label[@language=$language]/node() else $t/label[@language='en-US']/node()
                                            }
                                            </input>
                                    }
                                    </td>
                                </tr>
                                <!-- Labels -->
                                { if ($decor/issues/labels/label) then (
                                    <tr>
                                        <td>{i18n:getMessage($docMessages,'columnLabels',$language)}</td>
                                        <td colspan="5">
                                        {
                                            (: Get all labels that are actually in use (list will likely contains duplicates) :)
                                            let $availableLabels    := $issueList/issue/@currentLabels[string-length() > 0]/tokenize(., '\s')
                                            
                                            for $t in $decor/issues/labels/label[@code = $availableLabels]
                                            let $selectedLabelCode  := $t/@code
                                            let $selectedLabelColor := $t/@color
                                            let $selectedLabelName  := $t/@name
                                            let $selectedLabelDesc  := if ($t/desc[@language = $language]) then attribute title {$t/desc[@language = $language]} else ($t/@name)
                                            order by $selectedLabelCode
                                            return
                                                <input type="checkbox" name="labels" value="{$selectedLabelCode}" style="margin: 0 5px;">
                                                {   if ($labels[. = $selectedLabelCode]) then attribute checked {'checked'} else ()
                                                }
                                                    <span style="border: 1px solid gray;" title="{$selectedLabelDesc}">
                                                        <span style="background-color: {$selectedLabelColor}; padding-left: 10px;">&#160;</span>
                                                        <span style="background-color: white;">{concat('&#160;(',$selectedLabelCode,')&#160;',$selectedLabelName,'&#160;')}</span>
                                                    </span>
                                                </input>
                                        }
                                        </td>
                                    </tr>
                                ) else ()}
                                <!-- Description / Format -->
                                <tr>
                                    <td>{i18n:getMessage($docMessages,'columnDescription',$language)}</td>
                                    <td><input type="text" id="searchString" name="searchString" value="{$searchString}" style="width: 20em;"/></td>
                                    <td>{i18n:getMessage($docMessages,'columnAssignedTo',$language)}</td>
                                    <td>
                                    {
                                        <select id="assignedToSelector" name="assignedTo">
                                            <option value="">---</option>
                                            <option value="#UNASSIGNED#">---{i18n:getMessage($docMessages,'Unassigned',$language)}---</option>
                                        {
                                            for $assignee in distinct-values($issueList/meta/assignee[string-length(@to) > 0]/@to)
                                            return
                                                <option value="{$assignee}">{if ($assignee = $lastassignedids) then attribute selected {'true'} else (), $decor/project/author[@id = $assignee]/text()}</option>
                                        }
                                        </select>
                                    }
                                    </td>
                                    <td>{i18n:getMessage($docMessages,'columnSortBy',$language)}</td>
                                    <td>
                                        <select id="sort" name="sort">
                                            <option value="">--{i18n:getMessage($docMessages,'Default',$language), concat('(',i18n:getMessage($docMessages,'columnDate',$language),')')}--</option>
                                            <option value="id">{if ($sort = 'id') then attribute selected {'selected'} else (), i18n:getMessage($docMessages,'columnID',$language)}</option>
                                            <option value="issue">{if ($sort = 'issue') then attribute selected {'selected'} else (), i18n:getMessage($docMessages,'columnIssue',$language)}</option>
                                            <option value="status">{if ($sort = 'status') then attribute selected {'selected'} else (), i18n:getMessage($docMessages,'columnStatus',$language)}</option>
                                            <option value="priority">{if ($sort = 'priority') then attribute selected {'selected'} else (), i18n:getMessage($docMessages,'columnPriority',$language)}</option>
                                            <option value="type">{if ($sort = 'type') then attribute selected {'selected'} else (), i18n:getMessage($docMessages,'columnType',$language)}</option>
                                            <option value="date">{if ($sort = 'date') then attribute selected {'selected'} else (), i18n:getMessage($docMessages,'columnDate',$language)}</option>
                                            <option value="assigned-to">{if ($sort = 'assigned-to') then attribute selected {'selected'} else (), i18n:getMessage($docMessages,'columnAssignedTo',$language)}</option>
                                            <option value="label">{if ($sort = 'label') then attribute selected {'selected'} else (), i18n:getMessage($docMessages,'columnLabels',$language)}</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">&#160;</td>
                                    <td>{i18n:getMessage($docMessages,'columnLastModified',$language)}</td>
                                    <td>
                                        <input type="text" id="changedafter" name="changedafter" value="{$changedafter}" style="width: 20em;" title="{i18n:getMessage($docMessages,'variablevaguedatetime-hint',$language)}"/>
                                    </td>
                                    <td colspan="2">&#160;</td>
                                </tr>
                                <tr>
                                    <td>&#160;</td>
                                    <td colspan="3">
                                        <input type="submit" value="{i18n:getMessage($docMessages,'Find',$language)}" onclick="location.href=window.location.pathname+'?language={$language}"/>
                                        <span style="margin: 0 5px;">{i18n:getMessage($docMessages,'Format',$language)}: </span>
                                        <input type="radio" value="xml" name="format" style="margin: 0 5px;">xml</input>
                                        <input type="radio" value="html" name="format" checked="checked" style="margin: 0 5px;">html</input>
                                    </td>
                                    <td colspan="2" style="text-align: right;">
                                    {
                                        '&#160; &#160;',
                                        if (empty($issueList)) then (
                                            i18n:getMessage($docMessages,'errorNoResults',$language),' ',if (request:exists()) then request:get-query-string() else ('…')
                                        ) else (
                                            i18n:getMessage($docMessages,'ResultsCurrentTotal',$language, $issueList/@current, $issueList/@total)
                                        )
                                    }
                                        <span style="margin: 0 5px;">{i18n:getMessage($docMessages,'columnMax',$language)}: </span>
                                        <input type="number" id="max" name="max" value="{$max}" class="short-number"/>
                                    </td>
                                </tr>
                            </table>
                            {()(:if ($resultCount>0) then (concat(' (',i18n:getMessage($docMessages,'FoundXResults',$language,$resultCount),')')) else ():)}
                        </form>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="content">
            <table class="values" id="issueList">
                <thead>
                    <tr>
                        <th>XML</th>
                        <th>HTML</th>
                        <th>{i18n:getMessage($docMessages,'columnID',$language)}</th>
                        <th>{i18n:getMessage($docMessages,'columnIssue',$language)}</th>
                        <th>{i18n:getMessage($docMessages,'columnStatus',$language)}</th>
                        <th>{i18n:getMessage($docMessages,'columnPriority',$language)}</th>
                        <th>{i18n:getMessage($docMessages,'columnType',$language)}</th>
                        <th>{i18n:getMessage($docMessages,'columnDate',$language)}</th>
                        <th>{i18n:getMessage($docMessages,'columnAssignedTo',$language)}</th>
                        <th>{i18n:getMessage($docMessages,'columnLabels',$language)}</th>
                    </tr>
                </thead>
                <tbody>
                {
                    for $issue in $issueList/issue
                    return 
                        <tr style="background-color:white" onMouseover="this.style.backgroundColor='lightblue';" onMouseout="this.style.backgroundColor='white';">
                            <td><a href="RetrieveIssue?prefix={$projectPrefix}&amp;id={$issue/@id}&amp;format=xml">xml</a></td>
                            <!--<td><a href="RetrieveIssue?prefix={$projectPrefix}&amp;id={$issue/@id}&amp;format=html&amp;language={$language}">html</a></td>-->
                            <td><a href="{$artdecordeeplinkprefix}decor-issues--{$projectPrefix}?id={$issue/@id}">html</a></td>
                            <td>{tokenize($issue/@id,'\.')[last()]}</td>
                            <td>{$issue/@displayName/string()}</td>
                            <td>{$decorTypes/IssueStatusCodeLifeCycle/enumeration[@value=$issue/@currentStatusCode]/label[@language=$language]/text()}</td>
                            <td>{$decorTypes/IssuePriority/enumeration[@value=$issue/@priority]/label[@language=$language]/text()}</td>
                            <td>{$decorTypes/IssueType/enumeration[@value=$issue/@type]/label[@language=$language]/text()}</td>
                            <td>{if ($issue[@lastDate castable as xs:dateTime]) then format-dateTime(xs:dateTime($issue/@lastDate),'[Y0001]-[M01]-[D01] [H01]:[m01]:[s01]') else $issue/@lastDate}</td>
                            <td>{$issue/@lastAssignment/string()}</td>
                            <td>
                            {
                                for $l in $issue/@currentLabels/tokenize(., '\s')
                                let $t                  := $decor/issues/labels/label[@code = $l]
                                let $selectedLabelCode  := $t/@code
                                let $selectedLabelColor := $t/@color
                                let $selectedLabelName  := $t/@name
                                let $selectedLabelDesc  := if ($t/desc[@language = $language]) then attribute title {$t/desc[@language = $language]} else ($t/@name)
                                order by $selectedLabelCode
                                return (
                                    <span style="border: 1px solid gray;" title="{$selectedLabelName,' - ', $selectedLabelDesc}">
                                        <span style="background-color: {$selectedLabelColor}; padding-left: 10px;">&#160;</span>
                                        <span style="background-color: white;">{concat('&#160;',$selectedLabelCode,'&#160;')}</span>
                                    </span>,' '
                                )
                            }
                            </td>
                        </tr>
                }
                </tbody>
            </table>
        </div>
    </body>
</html>
        )
    )