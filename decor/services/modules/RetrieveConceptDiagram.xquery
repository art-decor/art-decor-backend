xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace dsapi       = "http://art-decor.org/ns/api/dataset" at "../../../api/modules/dataset-api.xqm";
import module namespace scapi       = "http://art-decor.org/ns/api/scenario" at "../../../api/modules/scenario-api.xqm";
import module namespace errors      = "http://e-editiones.org/roaster/errors";

(:
    function takes a DECOR concept hierarchy as argument and returns
    the concept hierarchie as a set of nested svg:g elements 
:)

let $format                     := if (request:exists()) then request:get-parameter('format','svg')[string-length()>0] else ('svg')
let $version                    := if (request:exists()) then request:get-parameter('version',())[string-length()>0] else ()
let $datasetId                  := if (request:exists()) then request:get-parameter('datasetId',())[string-length()>0] else ()
let $datasetEffectiveDate       := if (request:exists()) then request:get-parameter('datasetEffectiveDate',())[string-length()>0] else ()
let $transactionId              := if (request:exists()) then request:get-parameter('transactionId',())[string-length()>0] else ()
let $transactionEffectiveDate   := if (request:exists()) then request:get-parameter('transactionEffectiveDate',())[string-length()>0] else ()
(: our regular point of entry is RetrieveConceptDiagram, but as soon as the API generates the url in the svg, it switches syntax. id + effectiveDate = dataset in the API :)
let $conceptId                  := if (request:exists()) then (request:get-parameter('id',()), request:get-parameter('conceptId',()))[string-length()>0][1] else ()
let $conceptEffectiveDate       := if (request:exists()) then (request:get-parameter('effectiveDate',()), request:get-parameter('conceptEffectiveDate',()))[string-length()>0][1] else ()
let $language                   := if (request:exists()) then request:get-parameter('language',())[string-length()>0] else ()
let $filter                     := if (request:exists()) then request:get-parameter('filter',())[string-length()>0] else ()
let $interactive                := if (request:exists()) then not(request:get-parameter('interactive',())[string-length()>0] = 'false') else ()

return 
    if (empty($datasetId)) then
        if (empty($transactionId)) then (
            error($errors:BAD_REQUEST, 'Parameter datasetId or transactionId is required, without datasetEffectiveDate or transactionEffectiveDate you get the latest version')
        )
        else (
            scapi:getTransactionDiagram(map {
                "parameters": map {
                    "conceptId": $conceptId,
                    "conceptEffectiveDate": $conceptEffectiveDate,
                    "release": $version,
                    "language": $language,
                    "filter" : $filter,
                    "interactive": $interactive,
                    "format": $format,
                    "id": $transactionId,
                    "effectiveDate": $transactionEffectiveDate
                    }
                }
            )
        )
    else ( 
        dsapi:getDatasetDiagram(map {
            "parameters": map {
                "conceptId": $conceptId,
                "conceptEffectiveDate": $conceptEffectiveDate,
                "release": $version,
                "language": $language,
                "filter" : $filter,
                "interactive": $interactive,
                "format": $format,
                "id": $datasetId,
                "effectiveDate": $datasetEffectiveDate
                }
            }
        )
    )



