xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../../art/api/api-server-settings.xqm";
import module namespace templ       = "http://art-decor.org/ns/decor/template" at "../../../art/api/api-decor-template.xqm";
import module namespace i18n        = "http://art-decor.org/ns/decor/i18n" at "../../../art/api/api-decor-i18n.xqm";
(:import module namespace getf        = "http://art-decor.org/ns/fhir-settings" at "../../../fhir/3.0/api/fhir-settings.xqm";:)

declare namespace http              = "http://expath.org/ns/http-client";
declare namespace f                 = "http://hl7.org/fhir";

declare option exist:serialize "method=xhtml indent=no media-type=application/xhtml+html";
declare option exist:timeout "90000";

(:declare option exist:serialize "method=xml media-type=text/xml omit-xml-declaration=no indent=no";:)
(:declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=no indent=no 
        doctype-public=-//W3C//DTD&#160;XHTML&#160;1.0&#160;Transitional//EN
        doctype-system=http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd";:)

declare variable $useLocalAssets        := if (request:exists()) then request:get-parameter('useLocalAssets','false') else 'false';
declare variable $download              := if (request:exists()) then request:get-parameter('download','false') else '';
(:When called with localAssets=true then we need relative local paths, else 
    we need our server services URL, but with matching scheme (http or https)
    If the scheme is https and servicesUrl is http this leads to a security 
    problem in some browsers
:)
declare variable $resourcePath          := if ($download='true') then ('https://assets.art-decor.org/ADAR/rv/assets') else if ($useLocalAssets = 'true') then ('../assets') else ('../core/assets');

(: Set boolSerialize for template api as our actions here require serialize desc and constraint nodes :)
declare variable $boolSerialize         := false();
declare variable $docMessages           := i18n:getMessagesDoc('decor/services');

(: main proc :)
let $debug            := true()
let $dodev            := if (request:exists()) then request:get-parameter('dev','false')='true' else (true())
let $seetype          := if (request:exists()) then request:get-parameter('seetype','live-services') else ('live-services')
let $format           := if (request:exists() and string-length(request:get-parameter('format','')[1])>0) then request:get-parameter('format','html')[1] else ('html')
let $language         := if (request:exists() and string-length(request:get-parameter('language','')[1])>0) then request:get-parameter('language',$get:strArtLanguage)[1] else ($get:strArtLanguage)
(:let $language         := if ($language = ('en-US', 'de-DE', 'nl-NL')) then $language else 'en-US':)

let $id               := if (request:exists() and string-length(request:get-parameter('id',())[1])>0) then request:get-parameter('id',())[1] else ()
let $name             := if (request:exists() and string-length(request:get-parameter('name',())[1])>0) then request:get-parameter('name',())[1] else ()
let $ref              := if (request:exists() and string-length(request:get-parameter('ref',())[1])>0) then request:get-parameter('ref',())[1] else ()

let $effectiveDate    := if (request:exists() and string-length(request:get-parameter('effectiveDate',())[1])>0) then request:get-parameter('effectiveDate',())[1] else ()
let $projectPrefix    := if (request:exists() and string-length(request:get-parameter('prefix',())[1])>0) then request:get-parameter('prefix',())[1] else ()

let $projectVersion   := if (request:exists() and string-length(request:get-parameter('version',())[1])>0) then request:get-parameter('version',())[1] else ()

let $htmlInline       := if (request:exists() and string-length(request:get-parameter('inline',())[1])>0) then request:get-parameter('inline',())[1] else ()
let $displayHeader    := if ($htmlInline='true') then false() else true()

let $switchDoTreeTable      := if (request:exists()) then request:get-parameter('collapsable','true')='true' else (true())

let $artdecordeeplinkprefix := adserver:getServerURLArt()      

let $decor            := $get:colDecorData/decor[project/@prefix=$projectPrefix]
(:
    the folllowing two calls will be API calls to FHIR apis soon.
    
    Hard coded Accept header may turn out tricky, but relying on one particular FHIR server for this variable is even more tricky
:)
let $requestHeaders         := 
    <http:request method="GET">
        <!--<http:header name="Accept" value="{$getf:CT_FHIR_XML}"/>-->
        <http:header name="Accept" value="application/fhir+xml"/>
        <http:header name="Content-Type" value="text/xml"/>
        <http:header name="Cache-Control" value="no-cache"/>
        <http:header name="Max-Forwards" value="1"/>
    </http:request>

(: this is the structur definition to get :)
let $sds              := $decor//rules/structuredefinition[@id = $ref]

(: see if it is in cache :)
let $cache := $get:colDecorCache//cachedFHIRRepositories

let $cachedsd := 
    if ($cache/cacheme[@bbrurl=$sds/@publicationUrl][@bbrident=$sds/@referencedFrom]) then (
        $cache/cacheme[@bbrurl=$sds/@url][@bbrident=$sds/@referencedFrom]//f:Bundle/f:entry/f:resource/f:StructureDefinition
    ) else ()

(: let us do the first check, on failure don't continue :)
let $firsttrial       :=
    try {
        http:send-request($requestHeaders, ($sds/@publicationUrl)[1])
    } catch * {
        <error>{i18n:getMessage($docMessages,'getError',$language, ($sds)[1]/@publicationUrl)}</error>
    }
let $server-status    := 
    if ($firsttrial/@status) 
    then xs:integer($firsttrial/@status)
    else if ($firsttrial) 
    then (200)
    else (404)

let $firstcheck       :=
    if ($server-status=200)
    then () (: error(QName('http://art-decor.org/ns/fhir/error','ResourceNotCurrent1'), concat($server-status, fn:serialize($firsttrial,()))) :)
    else <error>{i18n:getMessage($docMessages,'getError',$language, $server-status, ($sds)[1]/@publicationUrl)}</error>
    
let $expndstrucdef    :=
    if ($firstcheck)
    then ()
    else 
    for $s in $sds
    let $url := $s/@publicationUrl
    return 
        <structuredefinition>
        {
            $s/@*
        }
        {
            if ($cachedsd) then (
                let $dummy := 0
                return
                    $cachedsd
            ) else (
                let $server-response        := http:send-request($requestHeaders, $url)
                let $strucdefs              := $server-response//f:StructureDefinition
                return
                    $strucdefs
            )
        }
        {
            for $c in $s/concept
            return
                <concept>
                {
                    $c/@*,
                    $c/name,
                    for $cc in art:getConcept($c/@ref, $c/@effectiveDate)
                    return  
                        <concept>
                        {
                            $cc/@*,
                            attribute {'shorthandId'} {art:getNameForOID($cc/@id, $language, $projectPrefix )},
                            attribute {'datasetId'} {$cc/ancestor::dataset/@id},
                            attribute {'datasetEffectiveDate'} {$cc/ancestor::dataset/@effectiveDate},
                            attribute {'datasetVersionLabel'} {$cc/ancestor::dataset/@versionLabel},
                            attribute {'datasetName'} {if ($cc/ancestor::dataset/name[@language = $language]) then $cc/ancestor::dataset/name[@language = $language] else $cc/ancestor::dataset/name[1]},
                            attribute {'prefix'} {$cc/ancestor::decor/project/@prefix},
                            $cc/name
                        }
                        </concept>
                }
                </concept>
        }
        </structuredefinition>
    
let $rawstrucdef      := $expndstrucdef (: for now the same :)


let $xsltParameters   :=
    <parameters>
        <param name="projectDefaultLanguage"    value="{if (string-length($language)>0 or $decor/project/name[@language = $language]) then $language else $decor/project/@defaultLanguage}"/>
        <param name="artdecordeeplinkprefix"    value="{$artdecordeeplinkprefix}"/>
        <param name="seeThisUrlLocation"        value="{$seetype}"/>
        <param name="displayHeader"             value="{$displayHeader}"/> 
        <param name="rand"                      value="128"/> 
        <param name="logLevel"                  value="'OFF'"/>
        <param name="switchCreateTreeTableHtml" value="{$switchDoTreeTable}"/>
    </parameters>

let $xslt := 
    if ($dodev) then 
        xs:anyURI('https://assets.art-decor.org/ADAR-dev/rv/StructureDefinition2html.xsl')
    else (
        xs:anyURI(concat('xmldb:exist://', $get:strDecorCore, '/StructureDefinition2html.xsl'))
        (:xs:anyURI('https:/assets.art-decor.org/ADAR/rv/StructureDefinition2html.xsl'):)
    )

return
    if ($firstcheck) then (
        response:set-header('Content-Type','text/html'),
        <h3 style="font-family: Verdana, Arial, sans-serif; font-size: 12px; font-weight: bold; background-color: #ece9e4; padding-top: 4px; padding-bottom: 4px; padding-left: 8px; color: #7a6e62; width: auto;">{$firstcheck}</h3>
    )
    else if ($format = 'xml') then (
        if (request:exists()) then 
            response:set-header('Content-Type','text/xml')
        else ''
        ,
        $rawstrucdef
    ) else (
        if (request:exists()) then
            response:set-header('Content-Type','text/html')
        else (),
        let $collapseString     := try { i18n:getMessage($docMessages,'Collapse',$language) } catch * {'Collapse'}
        let $expandString       := try { i18n:getMessage($docMessages,'Expand',$language) } catch * {'Expand'}
        let $logo                       := 
            if ($projectPrefix[string-length()>0]) then (
                concat('ProjectLogo?prefix=',$projectPrefix[string-length()>0][1],'&amp;version=',encode-for-uri($projectVersion[1]))
            ) else (
                let $server-logo    := adserver:getServerLogo()
                return if (starts-with($server-logo, 'http')) then $server-logo else concat('/art-decor/img/', $server-logo)
            )
        let $url                := if ($projectPrefix[string-length()>0]) then ($get:colDecorData//decor/project[@prefix=$projectPrefix]/reference/@url) else ()
        let $headerGoTo         := i18n:getMessage($docMessages,'goTo',$language)
        let $vscount            := count($expndstrucdef)
        
        return
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{substring($language,1,2)}" lang="{substring($language,1,2)}">
            <head>
                <meta http-equiv="Content-Type" content="text/xhtml; charset=utf-8"/>
                <title>{($expndstrucdef/@displayName)[1]/string()}</title>
                {
                    if ($switchDoTreeTable) then (
                        <!--{if ($collapsed) then <script type="text/javascript">window.treeTableCollapsed=true;</script> else <script type="text/javascript">window.treeTableCollapsed=false;</script>}--> |
                        <!--link href="{$resourcePath}/css/retrieve-template.css" rel="stylesheet" type="text/css"></link-->| 
                        <script type="text/javascript" xmlns="http://www.w3.org/1999/xhtml">
                            window.treeTableCollapsed = true;
                            window.treeTableStringCollapse = '{$collapseString}';
                            window.treeTableStringExpand = '{$expandString}';
                            window.treeTableColumn = 0;
                        </script> |
                        <script src="{$resourcePath}/scripts/jquery-1.11.3.min.js" type="text/javascript"></script> |
                        <script src="{$resourcePath}/scripts/jquery.treetable.js" type="text/javascript"></script> |
                        <script src="{$resourcePath}/scripts/jquery.easytabs.js" type="text/javascript"></script> |
                        <script src="{$resourcePath}/scripts/jquery.hashchange.min.js" type="text/javascript"></script> |
                        <script src="{$resourcePath}/scripts/retrieve-transaction.js" type="text/javascript"></script> |
                        <!--<script src="{$resourcePath}/scripts/jquery.cookie.js" type="text/javascript"></script>--> |
                        <!--{if ($draggable) then <script src="{$resourcePath}/scripts/dragtable.js" type="text/javascript"></script> else ()}--> |
                        <link href="{$resourcePath}/decor.css" rel="stylesheet" type="text/css"></link> |
                        <style type="text/css">body {{ background-color: white; }}</style>
                    ) else (
                        <link href="{$resourcePath}/decor.css" rel="stylesheet" type="text/css"></link>
                    )
                }
                {
                <script type="text/javascript" xmlns="http://www.w3.org/1999/xhtml">
                     <![CDATA[ $(document).ready( function() { $('#tab-container').easytabs(); }); ]]>
                </script>,
                <style type="text/css">
                <![CDATA[
                   .etabs { margin: 0; padding: 0; border: solid #96bf0b; border-width: 0 0 3px; margin: 0 0.16em 7px 0;
padding: 0;
zoom: 1;

}
                   .tab { display: inline-block; zoom:1; *display:inline; background: #eee;
                   /*-moz-border-radius: 4px 4px 0 0; -webkit-border-radius: 4px 4px 0 0; */ 
margin-right: 3px;
border: solid #a3a3a3;
border-width: 1px 1px 0;
border-bottom: none; 
background: #d8d8d8 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAABRCAIAAABKXIFMAAAAAXNSR0IArs4c6QAAAAZ0Uk5TAIIAkQC0lV1BMQAAAHlJREFUeJzszEEKggAABdF//xOKFGGRhBiapKGFunc/0GKGWb9sWJE+0CsWSf+w8sXKgpUZK9P0gc57HKHzGgbodH0PnbZ9QufRNNC51zV0qusNOqfzBTpFUUJLS0tLS0tLS0tLS0tLS0tLS0tLS0v/Bb0DAAD//wMAo87Weyx0YLgAAAAASUVORK5CYII=) repeat-x;
                   }
                   .tab a { font-size: 1.0em; line-height: 2em; display: block; padding: 0 10px; outline: none; 
color: #000;
position: relative;
text-decoration: none;}
                   .tab a:hover { text-decoration: none; background: #bfdaff url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAABPCAIAAABzgOKnAAAAAXNSR0IArs4c6QAAAAZ0Uk5TAIIAkQC0lV1BMQAAAItJREFUeJzszDEKg1AABNG9/6VsAiksQgoRJGJQ9BuIol5gc4N00+0w9ZOxtF6GJulyGVrlNLSW09AkPR+G1nQYWuNuaL03Q2vYDK3X19DqPoZWuxpaTTG0nouh9ZgMrXo0tO6DoXXrDa2qM3To0KFDhw4dOnTo0KFDhw4dOnTo0KFD//kHAAD//wMAOUd7qQjDY2UAAAAASUVORK5CYII=) repeat-x; outline: 0;}
                   .tab.active { background: #96bf0b; padding-top: 6px; position: relative; top: 1px; border-color: #96bf0b; }
                   .tab.active a:hover { background: #96bf0b;}
                   .tab a.active { color: #fff; font-weight: bold; }
                   .tab-container .panel-container { background: #fff; border: solid #666 1px; padding: 10px; -moz-border-radius: 0 4px 4px 4px; -webkit-border-radius: 0 4px 4px 4px; }
                   .panel-container { margin-bottom: 10px; }                   
                   ]]>
                  </style>
                }
                {
                <style type="text/css">
                <![CDATA[
                    img.hierarchy {
                    border: 0px;
                    padding: 0px;
                    vertical-align: top;
                    }
                    td.conf-label{
                    color: #564b4b;
                    border: 1px solid #cfcbcb;
                    background-color: #fef0eb;
                    margin: 4px;
                    padding: 2px;
                    }
                    td.fixed-label{ 
                    color: #564b4b;
                    border: 1px solid #cfcbcb;
                    background-color: #fcf4c9;
                    margin: 4px;
                    padding: 2px
                    }
                    td.binding-label{
                    color: #564b4b;
                    border: 1px solid #cfcbcb;
                    background-color: #ebf8fe;
                    margin: 4px;
                    padding: 2px;
                    }
                    td.url-label{
                    color: #564b4b;
                    border: 1px solid #cfcbcb;
                    background-color: #fcf4c9;
                    margin: 4px;
                    padding: 2px
                    }
                    ]]>
                </style>
                }
             </head>
            <body>
            { 
                if ($displayHeader) then (
                    <table width="100%">
                        <tr>
                            <td align="left">
                                <h1>
                                {
                                    if ($vscount = 1) then 
                                        i18n:getMessage($docMessages,'StructureDefinition',$language) 
                                    else
                                        i18n:getMessage($docMessages,'StructureDefinitions',$language)
                                }
                                </h1>
                            </td>
                            <td align="right"> 
                            {if ($logo and $url) then 
                                <a href="{$url}">
                                    <img src="{$logo}" alt=""  title="{$url}" height="50px"/>
                                </a>
                             else if ($logo) then
                                <img src="{$logo}" alt="" height="50px"/>
                             else ()
                            }
                            </td>
                        </tr>
                        <!--
                        <tr>
                            <td colspan="2">
                                {$headerGoTo}
                                <a href="TemplateIndex?prefix={$projectPrefix}{if (not(empty($projectVersion))) then concat('&amp;version=',$projectVersion) else ()}&amp;language={$language}" alt="">index</a> 
                            </td>
                        </tr>
                        -->
                    </table>
                )
                else()
            }
            {
                if ($expndstrucdef)
                then 
                    try {
                        transform:transform($expndstrucdef, $xslt, $xsltParameters)
                    } catch * {
                        <h3>{i18n:getMessage($docMessages,'transformError',$language, $xslt)}</h3>
                    }
                else ()
            }
            </body>
        </html>
    ) (: html :)