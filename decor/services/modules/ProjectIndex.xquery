xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace art         = "http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace vs          = "http://art-decor.org/ns/decor/valueset" at "../../../art/api/api-decor-valueset.xqm";
import module namespace cs          = "http://art-decor.org/ns/decor/codesystem" at "../../../art/api/api-decor-codesystem.xqm";
import module namespace templ       = "http://art-decor.org/ns/decor/template" at "../../../art/api/api-decor-template.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../api/api-server-settings.xqm";
import module namespace i18n        = "http://art-decor.org/ns/decor/i18n" at "../../../art/api/api-decor-i18n.xqm";
import module namespace mpapi       = "http://art-decor.org/ns/api/conceptmap" at "../../../api/modules/conceptmap-api.xqm";

declare option exist:serialize "method=xhtml media-type=text/html";

declare variable $artServerUrl          := adserver:getServerURLArt();
(:declare variable $artServerUrl3         := replace($artServerUrl, '/art-decor.*$', '/ad/#/');:)
declare variable $artServerUrl3         := adserver:getServerURLArt3();
declare variable $artDeepLinkServices   := adserver:getServerURLServices();
declare variable $useLocalAssets        := if (request:exists()) then request:get-parameter('useLocalAssets','false') else 'false';
declare variable $download              := if (request:exists()) then request:get-parameter('download','false') else '';
(:When called with localAssets=true then we need relative local paths, else 
    we need our server services URL, but with matching scheme (http or https)
    If the scheme is https and servicesUrl is http this leads to a security 
    problem in some browsers
:)
declare variable $resourcePath          := if ($download='true') then ('https://assets.art-decor.org/ADAR/rv/assets') else if ($useLocalAssets = 'true') then ('../assets') else ('../core/assets');

declare variable $strMessages           := 'decor/services';
(:declare variable $docMessages           := i18n:getMessagesDoc($strMessages);:)
let $decorSchemaTypes       := art:getDecorTypes()

let $projectPrefix          := if (request:exists()) then request:get-parameter('prefix',())[string-length()>0][1] else ()
let $projectVersion         := if (request:exists() and string-length($projectPrefix) gt 0) then request:get-parameter('version',())[string-length()>0][1] else ()
(:project content language:)
let $proj-lang              := if (request:exists()) then request:get-parameter('language',())[string-length()>0][1] else ()
(:interface language:)
let $ui-lang                := if (request:exists()) then request:get-parameter('ui',$get:strArtLanguage)[string-length()>0][1] else ($get:strArtLanguage)

(: optionally filters the output of ProjectIndex. Supported d for 'datasets', t for 'transactions', v for 'valuesets', r for 'templates/rules', q for 'questionnaires'
    view is the positive statement, i.e. show only these
    filter is the negative statement, i.e. show everything but these
   Added view, because positive statements seem more natural, but left filter in, because of uknown compatibility problems upon removal
   Note that filter to date is an undocumented feature...
:)
let $view                   := if (request:exists()) then request:get-parameter('view',())[string-length()>0][1] else ()
let $filter                 := if (request:exists()) then request:get-parameter('filter',())[string-length()>0] else ()  

(: these four are currently used only for valuesets :)
let $id                     := if (request:exists()) then (request:get-parameter('id',())[string-length()>0][1]) else ()
let $name                   := if (request:exists()) then (request:get-parameter('name',())[string-length()>0][1]) else ()
let $useRegexMatching       := if (request:exists()) then (request:get-parameter('regex','false')[string-length()>0][1]='true') else (false())
let $effectiveDate          := if (request:exists()) then (request:get-parameter('effectiveDate',())[string-length()>0][1]) else ()
let $format                 := if (request:exists() and request:get-parameter('format','html')[string-length()>0]) then (request:get-parameter('format','html')[string-length()>0][1]) else ('html')
let $includeArtefacts       := if (request:exists()) then not(request:get-parameter('includeArtefacts','true')[string-length()>0][1]='false') else (false())

(: get filtered projects for results :)
let $projects               := art:getDecorByPrefix($projectPrefix, $projectVersion, $proj-lang)

let $fhirServerBase         := adserver:getServerURLFhirServices()
let $installedFhirVersions  := adserver:getInstalledFhirServices()
let $fhirVersions           := $projects/project/restURI[@for = 'FHIR'][@format = $installedFhirVersions]/@format
let $doFHIRlinks            := exists($fhirVersions)

let $proj-lang           := 
    if ($projects/project/name[@language=$proj-lang] or $proj-lang = '*') 
    then ($proj-lang) 
    else if ($projects) 
    then ($projects/project/@defaultLanguage/string())[1]
    else ($get:strArtLanguage)
    
let $versions       := 
    if (string-length($projectPrefix) = 0) then (
        (:cannot process version without prefix so only live data by default:)
    )
    else (
        for $project in $get:colDecorVersion//project[@prefix = $projectPrefix][ancestor::decor[@versionDate][@language]]
        let $projectPrefix      := $project/@prefix
        let $versionLanguage    := $project/../@language
        let $versionDate        := $project/../@versionDate
        let $versionLabel       := if ($project/../@versionLabel) then $project/../@versionLabel else ($project/release[@date = $versionDate]/@versionLabel | $project/version[@date = $versionDate]/@versionLabel)
        order by lower-case($versionDate) descending, $versionLanguage
        return
            <project prefix="{$projectPrefix}" versionDate="{$versionDate}" language="{$versionLanguage}">
            {
                if ($versionLabel[string-length()>0]) then attribute versionLabel {$versionLabel} else (),
                attribute modifieddate {xmldb:last-modified(util:collection-name($project),util:document-name($project))}
            }
            </project>
    )

(: get all projects for drop-down population :)
let $allProjects    := 
    if (string-length($projectPrefix) gt 0 and $format = ('xml', 'json')) then () else (
        for $decor in ($get:colDecorData/decor[not(@private = 'true')] | art:getDecorByPrefix($projectPrefix))
        let $name               := $decor/project/name[@language = $proj-lang]
        let $name               := if (empty($name)) then $decor/project/name[1] else $name
        return
            <project>
            {
                $decor/project/@id,
                $decor/project/@prefix,
                $decor/project/@defaultLanguage,
                attribute repository {$decor/@repository='true'},
                attribute private {$decor/@private='true'},
                attribute experimental {$decor/project/@experimental='true'},
                attribute name {$name},
                attribute creationdate {xmldb:created(util:collection-name($decor),util:document-name($decor))},
                attribute modifieddate {xmldb:last-modified(util:collection-name($decor),util:document-name($decor))}
            }
            </project>
    )
    
let $title        := 
    if ($format = ('xml', 'json')) then 
        ()
    else if (contains($view,'d') and string-length($view)=1 and empty($filter)) then
        if (empty($projectPrefix)) then (i18n:getMessage($strMessages,'titleDatasetIndex',$ui-lang)) else (i18n:getMessage($strMessages,'titleDatasetIndex',$ui-lang, $allProjects[@prefix=$projectPrefix]/@name))
    else if (contains($view,'r') and string-length($view)=1 and empty($filter)) then
        if (empty($projectPrefix)) then (i18n:getMessage($strMessages,'titleTemplateIndex',$ui-lang)) else (i18n:getMessage($strMessages,'titleTemplateIndex',$ui-lang, $allProjects[@prefix=$projectPrefix]/@name))
    else if (contains($view,'t') and string-length($view)=1 and empty($filter)) then
        if (empty($projectPrefix)) then (i18n:getMessage($strMessages,'titleTransactionIndex',$ui-lang)) else (i18n:getMessage($strMessages,'titleTransactionIndex',$ui-lang, $allProjects[@prefix=$projectPrefix]/@name))
    else if (contains($view,'q') and string-length($view)=1 and empty($filter)) then
        if (empty($projectPrefix)) then (i18n:getMessage($strMessages,'titleQuestionnaireIndex',$ui-lang)) else (i18n:getMessage($strMessages,'titleQuestionnaireIndex',$ui-lang, $allProjects[@prefix=$projectPrefix]/@name))
    else if (contains($view,'v') and string-length($view)=1 and empty($filter)) then
        if (empty($projectPrefix)) then (i18n:getMessage($strMessages,'titleValueSetIndex',$ui-lang)) else (i18n:getMessage($strMessages,'titleValueSetIndex',$ui-lang, $allProjects[@prefix=$projectPrefix]/@name))
    else if (contains($view,'c') and string-length($view)=1 and empty($filter)) then
        if (empty($projectPrefix)) then (i18n:getMessage($strMessages,'titleCodeSystemIndex',$ui-lang)) else (i18n:getMessage($strMessages,'titleCodeSystemIndex',$ui-lang, $allProjects[@prefix=$projectPrefix]/@name))
    else if (contains($view,'m') and string-length($view)=1 and empty($filter)) then
        if (empty($projectPrefix)) then (i18n:getMessage($strMessages,'titleConceptMapIndex',$ui-lang)) else (i18n:getMessage($strMessages,'titleConceptMapIndex',$ui-lang, $allProjects[@prefix=$projectPrefix]/@name))
    else (
        if (empty($projectPrefix)) then (i18n:getMessage($strMessages,'titleProjectIndex',$ui-lang)) else (i18n:getMessage($strMessages,'titleProjectIndex',$ui-lang, $allProjects[@prefix=$projectPrefix]/@name))
    )
let $logo         := 
    if ($format = ('xml', 'json')) then 
        ()
    else if (count($projects)=1) then (
        concat('ProjectLogo?prefix=',$projects/project/@prefix,'&amp;version=',encode-for-uri($projectVersion[1]))
    ) else (
        let $server-logo := adserver:getServerLogo()
        return
        if (starts-with($server-logo, 'http')) then $server-logo else concat('/art-decor/img/', $server-logo)
    )
let $url          :=  if (count($projects)=1) then ($projects/project/@url) else ()

return 
    if ($format='xml' or $format='json') then (
        (:note: this output supports the BBR config in server-settings:)
        if (empty($projectPrefix) and empty($view) and empty($filter)) then (
            response:set-status-code(200),
            response:set-header('Content-Type','text/xml'),
            <return>
            {
                $allProjects
            }
            </return>
        )
        else 
        if (empty($projects)) then (
            response:set-status-code(404),
            response:set-header('Content-Type','text/xml'),
            <error>{i18n:getMessage($strMessages,'errorRetrieveProjectNoResults',$ui-lang),' ',if (request:exists()) then request:get-query-string() else()}</error>
        )
        (:note: this output supports the BBR index for template and valueset ref/resolving:)
        else (
            let $view := if ($includeArtefacts) then $view else ''
            let $xml  := 
                <return prefix="{$projectPrefix}" versionDate="{$projectVersion}" versionLabel="{($versions[@prefix=$projectPrefix][@versionDate=$projectVersion])[1]/@versionLabel}">
                {
                    comment { 'Versions of this project' },
                    $versions
                    ,
                    if (empty($view) or contains($view,'d')) then (
                        if (contains($filter, 'd')) then () else (
                            comment { 'Datasets of this project' },
                            for $datasets in $projects//dataset
                            let $dsid           := $datasets/@id
                            let $latestById     := art:getDataset($dsid, ())
                            group by $dsid
                            for $dataset in $datasets
                            let $datasetPrefix  := $dataset/ancestor::decor/project/@prefix
                            let $latestVersion  := $latestById/@effectiveDate = $dataset/@effectiveDate
                            order by $dataset/@effectiveDate
                            return 
                                <dataset>
                                {
                                    $dataset/@id,
                                    $dataset/@ref,
                                    attribute name {if ($dataset/name[@language = $proj-lang]) then $dataset/name[@language = $proj-lang][1] else $dataset/name[1]},
                                    attribute displayName {if ($dataset/name[@language = $proj-lang]) then $dataset/name[@language = $proj-lang][1] else $dataset/name[1]},
                                    $dataset/@statusCode,
                                    $dataset/@effectiveDate,
                                    $dataset/@officialReleaseDate,
                                    $dataset/@expirationDate,
                                    $dataset/@versionLabel,
                                    attribute url {$artDeepLinkServices},
                                    attribute ident {$dataset/ancestor::decor/project/@prefix},
                                    $dataset/@canonicalUri,
                                    $dataset/@lastModifiedDate,
                                    $dataset/name,
                                    $dataset/desc
                                    ,
                                    <usage>
                                    {
                                        for $rt in $get:colDecorData//representingTemplate[@sourceDataset = $dataset/@id]
                                        return
                                            if ($rt[@sourceDatasetFlexibility = $dataset/@effectiveDate] | $rt[$latestVersion][empty(@sourceDatasetFlexibility)] | $rt[$latestVersion][@sourceDatasetFlexibility = 'dynamic']) then
                                                <transaction>
                                                {
                                                    $rt/../@*, 
                                                    if ($rt/ancestor::decor/project[@prefix = $datasetPrefix]) then () else (
                                                        attribute url {$artDeepLinkServices},
                                                        attribute ident {$rt/ancestor::decor/project/@prefix}
                                                    ), 
                                                    $rt/../name,
                                                    <representingTemplate>{$rt/@*}</representingTemplate>
                                                }
                                                </transaction>
                                            else ()
                                    }
                                    </usage>
                                }
                                </dataset>
                        )
                    ) else ()
                }
                {
                    if (empty($view) or contains($view,'t')) then (
                        if (contains($filter, 't')) then () else (
                            comment { 'Transactions of this project' },
                            
                            let $dsmap      := map:merge(
                                for $rt in $projects//representingTemplate[@sourceDataset]
                                let $dsid   := $rt/@sourceDataset
                                let $dsed   := $rt/@sourceDatasetFlexibility[. castable as xs:dateTime]
                                
                                (: check compilation results inside current set first, so we get the published/compiled copy of the dataset :)
                                let $ds     := 
                                    if (empty($projectVersion)) then
                                        art:getDataset($dsid, $dsed)[1]
                                    else (
                                        if (empty($dsed)) then
                                            head(
                                                for $d in $projects//dataset[@id = $dsid]
                                                order by $d/@effectiveDate descending
                                                return $d
                                            )
                                        else (
                                            $projects//dataset[@id = $dsid][@effectiveDate = $dsed][1]
                                        )
                                    )
                                group by $dsid, $dsed
                                return
                                    map:entry($dsid || $dsed, $ds)
                            )
                            let $tmmap      := map:merge(
                                for $rt in $projects//representingTemplate[@ref]
                                let $tmid   := $rt/@ref
                                let $tmed   := $rt/@flexibility[. castable as xs:dateTime]
                                
                                (: check compilation results inside current set first, so we get the published/compiled copy of the template :)
                                let $tm     := 
                                    if (empty($projectVersion)) then
                                        templ:getTemplateById($tmid, ($tmed, 'dynamic')[1], $projects)//template/template[@id]
                                    else (
                                        if (empty($tmed)) then
                                            head(
                                                for $d in $projects//template[@id = $tmid]
                                                order by $d/@effectiveDate descending
                                                return $d
                                            )
                                        else (
                                            $projects//template[@id = $tmid][@effectiveDate = $tmed]
                                        )
                                    )
                                group by $tmid, $tmed
                                return
                                    map:entry($tmid || $tmed, $tm[1])
                            )
                            
                            for $transaction in $projects//transaction[representingTemplate]
                            order by $transaction/@effectiveDate
                            return 
                                <transaction>
                                {
                                    $transaction/@id,
                                    $transaction/@ref,
                                    attribute name {if ($transaction/name[@language = $proj-lang]) then $transaction/name[@language = $proj-lang][1] else $transaction/name[1]},
                                    attribute displayName {if ($transaction/name[@language = $proj-lang]) then $transaction/name[@language = $proj-lang][1] else $transaction/name[1]},
                                    $transaction/@statusCode,
                                    $transaction/@effectiveDate,
                                    $transaction/@officialReleaseDate,
                                    $transaction/@expirationDate,
                                    $transaction/@versionLabel,
                                    attribute url {$artDeepLinkServices},
                                    attribute ident {$transaction/ancestor::decor/project/@prefix},
                                    $transaction/@canonicalUri,
                                    $transaction/@lastModifiedDate,
                                    $transaction/name,
                                    $transaction/desc
                                    ,
                                    <uses>
                                    {
                                        for $rt in $transaction/representingTemplate[@sourceDataset]
                                        let $dsid   := $rt/@sourceDataset
                                        let $dsed   := $rt/@sourceDatasetFlexibility[. castable as xs:dateTime]
                                        let $ds     := map:get($dsmap, $dsid || $dsed)
                                        let $dspfx  := ($ds/@ident, $ds/ancestor::decor/project/@prefix)[1]
                                        return 
                                            <dataset>
                                            {
                                                $ds/@*[not(. = '')],
                                                if ($ds[@url | @ident]) then () else 
                                                if ($projects/project[@prefix = $dspfx]) then () else (  
                                                    attribute url {$artDeepLinkServices},
                                                    attribute ident {$dspfx}
                                                )
                                                ,
                                                $ds/name
                                            }
                                            </dataset>
                                        ,
                                        for $rt in $transaction/representingTemplate[@ref]
                                        let $tmid   := $rt/@ref
                                        let $tmed   := $rt/@flexibility[. castable as xs:dateTime]
                                        let $tm     := map:get($tmmap, $tmid || $tmed)
                                        return 
                                            <template>
                                            {
                                                $tm/@*,
                                                if ($tm[@url | @ident]) then () else
                                                if ($tm[@url = $artDeepLinkServices][@ident = $projects/project/@prefix]) then () else (
                                                    $tm/@url,
                                                    $tm/@ident
                                                )
                                            }
                                            </template>
                                    }
                                    </uses>
                                }
                                </transaction>
                        )
                    ) else ()
                }
                {
                    if (empty($view) or contains($view,'q')) then (
                        if (contains($filter, 'q')) then () else (
                            comment { 'Questionnaires of this project' },
                            
                            (:<relationship type="DRIV" ref="2.16.840.1.113883.3.1937.99.60.10.4.2" flexibility="2022-02-16T00:00:00"/>:)
                            let $trmap      := map:merge(
                                for $rt in $projects//questionnaire/relationship[@type = 'DRIV']
                                let $trid   := $rt/@ref
                                let $tred   := $rt/@flexibility[. castable as xs:dateTime]
                                
                                (: check compilation results inside current set first, so we get the published/compiled copy of the template :)
                                let $tr     := art:getTransaction($trid, ($tred, 'dynamic')[1], $projectVersion, ())
                                group by $trid, $tred
                                return
                                    map:entry($trid || $tred, $tr[1])
                            )
                            
                            for $transaction in $projects//questionnaire
                            order by $transaction/@effectiveDate
                            return 
                                <questionnaire>
                                {
                                    $transaction/@id,
                                    $transaction/@ref,
                                    attribute name {if ($transaction/name[@language = $proj-lang]) then $transaction/name[@language = $proj-lang][1] else $transaction/name[1]},
                                    attribute displayName {if ($transaction/name[@language = $proj-lang]) then $transaction/name[@language = $proj-lang][1] else $transaction/name[1]},
                                    $transaction/@statusCode,
                                    $transaction/@effectiveDate,
                                    $transaction/@officialReleaseDate,
                                    $transaction/@expirationDate,
                                    $transaction/@versionLabel,
                                    attribute url {$artDeepLinkServices},
                                    attribute ident {$transaction/ancestor::decor/project/@prefix},
                                    $transaction/@canonicalUri,
                                    $transaction/@lastModifiedDate,
                                    $transaction/name,
                                    $transaction/desc,
                                    let $trxs       :=
                                        for $rt in $projects//questionnaire/relationship[@type = 'DRIV']
                                        let $trid   := $rt/@ref
                                        let $tred   := $rt/@flexibility[. castable as xs:dateTime]
                                        group by $trid, $tred
                                        return
                                            map:get($trmap, $trid || $tred)
                                    return
                                        if ($trxs) then
                                            <derived-from>
                                            {
                                                for $trx in $trxs
                                                let $trxpfx := ($trx/@ident, $trx/ancestor::decor/project/@prefix)[1]
                                                return 
                                                    <transaction>
                                                    {
                                                        $trx/@*[not(. = '')],
                                                        if ($trx[@url | @ident]) then () else 
                                                        if ($projects/project[@prefix = $trxpfx]) then () else (  
                                                            attribute url {$artDeepLinkServices},
                                                            attribute ident {$trxpfx}
                                                        )
                                                        ,
                                                        $trx/name
                                                    }
                                                    </transaction> 
                                            }
                                            </derived-from>
                                        else ()
                                }
                                </questionnaire>
                        )
                    ) else ()
                }
                {
                    if (empty($view) or contains($view,'v')) then (
                        if (contains($filter, 'v')) then () else (
                            comment { 'ValueSets of this project' },
                            let $valueSets := vs:getValueSetList($id,$name,$effectiveDate,$projectPrefix,$projectVersion)
                            return
                            for $valueSet in $valueSets/*[empty(@url)]/valueSet
                            let $prefix      := 
                                if ($valueSet/parent::*[@referencedFrom]) 
                                then tokenize($valueSet/parent::*/@referencedFrom,' ')[1] 
                                else $valueSet/parent::*/@ident
                            order by $valueSet/@displayName, $valueSet/@effectiveDate
                            return 
                                <valueSet>
                                {
                                    $valueSet/@id,
                                    $valueSet/@ref,
                                    $valueSet/@name,
                                    attribute displayName {($valueSet/@displayName, $valueSet/@name)[1]},
                                    $valueSet/@statusCode,
                                    $valueSet/@effectiveDate,
                                    $valueSet/@officialReleaseDate,
                                    $valueSet/@expirationDate,
                                    $valueSet/@versionLabel,
                                    attribute url {$artDeepLinkServices},
                                    attribute ident {$prefix},
                                    $valueSet/@canonicalUri,
                                    $valueSet/@lastModifiedDate,
                                    $valueSet/desc
                                }
                                </valueSet>
                        )
                    ) else ()
                }
                {
                    if (empty($view) or contains($view,'r')) then (
                        if (contains($filter, 'r')) then () else (
                            comment { 'Templates of this project' },
                            for $template in $projects/rules/template
                            order by $template/@displayName, $template/@effectiveDate
                            return 
                                <template>
                                {
                                    $template/@id,
                                    $template/@ref,
                                    $template/@name,
                                    attribute displayName {($template/@displayName, $template/@name)[1]},
                                    $template/@statusCode,
                                    $template/@effectiveDate,
                                    $template/@officialReleaseDate,
                                    $template/@expirationDate,
                                    $template/@versionLabel,
                                    $template/@isClosed,
                                    attribute url {$artDeepLinkServices},
                                    attribute ident {$template/ancestor::decor/project/@prefix},
                                    $template/@canonicalUri,
                                    $template/@lastModifiedDate,
                                    $template/desc,
                                    $template/classification
                                }
                                </template>
                        )
                    ) else ()
                }
                {
                    if (empty($view) or contains($view,'c')) then (
                        if (contains($filter, 'c')) then () else (
                            comment { 'CodeSystems of this project' },
                            let $codeSystems := cs:getCodeSystemList($id,$name,$effectiveDate,$projectPrefix,$projectVersion)
                            return
                            for $codeSystem in $codeSystems/*[empty(@url)]/codeSystem
                            let $prefix      := 
                                if ($codeSystem/parent::*[@referencedFrom]) 
                                then tokenize($codeSystem/parent::*/@referencedFrom,' ')[1] 
                                else $codeSystem/parent::*/@ident
                            order by $codeSystem/@displayName, $codeSystem/@effectiveDate
                            return 
                                <codeSystem>
                                {
                                    $codeSystem/@id,
                                    $codeSystem/@ref,
                                    $codeSystem/@name,
                                    attribute displayName {($codeSystem/@displayName, $codeSystem/@name)[not(. = '')][1]},
                                    $codeSystem/@statusCode,
                                    $codeSystem/@effectiveDate,
                                    $codeSystem/@officialReleaseDate,
                                    $codeSystem/@expirationDate,
                                    $codeSystem/@versionLabel,
                                    attribute url {$artDeepLinkServices},
                                    attribute ident {$codeSystem/ancestor::decor/project/@prefix},
                                    $codeSystem/@canonicalUri,
                                    $codeSystem/@lastModifiedDate,
                                    $codeSystem/desc
                                }
                                </codeSystem>
                        )
                    ) else ()
                }
                {
                    if (empty($view) or contains($view,'m')) then (
                        if (contains($filter, 'm')) then () else (
                            comment { 'ConceptMaps of this project' },
                            let $conceptMaps := mpapi:getConceptMapList((), $projectPrefix,$projectVersion, $proj-lang, (), true(), (), (), (), true(), map { "id": $id, "effectiveDate": $effectiveDate })
                            return
                            for $conceptMap in $conceptMaps/*[empty(@url)]/conceptMap
                            let $prefix      := 
                                if ($conceptMap/parent::*[@referencedFrom]) 
                                then tokenize($conceptMap/parent::*/@referencedFrom,' ')[1] 
                                else $conceptMap/parent::*/@ident
                            order by $conceptMap/@displayName, $conceptMap/@effectiveDate
                            return 
                                <conceptMap>
                                {
                                    $conceptMap/@id,
                                    $conceptMap/@ref,
                                    $conceptMap/@name,
                                    attribute displayName {($conceptMap/@displayName, $conceptMap/@name)[not(. = '')][1]},
                                    $conceptMap/@statusCode,
                                    $conceptMap/@effectiveDate,
                                    $conceptMap/@officialReleaseDate,
                                    $conceptMap/@expirationDate,
                                    $conceptMap/@versionLabel,
                                    attribute url {$artDeepLinkServices},
                                    attribute ident {$projectPrefix},
                                    $conceptMap/@canonicalUri,
                                    $conceptMap/@lastModifiedDate,
                                    $conceptMap/desc
                                }
                                </conceptMap>
                        )
                    ) else ()
                }
                </return>
            return 
                if ($format='json') then (
                    response:set-status-code(200),
                    response:set-header('Content-Type','application/json; charset=utf-8'),
                    let $xml  := 
                        element {name($xml)} {
                            $xml/@*, 
                            namespace {"json"} {"http://www.json.org"}, 
                            art:addJsonArrayToElements($xml/*)
                        }
                    
                    return
                        fn:serialize($xml,
                            <output:serialization-parameters xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization">
                                <output:method>json</output:method>
                                <output:encoding>UTF-8</output:encoding>
                            </output:serialization-parameters>
                        )
                )
                (: 'xml' :)
                else (
                    response:set-status-code(200),
                    response:set-header('Content-Type','text/xml'),
                    $xml
                )
        )
    )
    else (
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>{$title}</title>
        <link href="{$resourcePath}/css/default.css" rel="stylesheet" type="text/css"/>
        <script>
            function getPrefix() {{
                return document.getElementById('prefixSelector').options[document.getElementById('prefixSelector').selectedIndex].value;
            }};
            function getVersionLanguage() {{
                return document.getElementById('versionSelector').options[document.getElementById('versionSelector').selectedIndex].value;
            }}
            function getVersion() {{
                var versionLanguage = getVersionLanguage();
                var spaceIndex      = versionLanguage.indexOf(' ');
                var version;        var language;
                if (spaceIndex == -1)
                    version = versionLanguage
                else
                    version = versionLanguage.substr(0, spaceIndex)
                
                return version
            }};
            function getLanguage() {{
                var versionLanguage = getVersionLanguage();
                var spaceIndex      = versionLanguage.indexOf(' ');
                var language;
                if (spaceIndex == -1)
                    version = versionLanguage
                else
                    version = versionLanguage.substr(spaceIndex + 1, versionLanguage.length)
                
                return version
            }};
        </script>
    </head>
    <body>
        <table width="100%" id="top">
            <tbody>
                <tr>
                    <td align="left">
                        <h1>{$title}</h1>
                    </td>
                    <td align="right">
                    {if ($logo and $url) then 
                        <a href="{$url}">
                            <img src="{$logo}" alt="" title="{$url}" height="50px"/>
                        </a>
                     else if ($logo) then
                        <img src="{$logo}" alt="" height="50px"/>
                     else ()
                    }
                    </td>
                </tr>
                <tr>
                    <td>
                        <select id="prefixSelector" onchange="javascript:location.href=window.location.pathname+'?prefix='+getPrefix()+'&amp;format=html&amp;language='+getLanguage()+'&amp;ui={$ui-lang}'">
                        <option value="">{i18n:getMessage($strMessages,'SelectProject',$ui-lang)}</option>
                        {
                            for $project in $allProjects
                            order by $project/@repository, lower-case($project/@name)
                            return
                                <option value="{$project/@prefix/string()}">
                                {
                                    if ($project/@prefix=$projectPrefix) 
                                    then attribute {'selected'} {'true'} 
                                    else (),
                                    if ($project/@repository='true') 
                                    then '(BBR) '
                                    else (),
                                    $project/@name/string()
                                }
                                </option>
                        }
                        </select>
                        {
                            if ($versions[2]) then (
                                <select id="versionSelector" onchange="javascript:location.href=window.location.pathname+'?prefix='+getPrefix()+'&amp;version='+getVersion()+'&amp;format=html&amp;language='+getLanguage()+'&amp;ui={$ui-lang}'" style="display:{if (empty($projectPrefix)) then 'none' else ('inline-block')};">
                                <option value="">--{i18n:getMessage($strMessages,'columnLiveVersion',$ui-lang)}--</option>
                                {
                                    for $version at $i in $versions
                                    order by lower-case($version/@versionDate) descending
                                    return
                                        <option value="{string-join(($version/@versionDate, $version/@language), ' ')}">
                                        {
                                            if ($version[@versionDate = $projectVersion][@language = $proj-lang]) then
                                                attribute {'selected'} {'true'}
                                            else
                                            if (string-length($projectVersion) = 0) then (
                                                (:let $newestVersionDate  := max($versions[@versionDate castable as xs:dateTime]/xs:dateTime(@versionDate))
                                                let $latestVersions     := $versions[@versionDate = $newestVersionDate]
                                                let $latestLang         := ($latestVersions[@language = $proj-lang]/@language, $latestVersions/@language)[1]
                                                return
                                                if ($version[@versionDate = $newestVersionDate][@language = $latestLang]) then
                                                    attribute {'selected'} {'true'}
                                                else ():)
                                            ) else (),
                                            string-join((
                                                $version/@versionDate, 
                                                ' - ', 
                                                $version/@language, 
                                                if ($version/@versionLabel) then (' (', $version/@versionLabel, ')') else ()
                                            ), '')
                                        }
                                        </option>
                                }
                                </select>
                                ,
                                if ($projectVersion castable as xs:dateTime or empty($projectVersion)) then () else (
                                    <span style="margin-left: 1em;">{i18n:getMessage($strMessages,'columnDate',$ui-lang), format-dateTime(xs:dateTime($versions[@versionDate = $projectVersion][1]/@modifieddate), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')}</span>
                                )
                            ) else (
                                <select id="versionSelector" style="display: none;"><option value="" selected="true"/></select>
                            )
                        }
                    </td>
                    <td align="right">
                        <img src="/art-decor/img/flags/nl.png" onclick="location.href=window.location.pathname+'?prefix={$projectPrefix}{if (empty($projectVersion)) then () else concat('&amp;version=',$projectVersion)}&amp;format=html&amp;language={$proj-lang}&amp;ui=nl-NL';" class="linked flag"/>
                        <img src="/art-decor/img/flags/de.png" onclick="location.href=window.location.pathname+'?prefix={$projectPrefix}{if (empty($projectVersion)) then () else concat('&amp;version=',$projectVersion)}&amp;format=html&amp;language={$proj-lang}&amp;ui=de-DE';" class="linked flag"/>
                        <img src="/art-decor/img/flags/us.png" onclick="location.href=window.location.pathname+'?prefix={$projectPrefix}{if (empty($projectVersion)) then () else concat('&amp;version=',$projectVersion)}&amp;format=html&amp;language={$proj-lang}&amp;ui=en-US';" class="linked flag"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                    {
                        if (empty($projects)) then () else (
                            if (string-length($view)>0 or string-length($filter)>0) then (
                                let $newUrl := concat(string-join(tokenize(request:get-uri(),'/')[position()!=last()],'/'),'/ProjectIndex?prefix=',$projectPrefix,if (empty($projectVersion)) then () else concat('&amp;version=',$projectVersion),'&amp;format=html&amp;language=',$proj-lang,'&amp;ui=',$ui-lang)
                                return
                                <span style="margin-right: 10px;"><a href="{$newUrl}">{i18n:getMessage($strMessages,'FullIndex',$ui-lang)}</a></span>
                            ) else ()
                        )
                    }
                    {
                        if (count($projects) != 1) then () else (
                            if ((empty($view) or string-length($view)>1) and (empty($filter) or string-length($filter)>1)) then (
                                if (empty($view) or contains($view,'d')) then (
                                    if (contains($filter, 'd')) then () else
                                        <span style="margin-right: 10px;"><a href="#dataSetList">{i18n:getMessage($strMessages,'DataSets',$ui-lang)}</a></span>
                                ) else ()
                                ,
                                if (empty($view) or contains($view,'t')) then (
                                    if (contains($filter, 't')) then () else
                                        <span style="margin-right: 10px;"><a href="#transactionList">{i18n:getMessage($strMessages,'Transactions',$ui-lang)}</a></span>
                                ) else ()
                                ,
                                if (empty($view) or contains($view,'q')) then (
                                    if (contains($filter, 'q')) then () else
                                        <span style="margin-right: 10px;"><a href="#questionnaireList">{i18n:getMessage($strMessages,'Questionnaires',$ui-lang)}</a></span>
                                ) else ()
                                ,
                                if (empty($view) or contains($view,'v')) then (
                                    if (contains($filter, 'v')) then () else
                                        <span style="margin-right: 10px;"><a href="#valueSetList">{i18n:getMessage($strMessages,'ValueSets',$ui-lang)}</a></span>
                                ) else ()
                                ,
                                if (empty($view) or contains($view,'r')) then (
                                    if (contains($filter, 'r')) then () else
                                        <span style="margin-right: 10px;"><a href="#templatesList">{i18n:getMessage($strMessages,'Templates',$ui-lang)}</a></span>
                                ) else ()
                                ,
                                if (empty($view) or contains($view,'c')) then (
                                    if (contains($filter, 'c')) then () else
                                        <span style="margin-right: 10px;"><a href="#codeSystemList">{i18n:getMessage($strMessages,'CodeSystems',$ui-lang)}</a></span>
                                ) else ()
                                ,
                                if (empty($view) or contains($view,'m')) then (
                                    if (contains($filter, 'm')) then () else
                                        <span style="margin-right: 10px;"><a href="#conceptMapList">{i18n:getMessage($strMessages,'ConceptMaps',$ui-lang)}</a></span>
                                ) else ()
                            ) else ()
                        )
                    }
                    </td>
                </tr>
            </tbody>
        </table>
        {
            if (empty($projects)) then (
        <div class="content">
        {
            i18n:getMessage($strMessages,'errorRetrieveProjectNoResults',$ui-lang),' ',if (request:exists()) then request:get-query-string() else ()
        }
        </div>
            )
            else
            if (count($projects) != 1) then (
        <div class="content"/>
            )
            else (
        <div class="content">
        {
            if (empty($view) or contains($view,'d')) then (
                if (contains($filter, 'd')) then () else (
                <div class="h2"id="dataSetList">
                    <h2>{i18n:getMessage($strMessages,'DataSets',$ui-lang), concat('(', count($projects//dataset[@id]), ')')} <span style="float:right; margin-right: 20px;"><a href="#top" style="text-decoration: none;">&#x2191;</a></span></h2>
                    <table class="values" id="dataSetList">
                        <thead>
                            <tr>
                                <th style="width: 5em;">Live</th>
                                <th style="width: 5em;">{i18n:getMessage($strMessages,'Diagram',$ui-lang)}</th>
                                <th style="width: 5em;">DECOR</th>
                                { if ($doFHIRlinks) then <th style="width: 5em;">FHIR</th> else () }
                                <th style="width: 5em;">{i18n:getMessage($strMessages,'columnAllView',$ui-lang)}</th>
                                <th style="width: 5em;">{i18n:getMessage($strMessages,'columnCareView',$ui-lang)}</th>
                                <th>{i18n:getMessage($strMessages,'columnName',$ui-lang)}</th>
                                <th>{i18n:getMessage($strMessages,'effectiveDate',$ui-lang)}</th>
                                {   if ($projects//dataset[@expirationDate]) then
                                    <th>{i18n:getMessage($strMessages,'expirationDate',$ui-lang)}</th>
                                    else ()
                                }
                                <th>{i18n:getMessage($strMessages,'columnStatus',$ui-lang)}</th>
                                {   if ($projects//dataset[@versionLabel]) then
                                    <th>{i18n:getMessage($strMessages,'columnVersionLabel',$ui-lang)}</th>
                                    else ()
                                }
                                {   if ($projectPrefix) then () else
                                    <th>{i18n:getMessage($strMessages,'columnProjects',$ui-lang)}</th>
                                }
                            </tr>
                        </thead>
                        <tbody>
                        {
                            let $statusMap  := map:merge(
                                for $s in $decorSchemaTypes//ItemStatusCodeLifeCycle/enumeration
                                return
                                    map:entry($s/@value, $s/label[@language = $ui-lang])
                            )
                            return
                            
                            for $dataset in $projects//dataset
                            let $fhirVersions           := $dataset/ancestor::decor/project/restURI[@for = 'FHIR'][@format = $installedFhirVersions]/@format
                            let $prefix                 := $dataset/ancestor::decor/project/@prefix
                            let $statusCode             := if ($dataset/@statusCode) then ($dataset/@statusCode) else
                                if (empty($dataset//concept[@statusCode='draft'] | $dataset//concept[@statusCode='new'])) then 'final' else ('draft')
                            let $statusCodeForDisplay   := if (empty($statusCode)) then () else map:get($statusMap, $statusCode)
                            
                            let $t_id                   := $dataset/@id
                            let $t_effectiveDate        := $dataset/@effectiveDate
                            let $baseHref               := concat('RetrieveDataSet?id=',$t_id,'&amp;effectiveDate=',$t_effectiveDate, '&amp;language=', $proj-lang, '&amp;ui=', $ui-lang)
                            let $versionSearchParams    := 
                                if ($dataset/ancestor::decor/@versionDate) then concat('&amp;version=', $dataset/ancestor::decor/@versionDate) else ()
                            let $href                   := concat($baseHref, $versionSearchParams)
                            let $datasetName            := if ($dataset/name[@language = $proj-lang]) then $dataset/name[@language = $proj-lang][1] else $dataset/name[1]
                            order by $prefix, $t_effectiveDate descending, $datasetName
                            return 
                                <tr id="_{string-join(($dataset/(@id|@ref), $dataset/@effectiveDate), '')}" style="background-color:white" onMouseover="this.style.backgroundColor='lightblue';" onMouseout="this.style.backgroundColor='white';">
                                    <td><a href="{$artServerUrl}decor-datasets--{$dataset/ancestor::decor/project/@prefix/string()}?id={$dataset/(@id|@ref)/string()}&amp;effectiveDate={$dataset/@effectiveDate/string()}">link</a></td>
                                    <td><a href="RetrieveConceptDiagram?datasetId={$t_id}&amp;datasetEffectiveDate={encode-for-uri($t_effectiveDate)}&amp;language={$proj-lang}&amp;ui={$ui-lang}{if (empty($projectVersion)) then () else concat('&amp;version=',encode-for-uri($projectVersion))}">link</a></td>
                                    <td>
                                        <a href="{$href}&amp;format=xml">xml</a>
                                        {' '}
                                        <a href="{$href}&amp;format=json">json</a>
                                    </td>
                                    { if ($doFHIRlinks) then 
                                    <td style="width: 5em;">
                                    { 
                                        for $fhirVersion in $fhirVersions
                                        order by $fhirVersion
                                        return (
                                            <a href="{concat($fhirServerBase, $fhirVersion, '/', $prefix, if (string-length($projectVersion) gt 0) then replace($projectVersion, '[^\d]', '') else (), '/StructureDefinition/', $dataset/(@id|@ref), '--', replace($dataset/@effectiveDate, '[^\d]', ''))}">{data($fhirVersion)}</a>, ' '
                                        )
                                    }
                                    </td>
                                    else () }
                                    <td><a href="{$href}&amp;format=html&amp;hidecolumns=3456gh">html</a></td>
                                    <td><a href="{$href}&amp;format=html&amp;hidecolumns=3456bcdefghijklmnop">html</a></td>
                                    <td>
                                    {
                                        $datasetName//text(),
                                        if ($dataset[@id][@ident[not(. = $projectPrefix)] | @prefix[not(. = $projectPrefix)]])
                                        then ( 
                                            '&#160;',
                                            <span class="repobox"><div class="repo ref sspacing">ref</div><div class="non-selectable repo refvalue sspacing">{string(($dataset/@ident[not(. = $projectPrefix)] | $dataset/@prefix[not(. = $projectPrefix)])[1])}</div></span>
                                        ) 
                                        else ()
                                    }
                                    </td>
                                    <td>{$dataset/string(@effectiveDate)}</td>
                                    {   if ($projects//dataset[@expirationDate]) then
                                        <td>{$dataset/string(@expirationDate)}</td>
                                        else ()
                                    }
                                    <td><span class="node-s{$statusCode}">{if ($statusCodeForDisplay) then data($statusCodeForDisplay) else data($statusCode)}</span></td>
                                    {   if ($projects//dataset[@versionLabel]) then
                                        <td>{$dataset/string(@versionLabel)}</td>
                                        else ()
                                    }
                                    {   if ($projectPrefix) then () else
                                        <td>{$dataset/ancestor::decor/project/string(@prefix)}</td>
                                    }
                               </tr>
                        }
                        </tbody>
                    </table>
                </div>
                )
            ) else ()
        }
        {
            if (empty($view) or contains($view,'t')) then (
                if (contains($filter, 't')) then () else (
                    <div class="h2" id="transactionList">
                        <h2>{i18n:getMessage($strMessages,'Transactions',$ui-lang), concat('(', count($projects//transaction[representingTemplate/@sourceDataset]), ')')} <span style="float:right; margin-right: 20px;"><a href="#top" style="text-decoration: none;">&#x2191;</a></span></h2>
                        <table class="values" id="transactionList">
                            <thead>
                                <tr>
                                    <th style="width: 5em;">Live</th>
                                    <th style="width: 5em;">{i18n:getMessage($strMessages,'Diagram',$ui-lang)}</th>
                                    <th style="width: 5em;">DECOR</th>
                                    <th style="width: 5em;">Questionnaire</th>
                                    { if ($doFHIRlinks) then <th style="width: 5em;">FHIR</th> else () }
                                    <th style="width: 5em;">{i18n:getMessage($strMessages,'columnAllView',$ui-lang)}</th>
                                    <th style="width: 5em;">{i18n:getMessage($strMessages,'columnCareView',$ui-lang)}</th>
                                    <th>{i18n:getMessage($strMessages,'columnName',$ui-lang)}</th>
                                    <th>{i18n:getMessage($strMessages,'effectiveDate',$ui-lang)}</th>
                                    {   if ($projects//transaction[@expirationDate]) then
                                        <th>{i18n:getMessage($strMessages,'expirationDate',$ui-lang)}</th>
                                        else ()
                                    }
                                    <th>{i18n:getMessage($strMessages,'columnStatus',$ui-lang)}</th>
                                    {   if ($projects//transaction[@versionLabel]) then
                                        <th>{i18n:getMessage($strMessages,'columnVersionLabel',$ui-lang)}</th>
                                        else ()
                                    }
                                    <th>{i18n:getMessage($strMessages,'columnScenario',$ui-lang)}</th>
                                    <th>{i18n:getMessage($strMessages,'columnDataset',$ui-lang)}</th>
                                    {   if ($projectPrefix) then () else
                                        <th>{i18n:getMessage($strMessages,'columnProjects',$ui-lang)}</th>
                                    }
                                </tr>
                            </thead>
                            <tbody>
                            {
                                let $statusMap  := map:merge(
                                    for $s in $decorSchemaTypes//ItemStatusCodeLifeCycle/enumeration
                                    return
                                        map:entry($s/@value, $s/label[@language = $ui-lang])
                                )
                                let $dsmap      := map:merge(
                                        for $rt in $projects//representingTemplate[@sourceDataset]
                                        let $dsid   := $rt/@sourceDataset
                                        let $dsed   := $rt/@sourceDatasetFlexibility[. castable as xs:dateTime]
                                        
                                        (: check compilation results inside current set first, so we get the published/compiled copy of the dataset :)
                                        let $ds     := 
                                            if (empty($projectVersion)) then art:getDataset($dsid, $dsed)[1] else (
                                                if (empty($dsed)) then
                                                    head(
                                                        for $d in $projects//dataset[@id = $dsid]
                                                        order by $d/@effectiveDate descending
                                                        return $d
                                                    )
                                                else (
                                                    $projects//dataset[@id = $dsid][@effectiveDate = $dsed][1]
                                                )
                                            )
                                        group by $dsid, $dsed
                                        return
                                            map:entry($dsid || $dsed, $ds)
                                    )
                                
                                for $transaction in $projects//transaction[representingTemplate/@sourceDataset]
                                let $datasetid              := $transaction/representingTemplate/@sourceDataset
                                let $dataseteff             := $transaction/representingTemplate/@sourceDatasetFlexibility
                                
                                let $prefix                 := $transaction/ancestor::decor/project/string(@prefix)
                                let $scenarioname           := if ($transaction/ancestor::scenario/name[@language = $proj-lang]) then $transaction/ancestor::scenario/name[@language = $proj-lang][1] else $transaction/ancestor::scenario/name[1]
                                let $transactionname        := if ($transaction/name[@language = $proj-lang]) then $transaction/name[@language = $proj-lang][1] else $transaction/name[1]
                                let $statusCode             := $transaction/@statusCode
                                let $statusCodeForDisplay   := if (empty($statusCode)) then () else map:get($statusMap, $statusCode)
                                (: check compilation results inside current set first, so we get the published/compiled copy of the dataset :)
                                let $ds                     := map:get($dsmap, $datasetid || $dataseteff)
                                let $dspfx                  := ($ds/@ident, $ds/ancestor::decor/project/@prefix)[1]
                                let $datasetname            := if ($ds/name[@language = $proj-lang]) then $ds/name[@language = $proj-lang][1] else $ds/name[1]
                                (:let $datasetstatus        := 
                                    if ($ds/@statusCode) then (
                                        $ds/string(@statusCode)
                                    ) else if (count($ds//concept[@statusCode='draft'])=0 and count($ds//concept[@statusCode='new'])=0) then ( 
                                        'final' 
                                    ) else ('draft'):)
                                let $baseHref             := concat('RetrieveTransaction?id=',$transaction/@id,'&amp;effectiveDate=',$transaction/@effectiveDate, '&amp;language=', $proj-lang, '&amp;ui=', $ui-lang)
                                let $baseQhref             := concat('RetrieveQuestionnaire?id=',$transaction/@id,'&amp;effectiveDate=',$transaction/@effectiveDate, '&amp;language=', $proj-lang, '&amp;ui=', $ui-lang)
                                let $versionSearchParams  := 
                                    if ($transaction/ancestor::decor/@versionDate) then concat('&amp;version=', $transaction/ancestor::decor/@versionDate) else ()
                                let $href                 := concat($baseHref, $versionSearchParams) 
                                let $Qhref                := concat($baseQhref, $versionSearchParams) 
                                order by $prefix, $transaction/@effectiveDate descending, $scenarioname, $transactionname
                                return 
                                   <tr style="background-color:white" onMouseover="this.style.backgroundColor='lightblue';" onMouseout="this.style.backgroundColor='white';" id="_{$transaction/(@id || @ref || @effectiveDate)}">
                                        <td><a href="{$artServerUrl}decor-scenarios--{$transaction/ancestor::decor/project/@prefix/string()}?id={$transaction/(@id|@ref)/string()}&amp;effectiveDate={$transaction/@effectiveDate/string()}">link</a></td>
                                        <td><a href="RetrieveConceptDiagram?transactionId={$transaction/@id}&amp;transactionEffectiveDate={encode-for-uri($transaction/@effectiveDate)}&amp;language={$proj-lang}&amp;ui={$ui-lang}{if (empty($projectVersion)) then () else concat('&amp;version=',encode-for-uri($projectVersion))}">link</a></td>
                                        <td>
                                            <a href="{$href}&amp;format=xml">xml</a>
                                            {' '}
                                            <a href="{$href}&amp;format=json">json</a>
                                        </td>
                                        <td>
                                            <a href="{$Qhref}&amp;format=xml">xml</a> {' '}
                                            <a href="{$Qhref}&amp;format=json">json</a>
                                        {' '}
                                            <a href="{$Qhref}&amp;format=html">html</a>
                                        </td>
                                        { if ($doFHIRlinks) then 
                                        <td style="width: 5em;">
                                        {
                                            for $fhirVersion in $fhirVersions
                                            order by $fhirVersion
                                            return (
                                                <a href="{concat($fhirServerBase, $fhirVersion, '/', $prefix, if (string-length($projectVersion) gt 0) then replace($projectVersion, '[^\d]', '') else (), '/StructureDefinition/', $transaction/(@id|@ref), '--', replace($transaction/@effectiveDate, '[^\d]', ''))}">{data($fhirVersion)}</a>, ' '
                                            )
                                        }
                                        </td>
                                        else () }
                                        <td><a href="{$href}&amp;format=html&amp;hidecolumns=34567">html</a></td>
                                        <td><a href="{$href}&amp;format=html&amp;hidecolumns=45ghijklmnop">html</a></td>
                                        <td>{$transactionname//text()}</td>
                                        <td>{$transaction/@effectiveDate/string()}</td>
                                        {   if ($projects//transaction[@expirationDate]) then
                                            <td>{$transaction/@expirationDate/string()}</td>
                                            else ()
                                        }
                                        <td><span class="node-s{$statusCode}">{if ($statusCodeForDisplay) then data($statusCodeForDisplay) else data($statusCode)}</span></td>
                                        {   if ($projects//transaction[@versionLabel]) then
                                            <td>{$transaction/@versionLabel/string()}</td>
                                            else ()
                                        }
                                        <td>{$scenarioname//text()}</td>
                                        <td>
                                        {
                                            <a href="#_{$ds/@id || $ds/@ref || $ds/@effectiveDate}">{$datasetname//text()}</a>,
                                            if ($dspfx = $projectPrefix) then () else (
                                                '&#160;',
                                                <span class="repobox"><div class="repo ref sspacing">ref</div><div class="non-selectable repo refvalue sspacing">{string($dspfx)}</div></span>
                                            )
                                        }
                                        </td>
                                        {   if ($projectPrefix) then () else
                                            <td>{$transaction/ancestor::decor/project/string(@prefix)}</td>
                                        }
                                   </tr>
                            }
                            </tbody>
                        </table>
                    </div>
                )
            ) else ()
        }
        {
            if (empty($view) or contains($view,'q')) then (
                if (contains($filter, 'q')) then () else (
                    <div class="h2" id="questionnaireList">
                        <h2>{i18n:getMessage($strMessages,'Questionnaires',$ui-lang), concat('(', count($projects//questionnaire), ')')} <span style="float:right; margin-right: 20px;"><a href="#top" style="text-decoration: none;">&#x2191;</a></span></h2>
                        <table class="values" id="questionnaireList">
                            <thead>
                                <tr>
                                    <th style="width: 5em;">Live</th>
                                    <!--<th style="width: 5em;">{i18n:getMessage($strMessages,'Diagram',$ui-lang)}</th>-->
                                    <th style="width: 5em;">Questionnaire</th>
                                {
                                    for $fhirVersion in $fhirVersions[not(. = ('1.0', '3.0'))]
                                    order by $fhirVersion
                                    return
                                    <th style="width: 10em;">FHIR {data($fhirVersion)}</th>
                                }
                                    <th>{i18n:getMessage($strMessages,'columnName',$ui-lang)}</th>
                                    <th>{i18n:getMessage($strMessages,'effectiveDate',$ui-lang)}</th>
                                    {   if ($projects//questionnaire[@expirationDate]) then
                                        <th>{i18n:getMessage($strMessages,'expirationDate',$ui-lang)}</th>
                                        else ()
                                    }
                                    <th>{i18n:getMessage($strMessages,'columnStatus',$ui-lang)}</th>
                                    {   if ($projects//questionnaire[@versionLabel]) then
                                        <th>{i18n:getMessage($strMessages,'columnVersionLabel',$ui-lang)}</th>
                                        else ()
                                    }
                                    <th>{i18n:getMessage($strMessages,'columnTransaction',$ui-lang)}</th>
                                    {   if ($projectPrefix) then () else
                                        <th>{i18n:getMessage($strMessages,'columnProjects',$ui-lang)}</th>
                                    }
                                </tr>
                            </thead>
                            <tbody>
                            {
                                let $statusMap  := map:merge(
                                    for $s in $decorSchemaTypes//ItemStatusCodeLifeCycle/enumeration
                                    return
                                        map:entry($s/@value, $s/label[@language = $ui-lang])
                                )
                                (:<relationship type="DRIV" ref="2.16.840.1.113883.3.1937.99.60.10.4.2" flexibility="2022-02-16T00:00:00"/>:)
                                let $trmap      := map:merge(
                                    for $relationship in $projects//scenarios/questionnaire/relationship[@type = 'DRIV']
                                    let $trid   := $relationship/@ref
                                    let $tred   := $relationship/@flexibility[. castable as xs:dateTime]
                                    
                                    (: check compilation results inside current set first, so we get the published/compiled copy of the template :)
                                    let $tr     := art:getTransaction($trid, ($tred, 'dynamic')[1], $projectVersion, ())
                                    group by $trid, $tred
                                    return
                                        map:entry($trid || $tred, $tr[1])
                                )
                                
                                for $questionnaire in $projects//questionnaire
                                let $questionnairename      := if ($questionnaire/name[@language = $proj-lang]) then $questionnaire/name[@language = $proj-lang][1] else $questionnaire/name[1]
                                let $trid                   := $questionnaire/relationship[@type = 'DRIV'][1]/@ref
                                let $tred                   := $questionnaire/relationship[@type = 'DRIV'][1]/@flexibility[. castable as xs:dateTime]
                                let $trx                    := map:get($trmap, $trid || $tred)
                                let $trxpfx                 := ($trx/@ident, $trx/ancestor::decor/project/@prefix)[1]
                                let $transactionname        := if ($trx/name[@language = $proj-lang]) then $trx/name[@language = $proj-lang][1] else $trx/name[1]
                                
                                let $prefix                 := $questionnaire/ancestor::decor/project/string(@prefix)
                                let $versionDate            := $questionnaire/ancestor::decor/@versionDate
                                let $statusCode             := $questionnaire/@statusCode
                                let $statusCodeForDisplay   := if (empty($statusCode)) then () else map:get($statusMap, $statusCode)
                                let $baseQhref             := concat('RetrieveQuestionnaire?id=',$questionnaire/@id,'&amp;effectiveDate=',$questionnaire/@effectiveDate, '&amp;language=', $proj-lang, '&amp;ui=', $ui-lang)
                                let $versionSearchParams  := 
                                    if ($questionnaire/ancestor::decor/@versionDate) then concat('&amp;prefix=', $prefix, '&amp;version=', $questionnaire/ancestor::decor/@versionDate) else ()
                                let $Qhref                := concat($baseQhref, $versionSearchParams) 
                                order by $prefix, $questionnaire/@fhirVersion, $questionnaire/@effectiveDate descending, $transactionname
                                return 
                                   <tr style="background-color:white" onMouseover="this.style.backgroundColor='lightblue';" onMouseout="this.style.backgroundColor='white';">
                                       <td><a href="{$artServerUrl3 || $questionnaire/ancestor::decor/project/@prefix || '/' || $questionnaire/(@id|@ref) || '/' || encode-for-uri($questionnaire/@effectiveDate)}">link</a></td>
                                       <td>
                                           <a href="{$Qhref}&amp;format=html">html</a>
                                       </td>
                                   {
                                        for $fhirVersion in $fhirVersions[not(. = ('1.0', '3.0'))]
                                        let $fhirUrl := concat($fhirServerBase, $fhirVersion, '/', $prefix, if (string-length($projectVersion) gt 0) then replace($projectVersion, '[^\d]', '') else (), '/Questionnaire/', $questionnaire/(@id|@ref), '--', replace($questionnaire/@effectiveDate, '[^\d]', ''))
                                        
                                        let $baseQhref             := concat('RetrieveQuestionnaire?fhirVersion=',$fhirVersion,'&amp;prefix=',$prefix, if ($versionDate) then '&amp;version=' || encode-for-uri($versionDate) else (), '&amp;id=',$questionnaire/@id,'&amp;effectiveDate=',$questionnaire/@effectiveDate, '&amp;language=', $proj-lang, '&amp;ui=', $ui-lang)
                                        let $versionSearchParams  := 
                                            if ($questionnaire/ancestor::decor/@versionDate) then concat('&amp;version=', $questionnaire/ancestor::decor/@versionDate) else ()
                                        let $Qhref                := concat($baseQhref, $versionSearchParams) 
                                        order by $fhirVersion
                                        return
                                            <td style="width: 10em;">
                                            {
                                                <a href="{$fhirUrl}?_format=xml">xml</a>, ' ',
                                                <a href="{$fhirUrl}?_format=json">json</a>, ' ',
                                                <a href="{$Qhref}&amp;format=html">html</a>
                                            }
                                            </td>
                                    }
                                        <td>{data($questionnairename)}</td>
                                        <td>{$questionnaire/@effectiveDate/string()}</td>
                                        {   if ($projects//questionnaire[@expirationDate]) then
                                            <td>{$questionnaire/@expirationDate/string()}</td>
                                            else ()
                                        }
                                        <td><span class="node-s{$statusCode}">{if ($statusCodeForDisplay) then data($statusCodeForDisplay) else data($statusCode)}</span></td>
                                        {   if ($projects//questionnaire[@versionLabel]) then
                                            <td>{$questionnaire/@versionLabel/string()}</td>
                                            else ()
                                        }
                                        <td>
                                            <a href="#_{$trx/(@id || @ref || @effectiveDate)}">{data($transactionname)}</a>
                                        {
                                            if (empty($trx) or $trxpfx = $projectPrefix) then () else (
                                                '&#160;',
                                                <span class="repobox"><div class="repo ref sspacing">ref</div><div class="non-selectable repo refvalue sspacing">{string($trxpfx)}</div></span>
                                            )
                                        }
                                        </td>
                                        {   if ($projectPrefix) then () else
                                            <td>{$questionnaire/ancestor::decor/project/string(@prefix)}</td>
                                        }
                                   </tr>
                            }
                            </tbody>
                        </table>
                    </div>
                )
            ) else ()
        }
        { 
            if (empty($view) or contains($view,'v')) then (
                if (contains($filter, 'v')) then () else (
                    let $statusMap  := map:merge(
                        for $s in $decorSchemaTypes//ItemStatusCodeLifeCycle/enumeration
                        return
                            map:entry($s/@value, $s/label[@language = $ui-lang])
                    )
                    let $codeSystem         := if (request:exists()) then request:get-parameter('codeSystem',())[string-length() gt 0] else ()
                    let $valueSets          := vs:getValueSetList($id, $name, $effectiveDate, $projectPrefix, $projectVersion, $codeSystem)
                    let $valueSets          := $valueSets/*/valueSet
                    let $doexpirationdate   := $valueSets[@expirationDate]
                    
                    return
                    <div class="h2">
                        <a name="valueSetList"/>
                        <h2>{i18n:getMessage($strMessages,'ValueSets',$ui-lang), concat('(', count($valueSets[@id]), ')')} <span style="float:right; margin-right: 20px;"><a href="#top" style="text-decoration: none;">&#x2191;</a></span></h2>
                        <table class="values" id="valueSetList">
                            <thead>
                                <tr>
                                    <th style="width: 5em;">Live</th>
                                    <th style="width: 5em;">HTML</th>
                                    <th style="width: 5em;">DECOR</th>
                                    { if ($doFHIRlinks) then <th style="width: 5em;">FHIR</th> else () }
                                    <th style="width: 5em;">CSV</th>
                                    <th style="width: 5em;">SQL</th>
                                    <th>{i18n:getMessage($strMessages,'columnName',$ui-lang)}</th>
                                    <th>{i18n:getMessage($strMessages,'columnID',$ui-lang)}</th>
                                    <th>{i18n:getMessage($strMessages,'effectiveDate',$ui-lang)}</th>
                                    <th style="{if ($doexpirationdate) then () else 'display: none;'}">{i18n:getMessage($strMessages,'expirationDate',$ui-lang)}</th>
                                    <th>{i18n:getMessage($strMessages,'columnStatus',$ui-lang)}</th>
                                    <th>{i18n:getMessage($strMessages,'columnVersionLabel',$ui-lang)}</th>
                                    {   if ($projectPrefix) then () else
                                        <th>{i18n:getMessage($strMessages,'columnProjects',$ui-lang)}</th>
                                    }
                                </tr>
                            </thead>
                            <tbody>
                            {
                                let $fhirVersions           := $projects/project/restURI[@for = 'FHIR'][@format = $installedFhirVersions]/@format
                                
                                for $valueSet in $valueSets
                                let $prefix                 := 
                                    if ($valueSet/parent::*[@referencedFrom]) 
                                    then tokenize($valueSet/parent::*/@referencedFrom,' ')[1] 
                                    else $valueSet/parent::*/@ident
                                (:let $refResolved :=
                                    if ($valueSet[@id]) 
                                    then true()
                                    else exists($valueSets//valueSet[@id=$valueSet/@ref][$prefix=parent::*/tokenize(@referencedFrom,' ')]):)
                                let $statusCode             := $valueSet/@statusCode
                                let $statusCodeForDisplay   := if (empty($statusCode)) then () else map:get($statusMap, $statusCode)
                                let $projectList            := distinct-values($valueSet/parent::*/(@ident|@referencedFrom))
                                order by $prefix, $valueSet/(@id | @ref), $valueSet/@effectiveDate descending, $valueSet/@displayName
                                return
                                    <tr style="background-color:white" onMouseover="this.style.backgroundColor='lightblue';" onMouseout="this.style.backgroundColor='white';">
                                        <td><a href="{$artServerUrl}decor-valuesets--{data($prefix)}?id={$valueSet/(@id|@ref)/string()}&amp;effectiveDate={$valueSet/@effectiveDate/string()}">link</a></td>
                                        <td><a href="RetrieveValueSet?id={$valueSet/(@id|@ref)/string()}&amp;effectiveDate={$valueSet/@effectiveDate/string()}&amp;prefix={$prefix}{if (empty($projectVersion)) then () else concat('&amp;version=',$projectVersion)}&amp;format=html&amp;collapsable=true&amp;language={$proj-lang}&amp;ui={$ui-lang}">html</a></td>
                                        <td>
                                            <a href="RetrieveValueSet?id={$valueSet/(@id|@ref)/string()}&amp;effectiveDate={$valueSet/@effectiveDate/string()}&amp;prefix={$prefix}{if (empty($projectVersion)) then () else concat('&amp;version=',$projectVersion)}&amp;format=xml&amp;language={$proj-lang}&amp;ui={$ui-lang}">xml</a>
                                            {' '}
                                            <a href="RetrieveValueSet?id={$valueSet/(@id|@ref)/string()}&amp;effectiveDate={$valueSet/@effectiveDate/string()}&amp;prefix={$prefix}{if (empty($projectVersion)) then () else concat('&amp;version=',$projectVersion)}&amp;format=json&amp;language={$proj-lang}&amp;ui={$ui-lang}">json</a>
                                        </td>
                                        { if ($doFHIRlinks) then 
                                        <td style="width: 5em;">
                                        {
                                            if ($valueSet[@ref]) then () else
                                                for $fhirVersion in $fhirVersions
                                                order by $fhirVersion
                                                return (
                                                    <a href="{concat($fhirServerBase, $fhirVersion, '/', $prefix, if (string-length($projectVersion) gt 0) then replace($projectVersion, '[^\d]', '') else (), '/ValueSet/', $valueSet/(@id|@ref), if ($valueSet/@effectiveDate) then concat('--', replace($valueSet/@effectiveDate, '[^\d]', '')) else ())}">{data($fhirVersion)}</a>, ' '
                                                )
                                        }
                                        </td>
                                        else () }
                                        <td><a href="RetrieveValueSet?id={$valueSet/(@id|@ref)/string()}&amp;effectiveDate={$valueSet/@effectiveDate/string()}&amp;prefix={$prefix}{if (empty($projectVersion)) then () else concat('&amp;version=',$projectVersion)}&amp;format=csv&amp;language={$proj-lang}&amp;ui={$ui-lang}">csv</a></td>
                                        <td><a href="RetrieveValueSet?id={$valueSet/(@id|@ref)/string()}&amp;effectiveDate={$valueSet/@effectiveDate/string()}&amp;prefix={$prefix}{if (empty($projectVersion)) then () else concat('&amp;version=',$projectVersion)}&amp;format=sql&amp;language={$proj-lang}&amp;ui={$ui-lang}">sql</a></td>
                                        <td>{if ($valueSet/@displayName) then $valueSet/@displayName/string() else ($valueSet/@name/string())}</td>
                                        <td>
                                        {
                                            if ($valueSet[@ref])
                                            then ( 
                                                <span class="repobox"><div class="repo ref sspacing">ref</div></span>
                                                (:,
                                                if (not($refResolved)) then
                                                    <span style="padding: 0px 5px 0px 5px; text-align: center; background-color: red; color: white; font-weight: bold;" title="{i18n:getMessage($strMessages,'errorCouldNotResolveReference',$ui-lang)}">!</span>
                                                else ():)
                                                ,
                                                '&#160;'
                                            ) 
                                            else ()
                                        }
                                        {
                                            $valueSet/(@id|@ref)/string(),
                                            if ($valueSet[@id][parent::*/@ident[not(. = $projectPrefix)]])
                                            then ( 
                                                '&#160;',
                                                <span class="repobox"><div class="repo ref sspacing">ref</div><div class="non-selectable repo refvalue sspacing">{string($valueSet/parent::*/@ident)}</div></span>
                                            ) 
                                            else ()
                                        }
                                        </td>
                                        <td>{$valueSet/@effectiveDate/string()}</td>
                                        <td style="{if ($doexpirationdate) then () else 'display: none;'}">{$valueSet/@expirationDate/string()}</td>
                                        <td><span class="node-s{$statusCode}">{if ($statusCodeForDisplay) then data($statusCodeForDisplay) else data($statusCode)}</span></td>
                                        <td>{$valueSet/@versionLabel/string()}</td>
                                        {   if ($projectPrefix) then () else
                                            <td>{$projectList}</td>
                                        }
                                    </tr>
                            }
                            </tbody>
                        </table>
                    </div>
                )
            ) else ()
        }
        {
            if (empty($view) or contains($view,'r')) then (
                if (contains($filter, 'r')) then () else (
                <div class="h2" id="templatesList">
                    <h2>{i18n:getMessage($strMessages,'Templates',$ui-lang), concat('(', count($projects//template[@id]), ')')} <span style="float:right; margin-right: 20px;"><a href="#top" style="text-decoration: none;">&#x2191;</a></span></h2>
                    <table class="values" id="templatesList">
                        <thead>
                            <tr>
                                <th style="width: 5em;">Live</th>
                                <th style="width: 5em;">{i18n:getMessage($strMessages,'Diagram',$ui-lang)}</th>
                                <th style="width: 5em;">DECOR</th>
                                <th style="width: 5em;">HTML</th>
                                <th>{i18n:getMessage($strMessages,'columnName',$ui-lang)}</th>
                                <th>{i18n:getMessage($strMessages,'columnID',$ui-lang)}</th>
                                <th>{i18n:getMessage($strMessages,'effectiveDate',$ui-lang)}</th>
                                <th>{i18n:getMessage($strMessages,'expirationDate',$ui-lang)}</th>
                                <th>{i18n:getMessage($strMessages,'columnStatus',$ui-lang)}</th>
                                <th>{i18n:getMessage($strMessages,'columnVersionLabel',$ui-lang)}</th>
                                {   if ($projectPrefix) then () else
                                    <th>{i18n:getMessage($strMessages,'columnProjects',$ui-lang)}</th>
                                }
                            </tr>
                        </thead>
                        <tbody>
                        {
                            let $statusMap  := map:merge(
                                for $s in $decorSchemaTypes//TemplateStatusCodeLifeCycle/enumeration
                                return
                                    map:entry($s/@value, $s/label[@language = $ui-lang])
                            )
                            (: deactivasted for performance reasons :)
                            (:let $namemap    := map:merge(
                                for $s in $projects//template[@ref]
                                let $id     := $s/@ref
                                group by $id
                                return (
                                    let $displayName    := 
                                        if (string-length($projectVersion) gt 0) then $s else (
                                            templ:getTemplateByRef($id, 'dynamic', $s[1]/ancestor::decor, $projectVersion)/template/template[@id]
                                        )
                                    return
                                    map:entry($id, ($displayName/@displayName, $displayName/@name)[not(.='')][1])
                                )
                            :)
                            return
                            (: Don't show links for templates in versioned projects :)
                            for $decor in $projects[.//template], $templatesById in $decor//template
                            let $prefix                 := $decor/project/@prefix
                            let $tid                    := $templatesById/@id | $templatesById/@ref
                            group by $prefix, $tid
                            order by $prefix, $tid
                            return (
                                for $template in $templatesById
                                let $ted                    := $template/@effectiveDate
                                let $statusCode             := $template/@statusCode
                                let $statusCodeForDisplay   := if (empty($statusCode)) then () else map:get($statusMap, $statusCode)
                                let $displayName            := ($template/@displayName, $template/@name)[not(.='')][1]
                                order by $ted descending
                                return
                                <tr style="background-color:white" onMouseover="this.style.backgroundColor='lightblue';" onMouseout="this.style.backgroundColor='white';">
                                    <td><a href="{$artServerUrl}decor-templates--{$template/ancestor::decor/project/@prefix/string()}?id={$tid}&amp;effectiveDate={$ted}">link</a></td>
                                    <td><a href="{$artDeepLinkServices}RetrieveTemplateDiagram?prefix={$template/ancestor::decor/project/@prefix/string()}&amp;version={$projectVersion}&amp;language={$proj-lang}&amp;ui={$ui-lang}&amp;id={$tid}&amp;effectiveDate={$ted}&amp;format=hgraph">link</a></td>
                                    <td><a href="{$artDeepLinkServices}RetrieveTemplate?prefix={$template/ancestor::decor/project/@prefix/string()}&amp;version={$projectVersion}&amp;language={$proj-lang}&amp;ui={$ui-lang}&amp;id={$tid}&amp;effectiveDate={$ted}&amp;format=xmlnowrapper">xml</a></td>
                                    <td><a href="{$artDeepLinkServices}RetrieveTemplate?prefix={$template/ancestor::decor/project/@prefix/string()}&amp;version={$projectVersion}&amp;language={$proj-lang}&amp;ui={$ui-lang}&amp;id={$tid}&amp;effectiveDate={$ted}&amp;format=html&amp;collapsable=true">html</a></td>
                                    <td>{string($displayName)}</td>
                                    <td>
                                    {
                                        if ($template[@ref])
                                        then ( 
                                            <span class="repobox"><div class="repo ref sspacing">ref</div></span>
                                            (:,
                                            if (not($refResolved)) then
                                                <span style="padding: 0px 5px 0px 5px; text-align: center; background-color: red; color: white; font-weight: bold;" title="{i18n:getMessage($strMessages,'errorCouldNotResolveReference',$ui-lang)}">!</span>
                                            else ():)
                                            ,
                                            '&#160;'
                                        ) 
                                        else ()
                                    }
                                    {
                                        $tid/string(),
                                        if ($template[@id][@ident[not(. = $projectPrefix)]])
                                        then ( 
                                            '&#160;',
                                            <span class="repobox"><div class="repo ref sspacing">ref</div><div class="non-selectable repo refvalue sspacing">{string($template/@ident)}</div></span>
                                        ) 
                                        else ()
                                    }
                                    </td>
                                    <td>{$ted/string()}</td>
                                    <td>{$template/@expirationDate/string()}</td>
                                    <td><span class="node-s{$statusCode}">{if ($statusCodeForDisplay) then data($statusCodeForDisplay) else data($statusCode)}</span></td>
                                    <td>{$template/@versionLabel/string()}</td>
                                    {   if ($projectPrefix) then () else
                                        <td>{$template/ancestor::decor/project/@prefix/string()}</td>
                                    }
                                </tr>
                                )
                        }
                        </tbody>
                    </table>
                </div>
                )
            ) else ()
        }
        { 
            if (empty($view) or contains($view,'c')) then (
                if (contains($filter, 'c')) then () else (
                    let $statusMap  := map:merge(
                        for $s in $decorSchemaTypes//ItemStatusCodeLifeCycle/enumeration
                        return
                            map:entry($s/@value, $s/label[@language = $ui-lang])
                    )
                    let $codeSystem     := if (request:exists()) then request:get-parameter('codeSystem',())[string-length() gt 0] else ()
                    let $codeSystems    := cs:getCodeSystemList($id, $name, $effectiveDate, $projectPrefix, $projectVersion)
                    let $codeSystems    := $codeSystems/*[empty(@url)]/codeSystem
                    return
                    <div class="h2" id="codeSystemList">
                        <h2>{i18n:getMessage($strMessages,'CodeSystems',$ui-lang), concat('(', count($codeSystems), ')')} <span style="float:right; margin-right: 20px;"><a href="#top" style="text-decoration: none;">&#x2191;</a></span></h2>
                        <table class="values" id="codeSystemList">
                            <thead>
                                <tr>
                                    <th style="width: 5em;">Live</th>
                                    <th style="width: 5em;">HTML</th>
                                    { if ($doFHIRlinks) then <th style="width: 5em;">FHIR</th> else () }
                                    <th style="width: 5em;" colspan="2">DECOR</th>
                                    <th style="width: 5em;">CSV</th>
                                    <th style="width: 5em;">SQL</th>
                                    <th>{i18n:getMessage($strMessages,'columnName',$ui-lang)}</th>
                                    <th>{i18n:getMessage($strMessages,'columnID',$ui-lang)}</th>
                                    <th>{i18n:getMessage($strMessages,'effectiveDate',$ui-lang)}</th>
                                    <th>{i18n:getMessage($strMessages,'expirationDate',$ui-lang)}</th>
                                    <th>{i18n:getMessage($strMessages,'columnStatus',$ui-lang)}</th>
                                    <th>{i18n:getMessage($strMessages,'columnVersionLabel',$ui-lang)}</th>
                                    {   if ($projectPrefix) then () else
                                        <th>{i18n:getMessage($strMessages,'columnProjects',$ui-lang)}</th>
                                    }
                                </tr>
                            </thead>
                            <tbody>
                            {
                                for $codeSystem in $codeSystems
                                let $fhirVersions           := $projects/project/restURI[@for = 'FHIR'][@format = $installedFhirVersions]/@format
                                let $prefix                 := 
                                    if ($codeSystem/parent::*[@referencedFrom]) 
                                    then tokenize($codeSystem/parent::*/@referencedFrom,' ')[1] 
                                    else $codeSystem/parent::*/@ident
                                (:let $refResolved :=
                                    if ($codeSystem[@id]) 
                                    then true()
                                    else exists($codeSystems//valueSet[@id=$codeSystem/@ref][$prefix=parent::*/tokenize(@referencedFrom,' ')]):)
                                let $statusCode             := $codeSystem/@statusCode
                                let $statusCodeForDisplay   := if (empty($statusCode)) then () else map:get($statusMap, $statusCode)
                                let $statusCodeForDisplay   := if (empty($statusCodeForDisplay)) then ($statusCode) else $statusCodeForDisplay
                                let $projectList            := string-join(distinct-values($codeSystem/parent::*/(@ident|@referencedFrom)),' ')
                                order by $prefix, $codeSystem/(@id | @ref), $codeSystem/@effectiveDate descending, $codeSystem/@displayName
                                return
                                    <tr style="background-color:white" onMouseover="this.style.backgroundColor='lightblue';" onMouseout="this.style.backgroundColor='white';">
                                        <td><a href="{$artServerUrl}decor-codesystems--{$prefix/string()}?id={$codeSystem/(@id|@ref)/string()}&amp;effectiveDate={$codeSystem/@effectiveDate/string()}">link</a></td>
                                        <td><a href="RetrieveCodeSystem?id={$codeSystem/(@id|@ref)/string()}&amp;effectiveDate={$codeSystem/@effectiveDate/string()}&amp;prefix={$prefix}{if (empty($projectVersion)) then () else concat('&amp;version=',$projectVersion)}&amp;format=html&amp;collapsable=true&amp;language={$proj-lang}&amp;ui={$ui-lang}">html</a></td>
                                        { if ($doFHIRlinks) then 
                                        <td style="width: 5em;">
                                        {
                                            if ($codeSystem[@ref]) then () else
                                                for $fhirVersion in $fhirVersions[not(. = '1.0')][not(. = '3.0')]
                                                order by $fhirVersion
                                                return (
                                                    <a href="{concat($fhirServerBase, $fhirVersion, '/', $prefix, if (string-length($projectVersion) gt 0) then replace($projectVersion, '[^\d]', '') else (), '/CodeSystem/', $codeSystem/(@id|@ref), if ($codeSystem/@effectiveDate) then concat('--', replace($codeSystem/@effectiveDate, '[^\d]', '')) else ())}">{data($fhirVersion)}</a>, ' '
                                                )
                                        }
                                        </td>
                                        else () }
                                        <td><a href="RetrieveCodeSystem?id={$codeSystem/(@id|@ref)/string()}&amp;effectiveDate={$codeSystem/@effectiveDate/string()}&amp;prefix={$prefix}{if (empty($projectVersion)) then () else concat('&amp;version=',$projectVersion)}&amp;format=xml&amp;language={$proj-lang}&amp;ui={$ui-lang}">xml</a></td>
                                        <td><a href="RetrieveCodeSystem?id={$codeSystem/(@id|@ref)/string()}&amp;effectiveDate={$codeSystem/@effectiveDate/string()}&amp;prefix={$prefix}{if (empty($projectVersion)) then () else concat('&amp;version=',$projectVersion)}&amp;format=json&amp;language={$proj-lang}&amp;ui={$ui-lang}">json</a></td>
                                        <td><a href="RetrieveCodeSystem?id={$codeSystem/(@id|@ref)/string()}&amp;effectiveDate={$codeSystem/@effectiveDate/string()}&amp;prefix={$prefix}{if (empty($projectVersion)) then () else concat('&amp;version=',$projectVersion)}&amp;format=csv&amp;language={$proj-lang}&amp;ui={$ui-lang}">csv</a></td>
                                        <td><a href="RetrieveCodeSystem?id={$codeSystem/(@id|@ref)/string()}&amp;effectiveDate={$codeSystem/@effectiveDate/string()}&amp;prefix={$prefix}{if (empty($projectVersion)) then () else concat('&amp;version=',$projectVersion)}&amp;format=sql&amp;language={$proj-lang}&amp;ui={$ui-lang}">sql</a></td>
                                        <td>{if ($codeSystem/@displayName) then $codeSystem/@displayName/string() else ($codeSystem/@name/string())}</td>
                                        <td>
                                        {
                                            if ($codeSystem[@ref])
                                            then ( 
                                                <span class="repobox"><div class="repo ref sspacing">ref</div></span>
                                                (:,
                                                if (not($refResolved)) then
                                                    <span style="padding: 0px 5px 0px 5px; text-align: center; background-color: red; color: white; font-weight: bold;" title="{i18n:getMessage($strMessages,'errorCouldNotResolveReference',$ui-lang)}">!</span>
                                                else ():)
                                                ,
                                                '&#160;'
                                            ) 
                                            else ()
                                        }
                                        {
                                            $codeSystem/(@id|@ref)/string()
                                        }
                                        </td>
                                        <td>{$codeSystem/@effectiveDate/string()}</td>
                                        <td>{$codeSystem/@expirationDate/string()}</td>
                                        <td><span class="node-s{$statusCode}">{if ($statusCodeForDisplay) then data($statusCodeForDisplay) else data($statusCode)}</span></td>
                                        <td>{$codeSystem/@versionLabel/string()}</td>
                                        {   if ($projectPrefix) then () else
                                            <td>{$projectList}</td>
                                        }
                                    </tr>
                            }
                            </tbody>
                        </table>
                    </div>
                )
            ) else ()
        }
        { 
            if (empty($view) or contains($view,'m')) then (
                if (contains($filter, 'm')) then () else (
                    let $statusMap  := map:merge(
                        for $s in $decorSchemaTypes//ItemStatusCodeLifeCycle/enumeration
                        return
                            map:entry($s/@value, $s/label[@language = $ui-lang])
                    )
                    let $conceptMap     := if (request:exists()) then request:get-parameter('conceptMap',())[string-length() gt 0] else ()
                    let $conceptMaps    := 
                        for $p in $projectPrefix
                        return mpapi:getConceptMapList((), $p, $projectVersion, $proj-lang, (), true(), (), (), (), true(), map { "id": $id, "effectiveDate": $effectiveDate })
                    let $conceptMaps    := $conceptMaps/*[empty(@url)]/conceptMap
                    return
                    <div class="h2" id="conceptMapList">
                        <h2>{i18n:getMessage($strMessages,'ConceptMap',$ui-lang), concat('(', count($conceptMaps), ')')} <span style="float:right; margin-right: 20px;"><a href="#top" style="text-decoration: none;">&#x2191;</a></span></h2>
                        <table class="values" id="conceptMapList">
                            <thead>
                                <tr>
                                    <th style="width: 5em;">Live</th>
                                    <th style="width: 5em;">HTML</th>
                                    { if ($doFHIRlinks) then <th style="width: 5em;">FHIR</th> else () }
                                    <th style="width: 5em;" colspan="2">DECOR</th>
                                    <!--<th style="width: 5em;">CSV</th>
                                    <th style="width: 5em;">SQL</th>-->
                                    <th>{i18n:getMessage($strMessages,'columnName',$ui-lang)}</th>
                                    <th>{i18n:getMessage($strMessages,'columnID',$ui-lang)}</th>
                                    <th>{i18n:getMessage($strMessages,'effectiveDate',$ui-lang)}</th>
                                    <th>{i18n:getMessage($strMessages,'expirationDate',$ui-lang)}</th>
                                    <th>{i18n:getMessage($strMessages,'columnStatus',$ui-lang)}</th>
                                    <th>{i18n:getMessage($strMessages,'columnVersionLabel',$ui-lang)}</th>
                                    {   if ($projectPrefix) then () else
                                        <th>{i18n:getMessage($strMessages,'columnProjects',$ui-lang)}</th>
                                    }
                                </tr>
                            </thead>
                            <tbody>
                            {
                                for $conceptMap in $conceptMaps
                                let $fhirVersions           := $projects/project/restURI[@for = 'FHIR'][@format = $installedFhirVersions]/@format
                                let $prefix                 := 
                                    if ($conceptMap/parent::*[@referencedFrom]) 
                                    then tokenize($conceptMap/parent::*/@referencedFrom,' ')[1] 
                                    else $conceptMap/ancestor-or-self::*/@ident
                                let $prefix                 := if ($prefix) then $prefix else $projectPrefix
                                (:let $refResolved :=
                                    if ($conceptMap[@id]) 
                                    then true()
                                    else exists($conceptMaps//valueSet[@id=$conceptMap/@ref][$prefix=parent::*/tokenize(@referencedFrom,' ')]):)
                                let $statusCode             := $conceptMap/@statusCode
                                let $statusCodeForDisplay   := if (empty($statusCode)) then () else map:get($statusMap, $statusCode)
                                let $statusCodeForDisplay   := if (empty($statusCodeForDisplay)) then ($statusCode) else $statusCodeForDisplay
                                let $projectList            := string-join(distinct-values($conceptMap/parent::*/(@ident|@referencedFrom)),' ')
                                order by $prefix, $conceptMap/(@id | @ref), $conceptMap/@effectiveDate descending, $conceptMap/@displayName
                                return
                                    <tr style="background-color:white" onMouseover="this.style.backgroundColor='lightblue';" onMouseout="this.style.backgroundColor='white';">
                                        <td><a href="{$artServerUrl3 || $prefix || '/terminology/conceptmap/' || $conceptMap/(@id|@ref) || '/' || $conceptMap/@effectiveDate}">link</a></td>
                                        <td><a href="RetrieveConceptMap?id={$conceptMap/(@id|@ref)/string()}&amp;effectiveDate={$conceptMap/@effectiveDate/string()}&amp;prefix={$prefix}{if (empty($projectVersion)) then () else concat('&amp;version=',$projectVersion)}&amp;format=html&amp;collapsable=true&amp;language={$proj-lang}&amp;ui={$ui-lang}">html</a></td>
                                        { if ($doFHIRlinks) then 
                                        <td style="width: 5em;">
                                        {
                                            if ($conceptMap[@ref]) then () else
                                                for $fhirVersion in $fhirVersions[not(. = '1.0')][not(. = '3.0')]
                                                order by $fhirVersion
                                                return (
                                                    <a href="{concat($fhirServerBase, $fhirVersion, '/', $prefix, if (string-length($projectVersion) gt 0) then replace($projectVersion, '[^\d]', '') else (), '/ConceptMap/', $conceptMap/(@id|@ref), if ($conceptMap/@effectiveDate) then concat('--', replace($conceptMap/@effectiveDate, '[^\d]', '')) else ())}">{data($fhirVersion)}</a>, ' '
                                                )
                                        }
                                        </td>
                                        else () }
                                        <td><a href="RetrieveConceptMap?id={$conceptMap/(@id|@ref)/string()}&amp;effectiveDate={$conceptMap/@effectiveDate/string()}&amp;prefix={$prefix}{if (empty($projectVersion)) then () else concat('&amp;version=',$projectVersion)}&amp;format=xml&amp;language={$proj-lang}&amp;ui={$ui-lang}">xml</a></td>
                                        <td><a href="RetrieveConceptMap?id={$conceptMap/(@id|@ref)/string()}&amp;effectiveDate={$conceptMap/@effectiveDate/string()}&amp;prefix={$prefix}{if (empty($projectVersion)) then () else concat('&amp;version=',$projectVersion)}&amp;format=json&amp;language={$proj-lang}&amp;ui={$ui-lang}">json</a></td>
                                        <!--<td><a href="RetrieveConceptMap?id={$conceptMap/(@id|@ref)/string()}&amp;effectiveDate={$conceptMap/@effectiveDate/string()}&amp;prefix={$prefix}{if (empty($projectVersion)) then () else concat('&amp;version=',$projectVersion)}&amp;format=csv&amp;language={$proj-lang}&amp;ui={$ui-lang}">csv</a></td>
                                        <td><a href="RetrieveConceptMap?id={$conceptMap/(@id|@ref)/string()}&amp;effectiveDate={$conceptMap/@effectiveDate/string()}&amp;prefix={$prefix}{if (empty($projectVersion)) then () else concat('&amp;version=',$projectVersion)}&amp;format=sql&amp;language={$proj-lang}&amp;ui={$ui-lang}">sql</a></td>-->
                                        <td>{if ($conceptMap/@displayName) then $conceptMap/@displayName/string() else ($conceptMap/@name/string())}</td>
                                        <td>
                                        {
                                            if ($conceptMap[@ref])
                                            then ( 
                                                <span class="repobox"><div class="repo ref sspacing">ref</div></span>
                                                (:,
                                                if (not($refResolved)) then
                                                    <span style="padding: 0px 5px 0px 5px; text-align: center; background-color: red; color: white; font-weight: bold;" title="{i18n:getMessage($strMessages,'errorCouldNotResolveReference',$ui-lang)}">!</span>
                                                else ():)
                                                ,
                                                '&#160;'
                                            ) 
                                            else ()
                                        }
                                        {
                                            $conceptMap/(@id|@ref)/string()
                                        }
                                        </td>
                                        <td>{$conceptMap/@effectiveDate/string()}</td>
                                        <td>{$conceptMap/@expirationDate/string()}</td>
                                        <td><span class="node-s{$statusCode}">{if ($statusCodeForDisplay) then data($statusCodeForDisplay) else data($statusCode)}</span></td>
                                        <td>{$conceptMap/@versionLabel/string()}</td>
                                        {   if ($projectPrefix) then () else
                                            <td>{$projectList}</td>
                                        }
                                    </tr>
                            }
                            </tbody>
                        </table>
                    </div>
                )
            ) else ()
        }
        </div>
            )
        }
    </body>
</html>
    )