xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
module namespace quest  = "http://art-decor.org/ns/quest";

import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";
import module namespace decor       = "http://art-decor.org/ns/decor" at "../../../art/api/api-decor.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../../art/api/api-server-settings.xqm";
(: import module namespace markdown = "http://art-decor.org/ns/api/markdown" at "../../../api/modules/library/markdown-lib.xqm"; :)

declare namespace json      = "http://www.json.org";
declare namespace f         = "http://hl7.org/fhir";
declare namespace xhtml     = "http://www.w3.org/1999/xhtml";

declare %private variable $quest:UCUMCODES := if (doc-available(concat($get:strDecorCore, '/DECOR-ucum.xml'))) then doc(concat($get:strDecorCore, '/DECOR-ucum.xml'))/* else ();

declare function quest:getQuestionnaireJson($id as xs:string, $effectiveDate as xs:string?, $version as xs:string?, $language as xs:string?, $fhirLinkItemStyle as xs:string?) as node() {
    let $xml    := $get:colDecorData//questionnaire[@id = $id]
    let $xml    := 
        if ($xml) then
            if ($effectiveDate castable as xs:dateTime) then $xml[@effectiveDate = $effectiveDate][1]/f:Questionnaire else $xml[@effectiveDate = max($xml/@effectiveDate)][1]/f:Questionnaire
        else (
            quest:getQuestionnaireXml($id, $effectiveDate, $version, $language, $fhirLinkItemStyle)
        )
    return <root>{element {'resourceType'} {$xml/local-name()}, quest:questionnaire2json($xml/*)}</root>
};
 
declare %private function quest:questionnaire2json ($els as element()*) as item()* {
    for $el in $els
    return 
        if ($el[self::xhtml:div]) then 
            <div>{serialize($el)}</div>
        else
        if ($el[self::f:text][parent::f:Questionnaire]) then
            <text>{quest:questionnaire2json($el/*)}</text>
        else
        if ($el/local-name() = ('repeats', 'required', 'readOnly', 'valueDecimal', 'valueInteger', 'valueBoolean')) then 
            element {$el/local-name()} {attribute json:literal {'true'}, $el/@value/string()} 
        else 
        if ($el/local-name() = 'subjectType') then (
            element {$el/local-name()} {attribute json:array {'true'}, $el/@value/string()}
            ,
            if ($el/f:extension)
            then element {concat('_', $el/local-name())} {quest:questionnaire2json($el/*)} 
            else ()
        )
        else 
        if ($el/@value/string()) then (
            element {$el/local-name()} {$el/@value/string()}
            ,
            if ($el/f:extension)
            then element {concat('_', $el/local-name())} {quest:questionnaire2json($el/*)} 
            else ()
        )
        else 
        if ($el[self::f:valueCoding]) then 
            element {$el/local-name()} {$el/@*, quest:questionnaire2json($el/*)}
        else
        if ($el[self::f:valueCodeableConcept][parent::f:extension[@url = 'http://hl7.org/fhir/StructureDefinition/questionnaire-itemControl']]) then 
            element {$el/local-name()} {$el/@*, quest:questionnaire2json($el/*)}
        else 
        if ($el/*) then 
            element {$el/local-name()} {attribute json:array {'true'}, $el/@*, quest:questionnaire2json($el/*)}
        else ()
};

declare function quest:getQuestionnaireXml($id as xs:string, $effectiveDate as xs:string?, $version as xs:string?, $language as xs:string?, $fhirLinkItemStyle as xs:string?) as node() {
    let $transactionOrDataset   := 
        if (string-length($id) > 0) then
            art:getDataset($id, $effectiveDate, $version, $language) | art:getTransaction($id, $effectiveDate, $version, $language)
        else ()
    
    let $language               := if (empty($language) or ($language = '*')) then $transactionOrDataset/ancestor::decor/project/@defaultLanguage else ($language)
    (: fix for de-CH :)
    let $language               := if ($language='de-CH') then 'de-DE' else $language
    
    let $fullDatasetTree        := 
        if ($version castable as xs:dateTime) then (
            let $datasets   := 
                if ($transactionOrDataset[self::dataset]) then ($transactionOrDataset) else (
                    if ($language = '*') then
                        $get:colDecorVersion//transactionDatasets[@versionDate = $version]//dataset[@transactionId = $transactionOrDataset/@id][@transactionEffectiveDate = $transactionOrDataset/@effectiveDate]
                    else (
                        $get:colDecorVersion//transactionDatasets[@versionDate = $version][@language = $language]//dataset[@transactionId = $transactionOrDataset/@id][@transactionEffectiveDate = $transactionOrDataset/@effectiveDate]
                    )
                )
            
            return $datasets[1]
        ) else if ($transactionOrDataset) then (
            art:getFullDatasetTree($transactionOrDataset, (), (), $language, (), false(), ())
        ) else ()
        
    let $result                 := 
        if ($fullDatasetTree) then (
            (:let $xsl            := doc('../resources/stylesheets/tr2quest.xsl')
            return transform:transform($fullDatasetTree, $xsl, ()):)
            let $oidMap           := 
                for $cs in $fullDatasetTree//@codeSystem
                let $csid := $cs
                let $csed := $cs/../@codeSystemVersion
                group by $csid, $csed
                return map:entry($csid || $csed, art:getCanonicalUriForOID('CodeSystem', $csid, $csed, $transactionOrDataset/ancestor::decor, $get:strKeyCanonicalUriPrefdR4))
            return
            quest:decorTransaction2Questionnaire($fullDatasetTree,
                map { 
                    "fhirLinkItemStyle": $fhirLinkItemStyle, 
                    "fhirCanonicalBase": adserver:getServerURLFhirServices(), 
                    "publisher": ($transactionOrDataset/ancestor::decor/project/copyright/@by, "ART-DECOR Expert Group")[1], 
                    "language":  $language,
                    "oids": map:merge($oidMap)
                }
            )
        )
        else ()
    return $result
};

declare %private function quest:decorTransaction2Questionnaire($transaction as element(dataset), $params as map(*)?) as element(f:Questionnaire) {
    let $language               := (map:get($params, 'language')[not(. = '')], 'nl-NL')[1]
    let $fhirLinkItemStyle      := map:get($params, 'fhirLinkItemStyle')
    let $fhirCanonicalBase      := map:get($params, 'fhirCanonicalBase')
    let $publisher              := map:get($params, 'publisher')
    let $transactionId          := $transaction/@transactionId
    let $targets                := if (map:keys($params) = 'targets') then map:get($params, 'targets') else $transaction/*:concept/@id
    
    let $check                  :=
        if (empty($targets)) then
            error(xs:QName('quest:NOCONCEPTS'), 'ERROR: no targets for this transaction')
        else ()

    return
    <Questionnaire xmlns="http://hl7.org/fhir">
        <id value="{concat($transactionId, '--', replace($transaction/@transactionEffectiveDate, '\D', ''))}"/>
        <text>
            <status value="generated"/>
            <div xmlns="http://www.w3.org/1999/xhtml">Questionnaire for {data($transaction/*:name)}</div>
        </text>
        <url value="{art:getCanonicalUri('Questionnaire', $transaction)}"/>
        {
            if (true()) then () else (
                <identifier> 
                    <system value="http://loinc.org"/> 
                    <value value="http://loinc.org/ValueSet/LL357-5"/> 
                </identifier> 
            )
        }
        {
            if (empty($transaction/@versionLabel)) then () else <version value="{$transaction/@versionLabel}"/>
        }
        <name value="{quest:shortName($transaction/*:name[1])}"/>
        <title value="{$transaction/*:name[1]}"/>
        <status value="draft"/>
        <subjectType value="Patient"/>
        <date value="{substring(string(current-date()), 1, 10)}"/>
    {
        if (empty($publisher)) then () else <publisher value="{$publisher}"/>
    }
    {
        for $c in $transaction/*:concept[@id = $targets] return quest:decorConcept2QuestionnaireItem($c, $params)
    }
    </Questionnaire>
};

(:~ Returns a name which is an acceptable FHIR .name which matches ('[A-Z]([A-Za-z0-9_]){0,254}')
    Most common diacritics are replaced
    
    Note: an implementation of this function exists in XSL too in the ADA package shortName.xsl. Changes here should be reflected there too and vice versa.
    
    Input:  xs:string, example: "Underscored Lowercase ë"
    Output: xs:string, example: "underscored_lowercase_e"
    
    @author Marc de Graauw, Alexander Henket
    @since 2013
:)
declare function quest:shortName($name as xs:string?) as xs:string? {
    let $shortname := 
        if ($name) then (
            (: add some readability to CamelCased names. E.g. MedicationStatement to Medication_Statement. :)
            let $r0 := replace($name, '([a-z])([A-Z])', '$1_$2')
            
            (: find matching alternatives for <=? smaller(equal) and >=? greater(equal) :)
            let $r1 := replace($r0, '<\s*=', 'le')
            let $r2 := replace($r1, '<', 'lt')
            let $r3 := replace($r2, '>\s*=', 'ge')
            let $r4 := replace($r3, '>', 'gt')
            
            (: find matching alternatives for more or less common diacriticals, replace single spaces with _ , replace ? with q (same name occurs quite often twice, with and without '?') :)
            let $r5 := translate(normalize-space($r4),' ÀÁÃÄÅÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝŸÇÑàáãäåèéêëìíîïòóôõöùúûüýÿç€ßñ?','_AAAAAEEEEIIIIOOOOOUUUUYYCNaaaaaeeeeiiiiooooouuuuyycEsnq')
            (: ditch anything that's not alpha numerical or underscore :)
            let $r6 := replace($r5,'[^a-zA-Z\d_]','')
            (: make sure we start with a capital :)
            let $r7 := 
                if (matches(substring($r6, 1, 1), '[A-Za-z]')) then 
                    upper-case(substring($r6, 1, 1)) || substring($r6, 2)
                else (
                    'A' || $r6
                )
            return $r7
        ) else ()
    
    return if (matches($shortname, '^[A-Z]([A-Za-z0-9_]){1,254}$')) then $shortname else ()
};

declare %private function quest:decorConcept2QuestionnaireItem($concept as element(concept), $params as map(*)?) as element(f:item)? {
    let $language               := map:get($params, 'language')
    let $fhirLinkItemStyle      := map:get($params, 'fhirLinkItemStyle')
    let $fhirCanonicalBase      := map:get($params, 'fhirCanonicalBase')
    let $publisher              := map:get($params, 'publisher')
    let $oidMap                 := map:get($params, 'oids')
    let $targetId               := 
        if ($fhirLinkItemStyle='idDisplay') 
        then $concept/@iddisplay 
        else if ($fhirLinkItemStyle='oid')
        then $concept/@id 
        else $concept/concat(@id, '--', replace(@effectiveDate, '\D', ''))
    let $conceptId              := $concept/@id
    
    let $conceptDescription     := $concept/*:desc[.//text()[not(normalize-space() = '')]]
    let $conceptDescriptionText := replace(replace(string-join($conceptDescription[1], '\n'), '&#13;?&#10;\s+' , '&#10;'), '^\s+|\s+$', '')
    let $conceptDescriptionHtml := if ($conceptDescription) then replace(replace(art:serializeNode($conceptDescription[1]), '&#13;?&#10;\s+' , '&#10;'), '^\s+|\s+$', '') else ()
    let $definitionUri          := 
        $fhirCanonicalBase || 'StructureDefinition/' || $concept/ancestor::*:dataset/@transactionId || '--' || replace($concept/ancestor::*:dataset/@transactionEffectiveDate, '\D', '') || 
                                                 '#' || $concept/@id                                || '--' || replace($concept/@effectiveDate, '\D', '')

    let $itemText               := ($concept/*:operationalization, $concept/*:name)[1] 
    (:let $itemText               :=
        if ($itemText[count(*) = 1][*:div][normalize-space(string-join(text(), '')) = '']) then
            md:html2markdown($itemText/*:div/node())
        else (
            md:html2markdown($itemText/node())
        ):)
    (:let $itemText               :=
        if ($itemText[count(*) = 1][*:div][normalize-space(string-join(text(), '')) = '']) then
            art:serializeNode($itemText/*:div)
        else (
            art:serializeNode($itemText)
        ):)
    let $itemText               :=
        if ($itemText[count(*) = 1][*:div][normalize-space(string-join(text(), '')) = '']) then
            $itemText/*:div
        else (
            $itemText/node()
        )
    
    let $defaultValue           := ($concept/*:valueDomain/*:property/@default)[1]
    (: In addition, the following extensions MUST be supported: minValue, maxValue, minLength, maxDecimalPlaces, unit :)
    let $minInclude             := min($concept/*:valueDomain/*:property[@minInclude castable as xs:decimal]/xs:decimal(@minInclude))
    let $maxInclude             := max($concept/*:valueDomain/*:property[@maxInclude castable as xs:decimal]/xs:decimal(@maxInclude))
    let $minLength              := min($concept/*:valueDomain/*:property[@minLength castable as xs:integer]/xs:integer(@minLength))
    let $maxDecimalPlaces       := 
        max(
            for $t in $concept/*:valueDomain/*:property/@fractionDigits
            let $md   := replace($t, '[^\d]', '')[not(. = '')]
            return
                if ($md castable as xs:integer) then xs:integer($md) else ()
        )
    return
    switch ($concept/@type)
    case 'group' return (
        <item xmlns="http://hl7.org/fhir">
            <linkId value="{$targetId}"/>
        {
            if ($conceptDescription) then
                (:<definition value="{md:html2markdown(art:parseNode($conceptDescription[1])/node())}"/>:)
                <definition value="{$definitionUri}"/>
            else ()
        }
            <text value="{replace(string-join($itemText, ''), '^\s+|\s+$', '')}">
            {
                (:Identifies how the specified element should be rendered when displayed.:)
                if (empty($concept/@renderingStyle)) then () else (
                    <extension url="http://hl7.org/fhir/StructureDefinition/rendering-style">
                        <valueString value="{$concept/@renderingStyle}"/>
                    </extension>
                )
            }
            </text>
        {
            for $assoc in $concept/*:terminologyAssociation[@conceptId = $conceptId]
            return
                <code>
                    <system value="{map:get($oidMap, $assoc/@codeSystem || $assoc/@codeSystemVersion)}"/>
                    <code value="{$assoc/@code}"/>
                {
                    if (empty($assoc/@displayName)) then () else ( 
                        <display value="{$assoc/@displayName}"/>
                    )
                }
                </code>
        }
            <type value="group"/>
            <required value="{$concept/@minimumMultiplicity and $concept/@minimumMultiplicity != '0'}"/>
            <repeats value="{not($concept/@maximumMultiplicity = '0' or $concept/@maximumMultiplicity = '1')}"/>
        {
            if ($conceptDescription) then
                (: Note: datatype string does not officially allow xhtmland will trigger a datatype error, however ... it is what the core extension is defined with
                https://chat.fhir.org/#narrow/stream/179166-implementers/topic/Questionnaire.20extension.20rendering-xhtml.20with.20datatype.20string :)
                <item>
                    <extension url="http://hl7.org/fhir/StructureDefinition/questionnaire-itemControl">
                        <valueCodeableConcept>
                            <coding>
                                <system value="http://hl7.org/fhir/questionnaire-item-control"/>
                                <code value="help"/>
                                <display value="Help-Button"/>
                            </coding>
                            <text value="Help-Button"/>
                        </valueCodeableConcept>
                    </extension>
                    <linkId value="{$targetId}-help"/>
                    <text value="{$conceptDescriptionText}">
                        <extension url="http://hl7.org/fhir/StructureDefinition/rendering-xhtml">
                            <valueString value="{$conceptDescriptionHtml}"/>
                        </extension>
                    </text>
                    <type value="display"/>
                </item>
            else ()
        }
        {
            for $c in $concept/*:concept return quest:decorConcept2QuestionnaireItem($c, $params)
        }
        </item>
    )
    case 'item' return (
        <item xmlns="http://hl7.org/fhir">
        {
            if (empty($minInclude)) then () else (
                <extension url="http://hl7.org/fhir/StructureDefinition/minValue">
                {
                    if ($minInclude castable as xs:integer) then
                        <valueInteger value="{xs:integer($minInclude)}"/>
                    else (
                        <valueDecimal value="{$minInclude}"/>
                    )
                }
                </extension>
            )
        }
        {
            if (empty($maxInclude)) then () else (
                <extension url="http://hl7.org/fhir/StructureDefinition/maxValue">
                {
                    if ($maxInclude castable as xs:integer) then
                        <valueInteger value="{xs:integer($maxInclude)}"/>
                    else (
                        <valueDecimal value="{$maxInclude}"/>
                    )
                }
                </extension>
            )
        }
        {
            if (empty($minLength)) then () else (
                <extension url="http://hl7.org/fhir/StructureDefinition/minLength">
                    <valueInteger value="{xs:integer($minLength)}"/>
                </extension>
            )
        }
        {
            if ($concept/*:valueDomain/@type = 'quantity') then
                for $unit in $concept/*:valueDomain/*:property/@unit
                let $unit     := normalize-space($unit)
                let $ucumUnit := $quest:UCUMCODES/*:ucum[@unit = $unit]
                return
                    <extension url="http://hl7.org/fhir/StructureDefinition/questionnaire-unitOption">
                        <valueCoding>
                        {
                            if (empty($ucumUnit)) then () else (
                                <system value="http://unitsofmeasure.org"/>
                            )
                        }
                            <code value="{$unit}"/>
                            <display value="{($ucumUnit/@displayName[not(. = '')], $unit)[1]}"/>
                        </valueCoding>
                    </extension>
            else ()
        }
        {
            if (empty($maxDecimalPlaces)) then () else (
                <extension url="http://hl7.org/fhir/StructureDefinition/maxDecimalPlaces">
                    <valueInteger value="{$maxDecimalPlaces}"/>
                </extension>
            )
        }
            <linkId value="{$targetId}"/>
        {
            if ($conceptDescription) then
                (:<definition value="{md:html2markdown(art:parseNode($conceptDescription[1])/node())}"/>:)
                <definition value="{$definitionUri}"/>
            else ()
        }
        {
            for $assoc in $concept/*:terminologyAssociation[@conceptId = $conceptId]
            return
                <code>
                    <system value="{map:get($oidMap, $assoc/@codeSystem || $assoc/@codeSystemVersion)}"/>
                    <code value="{$assoc/@code}"/>
                {
                    if (empty($assoc/@displayName)) then () else ( 
                        <display value="{$assoc/@displayName}"/>
                    )
                }
                </code>
        }
            <text value="{replace(string-join($itemText, ''), '^\s+|\s+$', '')}">
            {
                if ($itemText[*]) then
                    <extension url="http://hl7.org/fhir/StructureDefinition/rendering-xhtml">
                        <valueString value="{serialize($itemText)}"/>
                    </extension>
                else ()
            }
            {
                (:Identifies how the specified element should be rendered when displayed.:)
                if (empty($concept/@renderingStyle)) then () else (
                    <extension url="http://hl7.org/fhir/StructureDefinition/rendering-style">
                        <valueString value="{$concept/@renderingStyle}"/>
                    </extension>
                )
            }
            </text>
        {
            let $itemType     := quest:decor2questionnaireType($concept)
            
            return (
                <type value="{$itemType}"/>,
                if ($itemType = 'display') then () else (
                    <required value="{$concept/@minimumMultiplicity = '1'}"/> |
                    <repeats value="{not($concept/@maximumMultiplicity = '0' or $concept/@maximumMultiplicity = '1')}"/>
                )
            )
        }
        {
            if ($concept/*:valueDomain/*:property/@fixed='true') then 
                <readOnly value="true"/>
            else ()
        }
        {
            if ($concept/*:valueDomain/*:property[@maxLength castable as xs:integer]) then
                <maxLength value="{max($concept/*:valueDomain/*:property[@maxLength castable as xs:integer]/xs:integer(@maxLength))}"/>
            else ()
        }
        {
            for $vs in $concept/*:valueSet[*:completeCodeSystem | *:conceptList/*:include | *:conceptList/*:exclude]
            return
                <answerValueSet value="{
                  if ($vs[@canonicalUri]) then $vs/@canonicalUri else (
                      concat(adserver:getServerURLFhirServices(),'ValueSet/',$vs/@id, '--', replace($vs/@effectiveDate,'[^\d]',''))
                  )
                }"/>
        }
        {
            for $c in $concept/*:valueSet[empty(*:completeCodeSystem | *:conceptList/*:include | *:conceptList/*:exclude)]/*:conceptList/(*:concept | *:exception)
            return
                <answerOption>
                {
                    (: Ordinal Value: A numeric value that allows the comparison (less than, greater than) or other numerical manipulation of a concept 
                        (e.g. Adding up components of a score). Scores are usually a whole number, but occasionally decimals are encountered in scores. :)
                    if ($c/@ordinal) then 
                        <extension url="http://hl7.org/fhir/StructureDefinition/ordinalValue">
                            <valueDecimal value="{$c/@ordinal}"/>
                        </extension>
                    else ()
                }
                    <valueCoding>
                        <system value="{map:get($oidMap, $c/@codeSystem || $assoc/@codeSystemVersion)}"/>
                        <code value="{$c/@code}"/>
                        <display value="{$c/@displayName}"/>
                    </valueCoding>
                {
                    if ($c/@code = $defaultValue) then
                        <initialSelected value="true"/>
                    else ()
                }
                </answerOption>
        }
        {
            if ($defaultValue) then 
                (: For code, Questionnaire expect Coding, DECOR default is just a simple string :)
                if ($concept/*:valueDomain/@type != 'code') then
                    <initial>
                    {
                        element {quest:decor2questionnaireAnswerType($concept)} {
                            attribute value {$defaultValue}
                        }
                    }
                    </initial>
                else ()
            else ()
        }
        {
            (: Note: datatype string does not officially allow xhtml and will trigger a datatype error, however ... it is what the core extension is defined with
                https://chat.fhir.org/#narrow/stream/179166-implementers/topic/Questionnaire.20extension.20rendering-xhtml.20with.20datatype.20string :)
            if ($conceptDescription) then
                <item>
                    <extension url="http://hl7.org/fhir/StructureDefinition/questionnaire-itemControl">
                        <valueCodeableConcept>
                            <coding>
                                <system value="http://hl7.org/fhir/questionnaire-item-control"/>
                                <code value="help"/>
                                <display value="Help-Button"/>
                            </coding>
                            <text value="Help-Button"/>
                        </valueCodeableConcept>
                    </extension>
                    <linkId value="{$targetId}-help"/>
                    <text value="{$conceptDescriptionText}">
                        <extension url="http://hl7.org/fhir/StructureDefinition/rendering-xhtml">
                            <valueString value="{$conceptDescriptionHtml}"/>
                        </extension>
                    </text>
                    <type value="display"/>
                </item>
            else ()
        }
        </item>
    )
    default return (
        error(xs:QName('quest:UNKNOWNCONCEPTTYPE'), concat('ERROR: Unknown type of concept "', $concept/@type, '" expected item or group'))
    )
};

declare %private function quest:decor2questionnaireType($concept as element(concept)) as xs:string {
    let $valueDomainType    := $concept/valueDomain/@type
    let $conceptItemType    := $concept/property[@name = 'Quest::ItemType']
    let $conceptItem        := $conceptItemType/normalize-space(string-join(.//text()))
    return
    (: allow override of all other options by explicitly setting the questionnaire item type value.
        https://hl7.org/fhir/R4/valueset-item-type.html
    :)
    if ($conceptItemType) then 
        switch (lower-case($conceptItem))
        case 'group'
        case 'display'
        case 'question'
        case 'boolean'
        case 'decimal'
        case 'integer'
        case 'date'
        case 'time'
        case 'string'
        case 'text'
        case 'url'
        case 'choice'
        case 'open-choice'
        case 'attachment'
        case 'reference'
        case 'quantity' return $conceptItem
        case 'datetime' return 'dateTime'
        default return error(xs:QName('quest:IllegalItemType'), concat('Concept id=', $concept/@id, ' effectiveDate=', $concept/@effectiveDate, ' ', $concept/name[1], ' defines unknown property Quest::ItemType value: ''', $conceptItem, ''''))
    else
    (: Input param is a concept, since we need to look at group :)
    if ($concept/@type = 'group') then 'group' else
    if ($valueDomainType = 'boolean') then $valueDomainType else
    if ($valueDomainType = 'date') then $valueDomainType else
    if ($valueDomainType = 'decimal') then $valueDomainType else
    if ($valueDomainType = 'quantity') then $valueDomainType else
    if ($valueDomainType = 'text') then $valueDomainType else
    if ($valueDomainType = 'time') then $valueDomainType else
    if ($valueDomainType = 'count') then 'integer' else
    if ($valueDomainType = 'datetime') then 'dateTime' else
    if ($valueDomainType = 'duration') then 'quantity' else
    if ($valueDomainType = 'currency') then 'quantity' else
    if ($valueDomainType = 'identifier') then 'string' else
    if ($valueDomainType = 'ordinal') then 'choice' else
    if ($valueDomainType = 'blob') then 'attachment' else
    if ($valueDomainType = 'code') then (
        if ($concept/*:valueSet) then
            if ($concept/*:terminologyAssociation[@strength[not(. = 'required')]]) then
                'open-choice'
            else
            if ($concept/*:valueSet[*:completeCodeSystem | *:conceptList/*:include | *:conceptList/*:exclude]) then
                'open-choice'
            else
            if ($concept/*:valueSet/*:conceptList/*[@code = 'OTH'][@codeSystem = '2.16.840.1.113883.65.1008']) then
                'open-choice'
            else (
                'choice'
            )
        else (
            'string'
        )
    )
    else (
        'string'
    )
};
declare %private function quest:decor2questionnaireAnswerType($concept as element(concept)) as xs:string {
    (: Input param is a concept, since we need to look at group :)
    switch ($concept/valueDomain/@type)
    case 'count' return 'valueInteger'
    case 'date' return 'valueDate'
    case 'time'  return 'valueTime'
    case 'code' return 'valueCoding'
    case 'blob' return 'valueReference'
    default return 'valueString'
};
