xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get      = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art      = "http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";
import module namespace adserver = "http://art-decor.org/ns/art-decor-server" at "../../../art/api/api-server-settings.xqm";
import module namespace templ    = "http://art-decor.org/ns/decor/template" at "../../../art/api/api-decor-template.xqm";
import module namespace vs       = "http://art-decor.org/ns/decor/valueset" at "../../../art/api/api-decor-valueset.xqm";
import module namespace gg       = "http://art-decor.org/ns/decor/governancegroups" at "../../../art/api/api-decor-governancegroups.xqm";
import module namespace i18n     = "http://art-decor.org/ns/decor/i18n" at "../../../art/api/api-decor-i18n.xqm";

declare namespace error             = "http://art-decor.org/ns/decor/template/error";

declare option exist:serialize "method=xhtml indent=no media-type=application/xhtml+html";

declare variable $useLocalAssets        := if (request:exists()) then request:get-parameter('useLocalAssets','false') else 'false';
declare variable $resourcePath          := if ($useLocalAssets = 'true') then ('../assets') else ('../core/assets');
declare variable $docMessages           := i18n:getMessagesDoc('decor/services');

let $dodev              := if (request:exists()) then request:get-parameter('dev','false')='true' else (true())
(:interface language:)
let $ui-lang            := if (request:exists()) then request:get-parameter('ui',$get:strArtLanguage)[string-length()>0][1] else ($get:strArtLanguage)
(: language :)
let $language           := if (request:exists()) then request:get-parameter('language',$get:strArtLanguage) else $get:strArtLanguage
(: support HTML and future use: XML, CSV format :)
let $format             := if (request:exists()) then request:get-parameter('format','html') else ('html')

(: future use: instead of posting create the list first based on the governance group OID :)
let $gg                 := if (request:exists()) then request:get-parameter('gg',()) else ()

(: get the posting of list of projects, templates and value sets from parameter :)
let $gglist             := if (empty($gg)) then () else gg:getGovernanceGroups($gg, true(), $language, true())

(: get filtered projects for results :)
let $governancegroups   := if (empty($gg)) then (gg:getGovernanceGroupList($language)) else ()

(: start processing ... :)
let $error           :=
    if (empty($gg)) then 
        ()
        (:error(xs:QName('error:IllegalArgument'),concat('Governance Group List must be posted. ', 'Found none.')):)
    else if (count($gglist/group)=0) then
        error(xs:QName('error:IllegalArgument'),concat('Governance Group not found: ', string-join($gg, ' ')))
    else if ($format = ('html', 'csv', 'xml')) then
        ()
    else error(xs:QName('error:IllegalArgument'),concat('Unknown format requested: ', $format, '.'))

let $artdecordeeplinkprefix := adserver:getServerURLArt()

let $xsltParameters :=
    <parameters>
        <param name="language"                  value="{$language}"/>
        <param name="resourcePath"              value="{$resourcePath}"/>
        <param name="logLevel"                  value="'OFF'"/>
        <param name="switchCreateTreeTableHtml" value="'true'"/>
        <param name="artdecordeeplinkprefix"    value="{$artdecordeeplinkprefix}"/>
    </parameters>

let $xslt               := 
    if ($dodev) then 
        xs:anyURI('https://assets.art-decor.org/ADAR-dev/rv/GovernanceGroup2List.xsl')
    else (
        xs:anyURI(concat('xmldb:exist://', $get:strDecorCore, '/GovernanceGroup2List.xsl'))
        (:xs:anyURI('https://assets.art-decor.org/ADAR/rv/GovernanceGroup2List.xsl'):)
    )

let $error-transform    := "Error rendering the document"
let $css                := doc(xs:anyURI(concat('xmldb:exist://',$get:strArt, '/resources/stylesheets/common-decor-style.xml')))

return 
    if ($format="xml" and not(empty($gglist))) then (
        response:set-header('Content-Type','application/xml'),
        $gglist
    ) else (  
        response:set-header('Content-Type','text/html'),
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{substring($language,1,2)}" lang="{substring($language,1,2)}">
            <head>
                <meta http-equiv="Content-Type" content="text/xhtml; charset=utf-8"/>
                <title>Governance Group List of Artefacts</title> 
                <script src="{$resourcePath}/scripts/jquery-1.11.3.min.js" type="text/javascript"></script>
                <script src="{$resourcePath}/scripts/jquery.treetable.js" type="text/javascript"></script>
                <script src="{$resourcePath}/scripts/jquery.easytabs.js" type="text/javascript"></script>
                <link href="{$resourcePath}/decor.css" rel="stylesheet" type="text/css"></link>
                <style type="text/css"><![CDATA[ body { background-color: white; } ]]></style>
                <script type="text/javascript">
                         <![CDATA[ $(document).ready( function() { $('#tab-container').easytabs(); }); ]]>
                </script>
                <style type="text/css"><![CDATA[
                    .sspacing { line-height: 18px; }
                    .etabs { 
                        margin: 0; padding: 0; border: solid #96bf0b; border-width: 0 0 3px; margin: 0 0.16em 7px 0; padding: 0; zoom: 1;
                    }
                    .tab { 
                        display: inline-block; zoom:1; background: #eee;
                        /*-moz-border-radius: 4px 4px 0 0; -webkit-border-radius: 4px 4px 0 0; */ 
                        margin-right: 3px; border: solid #a3a3a3; border-width: 1px 1px 0; border-bottom: none;
                        background: #d8d8d8 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAABRCAIAAABKXIFMAAAAAXNSR0IArs4c6QAAAAZ0Uk5TAIIAkQC0lV1BMQAAAHlJREFUeJzszEEKggAABdF//xOKFGGRhBiapKGFunc/0GKGWb9sWJE+0CsWSf+w8sXKgpUZK9P0gc57HKHzGgbodH0PnbZ9QufRNNC51zV0qusNOqfzBTpFUUJLS0tLS0tLS0tLS0tLS0tLS0tLS0v/Bb0DAAD//wMAo87Weyx0YLgAAAAASUVORK5CYII=) repeat-x;
                    }
                    .tab a { font-size: 1.0em; line-height: 2em; display: block; padding: 0 10px; outline: none; color: #000; position: relative; text-decoration: none;}
                    .tab a:hover { 
                        text-decoration: none; 
                        background: #bfdaff url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAABPCAIAAABzgOKnAAAAAXNSR0IArs4c6QAAAAZ0Uk5TAIIAkQC0lV1BMQAAAItJREFUeJzszDEKg1AABNG9/6VsAiksQgoRJGJQ9BuIol5gc4N00+0w9ZOxtF6GJulyGVrlNLSW09AkPR+G1nQYWuNuaL03Q2vYDK3X19DqPoZWuxpaTTG0nouh9ZgMrXo0tO6DoXXrDa2qM3To0KFDhw4dOnTo0KFDhw4dOnTo0KFD//kHAAD//wMAOUd7qQjDY2UAAAAASUVORK5CYII=) repeat-x; outline: 0;}
                    .tab.active { background: #96bf0b; padding-top: 6px; position: relative; top: 1px; border-color: #96bf0b; }
                    .tab.active a:hover { background: #96bf0b;}
                    .tab a.active { color: #fff; font-weight: bold; }
                    .tab-container .panel-container { background: #fff; border: solid #666 1px; padding: 10px; -moz-border-radius: 0 4px 4px 4px; -webkit-border-radius: 0 4px 4px 4px; }
                    .panel-container { margin-bottom: 10px; }
                    .version-draft, .version-active, .version-retired { background-repeat: no-repeat; padding: 2px 0 0 18px; display: inline-block; }
                    .version-draft { background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAgBJREFUeNrEkz1oFEEUx3+zezu797kXvcBFjAlCIohNAqJgJYKF4hcWQcRYWUoQgpW1gl0axUKiYHoRRLQziAiKaCQq0QhCThLzZdgz+z1jsYl4XJnCB39mpvj/5s2b94TWmq2EwRYjt7n5MiFwnDKCRKbKOl6s7R2Sxf5BkLYOv896K9OPfO/HBLCoNfRf0K0AAJX6hUJ1z/XqzjMjll2H2AMVQaVnl1s7cHjt5+TQUuPlxSRJZtoyUArDLnRd3d59YsRIAlh6DioAFWYQw8F1+w6SNh8sNN6eBOZbAJa0j7qdA1eMoAH+HGgFOgEVZ4D4NwTfcPNd+71yxzBwswVQqtTO5c2gQjAPOgIdQxplZh2BTrOMwjXKjjzVBhBC9RPPQria3ayTzKQVoP/ROsIo97bVQIjVAMuDOGz9JyGADWkNjgQ/Ctv6IAriV2m+AKUSWBJsJ5OU2TlngbShUiKK1fs2QHMtHY/D9AP1bZnRNCBnQM4Ey8zWzg6UZenmSnS/DWCZzMx9/nWt6aWr9O2AqgtOPoMVS9DbBW4Btbw8ViumT/6+cHMWwqeCyXfw+I15fvhS/cbgQLU7K5EAQ7G4tB7eud24+3HKH61WhH/roWoFHNon8EOY+oq5u87Zy8P26WNHyj1S5uT0J29hbHz9xbPX+p7aaKBNn/jv0/hnACU92J/4AZPUAAAAAElFTkSuQmCC') }
                    .version-active { background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAj1JREFUeNrEk7tPU2EYxn/fufUGLVoUrXLRNKiEgQBGMUoMaNREY7wFN0dXNJH/wEEHdxJjYkyYcXFiEh1UEBNvKFG5mBZLKYVyOKc953yfi5iQjg4+07u8vzzvk/cRSin+RRr/KGNz6L6ZoC6eRCppCsWF9nTnQFMq3alreihXyHz9MPP26cKvHyNCiGUhNCaHV7cCBAKn7ERbm9vunj1xebBhxy5c38ZXHq3iQGN3R0//+Ouxq+MTYzcC6c9WOaj4ZdG8J33n/KkrgyIcMJ2bwAtcgsAjUD4hK0LXka5e19t4/Gpq/CJQ3AKIRWr7D3ccveVqeXJLGZQCqTwC6eNLj7VyjoKzwP6De3u/zzUMAMNbAE2pluvR7SSya9/wggpSevjSw5c+gfJRKsBzy1SsEjt311+qAhhhP73qzWH7JWTgI5VEIVEoUAqFQimFJz3MqNdUlYFTKdmVoAB4CAG69idaIQABgJIQDVnkAtup+oOV5Y3nhi+oq4liGQZh0yJsmoQMg5BhYOo60bBBrWlSzNtvqgEr9pOfc8WXDYkYNRELXQNTF5i6hq4JQoZGKhmnuOS42ezqo6oTdFNkpiYWh5Kx2EjXsVRz0S5jO2VQYFkGyXiE7Px6+d37zJAI89eB2OxCz+16ivMVlj97Z05fa3nQc7KxLR4PoWkC1/GZ/pQvjD78cj+fce4ZlkZ+xt0KCCd0lITKuhTo9B3q23au/Xj9PtMSxuzH0uLks8UX5YIaBUoAm3viv7fx9wAbJgb8OnzPEgAAAABJRU5ErkJggg==') }
                    .version-retired { background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAkJJREFUeNrEU01oE2EQfbP5dptkm25rksa2sbGC4B9IabGH2oJVRBB6Eaw9CoJQ8ChU4llUxKsXQfFSRKQ3T0oRqbRig8UWUWlLVExMyyZNmt1Ndr/v81AihD324MDAMIfHmzfvkZQSeykFeyzWGKLX0jD0CDQWCHlcjB3vTV05nOweVANqS94015c31mezhcJs3fN+K0TYfnKnGQAAuOCtyWgifXFoePpgVwpVl6PmSRw9pPSdOjF4di6zePnt58yU7dRWfQw452pne8fU+Oj5aU3vxPusCdvl8KSE60lEwmEMnDwzWhfi0bvlpQkAuSYNWkPh0wPH+m9WWQwfsgWYdQ+WAKqcsCOA72YZCzkbiQP9I4l98Ukfg5549wSL9MRWCmXUPAEuCXUu4AnAlRJCErYqFirhMNqjyUsAHjYByFDkyEaNoWg7gBTwJIFLgcaXJQBJgOsJBIJGyseg7Lo2QaAG2l0QQIrSGEEEgAtoOoNtcsfnA7u0Oc9QQWuHDsYALahA03Zb1RQEAgTWoiIUIdR2ChkfgFUyZ6yt7KLRY0DTgxCSgwIEYgRJAEiiozcObm/KSu7nM98J0vPW8pmFtG7oT7uGRpKVnAWrbAFCggVVGN0G3GIehS9Ld13Hfu23sqLA2foz9+n+rRtrzx+vMzIRSyiI7mdoa3NRXJl3Pt6+/qCa+3WP1JZ/GlAjTLGraYjqNkpvXmqKs3MhNjY+Hh8+18dCIVb6urqZf/Vi3vnxbUbRgnkQwG2rGeC/pfHvAKp7/MFAzKVhAAAAAElFTkSuQmCC') }
    
                ]]>
                </style>
                <style type="text/css">{$css}</style>
            {
                if ($gglist) then () else (
                    <script>
                        function getGovernanceGroup() {{
                            return document.getElementById('ggSelector').options[document.getElementById('ggSelector').selectedIndex].value;
                        }};
                        /*function getLanguage() {{
                            return document.getElementById('languageSelector').options[document.getElementById('languageSelector').selectedIndex].value;
                        }};
                        function getFormat() {{
                            return document.getElementById('formatSelector').options[document.getElementById('formatSelector').selectedIndex].value;
                        }};*/
                    </script>
                )
            }
            </head>
            <body>
            {
                if ($gglist) then transform:transform($gglist, $xslt, $xsltParameters) else (
                    <h1>{i18n:getMessage($docMessages,'GovernanceGroups',$ui-lang)}</h1>,
                    <form name="input" action="GoveranceGroupList" method="get">
                        <table border="0">
                            <!-- project -->
                            <tr>
                                <td>{i18n:getMessage($docMessages,'GovernanceGroup',$ui-lang)}:</td>
                                <td>
                                    <!--<select id="ggSelector" onchange="javascript:location.href=window.location.pathname+'?gg='+getGovernanceGroup()+'&amp;format=html&amp;language='+getLanguage()+'&amp;ui={$ui-lang}'+'&amp;format='+getFormat()" name="gg" style="width: 300px;">-->
                                    <select id="ggSelector" onchange="javascript:location.href=window.location.pathname+'?gg='+getGovernanceGroup()" name="gg" style="width: 300px;">
                                        <option value="">{i18n:getMessage($docMessages,'SelectGroup',$ui-lang)} ({count($governancegroups/*)})</option>
                                    {
                                        for $group in $governancegroups/*
                                        let $ggname     := ($group/*:name[@language = $language], $group/*:name[@language = $group/@defaultLanguage])[1]
                                        order by lower-case($ggname)
                                        return
                                            <option value="{$group/@id}">
                                            {
                                                if ($group/@id = $gg) then attribute {'selected'} {'true'} else (),
                                                data($ggname)
                                            }
                                            </option>
                                    }
                                    </select> (*)
                                </td>
                            </tr>
                            <!-- language -->
                            <!--<tr style="vertical-align: top;">
                                <td>Language to retrieve the group in if you prefer not to retrieve in the default language:</td>
                                <td>
                                    <select name="language" id="languageSelector" style="width: 300px;">
                                    {
                                        <option value="" selected="true">-/-default language ({data($governancegroups/*[@id = $gg]/@defaultLanguage)})-/-</option>,
                                        for $lang in distinct-values($governancegroups/*[@id = $gg]/*:name/@language)
                                        return
                                            if ($governancegroups/*[@id = $gg][@defaultLanguage = $lang]) then () else <option value="{$lang}">{$lang}</option>
                                    }
                                    </select> (optional)
                                </td>
                            </tr>-->
                            <!-- format -->
                            <!--<tr style="vertical-align: top; background-color: #eee;">
                                <td>Get governance group in html or csv</td>
                                <td>
                                    <!-/- Update forceRecompile disabled status onchange -/->
                                    <select id="formatSelector" name="format" style="width: 300px;">
                                        <option value="html">{ if ($format = 'html')  then attribute selected {'true'} else ()}html</option>
                                        <option value="csv">{ if ($format = 'csv')  then attribute selected {'true'} else ()}csv</option>,
                                    </select> (*)
                                </td>
                            </tr>-->
                        </table>
                    </form>
                )
            }
            </body>
        </html>
    )