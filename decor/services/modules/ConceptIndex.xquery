xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace i18n        = "http://art-decor.org/ns/decor/i18n" at "../../../art/api/api-decor-i18n.xqm";

(: TODO: media-type beter zetten en XML declaration zetten bij XML output :)
declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=yes";

declare variable $useLocalAssets        := if (request:exists()) then request:get-parameter('useLocalAssets','false') else 'false';
declare variable $download              := if (request:exists()) then request:get-parameter('download','false') else '';
(:When called with localAssets=true then we need relative local paths, else 
    we need our server services URL, but with matching scheme (http or https)
    If the scheme is https and servicesUrl is http this leads to a security 
    problem in some browsers
:)
declare variable $resourcePath          := if ($download='true') then ('https://assets.art-decor.org/ADAR/rv/assets') else if ($useLocalAssets = 'true') then ('../assets') else ('../core/assets');

declare variable $docMessages           := i18n:getMessagesDoc('decor/services');

let $id            := request:get-parameter('id','')
let $code          := request:get-parameter('code','')
let $effectiveDate := request:get-parameter('effectiveDate','')
let $format        := request:get-parameter('format','xml')
let $language      := request:get-parameter('language',$get:strArtLanguage)

let $parameters    :=  request:get-parameter-names()

let $searchString  := 
    for $parKey in $parameters
        let $parValue := request:get-parameter($parKey,'')
    return
        if ($parKey != 'format' and string-length($parValue) > 0) then
            (concat('@',$parKey,'=&apos;',$parValue,'&apos;'))
        else 
            ()

let $codes := ()

return 
    <warning>{i18n:getMessage($docMessages,'errorNotImplementedYet',$language),' ',$searchString}</warning>