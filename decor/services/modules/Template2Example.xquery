xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art     = "http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";
import module namespace templ   = "http://art-decor.org/ns/decor/template" at "../../../art/api/api-decor-template.xqm";
import module namespace vs      = "http://art-decor.org/ns/decor/valueset" at "../../../art/api/api-decor-valueset.xqm";
(:import module namespace temple  = "http://art-decor.org/ns/temple" at "../../../temple/modules/temple.xqm";:)

declare namespace error             = "http://art-decor.org/ns/decor/template/error";
declare namespace hl7               = "urn:hl7-org:v3";
declare namespace cda               = "urn:hl7-org:v3";
(:declare namespace temple            = "http://art-decor.org/ns/temple";:)

(: These three functions local:isOid / local:toOid / local:removeNamesFromRulesOids were duplicated from the Temple package as we would otherwise have a dependency on that package :)
declare %private function local:isOid($oid as xs:string?) as xs:boolean {
    matches($oid, "^[0-2](\.(0|[1-9][0-9]*))*$")
};
(: Will return OID for things like Datummeting[2.16.840.1.113883.3.1937.99.62.3.10.4], else just return what it got :)
declare %private function local:toOid($oid as xs:string) as xs:string {
    let $innerOid := substring-before(substring-after($oid,'['),']')
    return if (local:isOid($innerOid)) then $innerOid else $oid
};
declare %private function local:removeNamesFromRulesOids($xml as node()) as node() {
    let $result :=
        if (local-name($xml)='example') then 
            $xml
        else
            element {local-name($xml)}
            {
                for $att in $xml/@*
                return 
                    attribute {local-name($att)} {
                        if (local-name($att)='ref' or local-name($att)='contains' or local-name($att)='codeSystem' or local-name($att)='valueSet') 
                        then local:toOid($att/string()) else $att/string()
                    }
                , 
                for $child in $xml/node() 
                return 
                    if ($child instance of element()) then local:removeNamesFromRulesOids($child) else $child
            }
    return $result
};

let $dodev              := if (request:exists()) then request:get-parameter('dev','false')='true' else (false())
let $dopackage          := if (request:exists()) then request:get-parameter('package','false')='true' else (false())
(: relevant to template-editor to post something from in memory -- use id/effectiveDate parameters otherwise :)
let $template           := 
    if (request:exists()) then (
        if (request:get-parameter-names()[. = 'code']) then (
            (: This only happens to temple. Temple will have mangled/named OIDs so they need conversion before anything :)
            local:removeNamesFromRulesOids(fn:parse-xml(request:get-parameter('code', ''))//template)
        ) else (
            (: This normally only happens from the visual Template-editor and contains @selected="" elements :)
            request:get-data()/template
        )
    ) else ()

(: instead of posting a template like above, you can ask for an example based on any existing template with parameters id/effectiveDate. prefix is optional :)
let $tmid               := if (request:exists()) then request:get-parameter('id',()) else ('2.16.840.1.113883.3.1937.99.62.3.10.14')
let $tmed               := if (request:exists()) then request:get-parameter('effectiveDate',()) else ('2012-03-11T00:00:00')
let $elid               := if (request:exists()) then request:get-parameter('elementId',())[string-length() gt 0][1] else ()
let $prefix             := if (request:exists()) then request:get-parameter('prefix',()) else ('demo1-')

(: relevant to know if we should send the example back as string (true, usually Template-editor) or as xml (false):)
let $doSerialized       := if ($template) then true() else false()
let $doSerialized       := if (request:exists()) then request:get-parameter('serialized',  string($doSerialized))   = 'true' else ($doSerialized)
(: relevant to Template-editor that can send elements/attributes that contain @selected to indicate whether or not they should be processed :)
let $doSelectedOnly     := if ($template) then true() else false()
let $doSelectedOnly     := if (request:exists()) then request:get-parameter('selectedOnly',string($doSelectedOnly)) = 'true' else ($doSelectedOnly)
(: relevant to tell the processing if we should venture beyond @contains | include :)
let $doRecursive        := if ($template) then false() else false()
let $doRecursive        := if (request:exists()) then request:get-parameter('recursive',   string($doRecursive))    = 'true' else ($doRecursive)

(: start processing ... :)
let $template           := if ($template) then $template else if (empty($tmid)) then () else (templ:getTemplateById($tmid, $tmed, $prefix)/template/template[@id])[1]
let $prefix             := if (empty($prefix)) then (if ($template[@projectPrefix]) then $template/@projectPrefix else $template/@ident) else $prefix

let $format             := if ($template/classification[@format]) then $template/classification[@format][1]/@format else ('hl7v3xml1')

let $check              :=
    if ($template) then 
        if (empty($elid)) then () else if ($template//element[@id = $elid]) then () else (
            error(xs:QName('error:IllegalArgument'),concat('Argument elementId ', $elid, ' does not exist in template with id ', $template/@id, ' effectiveDate ', $template/@effectiveDate))
        )
    else (
        error(xs:QName('error:IllegalArgument'),concat('Argument id ', $tmid, ' effectiveDate ', $tmed, ' did not lead to a template and no template provided.'))
    )
    
let $templateChain      := 
    if ($template and $doRecursive) then $template | templ:getTemplateChain($prefix, $template, map:merge(map:entry(concat($template/@id, $template/@effectiveDate), ''))) else (
        $template,
        for $elm in $template//element[@contains] | $template//include
        return
            (templ:getTemplateById($elm/@contains | $elm/@ref, ($elm/@flexibility, 'dynamic')[1], $prefix)/template/template[@id])[1]
    )
let $templateChain      :=
    for $t in $templateChain
    let $ideff  := concat($t/@id, $t/@effectiveDate)
    group by $ideff
    order by $t[1]/@id
    return $t[1]

let $vocabChain         :=
    for $v in $templateChain//vocabulary[@valueSet]
    let $ideff  := concat($v/@valueSet, $v/@flexibility)
    group by $ideff
    return
        vs:getExpandedValueSetById($v[1]/@valueSet, if ($v[1]/@flexibility castable as xs:dateTime) then $v[1]/@flexibility else 'dynamic', $prefix, (), (), false())//valueSet[@id]

let $decor              := art:getDecorByPrefix($prefix)
let $decorPackage       :=
    <decor>
        {$decor/@*}
        <project>{$decor/project/@*, $decor/project/defaultElementNamespace}</project>
        <terminology>{$vocabChain}</terminology>
        <rules>{$templateChain}</rules>
    </decor>

let $xsltParameters :=
    <parameters>
        <param name="tmid"                      value="{if (empty($tmid)) then $template/@id else ($tmid)}"/>
        <param name="tmed"                      value="{if (empty($tmed)) then $template/@effectiveDate else ($tmed)}"/>
        <param name="elid"                      value="{$elid}"/>
        <param name="doSelectedOnly"            value="{$doSelectedOnly}"/>
        <param name="doRecursive"               value="{$doRecursive}"/>
        <param name="logLevel"                  value="'OFF'"/>
    </parameters>

let $xslt := 
    if ($dodev) then 
        xs:anyURI('https://assets.art-decor.org/ADAR-dev/rv/Template2Example.xsl')
    else (
        xs:anyURI(concat('xmldb:exist://', $get:strDecorCore, '/Template2Example.xsl'))
        (:xs:anyURI('https://assets.art-decor.org/ADAR/rv/Template2Example.xsl'):)
    )

(:we need a root element, it's the way it is...:)
let $tpath              := replace($template/context/@path,'[/\[].*','')
let $path               := if (string-length($tpath)>0) then $tpath else ('art:placeholder')
let $element            := <element name="{$path}" format="{$format}" xmlns:art="urn:art-decor:example" selected="">{$template/(attribute|element|include|choice)}</element>
let $example            := if ($dopackage) then () else if ($decor) then transform:transform($decorPackage, $xslt, $xsltParameters) else ()

(:if we did not need our pseudo root-element, just leave it off:)
let $example            := if ($example[count(@*)>0 or count(*)>1]) then $example else $example/node()

return
    if ($dopackage) then $decorPackage else (
        <example caption="" type="neutral">
        {
            if ($doSerialized) then
                fn:serialize($example,
                    <output:serialization-parameters xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization">
                        <output:method>xml</output:method>
                        <output:encoding>UTF-8</output:encoding>
                    </output:serialization-parameters>
                )
            else (
                $example
                (:,
                if ($dodev) then $decorPackage else ():)
            )
        }
        </example>
    )