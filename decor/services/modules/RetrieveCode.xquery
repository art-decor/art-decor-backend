xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace i18n        = "http://art-decor.org/ns/decor/i18n" at "../../../art/api/api-decor-i18n.xqm";
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../../art/api/api-server-settings.xqm";
import module namespace snomed      = "http://art-decor.org/ns/terminology/snomed" at "../../../terminology/snomed/api/api-snomed.xqm";
import module namespace adloinc     = "http://art-decor.org/ns/terminology/loinc" at "../../../terminology/loinc/api/api-loinc.xqm";
import module namespace claml       = "http://art-decor.org/ns/terminology/claml" at "../../../terminology/claml/api/api-claml.xqm";

declare namespace http              = "http://expath.org/ns/http-client";

(: TODO: media-type beter zetten en XML declaration zetten bij XML output :)
declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=yes";

declare variable $artDeepLinkServices   := adserver:getServerURLServices();
declare variable $artDeepLink           := adserver:getServerURLArt();
declare variable $artDeepLinkTerminology:= replace(replace($artDeepLink,'/art-decor/', '/terminology/'), ':8080/', ':8877/');

declare variable $useLocalAssets        := if (request:exists()) then request:get-parameter('useLocalAssets','false') else 'false';
declare variable $download              := if (request:exists()) then request:get-parameter('download','false') else '';
(:  When called with localAssets=true then we need relative local paths, else 
    we need our server services URL, but with matching scheme (http or https)
    If the scheme is https and servicesUrl is http this leads to a security 
    problem in some browsers
:)
declare variable $resourcePath          := if ($download='true') then ('https://assets.art-decor.org/ADAR/rv/assets') else if ($useLocalAssets = 'true') then ('../assets') else ('../core/assets');
declare variable $resourcePath2         := adserver:getServerURLArt();

declare variable $docMessages           := i18n:getMessagesDoc('decor/services');

declare variable $codeSystemSNOMED      := '2.16.840.1.113883.6.96';
declare variable $codeSystemLOINC       := '2.16.840.1.113883.6.1';
(:declare variable $codeSystemsCLAML      := collection($get:strTerminologyData)//ClaML/Identifier/@uid;:)
declare variable $codeSystemsCLAML      := collection(concat($get:strTerminology,'/claml'))/classificationIndex//classification;

declare variable $snomedgpscheck        := if (request:exists()) then request:get-parameter('snomedgps',())[1] = ('on', 'true') else true();
declare variable $localizationcheck     := if (request:exists()) then request:get-parameter('l10n',())[1] = ('on', 'true') else true();

declare variable $requestHeaders        := 
    <http:request method="GET">
        <http:header name="Content-Type" value="text/xml"/>
        <http:header name="Cache-Control" value="no-cache"/>
        <http:header name="Max-Forwards" value="1"/>
    </http:request>
;
declare variable $snomedRefsets         :=
    try { doc(concat(repo:get-root(), 'terminology-data/snomed-data/meta/refsets.xml'))/refsets }
    catch * {
        <refsets fallback="true">
            <refset id="98051000146103" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor contactallergenen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch contact allergen simple reference set</desc>
            </refset>
            <refset id="98011000146102" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch food allergen simple reference set</desc>
            </refset>
            <refset id="52801000146101" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset van landelijk implantatenregister</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch implant registry simple reference set</desc>
            </refset>
            <refset id="98021000146107" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch inhalation allergen simple reference set</desc>
            </refset>
            <refset id="98031000146109" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch insect venom allergen simple reference set</desc>
            </refset>
            <refset id="98041000146101" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch occupational allergen simple reference set</desc>
            </refset>
            <refset id="98061000146100" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor allergenen uitgezonderd medicatie</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch total non-drug allergen simple reference set</desc>
            </refset>
            <refset id="2581000146104" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor micro-organismen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch microorganism simple reference set</desc>
            </refset>
            <refset id="55451000146109" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch radio-allergosorbent test result simple reference set</desc>
            </refset>
            <refset id="97801000146108" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Netherlands microscopic ordinal test result simple reference set</desc>
            </refset>
            <refset id="46231000146109" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Netherlands ordinal test result simple reference set</desc>
            </refset>
            <refset id="110891000146105" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing intervention for delirium simple reference set</desc>
            </refset>
            <refset id="99051000146107" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor verpleegkundige interventies</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing intervention simple reference set</desc>
            </refset>
            <refset id="110881000146108" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing interventions for fall risk simple reference set</desc>
            </refset>
            <refset id="110861000146100" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing interventions for pain simple reference set</desc>
            </refset>
            <refset id="110911000146108" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing interventions for psychosocial care simple reference set</desc>
            </refset>
            <refset id="110901000146106" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing interventions for suicide simple reference set</desc>
            </refset>
            <refset id="110871000146106" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing interventions for wound simple reference set</desc>
            </refset>
            <refset id="117711000146107" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor patiëntproblemen inclusief secties van e-overdracht</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Simple reference set of Dutch nursing problems with sections of e-transfer</desc>
            </refset>
            <refset id="11721000146100" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">nationale kernset patiëntproblemen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing problem simple reference set</desc>
            </refset>
            <refset id="41000146103" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor optometrische diagnosen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch optometric diagnoses simple reference set</desc>
            </refset>
            <refset id="231000146105" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor optometrische verrichtingen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch optometric procedures simple reference set</desc>
            </refset>
            <refset id="8721000146106" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor optometrische bezoekredenen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch optometric reason for visit simple reference set</desc>
            </refset>
            <refset id="2551000146109" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor zeldzame neuromusculaire aandoeningen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch rare neuromuscular disorders simple reference set</desc>
            </refset>
            <refset id="31000147101" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">DHD Diagnosethesaurus-referentieset</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">DHD Diagnosis thesaurus reference set</desc>
            </refset>
            <refset id="110851000146103" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">PALGA thesaurus simple reference set for pathology</desc>
            </refset>
            <refset id="721144007" moduleId="900000000000012004">
                <desc languageCode="en" languageRefsetId="900000000000509007">General dentistry diagnostic reference set</desc>
            </refset>
            <refset id="450970008" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor huisartsen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">General Practice / Family Practice reference set</desc>
            </refset>
            <refset id="711112009" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset met eenvoudige 'mapping' naar ICNP-diagnosen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">ICNP diagnoses simple map reference set</desc>
            </refset>
            <refset id="712505008" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset met eenvoudige 'mapping' naar ICNP-interventies</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">ICNP interventions simple map reference set</desc>
            </refset>
            <refset id="723264001" moduleId="900000000000012004">
                <desc languageCode="en" languageRefsetId="900000000000509007">Lateralizable body structure reference set</desc>
            </refset>
            <refset id="733990004" moduleId="900000000000012004">
                <desc languageCode="en" languageRefsetId="900000000000509007">Nursing Activities Reference Set</desc>
            </refset>
            <refset id="733991000" moduleId="900000000000012004">
                <desc languageCode="en" languageRefsetId="900000000000509007">Nursing Health Issues Reference Set</desc>
            </refset>
            <refset id="721145008" moduleId="900000000000012004">
                <desc languageCode="en" languageRefsetId="900000000000509007">Odontogram reference set</desc>
            </refset>
            <refset id="32321000146103" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">SNOMED CT to ICF correlated extended map reference set</desc>
            </refset>
            <refset id="31451000146105" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">SNOMED CT to NANDA correlated map reference set</desc>
            </refset>
            <refset id="32311000146108" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">SNOMED CT to Omaha correlated extended map reference set</desc>
            </refset>
            <refset id="467614008" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset met eenvoudige 'mapping' naar GMDN</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">GMDN simple map reference set</desc>
            </refset>
            <refset id="447562003" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset met complexe 'mapping' naar ICD-10</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">ICD-10 complex map reference set</desc>
            </refset>
            <refset id="446608001" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset met eenvoudige 'mapping' naar ICD-O</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">ICD-O simple map reference set</desc>
            </refset>
            <refset id="450993002" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset met complexe 'mapping' naar ICPC-2</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">International Classification of Primary Care, Second edition complex map reference set</desc>
            </refset>
            <refset id="705112009" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset met 'mapping' naar LOINC Part-codes</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">LOINC Part map reference set</desc>
            </refset>
        </refsets>
    }
;
declare variable $snomedAssociations    :=
    try { doc(concat(repo:get-root(), 'terminology-data/snomed-data/meta/associationsValues.xml'))/associationsValues }
    catch * {
        <associationsValues fallback="true">
            <refset id="900000000000524003" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende geëxporteerde concepten</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">MOVED TO association reference set</desc>
            </refset>
            <refset id="900000000000523009" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende mogelijk equivalente concepten</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">POSSIBLY EQUIVALENT TO association reference set</desc>
            </refset>
            <refset id="900000000000526001" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende vervangende concepten</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">REPLACED BY association reference set</desc>
            </refset>
            <refset id="900000000000527005" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende identieke concepten</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">SAME AS association reference set</desc>
            </refset>
            <refset id="900000000000528000" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende voormalige ouderconcepten</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">WAS A association reference set</desc>
            </refset>
            <refset id="900000000000530003" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende alternatieve concepten</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">ALTERNATIVE association reference set</desc>
            </refset>
            <refset id="900000000000525002" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende geïmporteerde concepten</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">MOVED FROM association reference set</desc>
            </refset>
            <refset id="900000000000489007" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset met attribuutwaarden voor reden voor deactivatie van concept</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Concept inactivation indicator reference set</desc>
            </refset>
            <value id="900000000000487009" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">component verplaatst naar andere module</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Moved elsewhere</desc>
            </value>
            <value id="900000000000484002" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">ambigu component</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Ambiguous</desc>
            </value>
            <value id="900000000000482003" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">dubbel component</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Duplicate</desc>
            </value>
            <value id="900000000000483008" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">obsoleet component</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Outdated</desc>
            </value>
            <value id="723277005" moduleId="900000000000012004">
                <desc languageCode="en" languageRefsetId="900000000000509007">Nonconformance to editorial policy component</desc>
            </value>
            <value id="900000000000485001" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">foutief component</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Erroneous</desc>
            </value>
            <value id="900000000000486000" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">component van beperkte waarde</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Limited</desc>
            </value>
        </associationsValues>
    }
;

declare variable $snomedGPSmember       :=
    try { doc(concat(repo:get-root(), 'terminology-data/snomed-data/meta/snomed-gps.xml'))/gpsmembers }
    catch * {
        (: only a few on error :)
        <gpsmembers fallback="true">
            <code code="426263006"/>
            <code code="426282002"/>
            <code code="426284001"/>
            <code code="426290002"/>
            <code code="426347000"/>
            <code code="426361005"/>
            <code code="426367009"/>
            <code code="426373005"/>
            <code code="426389008"/>
            <code code="426396005"/>
            <code code="426417003"/>
            <code code="426424002"/>
            <code code="426439001"/>
            <code code="426000000"/>
        </gpsmembers>
    }
;

declare %private function local:getDisplayNameAndStatus($code as xs:string, $codeSystem as xs:string, $defaultLanguage as xs:string, $allLanguages as xs:string*) as element()* {
    if ($codeSystem = $codeSystemSNOMED) then
        let $lang                       := tokenize($defaultLanguage, '-')[1]
        let $languageRefsetId           := 
            switch ($defaultLanguage)
            case 'nl-NL' return '31000146106'
            case 'de-DE' return '722130004'
            default return '900000000000509007'
        let $fallbackLanguageRefsetId   := '900000000000509007'
        (:let $service-uri        := concat($artDeepLinkTerminology,'snomed/getConcept/',encode-for-uri($code)):)
        (: protection against empty responses. This leads to 
            exerr:ERROR Unable to add Source to result:Premature end of file. [source: String] :)
        (:let $server-response    := try { http:send-request($requestHeaders, $service-uri) } catch * {()}
        
        let $concept        := $server-response//concept[@conceptId = $code]:)
        
        let $concept        := $snomed:colDataBase//@conceptId[. = $code]/parent::concept
        let $displayNames   := $concept/desc[@active]
        let $statusCode     := if ($concept[@active]) then 'active' else if ($concept) then 'deprecated' else ()
        let $statusText     := $statusCode
        
        let $deprecation    := $concept/attributeValue[@active][@refsetId = '900000000000489007']
        let $deprCode       := $deprecation/@valueId
        let $deprText       := $snomedAssociations//value[@id = $deprCode]
        let $deprText       := ($deprText/desc[@languageRefsetId = $languageRefsetId], $deprText/desc[@languageRefsetId = $fallbackLanguageRefsetId])
        let $deprText       := if ($deprText) then $deprText[1] else $deprecation[1]/@refset
        let $l10nMissing    := empty($displayNames[@languageRefsetId = $languageRefsetId])
        return
            <codeSystem id="{$codeSystem}">
            {
                if (empty($concept)) then attribute missing {'true'} else (),
                if ($localizationcheck and $l10nMissing) then attribute missingTranslation {$l10nMissing} else (),
                if (empty($statusCode)) then () else attribute originalStatusCode {$statusCode},
                if (empty($statusText)) then () else attribute originalStatusText {$statusText},
                if ($deprCode) then attribute deprecationCode {$deprCode} else (),
                if ($deprText) then attribute deprecationText {$deprText} else ()
            }
            {
                for $displayName in $displayNames
                return
                    <desc language="{$displayName/@languageCode}" originalDisplayName="{$displayName}">
                    {
                        attribute type {
                            switch ($displayName/@type) 
                            case 'fsn' return 'fsn' 
                            case 'pref' return 'preferred' 
                            case 'syn' return 'synonym' 
                            default return $displayName/@type
                        },
                        attribute preferredDisplayName {
                            (: preferred term if in the right language refset and fsn :)
                            if ($displayNames[@languageRefsetId = $languageRefsetId][@type = 'fsn']) then 
                                $displayName[@languageRefsetId = $languageRefsetId]/@type = 'fsn'
                            else
                            (: preferred term if in the right language refset that has no fsn, but has preferred :)
                            if ($displayNames[@languageRefsetId = $languageRefsetId][@type = 'pref']) then 
                                $displayName[@languageRefsetId = $languageRefsetId]/@type = 'pref'
                            else
                            (: preferred term if in the fallback language refset and fsn :)
                            if ($displayNames[@languageRefsetId = $fallbackLanguageRefsetId][@type = 'fsn']) then
                                $displayName[@languageRefsetId = $fallbackLanguageRefsetId]/@type = 'fsn'
                            else
                            (: preferred term if in the fallback language refset that has no fsn, but has preferred :)
                            if ($displayNames[@languageRefsetId = $fallbackLanguageRefsetId][@type = 'pref']) then 
                                $displayName[@languageRefsetId = $fallbackLanguageRefsetId]/@type = 'pref'
                            else (
                                (: preferred term if not available in the right refset, but at least in the core or the right language as fsn :)
                                $displayName[empty(@languageRefsetId)]/@type = 'fsn' or $displayName[@languageCode = $lang]/@type = 'fsn'
                            )
                        },
                        $displayName/(@* except (@language | @originalDisplayName | @type | @preferred))
                    }
                    </desc>
            }
            {
                (:<!-- attribueValue | associations -->:)
                (:<!-- The refsets MOVE TO (900000000000524003) and MOVED FROM (900000000000525002) refer to a module that a concept was moved into or out of -->:)
                (:<!-- REFERS TO is for descriptions and may be ignored (900000000000531004) -->:)
                (:<!-- SIMILAR TO is no longer in use (900000000000529008) -->:)
                for $assocs in $concept/association[@active][not(@refsetId = '900000000000531004')]
                let $refsetId   := $assocs/@refsetId
                group by $refsetId
                return (
                    let $refset     := ($snomedAssociations//refset[@id = $refsetId], $snomedRefsets//refset[@id = $refsetId])
                    let $refsetName := ($refset/desc[@languageRefsetId = $languageRefsetId], $refset/desc[@languageRefsetId = $fallbackLanguageRefsetId])
                    let $refsetName := if ($refsetName) then $refsetName[1] else $assocs[1]/@refset
                    
                    for $assoc in $assocs 
                    return 
                        <association>{$assoc/(@* except @refset), attribute refset {$refsetName}, attribute uri {concat($artDeepLink,'snomed-ct?conceptId=',encode-for-uri($assoc/@targetComponentId))}}</association>
                )
            }
            </codeSystem>
    else 
    if ($codeSystem = $codeSystemLOINC and starts-with($code, 'LA')) then () else
    if ($codeSystem = $codeSystemLOINC) then
        (:let $service-uri        := concat($artDeepLinkTerminology,'loinc/searchLOINC/',encode-for-uri($code)):)
        (: protection against empty responses. This leads to 
            exerr:ERROR Unable to add Source to result:Premature end of file. [source: String] :)
        (:let $server-response    := try { http:send-request($requestHeaders, $service-uri) } catch * {()}:)
        
        (:let $concept        := $server-response//concept[@loinc_num = $code]:)
        
        let $concept        := collection(concat($adloinc:strLoincData,$adloinc:anyLanguage))//concept[@loinc_num = $code][1]
        let $displayNames   := $concept//longName | $concept//shortName
        let $statusText     := $concept/@status
        let $statusCode     := 
            if (empty($statusText)) then () else 
            if (lower-case($statusText)='active')                       then 'active' else 
            if (lower-case($statusText)='trial')                        then 'pending' else 
            if (lower-case($statusText)=('deprecated','discouraged'))   then 'deprecated' else (
                lower-case($statusText)
            )
        
        let $deprecation    := $concept/elem[@name = 'STATUS_REASON']
        let $deprCode       := $deprecation
        let $deprText       := $deprecation
        let $l10nlang       := $displayNames/ancestor::concept[1]/@language
        let $l10nMissing    := if (empty($l10nlang)) then not($defaultLanguage = 'en-US') else not($defaultLanguage = $l10nlang)
        return
            <codeSystem id="{$codeSystem}">
            {
                if (empty($concept)) then attribute missing {'true'} else (),
                (:if ($localizationcheck and $l10nMissing) then attribute missingTranslation {$l10nMissing} else (),:)
                if (empty($statusCode)) then () else attribute originalStatusCode {$statusCode},
                if (empty($statusText)) then () else attribute originalStatusText {$statusText},
                if ($deprCode) then attribute deprecationCode {$deprCode} else (),
                if ($deprText) then attribute deprecationText {$deprText} else ()
            }
            {
                for $displayName in $displayNames
                let $lang   := $displayName/ancestor::concept[1]/@language
                return
                    <desc language="{if ($lang) then $lang else 'en-US'}" originalDisplayName="{$displayName}"/>
            }
            {
                for $assoc in $concept/map[@from = $code]
                return
                    <association targetComponentId="{$assoc/@to}" uri="{concat($artDeepLink,'loinc?conceptId=',encode-for-uri($assoc/@to))}"/>
            }
            </codeSystem>
    else (
        (:let $service-uri        := concat($artDeepLinkTerminology,'claml/RetrieveClass?mode=xml&amp;classificationId=',$codeSystem,'&amp;code=',encode-for-uri($code)):)
        (: protection against empty responses. This leads to 
            exerr:ERROR Unable to add Source to result:Premature end of file. [source: String] :)
        (:let $server-response    := try { http:send-request($requestHeaders, $service-uri) } catch * {()}:)
        
        (:let $concept        := $server-response//Class[@code=$code][@classificationId=$codeSystem]:)
        
        let $concept        := claml:getPreparedClass((), $codeSystem, $code, '')
        let $displayNames   := $concept/Rubric[@kind='preferred']/Label
        let $statusCode     := $concept/Meta[@name='statusCode']/@value
        let $statusText     := $statusCode
        let $l10nMissing    := empty($displayNames[@xml:lang = $defaultLanguage])
        return
            <codeSystem id="{$codeSystem}">
            {
                if (empty($concept)) then attribute missing {'true'} else (),
                (:if ($localizationcheck and $l10nMissing) then attribute missingTranslation {$l10nMissing} else (),:)
                if (empty($statusCode)) then () else attribute originalStatusCode {$statusCode},
                if (empty($statusText)) then () else attribute originalStatusText {$statusText}
            }
            {
                for $displayName in $displayNames
                return
                    <desc>
                    {
                        if ($displayName[@xml:lang]) then (attribute language {$displayName/@xml:lang}) else (),
                        attribute originalDisplayName {$displayName},
                        attribute preferredDisplayName {$displayName/@xml:lang = $defaultLanguage}
                    }
                    </desc>
            }
            </codeSystem>
    )
};

let $projectPrefix      := if (request:exists()) then request:get-parameter('prefix',())[string-length() gt 0] else ()
let $projectVersion     := if (request:exists()) then request:get-parameter('version',())[string-length() gt 0] else ()
let $searchCode         := if (request:exists()) then request:get-parameter('code',())[string-length() gt 0][1] else ()
let $searchCodeSystem   := if (request:exists()) then request:get-parameter('codeSystem',())[string-length() gt 0][1] else ()
let $language           := if (request:exists()) then request:get-parameter('language', ())[string-length() gt 0] else ()
let $format             := if (request:exists()) then request:get-parameter('format','html')[string-length() gt 0] else ()

let $decor              :=
    if (empty($projectPrefix)) then
        $get:colDecorData/decor
    else
    if (empty($projectVersion)) then
        $get:colDecorData/decor[project/@prefix=$projectPrefix]
    else
        $get:colDecorVersion/decor[@versionDate=$projectVersion][project/@prefix=$projectPrefix][1]

let $language           := 
    if (empty($language)) then 
        if (count($decor) = 1) then $decor/ancestor-or-self::decor/project/@defaultLanguage else $get:strArtLanguage
    else ($language)
let $allLanguages       := distinct-values($decor/ancestor-or-self::decor/project/name/@language)
let $codes              := 
    if (empty($searchCode) or empty($searchCodeSystem)) then () else (
        $decor//terminologyAssociation[@code=$searchCode][@codeSystem=$searchCodeSystem] |
        $decor//concept[@code=$searchCode][@codeSystem=$searchCodeSystem] |
        $decor//vocabulary[@code=$searchCode][@codeSystem=$searchCodeSystem] |
        $decor//attribute[@code=$searchCode][@codeSystem=$searchCodeSystem]
    )
let $codeSystemName     := art:getNameForOID($searchCodeSystem, $language[1], $decor[1])

let $strTermAssoc       := i18n:getMessage($docMessages,'TerminologyAssociation',$language)
let $strValueSet        := i18n:getMessage($docMessages,'ValueSet',$language)
let $strCodeSystem      := i18n:getMessage($docMessages,'columnCodeSystem',$language)
let $strTemplate        := i18n:getMessage($docMessages,'Template',$language)

return 
    if ($format='xml') then (
        if (request:exists()) then (
            response:set-header('Content-Type','application/xml')
        ) else (),
        <result>
        {
            $codes,
            if (empty($searchCode) or empty($searchCodeSystem)) then () else local:getDisplayNameAndStatus($searchCode, $searchCodeSystem, $language, $allLanguages)
        }
        </result>
    ) else
    if ($format = 'csv') then (
        if (request:exists()) then (
            response:set-header('Content-Type','text/csv; charset=utf-8'),
            response:set-header('Content-Disposition', concat('filename=Code ',$searchCode[1],' (download ',substring(string(current-dateTime()),1,19),').csv'))
        ) else ()
        ,
        (:<concept code="xxxxxx" codeSystem="2.16.840.1.113883.2.4.15.4" displayName="Medicatie" level="0" type="A"/> :)
        (: Replace double quotes with single quotes in the CSV values, except in the code itself, 
        and place in between double quotes if there a white space character in a string
        Note that in the exceptional event that a code contains a double quote, the CSV renders invalid :)
        concat('Code;DisplayName;CodeSystem;CodeSystemName;CodeSystemVersion;Status;Type;Language;Source;Project','&#13;&#10;'),
        for $code at $pos in $codes | $codes/designation
        let $conceptCode                    := data($searchCode)
        let $quotedConceptCode              := if (matches($conceptCode,'\s+')) then (concat('&quot;',$conceptCode,'&quot;')) else ($conceptCode)
        let $conceptDisplayName             := replace(data($code/@displayName),'"','&apos;')
        let $quotedConceptDisplayName       := if (matches($conceptDisplayName,'\s+')) then (concat('&quot;',$conceptDisplayName,'&quot;')) else ($conceptDisplayName)
        let $conceptCodeSystem              := replace(data($searchCodeSystem),'"','&apos;')
        let $quotedConceptCodeSystem        := if (matches($conceptCodeSystem,'\s+')) then (concat('&quot;',$conceptCodeSystem,'&quot;')) else ($conceptCodeSystem)
        let $generatedCodeSystemName        := art:getNameForOID($searchCodeSystem,$language,$projectPrefix)
        let $conceptCodeSystemName          := replace(if (replace($generatedCodeSystemName,'[0-9\.]','')!='') then ($generatedCodeSystemName) else (''),'"','&apos;')
        let $quotedConceptCodeSystemName    := if (matches($conceptCodeSystemName,'\s+')) then (concat('&quot;',$conceptCodeSystemName,'&quot;')) else ($conceptCodeSystemName)
        let $conceptCodeSystemVersion       := replace(data($code/@codeSystemVersion),'"','&apos;')
        let $quotedConceptCodeSystemVersion := if (matches($conceptCodeSystemVersion,'\s+')) then (concat('&quot;',$conceptCodeSystemVersion,'&quot;')) else ($conceptCodeSystemVersion)
        let $conceptStatus                  := replace(if ($code/@type = 'D') then 'deprecated' else 'active','"','&apos;')
        let $quotedConceptStatus            := if (matches($conceptStatus,'\s+')) then (concat('&quot;',$conceptStatus,'&quot;')) else ($conceptStatus)
        let $conceptType                    := replace(data($code/@type),'"','&apos;')
        let $quotedConceptType              := if (matches($conceptType,'\s+')) then (concat('&quot;',$conceptType,'&quot;')) else ($conceptType)
        let $conceptLanguage                := replace(data($code/@language),'"','&apos;')
        let $quotedConceptLanguage          := if (matches($conceptLanguage,'\s+')) then (concat('&quot;',$conceptLanguage,'&quot;')) else ($conceptLanguage)
        let $conceptSource                  := 
                                        if ($code/ancestor-or-self::terminologyAssociation) then $strTermAssoc else
                                        if ($code/ancestor-or-self::valueSet) then $strValueSet else
                                        if ($code/ancestor-or-self::template) then $strTemplate else
                                        if ($code/ancestor-or-self::codeSystem) then $strCodeSystem else (
                                            '?'
                                        )
        let $quotedConceptSource            := if (matches($conceptSource,'\s+')) then (concat('&quot;',$conceptSource,'&quot;')) else ($conceptSource)
        let $codeProject                    := replace(data($code/ancestor::decor/project/@prefix),'"','&apos;')
        let $quotedCodeProject              := if (matches($codeProject,'\s+')) then (concat('&quot;',$codeProject,'&quot;')) else ($codeProject)
        return
            concat($quotedConceptCode,';',$quotedConceptDisplayName,';',$quotedConceptCodeSystem,';',$quotedConceptCodeSystemName,';',$quotedConceptCodeSystemVersion,';',$quotedConceptStatus,';',$quotedConceptType,';',$quotedConceptLanguage,';',$quotedConceptSource,';',$quotedCodeProject,'&#13;&#10;')
        ,
        if (empty($searchCode) or empty($searchCodeSystem)) then () else (
            for $code at $pos in local:getDisplayNameAndStatus($searchCode, $searchCodeSystem, $language, $allLanguages)/desc
            let $conceptCode                    := data($searchCode)
            let $quotedConceptCode              := if (matches($conceptCode,'\s+')) then (concat('&quot;',$conceptCode,'&quot;')) else ($conceptCode)
            let $conceptDisplayName             := replace(data($code/@originalDisplayName),'"','&apos;')
            let $quotedConceptDisplayName       := if (matches($conceptDisplayName,'\s+')) then (concat('&quot;',$conceptDisplayName,'&quot;')) else ($conceptDisplayName)
            let $conceptCodeSystem              := replace(data($searchCodeSystem),'"','&apos;')
            let $quotedConceptCodeSystem        := if (matches($conceptCodeSystem,'\s+')) then (concat('&quot;',$conceptCodeSystem,'&quot;')) else ($conceptCodeSystem)
            let $generatedCodeSystemName        := art:getNameForOID($searchCodeSystem,$language,$projectPrefix)
            let $conceptCodeSystemName          := replace(if (replace($generatedCodeSystemName,'[0-9\.]','')!='') then ($generatedCodeSystemName) else (''),'"','&apos;')
            let $quotedConceptCodeSystemName    := if (matches($conceptCodeSystemName,'\s+')) then (concat('&quot;',$conceptCodeSystemName,'&quot;')) else ($conceptCodeSystemName)
            let $conceptCodeSystemVersion       := replace('','"','&apos;')
            let $quotedConceptCodeSystemVersion := if (matches($conceptCodeSystemVersion,'\s+')) then (concat('&quot;',$conceptCodeSystemVersion,'&quot;')) else ($conceptCodeSystemVersion)
            let $conceptStatus                  := replace(data($code/../@originalStatusCode),'"','&apos;')
            let $quotedConceptStatus            := if (matches($conceptStatus,'\s+')) then (concat('&quot;',$conceptStatus,'&quot;')) else ($conceptStatus)
            let $conceptType                    := replace(data($code/@type),'"','&apos;')
            let $quotedConceptType              := if (matches($conceptType,'\s+')) then (concat('&quot;',$conceptType,'&quot;')) else ($conceptType)
            let $conceptLanguage                := replace(data($code/@language),'"','&apos;')
            let $quotedConceptLanguage          := if (matches($conceptLanguage,'\s+')) then (concat('&quot;',$conceptLanguage,'&quot;')) else ($conceptLanguage)
            let $quotedConceptSource            := if (matches($strCodeSystem,'\s+')) then (concat('&quot;',$strCodeSystem,'&quot;')) else ($strCodeSystem)
            return
                concat($quotedConceptCode,';',$quotedConceptDisplayName,';',$quotedConceptCodeSystem,';',$quotedConceptCodeSystemName,';',$quotedConceptCodeSystemVersion,';',$quotedConceptStatus,';',$quotedConceptType,';',$quotedConceptLanguage,';',$quotedConceptSource,';','&#13;&#10;')
        )
    ) else
    if ($format='html' and (empty($searchCode) or empty($searchCodeSystem))) then (
        if (request:exists()) then (
            response:set-header('Content-Type','text/html')
        ) else (),
        <html>
            <head>
                <title>RetrieveCode: {$searchCode} / {$searchCodeSystem}</title>
                <link href="{$resourcePath}/css/default.css" rel="stylesheet" type="text/css"></link>
                <script src="resources/scripts/retrievecode.js" type="text/javascript"></script>
            </head>
            <body>
                <h1>RetrieveCode</h1>
                <div class="content">
                <form name="input" action="RetrieveCode" method="get">
                <input type="hidden" name="language" value="{$language}"/>
                    <table border="0">
                        <tr>
                            <td style="width: 25%; vertical-align: top;">{i18n:getMessage($docMessages,'columnCode',$language)}</td>
                            <td><input name="code" id="codeSelector" style="width: 300px;"/></td>
                        </tr>
                        <tr>
                            <td style="width: 25%; vertical-align: top;">{i18n:getMessage($docMessages,'columnCodeSystem',$language)}</td>
                            <td>
                                <div>
                                    <select style="width: 300px;" id="dropdownSystems" onchange="handleSelectSystemChange(this)">
                                        <!--option value="verbatim">verbatim</option-->
                                        <option value="''">---</option>
                                        <option value="{$codeSystemSNOMED}">{if ($searchCodeSystem = $codeSystemSNOMED) then attribute selected {'selected'} else ()}SNOMED CT</option>
                                        <option value="{$codeSystemLOINC}">{if ($searchCodeSystem = $codeSystemLOINC) then attribute selected {'selected'} else ()}LOINC</option>
                                    {
                                        for $cs in $codeSystemsCLAML
                                        order by lower-case($cs/@name)
                                        return
                                            <option value="{$cs/@id}">{if ($searchCodeSystem = $cs/@id) then attribute selected {'selected'} else ()}{data($cs/@name)}</option>
                                    }
                                    </select>
                                </div>
                                <div>
                                    <input name="codeSystem" id="codeSystemSelector" style="width: 300px;" value="{$searchCodeSystem}" onchange="handleInputSystemChange(this)"></input>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;" colspan="2">
                                <input type="submit" value="{i18n:getMessage($docMessages,'Send',$language)}" style="font-weight: bold; color: black;"/>
                            </td>
                        </tr>
                    </table>
                </form>
                </div>
            </body>
        </html>
    )
    else
    if ($format='html') then (
        if (request:exists()) then (
            response:set-header('Content-Type','text/html')
        ) else (),
        <html>
            <head>
                <title>RetrieveCode: {$searchCode} / {$searchCodeSystem}</title>
                <link href="{$resourcePath}/css/default.css" rel="stylesheet" type="text/css"></link>
                <style type="text/css">
                    .zebra-table {{ border-collapse: collapse; border: 1px solid gray; }}
                    .zebra-table td {{ padding: 6px; }}
                    .zebra-row-even {{ background-color: #eee; }}
                    .zebra-row-odd {{ background-color: #fff; }}
                    .node-sdraft, .node-spending, .node-sreview, .node-srejected, .node-snew,
                    .node-sopen, .node-sclosed, .node-scancelled, .node-sdeprecated, .node-sretired,
                    .node-sfinal, .node-sactive, .node-sinactive, .node-supdate, .node-s {{ 
                        background-repeat:no-repeat;
                        padding:0 0 0 18px;
                        vertical-align: middle;
                        line-height:18px;
                    }}
                    .node-sdraft      {{ background-image:url('{$resourcePath2}img/node-sdraft.png') }}
                    .node-spending    {{ background-image:url('{$resourcePath2}img/node-spending.png') }}
                    .node-sreview     {{ background-image:url('{$resourcePath2}img/node-sreview.png') }}
                    .node-srejected   {{ background-image:url('{$resourcePath2}img/node-srejected.png') }}
                    .node-snew        {{ background-image:url('{$resourcePath2}img/node-snew.png') }}
                    .node-sopen       {{ background-image:url('{$resourcePath2}img/node-sopen.png') }}
                    .node-sclosed     {{ background-image:url('{$resourcePath2}img/node-sclosed.png') }}
                    .node-scancelled  {{ background-image:url('{$resourcePath2}img/node-scancelled.png') }}
                    .node-sdeprecated {{ background-image:url('{$resourcePath2}img/node-sdeprecated.png') }}
                    .node-sretired    {{ background-image:url('{$resourcePath2}img/node-sretired.png') }}
                    .node-sfinal      {{ background-image:url('{$resourcePath2}img/node-sfinal.png') }}
                    .node-sactive     {{ background-image:url('{$resourcePath2}img/node-sactive.png') }}
                    .node-sinactive   {{ background-image:url('{$resourcePath2}img/node-sinactive.png') }}
                    .node-supdate     {{ background-image:url('{$resourcePath2}img/node-supdate.png') }}
                    .node-s           {{ background-image:url('{$resourcePath2}img/node-s.png') }}
                </style>
            </head>
            <body>
                <h1>{i18n:getMessage($docMessages,'columnCode',$language)}: {$searchCode} - {art:getNameForOID($searchCodeSystem, $language, $projectPrefix)}</h1>
                <p>
                - {i18n:getMessage($docMessages,'displayAs',$language)}<a href="RetrieveCode?format=xml{string-join(for $p in request:get-parameter-names()[not(.='format')] return concat('&amp;',$p,'=',request:get-parameter($p,())[string-length() gt 0]),'')}" alt="">XML</a>
                - {i18n:getMessage($docMessages,'displayAs',$language)}<a href="RetrieveCode?format=csv{string-join(for $p in request:get-parameter-names()[not(.='format')] return concat('&amp;',$p,'=',request:get-parameter($p,())[string-length() gt 0]),'')}" alt="">CSV</a>
                <span style="float:right;">
                    <img src="/art-decor/img/flags/nl.png" onclick="location.href=window.location.pathname+'?language=nl-NL{string-join(for $p in request:get-parameter-names() return if ($p='language') then () else concat('&amp;',$p,'=',request:get-parameter($p,())[string-length() gt 0]),'')}';" class="linked flag"/>
                    <img src="/art-decor/img/flags/de.png" onclick="location.href=window.location.pathname+'?language=de-DE{string-join(for $p in request:get-parameter-names() return if ($p='language') then () else concat('&amp;',$p,'=',request:get-parameter($p,())[string-length() gt 0]),'')}';" class="linked flag"/>
                    <img src="/art-decor/img/flags/us.png" onclick="location.href=window.location.pathname+'?language=en-US{string-join(for $p in request:get-parameter-names() return if ($p='language') then () else concat('&amp;',$p,'=',request:get-parameter($p,())[string-length() gt 0]),'')}';" class="linked flag"/>
                </span>
                </p>
                <div class="content">
                    <table width="100%" cellpadding="5px">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>{i18n:getMessage($docMessages,'columnCode',$language)}</th>
                                <th>{i18n:getMessage($docMessages,'columnCodeSystem',$language)}</th>
                                {
                                    if ($codes[@codeSystemVersion]) then 
                                        <th>{i18n:getMessage($docMessages,'columnCodeSystemVersion',$language)}</th>
                                    else ()
                                }
                                <th>{i18n:getMessage($docMessages,'columnDisplayName',$language)}</th>
                                <!--<th>{i18n:getMessage($docMessages,'columnStatus',$language)}</th>-->
                                <th>{i18n:getMessage($docMessages,'columnType',$language)}</th>
                                <th>{i18n:getMessage($docMessages,'language',$language)}</th>
                                <th>{i18n:getMessage($docMessages,'columnSource',$language)}</th>
                            </tr>
                        </thead>
                        <tbody>
                        {
                            for $code at $pos in $codes | $codes/designation
                            return
                                <tr class="zebra-row-{if ($pos mod 2 = 0) then 'even' else 'odd'}">
                                    <td>{$pos}</td>
                                    <td>
                                    {
                                        if ($code/ancestor-or-self::terminologyAssociation) then (
                                            let $concept    := art:getConcept($code/@conceptId, $code/@conceptFlexibility)
                                            
                                            return
                                            <a href="{concat($resourcePath2, 'decor-terminology--', $concept/ancestor::decor/project/@prefix, '?datasetId=', $concept/ancestor::dataset/@id, '&amp;datasetEffectiveDate=', encode-for-uri($concept/ancestor::dataset/@effectiveDate), '&amp;conceptId=', $concept/@id, '&amp;conceptEffectiveDate=', encode-for-uri($concept/@effectiveDate))}" alt="" target="_new">
                                            {
                                                data($code/@code)
                                            }
                                            </a>
                                        )
                                        else
                                        if ($code/ancestor-or-self::valueSet) then 
                                            <a href="{concat($resourcePath2, 'decor-valuesets--', $code/ancestor::decor/project/@prefix, '?id=', $code/ancestor-or-self::valueSet/@id, '&amp;effectiveDate=', encode-for-uri($code/ancestor-or-self::valueSet/@effectiveDate))}" alt="" target="_new">
                                            {
                                               data($code/@code)
                                            }
                                            </a>
                                        else
                                        if ($code/ancestor-or-self::template) then 
                                            <a href="{concat($resourcePath2, 'decor-templates--', $code/ancestor::decor/project/@prefix, '?id=', $code/ancestor-or-self::template/@id, '&amp;effectiveDate=', encode-for-uri($code/ancestor-or-self::template/@effectiveDate))}" alt="" target="_new">
                                            {
                                                data($code/@code)
                                            }
                                            </a>
                                        else
                                        if ($code/ancestor-or-self::codeSystem) then
                                            <a href="{concat($resourcePath2, 'decor-codesystems--', $code/ancestor::decor/project/@prefix, '?id=', $code/ancestor-or-self::codeSystem/@id, '&amp;effectiveDate=', encode-for-uri($code/ancestor-or-self::codeSystem/@effectiveDate))}" alt="" target="_new">
                                            {
                                                data($code/@code)
                                            }
                                            </a>
                                        else (
                                            data($code/@code)
                                        )
                                    }
                                    </td>
                                    <td>{$code/@codeSystem/string()}</td>
                                    {
                                        if ($codes[@codeSystemVersion]) then 
                                            <td>{$code/@codeSystemVersion/string()}</td>
                                        else ()
                                    }
                                    <td>{$code/@displayName/string()}</td>
                                    <!--<td>{if ($code[@type='D']) then 'deprecated' else 'active'}</td>-->
                                    <td>{$code[self::designation]/@type/string()}</td>
                                    <td>{$code/@language/string()}</td>
                                    <td>
                                    <span class="node-s{if ($code[@type='D']) then 'deprecated' else 'active'}">
                                    {
                                        if ($code/ancestor-or-self::terminologyAssociation) then $strTermAssoc else
                                        if ($code/ancestor-or-self::valueSet) then $strValueSet else
                                        if ($code/ancestor-or-self::template) then $strTemplate else
                                        if ($code/ancestor-or-self::codeSystem) then $strCodeSystem else (
                                            '?'
                                        )
                                        , string-join((' - ', $code/ancestor::decor/project/(name[@language = $language], name)[1], ' (', $code/ancestor::decor/project/@prefix, ')'), '')
                                    }
                                    </span>
                                    </td>
                                </tr>
                        }
                        {
                            let $errorimg           := <img src="{$resourcePath2}img/error.png" alt="" title="{i18n:getMessage($docMessages, 'trConceptNotFoundOrMismatch', $language)}" style="padding:0 8px 0 0; vertical-align: middle;"/>
                            let $okimg              := <img src="{$resourcePath2}img/IssueStatusCodeLifeCycle_closed.png" alt="" title="{i18n:getMessage($docMessages, 'trNoProblemsFound', $language)}" style="padding:0 8px 0 0; vertical-align: middle;"/>
                            let $notsnomedgpsimg    := <img src="{$resourcePath2}img/error.png" alt="" title="SNOMED Global Patient Set (GPS)" style="padding:0 8px 0 0; vertical-align: middle;"/>
                            let $snomedgpsimg       := <img src="{$resourcePath2}img/IssueStatusCodeLifeCycle_closed.png" alt="" title="SNOMED Global Patient Set (GPS)" style="padding:0 8px 0 0; vertical-align: middle;"/>
                            let $nol10nimg          := <img src="{$resourcePath2}img/error.png" alt="" title="{i18n:getMessage($docMessages, 'translationMissing', $language)}" style="padding:0 8px 0 0; vertical-align: middle;"/>
                            let $l10nimg            := <img src="{$resourcePath2}img/IssueStatusCodeLifeCycle_closed.png" alt="" title="{i18n:getMessage($docMessages, 'translationFound', $language)}" style="padding:0 8px 0 0; vertical-align: middle;"/>
                            return
                            if (empty($searchCode) or empty($searchCodeSystem)) then () else (
                                for $concept at $pos in local:getDisplayNameAndStatus($searchCode, $searchCodeSystem, $language, $allLanguages)
                                return
                                    <tr class="zebra-row-{if (count($codes) mod 2 = 0) then (if ($pos mod 2 = 0) then 'even' else 'odd') else (if ($pos mod 2 = 0) then 'odd' else 'even')}">
                                        <td>{$pos + count($codes | $codes/designation)}</td>
                                        <td>
                                        {
                                            if ($searchCodeSystem = $codeSystemSNOMED) then
                                                <a href="{concat($artDeepLink, 'snomed-ct?conceptId=', $searchCode)}" alt="" target="_new">
                                                {
                                                    data($searchCode)
                                                }
                                                </a>
                                            else
                                            if ($searchCodeSystem = $codeSystemLOINC) then
                                                <a href="{concat($artDeepLink, 'loinc?conceptId=', $searchCode)}" alt="" target="_new">
                                                {
                                                    data($searchCode)
                                                }
                                                </a>
                                            else
                                            if ($codeSystemsCLAML[@id = $searchCodeSystem]) then
                                                <a href="{concat($artDeepLink, 'claml?classificationId=', $searchCodeSystem, 'conceptId=', $searchCode)}" alt="" target="_new">
                                                {
                                                    data($searchCode)
                                                }
                                                </a>
                                            else (
                                                data($searchCode)
                                            )
                                        }
                                        </td>
                                        <td>{data($searchCodeSystem)}</td>
                                        {
                                            if ($codes[@codeSystemVersion]) then 
                                                <td></td>
                                            else ()
                                        }
                                        <td>{for $desc in $concept/desc return <div>{(data($desc/@originalDisplayName), '&#160;')[1]}</div>}</td>
                                        <!--<td>{$concept/../@originalStatusCode/string()}</td>-->
                                        <td>{for $desc in $concept/desc return <div>{(data($desc/@type), '&#160;')[1]}</div>}</td>
                                        <td>{for $desc in $concept/desc return <div>{(data($desc/@language), '&#160;')[1]}</div>}</td>
                                        <!--<td><span class="node-s{$concept/@originalStatusCode}" title="{$concept/@originalStatusCode}">{$strCodeSystem, $codeSystemName}</span></td>-->
                                        <td style="vertical-align: top;">
                                            <div>{$strCodeSystem}</div>
                                        {
                                            if ($concept/@isSnomedGPSMember="true")
                                            then (<div>{ $snomedgpsimg, <span>SNOMED GPS</span>}</div>)
                                            else if ($concept/@isSnomedGPSMember="false")
                                            then (<div>{ $notsnomedgpsimg, <span>Not SNOMED GPS</span>}</div>)
                                            else ()
                                        }
                                        {
                                            if ($concept/@missing) then () else
                                            if ($concept/@missingTranslation="true")
                                            then (<div>{ $nol10nimg, <span>{i18n:getMessage($docMessages, 'translationMissing', $language)}</span>}</div>)
                                            else if ($concept/@missingTranslation="false")
                                            then (<div>{ $l10nimg, <span>{i18n:getMessage($docMessages, 'translationFound', $language)}</span>}</div>)
                                            else ()
                                        }
                                        {
                                            let $imgtitle       :=
                                                if ($concept/@conceptType='concept') then
                                                    i18n:getMessage($docMessages, 'trConceptNotFoundOrMismatchName', $language)
                                                else if ($concept/@conceptType='conceptListConcept') then
                                                    i18n:getMessage($docMessages, 'trConceptNotInValueSetOrMismatchName', $language)
                                                else (
                                                    i18n:getMessage($docMessages, 'trConceptTypeUnknownOrMismatchName', $language)
                                                )
                                            return
                                            if ($concept[@missing]) then
                                                <img src="{$errorimg/@src}" alt="{$errorimg/@alt}" title="{$imgtitle}" style="{$errorimg/@style}"/>
                                            else
                                            if ($concept[string-length(@originalDisplayName) gt 0]) then 
                                                <img src="{$errorimg/@src}" alt="{$errorimg/@alt}" title="{$imgtitle}" style="{$errorimg/@style}"/>
                                            else 
                                            if ($concept/@originalStatusCode[not(. = 'active')]) then 
                                                <img src="{$errorimg/@src}" alt="{$errorimg/@alt}" title="{$imgtitle}" style="{$errorimg/@style}"/>
                                            else ((:$okimg:))
                                        }
                                        {
                                            if ($concept/@originalStatusCode[not(. = 'active')]) then
                                                <span class="node-s{$concept/@originalStatusCode}" title="{$concept/@originalStatusText}">{data($concept/@originalStatusText)}</span>
                                            else
                                            if ($concept/..[@missing]) then
                                                <span class="node-s{$concept/@originalStatusCode}" style="color: red;" title="{$concept/@originalStatusText}">{i18n:getMessage($docMessages, 'codeNotFound', $language)}</span>
                                            else
                                                data($concept/@originalDisplayName)
                                        }
                                        {
                                            if ($concept/@deprecationCode) then ( 
                                                <div style="margin-left: 2.5em;">
                                                {   
                                                    i18n:getMessage($docMessages, 'Reason', $language), ': ',
                                                    <strong>{data(($concept/@deprecationText, $concept/@deprecationCode)[1])}</strong>
                                                    ,
                                                    for $assoc in $concept/association
                                                    let $deeplinktoassoc    := $assoc/@uri
                                                    let $assocDisplay       := if ($assoc/@targetComponent) then concat($assoc/@targetComponentId, ' | ', $assoc/@targetComponent, ' |') else data($assoc/@targetComponentId)
                                                    return (
                                                        <div>
                                                        {
                                                            i18n:getMessage($docMessages, 'columnReference', $language), ': ', 
                                                            if ($deeplinktoassoc) then 
                                                                <a href="{$deeplinktoassoc}" alt="" target="_new">
                                                                {
                                                                    if ($assoc/@refset) then (attribute title {$assoc/@refset}) else (),
                                                                    $assocDisplay
                                                                }
                                                                </a>
                                                            else (
                                                                $assocDisplay
                                                            )
                                                        }
                                                        </div>
                                                    )
                                                }
                                                </div>
                                            ) else ()
                                        }
                                        </td>
                                    </tr>
                            )
                        }
                        </tbody>
                    </table>
                </div>
            </body>
        </html>
    )
    else (
        if (request:exists()) then (
            response:set-header('Content-Type','application/xml')
        ) else ()
        ,
        <warning>{i18n:getMessage($docMessages,'errorNotImplementedYet',$language),' ',if (request:exists()) then (request:get-query-string()) else ()}</warning>
    )