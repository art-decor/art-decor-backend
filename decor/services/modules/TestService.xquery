xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace i18n        = "http://art-decor.org/ns/decor/i18n" at "../../../art/api/api-decor-i18n.xqm";

declare option exist:serialize "method=xhtml media-type=xhtml";

declare variable $useLocalAssets        := if (request:exists()) then request:get-parameter('useLocalAssets','false') else 'false';
declare variable $download              := if (request:exists()) then request:get-parameter('download','false') else '';
(:When called with localAssets=true then we need relative local paths, else 
    we need our server services URL, but with matching scheme (http or https)
    If the scheme is https and servicesUrl is http this leads to a security 
    problem in some browsers
:)
declare variable $resourcePath          := if ($download='true') then ('https://assets.art-decor.org/ADAR/rv/assets') else if ($useLocalAssets = 'true') then ('../assets') else ('../core/assets');

declare variable $docMessages           := i18n:getMessagesDoc('decor/services');

let $id             := request:get-parameter('id','')
let $language       := request:get-parameter('language',$get:strArtLanguage)

let $parameters     := request:get-parameter-names()

let $searchString   := 
    for $parKey in $parameters
        let $parValue := request:get-parameter($parKey,'')
    return
        if ($parKey != 'format' and string-length($parValue) > 0) then
            (concat('@',$parKey,'=&apos;',$parValue,'&apos;'))
        else 
            ()

let $projects := xmldb:xcollection($get:strDecorData)
(:<dataset id="2.999.999.999.77.1.1" effectiveDate="2009-10-01" expirationDate="" statusCode="" versionLabel="">:)
let $datasets := xmldb:xcollection($get:strDecorData)//datasets/dataset[if ($id='') then (@*) else (@id=$id)]

return 
    if (empty($datasets)) then
        (response:set-status-code(404), <error>{i18n:getMessage($docMessages,'errorRetrieveDatasetNoResults',$language),' ',$searchString}</error>)
    else (
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>DataSetIndex</title>
        <link href="{$resourcePath}/css/default.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <h1>Data Set Index</h1>
        <div class="content">
            <table class="values" id="dataSetList">
                <thead>
                    <tr>
                        <th>XML</th>
                        <th>HTML</th>
                        <th>{i18n:getMessage($docMessages,'columnID',$language)}</th>
                        <th>{i18n:getMessage($docMessages,'effectiveDate',$language)}</th>
                        <th>{i18n:getMessage($docMessages,'expirationDate',$language)}</th>
                        <th>{i18n:getMessage($docMessages,'columnStatus',$language)}</th>
                        <th>{i18n:getMessage($docMessages,'columnVersionLabel',$language)}</th>
                        <th>{i18n:getMessage($docMessages,'columnProjects',$language)}</th>
                    </tr>
                </thead>
                <tbody>
                {
                    for $dataset in $datasets
                        let $dataSetStatusCode := if ($dataset/@statusCode) then (data($dataset/@statusCode)) else
                            if (count($dataset//concept[@statusCode='draft'])=0 and count($dataset//concept[@statusCode='new'])=0) then 'final' else ('draft')
                        let $t_id := $dataset/@id
                        let $t_effectiveDate := $dataset/@effectiveDate
                    order by $dataset/@displayName, $dataset/@effectiveDate
                    return 
                       <tr>
                           <td><a href="RetrieveDataSet?id={data($dataset/@id)}&amp;effectiveDate={data($dataset/@effectiveDate)}&amp;format=xml">xml</a></td>
                           <td><a href="RetrieveDataSet?id={data($dataset/@id)}&amp;effectiveDate={data($dataset/@effectiveDate)}&amp;format=html">html</a></td>
                           <td>{data($dataset/@id)}</td>
                           <td>{data($dataset/@effectiveDate)}</td>
                           <td>{data($dataset/@expirationDate)}</td>
                           <td>{$dataSetStatusCode}</td>
                           <td>{data($dataset/@versionLabel)}</td>
                           <td>{data($projects/*[datasets/dataset[
                                    if ($t_effectiveDate!='') then (
                                        @id=$t_id and @effectiveDate=$t_effectiveDate
                                    ) else (
                                        @id=$t_id
                                    )
                                ]]/project/@prefix)}</td>
                       </tr>
                }
                   </tbody>
            </table>
        </div>
    </body>
</html>
)