xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace art         = "http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../../art/api/api-server-settings.xqm";
import module namespace i18n        = "http://art-decor.org/ns/decor/i18n" at "../../../art/api/api-decor-i18n.xqm";

declare namespace hl7               = "urn:hl7-org:v3";
declare option exist:serialize "indent=yes";
declare option exist:serialize "omit-xml-declaration=yes";

declare variable $artDeepLinkServices   := adserver:getServerURLServices();
declare variable $useLocalAssets        := if (request:exists()) then request:get-parameter('useLocalAssets','false') else 'false';
declare variable $download              := if (request:exists()) then request:get-parameter('download','false') else '';
(:When called with localAssets=true then we need relative local paths, else 
    we need our server services URL, but with matching scheme (http or https)
    If the scheme is https and servicesUrl is http this leads to a security 
    problem in some browsers
:)
declare variable $resourcePath          := if ($download='true') then ('https://assets.art-decor.org/ADAR/rv/assets') else if ($useLocalAssets = 'true') then ('../assets') else ('../core/assets');

declare variable $docMessages           := i18n:getMessagesDoc('decor/services');
declare variable $strArtURL             := adserver:getServerURLArt();

declare %private function local:getpage ($errorstring as xs:string?,$language as xs:string) as item()* {
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <title>{i18n:getMessage($docMessages,'titleRenderCDA',$language)}</title>
            <link href="{$resourcePath}/css/default.css" rel="stylesheet" type="text/css"/>
        </head>
        <body>
            <h1>{i18n:getMessage($docMessages,'titleRenderCDA',$language)}
                <span style="float:right;">
                    <img src="/art-decor/img/flags/nl.png" onclick="location.href=window.location.pathname+'?language=nl-NL{string-join(for $p in request:get-parameter-names() return if ($p='language') then () else concat('&amp;',$p,'=',request:get-parameter($p,())[string-length()>0]),'')}';" class="linked flag"/>
                    <img src="/art-decor/img/flags/de.png" onclick="location.href=window.location.pathname+'?language=de-DE{string-join(for $p in request:get-parameter-names() return if ($p='language') then () else concat('&amp;',$p,'=',request:get-parameter($p,())[string-length()>0]),'')}';" class="linked flag"/>
                    <img src="/art-decor/img/flags/us.png" onclick="location.href=window.location.pathname+'?language=en-US{string-join(for $p in request:get-parameter-names() return if ($p='language') then () else concat('&amp;',$p,'=',request:get-parameter($p,())[string-length()>0]),'')}';" class="linked flag"/>
                </span>
            </h1>
            <div>
            {
                if (string-length($errorstring)>0) then (
                    <h2>{i18n:getMessage($docMessages,'headingError',$language)}</h2>,
                    <p><strong>{$errorstring}</strong></p>
                ) else ()
            }
            <h2>{i18n:getMessage($docMessages,'headingUpload',$language)}</h2>
            <p>{i18n:getMessage($docMessages,'instructionsRenderCDA',$language)}</p>
            <form action="RenderCDA?language={$language}" enctype="multipart/form-data" method="post">
                <table>
                    <tr>
                        <td>
                            <input type="file" name="upload" />
                        </td>
                        <td>
                            <input type="radio" name="action" value="show" checked="checked">{i18n:getMessage($docMessages,'buttonShow',$language)}</input>
                            <input type="radio" name="action" value="convert">{i18n:getMessage($docMessages,'buttonConvert',$language)}</input> 
                        </td>
                        <td style="padding-left: 1em;" align="right">
                            <input type="submit" value="{i18n:getMessage($docMessages,'buttonGo',$language)}" />
                        </td>
                    </tr>
                </table>
            </form>
            <h2>{i18n:getMessage($docMessages,'headingCommandline',$language)}</h2>
            <p>{i18n:getMessage($docMessages,'instructionsRenderCDACommandLine1',$language)} <pre>curl -F "upload=@cda.xml" -o "cda.html" {adserver:getServerURLServices()}RenderCDA?action=show</pre></p>
            <p>{i18n:getMessage($docMessages,'instructionsRenderCDACommandLine2',$language)} <pre>curl -F "upload=@cdar2.xml" -o "cdar3.xml" {adserver:getServerURLServices()}RenderCDA?action=convert</pre></p>
            </div>
        </body>
    </html>
};

let $filedata         := if (request:exists() and request:is-multipart-content()) then request:get-uploaded-file-data('upload') else ()
let $fileaction       := if (request:exists()) then request:get-parameter('action','show') else ()
(:let $filename         := if (request:exists()) then request:get-uploaded-file-name('upload') else ()
let $filesize         := if (request:exists()) then request:get-uploaded-file-size('upload') else ():)
let $language           := request:get-parameter('language',$get:strArtLanguage)

let $xslfile          := 
    if ($fileaction='show') 
    then xs:anyURI(concat('xmldb:exist://',$get:strHl7,'/CDAr2/xsl/CDA.xsl'))
    else xs:anyURI(concat('xmldb:exist://',$get:strHl7,'/CDAr2/cdar2_to_cdar3/cda2_to_cdar3.xsl'))
let $xslparams       := 
    if ($fileaction='show') 
    then <parameters><param name="textLang" value="{$language}"/></parameters>
    else ()
    
let $error-transform  := "Error rendering the document"


return 
if (empty($filedata)) then (
        response:set-status-code(200), 
        response:set-header('Content-Type','text/html; charset=utf-8'),
        local:getpage('',$language)
) else if ($fileaction='convert') then (
    (:converting to CDAr3:)
    try {
        (: convert base64 to string :)
        (: parse into node :)
        response:set-status-code(200), 
        response:set-header('Content-Type','text/xml; charset=utf-8'),
        transform:transform(fn:parse-xml(util:binary-to-string($filedata)), $xslfile, ())
    }
    catch * {
        response:set-status-code(500), 
        response:set-header('Content-Type','text/html; charset=utf-8'),
        local:getpage(string-join(($error-transform,': ',$err:code,' - ',$err:description),''),$language)
    }
) else (
    (:rendering in HTML:)
    try {
        (: convert base64 to string :)
        (: parse into node :)
        response:set-status-code(200), 
        response:set-header('Content-Type','text/html; charset=utf-8'),
        transform:transform(fn:parse-xml(util:binary-to-string($filedata)), $xslfile, ())
    }
    catch * {
        response:set-status-code(500), 
        response:set-header('Content-Type','text/html; charset=utf-8'),
        local:getpage(string-join(($error-transform,': ',$err:code,' - ',$err:description),''),$language)
    }
)
