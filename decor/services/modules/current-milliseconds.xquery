xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

(:
    Get number of milliseconds since 01-01-2000 00:00:00 (no parameter) 
    or the current time as string if parameter format is 'string'
:)
declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace util      = "http://exist-db.org/xquery/util";

let $format := request:get-parameter('format','') 

let $cm := if ($format='string') then util:system-dateTime() else xs:decimal ( ( util:system-dateTime() - xs:dateTime('2000-01-01T00:00:00') ) div xs:dayTimeDuration('PT1S') * 1000 )

return 
    <random>{$cm}</random>