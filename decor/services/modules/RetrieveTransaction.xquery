xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "../../../api/modules/library/settings-lib.xqm";
import module namespace utillib     = "http://art-decor.org/ns/api/util" at "../../../api/modules/library/util-lib.xqm";
import module namespace decorlib    = "http://art-decor.org/ns/api/decor" at "../../../api/modules/library/decor-lib.xqm";
import module namespace serverapi   = "http://art-decor.org/ns/api/server" at "../../../api/modules/server-api.xqm";
import module namespace i18n        = "http://art-decor.org/ns/decor/i18n" at "../../../art/api/api-decor-i18n.xqm";
import module namespace tmapi       = "http://art-decor.org/ns/api/template" at "../../../api/modules/template-api.xqm";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "xhtml";
declare option output:encoding "UTF-8";

declare variable $artDeepLink           := serverapi:getServerURLArt();
declare variable $artDeepLinkTerminology:= if (contains($artDeepLink,'localhost')) then 'http://localhost:8877/terminology/' else concat($artDeepLink,'../terminology/');
declare variable $artDeepLinkServices   := serverapi:getServerURLServices();

declare variable $codeSystemSNOMED      := '2.16.840.1.113883.6.96';
declare variable $codeSystemLOINC       := '2.16.840.1.113883.6.1';
(:performance:)
declare variable $codeSystemNames       := <cs id="{$codeSystemSNOMED}" nm="SNOMED-CT"/>|<cs id="{$codeSystemLOINC}" nm="LOINC"/>;

declare variable $useLocalAssets        := if (request:exists()) then request:get-parameter('useLocalAssets','false') else 'false';
declare variable $download              := if (request:exists()) then request:get-parameter('download','false') else '';
declare variable $doMSconversion        := if (request:exists()) then request:get-parameter('msword','-') else '-';

(:When called with localAssets=true then we need relative local paths, else 
    we need our server services URL, but with matching scheme (http or https)
    If the scheme is https and servicesUrl is http this leads to a security 
    problem in some browsers
:)
declare variable $resourcePath                      := if ($download='true' or $doMSconversion='true') then ('https://assets.art-decor.org/ADAR/rv/assets') else if ($useLocalAssets = 'true') then ('../assets') else ('../core/assets');
declare variable $langparamtemp                     := if (request:exists()) then request:get-parameter('language',$setlib:strArtLanguage)[string-length()>0][1] else ($setlib:strArtLanguage);
declare variable $ui-lang                           := if (request:exists()) then (request:get-parameter('ui',$setlib:strArtLanguage)[. castable as xs:language], $setlib:strArtLanguage)[1] else ($setlib:strArtLanguage);
declare variable $seetype                           := if (request:exists()) then request:get-parameter('seetype','live-services') else ('live-services');
declare variable $strMessages                       := 'decor/services';
(:declare variable $docMessages                       := i18n:getMessagesDoc('decor/services');:)
declare variable $i18nColumnCodeSystem              := i18n:getMessage($strMessages,'columnCodeSystem',$ui-lang);
declare variable $i18nColumnCodeSystemWithFilters   := i18n:getMessage($strMessages,'columnCodeSystemWithFilters',$ui-lang);
declare variable $i18nStatic                        := i18n:getMessage($strMessages,'static',$ui-lang);
declare variable $i18nDynamic                       := i18n:getMessage($strMessages,'dynamic',$ui-lang);
declare variable $i18nLength                        := i18n:getMessage($strMessages,'Length',$ui-lang);
declare variable $i18nFractionDigits                := i18n:getMessage($strMessages,'FractionDigits',$ui-lang);
declare variable $i18nRange                         := i18n:getMessage($strMessages,'Range',$ui-lang);
declare variable $i18nTimeStampPrecision            := i18n:getMessage($strMessages,'TimeStampPrecision',$ui-lang);
declare variable $i18nUnit                          := i18n:getMessage($strMessages,'Unit',$ui-lang);
declare variable $i18nTypeGroup                     := i18n:getMessage($strMessages,'Group',$ui-lang);
declare variable $i18nTypeItem                      := i18n:getMessage($strMessages,'Item',$ui-lang);
declare variable $i18nValueSet                      := concat(i18n:getMessage($strMessages,'ValueSet',$ui-lang), ':');
declare variable $i18nOrWord                        := concat(' ',i18n:getMessage($strMessages,'orWord',$ui-lang),' ');
declare variable $inactiveStatusCodes               := ('cancelled','rejected','deprecated');
declare variable $draftStatusCodes                  := ('new','draft','pending');
declare variable $decortypes                        := utillib:getDecorTypes();
declare variable $decorDataSetValueType             := 
    map:merge(
        for $node in $decortypes//DataSetValueType/enumeration
        return map:entry($node/@value,
            if ($node/label[@language = $ui-lang]) 
            then string-join($node/label[@language = $ui-lang]/string(), $i18nOrWord)
            else string-join($node/label[@language = 'en-US']/string(), $i18nOrWord)
        )
    );
declare variable $decorCodingStrength               :=
    map:merge(
        for $node in $decortypes//CodingStrengthType/enumeration
        return map:entry($node/@value,
            if ($node/label[@language = $ui-lang]) 
            then string-join($node/label[@language = $ui-lang]/string(), $i18nOrWord)
            else string-join($node/label[@language = 'en-US']/string(), $i18nOrWord)
        )
    );
declare variable $decorDataSetTimeStampPrecision    := $decortypes//DataSetTimeStampPrecision/enumeration | $decortypes//DataSetTimePrecision/enumeration;

(: order is important!! :)
declare variable $columnNames                       := (
    'columnName', 
    'columnID', 
    'columnMandatory',
    'columnConformance',
    'columnCardinality', 
    'columnMax',
    'columnCCD',
    'columnDatatype',
    'columnProperty',
    'columnExample',
    'columnCodes',
    'columnDescription',
    'columnContext',
    'columnSource',
    'columnRationale',
    'columnOperationalization',
    'columnComment',
    'columnMapping', 
    'columnCondition',
    'columnEnableWhen',
    'columnStatus',
    'columnCommunity',
    'columnTerminology',
    'columnValueSet',
    'columnType',
    'columnParent',
    'columnInherit'
);

declare %private function local:getDisplayNameFromCodesystem($code as xs:string, $codeSystem as xs:string, $language as xs:string?) as xs:string* {
    try {
        if ($codeSystem = $codeSystemSNOMED) then (
            (: SNOMED CT may contain fsn for multiple language. If there's only 1 fsn, we return that, otherwise if we can find 1 for the language, that one, else the first. :)
            let $fsns       := 
                if (matches($code, '^\d+$')) then 
                    doc(concat($artDeepLinkTerminology,'snomed/getConcept/',encode-for-uri($code)))//concept[@conceptId=$code]/desc[@type='fsn'][@active]
                else ()
            let $fsn        := 
                if (count($fsns) le 1) then $fsns
                else (
                    let $fsn-for-lang := $fsns[@languageCode = substring($language, 1, 2)]
                    return if ($fsn-for-lang) then $fsn-for-lang[1] else $fsns[1]
                )
            return $fsn[1]
        )
        else 
        if ($codeSystem = $codeSystemLOINC) then (
            let $concepts   := 
                if (matches($code, '^(LA)?[\d\-]+$')) then
                    doc(concat($artDeepLinkTerminology,'loinc/searchLOINC/',encode-for-uri($code)))//concept[@loinc_num=$code]
                else ()
            let $fsn        :=
                if ($language = 'en-US') then 
                    $concepts/longName
                else (
                    let $fsn-for-lang := $concepts/concept[@language = $language][longName]
                    return if ($fsn-for-lang) then $fsn-for-lang[1]/longName else $concepts/longName
                )
            return $fsn[1]
        )
        else
            let $labels     := doc(concat($artDeepLinkTerminology,'claml/RetrieveClass?classificationId=',$codeSystem,'&amp;code=',encode-for-uri($code)))/Class[@code = $code][@classificationId = $codeSystem]/Rubric[@kind = 'preferred']/Label
            let $fsn        := 
                if (count($labels) le 1) then $labels
                else (
                    let $fsn-for-lang := $labels[@xml:lang = substring($language, 1, 2)]
                    return if ($fsn-for-lang) then $fsn-for-lang[1] else $labels[1]
                )
             return $fsn
    }
    catch * {()}
};

(: 
    Build HTML rows for concept 
:)
declare %private function local:getConceptRows(
    $concept as element(), 
    $parentId as xs:string, 
    $language as xs:string, 
    $columnmap as item()?, 
    $showonlyactiveconcepts as xs:boolean,
    $docommunity as xs:boolean,
    $isTransaction as xs:boolean,
    $version as xs:string?,
    $projectPrefix as xs:string,
    $templateChain as element()*
    ) as element()* {
    let $rowId := concat('id_', replace($concept/@id, '\.', '_'))
    (: assemble termlist from terminologyAssociations on the concept, and conceptList(s) :)
    let $termList       := 
        for $item in $concept/terminologyAssociation[@conceptId = $concept/@id][@code][@codeSystem]
        let $codeSystemName := if ($item/@codeSystemName) then $item/@codeSystemName else utillib:getNameForOID($item/@codeSystem, $language, ())
        let $codeSystemName := if (string-length($codeSystemName)=0) then $item/@codeSystem else $codeSystemName
        (: Quite often the displayName is not present on terminologyAssociation, but is there in $concept/valueSet/concept
        If it is, we pick it up there to avoid hitting the expensive getDisplayNameFromCodesystem :)
        let $displayName    := $item/@displayName
        let $displayName    := if (empty($displayName)) then (local:getDisplayNameFromCodesystem($item/@code, $item/@codeSystem, $language)) else $displayName
        
        let $deeplinktocode := 
            concat($artDeepLinkServices,'RedirectTerminology?codeSystem=',$item/@codeSystem,'&amp;conceptId=',encode-for-uri($item/@code),'&amp;language=',$language,'&amp;prefix=',$projectPrefix,'&amp;version=',$version)
        (: Don't cast displayName with $displayName/string(), since it already may be as string :)
        let $name := if ($displayName) then xs:string($displayName) else $item/@code/string()
        let $cidlevel   := ($concept/@level, 0)[1]
        let $cidtype    := ($item/@type, 'L')[1]
        let $css-margin := concat('margin-left: ', (0 + number($cidlevel)) * 15,'px;')
        return 
        <div class="concepttype{$cidtype}" style="{$css-margin}">
        {
            if ($deeplinktocode) then (
                <a href="{$deeplinktocode}">{$name}</a>
                ,
                concat(' (',$codeSystemName,': ', $item/@code, ')')
            )
            else
                concat($name, ' (',$codeSystemName,': ', $item/@code, ')')
        }
        </div>
    let $termList       := 
    (
        $termList |
        <ul>
        {
            for $cid at $i in $concept/valueDomain/conceptList/concept/@id
            for $item in $concept/terminologyAssociation[@conceptId = $cid][@code][@codeSystem]
            let $codeSystemName := if ($item/@codeSystemName) then $item/@codeSystemName else utillib:getNameForOID($item/@codeSystem, $language, ())
            let $codeSystemName := if (string-length($codeSystemName)=0) then $item/@codeSystem else $codeSystemName
            (: Quite often the displayName is not present on terminologyAssociation, but is there in $concept/valueSet/concept
            If it is, we pick it up there to avoid hitting the expensive getDisplayNameFromCodesystem :)
            let $displayName    := $item/@displayName
            let $displayName    := if (empty($displayName)) then (local:getDisplayNameFromCodesystem($item/@code, $item/@codeSystem, $language)) else $displayName
            
            let $deeplinktocode := 
                concat($artDeepLinkServices,'RedirectTerminology?codeSystem=',$item/@codeSystem,'&amp;conceptId=',encode-for-uri($item/@code),'&amp;language=',$language,'&amp;prefix=',$projectPrefix,'&amp;version=',$version)
            (: Don't cast displayName with $displayName/string(), since it already may be as string :)
            let $name := if ($displayName) then xs:string($displayName) else $item/@code/string()
            let $cidlevel   := ($cid/../@level, 0)[1]
            let $cidtype    := ($item/@type, 'L')[1]
            let $css-margin := concat('margin-left: ', (0 + number($cidlevel)) * 15,'px;')
            return 
            <li class="concepttype{$cidtype}" style="{$css-margin}">
            {
                if ($deeplinktocode) then (
                    <a href="{$deeplinktocode}">{$name}</a>
                    ,
                    concat(' (',$codeSystemName,': ', $item/@code, ')')
                )
                else
                    concat($name, ' (',$codeSystemName,': ', $item/@code, ')')
            }
            </li>
        }
        </ul>
     )
     
     let $valueSetList  :=
        for $valueSet in $concept/valueSet[@id]
        let $vsid           := $valueSet/@id/string()
        let $vsnm           := $valueSet/@name/string()
        let $vsef           := $valueSet/@effectiveDate/string()
        let $vsdn           := if ($valueSet/@displayName) then ($valueSet/@displayName/string()) else ($valueSet/@name/string())
        let $tatype         := if ($valueSet/terminologyAssociation[@flexibility castable as xs:dateTime]) then ($i18nStatic) else ($i18nDynamic)
        let $theStrength    := ($valueSet/terminologyAssociation/@strength[not(.='')])[1]
        let $tastrength     := if ($theStrength) then map:get($decorCodingStrength, $theStrength) else ()
        let $tastrength     := ($tastrength, $theStrength)[1]
        return
            <div>
                <a href="{$artDeepLinkServices}RetrieveValueSet?prefix={$projectPrefix}&amp;id={$vsid}&amp;effectiveDate={$vsef}&amp;version={$version}&amp;format=html&amp;language={$language}&amp;seetype={$seetype}">{$vsdn}</a>{concat(' ', $vsef,' (', string-join(($tatype, $tastrength), ' / '),')')}
            </div>
    (: assemble codelist :)
    let $codeList       := 
        if ($concept/valueSet/conceptList/concept) then (
            let $vscnt  := count($concept/valueSet) >= 1
            
            for $valueSet in $concept/valueSet
            let $vsid           := $valueSet/@id/string()
            let $vsnm           := $valueSet/@name/string()
            let $vsef           := $valueSet/@effectiveDate/string()
            let $vsdn           := if ($valueSet/@displayName) then ($valueSet/@displayName/string()) else ($valueSet/@name/string())
            let $tatype         := if ($valueSet/terminologyAssociation[@flexibility castable as xs:dateTime]) then ($i18nStatic) else ($i18nDynamic)
            let $list-style     := if ($vscnt) then ('margin-bottom: 10px;') else ()
            let $list-style     := if ($valueSet[following-sibling::valueSet]) then ('margin-bottom: 10px;') else ()
            let $list-title     := if ($vscnt) then if ($valueSet/@displayName) then ($valueSet/@displayName) else ($valueSet/@name) else ()
            let $list-effdt     := if ($vscnt) then if ($valueSet/@effectiveDate) then ($valueSet/@effectiveDate) else () else ()
            return (
            <div style="font-style: italic;">
                <a href="{$artDeepLinkServices}RetrieveValueSet?prefix={$projectPrefix}&amp;id={$vsid}&amp;effectiveDate={$vsef}&amp;version={$version}&amp;format=html&amp;language={$language}&amp;seetype={$seetype}">
                {
                    if (string-length($list-title)>0)
                    then concat($i18nValueSet, ' ', $list-title) 
                    else (),
                    if (string-length($list-effdt)>0)
                    then concat(' (', $list-effdt, ')') 
                    else ()
                }
                </a>
            </div>
            ,
            <ul style="{$list-style}" title="{$list-title}">
            {
                for $item in $valueSet/conceptList/*
                let $name       := if ($item/name[@language=$language]) then $item/name[@language=$language] else $item/name[1]
                let $synonym    := if ($item/synonym[string-length(.)>0]) then concat(' (', string-join($item/synonym, ', '), ')') else ()
                let $ordinal    := if ($item/@ordinal[string-length(.)>0]) then concat('(', string-join($item/@ordinal, ', '), ') :') else ()
                let $css-margin := concat('margin-left: ',$item/number(@level) * 15,'px;')
                return
                <li class="concepttype{$item/@type}" style="{$css-margin}">{$ordinal, $name/string(), $synonym}</li>
            }
            {
                for $item in $valueSet/completeCodeSystem | $valueSet/conceptList/include[@codeSystem][not(@code)]
                let $csid := $item/@codeSystem
                return 
                <li class="concepttype{$item/@type}">{concat(if ($item[filter]) then $i18nColumnCodeSystemWithFilters else $i18nColumnCodeSystem,': ', if ($item/@codeSystemName/string()) then $item/@codeSystemName/string() else utillib:getNameForOID($csid,$language,''))}</li>
            }
            </ul>
            )
        ) else 
        if ($concept/valueDomain/conceptList/concept) then (
            <ul>
            {
                for $item in $concept/valueDomain/conceptList/concept
                let $name       := if ($item/name[@language=$language]) then $item/name[@language=$language] else $item/name[1]
                let $synonym    := if ($item/synonym) then concat(' (', string-join($item/synonym, ', '), ')') else ()
                let $ordinal    := if ($item/@ordinal[string-length(.)>0]) then concat('(', string-join($item/@ordinal, ', '), ') :') else ()
                let $css-margin := concat('margin-left: ',$item/number(@level) * 15,'px;')
                return 
                <li class="concepttype{$item/@type}" style="{$css-margin}">{$ordinal, $name/string(), $synonym}</li>
            }
            </ul>
        ) else ()
    (: assemble the data content for table details :)
    let $cardinality    := 
        if ($isTransaction) then (
            let $min := utillib:getMinimumMultiplicity($concept)
            let $max := utillib:getMaximumMultiplicity($concept)
            
            return
                string-join((if ($min = '*' or $min castable as xs:integer) then $min else ('?'), '…', if ($max = '*' or $max castable as xs:integer) then $max else ('?')), ' ')
        ) else ()
    let $conformance    := $concept/@conformance/string()
    
    let $dtkey          := ($concept/valueDomain/@type)[1]
    let $datatype       := if (empty($dtkey)) then () else map:get($decorDataSetValueType, $dtkey)
    
    let $tr             :=
            <tr class="bg-{$concept/@type}" data-tt-id="{$rowId}" id="{$rowId}">
                {if (empty($parentId)) then () else attribute data-tt-parent-id {$parentId}}
                <td class="columnName node-s{$concept/@statusCode}">
                {   
                    if ($download='true') then (
                        for $i in (1 to count($concept/ancestor::concept)) return '&#160;&#160;&#160;&#160;'
                    ) else (),
                    $concept/name[@language=$language][1]/string(), if ($concept/synonym) then concat(' (', string-join($concept/synonym[@language=$language], ', '), ')') else ()
                }
                </td>
                <td class="columnID" style="{if (map:get($columnmap, 'columnID')) then ('display:none;') else ()}">
                {
                    if ($concept/@iddisplay) then data($concept/@iddisplay) else if ($concept/@refdisplay) then data($concept/@refdisplay) else data($concept/(@id|@ref))
                }
                </td>
                {
                    if ($isTransaction) then (
                    <td class="columnMandatory" style="{if (map:get($columnmap, 'columnMandatory')) then ('display:none;') else ()}">
                    {
                        if ($concept[@isMandatory="true"]) then "+" else "-"
                    }
                    </td>
                    ,
                    <td class="columnConformance" style="{if (map:get($columnmap, 'columnConformance')) then ('display:none;') else ()}">
                    {
                        $conformance
                    }
                    </td>
                    ,
                    <td class="columnCardinality" style="{if (map:get($columnmap, 'columnCardinality')) then ('display:none;') else ()}">
                    {
                        $cardinality
                    }
                    </td>
                    ,
                    <td class="columnMax" style="{if (map:get($columnmap, 'columnMax')) then ('display:none;') else ()}">
                    {
                        if ($concept/@conformance='NP') then () else replace($concept/@maximumMultiplicity, '\*', 'n')
                    }
                    </td>
                    ,
                    (: Single column with: 0..1 R Datetime etc. :)
                    <td class="columnCCD" style="{if (map:get($columnmap, 'columnCCD')) then ('display:none;') else ()}">
                    {
                        $cardinality, $conformance, ' ', <a href="https://art-decor.org/mediawiki/index.php?title=DECOR-dataset#{$datatype}">{$datatype}</a>
                    }
                    </td>
                    ) else ()
                }
                <td class="columnDatatype" style="{if (map:get($columnmap, 'columnDatatype')) then ('display:none;') else ()}">
                    <a href="https://art-decor.org/mediawiki/index.php?title=DECOR-dataset#{$datatype}">{$datatype}</a>
                </td>
                <td class="columnProperty" style="{if (map:get($columnmap, 'columnProperty')) then ('display:none;') else ()}">
                {
                    (:<property currency="" default="" fixed="" fractionDigits="" minInclude="" maxInclude="" minLength="" maxLength="" timeStampPrecision="" unit=""/>:)
                    for $property in $concept/valueDomain/property
                    return
                        <ul style="margin-bottom: 10px;">
                        {
                            if ($property/@minLength | $property/@maxLength) then
                                <li>{concat($i18nLength,': ',$property/@minLength,'…',$property/@maxLength)}</li>
                            else ()
                        }
                        {
                            if ($property/@minInclude | $property/@maxInclude) then
                                <li>{concat($i18nRange,': ',$property/@minInclude,'…',$property/@maxInclude)}</li>
                            else ()
                        }
                        {
                            if ($property/@fractionDigits) then
                                <li>{concat($i18nFractionDigits,': ',$property/@fractionDigits)}</li>
                            else ()
                        }
                        {
                            if ($property/@default) then
                                <li>{concat(i18n:getMessage($strMessages, 'DefaultValue', $ui-lang),': ',$property/@default)}</li>
                            else ()
                        }
                        {
                            if ($property/@fixed) then
                                <li>{concat(i18n:getMessage($strMessages, 'FixedValue', $ui-lang),': ',$property/@fixed)}</li>
                            else ()
                        }
                        {
                            if ($property/@timeStampPrecision) then
                                <li>{concat($i18nTimeStampPrecision,': ',$decorDataSetTimeStampPrecision[@value=$property/@timeStampPrecision]/label[@language=$ui-lang]/string())}</li>
                            else ()
                        }
                        {
                            if ($property/@currency) then
                                <li>{concat(i18n:getMessage($strMessages, 'Currency', $ui-lang),': ',$property/@currency)}</li>
                            else ()
                        }
                        {
                            if ($property/@unit) then
                                <li>{concat($i18nUnit,': ',$property/@unit)}</li>
                            else ()
                        }
                        {
                            for $att in $property/(@* except (@currency|@default|@fixed|@fractionDigits|@minInclude|@maxInclude|@minLength|@maxLength|@timeStampPrecision|@unit))
                            return
                                <li>{concat($att/name(),': ',$att/string())}</li>
                        }
                        </ul>
                }
                </td>
                <td class="columnExample" style="{if (map:get($columnmap, 'columnExample')) then ('display:none;') else ()}">
                {
                    string-join($concept/valueDomain/example, '; ')
                }
                </td>
                <td class="columnCodes" style="{if (map:get($columnmap, 'columnCodes')) then ('display:none;') else ()}">
                {
                    $codeList
                }
                </td>
                <td class="columnDescription" style="{if (map:get($columnmap, 'columnDescription')) then ('display:none;') else ()}">
                {
                    (:utillib:parseNode($concept/desc)/node():)
                    $concept/desc[@language=$language][1]/node()
                }
                </td>
                {
                    if ($isTransaction) then (
                    <td class="columnContext" style="{if (map:get($columnmap, 'columnContext')) then ('display:none;') else ()}">
                    {
                        $concept/context[@language=$language][1]/node()
                    }
                    </td>
                    ) else ()
                }
                <td class="columnSource" style="{if (map:get($columnmap, 'columnSource')) then ('display:none;') else ()}">
                {
                    $concept/source[@language=$language][1]/string()
                }
                </td>
                <td class="columnRationale" style="{if (map:get($columnmap, 'columnRationale')) then ('display:none;') else ()}">
                {
                    $concept/rationale[@language=$language][1]/string()
                }
                </td>
                <td class="columnOperationalization" style="{if (map:get($columnmap, 'columnOperationalization')) then ('display:none;') else ()}">
                {
                    $concept/operationalization[@language=$language][1]/string()
                }
                </td>
                <td class="columnComment" style="{if (map:get($columnmap, 'columnComment')) then ('display:none;') else ()}">
                    <ul>
                    {
                        for $comment in $concept/comment[@language = $language] return <li>{$comment/string()}</li>
                    }
                    </ul>
                </td>
                {
                    if ($isTransaction) then (
                    <td class="columnMapping" style="{if (map:get($columnmap, 'columnMapping')) then ('display:none;') else ()}">
                    {
                        (:get mappings from templates in or connected to the current representingTemplate:)
                        if ($templateChain) then (
                            let $decor      := utillib:getDecorByPrefix($projectPrefix)
                            for $templateAssociation in $decor//templateAssociation/concept[@ref=$concept/@id][@effectiveDate=$concept/@effectiveDate]
                            let $tmid       := $templateAssociation/../@templateId
                            let $tmed       := $templateAssociation/../@effectiveDate
                            let $elid       := $templateAssociation/@elementId
                            (:check decor templates first as those are indexed:)
                            let $tm         := $decor//template[@id = $tmid][@effectiveDate = $tmed]
                            (:check templateChain next which is in memory and as such slower:)
                            let $tm         := if ($tm) then $tm else ($templateChain[@id = $tmid][@effectiveDate = $tmed])
                            let $element    := $tm[1]//*[@id = $elid]
                            return (
                                for $e in $element 
                                let $d  := if ($e/desc[@language = $language]) then concat(' (',$e/desc[@language = $language],')') else if ($e[desc]) then concat(' (',$e/desc[1],')') else ()
                                let $cf := if ($e/@isMandatory='true') then 'M' else ($e/@conformance)
                                let $mc := concat(utillib:getMinimumMultiplicity($e),' … ',utillib:getMaximumMultiplicity($e),' ',$cf)
                                return 
                                    <div><a href="{concat($artDeepLinkServices,'RetrieveTemplate?prefix=',$projectPrefix,'&amp;id=',$tmid,'&amp;effectiveDate=',encode-for-uri($tmed),'#_',$elid,'_')}">{replace($e/@name,'hl7v2:','')}</a>{concat(' - ', $mc, $d)}</div>
                            )
                        ) else ()
                    }
                    </td>
                    ,
                    <td class="columnCondition" style="{if (map:get($columnmap, 'columnCondition')) then ('display:none;') else ()}">
                    {
                        if ($concept/condition) then (
                            <ul>
                            {
                                for $condition in $concept/condition 
                                return
                                if ($condition[desc]) then (
                                <li>{   $condition/@minimumMultiplicity/string(), '…', $condition/@maximumMultiplicity/string(), 
                                        $condition/@conformance/string(), ' ', data($condition/desc[1])}</li>
                                )
                                else
                                if ($condition[position()=last()]) then (
                                <li>{   $condition/@minimumMultiplicity/string(), '…', $condition/@maximumMultiplicity/string(), 
                                        $condition/@conformance/string(), ' ', i18n:getMessage($strMessages, 'else', $ui-lang)}</li>
                                )
                                else (
                                <li>{   $condition/@minimumMultiplicity/string(), '…', $condition/@maximumMultiplicity/string(), 
                                        $condition/@conformance/string(), ': ', data($condition)}</li>
                                )
                            }
                            </ul>
                        ) else ()
                    }
                    </td>
                    ,
                    <td class="columnEnableWhen" style="{if (map:get($columnmap, 'columnEnableWhen')) then ('display:none;') else ()}">
                    {
                        if ($concept/enableWhen) then (
                            if (count($concept/enableWhen) gt 1) then
                                <div>{i18n:getMessage($strMessages, 'enableBehavior' || ($concept/@enableBehavior, 'undefined')[1], $ui-lang)}</div>
                            else ()
                            ,
                            <ul>
                            {
                                for $condition in $concept/enableWhen
                                let $question         := $condition/@question
                                let $theReffedConcept := $concept/../concept[@id = $question]
                                return
                                    <li>
                                    {
                                        '"',
                                        <a href="#{concat('id_', replace($question, '\.', '_'))}" title="{string-join($theReffedConcept/ancestor-or-self::concept/name[1], ' / '), ' - ', $question}">{data(($theReffedConcept/name, $question)[1])}</a>,
                                        '"',
                                        data($condition/@operator),
                                        ' ',
                                        for $dt in $condition/*
                                        let $theCSN   := 
                                                if ($dt/@codeSystemName) then $dt/@codeSystemName else 
                                                if ($dt/@codeSystem) then utillib:getNameForOID($dt/@codeSystem, $language, ()) else $dt/@codeSystem
                                        return
                                        switch (name($dt))
                                        case 'answerCoding' return (
                                                data($dt/@code),
                                                ' ',
                                                <span title="{$theCSN}">{data($dt/@codeSystem)}</span>,
                                                if ($dt/@displayName) then ( 
                                                    ' "' || data($dt/@displayName) || '"'
                                                ) else ()
                                                ,
                                                if ($dt/@canonicalUri) then ( 
                                                    ' - FHIR: ', <i>{data($dt/@canonicalUri)}</i>
                                                ) else ()
                                        )
                                        case 'answerQuantity' return (
                                            data($dt/@comparator),
                                            ' ',
                                            data($dt/@value),
                                            ' ',
                                            data($dt/@unit),
                                            if ($dt/@code) then (
                                                ' (',
                                                data($dt/@code),
                                                ' ',
                                                <span title="{$theCSN}">{data($dt/@codeSystem)}</span>,
                                                if ($dt/@canonicalUri) then ( 
                                                    ' - FHIR: ', <i>{data($dt/@canonicalUri)}</i>
                                                ) else ()
                                                ,
                                                ')'
                                            ) else ()
                                        )
                                        default return data($dt/@value)
                                    }
                                    </li>
                            }
                            </ul>
                        ) else ()
                    }
                    </td>
                    ) else ()
                }
                <td class="columnStatus" style="{if (map:get($columnmap, 'columnStatus')) then ('display:none;') else ()}">
                {
                    $concept/@statusCode/string()
                }
                </td>
                {
                    if ($docommunity) then (
                        <td class="columnCommunity" style="{if (map:get($columnmap, 'columnCommunity')) then ('display:none;') else ()}">
                        {
                            for $commInfo in $concept/community
                            return (
                                <div><b>{data($commInfo/@name)}</b></div>,
                                <ul title="{$commInfo/@name}">
                                {
                                    for $association in $commInfo/data
                                    return
                                        <li>
                                            <span title="{$association/@label}" style="padding: inherit;">{data($association/@type)}</span>: {$association/node()}
                                        </li>
                                }
                                </ul>
                            )
                        }
                        </td>
                    ) else ()
                }
                <td class="columnTerminology" style="{if (map:get($columnmap, 'columnTerminology')) then ('display:none;') else ()}">
                {
                    $termList
                }
                </td>
                <td class="columnValueSet" style="{if (map:get($columnmap, 'columnValueSet')) then ('display:none;') else ()}">
                {
                    $valueSetList
                }
                </td>
                <td class="columnType" style="{if (map:get($columnmap, 'columnType')) then ('display:none;') else ()}">
                {
                    if ($concept[@type = 'group']) then $i18nTypeGroup else $i18nTypeItem
                }
                </td>
                <td class="columnParent" style="{if (map:get($columnmap, 'columnParent')) then ('display:none;') else ()}">
                {
                    $concept/parent::concept/name[@language=$language][1]/string()
                }
                </td>
                <td class="columnInherit" style="{if (map:get($columnmap, 'columnInherit')) then ('display:none;') else ()}">
                    <a href="{$artDeepLinkServices}RetrieveDataSet?conceptId={if ($concept/inherit) then $concept/inherit/@ref else $concept/contains/@ref}&amp;conceptEffectiveDate={if ($concept/inherit) then $concept/inherit/@effectiveDate else $concept/contains/@flexibility}&amp;format=html&amp;language={$language}&amp;seetype={$seetype}">
                    {
                        if ($concept/inherit/@refdisplay) then 
                            $concept/inherit/@refdisplay/string()
                        else
                        if ($concept/contains/@refdisplay) then 
                            $concept/contains/@refdisplay/string()
                        else
                        if ($concept/inherit/@ref) then
                            utillib:getNameForOID($concept/inherit/@ref,$language,$projectPrefix)
                        else
                        if ($concept/contains/@ref) then
                            utillib:getNameForOID($concept/contains/@ref,$language,$projectPrefix)
                        else ()
                    }
                    </a>
                </td>
            </tr>
    
    return ($tr, 
        let $children := 
            if ($showonlyactiveconcepts) then (
                for $concept in $concept/concept 
                return if ($concept[@statusCode = $inactiveStatusCodes]) then () else ($concept)
            )
            else $concept/concept
        for $conceptChild in $children
        return local:getConceptRows($conceptChild, $rowId, $language, $columnmap, $showonlyactiveconcepts, $docommunity, $isTransaction, $version, $projectPrefix, $templateChain)) 
};

(: Build the main HTML view 
Accepts hidecolumns string, i.e.: '123456789a' or '54'
Hides columns based on (hex) number. 
:)
declare %private function local:getHTML(
    $fullDatasetTree as node(), 
    $language as xs:string, 
    $hidecolumns as xs:string, 
    $showonlyactiveconcepts as xs:boolean, 
    $collapsed as xs:boolean, 
    $draggable as xs:boolean, 
    $version as xs:string?,
    $url as xs:anyURI?,
    $logo as xs:anyURI?,
    $docommunity as xs:boolean,
    $projectPrefix as xs:string,
    $templateChain as element()*,
    $doSubConcept as xs:boolean,
    $fileNameforDownload as xs:string
    )  as node() {
    let $isTransaction := exists($fullDatasetTree/@transactionId)
    let $title         := 
        if ($isTransaction) then 
            i18n:getMessage($strMessages, 'Transaction', $ui-lang, string-join(($fullDatasetTree/name[@language=$language][1]/string(), $fullDatasetTree/@versionLabel), ' '))
        else (
            i18n:getMessage($strMessages, 'DataSet', $ui-lang, string-join(($fullDatasetTree/name[@language=$language][1]/string(), $fullDatasetTree/@versionLabel), ' '))
        )
    let $title          := 
        if ($doSubConcept) then
            concat($title, ' - ', i18n:getMessage($strMessages, 'Concept', $ui-lang, $fullDatasetTree/concept[1]/name[@language=$language][1]/string()))
        else ($title)
        
    (: column name can't be hidden :)
    let $columnmap  := 
        map:merge((
            map:entry('columnName', '') ,
            map:entry('columnID', if (contains($hidecolumns, '2'))                      then true() else false()) ,
            if ($isTransaction) then (
            
                map:entry('columnMandatory', if (contains($hidecolumns, '3'))           then true() else false()) ,
                map:entry('columnConformance', if (contains($hidecolumns, '4'))         then true() else false()) ,
                map:entry('columnCardinality', if (contains($hidecolumns, '5'))         then true() else false()) ,
                map:entry('columnMax', if (contains($hidecolumns, '6'))                 then true() else false()) ,
                map:entry('columnCCD', if (contains($hidecolumns, '0'))                 then true() else false())
            
            ) else (),
            map:entry('columnDatatype', if (contains($hidecolumns, '7'))                then true() else false()) ,
            map:entry('columnProperty', if (contains($hidecolumns, '8'))                then true() else false()) ,
            map:entry('columnExample', if (contains($hidecolumns, '9'))                 then true() else false()) ,
            map:entry('columnCodes', if (contains($hidecolumns, 'a'))                   then true() else false()) ,
            map:entry('columnDescription', if (contains($hidecolumns, 'b'))             then true() else false()) ,
            if ($isTransaction) then (
            
                map:entry('columnContext', if (contains($hidecolumns, 'p'))             then true() else false())
            
            ) else (),
            map:entry('columnSource', if (contains($hidecolumns, 'c'))                  then true() else false()) ,
            map:entry('columnRationale', if (contains($hidecolumns, 'd'))               then true() else false()) ,
            map:entry('columnOperationalization', if (contains($hidecolumns, 'e'))      then true() else false()) ,
            map:entry('columnComment', if (contains($hidecolumns, 'f'))                 then true() else false()) ,
            if ($isTransaction) then (
            
                map:entry('columnMapping', if (contains($hidecolumns, 'o'))             then true() else false()) ,
                map:entry('columnCondition', if (contains($hidecolumns, 'g'))           then true() else false()) ,
                map:entry('columnEnableWhen', if (contains($hidecolumns, 'q'))          then true() else false())
            
            ) else (),
            map:entry('columnStatus', if (contains($hidecolumns, 'h'))                  then true() else false()) ,
            if (not($docommunity)) then () else (
            
                map:entry('columnCommunity', if (contains($hidecolumns, 'i'))           then true() else false())
            
            ),
            map:entry('columnTerminology', if (contains($hidecolumns, 'j'))             then true() else false()) ,
            map:entry('columnValueSet', if (contains($hidecolumns, 'k'))                then true() else false()) ,
            map:entry('columnType', if (contains($hidecolumns, 'l'))                    then true() else false()) ,
            map:entry('columnParent', if (contains($hidecolumns, 'm'))                  then true() else false()) ,
            map:entry('columnInherit', if (contains($hidecolumns, 'n'))                 then true() else false())
        ))

    (: for testing only :)
    (:let $linkroot := 'http://localhost:8877':)
    let $html := 
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>{$title}</title>
            <link href="{$resourcePath}/css/default.css" rel="stylesheet" type="text/css"></link>
            <link href="{$resourcePath}/css/retrieve-transaction.css" rel="stylesheet" type="text/css"></link>
            <script type="text/javascript">window.treeTableCollapsed={if ($collapsed) then 'true' else 'false'};</script>
            <script src="{$resourcePath}/scripts/jquery-1.11.3.min.js" type="text/javascript"></script>
            <script src="{$resourcePath}/scripts/jquery.treetable.js" type="text/javascript"></script>
            <script src="{$resourcePath}/scripts/retrieve-transaction.js" type="text/javascript"></script>
            <script src="{$resourcePath}/scripts/jquery.cookie.js" type="text/javascript"></script>
            {if ($draggable) then <script src="{$resourcePath}/scripts/dragtable.js" type="text/javascript"></script> else ()}
        </head>
        <body>
            <table style="width: 100%;">
                <tbody class="list">
                    <tr>
                        <td style="text-align: left;">
                            <h1>{$title}</h1>
                            <p>
                                <span style="margin-right: 1em;">{i18n:getMessage($strMessages, 'columnID', $ui-lang)}: {if ($isTransaction) then ($fullDatasetTree/@transactionId/string()) else ($fullDatasetTree/@id/string())}</span>
                                <span style="margin-right: 1em;">{i18n:getMessage($strMessages, 'columnVersionLabel', $ui-lang)}: {if ($version) then $version else i18n:getMessage($strMessages, 'columnLiveVersion', $ui-lang)}</span>
                                <span style="margin-right: 1em;">
                                {
                                    string-join((
                                        i18n:getMessage($strMessages, 'Concepts', $ui-lang), ': ', count($fullDatasetTree//concept[@type][not(ancestor::conceptList)]), 
                                        ' (', 
                                        i18n:getMessage($strMessages, 'Groups', $ui-lang), ': ', count($fullDatasetTree//concept[@type = 'group']),
                                        ' - ',
                                        i18n:getMessage($strMessages, 'Items', $ui-lang), ': ', count($fullDatasetTree//concept[@type = 'item']),
                                        ')'
                                    ), '')
                                }
                                </span>
                            </p>
                        </td>
                        <td style="text-align: right;">
                        {if ($logo and $url) then 
                            <a href="{$url}">
                                <img src="{$logo}" alt="" title="{$url}" style="max-height: 50px;"/>
                            </a>
                         else if ($logo) then
                            <img src="{$logo}" alt="" style="max-height: 50px;"/>
                         else ()
                        }
                        </td>
                    </tr>
                </tbody>
            </table>
            <!--<p>{i18n:getMessage($strMessages, 'columnID', $ui-lang)}: {$fullDatasetTree/@id/string()}</p>
            <p>{i18n:getMessage($strMessages, 'columnVersionLabel', $ui-lang)}: {if ($version) then $version else i18n:getMessage($strMessages, 'columnLiveVersion', $ui-lang)}</p>-->
            <div id="concepts">
                <button id="expandAll" type="button">{i18n:getMessage($strMessages, 'buttonExpandAll', $ui-lang)}</button> 
                <button id="collapseAll" type="button">{i18n:getMessage($strMessages, 'buttonCollapseAll', $ui-lang)}</button> 
                <input id="nameSearch" placeholder="{i18n:getMessage($strMessages, 'textSearch', $ui-lang)}" />
                <button id="nameSort" type="button">{i18n:getMessage($strMessages, 'buttonSortByName', $ui-lang)}</button> 
                <select id="hiddenColumns"> 
                    <option value="title">{i18n:getMessage($strMessages, 'showColumn', $ui-lang)}</option>
                {
                    for $column in $columnNames[not(. = 'columnName')]
                    let $columnName := i18n:getMessage($strMessages, $column, $ui-lang)
                    order by $columnName
                    return 
                        if (map:keys($columnmap)[. = $column]) then (
                            <option value="{$column}">
                            {
                                if (map:get($columnmap, $column)) then () else (attribute disabled {'disabled'}), 
                                $columnName
                            }
                            </option>
                        ) else ()
                }
                </select> 
                <button id="noTree" type="button">{i18n:getMessage($strMessages, 'buttonListView', $ui-lang)}</button> 
                <button id="resetToDefault" type="button">{i18n:getMessage($strMessages, 'resetToDefault', $ui-lang)}</button>
                <button id="download" type="button" onclick="javascript:location.href=window.location.href+'&amp;download=true';" title="{i18n:getMessage($strMessages, 'buttonDownloadHelp', $ui-lang)}">{i18n:getMessage($strMessages, 'buttonDownload', $ui-lang)}</button>
                <button id="excelExport" onclick="exportTableToExcel('transactionTable', '{$fileNameforDownload}')" title="{i18n:getMessage($strMessages, 'buttonExportTableToExcelHelp', $ui-lang)}">{i18n:getMessage($strMessages, 'buttonExportTableToExcel', $ui-lang)}</button>
                <p class="helptext" style="font-size:x-small;">{i18n:getMessage($strMessages, 'helpText', $ui-lang)}</p>
                <table id="transactionTable" class="draggable">
                    <thead>
                        <tr>
                            <th class="columnName">
                                <b>{i18n:getMessage($strMessages, 'columnName', $ui-lang)}</b>
                            </th>
                            {
                                for $column in $columnNames[not(. = 'columnName')]
                                let $columnName := i18n:getMessage($strMessages, $column, $ui-lang)
                                return
                                if (map:keys($columnmap)[. = $column]) then (
                                    <th class="{$column}">
                                        {if (map:get($columnmap, $column)) then (attribute style {'display:none;'}) else ()}
                                        <b>{$columnName}</b>
                                        <span class="hideMe" type="button">[&#8209;]</span>
                                    </th>
                                ) else ()
                            }
                        </tr>
                    </thead>
                    <tbody class="list">
                    {
                        let $children := 
                            if ($showonlyactiveconcepts) then (
                                for $concept in $fullDatasetTree/concept 
                                return if ($concept[@statusCode = $inactiveStatusCodes]) then () else ($concept)
                            )
                            else $fullDatasetTree/concept
                        for $concept in $children
                        return 
                            local:getConceptRows($concept, '', $language, $columnmap, $showonlyactiveconcepts, $docommunity, $isTransaction, $version, $projectPrefix, $templateChain)
                    }
                    </tbody>
                </table>
            </div>
            <script>
            function exportTableToExcel(tableID, filename = ''){{
                var downloadLink;
                var dataType = 'application/vnd.ms-excel';
                var tableSelect = document.getElementById(tableID);
                var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20').replace(/#/g, '%23');
                
                // Specify file name
                filename = filename?filename+'.xls':'excel_data.xls';
                
                // Create download link element
                downloadLink = document.createElement("a");
                
                document.body.appendChild(downloadLink);
                
                if (navigator.msSaveOrOpenBlob) {{
                    var blob = new Blob(['\ufeff', tableHTML], {{
                        type: dataType
                    }});
                    navigator.msSaveOrOpenBlob( blob, filename);
                }} else {{
                    // Create a link to the file
                    downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
                
                    // Setting the file name
                    downloadLink.download = filename;
                    
                    //triggering the function
                    downloadLink.click();
                }}
            }}
            </script>
            </body>
    </html>
    return $html
};

(: 
    Build a hierarchical list view in HTML
:)
declare %private function local:getSimpleHTML(
    $fullDatasetTree as node(), 
    $language as xs:string, 
    $hidecolumns as xs:string, 
    $showonlyactiveconcepts as xs:boolean,
    $version as xs:string?,
    $url as xs:anyURI?,
    $logo as xs:anyURI?,
    $docommunity as xs:boolean,
    $projectPrefix as xs:string,
    $doSubConcept as xs:boolean,
    $templateChain as element()*
    )  as node() {
    
    let $title          := 
        if ($fullDatasetTree/@transactionId) then 
            i18n:getMessage($strMessages, 'Transaction', $ui-lang, $fullDatasetTree/name[@language=$language][1]/string())
        else (
            i18n:getMessage($strMessages, 'DataSet', $ui-lang, $fullDatasetTree/name[@language=$language][1]/string())
        )
    let $title          := 
        if ($doSubConcept) then
            concat($title, ' - ', i18n:getMessage($strMessages, 'Concept', $ui-lang, $fullDatasetTree/concept[1]/name[@language=$language][1]/string()))
        else ($title)
        
    let $columnmap      := map:merge((
        map:entry('columnDescription', if (contains($hidecolumns, 'b'))             then true() else false()),
        map:entry('columnMapping',     if (contains($hidecolumns, 'o'))             then true() else false())
    ))
    
    let $isTransaction := exists($fullDatasetTree/@transactionId)
    
    let $html := 
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>{$title}</title>
            <link href="{$resourcePath}/css/default.css" rel="stylesheet" type="text/css"></link>
            <style type="text/css">
                 <![CDATA[
                 ol.ad-dataset-group { list-style-type: none;}
                 li.ad-dataset-group { font-weight: bold; padding: 7px 0 0 0;}
                 li.ad-dataset-item, li.ad-template { font-weight: normal; list-style-type: none; padding: 7px 0 0 0; }
                 ul.ad-terminology-code { list-style-position: inside; font-weight: normal; padding: 7px 0 3px 20px; border: 0px; list-style-type: disc;  }
                 ul.ad-transaction-condition { padding: 7px 0 3px 20px; list-style-position: inside;}
                 li.ad-transaction-condition { font-style: italic; font-weight: normal; list-style-type: circle; padding: 0 3px 0 5px; border-left: 5px solid #ddd; }
                 div.ad-dataset-itemnumber, div.ad-templatetype { margin: -2px 0 5px 6px; display: inline-block; border: 1px solid #c0c0c0; 
                    background-color: #eee; border-radius: 3px 3px 3px 3px; -moz-border-radius: 3px 3px 3px 3px; -webkit-border-radius: 3px 3px 3px 3px; 
                    width: auto !important; padding: 1px 5px 1px 5px;}
                 div.ad-dataset-level1 { font-size: 2ex; font-weight: bold; border-bottom: 2px solid #ddd;}
                 table.ad-transaction-table, table.ad-template-table {border: 1px solid #888; border-collapse: collapse; width:100%;}
                 div.cdadocumentlevel { background-color: #eef; }
                 div.cdaheaderlevel { background-color: #ffe; }
                 div.cdasectionlevel { background-color: #efe; }
                 div.cdaentrylevel { background-color: #fef; }
                 div.ad-itemnumber-green { background-color: #efe; }
                 div.ad-itemnumber-blue { background-color: #cef; }
                 div.ad-itemnumber-yellow { background-color: #ffe; }
                 div.description, div.context {font-weight: normal; font-size: 12px; color: #aaa;}
                 ul.tmap {list-style-type: none; padding: 0; margin: 0; color:#8b0000; font-weight: normal; font-size: 10px; }
                 ]]>
            </style>
        </head>
        <body>
            <table style="width: 100%;">
                <tbody class="list">
                    <tr>
                        <td style="text-align: left;">
                            <h1>{$title}</h1>
                        </td>
                        <td style="text-align: right;">
                        {if ($logo and $url) then 
                            <a href="{$url}">
                                <img src="{$logo}" alt="" title="{$url}" style="max-height: 50px;"/>
                            </a>
                         else if ($logo) then
                            <img src="{$logo}" alt="" style="max-height: 50px;"/>
                         else ()
                        }
                        </td>
                    </tr>
                </tbody>
            </table>
            <!--
            <p>{i18n:getMessage($strMessages, 'columnID', $ui-lang)}: {if ($isTransaction) then ($fullDatasetTree/@transactionId/string()) else ($fullDatasetTree/@id/string())}</p>
            <p>{i18n:getMessage($strMessages, 'columnVersionLabel', $ui-lang)}: {if ($version) then $version else i18n:getMessage($strMessages, 'columnLiveVersion', $ui-lang)}</p>
            -->
            <p>
                <span style="margin-right: 1em;">{i18n:getMessage($strMessages, 'columnID', $ui-lang)}: {if ($isTransaction) then ($fullDatasetTree/@transactionId/string()) else ($fullDatasetTree/@id/string())}</span>
                <span style="margin-right: 1em;">{i18n:getMessage($strMessages, 'columnVersionLabel', $ui-lang)}: {if ($version) then $version else i18n:getMessage($strMessages, 'columnLiveVersion', $ui-lang)}</span>
                <span style="margin-right: 1em;">
                {
                    string-join((
                        i18n:getMessage($strMessages, 'Concepts', $ui-lang), ': ', count($fullDatasetTree//concept[@type][not(ancestor::conceptList)]), 
                        ' (', 
                        i18n:getMessage($strMessages, 'Groups', $ui-lang), ': ', count($fullDatasetTree//concept[@type = 'group']),
                        ' - ',
                        i18n:getMessage($strMessages, 'Items', $ui-lang), ': ', count($fullDatasetTree//concept[@type = 'item']),
                        ')'
                    ), '')
                }
                </span>
            </p>
            <table class="ad-transaction-table">
            <tr>
            <td style="padding: 7px 7px 7px 7px;">
            {
                let $children := 
                    if ($showonlyactiveconcepts) then (
                        for $concept in $fullDatasetTree/concept 
                        return if ($concept[@statusCode = $inactiveStatusCodes]) then () else ($concept)
                    )
                    else $fullDatasetTree/concept
                for $concept in $children
                    return local:getSimpleConceptRows($concept, $language, $projectPrefix, $decortypes, $columnmap, $showonlyactiveconcepts, $docommunity, 1, $isTransaction, $templateChain)
            }
            </td>
            </tr>
            </table>
        </body>
    </html>
    return $html
};

declare %private function local:getSimpleConceptRows(
    $concept as element(), 
    $language as xs:string,
    $projectPrefix as xs:string,
    $decortypes as element(), 
    $columnmap as item()?, 
    $showonlyactiveconcepts as xs:boolean,
    $docommunity as xs:boolean,
    $level as xs:integer,
    $isTransaction as xs:boolean,
    $templateChain as element()*
    ) as element()* {
    
     (: assemble codelist :)
    let $codeList := 
        if ($concept/valueSet/conceptList/concept) then (
            <ul class="ad-terminology-code">
            {
            for $item in $concept/valueSet/conceptList/*
            return <li>{$item/name[1]/string()}</li>
            }
            {
            for $item in $concept/valueSet/completeCodeSystem | $concept/valueSet/conceptList/include[@codeSystem][not(@code)]
            let $csid := $item/@codeSystem
            return 
            <li>{concat(if ($item[filter]) then $i18nColumnCodeSystemWithFilters else $i18nColumnCodeSystem, ': ', if ($item/@codeSystemName/string()) then $item/@codeSystemName/string() else utillib:getNameForOID($csid,$language,''))}</li>
            }
            </ul>
        ) else if ($concept/valueDomain/conceptList/concept) then (
            <ul class="ad-terminology-code">
            {
            for $item in $concept/valueDomain/conceptList/concept
            return 
            <li>
            {
                if ($item/@type='D') then (attribute {'style'} {'text-decoration: line-through; font-style: italic; opacity: 0.5;'}) else (),
                $item/name[1]/string()
            }
            </li>
            }
            </ul>
        ) else ()
    let $condition := 
        if ($concept/condition) then (
            <ul class="ad-transaction-condition">
            {
                for $condition at $x in $concept/condition 
                return
                    <li class="ad-transaction-condition"> 
                    {
                        if ($condition[$x=count($concept/condition)]) then
                            concat($condition/@minimumMultiplicity, '..', $condition/@maximumMultiplicity, ' ', $condition/@conformance, ' ', i18n:getMessage($strMessages, 'else', $ui-lang))
                        else (
                            concat($condition/@minimumMultiplicity, '..', $condition/@maximumMultiplicity, ' ', $condition/@conformance, ': ', $condition)
                        )
                    }
                    </li>
            }
            </ul>
        ) else ()
    
    let $children       := 
        if ($showonlyactiveconcepts) then (
            for $c in $concept/concept 
            return if ($c[@statusCode = $inactiveStatusCodes]) then () else ($c)
        )
        else $concept/concept
    
    let $dtkey          := ($concept/valueDomain/@type)[1]
    let $datatype       := if (empty($dtkey)) then () else map:get($decorDataSetValueType, $dtkey)
    
    let $ci             := 
        concat(($concept/name[@language=$language])[1]/string(), 
            if ($concept/synonym) then concat(' (', string-join($concept/synonym, ', '), ')') else(), ' ',
            if ($concept/@type='group') then '' else concat(' (', $datatype, ') '),
            if ($isTransaction)
            then (
                if ($concept/@conformance='NP') 
                then 'NP' 
                else concat($concept/@minimumMultiplicity/string(), '..', $concept[1]/@maximumMultiplicity/string(), ' ')
            ) else '', 
            $concept/@conformance/string())
            
    let $statusColor := 
        if ($concept/@statusCode = $inactiveStatusCodes)
        then 'ad-itemnumber-blue'
        else if ($concept/@statusCode = $draftStatusCodes)
        then 'ad-itemnumber-yellow'
        else 'ad-itemnumber-green'
    
    let $cdesc    := if ($concept/desc[@language = $language]) then $concept/desc[@language = $language] else if ($concept[desc]) then ($concept/desc)[1] else ()
    let $ccontext := if ($concept/context[@language = $language]) then $concept/context[@language = $language] else if ($concept[context]) then ($concept/context)[1] else ()
    
    let $decor    := utillib:getDecorByPrefix($projectPrefix)
    let $mappings :=
        if (map:get($columnmap, 'columnMapping')) then () else
            for $templateAssociation in $decor//templateAssociation/concept[@ref=$concept/@id][@effectiveDate=$concept/@effectiveDate]
            let $tmid       := $templateAssociation/../@templateId
            let $tmed       := $templateAssociation/../@effectiveDate
            (:check decor templates first as those are indexed:)
            let $tm         := $decor//template[@id=$tmid][@effectiveDate=$tmed]
            (:check templateChain next which is in memory and as such slower:)
            let $tm         := if ($tm) then $tm else ($templateChain[@id=$tmid][@effectiveDate=$tmed])
            (: get the template by ref if nothing else found so far :)
            let $tm         := if ($tm) then $tm else tmapi:getTemplateById($tmid, $tmed)
            let $element    := $tm[1]//*[@id=$templateAssociation/@elementId]
            return (
                for $e in $element 
                let $d  := if ($e/desc[@language = $language]) then concat(' (',$e/desc[@language = $language],')') else if ($e[desc]) then concat(' (',($e/desc)[1],')') else ()
                let $cf := if ($e/@isMandatory='true') then 'M' else ($e/@conformance)
                let $mc := concat(utillib:getMinimumMultiplicity($e),' … ',utillib:getMaximumMultiplicity($e),' ',$cf)
                return
                    <li>
                    {
                        <i>{concat('&#x21a6; ', string($tm/@displayName))}</i>,
                        <div class="nowrapinline ad-templatetype {string(($tm[1]//classification/@type)[1])}">{string($tmid)}</div>,
                        concat(' ', replace($e/@name,'hl7v2:',''),' - ', $mc, $d)
                    }
                    </li>
                )
    let $maps := <ul class="tmap">{$mappings}</ul>
    
    let $html := 
        if ($level = 1) 
        then (
            <div class="ad-dataset-level1">{$ci}
              <div class="{concat('ad-dataset-itemnumber ', $statusColor)}">{tokenize($concept/@id,'\.')[last()]}</div>
              {if (map:get($columnmap, 'columnDescription')) then () else <div class="description"><i>{$cdesc/node()}</i></div>}
              {if ($isTransaction) then (if (map:get($columnmap, 'columnContext')) then () else <div class="context"><i>{$ccontext/node()}</i></div>) else ()}
              {if (empty($mappings)) then () else $maps}
            </div>,
            $condition,
            $codeList,
            for $conceptChild in $children
            return local:getSimpleConceptRows($conceptChild, $language, $projectPrefix, $decortypes, $columnmap, $showonlyactiveconcepts, $docommunity, $level+1, $isTransaction, $templateChain)
        )
        else (
            <ol class="ad-dataset-group">
                <li class="{if ($concept/@type='group') then 'ad-dataset-group' else 'ad-dataset-item'}">
                {
                    $ci
                } 
                <div class="ad-dataset-itemnumber {$statusColor}">{tokenize($concept/@id,'\.')[last()]}</div>
                {
                    if (map:get($columnmap, 'columnDescription')) then () else <div class="description"><i>{$cdesc/node()}</i></div>
                }
                {
                    if ($isTransaction) then (if (map:get($columnmap, 'columnContext')) then () else <div class="context"><i>{$ccontext/node()}</i></div>) else ()
                }
                {
                    if (empty($mappings)) then () else $maps
                }
                {
                    $condition
                }
                {
                    $codeList
                }
                {
                    for $conceptChild in $children
                    return local:getSimpleConceptRows($conceptChild, $language, $projectPrefix, $decortypes, $columnmap, $showonlyactiveconcepts, $docommunity, $level+1, $isTransaction, $templateChain)
                }
                </li>
            </ol>
        )
    return
        $html
    
};

declare %private function local:copy2mswordcompatible($input as node()*) {
    let $xslt := <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="td[div[@class='ad-dataset-level1']]">
        <table border="0" style="margin-top: 1px;">
            <xsl:apply-templates select="div[@class='ad-dataset-level1']"/>
            <xsl:apply-templates select="ol[@class='ad-dataset-group']"/>
        </table>
    </xsl:template>
    <xsl:template match="div[@class='ad-dataset-level1']">
        <tr><td colspan="2">
            <span style="font-size: 2ex; font-weight: bold;">
                <xsl:apply-templates/>
            </span>
        </td></tr>
    </xsl:template>
    <xsl:template match="div[contains(@class,'ad-dataset-itemnumber')]">
        <xsl:variable name="color">
            <xsl:choose>
                <xsl:when test="contains(@class,'ad-itemnumber-green')">#efe</xsl:when>
                <xsl:when test="contains(@class,'ad-itemnumber-blue')">#cef</xsl:when>
                <xsl:when test="contains(@class,'ad-itemnumber-yellow')">#ffe</xsl:when>
                <xsl:otherwise>#fff</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <span>&#160;</span>
        <span style="margin: -2px 0 5px 6px; display: inline; border: 1px solid #c0c0c0; 
             padding: 2px; width: auto !important; background-color:{concat('{$', 'color}')};">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="ol[@class='ad-dataset-group']">
        <tr><td style="width: 20px;"> </td><td><xsl:apply-templates/></td></tr>
    </xsl:template>
    <xsl:template match="li[@class='ad-dataset-group']">
        <div style="margin-top: 20px; font-weight: bold;">
            <xsl:apply-templates select="(@*|node()) except (ol[@class='ad-dataset-group'])"/>
            <table border="0" style="margin-top: 1px;">
                <xsl:apply-templates select="ol[@class='ad-dataset-group']"/>
            </table>
        </div>
    </xsl:template>
    <xsl:template match="li[@class='ad-dataset-item']">
        <div style="padding: 7px 0 0 0; font-weight: normal;">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    <xsl:template match="div[@class='description']">
        <div style="padding: 4px 0 0 0; font-weight: normal; font-size: 12px; color: #aaa;">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    <xsl:template match="ul[@class='tmap']">
        <span style="padding: 4px 0 0 0; margin: 0; margin-top: 6px; color:#8b0000; font-weight: normal; font-size: 12px; display: inline;">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="li[parent::ul[@class='tmap']]">
        <span style="padding: 0; margin: 0; color:#8b0000; font-weight: normal; font-size: 12px; display: inline;">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="div[contains(@class,'ad-templatetype')]">
        <xsl:variable name="color">
            <xsl:choose>
                <xsl:when test="contains(@class,'cdadocumentlevel')">#eef</xsl:when>
                <xsl:when test="contains(@class,'cdaheaderlevel')">#ffe</xsl:when>
                <xsl:when test="contains(@class,'cdasectionlevel')">#efe</xsl:when>
                <xsl:when test="contains(@class,'cdaentrylevel')">#fef</xsl:when>
                <xsl:otherwise>#fff</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <span style="margin: -2px 0 5px 6px; display: inline; border: 1px solid #c0c0c0; 
            padding: 1px 1px 1px 1px;width: auto !important; background-color:{concat('{$', 'color}')};">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="ul[@class='ad-terminology-code']">
        <ul style="list-style-position: inside; padding: 0; border: 0px; list-style-type: disc;">
            <xsl:apply-templates/>
        </ul>
    </xsl:template>
    <xsl:template match="ul[@class='ad-transaction-condition']">
        <ul style="list-style-type: circle;">
            <xsl:apply-templates/>    
        </ul>
    </xsl:template>
    <xsl:template match="li[@class='ad-transaction-condition']">
        <li style="font-style: italic; font-weight: normal; padding: 3px; ">
        <!-- border-left: 5px solid #ddd; -->
            <xsl:apply-templates/>
        </li>
    </xsl:template>
</xsl:stylesheet>

    return transform:transform($input, $xslt, ())
};
(:

function body

:)
let $id                     := if (request:exists()) then request:get-parameter('id',())[string-length()>0][1] else '2.16.840.1.113883.2.4.3.11.60.90.77.1.5'
let $effectiveDate          := if (request:exists()) then request:get-parameter('effectiveDate',()) else ()
let $conceptId              := if (request:exists()) then request:get-parameter('conceptId',())[string-length()>0][1] else '2.16.840.1.113883.2.4.3.11.60.90.77.1.5'
let $conceptEffectiveDate   := if (request:exists()) then request:get-parameter('conceptEffectiveDate',())[string-length()>0] else ()
let $communityprefix        := if (request:exists()) then request:get-parameter('community',())[string-length()>0] else ()
    (: rivm mdl bericht :)
    (: '2.16.840.1.113883.2.4.3.11.60.90.77.4.2301':) (: peri acute overdracht:)
    (:'2.16.840.1.113883.3.1937.99.62.3.4.2':) (: transactie demo1 :)
    (:'2.16.840.1.113883.2.4.3.36.77.1.1':) (: dataset rivm :)
    (:'2.16.840.1.113883.2.4.3.11.60.90.77.1.3':) (: dataset peri :)
    (:'2.16.840.1.113883.3.1937.99.62.3.1.1':) (: dataset demo1 :)
let $version                := if (request:exists()) then request:get-parameter('version',()) else ()
let $format                 := if (request:exists()) then request:get-parameter('format','html') else 'xml'
let $hidecolumns            := if (request:exists()) then request:get-parameter('hidecolumns','') else ''
let $language               := if (request:exists()) then request:get-parameter('language',()) else ()
let $unfiltered             := if (request:exists()) then request:get-parameter('unfiltered','false')='true' else false()
let $collapsed              := if (request:exists()) then request:get-parameter('collapsed', 'true')='true' else false()
let $draggable              := if (request:exists()) then request:get-parameter('draggable','true')='true' else false()
let $showonlyactiveconcepts := if (request:exists()) then request:get-parameter('showonlyactiveconcepts',if ($unfiltered) then 'false' else 'true')='true' else true()

(: if we have an id find dataset or transaction. if we do not have an id but we have a conceptId, then get that dataset :)

(: ==== Status deprecate RetrieveTransaction - xml and json dataset and transaction are replaced to api package, concept todo ===:)

let $transactionOrDataset   := 
    if (string-length($id) > 0) then
        utillib:getDataset($id, $effectiveDate, $version, $language) | utillib:getTransaction($id, $effectiveDate, $version, $language)
    else if (string-length($conceptId) > 0) then
        utillib:getConcept($conceptId, $conceptEffectiveDate, $version, $language)/ancestor::dataset
    else ()
let $transactionOrDataset   := 
    if (empty($language) or $language = '*') then ($transactionOrDataset[ancestor::decor[@language = '*']], $transactionOrDataset)[1] else $transactionOrDataset[1]

(: ==== PATCHZONE ===== :)
let $latestoncoversion      :=
    if (string-length($version) gt 0) then () else
    if ($transactionOrDataset/ancestor::decor/project[starts-with(@prefix, 'onco-')]) then (
        let $projectPrefix  := $transactionOrDataset/ancestor::decor/project/@prefix
        return 
            utillib:getDecorByPrefix($projectPrefix, 'dynamic', $language)/@versionDate
    ) else ()
    
let $transactionOrDataset   :=
    if ($latestoncoversion castable as xs:dateTime) then (
        if (string-length($id) > 0) then
            utillib:getDataset($id, $effectiveDate, $latestoncoversion) | utillib:getTransaction($id, $effectiveDate, $latestoncoversion)
        else if (string-length($conceptId) > 0) then
            utillib:getConcept($conceptId, $conceptEffectiveDate, $latestoncoversion)/ancestor::dataset
        else ()
    ) else ($transactionOrDataset)

let $version                :=
    if ($latestoncoversion castable as xs:dateTime) then ($latestoncoversion) else $version
(: ==== PATCHZONE ===== :)

(:concepts either are or are not part of a transaction. there's no point in filtering based on status:)
let $showonlyactiveconcepts := if ($transactionOrDataset[self::transaction]) then false() else $showonlyactiveconcepts

let $project                := ($transactionOrDataset/ancestor::decor)[1]
let $referenceLogo          := if ($download='true') then concat($artDeepLinkServices,'ProjectLogo?prefix=',$project/project/@prefix) else concat('ProjectLogo?prefix=',$project/project/@prefix)
let $referenceUrl           := if ($project/project/reference[@url castable as xs:anyURI]) then $project/project/reference/@url else ()
let $projectId              := $project/project/@id
let $projectPrefix          := $project/project/@prefix

let $language               := if (empty($language) or ($language = '*' and not($format = 'xml'))) then $project/project/@defaultLanguage else ($language)
(: fix for de-CH :)
let $language               := if ($language='de-CH') then 'de-DE' else $language

let $doSubConcept           := string-length($conceptId) > 0

let $fullDatasetTree        := 
    if (string-length($version) gt 0) then (
        let $datasets   := 
            if ($transactionOrDataset[self::dataset]) then ($transactionOrDataset) else (
                if ($language = '*') then
                    $setlib:colDecorVersion//transactionDatasets[@versionDate = $version]//dataset[@transactionId = $transactionOrDataset/@id][@transactionEffectiveDate = $transactionOrDataset/@effectiveDate]
                else (
                    $setlib:colDecorVersion//transactionDatasets[@versionDate = $version][@language = $language]//dataset[@transactionId = $transactionOrDataset/@id][@transactionEffectiveDate = $transactionOrDataset/@effectiveDate]
                )
            )
        
        let $datasets   :=
            if ($datasets and $doSubConcept) then (
                for $dataset in $datasets
                return
                    element {$dataset/name()} {
                        $dataset/@*,
                        $dataset/(* except (concept|history)),
                        $dataset//concept[@id = $conceptId]
                    }
            ) else ($datasets)
        
        return $datasets[1]
    ) else if ($transactionOrDataset) then (
        utillib:getFullDatasetTree($transactionOrDataset, $conceptId, $conceptEffectiveDate, $language, (), false(), (), ())
    ) else ()
    
let $community              := 
    if (empty($projectId)) then () else if ($fullDatasetTree//community) then () else (
        decorlib:getDecorCommunity($communityprefix, $projectId, $version)
    )

(:get everything hanging off from the current template so we don't miss anything, but may have duplicates in the result:)
let $templateChain          := 
    if ($format = ('xml', 'list', 'hlist')) then () else (
        if ($fullDatasetTree/@templateId) then (
            let $transactionTemplate    := tmapi:getTemplateById(string($fullDatasetTree/@templateId), string($fullDatasetTree/@templateEffectiveDate), $transactionOrDataset/ancestor::decor, $transactionOrDataset/ancestor::decor/@version, $language)/template/template[@id]
            let $decors                 := utillib:getBuildingBlockRepositories($transactionOrDataset/ancestor::decor, (), $utillib:strDecorServicesURL)
            return 
                $transactionTemplate | tmapi:getTemplateChain($transactionTemplate, $decors, map:merge(map:entry(concat($transactionTemplate/@id, $transactionTemplate/@effectiveDate), '')))
        )
        else ()
    )

let $fileNameforDownload    := 
    concat(
        if ($fullDatasetTree[@transactionId]) then 'TR_' else if ($fullDatasetTree) then 'DS_' else (),
        $fullDatasetTree/@shortName,'_(download_',substring(string(current-dateTime()),1,19),')'
    )

let $result                 := 
    if ($fullDatasetTree) then (
        let $xml            := if ($community) then utillib:mergeDatasetTreeWithCommunity($fullDatasetTree, $community, map {}) else ($fullDatasetTree)
        
        (: add-in usage of dataset for xml/json only. It is likely too much info to cram into html, so don't bother generating the info :)
        let $xml            := 
            if ($xml[@transactionId] or not($format = ('xml', 'json'))) then $xml else (
                let $latestVersion  := utillib:getDataset($transactionOrDataset/@id, ())/@effectiveDate = $transactionOrDataset/@effectiveDate
                let $transactions   := $transactionOrDataset/ancestor::decor//transaction[representingTemplate[@sourceDataset = $fullDatasetTree/@id][@sourceDatasetFlexibility = $fullDatasetTree/@effectiveDate]]
                let $transactions   := 
                    if ($latestVersion) then 
                        $transactions | $transactionOrDataset/ancestor::decor//transaction[representingTemplate[@sourceDataset = $fullDatasetTree/@id][not(@sourceDatasetFlexibility castable as xs:dateTime)]] 
                    else $transactions
                return
                    utillib:mergeDatasetWithUsage($fullDatasetTree, $transactions, $transactionOrDataset/ancestor::decor/project/@prefix)
            )
        
        let $docommunity    := exists($xml//community)
        return
        if ($format = 'xml') then (
            if (request:exists()) then (
                response:set-header('Content-Type','application/xml'), 
                if ($download='true') then response:set-header('Content-Disposition', concat('attachment; filename=',$fileNameforDownload, '.xml')) else ()
            ) else (),
            $xml
        ) 
        else 
        if ($format = 'json') then (
            if (request:exists()) then (
                response:set-header('Content-Type','application/json; charset=utf-8'),
                if ($download='true') then response:set-header('Content-Disposition', concat('attachment; filename=',$fileNameforDownload, '.json')) else ()
            ) else ()
            ,
            (: without <datasets> wrapper, projects in /releases will fail :)
            let $xml  := 
                element {'datasets'} {
                    namespace {"json"} {"http://www.json.org"}, 
                    utillib:addJsonArrayToElements($xml)
                }
            
            return
                fn:serialize($xml,
                    <output:serialization-parameters xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization">
                        <output:method>json</output:method>
                        <output:encoding>UTF-8</output:encoding>
                    </output:serialization-parameters>
                )
        ) 
        else 
        if ($format = ('list', 'hlist')) then (
            let $html   := local:getSimpleHTML($xml, $language, $hidecolumns, $showonlyactiveconcepts, (:$collapsed, $draggable, :) $version, $referenceUrl, $referenceLogo, $docommunity, $projectPrefix, $doSubConcept, $templateChain)
            return (
                if (request:exists()) then (
                    response:set-header('Content-Type','text/html'),
                    if ($download='true' or $doMSconversion='true') then response:set-header('Content-Disposition', concat('attachment; filename=',$fileNameforDownload, '.html')) else ()
                ) else (),
                if ($doMSconversion='true') then local:copy2mswordcompatible($html) else $html
            )
        )
        else (
            let $html   := local:getHTML($xml, $language, $hidecolumns, $showonlyactiveconcepts, $collapsed, $draggable, $version, $referenceUrl, $referenceLogo, $docommunity, $projectPrefix, $templateChain, $doSubConcept, $fileNameforDownload)
            return (
                if (request:exists()) then (
                    response:set-header('Content-Type','text/html'),
                    if ($download='true') then response:set-header('Content-Disposition', concat('attachment; filename=',$fileNameforDownload, '.html')) else ()
                ) else (),
                $html
            )
        )
    )
    else (
        if (request:exists()) then (
            response:set-status-code(404), response:set-header('Content-Type','application/xml')
        ) else ()
    )
return $result