xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:~
:   /decor/services/ValidateCode?prefix=demo1-&id=2.16.840.1.113883.3.1937.99.62.3.11.5&effectiveDate=2011-07-25T15:22:56&code=P&codeSystem=2.16.840.1.113883.3.1937.99.62.3.5.1
:   /decor/services/ValidateCode?prefix=demo1-&id=2.16.840.1.113883.2.4.3.11.60.40.2.0.1.2&code=OR
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../../art/api/api-server-settings.xqm";
import module namespace vs          = "http://art-decor.org/ns/decor/valueset" at "../../../art/api/api-decor-valueset.xqm";
import module namespace i18n        = "http://art-decor.org/ns/decor/i18n" at "../../../art/api/api-decor-i18n.xqm";

declare variable $artDeepLinkServices   := adserver:getServerURLServices();
declare variable $useLocalAssets        := if (request:exists()) then request:get-parameter('useLocalAssets','false') else 'false';
declare variable $download              := if (request:exists()) then request:get-parameter('download','false') else '';
(:When called with localAssets=true then we need relative local paths, else 
    we need our server services URL, but with matching scheme (http or https)
    If the scheme is https and servicesUrl is http this leads to a security 
    problem in some browsers
:)
declare variable $resourcePath          := if ($download='true') then ('https://assets.art-decor.org/ADAR/rv/assets') else if ($useLocalAssets = 'true') then ('../assets') else ('../core/assets');

declare variable $docMessages           := i18n:getMessagesDoc('decor/services');
declare variable $strArtURL             := adserver:getServerURLArt();

let $format             := if (request:exists() and request:get-parameter('format',())[string-length()>0][1]) then request:get-parameter('format','html')[string-length()>0][1] else ('html')
let $language           := if (request:exists() and request:get-parameter('language',())[string-length()>0][1]) then request:get-parameter('language',$get:strArtLanguage)[string-length()>0][1] else ($get:strArtLanguage)

let $id                 := if (request:exists() and request:get-parameter('id',())[string-length()>0][1]) then request:get-parameter('id',())[string-length()>0][1] else ()
let $effectiveDate      := if (request:exists() and request:get-parameter('effectiveDate',())[string-length()>0][1]) then request:get-parameter('effectiveDate',())[string-length()>0][1] else ()

let $code               := if (request:exists() and request:get-parameter('code',())[string-length()>0][1]) then request:get-parameter('code',())[string-length()>0][1] else ()
let $codeSystem         := if (request:exists() and request:get-parameter('codeSystem',())[string-length()>0][1]) then request:get-parameter('codeSystem',())[string-length()>0][1] else ()

let $projectPrefix      := if (request:exists() and request:get-parameter('prefix',())[string-length()>0][1]) then request:get-parameter('prefix',())[string-length()>0][1] else ()
let $projectVersion     := if (request:exists() and request:get-parameter('version',())[string-length()>0][1]) then request:get-parameter('version',())[string-length()>0][1] else ()
let $htmlInline         := if (request:exists() and request:get-parameter('inline',())[string-length()>0][1]) then request:get-parameter('inline',())[string-length()>0][1] else ()
let $displayHeader      := if ($htmlInline='true') then xs:boolean('false') else xs:boolean('true')

let $valueSets          := 
    if (empty($projectPrefix)) then
        vs:getExpandedValueSetById($id,$effectiveDate, false())
    else (
        vs:getExpandedValueSetById($id,$effectiveDate,$projectPrefix, $projectVersion, $language, false())
    )

let $result             :=
    if (empty($codeSystem)) then
        $valueSets//*[@code = $code]
    else (
        $valueSets//*[@code = $code][@codeSystem = $codeSystem] | $valueSets//completeCodeSystem[@codeSystem = $codeSystem] | $valueSets//include[@codeSystem = $codeSystem]
    )
    
return
    <result>{$result}</result>