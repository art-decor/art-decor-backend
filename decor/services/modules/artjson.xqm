xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
module namespace artjson  = "http://art-decor.org/ns/artjson";
declare namespace json      = "http://www.json.org";

(: 
    Return elements for JSON serialization 
    eXist will serialize JSON, but we want all (not only repeating) elements in JSON array
:)
declare function artjson:fastNode2json ($els as element()*) as item()* {
    for $el in $els
    return 
        if ($el[text()/string-length(normalize-space()) > 0]) 
        then element {$el/local-name()} {attribute json:array {'true'}, $el/@*, element {'content'} {string-join($el/text())}} 
        else element {$el/local-name()} {attribute json:array {'true'}, $el/@*, artjson:fastNode2json($el/*)} 
};

(:  Create json where every element is considered an array. Escape text
    \b  Backspace (ascii code 08)
    \f  Form feed (ascii code 0C)
    \n  New line
    \r  Carriage return
    \t  Tab
    \"  Double quote
    \\  Backslash character
:)
declare function artjson:node2json ($node as element()) as item()* {
    if ($node/preceding-sibling::*[local-name() = $node/local-name()]) then () else (
        if ($node/preceding-sibling::*) then ', ' else (), '"',$node/local-name(),'": [')
    ,
    '{',
        for $att at $pos in $node/@* 
        return 
            concat(if ($pos > 1) then ', ' else '', '"', $att/local-name(), '": "', artjson:escape-json-string($att), '"'),
        if ($node[text()/string-length(normalize-space()) > 0]) then (
            if ($node[@*]) then ', ' else (),
            concat('"content": "', artjson:escape-json-string(string-join($node/text(), ' ')),'"')
        )
        else (
            if ($node[@*][*]) then ', ' else (),
            for $subnode in $node/*
            return artjson:node2json($subnode)
        ),
    '}'
    ,
    if ($node/following-sibling::*[local-name() = $node/local-name()]) then (', ') else (']')
};
(: copied from https://github.com/joewiz/xqjson/blob/master/src/content/xqjson.xql 
    because I do not know if every server has xqjson loaded :)
declare %private function artjson:escape-json-string($val as xs:string) as xs:string {
  string-join(
    let $regex := '(")|(\\)|(/)|(&#x0A;)|(&#x0D;)|(&#x09;)|[^"\\/&#x0A;&#x0D;&#x09;]+'
    for $match in analyze-string($val, $regex)/*
    return
      if($match/*:group/@nr = 1) then "\"""
      else if($match/*:group/@nr = 2) then "\\"
      else if($match/*:group/@nr = 3) then "\/"
      else if($match/*:group/@nr = 4) then "\n"
      else if($match/*:group/@nr = 5) then "\r"
      else if($match/*:group/@nr = 6) then "\t"
      else string($match)
  ,"")
};