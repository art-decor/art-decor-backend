xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace i18n        = "http://art-decor.org/ns/decor/i18n" at "../../../art/api/api-decor-i18n.xqm";
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../../art/api/api-server-settings.xqm";
import module namespace vs          = "http://art-decor.org/ns/decor/valueset" at "../../../art/api/api-decor-valueset.xqm";
import module namespace aduser      = "http://art-decor.org/ns/art-decor-users" at "../../../art/api/api-user-settings.xqm";
import module namespace snomed      = "http://art-decor.org/ns/terminology/snomed" at "../../../terminology/snomed/api/api-snomed.xqm";
import module namespace adloinc     = "http://art-decor.org/ns/terminology/loinc" at "../../../terminology/loinc/api/api-loinc.xqm";
import module namespace claml       = "http://art-decor.org/ns/terminology/claml" at "../../../terminology/claml/api/api-claml.xqm";

declare namespace http              = "http://expath.org/ns/http-client";

declare variable $artDeepLinkServices   := adserver:getServerURLServices();
declare variable $artDeepLink           := adserver:getServerURLArt();
declare variable $artDeepLinkTerminology:= replace(replace($artDeepLink,'/art-decor/', '/terminology/'), ':8080/', ':8877/');

declare variable $useLocalAssets        := if (request:exists()) then request:get-parameter('useLocalAssets','false') else 'false';
declare variable $download              := if (request:exists()) then request:get-parameter('download','false') else '';
(:  When called with localAssets=true then we need relative local paths, else 
    we need our server services URL, but with matching scheme (http or https)
    If the scheme is https and servicesUrl is http this leads to a security 
    problem in some browsers
:)
declare variable $resourcePath          := 'https://assets.art-decor.org/ADAR/rv/assets/';
declare variable $resourcePath2         := adserver:getServerURLArt();

declare variable $docMessages           := i18n:getMessagesDoc('decor/services');

declare variable $codeSystemSNOMED      := '2.16.840.1.113883.6.96';
declare variable $codeSystemLOINC       := '2.16.840.1.113883.6.1';
(:declare variable $codeSystemsCLAML      := collection($get:strTerminologyData)//ClaML/Identifier/@uid;:)
declare variable $codeSystemsCLAML      := collection(concat($get:strTerminology,'/claml'))/classificationIndex//classification;

declare variable $codeSystemFilter      := if (request:exists()) then tokenize(request:get-parameter('csfilter',())[string-length()>0],'\s') else ();
declare variable $snomedgpscheck        := if (request:exists()) then request:get-parameter('snomedgps',())[1] = ('on', 'true') else true();
declare variable $localizationcheck     := if (request:exists()) then request:get-parameter('l10n',())[1] = ('on', 'true') else true();

declare variable $requestHeaders        := 
    <http:request method="GET">
        <http:header name="Content-Type" value="text/xml"/>
        <http:header name="Cache-Control" value="no-cache"/>
        <http:header name="Max-Forwards" value="1"/>
    </http:request>
;
declare variable $snomedRefsets         :=
    try { doc(concat(repo:get-root(), 'terminology-data/snomed-data/meta/refsets.xml'))/refsets }
    catch * {
        <refsets fallback="true">
            <refset id="98051000146103" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor contactallergenen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch contact allergen simple reference set</desc>
            </refset>
            <refset id="98011000146102" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch food allergen simple reference set</desc>
            </refset>
            <refset id="52801000146101" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset van landelijk implantatenregister</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch implant registry simple reference set</desc>
            </refset>
            <refset id="98021000146107" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch inhalation allergen simple reference set</desc>
            </refset>
            <refset id="98031000146109" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch insect venom allergen simple reference set</desc>
            </refset>
            <refset id="98041000146101" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch occupational allergen simple reference set</desc>
            </refset>
            <refset id="98061000146100" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor allergenen uitgezonderd medicatie</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch total non-drug allergen simple reference set</desc>
            </refset>
            <refset id="2581000146104" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor micro-organismen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch microorganism simple reference set</desc>
            </refset>
            <refset id="55451000146109" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch radio-allergosorbent test result simple reference set</desc>
            </refset>
            <refset id="97801000146108" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Netherlands microscopic ordinal test result simple reference set</desc>
            </refset>
            <refset id="46231000146109" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Netherlands ordinal test result simple reference set</desc>
            </refset>
            <refset id="110891000146105" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing intervention for delirium simple reference set</desc>
            </refset>
            <refset id="99051000146107" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor verpleegkundige interventies</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing intervention simple reference set</desc>
            </refset>
            <refset id="110881000146108" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing interventions for fall risk simple reference set</desc>
            </refset>
            <refset id="110861000146100" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing interventions for pain simple reference set</desc>
            </refset>
            <refset id="110911000146108" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing interventions for psychosocial care simple reference set</desc>
            </refset>
            <refset id="110901000146106" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing interventions for suicide simple reference set</desc>
            </refset>
            <refset id="110871000146106" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing interventions for wound simple reference set</desc>
            </refset>
            <refset id="117711000146107" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor patiëntproblemen inclusief secties van e-overdracht</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Simple reference set of Dutch nursing problems with sections of e-transfer</desc>
            </refset>
            <refset id="11721000146100" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">nationale kernset patiëntproblemen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing problem simple reference set</desc>
            </refset>
            <refset id="41000146103" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor optometrische diagnosen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch optometric diagnoses simple reference set</desc>
            </refset>
            <refset id="231000146105" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor optometrische verrichtingen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch optometric procedures simple reference set</desc>
            </refset>
            <refset id="8721000146106" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor optometrische bezoekredenen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch optometric reason for visit simple reference set</desc>
            </refset>
            <refset id="2551000146109" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor zeldzame neuromusculaire aandoeningen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch rare neuromuscular disorders simple reference set</desc>
            </refset>
            <refset id="31000147101" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">DHD Diagnosethesaurus-referentieset</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">DHD Diagnosis thesaurus reference set</desc>
            </refset>
            <refset id="110851000146103" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">PALGA thesaurus simple reference set for pathology</desc>
            </refset>
            <refset id="721144007" moduleId="900000000000012004">
                <desc languageCode="en" languageRefsetId="900000000000509007">General dentistry diagnostic reference set</desc>
            </refset>
            <refset id="450970008" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor huisartsen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">General Practice / Family Practice reference set</desc>
            </refset>
            <refset id="711112009" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset met eenvoudige 'mapping' naar ICNP-diagnosen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">ICNP diagnoses simple map reference set</desc>
            </refset>
            <refset id="712505008" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset met eenvoudige 'mapping' naar ICNP-interventies</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">ICNP interventions simple map reference set</desc>
            </refset>
            <refset id="723264001" moduleId="900000000000012004">
                <desc languageCode="en" languageRefsetId="900000000000509007">Lateralizable body structure reference set</desc>
            </refset>
            <refset id="733990004" moduleId="900000000000012004">
                <desc languageCode="en" languageRefsetId="900000000000509007">Nursing Activities Reference Set</desc>
            </refset>
            <refset id="733991000" moduleId="900000000000012004">
                <desc languageCode="en" languageRefsetId="900000000000509007">Nursing Health Issues Reference Set</desc>
            </refset>
            <refset id="721145008" moduleId="900000000000012004">
                <desc languageCode="en" languageRefsetId="900000000000509007">Odontogram reference set</desc>
            </refset>
            <refset id="32321000146103" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">SNOMED CT to ICF correlated extended map reference set</desc>
            </refset>
            <refset id="31451000146105" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">SNOMED CT to NANDA correlated map reference set</desc>
            </refset>
            <refset id="32311000146108" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">SNOMED CT to Omaha correlated extended map reference set</desc>
            </refset>
            <refset id="467614008" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset met eenvoudige 'mapping' naar GMDN</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">GMDN simple map reference set</desc>
            </refset>
            <refset id="447562003" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset met complexe 'mapping' naar ICD-10</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">ICD-10 complex map reference set</desc>
            </refset>
            <refset id="446608001" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset met eenvoudige 'mapping' naar ICD-O</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">ICD-O simple map reference set</desc>
            </refset>
            <refset id="450993002" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset met complexe 'mapping' naar ICPC-2</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">International Classification of Primary Care, Second edition complex map reference set</desc>
            </refset>
            <refset id="705112009" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset met 'mapping' naar LOINC Part-codes</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">LOINC Part map reference set</desc>
            </refset>
        </refsets>
    }
;
declare variable $snomedAssociations    :=
    try { doc(concat(repo:get-root(), 'terminology-data/snomed-data/meta/associationsValues.xml'))/associationsValues }
    catch * {
        <associationsValues fallback="true">
            <refset id="900000000000524003" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende geëxporteerde concepten</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">MOVED TO association reference set</desc>
            </refset>
            <refset id="900000000000523009" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende mogelijk equivalente concepten</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">POSSIBLY EQUIVALENT TO association reference set</desc>
            </refset>
            <refset id="900000000000526001" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende vervangende concepten</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">REPLACED BY association reference set</desc>
            </refset>
            <refset id="900000000000527005" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende identieke concepten</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">SAME AS association reference set</desc>
            </refset>
            <refset id="900000000000528000" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende voormalige ouderconcepten</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">WAS A association reference set</desc>
            </refset>
            <refset id="900000000000530003" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende alternatieve concepten</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">ALTERNATIVE association reference set</desc>
            </refset>
            <refset id="900000000000525002" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende geïmporteerde concepten</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">MOVED FROM association reference set</desc>
            </refset>
            <refset id="900000000000489007" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset met attribuutwaarden voor reden voor deactivatie van concept</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Concept inactivation indicator reference set</desc>
            </refset>
            <value id="900000000000487009" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">component verplaatst naar andere module</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Moved elsewhere</desc>
            </value>
            <value id="900000000000484002" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">ambigu component</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Ambiguous</desc>
            </value>
            <value id="900000000000482003" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">dubbel component</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Duplicate</desc>
            </value>
            <value id="900000000000483008" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">obsoleet component</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Outdated</desc>
            </value>
            <value id="723277005" moduleId="900000000000012004">
                <desc languageCode="en" languageRefsetId="900000000000509007">Nonconformance to editorial policy component</desc>
            </value>
            <value id="900000000000485001" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">foutief component</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Erroneous</desc>
            </value>
            <value id="900000000000486000" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">component van beperkte waarde</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Limited</desc>
            </value>
        </associationsValues>
    }
;

declare variable $snomedGPSmember       :=
    try { doc(concat(repo:get-root(), 'terminology-data/snomed-data/meta/snomed-gps.xml'))/gpsmembers }
    catch * {
        (: only a few on error :)
        <gpsmembers fallback="true">
            <code code="426263006"/>
            <code code="426282002"/>
            <code code="426284001"/>
            <code code="426290002"/>
            <code code="426347000"/>
            <code code="426361005"/>
            <code code="426367009"/>
            <code code="426373005"/>
            <code code="426389008"/>
            <code code="426396005"/>
            <code code="426417003"/>
            <code code="426424002"/>
            <code code="426439001"/>
            <code code="426000000"/>
        </gpsmembers>
    }
;

declare %private function local:getDisplayNameFromCodesystem($code as xs:string, $codeSystem as xs:string, $language as xs:string) as xs:string* {
    if ($codeSystem=$codeSystemSNOMED) then (
        (:let $service-uri        := concat($artDeepLinkTerminology,'snomed/getConcept/',encode-for-uri($code)):)
        (: protection against empty responses. This leads to 
            exerr:ERROR Unable to add Source to result:Premature end of file. [source: String] :)
        (:let $server-response    := try { http:send-request($requestHeaders, $service-uri) } catch * {()}:)
        
        (:return :)
            (:$server-response//concept[@conceptId=$code]/desc[@type='fsn'][@active]:)
            $snomed:colDataBase//@conceptId[. = $code]/parent::concept
    )
    else if ($codeSystem=$codeSystemLOINC) then (
        (:let $service-uri        := concat($artDeepLinkTerminology,'loinc/searchLOINC/',encode-for-uri($code)):)
        (: protection against empty responses. This leads to 
            exerr:ERROR Unable to add Source to result:Premature end of file. [source: String] :)
        (:let $server-response    := try { http:send-request($requestHeaders, $service-uri) } catch * {()}:)
        
        (:return
            $server-response//concept[@loinc_num=$code]/longName:)
        let $concept        := collection(concat($adloinc:strLoincData,$adloinc:anyLanguage))//concept[@loinc_num = $code]
        
        return
            $concept[1]/longName | $concept[1]/shortName
    )
    else (
        (:let $service-uri        := concat($artDeepLinkTerminology,'claml/RetrieveClass?mode=xml&amp;classificationId=',$codeSystem,'&amp;code=',encode-for-uri($code)):)
        (: protection against empty responses. This leads to 
            exerr:ERROR Unable to add Source to result:Premature end of file. [source: String] :)
        (:let $server-response    := try { http:send-request($requestHeaders, $service-uri) } catch * {()}:)
        
        (:return
            $server-response//Class[@code=$code][@classificationId=$codeSystem]/Rubric[@kind='preferred']/Label[1]:)
        let $class  := claml:getPreparedClass((), $codeSystem, $code, '')
        
        return  ($class/Rubric[@kind='preferred']/Label[@xml:lang = $language], $class/Rubric[@kind='preferred']/Label)[1]
    )
};

declare %private function local:getDisplayNameAndStatus($code as xs:string, $codeSystem as xs:string, $defaultLanguage as xs:string, $allLanguages as xs:string+) as element()* {
    if ($codeSystem = $codeSystemSNOMED) then
        let $lang                       := tokenize($defaultLanguage, '-')[1]
        let $languageRefsetId           := 
            switch ($defaultLanguage)
            case 'nl-NL' return '31000146106'
            case 'de-DE' return '722130004'
            default return '900000000000509007'
        let $fallbackLanguageRefsetId   := '900000000000509007'
        (:let $service-uri        := concat($artDeepLinkTerminology,'snomed/getConcept/',encode-for-uri($code)):)
        (: protection against empty responses. This leads to 
            exerr:ERROR Unable to add Source to result:Premature end of file. [source: String] :)
        (:let $server-response    := try { http:send-request($requestHeaders, $service-uri) } catch * {()}
        
        let $concept        := $server-response//concept[@conceptId = $code]:)
        
        let $concept        := $snomed:colDataBase//@conceptId[. = $code]/parent::concept
        let $displayNames   := $concept/desc[@active]
        let $statusCode     := if ($concept[@active]) then 'active' else if ($concept) then 'deprecated' else ()
        let $statusText     := $statusCode
        
        let $deprecation    := $concept/attributeValue[@active][@refsetId = '900000000000489007']
        let $deprCode       := $deprecation/@valueId
        let $deprText       := $snomedAssociations//value[@id = $deprCode]
        let $deprText       := ($deprText/desc[@languageRefsetId = $languageRefsetId], $deprText/desc[@languageRefsetId = $fallbackLanguageRefsetId])
        let $deprText       := if ($deprText) then $deprText[1] else $deprecation[1]/@refset
        let $l10nMissing    := empty($displayNames[@languageRefsetId = $languageRefsetId])
        return
            <codeSystem id="{$codeSystem}">
            {
                if (empty($concept)) then attribute missing {'true'} else (),
                if ($localizationcheck and $l10nMissing) then attribute missingTranslation {$l10nMissing} else (),
                if (empty($statusCode)) then () else attribute originalStatusCode {$statusCode},
                if (empty($statusText)) then () else attribute originalStatusText {$statusText},
                if ($deprCode) then attribute deprecationCode {$deprCode} else (),
                if ($deprText) then attribute deprecationText {$deprText} else ()
            }
            {
                for $displayName in $displayNames
                return
                    <desc language="{$displayName/@languageCode}" originalDisplayName="{$displayName}">
                    {
                        attribute type {
                            switch ($displayName/@type) 
                            case 'fsn' return 'fsn' 
                            case 'pref' return 'preferred' 
                            case 'syn' return 'synonym' 
                            default return $displayName/@type
                        },
                        attribute preferredDisplayName {
                            (: preferred term if in the right language refset and fsn :)
                            if ($displayNames[@languageRefsetId = $languageRefsetId][@type = 'fsn']) then 
                                $displayName[@languageRefsetId = $languageRefsetId]/@type = 'fsn'
                            else
                            (: preferred term if in the right language refset that has no fsn, but has preferred :)
                            if ($displayNames[@languageRefsetId = $languageRefsetId][@type = 'pref']) then 
                                $displayName[@languageRefsetId = $languageRefsetId]/@type = 'pref'
                            else
                            (: preferred term if in the fallback language refset and fsn :)
                            if ($displayNames[@languageRefsetId = $fallbackLanguageRefsetId][@type = 'fsn']) then
                                $displayName[@languageRefsetId = $fallbackLanguageRefsetId]/@type = 'fsn'
                            else
                            (: preferred term if in the fallback language refset that has no fsn, but has preferred :)
                            if ($displayNames[@languageRefsetId = $fallbackLanguageRefsetId][@type = 'pref']) then 
                                $displayName[@languageRefsetId = $fallbackLanguageRefsetId]/@type = 'pref'
                            else (
                                (: preferred term if not available in the right refset, but at least in the core or the right language as fsn :)
                                $displayName[empty(@languageRefsetId)]/@type = 'fsn' or $displayName[@languageCode = $lang]/@type = 'fsn'
                            )
                        },
                        $displayName/(@* except (@language | @originalDisplayName | @type | @preferred))
                    }
                    </desc>
            }
            {
                (:<!-- attribueValue | associations -->:)
                (:<!-- The refsets MOVE TO (900000000000524003) and MOVED FROM (900000000000525002) refer to a module that a concept was moved into or out of -->:)
                (:<!-- REFERS TO is for descriptions and may be ignored (900000000000531004) -->:)
                (:<!-- SIMILAR TO is no longer in use (900000000000529008) -->:)
                for $assocs in $concept/association[@active][not(@refsetId = '900000000000531004')]
                let $refsetId   := $assocs/@refsetId
                group by $refsetId
                return (
                    let $refset     := ($snomedAssociations//refset[@id = $refsetId], $snomedRefsets//refset[@id = $refsetId])
                    let $refsetName := ($refset/desc[@languageRefsetId = $languageRefsetId], $refset/desc[@languageRefsetId = $fallbackLanguageRefsetId])
                    let $refsetName := if ($refsetName) then $refsetName[1] else $assocs[1]/@refset
                    
                    for $assoc in $assocs 
                    return 
                        <association>{$assoc/(@* except @refset), attribute refset {$refsetName}, attribute uri {concat($artDeepLink,'snomed-ct?conceptId=',encode-for-uri($assoc/@targetComponentId))}}</association>
                )
            }
            </codeSystem>
    else 
    if ($codeSystem = $codeSystemLOINC and starts-with($code, 'LA')) then () else
    if ($codeSystem = $codeSystemLOINC) then
        (:let $service-uri        := concat($artDeepLinkTerminology,'loinc/searchLOINC/',encode-for-uri($code)):)
        (: protection against empty responses. This leads to 
            exerr:ERROR Unable to add Source to result:Premature end of file. [source: String] :)
        (:let $server-response    := try { http:send-request($requestHeaders, $service-uri) } catch * {()}:)
        
        (:let $concept        := $server-response//concept[@loinc_num = $code]:)
        
        let $concept        := collection(concat($adloinc:strLoincData,$adloinc:anyLanguage))//concept[@loinc_num = $code][1]
        let $displayNames   := $concept//longName | $concept//shortName
        let $statusText     := $concept/@status
        let $statusCode     := 
            if (empty($statusText)) then () else 
            if (lower-case($statusText)='active')                       then 'active' else 
            if (lower-case($statusText)='trial')                        then 'pending' else 
            if (lower-case($statusText)=('deprecated','discouraged'))   then 'deprecated' else (
                lower-case($statusText)
            )
        
        let $deprecation    := $concept/elem[@name = 'STATUS_REASON']
        let $deprCode       := $deprecation
        let $deprText       := $deprecation
        let $l10nlang       := $displayNames/ancestor::concept[1]/@language
        let $l10nMissing    := if (empty($l10nlang)) then not($defaultLanguage = 'en-US') else not($defaultLanguage = $l10nlang)
        return
            <codeSystem id="{$codeSystem}">
            {
                if (empty($concept)) then attribute missing {'true'} else (),
                (:if ($localizationcheck and $l10nMissing) then attribute missingTranslation {$l10nMissing} else (),:)
                if (empty($statusCode)) then () else attribute originalStatusCode {$statusCode},
                if (empty($statusText)) then () else attribute originalStatusText {$statusText},
                if ($deprCode) then attribute deprecationCode {$deprCode} else (),
                if ($deprText) then attribute deprecationText {$deprText} else ()
            }
            {
                for $displayName in $displayNames
                let $lang   := $displayName/ancestor::concept[1]/@language
                return
                    <desc language="{if ($lang) then $lang else 'en-US'}" originalDisplayName="{$displayName}"/>
            }
            {
                for $assoc in $concept/map[@from = $code]
                return
                    <association targetComponentId="{$assoc/@to}" uri="{concat($artDeepLink,'loinc?conceptId=',encode-for-uri($assoc/@to))}"/>
            }
            </codeSystem>
    else (
        (:let $service-uri        := concat($artDeepLinkTerminology,'claml/RetrieveClass?mode=xml&amp;classificationId=',$codeSystem,'&amp;code=',encode-for-uri($code)):)
        (: protection against empty responses. This leads to 
            exerr:ERROR Unable to add Source to result:Premature end of file. [source: String] :)
        (:let $server-response    := try { http:send-request($requestHeaders, $service-uri) } catch * {()}:)
        
        (:let $concept        := $server-response//Class[@code=$code][@classificationId=$codeSystem]:)
        
        let $concept        := claml:getPreparedClass((), $codeSystem, $code, '')
        let $displayNames   := $concept/Rubric[@kind='preferred']/Label
        let $statusCode     := $concept/Meta[@name='statusCode']/@value
        let $statusText     := $statusCode
        let $l10nMissing    := empty($displayNames[@xml:lang = $defaultLanguage])
        return
            <codeSystem id="{$codeSystem}">
            {
                if (empty($concept)) then attribute missing {'true'} else (),
                (:if ($localizationcheck and $l10nMissing) then attribute missingTranslation {$l10nMissing} else (),:)
                if (empty($statusCode)) then () else attribute originalStatusCode {$statusCode},
                if (empty($statusText)) then () else attribute originalStatusText {$statusText}
            }
            {
                for $displayName in $displayNames
                return
                    <desc>
                    {
                        if ($displayName[@xml:lang]) then (attribute language {$displayName/@xml:lang}) else (),
                        attribute originalDisplayName {$displayName},
                        attribute preferredDisplayName {$displayName/@xml:lang = $defaultLanguage}
                    }
                    </desc>
            }
            </codeSystem>
    )
};

(: support concept and conceptList/concept :)
declare %private function local:getOriginalConceptName($id as xs:string, $effectiveDate as xs:string?) as element(conceptName)* {
    let $concept            := art:getOriginalForConcept(<concept><inherit ref="{$id}">{if ($effectiveDate castable as xs:dateTime) then                attribute effectiveDate {$effectiveDate}            else ()}</inherit></concept>)
    
    for $name in $concept/name
        return <conceptName>{$name/@*, $name/node()}</conceptName>
};

declare %private function local:handleTerminologyAssociations($association as element(terminologyAssociation), $project as element(decor), $language as xs:string) as element()* {
    (: only proces if not filtering or if matches filter :)
    if (empty($codeSystemFilter) or $association[@codeSystem = $codeSystemFilter]) then (
        let $prefix         := $project/project/@prefix
        
        let $conceptIds     := $get:colDecorData//concept[@id = $association/@conceptId][not(ancestor::history | ancestor::conceptList)]
        let $conceptIds     := 
            if ($association/@conceptFlexibility[not(. = 'dynamic')]) then 
                $conceptIds[@effectiveDate = $association/@conceptFlexibility]
            else
            if ($conceptIds/@effectiveDate | $conceptIds/@flexibility) then (
                let $max    := max($conceptIds/xs:dateTime(@effectiveDate))
                return
                    if (empty($max)) then $conceptIds else $conceptIds[@effectiveDate = $max]
            )
            else (
                $conceptIds
            )
        let $conceptListIds := $get:colDecorData//concept[@id = $association/@conceptId][not(ancestor::history)]/ancestor::conceptList[1]/@id
        let $valueSets      := ()
        
        let $matchingCodes  := 
            if ($conceptIds) then 
                (:if it is a normal concept, get from codesystem.:)
                local:getDisplayNameAndStatus($association/@code, $association/@codeSystem, $project/project/@defaultLanguage, $project/project/name/@language)
            else (
                (:if it is a conceptList/concept, check valueSets that the conceptList is bound to:)
                let $vsTermAssocs   := $project//terminologyAssociation[@conceptId = $conceptListIds]
                let $valueSets      := for $vsta in $vsTermAssocs return vs:getExpandedValueSetByRef($vsta/@valueSet, if ($vsta/@flexibility) then $vsta/@flexibility else ('dynamic'), $prefix, $project/@versionDate, $project/@language, false())//valueSet[@id]
                
                return
                distinct-values($valueSets//concept[@code = $association/@code][@codeSystem=$association/@codeSystem]/@displayName |
                                $valueSets//exception[@code = $association/@code][@codeSystem=$association/@codeSystem]/@displayName)
           )
        
        let $matchingCodes  := 
            if (empty($matchingCodes) and $conceptIds) then 
                (: if we still don't have a displayName and it is a normal concept try all project valueSets as it might be a locally defined code:)
                distinct-values($project//valueSet/*/concept[@code=$association/@code][@codeSystem=$association/@codeSystem]/@displayName) 
            else 
            if (empty($matchingCodes) and $conceptListIds and empty($valueSets)) then 
                (: if we still don't have a displayName and it is a conceptList/concept and there was no valueSet binding at conceptList level, then try all project valueSets:)
                distinct-values($project//valueSet/*/concept[@code=$association/@code][@codeSystem=$association/@codeSystem]/@displayName |
                                $project//valueSet/*/exception[@code=$association/@code][@codeSystem=$association/@codeSystem]/@displayName)
            else 
                $matchingCodes
        
        let $matchingCodes  :=
            if (empty($matchingCodes) and $conceptIds) then
                if ($association/@codeSystem = $codeSystemLOINC and starts-with($association/@code, 'LA')) then $association/@displayName else ()
            else
                $matchingCodes
        
        let $matchingCodes  :=
            for $matchingCode in $matchingCodes
            return
                if ($matchingCode instance of element()) then
                    $matchingCode
                else (
                    <codeSystem originalStatusCode="" originalStatusText="">
                        <desc originalDisplayName="{$matchingCode}" type="fsn"/>
                    </codeSystem>
                )
        let $preferredDisplayNames          := 
            if ($matchingCodes/desc[@preferredDisplayName = 'true']) then 
                $matchingCodes/desc[@preferredDisplayName = 'true'] 
            else 
                $matchingCodes/desc
        let $preferredDisplayNames          := 
            if ($preferredDisplayNames[@type = 'fsn']) then 
                $preferredDisplayNames[@type = 'fsn']/@originalDisplayName
            else (
                $preferredDisplayNames/@originalDisplayName
            )
        let $statusCode         := $matchingCodes/@originalStatusCode[string-length()>0]
        let $matchingNameFound  := exists($matchingCodes//@originalDisplayName[lower-case(.)=$association/lower-case(@displayName)])
        (:let $matchingCodes  :=
            if ($matchingCodes//@preferredDisplayName = 'true') then $matchingCodes/descendant-or-self::*[@preferredDisplayName = 'true'] else $matchingCodes:)
        
        return
            (:update delete $association/@displayName
            ,
            update insert attribute displayName {$matchingCodes} into $association
            ,:)
            <terminologyAssociation>
            {
                $association/@*,
                attribute conceptType {if ($conceptIds) then 'concept' else if ($conceptListIds) then 'conceptListConcept' else ()},
                ($matchingCodes/@missing)[1],
                ($matchingCodes/@missingTranslation)[1],
                $statusCode,
                ($matchingCodes/@originalStatusText)[1],
                ($matchingCodes/@deprecationCode)[1],
                ($matchingCodes/@deprecationText)[1],
                attribute pfx  {($conceptIds/ancestor::decor/project/@prefix | $conceptListIds/ancestor::decor/project/@prefix)[1]},
                attribute dsid {($conceptIds/ancestor::dataset/@id | $conceptListIds/ancestor::dataset/@id)[1]},
                attribute dsed {($conceptIds/ancestor::dataset/@effectiveDate | $conceptListIds/ancestor::dataset/@effectiveDate)[1]},
                attribute deid {($conceptIds/ancestor-or-self::concept[not(ancestor::conceptList)][1]/@id               | $conceptListIds/ancestor-or-self::concept[not(ancestor::conceptList)][1]/@id)[1]},
                attribute deed {($conceptIds/ancestor-or-self::concept[not(ancestor::conceptList)][1]/@effectiveDate    | $conceptListIds/ancestor-or-self::concept[not(ancestor::conceptList)][1]/@effectiveDate)[1]},
                if ($matchingNameFound and (empty($statusCode) or $statusCode='active')) then () else (
                    attribute originalDisplayName {$preferredDisplayNames}
                ),
                if ($snomedgpscheck and $association[@codeSystem = $codeSystemSNOMED]) then (
                    attribute isSnomedGPSMember {local:isSnomedGPSMember($association/@code)}
                ) else ()
                ,
                if ($matchingNameFound) then () else (
                    for $matchingCode in $matchingCodes
                    let $displayName            := $matchingCode//@originalDisplayName
                    let $preferredDisplayName   := ($matchingCode/descendant-or-self::*[@type='fsn']/@originalDisplayName, $displayName)[1]
                    let $statusCode             := $matchingCode/ancestor-or-self::*/@originalStatusCode[string-length()>0]
                    let $statusText             := $matchingCode/ancestor-or-self::*/@originalStatusText
                    return <originalDisplayName originalStatusCode="{$statusCode}" originalStatusText="{$statusText}">{data($preferredDisplayName)}</originalDisplayName>
                )
                ,
                if (not($matchingNameFound) or $matchingCodes/@missing = 'true' or $matchingCodes/@missingTranslation = 'true') then (
                    for $conceptName in local:getOriginalConceptName($association/@conceptId, $association/@conceptFlexibility)
                    let $name := $conceptName/text()
                    group by $name
                    return $conceptName[1]
                ) else (),
                $matchingCodes/association
            }
            </terminologyAssociation>
    )
    else (
        $association
    )
};

declare %private function local:handleValuesets($valueSet as element(valueSet), $terminologyAssociations as item(), $language as xs:string) as element()* {
    if (empty($codeSystemFilter) or $valueSet[conceptList//@codeSystem = $codeSystemFilter]) then (
        <valueSet>
        {
            $valueSet/@*,
            $valueSet/(* except conceptList)
        }
        {
            if ($valueSet/conceptList) then
                <conceptList>
                {
                    for $node in $valueSet/conceptList/*
                    return
                        if ($node/self::include or $node[not(@code and @codeSystem)]) then $node else (
                            (:
                            <codeSystem originalStatusCode="active" originalStatusText="active">
                                <desc originalDisplayName="Complaint (finding)" type="fsn"/>
                                <desc originalDisplayName="Complaint" type="pref"/>
                                <desc originalDisplayName="klacht" type="pref"/>
                            </codeSystem>
                            :)
                            let $termAssocs                     := map:get($terminologyAssociations, concat($node/@code, $node/@codeSystem))
                            let $displayNameAndStatus           := local:getDisplayNameAndStatus($node/@code, $node/@codeSystem, $valueSet/ancestor::decor/project/@defaultLanguage, $valueSet/ancestor::decor/project/name/@language)
                            let $displayNames                   := $displayNameAndStatus/desc/@originalDisplayName
                            let $displayNames                   :=
                                if (empty($displayNames)) then
                                    if ($node/@codeSystem = $codeSystemLOINC and starts-with($node/@code, 'LA')) then $node/@displayName else ()
                                else
                                    $displayNames
                            let $lowerCasedDisplayName          := for $d in $displayNames return lower-case($d)
                            let $preferredDisplayNames          := 
                                if ($displayNameAndStatus/desc[@preferredDisplayName = 'true']) then 
                                    $displayNameAndStatus/desc[@preferredDisplayName = 'true'] 
                                else 
                                    $displayNameAndStatus/desc
                            let $preferredDisplayNames          := 
                                if ($preferredDisplayNames[@type = 'fsn']) then 
                                    $preferredDisplayNames[@type = 'fsn']/@originalDisplayName
                                else (
                                    $preferredDisplayNames/@originalDisplayName
                                )
                            let $lowerCasedPreferredDisplayName := for $d in $preferredDisplayNames return lower-case($d)
                            let $statusCode                     := $displayNameAndStatus/@originalStatusCode[string-length()>0]
                            let $statusText                     := $displayNameAndStatus/@originalStatusText
                            let $deprCode                       := $displayNameAndStatus/@deprecationCode
                            let $deprText                       := $displayNameAndStatus/@deprecationText
                            let $conceptMissing                 := $displayNameAndStatus/@missing
                            let $l10nMissing                    := $displayNameAndStatus/@missingTranslation
                            return
                            element {$node/name()} {
                                $node/@*,
                                if ($node[@codeSystem = $codeSystemSNOMED]) then (
                                    attribute uri {concat($artDeepLink,'snomed-ct?conceptId=',encode-for-uri($node/@code))},
                                    if ($lowerCasedDisplayName = lower-case($node/@displayName) and 
                                        (empty($statusCode) or $statusCode='active')) then () else (
                                        attribute originalDisplayName {$preferredDisplayNames}
                                    ),
                                    if (empty($statusCode) or $statusCode='active') then () else ($statusCode, $statusText)
                                    ,
                                    if ($snomedgpscheck and $node[@codeSystem = $codeSystemSNOMED]) then (
                                        attribute isSnomedGPSMember {local:isSnomedGPSMember($node/@code)}
                                    ) else (),
                                    $deprCode, $deprText, $conceptMissing, $l10nMissing
                                )
                                else if ($node[@codeSystem = $codeSystemLOINC]) then (
                                    attribute uri {concat($artDeepLink,'loinc?conceptId=',encode-for-uri($node/@code))},
                                    if ($lowerCasedDisplayName = lower-case($node/@displayName) and 
                                        (empty($statusCode) or $statusCode='active')) then () else (
                                        attribute originalDisplayName {$preferredDisplayNames}
                                    ),
                                    if (empty($statusCode) or $statusCode='active') then () else ($statusCode, $statusText)
                                    ,
                                    $deprCode, $deprText, $conceptMissing, $l10nMissing
                                )
                                else if ($codeSystemsCLAML[@id = $node/@codeSystem]) then (
                                    attribute uri {concat($artDeepLink,'claml?classificationId=',encode-for-uri($node/@codeSystem),'&amp;conceptId=',encode-for-uri($node/@code))},
                                    if ($lowerCasedDisplayName = lower-case($node/@displayName) and 
                                        (empty($statusCode) or $statusCode='active')) then () else (
                                        attribute originalDisplayName {$preferredDisplayNames}
                                    ),
                                    if (empty($statusCode) or $statusCode='active') then () else ($statusCode, $statusText)
                                    ,
                                    $deprCode, $deprText, $conceptMissing, $l10nMissing
                                )
                                else if (count($displayNames) gt 0) then (
                                    if ($lowerCasedDisplayName = lower-case($node/@displayName) and 
                                        (empty($statusCode) or $statusCode='active')) then () else (
                                        attribute originalDisplayName {$preferredDisplayNames}
                                    ),
                                    if (empty($statusCode) or $statusCode='active') then () else ($statusCode, $statusText)
                                    ,
                                    $deprCode, $deprText, $conceptMissing, $l10nMissing
                                )
                                else (),
                                $node/node(),
                                let $conceptNames       :=
                                    for $terminologyAssociation in $termAssocs
                                    let $conceptId := $terminologyAssociation/@conceptId
                                    group by $conceptId
                                    return
                                        local:getOriginalConceptName($terminologyAssociation[1]/@conceptId, $terminologyAssociation[1]/@conceptFlexibility)
                                
                                for $conceptName in $conceptNames
                                let $name := $conceptName/text()
                                group by $name
                                return $conceptName[1]
                                ,
                                $displayNameAndStatus/association
                            }
                        )
                }
                </conceptList>
            else ()
        }
        </valueSet>
    )
    else (
        $valueSet
    )
};

declare %private function local:isSnomedGPSMember($code as xs:string) as xs:boolean {
    exists($snomedGPSmember/code[@code=$code])
};

declare %private function local:doReport($decorproject as element(decor), $reporttype as xs:string, $now as xs:string, $language as xs:string) {
    <decor>
    {
        $decorproject/@*,
        '&#10;',
        comment {
            '&#10;',
            'This is a report version of a DECOR based project. Report date: ', $now ,'&#10;',
            'PLEASE NOTE THAT ITS ONLY PURPOSE IS TO REPORT INFORMATION. HENCE THIS IS A ONE OFF FILE UNSUITED FOR ANY OTHER PURPOSE','&#10;'
        },
        for $node in $decorproject/node()
        return
            if ($node/name()='terminology') then (
                <terminology>
                {
                    for $subnode in $node/node()
                    return
                        if ($reporttype=('overview', 'terminologyassociations') and $subnode[name()='terminologyAssociation'][@code][@codeSystem][empty(@expirationDate)]) then (
                            local:handleTerminologyAssociations($subnode, $decorproject, $language)
                        )
                        else 
                        if ($reporttype=('overview', 'valuesets') and $subnode[name() = 'valueSet'][empty(@expirationDate)][@statusCode = ('new','draft','final','pending')]) then (
                            local:handleValuesets($subnode, 
                                map:merge(
                                    for $ta in $node/terminologyAssociation
                                    let $key    := concat($ta/@code, $ta/@codeSystem)
                                    group by $key
                                    return
                                        if (string-length($key) = 0) then () else map:entry($key, $ta[1])
                                ), $language)
                        )
                        else (
                            $subnode
                        )
                }
                </terminology>
            ) else (
                $node
            )
    }
    </decor>
};

declare %private function local:projectTerminology2html($decorproject as element(), $language as xs:string, $mode as xs:string, $i18nmap as item()) as item()* {
    let $errorimg           := <img src="{$resourcePath2}img/error.png" alt="" title="{map:get($i18nmap, 'trConceptNotFoundOrMismatch')}" style="padding:0 8px 0 0; vertical-align: middle;"/>
    let $okimg              := <img src="{$resourcePath2}img/IssueStatusCodeLifeCycle_closed.png" alt="" title="{map:get($i18nmap, 'trNoProblemsFound')}" style="padding:0 8px 0 0; vertical-align: middle;"/>
    let $notsnomedgpsimg    := <img src="{$resourcePath2}img/error.png" alt="" title="SNOMED Global Patient Set (GPS)" style="padding:0 8px 0 0; vertical-align: middle;"/>
    let $snomedgpsimg       := <img src="{$resourcePath2}img/IssueStatusCodeLifeCycle_closed.png" alt="" title="SNOMED Global Patient Set (GPS)" style="padding:0 8px 0 0; vertical-align: middle;"/>
    let $nol10nimg          := <img src="{$resourcePath2}img/error.png" alt="" title="{map:get($i18nmap, 'translationMissing')}" style="padding:0 8px 0 0; vertical-align: middle;"/>
    let $l10nimg            := <img src="{$resourcePath2}img/IssueStatusCodeLifeCycle_closed.png" alt="" title="{map:get($i18nmap, 'translationFound')}" style="padding:0 8px 0 0; vertical-align: middle;"/>
    
    let $language               := $decorproject/project/@defaultLanguage
    let $projectPrefix          := $decorproject/project/@prefix
    let $associations           := $decorproject/terminology/terminologyAssociation[@code]
    let $diffAssociations       := 
        if (empty($codeSystemFilter)) then 
            $associations[@originalDisplayName | @originalStatusCode[not(. = 'active')] | @isSnomedGPSMember | @missing | @missingTranslation | originalDisplayName]
        else (
            $associations[@originalDisplayName | @originalStatusCode[not(. = 'active')] | @isSnomedGPSMember | @missing | @missingTranslation | originalDisplayName][@codeSystem = $codeSystemFilter]
        )
    let $valueSets              := $decorproject/terminology/valueSet[@id]
    let $diffValueSets          := 
        if (empty($codeSystemFilter)) then 
            $valueSets[.//@originalDisplayName | .//@originalStatusCode[not(. = 'active')] | .//@isSnomedGPSMember | .//@missing | .//@missingTranslation]
        else (
            $valueSets[.//@originalDisplayName | .//@originalStatusCode[not(. = 'active')] | .//@isSnomedGPSMember | .//@missing | .//@missingTranslation][conceptList//@codeSystem = $codeSystemFilter]
        )
    let $countAssociations      := count($decorproject/terminology/terminologyAssociation[@code])
    let $countValueSets         := count($decorproject/terminology/valueSet[@id])
    let $diffCountAssociations  := count($diffAssociations)
    let $diffCountValueSets     := count($diffValueSets)
    let $r := 
    <body>
        {if ($associations) then ( 
            <h2 id="associationHeader">{i18n:getMessage($docMessages, 'trAssociationFoundCountTitle', $language, $diffCountAssociations)}</h2>,
            <table width="100%" class="zebra-table">
                <tr>
                    <th>{map:get($i18nmap, 'columnConceptId')}</th>
                    <th>{map:get($i18nmap, 'columnConceptType')}</th>
                    <th>{map:get($i18nmap, 'columnCode')}</th>
                    <th>{map:get($i18nmap, 'columnCodeSystem')}</th>
                    <th>{map:get($i18nmap, 'columnDisplayName')}</th>
                    <th>{map:get($i18nmap, 'columnConceptName')}</th>
                    <th>{map:get($i18nmap, 'columnOriginalData')}</th>
                </tr>
            {
                for $association at $pos in $diffAssociations
                let $deeplinktocode := 
                    if ($association[@codeSystem=$codeSystemSNOMED]) then
                        concat($artDeepLink,'snomed-ct?conceptId=',encode-for-uri($association/@code))
                    else if ($association[@codeSystem=$codeSystemLOINC]) then
                        concat($artDeepLink,'loinc?conceptId=',encode-for-uri($association/@code))
                    else if ($codeSystemsCLAML[@id=$association/@codeSystem]) then
                        concat($artDeepLink,'claml?classificationId=',encode-for-uri($association/@codeSystem),'&amp;conceptId=',encode-for-uri($association/@code))
                    else ()
                return (
                <tr class="zebra-row-{if ($pos mod 2 = 0) then 'even' else 'odd'}">
                    <td>
                        <a href="{concat(adserver:getServerURLArt(), 'decor-terminology--', $association/@pfx, '?datasetId=', $association/@dsid, '&amp;datasetEffectiveDate=', encode-for-uri($association/@dsed), '&amp;conceptId=', $association/@deid, '&amp;conceptEffectiveDate=', encode-for-uri($association/@deed))}" alt="" target="_new">
                        {
                            data($association/@conceptId)
                        }
                        </a>
                        {
                            local:buildReferenceBox($projectPrefix, $association/@pfx)
                        }
                    </td>
                    <td>{if ($association/@conceptType = 'concept') then map:get($i18nmap, 'Concept') else map:get($i18nmap, 'Value')}</td>
                    <td>
                    {
                        if ($deeplinktocode) then 
                            <a href="{$deeplinktocode}" alt="" target="_new">{data($association/@code)}</a>
                        else
                            $association/@code/string()
                    }
                    </td>
                    <td>{data($association/@codeSystem), if ($association/@codeSystemVersion) then concat(' version ',$association/@codeSystemVersion) else ()}</td>
                    <td>{data($association/@displayName)}</td>
                    <td>
                    {
                        for $conceptName in $association/conceptName
                        return <div>({$conceptName/@language/string()}) {data($conceptName)}</div>
                    }
                    </td>
                    <td>
                    {
                        if ($association/@isSnomedGPSMember="true")
                        then (<div>{ $snomedgpsimg, <span>SNOMED GPS</span>}</div>)
                        else if ($association/@isSnomedGPSMember="false")
                        then (<div>{ $notsnomedgpsimg, <span>Not SNOMED GPS</span>}</div>)
                        else ()
                    }
                    {
                        if ($association/@missing) then () else
                        if ($association/@missingTranslation="true")
                        then (<div>{ $nol10nimg, <span>{map:get($i18nmap, 'translationMissing')}</span>}</div>)
                        else if ($association/@missingTranslation="false")
                        then (<div>{ $l10nimg, <span>{map:get($i18nmap, 'translationFound')}</span>}</div>)
                        else ()
                    }
                    {
                        let $imgtitle       :=
                            if ($association/@conceptType='concept') then
                                map:get($i18nmap, 'trConceptNotFoundOrMismatchName')
                            else if ($association/@conceptType='conceptListConcept') then
                                map:get($i18nmap, 'trConceptNotInValueSetOrMismatchName')
                            else (
                                map:get($i18nmap, 'trConceptTypeUnknownOrMismatchName')
                            )
                        return
                        if ($association[@missing]) then
                            <img src="{$errorimg/@src}" alt="{$errorimg/@alt}" title="{$imgtitle}" style="{$errorimg/@style}"/>
                        else
                        if ($association[string-length(@originalDisplayName) gt 0]) then 
                            <img src="{$errorimg/@src}" alt="{$errorimg/@alt}" title="{$imgtitle}" style="{$errorimg/@style}"/>
                        else 
                        if ($association/@originalStatusCode[not(. = 'active')]) then 
                            <img src="{$errorimg/@src}" alt="{$errorimg/@alt}" title="{$imgtitle}" style="{$errorimg/@style}"/>
                        else ((:$okimg:))
                    }
                    {
                        if ($association/@originalStatusCode[not(. = 'active')]) then
                            <span class="node-s{$association/@originalStatusCode}" title="{$association/@originalStatusText}">{data($association/@originalDisplayName)}</span>
                        else
                        if ($association[@missing]) then
                            <span class="node-s{$association/@originalStatusCode}" style="color: red;" title="{$association/@originalStatusText}">{map:get($i18nmap, 'codeNotFound')}</span>
                        else
                            data($association/@originalDisplayName)
                    }
                    {
                        if ($association/@deprecationCode) then ( 
                            <div style="margin-left: 2.5em;">
                            {   
                                map:get($i18nmap, 'Reason'), ': ',
                                <strong>{data(($association/@deprecationText, $association/@deprecationCode)[1])}</strong>
                                ,
                                for $assoc in $association/association
                                let $deeplinktoassoc    := $assoc/@uri
                                let $assocDisplay       := if ($assoc/@targetComponent) then concat($assoc/@targetComponentId, ' | ', $assoc/@targetComponent, ' |') else data($assoc/@targetComponentId)
                                return (
                                    <div>
                                    {
                                        map:get($i18nmap, 'columnReference'), ': ', 
                                        if ($deeplinktoassoc) then 
                                            <a href="{$deeplinktoassoc}" alt="" target="_new">
                                            {
                                                if ($assoc/@refset) then (attribute title {$assoc/@refset}) else (),
                                                $assocDisplay
                                            }
                                            </a>
                                        else (
                                            $assocDisplay
                                        )
                                    }
                                    </div>
                                )
                            }
                            </div>
                        ) else ()
                    }
                    </td>
                </tr>
                )
            }
            </table>
            
        ) else ()
        }
        {if ($valueSets) then (
            <h2 id="valuesetsHeader">{i18n:getMessage($docMessages,'trValuesetsFoundCountTitle', $language, $diffCountValueSets, $countValueSets)}</h2>,
            for $valueSet in $diffValueSets
            let $valuesetConcepts       := 
                if (empty($codeSystemFilter)) then 
                    $valueSet/conceptList/*[@originalDisplayName | @originalStatusCode | @isSnomedGPSMember | @missing | @missingTranslation]
                else 
                    $valueSet/conceptList/*[@originalDisplayName | @originalStatusCode | @isSnomedGPSMember | @missing | @missingTranslation][@codeSystem = $codeSystemFilter]
            let $completeCodeSystems    := $valueSet/completeCodeSystem | $valueSet/conceptList/include[@codeSystem][not(@code)]
            let $otherValueSetParts     := $valueSet/conceptList[concept | exception | exclude | include[@code | @ref]]
            order by $valueSet/lower-case(@name)
            return
                <div class="content">
                    <h3>
                        <span class="node-s{$valueSet/@statusCode}" title="{$valueSet/@statusCode}">
                        <a href="{concat(adserver:getServerURLArt(), 'decor-valuesets--', $decorproject/project/@prefix, '?id=', $valueSet/@id, '&amp;effectiveDate=', encode-for-uri($valueSet/@effectiveDate))}" alt="" target="_new">
                        {
                            if ($valueSet/@displayName) then $valueSet/@displayName/string() else ($valueSet/@name/string())
                        }
                        </a>
                        {
                            ' - ', 
                            lower-case(map:get($i18nmap, 'effectiveTime')),
                            ' ', 
                            $valueSet/@effectiveDate/string(), 
                            if ($valueSet/@versionLabel) then concat(' - (', $valueSet/@versionLabel/string(),')') else (),
                            ' - ',
                            $valueSet/@id/string()
                        }
                        </span>
                    </h3>
                    {
                        if ($valueSet/desc[@language=$language]) then (
                            <table width="100%">
                                <tr>
                                    <td>
                                        {$valueSet/desc[@language=$language]/node()}
                                    </td>
                                </tr>
                            </table>
                        ) else ()
                    }
                    {if ($valueSet[sourceCodeSystem]) then (
                            <table width="100%" class="zebra-table">
                                <tr>
                                    <th align="right">{map:get($i18nmap, 'xSourceCodeSystem')}</th>
                                    <td>
                                    {
                                        for $sourceCodeSystem at $pos in $valueSet/sourceCodeSystem
                                        return
                                            <div class="zebra-row-{if ($pos mod 2 = 0) then 'even' else 'odd'}">
                                                &quot;
                                                <a href="CodeSystemIndex?id={$sourceCodeSystem/@id/string()}" alt="" target="_new">{$sourceCodeSystem/@id/string()}&quot;&#160; ({$sourceCodeSystem/@identifierName/string()})</a>
                                            </div>
                                    }
                                    </td>
                                </tr>
                            </table>
                        ) else ()}
                    <p/>
                    {
                        if (count($valueSet/completeCodeSystem | $valueSet/conceptList/include[@codeSystem][not(@code)])=1) then (
                                <p><b>{map:get($i18nmap, 'xCompleteCodeSystem')}</b></p>
                        ) else 
                        if (count($valueSet/completeCodeSystem)>1) then (
                            <p><b>{i18n:getMessage($docMessages,'xCompleteCodeSystems',$language,string(count($completeCodeSystems)))}</b></p>
                        ) 
                        else ()
                        ,
                        if ($completeCodeSystems) then (
                            <table class="values zebra-table" cellpadding="5px">
                              <thead>
                                  <tr>
                                      <th>{map:get($i18nmap, 'columnCodeSystemName')}</th>
                                      <th>{map:get($i18nmap, 'columnCodeSystemID')}</th>
                                      <th>{map:get($i18nmap, 'columnCodeSystemVersion')}</th>
                                      <th>{map:get($i18nmap, 'columnFlexibility')}</th>
                                  </tr>
                              </thead>
                              <tbody>
                              {for $completeCodeSystem at $pos in $completeCodeSystems
                               let $filters     := $completeCodeSystem/filter
                               return (
                                  <tr class="zebra-row-{if ($pos mod 2 = 0) then 'even' else 'odd'}">
                                        <td>{data($completeCodeSystem/@codeSystemName)}</td>
                                        <td>{data($completeCodeSystem/@codeSystem)}</td>
                                        <td>{data($completeCodeSystem/@codeSystemVersion)}</td>
                                        {if ($completeCodeSystem[@flexibility castable as xs:dateTime]) then (
                                            <td>{data($completeCodeSystem/@flexibility)}</td>
                                        ) else (
                                            <td>{map:get($i18nmap,'flexibilityDynamic')}</td>
                                        )}
                                  </tr>
                                  ,
                                  if ($filters) then
                                      <tr>
                                          <td>&#160;</td>
                                          <th style="border-top: 1px solid black;">{map:get($i18nmap, 'columnFilter')}: {map:get($i18nmap, 'columnProperty')}</th>
                                          <th style="border-top: 1px solid black;">{map:get($i18nmap, 'columnOp')}</th>
                                          <th style="border-top: 1px solid black;">{map:get($i18nmap, 'columnValue')}</th>
                                      </tr> 
                                  else ()
                                  ,
                                  for $f in $filters
                                  return
                                      <tr>
                                          <td>&#160;</td>
                                          <td>{data($f/@property)}</td>
                                          <td>{data($f/@op)}</td>
                                          <td>{data($f/@value)}</td>
                                      </tr> 
                               )
                               }
                              </tbody>
                            </table>
                        ) else ()
                    }
                    {if ($completeCodeSystems and $otherValueSetParts) then (
                            <p><b>{map:get($i18nmap, 'orOneOfTheFollowing')}</b></p>
                        ) else ()}
                    {if ($otherValueSetParts) then (
                          <table width="100%" class="zebra-table" cellpadding="5px">
                              <thead>
                              <tr>
                                  <th width="5%">{map:get($i18nmap, 'columnLevelSlashType')}</th>
                                  <th width="5%">{map:get($i18nmap, 'columnCode')}</th>
                                  <th width="10%">{map:get($i18nmap, 'columnCodeSystem')}</th>
                                  {if ($valueSet/conceptList/*[@codeSystemVersion]) then (<th>{map:get($i18nmap, 'columnCodeSystemVersion')}</th>) else ()}
                                  <th width="15%">{map:get($i18nmap, 'columnDisplayName')}</th>
                                  <th width="15%">{map:get($i18nmap, 'columnConceptName')}</th>
                                  <th width="15%">{map:get($i18nmap, 'columnOriginalCodeSystemData')}</th>
                                  <!--<th>{map:get($i18nmap, 'columnDescription')}</th>-->
                              </tr>
                              </thead>
                              <tbody>
                                {for $concept at $pos in $valuesetConcepts
                                    let $levelNumber := if ($concept/@level castable as xs:integer) then (xs:integer($concept/@level)) else (0)
                                    let $typeString := if ($concept/@type) then (data($concept/@type)) else ('L')
                                    let $levelType := if (string($levelNumber)!='' or $typeString!='') then (concat($levelNumber,'-',$typeString)) else ('')
                                    let $deeplinktocode := $concept/@uri
                                 return (
                                    if ($concept[name() = 'exception']) then 
                                        if ($concept/preceding-sibling::exception) then () else (
                                            <tr><td colspan="7"><hr/></td></tr>
                                        )
                                    else ()
                                    ,
                                    <tr class="zebra-row-{if ($pos mod 2 = 0) then 'even' else 'odd'}">
                                        <td>{$levelType}</td>
                                        <td>
                                            {for $i in 1 to $levelNumber return '&#160;&#160;&#160;'}
                                        {
                                            if ($deeplinktocode) then 
                                                <a href="{$deeplinktocode}" alt="" target="_new">{data($concept/@code)}</a>
                                            else
                                                data($concept/@code)
                                        }
                                        </td>
                                        <td>{if ($concept/@codeSystemName) then attribute title {$concept/@codeSystemName} else ()}<i>{data($concept/@codeSystem)}</i></td>
                                        {if ($valueSet/conceptList/*[@codeSystemVersion]) then (<td>{$concept/@codeSystemVersion/string()}</td>) else ()}
                                        <td>{$concept/@displayName/string()}</td>
                                        <td>
                                        {
                                            for $conceptName in $concept/conceptName
                                            return <div>({data($conceptName/@language)}) {data($conceptName)}</div>
                                        }
                                        </td>
                                        <td>
                                        {
                                            if ($concept/@isSnomedGPSMember="true")
                                            then (<div>{ $snomedgpsimg, <span>SNOMED GPS</span>}</div>)
                                            else if ($concept/@isSnomedGPSMember="false")
                                            then (<div>{ $notsnomedgpsimg, <span>Not SNOMED GPS</span>}</div>)
                                            else ()
                                        }
                                        {
                                            if ($concept/@missing) then () else
                                            if ($concept/@missingTranslation="true")
                                            then (<div>{ $nol10nimg, <span>{map:get($i18nmap, 'translationMissing')}</span>}</div>)
                                            else if ($concept/@missingTranslation="false")
                                            then (<div>{ $l10nimg, <span>{map:get($i18nmap, 'translationFound')}</span>}</div>)
                                            else ()
                                        }
                                        {
                                            if ($concept[@missing]) then
                                                $errorimg
                                            else
                                            if ($concept[string-length(@originalDisplayName) gt 0]) then 
                                                $errorimg
                                            else 
                                            if ($concept/@originalStatusCode[not(. = 'active')]) then 
                                                $errorimg
                                            else ((:$okimg:))
                                        }
                                        {
                                            if ($concept/@originalStatusCode[not(. = 'active')]) then
                                                <span class="node-s{$concept/@originalStatusCode}" title="{$concept/@originalStatusText}">{data($concept/@originalDisplayName)}</span>
                                            else
                                            if ($concept[@missing]) then
                                                <span class="node-s{$concept/@originalStatusCode}" style="color: red;" title="{$concept/@originalStatusText}">{map:get($i18nmap, 'codeNotFound')}</span>
                                            else
                                                data($concept/@originalDisplayName)
                                        }
                                        {
                                            if ($concept/@deprecationCode) then ( 
                                                <div style="margin-left: 2.5em;">
                                                {   
                                                    map:get($i18nmap, 'Reason'), ': ',
                                                    <strong>{data(($concept/@deprecationText, $concept/@deprecationCode)[1])}</strong>
                                                    ,
                                                    for $assoc in $concept/association
                                                    let $deeplinktoassoc    := $assoc/@uri
                                                    let $assocDisplay       := if ($assoc/@targetComponent) then concat($assoc/@targetComponentId, ' | ', $assoc/@targetComponent, ' |') else data($assoc/@targetComponentId)
                                                    return (
                                                        <div>
                                                        {
                                                            map:get($i18nmap, 'columnReference'), ': ', 
                                                            if ($deeplinktoassoc) then 
                                                                <a href="{$deeplinktoassoc}" alt="" target="_new">
                                                                {
                                                                    if ($assoc/@refset) then (attribute title {$assoc/@refset}) else (),
                                                                    $assocDisplay
                                                                }
                                                                </a>
                                                            else (
                                                                $assocDisplay
                                                            )
                                                        }
                                                        </div>
                                                    )
                                                }
                                                </div>
                                            ) else ()
                                        }
                                        </td>
                                        <!--<td>{$concept/desc[@language=$language or $language=''][1]/string()}</td>-->
                                    </tr>
                                 )
                                 }
                                 
                                 <!--<tr>
                                    <td colspan="6">&#160;</td>
                                 </tr>-->
                                 <!--<tr style="background-color : #FAFAD2;">
                                    <td colspan="6">{i18n:getMessage($docMessages,'CodeSystemLegendaLine',$language)}</td>
                                 </tr>-->
                              </tbody>
                          </table>
                    ) else ()}
                </div>
            
        ) else ()
        }
    </body>
    
    return $r/node()
};

declare %private function local:getStyles() as element() {
    <style type="text/css">
        .zebra-table {{ border-collapse: collapse; border: 1px solid gray; }}
        .zebra-table td {{ padding: 6px; }}
        .zebra-row-even {{ background-color: #eee; }}
        .zebra-row-odd {{ background-color: #fff; }}
        .node-sdraft, .node-spending, .node-sreview, .node-srejected, .node-snew,
        .node-sopen, .node-sclosed, .node-scancelled, .node-sdeprecated, .node-sretired,
        .node-sfinal, .node-sactive, .node-sinactive, .node-supdate, .node-s {{ 
            background-repeat:no-repeat;
            padding:0 0 0 18px;
            vertical-align: middle;
            line-height:18px;
        }}
        .node-sdraft      {{ background-image:url('{$resourcePath2}img/node-sdraft.png') }}
        .node-spending    {{ background-image:url('{$resourcePath2}img/node-spending.png') }}
        .node-sreview     {{ background-image:url('{$resourcePath2}img/node-sreview.png') }}
        .node-srejected   {{ background-image:url('{$resourcePath2}img/node-srejected.png') }}
        .node-snew        {{ background-image:url('{$resourcePath2}img/node-snew.png') }}
        .node-sopen       {{ background-image:url('{$resourcePath2}img/node-sopen.png') }}
        .node-sclosed     {{ background-image:url('{$resourcePath2}img/node-sclosed.png') }}
        .node-scancelled  {{ background-image:url('{$resourcePath2}img/node-scancelled.png') }}
        .node-sdeprecated {{ background-image:url('{$resourcePath2}img/node-sdeprecated.png') }}
        .node-sretired    {{ background-image:url('{$resourcePath2}img/node-sretired.png') }}
        .node-sfinal      {{ background-image:url('{$resourcePath2}img/node-sfinal.png') }}
        .node-sactive     {{ background-image:url('{$resourcePath2}img/node-sactive.png') }}
        .node-sinactive   {{ background-image:url('{$resourcePath2}img/node-sinactive.png') }}
        .node-supdate     {{ background-image:url('{$resourcePath2}img/node-supdate.png') }}
        .node-s           {{ background-image:url('{$resourcePath2}img/node-s.png') }}
        .repobox          {{ display: inline; }}
        .repo             {{
            box-sizing: border-box;
            display: inline;
            font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-size: 11px;
            font-style: normal !important;
            font-weight: normal !important;
            height: 16px;
            line-height: 14px;
            text-align: left;
            width: auto !important;
            padding: 1px 5px 1px 5px;
        }}
        .ref              {{
            background-color: #aaa;
            color: #fff;
            margin-right: 0px;
            text-shadow: #000 1px 0px 2px;
            width: auto !important;
            -webkit-border-radius: 3px 0px 0px 3px;
            -moz-border-radius: 3px 0px 0px 3px;
            border-radius: 3px 0px 0px 3px;
        }}
        .refonly          {{
            background-color: #aaa;
            color: #fff;
            margin-right: 0px;
            text-shadow: #000 1px 0px 2px;
            width: auto !important;
            -webkit-border-radius: 3px 0px 0px 3px;
            -moz-border-radius: 3px 0px 0px 3px;
            border-radius: 3px;
        }}
        .refvalue        {{
            background-color: blueviolet;
            color: #fff;
            margin-left: -4px;
            width: auto !important;
            -webkit-border-radius: 0px 3px 3px 0px;
            -moz-border-radius: 3px 0px 0px 3px;
            border-radius: 0px 3px 3px 0px;
        }}
        .nowrapinline    {{
            display: inline;
            white-space: nowrap !important;
        }}
        h2 {{
            background-color: white;
            font-size: 16px;
        }}
        h3 {{
            background-color: white;
            padding-left: 0px;
             font-size: 14px;
        }}
    </style>
};

declare %private function local:buildReferenceBox($projectPrefix as xs:string, $ident as item()?) as element()? {
    if ($ident[. = $projectPrefix]) then () else (
        <span class="repobox">
            <div class="repo ref sspacing">ref</div>
            <div class="repo refvalue sspacing">{data($ident)}</div>
        </span>
    )
};

(:currently support HTML only to avoid travelling copies of the project:)
let $format           := if (request:exists()) then request:get-parameter('format','html')[1] else ('html')
(:currently do not support verbatim to avoid travelling copies of the project:)
let $mode             := if (request:exists()) then request:get-parameter('mode','valuesets')[1] else ()
let $mode             := if ($mode=('overview','valuesets','terminologyassociations')) then ($mode) else ('valuesets')
let $projectPrefix    := if (request:exists()) then request:get-parameter('prefix',())[1] else ('zib2017bbr-')
let $projectVersion   := if (request:exists()) then request:get-parameter('version',())[1] else ()
let $language         := if (request:exists()) then request:get-parameter('language',())[1] else ()

let $decorproject     := 
    if (empty($projectPrefix)) then () else ( 
        art:getDecorByPrefix($projectPrefix, $projectVersion, $language)
    )

let $now              := format-dateTime(current-dateTime(), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')
let $language         := if ($decorproject) then $decorproject/project/@defaultLanguage else (aduser:getUserLanguage(get:strCurrentUserName()))
let $filenameverbatim := if (count($decorproject)=1) then concat(string-join(tokenize(util:document-name($decorproject),'\.')[position()!=last()],'.'),'-',replace($now,':',''),'.',$format) else ()
let $filenamecompiled := if (count($decorproject)=1) then concat(string-join(tokenize(util:document-name($decorproject),'\.')[position()!=last()],'.'),'-',replace($now,':',''),'-report.',$format) else ()

return 
    (:if (1 = 1) then (
        let $code   := '308402004'
        return
        (\:<x>
        {
            for $c in collection('/db/apps/decor/data')//terminologyAssociation[@code=$code] |
                      collection('/db/apps/decor/data')//concept[@code=$code] |
                      collection('/db/apps/decor/data')//vocabulary[@code=$code] |
                      collection('/db/apps/decor/data')//attribute[@value=$code]
            let $projectPrefix := $c/ancestor::decor/project/@prefix
            group by $projectPrefix
            return
                <project>{$c[1]/ancestor::decor/project/@*, $c[1]/ancestor::decor/project/name, $c}</project>
        }
        
        </x>:\)
        local:getDisplayNameAndStatus($code, $codeSystemSNOMED, 'nl-NL', 'nl-NL')
    )
    else:)
    if (empty($decorproject)) then (
        (:response:set-status-code(404), <error>{i18n:getMessage($docMessages,'errorRetrieveProjectNoResults',$language),' ',if (request:exists()) then request:get-query-string() else()}</error>:)
        if (response:exists()) then response:set-header('Content-Type','text/html; charset=utf-8') else (),
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                <title>TerminologyReport</title>
                <link href="{$resourcePath}/css/default.css" rel="stylesheet" type="text/css"/>
            </head>
            <body>
                <h1>TerminologyReport</h1>
                <div class="content">
                <form name="input" action="TerminologyReport" method="get">
                    <input type="hidden" name="language" value="{$language}"/>
                    <table border="0">
                        <tr>
                            <td style="width: 20%;">{i18n:getMessage($docMessages,'Project',$language)}:</td>
                            <td>
                                <select name="prefix" id="prefixSelector" style="width: 300px;">
                                {
                                    for $p in $get:colDecorData//decor/project
                                    order by lower-case($p/name[1])
                                    return
                                        <option value="{$p/@prefix}">{data($p/name[1]),' (',$p/@defaultLanguage/string(),')'}</option>
                                }
                                </select> (*)
                            </td>
                            <td>&#160;</td>
                        </tr>
                        <tr>
                            <td>{i18n:getMessage($docMessages,'ReportType',$language)}:</td>
                            <td>
                                <select name="mode" id="modeSelector" style="width: 300px;">
                                    <!--option value="verbatim">verbatim</option-->
                                    <option value="overview">overview</option>
                                    <option value="valuesets" selected="selected">valuesets</option>
                                    <option value="terminologyassociations">terminologyassociations</option>
                                </select> (*)
                            </td>
                            <td>
                                <!--div><i>verbatim</i> will give you the project as-is, no modification</div-->
                                <ul><li><i>overview</i> will you both valuesets and terminology associations as described below</li>
                                <li><i>valuesets</i> will give you, as much as possible, differences between display names in the value set and the original display name from the codesystem. Look for @originalDisplayName in the result.</li>
                                <li><i>terminologyassociations</i> will give you, as much as possible, differences between display names in the terminology associations and the display name in the value set (if it links to a dataset conceptList/concept and the conceptList binds to a value set) or codesystem (if it links to a dataset concept). This option gives you best results when the valueSets are correct.</li></ul>
                            </td>
                        </tr>
                        <tr>
                            <td>{i18n:getMessage($docMessages,'Filter',$language)}:</td>
                            <td>
                                <input name="csfilter" id="filterSelector" style="width: 300px;"/>
                            </td>
                            <td>Default is not to filter/check everything. Allows filtering the returned HTML based on one or more code systems. Separate with a space in between. Examples <ul><li>(SNOMED CT): 2.16.840.1.113883.6.96</li><li>(SNOMED CT and LOINC): 2.16.840.1.113883.6.96 2.16.840.1.113883.6.1</li></ul>
                            </td>
                        </tr>
                        <tr>
                            <td>SNOMED Global Patient Set (GPS):</td>
                            <td>
                                <input type="checkbox" name="snomedgps" id="snomedgpsSelector" style="width: 300px;"/>
                            </td>
                            <td>Default is off. Checks/Indicates for each SNOMED CT term if it is part of SNOMED GPS</td>
                        </tr>
                        <tr>
                            <td>Localization found:</td>
                            <td>
                                <input type="checkbox" name="l10n" id="l10nSelector" style="width: 300px;"/>
                            </td>
                            <td>Default is off. Checks/Indicates for each SNOMED CT term if there is a translation in the official language refset at all.<!-- For LOINC and ClaML checks if a translation exists --></td>
                        </tr>
                        <!--tr>
                            <td>Output format:</td>
                            <td>HTML <option value="html" type="hidden">HTML</option>
                            </td>
                            <td></td>
                        </tr-->
                        <tr>
                            <td>Download to disk or show in browser:</td>
                            <td>
                                <select name="download" style="width: 300px;">
                                    <option value="true">Download</option>
                                    <option value="false" selected="selected">Show</option>
                                </select>
                            </td>
                            <td>&#160;</td>
                        </tr>
                        <tr>
                            <td>&#160;</td>
                            <td style="text-align: right;">
                                <input type="submit" value="{i18n:getMessage($docMessages,'Send',$language)}" style="font-weight: bold; color: black;"/>
                            </td>
                            <td>&#160;</td>
                        </tr>
                    </table>
                </form>
                </div>
            </body>
        </html>
    )
    else if (count($decorproject) != 1) then (
        if (response:exists()) then response:set-status-code(404) else (), 
        <error>{i18n:getMessage($docMessages,'errorRetrieveProjectNoSingleResult',$language),' ',if (request:exists()) then request:get-query-string() else()}</error>
    )
    else if ($format = 'xml') then (
        if (response:exists()) then response:set-header('Content-Type','text/xml; charset=utf-8') else (),
        if ($download='true') then (
            if (response:exists()) then response:set-header('Content-Disposition', concat('attachment; filename=',$filenamecompiled)) else (),
            processing-instruction {'xml-stylesheet'} {' type="text/xsl" href="https://assets.art-decor.org/ADAR/rv/DECOR2schematron.xsl"'}
        ) else (),
        processing-instruction {'xml-model'} {' href="https://assets.art-decor.org/ADAR/rv/DECOR.sch" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"'},
        local:doReport($decorproject, $mode, $now, $language)
    )
    else (
        if (response:exists()) then response:set-header('Content-Type','text/html; charset=utf-8') else (),
        if ($download='true') then (
            if (response:exists()) then response:set-header('Content-Disposition', concat('attachment; filename=',$filenameverbatim)) else ()
        ) else (),
        let $i18nmap          :=
            map:merge((
                map:entry('trConceptNotFoundOrMismatch', i18n:getMessage($docMessages,'trConceptNotFoundOrMismatch',$language)),
                map:entry('trNoProblemsFound', i18n:getMessage($docMessages,'trNoProblemsFound',$language)),
                map:entry('trConceptNotFoundOrMismatchName', i18n:getMessage($docMessages,'trConceptNotFoundOrMismatchName',$language)),
                map:entry('trConceptNotInValueSetOrMismatchName', i18n:getMessage($docMessages,'trConceptNotInValueSetOrMismatchName',$language)),
                map:entry('trConceptTypeUnknownOrMismatchName', i18n:getMessage($docMessages,'trConceptTypeUnknownOrMismatchName',$language)),
                map:entry('xSourceCodeSystem', i18n:getMessage($docMessages,'xSourceCodeSystem',$language)),
                map:entry('columnConceptId', i18n:getMessage($docMessages,'columnConceptId',$language)),
                map:entry('columnConceptName', i18n:getMessage($docMessages,'columnConceptName',$language)),
                map:entry('xCompleteCodeSystem', i18n:getMessage($docMessages,'xCompleteCodeSystem',$language)),
                map:entry('columnOriginalData', i18n:getMessage($docMessages,'columnOriginalData',$language)),
                map:entry('columnOriginalCodeSystemData', i18n:getMessage($docMessages,'columnOriginalCodeSystemData',$language)),
                map:entry('columnConceptType', i18n:getMessage($docMessages,'columnConceptType',$language)),
                map:entry('Concept', i18n:getMessage($docMessages,'Concept',$language)),
                map:entry('Value', i18n:getMessage($docMessages,'Value',$language)),
                map:entry('columnCode', i18n:getMessage($docMessages,'columnCode',$language)),
                map:entry('columnDisplayName', i18n:getMessage($docMessages,'columnDisplayName',$language)),
                map:entry('columnCodeSystem', i18n:getMessage($docMessages,'columnCodeSystem',$language)),
                map:entry('columnCodeSystemID', i18n:getMessage($docMessages,'columnCodeSystemID',$language)),
                map:entry('columnCodeSystemName', i18n:getMessage($docMessages,'columnCodeSystemName',$language)),
                map:entry('columnCodeSystemVersion', i18n:getMessage($docMessages,'columnCodeSystemVersion',$language)),
                map:entry('effectiveTime', i18n:getMessage($docMessages,'effectiveTime',$language)),
                map:entry('columnFlexibility', i18n:getMessage($docMessages,'columnFlexibility',$language)),
                map:entry('flexibilityDynamic', i18n:getMessage($docMessages,'flexibilityDynamic',$language)),
                map:entry('orOneOfTheFollowing', i18n:getMessage($docMessages,'orOneOfTheFollowing',$language)),
                map:entry('columnLevelSlashType', i18n:getMessage($docMessages,'columnLevelSlashType',$language)),
                map:entry('columnDescription', i18n:getMessage($docMessages,'columnDescription',$language)),
                map:entry('columnReference', i18n:getMessage($docMessages,'columnReference',$language)),
                map:entry('Reason', i18n:getMessage($docMessages,'Reason',$language)),
                map:entry('codeNotFound', i18n:getMessage($docMessages,'codeNotFound',$language)),
                map:entry('translationMissing', i18n:getMessage($docMessages,'translationMissing', $language, $decorproject/project/@defaultLanguage)),
                map:entry('translationFound', i18n:getMessage($docMessages,'translationFound', $language, $decorproject/project/@defaultLanguage))
            ))
        let $res        := local:doReport($decorproject, $mode, $now, $language)
        let $reshtml    :=
            <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                <title>{$get:colDecorData//decor/project[@prefix=$projectPrefix]/name[@language=$language]/string()} report for {$mode}</title>
                <link href="{$resourcePath}/css/default.css" rel="stylesheet" type="text/css"/>
                {
                    local:getStyles()
                }
            </head>
            <body>
                <h1>{$get:colDecorData//decor/project[@prefix=$projectPrefix]/name[@language=$language]/string()} report for {$mode} ({$now})
                    <a href="#" onclick="javascript:location.href=window.location.pathname" style="float:right; font-size: 12px;">Form</a>
                </h1>
            {
                if (empty($codeSystemFilter)) then 
                    <p><b>No filtering applied</b></p> 
                else if (count($codeSystemFilter)=1) then 
                    <p><b>Only for code system: {for $cs in $codeSystemFilter return concat($cs, ' (',art:getNameForOID($cs,$language,()),')')}</b></p>
                else 
                    <p><b>Only for code systems: {for $cs in $codeSystemFilter return concat($cs, ' (',art:getNameForOID($cs,$language,()),')')}</b></p>
            }
            {
                local:projectTerminology2html($res, $language, $mode, $i18nmap)
            }
            </body>
        </html>
    
        return
            $reshtml
    )