xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
module namespace ds  = "http://art-decor.org/ns/decor-services";
declare namespace json      = "http://www.json.org";

declare function ds:mergeDatasetTreeWithCommunity (
    $fullDatasetTree as element(), 
    $language as xs:string, 
    $community as element()*
    ) as element(dataset) {
    <dataset>
    {
        $fullDatasetTree/@*
        ,
        for $node in $fullDatasetTree/(* | comment())
        return 
            if ($node instance of element(concept)) then
                ds:mergeConceptTreeWithCommunity($node,$language,$community)
            else ($node)
    }
    </dataset>
};

declare %private function ds:mergeConceptTreeWithCommunity (
    $concept as element(concept), 
    $language as xs:string, 
    $community as element()*
    ) as element(concept) {
    <concept>
    {
        $concept/@*,
        $concept/inherit,
        $concept/name,
        $concept/desc
    }
    {
        let $communityInfo := $community//associations/association[object[@ref=$concept/@id][@type='DE']][data]
        
        for $commInfo in $communityInfo
        let $prototypes    :=
            if ($commInfo/ancestor::community/prototype/@ref) then 
                doc(xs:anyURI($commInfo/prototype/@ref))/prototype
            else
                $commInfo/ancestor::community/prototype
        return
            <community name="{$commInfo/ancestor::community/@name}">
            {
                for $association in $commInfo/data
                let $assocType  := $association/@type
                let $typeDef    := $prototypes/data[@type=$assocType]
                return
                    <data>
                    {
                        $assocType,
                        $typeDef/(@* except @type),
                        $association/node()
                    }
                    </data>
            }
            </community>
    }
    {
        for $node in $concept/(* except (inherit|name|desc))
        return 
            if  ($node instance of element(concept)) then
                ds:mergeConceptTreeWithCommunity($node,$language,$community)
            else ($node)
    }
    </concept>
};
