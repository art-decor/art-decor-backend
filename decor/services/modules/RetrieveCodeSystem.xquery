xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "../../../api/modules/library/settings-lib.xqm";
import module namespace utillib     = "http://art-decor.org/ns/api/util" at "../../../api/modules/library/util-lib.xqm";
import module namespace serverapi   = "http://art-decor.org/ns/api/server" at "../../../api/modules/server-api.xqm";
import module namespace cs          = "http://art-decor.org/ns/decor/codesystem" at "../../../art/api/api-decor-codesystem.xqm";
import module namespace i18n        = "http://art-decor.org/ns/decor/i18n" at "../../../art/api/api-decor-i18n.xqm";
import module namespace csapi       = "http://art-decor.org/ns/api/codesystem" at "../../../api/modules/codesystem-api.xqm";
declare namespace       ihesvs      = "urn:ihe:iti:svs:2008" ;

declare option exist:serialize "method=xhtml indent=no media-type=application/xhtml+html";

declare option exist:timeout "90000";

declare variable $artDeepLinkServices   := serverapi:getServerURLServices();
declare variable $useLocalAssets        := if (request:exists()) then request:get-parameter('useLocalAssets','false') else 'false';
declare variable $download              := if (request:exists()) then request:get-parameter('download','false') else '';
(:When called with localAssets=true then we need relative local paths, else 
    we need our server services URL, but with matching scheme (http or https)
    If the scheme is https and servicesUrl is http this leads to a security 
    problem in some browsers
:)
declare variable $resourcePath          := if ($download='true') then ('https://assets.art-decor.org/ADAR/rv/assets') else if ($useLocalAssets = 'true') then ('../assets') else ('../core/assets');

declare variable $format                := if (request:exists() and string-length(request:get-parameter('format','')[1])>0) then request:get-parameter('format','html')[1] else ('html');
declare variable $boolSerialize         := if ($format = ('xml','html')) then false() else true();
declare variable $docMessages           := i18n:getMessagesDoc('decor/services');
declare variable $strArtURL             := serverapi:getServerURLArt();
declare variable $strDecorServicesURL   := $utillib:strDecorServicesURL;
declare variable $strFhirServicesURL    := $utillib:strFhirServicesURL;

let $dodev              := if (request:exists()) then request:get-parameter('dev','false')='true' else (true())
let $seetype            := if (request:exists()) then request:get-parameter('seetype','live-services') else ('live-services')
let $language           := if (request:exists() and string-length(request:get-parameter('language','')[1])>0) then request:get-parameter('language',$setlib:strArtLanguage)[1] else ($setlib:strArtLanguage)
let $languageorig       := if (request:exists()) then request:get-parameter('language','')[1] else ('')

let $id                 := if (request:exists()) then request:get-parameter('id',())[string-length()>0][1] else ()
let $name               := if (request:exists()) then request:get-parameter('name',())[string-length()>0][1] else ()
let $ref                := if (request:exists()) then request:get-parameter('ref',())[string-length()>0][1] else ()
let $useRegexMatching   := if (request:exists()) then request:get-parameter('regex','false')[string-length()>0][1]='true' else ()

let $effectiveDate      := if (request:exists()) then request:get-parameter('effectiveDate',())[string-length()>0][1] else ()
let $projectPrefix      := if (request:exists()) then request:get-parameter('prefix',())[string-length()>0][1] else ()
let $projectVersion     := if (request:exists()) then request:get-parameter('version',())[string-length()>0][1] else ()
let $htmlInline         := if (request:exists() and string-length(request:get-parameter('inline',())[1])>0) then request:get-parameter('inline',())[1] else ()
let $displayHeader      := if ($htmlInline='true') then false() else true()

let $switchDoTreeTable      := if (request:exists()) then request:get-parameter('collapsable','true')='true' else (true())

let $codeSystems := 
    if (not(empty($id))) then
        if (empty($projectPrefix)) then
            cs:getExpandedCodeSystemById($id, $effectiveDate, $boolSerialize)
        else (
            cs:getExpandedCodeSystemById($id, $effectiveDate, $projectPrefix, $projectVersion, $language, $boolSerialize)
        )
    
    else if (not(empty($name))) then
        if (empty($projectPrefix)) then
            cs:getExpandedCodeSystemByName($name, $effectiveDate, $useRegexMatching, $boolSerialize)
        else (
            cs:getExpandedCodeSystemByName($name, $effectiveDate, $useRegexMatching, $projectPrefix, $projectVersion, $language, $boolSerialize)
        )
    
    else if (not(empty($ref)) and not(empty($projectPrefix))) then
        cs:getExpandedCodeSystemByRef($ref, $effectiveDate, $projectPrefix, $projectVersion, $language, $boolSerialize)
    
    else if (not(empty($projectPrefix))) then
        let $codeSystemList := cs:getCodeSystemList($id,$name,$effectiveDate,$projectPrefix, $projectVersion)
        for $codeSystem in $codeSystemList/project[empty(@url)][@ident=$projectPrefix]/codeSystem
        return
            cs:getExpandedCodeSystemById($codeSystem/(@id|@ref), $codeSystem/@effectiveDate, $projectPrefix, $projectVersion, $language, $boolSerialize)
    else ()

let $codeSystemNames    := distinct-values($codeSystems//codeSystem/@name)
let $codeSystemName     := if (count($codeSystemNames)>1) then 'multiple' else $codeSystemNames

return
    if (string-length($id)=0 and string-length($name)=0 and string-length($projectPrefix)=0) then
        if (request:exists()) then 
            (response:set-status-code(404), response:set-header('Content-Type','text/xml'), <error>{i18n:getMessage($docMessages,'errorRetrieveCodesystemNotEnoughParameters',$language)}</error>)
        else ''
    else if (empty($codeSystems)) then (
        if ($htmlInline='true') then (
            response:set-status-code(404),
            response:set-header('Content-Type','text/html'),
            <html><head><title>HTTP 404 Not Found</title></head><body/></html>
        ) else (
            response:set-status-code(404),
            response:set-header('Content-Type','text/xml'), 
            <error>{i18n:getMessage($docMessages,'errorRetrieveCodesystemNoResults',$language),' ',if (request:exists()) then request:get-query-string() else()}</error>
        )
    )
    else if ($format = 'xml') then (
        if (request:exists()) then (
            response:set-header('Content-Type','application/xml; charset=utf-8'),
            response:set-header('Content-Disposition', concat('filename=CS_',$codeSystemName,'_(download_',substring(string(current-dateTime()),1,19),').xml'))
        ) else ()
        ,
        <codeSystems>
        {
            $codeSystems/*
        }
        </codeSystems>
    )
    else if ($format = 'csv') then (
        if (request:exists()) then (
            response:set-header('Content-Type','text/csv; charset=utf-8'),
            response:set-header('Content-Disposition', concat('filename=CS_',($codeSystems//project/codeSystem/@name)[1],' (download ',substring(string(current-dateTime()),1,19),').csv'))
        ) else ()
        ,
        let $csv := csapi:convertCodeSystem2Csv($codeSystems, $language)
        return $csv
    ) 
    else if ($format = 'sql') then (
        if (request:exists()) then (
            response:set-header('Content-Type','text/sql; charset=utf-8'),
            response:set-header('Content-Disposition', concat('filename=CS_',if (count($codeSystems//codeSystem)=1) then $codeSystems//codeSystem/@name[1] else $projectPrefix,' (download ',substring(string(current-dateTime()),1,19),').sql'))
        ) else ()
        ,
        let $SQL := transform:transform($codeSystems, doc('../resources/stylesheets/ToSql4ValueSets.xsl'), ())
        return $SQL
    )
    else if ($format = 'json') then (
        if (request:exists()) then (
            response:set-header('Content-Type','application/json; charset=utf-8'),
            response:set-header('Content-Disposition', concat('filename=CS_',if (count($codeSystems//codeSystem)=1) then $codeSystems//codeSystem/@name[1] else $projectPrefix,' (download ',substring(string(current-dateTime()),1,19),').json'))
        ) else ()
        ,
        let $xml  := 
            element {'codeSystems'} {
                namespace {"json"} {"http://www.json.org"}, 
                utillib:addJsonArrayToElements($codeSystems//codeSystem[@id])
            }
        
        return
            fn:serialize($xml,
                <output:serialization-parameters xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization">
                    <output:method>json</output:method>
                    <output:encoding>UTF-8</output:encoding>
                </output:serialization-parameters>
            )
    )
    else (
        let $xsltParameters     :=
            <parameters>
                <param name="projectDefaultLanguage"    value="{$language}"/>
                <param name="artdecordeeplinkprefix"    value="{$strArtURL}"/>
                <param name="seeThisUrlLocation"        value="{$seetype}"/>
                <param name="displayHeader"             value="{$displayHeader}"/>
                <param name="rand"                      value="128"/>
                <param name="logLevel"                  value="'OFF'"/>
                <param name="switchCreateTreeTableHtml" value="{$switchDoTreeTable}"/>
            </parameters>
        let $xslt := 
            if ($dodev) then 
                xs:anyURI('https://assets.art-decor.org/ADAR-dev/rv/CodeSystem2html.xsl')
            else (
                xs:anyURI(concat('xmldb:exist://', $setlib:strDecorCore, '/CodeSystem2html.xsl'))
                (:xs:anyURI('https://assets.art-decor.org/ADAR/rv/CodeSystem2html.xsl'):)
            )
        let $collapseString     := try { i18n:getMessage($docMessages,'Collapse',$language) } catch * {'Collapse'}
        let $expandString       := try { i18n:getMessage($docMessages,'Expand',$language) } catch * {'Expand'}
        let $logo                       := 
            if ($projectPrefix[string-length()>0]) then (
                concat('ProjectLogo?prefix=',$projectPrefix[string-length()>0][1],'&amp;version=',encode-for-uri($projectVersion[1]))
            ) else (
                let $server-logo    := serverapi:getServerLogo()
                return if (starts-with($server-logo, 'http')) then $server-logo else concat('/art-decor/img/', $server-logo)
            )
        let $decor                      := utillib:getDecorByPrefix($projectPrefix, $projectVersion)[1]
        let $url                        := if ($projectPrefix[string-length()>0]) then ($decor/project/reference/@url) else ()
        let $headerGoTo                 := i18n:getMessage($docMessages,'goTo',$language)
        let $cscount                    := count($codeSystems/descendant-or-self::codeSystem[@id])
        (:this preserves configuration like @deeplinkprefix, and project/restURI for FHIR:)
        let $codeSystemPackage          := 
            <decor>
            {
                $decor/@*, 
                if ($decor/@deeplinkprefix) then () else if (string-length($strArtURL) gt 0) then 
                    attribute deeplinkprefix {$strArtURL}
                else (),
                if ($decor/@deeplinkprefixservices) then () else if (string-length($strDecorServicesURL) gt 0) then 
                    attribute deeplinkprefixservices {$strDecorServicesURL}
                else (),
                if ($decor/@deeplinkprefixservicesfhir) then () else if (string-length($strFhirServicesURL) gt 0) then 
                    attribute deeplinkprefixservicesfhir {$strFhirServicesURL}
                else (),
                $decor/project
            }
                <terminology>{$codeSystems}</terminology>
            </decor>
        return (
        if (request:exists()) then (
            response:set-header('Content-Type','text/html')
        ) else ()
        ,
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{substring($language,1,2)}" lang="{substring($language,1,2)}">
            <head>
                <meta http-equiv="Content-Type" content="text/xhtml; charset=utf-8"/>
                <title>{i18n:getMessage($docMessages, 'CodeSystem', $language),' ',($codeSystems//@name)[1]/string()}</title>
                {
                    if ($switchDoTreeTable) then (
                        <!--{if ($collapsed) then <script type="text/javascript">window.treeTableCollapsed=true;</script> else <script type="text/javascript">window.treeTableCollapsed=false;</script>}--> |
                        <link href="{$resourcePath}/css/retrieve-template.css" rel="stylesheet" type="text/css"></link> |
                        <script type="text/javascript" xmlns="http://www.w3.org/1999/xhtml">
                            window.treeTableCollapsed = true;
                            window.treeTableStringCollapse = '{$collapseString}';
                            window.treeTableStringExpand = '{$expandString}';
                            window.treeTableColumn = 0;
                        </script> |
                        <script src="{$resourcePath}/scripts/jquery-1.11.3.min.js" type="text/javascript"></script> |
                        <script src="{$resourcePath}/scripts/jquery.treetable.js" type="text/javascript"></script> |
                        <script src="{$resourcePath}/scripts/retrieve-transaction.js" type="text/javascript"></script> |
                        <!--<script src="{$resourcePath}/scripts/jquery.cookie.js" type="text/javascript"></script>--> |
                        <!--{if ($draggable) then <script src="{$resourcePath}/scripts/dragtable.js" type="text/javascript"></script> else ()}--> |
                        <link href="{$resourcePath}/decor.css" rel="stylesheet" type="text/css"></link> |
                        <style type="text/css">body {{ background-color: white; }}</style>
                    ) else (
                        <link href="{$resourcePath}/decor.css" rel="stylesheet" type="text/css"></link>
                    )
                }
            </head>
            <body>
            {
                if ($displayHeader) then (
                    <table width="100%">
                        <tr>
                            <td align="left">
                                <h1>
                                {
                                    if ($cscount = 1) then 
                                        i18n:getMessage($docMessages,'CodeSystem',$language) 
                                    else
                                        i18n:getMessage($docMessages,'CodeSystems',$language)
                                }
                                </h1>
                            </td>
                            <td align="right">
                            {if ($logo and $url) then 
                                <a href="{$url}">
                                    <img src="{$logo}" alt="" title="{$url}" height="50px"/>
                                </a>
                             else if ($logo) then
                                <img src="{$logo}" alt="" height="50px"/>
                             else ()
                            }
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                {$headerGoTo}
                                <a href="CodeSystemIndex?prefix={$projectPrefix}{if (empty($projectVersion)) then () else concat('&amp;version=',$projectVersion)}&amp;language={$language}" alt="">index</a> 
                            </td>
                        </tr>
                    </table>
                )
                else()
            }
            {
                transform:transform($codeSystemPackage, $xslt, $xsltParameters)
            }
            </body>
        </html>
        )
    ) (: html :)