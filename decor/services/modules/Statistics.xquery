xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace i18n        = "http://art-decor.org/ns/decor/i18n" at "../../../art/api/api-decor-i18n.xqm";

declare option exist:serialize "method=xhtml media-type=xhtml";
declare variable $useLocalAssets        := if (request:exists()) then request:get-parameter('useLocalAssets','false') else 'false';
declare variable $download              := if (request:exists()) then request:get-parameter('download','false') else 'false';
(:When called with localAssets=true then we need relative local paths, else 
    we need our server services URL, but with matching scheme (http or https)
    If the scheme is https and servicesUrl is http this leads to a security 
    problem in some browsers
:)
declare variable $sortby                := if (request:exists()) then request:get-parameter('sortby','prefix') else 'prefix';
declare variable $resourcePath          := if ($download='true') then ('https://assets.art-decor.org/ADAR/rv/assets') else if ($useLocalAssets = 'true') then ('../assets') else ('../core/assets');

declare variable $docMessages           := i18n:getMessagesDoc('decor/services');


let $language           := if (request:exists()) then request:get-parameter('language',$get:strArtLanguage) else $get:strArtLanguage
let $listsonly          := if (request:exists()) then request:get-parameter('list','') else ''

let $projects           := 
    if ($listsonly='bbrs')
    then collection($get:strDecorData)//decor[@repository='true']
    else collection($get:strDecorData)//decor
    
let $projectlistsorted :=
    for $p in $projects[not(@private='true')]
    let $sort := if ($sortby = 'id') then replace(replace(concat($p/project/@id, '.'), '\.', '.0000000000'), '.0*(\d{9,})', '.$1') else $p/project/@prefix
    order by $sort ascending
    return $p
    
return 
    if (empty($projects)) then
        (response:set-status-code(404), <error>{i18n:getMessage($docMessages,'errorRetrieveProjectNoResults',$language)}</error>)
    else (
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Statistics</title>
        <link href="{$resourcePath}/css/default.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <h1>Statistics</h1>
        <div class="content">
            <table class="zebra-table" cellpadding="3" style="min-width: 40%;">
                <tbody>
                    <tr>
                        <th><h4>{i18n:getMessage($docMessages,'Templates',$language)}</h4></th>
                        <th align="right">#</th>
                    </tr>
                    <tr>
                        <td>Total definitions in repositories</td>
                        <td align="right">{count(distinct-values($projects[@repository='true']//rules/template/@id))}</td>
                    </tr>
                    <tr>
                        <td>Total definitions in projects (excl. repositories)</td>
                        <td align="right">{count(distinct-values($projects[not(@repository='true')]//rules/template/@id))}</td>
                    </tr>
                    <tr>
                        <td>Total definitions in projects/repositories as a refinement</td>
                        <td align="right">{count(distinct-values($projects//rules/template[relationship]/@id))}</td>
                    </tr>
                    <tr>
                       <td>Total references in projects</td>
                       <td align="right">{count(distinct-values($projects[not(@repository='true')]//rules/template/@ref))}</td>
                    </tr>
                    <tr>
                        <th><h4>{i18n:getMessage($docMessages,'ValueSets',$language)}</h4></th>
                        <th align="right">#</th>
                    </tr>
                     <tr>
                        <td>Total definitions in repositories</td>
                        <td align="right">{count(distinct-values($projects[@repository='true']//terminology/valueSet/@id))}</td>
                    </tr>
                    <tr>
                        <td>Total definitions in projects (excl. repositories)</td>
                        <td align="right">{count(distinct-values($projects[not(@repository='true')]//terminology/valueSet/@id))}</td>
                    </tr>
                    <tr>
                       <td>Total references in projects</td>
                       <td align="right">{count(distinct-values($projects[not(@repository='true')]//terminology/valueSet/@ref))}</td>
                    </tr>
                </tbody>
            </table>
            <p/>
            <table class="zebra-table values" cellpadding="3">
                <thead>
                    <tr>
                        <th><h4>{i18n:getMessage($docMessages,'Project',$language)}</h4></th>
                        <th colspan="7" align="right" style="padding-top: 7px;">{concat('N=', count($projects), ' (skipped&#160;private=', count($projects[(@private='true')]), ')')}</th>
                    </tr>
                    <tr>
                        <th>{i18n:getMessage($docMessages,'columnID',$language)}</th>
                        <th>{i18n:getMessage($docMessages,'columnPrefix',$language)}</th>
                        <th>{i18n:getMessage($docMessages,'columnName',$language)}</th>
                        <th style="text-align: right;">#&#160;{i18n:getMessage($docMessages,'DataSets',$language)}</th>
                        <th style="text-align: right;">#&#160;{i18n:getMessage($docMessages,'ValueSets',$language)}</th>
                        <th style="text-align: right;">#&#160;{i18n:getMessage($docMessages,'ValueSet',$language)}&#160;Ref</th>
                        <th style="text-align: right;">#&#160;{i18n:getMessage($docMessages,'Templates',$language)}</th>
                        <th style="text-align: right;">#&#160;{i18n:getMessage($docMessages,'Template',$language)}&#160;Ref</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        for $p at $pos in $projectlistsorted
                        return
                            <tr class="zebra-row-{if ($pos mod 2 = 0) then 'even' else 'odd'}">
                                <td>{concat($p/project/@id, ' ')}</td>
                                <td>
                                {
                                    if ($p/@repository='true') then <img src="{$resourcePath}/bbr.png" width="16" style="padding-right: 4px;"/> else (),
                                    if ($p/project/@experimental='true') then <img src="{$resourcePath}/experimental.png" width="16" style="padding-right: 4px;"/> else (),
                                    <a href="/art-decor/decor-project--{$p/project/@prefix}">{replace($p/project/@prefix,'-','&#8209;')}</a>
                                }
                                </td>
                                <td>
                                {
                                    if ($p[terminology/valueSet][rules/template])
                                    then <b><span style="white-space: nowrap;">{$p/project/name[@language=$p/project/@defaultLanguage]}</span></b>
                                    else <span style="white-space: nowrap;">{$p/project/name[@language=$p/project/@defaultLanguage]}</span>
                                }
                                </td>
                                <td align="right">{count($p/datasets/dataset)}</td>
                                <td align="right">{count($p/terminology/valueSet[@id])}</td>
                                <td align="right">{count($p/terminology/valueSet[@ref])}</td>
                                <td align="right">{count($p/rules/template[@id])}</td>
                                <td align="right">{count($p/rules/template[@ref])}</td>
                            </tr>
                    }
                    <tr>
                        <th align="right" colspan="8">
                        {
                            <span style="padding-left: 3px;">
                            {
                            'Legenda: ',
                            ' BBR ',
                            <img src="{$resourcePath}/bbr.png" width="16"/>,
                            ' | Experimental ',
                            <img src="{$resourcePath}/experimental.png" width="16"/>,
                            ' | Projects in bold contain value sets and templates'
                            }
                            </span>
                        }
                        </th>
                    </tr>
                </tbody>
            </table>
        </div>
        {
        (:
        <snomeds>{
            for $p in $projects//*[@codeSystem='2.16.840.1.113883.6.96']
            return if ($p/@code) then concat('<code code="', string($p/@code), '"/>') else ''
        }</snomeds>
        :)
        
        <div class="extra"> </div>
        }
    </body>
</html>
)