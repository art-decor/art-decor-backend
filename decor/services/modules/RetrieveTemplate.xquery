xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "../../../api/modules/library/settings-lib.xqm";
import module namespace utillib     = "http://art-decor.org/ns/api/util" at "../../../api/modules/library/util-lib.xqm";
import module namespace serverapi   = "http://art-decor.org/ns/api/server" at "../../../api/modules/server-api.xqm";
import module namespace templ       = "http://art-decor.org/ns/decor/template" at "../../../art/api/api-decor-template.xqm";
import module namespace i18n        = "http://art-decor.org/ns/decor/i18n" at "../../../art/api/api-decor-i18n.xqm";

declare option exist:serialize "method=xhtml indent=no media-type=application/xhtml+html";
declare option exist:timeout "90000";


(:declare option exist:serialize "method=xml media-type=text/xml omit-xml-declaration=no indent=no";:)
(:declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=no indent=no 
        doctype-public=-//W3C//DTD&#160;XHTML&#160;1.0&#160;Transitional//EN
        doctype-system=http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd";:)

declare variable $useLocalAssets        := if (request:exists()) then request:get-parameter('useLocalAssets','false') else 'false';
declare variable $download              := if (request:exists()) then request:get-parameter('download','false') else '';
(:When called with localAssets=true then we need relative local paths, else 
    we need our server services URL, but with matching scheme (http or https)
    If the scheme is https and servicesUrl is http this leads to a security 
    problem in some browsers
:)
declare variable $resourcePath          := if ($download='true') then ('https://assets.art-decor.org/ADAR/rv/assets') else if ($useLocalAssets = 'true') then ('../assets') else ('../core/assets');

(: Set boolSerialize for template api as our actions here require serialize desc and constraint nodes :)
declare variable $boolSerialize         := false();
declare variable $docMessages           := i18n:getMessagesDoc('decor/services');
declare variable $strArtURL             := serverapi:getServerURLArt();
declare variable $strDecorServicesURL   := $utillib:strDecorServicesURL;
declare variable $strFhirServicesURL    := $utillib:strFhirServicesURL;

(: main proc :)
let $debug            := true()
let $dodev            := if (request:exists()) then request:get-parameter('dev','false')='true' else (true())
let $seetype          := if (request:exists()) then request:get-parameter('seetype','live-services') else ('live-services')
let $format           := if (request:exists() and string-length(request:get-parameter('format','')[1])>0) then request:get-parameter('format','html')[1] else ('html')
let $language         := if (request:exists() and string-length(request:get-parameter('language','')[1])>0) then request:get-parameter('language',$setlib:strArtLanguage)[1] else ($setlib:strArtLanguage)
(:let $language         := if ($language = ('en-US', 'de-DE', 'nl-NL')) then $language else 'en-US':)

let $id               := if (request:exists() and string-length(request:get-parameter('id',())[1])>0) then request:get-parameter('id',())[1] else ()
let $name             := if (request:exists() and string-length(request:get-parameter('name',())[1])>0) then request:get-parameter('name',())[1] else ()
let $ref              := if (request:exists() and string-length(request:get-parameter('ref',())[1])>0) then request:get-parameter('ref',())[1] else ()

let $effectiveDate    := if (request:exists() and string-length(request:get-parameter('effectiveDate',())[1])>0) then request:get-parameter('effectiveDate',())[1] else ()
let $projectPrefix    := if (request:exists() and string-length(request:get-parameter('prefix',())[1])>0) then request:get-parameter('prefix',())[1] else ()

let $projectVersion   := if (request:exists() and string-length(request:get-parameter('version',())[1])>0) then request:get-parameter('version',())[1] else ()

let $htmlInline       := if (request:exists() and string-length(request:get-parameter('inline',())[1])>0) then request:get-parameter('inline',())[1] else ()
let $displayHeader    := if ($htmlInline='true') then false() else true()

let $switchDoTreeTable      := if (request:exists()) then request:get-parameter('collapsable','true')='true' else (true())

let $decor            := $setlib:colDecorData/decor[project/@prefix=$projectPrefix]
let $expndtmp         := 
    if ($format = ('expandedxml', 'html')) then
        if ($projectPrefix and $ref) then
            templ:getExpandedTemplateByRef($ref, $effectiveDate, $projectPrefix, $projectVersion, $language, $boolSerialize)
        else if ($projectPrefix and $id) then
            templ:getExpandedTemplateById($id, $effectiveDate, $projectPrefix, $projectVersion, $language, $boolSerialize)
        else if ($projectPrefix and $name) then
            templ:getExpandedTemplateByName($name, $effectiveDate, false(), $projectPrefix, $projectVersion, $language, $boolSerialize)
        else ()
    else ()
let $rawtemplates     := 
    if ($format = ('xml', 'xmlnowrapper')) then
        if ($projectPrefix and $ref) then
            templ:getTemplateByRef($ref, $effectiveDate, $projectPrefix, $projectVersion)
        else if ($projectPrefix and $id) then
            templ:getTemplateById($id, $effectiveDate, $projectPrefix, $projectVersion)
        else if ($projectPrefix and $name) then
            templ:getTemplateByName($name, $effectiveDate, $projectPrefix, $projectVersion)
        else ()
    else ()
    
(: retro fill the template's usage and dependencies 
   ... as it is no longer part of an extended template due to performance issues for ART forms :)
let $expndtemplates :=
    <result>
    {
        for $template in $expndtmp/template[template/@id]
        return
        <template>
        {
            $template/@*
        }
        {
            for $t in $template/template[@id]
            return
                <template>
                {
                    $t/@*,
                    $t/*,
                    for $ur in templ:getDependenciesAndUsage($t/@id, $t/@effectiveDate, $projectPrefix)
                    let $if := concat($ur/@id, $ur/@effectiveDate)
                    group by $if
                    order by replace(replace(concat(($ur/@id)[1], '.'), '\.', '.0000000000'), '.0*([0-9]{9,})', '.$1'), $if
                    return $ur
                }
                </template>
        }
        </template>
    }
    </result>
    
let $xsltParameters :=
    <parameters>
        <param name="projectDefaultLanguage"    value="{if ($decor/project/name[@language = $language]) then $language else $decor/project/@defaultLanguage}"/>
        <param name="artdecordeeplinkprefix"    value="{$strArtURL}"/>
        <param name="seeThisUrlLocation"        value="{$seetype}"/>
        <param name="displayHeader"             value="{$displayHeader}"/>
        <param name="rand"                      value="128"/>
        <param name="logLevel"                  value="'OFF'"/>
        <!--<param name="bindingBehaviorValueSetsURL"/>
        <param name="bindingBehaviorValueSets" select="'preserve'"/>
        <param name="theBaseURI2DECOR"/>-->
        <param name="switchCreateTreeTableHtml" value="{$switchDoTreeTable}"/>
    </parameters>

let $xslt := 
    if ($dodev) then 
        xs:anyURI('https://assets.art-decor.org/ADAR-dev/rv/Template2html.xsl')
    else (
        xs:anyURI(concat('xmldb:exist://', $setlib:strDecorCore, '/Template2html.xsl'))
        (:xs:anyURI('https://assets.art-decor.org/ADAR/rv/Template2html.xsl'):)
    )

return
    if ((string-length($id)=0 and string-length($name)=0 and string-length($ref)=0) or string-length($projectPrefix)=0) then
        if (request:exists()) then 
            if ($htmlInline='true') then (
                response:set-status-code(404),
                response:set-header('Content-Type','text/html'),
                <html><head><title>HTTP 404 Not Found</title></head><body/></html>
            ) else (
                response:set-status-code(404),
                response:set-header('Content-Type','text/xml'), 
                <error>{i18n:getMessage($docMessages,'errorRetrieveTemplateNotEnoughParameters',$language),' ',
                if (request:exists()) then request:get-query-string() else()}</error>
            )
        else ''
    else if ($format = 'expandedxml') then (
        if (request:exists()) then 
            response:set-header('Content-Type','text/xml')
        else ''
        ,
        $expndtemplates
    ) else if ($format = 'xml') then (
        if (request:exists()) then
            response:set-header('Content-Type','text/xml')
        else ''
        ,
        $rawtemplates
    ) else if ($format = 'xmlnowrapper') then (
        if (request:exists()) then 
            response:set-header('Content-Type','text/xml')
        else ''
        ,
        ($rawtemplates/template/template)[1]
    ) else (
        if (request:exists()) then
            response:set-header('Content-Type','text/html')
        else (),
        let $collapseString     := try { i18n:getMessage($docMessages,'Collapse',$language) } catch * {'Collapse'}
        let $expandString       := try { i18n:getMessage($docMessages,'Expand',$language) } catch * {'Expand'}
        let $logo                       := 
            if ($projectPrefix[string-length()>0]) then (
                concat('ProjectLogo?prefix=',$projectPrefix[string-length()>0][1],'&amp;version=',encode-for-uri($projectVersion[1]))
            ) else (
                let $server-logo    := serverapi:getServerLogo()
                return if (starts-with($server-logo, 'http')) then $server-logo else concat('/art-decor/img/', $server-logo)
            )
        let $url                := if ($projectPrefix[string-length()>0]) then ($setlib:colDecorData//decor/project[@prefix=$projectPrefix]/reference/@url) else ()
        let $headerGoTo         := i18n:getMessage($docMessages,'goTo',$language)
        let $vscount            := count($expndtemplates/template/template[@id])
        
        return
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{substring($language,1,2)}" lang="{substring($language,1,2)}">
            <head>
                <meta http-equiv="Content-Type" content="text/xhtml; charset=utf-8"/>
                <title>{($expndtemplates//@name)[1]/string()}</title>
                {
                    if ($switchDoTreeTable) then (
                        <!--{if ($collapsed) then <script type="text/javascript">window.treeTableCollapsed=true;</script> else <script type="text/javascript">window.treeTableCollapsed=false;</script>}--> |
                        <link href="{$resourcePath}/css/retrieve-template.css" rel="stylesheet" type="text/css"></link> |
                        <script type="text/javascript" xmlns="http://www.w3.org/1999/xhtml">
                            window.treeTableCollapsed = true;
                            window.treeTableStringCollapse = '{$collapseString}';
                            window.treeTableStringExpand = '{$expandString}';
                            window.treeTableColumn = 0;
                        </script> |
                        <script src="{$resourcePath}/scripts/jquery-1.11.3.min.js" type="text/javascript"></script> |
                        <script src="{$resourcePath}/scripts/jquery.treetable.js" type="text/javascript"></script> |
                        <script src="{$resourcePath}/scripts/retrieve-transaction.js" type="text/javascript"></script> |
                        <!--<script src="{$resourcePath}/scripts/jquery.cookie.js" type="text/javascript"></script>--> |
                        <!--{if ($draggable) then <script src="{$resourcePath}/scripts/dragtable.js" type="text/javascript"></script> else ()}--> |
                        <link href="{$resourcePath}/decor.css" rel="stylesheet" type="text/css"></link> |
                        <style type="text/css">body {{ background-color: white; }}</style>
                    ) else (
                        <link href="{$resourcePath}/decor.css" rel="stylesheet" type="text/css"></link>
                    )
                }
            </head>
            <body>
            {
                if ($displayHeader) then (
                    <table width="100%">
                        <tr>
                            <td align="left">
                                <h1>
                                {
                                    if ($vscount = 1) then 
                                        i18n:getMessage($docMessages,'Template',$language) 
                                    else
                                        i18n:getMessage($docMessages,'Templates',$language)
                                }
                                </h1>
                            </td>
                            <td align="right">
                            {if ($logo and $url) then 
                                <a href="{$url}">
                                    <img src="{$logo}" alt="" title="{$url}" height="50px"/>
                                </a>
                             else if ($logo) then
                                <img src="{$logo}" alt="" height="50px"/>
                             else ()
                            }
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                {$headerGoTo}
                                <a href="TemplateIndex?prefix={$projectPrefix}{if (not(empty($projectVersion))) then concat('&amp;version=',$projectVersion) else ()}&amp;language={$language}" alt="">index</a> 
                            </td>
                        </tr>
                    </table>
                )
                else()
            }
            {
                try {
                    transform:transform($expndtemplates, $xslt, $xsltParameters)
                } catch * {
                    <h3>{i18n:getMessage($docMessages,'transformError',$language, $xslt)}</h3>
                }
            }
            </body>
        </html>
    ) (: html :)