xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../../art/api/api-server-settings.xqm";
import module namespace i18n        = "http://art-decor.org/ns/decor/i18n" at "../../../art/api/api-decor-i18n.xqm";

(:declare option exist:serialize "method=xml media-type=text/xml omit-xml-declaration=no indent=no";:)
declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=no indent=no 
        doctype-public=-//W3C//DTD&#160;XHTML&#160;1.0&#160;Transitional//EN
        doctype-system=http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd";

declare variable $artDeepLinkServices   := adserver:getServerURLServices();
declare variable $useLocalAssets        := if (request:exists()) then request:get-parameter('useLocalAssets','false') else 'false';
declare variable $download              := if (request:exists()) then request:get-parameter('download','false') else '';
(:When called with localAssets=true then we need relative local paths, else 
    we need our server services URL, but with matching scheme (http or https)
    If the scheme is https and servicesUrl is http this leads to a security 
    problem in some browsers
:)
declare variable $resourcePath          := if ($download='true') then ('https://assets.art-decor.org/ADAR/rv/assets') else if ($useLocalAssets = 'true') then ('../assets') else ('../core/assets');

declare variable $docMessages           := i18n:getMessagesDoc('decor/services');
declare variable $strArtURL             := adserver:getServerURLArt();

declare %private function local:isWithinDateRange($input as xs:string,$lowBoundary as xs:string?,$highBoundary as xs:string?) as xs:boolean {
    let $i := number(replace($input,'^\d',''))
    let $l := number(replace($lowBoundary,'^\d',''))
    let $h := number(replace($highBoundary,'^\d',''))
    return
    if (empty($input) or string-length($i) < 8) then (
        true()
    ) else if (empty($lowBoundary) and empty($highBoundary)) then (
        true()
    ) else if (not(empty($lowBoundary)) and empty($highBoundary) and $i >= $l) then (
        true()
    ) else if (not(empty($highBoundary)) and empty($lowBoundary) and $i <= $h) then (
        true()
    ) else if (not(empty($lowBoundary) or empty($highBoundary)) and $i >= $l and $i <= $h) then (
        true()
    ) else (
        false()
    )
};

(: TODO: media-type beter zetten en XML declaration zetten bij XML output :)
(:declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=yes";:)

let $searchRegistry     := if (request:exists()) then request:get-parameter('prefix',request:get-parameter('registry',())[not(. = '')][1])[not(. = '')][1] else ()
let $searchId           := if (request:exists()) then request:get-parameter('id',())[not(. = '')][1] else ()
let $searchName         := if (request:exists()) then tokenize(lower-case(request:get-parameter('name',())[not(. = '')][1]),'\s') else (('icd','10'))
let $searchAuthority    := if (request:exists()) then tokenize(lower-case(request:get-parameter('assigningAuthority',())[not(. = '')][1]),'\s') else ()
let $hasFhirProperty    := if (request:exists()) then request:get-parameter('hasFhirProperty',())[. castable as xs:boolean or . = 'on' or . = 'off'][1] else ()
let $hasFhirBoolean    := 
    if (empty($hasFhirProperty)) then () else 
    if ($hasFhirProperty castable as xs:boolean) then xs:boolean($hasFhirProperty) else 
    if ($hasFhirProperty = 'on') then true() else
    if ($hasFhirProperty = 'off') then false()
    else ()
let $effectiveDate      := if (request:exists()) then request:get-parameter('effectiveDate',())[not(. = '')][1] else ()
let $language           := if (request:exists()) then request:get-parameter('language',$get:strArtLanguage)[1] else ()

let $maxResults         := xs:integer('50')
let $queryName          := 
    <query><bool>{
        for $term in $searchName return <wildcard occur="must">{concat($term,'*')}</wildcard>
    }</bool></query>
let $queryAuthority             := 
    <query><bool>{
        for $term in $searchAuthority return <wildcard occur="must">{concat($term,'*')}</wildcard>
    }</bool></query>
let $allRegistries              :=
    for $registry in collection($get:strOidsData)/myoidregistry
    return <registry name="{$registry/@name}" displayName="{$registry/registry/name/@value}"/>

let $resultOnRegistry           :=
    if (empty($searchRegistry)) then
        collection($get:strOidsData)/myoidregistry//oid
    else (
        collection($get:strOidsData)/myoidregistry[@name=$searchRegistry]//oid
    )

let $resultsOnId                := 
    if (empty($searchId)) then (
        $resultOnRegistry
    )
    else (
        $resultOnRegistry[dotNotation[@value=$searchId]] |
        $resultOnRegistry[dotNotation[starts-with(@value,concat($searchId,'.'))]]
    )

let $resultsOnName              := 
    if (empty($searchName[string-length()>0])) then (
        $resultsOnId
    )
    else (
        $resultsOnId[ft:query(description/@value|symbolicName/@value,$queryName)]
    )

let $resultsOnAuthority         :=
    if (empty($searchAuthority[string-length()>0])) then (
        $resultsOnName
    )
    else (
        $resultsOnName[ft:query(responsibleAuthority/scopingOrganization/name/part/@value,$queryAuthority)]
    )

let $resultsOnEffectiveDate     :=
    if (empty($effectiveDate)) then (
        $resultsOnAuthority
    ) else (
        for $oid in $resultsOnAuthority
        let $isWithinRange := local:isWithinDateRange($effectiveDate,$oid/responsibleAuthority/validTime/low/@value/string(),$oid/responsibleAuthority/validTime/high/@value/string())
        return if ($isWithinRange) then $oid else ()
    )

let $resultsOnFHIRUri           :=
    if (empty($hasFhirBoolean)) then
        $resultsOnEffectiveDate
    else
    if ($hasFhirBoolean) then
        $resultsOnEffectiveDate[additionalProperty/attribute[contains(@value, 'FHIR')]]
    else (
        $resultsOnEffectiveDate[empty(additionalProperty/attribute[contains(@value, 'FHIR')])]
    )

let $results                    := $resultsOnFHIRUri

let $logo                       := adserver:getServerLogo()
let $logo                       := if (starts-with($logo, 'http')) then $logo else concat('/art-decor/img/', $logo)
let $url                        := adserver:getServerURLArt()

let $lblLanguage          := i18n:getMessage($docMessages,'language',$language)
let $lblEffectiveTimeFrom := i18n:getMessage($docMessages,'effectiveTimeFrom',$language)
let $lblEffectiveTimeTo   := i18n:getMessage($docMessages,'effectiveTimeTo',$language)
let $resultCount          := count($results)

return (
    response:set-header('Content-Type','text/html; charset=utf-8'),
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>{i18n:getMessage($docMessages,'titleOIDIndex',$language)}</title>
        <link href="{$resourcePath}/css/default.css" rel="stylesheet" type="text/css"/>
    </head>
    <body onload="javascript:document.getElementById('searchName').focus();">
        <a name="top"></a>
        <table width="100%">
            <tbody>
                <tr>
                    <td align="left">
                        <h1>{i18n:getMessage($docMessages,'titleOIDIndex',$language)}</h1>
                    </td>
                    <td align="right">
                    {if ($logo and $url) then 
                        <a href="{$url}">
                            <img src="{$logo}" alt="" title="{$url}" height="50px"/>
                        </a>
                     else if ($logo) then
                        <img src="{$logo}" alt="" height="50px"/>
                     else
                        <a href="http://www.art-decor.org">
                            <img src="{$resourcePath}/logos/art-decor-logo-small.jpg" alt="www.art-decor.org" title="http://www.art-decor.org" height="50px"/>
                        </a>
                    }
                    </td>
                </tr>
                <tr>
                    <td>
                        <form name="input">
                            <input type="hidden" name="language" value="{$language}"/>
                            <input type="hidden" id="registry" name="prefix" value="{$searchRegistry}"/>
                            <span style="margin-left: 5px;">{i18n:getMessage($docMessages,'columnRegistry',$language)}: </span>
                            <select id="registrySelector" onchange="javascript:document.getElementById('registry').value=this.options[this.selectedIndex].value;">
                            <option value="">{i18n:getMessage($docMessages,'OptionAll',$language)}</option>
                            {
                                for $registry in $allRegistries
                                order by lower-case($registry/@name)
                                return
                                    <option value="{$registry/@name/string()}">
                                    {
                                        if ($registry/@name=$searchRegistry) 
                                        then attribute {'selected'} {'true'} 
                                        else (),
                                        $registry/@displayName/string()
                                    }
                                    </option>
                            }
                            </select>
                            <br/>
                            <span style="margin-left: 5px;">{i18n:getMessage($docMessages,'columnOID',$language)}: </span>
                            <input type="text" id="searchId" name="id" value="{$searchId}"/>
                            <span style="margin-left: 5px;">{i18n:getMessage($docMessages,'columnName',$language)}: </span>
                            <input type="text" id="searchName" name="name" value="{$searchName}"/>
                            <span style="margin-left: 5px;">{i18n:getMessage($docMessages,'columnResponsibleAuthority',$language)}: </span>
                            <input type="text" id="searchAuthority" name="assigningAuthority" value="{$searchAuthority}"/>
                            <span style="margin-left: 5px;">FHIR URI: </span>
                            <input type="checkbox" id="hasFhir" name="hasFhirProperty" value="{$hasFhirProperty}">{if ($hasFhirBoolean) then attribute checked {'true'} else ()}</input>
                            <!--<span style="margin-left: 5px;">{replace(i18n:getMessage($docMessages,'columnResponsibleAuthorityEffectiveTime',$language),':','')}: </span><input type="text" name="effectiveDate" value="{$effectiveDate}"/>-->
                            <input type="submit" value="{i18n:getMessage($docMessages,'Find',$language)}" onclick="location.href=window.location.pathname+'?language={$language}"/>
                            {if ($resultCount>0) then (concat(' (',i18n:getMessage($docMessages,'FoundXResults',$language,$resultCount),')')) else ()}
                        </form>
                    </td>
                    <td align="right">
                        <img src="/art-decor/img/flags/nl.png" onclick="location.href=window.location.pathname+'?language=nl-NL{string-join(for $p in request:get-parameter-names() return if ($p='language') then () else concat('&amp;',$p,'=',request:get-parameter($p,())[string-length()>0]),'')}';" class="linked flag"/>
                        <img src="/art-decor/img/flags/de.png" onclick="location.href=window.location.pathname+'?language=de-DE{string-join(for $p in request:get-parameter-names() return if ($p='language') then () else concat('&amp;',$p,'=',request:get-parameter($p,())[string-length()>0]),'')}';" class="linked flag"/>
                        <img src="/art-decor/img/flags/us.png" onclick="location.href=window.location.pathname+'?language=en-US{string-join(for $p in request:get-parameter-names() return if ($p='language') then () else concat('&amp;',$p,'=',request:get-parameter($p,())[string-length()>0]),'')}';" class="linked flag"/>
                    </td>
                </tr>
            </tbody>
        </table>
    {
        if (empty($results)) then (
            <div class="content">
            {
                i18n:getMessage($docMessages,'errorRetrieveOIDNoResults',$language),' ',if (request:exists()) then request:get-query-string() else()
            }
            </div>
        ) else if ($resultCount>$maxResults) then (
            <div class="content">
            {
                i18n:getMessage($docMessages,'warningRetrieveOIDMaxResultsExceeded',$language, string($maxResults), string($resultCount))
            }
            </div>
        ) else ()
    }
        <div class="content">
            <table class="values" id="oidList">
                <thead>
                    <tr>
                        <th>XML</th>
                        <th>HTML</th>
                        <th>{i18n:getMessage($docMessages,'columnOID',$language)}</th>
                        <!--<th>{i18n:getMessage($docMessages,'columnSymbolicName',$language)}</th>-->
                        <th>{i18n:getMessage($docMessages,'columnStatus',$language)}</th>
                        <th>{i18n:getMessage($docMessages,'columnRealm',$language)}</th>
                        <th>{i18n:getMessage($docMessages,'columnName',$language)}</th>
                        <th>{i18n:getMessage($docMessages,'columnDescription',$language)}</th>
                        <th>{i18n:getMessage($docMessages,'columnResponsibleAuthority',$language)}</th>
                        <!--<th>{i18n:getMessage($docMessages,'columnResponsibleAuthorityEffectiveTime',$language)}</th>-->
                        <th>{i18n:getMessage($docMessages,'columnRegistrationAuthority',$language)}</th>
                    </tr>
                </thead>
                <tbody>
                {
                    for $oid in subsequence($results,1,$maxResults)
                    let $registry := $oid/ancestor::myoidregistry
                    order by $oid/dotNotation/@value
                    return 
                        <tr style="background-color:white" onMouseover="this.style.backgroundColor='lightblue';" onMouseout="this.style.backgroundColor='white';">
                            <td><a href="RetrieveOID?prefix={$registry/@name/string()}&amp;id={$oid/dotNotation/@value/string()}&amp;format=xml">xml</a></td>
                            <td><a href="RetrieveOID?prefix={$registry/@name/string()}&amp;id={$oid/dotNotation/@value/string()}&amp;format=html&amp;language={$language}">html</a></td>
                            <td>{$oid/dotNotation/@value/string()}</td>
                            <!--<td>{$oid/symbolicName/@value/string()}</td>-->
                            <td>{$oid/status/@code/string()}</td>
                            <td style="text-align: center;">{$oid/realm/@code/string()}</td>
                            <td>{
                                if ($oid/description[@language=$language]) then (
                                    if ($oid/description[@language=$language]/thumbnail) then (
                                        <p>{data($oid/description[@language=$language]/thumbnail/@value)}</p>
                                    ) else ()
                                ) else (
                                    if ($oid/description[1]/thumbnail) then (
                                        <p>{concat($oid/description[1]/@language,': ',$oid/description[1]/thumbnail/@value)}</p>
                                    ) else ()
                                )
                            }</td>
                            <td>
                            {
                                if ($oid/description[@language=$language]) then (
                                    <p>{data($oid/description[@language=$language]/@value)}</p>
                                ) else (
                                    <p>{concat($oid/description[1]/@language,': ',$oid/description[1]/@value)}</p>
                                )
                            }
                                <div>
                                {
                                    for $additionalProperty in $oid/additionalProperty[attribute[contains(@value, 'FHIR')]]
                                    return
                                        concat($additionalProperty/attribute/@value,' = ',$additionalProperty/value/@value)
                                }
                                </div>
                            </td>
                            <td>{string-join(($oid/responsibleAuthority[1]/code/@code[not(. = 'PRI')], $oid/responsibleAuthority[1]/scopingOrganization/name/part/@value), ' - ')}</td>
                            <!--<td>
                            {
                                if ($oid/responsibleAuthority/validTime/low/@value) then (
                                    $lblEffectiveTimeFrom,$oid/responsibleAuthority/validTime/low/@value/string()
                                ) else ()
                            }
                            {
                                if ($oid/responsibleAuthority/validTime/high/@value) then (
                                    $lblEffectiveTimeTo,$oid/responsibleAuthority/validTime/high/@value/string()
                                ) else ()
                            }</td>-->
                            <td>{string-join(($oid/registrationAuthority[1]/code/@code[not(. = 'PRI')], $oid/registrationAuthority[1]/scopingOrganization/name/part/@value), ' - ')}</td>
                        </tr>
                }
                </tbody>
            </table>
        </div>
    </body>
</html>
)
(:
        <oid>
        <dotNotation value="1.0.3166.1.2.2"/>
        <category code="LNS"/>
        <status code="completed"/>
        <realm code="UV"/>
        <description>
            <text language="en-US" value="ISO 3166 2 alpha Landcodes" identifierName="ISO 3166 Alpha 2"/>
            <text language="nl-NL" value="ISO 3166 2 alpha Landcodes" identifierName="ISO 3166 Alpha 2"/>
        </description>
        <registrationAuthority>
            <code code="OBO"/>
            <scopingOrganization>
                <name>
                    <part value="Nictiz"/>
                </name>
            </scopingOrganization>
        </registrationAuthority>
        <responsibleAuthority>
            <code code="PRI"/>
            <statusCode code="completed"/>
            <validTime>
                <low value="20111228"/>
            </validTime>
            <scopingOrganization>
                <name>
                    <part value="ISO"/>
                </name>
            </scopingOrganization>
        </responsibleAuthority>
        <additionalProperty>
            <attribute value="purpose"/>
            <value value="codesystem"/>
        </additionalProperty>
        <reference>
            <ref value="http://www.iso.org/iso/english_country_names_and_code_elements"/>
            <type code="LINK"/>
            <lastVisitedDate value="20111228"/>
        </reference>
    </oid>
        :)