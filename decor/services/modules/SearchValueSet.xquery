xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

(:
Searches for valueset by @id, @name and @displayName
If project prefix is present search is limited to that project
:)
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";

let $searchString := util:unescape-uri(request:get-parameter('searchString',''),'UTF-8')
let $searchTerms  := tokenize(lower-case($searchString),'\s')
let $prefix       := request:get-parameter('prefix','')
let $type         := request:get-parameter('type','valueset')

(:
let $searchTerms     := 'gend'
let $prefix          := 'ad2bbr-'
:)

let $status          := ()
let $desc            := xs:boolean('false')
let $ref             := xs:boolean('false')

let $validSearch     := 
    if (matches($searchString,'^[a-z|0-9|\.]') and string-length($searchString)>1 and string-length($searchString)<50) then
        xs:boolean('true')
    else if (matches($searchString,'^[A-Z]') and string-length($searchString)>1 and string-length($searchString)<40) then
        xs:boolean('true')
    else(
        xs:boolean('false')
    )
let $maxResults      := xs:integer('100')
let $query           := 
    <query>
        <bool>
        {
            for $term in $searchTerms
            return
                <wildcard occur="must">{concat('*',$term,'*')}</wildcard>
        }
        </bool>
    </query>

let $options := 
    <options>
        <filter-rewrite>yes</filter-rewrite>
        <leading-wildcard>yes</leading-wildcard>
    </options>

let $decor           :=
    if (string-length($prefix)=0) then
        switch ($type)
        case 'conceptmap' return 
            $get:colDecorData//decor[@repository='true'][not(@private='true')]/terminology/conceptMap
        case 'codesystem' return 
            $get:colDecorData//decor[@repository='true'][not(@private='true')]/terminology/codeSystem
        default return
            $get:colDecorData//decor[@repository='true'][not(@private='true')]/terminology/valueSet
    else (
        switch ($type)
        case 'conceptmap' return 
            $get:colDecorData//decor[project/@prefix=$prefix]/terminology/conceptMap
        case 'codesystem' return 
            $get:colDecorData//decor[project/@prefix=$prefix]/terminology/codeSystem
        default return
            $get:colDecorData//decor[project/@prefix=$prefix]/terminology/valueSet
    )

let $searchResult    := 
    if ($validSearch) then
        for $object in $decor[@id=$searchString or ft:query(@name | @displayName, $query)]
        order by $object/@name
        return
            element {$object/name()} {
                attribute ident {$object/ancestor::decor/project/@prefix},
                $object/(@* except @ident)
            }
    else(
        <result current="0" count="0"/>
    )

let $count           := count($searchResult)
let $current         := if ($count>$maxResults) then $maxResults else ($count)

return
<result current="{$current}" count="{$count}">
{
    for $result in subsequence($searchResult,1,$maxResults)
    return
        $result
}
</result>