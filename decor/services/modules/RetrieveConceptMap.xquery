xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../../art/api/api-server-settings.xqm";
import module namespace i18n        = "http://art-decor.org/ns/decor/i18n" at "../../../art/api/api-decor-i18n.xqm";
import module namespace mpapi       = "http://art-decor.org/ns/api/conceptmap" at "../../../api/modules/conceptmap-api.xqm";
declare namespace       ihesvs      = "urn:ihe:iti:svs:2008" ;

declare option exist:serialize "method=xhtml indent=no media-type=application/xhtml+html";

declare option exist:timeout "90000";

declare variable $artDeepLinkServices   := adserver:getServerURLServices();
declare variable $useLocalAssets        := if (request:exists()) then request:get-parameter('useLocalAssets','false') else 'false';
declare variable $download              := if (request:exists()) then request:get-parameter('download','false') else '';
(:When called with localAssets=true then we need relative local paths, else 
    we need our server services URL, but with matching scheme (http or https)
    If the scheme is https and servicesUrl is http this leads to a security 
    problem in some browsers
:)
declare variable $resourcePath          := if ($download='true') then ('https://assets.art-decor.org/ADAR/rv/assets') else if ($useLocalAssets = 'true') then ('../assets') else ('../core/assets');

declare variable $format                := if (request:exists() and string-length(request:get-parameter('format','')[1])>0) then request:get-parameter('format','html')[1] else ('html');
declare variable $boolSerialize         := if ($format = ('xml','html')) then false() else true();
declare variable $docMessages           := i18n:getMessagesDoc('decor/services');
declare variable $strArtURL             := adserver:getServerURLArt();
declare variable $strDecorServicesURL   := adserver:getServerURLServices();
declare variable $strFhirServicesURL    := adserver:getServerURLFhirServices();

let $dodev              := if (request:exists()) then request:get-parameter('dev','false')='true' else (true())
let $seetype            := if (request:exists()) then request:get-parameter('seetype','live-services') else ('live-services')
let $language           := if (request:exists() and string-length(request:get-parameter('language','')[1])>0) then request:get-parameter('language',$get:strArtLanguage)[1] else ($get:strArtLanguage)
let $languageorig       := if (request:exists()) then request:get-parameter('language','')[1] else ('')

let $id                 := if (request:exists()) then request:get-parameter('id',())[string-length()>0][1] else ()
let $ref                := if (request:exists()) then request:get-parameter('ref',())[string-length()>0][1] else ()

let $effectiveDate      := if (request:exists()) then request:get-parameter('effectiveDate',())[string-length()>0][1] else ()
let $projectPrefix      := if (request:exists()) then request:get-parameter('prefix',())[string-length()>0][1] else ()
let $projectVersion     := if (request:exists()) then request:get-parameter('version',())[string-length()>0][1] else ()
let $htmlInline         := if (request:exists() and string-length(request:get-parameter('inline',())[1])>0) then request:get-parameter('inline',())[1] else ()
let $displayHeader      := if ($htmlInline='true') then false() else true()

let $switchDoTreeTable  := if (request:exists()) then request:get-parameter('collapsable','true')='true' else (true())

let $conceptMaps := 
    if (empty($id) and empty($ref)) then () else (
        if (empty($projectPrefix)) then
            mpapi:getConceptMap('', '', $language, $id || $ref, $effectiveDate, true(), true())
        else (
            mpapi:getConceptMap($projectPrefix, $projectVersion, $language, $id || $ref, $effectiveDate, true(), true())
        )
     )
    
let $conceptMapNames    := distinct-values($conceptMaps/descendant-or-self::conceptMap/@displayName)
let $conceptMapName     := if (count($conceptMapNames) gt 1) then 'multiple' else $conceptMapNames

return
    if (string-length($id)=0 and string-length($ref)=0) then
        if (request:exists()) then 
            (response:set-status-code(404), response:set-header('Content-Type','text/xml'), <error>{i18n:getMessage($docMessages,'errorRetrieveConceptMapNotEnoughParameters',$language)}</error>)
        else ''
    else if (empty($conceptMaps)) then (
        if ($htmlInline='true') then (
            response:set-status-code(404),
            response:set-header('Content-Type','text/html'),
            <html><head><title>HTTP 404 Not Found</title></head><body/></html>
        ) else (
            response:set-status-code(404),
            response:set-header('Content-Type','text/xml'), 
            <error>{i18n:getMessage($docMessages,'errorRetrieveConceptMapNoResults',$language),' ',if (request:exists()) then request:get-query-string() else()}</error>
        )
    )
    else if ($format = 'xml') then (
        if (request:exists()) then (
            response:set-header('Content-Type','application/xml; charset=utf-8'),
            response:set-header('Content-Disposition', concat('filename=CS_',$conceptMapName,'_(download_',substring(string(current-dateTime()),1,19),').xml'))
        ) else ()
        ,
        <conceptMaps>
        {
            $conceptMaps
        }
        </conceptMaps>
    )
    (:else if ($format = 'csv') then (
        if (request:exists()) then (
            response:set-header('Content-Type','text/csv; charset=utf-8'),
            response:set-header('Content-Disposition', concat('filename=MP ',if (count($conceptMaps)=1) then $conceptMaps/@displayName[1] else $projectPrefix,' (download ',substring(string(current-dateTime()),1,19),').csv'))
        ) else ()
        ,
        let $mapentries :=
            for $d in distinct-values($conceptMaps//designation/string-join((@type,@language),' / '))
            return map:entry($d, max($conceptMaps//*/count(designation[$d = string-join((@type,@language),' / ')])))
        let $designationmap     := map:merge($mapentries)
        let $header             := 
            concat('Level;Type;Code;DisplayName;conceptMap;conceptMapName;conceptMapVersion;StatusCode',
                if (count($mapentries)=0) then () else (
                    concat(';',string-join(
                        for $d in map:keys($designationmap)
                        for $i in (1 to map:get($designationmap, $d))
                        return concat('&quot;Designation ',$d,'&quot;')
                    ,';'))
                )
            ,'&#13;&#10;')
        let $columncount        := count(tokenize($header,';'))
        let $i18nconceptMap     := i18n:getMessage($docMessages,'conceptMap',$language)
        return (
        (\:<concept code="xxxxxx" conceptMap="2.16.840.1.113883.2.4.15.4" displayName="Medicatie" level="0" type="A"/> :\)
        (\: Replace double quotes with single quotes in the CSV values, except in the code itself, 
        and place in between double quotes if there a white space character in a string
        Note that in the exceptional event that a code contains a double quote, the CSV renders invalid :\)
        for $conceptMap at $i in $conceptMaps//conceptMap[@id]
        return (
            if ($i > 1) then ('&#13;&#10;') else (),
            concat('&quot;',$i18nconceptMap,' ',$conceptMap/@displayName,' - ',$conceptMap/(@id|@ref),' ',$conceptMap/@effectiveDate,'&quot;',string-join(for $i in (1 to ($columncount - 1)) return ';',''),'&#13;&#10;'),
            $header,
            for $concept in $conceptMap/conceptList/codedConcept
                let $conceptCode := data($concept/@code)
                let $quotedConceptCode := if (matches($conceptCode,'\s+')) then (concat('&quot;',$conceptCode,'&quot;')) else ($conceptCode)
                let $conceptDisplayName := replace(data($concept/designation[@language=$language]/@displayName),'"','&apos;')
                let $quotedConceptDisplayName := if (matches($conceptDisplayName,'\s+')) then (concat('&quot;',$conceptDisplayName,'&quot;')) else ($conceptDisplayName)
                let $conceptconceptMap := replace(data($concept/ancestor::conceptMap/@id),'"','&apos;')
                let $quotedConceptconceptMap := if (matches($conceptconceptMap,'\s+')) then (concat('&quot;',$conceptconceptMap,'&quot;')) else ($conceptconceptMap)
                let $generatedconceptMapName := if ($concept/ancestor::conceptMap/@displayName) then (data($concept/ancestor::conceptMap/@displayName)) else (data($concept/ancestor::conceptMap/@name))
                let $conceptconceptMapName := replace(if ($concept/@conceptMapName) then (data($concept/@conceptMapName)) else if (replace($generatedconceptMapName,'[0-9\.]','')!='') then ($generatedconceptMapName) else (''),'"','&apos;')
                let $quotedConceptconceptMapName := if (matches($conceptconceptMapName,'\s+')) then (concat('&quot;',$conceptconceptMapName,'&quot;')) else ($conceptconceptMapName)
                let $conceptconceptMapVersion := replace(data($concept/ancestor::conceptMap/@effectiveDate),'"','&apos;')
                let $quotedConceptconceptMapVersion := if (matches($conceptconceptMapVersion,'\s+')) then (concat('&quot;',$conceptconceptMapVersion,'&quot;')) else ($conceptconceptMapVersion)
                let $conceptStatusCode := replace(data($concept/@statusCode),'"','&apos;')
                let $quotedConceptStatusCode := if (matches($conceptStatusCode,'\s+')) then (concat('&quot;',$conceptStatusCode,'&quot;')) else ($conceptStatusCode)
                let $conceptEffectiveDate := replace(data($concept/@effectiveDate),'"','&apos;')
                let $quotedConceptEffectiveDate := if (matches($conceptEffectiveDate,'\s+')) then (concat('&quot;',$conceptEffectiveDate,'&quot;')) else ($conceptEffectiveDate)
                let $conceptExpirationDate := replace(data($concept/@expirationDate),'"','&apos;')
                let $quotedConceptExpirationDate := if (matches($conceptExpirationDate,'\s+')) then (concat('&quot;',$conceptExpirationDate,'&quot;')) else ($conceptExpirationDate)
            return (
                concat(data($concept/@level),';',data($concept/@type),';',$quotedConceptCode,';',$quotedConceptDisplayName,';',$quotedConceptconceptMap,';',$quotedConceptconceptMapName,';',$quotedConceptconceptMapVersion,';',$quotedConceptStatusCode,';',$quotedConceptEffectiveDate,';',$quotedConceptExpirationDate,
                    concat(';',string-join(
                        for $d in map:keys($designationmap)
                        for $i in (1 to map:get($designationmap, $d))
                        let $designation    := $concept/designation[$d=string-join((@type,@language),' / ')][1]
                        return 
                            if ($designation[@displayName]) then
                            if (matches($designation/@displayName,'\s+')) then (
                                concat('&quot;',$designation/@displayName,'&quot;')
                            ) else (
                                $designation/@displayName
                            )
                            else ('')
                    ,';'))
                ,'&#13;&#10;')
            )
        ))
    ) 
    else if ($format = 'sql') then (
        if (request:exists()) then (
            response:set-header('Content-Type','text/csv; charset=utf-8'),
            response:set-header('Content-Disposition', concat('filename=MP ',if (count($conceptMaps)=1) then $conceptMaps/@displayName[1] else $projectPrefix,' (download ',substring(string(current-dateTime()),1,19),').sql'))
        ) else ()
        ,
        let $SQL := transform:transform($conceptMaps, doc('../resources/stylesheets/ToSql4ValueSets.xsl'), ())
        return $SQL
    ):)
    else if ($format = 'json') then (
        if (request:exists()) then (
            response:set-header('Content-Type','application/json; charset=utf-8'),
            response:set-header('Content-Disposition', concat('filename=MP ',if (count($conceptMaps)=1) then $conceptMaps/@displayName[1] else $projectPrefix,' (download ',substring(string(current-dateTime()),1,19),').json'))
        ) else ()
        ,
        let $xml  := 
            element {'conceptMaps'} {
                namespace {"json"} {"http://www.json.org"}, 
                art:addJsonArrayToElements($conceptMaps//conceptMap[@id])
            }
        
        return
            fn:serialize($xml,
                <output:serialization-parameters xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization">
                    <output:method>json</output:method>
                    <output:encoding>UTF-8</output:encoding>
                </output:serialization-parameters>
            )
    )
    else (
        let $xsltParameters     :=
            <parameters>
                <param name="projectDefaultLanguage"    value="{$language}"/>
                <param name="artdecordeeplinkprefix"    value="{$strArtURL}"/>
                <param name="seeThisUrlLocation"        value="{$seetype}"/>
                <param name="displayHeader"             value="{$displayHeader}"/>
                <param name="rand"                      value="128"/>
                <param name="logLevel"                  value="'OFF'"/>
                <param name="switchCreateTreeTableHtml" value="{$switchDoTreeTable}"/>
            </parameters>
        let $xslt := 
            if ($dodev) then 
                xs:anyURI('https://assets.art-decor.org/ADAR-dev/rv/ConceptMap2html.xsl')
            else (
                xs:anyURI(concat('xmldb:exist://', $get:strDecorCore, '/ConceptMap2html.xsl'))
                (:xs:anyURI('https://assets.art-decor.org/ADAR/rv/ConceptMap2html.xsl'):)
            )
        let $collapseString     := try { i18n:getMessage($docMessages,'Collapse',$language) } catch * {'Collapse'}
        let $expandString       := try { i18n:getMessage($docMessages,'Expand',$language) } catch * {'Expand'}
        let $logo                       := 
            if ($projectPrefix[string-length()>0]) then (
                concat('ProjectLogo?prefix=',$projectPrefix[string-length()>0][1],'&amp;version=',encode-for-uri($projectVersion[1]))
            ) else (
                let $server-logo    := adserver:getServerLogo()
                return if (starts-with($server-logo, 'http')) then $server-logo else concat('/art-decor/img/', $server-logo)
            )
        let $decor                      := art:getDecorByPrefix($projectPrefix, $projectVersion)[1]
        let $url                        := if ($projectPrefix[string-length()>0]) then ($decor/project/reference/@url) else ()
        let $headerGoTo                 := i18n:getMessage($docMessages,'goTo',$language)
        let $cscount                    := count($conceptMaps/descendant-or-self::conceptMap[@id])
        (:this preserves configuration like @deeplinkprefix, and project/restURI for FHIR:)
        let $conceptMapPackage          := 
            <decor>
            {
                $decor/@*, 
                if ($decor/@deeplinkprefix) then () else if (string-length($strArtURL) gt 0) then 
                    attribute deeplinkprefix {$strArtURL}
                else (),
                if ($decor/@deeplinkprefixservices) then () else if (string-length($strDecorServicesURL) gt 0) then 
                    attribute deeplinkprefixservices {$strDecorServicesURL}
                else (),
                if ($decor/@deeplinkprefixservicesfhir) then () else if (string-length($strFhirServicesURL) gt 0) then 
                    attribute deeplinkprefixservicesfhir {$strFhirServicesURL}
                else (),
                $decor/project
            }
                <terminology>{$conceptMaps}</terminology>
            </decor>
        return (
        if (request:exists()) then (
            response:set-header('Content-Type','text/html')
        ) else ()
        ,
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{substring($language,1,2)}" lang="{substring($language,1,2)}">
            <head>
                <meta http-equiv="Content-Type" content="text/xhtml; charset=utf-8"/>
                <title>{i18n:getMessage($docMessages, 'ConceptMap', $language),' ',($conceptMaps//@name)[1]/string()}</title>
                {
                    if ($switchDoTreeTable) then (
                        <!--{if ($collapsed) then <script type="text/javascript">window.treeTableCollapsed=true;</script> else <script type="text/javascript">window.treeTableCollapsed=false;</script>}--> |
                        <link href="{$resourcePath}/css/retrieve-template.css" rel="stylesheet" type="text/css"></link> |
                        <script type="text/javascript" xmlns="http://www.w3.org/1999/xhtml">
                            window.treeTableCollapsed = true;
                            window.treeTableStringCollapse = '{$collapseString}';
                            window.treeTableStringExpand = '{$expandString}';
                            window.treeTableColumn = 0;
                        </script> |
                        <script src="{$resourcePath}/scripts/jquery-1.11.3.min.js" type="text/javascript"></script> |
                        <script src="{$resourcePath}/scripts/jquery.treetable.js" type="text/javascript"></script> |
                        <script src="{$resourcePath}/scripts/retrieve-transaction.js" type="text/javascript"></script> |
                        <!--<script src="{$resourcePath}/scripts/jquery.cookie.js" type="text/javascript"></script>--> |
                        <!--{if ($draggable) then <script src="{$resourcePath}/scripts/dragtable.js" type="text/javascript"></script> else ()}--> |
                        <link href="{$resourcePath}/decor.css" rel="stylesheet" type="text/css"></link> |
                        <style type="text/css">body {{ background-color: white; }}</style>
                    ) else (
                        <link href="{$resourcePath}/decor.css" rel="stylesheet" type="text/css"></link>
                    )
                }
            </head>
            <body>
            {
                if ($displayHeader) then (
                    <table width="100%">
                        <tr>
                            <td align="left">
                                <h1>
                                {
                                    if ($cscount = 1) then 
                                        i18n:getMessage($docMessages,'ConceptMap',$language) 
                                    else
                                        i18n:getMessage($docMessages,'ConceptMaps',$language)
                                }
                                </h1>
                            </td>
                            <td align="right">
                            {if ($logo and $url) then 
                                <a href="{$url}">
                                    <img src="{$logo}" alt="" title="{$url}" height="50px"/>
                                </a>
                             else if ($logo) then
                                <img src="{$logo}" alt="" height="50px"/>
                             else ()
                            }
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                {$headerGoTo}
                                <a href="ConceptMapIndex?prefix={$projectPrefix}{if (empty($projectVersion)) then () else concat('&amp;version=',$projectVersion)}&amp;language={$language}" alt="">index</a> 
                            </td>
                        </tr>
                    </table>
                )
                else()
            }
            {
                transform:transform($conceptMapPackage, $xslt, $xsltParameters)
            }
            </body>
        </html>
        )
    ) (: html :)