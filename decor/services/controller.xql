(:  This is the main controller for the web application. It is called from the
    XQueryURLRewrite filter configured in web.xml. :)
xquery version "3.1";

(:~ -------------------------------------------------------
    Main controller: handles all requests not matched by
    sub-controllers.
    ------------------------------------------------------- :)

declare namespace c               = "http://exist-db.org/xquery/controller";
import module namespace adserver  = "http://art-decor.org/ns/art-decor-server" at "../../art/api/api-server-settings.xqm";
import module namespace request   = "http://exist-db.org/xquery/request";

declare variable $exist:path external;
declare variable $exist:resource external;
declare variable $exist:controller external;

let $format := if (request:exists()) then request:get-parameter('format','') else 'xml' 
let $format :=  
    if (contains($format, 'json')) then 'json' 
    else if (contains($format, 'xml')) then 'xml' 
    else if (contains($format, 'html')) then 'html' 
    else if (contains(request:get-header('Accept'), 'application/json')) then 'json'
    else if (contains(request:get-header('Accept'), 'application/xml')) then 'xml'
    else if (contains(request:get-header('Accept'), 'text/html')) then 'html'
    else 'xml'

let $query    := request:get-parameter("q", ())
return
    (: redirect webapp root to index.xml :)
    if ($exist:path = ('/', '')) then (
        (:<dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <redirect url="index.xml"/>
        </dispatch>:)
        response:set-header('Content-Type','text/html; charset=utf-8'),
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                <title>ART-DECOR® Service Index</title>
                <link href="../{if ($exist:path = '') then 'decor/' else ()}core/assets/decor.css" rel="stylesheet" type="text/css"/>
            </head>
            <body style="margin: 20px;">
                <h1>ART-DECOR® Service Index</h1>
                <p><img src="../{if ($exist:path = '') then 'decor/' else ()}core/assets/info.png" width="32px" alt=""/>Documentation of all ART-DECOR® services on <a href="https://art-decor.org/mediawiki/index.php?title=URIs">art-decor.org</a></p>
                <table cellpadding="10px" class="tabtab">
                        <tr style="background-color:#f6f3ee;">
                            <td><img src="../{if ($exist:path = '') then 'decor/' else ()}core/assets/images/file.png" width="16px" alt=""/></td>
                            <td>List of Project artefacts</td>
                            <td>
                                <a href="{if ($exist:path = '') then 'services/' else ()}ProjectIndex">Project Index</a>
                            </td>
                        </tr>
                       <tr style="background-color:#f6f3ee;">
                            <td><img src="../{if ($exist:path = '') then 'decor/' else ()}core/assets/images/file.png" width="16px" alt="" style="margin-left: 30px;"/></td>
                            <td>List of Datasets for a project</td>
                            <td>
                                <a href="{if ($exist:path = '') then 'services/' else ()}DatasetIndex">Dataset Index</a>
                            </td>
                        </tr>
                        <tr style="background-color:#f6f3ee;">
                            <td><img src="../{if ($exist:path = '') then 'decor/' else ()}core/assets/images/file.png" width="16px" alt="" style="margin-left: 30px;"/></td>
                            <td>List of Transactions for a project</td>
                            <td>
                                <a href="{if ($exist:path = '') then 'services/' else ()}TransactionIndex">Transaction Index</a>
                            </td>
                        </tr>
                        <tr style="background-color:#f6f3ee;">
                            <td><img src="../{if ($exist:path = '') then 'decor/' else ()}core/assets/images/file.png" width="16px" alt="" style="margin-left: 30px;"/></td>
                            <td>List of Questionnaires for a project</td>
                            <td>
                                <a href="{if ($exist:path = '') then 'services/' else ()}QuestionnaireIndex">Questionnaire Index</a>
                            </td>
                        </tr>
                        <tr style="background-color:#f6f3ee;">
                            <td><img src="../{if ($exist:path = '') then 'decor/' else ()}core/assets/images/file.png" width="16px" alt="" style="margin-left: 30px;"/></td>
                            <td>List of Code Systems for a project</td>
                            <td>
                                <a href="{if ($exist:path = '') then 'services/' else ()}CodeSystemIndex">Code Systems Index</a>
                            </td>
                        </tr>
                        <tr style="background-color:#f6f3ee;">
                            <td><img src="../{if ($exist:path = '') then 'decor/' else ()}core/assets/images/file.png" width="16px" alt="" style="margin-left: 30px;"/></td>
                            <td>List of Value Sets for a project</td>
                            <td>
                                <a href="{if ($exist:path = '') then 'services/' else ()}ValueSetIndex">Value Sets Index</a>
                            </td>
                        </tr>
                        <tr style="background-color:#f6f3ee;">
                            <td><img src="../{if ($exist:path = '') then 'decor/' else ()}core/assets/images/file.png" width="16px" alt="" style="margin-left: 30px;"/></td>
                            <td>List of Templates for a project</td>
                            <td>
                                <a href="{if ($exist:path = '') then 'services/' else ()}TemplateIndex">Templates Index</a>
                            </td>
                        </tr>
                        <tr style="background-color:#f6f3ee;">
                            <td><img src="../{if ($exist:path = '') then 'decor/' else ()}core/assets/images/file.png" width="16px" alt=""/></td>
                            <td>Retrieve a project</td>
                            <td>
                                <a href="{if ($exist:path = '') then 'services/' else ()}RetrieveProject">Retrieve Project</a>
                            </td>
                         </tr>
                         <tr style="background-color:#f6f3ee;">
                            <td><img src="../{if ($exist:path = '') then 'decor/' else ()}core/assets/images/file.png" width="16px" alt=""/></td>
                            <td>List Projects/Artefacts of a Governance Group</td>
                            <td>
                                <a href="{if ($exist:path = '') then 'services/' else ()}GovernanceGroupList">Governance Group List</a>
                            </td>
                        </tr>
                        <tr style="background-color:#f6f3ee;">
                            <td><img src="../{if ($exist:path = '') then 'decor/' else ()}core/assets/images/file.png" width="16px" alt=""/></td>
                            <td>Get Terminology Report for a Project</td>
                            <td>
                                <a href="{if ($exist:path = '') then 'services/' else ()}TerminologyReport">Terminology Report</a>
                            </td>
                        </tr>
                        <tr style="background-color:#f6f3ee;">
                            <td><img src="../{if ($exist:path = '') then 'decor/' else ()}core/assets/images/file.png" width="16px" alt=""/></td>
                            <td>List Object IDentifiers (OIDs)</td>
                            <td>
                                <a href="{if ($exist:path = '') then 'services/' else ()}OIDIndex">OID Index</a>
                            </td>
                        </tr>
                </table>
            </body>
        </html>
    )
    (: ignore Cocoon :)
(:    else if (matches($exist:path, "/cocoon")) then
        <ignore xmlns="http://exist.sourceforge.net/NS/exist">
            <cache-control cache="yes"/>
        </ignore>:)
        
(:    else if ($exist:resource eq 'applications.xml') then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <!-- query results are passed to XSLT servlet via request attribute -->
            <set-attribute name="xquery.attribute" value="model"/>
            <view>
                <forward servlet="XSLTServlet">
                    <set-attribute name="xslt.input" value="model"/>
                    <set-attribute name="xslt.stylesheet" value="apps/applications.xsl"/>
                </forward>
                <forward servlet="XSLTServlet">
                    <set-attribute name="xslt.input" value=""/>
                    <set-attribute name="xslt.stylesheet" value="stylesheets/db2html.xsl"/>
                </forward>
            </view>
        </dispatch>:)

(:
    DECOR REST Services
    RetrieveDataset, RetrieveValueset, RetrieveCode,
    RetrieveOID, DatasetIndex, ValuesetIndex, CodeSystemIndex, OIDIndex, RetrieveTransactionGroupDiagram (formerly known as GetImage)
:)
    else if (lower-case($exist:resource) = lower-case('ProjectIndex')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/ProjectIndex.xquery"/>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('RetrieveDataSet')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/RetrieveTransaction.xquery"/>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('RetrieveConcept')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/RetrieveTransaction.xquery"/>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('RetrieveConceptDiagram')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/RetrieveConceptDiagram.xquery"/>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('RetrieveCodeSystem')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/RetrieveCodeSystem.xquery"/>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('RetrieveValueSet')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/RetrieveValueSet.xquery"/>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('RetrieveConceptMap')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/RetrieveConceptMap.xquery"/>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('RetrieveCode')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/RetrieveCode.xquery"/>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('RetrieveOID')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/RetrieveOID.xquery"/>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('RetrieveXpathsForTransaction')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/RetrieveXpathsForTransaction.xquery"/>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('RetrieveTransaction')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/RetrieveTransaction.xquery"/>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('RetrieveQuestionnaire') or
             lower-case($exist:resource) = lower-case('Questionnaire')) then
        if ($format = ('json', 'xml')) then (
            if (xmldb:collection-available(repo:get-root() || 'fhir/4.0')) then
                response:redirect-to(xs:anyURI(adserver:getServerURLFhirServices() || '4.0/public/Questionnaire/' || request:get-parameter('id', ()) || '--' || replace(request:get-parameter('effectiveDate', ()), '\D', '') || '?_format=' || $format || '&amp;fhirLinkItemStyle=oid'))
            else
            if ($format = 'json') then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/RetrieveQuestionnaireJson.xquery"/>
            </dispatch>
            else
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/RetrieveQuestionnaireXml.xquery"/>
            </dispatch>
        )
        else (
            (: default renderer is lhcforms, add a dispatch hook for other renderers :)
            if (request:get-parameter('render', 'lhcforms') != 'lhcforms') then
                (response:set-status-code(404), <error>Renderers other than LHCForms are not supported</error>)
            else
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/Questionnaire.html"/>
            </dispatch>
        )
    else if (lower-case($exist:resource) = lower-case('RetrieveProject')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/RetrieveProject.xquery"/>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('TerminologyReport')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/TerminologyReport.xquery"/>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('DataSetIndex')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/ProjectIndex.xquery">
                    <add-parameter name="view" value="d"/>
                </forward>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('QuestionnaireIndex')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/ProjectIndex.xquery">
                    <add-parameter name="view" value="q"/>
                </forward>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('TemplateIndex')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/ProjectIndex.xquery">
                    <add-parameter name="view" value="r"/>
                </forward>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('TransactionIndex')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/ProjectIndex.xquery">
                    <add-parameter name="view" value="t"/>
                </forward>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('ValueSetIndex')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/ProjectIndex.xquery">
                    <add-parameter name="view" value="v"/>
                </forward>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('CodeSystemIndex')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/ProjectIndex.xquery">
                    <add-parameter name="view" value="c"/>
                </forward>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('ConceptMapIndex')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/ProjectIndex.xquery">
                    <add-parameter name="view" value="m"/>
                </forward>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('OIDIndex')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/OIDIndex.xquery"/>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('RetrieveTransactionGroupDiagram')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/RetrieveTransactionGroupDiagram.xquery"/>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('GetImage')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/RetrieveTransactionGroupDiagram.xquery"/>
            </dispatch>
   else if (lower-case($exist:resource) = lower-case('RetrieveMessageForInstance')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/retrieve-message-for-instance.xquery"/>
            </dispatch>
   else if (lower-case($exist:resource) = lower-case('Template2XSL')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/Template2XSL.xquery"/>
            </dispatch>
   else if (lower-case($exist:resource) = lower-case('Template2Example')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/Template2Example.xquery"/>
            </dispatch>
   else if (lower-case($exist:resource) = lower-case('GovernanceGroupList')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/GovernanceGroupList.xquery"/>
            </dispatch>
   (:else if (lower-case($exist:resource) = lower-case('RetrieveTemplatePrototypeList')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/retrieve-template-prototype-list.xquery"/>
            </dispatch>:)
   (:else if (lower-case($exist:resource) = lower-case('RetrieveTemplatePrototype')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/retrieve-template-prototype.xquery"/>
            </dispatch>:)
   (:else if (lower-case($exist:resource) = lower-case('RetrieveTemplatePrototypeForEditor')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/retrieve-template-prototype-for-editor.xquery"/>
            </dispatch>:)
    else if (lower-case($exist:resource) = lower-case('RetrieveTemplateDiagram')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/RetrieveTemplateDiagram.xquery"/>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('RetrieveTemplate')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/RetrieveTemplate.xquery"/>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('IssueIndex')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/IssueIndex.xquery"/>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('RetrieveIssue')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="../../art/modules/get-decor-issue.xq"/>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('SearchConceptMap')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/SearchValueSet.xquery">
                    <add-parameter name="type" value="conceptmap"/>
                </forward>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('SearchCodeSystem')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/SearchValueSet.xquery">
                    <add-parameter name="type" value="codesystem"/>
                </forward>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('SearchValueSet')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/SearchValueSet.xquery">
                    <add-parameter name="type" value="valueset"/>
                </forward>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('RenderCDA')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/RenderCDA.xquery"/>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('RedirectTerminology')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/RedirectTerminology.xquery"/>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('RetrieveArtefacts4Wiki')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/RetrieveArtefacts4Wiki.xquery"/>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('CompareArtefacts')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/CompareArtefacts.xquery"/>
            </dispatch>
    (:else if (lower-case($exist:resource) = lower-case('TestService')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/TestService.xquery"/>
            </dispatch>:)
    else if (lower-case($exist:resource) = lower-case('ProjectLogo')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/ProjectLogo.xquery"/>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('Statistics')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/Statistics.xquery"/>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('ValidateCode')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/ValidateCode.xquery"/>
            </dispatch>
    else if (lower-case($exist:resource) = lower-case('RetrieveStructureDefinition')) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="modules/RetrieveStructureDefinition.xquery"/>
            </dispatch>
   else
            <ignore xmlns="http://exist.sourceforge.net/NS/exist">
                <cache-control cache="yes"/>
            </ignore>
