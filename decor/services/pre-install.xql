xquery version "3.0";
(:
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace sm      = "http://exist-db.org/xquery/securitymanager";
import module namespace xmldb   = "http://exist-db.org/xquery/xmldb";
import module namespace repo    = "http://exist-db.org/xquery/repo";
declare namespace cfg           = "http://exist-db.org/collection-config/1.0";
(: The following external variables are set by the repo:deploy function :)

(: file path pointing to the exist installation directory :)
declare variable $home external;
(: path to the directory containing the unpacked .xar package :)
declare variable $dir external;
(: the target collection into which the app is deployed :)
declare variable $target external;
(:install path for art (/db, /db/apps), no trailing slash :)
declare variable $root := repo:get-root();

(: helper function for creating top level database collection and index definitions required for Art webapplication :)
declare %private function local:createTopCollections() {
    let $servicesConf    :=
        <collection xmlns="http://exist-db.org/collection-config/1.0">
            <index xmlns:xs="http://www.w3.org/2001/XMLSchema">
                <fulltext default="none" attributes="false"/>
                <range>
                    <create qname="@packageRoot" type="xs:string"/>
                    <create qname="@key" type="xs:string"/>
                </range>
            </index>
        </collection>
        
    return (
        (:/db/system collections:)
        for $coll in ('decor/services')
        return (
            if (not(xmldb:collection-available(concat('/db/system/config',$root,$coll)))) then (
                xmldb:create-collection(concat('/db/system/config',$root),$coll)
            ) else ()
        )
        ,
        (:== indexes ==:)
        for $coll in ('decor/services')
        let $index-file := concat('/db/system/config',$root,$coll,'/collection.xconf')
        return
        if (doc-available($index-file) and deep-equal(doc($index-file)/cfg:collection,$servicesConf)) then () else (
            xmldb:store(concat('/db/system/config',$root,$coll),'collection.xconf',$servicesConf),
            if (xmldb:collection-available(concat($root,$coll))) then (
                xmldb:reindex(concat($root,$coll))
            ) else ()
        )
    )
};

let $update := local:createTopCollections()

return ()
