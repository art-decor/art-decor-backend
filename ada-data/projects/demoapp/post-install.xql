(: ** NOTE: THIS IS A GENERATED FILE. DO NOT EDIT HERE UNLESS YOU KNOW WHAT YOU ARE DOING ** :)
xquery version "3.0";
import module namespace ada     = "http://art-decor.org/ns/ada-common" at "xmldb:exist:///db/apps/ada/modules/ada-common.xqm";

(:  app name SHALL match final part of install location of the application and as such SHALL match
    the final part of the expath-pkg <target> element
:)
let $projectName := 'demoapp'

(:  create the app data directory. if one already exists, this 
    does nothing except that the full path is returned:)
let $datapath   := xmldb:create-collection($ada:strAdaData, $projectName)
let $x          := xmldb:create-collection($datapath, 'data')

(:  create the app data backup directory. if one already exists, this 
    does nothing except that the full path is returned:)
let $bkuppath   := xmldb:create-collection($ada:strAdaBackup, $projectName)

(:  backup current data in case this new package mangles something during use :)
let $x      := 
    if (xmldb:collection-available($bkuppath)) then 
        xmldb:remove($bkuppath) 
    else ()
let $x      := 
    if (xmldb:collection-available($datapath)) then 
        xmldb:copy-collection($datapath, $ada:strAdaBackup)
    else ()
let $x      := ada:setPermissions($projectName)

return ()