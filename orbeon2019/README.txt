Orbeon web archive for Art-Decor

You will have to do two patches in the Orbeon distribution:

- https://github.com/orbeon/orbeon-forms/commit/f62cca58cdfeca7196297a0a01337f70c21c9277

This fixes accordion close/open behavior

    In art-decor/WEB-INF/lib/orbeon-form-runner.jar go to 
        xbl/orbeon/accordion/accordion.css
    Replace the class html.accordion-menu-js dd.a-m-d with:
        html.accordion-menu-js dd.a-m-d {
            /*See https://github.com/orbeon/orbeon-forms/issues/3621*/
            /*display: none;*/
            top: -10000px;
            left: -10000px;
            position: absolute;
        }
    Save the jar file with the change. You don't seem to be able to influence this from outside the jar
    
- Focus issue in tree-select1
     
     In art-decor/WEB-INF/lib/orbeon-form-runner.jar go to 
        xbl/orbeon/tree-select1/fancytree/jquery.fancytree-all-min.js
     
     Remove "&& a(f.$container).focus()" -- note that due to obfuscation the "a" might differ from version to version
     
     TODO: better guidance on where this text is....
    