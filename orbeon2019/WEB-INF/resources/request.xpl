<?xml version="1.0" encoding="UTF-8"?>
<p:config xmlns:p="http://www.orbeon.com/oxf/pipeline" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:oxf="http://www.orbeon.com/oxf/processors">
   <p:param name="data" type="input"/>
   
   <p:param name="data" type="output"/>
   <p:processor name="oxf:request">
      <p:input name="config">
         <config>
            <include>/request</include>
            <exclude>/request/body</exclude>
         </config>
      </p:input>
      <p:output name="data" ref="data"/>
   </p:processor>
</p:config>
