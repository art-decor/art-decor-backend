(:     This is the main controller for the web application. It is called from the
    XQueryURLRewrite filter configured in web.xml. :)
xquery version "3.0";

(:~ -------------------------------------------------------
    Main controller: handles all requests not matched by
    sub-controllers.
    ------------------------------------------------------- :)
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "../art/modules/art-decor-settings.xqm";
import module namespace adxss   = "http://art-decor.org/ns/art-decor-xis-services" at "api/api-xis-services.xqm";

declare variable $exist:root external;
declare variable $exist:path external;
declare variable $exist:resource external;
declare variable $exist:controller external;

let $getWSDL        := 'wsdl' = request:get-parameter-names()
let $hasSOAPAction  := exists(request:get-header-names()[lower-case(.)='soapaction'])
let $service        := if (starts-with($exist:path, '/')) then substring($exist:path, 2) else $exist:path
return

(:
    XIS Webservices
:)
    if ($exist:resource = 'RetrieveMessage') then (
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
             <forward url="modules/retrieve-message.xquery"/>
        </dispatch>
    )
    else if ($exist:resource = 'UploadMessage') then (
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
             <forward url="modules/save-file-upload.xquery"/>
        </dispatch>
    )
    else if ($exist:resource = 'RenderMessage') then (
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
             <forward url="modules/retrieve-message2html.xquery"/>
        </dispatch>
    )
    else if ($exist:resource = 'ViewTestresult') then (
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="modules/retrieve-test-result.xquery"/>
            <view>
                <forward servlet="XSLTServlet">
                    <set-attribute name="xslt.stylesheet" value="{$exist:root}/resources/stylesheets/analyseMessage2html.xsl"/>
                    <set-attribute name="xslt.output.media-type" value="text/html"/>
                </forward>
            </view>
        </dispatch>
    ) 
    else if ($exist:resource = 'CreateZippedAccount') then (
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
             <forward url="modules/create-zipped-account.xquery"/>
        </dispatch>
    )
    else if (matches($exist:path,'^/hl7/[^/]+/')) then (
        let $path       := replace($exist:path,'/\.\.|~','')
        let $fullpath   := concat(repo:get-root(),$path)
        let $file       := tokenize($path,'/')[last()]
        let $file-ext   := lower-case(tokenize($file,'\.')[last()])
        let $mediatype  := 
            if ($file-ext[. = ('gif','jpg','png','jpeg')]) then concat('image/',$file-ext) else
            (:https://www.w3.org/TR/SVG/mimereg.html:)
            if ($file-ext[. = ('svg')]) then concat('image/',$file-ext,'+xml') else
            if ($file-ext[. = ('html','js','css','csv','txt','rtf')]) then concat('text/',$file-ext) else 
            if ($file-ext[. = ('xml','pdf','zip')]) then concat('application/',$file-ext) else
            if ($file-ext[. = ('xsd','sch')]) then concat('application/','xml') else
            if ($file-ext[. = ('xhtml','xslt', 'wsdl')]) then concat('application/',$file-ext,'+xml') else
            if ($file-ext[. = ('xsl')]) then concat('application/',$file-ext,'t+xml') else
            'application/octet-stream'
        
        return
            if (util:binary-doc-available($fullpath)) then 
                response:stream-binary(util:binary-doc($fullpath), $mediatype, $file)
            else
            if (doc-available($fullpath)) then 
                doc($fullpath)
            else (
                response:set-status-code(404), <html><head><title>HTTP 404 File Not Found</title></head><body>File {$exist:path} not found or readable.</body></html>
            )
    )
    (: This is vulnerable. Any xquery that needs parameter wsdl is now bypassed. However:
        - currently no xquery does that
        - we have no alternative methode to find whether or not $exist:resource is a service
    :)
    else if ($getWSDL=true()) then (
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="/modules/retrieve-WSDL-response.xquery">
                <add-parameter name="service" value="{$service}"/>
            </forward>
        </dispatch>
    )
    (: This is vulnerable. This assumes that any webservice requires a SOAPAction, and that the sender actually implemented it
       WS-i Basic Profile states that SOAPAction is not required. The sender may have made a mistake. However:
       - currently every soap webservice to date uses a SOAPAction header
       - we have no alternative method to detect whether or not we're dealing with soap. When you call request:get-data() here, 
         you cannot call it again in SOAP-response.xquery or anywhere else
    :)
    else if ($hasSOAPAction=true()) then (
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="/modules/retrieve-SOAP-response.xquery">
                <add-parameter name="service" value="{$service}"/>
                <add-parameter name="supported" value="{exists(adxss:getActiveServices()/*:service[@locationResource = $service])}"/>
            </forward>
        </dispatch>
    )
    (: We purposely do NOT have cache enabled. The cache would prevent calling the same URL twice, when the URL is actually a 
        webservice that needs a SOAPAction header. When you forget to set that the first time and add it later, the cache will 
        still give you a 400 for as long as the cache lives.
    :)
    else (
        <ignore xmlns="http://exist.sourceforge.net/NS/exist">
            <cache-control cache="no"/>
        </ignore>
    )