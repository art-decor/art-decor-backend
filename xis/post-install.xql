xquery version "3.0";
(:
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../art/api/api-server-settings.xqm";
import module namespace artfix      = "http://art-decor.org/ns/art-decor-permissions" at "../art/api/api-permissions.xqm";
import module namespace adpfix      = "http://art-decor.org/ns/xis-permissions" at "api/api-permissions.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at "../art/modules/art-decor.xqm";
declare namespace repo              = "http://exist-db.org/xquery/repo";

(: The following external variables are set by the repo:deploy function :)

(: file path pointing to the exist installation directory :)
declare variable $home external;
(: path to the directory containing the unpacked .xar package :)
declare variable $dir external;
(: the target collection into which the app is deployed :)
declare variable $target external;
declare variable $root := repo:get-root();

(: helper function for creating database group 'xis' required for Art XIS webapplication :)
declare %private function local:createXisGroup() {
    if (not(sm:group-exists('xis'))) then
        sm:create-group('xis','admin','Group for general access to XIS data')
    else()
};

(: helper function for creating database user 'webservice' required for Art XIS webapplication :)
declare %private function local:createXisUser() {
    let $user   := 'xis-webservice'
    let $pass   := 'webservice-xs2messages'
    return (
        if (not(sm:user-exists($user))) then
            sm:create-account($user,$pass,'xis','XIS Webservice','Used for storing received messages')
        else ()
        ,
        if (not(adserver:getSavedUsernames()=$user)) then
            adserver:setPassword($user,$pass)
        else ()
    )
};

let $update := local:createXisGroup()
let $update := local:createXisUser()

let $update := adpfix:createTestAccounts(()) (: check if message collection exists, if not then create and set permissions :)
let $update := adpfix:setXisPermissions()
let $update := art:saveFormResources('xis',art:mergeLocalLanguageUpdates('xis'))

(: upgrade or downgrade the XForms depending on Orbeon version :)
let $updatexforms           := if (adserver:getServerOrbeonVersion() = '3.9') then artfix:updateXForms('downgrade') else artfix:updateXForms('upgrade')

return ()
