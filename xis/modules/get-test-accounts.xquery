xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adxfiles    = "http://art-decor.org/ns/art-decor/xis/files" at "../api/api-xis-files.xqm";
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";

declare namespace sm  = "http://exist-db.org/xquery/securitymanager";
declare namespace xis = "http://art-decor.org/ns/xis";
declare namespace xs  = "http://www.w3.org/2001/XMLSchema";

declare %private function local:getLastModified($account as xs:string) as xs:dateTime? {
    max(collection(concat($get:strXisAccounts, '/', $account,'/',$adxfiles:_filedir))/file/xs:dateTime(@created))
};

let $user           := (get:strCurrentUserName(),'guest')[not(. = '')][1]
let $testAccounts   := doc($get:strTestAccounts)/xis:testAccounts
let $dummy          := 
    if (sm:has-access(xs:anyURI($get:strTestAccounts),'rwx')) then
        for $xisCfg in $testAccounts//xis:xis
        let $dummy  := 
            if (not($xisCfg/xis:xmlValidation)) then 
                update insert element {QName('http://art-decor.org/ns/xis','xmlValidation')} {true()} following $xisCfg/*[last()] 
            else ()
        let $dummy  := 
            if (not($xisCfg/xis:getMessageXml)) then
                update insert element {QName('http://art-decor.org/ns/xis','getMessageXml')} {true()} following $xisCfg/*[last()]
            else ()
        let $dummy  :=
            for $node in $xisCfg/../xis:application[empty(@organizationRegisterId)]
            return
                update insert attribute organizationRegisterId {''} into $node
        (:mark the first path in an account as default=true and the rest default=false:)
        let $dummy  :=
            for $node in $xisCfg/xis:xmlResourcesPath[empty(@default)]
            return
                update insert attribute default {not($node/preceding-sibling::xis:xmlResourcesPath)} into $node
        return ()
    else ()

let $testAccounts   := 
    <testAccounts xmlns="http://art-decor.org/ns/xis">
    {
        if (sm:is-dba($user)) then 
            for $testAccount in $testAccounts/xis:testAccount
            return
                <testAccount>
                {
                    $testAccount/(@* except (@lastmodified | @filecount)),
                    try {attribute lastmodified {local:getLastModified($testAccount/@name)}} catch * {()},
                    attribute filecount {try {count(xmldb:get-child-resources(concat($get:strXisAccounts, '/', $testAccount/@name, '/files')))} catch * {0}},
                    $testAccount/node()
                }
                </testAccount>
        else ()
    }
    </testAccounts>

return
    $testAccounts