xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace val         = "http://art-decor.org/ns/art-decor/xis/validation" at "../api/api-xis-validation.xqm";
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace artx        = "http://art-decor.org/ns/art/xpath" at  "../../art/modules/art-decor-xpath.xqm";
import module namespace adxfiles    = "http://art-decor.org/ns/art-decor/xis/files" at "../api/api-xis-files.xqm";

declare namespace transform     = "http://exist-db.org/xquery/transform";
declare namespace request       = "http://exist-db.org/xquery/request";
declare namespace response      = "http://exist-db.org/xquery/response";
declare namespace hl7           = "urn:hl7-org:v3";
declare namespace util          = "http://exist-db.org/xquery/util";
declare namespace xis           = "http://art-decor.org/ns/xis";
declare namespace svrl          = "http://purl.oclc.org/dsdl/svrl";
declare namespace xmldb         = "http://exist-db.org/xquery/xmldb";
declare option exist:serialize "indent=no";
declare option exist:serialize "omit-xml-declaration=no";

(: Log debug messages? :)
let $debug              := true()

let $testsuiteId        := if (request:exists()) then request:get-parameter('testsuiteId','') else '3'
let $validationId       := if (request:exists()) then request:get-parameter('validationId','') else '3'
let $account            := if (request:exists()) then request:get-parameter('account','') else 'rivmsp-graauw'

(: Get validation from id:)
let $testUrl            := concat($get:strXisAccounts, '/',$account)
let $testsuiteTest      := collection($testUrl)//xis:test[@testsuiteId = $testsuiteId]
let $validation         := $testsuiteTest//xis:validation[@id = $validationId]
(: Make history file if not exists, store path to history.xml :)
let $history            := 
    if (doc-available(concat($testUrl, '/history.xml'))) then 
        concat($testUrl, '/history.xml')
    else
        xmldb:store($testUrl, 'history.xml', <xis:history xmlns:xis="http://art-decor.org/ns/xis"/>) 

(: Get XMLresourcesPath from testsuite:)
let $xmlResourcesPath   := val:getTestSuiteXmlResourcesById($testsuiteTest/@testsuiteId)
(: Get message from path:)
let $message            := try {adxfiles:getMessage($account, $validation/@messageFile, $validation/@messageXpath[string-length()>0])} catch * {()}
let $message            := 
    if ($message) then $message else 
    if ($validation[messageFile/*]) then (
        (: the account files section does not contain the message in the testseries anymore. Restore the message 
            from the testseries as a new file under the original name. Makes the message root of the file, so update 
            the xpath to be sure this matches :)
        let $m      := $validation/messageFile/*
 let $resave := adxfiles:saveFile($account, $validation/@messageFile, $m)
        let $xp     := update replace $validation/@messageXpath with '*[1]'
        
        return $m
     ) else ()
(: Get report from path:)
let $report             := 
    if ($validation[string-length(@messageFile) = 0]) then () else (
        (: Validate if not validated before, re-validate if validated with different validation resources. This will fail if the only copy is in the testseries, hence the try/catch :)
        let $revalidated        := 
            try {val:revalidate($account, $validation/@messageFile, $validation/@messageXpath[string-length() gt 0], $xmlResourcesPath, false())} catch * {()}
        
        return
            adxfiles:getReports($account, $validation/@messageFile, $validation/@messageXpath[string-length() gt 0], $xmlResourcesPath)
    )
let $schemaValid        := 
    if ($report) then 
        if ($report//error[@type = 'schema']) then false() else true()
    else false()
let $schematronValid    := 
    if ($report) then 
        if ($report//error[not(@type = 'schema')] | $report//warning[not(@type = 'schema')]) then false() else true()
    else false()

(: ==== START Schematron validation of messageInstance. Requires SVRL version of SCH (!) ==== :)
let $messageSchematronFile  := concat($xmlResourcesPath,'/test_xslt/',$validation/parent::xis:test/@ref,'.xsl')

(: hacky, but necessary. The transform routine for schematron will kill the carefully reproduced @xsi:type value prefix declarations. When we save to disk first... it doesn't. Go figure :)
let $tempFile               := adxfiles:saveTempMessage($account, $message)
let $message                := doc($tempFile)/*

let $schematronIssues       := if ($message) then val:validateSchematron($message, $messageSchematronFile) else () 

let $issueReport            := <validationReport>{$schematronIssues}</validationReport>
let $result                 := val:makeIssueReport($issueReport)

let $statusCode             :=
    if (not($schemaValid))                      then 'invalid'
    else if (not($schematronValid))             then 'fail'
    else if ($result//error | $result//warning) then 'fail'
    else 'pass'

let $testUpdate             :=
    let $delete             := update delete $validation/validationReport
    let $insert             := update insert $result into $validation
    
    let $update             := 
        if ($validation[messageFile/*]) 
        then update replace $validation/messageFile with <messageFile>{$message}</messageFile>
        else (update delete $validation/messageFile, update insert <messageFile>{$message}</messageFile> into $validation)
    
    let $updateStatus       :=  update value $validation/@statusCode with $statusCode
    (:let $updateStatus       := update value $validation/@approvalStatus with ''
    let $updateStatus       := update value $validation/@approvedBy with ''
    let $updateStatus       := update value $validation/@approvalDate with '':)
    let $updateDateTime     := update value $validation/@dateTime with current-dateTime()
    let $suiteStatus        :=
        (: if there's a validation that didn't get a status yet we're incomplete :)
        if ($testsuiteTest//xis:validation[string-length(@statusCode)=0][string-length(@approvalStatus)=0]) then
            'incomplete'
        (: if there's a requirement that didn't get an approval status 'pass' yet we have a fail :)
        else if ($testsuiteTest//xis:requirement[not(@approvalStatus='pass')]) then
            'fail'
        (: if there's a validation that didn't get an approval status 'pass' yet we have a fail :)
        else if ($testsuiteTest//xis:validation[@approvalStatus[not(.='pass')]]) then
            'fail'
        (: if there's a validation that has a status other than 'pass' and has no overriding approval status yet we have a fail :)
        else if ($testsuiteTest//xis:validation[@statusCode[not(.='pass')]][string-length(@approvalStatus)=0]) then
            'fail'
        (: all looks well :)
        else (
            'pass'
        )
    let $suiteStatusUpdate  := 
        update value $testsuiteTest/@statusCode with $suiteStatus
    let $dummy              := 
        update insert <xis:result testsuiteId="{$testsuiteTest/@testsuiteId}" testsuiteStatusCode="{$testsuiteTest/@statusCode}">{$validation/@*}</xis:result> into doc($history)/xis:history
    return
        <dummy/>

(: Cleanup of hacky stuff when we retrieved the message :)
let $deleteTempFile         := adxfiles:deleteFile($account, $tempFile)

return
<result testsuiteId="{$testsuiteId}" validationId="{$validationId}">{$statusCode}</result>
(:$result:)
