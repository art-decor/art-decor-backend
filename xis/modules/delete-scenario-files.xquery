xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace adxfiles    = "http://art-decor.org/ns/art-decor/xis/files" at "../api/api-xis-files.xqm";

declare namespace xis = "http://art-decor.org/ns/xis";

let $account        := request:get-parameter('account','')
let $scenarioId     := request:get-parameter('scenarioId','')
(:let $account      := 'art-decor'
let $scenarioId     := '1':)
(:let $file         := 'XK_HAPIS1_REPC_IN990003NL_555555112_bijlage XI.xml':)
let $resultsdir     := concat($get:strXisAccounts, '/',$account,'/', $adxfiles:_filedir)
let $scenario       := collection($get:strXisResources)//xis:scenario[@id=$scenarioId]

for $step in $scenario/xis:step
return
    if ($step/xis:action/@type='store' and doc(concat($resultsdir, '/', $step/xis:action/@filename))) then
        adxfiles:deleteAccountData($account, $adxfiles:_delete-all, $step/xis:action/@filename, (), ())
    else ()

