xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "html";
declare option output:media-type "text/html";

let $nl := "&#10;"
let $tab := "&#9;"

let $resourcesPath := if (request:exists()) then request:get-parameter('resourcesPath','') else '/db/apps/hl7/peri20-test-20141119T171544'

for $testset in collection(concat($resourcesPath, '/test_xslt'))/testset
    let $html := 
    <html>
        <head>
            <title>{$testset/@name/string()}</title>
            <meta charset="UTF-8"></meta>
        </head>
        <body>
            <h1>{$testset/@name/string()}</h1>
            {
                for $test in $testset/test
                return
                <div>
                    <h2>{$test/name}</h2>
                    <p>{$test/desc}</p>
                    <h3>Multiplicities</h3>
                    <ol>
                    {
                        for $testConcept in $test/suppliedConcepts/concept[@multiplicity]
                        return <li>{if ($testConcept/string-length()>0) then $testConcept/string() else 'No description.'}</li>
                    }
                    </ol>
                    <h3>Assertions</h3>
                    <ol>
                    {
                        for $testConcept in $test/suppliedConcepts/concept[@assert]
                        return <li>{if ($testConcept/string-length()>0) then $testConcept/string() else 'No description.'}</li>
                    }
                    </ol>
                    <h3>Schematron</h3>
                    <ol>
                    {
                        for $testConcept in $test/suppliedConcepts/assert
                        return <li>{if ($testConcept/string-length()>0) then $testConcept/string() else 'No description.'}</li>
                    }
                    </ol>
                </div>
            }
        </body>
    </html>
    let $store := xmldb:store(util:collection-name($testset), concat(xmldb:encode-uri($testset/@name), '.html'), $html)
    return $html