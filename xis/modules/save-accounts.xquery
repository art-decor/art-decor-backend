xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace adpfix      = "http://art-decor.org/ns/xis-permissions" at "../api/api-permissions.xqm";

declare namespace sm  = "http://exist-db.org/xquery/securitymanager";
declare namespace xis = "http://art-decor.org/ns/xis";

let $editedAccounts         := if (request:exists()) then request:get-data()/xis:testAccounts else ()
let $currentUser            := get:strCurrentUserName()

let $accounts               := doc($get:strTestAccounts)/xis:testAccounts
let $currentAccount         := $accounts/xis:testAccount/xis:members/xis:user[@id = $currentUser][@lastSelected = 'true']
let $currentAccountDetails  := <account>{$currentAccount/ancestor::xis:testAccount[1]/@id}</account>

let $updateAccounts     := 
    if ($editedAccounts) then (
        for $editedAccount in $editedAccounts/xis:testAccount[*:new | *:edit]
        let $storedAccount  := $accounts/xis:testAccount[@id = $editedAccount/@id]
        return
            if ($storedAccount) then
                if (count($storedAccount) = 1) then 
                    update replace $storedAccount with $editedAccount
                else (
                    (: exceptional. if the test-accounts file holds two or more accounts by the same id, we delete them all and add the new one from the UI
                        If UI has two or more accounts by the same id, the last one wins. Tough luck for the others...
                    :)
                    update delete $storedAccount,
                    update insert $editedAccount into $accounts
                )
            else (
                update insert $editedAccount into $accounts
            )
    )
    else ()
let $updateConfig       := adpfix:createTestAccounts($editedAccounts/xis:testAccount[*:new | *:edit])
let $deleteMarker       :=
    for $editedAccount in $editedAccounts/xis:testAccount[*:new | *:edit]
    let $storedAccount  := $accounts/xis:testAccount[@id = $editedAccount/@id]
    return (
        update delete $storedAccount/@filecount,
        update delete $storedAccount/@lastmodified,
        update delete $storedAccount/*:new,
        update delete $storedAccount/*:edit
    )

(: reset the @lastSelected marker :)
let $userAllAccounts    := $accounts//xis:testAccount/xis:members/xis:user[@id = $currentUser]
let $userThisAccount    := $accounts//xis:testAccount[@id = $currentAccountDetails/@id]/xis:members/xis:user[@id = $currentUser]
let $resetLastUsed      :=
    if (empty($userThisAccount)) then () else (
        update delete $userAllAccounts/@lastSelected,
        update insert attribute lastSelected {'true'} into $userThisAccount
    )

return
    <result/>
