xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace adxfiles    = "http://art-decor.org/ns/art-decor/xis/files" at "../api/api-xis-files.xqm";
import module namespace adxaccounts = "http://art-decor.org/ns/art-decor/xis/accounts" at "../api/api-xis-accounts.xqm";

declare namespace soap              = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace http              = "http://expath.org/ns/http-client";
declare namespace request           = "http://exist-db.org/xquery/request";
declare namespace xis               = "http://art-decor.org/ns/xis";
declare namespace hl7               = "urn:hl7-org:v3";

(: Log debug messages? :)
declare variable $debug     := false();

let $messageInfo            := if (request:exists()) then request:get-data()/message else (
<message id="32250207365621497" interactionId="ZTKZ_IN000001NL02" soapAction="urn:hl7-org:v3/verwijzingHuisartsZorggroep02_Verwijzing" account="art-decor" resources="/db/apps/hl7/ketenzorg-qual-20180820T104547" payloadFile="/db/apps/hl7/ketenzorg-qual-20180820T104547/xml/ZTKZ_EX000001NL02_01_verwijzingHuisartsZorgroep.xml" endpoint="/verwijzingHuisartsZorggroep02" endpoints="/verwijzingHuisartsZorggroep02">
    <uziInfo>
        <issuer>CN=TEST UZI-register Zorgverlener CA G21, O=agentschap Centraal Informatiepunt Beroepen Gezondheidszorg, C=NL</issuer>
        <issuerName>TEST UZI-register Zorgverlener CA G21</issuerName>
        <decimalSerialNumber>268402373983222641038909939159818137043</decimalSerialNumber>
        <subject>C=NL, O=Gezondheidscentrum Amby, CN=Peter van Pulver, SERIALNUMBER=900009451, T=huisarts</subject>
        <subjectName>Peter van Pulver</subjectName>
        <subjectTitle>Huisarts</subjectTitle>
        <subjectOrganisation>Gezondheidscentrum Amby</subjectOrganisation>
        <notAfter>2014-12-21T10:13:46+01:00</notAfter>
        <notBefore>2011-12-22T10:13:46+01:00</notBefore>
        <uziNumber>900009451</uziNumber>
        <uziType>Z</uziType>
        <uraNumber>00004298</uraNumber>
        <roleCode>01.015</roleCode>
    </uziInfo>
    <patientId/>
    <sender applicationId="42"/>
    <receiver applicationId="84" url="http://localhost:8877/xis" organizationRegisterId="00002222"/>
</message>
)

let $account                := $messageInfo/@account
let $resources              := $messageInfo/@resources
let $configuration          := doc($get:strTestAccounts)//xis:testAccount[@name=$account]/xis:xis
let $resourcesPath          := if (string-length($resources)=0) then adxaccounts:getDefaultXmlResourcesPath($account) else $resources

let $g                      := if ($debug) then (util:log('DEBUG', concat('======xis-send-message.xquery====== Message info: ',$account, ' resourcePath: ', $resourcesPath))) else ()
let $g                      := if ($debug) then (util:log('DEBUG', <i>======xis-send-message.xquery====== $messageInfo: {$messageInfo}</i>)) else ()

(: path where messages are stored :)
let $resultdir              := concat($get:strXisAccounts, '/',$account,'/',$adxfiles:_filedir)

let $messageTemplateFile    := concat($resourcesPath,'/message-templates/',$messageInfo/@interactionId,'.xml')
let $messageId              := $messageInfo/@id
let $applicationId          := $messageInfo/sender/@applicationId
let $systemCertificateId    := $messageInfo/uziInfo/uziNumber/text()
let $messageIdLeaf          := 
    if ($applicationId castable as xs:integer) 
    then xs:integer($applicationId) 
    else (substring(string-join(for $i in string-to-codepoints($applicationId) return string($i),''),1,8))
let $messageIdRoot          := concat('2.16.840.1.113883.2.4.6.6.',$messageIdLeaf,'.1')
let $receiverId             := $messageInfo/receiver/@applicationId
let $receiverUrl            := concat($messageInfo/receiver/@url,$messageInfo/@endpoint)
let $soapAction             := $messageInfo/@soapAction
let $receiverRegisterId     := if (string-length($messageInfo/receiver/@organizationRegisterId)>0) then
                                    $messageInfo/receiver/@organizationRegisterId
                                 else '12345678'
let $authorId               := $messageInfo/uziInfo/uziNumber/text()
let $authorName             := $messageInfo/uziInfo/subjectName/text()
let $authorRoleCode         := $messageInfo/uziInfo/roleCode/text()
let $authorRole             := $messageInfo/uziInfo/subjectTitle/text()
let $organizationId         := $messageInfo/uziInfo/uraNumber/text()
let $organizationName       := $messageInfo/uziInfo/subjectOrganisation/text()
let $organizationCode       := $configuration/xis:organizationRoleCode/@code
let $organizationCodeName   := $configuration/xis:organizationRoleCode/@displayName
let $organizationCity       := $configuration/xis:organizationCity/text()
let $controlActFileExists   := 
    if ($messageInfo[string-length(@payloadFile)>0]) then doc-available(xmldb:encode($messageInfo/@payloadFile)) else false()

let $controlActFile         := if ($controlActFileExists) then (doc(xmldb:encode($messageInfo/@payloadFile))) else ()
let $patientId              := 
    if (string-length(normalize-space(($messageInfo/patientId/text())[1]))>0) then
        ($messageInfo/patientId/text())[1]
    else if ($controlActFileExists) then (
        if (exists($controlActFile//hl7:attentionLine[hl7:keyWordText[@code='PATID'][@codeSystem='2.16.840.1.113883.2.4.15.1']])) then
            ($controlActFile//hl7:attentionLine[hl7:keyWordText[@code='PATID'][@codeSystem='2.16.840.1.113883.2.4.15.1']]/hl7:value/@extension/string())[1]
        else (
            ($controlActFile//hl7:ControlActProcess/hl7:subject//hl7:*[@root='2.16.840.1.113883.2.4.6.3']/@extension/string())[1]
        )
    )
    else ()
let $parsedAuthorName       :=
    <name xmlns="urn:hl7-org:v3">
        {if (string-length(tokenize($authorName,'\s')[1])>0) then
            <given>{tokenize($authorName,'\s')[1]}</given>
         else ()
        }
        {if (contains(tokenize($authorName,'\s'),'van')) then
            <prefix qualifier="VV">van </prefix>
         else ()
        }
        <family qualifier="BR">{tokenize($authorName,'\s')[last()]}</family>
    </name>
let $queryParameters        := $messageInfo/parameters

(: this variable is used as subject in the message template, for instance: /db/apps/hl7/AORTA_v61000/message-templates/PORX_IN932000NL.xml :)

let $controlActSubjects     :=
    if ($controlActFileExists) then (
        util:eval(fn:serialize($controlActFile//hl7:ControlActProcess))/hl7:subject
    ) else ()
    
(: this variable can be used in the message template, for instance: /db/apps/hl7/AORTA_v61000/message-templates/PORX_IN932000NL.xml :)
let $attentionLine          := $controlActFile//hl7:attentionLine

let $g                      := if (exists($controlActSubjects)) then util:log('DEBUG', concat('======xis-send-message.xquery====== controlActSubjects node: ',string-join($controlActSubjects/*/local-name(),' '))) else ()
let $g                      := util:log('DEBUG', concat('======xis-send-message.xquery====== Before util:eval: ',$messageTemplateFile))
let $g                      := util:log('DEBUG', concat('======xis-send-message.xquery====== Before util:eval. doc-available? ',doc-available($messageTemplateFile)))

let $hl7message             :=
    if (doc-available($messageTemplateFile)) then (
        util:eval(
            fn:serialize(doc($messageTemplateFile)/*,
            <output:serialization-parameters xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization">
                <output:method>xml</output:method>
            </output:serialization-parameters>)
        )
    ) else ()

let $g                      := util:log('DEBUG', concat('======xis-send-message.xquery====== After util:eval: ',$messageTemplateFile))

let $soapMessage            := <soap:Envelope><soap:Body>{$hl7message}</soap:Body></soap:Envelope>
let $requestHeaders         := 
    <http:request method="POST" href="{$receiverUrl}">
        <http:header name="Content-Type" value="text/xml;charset=UTF-8"/>
        <http:header name="Cache-Control" value="no-cache"/>
        <http:header name="Max-Forwards" value="1"/>
        <http:header name="SOAPAction" value="""{$soapAction}"""/>
        <http:body media-type="text/xml" method="xml"encoding="UTF-8" omit-xml-declaration="yes">{$soapMessage}</http:body>
    </http:request>
   
let $g                      := util:log('DEBUG', concat('======xis-send-message.xquery====== Endpoint URL   : ', $receiverUrl))
let $g                      := util:log('DEBUG', concat('======xis-send-message.xquery====== SOAPAction     : ', $soapAction))
let $g                      := util:log('DEBUG', concat('======xis-send-message.xquery====== Message to send: ', $soapMessage))

let $response               := http:send-request($requestHeaders)

(: check return status. If not 200, not OK :)
let $result                 := 
    if ($response[self::http:response]/@status = '200') then
        $response[not(self::http:response)]
    else
        $response

(: get all the logged messages. If we just sent a message to our own server, this will include a message with that messageId :)
let $messages               := (collection($resultdir)/file/data//*[hl7:interactionId] |
                                collection($resultdir)/file/data//hl7:ClinicalDocument[not(hl7:text)] |
                                collection($resultdir)/file/data//*[@representation='B64'] |
                                collection($resultdir)/file/data//*[@xsi:type='xs:base64Binary'])

(: filter out the message matching the same messageId we just sent :)
let $matchingMessages       := $messages//*[@extension=$messageId]

(: if there was a matching message (already stored by SOAP-reponse.xquery) then set variable to false :)
let $storeMessageBoolean    := if ($matchingMessages) then false() else true()

(: store outgoing message :)
let $store                  := 
    if (string-length($account) gt 0 and $storeMessageBoolean) then (
        adxfiles:saveFile($account, concat(util:uuid(),'.xml'), $soapMessage)
    ) else ()

(: store incoming response including headers :)
let $responseStore          := 
    if (string-length($account) gt 0 and $storeMessageBoolean) then (
        adxfiles:saveFile($account, concat(util:uuid(),'.xml'), <http:response>{$response}</http:response>)
    ) else ()

return
    $result