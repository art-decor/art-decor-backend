xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adpfix   = "http://art-decor.org/ns/xis-permissions" at "../api/api-permissions.xqm";

let $fix    := adpfix:setXisPermissions()

return
    (:<result>{adpfix:getCurrentPermissions('/db/apps/art/modules')}</result>:)
    <return>true</return>
