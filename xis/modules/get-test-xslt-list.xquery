xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

(: resources path:)
let $resourcesPath := if (request:exists()) then request:get-parameter('resourcesPath','') else '/db/apps/hl7/rivmsp-20130812T114806'

return
<tests>
{
    if (xmldb:collection-available(concat($resourcesPath,'/test_xslt'))) then
        if (collection(concat($resourcesPath,'/test_xslt'))//testset) then
            collection(concat($resourcesPath,'/test_xslt'))//testset/test
        else 
            for $file in xmldb:get-child-resources(concat($resourcesPath,'/test_xslt'))
            order by $file
            return if (not(ends-with($file, '.xsl'))) then () else 
                <test name="{substring-before($file,'.xsl')}">
                    <name>{substring-before($file,'.xsl')}</name>
                </test>
    else ()
}
</tests>