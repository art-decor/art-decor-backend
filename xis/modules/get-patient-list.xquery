xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace adxfiles    = "http://art-decor.org/ns/art-decor/xis/files" at "../api/api-xis-files.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../art/api/api-server-settings.xqm";

declare namespace hl7       = "urn:hl7-org:v3";

let $account                := request:get-parameter('account','')

let $xmlPath                := 
   if (string-length($account)>0) then
      concat($get:strXisAccounts, '/',$account,'/',$adxfiles:_filedir)
   else ()

let $patients               :=
    for $patient in (collection($xmlPath)/file/data//hl7:Patient[hl7:Person/hl7:name] | 
                     collection($xmlPath)/file/data//hl7:patient[hl7:patientPerson/hl7:name])
    let $id := $patient/hl7:id[1]/concat(@root,@extension)
    group by $id
    order by $id
    return $patient[1]

return
<patients>
{
    for $patient in $patients
    let $prescriptionLists  := $patients/ancestor::hl7:MedicationPrescriptionList
    let $dispenseLists      := $patients/ancestor::hl7:MedicationDispenseList
    let $observationReports := $patients/ancestor::hl7:observationReport
    let $conditions         := $patients/ancestor::hl7:Condition
    (:order by $patient/*/hl7:name/hl7:family[1]:)
    return
        <patient>
            <name>{adxfiles:getNameFromHL7Name($patient/hl7:Person/hl7:name | $patient/hl7:patientPerson/hl7:name)}</name>
            <bsn>{$patient/hl7:id[@root='2.16.840.1.113883.2.4.6.3']/@extension/string()}</bsn>
            <birthDate>{adxfiles:getDateFromHL7Date($patient/*/hl7:birthTime/@value)}</birthDate>
            <gender>
            {if ($patient/hl7:Person/hl7:administrativeGenderCode/@code='M') then
                'M'
            else if ($patient/hl7:Person/hl7:administrativeGenderCode/@code='F') then
                'V'
            else ('Onbekend')}
            </gender>
            <conditions>{count($conditions)}</conditions>
            <prescriptions>{count($prescriptionLists//hl7:prescription)}</prescriptions>
            <dispenseEvents>{count($dispenseLists//hl7:medicationDispenseEvent)}</dispenseEvents>
            <labresults>{count($observationReports//hl7:observationEvent)}</labresults>
        </patient>
}
</patients>
