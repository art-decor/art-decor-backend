xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace adxfiles    = "http://art-decor.org/ns/art-decor/xis/files" at "../api/api-xis-files.xqm";

declare namespace xis="http://art-decor.org/ns/xis";
let $account    := request:get-parameter('account','')
let $scenarioId := request:get-parameter('scenarioId','')
let $stepId     := request:get-parameter('stepId','')
(:let $account  := 'art-decor'
let $scenarioId := '1'
let $stepId     := '1':)
let $collection := concat($get:strXisAccounts, '/',$account,'/', $adxfiles:_messagedir)
let $action     :=
    collection($get:strXisResources)//xis:scenario[@id=$scenarioId]/xis:step[@id=$stepId]/xis:action
let $resultOK   :=
    if ($action/@type='store') then
        adxfiles:saveFile($account, $action/@filename, $action/xis:message/node())
    else()
return
<response>{$resultOK}</response>