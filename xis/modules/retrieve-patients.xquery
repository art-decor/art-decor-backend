xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:
   Retrieve all patients in data collection
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";

declare namespace hl7="urn:hl7-org:v3";

<patients>
{
    for $patients in collection($get:strXisHelperConfig)//hl7:Patient
    order by $patients/hl7:Person/hl7:name/hl7:family[1]
    return
    $patients
}
</patients>

