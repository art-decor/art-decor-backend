xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace adxfiles    = "http://art-decor.org/ns/art-decor/xis/files" at "../api/api-xis-files.xqm";

declare namespace soap              = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace http              = "http://expath.org/ns/http-client";

(: Log debug messages? :)
declare variable $debug     := false();

let $account                := if (request:exists()) then request:get-parameter('account','') else ('aorta-nictiz')
let $file                   := if (request:exists()) then xmldb:encode(request:get-parameter('file','')) else ('e265436a-e583-48ac-a543-6ae583a8ac4c.xml')
let $receiverUrl            := if (request:exists()) then request:get-parameter('url','') else ('https://kwalificatie.nictiz.nl/xis/Ping')
let $soapAction             := if (request:exists()) then request:get-parameter('soapAction','') else ('urn:hl7-org:v3/Ping_TickTock')

let $fileContent            := adxfiles:getFile($account, $file)
let $soapMessage            :=
    if ($fileContent[self::soap:Envelope]) then
        $fileContent[self::soap:Envelope]
    else (
        <soap:Envelope><soap:Body>{$fileContent}</soap:Body></soap:Envelope>
    )
let $requestHeaders         := 
    <http:request method="POST" href="{$receiverUrl}">
        <http:header name="Content-Type" value="text/xml;charset=UTF-8"/>
        <http:header name="Cache-Control" value="no-cache"/>
        <http:header name="Max-Forwards" value="1"/>
        <http:header name="SOAPAction" value="""{$soapAction}"""/>
        <http:body media-type="text/xml" method="xml"encoding="UTF-8" omit-xml-declaration="yes">{$soapMessage}</http:body>
    </http:request>
   
let $g                      := util:log('DEBUG', concat('======xis-send-file.xquery====== Endpoint URL: ', $receiverUrl))
let $g                      := util:log('DEBUG', concat('======xis-send-file.xquery====== SOAPAction  : ', $soapAction))
let $g                      := util:log('DEBUG', concat('======xis-send-file.xquery====== File to send: ', $soapMessage))

let $response               := http:send-request($requestHeaders)

(: check return status. If not 200, not OK :)
let $result                 := 
    if ($response[self::http:response]/@status = '200') then
        $response[not(self::http:response)]
    else
        $response

(: if there was a matching message (already stored by SOAP-reponse.xquery) then set variable to false :)
let $storeMessageBoolean    := true()

(: store outgoing message :)
let $store                  := 
    if (string-length($account) gt 0 and $storeMessageBoolean) then (
        adxfiles:saveFile($account, concat(util:uuid(),'.xml'), $soapMessage)
    ) else ()

(: store incoming response including headers :)
let $responseStore          := 
    if (string-length($account) gt 0 and $storeMessageBoolean) then (
        adxfiles:saveFile($account, concat(util:uuid(),'.xml'), <http:response>{$response}</http:response>)
    ) else ()

return
    $result