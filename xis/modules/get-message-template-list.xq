xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adxss   = "http://art-decor.org/ns/art-decor-xis-services" at "../api/api-xis-services.xqm";

adxss:getAvailableServices(request:get-parameter('xmlResources',()))
