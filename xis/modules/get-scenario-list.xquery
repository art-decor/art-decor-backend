xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at  "../../art/modules/art-decor.xqm";
import module namespace adxfiles    = "http://art-decor.org/ns/art-decor/xis/files" at "../api/api-xis-files.xqm";

declare namespace xis="http://art-decor.org/ns/xis";

let $account        := request:get-parameter('account','')
(:let $account      := 'art-decor':)
let $scenarios      := collection($get:strXisResources)//xis:scenarios
let $resultdir      := concat($get:strXisAccounts, '/',$account,'/',$adxfiles:_filedir)

return
<scenarioList xmlns="http://art-decor.org/ns/xis">
{
    for $scenario in $scenarios/xis:scenario
    return
        <scenario>
        {
            $scenario/@*,
            $scenario/xis:name,
            $scenario/xis:desc,
            for $step in $scenario/xis:step
            let $statusCode :=
                if ($step/xis:action/@type='store' and doc-available(concat($resultdir,'/',$step/xis:action/@filename))) then
                    'completed'
                else ($step/@statusCode)
            return
                <step id="{$step/@id}" seqnr="{$step/@seqnr}" referenceDate="{$step/@referenceDate}" statusCode="{$statusCode}">
                {
                    $step/xis:name,
                    $step/xis:desc
                }
                </step>
        }
        </scenario>
}
</scenarioList>