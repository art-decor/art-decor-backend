xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace val         = "http://art-decor.org/ns/art-decor/xis/validation" at "../api/api-xis-validation.xqm";
import module namespace adxfiles    = "http://art-decor.org/ns/art-decor/xis/files" at "../api/api-xis-files.xqm";
import module namespace adxaccounts = "http://art-decor.org/ns/art-decor/xis/accounts" at "../api/api-xis-accounts.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../art/api/api-server-settings.xqm";

declare namespace aderr     = "http://art-decor.org/ns/error";
declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";
declare namespace transform = "http://exist-db.org/xquery/transform";
declare namespace xis       = "http://art-decor.org/ns/xis";
declare namespace hl7       = "urn:hl7-org:v3";
declare option exist:serialize "method=xhtml media-type=text/html";

let $account            := if (request:exists()) then request:get-parameter('account',()) else ('art-decor')
let $recreate           := if (request:exists()) then request:get-parameter('recreate',())='true' else (true())
(:
    normally you would use encode-for-uri() on $file, but when the filename contains 
    () for example, this leads to FileNotFound. Apparently only spaces need to be handled
:)
let $fileName           := if (request:exists()) then replace(request:get-parameter('file',()),' ','%20') else ('MCCI_EX200101_01_Medicatieoverzicht.xml')

let $xpath              := if (request:exists()) then request:get-parameter('xpath','*') else ('*[1]')
let $language           := if (request:exists()) then request:get-parameter('language','en-US') else ('en-US')

(:Need to know which resources to get the viewer from:)
let $resources          := if (request:exists()) then request:get-parameter('resources',())[string-length()>0] else ()
let $viewer             := if (request:exists()) then request:get-parameter('viewer',())[string-length()>0] else ()

let $xsltParametersCDA  := ()
let $xsltParameters     :=
    <parameters>
        <param name="textLang" value="{$language}"/>
    </parameters>

(:
    there two types of calls here. 
  - file name only, which should render a message in the captured messages under the test account
  - full path, which should render a message, normally in the HL7 package directory
:)
return
try {
    let $message           := 
        if (contains($fileName,'/') and doc-available($fileName)) then
            util:eval(concat('doc($fileName)/',$xpath))
        else (
            adxfiles:getMessage($account, $fileName, $xpath)
        )
    
    return
        if (count($message) != 1) then (
            error(QName('http://art-decor.org/ns/error', 'RenderError'), concat('Can only render exactly one message at a time. Found ',count($message),' in ',$fileName,' with xpath=',$xpath))
        )
        else (
            let $htmlcontent    := adxfiles:getRendering($account, $fileName, $xpath, $resources, $viewer)[1]/node()
            return
            if ($htmlcontent and not($recreate)) then
                $htmlcontent
            else
            if (string-length($resources)>0 and string-length($viewer)>0) then (
                let $fullviewer     := adxaccounts:getFullViewerPath($resources, $viewer)
                let $xsltParameters     :=
                    <parameters>
                        <param name="textLang" value="{$language}"/>
                        <param name="packageBase" value="{string-join(tokenize($fullviewer,'/')[position() != last()],'/')}"/>
                    </parameters>
                let $stylesheet     := doc($fullviewer)
                
                (: hacky, but necessary. The transform routine for schematron will kill the carefully reproduced @xsi:type value prefix declarations. When we save to disk first... it doesn't. Go figure :)
                let $tempFile           := adxfiles:saveTempMessage($account, $message)
                let $message            := doc($tempFile)/*
                
                let $htmlcontent    := transform:transform($message, $stylesheet, $xsltParameters)
                
                (: Cleanup of hacky stuff when we retrieved the message :)
                let $deleteTempFile     := adxfiles:deleteFile($account, $tempFile)
                
                let $update         := try { adxfiles:saveRendering($account, $fileName, $xpath, $resources, $viewer, $htmlcontent) } catch * {()}
                return $htmlcontent
            )
            else
            if ($message/self::hl7:ClinicalDocument) then (
                let $fullviewer     := $adxaccounts:strCDAr2Viewer
                let $stylesheet     := doc($fullviewer)
                
                (: hacky, but necessary. The transform routine for schematron will kill the carefully reproduced @xsi:type value prefix declarations. When we save to disk first... it doesn't. Go figure :)
                let $tempFile           := adxfiles:saveTempMessage($account, $message)
                let $message            := doc($tempFile)/*
                
                let $htmlcontent    := transform:transform($message, $stylesheet, $xsltParametersCDA)
                
                (: Cleanup of hacky stuff when we retrieved the message :)
                let $deleteTempFile     := adxfiles:deleteFile($account, $tempFile)
                
                let $update         := try { adxfiles:saveRendering($account, $fileName, $xpath, (), (), $htmlcontent) } catch * {()}
                return $htmlcontent
            )
            else (
                let $fullviewer     := $adxaccounts:strInteractionViewer
                let $stylesheet     := doc($fullviewer)
                
                (: hacky, but necessary. The transform routine for schematron will kill the carefully reproduced @xsi:type value prefix declarations. When we save to disk first... it doesn't. Go figure :)
                let $tempFile           := adxfiles:saveTempMessage($account, $message)
                let $message            := doc($tempFile)/*
                
                let $htmlcontent    := transform:transform($message, $stylesheet, $xsltParameters)
                
                (: Cleanup of hacky stuff when we retrieved the message :)
                let $deleteTempFile     := adxfiles:deleteFile($account, $tempFile)
                
                let $update         := try { adxfiles:saveRendering($account, $fileName, $xpath, (), (), $htmlcontent) } catch * {()}
                return $htmlcontent
            )
        )
} 
catch * {
    let $code   := if (string($err:code) = 'de:UnsufficientPermissions') then 401 else (500)
    
    return (
    if (response:exists()) then (
        response:set-status-code($code),
        (: RFC 2617 https://tools.ietf.org/html/rfc2617#section-3.2.1 :)
        if ($code = 401) then (response:set-header('WWW-Authenticate','Basic realm="exist"')) else ()
    ) else (),
    <html>
    <head>
        <title>HTTP {$code} Rendering error</title>
        <style type="text/css" media="print, screen">body {{ font-family: Verdana, Arial, sans-serif; font-size:10px; }}</style>
    </head>
    <body>
    {
        if ($code = 401) then (
            <h3>HTTP {$code}. Error rendering message for account "{$account}". You might need to authenticate first.</h3>,
            <p>ERROR {$err:code}. Description: {$err:description}</p>
        )
        else (
            <h3>HTTP {$code}. Error rendering message for account "{$account}".</h3>,
            <p>ERROR {$err:code}. Description: {$err:description}. Module: {$err:module,' (',$err:line-number,' ',$err:column-number,')'}</p>,
            <p>{try { adxaccounts:getFullViewerPath($resources, $viewer) } catch * {()}}</p>
        )
    }
    </body>
    </html>
    )
}