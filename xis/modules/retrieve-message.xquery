xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at  "../../art/modules/art-decor.xqm";
import module namespace adxfiles    = "http://art-decor.org/ns/art-decor/xis/files" at "../api/api-xis-files.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../art/api/api-server-settings.xqm";

declare namespace request       = "http://exist-db.org/xquery/request";
declare namespace response      = "http://exist-db.org/xquery/response";
declare namespace soap          = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace hl7           = "urn:hl7-org:v3";
declare namespace xs            = "http://www.w3.org/2001/XMLSchema";
declare namespace xis           = "http://art-decor.org/ns/xis";
declare namespace de            = "http://art-decor.org/ns/error";
declare namespace hl7nl         = "urn:hl7-nl:v3";

(: Log debug messages? :)
let $debug             := false()

(: account :)
let $account           := if (request:exists()) then request:get-parameter('account',()) else ('art-decor')
(: tests :)
let $tests             := if (request:exists()) then request:get-parameter('tests','false') else ('false')
(: file name :)
let $file              := if (request:exists()) then request:get-parameter('file',()) else ('e753c45b-d5f3-4426-8003-d95878df97ad.xml')
(: xpath to fragment :)
let $xpath             := if (request:exists()) then request:get-parameter('xpath',()) else ('*[1]/*[2]/*[1]/*[1]/*[1]/*[13]')
(: serialized contents or not :)
let $serialized        := if (request:exists()) then request:get-parameter('serialized','false')='true' else (false())
(: download :)
let $download          := if (request:exists()) then request:get-parameter('download','false')='true' else (false())
(: preprocess :)
let $preprocess        := if (request:exists()) then request:get-parameter('preprocess','false')='true' else (true())

(:let $g := if ($debug) then (util:log('DEBUG', concat('============ Supplied parameters: file=',$file,' root=',$root,' extension=',$extension))) else ():)
let $g := if ($debug) then (util:log('DEBUG', concat('===(RetrieveMessage)=== Supplied parameters: account=',$account, 'file=',$file,' xpath=',$xpath))) else ()

(: Messages are normally only located in the collection xis/<account>/messages/. Get the resource (file). 
   Then return the requested fragment of that file based on the provided xpath expression, or the full contents if
   xpath is empty
:)

(: Read account option to switch xml display off :)
(:2014-08-27 This check is not useful anymore since display is removed from the tests/messages forms and loaded in separate tab/window:)
let $getMessageXmlOff   := false()
    (:if (not(empty($account))) then
        exists(doc($get:strTestAccounts)//xis:testAccount[@name=$account]//xis:getMessageXml[string(.)='false'])
    else (false()):)

(: write the $getMessageXmlOff to exist log :)
let $g                  := util:log('DEBUG', concat('---($getMessageXmlOff)--- Is xis display on (switch=false=default) or off (switch=true) ', $getMessageXmlOff))

(:
    normally you would use encode-for-uri() on $file, but when the filename contains 
    () for example, this leads to FileNotFound. Apparently only spaces need to be handled
:)
let $strFile                := 
    if (contains($file,'/')) then
        replace($file,' ','%20')
    else if (not(empty($account)) and $tests='true') then
        concat($get:strXisAccounts,'/',$account,'/testseries.xml')
    else if (not(empty($account) or empty($file))) then
        $file
    else ()

let $messages               := 
    try {
        if ($getMessageXmlOff) then (
            <result>Message retrieval switched off for this account. {$account}</result>
        )
        else if ($tests='true' and not(sm:has-access($strFile,'r'))) then (
            error(QName('http://art-decor.org/ns/error', 'UnsufficientPermissions'), concat('You have insufficient permissions for this account: ''',$account,'''. You need read access'))
        )
        else if ($tests='true' and not(doc-available($strFile))) then (
            <result>Message '{$file}' could not be found in tests as '{$strFile}'. Account {$account}</result>
        )
        else if (empty($strFile)) then (
            <result>Message could not be found. Account {$account}, File {$file}</result>
        )
        else (
            if ($tests = 'true' and string-length($xpath) gt 0) then (
                (:kinda hacky... we're normally called from tests.xhtml.. please note that this logic needs to be in sync with that:)
                doc($strFile)//*[starts-with($xpath, concat('/',string-join(ancestor-or-self::*/concat('*[',count(preceding-sibling::xis:*)+1,']'),'/')))]/xis:validation/messageFile/*
            ) else 
            if (string-length($account) gt 0) then (
                (:we are called for a file or message inside some account.:)
                if (string-length($xpath) gt 0) then (
                    adxfiles:getMessage($account, $file, $xpath)
                ) else (
                    adxfiles:getFile($account, $file)
                )
            ) else 
            if (string-length($xpath) gt 0) then (
                util:eval(concat('doc($strFile)/',$xpath))
                (:doc($strFile)//*[$xpath=util:node-xpath(.)]:)
            ) else (
                doc($strFile)/*
            )
        )
    }
    catch * {
        (: RFC 2617 https://tools.ietf.org/html/rfc2617#section-3.2.1 :)
        if (request:exists() and $err:code = 'de:UnsufficientPermissions') then (
            response:set-status-code(401),response:set-header('WWW-Authenticate','Basic realm="exist"')
        ) else (),
        <issue type="message" role="error">
            <description>ERROR (RetrieveMessage) {$err:code} in retrieval of message from file '{$file}'{if (string-length($xpath)=0) then () else concat(', with xpath ',$xpath)}: 
            {$err:description, "', module: ", $err:module, "(", $err:line-number, ",", $err:column-number, ")"}</description>
            <location line=""/>
        </issue>
    }

let $g                      := if ($debug) then (util:log('DEBUG', concat('===(RetrieveMessage)=== Message instance found? ',count($messages)))) else ()

let $preprocessxsl          := '../resources/stylesheets/pre-process-xml.xsl'
let $fileNameforDownload    := $file

return (
    if (request:exists()) then (
        response:set-header('Content-Type','application/xml'),
        if ($download) then response:set-header('Content-Disposition', concat('attachment; filename=',$fileNameforDownload)) else ()
    )
    else (),
    if ($serialized) then (
        art:serializeNode(transform:transform(<messages>{$messages}</messages>, doc($preprocessxsl), ()))
    ) else
    if (count($messages) = 1) then (
        if ($preprocess) then transform:transform($messages, doc($preprocessxsl), ()) else $messages
    ) else (
        if ($preprocess) then transform:transform(<messages>{$messages}</messages>, doc($preprocessxsl), ()) else <messages>{$messages}</messages>
    )
)