xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace adxaccounts = "http://art-decor.org/ns/art-decor/xis/accounts" at "../api/api-xis-accounts.xqm";
declare namespace xis       = "http://art-decor.org/ns/xis";
declare namespace hl7       = "urn:hl7-org:v3";

(: Log debug messages? :)
let $debug              := false()

(: server path:)
let $account           := if (request:exists()) then request:get-parameter('account',()) else ()
let $resources         := if (request:exists()) then request:get-parameter('resources',())[string-length()>0] else ()
(:let $account           := 'art-decor':)
let $query-data        := if (request:exists()) then request:get-data()/queries else ()

let $g := if ($debug) then (util:log('DEBUG', concat('============ Supplied parameters: account=',$account))) else ()

let $resourcesPath     := if (empty($resources)) then adxaccounts:getDefaultXmlResourcesPath($account) else $resources
let $queryManifestFile := concat($resourcesPath,'/message-templates/query_manifest.xml')
let $queryManifest     := if (doc-available($queryManifestFile)) then doc($queryManifestFile)/* else ()

return
    if ($queryManifest) then (
        $queryManifest
    )
    else (
        $query-data
    )