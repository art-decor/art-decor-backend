xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace adxfiles    = "http://art-decor.org/ns/art-decor/xis/files" at "../api/api-xis-files.xqm";
declare namespace compression   = "http://exist-db.org/xquery/compression";
declare option exist:serialize "method=xml media-type=application/zip,application/octet-stream charset=utf-8";

declare %private function local:writeFilesForZip($account as xs:string, $zipdir as xs:string) {
    let $files      := adxfiles:getFile($account, ())
    
    (:write elements only, not comments/processing-instructions:)
    let $writeFiles := 
        for $f in $files
        return 
            if ($f instance of element()) then xmldb:store($zipdir, util:document-name($f), $f) else ()
    
    return count($writeFiles)
};

(:which xis-data/accounts/ account do we want to download?:)
let $account        := if (request:exists()) then request:get-parameter('account',()) else ()
(:set date to a variable:)
let $date           := format-dateTime(current-dateTime(),'[Y0001][M01][D01]-[H01][m01]')

let $accountdir     := concat($get:strXisAccounts, '/', $account)
let $zipdir         := concat($adxfiles:_zipdir,'_',$account,'_',$date)
let $zipcoll        := concat($accountdir, '/', $zipdir)
let $zipcoll        := 
    if (xmldb:collection-available($zipcoll)) then ($zipcoll) else (xmldb:create-collection($accountdir, $zipdir))

let $writeFiles     := local:writeFilesForZip($account, $zipcoll)

(: todo: would like to get all folders at once but then filename collisions occur, same filename in /messages and /reports:)
(:let $attachments := concat($get:strXisAccounts, '/', $account, '/attachments')
let $reports     := concat($get:strXisAccounts, '/', $account, '/reports')
let $testseries  := concat($get:strXisAccounts, '/', $account, '/testseries.xml'):)

(:create response which contains zip:)
return (
    response:set-header("Content-Disposition", concat('attachment; filename=',$zipdir,'.zip'))
    ,
    response:stream-binary(
        compression:zip( xs:anyURI($zipcoll), false() ),
        'application/zip',
        concat($zipdir,'.zip')
    )
    ,
    xmldb:remove($zipcoll)
)