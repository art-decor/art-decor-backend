xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace art = "http://art-decor.org/ns/art" at  "../../art/modules/art-decor.xqm";

declare namespace xis="http://art-decor.org/ns/xis";

let $rawTestsuites := if (request:exists()) then request:get-data()/* else ()

let $editedTestsuites   :=
    <testsuites>
    {
        for $testsuite in $rawTestsuites/testsuite
        order by $testsuite/number(@id)
        return
        <testsuite>
        {
            $testsuite/@*,
            for $name in $testsuite/name
            return
            art:parseNode($name)
            ,
            $testsuite/application-role,
            for $desc in $testsuite/desc
            return
            art:parseNode($desc)
            ,
            $testsuite/xmlResourcesPath,
            for $test in $testsuite/test
            return
            <test>
            {
            $test/@*,
            for $name in $test/name
            return
            art:parseNode($name)
            ,
            for $desc in $test/desc
            return
            art:parseNode($desc)
            }
            </test>
        }
        </testsuite>
    }
    </testsuites>

let $testsuites     := doc($get:strTestSuites)/testsuites
let $update         := update value $testsuites with $editedTestsuites/*

return
<response>
{
    $testsuites
}
</response>

