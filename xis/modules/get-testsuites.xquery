xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at  "../../art/modules/art-decor.xqm";
import module namespace val         = "http://art-decor.org/ns/art-decor/xis/validation" at "../api/api-xis-validation.xqm";

declare namespace xhtml = "http://www.w3.org/1999/xhtml";
declare namespace xis   = "http://art-decor.org/ns/xis";

let $resources  := if (request:exists()) then request:get-parameter('resources',()) else ()
let $testsuites := 
    if (empty($resources)) then
        val:getTestSuites()
    else (
        val:getTestSuitesByXmlResources($resources)
    )

return
<testsuites>
{
    for $suite in $testsuites
    order by $suite/name/lower-case(text())
    return
    <testsuite>
    {
        $suite/@*,
        for $name in $suite/name
        return
            art:serializeNode($name)
        ,
        $suite/application-role,
        for $desc in $suite/desc
        return
            art:serializeNode($desc)
        ,
        $suite/xmlResourcesPath,
        for $test in $suite/test
        return
        <test>
        {
            $test/@*,
            if ($test[name]) then () else (<name/>),
            for $name in $test/name
            return
                art:serializeNode($name)
            ,
            if ($test[desc]) then () else (<desc/>),
            for $desc in $test/desc
            return
                art:serializeNode($desc)
        }
        </test>
    }
    </testsuite>
}
</testsuites>