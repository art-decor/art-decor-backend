xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace art     = "http://art-decor.org/ns/art" at "../../art/modules/art-decor.xqm";
import module namespace val     = "http://art-decor.org/ns/art-decor/xis/validation" at "../api/api-xis-validation.xqm";

declare namespace transform     = "http://exist-db.org/xquery/transform";
declare namespace request       = "http://exist-db.org/xquery/request";
declare namespace response      = "http://exist-db.org/xquery/response";
declare namespace util          = "http://exist-db.org/xquery/util";
declare namespace xis           = "http://art-decor.org/ns/xis";
declare namespace svrl          = "http://purl.oclc.org/dsdl/svrl";
declare namespace xmldb         = "http://exist-db.org/xquery/xmldb";

declare option exist:serialize "method=xml indent=no omit-xml-declaration=no";

declare %private function local:validate-iso-schematron-svrl($content as item(), $grammar as xs:anyURI) as element(report) {
    (:
    let $grammartransform           := art:get-iso-schematron-svrl(xs:anyURI($grammar))
    
    TEMPORARILY SOURCE CODE COPIED TO HERE FROM art:get-iso-schematron-svrl FOR TESTING, SHOULD BE BACK WITH THE LINE ABOVE AFTER THAT
    
    :)
    let $input-coll                 := string-join(tokenize($grammar,'/')[not(position()=last())],'/')
    let $input-res                  := replace(tokenize($grammar,'/')[last()],'.sch$','.xsl')
    
    let $dummy                      := util:log('INFO', 'validate-iso-schematron-svrl preparations with "' || tokenize($grammar,'/')[last()] || '"')
    let $grammartransform           :=
        if (doc-available(concat($input-coll,'/',$input-res))) then (
            doc(concat($input-coll,'/',$input-res))
        ) 
        else (
            let $isoschematrons := $get:strUtilISOSCH2SVRL
            
            let $path2schematroninclude     := xs:anyURI(concat($isoschematrons, "/iso_dsdl_include.xsl"))
            let $path2schematronabstract    := xs:anyURI(concat($isoschematrons, "/iso_abstract_expand.xsl"))
            let $path2schematronxsl         := xs:anyURI(concat($isoschematrons, "/iso_svrl_for_xslt2.xsl"))
            
             let $xsltParameters :=
                <parameters>
                    <param name="allow-foreign" value="true"/>
                    <param name="generate-fired-rule" value="true"/>
                    <param name="generate-paths" value="true"/>
                    <param name="diagnose" value="yes"/>
                    <param name="exist:stop-on-error" value="yes"/>
                </parameters>
                (: param name="xinclude-path" value="{$grammar}"/ :)
                
            let $input                      := doc($grammar)
            
            (: let $includetransform           := transform:transform($input, doc($path2schematroninclude), $xsltParameters) :)
            let $includetransform           := transform:transform($input, xs:anyURI(concat("xmldb:exist://", $input-coll, "/sch2xsl.xsl")), $xsltParameters)
            let $abstracttransform          := transform:transform($includetransform, doc($path2schematronabstract), $xsltParameters)
            let $grammartransform           := transform:transform($abstracttransform, doc($path2schematronxsl), $xsltParameters)
            let $store                      := xmldb:store($input-coll, $input-res, $grammartransform)
            
            return doc($store)
        )
    
    let $rulescnt                   := count($grammartransform//svrl:failed-assert)
    let $dummy                      := util:log('INFO', 'validate-iso-schematron-svrl started with "' || $input-res || '", #rules=' || $rulescnt)
    let $resulttransform            := transform:transform($content, $grammartransform, ())
    let $vstatus                    := 
        if ($resulttransform/svrl:failed-assert[@role='error'] | $resulttransform/svrl:failed-assert[empty(@role)])
        then 'invalid'
        else if ($resulttransform/svrl:failed-assert[@role='warning'] | $resulttransform/svrl:successful-report)
        then 'warning'
        else if ($resulttransform/svrl:failed-assert[@role='hint'] | $resulttransform/svrl:successful-report)
        then 'info'
        else 'valid'
    let $dummy                      := util:log('INFO', 'validate-iso-schematron-svrl finished, status ' || $vstatus)

    return (
        <report>
            <status>
            {
                $vstatus
            }
            </status>
            <schematron rulescnt="{$rulescnt}">
            {
                <dummy/>
            }
            </schematron>
            <message>
            {
                local:strip-namespace-in-svrl-locations($resulttransform)
            }
            </message>
        </report>
    )
};

declare %private function local:strip-namespace-in-svrl-locations ($input as node()?) as element() {
    let $xslt := 
        <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svrl="http://purl.oclc.org/dsdl/svrl" version="2.0">
             <xsl:template match="/">
                <xsl:copy>
                     <xsl:apply-templates select="@*|node()"/>
                </xsl:copy>
             </xsl:template>
             <xsl:template match="svrl:failed-assert">
                <xsl:copy>
                     <xsl:apply-templates select="@* except @location"/>
                     <xsl:if test="@location">
                        <!-- [namespace-uri()='urn:hl7-org:v3'] -->
                        <xsl:attribute name="location" select="replace(replace(@location, '\[namespace-uri\(\)=[^\]]*\]', ''), '/\*:', '/')"/>
                     </xsl:if>
                     <xsl:apply-templates select="node()"/>
                </xsl:copy>
             </xsl:template>
             <xsl:template match="@*|node()">
                 <xsl:copy>
                     <xsl:apply-templates select="@*|node()"/>
                 </xsl:copy>
             </xsl:template>
         </xsl:stylesheet>
    return transform:transform($input, $xslt, ())
};

let $request            := if (request:exists()) then (request:get-data()/content) else ()
let $schref             := $request/@schref

(:
<compiled for="demo5-" on="2016-05-07T20:06:52" as="fbb9513c-17c5-4668-8f2e-f98baf53d8fb"
  by="dr Kai U. Heitmann" transactions="" closed="false" status="active">
  <dir>
    <resource name="demo5-summaryvitalsigns.sch"
      path="/db/apps/decor/releases/demo5/development/fbb9513c-17c5-4668-8f2e-f98baf53d8fb/demo5-summaryvitalsigns.sch"
      title="Scenario: POCD_MT000040 - Summary vital signs data"
      ref="65c50384-77a2-4ed2-b164-47686e8c7649"/>
  </dir>
  ...
:)
let $compiledF          := $get:colDecorVersion//compiled/dir/resource[@ref=$schref]
let $schematron         := if (empty($compiledF)) then '' else $compiledF/@path

let $tmp                := if (empty($request)) then () else util:base64-decode($request//data)
(:let $filecontent       := if (empty($tmp)) then () else fn:parse-xml($tmp):)
let $filecontent        := 
    if (empty($tmp)) then () else (
        (:Hack alert: upload fails when content has UTF-8 Byte Order Marker. the UTF-8 representation of the BOM is the byte sequence 0xEF,0xBB,0xBF:)
        let $content        := 
            if ($tmp instance of xs:base64Binary) 
            then util:base64-decode($tmp) 
            else if ($tmp instance of xs:string) 
            then $tmp
            else fn:serialize($tmp,
                <output:serialization-parameters xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization">
                    <output:method>xml</output:method>
                </output:serialization-parameters>
            )
        let $content-no-bom := 
            if (string-to-codepoints(substring($content,1,1))=65279) 
            then (substring($content,2)) 
            else ($content)
        
        return $content-no-bom
    )
(: must store the document because of the crappy @attribute transformation bug in exist :)
let $input-coll        := string-join(tokenize($schematron, '/')[not(position()=last())], '/')
let $testxml           := concat(util:uuid(), '.xml')
let $store             := xmldb:store($input-coll, $testxml, $filecontent)
let $filecontent       := doc(concat($input-coll, '/', $testxml))
(: :)


(: CDA schema (if input is a ClinicalDocument :)
let $baseCDAXSD         := concat('xmldb:exist://', '/db/apps/hl7/CDAr2/schemas_codeGen_flat/CDA.xsd')
let $cdaSchema          := 
    if ($filecontent/descendant-or-self::*[name()='ClinicalDocument']) then ( 
        if (doc-available($baseCDAXSD)) then $baseCDAXSD else ()
    ) else ()

return
    if (empty($filecontent)) then (
        <report at="{current-dateTime()}">
            <error requestlength="{string-length($request)}">Missing input file</error>
            <request>{$filecontent}</request>
        </report>
    )
    else if (string-length($schematron)=0) then (
        <report at="{current-dateTime()}">
            <error requestlength="{string-length($request)}">Cannot find schematron package {$schref} {$compiledF}</error>
            <request>{$compiledF}</request>
        </report>
    )
    else (
        if (response:exists()) then (response:set-header('Content-Type','text/xml; charset=utf-8')) else (),
        <report at="{current-dateTime()}" isCda="{not(empty($cdaSchema))}" schema="{$baseCDAXSD}" schref="{$schref}" path="{$schematron}">
            <schema>
            {
                (: validate against CDA schema if needed :)
                if (empty($cdaSchema))
                then <report><error>No associated schema found</error></report>
                else
                if (count($filecontent/descendant-or-self::*[name()='ClinicalDocument'])=0)
                then <report><error>Only ClinicalDocument XML instance implemented at this time</error></report>
                else
                    try { val:validateSchemaCDA($filecontent, $cdaSchema) }
                    catch * { <report><status>invalid</status><error>+++ Validating against schema failed (0): {$err:code}: {$err:description}</error></report> }
            }
            </schema>
            <schematron>
            {
                (: validate against schematron :)
                try { local:validate-iso-schematron-svrl ($filecontent, $schematron) }
                catch * { <report><status>invalid</status><error>+++ Validating against schematron failed (0): {$err:code}: {$err:description}</error></report> }
            }
            </schematron>
            <filecontent>
            {
                $filecontent
            }
            </filecontent>
        </report>
    )