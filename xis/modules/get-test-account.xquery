xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";

declare namespace xis = "http://art-decor.org/ns/xis";

let $user           := (get:strCurrentUserName(),'guest')[not(. = '')][1]
let $account        := if (request:exists()) then (request:get-parameter('account',())) else ()

let $testAccounts   := if (doc-available($get:strTestAccounts)) then (doc($get:strTestAccounts)/xis:testAccounts) else ()

return
    <testAccounts xmlns="http://art-decor.org/ns/xis">
    {
        for $testAccount in $testAccounts/xis:testAccount[(@id | @name)=$account]
        let $name   := if ($testAccount[string-length(@displayName)>0]) then lower-case($testAccount/@displayName) else lower-case($testAccount/@name)
        order by $name
        return
            <testAccount>
            {
                $testAccount/@*,
                for $n in $testAccount/*
                return
                    if ($n[self::xis:members]) then
                        <members>
                        {
                            $n/@*,
                            if (sm:is-dba($user)) then $n/xis:user else $n/xis:user[@id = $user]
                        }
                        </members>
                    else (
                        $n
                    )
            }
            </testAccount>
    }
    </testAccounts>