xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace adxfiles    = "http://art-decor.org/ns/art-decor/xis/files" at "../api/api-xis-files.xqm";
declare namespace xis               = "http://art-decor.org/ns/xis";

(: server path:)
let $newTests   := request:get-data()/xis:tests
(:let $user       := get:strCurrentUserName():)

(:let $update     :=
    <xis:tests>
    {
        for $test in $newTests/xis:test
        return
        <xis:test>
        {
            $test/@*,
            $test/xis:requirement,
            for $test in $test/xis:test
            return
                <xis:test>
                {
                    $test/@*,
                    for $validation in $test/xis:validation
                    return
                        <xis:validation>
                        {
                            $validation/@*,
                            $validation/messageFile | $validation/validationReport
                        }
                        </xis:validation>
                }
                </xis:test>
        }
        </xis:test>
    }
    </xis:tests>:)

let $tests      := collection(concat($get:strXisAccounts, '/',$newTests/@account))//xis:tests
let $update     :=
    if (empty($tests) or $tests[2]) then () else (
        let $u  := xmldb:store(util:collection-name($tests), util:document-name($tests), $newTests)
        let $t  := doc($u)/xis:tests
        let $d  := update delete $t//xis:test/name
        let $d  := update delete $t//xis:test/application-role
        let $d  := update delete $t//xis:test/xmlResourcesPath
        let $d  := update delete $t//xis:test/desc
        let $d  := update delete $t//xis:test/xis:validation/coreValidation
        
        return $u
    )
return
    <response>
    {
        (:update value $tests with $update/*:)
        (:$editedAccounts:)
        not(empty($update))
    }
    </response>