xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:
    Purpose: handling partial or full deletes in xis-data/account/<account>
    Minimal requirements for calling this function is parameter account ('where to delete')
    and parameter type ('what to delete'). May have parameter file to delete the file itself
    and/or validation data for it.
    
    Returns <result>OK</result>
    or      <error>...</error>
    
    Errors could be:
    1. Parameter error. (missing, too many, non-existent account, unsupported type)
       If parameter file is used and it does not exist then no error is returned
    2. Permissions error on account
    3. Any unexpected error is caught and returned as-is
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace adxfiles    = "http://art-decor.org/ns/art-decor/xis/files" at "../api/api-xis-files.xqm";

declare namespace request       = "http://exist-db.org/xquery/request";
declare namespace response      = "http://exist-db.org/xquery/response"; 
declare namespace xis           = "http://art-decor.org/ns/xis";
declare option exist:serialize "indent=no";
declare option exist:serialize "omit-xml-declaration=no";

let $params                     := if (request:exists()) then request:get-data()/* else (
<delete-type>
    <file/>
    <xpath/>
    <rootelement>PORX_IN932000NL</rootelement>
    <!--<beforedate>2018-02-24</beforedate>-->
    <type>all</type>
</delete-type>
)
let $param-account              := if (request:exists()) then request:get-parameter('account',()) else ('art-decor')
let $param-file                 := $params/file[string-length()>0]
let $param-xpath                := $params/xpath[string-length()>0]
let $param-rootelement          := if (empty($param-file)) then $params/rootelement[string-length()>0] else ()
let $param-beforedate           := if (empty($param-file)) then $params/beforedate[. castable as xs:date]/xs:date(.) else ()
let $param-type                 := $params/type
(: e.g. /db/apps/hl7/mp-test-20141101T114522 :)
let $param-base                 := if (request:exists()) then request:get-parameter('base',()) else ()

let $account                    := concat($get:strXisAccounts,'/', $param-account[.=$adxfiles:_existing_accounts])

let $result := 
    if ($param-account[string-length()=0] or count($param-account)>1) then
    (
        let $error  := <error>ERROR: must have exactly one parameter 'account'.</error>
        let $g      := util:log('ERROR', concat('---------- xis: delete-data-for-account.xquery: ',$error))
        return ($error)
    )
    else if (string-length($param-account)=0) then
    (
        let $error  := <error>ERROR: account does not exist: "{$param-account}"</error>
        let $g      := util:log('ERROR', concat('---------- xis: delete-data-for-account.xquery: ',$error))
        return ($error)
    )
    else if (not(sm:has-access(xs:anyURI($account),'rw-'))) then
    (
        let $error  := <error>ERROR: account "{$param-account}" not writable.</error>
        let $g      := util:log('ERROR', concat('---------- xis: delete-data-for-account.xquery: ',$error))
        return ($error)
    )
    else if (string-length($param-type)=0) then
    (
        let $error  := <error>ERROR: must have at least one parameter 'type'.</error>
        let $g      := util:log('ERROR', concat('---------- xis: delete-data-for-account.xquery: ',$error))
        return ($error)
    )
    else if ($param-type[not(.=$adxfiles:_delete-types)]) then
    (
        let $error  := <error>ERROR: unsupported value for parameter 'type': "{$param-type}"</error>
        let $g      := util:log('ERROR', concat('---------- xis: delete-data-for-account.xquery: ',$error))
        return ($error)
    )
    else
    (
        adxfiles:deleteAccountData($param-account, $param-type, $param-file, $param-xpath, $param-base, $param-rootelement, $param-beforedate)
    )
    
return $result