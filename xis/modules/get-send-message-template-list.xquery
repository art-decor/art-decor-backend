xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace adxaccounts = "http://art-decor.org/ns/art-decor/xis/accounts" at "../api/api-xis-accounts.xqm";

declare namespace hl7  = "urn:hl7-org:v3";
declare namespace xs   = "http://www.w3.org/2001/XMLSchema";
declare namespace soap = "http://schemas.xmlsoap.org/wsdl/soap/";
declare namespace wsdl = "http://schemas.xmlsoap.org/wsdl/";
declare namespace xis  = "http://art-decor.org/ns/xis";

let $account            := if (request:exists()) then request:get-parameter('account','') else ()
let $resources          := if (request:exists()) then request:get-parameter('resources',())[string-length()>0] else ()
(:let $account           := 'art-decor':)
let $resourcesPath      := if (empty($resources)) then adxaccounts:getDefaultXmlResourcesPath($account) else $resources
let $schemasPath        := concat($resourcesPath,'/schemas_codeGen_flat')
let $messageTemplates   := collection(concat($resourcesPath,'/message-templates'))/*
let $interactionFile    := doc(concat($get:strXisResources, '/vocab/2.16.840.1.113883.1.6.xml'))

return
<messageTemplates account="{$account}" resourcesPath="{$resourcesPath}">
{
    for $template in $messageTemplates
    let $authorType     :=
        if (not(exists($template/hl7:*[lower-case(local-name())='controlactprocess']))) then
            'none'
        else if (exists($template/hl7:*[lower-case(local-name())='controlactprocess']/hl7:authorOrPerformer//hl7:*[lower-case(local-name())='assigneddevice'])) then
            'device'
        else (
            'person'
        )
    let $interactionId  := $template/local-name()
    let $messageName    := $interactionFile//*[@code=$interactionId]/@displayName
    let $operations     := collection($resourcesPath)//wsdl:operation[wsdl:input/@message=concat('hl7:',$interactionId)]
    
    let $messageElement := collection($schemasPath)//xs:schema/xs:element[@name=$template/local-name()]
    let $messageSchema  := $messageElement/ancestor::xs:schema
    let $messageType    := $messageElement/@type
    let $controlAct     := 
        if (exists($messageSchema//xs:complexType[@name=$messageType]//xs:group[@ref='ControlAct'])) then (
            $messageSchema//xs:group[@name='ControlAct']//xs:element[@name='ControlActProcess']/@type
        ) else 
        if (exists($messageSchema//xs:complexType[@name=$messageType]//xs:element[@name='ControlActProcess'])) then (
            $messageSchema//xs:complexType[@name=$messageType]//xs:element[@name='ControlActProcess']/@type
        ) else ()
    let $subject        := $messageSchema//xs:complexType[@name=$controlAct]//xs:element[@name='subject']/@type
    let $payloadT       := $messageSchema//xs:group[@ref='Payload'][ancestor::xs:complexType/@name=$subject]
    let $payload        := 
        if ($payloadT) then ($payloadT//xs:element/@name) else (
            let $payloadT   := $messageSchema//xs:complexType[@name=$subject]//xs:element
            return
            if ($payloadT[@name]) then ($payloadT/@name) else
            if ($payloadT[@ref]) then ($payloadT/@ref) else (
                let $payloadT   := $messageSchema//xs:element[@name='queryByParameter'][ancestor::xs:complexType/@name=$controlAct]
                return
                if ($payloadT) then ($payloadT/@name) else ()
            )
        )
    order by lower-case($template)
    return
    if ($operations) then 
        <messageTemplate interactionId="{$template/local-name()}" name="{$messageName}" authortype="{$authorType}" payload="{$payload}">
        {
            for $operation in $operations
            (:let $soapAction       := collection($resourcesPath)//wsdl:binding/wsdl:operation[@name = $operation/@name]/soap:operation/@soapAction
            let $wsdlBindingName  := collection($resourcesPath)//wsdl:binding[wsdl:operation[@name=$operation/@name]]/@name/string():)
            let $wsdlOperation      := collection($resourcesPath)//wsdl:operation[@name = $operation/@name]
            let $soapAction         := $wsdlOperation/soap:operation/@soapAction
            let $wsdlBindingName    := $wsdlOperation/parent::wsdl:binding/@name/string()
            (: at this point there should only one endpoint, but who knows... throw away URI scheme, hostname and port so we are only left with the path in the URI :)
            let $endpoint         := replace(string-join(collection($resourcesPath)//wsdl:service/wsdl:port[ends-with(@binding,$wsdlBindingName)]/soap:address/@location/string(),' '),'https?://?[^/]+','')
            return
            <operation name="{$operation/@name}" soapAction="{$soapAction[1]}" endpoint="{tokenize($endpoint,' ')[1]}" endpoints="{$endpoint}">
            <!-- @endpoint contains the chosen endpoint, while @endpoints contains the full list (batch, non batched etc.) -->
            <input message="{$operation/wsdl:input/@message}"/>
            <output message="{$operation/wsdl:output/@message}"/>
            </operation>
            ,
            for $messagePayload in collection($resourcesPath)//*[hl7:interactionId/@extension=$template/local-name()][hl7:ControlActProcess/hl7:subject]
            order by $messagePayload
            return
            <payload file="{xmldb:decode(util:document-name($messagePayload))}" fullPath="{concat(util:collection-name($messagePayload),'/',xmldb:decode(util:document-name($messagePayload)))}"/>
        }
        </messageTemplate>
    else ()
}
</messageTemplates>