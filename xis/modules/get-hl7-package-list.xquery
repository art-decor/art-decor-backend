xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace adxaccounts = "http://art-decor.org/ns/art-decor/xis/accounts" at "../api/api-xis-accounts.xqm";

declare namespace xmldb         = "http://exist-db.org/xquery/xmldb";
declare namespace expath        = "http://expath.org/ns/pkg";

let $account            := if (request:exists()) then request:get-parameter('account',()) else ('art-decor')

let $packages           := 
    if (string-length($account)=0) then
        xmldb:get-child-collections($get:strHl7)
    else (
        adxaccounts:getXmlResourcesPaths($account)
    )

return
<list>
{
    for $package in $packages
    let $packageName            := if (string-length($account)=0) then $package else tokenize($package,'/')[last()]
    let $packagePath            := if (string-length($account)=0) then concat($get:strHl7,'/',$packageName) else $package
    let $packageDisplayName     := try { collection(concat($get:strHl7,'/',$packageName))/expath:package/expath:title } catch * {()}
    let $packageDisplayName     := if (string-length($packageDisplayName)=0) then $packageName else $packageDisplayName
    order by lower-case($packageName)
    return
    <version name="{$packageName}" displayName="{$packageDisplayName}" uriString="{$packagePath}">
    {
        adxaccounts:getViewersByPackage($packageName)/viewer
    }
    </version>
}
</list>