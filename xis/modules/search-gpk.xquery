xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";

let $searchString    := util:unescape-uri(request:get-parameter('string',('sys bloo')),'UTF-8')
(:let $searchString :='salb':)
let $searchTerms     := tokenize($searchString,'\s')
(:let $searchType := request:get-parameter('type',('wildcard')):)
let $searchType      := 'wildcard'

let $validSearch     := 
    if (matches($searchString,'^[a-z|0-9]') and string-length($searchString)>2 and string-length($searchString)<40) then
        xs:boolean('true')
    else if (matches($searchString,'^[A-Z]') and string-length($searchString)>1 and string-length($searchString)<40) then
        xs:boolean('true')
    else(
        xs:boolean('false')
    )
(:let $searchTerms := tokenize('ast','\s'):)
let $maxResults := xs:integer('50')
let $options := 
    <options>
        <filter-rewrite>yes</filter-rewrite>
    </options>
let $query := 
    <query>
        <bool>
        {
            for $term in $searchTerms
            return
                if ($searchType='fuzzy') then
                    <fuzzy occur="must">{concat($term,'~')}</fuzzy>
                else if ($searchType!='fuzzy' and matches($term,'^[a-z|0-9]')) then
                    <wildcard occur="must">{concat($term,'*')}</wildcard>
                else if ($searchType!='fuzzy' and matches($term,'^[A-Z]')) then
                    <term occur="must">{lower-case($term)}</term>
                else()
        }
        </bool>
    </query>

let $result  :=
    if ($validSearch) then
        if (matches($searchString,'^\d+$')) then
            collection($get:strXisHelperConfig)//product[@gpkode=$searchString] |
            collection($get:strXisHelperConfig)//product[prk/@prkode=$searchString] |
            collection($get:strXisHelperConfig)//product[prk/hpk/@hpkode=$searchString] |
            collection($get:strXisHelperConfig)//product[prk/hpk/artikel/@atkode=$searchString]
        else (
            collection($get:strXisHelperConfig)//product[@atcode=$searchString] |
            collection($get:strXisHelperConfig)//product[ft:query(naam,$query,$options)] |
            collection($get:strXisHelperConfig)//product[ft:query(prk/hpk/naam,$query,$options)]
        )
    else(<result current="0" count="0"/>)
let $count   := count($result)
let $current := 
    if ($count>$maxResults) then
        $maxResults
    else($count)
let $concepts := 
    for $concept in $result
    order by $concept/naam/volledig
    return
        $concept
return
<result current="{$current}" count="{$count}">
{
    subsequence($concepts,1,$maxResults)
}
</result>