xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at  "../../art/modules/art-decor.xqm";
import module namespace adxfiles    = "http://art-decor.org/ns/art-decor/xis/files" at "../api/api-xis-files.xqm";
import module namespace val         = "http://art-decor.org/ns/art-decor/xis/validation" at "../api/api-xis-validation.xqm";

declare namespace xmldb     = "http://exist-db.org/xquery/xmldb";
declare namespace sm        = "http://exist-db.org/xquery/securitymanager";
declare namespace xis       = "http://art-decor.org/ns/xis";

(: server path:)
let $account            := if (request:exists()) then request:get-parameter('account',()) else 'test-test'
let $user               := get:strCurrentUserName()

(: store which testAccount was last selected, so messages and test screen can show this at startup :)
let $result             := 
    if ($user=sm:get-group-members('xis')) then (
        let $userAllAccounts    := doc($get:strTestAccounts)//xis:testAccount/xis:members/xis:user[@id=$user]
        let $userThisAccount    := doc($get:strTestAccounts)//xis:testAccount[@name=$account]/xis:members/xis:user[@id=$user]
        
        return (
            update delete $userAllAccounts/@lastSelected,
            update insert attribute lastSelected {'true'} into $userThisAccount
        )
    ) else ()

let $tests              :=
   if ($account=sm:get-user-groups($user) or $account='art-decor') then
        collection(concat($get:strXisAccounts, '/',$account))//xis:tests
   else(<nope/>)

return
<xis:tests account="{$account}">
{
    for $testsuite in $tests/xis:test
    let $basetestsuite      := val:getTestSuiteById($testsuite/@testsuiteId)
    (: Get XMLresourcesPath from testsuite:)
    let $resources          := val:getTestSuiteXmlResourcesById($testsuite/@testsuiteId)
    return
    <xis:test>
    {
        $testsuite/@*,
        if ($testsuite[empty(@statusCode)]) then attribute statusCode {} else (),
        if ($testsuite[empty(@approvedBy)]) then attribute approvedBy {} else (),
        if ($testsuite[empty(@approvalDate)]) then attribute approvalDate {} else (),
        if ($basetestsuite) then (
            for $name in $basetestsuite/name
            return
                art:serializeNode($name)
        ) else (<name>Unknown (id="{data($testsuite/@testsuiteId)}")</name>)
        ,
        $basetestsuite/application-role,
        $basetestsuite/xmlResourcesPath,
        for $desc in $basetestsuite/desc
        return
            art:serializeNode($desc)
        ,
        for $requirement in $testsuite/xis:requirement
        return $requirement
        ,
        for $test in $testsuite/xis:test
        return
        <xis:test>
        {
            $test/@*,
            $basetestsuite/test[@schematron = $test/@ref]/name,
            for $desc in $basetestsuite/test[@schematron = $test/@ref]/desc[.//text()[string-length(normalize-space()) gt 0]]
            return
                art:serializeNode($desc)
            ,
            for $validation in $test/xis:validation
            let $file               := $validation/@messageFile
            let $xpath              := $validation/@messageXpath
            let $revalidated        := 
                if (empty($resources)) then () else ((:val:revalidate($account, $file, $xpath, $resources):))
            let $report             := 
                if (empty($resources)) then () else if (string-length($file)=0) then () else adxfiles:getReports($account, $file, $xpath, $resources)
            (:let $creationDate       :=
                if ($validation[@created]) then () else (
                    let $f          := try {adxfiles:getFile($account, $validation/@messageFile)} catch * {()}
                    return update insert $f/ancestor::file/@created into $validation
                ):)
            return
            <xis:validation>
            {
                $validation/@*,
                if ($validation[empty(@approvalStatus)]) then attribute approvalStatus {} else (),
                if ($validation[empty(@approvedBy)]) then attribute approvedBy {} else (),
                if ($validation[empty(@approvalDate)]) then attribute approvalDate {} else (),
                if ($validation[empty(@statusCode)]) then attribute statusCode {} else (),
                if ($validation[empty(@messageXpath)]) then attribute messageXpath {} else (),
                if ($validation[empty(@messageName)]) then attribute messageName {} else (),
                if ($validation[empty(@messageRoot)]) then attribute messageRoot {} else (),
                if ($validation[empty(@created)]) then attribute created {} else (),
                if ($validation[empty(@patient)]) then attribute patient {} else (),
                $validation/(* except coreValidation),
                if (string-length($file)=0) then () else (
                    if ($resources) then (
                        <coreValidation>
                        {
                            if ($report) then ($report) else (
                                <validationReport validationBase="{$resources}">
                                    <error type="schema">
                                        <issue type="schema" role="error" count="1">
                                            <description>Unexpected validation error: no report generated by validate-message. 
                                            {
                                                if (string-length($file)=0) then ('Unknown file.') else ()
                                            }
                                            </description>
                                        </issue>
                                    </error>
                                </validationReport>
                            )
                        }
                        </coreValidation>
                    ) else (
                        <coreValidation>
                            <validationReport validationBase="{$resources}">
                                <error type="schema">
                                    <issue type="schema" role="error" count="1">
                                        <description>Unexpected validation error: no report generated by validate-message. Resources cannot be determined. Maybe the testsuite with id="{data($testsuite/@testsuiteId)}" was deleted?</description>
                                    </issue>
                                </error>
                            </validationReport>
                        </coreValidation>
                    )
                )
            }
            </xis:validation>
        }
        </xis:test>
    }
    </xis:test>
}
</xis:tests>