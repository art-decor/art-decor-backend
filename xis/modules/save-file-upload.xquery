xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace adxfiles    = "http://art-decor.org/ns/art-decor/xis/files" at "../api/api-xis-files.xqm";

declare namespace request = "http://exist-db.org/xquery/request";

(:<content xsi:type="xs:base64Binary" mediatype="" filename="" size=""/>:)
let $account            := if (request:exists()) then (request:get-parameter('account',())[string-length()>0]) else ('art-decor')
let $filecontent        := if (request:exists()) then (request:get-data()/content) else ()

let $filename           := adxfiles:cleanupFileName($filecontent/@filename)
let $mediatype          := $filecontent/@mediatype
let $size               := $filecontent/@size

let $return             :=
    if (empty($account)) then 
        error(QName('http://art-decor.org/ns/error', 'MissingParameter'), 'Missing required parameter account')
    else
    if ($filecontent) then 
        if (empty($filename)) then 
            error(QName('http://art-decor.org/ns/error', 'MissingParameter'), 'HTTP Body <content/> is missing required data item @filename.')
        else 
        if ($mediatype = $adxfiles:acceptMediaType) then () else ( 
            error(QName('http://art-decor.org/ns/error', 'UnsupportedFileType'), concat('File must be an XML file with media type ',string-join($adxfiles:acceptMediaType,', '),'. Found ',$mediatype))
        )
    else (
        error(QName('http://art-decor.org/ns/error', 'MissingParameter'), 'Missing required content in HTTP Body. Expected: <content xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" mediatype="text/xml | application/xml" filename="filename.xml" size="23332"><data xsi:type="xs:base64Binary">Base64 contents</data></content>')
    )

return
<data-safe>{adxfiles:saveFile($account, replace($filename, '\s', '_'), xs:base64Binary($filecontent))}</data-safe>