xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at  "../../art/modules/art-decor.xqm";
import module namespace val         = "http://art-decor.org/ns/art-decor/xis/validation" at "../api/api-xis-validation.xqm";

declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace xis       = "http://art-decor.org/ns/xis";

declare %private function local:addTestSuiteToTest($testAccount as xs:string, $testsuiteId as xs:string) as xs:boolean {
    let $testsuite          := val:getTestSuiteById($testsuiteId)
    let $accountCollection  := concat($get:strXisAccounts,'/',$testAccount)
    let $testseries         := concat($accountCollection,'/testseries.xml')
    
    let $createSeries   := 
        if (doc-available($testseries)) then () else (
            xmldb:store($accountCollection, 'testseries.xml', <xis:tests xmlns:xis="http://art-decor.org/ns/xis"/>)
        )
    
    let $tests              := collection($accountCollection)//xis:tests
    let $maxId              := if ($tests//xis:validation/@id) then max($tests//xis:validation/@id) else (0)
              
    let $newTest            :=
        <xis:test testsuiteId="{$testsuiteId}" statusCode="">
        {
            for $requirement in $testsuite/requirement
            return
            <xis:requirement approvalStatus="" approvedBy="" approvalDate="">{$requirement/@*}</xis:requirement>
            ,
            for $test at $position in $testsuite/test
            return
            <xis:test ref="{$test/@schematron}">
                <xis:validation id="{$maxId+$position}" dateTime="" messageFile="" statusCode="" approvalStatus="" approvedBy="" approvalDate=""/>
            </xis:test>
        }
        </xis:test>
    (: don't add existing testsuites again :)
    let $update :=  if (doc($testseries)//xis:test[@testsuiteId=$testsuiteId])
                    then ()
                    else update insert $newTest into $tests
    return true()
};

let $testAccount        := request:get-parameter('account',())
let $testsuiteId        := request:get-parameter('id',())
(:
let $testAccount        := 'art-decor'
let $testsuiteId        := '1':)

let $user               := get:strCurrentUserName()
let $update             :=
    if ($testAccount=sm:get-user-groups($user) or $testAccount='art-decor') then
        local:addTestSuiteToTest($testAccount, $testsuiteId)
    else (
        false()
    )

return
    <response>{$update}</response>