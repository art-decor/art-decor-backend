xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

let $nl := "&#10;"
let $tab := "&#9;"

let $lang       := 'nl-NL'
let $version    := '2014-04-16T16:02:11'
let $id         := '2.16.840.1.113883.2.4.3.11.60.90.77.4.2404'
let $uri        := concat('http://decor.nictiz.nl/decor/services/RetrieveTransaction?id=', $id, '&amp;language=', $lang, '&amp;version=', $version, '&amp;format=xml')
let $transaction := doc($uri)

let $tests :=
<testset name="" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="https://assets.art-decor.org/ADAR/rv/DECOR-tests.xsd">
    <release uri="{$uri}"/>
    {
    if ($transaction = () ) then <error>no xpaths found</error> else 
        <test name="{concat($transaction/dataset/@shortName, '_test')}" transactionRef="{$transaction/dataset/@transactionId}">
            <name language="nl-NL">?</name>
            <desc language="nl-NL">?</desc>
            <suppliedConcepts>{$nl, comment{'Add @context if context is not root.'}, $nl}
            <!-- Restrictions on multiplicities -->
            {(for $concept in $transaction//concept[implementation/@xpath] return                 
                <concept multiplicity="1" ref="{$concept/@id}">{concat($concept/name[1], ' moet 1 keer voorkomen.')}</concept>
            ),
            <!-- Restrictions on values -->,
            (for $concept in $transaction//concept[implementation/@xpath][@type='item'] return                 
                <concept occurrence="1" assert="{$concept/implementation/@valueLocation}='?'" ref="{$concept/@id}">{concat($concept/name[1], ' moet "?" zijn.')}</concept>
            )}
            </suppliedConcepts>
        </test>
}</testset>

return $tests