xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
declare namespace xis = "http://art-decor.org/ns/xis";

let $account            := if (request:exists()) then request:get-parameter('account',())[string-length()>0] else ()

(: store which testAccount was last selected, so messages and test screen can show this at startup :)
let $user               := get:strCurrentUserName()
let $check              := sm:has-access(xs:anyURI($get:strTestAccounts),'rw-')
let $result             := 
    if ($account and $check) then (
        let $userAccounts       := doc($get:strTestAccounts)
        let $userAllAccounts    := $userAccounts//xis:testAccount/xis:members/xis:user[@id=$user]
        let $userThisAccount    := $userAccounts//xis:testAccount[@name=$account]/xis:members/xis:user[@id=$user]
        
        let $update             := 
            if (empty($userThisAccount)) then () else (
                update delete $userAllAccounts/@lastSelected,
                update insert attribute lastSelected {'true'} into $userThisAccount
            )
        return ()
    ) else ()
    
return
    <result user="{$user}" account="{$account}"/>