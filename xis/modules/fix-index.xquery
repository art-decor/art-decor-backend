xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adxfiles    = "http://art-decor.org/ns/art-decor/xis/files" at "../api/api-xis-files.xqm";
import module namespace adpfix      = "http://art-decor.org/ns/xis-permissions" at "../api/api-permissions.xqm";
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";

declare namespace xis   = "http://art-decor.org/ns/xis";

let $accounts           := if (request:exists()) then request:get-parameter('account',()) else ()

let $reindexExisting    :=
    if ($accounts) then 
        for $account in $accounts
        return adxfiles:reindexAccount($account)/*
    else
        adxfiles:reindexAccounts()

let $reindexRemaining   :=
    if ($accounts) then 
        for $account in $accounts
        return adxfiles:indexAccount($account)/*
    else
        adxfiles:indexAccounts()
        
return
    <result>
    {
        $reindexExisting
    }
    </result>