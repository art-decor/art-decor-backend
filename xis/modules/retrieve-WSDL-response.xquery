xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";

declare namespace wsdlsoap   = "http://schemas.xmlsoap.org/wsdl/soap/";
declare namespace wsdl       = "http://schemas.xmlsoap.org/wsdl/";
declare option exist:serialize "method=xml media-type=text/xml omit-xml-declaration=no";

let $get-uri          := request:get-uri()
let $get-url          := request:get-url()
(:by using service, we would only get the final apart of the URI thereby skipping any version in versioned URI:)
(:let $soapService      := request:get-parameter('service',''):)
let $soapService      := replace($get-url,'.*/xis/','/')
(:let $format           := if (string-length(request:get-parameter('format',''))>0) then (request:get-parameter('format','')) else ('xml'):) 

(: Consider rewrite of file so the address URI actually matches the actual service URI :)
let $return           := (collection($get:strHl7)//wsdlsoap:address[ends-with(string(@location),$soapService)]/ancestor::wsdl:definitions)[1]

(: /db/apps/hl7/AORTA_v61000/wsdl/VerstrekkingsLijstquery.wsdl :)
let $reallocation           := concat(util:collection-name($return), '/', util:document-name($return))

(: /xis/hl7/AORTA_v61000/wsdl/VerstrekkingsLijstquery.wsdl :)
let $rewrite                := concat('/xis/', substring-after($reallocation, 'db/apps/'))

return (
    (:comment {$location}, $return:)
    response:redirect-to($rewrite)
)