xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at  "../../art/modules/art-decor.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../art/api/api-server-settings.xqm";

declare namespace xis = "http://art-decor.org/ns/xis";

(: server path:)
let $testAccount  := request:get-parameter('account','')
let $validationId := request:get-parameter('id','')
let $testresult   := collection(concat($get:strXisAccounts, '/',$testAccount))//xis:validation[@id=$validationId]

return
$testresult