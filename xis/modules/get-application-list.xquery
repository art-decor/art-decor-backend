xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";

declare namespace xis="http://art-decor.org/ns/xis";
(:let $account := 'art-decor':)
let $account := request:get-parameter('account','')
let $applications := doc($get:strTestAccounts)//xis:testAccount[@name=$account]/xis:application
return
<applications>
{
for $application in $applications
return
<application id="{$application/@id}" name="{$application/@name}" url="{$application/@url}" organizationRegisterId="{$application/@organizationRegisterId}"/>
}
</applications>