xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace adxfiles    = "http://art-decor.org/ns/art-decor/xis/files" at "../api/api-xis-files.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../art/api/api-server-settings.xqm";
import module namespace adxaccounts = "http://art-decor.org/ns/art-decor/xis/accounts" at "../api/api-xis-accounts.xqm";

declare namespace xis        = "http://art-decor.org/ns/xis";
declare namespace hl7        = "urn:hl7-org:v3";
declare namespace soap       = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace wsdlsoap   = "http://schemas.xmlsoap.org/wsdl/soap/";
declare namespace wsdl       = "http://schemas.xmlsoap.org/wsdl/";
declare namespace datetime   = "http://exist-db.org/xquery/datetime";
declare option exist:serialize "method=xml media-type=text/xml omit-xml-declaration=no";

(: Log debug messages? :)
declare variable $debug       := false();

declare %private function local:loopMessage($matchingMessages as element(), $message as element()) {
   (: get next /message :)
   let $followingNode   := <messageFilter>{$matchingMessages/message[position()>1]}</messageFilter>
   
   (: what is the current message filter we are processing :)
   let $thisMessage     := $matchingMessages/message[1]
   let $parameters      := <queryParameters>{$thisMessage//queryParameters/parameter}</queryParameters>
   let $checkParameters := local:returnNode($parameters,$matchingMessages,$message)

   return
   (: if returnNode does return false, there is no match yet :)
   (: continue with next /message filter :)
      if (matches($checkParameters,'false')) then
         (: if there is a following message :)
         if ($followingNode/node()) then
            local:loopMessage($followingNode,$message)
         else
            ()
      else 
         (: all queryParameters in the /message filter match, so return responseTemplateFile :)
         ($checkParameters)
};

declare %private function local:returnNode($node as element(),$matchingMessage as element(),$message as element()) {
   (: $node = parameter from messageFilter_manifest :)
   (: <parameter name="patientId" value="555555112">//*:queryByParameter/*:patientId/*:value[@root='2.16.840.1.113883.2.4.6.3']/@extension</parameter> :)
   let $followingNode := <queryParameters>{$node/parameter[position()>1]}</queryParameters>
   
   (: filter the incoming message by XPATH to the patientId, or other node we want to match with a messageFilter parameter value (configured in /hl7/../message-templates/messageFilter_manifest.xml)
      if the filter parameter = <parameter name="patientId" value="100040007">//*:queryByParameter/*:patientId/*:value[@root='2.16.840.1.113883.2.4.6.3']/@extension</parameter>
      and the incoming SOAP message has queryByParameter/patientId/value/@extension="555555112"
      the outcome is 555555112 (the patient identifier)
   :)
   let $g := if ($debug) then (util:log('DEBUG', '======SOAP-response.xquery====== string($node/parameter[1]): ')) else ()
   let $g := if ($debug) then (util:log('DEBUG', string($node/parameter[1]))) else ()
   let $message_xpath := util:eval(concat('$message',string($node/parameter[1])))
   let $g := if ($debug) then (util:log('DEBUG', '======SOAP-response.xquery====== $message_xpath: ')) else ()
   let $g := if ($debug) then (util:log('DEBUG', $message_xpath)) else ()
   return
   (: check if incoming node (patientId, ..) matches the parameter/@value from the filter parameter :)
   if (matches($message_xpath,$node//parameter[1]/@value)) then
            (: if there is a following node, see if that parameter matches also :)
            if ($followingNode//parameter) then local:returnNode($followingNode,$matchingMessage,$message)
            else
               <value>{($matchingMessage//responseTemplateFile/@value)[1]}</value>
   (: else: false :)
   else ('false')
};

(:
    Universal SOAP response stub
    Known issue: for services that define an <InputInteraction>Response element, this setup will fail, 
    for one because we can't compute which is the accept and which the reject message. Need special setup....
:)
let $login            := adserver:login('xis-webservice','/db')

let $g := if ($debug) then (util:log('DEBUG', '======SOAP-response.xquery====== start of script')) else ()
let $soapRequest      :=  
    try { adxfiles:getRequestBody() } 
    catch * {
        <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
            <soap:Body>
                <soap:Fault>
                    <faultcode>soap:Client</faultcode>
                    <faultstring>Incoming message could not be processed</faultstring>
                    <faultactor>http://art-decor.org/ns/xis/actor/zim</faultactor>
                    <detail>
                        <zim:text xmlns:zim="http://art-decor.org/ns/xis/actor/zim/soapFault/detail">Incoming message could not be processed. 
                            {QName($err:module,$err:code),' ',$err:description,' ', $err:value}</zim:text>
                    </detail>
                </soap:Fault>
            </soap:Body>
        </soap:Envelope>
    }
let $soapService      := request:get-parameter('service','')
let $g := if ($debug) then (util:log('DEBUG', concat('======SOAP-response.xquery====== $soapService: ',$soapService))) else ()
return 
if (empty($soapRequest)) then (
    response:set-status-code(500), response:set-header('Content-Type','text/xml; charset=utf-8'), 
        <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
            <soap:Body>
                <soap:Fault>
                    <faultcode>soap:Client</faultcode>
                    <faultstring>Service {$soapService} requires soap:Envelope</faultstring>
                    <faultactor>http://art-decor.org/ns/xis/actor/zim</faultactor>
                    <detail>
                        <zim:text xmlns:zim="http://art-decor.org/ns/xis/actor/zim/soapFault/detail">Service {$soapService} requires soap:Envelope. Please update the contents of your HTTP request.</zim:text>
                    </detail>
                </soap:Fault>
            </soap:Body>
        </soap:Envelope>
) else if ($soapRequest//soap:Fault) then (
    response:set-status-code(500), response:set-header('Content-Type','text/xml; charset=utf-8'),
        $soapRequest
) else (
    (: this parameter, if false, will lead to an HTTP 500 SOAP:Fault that says unsupported service. This parameter may be set through the controller.xql :)
    let $supportedService   := request:get-parameter('supported','true')
    let $soapAction         := request:get-header((request:get-header-names()[lower-case(.)='soapaction'])[1])
    let $g := if ($debug) then (util:log('DEBUG', concat('======SOAP-response.xquery====== $soapAction: ',$soapAction))) else ()
    (:SOAPAction is optionally wrapped in single or double quotes:)
    let $soapAction         := replace(replace($soapAction,'^["'']',''),'["'']$','')
    let $message            := $soapRequest/soap:Body/*
    let $rootElement        := if ($message[self::hl7:*]) then $message[self::hl7:*]/local-name() else ()
    let $g := if ($debug) then (util:log('DEBUG', concat('======SOAP-response.xquery====== $rootElement: ',$rootElement))) else ()
    let $interactionId      := ($soapRequest/soap:Body/hl7:*/hl7:interactionId[@root='2.16.840.1.113883.1.6'])[1]/@extension/string()
    let $g := if ($debug) then (util:log('DEBUG', concat('======SOAP-response.xquery====== $interactionId: ',$interactionId))) else ()
    
    (: get sender and receiver application ids to determine account for configuration and message storage:)
    let $senderId           := $soapRequest/soap:Body/hl7:*/hl7:sender/hl7:device/hl7:id[@root='2.16.840.1.113883.2.4.6.6']/@extension/string()
    let $receiverId         := $soapRequest/soap:Body/hl7:*/hl7:receiver/hl7:device/hl7:id[@root='2.16.840.1.113883.2.4.6.6']/@extension/string()
    let $g := if ($debug) then (util:log('DEBUG', concat('======SOAP-response.xquery====== $senderId: ',$senderId))) else ()
    let $g := if ($debug) then (util:log('DEBUG', concat('======SOAP-response.xquery====== $receiverId: ',$receiverId))) else ()
    
    (: find account, default account = art-decor :)
    let $account            := distinct-values(doc($get:strTestAccounts)//xis:application[@id=($senderId)]/parent::xis:testAccount/@name)
    let $accounterror       := $account
    let $account            :=  if (count($account)=1) then ($account) else ()
    let $g := if ($debug) then (util:log('DEBUG', concat('======SOAP-response.xquery====== $account: "',$account,'"'))) else ()
    let $g := if ($debug and empty($account)) then (util:log('DEBUG', concat('======SOAP-response.xquery====== $accounterror: "',$accounterror,'"'))) else ()
    
    let $config             := doc($get:strTestAccounts)//xis:testAccount[@name=$account]/xis:xis
    let $defaultResources   := adxaccounts:getDefaultXmlResourcesPath($account)
    let $otherResources     := adxaccounts:getXmlResourcesPaths($account)[not(.=$defaultResources)]
    let $g := if ($debug) then (util:log('DEBUG', '======SOAP-response.xquery====== $config: ')) else ()
    let $g := if ($debug) then (util:log('DEBUG', $config)) else ()
    let $g := if ($debug) then (util:log('DEBUG', concat('======SOAP-response.xquery====== $defaultResources: ',$defaultResources))) else ()
    let $g := if ($debug) then (util:log('DEBUG', concat('======SOAP-response.xquery====== $otherResources: ',$otherResources))) else ()

    (: wsdl file -- note: this fails in case there is more than one match for a given SOAPAction ... :)
    let $wsdlContent        := 
        collection($defaultResources)/wsdl:definitions[.//wsdlsoap:address[ends-with(@location/string(),$soapService)]][.//wsdl:operation/wsdl:input[@message/string()=concat('hl7:',$interactionId)]]
    let $wsdlContent        := 
        if ($wsdlContent) then ($wsdlContent)[1] else (
            for $path in $otherResources
            return
            collection($path)/wsdl:definitions[.//wsdlsoap:address[ends-with(@location/string(),$soapService)]][.//wsdl:operation/wsdl:input[@message/string()=concat('hl7:',$interactionId)]]
        )[1]
        
    let $resourcesPath      :=
        if ($wsdlContent) then (($defaultResources, $otherResources)[starts-with(util:collection-name($wsdlContent),.)])[1] else ($defaultResources)
    let $g := if ($debug) then (util:log('DEBUG', '======SOAP-response.xquery====== $wsdlContent: ')) else ()
    let $g := if ($debug) then (util:log('DEBUG', $wsdlContent)) else ()
    
    (: resolve SOAPAction into the correct operation name :)
    (:/wsdl:definitions/wsdl:binding[1]/wsdl:operation[1]/@name:)
    let $operationName      := $wsdlContent//wsdl:binding/wsdl:operation[wsdlsoap:operation/@soapAction=$soapAction]/@name/string()
    let $g := if ($debug) then (util:log('DEBUG', concat('======SOAP-response.xquery====== $operationName: ',$operationName))) else ()
    (: get list of valid input messages for this service based on SOAPAction :)
    let $inputInOperation   := count($wsdlContent//wsdl:portType/wsdl:operation[@name/string()=$operationName]/wsdl:input[@message=concat('hl7:',$rootElement)])>0
    let $g := if ($debug) then (util:log('DEBUG', concat('======SOAP-response.xquery====== $inputInOperation: ',$inputInOperation))) else ()
    
    (: get list of valid output messages for this service based on SOAPAction :)        
    let $outputMessage      := substring-after(($wsdlContent//wsdl:portType/wsdl:operation[@name/string()=$operationName][wsdl:input[@message=concat('hl7:',$rootElement)]]/wsdl:output/@message)[1],'hl7:')
    let $g := if ($debug) then (util:log('DEBUG', concat('======SOAP-response.xquery====== $outputMessage: ',$outputMessage))) else ()

    (: load an optional messageFilter :)
    let $messageFilterFile  := concat($resourcesPath,'/message-templates/messageFilter_manifest.xml')
    let $messageFilter      := if (doc-available($messageFilterFile)) then doc($messageFilterFile) else ()
    
    let $g := if ($debug) then (util:log('DEBUG', concat('======SOAP-response.xquery====== $messageFilter present?: ',string-length($messageFilter)>0))) else ()
    let $g := util:log-system-out(concat('======SOAP-response.xquery====== $messageFilter present?: ',string-length($messageFilter)>0))
    
    (: get message filters that are active and match the incoming rootElement :)    
    let $matchingMessages := 
        <messageFilter>
        {
            $messageFilter//message[@active="true"][@interactionId=$rootElement][@soapAction=$soapAction] |
            $messageFilter//message[@active="true"][@interactionId=$rootElement][empty(@soapAction)]
        }
        </messageFilter>
    (: test which message filter parameters match the payload of the incoming message :) 
    let $messageFilterOutput := 
        if ($messageFilter and $matchingMessages//message) then
           local:loopMessage($matchingMessages,$message)
        else ()
    let $g := if ($debug) then (util:log('DEBUG', concat('======SOAP-response.xquery====== $messageFilterOutput: ',$messageFilterOutput))) else ()

    (: file that contains the response template. pretty rudimentarily based on the output message in the wsdl :)        
    let $responseTemplateFile := 
        (: check if there is messageFilter output :)
           if ($messageFilterOutput/@value) then
               (: outputMessage is output from first filter that matches :)
               concat($resourcesPath,'/message-templates/',string($messageFilterOutput/@value))
           else
               (: if the response is a batch, look for a message template in this form: MCCI_IN200101_<input message>.xml :) 
               (: for instance: /hl7/AORTA_trunk/message-templates/MCCI_IN200101_QURX_IN990111NL.xml :)
               if ($outputMessage = 'MCCI_IN200101') then
                  concat($resourcesPath,'/message-templates/',$outputMessage,'_',$rootElement,'.xml')
               else concat($resourcesPath,'/message-templates/',$outputMessage,'.xml')

    (: respond if request is valid for this service:)
    let $responseGiven := false()
    let $response := 
        if ($supportedService!='true') then (
            response:set-status-code(500), response:set-header('Content-Type','text/xml; charset=utf-8'), 
            <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                <soap:Body>
                    <soap:Fault>
                        <faultcode>soap:Client</faultcode>
                        <faultstring>Service {$soapService} is not (yet) supported</faultstring>
                        <faultactor>http://art-decor.org/ns/xis/actor/zim</faultactor>
                        <detail>
                            <zim:text xmlns:zim="http://art-decor.org/ns/xis/actor/zim/soapFault/detail">Service {$soapService} is not (yet) supported. Please check with your administrator if this service is a /zim service rather than a /xis service or what a valid value for the parameter 'service' is.</zim:text>
                        </detail>
                    </soap:Fault>
                </soap:Body>
            </soap:Envelope>
        ) else if (empty($rootElement) or $rootElement = '') then (
            response:set-status-code(500), response:set-header('Content-Type','text/xml; charset=utf-8'), 
            <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                <soap:Body>
                    <soap:Fault>
                        <faultcode>soap:Client</faultcode>
                        <faultstring>The input document is not in the HL7 V3 namespace</faultstring>
                        <faultactor>http://art-decor.org/ns/xis/actor/zim</faultactor>
                        <detail>
                            <zim:text xmlns:zim="http://art-decor.org/ns/xis/actor/zim/soapFault/detail">There's no element in the HL7 V3 namespace ('urn:hl7-org:v3') under the SOAP:Body element</zim:text>
                        </detail>
                    </soap:Fault>
                </soap:Body>
            </soap:Envelope>
        ) else if (empty($interactionId) or $rootElement != $interactionId) then (
            response:set-status-code(500), response:set-header('Content-Type','text/xml; charset=utf-8'), 
            <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                <soap:Body>
                    <soap:Fault>
                        <faultcode>soap:Client</faultcode>
                        <faultstring>The input document root element and the interactionId do not match</faultstring>
                        <faultactor>http://art-decor.org/ns/xis/actor/zim</faultactor>
                        <detail>
                            <zim:text xmlns:zim="http://art-decor.org/ns/xis/actor/zim/soapFault/detail">The element name '{$rootElement}' under soap:Body does not match the value in interactionId[@root='2.16.840.1.113883.1.6']/@extension '{$interactionId}'</zim:text>
                        </detail>
                    </soap:Fault>
                </soap:Body>
            </soap:Envelope>
        ) else if (empty($account) and count($accounterror)=0) then (
            response:set-status-code(500), response:set-header('Content-Type','text/xml; charset=utf-8'), 
            <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                <soap:Body>
                    <soap:Fault>
                        <faultcode>soap:Client</faultcode>
                        <faultstring>There was no unique account where this message belongs</faultstring>
                        <faultactor>http://art-decor.org/ns/xis/actor/zim</faultactor>
                        <detail>
                            <zim:text xmlns:zim="http://art-decor.org/ns/xis/actor/zim/soapFault/detail">The sender id {$senderId} did not match an account to store the message in.</zim:text>
                        </detail>
                    </soap:Fault>
                </soap:Body>
            </soap:Envelope>
        ) else if (empty($account) and count($accounterror)>1) then (
            response:set-status-code(500), response:set-header('Content-Type','text/xml; charset=utf-8'), 
            <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                <soap:Body>
                    <soap:Fault>
                        <faultcode>soap:Client</faultcode>
                        <faultstring>There was no unique account where this message belongs</faultstring>
                        <faultactor>http://art-decor.org/ns/xis/actor/zim</faultactor>
                        <detail>
                            <zim:text xmlns:zim="http://art-decor.org/ns/xis/actor/zim/soapFault/detail">The sender id {$senderId} matched multiple accounts "{$accounterror}" to store the message in.</zim:text>
                        </detail>
                    </soap:Fault>
                </soap:Body>
            </soap:Envelope>
        ) else if (empty($wsdlContent)) then (
            response:set-status-code(500), response:set-header('Content-Type','text/xml; charset=utf-8'), 
            <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                <soap:Body>
                    <soap:Fault>
                        <faultcode>soap:Client</faultcode>
                        <faultstring>Service {$soapService} with interaction {$interactionId} is not supported in account '{$account}'</faultstring>
                        <faultactor>http://art-decor.org/ns/xis/actor/zim</faultactor>
                        <detail>
                            <zim:text xmlns:zim="http://art-decor.org/ns/xis/actor/zim/soapFault/detail">There's no WSDL for the service {$soapService} in the resources for the account '{$account}' [default value]. If this is not the expected account, please check the application ID in */sender/device/id[@root='2.16.840.1.113883.2.4.6.6']/@extension. If this is the correct account, please check the service URI and the interaction ID in */interactionId[@root='2.16.840.1.113883.1.6']/@extension.</zim:text>
                        </detail>
                    </soap:Fault>
                </soap:Body>
            </soap:Envelope>
        ) else if (empty($operationName) or $operationName = '') then (
            response:set-status-code(500), response:set-header('Content-Type','text/xml; charset=utf-8'), 
            <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                <soap:Body>
                    <soap:Fault>
                        <faultcode>soap:Client</faultcode>
                        <faultstring>There's no binding/operation with the SOAPAction {$soapAction} in the service {$soapService}</faultstring>
                        <faultactor>http://art-decor.org/ns/xis/actor/zim</faultactor>
                        <detail>
                            <zim:text xmlns:zim="http://art-decor.org/ns/xis/actor/zim/soapFault/detail">There's no binding/operation with the SOAPAction {$soapAction} in the service {$soapService}. Please check the SOAPAction.</zim:text>
                        </detail>
                    </soap:Fault>
                </soap:Body>
            </soap:Envelope>
        ) else if ($inputInOperation = false()) then (
            response:set-status-code(500), response:set-header('Content-Type','text/xml; charset=utf-8'), 
            <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                <soap:Body>
                    <soap:Fault>
                        <faultcode>soap:Client</faultcode>
                        <faultstring>The input document {$rootElement} does not match the operation with SOAPAction {$soapAction}</faultstring>
                        <faultactor>http://art-decor.org/ns/xis/actor/zim</faultactor>
                        <detail>
                            <zim:text xmlns:zim="http://art-decor.org/ns/xis/actor/zim/soapFault/detail">The input document {$rootElement} does not match the operation {$operationName} and SOAPAction {$soapAction}. Please check the combination of interaction and SOAPAction.</zim:text>
                        </detail>
                    </soap:Fault>
                 </soap:Body>
            </soap:Envelope>
        ) else if (not(doc-available($responseTemplateFile))) then (
            response:set-status-code(500), response:set-header('Content-Type','text/xml; charset=utf-8'), 
            <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                <soap:Body>
                    <soap:Fault>
                        <faultcode>soap:Server</faultcode>
                        <faultstring>Service {$soapService} has not yet been configured (correctly). Reply message template is missing: {$responseTemplateFile}</faultstring>
                        <faultactor>http://art-decor.org/ns/xis/actor/zim</faultactor>
                        <detail>
                            <zim:text xmlns:zim="http://art-decor.org/ns/xis/actor/zim/soapFault/detail">Service {$soapService} has not yet been configured (correctly). Reply message template is missing: {$responseTemplateFile}.</zim:text>
                        </detail>
                    </soap:Fault>
                </soap:Body>
            </soap:Envelope>
        ) else (
            let $applicationId      := $config/xis:applicationId
            let $messageIdLeaf      := 
                if ($applicationId castable as xs:integer) 
                then xs:integer($applicationId) 
                else (substring(string-join(for $i in string-to-codepoints($applicationId) return string($i),''),1,8))
            let $messageIdRoot      := concat('2.16.840.1.113883.2.4.6.6.',$messageIdLeaf,'.1')
            let $validationReport   := ''
            (:let $valid            := if ($validationReport//error) then false() else (true()):)
            let $valid              := true()
            
            return
                <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                    <soap:Body>
                    {
                        util:eval-inline($message, fn:serialize(doc($responseTemplateFile)/*,
                            <output:serialization-parameters xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization">
                                <output:method>xml</output:method>
                            </output:serialization-parameters>
                        ))
                    }
                    </soap:Body>
                </soap:Envelope>
        )
        
    let $responseStore := 
        if ($account) then (
            let $store-incoming     := adxfiles:saveFile($account, concat(util:uuid(),'.xml'), $soapRequest)
            let $store-response     := 
                if ($response) then (adxfiles:saveFile($account, concat(util:uuid(),'.xml'), $response)) else ()
            return ()
        ) else ()
    
    return
        $response
)