xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adxfiles    = "http://art-decor.org/ns/art-decor/xis/files" at "../api/api-xis-files.xqm";

(: Log debug messages? :)
let $debug              := true()

(: server path:)
let $account            := if (request:exists()) then request:get-parameter('account','') else ('')
let $searchData         := if (request:exists()) then request:get-data()/root else ()

let $param-beforedate   := $searchData/@beforedate[. castable as xs:date]/xs:date(.)
let $param-rootelement  := $searchData/@rootelement[string-length()>0]

let $offsetResults      := if ($searchData/@offset castable as xs:integer) then $searchData/xs:integer(@offset) else 1
let $maxResults         := if ($searchData/@count castable as xs:integer) then $searchData/xs:integer(@count) else $adxfiles:maxResults
let $sortBy             := $searchData/@sort
let $searchTerms        := tokenize(lower-case($searchData/search),'\s')

(:let $account          :='art-decor':)

let $g := if ($debug) then (util:log('DEBUG', concat('============ Supplied parameters (get-file-list): account=',$account,' sort=',$sortBy,' offset=',$offsetResults,' max=',$maxResults))) else ()

return
    adxfiles:getFileList($account, $sortBy, $offsetResults, $maxResults, $searchTerms, $param-beforedate, $param-rootelement)