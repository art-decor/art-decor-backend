xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace val         = "http://art-decor.org/ns/art-decor/xis/validation" at "../api/api-xis-validation.xqm";
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace adxfiles    = "http://art-decor.org/ns/art-decor/xis/files" at "../api/api-xis-files.xqm";

declare namespace request    = "http://exist-db.org/xquery/request";
declare namespace de         = "http://art-decor.org/ns/error";

let $params             := if (request:exists()) then request:get-data()/validateParams else (
        <validateParams>
            <account>aorta-nictiz</account>
            <file>59d5f86e-dc25-4a01-b708-fcf709248ba4.xml</file>
            <xpath>*[1]/*[1]/*[1]</xpath>
            <resources>/db/apps/hl7/mp-qual-20180426T140905</resources>
            <force>false</force>
        </validateParams>
    )
(: Test account string :)
let $account            := $params/account[string-length()>0][1]
(: file name :)
let $file               := $params/file[string-length()>0][1]
(: xpath to fragment :)
let $xpath              := $params/xpath[string-length()>0][1]
(: resources to validate fragment against :)
let $resources          := $params/resources[string-length()>0]
let $resources          := if (empty($resources)) then val:getResourcesByMessage($file, $account, $xpath, $resources) else $resources

(: force revalidation? :)
let $forceRevalidation  := $params/force[string-length()>0] = 'true'

let $accountPath        := concat($get:strXisAccounts, '/',$account)

return
<validationReports>
{
    for $resource in $resources
    (: Validate if not validated before and if $validationSwitchOff is not true, re-validate if validated with different validation resources :)
    let $revalidated        := val:revalidate($account, $file, $xpath, $resource, $forceRevalidation)
    let $report             := adxfiles:getReports($account, $file, $xpath, $resource)
    return
        if ($report) then $report else (
            <validationReport validationBase="{$resource}">
                <error type="schema">
                    <issue type="schema" role="error" count="1">
                        <description>Unexpected validation error: no report generated by validate-message</description>
                    </issue>
                </error>
            </validationReport>
        )
}
</validationReports>