<!--
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
-->
<xsl:stylesheet xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:hl7="urn:hl7-org:v3" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="#all" version="2.0">
    <!-- PURPOSE: removes anything that is not in the HL7 (urn:hl7-org:v3) namespace. This is useful for pre-processing CDA documents before 
        feeding them off to XML Schema validation. In CDA it is legal to have extra element in your own namespace, but the default XML Schema 
        doesn't like it
    -->
    <xsl:output indent="yes"/>
    <xsl:template match="/">
        <xsl:apply-templates select="hl7:*"/>
    </xsl:template>
    <xsl:template match="*[namespace-uri() != 'urn:hl7-org:v3']"/>
    <xsl:template match="text()|comment()|processing-instruction()">
        <xsl:copy-of select="."/>
    </xsl:template>
    <xsl:template match="hl7:*">
        <xsl:if test="not(@xsi:type) or namespace-uri-from-QName(resolve-QName(@xsi:type, .)) = 'urn:hl7-org:v3'">
            <xsl:copy>
                <xsl:copy-of select="@*[namespace-uri-for-prefix(substring-before(name(), ':'), ..) = ('urn:hl7-org:v3', 'http://www.w3.org/2001/XMLSchema-instance')]"/>
                <xsl:apply-templates select="hl7:* | text() | comment()"/>
            </xsl:copy>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>