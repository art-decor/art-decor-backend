<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" exclude-result-prefixes="#all" version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Apr 8, 2016</xd:p>
            <xd:p><xd:b>Author:</xd:b> ahenket</xd:p>
            <xd:p>Pre-processor step to eliminate long (usually Base64) lines</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="text()[parent::*[@xsi:type = 'xs:base64Binary'] | parent::*[@representation = 'B64'] | parent::*[@mediaType][not(starts-with(@mediaType, 'text'))]][string-length() &gt; 80]" as="xs:string">
        <xsl:value-of select="replace(normalize-space(.), '(.{80})', '$1&#10;')"/>
    </xsl:template>
    <xsl:template match="node()">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>
