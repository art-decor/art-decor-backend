<!--
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
--><!-- 
    DISCLAIMER
    This stylesheet and the output thereof are exclusive suited for testing purposes.
    They are NOT suited for any medical practise.
    
    Boxover javascript by http://boxover.swazz.org
    (BoxOver is free and distributed under the GNU license)
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:hl7="urn:hl7-org:v3" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="#all" version="2.0">
    <xsl:output method="html" indent="yes" version="4.01" encoding="UTF-8" doctype-public="-//W3C//DTD HTML 4.01//EN" doctype-system="http://www.w3.org/TR/html4/strict.dtd"/>
    <xd:doc>
        <xd:desc>
            <xd:p>Place to acquire coding systems, e.g. for interactions and trigger events.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:param name="vocabPath" select="'../vocab/'"/>
    <xd:doc>
        <xd:desc>
            <xd:p>XSLT 1.0 does not have date function, so we need something to compare against e.g. to get someone age</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:param name="currentDate" select="(hl7:creationTime/@value | hl7:effectiveTime/@value)[1]"/>
    <xd:doc>
        <xd:desc>
            <xd:p>Vocabulary file containing language dependant strings such as labels</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:param name="vocFile" select="'cda_l10n.xml'"/>
    <xd:doc>
        <xd:desc>
            <xd:p>Cache language dependant strings</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:variable name="keys" select="doc($vocFile)//translation" as="element()*"/>
    <xd:doc>
        <xd:desc>
            <xd:p>Default language for retrieval of language dependant strings such as labels, e.g. 'en-US'. This is the fallback language in case the string is not available in the actual language. See also <xd:ref name="textLang" type="parameter">textLang</xd:ref>.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:param name="textlangDefault" select="'en-US'"/>
    <xd:doc>
        <xd:desc>
            <xd:p>Actual language for retrieval of language dependant strings such as labels, e.g. 'en-US'. Unless supplied, this is taken from the ClinicalDocument/language/@code attribute, or in case that is not present from <xd:ref name="textlangDefault" type="parameter">textlangDefault</xd:ref>.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:param name="textLang">
        <xsl:choose>
            <xsl:when test="/hl7:ClinicalDocument/hl7:languageCode/@code">
                <xsl:value-of select="/hl7:ClinicalDocument/hl7:languageCode/@code"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>nl-NL</xsl:text>
                <!--<xsl:value-of select="$textlangDefault"/>-->
            </xsl:otherwise>
        </xsl:choose>
    </xsl:param>
    <xd:doc>
        <xd:desc>
            <xd:p>Do lowercase compare of language+region</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:variable name="textLangLowerCase">
        <xsl:call-template name="caseDown">
            <xsl:with-param name="data" select="$textLang"/>
        </xsl:call-template>
    </xsl:variable>
    <xd:doc>
        <xd:desc>
            <xd:p>Do lowercase compare of language (assume alpha2 not alpha3)</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:variable name="textLangPartLowerCase" select="substring($textLangLowerCase, 1, 2)"/>
    <xd:doc>
        <xd:desc>
            <xd:p>Do lowercase compare of default language+region</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:variable name="textLangDefaultLowerCase">
        <xsl:call-template name="caseDown">
            <xsl:with-param name="data" select="$textlangDefault"/>
        </xsl:call-template>
    </xsl:variable>
    <xd:doc>
        <xd:desc>
            <xd:p>Do lowercase compare of default language (assume alpha2 not alpha3)</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:variable name="textLangDefaultPartLowerCase" select="substring($textLangDefaultLowerCase, 1, 2)"/>
    <xd:doc>
        <xd:desc>Privacy parameter. Accepts a comma separated list of patient ID root values (normally OID's). When a patient ID is encountered with a root value in this list, then the rendering of the extension will be xxx-xxx-xxx regardless of what the actual value is. This is useful to prevent public display of for example the US SSN. Default is to render any ID as it occurs in the document. Note that this setting only affects human rendering and that it does not affect automated processing of the underlying document. If the same value also occurs in the <xd:ref name="skip-ids" type="parameter">skip-ids</xd:ref> list, then that takes precedence.</xd:desc>
    </xd:doc>
    <xsl:param name="skip-ids"/>
    <xsl:variable name="skip-ids-var" select="concat(',', $skip-ids, ',')"/>
    
    <xd:doc>
        <xd:desc>Privacy parameter. Accepts a comma separated list of patient ID root values (normally OID's). When a patient ID is encountered with a root value in this list, then the rendering of this ID will be skipped. This is useful to prevent public display of for example the US SSN. Default is to render any ID as it occurs in the document. Note that this setting only affects human rendering and that it does not affect automated processing of the underlying document.</xd:desc>
    </xd:doc>
    <xsl:param name="mask-ids"/>
    <xsl:variable name="mask-ids-var" select="concat(',', $mask-ids, ',')"/>
    
    <xd:doc>
        <xd:desc>
            <xd:p>String processing variable. Lower-case alphabet</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:variable name="lc" select="'abcdefghijklmnopqrstuvwxyz'" />
    
    <xd:doc>
        <xd:desc>
            <xd:p>String processing variable. Upper-case alphabet</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:variable name="uc" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
    
    <xd:doc>
        <xd:desc>
            <xd:p>String processing variable. Removes the following characters, in addition to line breaks "':;?`{}“”„‚’</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:variable name="simple-sanitizer-match"><xsl:text>&#10;&#13;&#34;&#39;&#58;&#59;&#63;&#96;&#123;&#125;&#8220;&#8221;&#8222;&#8218;&#8217;</xsl:text></xsl:variable>
    
    <xd:doc>
        <xd:desc>
            <xd:p>String processing variable.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:variable name="simple-sanitizer-replace" select="'***************'"/>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="/">
        <html>
            <head>
                <title>
                    <xsl:value-of select="local-name(*)"/>
                </title>
                <!-- Javascript for toggling visibility of elements. param toggled 
                    is the element ID of the element to toggle and param toggler 
                    os the element ID that serves as trigger.
                -->
                <script type="text/javascript">
                    function toggle(toggled,toggler) {
                        if (document.getElementById) {
                            var currentStyle = document.getElementById(toggled).style;
                            var togglerStyle = document.getElementById(toggler).style;
                            if (currentStyle.display == "block"){
                                currentStyle.display = "none";
                                togglerStyle.backgroundImage = "url(/xis/resources/images/trClosed.gif)";
                            } else {
                                currentStyle.display = "block";
                                togglerStyle.backgroundImage = "url(/xis/resources/images/triangleOpen.gif)";
                            }
                            return false;
                        } else {
                            return true;
                        }
                    }
                </script>
                <script src="/xis/resources/scripts/boxover.js" type="text/javascript"/>
                <link href="/xis/resources/css/nictiz.css" type="text/css" rel="stylesheet"/>
            </head>
            <body>
                <xsl:apply-templates/>
            </body>
        </html>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="*[hl7:interactionId]">
        <xsl:choose>
            <!-- We are top level as opposed to one of the many in a bundle/batch -->
            <xsl:when test="not(parent::*)">
                <table width="100%">
                    <tr>
                        <td valign="bottom">
                            <h1>
                                <xsl:choose>
                                    <xsl:when test="hl7:interactionId/@extension">
                                        <xsl:value-of select="hl7:interactionId/@extension"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="local-name()"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </h1>
                        </td>
                        <td align="right">
                            <div class="logo"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <p/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <xsl:call-template name="getLocalizedString">
                                <xsl:with-param name="key" select="'Display Disclaimer'"/>
                            </xsl:call-template>
                        </td>
                    </tr>
                </table>
                <p/>
                <table class="container" width="100%">
                    <tr>
                        <td class="content">
                            <h2>Transmission Wrapper</h2>
                            <div class="level2">
                                <xsl:apply-templates/>
                            </div>
                        </td>
                    </tr>
                </table>
            </xsl:when>
            <xsl:otherwise>
                <h1>
                    <xsl:choose>
                        <xsl:when test="hl7:interactionId/@extension">
                            <xsl:value-of select="hl7:interactionId/@extension"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="local-name()"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </h1>
                <h2>Transmission Wrapper</h2>
                <div class="level2">
                    <xsl:apply-templates/>
                </div>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:creationTime">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="'effectiveTime'"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:call-template name="show-timestamp">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:versionCode">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="local-name()"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:value-of select="@code"/>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:interactionId">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="local-name()"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:value-of select="@extension"/>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:profileId">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="local-name()"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:value-of select="@extension"/>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:transmissionQuantity">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="local-name()"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:value-of select="@value"/>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:acknowledgement">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="local-name()"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="concat('2.16.840.1.113883.5.18-', @typeCode)"/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
        <xsl:apply-templates/>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:acknowledgementDetail">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="local-name()"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="concat('2.16.840.1.113883.5.1082-', @typeCode)"/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
        <xsl:apply-templates/>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:processingCode">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="local-name()"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:value-of select="@code"/>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:processingModeCode">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="local-name()"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:value-of select="@code"/>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:acceptAckCode">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="local-name()"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:value-of select="@code"/>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:typeId" name="displayTypeId">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="local-name()"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:value-of select="@extension"/>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:respondTo">
        <xsl:call-template name="section">
            <xsl:with-param name="label">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="local-name()"/>
                </xsl:call-template>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    
    <xd:doc>
        <xd:desc> Separate template for entityRsp to create a separate section in the html; otherwise all content will be displayed flat,
            losing the difference between the 2 levels of telecom entries.
        </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:entityRsp">
        <xsl:call-template name="section">
            <xsl:with-param name="label">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="local-name()"/>
                </xsl:call-template>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:attentionLine">
        <xsl:call-template name="section">
            <xsl:with-param name="label">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="local-name()"/>
                </xsl:call-template>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:keyWordText">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="local-name()"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:value-of select="."/>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:sender">
        <xsl:call-template name="section">
            <xsl:with-param name="label">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="local-name()"/>
                </xsl:call-template>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:receiver">
        <xsl:call-template name="section">
            <xsl:with-param name="label">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="local-name()"/>
                </xsl:call-template>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:ControlActProcess | hl7:controlActProcess">
        <h2>Control Act Wrapper</h2>
        <div class="level2">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:author | hl7:authorOrPerformer">
        <xsl:call-template name="section">
            <xsl:with-param name="label">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'author'"/>
                </xsl:call-template>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:assignedDevice | hl7:AssignedDevice">
        <xsl:call-template name="section">
            <xsl:with-param name="label">Apparaat</xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:assignedPerson | hl7:AssignedPerson">
        <xsl:call-template name="section">
            <xsl:with-param name="label">Persoon</xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:assignedEntity | hl7:AssignedEntity | hl7:assignedEntity1">
        <xsl:call-template name="section">
            <xsl:with-param name="label">
                <xsl:choose>
                    <xsl:when test="@classCode = 'PRS' or hl7:assignedPerson or hl7:id[@root = ('2.16.528.1.1007.3.1', '2.16.528.1.1007.5.1')]">Persoon</xsl:when>
                    <xsl:when test="@classCode = 'DEV' or hl7:assignedDevice or hl7:id[@root = '2.16.528.1.1007.3.2']">Apparaat</xsl:when>
                    <xsl:when test="@classCode = 'ORG' or hl7:assignedOrganization or hl7:id[@root = '2.16.528.1.1007.3.3']">Organisatie</xsl:when>
                    <xsl:otherwise>Entiteit</xsl:otherwise>
                </xsl:choose>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:custodian">
        <xsl:call-template name="section">
            <!--<xsl:with-param name="label" select="'Beheerverantwoordelijke'"/>-->
            <xsl:with-param name="label">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'custodian'"/>
                </xsl:call-template>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:authenticator">
        <xsl:call-template name="section">
            <xsl:with-param name="label">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="local-name()"/>
                </xsl:call-template>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:legalAuthenticator">
        <xsl:call-template name="section">
            <xsl:with-param name="label">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="local-name()"/>
                </xsl:call-template>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:participant[not(parent::hl7:authorOrPerformer)]">
        <xsl:call-template name="section">
            <xsl:with-param name="label">
                <xsl:choose>
                    <xsl:when test="@typeCode">
                        <xsl:call-template name="getLocalizedString">
                            <xsl:with-param name="key" select="concat('2.16.840.1.113883.5.90-', @typeCode)"/>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="getLocalizedString">
                            <xsl:with-param name="key" select="'Participant'"/>
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:performer">
        <xsl:call-template name="section">
            <xsl:with-param name="label">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="local-name()"/>
                </xsl:call-template>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:responsibleParty">
        <xsl:call-template name="section">
            <xsl:with-param name="label">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="local-name()"/>
                </xsl:call-template>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:pertinentInformation">
        <xsl:call-template name="section">
            <xsl:with-param name="label">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="local-name()"/>
                </xsl:call-template>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:subjectOf/hl7:careStatus">
        <xsl:call-template name="careStatement">
            <xsl:with-param name="label">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="local-name()"/>
                </xsl:call-template>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:overseer">
        <xsl:call-template name="section">
            <xsl:with-param name="label">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="local-name()"/>
                </xsl:call-template>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:justifiedDetectedIssue">
        <h4>
            <xsl:call-template name="getLocalizedString">
                <xsl:with-param name="key" select="local-name()"/>
            </xsl:call-template>
        </h4>
        <div class="level2">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:Patient | hl7:patient | hl7:patientRole">
        <xsl:call-template name="section">
            <xsl:with-param name="label">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'recordTarget'"/>
                </xsl:call-template>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:queryAck">
        <table class="section">
            <tr>
                <td class="section-label">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="local-name()"/>
                    </xsl:call-template>
                </td>
                <td>
                    <table class="values">
                        <tr>
                            <td class="labelSmall">
                                <xsl:call-template name="getLocalizedString">
                                    <xsl:with-param name="key" select="'queryId'"/>
                                </xsl:call-template>
                            </td>
                            <td class="value">
                                <xsl:value-of select="hl7:queryId/concat(@root, ' - ', @extension)"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelSmall">
                                <xsl:call-template name="getLocalizedString">
                                    <xsl:with-param name="key" select="'queryResponseCode'"/>
                                </xsl:call-template>
                            </td>
                            <td class="value">
                                <xsl:choose>
                                    <xsl:when test="hl7:queryResponseCode[@code = ('AE', 'QE', 'NF', 'OK')]">
                                        <xsl:call-template name="getLocalizedString">
                                            <xsl:with-param name="key" select="concat('2.16.840.1.113883.5.1067-', hl7:queryResponseCode/@code)"/>
                                        </xsl:call-template>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="hl7:queryResponseCode/@code"/>
                                        <xsl:call-template name="getLocalizedString">
                                            <xsl:with-param name="pre" select="' ('"/>
                                            <xsl:with-param name="key" select="'nullFlavor_NI'"/>
                                            <xsl:with-param name="post" select="')'"/>
                                        </xsl:call-template>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelSmall">
                                <xsl:call-template name="getLocalizedString">
                                    <xsl:with-param name="key" select="'resultTotalQuantity'"/>
                                </xsl:call-template>
                            </td>
                            <td class="value">
                                <xsl:value-of select="hl7:resultTotalQuantity/@value"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelSmall">
                                <xsl:call-template name="getLocalizedString">
                                    <xsl:with-param name="key" select="'resultCurrentQuantity'"/>
                                </xsl:call-template>
                            </td>
                            <td class="value">
                                <xsl:value-of select="hl7:resultCurrentQuantity/@value"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelSmall">
                                <xsl:call-template name="getLocalizedString">
                                    <xsl:with-param name="key" select="'resultRemainingQuantity'"/>
                                </xsl:call-template>
                            </td>
                            <td class="value">
                                <xsl:value-of select="hl7:resultRemainingQuantity/@value"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:queryByParameter">
        <table class="section">
            <tr>
                <td class="section-label">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="local-name()"/>
                    </xsl:call-template>
                </td>
                <td>
                    <table class="values">
                        <tr>
                            <td class="labelSmall">
                                <xsl:call-template name="getLocalizedString">
                                    <xsl:with-param name="key" select="'queryId'"/>
                                </xsl:call-template>
                            </td>
                            <td class="value">
                                <xsl:value-of select="hl7:queryId/concat(@root, ' - ', @extension)"/>
                            </td>
                        </tr>
                        <xsl:apply-templates select="hl7:statusCode"/>
                        <xsl:apply-templates select="hl7:reponseModalityCode"/>
                        <xsl:apply-templates select="hl7:responsePriorityCode"/>
                        <xsl:if test="hl7:initialQuantity">
                            <tr>
                                <td class="labelSmall">
                                    <xsl:call-template name="getLocalizedString">
                                        <xsl:with-param name="key" select="'initialQuantity'"/>
                                    </xsl:call-template>
                                </td>
                                <td class="value">
                                    <xsl:value-of select="hl7:initialQuantity/@value"/>
                                </td>
                            </tr>
                        </xsl:if>
                        <xsl:apply-templates select="hl7:initialQuantityCode"/>
                        <tr>
                            <td class="labelSmall">
                                <xsl:call-template name="getLocalizedString">
                                    <xsl:with-param name="key" select="'executionAndDeliveryTime'"/>
                                </xsl:call-template>
                            </td>
                            <td class="value">
                                <xsl:call-template name="show-timestamp">
                                    <xsl:with-param name="in" select="hl7:executionAndDeliveryTime"/>
                                </xsl:call-template>
                            </td>
                        </tr>
                    </table>
                    <table class="values" style="border: 1px solid #d7b0c6;">
                        <xsl:for-each select="*[hl7:value]">
                            <tr>
                                <td class="labelSmall">
                                    <xsl:value-of select="local-name()"/>
                                </td>
                                <td class="value">
                                    <xsl:for-each select="hl7:value">
                                        <xsl:choose>
                                            <xsl:when test="@root or @extension">
                                                <!--<xsl:value-of select="concat(@root,' - ',@extension)"/>-->
                                                <xsl:call-template name="show-id-set">
                                                    <xsl:with-param name="in" select="."/>
                                                </xsl:call-template>
                                            </xsl:when>
                                            <xsl:when test="hl7:low or hl7:high">
                                                <xsl:call-template name="IVL_TS">
                                                    <xsl:with-param name="theIVL" select="."/>
                                                </xsl:call-template>
                                            </xsl:when>
                                            <xsl:when test="@value and @unit">
                                                <xsl:value-of select="@value"/> - <xsl:value-of select="@unit"/>
                                            </xsl:when>
                                            <xsl:when test="@value">
                                                <xsl:value-of select="@value"/>
                                            </xsl:when>
                                            <xsl:when test="@code and @codeSystem">
                                                <xsl:call-template name="show-code-set">
                                                    <xsl:with-param name="in" select="."/>
                                                </xsl:call-template>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:call-template name="show-text">
                                                    <xsl:with-param name="in" select="."/>
                                                </xsl:call-template>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                        <xsl:if test="position() != last()">
                                            <xsl:call-template name="getLocalizedString">
                                                <xsl:with-param name="key" select="'logicalOR'"/>
                                            </xsl:call-template>
                                        </xsl:if>
                                    </xsl:for-each>
                                </td>
                            </tr>
                            <xsl:if test="position() != last()">
                                <tr>
                                    <td align="right">
                                        <xsl:call-template name="getLocalizedString">
                                            <xsl:with-param name="key" select="'logicalAND'"/>
                                        </xsl:call-template>
                                    </td>
                                    <td/>
                                </tr>
                            </xsl:if>
                        </xsl:for-each>
                    </table>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:ClinicalDocument">
        <xsl:variable name="key" select="concat(hl7:code/@codeSystem, '-', hl7:code/@code)"/>
        <xsl:variable name="displayName">
            <xsl:call-template name="getLocalizedString">
                <xsl:with-param name="key" select="$key"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="label">
            <xsl:choose>
                <xsl:when test="hl7:title">
                    <xsl:value-of select="hl7:title"/>
                </xsl:when>
                <xsl:when test="$displayName = $key and hl7:code[@displayName]">
                    <xsl:value-of select="hl7:code/@displayName"/>
                </xsl:when>
                <xsl:when test="$displayName = $key and hl7:code[hl7:displayName/@value]">
                    <xsl:value-of select="(hl7:code/hl7:displayName/@value)[1]"/>
                </xsl:when>
                <xsl:when test="$displayName = $key">
                    <xsl:value-of select="hl7:code/@code"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$displayName"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="not(parent::*)">
                <table width="100%">
                    <tr>
                        <td valign="bottom">
                            <h1>
                                <xsl:value-of select="$label"/>
                            </h1>
                        </td>
                        <td align="right">
                            <div class="logo"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <p/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <xsl:call-template name="getLocalizedString">
                                <xsl:with-param name="key" select="'Display Disclaimer'"/>
                            </xsl:call-template>
                        </td>
                    </tr>
                </table>
                <p/>
                <table class="container" width="100%">
                    <tr>
                        <td class="content">
                            <h2>
                                <xsl:call-template name="getLocalizedString">
                                    <xsl:with-param name="key" select="'ClinicalDocument'"/>
                                </xsl:call-template>
                            </h2>
                            <div class="level2">
                                <xsl:apply-templates/>
                            </div>
                        </td>
                    </tr>
                </table>
            </xsl:when>
            <xsl:otherwise>
                <h1>
                    <xsl:value-of select="$label"/>
                </h1>
                <h2>
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="'ClinicalDocument'"/>
                    </xsl:call-template>
                </h2>
                <div class="level2">
                    <xsl:apply-templates/>
                </div>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xd:doc>
        <xd:desc> Templates op alfabetische volgorde </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:act">
        <xsl:variable name="label">
            <xsl:choose>
                <xsl:when test="@classCode = 'DOCCLIN'">
                    <xsl:value-of select="'Document'"/>
                </xsl:when>
                <xsl:when test="@moodCode = 'INT'">
                    <xsl:value-of select="'Intentie'"/>
                </xsl:when>
                <xsl:when test="@moodCode = 'RQO'">
                    <xsl:value-of select="'Verzoek'"/>
                </xsl:when>
                <xsl:when test="@moodCode = 'PRMS'">
                    <xsl:value-of select="'Toezegging'"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="'Act'"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="$label"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:adverseReaction">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Bijwerking'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:advice">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Voorlichting'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:annotation">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Annotatie'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:appendage/hl7:document">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Toegevoegd document'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:appointmentAbortEvent">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Afspraak annulering'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:asCareSubject">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Niet-medische voorziening'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:asEmployee">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Werknemer van'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:asMember">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Deel van'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:asStudent">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Student bij'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:schoolOrganization">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Onderwijsinstelling'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:organizationContains">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Organisatiedeel'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:asPatientOfOtherProvider">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Patiënt/cliënt bij'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:carePlan">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Zorgplan'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:careProvisionEvent | hl7:CareProvisionEvent">
        <xsl:call-template name="headingToggle">
            <xsl:with-param name="heading" select="'h2'"/>
            <xsl:with-param name="label" select="'CareProvisionEvent'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:CareProvisionRequest">
        <xsl:call-template name="headingToggle">
            <xsl:with-param name="heading" select="'h2'"/>
            <xsl:with-param name="label" select="'CareProvisionRequest'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:causativeAgent">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Veroorzakend Agens'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:ClinicalDocument/hl7:component/hl7:structuredBody">
        <xsl:call-template name="headingToggle">
            <xsl:with-param name="heading" select="'h2'"/>
            <xsl:with-param name="label" select="'Gestructureerde inhoud'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:ClinicalDocument/hl7:component/hl7:nonXMLBody">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Ongestructureerd inhoud'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:conclusion">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Conclusie'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:condition">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Aandoening'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:Condition">
        <xsl:call-template name="headingToggle">
            <xsl:with-param name="label" select="'Conditie'"/>
            <xsl:with-param name="heading" select="'h2'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:consumable">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Product'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:contact">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Contact'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:contactParty">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Contactpersoon'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:ContraIndication">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Contra-indicatie'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:consentEvent">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Consent'"/>
            <xsl:with-param name="negationIndLabel" select="'Toestemming geweigerd'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:CoveredParty">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Verzekering'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:device">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Apparaat'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:delivery">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Bevalling'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:deliveryObservation">
        <xsl:call-template name="careStatement">
            <xsl:with-param name="label" select="'Baringsobservatie'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:employerOrganization">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Werkgever'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:encounterAppointment">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Afspraak'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:encounter">
        <xsl:variable name="label">
            <xsl:choose>
                <xsl:when test="string-length(hl7:code/@displayName) &gt; 0">
                    <xsl:value-of select="hl7:code/@displayName"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="'Contact'"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <!-- encounter in PrimaryCareProvision in deelcontacten -->
        <xsl:choose>
            <xsl:when test="local-name(../..) = 'PrimaryCareProvision' and count(distinct-values(hl7:component/hl7:sequenceNumber/@value)) &gt; 1">
                <xsl:variable name="id" select="generate-id()"/>
                <xsl:variable name="id-toggler" select="concat($id, '-toggler')"/>
                <table class="section">
                    <tr>
                        <xsl:element name="td">
                            <xsl:attribute name="class">
                                <xsl:value-of select="'section-label-toggler'"/>
                            </xsl:attribute>
                            <xsl:attribute name="id">
                                <xsl:value-of select="$id-toggler"/>
                            </xsl:attribute>
                            <xsl:attribute name="onclick">
                                <xsl:value-of select="concat('return toggle(&#34;', $id, '&#34;,&#34;', $id-toggler, '&#34;)')"/>
                            </xsl:attribute>
                            <xsl:value-of select="$label"/>
                        </xsl:element>
                        <xsl:element name="td">
                            <xsl:attribute name="id">
                                <xsl:value-of select="$id"/>
                            </xsl:attribute>
                            <xsl:attribute name="class">
                                <xsl:value-of select="'toggle'"/>
                            </xsl:attribute>
                            <xsl:for-each-group select="hl7:component" group-by="hl7:sequenceNumber/@value">
                                <xsl:sort select="current-grouping-key()" data-type="number"/>
                                <xsl:variable name="currentSequenceNumber" select="current-grouping-key()"/>
                                <table class="section">
                                    <tr>
                                        <td class="section-label">
                                            <xsl:value-of select="'Deelcontact'"/>
                                        </td>
                                        <td>
                                            <xsl:for-each select="../hl7:component[hl7:sequenceNumber/@value = $currentSequenceNumber]">
                                                <xsl:apply-templates select="."/>
                                            </xsl:for-each>
                                        </td>
                                    </tr>
                                </table>
                            </xsl:for-each-group>
                        </xsl:element>
                    </tr>
                </table>
            </xsl:when>
            <!-- andere encounters standaard afhandeling -->
            <xsl:otherwise>
                <xsl:call-template name="sectionToggle">
                    <xsl:with-param name="label" select="$label"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:encounterEvent | hl7:encounterNoEvent">
        <xsl:call-template name="sectionToggle">
            <xsl:with-param name="label" select="'Contactmoment'"/>
            <xsl:with-param name="negationIndLabel" select="'Niet plaatsgevonden'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:endOfCareEvent">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Beëindiging zorg'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:ClinicalDocument/hl7:component/hl7:structuredBody/hl7:component/hl7:section/hl7:entry">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'CDA Level 3'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:entryRelationship">
        <xsl:variable name="key" select="concat('typeCode-', @typeCode, if (@inversionInd = 'true') then '-rev' else ())"/>
        <xsl:variable name="label">
            <xsl:call-template name="getLocalizedString">
                <xsl:with-param name="key" select="$key"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:call-template name="section">
            <xsl:with-param name="label">
                <xsl:choose>
                    <xsl:when test="$key = $label">
                        <xsl:text>Relatie (</xsl:text>
                        <xsl:value-of select="@typeCode"/>
                        <xsl:text>)</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$label"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:episodOfCare">
        <xsl:call-template name="sectionToggle">
            <xsl:with-param name="label" select="'Behandelepisode'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:episodOfCare/hl7:component/hl7:actReference">
        <xsl:variable name="idStr" select="concat(hl7:id/@root, '-', hl7:id/@extension)"/>
        <xsl:choose>
            <!-- test of actReference in bericht voorkomt -->
            <xsl:when test="count(//hl7:id[concat(@root, '-', @extension) = $idStr and local-name(../.) != 'actReference' and string-length($idStr) &gt; 1]) = 0">
                <table>
                    <tr>
                        <td class="labelSmall"> ActReferentie Id </td>
                        <td class="value-error">
                            <xsl:call-template name="toolTip">
                                <xsl:with-param name="toolTipText" select="'Referentie niet gevonden'"/>
                            </xsl:call-template>
                            <xsl:value-of select="$idStr"/>
                        </td>
                    </tr>
                </table>
            </xsl:when>
            <!-- test of actReference uniek is -->
            <xsl:when test="count(//hl7:id[concat(@root, '-', @extension) = $idStr and local-name(../.) != 'actReference' and local-name(../../.) != 'reason' and string-length($idStr) &gt; 1]) &gt; 1">
                <table>
                    <tr>
                        <td class="labelSmall"> ActReferentie Id </td>
                        <td class="value-error">
                            <xsl:call-template name="toolTip">
                                <xsl:with-param name="toolTipText" select="'Referentie is niet uniek'"/>
                            </xsl:call-template>
                            <xsl:value-of select="$idStr"/>
                        </td>
                    </tr>
                </table>
            </xsl:when>
            <!-- act die gerefereerd wordt tonen -->
            <xsl:otherwise>
                <xsl:apply-templates select="//hl7:id[concat(@root, '-', @extension) = $idStr and local-name(../.) != 'actReference' and local-name(../../.) != 'reason']/../."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:episodeOfCondition">
        <xsl:variable name="label">
            <xsl:choose>
                <xsl:when test="string-length(hl7:text) &gt; 0">
                    <xsl:value-of select="hl7:text"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="'Zonder titel'"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:call-template name="sectionToggle">
            <xsl:with-param name="label" select="$label"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:escort">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Begeleider'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:evaluationEvent">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Evaluatie'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:groupCluster">
        <xsl:call-template name="careStatement">
            <xsl:with-param name="label" select="'Groep'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:guardian">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Ouder/voogd'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:heelPrick">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Hielprik'"/>
            <xsl:with-param name="negationIndLabel" select="'Niet uitgevoerd'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:houseMate">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Huisgenoot'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:incubatorAccomodation">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Couveuse'"/>
            <xsl:with-param name="negationIndLabel" select="'Niet gebruikt'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:indication">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Indicatie'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:indication/hl7:reasonOf">
        <table>
            <tr>
                <td class="labelSmall"> Soort interventie </td>
                <td class="value">
                    <xsl:choose>
                        <xsl:when test="hl7:informIntent">Voorlichting </xsl:when>
                        <xsl:when test="hl7:informRequest">Advies, Consultatie/inlichtingen vragen </xsl:when>
                        <xsl:when test="hl7:observationIntent">Extra (medisch) onderzoek </xsl:when>
                        <xsl:when test="hl7:registrationIntent">Melding </xsl:when>
                        <xsl:when test="hl7:referral">Verwijzing </xsl:when>
                    </xsl:choose>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:informant">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Informatieverstrekker'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:informationControlActEvent">
        <xsl:call-template name="careStatement">
            <xsl:with-param name="label" select="'Informatie'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:inpatientEncounter">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Opname'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:IntoleranceCondition">
        <xsl:call-template name="headingToggle">
            <xsl:with-param name="label" select="'Intolerantie'"/>
            <xsl:with-param name="heading" select="'h2'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:JournalEntry">
        <xsl:variable name="label">
            <xsl:choose>
                <xsl:when test="./hl7:code/@code = 'S'"> S-regel </xsl:when>
                <xsl:when test="./hl7:code/@code = 'O'"> O-regel </xsl:when>
                <xsl:when test="./hl7:code/@code = 'E'"> E-regel </xsl:when>
                <xsl:when test="./hl7:code/@code = 'P'"> P-regel </xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="$label"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:location">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Locatie'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:location/hl7:serviceDeliveryLocation">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Afdeling'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:medication | hl7:MedicationKind">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Medicatie'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:medicationAdministrationRequest">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Toedieningsverzoek'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:medicationDispenseEvent | hl7:MedicationDispenseEvent">
        <xsl:call-template name="headingToggle">
            <xsl:with-param name="heading" select="'h2'"/>
            <xsl:with-param name="label" select="'Medicatieverstrekking'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:MedicationDispenseList">
        <xsl:call-template name="headingToggle">
            <xsl:with-param name="heading" select="'h2'"/>
            <xsl:with-param name="label" select="'Lijst Medicatieverstrekkingen'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:medicationDispenseRequest">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Afleververzoek'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:member/hl7:group">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Woonverband'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:neonateData">
        <xsl:call-template name="sectionToggle">
            <xsl:with-param name="label" select="'Pasgeborene'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:neonateObservations">
        <xsl:call-template name="careStatement">
            <xsl:with-param name="label" select="'Neonatale observatie'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:nonBDSData">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Non-BDS'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:nonEncounterCareActivity">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Contactloze activiteit'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:nonMedicalCareProvision">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Niet-medische voorziening'"/>
            <xsl:with-param name="negationIndLabel" select="'Geen gebruik'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:ObservationDx">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Diagnose'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:observationGoal">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Doel'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:observationIntent">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Interventie'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:ObservationIntolerance">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Intolerantie'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:Organization | hl7:representedOrganization | hl7:representedCustodianOrganization">
        <xsl:call-template name="section">
            <xsl:with-param name="label">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'organization'"/>
                </xsl:call-template>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:observation | hl7:patientCareObservation | hl7:pregnancyObservations">
        <xsl:call-template name="careStatement">
            <xsl:with-param name="label">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'2.16.840.1.113883.5.6-OBS'"/>
                </xsl:call-template>
                <xsl:if test="@moodCode[not(. = 'EVN')] | @negationInd[. = 'true']">
                    <xsl:text> (</xsl:text>
                    <xsl:call-template name="show-moodCode">
                        <xsl:with-param name="in" select="@moodCode"/>
                    </xsl:call-template>
                    <xsl:text>)</xsl:text>
                </xsl:if>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:patientCareProvision">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Ontvangen zorg'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:patientCareProvision/hl7:subject">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Ontvanger van de zorg'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:patientOfOtherProvider">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Zorgrelatie'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:personalRelationship">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Persoonlijke relatie'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:relationshipHolder1">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Relatie'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:pertainsTo/hl7:categoryInBDS">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'BDS rubriek'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:pertinentAnnotationObsEvent">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Attentieregel'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:policyOrAccount">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Polis'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:pregnancyCondition">
        <xsl:call-template name="sectionToggle">
            <xsl:with-param name="label" select="'Zwangerschap'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:prescription | hl7:Prescription">
        <xsl:call-template name="section">
            <xsl:with-param name="label">
                <xsl:choose>
                    <xsl:when test="hl7:code[1]/@displayName">
                        <xsl:value-of select="hl7:code[1]/@displayName"/>
                    </xsl:when>
                    <xsl:otherwise>Medicatievoorschrift</xsl:otherwise>
                </xsl:choose>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:PrimaryCareProvision">
        <xsl:call-template name="headingToggle">
            <xsl:with-param name="heading" select="'h2'"/>
            <xsl:with-param name="label" select="'Dossier'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:problem">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Probleem'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:procedure">
        <xsl:call-template name="careStatement">
            <xsl:with-param name="label">
                <xsl:choose>
                    <xsl:when test="@classCode">
                        <xsl:call-template name="show-actClassCode">
                            <xsl:with-param name="clsCode" select="@classCode"/>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="getLocalizedString">
                            <xsl:with-param name="key" select="'2.16.840.1.113883.5.6-PROC'"/>
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:if test="@moodCode[not(. = 'EVN')]">
                    <xsl:text> (</xsl:text>
                    <xsl:call-template name="show-moodCode">
                        <xsl:with-param name="in" select="@moodCode"/>
                    </xsl:call-template>
                    <xsl:text>)</xsl:text>
                </xsl:if>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:registrationEvent">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Melding'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:registrationProcess">
        <h2>Verwijsindex registratie <xsl:choose>
                <xsl:when test="//hl7:MFMT_IN002101"> aanmaken</xsl:when>
                <xsl:when test="//hl7:MFMT_IN002102"> bijwerken</xsl:when>
                <xsl:when test="//hl7:MFMT_IN002103"> verwijderen</xsl:when>
            </xsl:choose>
        </h2>
        <div class="level2">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:referenceRange">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Referentiewaarden'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:registrationScope">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Inperking'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:rubricCluster">
        <xsl:call-template name="careStatement">
            <xsl:with-param name="label" select="'Rubriek'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:scopingOrganization">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Organisatie'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:section">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Sectie'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:severityObservation">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Ernst'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:substanceAdministration">
        <xsl:variable name="label">
            <xsl:choose>
                <xsl:when test="hl7:code[1]/@displayName">
                    <xsl:value-of select="hl7:code[1]/@displayName"/>
                </xsl:when>
                <xsl:when test="@moodCode = 'EVN'">
                    <xsl:value-of select="'Medicatietoediening'"/>
                </xsl:when>
                <xsl:when test="@moodCode = 'RQO'">
                    <xsl:value-of select="'Medicatievoorschrift'"/>
                </xsl:when>
                <xsl:when test="@moodCode = 'PRP'">
                    <xsl:value-of select="'Medicatievoorstel'"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="'Medicatie'"/>
                    <xsl:if test="@moodCode[not(. = 'EVN')]">
                        <xsl:text> (</xsl:text>
                        <xsl:call-template name="show-moodCode">
                            <xsl:with-param name="in" select="@moodCode"/>
                        </xsl:call-template>
                        <xsl:text>)</xsl:text>
                    </xsl:if>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="$label"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:substanceAdministrationEvent">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Toediening'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:summary">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Samenvatting'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:symptoms">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Symptomen'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:vaccinations">
        <xsl:call-template name="sectionToggle">
            <xsl:with-param name="label" select="'Rijksvaccinatie'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:vaccinationConsent">
        <xsl:call-template name="section">
            <xsl:with-param name="label" select="'Vaccinatie consent'"/>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:vaccinationObservation">
        <xsl:call-template name="careStatement">
            <xsl:with-param name="label" select="'Observatie'"/>
        </xsl:call-template>
    </xsl:template>
    
    <xd:doc>
        <xd:desc> Template voor JGZ careStatement Rubrieken, groepen en elementen </xd:desc>
        <xd:param name="label"/>
    </xd:doc>
    <xsl:template name="careStatement">
        <xsl:param name="label"/>
        <table class="section">
            <tr>
                <td class="section-label">
                    <xsl:value-of select="$label"/>
                </td>
                <td>
                    <h4>
                        <xsl:if test="@negationInd = 'true'">
                            <span style="margin-right: 1em;">GEEN</span>
                        </xsl:if>
                        <xsl:value-of select="hl7:code/@displayName"/>
                    </h4>
                    <xsl:apply-templates/>
                </td>
            </tr>
        </table>
    </xsl:template>
    
    <xd:doc>
        <xd:desc> Templates op alfabetische volgorde </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:addr">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="'addr'"/>
                    </xsl:call-template>
                </td>
                <td>
                    <xsl:call-template name="show-address-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:administrativeGenderCode">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="'administrativeGenderCode'"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:call-template name="show-code-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:asCitizen/hl7:politicalNation/hl7:code">
        <table class="values">
            <tr>
                <td class="labelSmall">Nationaliteit</td>
                <td class="value">
                    <xsl:call-template name="show-code-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:batchComment">
        <table class="values">
            <tr>
                <td class="labelSmall">Commentaar</td>
                <td class="value">
                    <xsl:value-of select="."/>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:birthTime">
        <table class="values">
            <tr>
                <td class="labelSmall"> Geboortedatum</td>
                <td class="value">
                    <xsl:call-template name="show-timestamp">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:birthplace">
        <table class="values">
            <tr>
                <td class="labelSmall">Geboren te </td>
                <td class="value">
                    <xsl:apply-templates/>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:code">
        <table class="values">
            <tr>
                <td class="labelSmall">Code</td>
                <td class="value">
                    <xsl:call-template name="show-code-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:confidentialityCode">
        <table class="values">
            <tr>
                <td class="labelSmall">Vertrouwelijkheid</td>
                <td class="value">
                    <xsl:call-template name="show-code-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:desc">
        <table class="values">
            <tr>
                <td class="labelSmall">Omschrijving</td>
                <td class="value">
                    <xsl:value-of select="."/>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:doseQuantity">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="local-name()"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:call-template name="IVL_PQ">
                        <xsl:with-param name="theIVL" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:educationLevelCode">
        <table class="values">
            <tr>
                <td class="labelSmall">Opleidingsniveau</td>
                <td class="value">
                    <xsl:call-template name="show-code-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:effectiveTime">
        <xsl:variable name="theNS">
            <xsl:choose>
                <xsl:when test="contains(@xsi:type, ':')">
                    <xsl:value-of select="namespace-uri-for-prefix(substring-before(@xsi:type, ':'), .)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="namespace-uri-for-prefix('', .)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="theNSValue">
            <xsl:choose>
                <xsl:when test="contains(@xsi:type, ':')">
                    <xsl:value-of select="substring-after(@xsi:type, ':')"/>
                </xsl:when>
                <xsl:when test="empty(@xsi:type)">
                    <xsl:text>IVL_TS</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@xsi:type"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <table class="values">
            <tr>
                <td class="labelSmall">Datum</td>
                <td class="value">
                    <xsl:choose>
                        <xsl:when test="$theNSValue = 'IVL_TS' and $theNS = 'urn:hl7-org:v3'">
                            <xsl:call-template name="IVL_TS">
                                <xsl:with-param name="theIVL" select="."/>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="$theNSValue = 'PIVL_TS' and $theNS = 'urn:hl7-org:v3'">
                            <xsl:call-template name="PIVL_TS">
                                <xsl:with-param name="thePIVL" select="."/>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="$theNSValue = 'SXPR_TS' and $theNS = 'urn:hl7-org:v3'">
                            <xsl:call-template name="SXPR_TS">
                                <xsl:with-param name="theSXPR_TS" select="."/>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="$theNSValue = 'IVL_TS' and $theNS = 'urn:hl7-nl:v3'">
                            <xsl:call-template name="hl7nl_IVL_TS">
                                <xsl:with-param name="theIVL" select="."/>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="$theNSValue = 'PIVL_TS' and $theNS = 'urn:hl7-nl:v3'">
                            <xsl:call-template name="hl7nl_PIVL_TS">
                                <xsl:with-param name="thePIVL" select="."/>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="$theNSValue = 'SXPR_TS' and $theNS = 'urn:hl7-nl:v3'">
                            <xsl:call-template name="hl7nl_SXPR_TS">
                                <xsl:with-param name="theSXPR_TS" select="."/>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="$theNSValue = 'TS' or @value">
                            <xsl:call-template name="show-timestamp">
                                <xsl:with-param name="in" select="."/>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates select="@* | *"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:functionCode">
        <table class="values">
            <tr>
                <td class="labelSmall">Functie</td>
                <td class="value">
                    <xsl:call-template name="show-code-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:expectedUseTime">
        <table class="values">
            <tr>
                <td class="labelSmall">Verwacht gebruik</td>
                <td class="value">
                    <xsl:call-template name="IVL_TS">
                        <xsl:with-param name="theIVL" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:id | hl7:queryId | hl7:*[@xsi:type = 'II']">
        <table class="values">
            <tr>
                <xsl:choose>
                    <xsl:when test="parent::hl7:ClinicalDocument">
                        <td class="labelSmall">Document-id</td>
                        <td class="value">
                            <xsl:call-template name="show-id-set">
                                <xsl:with-param name="in" select="."/>
                            </xsl:call-template>
                        </td>
                    </xsl:when>
                    <xsl:when test="../hl7:versionCode">
                        <td class="labelSmall">Bericht-id</td>
                        <td class="value">
                            <xsl:call-template name="show-id-set">
                                <xsl:with-param name="in" select="."/>
                            </xsl:call-template>
                        </td>
                    </xsl:when>
                    <xsl:when test="local-name(..) = 'targetMessage'">
                        <td class="labelSmall">Doelbericht-id</td>
                        <td class="value">
                            <xsl:call-template name="show-id-set">
                                <xsl:with-param name="in" select="."/>
                            </xsl:call-template>
                        </td>
                    </xsl:when>
                    <xsl:when test="local-name(..) = 'targetTransmission'">
                        <td class="labelSmall">Doeltransmissie-id</td>
                        <td class="value">
                            <xsl:call-template name="show-id-set">
                                <xsl:with-param name="in" select="."/>
                            </xsl:call-template>
                        </td>
                    </xsl:when>
                    <xsl:when test="@root = '2.16.840.1.113883.2.4.6.6'">
                        <td class="labelSmall">Applicatie-id</td>
                        <td class="value">
                            <xsl:value-of select="@extension"/>
                        </td>
                    </xsl:when>
                    <xsl:when test="@root = '2.16.528.1.1007.3.1'">
                        <td class="labelSmall">UZI</td>
                        <td class="value">
                            <xsl:value-of select="@extension"/>
                        </td>
                    </xsl:when>
                    <xsl:when test="@root = '2.16.528.1.1007.3.2'">
                        <td class="labelSmall">Systeemcertificaat-id</td>
                        <td class="value">
                            <xsl:value-of select="@extension"/>
                        </td>
                    </xsl:when>
                    <xsl:when test="@root = '2.16.528.1.1007.3.3'">
                        <td class="labelSmall">URA</td>
                        <td class="value">
                            <xsl:value-of select="@extension"/>
                        </td>
                    </xsl:when>
                    <xsl:when test="@root = '2.16.840.1.113883.2.4.6.1'">
                        <td class="labelSmall">Vektis AGB-Z</td>
                        <td class="value">
                            <xsl:value-of select="@extension"/>
                        </td>
                    </xsl:when>
                    <xsl:when test="@root = '2.16.840.1.113883.2.4.6.3'">
                        <td class="labelSmall">BSN</td>
                        <td class="value">
                            <xsl:value-of select="@extension"/>
                        </td>
                    </xsl:when>
                    <xsl:otherwise>
                        <td class="labelSmall">
                            <xsl:call-template name="getLocalizedString">
                                <xsl:with-param name="key" select="'id'"/>
                            </xsl:call-template>
                        </td>
                        <td class="value">
                            <xsl:call-template name="show-id-set">
                                <xsl:with-param name="in" select="."/>
                            </xsl:call-template>
                        </td>
                    </xsl:otherwise>
                </xsl:choose>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:initialQuantityCode">
        <tr>
            <td class="labelSmall">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'initialQuantityCode'"/>
                </xsl:call-template>
            </td>
            <td class="value">
                <xsl:call-template name="show-code-set">
                    <xsl:with-param name="in" select="."/>
                </xsl:call-template>
            </td>
        </tr>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:interpretationCode">
        <table class="values">
            <tr>
                <td class="labelSmall">Interpretatiecode</td>
                <td class="value">
                    <xsl:call-template name="show-code-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:interruptibleInd">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="'interruptibleInd'"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:call-template name="show-boolean">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:languageCode">
        <table class="values">
            <tr>
                <td class="labelSmall">Taal</td>
                <td class="value">
                    <xsl:value-of select="@code"/>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:lotNumberText">
        <table class="values">
            <tr>
                <td class="labelSmall">Partijnummer</td>
                <td class="value">
                    <xsl:value-of select="."/>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:methodCode">
        <table class="values">
            <tr>
                <td class="labelSmall">Methode</td>
                <td class="value">
                    <xsl:call-template name="show-code-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:multipleBirthInd">
        <table class="values">
            <tr>
                <td class="labelSmall">Meerling</td>
                <td class="value">
                    <xsl:choose>
                        <xsl:when test="@value = 'true'">Ja</xsl:when>
                        <xsl:otherwise>Nee</xsl:otherwise>
                    </xsl:choose>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:multipleBirthOrderNumber">
        <table class="values">
            <tr>
                <td class="labelSmall">Meerlingvolgnummer</td>
                <td class="value">
                    <xsl:value-of select="@value"/>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:name">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:choose>
                        <xsl:when test="local-name(..) = 'device' or local-name(..) = 'AssignedDevice'">Applicatie</xsl:when>
                        <xsl:when test="local-name(..) = 'Organization' or local-name(..) = 'representedOrganization'">Organisatie</xsl:when>
                        <xsl:otherwise>Naam</xsl:otherwise>
                    </xsl:choose>
                </td>
                <td class="value">
                    <xsl:call-template name="show-name-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:occupationCode">
        <table class="values">
            <tr>
                <td class="labelSmall">Code beroep</td>
                <td class="value">
                    <xsl:call-template name="show-code-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:precondition/hl7:criterion">
        <xsl:call-template name="section">
            <xsl:with-param name="label">Criterium</xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:priorityCode">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="'priorityCode'"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:call-template name="show-code-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:quantity | hl7:rateQuantity | hl7:doseCheckQuantity | hl7:maxDoseQuantity">
        <xsl:choose>
            <xsl:when test="@xsi:type">
                <xsl:call-template name="show-by-datatype">
                    <xsl:with-param name="label">
                        <xsl:call-template name="getLocalizedString">
                            <xsl:with-param name="key" select="local-name()"/>
                        </xsl:call-template>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <table class="values">
                    <tr>
                        <td class="labelSmall">
                            <xsl:call-template name="getLocalizedString">
                                <xsl:with-param name="key" select="local-name()"/>
                            </xsl:call-template>
                        </td>
                        <td class="value">
                            <xsl:choose>
                                <xsl:when test="@nullFlavor">
                                    <xsl:call-template name="show-nullFlavor">
                                        <xsl:with-param name="in" select="@nullFlavor"/>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="@value">
                                    <xsl:call-template name="show-quantity-set">
                                        <xsl:with-param name="in" select="."/>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="*:low | *:high | *:center | *:width">
                                    <xsl:call-template name="IVL_PQ">
                                        <xsl:with-param name="theIVL" select="."/>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:call-template name="RTO">
                                        <xsl:with-param name="theRTO" select="."/>
                                    </xsl:call-template>
                                </xsl:otherwise>
                            </xsl:choose>
                        </td>
                    </tr>
                </table>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:reasonCode">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="local-name()"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:call-template name="show-code-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:repeatNumber">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="local-name()"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:value-of select="@value"/>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:responseModalityCode | hl7:responsePriorityCode | hl7:administrationUnitCode">
        <tr>
            <td class="labelSmall">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="local-name()"/>
                </xsl:call-template>
            </td>
            <td class="value">
                <xsl:call-template name="show-code-set">
                    <xsl:with-param name="in" select="."/>
                </xsl:call-template>
            </td>
        </tr>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:routeCode | hl7:statusCode">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="local-name()"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:call-template name="show-code-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:seperatableInd">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="local-name()"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:call-template name="show-boolean">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:sequenceNumber">
        <table class="values">
            <tr>
                <td class="labelSmall">Volgnummer</td>
                <td class="value">
                    <xsl:call-template name="show-integer-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:targetSiteCode">
        <table class="values">
            <tr>
                <td class="labelSmall">Locatie</td>
                <td class="value">
                    <xsl:call-template name="show-code-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:telecom">
        <table class="values">
            <tr>
                <td class="labelSmall">Telecom</td>
                <td class="value">
                    <xsl:call-template name="show-telecom-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:text">
        <table class="values">
            <tr>
                <td class="labelSmall">Tekst</td>
                <td class="value">
                    <xsl:call-template name="show-ed-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:realmCode">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="local-name()"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:value-of select="@code"/>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:setId">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="local-name()"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:call-template name="show-id-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:templateId">
        <table class="values">
            <tr>
                <td class="labelSmall">Template-id</td>
                <td class="value">
                    <xsl:call-template name="show-id-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:time">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="local-name()"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:choose>
                        <xsl:when test="@nullFlavor">
                            <xsl:call-template name="show-nullFlavor">
                                <xsl:with-param name="in" select="@nullFlavor"/>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="@value">
                            <xsl:call-template name="show-timestamp">
                                <xsl:with-param name="in" select="."/>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="*:low | *:high | *:center">
                            <xsl:call-template name="show-ivlts">
                                <xsl:with-param name="in" select="."/>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:call-template name="SXPR_TS">
                                <xsl:with-param name="theSXPR_TS" select="."/>
                            </xsl:call-template>
                        </xsl:otherwise>
                    </xsl:choose>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:title">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="local-name()"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:call-template name="show-text-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:uncertaintyCode">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="local-name()"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:call-template name="show-code-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:value">
        <xsl:choose>
            <xsl:when test="@xsi:type">
                <xsl:call-template name="show-by-datatype">
                    <xsl:with-param name="label" select="'Waarde'"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <table class="values">
                    <tr>
                        <td class="labelSmall">Waarde</td>
                        <td class="value">
                            <xsl:choose>
                                <!-- Check if coded value -->
                                <xsl:when test="@code | @codeSystem | hl7:originalText">
                                    <xsl:call-template name="show-code-set">
                                        <xsl:with-param name="in" select="."/>
                                    </xsl:call-template>
                                </xsl:when>
                                <!-- Check if nullFlavor -->
                                <xsl:when test="@nullFlavor">
                                    <xsl:call-template name="show-nullFlavor">
                                        <xsl:with-param name="in" select="@nullFlavor"/>
                                    </xsl:call-template>
                                </xsl:when>
                                <!-- boolean -->
                                <xsl:when test="@value = 'true' or @value = 'false'">
                                    <xsl:call-template name="show-boolean">
                                        <xsl:with-param name="in" select="."/>
                                    </xsl:call-template>
                                </xsl:when>
                                <!-- PQ -->
                                <xsl:when test="@value and @unit">
                                    <xsl:call-template name="show-quantity-set">
                                        <xsl:with-param name="in" select="."/>
                                    </xsl:call-template>
                                </xsl:when>
                                <!-- IVL_PQ -->
                                <xsl:when test="hl7:low | hl7:high | hl7:center">
                                    <xsl:call-template name="IVL_TS">
                                        <xsl:with-param name="theIVL" select="."/>
                                    </xsl:call-template>
                                </xsl:when>
                                <!-- INT -->
                                <xsl:when test="@value castable as xs:decimal">
                                    <xsl:call-template name="show-integer-set">
                                        <xsl:with-param name="in" select="."/>
                                    </xsl:call-template>
                                </xsl:when>
                                <!-- II -->
                                <xsl:when test="@extension | @root">
                                    <xsl:call-template name="show-id-set">
                                        <xsl:with-param name="in" select="."/>
                                    </xsl:call-template>
                                </xsl:when>
                                <!-- TS -->
                                <xsl:when test="matches(@value, '^\d{4}')">
                                    <xsl:call-template name="show-timestamp">
                                        <xsl:with-param name="in" select="."/>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="@representation = 'B64'">
                                    <iframe name="nonXMLBody{generate-id(.)}" id="nonXMLBody{generate-id(.)}" width="100%" height="600">
                                        <xsl:attribute name="src">
                                            <xsl:value-of select="concat('data:', @mediaType, ';base64,', .)"/>
                                        </xsl:attribute>
                                    </iframe>
                                </xsl:when>
                                <!-- content in element -->
                                <xsl:otherwise>
                                    <xsl:value-of select="."/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </td>
                    </tr>
                </table>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="hl7:versionNumber">
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="local-name()"/>
                    </xsl:call-template>
                </td>
                <td class="value">
                    <xsl:call-template name="show-integer-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    
    <xd:doc>
        <xd:desc/>
        <xd:param name="theNS"/>
        <xd:param name="theNSValue"/>
    </xd:doc>
    <xsl:template name="show-typed-value">
        <xsl:param name="theNS"/>
        <xsl:param name="theNSValue"/>
        
        <xsl:choose>
            <xsl:when test="$theNSValue = 'AD'">
                <xsl:call-template name="show-address-set">
                    <xsl:with-param name="in" select="."/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$theNSValue = 'BL' or $theNSValue = 'BN'">
                <xsl:call-template name="show-boolean">
                    <xsl:with-param name="in" select="."/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$theNSValue = 'CD' or $theNSValue = 'CE' or $theNSValue = 'CV' or $theNSValue = 'CS' or $theNSValue = 'CO'">
                <xsl:call-template name="show-code-set">
                    <xsl:with-param name="in" select="."/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$theNSValue = 'ED'">
                <xsl:call-template name="show-ed-set">
                    <xsl:with-param name="in" select="."/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$theNSValue = 'II'">
                <xsl:call-template name="show-id-set">
                    <xsl:with-param name="in" select="."/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$theNSValue = 'INT' or $theNSValue = 'REAL'">
                <xsl:call-template name="show-integer-set">
                    <xsl:with-param name="in" select="."/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$theNSValue = 'EN' or $theNSValue = 'PN' or $theNSValue = 'ON' or $theNSValue = 'TN'">
                <xsl:call-template name="show-name-set">
                    <xsl:with-param name="in" select="."/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$theNSValue = 'PQ'">
                <xsl:call-template name="show-quantity-set">
                    <xsl:with-param name="in" select="."/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$theNSValue = 'ST'">
                <xsl:call-template name="show-text-set">
                    <xsl:with-param name="in" select="."/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$theNSValue = 'TS'">
                <xsl:call-template name="show-timestamp">
                    <xsl:with-param name="in" select="."/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$theNSValue = 'IVL_PQ'">
                <xsl:call-template name="IVL_PQ">
                    <xsl:with-param name="theIVL" select="."/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$theNSValue = 'RTO_INT_PQ' or $theNSValue = 'RTO_PQ_PQ'">
                <xsl:call-template name="RTO">
                    <xsl:with-param name="theRTO" select="."/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$theNSValue = 'IVL_TS' and $theNS = 'urn:hl7-org:v3'">
                <xsl:call-template name="IVL_TS">
                    <xsl:with-param name="theIVL" select="."/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$theNSValue = 'PIVL_TS' and $theNS = 'urn:hl7-org:v3'">
                <xsl:call-template name="PIVL_TS">
                    <xsl:with-param name="thePIVL" select="."/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$theNSValue = 'SXPR_TS' and $theNS = 'urn:hl7-org:v3'">
                <xsl:call-template name="SXPR_TS">
                    <xsl:with-param name="theSXPR_TS" select="."/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$theNSValue = 'IVL_TS' and $theNS = 'urn:hl7-nl:v3'">
                <xsl:call-template name="hl7nl_IVL_TS">
                    <xsl:with-param name="theIVL" select="."/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$theNSValue = 'PIVL_TS' and $theNS = 'urn:hl7-nl:v3'">
                <xsl:call-template name="hl7nl_PIVL_TS">
                    <xsl:with-param name="thePIVL" select="."/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$theNSValue = 'SXPR_TS' and $theNS = 'urn:hl7-nl:v3'">
                <xsl:call-template name="hl7nl_SXPR_TS">
                    <xsl:with-param name="theSXPR_TS" select="."/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <table>
                    <caption style="color: red; text-align: left;">
                        <xsl:text>Onbekend datatype</xsl:text>
                    </caption>
                    <xsl:for-each select="@*">
                        <tr>
                            <td class="labelSmall">
                                <xsl:value-of select="name()"/>
                            </td>
                            <td class="value">
                                <xsl:value-of select="."/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:template>
    
    <xd:doc>
        <xd:desc>Display all leaf elements that have an xsi:type and that are not handled explicitly</xd:desc>
        <xd:param name="label"/>
    </xd:doc>
    <xsl:template match="*[@xsi:type]" name="show-by-datatype" priority="-2">
        <xsl:param name="label"/>
        <xsl:variable name="theNS">
            <xsl:choose>
                <xsl:when test="contains(@xsi:type, ':')">
                    <xsl:value-of select="namespace-uri-for-prefix(substring-before(@xsi:type, ':'), .)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="namespace-uri-for-prefix('', .)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="theNSValue">
            <xsl:choose>
                <xsl:when test="contains(@xsi:type, ':')">
                    <xsl:value-of select="substring-after(@xsi:type, ':')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@xsi:type"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="theLabel">
            <xsl:choose>
                <xsl:when test="string-length($label) gt 0">
                    <xsl:value-of select="$label"/>
                </xsl:when>
                <xsl:when test="local-name() = 'value'">Waarde</xsl:when>
                <xsl:when test="$theNSValue = 'AD'">Adres</xsl:when>
                <xsl:when test="$theNSValue = 'BL'">Boolean</xsl:when>
                <xsl:when test="$theNSValue = 'BN'">Boolean</xsl:when>
                <xsl:when test="$theNSValue = 'CS'">Code</xsl:when>
                <xsl:when test="$theNSValue = 'CD'">Concept</xsl:when>
                <xsl:when test="$theNSValue = 'CE'">Code</xsl:when>
                <xsl:when test="$theNSValue = 'CV'">Code</xsl:when>
                <xsl:when test="$theNSValue = 'CO'">Ordinaal</xsl:when>
                <xsl:when test="$theNSValue = 'ED'">Data</xsl:when>
                <xsl:when test="$theNSValue = 'EN'">Naam</xsl:when>
                <xsl:when test="$theNSValue = 'PN'">Naam</xsl:when>
                <xsl:when test="$theNSValue = 'ON'">Naam</xsl:when>
                <xsl:when test="$theNSValue = 'TN'">Naam</xsl:when>
                <xsl:when test="$theNSValue = 'II'">Identificatie</xsl:when>
                <xsl:when test="$theNSValue = 'INT'">Getal</xsl:when>
                <xsl:when test="$theNSValue = 'REAL'">Decimaal</xsl:when>
                <xsl:when test="$theNSValue = 'PQ'">Hoeveelheid</xsl:when>
                <xsl:when test="$theNSValue = 'ST'">Adres</xsl:when>
                <xsl:when test="$theNSValue = 'TEL'">Telecom</xsl:when>
                <xsl:when test="$theNSValue = 'URL'">URL</xsl:when>
                <xsl:when test="$theNSValue = 'IVL_PQ'">Interval van hoeveelheden</xsl:when>
                <xsl:when test="$theNSValue = 'RTO_INT_PQ'">Ratio van getal/hoeveelheid</xsl:when>
                <xsl:when test="$theNSValue = 'RTO_PQ_PQ'">Ratio van hoeveelheid/hoeveelheid</xsl:when>
                <xsl:when test="$theNSValue = 'IVL_TS'">Periode</xsl:when>
                <xsl:when test="$theNSValue = 'PIVL_TS'">Periodiek interval</xsl:when>
                <xsl:when test="$theNSValue = 'SXPR_TS'">Tijdschema</xsl:when>
                <xsl:when test="$theNSValue = 'TS'">Tijdstip</xsl:when>
                <xsl:otherwise>
                    <xsl:message>
                        <xsl:text>Niet ondersteund datatype '</xsl:text>
                        <xsl:value-of select="@xsi:type"/>
                        <xsl:text>'</xsl:text>
                    </xsl:message>
                    <!-- ... -->
                    <!--<xsl:text>Onbekend datatype </xsl:text>-->
                    <xsl:value-of select="@xsi:type"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <table class="values">
            <tr>
                <td class="labelSmall">
                    <xsl:value-of select="$theLabel"/>
                </td>
                <td class="value">
                    <xsl:call-template name="show-typed-value">
                        <xsl:with-param name="theNS" select="$theNS"/>
                        <xsl:with-param name="theNSValue" select="$theNSValue"/>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>Display all leaf elements that are not handled explicitly</xd:desc>
    </xd:doc>
    <xsl:template match="*[not(@xsi:type)]" priority="-2">
        <xsl:choose>
            <!-- Skip unhandled tags that have child elements - otherwise the html tables get nested even more -->
            <xsl:when test="*">
                <xsl:apply-templates/>
            </xsl:when>
            <!-- Display unhandled leaf nodes -->
            <xsl:otherwise>
                <table class="section">
                    <tr>
                        <td class="labelSmall">
                            <!--For debugging add: style="color:red">-->
                            <xsl:value-of select="name()"/>
                        </td>
                        <td>
                            <table class="values">
                                <xsl:for-each select="@*">
                                    <tr>
                                        <td class="labelSmall">
                                            <xsl:value-of select="name()" />
                                            
                                        </td>
                                        <td class="value">
                                            <xsl:value-of select="." />
                                        </td>
                                    </tr>
                                </xsl:for-each>
                            </table>
                        </td>
                    </tr>
                </table>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xd:doc>
        <xd:desc> Named templates voor afhandeling van datatypes </xd:desc>
        <xd:param name="theIVL"/>
    </xd:doc>
    <xsl:template name="IVL_PQ">
        <xsl:param name="theIVL"/>
        <!-- type of interval -->
        <xsl:choose>
            <!-- center -->
            <xsl:when test="$theIVL[*:center]">
                <xsl:call-template name="show-quantity-set">
                    <xsl:with-param name="in" select="$theIVL/*:center"/>
                </xsl:call-template>
            </xsl:when>
            <!-- low and high -->
            <xsl:when test="$theIVL[*:low][*:high]">
                <table>
                    <tr>
                        <td class="labelSmall">Ondergrens</td>
                        <td class="value">
                            <xsl:call-template name="show-quantity-set">
                                <xsl:with-param name="in" select="$theIVL/*:low"/>
                            </xsl:call-template>
                        </td>
                    </tr>
                    <tr>
                        <td class="labelSmall">Bovengrens</td>
                        <td class="value">
                            <xsl:call-template name="show-quantity-set">
                                <xsl:with-param name="in" select="$theIVL/*:high"/>
                            </xsl:call-template>
                        </td>
                    </tr>
                </table>
            </xsl:when>
            <!-- low only -->
            <xsl:when test="$theIVL[*:low][not(*:high)]">
                <table>
                    <tr>
                        <xsl:choose>
                            <xsl:when test="$theIVL/*:low/@inclusive = 'true'">
                                <td class="labelSmall">Groter of gelijk aan</td>
                            </xsl:when>
                            <xsl:when test="$theIVL/*:low/@inclusive = 'false'">
                                <td class="labelSmall">Groter dan</td>
                            </xsl:when>
                        </xsl:choose>
                        <td class="value">
                            <xsl:call-template name="show-quantity-set">
                                <xsl:with-param name="in" select="$theIVL/*:low"/>
                            </xsl:call-template>
                        </td>
                    </tr>
                </table>
            </xsl:when>
            <!-- high only -->
            <xsl:when test="$theIVL[*:high][not(*:low)]">
                <table>
                    <tr>
                        <xsl:choose>
                            <xsl:when test="$theIVL/*:high/@inclusive = 'true'">
                                <td class="labelSmall">Kleiner of gelijk aan</td>
                            </xsl:when>
                            <xsl:when test="$theIVL/*:high/@inclusive = 'false'">
                                <td class="labelSmall">Kleiner dan</td>
                            </xsl:when>
                        </xsl:choose>
                        <td class="value">
                            <xsl:call-template name="show-quantity-set">
                                <xsl:with-param name="in" select="$theIVL/*:high"/>
                            </xsl:call-template>
                        </td>
                    </tr>
                </table>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
        <xd:param name="theIVL"/>
    </xd:doc>
    <xsl:template name="IVL_TS">
        <xsl:param name="theIVL"/>
        <xsl:choose>
            <xsl:when test="$theIVL[@value | @nullFlavor]">
                <xsl:call-template name="show-timestamp">
                    <xsl:with-param name="in" select="$theIVL"/>
                </xsl:call-template>
            </xsl:when>
            <!-- On [date] at [time]  ||   At [date] -->
            <xsl:when test="$theIVL/hl7:center">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'On'"/>
                    <xsl:with-param name="post" select="' '"/>
                </xsl:call-template>
                <xsl:call-template name="show-timestamp">
                    <xsl:with-param name="in" select="$theIVL/hl7:center"/>
                </xsl:call-template>
            </xsl:when>
            <!-- On [date] from [time] to [time] -->
            <xsl:when test="$theIVL/hl7:low and $theIVL/hl7:high and substring($theIVL/hl7:low/@value, 1, 8) = substring($theIVL/hl7:high/@value, 1, 8)">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'On'"/>
                    <xsl:with-param name="post" select="' '"/>
                </xsl:call-template>
                <xsl:call-template name="formatDate">
                    <xsl:with-param name="hl7date">
                        <xsl:value-of select="substring($theIVL/hl7:low/@value, 1, 8)"/>
                    </xsl:with-param>
                </xsl:call-template>
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="pre" select="' '"/>
                    <xsl:with-param name="key" select="'from'"/>
                    <xsl:with-param name="post" select="' '"/>
                </xsl:call-template>
                <xsl:call-template name="formatTime">
                    <xsl:with-param name="hl7date">
                        <xsl:value-of select="$theIVL/hl7:low/@value"/>
                    </xsl:with-param>
                </xsl:call-template>
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="pre" select="' '"/>
                    <xsl:with-param name="key" select="'to'"/>
                    <xsl:with-param name="post" select="' '"/>
                </xsl:call-template>
                <xsl:call-template name="formatTime">
                    <xsl:with-param name="hl7date">
                        <xsl:value-of select="$theIVL/hl7:high/@value"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <!-- From [date] at [time] for [real] [unit] to [date] at [time]-->
            <xsl:when test="$theIVL/hl7:low or $theIVL/hl7:high">
                <xsl:if test="$theIVL/hl7:low">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="'From'"/>
                        <xsl:with-param name="post" select="' '"/>
                    </xsl:call-template>
                    <xsl:call-template name="show-timestamp">
                        <xsl:with-param name="in" select="$theIVL/hl7:low"/>
                    </xsl:call-template>
                </xsl:if>
                <xsl:if test="$theIVL/hl7:width">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="pre" select="' '"/>
                        <xsl:with-param name="key" select="'for'"/>
                        <xsl:with-param name="post" select="' '"/>
                    </xsl:call-template>
                    <xsl:value-of select="$theIVL/hl7:width/@value"/>
                    <xsl:text> </xsl:text>
                    <xsl:call-template name="getPeriod">
                        <xsl:with-param name="theValue" select="$theIVL/hl7:width/@value"/>
                        <xsl:with-param name="theUnit" select="$theIVL/hl7:width/@unit"/>
                    </xsl:call-template>
                </xsl:if>
                <xsl:if test="$theIVL/hl7:high">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="pre" select="' '"/>
                        <xsl:with-param name="key" select="'to'"/>
                        <xsl:with-param name="post" select="' '"/>
                    </xsl:call-template>
                    <xsl:call-template name="show-timestamp">
                        <xsl:with-param name="in" select="$theIVL/hl7:high"/>
                    </xsl:call-template>
                </xsl:if>
            </xsl:when>
            <!-- Just a period -->
            <xsl:when test="$theIVL/hl7:width">
                <xsl:value-of select="$theIVL/hl7:width/@value"/>
                <xsl:text> </xsl:text>
                <xsl:call-template name="getPeriod">
                    <xsl:with-param name="theValue" select="$theIVL/hl7:width/@value"/>
                    <xsl:with-param name="theUnit" select="$theIVL/hl7:width/@unit"/>
                </xsl:call-template>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
        <xd:param name="thePIVL"/>
    </xd:doc>
    <xsl:template name="PIVL_TS">
        <xsl:param name="thePIVL"/>
        <xsl:if test="$thePIVL/@isFlexible = 'true'">
            <xsl:text>Flexibel schema. </xsl:text>
        </xsl:if>
        <xsl:choose>
            <xsl:when test="$thePIVL/hl7:phase">
                <xsl:call-template name="IVL_TS">
                    <xsl:with-param name="theIVL" select="$thePIVL/hl7:phase"/>
                </xsl:call-template>
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="pre" select="' '"/>
                    <xsl:with-param name="key" select="'every'"/>
                    <xsl:with-param name="post" select="' '"/>
                </xsl:call-template>
                <xsl:if test="$thePIVL/hl7:period/@value != 1">
                    <xsl:value-of select="$thePIVL/hl7:period/@value"/>
                    <xsl:text> </xsl:text>
                </xsl:if>
                <xsl:call-template name="getPeriod">
                    <xsl:with-param name="theValue">
                        <xsl:value-of select="$thePIVL/hl7:period/@value"/>
                    </xsl:with-param>
                    <xsl:with-param name="theUnit">
                        <xsl:value-of select="$thePIVL/hl7:period/@unit"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="formatPeriod">
                    <xsl:with-param name="theValue">
                        <xsl:value-of select="$thePIVL/hl7:period/@value"/>
                    </xsl:with-param>
                    <xsl:with-param name="theUnit">
                        <xsl:value-of select="$thePIVL/hl7:period/@unit"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
        <xd:param name="theSXPR_TS"/>
    </xd:doc>
    <xsl:template name="SXPR_TS">
        <xsl:param name="theSXPR_TS"/>
        <xsl:for-each select="*:comp">
            <xsl:choose>
                <xsl:when test="string(@xsi:type) = 'IVL_TS'">
                    <xsl:call-template name="IVL_TS">
                        <xsl:with-param name="theIVL" select="."/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="string(@xsi:type) = 'PIVL_TS'">
                    <xsl:call-template name="PIVL_TS">
                        <xsl:with-param name="thePIVL" select="."/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="@xsi:type = 'SXPR_TS'">
                    <xsl:call-template name="SXPR_TS">
                        <xsl:with-param name="theSXPR_TS" select="."/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>Niet ondersteund componenttype '</xsl:text>
                    <xsl:value-of select="@xsi:type"/>
                    <xsl:text>'</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:if test="following-sibling::*:comp[@operator]">
                <xsl:choose>
                    <xsl:when test="following-sibling::*:comp[@operator = 'A']">
                        <xsl:text> intersectie </xsl:text>
                    </xsl:when>
                    <xsl:when test="following-sibling::*:comp[@operator = 'E']">
                        <xsl:text> behalve </xsl:text>
                    </xsl:when>
                    <xsl:when test="following-sibling::*:comp[@operator = 'H']">
                        <xsl:text> convex hull </xsl:text>
                    </xsl:when>
                    <xsl:when test="following-sibling::*:comp[@operator = 'I']">
                        <xsl:text> en </xsl:text>
                    </xsl:when>
                    <xsl:when test="following-sibling::hl7:comp[@operator = 'P']">
                        <xsl:text> periodic hull </xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="following-sibling::*:comp/@operator"/>
                        <xsl:text> (onbekend type operator) </xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:if>
            <xsl:if test="following-sibling::hl7:comp">
                <br/>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
        <xd:param name="theRTO"/>
    </xd:doc>
    <xsl:template name="RTO">
        <xsl:param name="theRTO"/>
        <xsl:for-each select="$theRTO">
            <xsl:choose>
                <xsl:when test="@nullFlavor">
                    <xsl:call-template name="show-nullFlavor">
                        <xsl:with-param name="in" select="@nullFlavor"/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="@value">
                    <xsl:call-template name="show-quantity-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="*:numerator | *:denominator">
                    <xsl:choose>
                        <xsl:when test="*:numerator/@value">
                            <xsl:call-template name="show-quantity-set">
                                <xsl:with-param name="in" select="*:numerator"/>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="*:numerator/*:uncertainRange">
                            <xsl:choose>
                                <xsl:when test="*:numerator/*:uncertainRange[*:low][*:high]">
                                    <xsl:call-template name="show-integer-set">
                                        <xsl:with-param name="in" select="*:numerator/*:uncertainRange/*:low"/>
                                    </xsl:call-template>
                                    <xsl:text> tot </xsl:text>
                                    <xsl:call-template name="show-integer-set">
                                        <xsl:with-param name="in" select="*:numerator/*:uncertainRange/*:high"/>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="*:numerator/*:uncertainRange[*:low]">
                                    <xsl:text> minimaal </xsl:text>
                                    <xsl:call-template name="show-integer-set">
                                        <xsl:with-param name="in" select="*:numerator/*:uncertainRange/*:low"/>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="*:numerator/*:uncertainRange[*:high]">
                                    <xsl:text> tot </xsl:text>
                                    <xsl:call-template name="show-integer-set">
                                        <xsl:with-param name="in" select="*:numerator/*:uncertainRange/*:high"/>
                                    </xsl:call-template>
                                    <xsl:text> maal</xsl:text>
                                </xsl:when>
                            </xsl:choose>
                        </xsl:when>
                    </xsl:choose>
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="pre" select="' '"/>
                        <xsl:with-param name="key" select="'per'"/>
                        <xsl:with-param name="post" select="' '"/>
                    </xsl:call-template>
                    <xsl:call-template name="show-quantity-set">
                        <xsl:with-param name="in" select="*:denominator"/>
                    </xsl:call-template>
                </xsl:when>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
        <xd:param name="theIVL"/>
    </xd:doc>
    <xsl:template name="hl7nl_IVL_TS">
        <xsl:param name="theIVL"/>
        <xsl:choose>
            <xsl:when test="$theIVL[@value | @nullFlavor]">
                <xsl:call-template name="show-timestamp">
                    <xsl:with-param name="in" select="$theIVL"/>
                </xsl:call-template>
            </xsl:when>
            <!-- On [date] at [time]  ||   At [date] -->
            <xsl:when test="$theIVL/hl7:center">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'On'"/>
                    <xsl:with-param name="post" select="' '"/>
                </xsl:call-template>
                <xsl:call-template name="show-timestamp">
                    <xsl:with-param name="in" select="$theIVL/hl7:center"/>
                </xsl:call-template>
            </xsl:when>
            <!-- On [date] from [time] to [time] -->
            <xsl:when test="$theIVL/hl7:low and $theIVL/hl7:high and substring($theIVL/hl7:low/@value, 1, 8) = substring($theIVL/hl7:high/@value, 1, 8)">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'On'"/>
                    <xsl:with-param name="post" select="' '"/>
                </xsl:call-template>
                <xsl:call-template name="formatDate">
                    <xsl:with-param name="hl7date">
                        <xsl:value-of select="substring($theIVL/hl7:low/@value, 1, 8)"/>
                    </xsl:with-param>
                </xsl:call-template>
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="pre" select="' '"/>
                    <xsl:with-param name="key" select="'from'"/>
                    <xsl:with-param name="post" select="' '"/>
                </xsl:call-template>
                <xsl:call-template name="formatTime">
                    <xsl:with-param name="hl7date">
                        <xsl:value-of select="$theIVL/hl7:low/@value"/>
                    </xsl:with-param>
                </xsl:call-template>
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="pre" select="' '"/>
                    <xsl:with-param name="key" select="'to'"/>
                    <xsl:with-param name="post" select="' '"/>
                </xsl:call-template>
                <xsl:call-template name="formatTime">
                    <xsl:with-param name="hl7date">
                        <xsl:value-of select="$theIVL/hl7:high/@value"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <!-- From [date] at [time] for [real] [unit] to [date] at [time]-->
            <xsl:when test="$theIVL/hl7:low or $theIVL/hl7:high">
                <xsl:if test="$theIVL/hl7:low">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="'From'"/>
                        <xsl:with-param name="post" select="' '"/>
                    </xsl:call-template>
                    <xsl:call-template name="show-timestamp">
                        <xsl:with-param name="in" select="$theIVL/hl7:low"/>
                    </xsl:call-template>
                </xsl:if>
                <xsl:if test="$theIVL/hl7:width">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="pre" select="' '"/>
                        <xsl:with-param name="key" select="'for'"/>
                        <xsl:with-param name="post" select="' '"/>
                    </xsl:call-template>
                    <xsl:value-of select="$theIVL/hl7:width/@value"/>
                    <xsl:text> </xsl:text>
                    <xsl:call-template name="getPeriod">
                        <xsl:with-param name="theValue" select="$theIVL/hl7:width/@value"/>
                        <xsl:with-param name="theUnit" select="$theIVL/hl7:width/@unit"/>
                    </xsl:call-template>
                </xsl:if>
                <xsl:if test="$theIVL/hl7:high">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="pre" select="' '"/>
                        <xsl:with-param name="key" select="'to'"/>
                        <xsl:with-param name="post" select="' '"/>
                    </xsl:call-template>
                    <xsl:call-template name="show-timestamp">
                        <xsl:with-param name="in" select="$theIVL/hl7:high"/>
                    </xsl:call-template>
                </xsl:if>
            </xsl:when>
            <!-- Just a period -->
            <xsl:when test="$theIVL/hl7:width">
                <xsl:value-of select="$theIVL/hl7:width/@value"/>
                <xsl:text> </xsl:text>
                <xsl:call-template name="getPeriod">
                    <xsl:with-param name="theValue" select="$theIVL/hl7:width/@value"/>
                    <xsl:with-param name="theUnit" select="$theIVL/hl7:width/@unit"/>
                </xsl:call-template>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
        <xd:param name="thePIVL"/>
    </xd:doc>
    <xsl:template name="hl7nl_PIVL_TS">
        <xsl:param name="thePIVL"/>
        <xsl:if test="$thePIVL/@isFlexible = 'true'">
            <xsl:text>Flexibel schema. </xsl:text>
        </xsl:if>
        <xsl:choose>
            <!--<hl7nl:frequency>
                <hl7nl:numerator xsi:type="hl7nl:INT" value="2"/>
                <hl7nl:denominator xsi:type="hl7nl:PQ" value="1" unit="d"/>
            </hl7nl:frequency>-->
            <xsl:when test="$thePIVL/*:frequency">
                <xsl:call-template name="RTO">
                    <xsl:with-param name="theRTO" select="$thePIVL/*:frequency"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$thePIVL/*:count">
                <xsl:call-template name="show-integer-set">
                    <xsl:with-param name="in" select="$thePIVL/*:count"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$thePIVL/*:phase">
                <xsl:call-template name="hl7nl_IVL_TS">
                    <xsl:with-param name="theIVL" select="$thePIVL/*:phase"/>
                </xsl:call-template>
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="pre" select="' '"/>
                    <xsl:with-param name="key" select="'every'"/>
                    <xsl:with-param name="post" select="' '"/>
                </xsl:call-template>
                <xsl:if test="$thePIVL/*:period/@value != 1">
                    <xsl:value-of select="$thePIVL/*:period/@value"/>
                    <xsl:text> </xsl:text>
                </xsl:if>
                <xsl:call-template name="getPeriod">
                    <xsl:with-param name="theValue">
                        <xsl:value-of select="$thePIVL/*:period/@value"/>
                    </xsl:with-param>
                    <xsl:with-param name="theUnit">
                        <xsl:value-of select="$thePIVL/*:period/@unit"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$thePIVL/*:period">
                <xsl:call-template name="formatPeriod">
                    <xsl:with-param name="theValue">
                        <xsl:value-of select="$thePIVL/*:period/@value"/>
                    </xsl:with-param>
                    <xsl:with-param name="theUnit">
                        <xsl:value-of select="$thePIVL/*:period/@unit"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
        <xd:param name="theSXPR_TS"/>
    </xd:doc>
    <xsl:template name="hl7nl_SXPR_TS">
        <xsl:param name="theSXPR_TS"/>
        <xsl:for-each select="hl7:comp">
            <xsl:choose>
                <xsl:when test="string(@xsi:type) = 'IVL_TS'">
                    <xsl:call-template name="IVL_TS">
                        <xsl:with-param name="theIVL" select="."/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="string(@xsi:type) = 'PIVL_TS'">
                    <xsl:call-template name="PIVL_TS">
                        <xsl:with-param name="thePIVL" select="."/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="@xsi:type = 'SXPR_TS'">
                    <xsl:call-template name="SXPR_TS">
                        <xsl:with-param name="theSXPR_TS" select="."/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>Niet ondersteund componenttype '</xsl:text>
                    <xsl:value-of select="@xsi:type"/>
                    <xsl:text>'</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:if test="following-sibling::hl7:comp[@operator]">
                <xsl:choose>
                    <xsl:when test="following-sibling::hl7:comp[@operator = 'A']">
                        <xsl:text> intersectie </xsl:text>
                    </xsl:when>
                    <xsl:when test="following-sibling::hl7:comp[@operator = 'E']">
                        <xsl:text> behalve </xsl:text>
                    </xsl:when>
                    <xsl:when test="following-sibling::hl7:comp[@operator = 'H']">
                        <xsl:text> convex hull </xsl:text>
                    </xsl:when>
                    <xsl:when test="following-sibling::hl7:comp[@operator = 'I']">
                        <xsl:text> en </xsl:text>
                    </xsl:when>
                    <xsl:when test="following-sibling::hl7:comp[@operator = 'P']">
                        <xsl:text> periodic hull </xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="following-sibling::hl7:comp/@operator"/>
                        <xsl:text> (onbekend type operator) </xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:if>
            <xsl:if test="following-sibling::hl7:comp">
                <br/>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>
    
    <xd:doc>
        <xd:desc/>
        <xd:param name="hl7date"/>
    </xd:doc>
    <xsl:template name="formatDate">
        <xsl:param name="hl7date"/>
        <xsl:choose>
            <!-- Must be valid HL7 date yyyy... to format -->
            <xsl:when test="matches($hl7date, '^\d+(\.\d+)?([+-]\d*)?$')">
                <xsl:variable name="year" select="substring($hl7date, 1, 4)"/>
                <xsl:variable name="month" select="substring($hl7date, 5, 2)"/>
                <xsl:variable name="day" select="substring($hl7date, 7, 2)"/>
                <xsl:variable name="hours" select="substring($hl7date, 9, 2)"/>
                <xsl:variable name="minutes" select="substring($hl7date, 11, 2)"/>
                <xsl:choose>
                    <xsl:when test="string-length($minutes) > 0">
                        <xsl:value-of select="concat($day, '-', $month, '-', $year, ' om ', $hours, ':', $minutes, 'u')"/>
                    </xsl:when>
                    <xsl:when test="string-length($hours) > 0">
                        <xsl:value-of select="concat($day, '-', $month, '-', $year, ' om ', $hours, 'u')"/>
                    </xsl:when>
                    <xsl:when test="(string-length($month) > 0) and (string-length($day) = 0)">
                        <xsl:value-of select="concat($month, '-', $year)"/>
                    </xsl:when>
                    <xsl:when test="string-length($day) > 0">
                        <xsl:value-of select="concat($day, '-', $month, '-', $year)"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$year"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$hl7date"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
        <xd:param name="theValue"/>
        <xd:param name="theUnit"/>
    </xd:doc>
    <xsl:template name="formatPeriod">
        <xsl:param name="theValue"/>
        <xsl:param name="theUnit"/>
        <xsl:choose>
            <xsl:when test="$theValue &lt;= 1">
                <!-- correctie voor period.value kleiner 1 -> door ronden van 1/n op hele getal -->
                <xsl:value-of select="round(round(100000 div $theValue div 100) div 1000)"/>
                <xsl:text> maal per </xsl:text>
            </xsl:when>
            <xsl:when test="$theValue > 1">
                <!-- 1 maal per value -->
                <xsl:text>1 maal per </xsl:text>
                <xsl:value-of select="$theValue"/>
            </xsl:when>
            <xsl:otherwise>
                <!-- in alle anderen gevallen gaat het zo goed -->
                <!--                <xsl:value-of
                    select="round(100000 div $theValue div 100) div 1000"
                    />-->
            </xsl:otherwise>
        </xsl:choose>
        <xsl:choose>
            <xsl:when test="$theUnit = 's'">seconde</xsl:when>
            <xsl:when test="$theUnit = 'min'">minuut</xsl:when>
            <xsl:when test="$theUnit = 'h'">uur</xsl:when>
            <xsl:when test="$theUnit = 'd'">dag</xsl:when>
            <xsl:when test="$theUnit = 'wk'">week</xsl:when>
            <xsl:when test="$theUnit = 'mo'">maand</xsl:when>
            <xsl:when test="$theUnit = 'a'">jaar</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$theUnit"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
        <xd:param name="hl7date"/>
    </xd:doc>
    <xsl:template name="formatTime">
        <xsl:param name="hl7date"/>
        <xsl:choose>
            <!-- Must be valid HL7 date yyyy... to format -->
            <xsl:when test="matches($hl7date, '^\d+(\.\d+)?([+-]\d*)?$')">
                <xsl:variable name="hours" select="substring($hl7date, 9, 2)"/>
                <xsl:variable name="minutes" select="substring($hl7date, 11, 2)"/>
                <xsl:choose>
                    <xsl:when test="string-length($minutes) > 0">
                        <xsl:value-of select="concat($hours, ':', $minutes, 'u')"/>
                    </xsl:when>
                    <xsl:when test="string-length($hours) > 0">
                        <xsl:value-of select="concat($hours, 'u')"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$hl7date"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xd:doc>
        <xd:desc/>
        <xd:param name="theValue"/>
        <xd:param name="theUnit"/>
    </xd:doc>
    <xsl:template name="getPeriod">
        <xsl:param name="theValue"/>
        <xsl:param name="theUnit"/>
        <xsl:choose>
            <xsl:when test="$theValue &lt;= 1">
                <xsl:choose>
                    <xsl:when test="$theUnit = 's'">seconde</xsl:when>
                    <xsl:when test="$theUnit = 'min'">minuut</xsl:when>
                    <xsl:when test="$theUnit = 'h'">uur</xsl:when>
                    <xsl:when test="$theUnit = 'd'">dag</xsl:when>
                    <xsl:when test="$theUnit = 'wk'">week</xsl:when>
                    <xsl:when test="$theUnit = 'mo'">maand</xsl:when>
                    <xsl:when test="$theUnit = 'a'">jaar</xsl:when>
                    <xsl:otherwise>
                        <xsl:if test="$theUnit and $theUnit != 1 and $theUnit != ''">
                            <xsl:value-of select="$theUnit"/>
                        </xsl:if>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="$theUnit = 's'">seconden</xsl:when>
                    <xsl:when test="$theUnit = 'min'">minuten</xsl:when>
                    <xsl:when test="$theUnit = 'h'">uur</xsl:when>
                    <xsl:when test="$theUnit = 'd'">dagen</xsl:when>
                    <xsl:when test="$theUnit = 'wk'">weken</xsl:when>
                    <xsl:when test="$theUnit = 'mo'">maanden</xsl:when>
                    <xsl:when test="$theUnit = 'a'">jaren</xsl:when>
                    <xsl:otherwise>
                        <xsl:if test="$theUnit and $theUnit != 1 and $theUnit != ''">
                            <xsl:value-of select="$theUnit"/>
                        </xsl:if>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!-- ===================================================================== -->
    <!--                          CDA section/text things                      -->
    <!-- ===================================================================== -->
    
    <xd:doc>
        <xd:desc>
            <xd:p>Handle    paragraph  </xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:paragraph">
        <p>
            <xsl:apply-templates select="." mode="handleSectionTextAttributes"/>
            <!--<xsl:if test="@ID">
                <a name="{@ID}"/>
            </xsl:if>-->
            <xsl:apply-templates/>
        </p>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Handle    linkHtml  </xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:linkHtml">
        <xsl:element name="a">
            <xsl:apply-templates select="." mode="handleSectionTextAttributes">
                <xsl:with-param name="class">linkHtml</xsl:with-param>
            </xsl:apply-templates>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Handle pre</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:pre">
        <pre>
            <xsl:apply-templates/>
        </pre>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Handle content. Content w/ deleted text is hidden</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:content">
        <span>
            <xsl:apply-templates select="." mode="handleSectionTextAttributes"/>
            <!--<xsl:if test="@ID">
                <a name="{@ID}"/>
            </xsl:if>-->
            <!-- IHE PCC MCV -->
            <!--<xsl:if test="@styleCode = 'xHR' or starts-with(@styleCode, 'xHR ')">
                <hr class="xHR"/>
            </xsl:if>-->
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Handle line break </xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:br">
        <xsl:element name="br">
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Handle list  </xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:list">
        <!-- caption -->
        <xsl:if test="hl7:caption">
            <div style="font-weight:bold; ">
                <xsl:apply-templates select="hl7:caption"/>
            </div>
        </xsl:if>
        <!-- item -->
        <xsl:choose>
            <xsl:when test="@listType='ordered'">
                <ol>
                    <xsl:apply-templates select="." mode="handleSectionTextAttributes"/>
                    <!--<xsl:if test="@ID">
                        <a name="{@ID}"/>
                    </xsl:if>-->
                    <xsl:for-each select="hl7:item">
                        <li>
                            <xsl:apply-templates select="." mode="handleSectionTextAttributes"/>
                            <!--<xsl:if test="@ID">
                                <a name="{@ID}"/>
                            </xsl:if>-->
                            <xsl:apply-templates/>
                        </li>
                    </xsl:for-each>
                </ol>
            </xsl:when>
            <xsl:otherwise>
                <!-- list is unordered -->
                <ul>
                    <xsl:apply-templates select="." mode="handleSectionTextAttributes"/>
                    <!--<xsl:if test="@ID">
                        <a name="{@ID}"/>
                    </xsl:if>-->
                    <xsl:for-each select="hl7:item">
                        <li>
                            <xsl:apply-templates select="." mode="handleSectionTextAttributes"/>
                            <!--<xsl:if test="@ID">
                                <a name="{@ID}"/>
                            </xsl:if>-->
                            <xsl:apply-templates/>
                        </li>
                    </xsl:for-each>
                </ul>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Handle caption  </xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:caption">
        <xsl:choose>
            <xsl:when test="parent::hl7:table">
                <caption>
                    <xsl:apply-templates select="." mode="handleSectionTextAttributes"/>
                    <!--<xsl:if test="@ID">
                        <a name="{@ID}"/>
                    </xsl:if>-->
                    <xsl:apply-templates/>
                </caption>
            </xsl:when>
            <xsl:otherwise>
                <div class="caption">
                    <xsl:apply-templates select="." mode="handleSectionTextAttributes"/>
                    <!--<xsl:if test="@ID">
                        <a name="{@ID}"/>
                    </xsl:if>-->
                    <xsl:apply-templates/>
                </div>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Handle footnote </xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:footnote">
        <xsl:variable name="id" select="@ID"/>
        <xsl:variable name="footNoteNum">
            <xsl:for-each select="//hl7:footnote">
                <xsl:if test="@ID = $id">
                    <xsl:value-of select="position()"/>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <div>
            <xsl:apply-templates select="." mode="handleSectionTextAttributes">
                <xsl:with-param name="class" select="'narr_footnote'"/>
            </xsl:apply-templates>
            <xsl:text>[</xsl:text>
            <xsl:value-of select="$footNoteNum"/>
            <xsl:text>]. </xsl:text>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Handle footnoteRef. Produces a superscript [n] where n is the occurence number of this ref in the
                whole document. Also adds a title with the first 50 characters of th footnote on the number so you 
                don't have to navigate to the footnote and just continue to read.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:footnoteRef">
        <xsl:variable name="idref" select="@IDREF"/>
        <xsl:variable name="footNoteNum">
            <xsl:for-each select="//hl7:footnote">
                <xsl:if test="@ID = $idref">
                    <xsl:value-of select="position()"/>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="footNoteText">
            <xsl:copy-of select="//hl7:footnote[@ID = $idref]//text()"/>
        </xsl:variable>
        <sup>
            <xsl:text>[</xsl:text>
            <a href="#{$idref}">
                <!-- Render footnoteref with the first 50 characters of the text -->
                <xsl:attribute name="title">
                    <xsl:value-of select="substring($footNoteText, 1, 50)"/>
                    <xsl:if test="string-length($footNoteText) > 50">
                        <xsl:text>...</xsl:text>
                    </xsl:if>
                </xsl:attribute>
                <xsl:value-of select="$footNoteNum"/>
            </a>
            <xsl:text>]</xsl:text>
        </sup>
    </xsl:template>

    <!--<xd:doc>
        <xd:desc>
            <xd:p>Handle renderMultiMedia. Produces one or more iframes depending on the number of IDREFS in @referencedObject. Can have a caption on all of them.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:renderMultiMedia">
        <xsl:variable name="idrefs">
            <xsl:call-template name="tokenize">
                <xsl:with-param name="string" select="@referencedObject"/>
            </xsl:call-template>
        </xsl:variable>
        
        <xsl:apply-templates select="ancestor::hl7:ClinicalDocument//hl7:observationMedia[@ID = $idrefs]"/>
    </xsl:template>-->
    
    <xsl:variable name="table-elem-attrs">
        <tableElems>
            <elem name="table">
                <attr name="ID"/>
                <attr name="language"/>
                <attr name="styleCode"/>
                <attr name="summary"/>
                <attr name="width"/>
                <attr name="border"/>
                <attr name="frame"/>
                <attr name="rules"/>
                <attr name="cellspacing"/>
                <attr name="cellpadding"/>
            </elem>
            <elem name="thead">
                <attr name="ID"/>
                <attr name="language"/>
                <attr name="styleCode"/>
                <attr name="align"/>
                <attr name="char"/>
                <attr name="charoff"/>
                <attr name="valign"/>
            </elem>
            <elem name="tfoot">
                <attr name="ID"/>
                <attr name="language"/>
                <attr name="styleCode"/>
                <attr name="align"/>
                <attr name="char"/>
                <attr name="charoff"/>
                <attr name="valign"/>
            </elem>
            <elem name="tbody">
                <attr name="ID"/>
                <attr name="language"/>
                <attr name="styleCode"/>
                <attr name="align"/>
                <attr name="char"/>
                <attr name="charoff"/>
                <attr name="valign"/>
            </elem>
            <elem name="colgroup">
                <attr name="ID"/>
                <attr name="language"/>
                <attr name="styleCode"/>
                <attr name="span"/>
                <attr name="width"/>
                <attr name="align"/>
                <attr name="char"/>
                <attr name="charoff"/>
                <attr name="valign"/>
            </elem>
            <elem name="col">
                <attr name="ID"/>
                <attr name="language"/>
                <attr name="styleCode"/>
                <attr name="span"/>
                <attr name="width"/>
                <attr name="align"/>
                <attr name="char"/>
                <attr name="charoff"/>
                <attr name="valign"/>
            </elem>
            <elem name="tr">
                <attr name="ID"/>
                <attr name="language"/>
                <attr name="styleCode"/>
                <attr name="align"/>
                <attr name="char"/>
                <attr name="charoff"/>
                <attr name="valign"/>
            </elem>
            <elem name="th">
                <attr name="ID"/>
                <attr name="language"/>
                <attr name="styleCode"/>
                <attr name="abbr"/>
                <attr name="axis"/>
                <attr name="headers"/>
                <attr name="scope"/>
                <attr name="rowspan"/>
                <attr name="colspan"/>
                <attr name="align"/>
                <attr name="char"/>
                <attr name="charoff"/>
                <attr name="valign"/>
            </elem>
            <elem name="td">
                <attr name="ID"/>
                <attr name="language"/>
                <attr name="styleCode"/>
                <attr name="abbr"/>
                <attr name="axis"/>
                <attr name="headers"/>
                <attr name="scope"/>
                <attr name="rowspan"/>
                <attr name="colspan"/>
                <attr name="align"/>
                <attr name="char"/>
                <attr name="charoff"/>
                <attr name="valign"/>
            </elem>
        </tableElems>
    </xsl:variable>
    
    <xd:doc>
        <xd:desc>Handle table and constituents of table</xd:desc>
    </xd:doc>
    <xsl:template match="hl7:table | hl7:thead | hl7:tfoot | hl7:tbody | hl7:colgroup | hl7:col | hl7:tr | hl7:th | hl7:td">
        <xsl:element name="{local-name()}">
            <xsl:apply-templates select="." mode="handleSectionTextAttributes"/>
            <!--<xsl:if test="@ID">
                <a name="{@ID}"/>
            </xsl:if>-->
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

    <!--<xd:doc>
        <xd:desc>
            <xd:p>Handle table</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:table">
        <table>
            <xsl:apply-templates select="." mode="handleSectionTextAttributes">
                <xsl:with-param name="class" select="'narr_table'"/>
            </xsl:apply-templates>
            <!-\-<xsl:if test="@ID">
                <a name="{@ID}"/>
            </xsl:if>-\->
            <xsl:apply-templates/>
        </table>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Handle thead</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:thead">
        <thead>
            <xsl:apply-templates select="." mode="handleSectionTextAttributes"/>
            <!-\-<xsl:if test="@ID">
                <a name="{@ID}"/>
            </xsl:if>-\->
            <xsl:apply-templates/>
        </thead>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Handle tfoot</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:tfoot">
        <tfoot>
            <xsl:apply-templates select="." mode="handleSectionTextAttributes"/>
            <!-\-<xsl:if test="@ID">
                <a name="{@ID}"/>
            </xsl:if>-\->
            <xsl:apply-templates/>
        </tfoot>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>
            <xd:p>Handle tbody</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:tbody">
        <tbody>
            <xsl:apply-templates select="." mode="handleSectionTextAttributes"/>
            <!-\-<xsl:if test="@ID">
                <a name="{@ID}"/>
            </xsl:if>-\->
            <xsl:apply-templates/>
        </tbody>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Handle colgroup</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:colgroup">
        <colgroup>
            <xsl:apply-templates select="." mode="handleSectionTextAttributes"/>
            <!-\-<xsl:if test="@ID">
                <a name="{@ID}"/>
            </xsl:if>-\->
            <xsl:apply-templates/>
        </colgroup>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Handle col</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:col">
        <col>
            <xsl:apply-templates select="." mode="handleSectionTextAttributes"/>
            <!-\-<xsl:if test="@ID">
                <a name="{@ID}"/>
            </xsl:if>-\->
            <xsl:apply-templates/>
        </col>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Handle tr</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:tr">
        <tr>
            <xsl:apply-templates select="." mode="handleSectionTextAttributes">
                <xsl:with-param name="class" select="'narr_tr'"/>
            </xsl:apply-templates>
            <!-\-<xsl:if test="@ID">
                <a name="{@ID}"/>
            </xsl:if>-\->
            <xsl:apply-templates/>
        </tr>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Handle th</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:th">
        <th>
            <xsl:apply-templates select="." mode="handleSectionTextAttributes">
                <xsl:with-param name="class" select="'narr_th'"/>
            </xsl:apply-templates>
            <!-\-<xsl:if test="@ID">
                <a name="{@ID}"/>
            </xsl:if>-\->
            <xsl:apply-templates/>
        </th>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Handle td</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:td">
        <td>
            <xsl:apply-templates select="." mode="handleSectionTextAttributes"/>
            <!-\-<xsl:if test="@ID">
                <a name="{@ID}"/>
            </xsl:if>-\->
            <xsl:apply-templates/>
        </td>
    </xsl:template>-->
    
    <xd:doc>
        <xd:desc>Security measure. Only process images on the image whitelist</xd:desc>
        <xd:param name="current-whitelist"/>
        <xd:param name="image-uri"/>
        <xd:param name="altTitleText"/>
    </xd:doc>
    <xsl:template name="check-external-image-whitelist">
        <xsl:param name="current-whitelist"/>
        <xsl:param name="image-uri"/>
        <xsl:param name="altTitleText"/>
        <xsl:choose>
            <xsl:when test="string-length($current-whitelist) &gt; 0">
                <xsl:variable name="whitelist-item">
                    <xsl:choose>
                        <xsl:when test="contains($current-whitelist,'|')">
                            <xsl:value-of select="substring-before($current-whitelist,'|')"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$current-whitelist"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="starts-with($image-uri,$whitelist-item)">
                        <br clear="all"/>
                        <img src="{$image-uri}" alt="{$altTitleText}" title="{$altTitleText}"/>
                        <xsl:message>
                            <xsl:value-of select="$image-uri"/>
                            <xsl:call-template name="getLocalizedString">
                                <xsl:with-param name="key" select="'is-in-the-whitelist'"/>
                            </xsl:call-template>
                        </xsl:message>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="check-external-image-whitelist">
                            <xsl:with-param name="current-whitelist" select="substring-after($current-whitelist,'|')"/>
                            <xsl:with-param name="image-uri" select="$image-uri"/>
                            <xsl:with-param name="altTitleText" select="$altTitleText"/>
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
                
            </xsl:when>
            <xsl:otherwise>
                <p>
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="'non-local-image-found-1'"/>
                    </xsl:call-template>
                    <xsl:value-of select="$image-uri"/>
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="'non-local-image-found-2'"/>
                    </xsl:call-template>
                </p>
                <xsl:message>
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="'non-local-image-found-1'"/>
                    </xsl:call-template>
                    <xsl:value-of select="$image-uri"/>
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="'non-local-image-found-2'"/>
                    </xsl:call-template>
                </xsl:message>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>
            <xd:p>Handle RenderMultiMedia. This currently only handles GIF's and JPEG's. It could, however, be extended 
                by including other image MIME types in the predicate and/or by generating &lt;object&gt; or &lt;applet&gt; 
                tag with the correct params depending on the media type @ID =$imageRef referencedObject </xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:renderMultiMedia">
        <xsl:variable name="imageRefs" select="@referencedObject"/>
        <!--<xsl:variable name="imageRefs">
            <xsl:call-template name="tokenize">
                <xsl:with-param name="string" select="@referencedObject"/>
                <xsl:with-param name="delimiters" select="' '"/>
            </xsl:call-template>
        </xsl:variable>-->
        <xsl:variable name="referencedObjects" select="ancestor::hl7:ClinicalDocument//hl7:regionOfInterest[@ID = $imageRefs] | ancestor::hl7:ClinicalDocument//hl7:observationMedia[@ID = $imageRefs]"/>
        <br/>
        <span>
            <xsl:apply-templates select="hl7:caption"/>
            <xsl:for-each select="$referencedObjects">
                <xsl:choose>
                    <xsl:when test="self::hl7:regionOfInterest">
                        <!-- What we actually would want is an svg with fallback to just the image that renders the ROI on top of image
                            The only example (in the CDA standard itself) that we've seen so far has unusable coordinates. That for now
                            is not very encouraging to put in the effort, so we just render the images for now
                        -->
                        <xsl:apply-templates select=".//hl7:observationMedia">
                            <!--<xsl:with-param name="usemap" select="@ID"/>-->
                        </xsl:apply-templates>
                        <!--<xsl:variable name="coords">
                            <xsl:variable name="tcoords">
                                <xsl:for-each select="hl7:value/@value">
                                    <xsl:value-of select="."/>
                                    <xsl:text> </xsl:text>
                                </xsl:for-each>
                            </xsl:variable>
                            <xsl:value-of select="translate(normalize-space($tcoords),' ',',')"/>
                        </xsl:variable>-->
                        <!--<svg id="graph" width="100%" height="400px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink">
                            <!-\- pattern -\->
                            <defs>
                                <pattern id="image" x="0%" y="0%" height="100%" width="100%" viewBox="0 0 512 512">
                                    <image x="0%" y="0%" width="512" height="512" xlink:href="https://cdn3.iconfinder.com/data/icons/people-professions/512/Baby-512.png"/>
                                </pattern>
                            </defs>
                            <circle id="sd" class="medium" cx="5%" cy="40%" r="5%" fill="url(#image)" stroke="lightblue" stroke-width="0.5%"/>
                        </svg>-->
                        <!--<map id="{@ID}" name="{@ID}">
                            <xsl:choose>
                                <!-\- A circle defined by two (column,row) pairs. The first point is the center of the circle and the second point is a point on the perimeter of the circle. -\->
                                <xsl:when test="hl7:code/@code = 'CIRCLE'">
                                    <area shape="circle" coords="{$coords}" alt="Computer" href="computer.htm"/>
                                </xsl:when>
                                <!-\- An ellipse defined by four (column,row) pairs, the first two points specifying the endpoints of the major axis and the second two points specifying the endpoints of the minor axis. -\->
                                <xsl:when test="hl7:code/@code = 'ELLIPSE'">
                                    <area shape="poly" coords="{$coords}" alt="Computer" href="computer.htm"/>
                                </xsl:when>
                                <!-\- A single point denoted by a single (column,row) pair, or multiple points each denoted by a (column,row) pair. -\->
                                <xsl:when test="hl7:code/@code = 'POINT'">
                                    <area shape="poly" coords="{$coords}" alt="Computer" href="computer.htm"/>
                                </xsl:when>
                                <!-\- A series of connected line segments with ordered vertices denoted by (column,row) pairs; if the first and last vertices are the same, it is a closed polygon. -\->
                                <xsl:when test="hl7:code/@code = 'POLY'">
                                    <area shape="poly" coords="{$coords}" alt="Computer" href="computer.htm"/>
                                </xsl:when>
                            </xsl:choose>
                        </map>-->
                    </xsl:when>
                    <!-- Here is where the direct MultiMedia image referencing goes -->
                    <xsl:when test="self::hl7:observationMedia">
                        <xsl:apply-templates select="."/>
                    </xsl:when>
                </xsl:choose>
            </xsl:for-each>
        </span>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>
            <xd:p>Handle superscript</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:sup">
        <sup>
            <xsl:apply-templates select="." mode="handleSectionTextAttributes"/>
            <!--<xsl:if test="@ID">
                <a name="{@ID}"/>
            </xsl:if>-->
            <xsl:apply-templates/>
        </sup>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>
            <xd:p>Handle subscript</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:sub">
        <sub>
            <xsl:apply-templates select="." mode="handleSectionTextAttributes"/>
            <!--<xsl:if test="@ID">
                <a name="{@ID}"/>
            </xsl:if>-->
            <xsl:apply-templates/>
        </sub>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>
            <xd:p>Attribute processing for CDAr2 and CDAr3</xd:p>
        </xd:desc>
        <xd:param name="class">If valued then this gets added to potential other class codes</xd:param>
    </xd:doc>
    <xsl:template match="*" mode="handleSectionTextAttributes">
        <xsl:param name="class">
            <xsl:choose>
                <xsl:when test="local-name() = 'table'">narr_table</xsl:when>
                <xsl:when test="local-name() = 'tr'">narr_tr</xsl:when>
                <xsl:when test="local-name() = 'th'">narr_th</xsl:when>
            </xsl:choose>
        </xsl:param>
        
        <xsl:variable name="classes">
            <xsl:if test="string-length($class)">
                <xsl:value-of select="$class"/>
            </xsl:if>
            <xsl:if test="@revised">
                <xsl:text> </xsl:text>
                <xsl:text>revision_</xsl:text>
                <xsl:value-of select="@revised"/>
                <xsl:text>_final</xsl:text>
            </xsl:if>
            <xsl:if test="@styleCode">
                <xsl:text> </xsl:text>
                <xsl:value-of select="@styleCode"/>
            </xsl:if>
            <xsl:if test="@class">
                <xsl:text> </xsl:text>
                <xsl:value-of select="@class"/>
            </xsl:if>
        </xsl:variable>
        
        <xsl:variable name="elem-name" select="local-name(.)"/>
        
        <!-- Write @class attribute if there's data for it -->
        <xsl:if test="string-length(normalize-space($classes))>0">
            <xsl:attribute name="class">
                <xsl:value-of select="normalize-space($classes)"/>
            </xsl:attribute>
        </xsl:if>
        <!-- Write title with @revised (CDAr1 / CDAr2) prefixing to @title if one exists already -->
        <xsl:if test="@revised">
            <xsl:attribute name="title">
                <xsl:value-of select="normalize-space(concat(@revised,' ',@title))"/>
            </xsl:attribute>
        </xsl:if>
        <!-- Write default table cellspacing / cellpadding -->
        <xsl:if test="self::hl7:table">
            <xsl:if test="not(@cellspacing)">
                <xsl:attribute name="cellspacing">
                    <xsl:value-of select="'1'"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:if test="not(@cellpadding)">
                <xsl:attribute name="cellpadding">
                    <xsl:value-of select="'1'"/>
                </xsl:attribute>
            </xsl:if>
        </xsl:if>
        
        <xsl:for-each select="@*">
            <xsl:sort select="local-name()" order="descending"/>
            <xsl:variable name="attr-name" select="local-name(.)"/>
            <xsl:variable name="attr-value" select="."/>
            <xsl:variable name="lcSource" select="translate($attr-value, $uc, $lc)"/>
            <xsl:variable name="scrubbedSource" select="translate($attr-value, $simple-sanitizer-match, $simple-sanitizer-replace)"/>
            <xsl:choose>
                <xsl:when test="contains($lcSource,'javascript')">
                    <xsl:variable name="warningText">
                        <xsl:call-template name="getLocalizedString">
                            <xsl:with-param name="key" select="'javascript-injection-warning'"/>
                            <xsl:with-param name="post" select="': '"/>
                        </xsl:call-template>
                    </xsl:variable>
                    <xsl:message terminate="yes">
                        <xsl:value-of select="$warningText"/>
                        <xsl:value-of select="$attr-value"/>
                    </xsl:message>
                    <xsl:if test="$attr-name = 'href'">
                        <xsl:attribute name="title">
                            <xsl:value-of select="concat(normalize-space(concat(../@title, ' ', $warningText)), ' ', $attr-value)"/>
                        </xsl:attribute>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="not($attr-value = $scrubbedSource)">
                    <xsl:variable name="warningText">
                        <xsl:call-template name="getLocalizedString">
                            <xsl:with-param name="key" select="'malicious-content-warning'"/>
                            <xsl:with-param name="post" select="': '"/>
                        </xsl:call-template>
                    </xsl:variable>
                    <xsl:message>
                        <xsl:value-of select="$warningText"/>
                        <xsl:value-of select="$attr-value"/>
                    </xsl:message>
                    <xsl:if test="$attr-name = 'href'">
                        <xsl:attribute name="title">
                            <xsl:value-of select="concat(normalize-space(concat(../@title, ' ', $warningText)), ' ', $attr-value)"/>
                        </xsl:attribute>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="$table-elem-attrs/elem[@name = $elem-name] and 
                                not($table-elem-attrs//elem[@name = $elem-name]/attr[@name = $attr-name])">
                    <xsl:message>
                        <xsl:value-of select="$attr-name"/> is not legal in <xsl:value-of select="$elem-name"/>
                    </xsl:message>
                </xsl:when>
                <!-- Regular handling from here -->
                <xsl:when test="$attr-name = 'ID'">
                    <xsl:attribute name="id">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="$attr-name = 'class'">
                    <!-- Already handled -->
                </xsl:when>
                <xsl:when test="$attr-name = 'revised'">
                    <!-- Already handled -->
                </xsl:when>
                <xsl:when test="$attr-name = 'styleCode'">
                    <!-- Already handled -->
                </xsl:when>
                <xsl:when test="$attr-name = 'ID'">
                    <!-- @ID should be handled in a name tag, so don't add here -->
                </xsl:when>
                <xsl:when test="$attr-name = 'IDREF'">
                    <!-- @IDREF doubtful. Should be in an href attribute, but doesn't hurt to add here -->
                    <xsl:attribute name="idref">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="$attr-name = 'language'">
                    <xsl:attribute name="lang">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>

                <!-- Table stuff -->
                <xsl:when test="$attr-name = 'border'">
                    <xsl:attribute name="border">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="$attr-name = 'frame'">
                    <xsl:attribute name="frame">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="$attr-name = 'rules'">
                    <xsl:attribute name="rules">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="$attr-name = 'cellpadding'">
                    <xsl:attribute name="cellpadding">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="$attr-name = 'cellspacing'">
                    <xsl:attribute name="cellspacing">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="$attr-name = 'span'">
                    <xsl:attribute name="span">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="$attr-name = 'summary'">
                    <xsl:attribute name="summary">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="$attr-name = 'width'">
                    <xsl:attribute name="width">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="$attr-name = 'align'">
                    <xsl:attribute name="align">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="$attr-name = 'valign'">
                    <xsl:attribute name="valign">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="$attr-name = 'char'">
                    <xsl:attribute name="char">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="$attr-name = 'charoff'">
                    <xsl:attribute name="charoff">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="$attr-name = 'abbr'">
                    <xsl:attribute name="abbr">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="$attr-name = 'scope'">
                    <xsl:attribute name="scope">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="$attr-name = 'headers'">
                    <xsl:attribute name="headers">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="$attr-name = 'axis'">
                    <xsl:attribute name="axis">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="$attr-name = 'colspan'">
                    <xsl:attribute name="colspan">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="$attr-name = 'rowspan'">
                    <xsl:attribute name="rowspan">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>

                <!-- LinkHTML stuff -->
                <xsl:when test="$attr-name = 'name'">
                    <xsl:attribute name="name">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="$attr-name = 'rel'">
                    <xsl:attribute name="rel">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="$attr-name = 'href'">
                    <xsl:attribute name="href">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="$attr-name = 'title'">
                    <xsl:attribute name="title">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="$attr-name = 'rev'">
                    <xsl:attribute name="rev">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <!-- For CDAr3 we might get a slew of attributes not catered for explicitly, 
                        but supposedly HTML compatible so just could add them as-is -->
                    <!-- However... CDAr3 never happened and this poses a security risk, so ignore -->
                    <!--<xsl:attribute name="{$attr-name}">
                        <xsl:value-of select="."/>
                    </xsl:attribute>-->
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>
    
    <!-- 
        ====================================
        START CDAr3 NarrativeBlock specifics
        ====================================
    -->
    
    <xd:doc>
        <xd:desc>
            <xd:p>Handle HTML like CDAr3 style Section.text elements that are not handled already above</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="hl7:a | hl7:dd | hl7:dl | hl7:img | hl7:ins | hl7:span | hl7:p | hl7:ol | hl7:ul| hl7:li">
        <xsl:element name="{local-name()}" namespace="http://www.w3.org/1999/xhtml">
            <xsl:apply-templates select="." mode="handleSectionTextAttributes"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    
    <!-- ===================================================================== -->
    <!--                            Javascript things                          -->
    <!-- ===================================================================== -->
    <!-- Named templates voor layout doeleinden -->
    
    <xd:doc>
        <xd:desc> Template voor heading waarop een div volgt die opengeklikt kan worden </xd:desc>
        <xd:param name="label"/>
        <xd:param name="heading"/>
    </xd:doc>
    <xsl:template name="headingToggle">
        <xsl:param name="label"/>
        <xsl:param name="heading"/>
        <xsl:variable name="id" select="generate-id()"/>
        <xsl:variable name="id-toggler" select="concat($id, '-toggler')"/>
        <xsl:element name="{$heading}">
            <xsl:attribute name="class">
                <xsl:value-of select="'toggler'"/>
            </xsl:attribute>
            <xsl:attribute name="id">
                <xsl:value-of select="$id-toggler"/>
            </xsl:attribute>
            <xsl:attribute name="onclick">
                <xsl:value-of select="concat('return toggle(&#34;', $id, '&#34;,&#34;', $id-toggler, '&#34;)')"/>
            </xsl:attribute>
            <xsl:value-of select="$label"/>
        </xsl:element>
        <div class="level2">
            <xsl:element name="div">
                <xsl:attribute name="id">
                    <xsl:value-of select="$id"/>
                </xsl:attribute>
                <xsl:attribute name="class">
                    <xsl:value-of select="'toggle'"/>
                </xsl:attribute>
                <xsl:apply-templates/>
            </xsl:element>
        </div>
    </xsl:template>
    
    <xd:doc>
        <xd:desc> Template voor secties zoals b.v. patiënt, auteur etc.  </xd:desc>
        <xd:param name="label"/>
        <xd:param name="negationIndLabel"/>
    </xd:doc>
    <xsl:template name="section">
        <xsl:param name="label"/>
        <xsl:param name="negationIndLabel" select="'NIET'"/>
        <table class="section">
            <tr>
                <td class="section-label">
                    <xsl:if test="@negationInd = 'true'">
                        <div>
                            <xsl:value-of select="$negationIndLabel"/>
                        </div>
                    </xsl:if>
                    <xsl:value-of select="$label"/>
                </td>
                <td>
                    <xsl:choose>
                        <xsl:when test="@xsi:nil = 'true' or @nullFlavor">
                            <xsl:call-template name="show-nullFlavor">
                                <xsl:with-param name="in" select="(@nullFlavor, 'NI')[1]"/>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates/>
                        </xsl:otherwise>
                    </xsl:choose>
                </td>
            </tr>
        </table>
    </xsl:template>
    
    <xd:doc>
        <xd:desc> Template voor secties waarbij de inhoud 'opengeklikt' kan worden    </xd:desc>
        <xd:param name="label"/>
        <xd:param name="negationIndLabel"/>
    </xd:doc>
    <xsl:template name="sectionToggle">
        <xsl:param name="label"/>
        <xsl:param name="negationIndLabel" select="'NIET'"/>
        <xsl:variable name="id" select="generate-id()"/>
        <xsl:variable name="id-toggler" select="concat($id, '-toggler')"/>
        <table class="section">
            <tr>
                <xsl:element name="td">
                    <xsl:attribute name="class">
                        <xsl:value-of select="'section-label-toggler'"/>
                    </xsl:attribute>
                    <xsl:attribute name="id">
                        <xsl:value-of select="$id-toggler"/>
                    </xsl:attribute>
                    <xsl:attribute name="onclick">
                        <xsl:value-of select="concat('return toggle(&#34;', $id, '&#34;,&#34;', $id-toggler, '&#34;)')"/>
                    </xsl:attribute>
                    <xsl:value-of select="$label"/>
                    <xsl:if test="./@negationInd = 'true'">
                        <div>
                            <xsl:value-of select="$negationIndLabel"/>
                        </div>
                    </xsl:if>
                </xsl:element>
                <xsl:element name="td">
                    <xsl:attribute name="id">
                        <xsl:value-of select="$id"/>
                    </xsl:attribute>
                    <xsl:attribute name="class">
                        <xsl:value-of select="'toggle'"/>
                    </xsl:attribute>
                    <xsl:apply-templates/>
                </xsl:element>
            </tr>
        </table>
    </xsl:template>
    
    <xd:doc>
        <xd:desc> template voor toolTip, maakt gebruik van Boxover javascript </xd:desc>
        <xd:param name="toolTipText"/>
    </xd:doc>
    <xsl:template name="toolTip">
        <xsl:param name="toolTipText"/>
        <xsl:attribute name="title">
            <xsl:text>cssbody=[toolTipBody] cssheader=[toolTipHeader]  body=[</xsl:text>
            <xsl:value-of select="$toolTipText"/>
            <xsl:text>]</xsl:text>
        </xsl:attribute>
    </xsl:template>
    
    <!-- ===================================================================== -->
    <!--                         Datatype based functions                      -->
    <!-- ===================================================================== -->
    <xd:doc>
        <xd:desc>
            <xd:p>Show elements with datatype ED separated with the value in 'sep'. Calls <xd:ref name="show-ed" type="template">show-ed</xd:ref>
            </xd:p>
        </xd:desc>
        <xd:param name="in">Set of 0 to * elements</xd:param>
        <xd:param name="sep">Separator between output of different elements. Default ', ' and special is 'br' which generates an HTML br tag</xd:param>
    </xd:doc>
    <xsl:template name="show-ed-set">
        <xsl:param name="in"/>
        <xsl:param name="sep" select="', '"/>
        <xsl:if test="$in">
            <xsl:choose>
                <!-- DTr1 -->
                <xsl:when test="count($in) &gt; 1">
                    <xsl:for-each select="$in">
                        <xsl:call-template name="show-ed">
                            <xsl:with-param name="in" select="."/>
                        </xsl:call-template>
                        <xsl:if test="position() != last()">
                            <xsl:choose>
                                <xsl:when test="$sep = 'br'">
                                    <br/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$sep"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:when>
                <!-- DTr2 -->
                <xsl:when test="$in[hl7:item]">
                    <xsl:for-each select="$in/hl7:item">
                        <xsl:call-template name="show-ed">
                            <xsl:with-param name="in" select="."/>
                        </xsl:call-template>
                        <xsl:if test="position() != last()">
                            <xsl:choose>
                                <xsl:when test="$sep = 'br'">
                                    <br/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$sep"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:when>
                <!-- DTr1 or DTr2 -->
                <xsl:otherwise>
                    <xsl:call-template name="show-ed">
                        <xsl:with-param name="in" select="$in"/>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>
            <xd:p>Show element with datatype ED</xd:p>
        </xd:desc>
        <xd:param name="in">One element, possibly out of a set</xd:param>
    </xd:doc>
    <xsl:template name="show-ed">
        <xsl:param name="in"/>
        <xsl:variable name="renderID">
            <xsl:choose>
                <xsl:when test="$in/parent::*/@ID">
                    <xsl:value-of select="$in/parent::*/@ID"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat(generate-id(.), '_', local-name(.))"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="renderAltText">
            <xsl:variable name="i18nid">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'id'"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:if test="$in/parent::*/hl7:id">
                <xsl:value-of select="concat($i18nid, ' = ', $in/parent::*/hl7:id[1]/@root, ' ', $in/parent::*/hl7:id[1]/@extension)"/>
            </xsl:if>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="$in[@representation = 'B64']">
                <p>
                    <i style="font-size: -1;">
                        <xsl:call-template name="getLocalizedString">
                            <xsl:with-param name="key" select="'If the contents are not displayed here, it may be offered as a download.'"/>
                        </xsl:call-template>
                    </i>
                    <div>
                        <xsl:if test="$in/hl7:reference">
                            <xsl:choose>
                                <xsl:when test="$in[starts-with(@mediaType, 'image/')]">
                                    <img alt="{$renderAltText}" title="{$renderAltText}" src="{$in/hl7:reference/@value}"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$in/hl7:reference/@value"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </div>
                </p>
                <iframe name="{$renderID}" id="{$renderID}" width="100%" height="600" title="{$renderAltText}">
                    <xsl:attribute name="src">
                        <xsl:value-of select="concat('data:', $in/@mediaType, ';base64,', string-join($in/text(), ''))"/>
                    </xsl:attribute>
                </iframe>
            </xsl:when>
            <!-- if there is a reference, use that in an iframe -->
            <xsl:when test="$in/hl7:reference[not(@value = ('#', '.'))]">
                <xsl:choose>
                    <xsl:when test="$in[starts-with(@mediaType, 'image/')]">
                        <img alt="{$renderAltText}" title="{$renderAltText}" src="{$in/hl7:reference/@value}"/>
                    </xsl:when>
                    <xsl:when test="$in/hl7:reference[starts-with(@value, '#')]">
                        <xsl:variable name="theID" select="$in/hl7:reference/substring-after(@value, '#')"/>
                        <xsl:variable name="targetData" select="$in/ancestor::*[last()]/descendant-or-self::*[@ID = $theID]"/>
                        
                        <a href="{$in/hl7:reference/@value}" alt="" title="{substring($targetData, 1, 200)}">
                            <xsl:call-template name="getLocalizedString">
                                <xsl:with-param name="key" select="'Value'"/>
                            </xsl:call-template>
                        </a>
                        <xsl:if test="empty($targetData)">
                            <span style="font-weight: bold; color: red;">
                                <xsl:attribute name="title">
                                    <xsl:call-template name="getLocalizedString">
                                        <xsl:with-param name="key" select="'Cannot_display_the_text'"/>
                                        <xsl:with-param name="post">
                                            <xsl:text>: </xsl:text>
                                            <xsl:value-of select="$theID"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </xsl:attribute>
                                <xsl:text>!</xsl:text>
                            </span>
                        </xsl:if>
                    </xsl:when>
                    <xsl:otherwise>
                        <iframe name="{$renderID}" id="{$renderID}" width="100%" height="600" src="{$in/hl7:reference/@value}" title="{$renderAltText}" sandbox=""/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="$in[not(@mediaType) or @mediaType = 'text/plain' or @mediaType = 'text/x-hl7-text+xml'][ancestor-or-self::hl7:ClinicalDocument]">
                <xsl:apply-templates select="$in/node()"/>
            </xsl:when>
            <xsl:when test="$in[not(@mediaType) or @mediaType = 'text/plain']">
                <span title="{$renderAltText}">
                    <xsl:value-of select="$in/text()"/>
                </span>
            </xsl:when>
            <xsl:when test="$in[starts-with(@mediaType, 'image/')]">
                <img alt="{$renderAltText}" title="{$renderAltText}">
                    <xsl:attribute name="src">
                        <xsl:value-of select="concat('data:', $in/@mediaType, ';base64,', $in/text())"/>
                    </xsl:attribute>
                </img>
            </xsl:when>
            <xsl:otherwise>
                <span title="{$renderAltText}">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="'Cannot display the text'"/>
                    </xsl:call-template>
                </span>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>
            <xd:p>Show elements with datatype II separated with the value in 'sep'. Calls <xd:ref name="show-id" type="template">show-id</xd:ref>
        </xd:p>
        </xd:desc>
        <xd:param name="in">Set of 0 to * elements</xd:param>
        <xd:param name="sep">Separator between output of different elements. Default ', ' and special is 'br' which generates an HTML br tag</xd:param>
    </xd:doc>
    <xsl:template name="show-id-set">
        <xsl:param name="in"/>
        <xsl:param name="sep" select="', '"/>
        <xsl:if test="$in">
            <xsl:choose>
                <!-- DTr1 -->
                <xsl:when test="count($in) &gt; 1">
                    <xsl:for-each select="$in">
                        <xsl:call-template name="show-id">
                            <xsl:with-param name="in" select="."/>
                        </xsl:call-template>
                        <xsl:if test="position() != last()">
                            <xsl:choose>
                                <xsl:when test="$sep = 'br'">
                                    <br/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$sep"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:when>
                <!-- DTr2 -->
                <xsl:when test="$in[hl7:item]">
                    <xsl:for-each select="$in/hl7:item">
                        <xsl:call-template name="show-id">
                            <xsl:with-param name="in" select="."/>
                        </xsl:call-template>
                        <xsl:if test="position() != last()">
                            <xsl:choose>
                                <xsl:when test="$sep = 'br'">
                                    <br/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$sep"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:when>
                <!-- DTr1 or DTr2 -->
                <xsl:otherwise>
                    <xsl:call-template name="show-id">
                        <xsl:with-param name="in" select="$in"/>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Show element with datatype II</xd:p>
        </xd:desc>
        <xd:param name="in">One element, possibly out of a set</xd:param>
    </xd:doc>
    <xsl:template name="show-id">
        <xsl:param name="in"/>
        <xsl:if test="$in">
            <span>
                <xsl:if test="$in[@assigningAuthorityName]">
                    <xsl:attribute name="title">
                        <xsl:value-of select="$in/@assigningAuthorityName"/>
                    </xsl:attribute>
                </xsl:if>
                <xsl:variable name="extension">
                    <xsl:if test="$in[@extension][@root]">
                        <xsl:choose>
                            <xsl:when test="$in[contains($mask-ids-var, concat(',', @root, ','))]">
                                <span>
                                    <xsl:attribute name="title">
                                        <xsl:call-template name="show-nullFlavor">
                                            <xsl:with-param name="in" select="'MSK'"/>
                                        </xsl:call-template>
                                    </xsl:attribute>
                                    <xsl:text>xxx-xxx-xxx</xsl:text>
                                </span>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="$in/@extension"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:if>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="$in[@extension][@root]">
                        <xsl:copy-of select="$extension"/>
                        <xsl:call-template name="getLocalizedString">
                            <xsl:with-param name="pre" select="' ('"/>
                            <xsl:with-param name="key" select="$in/@root"/>
                            <xsl:with-param name="post" select="')'"/>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:when test="$in[@root]">
                        <xsl:value-of select="$in/@root"/>
                    </xsl:when>
                    <xsl:when test="$in[@extension]">
                        <xsl:copy-of select="$extension"/>
                    </xsl:when>
                </xsl:choose>
                <xsl:if test="$in[@nullFlavor]">
                    <xsl:if test="$in[@extension]">
                        <xsl:value-of select="$in/@extension"/>
                        <xsl:text> </xsl:text>
                    </xsl:if>
                    <xsl:text>(</xsl:text>
                    <xsl:call-template name="show-nullFlavor">
                        <xsl:with-param name="in" select="$in/@nullFlavor"/>
                    </xsl:call-template>
                    <xsl:text>)</xsl:text>
                </xsl:if>
            </span>
        </xsl:if>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Show elements with datatype INT separated with the value in 'sep'. Calls <xd:ref name="show-integer" type="template">show-integer</xd:ref>
            </xd:p>
        </xd:desc>
        <xd:param name="in">Set of 0 to * elements</xd:param>
        <xd:param name="sep">Separator between output of different elements. Default ', ' and special is 'br' which generates an HTML br tag</xd:param>
    </xd:doc>
    <xsl:template name="show-integer-set">
        <xsl:param name="in"/>
        <xsl:param name="sep" select="', '"/>
        <xsl:if test="$in">
            <xsl:choose>
                <!-- DTr1 -->
                <xsl:when test="count($in) &gt; 1">
                    <xsl:for-each select="$in">
                        <xsl:call-template name="show-integer">
                            <xsl:with-param name="in" select="."/>
                        </xsl:call-template>
                        <xsl:if test="position() != last()">
                            <xsl:choose>
                                <xsl:when test="$sep = 'br'">
                                    <br/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$sep"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:when>
                <!-- DTr2 -->
                <xsl:when test="$in[hl7:item]">
                    <xsl:for-each select="$in/hl7:item">
                        <xsl:call-template name="show-integer">
                            <xsl:with-param name="in" select="."/>
                        </xsl:call-template>
                        <xsl:if test="position() != last()">
                            <xsl:choose>
                                <xsl:when test="$sep = 'br'">
                                    <br/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$sep"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:when>
                <!-- DTr1 or DTr2 -->
                <xsl:otherwise>
                    <xsl:call-template name="show-integer">
                        <xsl:with-param name="in" select="$in"/>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Show element with datatype INT</xd:p>
        </xd:desc>
        <xd:param name="in">One element, possibly out of a set</xd:param>
    </xd:doc>
    <xsl:template name="show-integer">
        <xsl:param name="in"/>
        <xsl:if test="$in">
            <xsl:choose>
                <xsl:when test="$in[@value]">
                    <xsl:value-of select="$in/@value"/>
                </xsl:when>
            </xsl:choose>
            <xsl:if test="$in[@value]">
                <xsl:text> </xsl:text>
            </xsl:if>
            <xsl:if test="$in[@nullFlavor]">
                <xsl:text> (</xsl:text>
                <xsl:call-template name="show-nullFlavor">
                    <xsl:with-param name="in" select="$in/@nullFlavor"/>
                </xsl:call-template>
                <xsl:text>)</xsl:text>
            </xsl:if>
        </xsl:if>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Show elements with datatype CD, CE, CV, CO separated with the value in 'sep'. Calls <xd:ref name="show-code" type="template">show-code</xd:ref></xd:p>
        </xd:desc>
        <xd:param name="in">Set of 0 to * elements</xd:param>
        <xd:param name="sep">Separator between output of different elements. Default ', ' and special is 'br' which generates an HTML br tag</xd:param>
    </xd:doc>
    <xsl:template name="show-code-set">
        <xsl:param name="in"/>
        <xsl:param name="sep" select="', '"/>
        <xsl:if test="$in">
            <xsl:choose>
                <!-- DTr1 -->
                <xsl:when test="count($in) &gt; 1">
                    <xsl:for-each select="$in">
                        <xsl:call-template name="show-code">
                            <xsl:with-param name="in" select="."/>
                        </xsl:call-template>
                        <xsl:if test="position() != last()">
                            <xsl:choose>
                                <xsl:when test="$sep = 'br'">
                                    <br/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$sep"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:when>
                <!-- DTr2 -->
                <xsl:when test="$in[hl7:item]">
                    <xsl:for-each select="$in/hl7:item">
                        <xsl:call-template name="show-code">
                            <xsl:with-param name="in" select="."/>
                        </xsl:call-template>
                        <xsl:if test="position() != last()">
                            <xsl:choose>
                                <xsl:when test="$sep = 'br'">
                                    <br/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$sep"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:when>
                <!-- DTr1 or DTr2 -->
                <xsl:otherwise>
                    <xsl:call-template name="show-code">
                        <xsl:with-param name="in" select="$in"/>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Show elements with datatype CD, CE, CV, CO</xd:p>
        </xd:desc>
        <xd:param name="in">One element, possibly out of a set</xd:param>
    </xd:doc>
    <xsl:template name="show-code">
        <xsl:param name="in"/>
        <xsl:if test="$in">
            <xsl:variable name="codeSystem">
                <xsl:choose>
                    <xsl:when test="$in/@codeSystem">
                        <xsl:value-of select="$in/@codeSystem"/>
                    </xsl:when>
                    <xsl:when test="$in/self::hl7:initialQuantityCode">2.16.840.1.113883.5.1112</xsl:when>
                    <xsl:when test="$in/self::hl7:responseModalityCode[not(@codeSystem)]">2.16.840.1.113883.5.109</xsl:when>
                    <xsl:when test="$in/self::hl7:responsePriorityCode[not(@codeSystem)]">2.16.840.1.113883.5.102</xsl:when>
                    <xsl:when test="$in/self::hl7:signatureCode[not(@codeSystem)]">2.16.840.1.113883.5.89</xsl:when>
                </xsl:choose>
            </xsl:variable>
            <xsl:if test="$in[@value]">
                <xsl:value-of select="$in/@value"/>
                <xsl:text> </xsl:text>
            </xsl:if>
            <xsl:choose>
                <!-- DTr1 -->
                <xsl:when test="$in[@code] and string-length($codeSystem) > 0">
                    <xsl:variable name="key" select="concat($codeSystem, '-', $in/@code)"/>
                    <xsl:variable name="displayName">
                        <xsl:call-template name="getLocalizedString">
                            <xsl:with-param name="key" select="$key"/>
                        </xsl:call-template>
                    </xsl:variable>
                    <xsl:choose>
                        <xsl:when test="$displayName = $key and $in[@displayName]">
                            <xsl:value-of select="$in/@displayName"/>
                            <xsl:text> - </xsl:text>
                            <xsl:value-of select="$in/@code"/>
                        </xsl:when>
                        <xsl:when test="$displayName = $key and $in[hl7:displayName/@value]">
                            <xsl:value-of select="($in/hl7:displayName/@value)[1]"/>
                            <xsl:text> - </xsl:text>
                            <xsl:value-of select="$in/@code"/>
                        </xsl:when>
                        <xsl:when test="$displayName = $key">
                            <xsl:value-of select="$in/@code"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$displayName"/>
                            <xsl:text> - </xsl:text>
                            <xsl:value-of select="$in/@code"/>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:if test="$in[@codeSystem]">
                        <xsl:call-template name="getLocalizedString">
                            <xsl:with-param name="pre" select="' ('"/>
                            <xsl:with-param name="key" select="$codeSystem"/>
                            <xsl:with-param name="post" select="')'"/>
                        </xsl:call-template>
                    </xsl:if>
                </xsl:when>
                <!-- DTr1 -->
                <xsl:when test="$in[@displayName]">
                    <xsl:value-of select="$in/@displayName"/>
                </xsl:when>
                <!-- DTr2 -->
                <xsl:when test="$in[*:displayName/@value]">
                    <xsl:value-of select="($in/*:displayName/@value)[1]"/>
                </xsl:when>
                <!-- DTr1 or DTr2 -->
                <xsl:when test="$in[@code]">
                    <xsl:value-of select="$in/@code"/>
                </xsl:when>
                <!-- DTr1 or DTr2 -->
                <xsl:when test="$in[@nullFlavor]">
                    <xsl:call-template name="show-nullFlavor">
                        <xsl:with-param name="in" select="$in/@nullFlavor"/>
                    </xsl:call-template>
                </xsl:when>
            </xsl:choose>
            <!-- DTr1 | DTr2 -->
            <xsl:for-each select="$in/*:originalText | $in/*:originalText/*:xml">
                <xsl:text> - </xsl:text>
                <xsl:value-of select="."/>
            </xsl:for-each>
            <xsl:for-each select="$in/*:qualifier[*:name | *:value]">
                <div style="margin-left: 2em; border-top: 1px solid #d7b0c6;">
                    <div style="margin-bottom: .5em">
                        <strong>
                            <xsl:call-template name="getLocalizedString">
                                <xsl:with-param name="key" select="local-name()"/>
                                <xsl:with-param name="post" select="' '"/>
                            </xsl:call-template>
                        </strong>
                        </div>
                    <xsl:for-each select="*:name | *:value">
                        <div>
                            <xsl:call-template name="getLocalizedString">
                                <xsl:with-param name="key" select="if (local-name() = 'name') then 'Name' else 'Value'"/>
                                <xsl:with-param name="post" select="' '"/>
                            </xsl:call-template>
                            <xsl:call-template name="show-code-set">
                                <xsl:with-param name="in" select="."/>
                            </xsl:call-template>
                        </div>
                        </xsl:for-each>
                </div>
            </xsl:for-each>
            <xsl:for-each select="$in/*:translation">
                <div style="margin-left: 2em; border-top: 1px solid #d7b0c6;">
                    <strong>
                        <xsl:call-template name="getLocalizedString">
                            <xsl:with-param name="key" select="local-name()"/>
                            <xsl:with-param name="post" select="' '"/>
                        </xsl:call-template>
                    </strong>
                    <xsl:call-template name="show-code-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </div>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Show elements with datatype EN, ON, PN or TN separated with the value in 'sep'. Calls <xd:ref name="show-name" type="template">show-name</xd:ref></xd:p>
        </xd:desc>
        <xd:param name="in">Set of 0 to * elements</xd:param>
        <xd:param name="sep">Separator between output of different elements. Default ', ' and special is 'br' which generates an HTML br tag</xd:param>
    </xd:doc>
    <xsl:template name="show-name-set">
        <xsl:param name="in"/>
        <xsl:param name="sep" select="', '"/>
        <xsl:if test="$in">
            <xsl:choose>
                <!-- DTr1 -->
                <xsl:when test="count($in) > 1">
                    <xsl:for-each select="$in">
                        <xsl:call-template name="show-name">
                            <xsl:with-param name="in" select="."/>
                        </xsl:call-template>
                        <xsl:if test="position() != last()">
                            <xsl:choose>
                                <xsl:when test="$sep = 'br'">
                                    <br/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$sep"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:when>
                <!-- DTr2 -->
                <xsl:when test="$in[hl7:item]">
                    <xsl:for-each select="$in/hl7:item">
                        <xsl:call-template name="show-name">
                            <xsl:with-param name="in" select="."/>
                        </xsl:call-template>
                        <xsl:if test="position() != last()">
                            <xsl:choose>
                                <xsl:when test="$sep = 'br'">
                                    <br/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$sep"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:when>
                <!-- DTr1 or DTr2 -->
                <xsl:otherwise>
                    <xsl:call-template name="show-name">
                        <xsl:with-param name="in" select="$in"/>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Show element with datatype EN, ON, PN, or TN</xd:p>
        </xd:desc>
        <xd:param name="in">One element, possibly out of a set</xd:param>
    </xd:doc>
    <xsl:template name="show-name">
        <xsl:param name="in"/>
        <xsl:if test="$in">
            <xsl:if test="$in/@use">
                <xsl:call-template name="tokenize">
                    <xsl:with-param name="prefix" select="'nameUse_'"/>
                    <xsl:with-param name="string" select="$in/@use"/>
                    <xsl:with-param name="delimiters" select="' '"/>
                </xsl:call-template>
                <xsl:text>: </xsl:text>
            </xsl:if>
            <xsl:if test="$in[@use][@nullFlavor]">
                <xsl:text> </xsl:text>
            </xsl:if>
            <xsl:call-template name="show-nullFlavor">
                <xsl:with-param name="in" select="$in/@nullFlavor"/>
            </xsl:call-template>
            <xsl:for-each select="$in/node()">
                <!-- 
                        Except for prefix, suffix and delimiter name parts, every name part is surrounded by implicit whitespace.
                        Leading and trailing explicit whitespace is insignificant in all those name parts. 
                    -->
                <xsl:if test="self::hl7:given[string-length(normalize-space(.)) > 0] | self::hl7:family[string-length(normalize-space(.)) > 0] | self::hl7:part[@type='GIV' or @type='FAM'][string-length(normalize-space(@value)) > 0]">
                    <xsl:if test="preceding-sibling::node()[string-length(normalize-space(.)) > 0]">
                        <xsl:text> </xsl:text>
                    </xsl:if>
                </xsl:if>
                <xsl:choose>
                    <xsl:when test="self::comment() | self::processing-instruction()"/>
                    <!-- DTr1 -->
                    <xsl:when test="self::hl7:family">
                        <xsl:call-template name="caseUp">
                            <xsl:with-param name="data" select="."/>
                        </xsl:call-template>
                    </xsl:when>
                    <!-- DTr2 -->
                    <xsl:when test="self::hl7:part[@type = 'FAM']">
                        <xsl:call-template name="caseUp">
                            <xsl:with-param name="data" select="@value"/>
                        </xsl:call-template>
                    </xsl:when>
                    <!-- DTr1 -->
                    <xsl:when test="self::hl7:prefix[contains(@qualifier, 'VV')]">
                        <xsl:call-template name="caseUp">
                            <xsl:with-param name="data" select="."/>
                        </xsl:call-template>
                        <xsl:text> </xsl:text>
                    </xsl:when>
                    <!-- DTr2 -->
                    <xsl:when test="self::hl7:part[@type = 'PFX' and contains(@qualifier, 'VV')]">
                        <xsl:call-template name="caseUp">
                            <xsl:with-param name="data" select="@value"/>
                        </xsl:call-template>
                        <xsl:text> </xsl:text>
                    </xsl:when>
                    <!-- DTr1 -->
                    <xsl:when test="self::hl7:prefix | self::hl7:given | self::delimiter">
                        <xsl:value-of select="."/>
                    </xsl:when>
                    <!-- DTr2 -->
                    <xsl:when test="self::hl7:part[@type = 'PFX' or @type = 'GIV' or @type = 'DEL']">
                        <xsl:value-of select="@value"/>
                    </xsl:when>
                    <xsl:when test="string-length(normalize-space(.)) &gt; 0">
                        <xsl:value-of select="."/>
                    </xsl:when>
                    <!-- DTr2 -->
                    <xsl:when test="self::hl7:part[not(@type)][string-length(normalize-space(@value)) &gt; 0]">
                        <xsl:value-of select="@value"/>
                    </xsl:when>
                </xsl:choose>
                <xsl:if test="self::hl7:given[string-length(normalize-space(.)) > 0] | self::hl7:family[string-length(normalize-space(.)) > 0] | self::hl7:part[@type='GIV' or @type='FAM'][string-length(normalize-space(@value)) > 0]">
                    <xsl:if test="following-sibling::node()[string-length(normalize-space(.)) > 0]">
                        <xsl:text> </xsl:text>
                    </xsl:if>
                </xsl:if>
            </xsl:for-each>
            <xsl:if test="$in/hl7:validTime">
                <xsl:text> (</xsl:text>
                <xsl:call-template name="show-ivlts">
                    <xsl:with-param name="in" select="$in/hl7:validTime"/>
                </xsl:call-template>
                <xsl:text>)</xsl:text>
            </xsl:if>
        </xsl:if>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Show elements with datatype AD separated with the value in 'sep'. Calls <xd:ref name="show-address" type="template">show-address</xd:ref></xd:p>
        </xd:desc>
        <xd:param name="in">Set of 0 to * elements</xd:param>
        <xd:param name="sep">Separator between output of different elements. Default ', ' and special is 'br' which generates an HTML br tag</xd:param>
    </xd:doc>
    <xsl:template name="show-address-set">
        <xsl:param name="in"/>
        <xsl:param name="sep" select="', '"/>
        <xsl:if test="$in">
            <xsl:choose>
                <!-- DTr1 -->
                <xsl:when test="count($in) &gt; 1">
                    <xsl:for-each select="$in">
                        <xsl:call-template name="show-address">
                            <xsl:with-param name="in" select="."/>
                        </xsl:call-template>
                        <xsl:if test="position() != last()">
                            <xsl:choose>
                                <xsl:when test="$sep = 'br'">
                                    <br/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$sep"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:when>
                <!-- DTr2 -->
                <xsl:when test="$in[hl7:item]">
                    <xsl:for-each select="$in/hl7:item">
                        <xsl:call-template name="show-address">
                            <xsl:with-param name="in" select="."/>
                        </xsl:call-template>
                        <xsl:if test="position() != last()">
                            <xsl:choose>
                                <xsl:when test="$sep = 'br'">
                                    <br/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$sep"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:when>
                <!-- DTr1 or DTr2 -->
                <xsl:otherwise>
                    <xsl:call-template name="show-address">
                        <xsl:with-param name="in" select="$in"/>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Show element with datatype AD</xd:p>
        </xd:desc>
        <xd:param name="in">One element, possibly out of a set</xd:param>
        <xd:param name="withLineBreaks">Default true(). If false, the whole address is displayed on a single line</xd:param>
    </xd:doc>
    <xsl:template name="show-address">
        <xsl:param name="in"/>
        <xsl:param name="withLineBreaks" select="true()"/>

        <xsl:if test="$in">
            <xsl:if test="$in/@use">
                <xsl:call-template name="tokenize">
                    <xsl:with-param name="prefix" select="'addressUse_'"/>
                    <xsl:with-param name="string" select="$in/@use"/>
                    <xsl:with-param name="delimiters" select="' '"/>
                </xsl:call-template>
                <xsl:text>: </xsl:text>
            </xsl:if>
            <xsl:if test="$in[@use][@nullFlavor]">
                <xsl:text> </xsl:text>
            </xsl:if>
            <xsl:call-template name="show-nullFlavor">
                <xsl:with-param name="in" select="$in/@nullFlavor"/>
            </xsl:call-template>
            <xsl:for-each select="$in/text() | $in/*">
                <xsl:choose>
                    <xsl:when test="self::hl7:useablePeriod"/>
                    <!-- DTr1 only if not streetAddressLine -->
                    <xsl:when test="self::hl7:streetName">
                        <xsl:if test="not(../hl7:streetAddressLine)">
                            <xsl:variable name="additionalLocator" select="following-sibling::hl7:*[1][local-name() = 'additionalLocator'] |
                                                                           following-sibling::hl7:*[1][local-name() = 'houseNumberNumeric' or local-name() = 'houseNumber' or local-name() = 'buildingNumberSuffix']/following-sibling::hl7:*[1][local-name() = 'additionalLocator'] | 
                                                                           following-sibling::hl7:*[1][local-name() = 'houseNumberNumeric' or local-name() = 'houseNumber']/following-sibling::hl7:*[1][local-name() = 'buildingNumberSuffix']/following-sibling::hl7:*[1][local-name() = 'additionalLocator']"/>
                            <xsl:variable name="houseNumber" select="following-sibling::hl7:*[1][local-name() = 'houseNumberNumeric'] | 
                                                                     following-sibling::hl7:*[1][local-name() = 'houseNumber']"/>
                            <xsl:variable name="buildingNumberSuffix" select="following-sibling::hl7:*[1][local-name() = 'buildingNumberSuffix'] | 
                                                                              following-sibling::hl7:*[1][local-name() = 'houseNumberNumeric' or local-name() = 'houseNumber']/following-sibling::hl7:*[1][local-name() = 'buildingNumberSuffix']"/>
                            <!-- 
                                Look for
                                - streetName houseNumber|houseNumberNumeric|buildingNumberSuffix
                                - streetName houseNumber|houseNumberNumeric|buildingNumberSuffix additionalLocator houseNumber|houseNumberNumeric|buildingNumberSuffix
                                - streetName additionalLocator houseNumber|houseNumberNumeric|buildingNumberSuffix
                                in that order and nothing in between.
                            -->
                            <xsl:value-of select="."/>
                            <xsl:choose>
                                <xsl:when test="string-length($houseNumber) > 0">
                                    <xsl:text>&#160;</xsl:text>
                                    <xsl:value-of select="$houseNumber"/>
                                    <xsl:if test="string-length($buildingNumberSuffix) > 0">
                                        <xsl:text>&#160;</xsl:text>
                                        <xsl:value-of select="$buildingNumberSuffix"/>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when test="string-length($buildingNumberSuffix) > 0">
                                    <xsl:text>&#160;</xsl:text>
                                    <xsl:value-of select="$buildingNumberSuffix"/>
                                </xsl:when>
                            </xsl:choose>
                            <xsl:if test="string-length($additionalLocator) > 0">
                                <xsl:text>&#160;</xsl:text>
                                <xsl:value-of select="$additionalLocator"/>
                                
                                <xsl:variable name="houseNumber2" select="$additionalLocator/following-sibling::hl7:*[1][local-name() = 'houseNumberNumeric'] | 
                                                                          $additionalLocator/following-sibling::hl7:*[1][local-name() = 'houseNumber']"/>
                                <xsl:variable name="buildingNumberSuffix2" select="$additionalLocator/following-sibling::hl7:*[1][local-name() = 'buildingNumberSuffix'] | 
                                                                                   $additionalLocator/following-sibling::hl7:*[1][local-name() = 'houseNumberNumeric' or local-name() = 'houseNumber']/following-sibling::hl7:*[1][local-name() = 'buildingNumberSuffix']"/>
                                
                                <xsl:choose>
                                    <xsl:when test="string-length($houseNumber2) > 0">
                                        <xsl:text>&#160;</xsl:text>
                                        <xsl:value-of select="$houseNumber2"/>
                                        <xsl:if test="string-length($buildingNumberSuffix2) > 0">
                                            <xsl:text>&#160;</xsl:text>
                                            <xsl:value-of select="$buildingNumberSuffix2"/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="string-length($buildingNumberSuffix2) > 0">
                                        <xsl:text>&#160;</xsl:text>
                                        <xsl:value-of select="$buildingNumberSuffix2"/>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:if>
                            <xsl:if test="following-sibling::*[not(local-name() = 'houseNumber' or local-name() = 'houseNumberNumeric' or local-name() = 'buildingNumberSuffix' or local-name() = 'additionalLocator')][string-length(.) > 0 or @code]">
                                <xsl:choose>
                                    <xsl:when test="$withLineBreaks">
                                        <br/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      <xsl:text> </xsl:text>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:if>
                        </xsl:if>
                    </xsl:when>
                    <!-- DTr2 only if not streetAddressLine -->
                    <xsl:when test="self::hl7:part[@type='STR']">
                        <xsl:if test="not(../hl7:part[@type='SAL'])">
                            <xsl:variable name="additionalLocator" select="following-sibling::hl7:part[1][@type='ADL'] |
                                                                           following-sibling::hl7:part[1][@type='BNN' or @type = 'BNR' or @type = 'BNS']/following-sibling::hl7:part[1][@type = 'ADL'] | 
                                                                           following-sibling::hl7:part[1][@type='BNN' or @type = 'BNR' or @type = 'BNS']/following-sibling::hl7:part[1][@type = 'BNS']/following-sibling::hl7:part[1][@type = 'ADL']"/>
                            <xsl:variable name="houseNumber" select="following-sibling::hl7:part[1][@type = 'BNN'] | 
                                following-sibling::hl7:part[1][@type = 'BNR']"/>
                            <xsl:variable name="buildingNumberSuffix" select="following-sibling::hl7:part[1][@type = 'BNS'] | 
                                following-sibling::hl7:part[1][@type = 'BNN' or @type = 'BNR']/following-sibling::hl7:part[1][@type = 'BNS']"/>
                            <!-- 
                                Look for
                                - streetName houseNumber|houseNumberNumeric|buildingNumberSuffix
                                - streetName houseNumber|houseNumberNumeric|buildingNumberSuffix additionalLocator houseNumber|houseNumberNumeric|buildingNumberSuffix
                                - streetName additionalLocator houseNumber|houseNumberNumeric|buildingNumberSuffix
                                in that order and nothing in between.
                            -->
                            <xsl:value-of select="."/>
                            <xsl:choose>
                                <xsl:when test="string-length($houseNumber) > 0">
                                    <xsl:text>&#160;</xsl:text>
                                    <xsl:value-of select="$houseNumber"/>
                                    <xsl:if test="string-length($buildingNumberSuffix) > 0">
                                        <xsl:text>&#160;</xsl:text>
                                        <xsl:value-of select="$buildingNumberSuffix"/>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when test="string-length($buildingNumberSuffix) > 0">
                                    <xsl:text>&#160;</xsl:text>
                                    <xsl:value-of select="$buildingNumberSuffix"/>
                                </xsl:when>
                            </xsl:choose>
                            <xsl:if test="string-length($additionalLocator) > 0">
                                <xsl:text>&#160;</xsl:text>
                                <xsl:value-of select="$additionalLocator"/>
                                
                                <xsl:variable name="houseNumber2" select="$additionalLocator/following-sibling::hl7:part[1][@type = 'BNN'] | 
                                    $additionalLocator/following-sibling::hl7:part[1][@type = 'BNR']"/>
                                <xsl:variable name="buildingNumberSuffix2" select="$additionalLocator/following-sibling::hl7:part[1][@type = 'BNS'] | 
                                    $additionalLocator/following-sibling::hl7:part[1][@type = 'BNN' or @type = 'BNR']/following-sibling::hl7:part[1][@type = 'BNS']"/>
                                
                                <xsl:choose>
                                    <xsl:when test="string-length($houseNumber2) > 0">
                                        <xsl:text>&#160;</xsl:text>
                                        <xsl:value-of select="$houseNumber2"/>
                                        <xsl:if test="string-length($buildingNumberSuffix2) > 0">
                                            <xsl:text>&#160;</xsl:text>
                                            <xsl:value-of select="$buildingNumberSuffix2"/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="string-length($buildingNumberSuffix2) > 0">
                                        <xsl:text>&#160;</xsl:text>
                                        <xsl:value-of select="$buildingNumberSuffix2"/>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:if>
                            <xsl:if test="following-sibling::*[not(@type = 'BNR' or local-name() = 'houseNumberNumeric' or @type = 'BNS' or @type = 'ADL')][string-length(.) > 0 or @code]">
                                <xsl:choose>
                                    <xsl:when test="$withLineBreaks">
                                        <br/>
                                    </xsl:when>
                                    <xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
                                </xsl:choose>
                            </xsl:if>
                        </xsl:if>
                    </xsl:when>
                    <!-- DTr1 only if not streetAddressLine -->
                    <xsl:when test="self::hl7:houseNumber or self::hl7:houseNumberNumeric or self::hl7:buildingNumberSuffix">
                        <xsl:if test="not(../hl7:streetAddressLine)">
                            <xsl:if test="not(preceding-sibling::hl7:*[1][local-name() = 'streetName' or local-name() = 'additionalLocator'])">
                                <xsl:if test="not(self::hl7:buildingNumberSuffix and preceding-sibling::hl7:*[1][local-name() = 'houseNumberNumeric' or local-name() = 'houseNumber'])">
                                    <xsl:value-of select="."/>
                                    <xsl:if test="following-sibling::hl7:*[1][string-length(.) > 0 or @code]">
                                        <xsl:choose>
                                            <xsl:when test="$withLineBreaks">
                                                <br/>
                                            </xsl:when>
                                            <xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:if>
                                </xsl:if>
                            </xsl:if>
                        </xsl:if>
                    </xsl:when>
                    <!-- DTr2 only if not streetAddressLine -->
                    <xsl:when test="self::hl7:part[@type = 'BNN' or @type = 'BNR' or @type = 'BNS']">
                        <xsl:if test="not(../hl7:part[@type = 'SAL'])">
                            <xsl:if test="not(preceding-sibling::hl7:*[1][hl7:part[@type = 'STR' or @type = 'ADL']])">
                                <xsl:if test="not(self::hl7:part[@type = 'BNS'] and preceding-sibling::hl7:*[1][@type = 'BNN' or @type = 'BNR'])">
                                    <xsl:value-of select="@value"/>
                                    <xsl:if test="following-sibling::hl7:part[1][string-length(@value) > 0 or @code]">
                                         <xsl:choose>
                                            <xsl:when test="$withLineBreaks">
                                                <br/>
                                            </xsl:when>
                                            <xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:if>
                                </xsl:if>
                            </xsl:if>
                        </xsl:if>
                    </xsl:when>
                    <!-- DTr1 -->
                    <xsl:when test="self::hl7:additionalLocator">
                        <xsl:if test="not(preceding-sibling::hl7:*[1][local-name() = 'houseNumber' or local-name() = 'houseNumberNumeric' or local-name() = 'buildingNumberSuffix'])">
                            <xsl:value-of select="."/>
                            <xsl:if test="following-sibling::hl7:*[1][local-name() = 'houseNumberNumeric']">
                                <xsl:text>&#160;</xsl:text>
                                <xsl:value-of select="following-sibling::hl7:*[1][local-name() = 'houseNumberNumeric']"/>
                            </xsl:if>
                            <xsl:if test="following-sibling::hl7:*[1][local-name() = 'houseNumber']">
                                <xsl:text>&#160;</xsl:text>
                                <xsl:value-of select="following-sibling::hl7:*[1][local-name() = 'houseNumber']"/>
                            </xsl:if>
                            <xsl:if test="following-sibling::hl7:*[1][local-name() = 'buildingNumberSuffix']">
                                <xsl:text>&#160;</xsl:text>
                                <xsl:value-of select="following-sibling::hl7:*[1][local-name() = 'buildingNumberSuffix']"/>
                            </xsl:if>
                            <xsl:if test="following-sibling::hl7:*[1][string-length(.) > 0 or @code]">
                                <xsl:choose>
                                    <xsl:when test="$withLineBreaks">
                                        <br/>
                                    </xsl:when>
                                    <xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
                                </xsl:choose>
                            </xsl:if>
                        </xsl:if>
                    </xsl:when>
                    <!-- DTr2 -->
                    <xsl:when test="self::hl7:part[@type = 'ADL']">
                        <xsl:if test="not(preceding-sibling::hl7:*[1][@type = 'BNN' or @type = 'BNR' or @type = 'BNS'])">
                            <xsl:value-of select="@value"/>
                            <xsl:if test="following-sibling::hl7:*[1][@type = 'BNN']">
                                <xsl:text>&#160;</xsl:text>
                                <xsl:value-of select="following-sibling::hl7:*[1][@type = 'BNN']/@value"/>
                            </xsl:if>
                            <xsl:if test="following-sibling::hl7:*[1][@type = 'BNR']">
                                <xsl:text>&#160;</xsl:text>
                                <xsl:value-of select="following-sibling::hl7:*[1][@type = 'BNR']/@value"/>
                            </xsl:if>
                            <xsl:if test="following-sibling::hl7:*[1][@type = 'BNS']">
                                <xsl:text>&#160;</xsl:text>
                                <xsl:value-of select="following-sibling::hl7:*[1][@type = 'BNS']/@value"/>
                            </xsl:if>
                            <xsl:if test="following-sibling::hl7:part[1][string-length(@value) > 0 or @code]">
                                <xsl:choose>
                                    <xsl:when test="$withLineBreaks">
                                        <br/>
                                    </xsl:when>
                                    <xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
                                </xsl:choose>
                            </xsl:if>
                        </xsl:if>
                    </xsl:when>
                    <!-- DTr1 -->
                    <xsl:when test="self::hl7:postBox">
                        <xsl:call-template name="getLocalizedString">
                            <xsl:with-param name="key" select="'Postbox'"/>
                            <xsl:with-param name="post" select="' '"/>
                        </xsl:call-template>
                        <xsl:value-of select="."/>
                        <xsl:if test="following-sibling::hl7:*[1][string-length(.) &gt; 0 or @code]">
                            <xsl:choose>
                                <xsl:when test="$withLineBreaks">
                                    <br/>
                                </xsl:when>
                                <xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:when>
                    <!-- DTr2 -->
                    <xsl:when test="self::hl7:part[@type = 'POB']">
                        <xsl:call-template name="getLocalizedString">
                            <xsl:with-param name="key" select="'Postbox'"/>
                            <xsl:with-param name="post" select="' '"/>
                        </xsl:call-template>
                        <xsl:value-of select="@value"/>
                        <xsl:if test="following-sibling::hl7:part[1][string-length(@value) &gt; 0 or @code]">
                            <xsl:choose>
                                <xsl:when test="$withLineBreaks">
                                    <br/>
                                </xsl:when>
                                <xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:when>
                    <!-- DTr1 ZIP CITY, STATE or CITY, STATE ZIP depending on addr part contents -->
                    <xsl:when test="self::hl7:city">
                        <xsl:if test="preceding-sibling::hl7:postalCode[1][string-length(.) > 0 or @code]">
                            <xsl:choose>
                                <xsl:when test="preceding-sibling::hl7:postalCode[1][string-length(.) > 0]">
                                    <xsl:value-of select="preceding-sibling::hl7:postalCode[1][string-length(.) > 0]"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="preceding-sibling::hl7:postalCode[1][@code]/@code"/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:text> </xsl:text>
                        </xsl:if>
                        <xsl:value-of select="."/>
                        <xsl:if test="../hl7:state[string-length(.) > 0]">
                            <xsl:text>, </xsl:text>
                            <xsl:value-of select="../hl7:state"/>
                        </xsl:if>
                        <xsl:if test="following-sibling::hl7:postalCode[1][string-length(.) > 0 or @code]">
                            <xsl:text> </xsl:text>
                            <xsl:choose>
                                <xsl:when test="following-sibling::hl7:postalCode[1][string-length(.) > 0]">
                                    <xsl:value-of select="following-sibling::hl7:postalCode[1][string-length(.) > 0]"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="following-sibling::hl7:postalCode[1][@code]/@code"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                        <xsl:if test="following-sibling::hl7:*[1][string-length(.) > 0 or @code]">
                            <xsl:choose>
                                <xsl:when test="$withLineBreaks">
                                    <br/>
                                </xsl:when>
                                <xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:when>
                    <!-- DTr2 ZIP CITY, STATE or CITY, STATE ZIP depending on addr part contents -->
                    <xsl:when test="self::hl7:part[@type = 'CTY']">
                        <xsl:if test="preceding-sibling::hl7:part[@type = 'ZIP'][1][string-length(@value) > 0 or @code]">
                            <xsl:choose>
                                <xsl:when test="preceding-sibling::hl7:postalCode[1][string-length(@value) > 0]">
                                    <xsl:value-of select="preceding-sibling::hl7:postalCode[1][string-length(@value) > 0]/@value"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="preceding-sibling::hl7:postalCode[1][@code]/@code"/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:text> </xsl:text>
                        </xsl:if>
                        <xsl:value-of select="@value"/>
                        <xsl:if test="../hl7:part[@type = 'STA'][string-length(@value) > 0]">
                            <xsl:text>, </xsl:text>
                            <xsl:value-of select="../hl7:part[@type = 'STA']/@value"/>
                        </xsl:if>
                        <xsl:if test="following-sibling::hl7:part[@type = 'ZIP'][1][string-length(@value) > 0 or @code]">
                            <xsl:choose>
                                <xsl:when test="following-sibling::hl7:postalCode[1][string-length(@value) > 0]">
                                    <xsl:value-of select="following-sibling::hl7:postalCode[1][string-length(@value) > 0]/@value"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="following-sibling::hl7:postalCode[1][@code]/@code"/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:text> </xsl:text>
                        </xsl:if>
                        <xsl:if test="following-sibling::hl7:part[1][string-length(@value) > 0 or @code]">
                            <xsl:choose>
                                <xsl:when test="$withLineBreaks">
                                    <br/>
                                </xsl:when>
                                <xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:when>
                    <!-- DTr1 -->
                    <xsl:when test="self::hl7:postalCode and ../hl7:city"/>
                    <!-- DTr2 -->
                    <xsl:when test="self::hl7:part[@type = 'ZIP'] and ../hl7:part[@type = 'CTY']"/>
                    <!-- DTr1 -->
                    <xsl:when test="self::hl7:state">
                        <xsl:if test="not(../hl7:city)">
                            <xsl:if test="(string-length(preceding-sibling::hl7:*[1]) > 0 or preceding-sibling::*/@code)">
                                <xsl:choose>
                                    <xsl:when test="$withLineBreaks">
                                        <br/>
                                    </xsl:when>
                                    <xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
                                </xsl:choose>
                            </xsl:if>
                            <xsl:value-of select="."/>
                            <xsl:if test="(string-length(following-sibling::hl7:*[1]) > 0 or following-sibling::*/@code)">
                                <xsl:choose>
                                    <xsl:when test="$withLineBreaks">
                                        <br/>
                                    </xsl:when>
                                    <xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
                                </xsl:choose>
                            </xsl:if>
                        </xsl:if>
                    </xsl:when>
                    <!-- DTr2 -->
                    <xsl:when test="self::hl7:part[@type = 'STA']">
                        <xsl:if test="not(../hl7:part[@type = 'CTY'])">
                            <xsl:if test="(string-length(preceding-sibling::hl7:*[1]/@value) > 0 or preceding-sibling::hl7:*/@code)">
                                <xsl:choose>
                                    <xsl:when test="$withLineBreaks">
                                        <br/>
                                    </xsl:when>
                                    <xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
                                </xsl:choose>
                            </xsl:if>
                            <xsl:value-of select="@value"/>
                            <xsl:if test="(string-length(following-sibling::hl7:*[1]/@value) > 0 or following-sibling::hl7:*/@code)">
                                <xsl:choose>
                                    <xsl:when test="$withLineBreaks">
                                        <br/>
                                    </xsl:when>
                                    <xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
                                </xsl:choose>
                            </xsl:if>
                        </xsl:if>
                    </xsl:when>
                    <!-- DTr1 -->
                    <xsl:when test="string-length(text()) &gt; 0">
                        <xsl:value-of select="."/>
                        <xsl:if test="(string-length(following-sibling::hl7:*[1]) &gt; 0 or following-sibling::hl7:*/@code)">
                            <xsl:choose>
                                <xsl:when test="$withLineBreaks">
                                    <br/>
                                </xsl:when>
                                <xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:when>
                    <!-- DTr2 -->
                    <xsl:when test="string-length(@value) &gt; 0">
                        <xsl:value-of select="@value"/>
                        <xsl:if test="(string-length(following-sibling::hl7:*[1]/@value) &gt; 0 or following-sibling::hl7:*/@code)">
                            <xsl:choose>
                                <xsl:when test="$withLineBreaks">
                                    <br/>
                                </xsl:when>
                                <xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:when>
                    <xsl:otherwise> </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
            <xsl:for-each select="$in/hl7:useablePeriod">
                <div>
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="'Period'"/>
                    </xsl:call-template>
                    <xsl:text> </xsl:text>
                    <xsl:call-template name="show-ivlts">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </div>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Show elements with datatype QTY/PQ separated with the value in 'sep'. Calls <xd:ref name="show-quantity" type="template">show-quantity</xd:ref>
            </xd:p>
        </xd:desc>
        <xd:param name="in">Set of 0 to * elements</xd:param>
        <xd:param name="sep">Separator between output of different elements. Default ', ' and special is 'br' which generates an HTML br tag</xd:param>
    </xd:doc>
    <xsl:template name="show-quantity-set">
        <xsl:param name="in"/>
        <xsl:param name="sep" select="', '"/>
        <xsl:if test="$in">
            <xsl:choose>
                <!-- DTr1 -->
                <xsl:when test="count($in) &gt; 1">
                    <xsl:for-each select="$in">
                        <xsl:call-template name="show-quantity">
                            <xsl:with-param name="in" select="."/>
                        </xsl:call-template>
                        <xsl:if test="position() != last()">
                            <xsl:choose>
                                <xsl:when test="$sep = 'br'">
                                    <br/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$sep"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:when>
                <!-- DTr2 -->
                <xsl:when test="$in[hl7:item]">
                    <xsl:for-each select="$in/hl7:item">
                        <xsl:call-template name="show-quantity">
                            <xsl:with-param name="in" select="."/>
                        </xsl:call-template>
                        <xsl:if test="position() != last()">
                            <xsl:choose>
                                <xsl:when test="$sep = 'br'">
                                    <br/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$sep"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:when>
                <!-- DTr1 or DTr2 -->
                <xsl:otherwise>
                    <xsl:call-template name="show-quantity">
                        <xsl:with-param name="in" select="$in"/>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Show element with datatype QTY/PQ</xd:p>
        </xd:desc>
        <xd:param name="in">One element, possibly out of a set</xd:param>
    </xd:doc>
    <xsl:template name="show-quantity">
        <xsl:param name="in" as="element()?"/>
        <xsl:if test="$in">
            <xsl:value-of select="string-join(($in/@value, $in/@unit[not(. = '1')]), ' ')"/>
            <xsl:if test="$in[@value | @unit]">
                <xsl:text> </xsl:text>
            </xsl:if>
            <xsl:if test="$in[@nullFlavor]">
                <xsl:text>(</xsl:text>
                <xsl:call-template name="show-nullFlavor">
                    <xsl:with-param name="in" select="$in/@nullFlavor"/>
                </xsl:call-template>
                <xsl:text>)</xsl:text>
            </xsl:if>
            <xsl:for-each select="$in/*:translation">
                <div style="margin-left: 2em;">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="local-name()"/>
                        <xsl:with-param name="post" select="' '"/>
                    </xsl:call-template>
                    <xsl:call-template name="show-code-set">
                        <xsl:with-param name="in" select="."/>
                    </xsl:call-template>
                </div>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xd:doc>
        <xd:desc>Show element with datatype IVL_TS</xd:desc>
        <xd:param name="in">One element, possibly out of a set</xd:param>
    </xd:doc>
    <xsl:template name="show-ivlts">
        <xsl:param name="in"/>
        <xsl:if test="$in">
            <xsl:variable name="fromDate">
                <xsl:call-template name="show-timestamp">
                    <xsl:with-param name="in" select="$in/hl7:low"/>
                    <xsl:with-param name="part" select="'date'"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:variable name="toDate">
                <xsl:call-template name="show-timestamp">
                    <xsl:with-param name="in" select="$in/hl7:high"/>
                    <xsl:with-param name="part" select="'date'"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:variable name="fromTime">
                <xsl:call-template name="show-timestamp">
                    <xsl:with-param name="in" select="$in/hl7:low"/>
                    <xsl:with-param name="part" select="'time'"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:variable name="toTime">
                <xsl:call-template name="show-timestamp">
                    <xsl:with-param name="in" select="$in/hl7:high"/>
                    <xsl:with-param name="part" select="'time'"/>
                </xsl:call-template>
            </xsl:variable>
            
            <xsl:choose>
                <xsl:when test="$in/@value">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="'at'"/>
                        <xsl:with-param name="post" select="'&#160;'"/>
                    </xsl:call-template>
                    <xsl:call-template name="show-timestamp">
                        <xsl:with-param name="in" select="$in"/>
                    </xsl:call-template>
                </xsl:when>
                <!-- Same day, different times -->
                <xsl:when test="$fromDate = $toDate">
                    <xsl:call-template name="show-timestamp">
                        <xsl:with-param name="in" select="$in/hl7:low"/>
                        <xsl:with-param name="part" select="'date'"/>
                    </xsl:call-template>
                    
                    <xsl:if test="string-length(normalize-space($fromTime)) > 0">
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="normalize-space($fromTime)"/>
                        
                        <xsl:if test="string-length(normalize-space($toTime)) > 0">
                            <xsl:text> - </xsl:text>
                            <xsl:value-of select="normalize-space($toTime)"/>
                        </xsl:if>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="$in/hl7:low">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="'from'"/>
                        <xsl:with-param name="post" select="'&#160;'"/>
                    </xsl:call-template>
                    <xsl:call-template name="show-timestamp">
                        <xsl:with-param name="in" select="$in/hl7:low"/>
                    </xsl:call-template>
                    <xsl:if test="$in/hl7:high">
                        <xsl:call-template name="getLocalizedString">
                            <xsl:with-param name="pre" select="' '"/>
                            <xsl:with-param name="key" select="'to'"/>
                            <xsl:with-param name="post" select="'&#160;'"/>
                        </xsl:call-template>
                        <xsl:call-template name="show-timestamp">
                            <xsl:with-param name="in" select="$in/hl7:high"/>
                        </xsl:call-template>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="$in/hl7:high">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="'to'"/>
                        <xsl:with-param name="post" select="'&#160;'"/>
                    </xsl:call-template>
                    <xsl:call-template name="show-timestamp">
                        <xsl:with-param name="in" select="$in/hl7:high"/>
                    </xsl:call-template>
                </xsl:when>
            </xsl:choose>
        </xsl:if>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>
            <xd:p>Show elements with datatype TEL or URI separated with the value in 'sep'. Calls <xd:ref name="show-telecom" type="template">show-telecom</xd:ref></xd:p>
        </xd:desc>
        <xd:param name="in">Set of 0 to * elements</xd:param>
        <xd:param name="sep">Separator between output of different elements. Default ', ' and special is 'br' which generates an HTML br tag</xd:param>
    </xd:doc>
    <xsl:template name="show-telecom-set">
        <xsl:param name="in"/>
        <xsl:param name="sep" select="', '"/>
        <xsl:if test="$in">
            <xsl:choose>
                <!-- DTr1 -->
                <xsl:when test="count($in) &gt; 1">
                    <xsl:for-each select="$in">
                        <xsl:call-template name="show-telecom">
                            <xsl:with-param name="in" select="."/>
                        </xsl:call-template>
                        <xsl:if test="position() != last()">
                            <xsl:choose>
                                <xsl:when test="$sep = 'br'">
                                    <br/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$sep"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:when>
                <!-- DTr2 -->
                <xsl:when test="$in[hl7:item]">
                    <xsl:for-each select="$in/hl7:item">
                        <xsl:call-template name="show-telecom">
                            <xsl:with-param name="in" select="."/>
                        </xsl:call-template>
                        <xsl:if test="position() != last()">
                            <xsl:choose>
                                <xsl:when test="$sep = 'br'">
                                    <br/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$sep"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:when>
                <!-- DTr1 or DTr2 -->
                <xsl:otherwise>
                    <xsl:call-template name="show-telecom">
                        <xsl:with-param name="in" select="$in"/>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Show element with datatype TEL or URI</xd:p>
        </xd:desc>
        <xd:param name="in">One element, possibly out of a set</xd:param>
    </xd:doc>
    <xsl:template name="show-telecom">
        <xsl:param name="in"/>
        <xsl:choose>
            <xsl:when test="$in">
                <xsl:for-each select="$in">
                    <xsl:if test="position() &gt; 1">
                        <br/>
                    </xsl:if>
                    
                    <xsl:variable name="type" select="substring-before(@value, ':')"/>
                    <xsl:variable name="value" select="substring-after(@value, ':')"/>
                    <xsl:if test="$type">
                        <xsl:call-template name="translateTelecomUriScheme">
                            <xsl:with-param name="code" select="$type"/>
                        </xsl:call-template>
                    </xsl:if>
                    <xsl:if test="@use">
                        <xsl:text> </xsl:text>
                        <xsl:call-template name="tokenize">
                            <xsl:with-param name="prefix" select="'addressUse_'"/>
                            <xsl:with-param name="string" select="@use"/>
                            <xsl:with-param name="delimiters" select="' '"/>
                        </xsl:call-template>
                    </xsl:if>
                    <xsl:if test="$type or @use">
                        <xsl:text>: </xsl:text>
                    </xsl:if>
                    <xsl:choose>
                        <xsl:when test="$type">
                            <xsl:value-of select="$value"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="@value"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'telecom information not available'"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Show element with datatype TS</xd:p>
        </xd:desc>
        <xd:param name="in">One element, possibly out of a set</xd:param>
        <xd:param name="part">value to tell if we want the full thing 'datetime', date only 'date', or time only 'time'</xd:param>
    </xd:doc>
    <xsl:template name="show-timestamp">
        <xsl:param name="in"/>
        <xsl:param name="part" select="'datetime'"/>
        
        <xsl:call-template name="formatDateTime">
            <xsl:with-param name="date" select="$in/@value"/>
            <xsl:with-param name="part" select="$part"/>
        </xsl:call-template>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Show elements with datatype ST separated with the value in 'sep'. Calls <xd:ref name="show-text" type="template">show-text</xd:ref></xd:p>
        </xd:desc>
        <xd:param name="in">Set of 0 to * elements</xd:param>
        <xd:param name="sep">Separator between output of different elements. Default ', ' and special is 'br' which generates an HTML br tag</xd:param>
    </xd:doc>
    <xsl:template name="show-text-set">
        <xsl:param name="in"/>
        <xsl:param name="sep" select="', '"/>
        <xsl:if test="$in">
            <xsl:choose>
                <!-- DTr1 -->
                <xsl:when test="count($in) &gt; 1">
                    <xsl:for-each select="$in">
                        <xsl:call-template name="show-text">
                            <xsl:with-param name="in" select="."/>
                        </xsl:call-template>
                        <xsl:if test="position() != last()">
                            <xsl:choose>
                                <xsl:when test="$sep = 'br'">
                                    <br/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$sep"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:when>
                <!-- DTr2 -->
                <xsl:when test="$in[hl7:item]">
                    <xsl:for-each select="$in/hl7:item">
                        <xsl:call-template name="show-text">
                            <xsl:with-param name="in" select="."/>
                        </xsl:call-template>
                        <xsl:if test="position() != last()">
                            <xsl:choose>
                                <xsl:when test="$sep = 'br'">
                                    <br/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$sep"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:when>
                <!-- DTr1 or DTr2 -->
                <xsl:otherwise>
                    <xsl:call-template name="show-text">
                        <xsl:with-param name="in" select="$in"/>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Show element with datatype ST</xd:p>
        </xd:desc>
        <xd:param name="in">One element, possibly out of a set</xd:param>
    </xd:doc>
    <xsl:template name="show-text">
        <xsl:param name="in"/>
        <xsl:if test="$in">
            <xsl:choose>
                <!-- DTr1 and DTr2 -->
                <xsl:when test="$in[@nullFlavor]">
                    <xsl:call-template name="show-nullFlavor">
                        <xsl:with-param name="in" select="$in/@nullFlavor"/>
                    </xsl:call-template>
                </xsl:when>
                <!-- DTr2 -->
                <xsl:when test="$in[@value]">
                    <xsl:copy-of select="translate($in/@value, '&#xD;&#xA;', '&lt;br/&gt;')"/>
                </xsl:when>
                <!-- DTr1 -->
                <xsl:otherwise>
                    <xsl:copy-of select="translate($in/string-join(text(), ''), '&#xD;&#xA;', '&lt;br/&gt;')"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Show element with datatype BL/BN</xd:p>
        </xd:desc>
        <xd:param name="in">One element, possibly out of a set</xd:param>
    </xd:doc>
    <xsl:template name="show-boolean">
        <xsl:param name="in"/>
        <xsl:if test="$in">
            <xsl:choose>
                <!-- DTr1 and DTr2 -->
                <xsl:when test="$in[@value]">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="concat('boolean-', $in/@value)"/>
                    </xsl:call-template>
                </xsl:when>
                <!-- DTr1 and DTr2 -->
                <xsl:when test="$in[@nullFlavor]">
                    <xsl:call-template name="show-nullFlavor">
                        <xsl:with-param name="in" select="$in/@nullFlavor"/>
                    </xsl:call-template>
                </xsl:when>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Show a nullFlavor as text</xd:p>
        </xd:desc>
        <xd:param name="in">The nullFlavor code, e.g. NI, OTH</xd:param>
    </xd:doc>
    <xsl:template name="show-nullFlavor">
        <xsl:param name="in"/>
        <xsl:if test="string-length($in) &gt; 0">
            <xsl:call-template name="getLocalizedString">
                <xsl:with-param name="key" select="concat('nullFlavor_', $in)"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Get localized string for a classCode</xd:p>
        </xd:desc>
        <xd:param name="clsCode">Class code string</xd:param>
    </xd:doc>
    <xsl:template name="show-actClassCode">
        <xsl:param name="clsCode"/>
        <xsl:variable name="key" select="concat('2.16.840.1.113883.5.6-', $clsCode)"/>
        <xsl:variable name="label">
            <xsl:call-template name="getLocalizedString">
                <xsl:with-param name="key" select="$key"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="$key = $label">
                <xsl:value-of select="$clsCode"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$label"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>
            <xd:p>Get localized string for a typeCode from an act relationship</xd:p>
        </xd:desc>
        <xd:param name="ptype">ActRelationship type string</xd:param>
    </xd:doc>
    <xsl:template name="show-actRelationship">
        <xsl:param name="ptype"/>
        <xsl:variable name="key" select="concat('2.16.840.1.113883.5.1002-', $ptype)"/>
        <xsl:variable name="label">
            <xsl:call-template name="getLocalizedString">
                <xsl:with-param name="key" select="$key"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="$key = $label">
                <xsl:value-of select="$ptype"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$label"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>
            <xd:p>Get localized string for a typeCode from a participation</xd:p>
        </xd:desc>
        <xd:param name="ptype">Participation type string</xd:param>
    </xd:doc>
    <xsl:template name="show-participationType">
        <xsl:param name="ptype"/>
        <xsl:variable name="key" select="concat('2.16.840.1.113883.5.90-', $ptype)"/>
        <xsl:variable name="label">
            <xsl:call-template name="getLocalizedString">
                <xsl:with-param name="key" select="$key"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="$key = $label">
                <xsl:value-of select="$ptype"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$label"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>
            <xd:p>Get localized string for a moodCode from an act relationship</xd:p>
        </xd:desc>
        <xd:param name="in">The moodCode</xd:param>
    </xd:doc>
    <xsl:template name="show-moodCode">
        <xsl:param name="in"/>
        <xsl:if test="$in != 'EVN'">
            <xsl:variable name="key" select="concat('moodCode-', $in)"/>
            <xsl:variable name="label">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="$key"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="$key = $label">
                    <xsl:value-of select="$in"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$label"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>
    
    <!-- ====================================================================== -->
    <!--                           Supporting functions                         -->
    <!-- ====================================================================== -->

    <xd:doc>
        <xd:desc>
            <xd:p>Takes the 5th and 6th character from a timestamp, and returns the localized month name</xd:p>
        </xd:desc>
        <xd:param name="date">Timestamp string</xd:param>
    </xd:doc>
    <xsl:template name="formatMonth">
        <xsl:param name="date"/>
        <!-- month -->
        <xsl:variable name="month" select="substring($date, 5, 2)"/>
        <xsl:choose>
            <xsl:when test="$month = '01'">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'January'"/>
                    <xsl:with-param name="post" select="' '"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$month = '02'">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'February'"/>
                    <xsl:with-param name="post" select="' '"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$month = '03'">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'March'"/>
                    <xsl:with-param name="post" select="' '"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$month = '04'">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'April'"/>
                    <xsl:with-param name="post" select="' '"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$month = '05'">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'May'"/>
                    <xsl:with-param name="post" select="' '"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$month = '06'">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'June'"/>
                    <xsl:with-param name="post" select="' '"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$month = '07'">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'July'"/>
                    <xsl:with-param name="post" select="' '"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$month = '08'">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'August'"/>
                    <xsl:with-param name="post" select="' '"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$month = '09'">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'September'"/>
                    <xsl:with-param name="post" select="' '"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$month = '10'">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'October'"/>
                    <xsl:with-param name="post" select="' '"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$month = '11'">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'November'"/>
                    <xsl:with-param name="post" select="' '"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$month = '12'">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'December'"/>
                    <xsl:with-param name="post" select="' '"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$month"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Formats a timestamp</xd:p>
        </xd:desc>
        <xd:param name="date"/>
        <xd:param name="part">value to tell if we want the full thing 'datetime', date only 'date', or time only 'time'</xd:param>
    </xd:doc>
    <xsl:template name="formatDateTime">
        <xsl:param name="date"/>
        <xsl:param name="part" select="'datetime'"/>
        
        <xsl:variable name="yearNum" select="substring($date, 1, 4)"/>
        <xsl:variable name="monthNum" select="substring($date, 5, 2)"/>
        <xsl:variable name="monthText">
            <xsl:call-template name="formatMonth">
                <xsl:with-param name="date" select="$date"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="dayNum">
            <xsl:choose>
                <xsl:when test="substring($date, 7, 1) = '0'">
                    <xsl:value-of select="substring($date, 8, 1)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="substring($date, 7, 2)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:if test="not($part = 'time')">
            <xsl:choose>
                <xsl:when test="$textLangPartLowerCase = 'nl'">
                    <xsl:value-of select="$dayNum"/>
                    <xsl:text> </xsl:text>
                    <xsl:call-template name="caseDown">
                        <xsl:with-param name="data" select="$monthText"/>
                    </xsl:call-template>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$yearNum"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="firstCharCaseUp">
                        <xsl:with-param name="data" select="$monthText"/>
                    </xsl:call-template>
                    <xsl:if test="string-length($dayNum) > 0">
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="$dayNum"/>
                    </xsl:if>
                    <xsl:if test="string-length($yearNum) > 0">
                        <xsl:text>, </xsl:text>
                        <xsl:value-of select="$yearNum"/>
                    </xsl:if>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>

        <!-- time and US timezone -->
        <xsl:if test="string-length($date) > 8 and not($part = 'date')">
            <xsl:if test="not($part = 'time')">
                <xsl:text>, </xsl:text>
            </xsl:if>
            <!-- time -->
            <xsl:variable name="time">
                <xsl:value-of select="substring($date, 9, 6)"/>
            </xsl:variable>
            <xsl:variable name="hh">
                <xsl:value-of select="substring($time, 1, 2)"/>
            </xsl:variable>
            <xsl:variable name="mm">
                <xsl:value-of select="substring($time, 3, 2)"/>
            </xsl:variable>
            <xsl:variable name="ss">
                <xsl:value-of select="substring($time, 5, 2)"/>
            </xsl:variable>
            <xsl:if test="string-length($hh) &gt; 1">
                <xsl:choose>
                    <xsl:when test="$textLangPartLowerCase = 'en' and number($hh) > 12">
                        <xsl:value-of select="number($hh) - 12"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$hh"/>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:choose>
                    <xsl:when test="string-length($mm) > 1 and not(contains($mm, '-')) and not(contains($mm, '+')) and not($mm = '00' and $ss = '00')">
                        <xsl:text>:</xsl:text>
                        <xsl:value-of select="$mm"/>
                        <xsl:if test="string-length($ss) > 1 and not(contains($ss, '-')) and not(contains($ss, '+')) and not($ss = '00')">
                            <xsl:text>:</xsl:text>
                            <xsl:value-of select="$ss"/>
                        </xsl:if>
                    </xsl:when>
                    <xsl:when test="$textLangPartLowerCase = 'nl'">
                        <xsl:call-template name="getLocalizedString">
                            <xsl:with-param name="key" select="'h'"/>
                        </xsl:call-template>
                    </xsl:when>
                </xsl:choose>
                <xsl:if test="$textLangPartLowerCase = 'en'">
                    <xsl:choose>
                        <xsl:when test="number($hh) > 12">
                            <xsl:text>PM</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>AM</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:if>
            </xsl:if>
            <!-- time zone. Don't try getting a name for it as that will always fail parts of the year due to daylight savings -->
            <xsl:choose>
                <xsl:when test="contains($date, '+')">
                    <xsl:text> +</xsl:text>
                    <xsl:value-of select="substring-after($date, '+')"/>
                </xsl:when>
                <xsl:when test="contains($date, '-')">
                    <xsl:text> -</xsl:text>
                    <xsl:value-of select="substring-after($date, '-')"/>
                </xsl:when>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Get someones age based on the difference between 'date' and <xd:ref name="currentDate" type="parameter">currentDate</xd:ref>.</xd:p>
        </xd:desc>
        <xd:param name="date">Persons date of birth as HL7 TS</xd:param>
        <xd:param name="comparedate">The date, format yyyymmdd as HL7 TS, that the age should be calculated relative to</xd:param>
    </xd:doc>
    <xsl:template name="getAge">
        <xsl:param name="comparedate"/>
        <xsl:param name="date"/>
        <xsl:variable name="yearNum" select="substring($date, 1, 4)"/>
        <xsl:variable name="monthNum" select="substring($date, 5, 2)"/>
        <xsl:variable name="dayNum">
            <xsl:choose>
                <xsl:when test="substring ($date, 7, 1)=&#34;0&#34;">
                    <xsl:value-of select="substring($date, 8, 1)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="substring($date, 7, 2)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="yearNumCreate" select="substring($comparedate, 1, 4)"/>
        <xsl:variable name="monthNumCreate" select="substring($comparedate, 5, 2)"/>
        <xsl:variable name="dayNumCreate">
            <xsl:choose>
                <xsl:when test="substring ($comparedate, 7, 1)=&#34;0&#34;">
                    <xsl:value-of select="substring($comparedate, 8, 1)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="substring($comparedate, 7, 2)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="yearDiff" select="number($yearNumCreate) - number($yearNum)"/>
        <xsl:choose>
            <xsl:when test="number($monthNum) &lt; number($monthNumCreate)">
                <xsl:value-of select="$yearDiff"/>
            </xsl:when>
            <xsl:when test="number($monthNum) &gt; number($monthNumCreate)">
                <xsl:value-of select="$yearDiff - 1"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="number($dayNum) &lt;= number($dayNumCreate)">
                        <xsl:value-of select="$yearDiff"/>
                    </xsl:when>
                    <xsl:when test="number($dayNum) &gt; number($dayNumCreate)">
                        <xsl:value-of select="$yearDiff - 1"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Convert Telecom URI scheme (tel, fax, http, mailto) to display text</xd:p>
        </xd:desc>
        <xd:param name="code">Scheme string</xd:param>
    </xd:doc>
    <xsl:template name="translateTelecomUriScheme">
        <xsl:param name="code"/>
        <!--xsl:value-of select="document('voc.xml')/systems/system[@root=$code/@codeSystem]/code[@value=$code/@code]/@displayName"/-->
        <!--xsl:value-of select="document('codes.xml')/*/code[@code=$code]/@display"/-->
        <xsl:choose>
            <!-- lookup table Telecom URI -->
            <xsl:when test="$code = 'tel'">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'Tel'"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$code = 'fax'">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'Fax'"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$code = 'http' or $code = 'https'">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'Web'"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$code = 'mailto'">
                <xsl:call-template name="getLocalizedString">
                    <xsl:with-param name="key" select="'Mail'"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>{$code='</xsl:text>
                <xsl:value-of select="$code"/>
                <xsl:text>'?}</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Converts Latin characters in input to lower case and returns the result</xd:p>
        </xd:desc>
        <xd:param name="data">Input string</xd:param>
    </xd:doc>
    <xsl:template name="caseDown">
        <xsl:param name="data"/>
        <xsl:if test="$data">
            <xsl:value-of select="translate($data, $uc, $lc)"/>
        </xsl:if>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Converts Latin characters in input to upper case and returns the result</xd:p>
        </xd:desc>
        <xd:param name="data">Input string</xd:param>
    </xd:doc>
    <xsl:template name="caseUp">
        <xsl:param name="data"/>
        <xsl:if test="$data">
            <xsl:value-of select="translate($data, $lc, $uc)"/>
        </xsl:if>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Converts first character in input to upper case if it is a Latin character and returns the result</xd:p>
        </xd:desc>
        <xd:param name="data">Input string</xd:param>
    </xd:doc>
    <xsl:template name="firstCharCaseUp">
        <xsl:param name="data"/>
        <xsl:if test="$data">
            <xsl:call-template name="caseUp">
                <xsl:with-param name="data" select="substring($data, 1, 1)"/>
            </xsl:call-template>
            <xsl:value-of select="substring($data, 2)"/>
        </xsl:if>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Tokenize based on delimiters, or if no delimiter do character tokenization</xd:p>
        </xd:desc>
        <xd:param name="string">String to tokenize</xd:param>
        <xd:param name="delimiters">Optional delimiter string</xd:param>
        <xd:param name="prefix">Optional prefix for every 'array' item</xd:param>
        <xd:param name="localize">Optional parameter to determine if we should just tokenize or also try to localize array items (default)</xd:param>
    </xd:doc>
    <xsl:template name="tokenize">
        <xsl:param name="string" select="''"/>
        <xsl:param name="delimiters" select="' '"/>
        <xsl:param name="prefix"/>
        <xsl:param name="localize" select="true()"/>
        <xsl:choose>
            <xsl:when test="not($string)"/>
            <xsl:when test="not($delimiters)">
                <xsl:call-template name="_tokenize-characters">
                    <xsl:with-param name="string" select="$string"/>
                    <xsl:with-param name="prefix" select="$prefix"/>
                    <xsl:with-param name="localize" select="$localize"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="_tokenize-delimiters">
                    <xsl:with-param name="string" select="$string"/>
                    <xsl:with-param name="delimiters" select="$delimiters"/>
                    <xsl:with-param name="prefix" select="$prefix"/>
                    <xsl:with-param name="localize" select="$localize"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Tokenize every character</xd:p>
        </xd:desc>
        <xd:param name="string">String to tokenize</xd:param>
        <xd:param name="prefix">Optional prefix for every 'array' item</xd:param>
        <xd:param name="localize">Optional parameter to determine if we should just tokenize or also try to localize array items (default)</xd:param>
    </xd:doc>
    <xsl:template name="_tokenize-characters">
        <xsl:param name="string"/>
        <xsl:param name="prefix"/>
        <xsl:param name="localize" select="true()"/>
        <xsl:if test="$string">
            <xsl:choose>
                <xsl:when test="$localize">
                    <xsl:call-template name="getLocalizedString">
                        <xsl:with-param name="key" select="concat($prefix, substring($string, 1, 1))"/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="substring($string, 1, 1)"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:call-template name="_tokenize-characters">
                <xsl:with-param name="string" select="substring($string, 2)"/>
                <xsl:with-param name="prefix" select="$prefix"/>
                <xsl:with-param name="localize" select="$localize"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Tokenize based on delimiters</xd:p>
        </xd:desc>
        <xd:param name="string">String to tokenize</xd:param>
        <xd:param name="delimiters">Required delimiter string</xd:param>
        <xd:param name="prefix">Optional prefix for every 'array' item</xd:param>
        <xd:param name="localize">Optional parameter to determine if we should just tokenize or also try to localize array items (default)</xd:param>
    </xd:doc>
    <xsl:template name="_tokenize-delimiters">
        <xsl:param name="string"/>
        <xsl:param name="delimiters"/>
        <xsl:param name="prefix"/>
        <xsl:param name="localize" select="true()"/>
        
        <xsl:variable name="delimiter" select="substring($delimiters, 1, 1)"/>
        <xsl:choose>
            <xsl:when test="not($delimiter)">
                <xsl:choose>
                    <xsl:when test="$localize">
                        <xsl:call-template name="getLocalizedString">
                            <xsl:with-param name="key" select="concat($prefix, $string)"/>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$string"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="contains($string, $delimiter)">
                <xsl:if test="not(starts-with($string, $delimiter))">
                    <xsl:call-template name="_tokenize-delimiters">
                        <xsl:with-param name="string" select="substring-before($string, $delimiter)"/>
                        <xsl:with-param name="delimiters" select="substring($delimiters, 2)"/>
                        <xsl:with-param name="prefix" select="$prefix"/>
                        <xsl:with-param name="localize" select="$localize"/>
                    </xsl:call-template>
                </xsl:if>
                <xsl:text> </xsl:text>
                <xsl:call-template name="_tokenize-delimiters">
                    <xsl:with-param name="string" select="substring-after($string, $delimiter)"/>
                    <xsl:with-param name="delimiters" select="$delimiters"/>
                    <xsl:with-param name="prefix" select="$prefix"/>
                    <xsl:with-param name="localize" select="$localize"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="_tokenize-delimiters">
                    <xsl:with-param name="string" select="$string"/>
                    <xsl:with-param name="delimiters" select="substring($delimiters, 2)"/>
                    <xsl:with-param name="prefix" select="$prefix"/>
                    <xsl:with-param name="localize" select="$localize"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xd:doc>
        <xd:desc>
            <xd:p>Retrieves a language dependant string from our <xd:ref name="vocFile" type="parameter">language file</xd:ref> such as a label based on a key. Returns string based on <xd:ref name="textLang" type="parameter">textLang</xd:ref>, <xd:ref name="textLangDefault" type="parameter">textLangDefault</xd:ref>, the first two characters of the textLangDefault, e.g. 'en' in 'en-US' and finally if all else fails just the key text.</xd:p>
        </xd:desc>
        <xd:param name="pre">Some text or space to prefix our string with</xd:param>
        <xd:param name="key">The key to find our text with</xd:param>
        <xd:param name="post">Some text like a colon or space to postfix our text with</xd:param>
    </xd:doc>
    <xsl:template name="getLocalizedString">
        <xsl:param name="pre" select="''"/>
        <xsl:param name="key"/>
        <xsl:param name="post" select="''"/>
        <xsl:variable name="keyval" select="$keys[@key = $key]/value" as="element()*"/>
        <xsl:choose>
            <!-- compare 'de-CH' -->
            <xsl:when test="$keyval[@lang = $textLangLowerCase]">
                <xsl:value-of select="concat($pre, $keyval[@lang = $textLangLowerCase][1]/text(), $post)"/>
            </xsl:when>
            <!-- compare 'de' in 'de-CH' -->
            <xsl:when test="$keyval[substring(@lang, 1, 2) = $textLangPartLowerCase]">
                <xsl:value-of select="concat($pre, $keyval[substring(@lang, 1, 2) = $textLangPartLowerCase][1]/text(), $post)"/>
            </xsl:when>
            <!-- compare 'en-US' -->
            <xsl:when test="$keyval[@lang = $textLangDefaultLowerCase]">
                <xsl:value-of select="concat($pre, $keyval[@lang = $textLangDefaultLowerCase][1]/text(), $post)"/>
            </xsl:when>
            <!-- compare 'en' in 'en-US' -->
            <xsl:when test="$keyval[substring(@lang, 1, 2) = $textLangDefaultPartLowerCase]">
                <xsl:value-of select="concat($pre, $keyval[substring(@lang, 1, 2) = $textLangDefaultPartLowerCase][1]/text(), $post)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat($pre, $key[1], $post)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
