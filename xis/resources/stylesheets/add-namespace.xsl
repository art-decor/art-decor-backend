<!-- 
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
-->
<xsl:stylesheet xmlns:local="urn:local" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="local" version="2.0">
    <xsl:param name="xpath" as="xs:string?"/>
    <xsl:output omit-xml-declaration="yes"/>
    <xsl:template match="/">
        <xsl:choose>
            <xsl:when test="string-length($xpath) = 0">
                <xsl:apply-templates select="*"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:for-each select="descendant-or-self::*">
                    <xsl:variable name="nodepath">
                        <xsl:call-template name="getpath"/>
                    </xsl:variable>
                    <xsl:if test="$nodepath = $xpath">
                        <xsl:apply-templates select="."/>
                    </xsl:if>
                </xsl:for-each>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="*[contains(@xsi:type, ':')]">
        <xsl:variable name="xsins" select="tokenize(@xsi:type, ':')[1]"/>
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:namespace name="{$xsins}" select="namespace-uri-for-prefix($xsins, .)"/>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="@* | node()">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>
    <xsl:template name="getpath">
        <xsl:variable name="parts" as="xs:string+">
            <xsl:for-each select="ancestor-or-self::*">
                <xsl:value-of select="concat('*[',count(preceding-sibling::*) + 1,']')"/>
            </xsl:for-each>
        </xsl:variable>
        <xsl:value-of select="string-join($parts, '/')"/>
    </xsl:template>
</xsl:stylesheet>