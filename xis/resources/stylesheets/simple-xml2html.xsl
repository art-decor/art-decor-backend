<?xml version="1.0" encoding="UTF-8"?>
<!--
 |
 | XSLT REC Compliant Version of IE5 Default Stylesheet
 |
 | Original version by Jonathan Marsh (jmarsh@microsoft.com)
 | http://msdn.microsoft.com/xml/samples/defaultss/defaultss.xsl
 |
 | Conversion to XSLT 1.0 REC Syntax by Steve Muench (smuench@oracle.com)
 |
 +-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output indent="no" method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <script>
                    <xsl:comment><![CDATA[
                  function f(e){
                     if (e.className=="ci") {
                       if (e.children(0).innerText.indexOf("\n")>0) fix(e,"cb");
                     }
                     if (e.className=="di") {
                       if (e.children(0).innerText.indexOf("\n")>0) fix(e,"db");
                     } e.id="";
                  }
                  function fix(e,cl){
                    e.className=cl;
                    e.style.display="block";
                    j=e.parentElement.children(0);
                    j.className="c";
                    k=j.children(0);
                    k.style.visibility="visible";
                    k.href="#";
                  }
                  function ch(e) {
                    mark=e.children(0).children(0);
                    if (mark.innerText=="+") {
                      mark.innerText="-";
                      for (var i=1;i<e.children.length;i++) {
                        e.children(i).style.display="block";
                      }
                    }
                    else if (mark.innerText=="-") {
                      mark.innerText="+";
                      for (var i=1;i<e.children.length;i++) {
                        e.children(i).style.display="none";
                      }
                    }
                  }
                  function ch2(e) {
                    mark=e.children(0).children(0);
                    contents=e.children(1);
                    if (mark.innerText=="+") {
                      mark.innerText="-";
                      if (contents.className=="db"||contents.className=="cb") {
                        contents.style.display="block";
                      }
                      else {
                        contents.style.display="inline";
                      }
                    }
                    else if (mark.innerText=="-") {
                      mark.innerText="+";
                      contents.style.display="none";
                    }
                  }
                  function cl() {
                    e=window.event.srcElement;
                    if (e.className!="c") {
                      e=e.parentElement;
                      if (e.className!="c") {
                        return;
                      }
                    }
                    e=e.parentElement;
                    if (e.className=="e") {
                      ch(e);
                    }
                    if (e.className=="k") {
                      ch2(e);
                    }
                  }
                  function ex(){}
                  function h(){window.status=" ";}
                  document.onclick=cl;
              ]]></xsl:comment>
                </script>
                <style>
              body {font-size: x-small; font-family: Verdana, Arial, sans-serif; margin-right:1.5em}
                .c  {cursor:hand}
                .b  {color:red; font-family:'Courier New'; font-weight:bold;
                     text-decoration:none}
                .e  {margin-left:1em; text-indent:-1em; margin-right:1em}
                .k  {margin-left:1em; text-indent:-1em; margin-right:1em}
                .t  {color:#990000}
                .xt {color:#990099}
                .ns {color:red}
                .dt {color:green}
                .m  {color:blue}
                .tx {font-weight:bold}
                .db {text-indent:0px; margin-left:1em; margin-top:0px;
                     margin-bottom:0px;padding-left:.3em;
                     border-left:1px solid #CCCCCC; font:small Courier}
                .di {font:small Courier}
                .d  {color:blue}
                .pi {color:blue}
                .cb {text-indent:0px; margin-left:1em; margin-top:0px;
                     margin-bottom:0px;padding-left:.3em; font:small Courier;
                     color:#888888}
                .ci {font:small Courier; color:#888888}
                PRE {margin:0px; display:inline}
           </style>
            </head>
            <body class="st">
                <xsl:apply-templates/>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="processing-instruction()">
        <div class="e">
            <span class="b">
                <xsl:call-template name="nbsp-ref"/>
            </span>
            <span class="m">
                <xsl:text>&lt;?</xsl:text>
            </span>
            <span class="pi">
                <xsl:value-of select="name(.)"/>
                <xsl:value-of select="."/>
            </span>
            <span class="m">
                <xsl:text>?&gt;</xsl:text>
            </span>
        </div>
    </xsl:template>
    <xsl:template match="processing-instruction('xml')">
        <div class="e">
            <span class="b">
                <xsl:call-template name="nbsp-ref"/>
            </span>
            <span class="m">
                <xsl:text>&lt;?</xsl:text>
            </span>
            <span class="pi">
                <xsl:text>xml </xsl:text>
                <xsl:for-each select="@*">
                    <xsl:value-of select="name(.)"/>
                    <xsl:text>="</xsl:text>
                    <xsl:value-of select="."/>
                    <xsl:text>" </xsl:text>
                </xsl:for-each>
            </span>
            <span class="m">
                <xsl:text>?&gt;</xsl:text>
            </span>
        </div>
    </xsl:template>
    <xsl:template match="@*">
        <span>
            <xsl:attribute name="class">
                <xsl:if test="xsl:*/@*">
                    <xsl:text>x</xsl:text>
                </xsl:if>
                <xsl:text>t</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="name(.)"/>
        </span>
        <span class="m">="</span>
        <b>
            <xsl:value-of select="."/>
        </b>
        <span class="m">"</span>
    </xsl:template>
    <xsl:template match="text()">
        <div class="e">
            <span class="b"/>
            <span class="tx">
                <xsl:value-of select="."/>
            </span>
        </div>
    </xsl:template>
    <xsl:template match="comment()">
        <div class="k">
            <span>
                <a style="visibility:hidden" class="b" onclick="return false" onfocus="h()">-</a>
                <span class="m">
                    <xsl:text>&lt;!--</xsl:text>
                </span>
            </span>
            <span class="ci" id="clean">
                <pre>
                    <xsl:value-of select="."/>
                </pre>
            </span>
            <span class="b">
                <xsl:call-template name="nbsp-ref"/>
            </span>
            <span class="m">
                <xsl:text>--&gt;</xsl:text>
            </span>
            <script>f(clean);</script>
        </div>
    </xsl:template>
    <xsl:template match="*">
        <div class="e">
            <div style="margin-left:1em;text-indent:-2em">
                <span class="b">
                    <xsl:call-template name="nbsp-ref"/>
                </span>
                <span class="m">&lt;</span>
                <span>
                    <xsl:attribute name="class">
                        <xsl:if test="xsl:*">
                            <xsl:text>x</xsl:text>
                        </xsl:if>
                        <xsl:text>t</xsl:text>
                    </xsl:attribute>
                    <xsl:value-of select="name(.)"/>
                    <xsl:if test="@*">
                        <xsl:text> </xsl:text>
                    </xsl:if>
                </span>
                <xsl:apply-templates select="@*"/>
                <span class="m">
                    <xsl:text>/&gt;</xsl:text>
                </span>
            </div>
        </div>
    </xsl:template>
    <xsl:template match="*[node()]">
        <div class="e">
            <div class="c">
                <a class="b" href="#" onclick="return false" onfocus="h()">-</a>
                <span class="m">&lt;</span>
                <span>
                    <xsl:attribute name="class">
                        <xsl:if test="xsl:*">
                            <xsl:text>x</xsl:text>
                        </xsl:if>
                        <xsl:text>t</xsl:text>
                    </xsl:attribute>
                    <xsl:value-of select="name(.)"/>
                    <xsl:if test="@*">
                        <xsl:text> </xsl:text>
                    </xsl:if>
                </span>
                <xsl:apply-templates select="@*"/>
                <span class="m">
                    <xsl:text>&gt;</xsl:text>
                </span>
            </div>
            <div>
                <xsl:apply-templates/>
                <div>
                    <span class="b">
                        <xsl:call-template name="nbsp-ref"/>
                    </span>
                    <span class="m">
                        <xsl:text>&lt;/</xsl:text>
                    </span>
                    <span>
                        <xsl:attribute name="class">
                            <xsl:if test="xsl:*">
                                <xsl:text>x</xsl:text>
                            </xsl:if>
                            <xsl:text>t</xsl:text>
                        </xsl:attribute>
                        <xsl:value-of select="name(.)"/>
                    </span>
                    <span class="m">
                        <xsl:text>&gt;</xsl:text>
                    </span>
                </div>
            </div>
        </div>
    </xsl:template>
    <xsl:template match="*[text() and not (comment() or processing-instruction())]">
        <div class="e">
            <div style="margin-left:1em;text-indent:-2em">
                <span class="b">
                    <xsl:call-template name="nbsp-ref"/>
                </span>
                <span class="m">
                    <xsl:text>&lt;</xsl:text>
                </span>
                <span>
                    <xsl:attribute name="class">
                        <xsl:if test="xsl:*">
                            <xsl:text>x</xsl:text>
                        </xsl:if>
                        <xsl:text>t</xsl:text>
                    </xsl:attribute>
                    <xsl:value-of select="name(.)"/>
                    <xsl:if test="@*">
                        <xsl:text> </xsl:text>
                    </xsl:if>
                </span>
                <xsl:apply-templates select="@*"/>
                <span class="m">
                    <xsl:text>&gt;</xsl:text>
                </span>
                <span class="tx">
                    <xsl:value-of select="."/>
                </span>
                <span class="m">&lt;/</span>
                <span>
                    <xsl:attribute name="class">
                        <xsl:if test="xsl:*">
                            <xsl:text>x</xsl:text>
                        </xsl:if>
                        <xsl:text>t</xsl:text>
                    </xsl:attribute>
                    <xsl:value-of select="name(.)"/>
                </span>
                <span class="m">
                    <xsl:text>&gt;</xsl:text>
                </span>
            </div>
        </div>
    </xsl:template>
    <xsl:template match="*[*]" priority="20">
        <div class="e">
            <div style="margin-left:1em;text-indent:-2em" class="c">
                <a class="b" href="#" onclick="return false" onfocus="h()">-</a>
                <span class="m">&lt;</span>
                <span>
                    <xsl:attribute name="class">
                        <xsl:if test="xsl:*">
                            <xsl:text>x</xsl:text>
                        </xsl:if>
                        <xsl:text>t</xsl:text>
                    </xsl:attribute>
                    <xsl:value-of select="name(.)"/>
                    <xsl:if test="@*">
                        <xsl:text> </xsl:text>
                    </xsl:if>
                </span>
                <xsl:apply-templates select="@*"/>
                <span class="m">
                    <xsl:text>&gt;</xsl:text>
                </span>
            </div>
            <div>
                <xsl:apply-templates/>
                <div>
                    <span class="b">
                        <xsl:call-template name="nbsp-ref"/>
                    </span>
                    <span class="m">
                        <xsl:text>&lt;/</xsl:text>
                    </span>
                    <span>
                        <xsl:attribute name="class">
                            <xsl:if test="xsl:*">
                                <xsl:text>x</xsl:text>
                            </xsl:if>
                            <xsl:text>t</xsl:text>
                        </xsl:attribute>
                        <xsl:value-of select="name(.)"/>
                    </span>
                    <span class="m">
                        <xsl:text>&gt;</xsl:text>
                    </span>
                </div>
            </div>
        </div>
    </xsl:template>
    <xsl:template name="nbsp-ref">
        <xsl:text>&#160;</xsl:text>
    </xsl:template>
</xsl:stylesheet>