xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
module namespace val                = "http://art-decor.org/ns/art-decor/xis/validation";
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace adxfiles    = "http://art-decor.org/ns/art-decor/xis/files" at "api-xis-files.xqm";
import module namespace adxaccounts = "http://art-decor.org/ns/art-decor/xis/accounts" at "api-xis-accounts.xqm";

declare namespace hl7           = "urn:hl7-org:v3";
declare namespace xis           = "http://art-decor.org/ns/xis";
declare namespace xs            = "http://www.w3.org/2001/XMLSchema";
declare namespace svrl          = "http://purl.oclc.org/dsdl/svrl";
declare namespace util          = "http://exist-db.org/xquery/util";
declare namespace validation    = "http://exist-db.org/xquery/validation";
declare namespace transform     = "http://exist-db.org/xquery/transform";
declare namespace de            = "http://art-decor.org/ns/error";

(: Log debug messages? :)
declare variable $val:debug             := false();
(: DB path to XSL that handles the final returned view of all errors/warnings :)
declare variable $val:issueReportXSL    := concat($get:strXisResources, '/stylesheets/groupIssues.xsl');
(: this XSL gets everything HL7 and leaves everything non-HL7 :)
declare variable $val:cdaNormalizeXSL   := concat($get:strXisResources, '/stylesheets/remove_non_hl7_namespace.xsl');
    
(: http://www.xqueryfunctions.com/xq/functx_path-to-node-with-pos.html -- Adaptations:
    Adds leading slash to returned path
    Outputs hl7:node for nodes in HL7v3 namespace
    Outputs *:node[namespace-uri()='...'] for nodes in other namespaces where ... may be an empty string
:)
declare %private function val:path-to-node-with-pos ( $node as node()? )  as xs:string {
    let $return := 
        string-join (
            for $ancestor in $node/ancestor-or-self::*
            let $sibsOfSameName := $ancestor/../*[name() = name($ancestor)]
            return 
                concat(
                    if (namespace-uri($ancestor)='urn:hl7-org:v3') then ( 
                        concat('hl7:', local-name($ancestor)) 
                    ) else (
                        concat('*:', local-name($ancestor))
                    ),
                    if (count($sibsOfSameName) <= 1) then (
                        ''
                    ) else (
                        concat('[',val:index-of-node($sibsOfSameName,$ancestor),']')
                    ),
                    if (namespace-uri($ancestor) != 'urn:hl7-org:v3') then ( 
                        concat('[namespace-uri()=''', namespace-uri($ancestor), ''']') 
                    ) else (
                        ''
                    )
                )
        , '/')
        
    return concat('/',$return)
};
(: http://www.xqueryfunctions.com/xq/functx_index-of-node.html -- Copied as-is :)
declare %private function val:index-of-node ( $nodes as node()*, $nodeToFind as node() )  as xs:integer* {
    for $seq in (1 to count($nodes))
    return $seq[$nodes[$seq] is $nodeToFind]
};

declare function val:validateSchemaCDA($input as node(), $cdaSchema as xs:string) as node()* {
    let $check         := 
        if (doc-available($cdaSchema)) then 
            true() 
        else (
            error(QName('http://art-decor.org/ns/error', 'SchemaFileMissing'), concat('XML Schema file missing: ',$cdaSchema))
        )
    
    (: remove anything not in the HL7 namespace :)
    let $normalizedMessage := transform:transform($input, doc($val:cdaNormalizeXSL), ())
    
    (: return validation result :)
    return 
        validation:jaxv-report($normalizedMessage, doc($cdaSchema))
};

(: FIXME We're basically asssuming that input is B64 CDA, but it might be a JPG or PDF... should properly catch that and just skip :)
declare function val:validateSchematronCDA($input as node(), $baseCollectionSCH as xs:string) as node()* {
    let $re                := $input/local-name()
    
    (:let $g                 := util:log('DEBUG', concat('============ validateSchematronCDA parameters mapping count=',count($mappings/*))):)
    
    (: return validation result :)
    return 
        try {
            let $messageSchematronFile  := val:getSchematronFile($input, $baseCollectionSCH)
            (:let $g                      := util:log('DEBUG', concat('============ validateSchematronCDA determined schematron=',$messageSchematronFile)):)
            return
            if (string-length($messageSchematronFile)>0) then 
                transform:transform($input, doc($messageSchematronFile), ())
            else (
                <svrl:issues>
                    <svrl:issue type="schematron" role="error">
                        <svrl:text>Could not determine any schematron file to validate with based on templateId and/or root element. If this instance is meant to be validated using these resources please check the templateId(s) used in this instance.</svrl:text>
                    </svrl:issue>
                </svrl:issues>
            )
        }
        catch * {
            <svrl:issues>
                <svrl:issue type="schematron" role="error">
                    <svrl:text>ERROR {$err:code} in main Schematron validation: {$err:description, "', module: ", $err:module, "(", $err:line-number, ",", $err:column-number, ")"}</svrl:text>
                </svrl:issue>
            </svrl:issues>
        }
};

declare %private function val:getSchematronPath($validationBase as xs:string) as xs:string {
    if (xmldb:collection-available(concat("xmldb:exist://",$validationBase,"/schematron_closed_warnings_svrl/"))) then
        concat("xmldb:exist://",$validationBase,"/schematron_closed_warnings_svrl/")
    else if (xmldb:collection-available(concat("xmldb:exist://",$validationBase,"/schematron_svrl/"))) then
        concat("xmldb:exist://",$validationBase,"/schematron_svrl/")
    else if (xmldb:collection-available(concat("xmldb:exist://",$validationBase,"/schematron_xslt/"))) then
        concat("xmldb:exist://",$validationBase,"/schematron_xslt/")
    else if (xmldb:collection-available(concat("xmldb:exist://",$validationBase,"/schematron_closed_warnings/"))) then
        concat("xmldb:exist://",$validationBase,"/schematron_closed_warnings/")
    else if (xmldb:collection-available(concat("xmldb:exist://",$validationBase,"/schematron/"))) then
        concat("xmldb:exist://",$validationBase,"/schematron/")
    else (
        concat("xmldb:exist://",$validationBase,"/schematron_svrl/")
    )
};
declare %private function val:getSchemaPath($validationBase as xs:string) as xs:string {
    concat("xmldb:exist://",$validationBase,"/schemas_codeGen_flat/")
};

(:
:   The first instance templateId based mapping wins. Fallback on root element based mapping. Final fallback checks if there's schematron <rootelement>.xsl
:   Else return empty
:   
:   There are two structures for instance2schematron.xml around that we want to support both
:   Old school:
:       <mappings>
:           <map rootelement="ClinicalDocument" namespace="urn:hl7-org:v3" schsvrl="">
:               <templateId root="2.16.840.1.113883.2.4.3.36.10.10" schsvrl="rivmsp-nsp-bc-mdl.xsl"/>
:               <templateId root="2.16.840.1.113883.2.4.3.36.10.20" schsvrl="rivmsp-nsp-bc-pa.xsl"/>
:           </map>
:       </mappings>
:   New school:
:       <mappings>
:           <!-- template name: counseling-fase-1c -->
:           <map model="REPC_IN004110UV01" namespace="urn:hl7-org:v3" templateRoot="2.16.840.1.113883.2.4.6.10.90.59" sch="peri20-counseling-fase-1c.sch" schsvrl="peri20-counseling-fase-1c.xsl"/>
:           <!-- template name: counseling-1c -->
:           <map model="REPC_IN004110UV01" namespace="urn:hl7-org:v3" templateRoot="2.16.840.1.113883.2.4.6.10.90.54" sch="peri20-counseling-1c.sch" schsvrl="peri20-counseling-1c.xsl"/>
:       </mappings>
:)
declare %private function val:getSchematronFile($input as node(), $baseCollectionSCH as xs:string) as xs:string? {
    (: DB path to mapping file that holds relations to Schematron :)
    let $mappings       := if (xmldb:collection-available($baseCollectionSCH)) then collection($baseCollectionSCH)//mappings else ()
    
    let $checkParameter := 
        if (empty($mappings)) then (
            error(QName('http://art-decor.org/ns/error', 'MappingsMissing'), 'Mapping configuration for Schematron missing. This is usually a file called schematron_svrl/(prefix-)?instance2schematron.xml and needs a toplevel element mappings.')
        ) else ()
    
    let $ns             := $input[1]/namespace-uri()
    let $re             := $input[1]/local-name()
    
    (:let $g := util:log('DEBUG', concat('===(getSchematronFile)=== Got root node: ', $re, ' in namespace: ',$ns,' found templateId? ', $templateId/@root)):)
    
    (: First try to find the schematron based on a template in the input instance :)
    let $g := if ($val:debug) then (util:log('DEBUG', '============ $input//hl7:templateId ')) else ()
    let $g := if ($val:debug) then (util:log('DEBUG', <l>{$input//hl7:templateId}</l>)) else ()
    
    (: Retrieve the first templateId which also occurs in map :)
    let $templateId          := $input//hl7:templateId[@root = ($mappings//@root | $mappings//@templateRoot)]
    let $g := if ($val:debug) then (util:log('DEBUG', concat('============ $templateId ', $templateId/@root))) else ()
    (: 
    :   If there is no (mapping line based on the) templateId, get the first mapping line based on the instance root element,
    :   else return the matching mapping line
    :)
    let $templateMappingLine := 
        if (empty($templateId) or ($ns='urn:hl7-org:v3' and starts-with($re,'MCCI_IN200101'))) then (
            (: root element based :)
            $mappings//map[@rootelement = $re][@namespace = $ns][@schsvrl]
        ) else (
            (: templateId based :)
            for $t in $templateId
            return (
                $mappings//*[@root = $t/@root][@schsvrl] | $mappings//*[@templateRoot = $t/@root][@schsvrl]
            ) 
        )
    let $templateMappingLine    := $templateMappingLine[1]
    let $g := if ($val:debug) then (util:log('DEBUG', '============ $templateMappingLine ')) else ()
    let $g := if ($val:debug) then (util:log('DEBUG', <l>{$templateMappingLine}</l>)) else ()
    
    (:
    :   If there still is no mapping line, then check if we can fallback onto a root element based schematron
    :)
    let $schsvrl            :=
        if ($templateMappingLine/@schsvrl) then (
            concat($baseCollectionSCH,'/',$templateMappingLine/@schsvrl)
        )
        else (
            concat($baseCollectionSCH,'/',$re,'.xsl')
        )
    
    let $checkParameter := 
        if (doc-available($schsvrl)) then () else (
            error(QName('http://art-decor.org/ns/error', 'MappingMissing'), concat('Could not determine any available schematron file to validate with based on templateId and/or root element. root=''',$re,'[namespace-uri()=''',$ns,''']'''))
        )
    
    return
        $schsvrl
};
declare %private function val:getSchemaFile($input as node(), $baseCollectionXSD as xs:string, $cdaFallBack as xs:boolean) as xs:string? {
    (: DB path to mapping file that holds relations to Schematron :)
    let $mappings       := if (xmldb:collection-available($baseCollectionXSD)) then collection($baseCollectionXSD)//mappings else ()
    let $ns             := $input[1]/namespace-uri()
    let $re             := $input[1]/local-name()
    
    (:let $g := util:log('DEBUG', concat('===(getSchematronFile)=== Got root node: ', $re, ' in namespace: ',$ns,' found templateId? ', $templateId/@root)):)
    
    (: First try to find the schematron based on a template in the input instance :)
    let $g := if ($val:debug) then (util:log('DEBUG', '============ $input//hl7:templateId ')) else ()
    let $g := if ($val:debug) then (util:log('DEBUG', <l>{$input//hl7:templateId}</l>)) else ()
    
    (: Retrieve the first templateId which also occurs in map :)
    let $templateId          := ($input//hl7:templateId[@root = $mappings//*[string-length(@xsd) gt 0]/(@root|@templateRoot)])[1]
    let $g := if ($val:debug) then (util:log('DEBUG', concat('============ $templateId ', $templateId/@root))) else ()
    (: 
    :   If there is no (mapping line based on the) templateId, get the first mapping line based on the instance root element,
    :   else return the matching mapping line
    :)
    let $templateMappingLine := 
        if (empty($templateId) or ($ns = 'urn:hl7-org:v3' and starts-with($re, 'MCCI_IN200101'))) then (
            (: root element based :)
            $mappings//map[@rootelement = $re][@namespace = $ns][string-length(@xsd) gt 0]
        ) else (
            (: templateId based :)
            for $t in $templateId
            return (
                $mappings//*[@root = $t/@root][@xsd] | $mappings//*[@rootelement = $re][@namespace = $ns][@templateRoot = $t/@root][@xsd]
            )
        )
    let $g := if ($val:debug) then (util:log('DEBUG', '============ $templateMappingLine ')) else ()
    let $g := if ($val:debug) then (util:log('DEBUG', <l>{$templateMappingLine}</l>)) else ()
    
    let $xsd    :=
        if ($mappings) then (
            for $m in $templateMappingLine
            return
                if (doc-available(concat(util:collection-name($mappings),'/',$m/@xsd))) then 
                    concat(util:collection-name($mappings),'/',$m/@xsd)
                else ()
        ) else ()
   
    (: If there still is no mapping line, then check if we can fallback onto a root element based schema :)
    let $xsd                :=
        if (empty($xsd)) then (
            (: DB path to flattened XML Schema for this message instance based on interaction-id, batch could contain multiple ... :)
            if ($input[self::hl7:ClinicalDocument]) then (
                if (xmldb:get-child-resources($baseCollectionXSD)[.='CDA.xsd']) then 
                    concat($baseCollectionXSD,'CDA.xsd')
                else 
                if (xmldb:get-child-resources($baseCollectionXSD)[.='ClinicalDocument.xsd']) then 
                    concat($baseCollectionXSD,'ClinicalDocument.xsd')
                else
                if ($cdaFallBack) then
                    concat($get:strHl7, '/CDAr2/schemas_codeGen_flat/CDA.xsd')
                else ()
            ) 
            else (
                concat($baseCollectionXSD,($input[1]/local-name())[1],'.xsd')
            )
        )
        else ($xsd)
    
    let $checkParameter := 
        if (doc-available($xsd)) then () else (
            error(QName('http://art-decor.org/ns/error', 'MappingMissing'), concat('Could not determine any available schema file to validate with based on templateId and/or root element. root=''',$re,'[namespace-uri()=''',$ns,''']'': ', $xsd))
        )
    
    return
        $xsd[string-length(.) gt 0][1]
};

(:~
:   Tries to come up with a reasonable file/resource name extension including the dot, based on mediaType and file name. Can return empty string.
:   
:   @param $mediaType - (optional) contains the mime type
:   @param $fileUri   - (optional) contains the URI for
:   @since 2013-06-14
:)
declare %private function val:getExtension($mediaType as xs:string?, $fileUri as xs:string?) as xs:string {
    let $mediaType := lower-case($mediaType)
    
    return
    if (tokenize($mediaType,'/')[last()]=('pdf','xml','jpg','jpeg','gif','html','rtf','xhtml','xsl')) then
        (:should be a valid extension:)
        concat('.',tokenize($mediaType,'/')[last()])
    else if ($mediaType='text/plain') then
        (:plain text file:)
        '.txt'
    else if (contains($mediaType,'xml') and contains($mediaType,'html')) then
        (:xhtml:)
        '.xhtml'
    else if (contains($mediaType,'xml')) then
        (:xml of some sort:)
        '.xml'
    else if (string-length((tokenize($fileUri,'\.')[last()])[1])>0) then
        (:get extension from file path in hl7:reference/@value:)
        concat('.',(tokenize($fileUri,'\.')[last()])[1])
    else (
        (:we don't know what extension so leave empty:)
        ''
    )
};

(:~
:   Stores a given base64Binary as resource under the given name in xis-data/accounts/<account>/attachments
:   
:   @param $resourceName - (required) contains the "file" name, without collection path
:   @param $input        - (required but may be empty) contains the base64 encoded contents of the resource
:   @param $mediaType    - (optional) contains the mime type of the $input
:   @since 2013-06-14
:)
declare %private function val:saveAsResource($resourceName as xs:string, $input as xs:base64Binary*, $mediaType as xs:string?) as xs:boolean {
    (: write the decoded CDA content to database :)
    (: to fix: this writes the CDA content to the database each time you click on the HL7-interaction in ART :)
    let $account            := request:get-parameter('account',(''))
    let $messageStoragePath := concat($get:strXisAccounts, '/',$account,'/attachments')

    let $createCollection   := 
        if (not(xmldb:collection-available($messageStoragePath))) then (
            xmldb:create-collection(concat($get:strXisAccounts, '/',$account),'/attachments')
        ) else ()
    
    (: write the decoded CDA content filepath to exist log :)
    let $g                  := util:log('DEBUG', concat('---(saveAsResource)--- Storing attachment in ', $messageStoragePath, '/' , $resourceName))
    
    let $return             :=
        if (string-length($mediaType)>0) then
            xmldb:store($messageStoragePath, $resourceName, $input, $mediaType)
        else (
            xmldb:store($messageStoragePath, $resourceName, $input)
        )
    
    let $g                  := util:log('DEBUG', concat('---(saveAsResource)--- Stored attachment in ', $return))
    
    return
        string-length($return)>0
};

declare function val:validateSchematron($messageInstance as node(),  $messageSchematronFile as xs:string) as node()* {
    (: ==== START Schematron validation of messageInstance. Requires SVRL version of SCH (!) ==== :)
    try {
        (: DB path to Schematron for this message instance based on interaction-id :)
        let $g := if ($val:debug) then (util:log('DEBUG', concat('============ START MAIN XML Schematron validation of instance with ', $messageSchematronFile, ' doc-available: ', doc-available($messageSchematronFile)))) else ()
        
        let $messageSchematron := 
            if (doc-available($messageSchematronFile)) 
            then doc($messageSchematronFile) 
            else (error(QName('http://art-decor.org/ns/error', 'SchematronFileMissing'), concat('Schematron file missing: ', $messageSchematronFile)))
        
        let $schematronReport  := transform:transform($messageInstance, $messageSchematron, ())
        return
        if (empty($schematronReport)) then (
            error(QName('http://art-decor.org/ns/error', 'SchematronReportEmpty'), concat('Schematron report is empty for: ', $messageSchematron))
        ) else (
            if ($schematronReport//svrl:*[@test][@role=('warn','warning','error')] | $schematronReport//svrl:*[@test][empty(@role)]) then () else (
                <issue type="schematron" role="info" file="{tokenize($messageSchematronFile,'/')[last()]}">
                    <description>INFO no errors or warnings in XML Schematron validation.</description>
                    <location line="1"/>
                </issue>
            )
            ,
            for $issue in $schematronReport//svrl:*[@test]
            let $location   := replace($issue/@location,'\*:([^\[]+)\[namespace-uri\(\)=''urn:hl7-org:v3''\]','hl7:$1')
            let $role       := 
                if ($issue[@role='information']) then 'info' else 
                if ($issue[@role='warn']) then 'warning' else 
                if ($issue[@role]) then ($issue/@role) 
                else 'error'
            return
                <issue type="schematron" role="{$role}" test="{$issue/@test}" file="{tokenize($messageSchematronFile,'/')[last()]}">
                {   $issue/@see, $issue/@flag }
                    <description>{$issue/svrl:text/text()}</description>
                    <location path="{$location}"/>
                </issue>
        )
    } 
    catch * {
        <issue type="schematron" role="error" file="{tokenize($messageSchematronFile,'/')[last()]}">
            <description>ERROR {$err:code} in main Schematron validation: {$err:description, "', module: ", $err:module, "(", $err:line-number, ",", $err:column-number, ")"}</description>
            <location path=""/>
        </issue>
    }
    (: ==== END Schematron validation of messageInstance. Requires SVRL version of SCH (!) ==== :)
};

declare function val:makeIssueReport($issueReport as node()) as node(){
    let $xsltParameters     := ()
        (:<parameters>
            <param name="textLang" value="{$language}"/>
        </parameters>:)
    
    return
    try {transform:transform($issueReport, doc($val:issueReportXSL), $xsltParameters)}
    catch * {
        <issue type="schema" role="error" file="{tokenize($val:issueReportXSL,'/')[last()]}">
            <description>ERROR {$err:code} in main XSLT issue transform: {$err:description, "', module: ", $err:module, "(", $err:line-number, ",", $err:column-number, ")"}</description>
            <location line=""/>
        </issue>
    }
};

(:~ Returns the XML Resources Path as configured in the account that holds a relevant XML schema in a subcollection 
:   /schemas_codeGen_flat/. If param validationBase contains a value, then this value is what is returned (without checking)
:   If the param validationBase does not have a value, then the message is actually retrieved from the account and
:   the root element is used to determine the XSD on. E.g. REPC_IN002120UV.xsd
:
:   @param $file            - (optional) contains the "file" name, without collection path
:   @param $account         - (optional) contains the account name
:   @param $xpath           - (optional) contains the xpath to the message in the $file
:   @param $validationBase  - (optional) contains the XML Resources path to validate against
:   @since 2017-01-12
:)
declare function val:getResourcesByMessage($file as xs:string?, $account as xs:string?, $xpath as xs:string?, $validationBase as xs:string?) as item()* {
    let $defaultResources       := adxaccounts:getDefaultXmlResourcesPath($account)
    let $allAccountResources    := adxaccounts:getXmlResourcesPaths($account)
    
    return
    if ($allAccountResources[. = $validationBase]) then ($validationBase) else (
        (: try { :)
            let $messageInstance    := adxfiles:getMessage($account, $file, $xpath)
            let $baseCollectionXSD  := concat($defaultResources,"/schemas_codeGen_flat/")
            let $xsd                := try { val:getSchemaFile($messageInstance, $baseCollectionXSD, false()) } catch * {()}
            
            let $validationBase     :=
                if (empty($xsd)) then (
                    for $resources in $allAccountResources[not(. = $defaultResources)]
                    let $baseCollectionXSD  := concat($resources,"/schemas_codeGen_flat/")
                    let $xsd                := try { val:getSchemaFile($messageInstance, $baseCollectionXSD, false()) } catch * {()}
                    return
                        if (empty($xsd)) then () else ($resources[string-length()>0])
                )
                else ($defaultResources)
            
            return
                if (empty($validationBase)) then $defaultResources else $validationBase[1]
        (: }
        catch * {()} :)
    )
};

declare function val:validateMessage($file as xs:string, $account as xs:string, $xpath as xs:string, $validationBase as xs:string?) as node()* {
    let $g := if ($val:debug) then (util:log('DEBUG', concat('============ Supplied parameters: account=',$account,' file=',$file,' xpath=',$xpath,' validationBase=',$validationBase))) else ()
    
    (: DB path to Schematron for instance and contained instances in this message :)
    let $baseCollectionSCH  := val:getSchematronPath($validationBase)
    (: DB path to Schema for instance and contained instances in this message :)
    let $baseCollectionXSD  := val:getSchemaPath($validationBase)
    let $messageSchemaFile  := ()
    
    let $messageInstance    := 
        try { 
            adxfiles:getMessage($account, $file, $xpath)
        }
        catch * {
            <de:error xmlns:de="http://art-decor.org/ns/error">{concat($err:code, ' while getting message instance: ', $err:description, ', module: ', $err:module, ' (', $err:line-number, ',', $err:column-number, ')')}</de:error>
        }
    
    (: hacky, but necessary. The transform routine for schematron will kill the carefully reproduced @xsi:type value prefix declarations. When we save to disk first... it doesn't. Go figure :)
    let $tempFile           := adxfiles:saveTempMessage($account, $messageInstance)
    let $messageInstance    := doc($tempFile)/*
    
    let $g := if ($val:debug and $messageInstance instance of element(de:error)) then (util:log('ERROR', $messageInstance/text())) else ()
    let $g := if ($val:debug) then (util:log('DEBUG', concat('============ Message instance found: ',count($messageInstance)))) else ()
    let $g := if ($val:debug) then (util:log('DEBUG', '============ START MAIN XML Schema validation')) else ()
    
    (: ==== START XML Schema validation of messageInstance. Requires flattened XSD (!) ==== :)
    (: schema:
        <report>
            <status>invalid</status>
            <duration unit="msec">202</duration>
            <message level="Error" line="355" column="70">UndeclaredPrefix: Cannot resolve 'hl7:INT' as a QName: the prefix 'hl7' is not declared.</message>
        </report>
    :)
    let $schemaIssues           := 
        try {
            let $messageSchemaFile  := 
                if ($messageInstance instance of element(de:error)) then () else (
                    val:getSchemaFile($messageInstance, $baseCollectionXSD, true())
                )
            let $schemaReport       :=
                if ($messageInstance instance of element(de:error)) then (
                    <issues>
                        <status>invalid</status>
                        <exception>
                            <message>{$messageInstance//text()}</message>
                        </exception>
                    </issues>
                ) 
                else (
                    if ($messageInstance[self::hl7:ClinicalDocument]) then
                        val:validateSchemaCDA($messageInstance,$messageSchemaFile)
                    else (
                        validation:jaxv-report($messageInstance,doc($messageSchemaFile))
                    )
                )
            
            return 
                if ($schemaReport[status='valid']) then (
                    <issue type="schema" role="info" count="1" file="{tokenize($messageSchemaFile,'/')[last()]}">
                        <description>thiswillbecut:INFO no errors in XML Schema validation.</description>
                        <location line="1"/>
                    </issue>
                )
                else 
                if ($schemaReport//*[@level=('Error','Warning')]) then
                    for $schemaIssue in $schemaReport//*[@level=('Error','Warning')]
                    let $location      := concat($schemaIssue/@line,':',$schemaIssue/@column)
                    return
                    <issue type="schema"
                           role="{lower-case($schemaIssue/@level)}" 
                           count="{if ($schemaIssue/@repeat) then $schemaIssue/@repeat else ('1')}"
                           file="{tokenize($messageSchemaFile,'/')[last()]}">
                        <description>{$schemaIssue/text()}</description>
                        <location line="{$location}"/>
                    </issue>
                else 
                    (: Ensures an unexpected error is thrown unless there are warnings/errors or file has status 'valid'. The 'thiswillbecut' is needed since groupIssues cuts all before first colon :) 
                    <issue type="schema" 
                           role="error"
                           count="1"
                           file="{tokenize($messageSchemaFile,'/')[last()]}">
                        <description>thiswillbecut:ERROR in main XML Schema validation, {$schemaReport/exception/message/string()}, {$schemaReport/exception/stacktrace/string()}</description>
                        <location line="1"/>
                    </issue>
        } 
        catch * {
            <issue type="schema" role="error" count="1" file="{tokenize($messageSchemaFile,'/')[last()]}">
                <description>thiswillbecut:ERROR {$err:code} in main XML Schema validation: {$err:description, "', module: ", $err:module, "(", $err:line-number, ",", $err:column-number, ")"}</description>
                <location line="1"/>
            </issue>
        }
    
    let $seriousSchemaErrors    := $schemaIssues//description[starts-with(.,'UndeclaredPrefix')]
    (: ==== END XML Schema validation of messageInstance. Requires flattened XSD (!) ==== :)
    
    let $g := if ($val:debug) then (util:log('DEBUG', '============ START MAIN Schematron validation of instance')) else ()
    
    (: ==== START Schematron validation of messageInstance. Requires SVRL version of SCH (!) ==== :)
    let $schematronIssues       :=
        if ($seriousSchemaErrors) then (
            <issue type="schematron" role="error" test="." see="" flag="">
                <description>Did not check schematron due to the detection of too serious XML schema errors. Fix these XML schema errors first.</description>
                <location path="/"/>
            </issue>
        ) else (
            try {
                let $messageSchematronFile  := val:getSchematronFile($messageInstance, $baseCollectionSCH)
                return
                    val:validateSchematron($messageInstance, $messageSchematronFile)
            }
            catch * {
                <issue type="schematron" role="error" test="." see="" flag="">
                    <description>ERROR {$err:code} in main Schematron validation: {$err:description, "', module: ", $err:module, "(", $err:line-number, ",", $err:column-number, ")"}</description>
                    <location line=""/>
                </issue>
            }
        )
    (: ==== END Schematron validation of messageInstance. Requires SVRL version of SCH (!) ==== :)
    
    (: ==== START saving messageInstance attachments. ==== :)
    let $embeddedStorage      :=
        if ($val:debug) then (
            try {
                for $b64 in $messageInstance//*[string(@representation)='B64' or string(@xsi:type)='xs:base64Binary']
                let $mediaType    := $b64/@mediaType/string()
                let $resourceUri  := ($b64/hl7:reference/@value/string())[1]
                let $resourceName := concat(util:document-name($messageInstance),'_',$b64/position())
                let $resourceName := concat($resourceName, val:getExtension($mediaType,$resourceUri))
                let $g   := util:log('DEBUG', concat('============ STORE EMBEDDED BASE64 as separate file: ',$resourceName))
                return
                    val:saveAsResource($resourceName, xs:base64Binary(($b64/text())[1]), $mediaType)
            }
            catch * {
                concat('ERROR', $err:code, ' in saving attachment: ', $err:description, ', module: ', $err:module, '(', $err:line-number, ',', $err:column-number, ')')
            }
        ) else ()
    let $g := if ($val:debug) then (util:log('DEBUG', concat('============ STORE EMBEDDED BASE64 as separate file succeeded? ',string-join($embeddedStorage,' ')))) else ()
    (: ==== END saving messageInstance attachments. ==== :)
    
    let $g := if ($val:debug) then (util:log('DEBUG', concat('============ START EMBEDDED XML Schema validation of embedded instances (if any) with ',$messageSchemaFile))) else ()
    
    (: ==== START XML Schema validation of B64 encoded parts in messageInstance. Requires flattened CDA XSD (!) ==== :)
    (:            handle all B64 encoded instances in for loop and add to schematronReport:)
    let $embeddedSchemaIssues :=  
        if ($seriousSchemaErrors) then (
            <issue type="schema" role="warning" count="1" file="{tokenize($messageSchemaFile,'/')[last()]}">
                <description>Did not check schema of any embedded fragments due to the detection of too serious XML schema errors in the main message. Fix these XML schema errors first.</description>
                <location path="/"/>
            </issue>
        ) else (
            try {
                for $b64 in $messageInstance//*[string(@representation)='B64' or string(@xsi:type)='xs:base64Binary']
                let $embedPath                := val:path-to-node-with-pos($b64)
                (: decode :)
                let $decodedString            := util:base64-decode($b64)
                (: parse into XML :)
                let $decodedMessage           := 
                    try {
                        fn:parse-xml($decodedString)
                    }
                    catch * {
                        <de:error xmlns:de="http://art-decor.org/ns/error">{concat($err:code, ' while getting message instance: ', $err:description, ', module: ', $err:module, '(', $err:line-number, ',', $err:column-number, ')')}</de:error>
                    }
                let $embeddedSchemaReport     := 
                    if ($decodedMessage instance of element(de:error)) then (
                        <noerror/>
                    )
                    else
                    if ($decodedMessage instance of element(hl7:ClinicalDocument)) then (
                        val:validateSchemaCDA($decodedMessage,$messageSchemaFile)
                    )
                    else (
                    )
                return
                    for $schemaIssue in $embeddedSchemaReport//*[@level='Warning' or @level='Error']
                    let $location             := concat($schemaIssue/@line,':',$schemaIssue/@column)
                    return
                        <issue type="schema" role="{if ($schemaIssue/@level='Error') then 'error' else($schemaIssue/@level)}" count="{if ($schemaIssue/@repeat) then $schemaIssue/@repeat else ('1')}" embed="{$embedPath}" file="{tokenize($messageSchemaFile,'/')[last()]}">
                            <description>{$schemaIssue/text()}</description>
                            <location line="{$location}"/>
                        </issue>
            }
            catch * {
                <issue type="schema" role="error">
                    <description>ERROR {$err:code} in XML Schema validation: {$err:description, "', module: ", $err:module, "(", $err:line-number, ",", $err:column-number, ")"}</description>
                    <location line=""/>
                </issue>
            }
        )
    (: ==== END XML Schema validation of B64 encoded parts in messageInstance. Requires flattened CDA XSD (!) ==== :)
    
    let $g := if ($val:debug) then (util:log('DEBUG', '============ START EMBEDDED Schematron validation of embedded instances.')) else ()
    
    (: ==== START Schematron validation of B64 encoded parts in messageInstance. Requires SVRL version of SCH (!) ==== :)
    (:            TODO: this refers hardcoded to schematronfile peri20-seo-1c.xsl, should be replaced with a function to derive correct schematron :)
    let $embeddedSchematronIssues := 
        if ($seriousSchemaErrors) then (
            <issue type="schematron" role="warning" test="." see="" flag="">
                <description>Did not check schematron of any embedded fragments due to the detection of too serious XML schema errors in the main message. Fix these XML schema errors first.</description>
                <location path="/"/>
            </issue>
        ) else (
            try {
                for $b64 in $messageInstance//*[string(@representation)='B64' or string(@xsi:type)='xs:base64Binary']
                let $embedPath                := val:path-to-node-with-pos($b64)
                (:decode:)
                let $decodedString     := util:base64-decode($b64)
                (: parse into XML and get first node (otherwise we are document-node level and getSchematronFile will fail to get the root element name) :)
                let $decodedMessage    := 
                    try {
                        fn:parse-xml($decodedString)/*
                    }
                    catch * {
                        <de:error xmlns:de="http://art-decor.org/ns/error">{concat($err:code, ' while getting message instance: ', $err:description, ', module: ', $err:module, '(', $err:line-number, ',', $err:column-number, ')')}</de:error>
                    }
                let $embeddedSchematronReport := 
                    if ($decodedMessage instance of element(de:error)) then (
                        <noerror/>
                    )
                    else
                    if ($decodedMessage instance of element(hl7:ClinicalDocument)) then (
                        val:validateSchematronCDA($decodedMessage, $baseCollectionSCH)
                    ) 
                    else (
                    )
                return
                    if (not(empty($embeddedSchematronReport))) then (
                        for $issue in $embeddedSchematronReport//svrl:*[@test][@location]
                        let $location   := replace($issue/@location,'\*:([^\[]+)\[namespace-uri\(\)=''urn:hl7-org:v3''\]','hl7:$1')
                        let $role       := 
                            if ($issue[@role='information']) then 'info' else 
                            if ($issue[@role='warn']) then 'warning' else 
                            if ($issue[@role]) then ($issue/@role) 
                            else 'error'
                        return
                            <issue type="schematron" role="{$role}" test="{$issue/@test}" embed="{$embedPath}">
                            {   $issue/@see, $issue/@flag }
                                <description>{$issue/svrl:text/text()}</description>
                                <location path="{$location}"/>
                            </issue>
                    ) else ()
            }
            catch * {
                <issue type="schematron" role="error">
                    <description>ERROR {$err:code} in Schematron validation on embedded instance: {$err:description, "', module: ", $err:module, "(", $err:line-number, ",", $err:column-number, ")"}</description>
                    <location path=""/>
                </issue>
            }
        )
    (: ==== END Schematron validation of B64 encoded parts in messageInstance. Requires SVRL version of SCH (!) ==== :)
    
    let $issueReport := <validationReport validationBase="{$validationBase}">{$schemaIssues|$schematronIssues|$embeddedSchemaIssues|$embeddedSchematronIssues}</validationReport>
    (: DB path to XSL that handles the final returned view of all errors/warnings :)
    
    (: Cleanup of hacky stuff when we retrieved the message :)
    let $deleteTempFile     := adxfiles:deleteFile($account, $tempFile)
    
    return val:makeIssueReport($issueReport)
};

declare function val:pseudoValidation ($file as xs:string, $account as xs:string, $validationBase as xs:string) as node()* {
    let $schemaIssues :=
        <issue type="schema" role="error">
            <description>Message not validated against XML Schema. XML validation is switched off for this account</description>
            <location path=""/>
        </issue>
    let $schematronIssues :=
        <issue type="schematron" role="warning">
            <description>Message not validated against Schematron. XML validation is switched off for this account</description>
            <location path=""/>
        </issue>
    
    let $issueReport := <validationReport validationBase="{$validationBase}">{$schemaIssues|$schematronIssues}</validationReport>
    (: DB path to XSL that handles the final returned view of all errors/warnings :)
    return val:makeIssueReport($issueReport)
};

declare function val:revalidate($account as xs:string, $file as xs:string, $xpath as xs:string?) as xs:boolean {
    val:revalidate($account, $file, $xpath, adxaccounts:getXmlResourcesPaths($account)[1], false())
};
declare function val:revalidate($account as xs:string, $file as xs:string, $xpath as xs:string?, $validationBase as xs:string) as xs:boolean {
    val:revalidate($account, $file, $xpath, $validationBase, false())
};
declare function val:revalidate($account as xs:string, $file as xs:string, $xpath as xs:string?, $validationBase as xs:string, $forceRevalidation as xs:boolean) as xs:boolean {
    (: create on option to switch validation off :)
    (: set this option on test-accounts.xml, for example :)
    (:   <testAccount id="3" name="route66" displayName="Route 66" validationSwitchOff="true"> :)
    let $validationSwitchOff := 
        if (empty($account)) then (false()) else (
            exists(doc($get:strTestAccounts)//xis:testAccount[@name=$account]//xis:xmlValidation[string(.)='false'])
        )
    
    (: write the $validationSwitchOff to exist log :)
    let $g                  := util:log('DEBUG', concat('---($validationSwitchOff)--- Is validation on (switch=false=default) or off (switch=true) ', $validationSwitchOff))
    let $currentVal         := adxfiles:getReports($account, $file, $xpath, $validationBase)
    
    return 
    if (string-length($account)>0 and string-length($file)>0 and string-length($xpath)>0 and string-length($validationBase)>0) then
        
        if ($currentVal and not($forceRevalidation)) then (
            (: No need for anything. Message was already done with this package :)
            let $store          := adxfiles:saveReport($account, $file, $xpath, $validationBase, ())
            
            return false()
        ) else if ($validationSwitchOff) then (
            (: Does pseudo-validation if validation is turned off :) 
            let $validationData := <message path="{$xpath}">{val:pseudoValidation($file, $account, $validationBase)}</message>
            let $store          := adxfiles:saveReport($account, $file, $xpath, $validationBase, $validationData)
            
            return false()
        ) else (
            let $validationData := <message path="{$xpath}">{val:validateMessage($file, $account, $xpath, $validationBase)}</message>
            (: Does (re)validation if the message inside the file was not validated before :) 
            (: Does revalidation if validationBase for this message has changed in the account since last validation :)
            let $store          := adxfiles:saveReport($account, $file, $xpath, $validationBase, $validationData)
            
            return true()
        )
    else (false())
};

(: == TESTSUITES == :)
declare function val:getTestSuites() as element(testsuite)* {
    doc($get:strTestSuites)//testsuite
};
declare function val:getTestSuiteById($id as xs:string?) as element(testsuite)? {
    if (empty($id)) then () else (
        val:getTestSuites()[@id=$id]
    )
};
declare function val:getTestSuitesByXmlResources($resources as xs:string?) as element(testsuite)* {
    if (empty($resources)) then () else (
        val:getTestSuites()[xmlResourcesPath=$resources]
    )
};
declare function val:getTestSuiteXmlResourcesById($id as xs:string?) as xs:string? {
    if (empty($id)) then () else (
        val:getTestSuiteById($id)/xmlResourcesPath
    )
};
