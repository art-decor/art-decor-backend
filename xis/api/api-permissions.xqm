xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
module namespace adpfix             = "http://art-decor.org/ns/xis-permissions";
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
declare namespace sm                = "http://exist-db.org/xquery/securitymanager";
declare namespace xis               = "http://art-decor.org/ns/xis";

(:  Mode            Octal
 :  rw-r--r--   ==  0644
 :  rw-rw-r--   ==  0664
 :  rwxr-xr--   ==  0754
 :  rwxr-xr-x   ==  0755
 :  rwxrwxr-x   ==  0775
 :  rwxrwxrw-   ==  0776
 :)

(:
:   Call to fix any potential permissions problems in the paths:
:       /db/apps/xis
:       /db/apps/xis-data
:   Dependency: $get:strXisData, $get:strXisHelperConfig, $get:strXisAccounts
:   NOTE: path /db/apps/decor/core has its own installer
:)
declare function adpfix:setXisPermissions() {
    adpfix:checkIfUserDba(),
    
    (:install stuff from the xis package if necessary:)
    adpfix:installSettings(),
    
    (:add xis to the admin group if necessary:)
    if (sm:get-user-groups('admin')='xis') then () else (sm:add-group-member('xis','admin')),
    
    (:set permissions on xis-data collection:)
    sm:chown(xs:anyURI($get:strXisData),'admin:xis'),
    sm:chmod(xs:anyURI($get:strXisData),sm:octal-to-mode('0775')),
    sm:clear-acl(xs:anyURI($get:strXisData)),
    
    (:set permissions on xis/modules queries:)
    adpfix:setXisQueryPermissions(),
    
    (:default for all resources immediately under xis-data:)
    for $res in xmldb:get-child-resources($get:strXisData)
    return (
        sm:chown(xs:anyURI(concat($get:strXisData,'/',$res)),'admin:xis'),
        sm:chmod(xs:anyURI(concat($get:strXisData,'/',$res)),sm:octal-to-mode('0664')),
        sm:clear-acl(xs:anyURI(concat($get:strXisData,'/',$res)))
    )
    ,
    (:exceptions for these resources immediately under xis-data:)
    for $res in xmldb:get-child-resources($get:strXisData)[.=('soap-service-list.xml')]
    return (
        sm:chown(xs:anyURI(concat($get:strXisData,'/',$res)),'admin:xis'),
        sm:chmod(xs:anyURI(concat($get:strXisData,'/',$res)),sm:octal-to-mode('0644')),
        sm:clear-acl(xs:anyURI(concat($get:strXisData,'/',$res)))
    )
    ,
    adpfix:setXisAccountPermissions()
    ,
    if (xmldb:collection-available($get:strXisHelperConfig)) then
        adpfix:setPermissions($get:strXisHelperConfig, 'admin', 'xis', sm:octal-to-mode('0755'), 'admin', 'xis', sm:octal-to-mode('0644'))
    else (),
    if (xmldb:collection-available(concat($get:strXisData,'/vocab'))) then
        adpfix:setPermissions(concat($get:strXisData,'/vocab'), 'admin', 'xis', sm:octal-to-mode('0755'), 'admin', 'xis', sm:octal-to-mode('0644'))
    else ()
};

(:~ helper function for creating test account collections and settings permissions
:   @param $createaccounts  Optional. If omitted, creates accounts for every listed account in the test-accounts.xml file. Otherwise only for those listed in $createaccounts that match an account in test-accounts.xml based on @name
:)
declare function adpfix:createTestAccounts($createaccounts as element(xis:testAccount)*) {
    let $check              := adpfix:checkIfUserDba()
    
    let $accounts           :=
        if ($createaccounts) then
            doc(concat($get:strXisData,'/test-accounts.xml'))/xis:testAccounts/xis:testAccount[@name = $createaccounts/@name]
        else
            doc(concat($get:strXisData,'/test-accounts.xml'))/xis:testAccounts/xis:testAccount
    
    let $accounts           := 
        for $account in $accounts
        let $accountName    := $account/@name
        let $groupadd       := (: create group if needed :)
            if ($accountName) then (
                if (sm:group-exists($accountName)) then () else (
                    sm:create-group($accountName,'admin','test account')
                )
            ) else ()
        let $accountadd     := xmldb:create-collection($get:strXisAccounts,$accountName) (: create collections if needed :)
            
        let $userupdate     := (: if users exist in database, add users to group. if not, delete user from test account :)
            for $user in $account/xis:members/xis:user
            let $username := $user/@id
            return
                if (sm:user-exists($username)) then
                    (:if this user is an eXist-db user, but is not yet in the test account group, add him:)
                    if (sm:get-user-groups($username)=$accountName) then () else (
                        sm:add-group-member($accountName, $username) 
                    )
                else (
                    (:if this user is not an eXist-db user, then delete this user from the test account:)
                    update delete $user
                )
        let $userupdate     := (:remove users in the eXist-db accounts group that are not configured to be in that group:)
            for $username in sm:get-group-members($accountName)[not(.=$account/xis:members/xis:user/@id)]
            return sm:remove-group-member($accountName, $username)
        return ()
    
    let $permissions        := 
        if ($createaccounts[*:new]) then
            adpfix:setXisAccountPermissions()
        else ()
    
    return ()
};

(: helper function to delete test account collections and settings permissions :)
declare function adpfix:deleteTestAccount($accountId as xs:string) as xs:string* {
    let $check              := adpfix:checkIfUserDba()
    
    let $accountConfig      := 
        doc(concat($get:strXisData,'/test-accounts.xml'))/xis:testAccounts/xis:testAccount[@id=$accountId]
    let $account            := $accountConfig/@name
    
    let $delete             := 
        if (string-length($account)>0) then (
            if (xmldb:collection-available(concat($get:strXisAccounts,'/',$account))) then
                xmldb:remove(concat($get:strXisAccounts,'/',$account))
            else ()
            ,
            sm:remove-group($account)
            ,
            update delete $accountConfig 
        )
        else ()
    
    return $account
};

(:
:   Call to fix any potential permissions problems in the path /db/apps/xis/modules
:   Dependency: $get:strXis
:)
declare %private function adpfix:setXisQueryPermissions() {
    for $query in xmldb:get-child-resources(xs:anyURI(concat($get:strXis,'/modules')))
    return (
        sm:chown(xs:anyURI(concat($get:strXis,'/modules/',$query)),'admin:xis'),
        if (starts-with($query,('art-decor','check','collect','get','login','retrieve','search', 'validate'))) then
            sm:chmod(xs:anyURI(concat($get:strXis,'/modules/',$query)),sm:octal-to-mode('0755'))
        else(
            sm:chmod(xs:anyURI(concat($get:strXis,'/modules/',$query)),sm:octal-to-mode('0754'))
        )
        ,
        sm:clear-acl(xs:anyURI(concat($get:strXis,'/modules/',$query)))
    )
};

(: helper function to set up the right collections :)
declare function adpfix:setupXisAccountCollections($accountName as xs:string) {
    for $c in ('files', 'messages', 'reports')
    return 
        xmldb:create-collection(concat($get:strXisAccounts,'/',$accountName), $c)
    ,
    if (xmldb:get-child-resources(concat($get:strXisAccounts,'/',$accountName))[. = 'testseries.xml']) then () else (
        xmldb:store(concat($get:strXisAccounts,'/',$accountName), 'testseries.xml', <xis:tests/>)
    )
};

(:
:   Call to fix any potential permissions problems in the path /db/apps/xis-data/accounts
:   Dependency: $get:strXisAccounts
:)
declare function adpfix:setXisAccountPermissions() {
    adpfix:setXisAccountPermissions(())
};
declare function adpfix:setXisAccountPermissions($account as xs:string?) {
    let $accountlist        := xmldb:get-child-collections($get:strXisAccounts)
    let $accountlist        := if (empty($account)) then $accountlist else $accountlist[. = $account]
    
    let $update             := sm:chown(xs:anyURI($get:strXisAccounts),'admin:xis')
    let $update             := sm:chmod(xs:anyURI($get:strXisAccounts),'rwxrwxr-x')
    let $update             := sm:clear-acl(xs:anyURI($get:strXisAccounts))
    
    let $update             := 
        for $accountcoll in $accountlist
        let $createCollections  := adpfix:setupXisAccountCollections($accountcoll)
        let $chown              := concat('admin:',$accountcoll)
        return
            if (sm:group-exists($accountcoll)) then
                if (matches($accountcoll,'art-decor')) then (
                    (:need full permissions as this account is used by guest. guest needs to read and write:)
                    adpfix:setPermissions(concat($get:strXisAccounts,'/',$accountcoll), (), 'xis', 'rwxrwsrwx', 'admin', 'xis', 'rwxrwsrw-')
                )
                else (
                    adpfix:setPermissions(concat($get:strXisAccounts,'/',$accountcoll), 'admin', $accountcoll, 'rwxrws---', (), $accountcoll, 'rw-rw----')
                )
            else ()
    
    return ()
};

(: Helper function with recursion for adpfix:setXisPermissions() :)
declare %private function adpfix:setPermissions($path as xs:string, $collusrown as xs:string?, $collgrpown as xs:string?, $collmode as xs:string, $resusrown as xs:string?, $resgrpown as xs:string?, $resmode as xs:string) {
    if (string-length($collusrown) = 0) then () else sm:chown(xs:anyURI($path),$collusrown),
    if (string-length($collgrpown) = 0) then () else sm:chgrp(xs:anyURI($path),$collgrpown),
    sm:chmod(xs:anyURI($path),$collmode),
    sm:clear-acl(xs:anyURI($path)),
    for $res in xmldb:get-child-resources(xs:anyURI($path))
    return (
        if (empty($resusrown)) then () else sm:chown(xs:anyURI(concat($path,'/',$res)),$resusrown),
        if (empty($resgrpown)) then () else sm:chgrp(xs:anyURI(concat($path,'/',$res)),$resgrpown),
        sm:chmod(xs:anyURI(concat($path,'/',$res)),$resmode),
        sm:clear-acl(xs:anyURI(concat($path,'/',$res)))
    )
    ,
    for $collection in xmldb:get-child-collections($path)
    return
        adpfix:setPermissions(concat($path,'/',$collection), $collusrown, $collgrpown, $collmode, $resusrown, $resgrpown, $resmode)
};

declare %private function adpfix:installSettings() {
    if (doc-available(concat($get:strXisData,'/test-accounts.xml'))) then () else (
        xmldb:copy-resource($get:strXisResources,'test-accounts.xml',$get:strXisData,'test-accounts.xml')
    ),
    if (doc-available(concat($get:strXisData,'/soap-service-list.xml'))) then () else (
        xmldb:copy-resource($get:strXisResources,'soap-service-list.xml',$get:strXisData,'soap-service-list.xml')
    ),
    if (doc-available(concat($get:strXisData,'/test-suites.xml'))) then () else (
        xmldb:store($get:strXisData,'test-suites.xml', <testsuites/>)
    ),
    if (xmldb:collection-available($get:strXisHelperConfig)) then () else (
        xmldb:create-collection($get:strXisData,'data'),
        xmldb:copy-collection(concat($get:strXis,'/data'),$get:strXisData)
    ),
    (:used to have a different location. move first if exists:)
    if (xmldb:collection-available(concat($get:strXisData,'/vocab'))) then (
        xmldb:move(concat($get:strXisData,'/vocab'),$get:strXisHelperConfig)
    ) else ()
    ,
    (:if didn't exist get from install package:)
    if (xmldb:collection-available(concat($get:strXisHelperConfig,'/vocab'))) then () else (
        xmldb:create-collection($get:strXisHelperConfig,'vocab'),
        xmldb:copy-collection(concat($get:strXisResources,'/vocab'),$get:strXisHelperConfig)
    )
};

declare %private function adpfix:checkIfUserDba() {
    if (sm:is-dba(get:strCurrentUserName())) then () else (
        error(QName('http://art-decor.org/ns/art-decor-permissions', 'NotAllowed'), concat('Only dba user can use this module. ',get:strCurrentUserName()))
    )
};
