xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
module namespace adxaccounts        = "http://art-decor.org/ns/art-decor/xis/accounts";
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace adpfix      = "http://art-decor.org/ns/xis-permissions" at "api-permissions.xqm";

declare namespace xis = "http://art-decor.org/ns/xis";

declare variable $adxaccounts:testAccounts          := 
    if (doc-available($get:strTestAccounts)) then 
        doc($get:strTestAccounts)/xis:testAccounts
    else ();
declare variable $adxaccounts:strInteractionViewer  := 
    if (doc-available(concat('xmldb:exist://',$get:strXisResources,'/stylesheets/message2html.xsl'))) then
        concat('xmldb:exist://',$get:strXisResources,'/stylesheets/message2html.xsl')
    else ();
declare variable $adxaccounts:strCDAr2Viewer        := 
    if (doc-available(concat('xmldb:exist://',$get:strCdaXsl))) then
        concat('xmldb:exist://',$get:strCdaXsl)
    else ();

declare function adxaccounts:getAccount($account as xs:string?) as element()? {
    $adxaccounts:testAccounts/xis:testAccount[(@id|@name)=$account]
};
declare function adxaccounts:shouldValidate($account as xs:string?) as xs:boolean {
    exists(adxaccounts:getAccount($account)/xis:xis/xis:xmlValidation[string(.)='false'])
};
declare function adxaccounts:getXmlResourcesPaths($account as xs:string?) as xs:string* {
    adxaccounts:getAccount($account)/xis:xis/xis:xmlResourcesPath[string-length() gt 0]
};
declare function adxaccounts:getDefaultXmlResourcesPath($account as xs:string?) as xs:string* {
    let $paths  := adxaccounts:getAccount($account)/xis:xis/xis:xmlResourcesPath
    
    return
        if ($paths[@default='true']) then $paths[@default='true'] else $paths[1]
};
declare function adxaccounts:getAllViewers($account as xs:string?) as element(viewer)* {
    let $resources          :=
        adxaccounts:getXmlResourcesPaths($account)
    
    let $packageviewers     :=
        for $path in $resources
        let $viewers        := adxaccounts:getViewersByPackage($resources)
        return (
            for $viewer in $viewers
            return
                <viewer source="{$path}">{$viewer/node()}</viewer>
        )
    
    return (
        $packageviewers, 
        if (string-length($adxaccounts:strCDAr2Viewer)>0) then
            <viewer rootelement="ClinicalDocument" namespace="urn:hl7-org:v3" viewer="{$adxaccounts:strCDAr2Viewer}"/>
        else (),
        <viewer namespace="urn:hl7-org:v3" viewer="{$adxaccounts:strInteractionViewer}"/>
    )
};
declare function adxaccounts:getViewersByPackage($resources as xs:string?) as element(viewers)* {
    let $viewers-collection := concat($get:strHl7, '/', $resources,'/viewers')
    
    return
        if (string-length($resources) = 0) then () else
        if (xmldb:collection-available($viewers-collection)) then
            collection($viewers-collection)/viewers
        else ()
};
declare function adxaccounts:getFullViewerPath($resources as xs:string, $viewer as xs:string) as xs:string {
    if (matches($viewer,'^http?s://')) then $viewer else (
        concat($get:strHl7, '/', $resources, '/viewers/', $viewer)
    )
};