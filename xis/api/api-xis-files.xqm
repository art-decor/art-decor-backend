xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
module namespace adxfiles           = "http://art-decor.org/ns/art-decor/xis/files";
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace adpfix      = "http://art-decor.org/ns/xis-permissions" at "api-permissions.xqm";
import module namespace val         = "http://art-decor.org/ns/art-decor/xis/validation" at "api-xis-validation.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../art/api/api-server-settings.xqm";

declare namespace xmldb             = "http://exist-db.org/xquery/xmldb";
declare namespace util              = "http://exist-db.org/xquery/util";
declare namespace validation        = "http://exist-db.org/xquery/validation";
declare namespace soap              = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace http              = "http://expath.org/ns/http-client";
declare namespace xsi               = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace xs                = "http://www.w3.org/2001/XMLSchema";
declare namespace hl7               = "urn:hl7-org:v3";
declare namespace xis               = "http://art-decor.org/ns/xis";
declare namespace de                = "http://art-decor.org/ns/error";

declare variable $adxfiles:maxResults           := 50;

declare variable $adxfiles:_existing_accounts   := xmldb:get-child-collections($get:strXisAccounts);

declare variable $adxfiles:acceptMediaType      := ('text/xml','application/xml');
declare variable $adxfiles:interactions         := collection($get:strXisHelperConfig)//hl7:code[@codeSystem='2.16.840.1.113883.1.6'];
declare variable $adxfiles:applications         := collection($get:strXisHelperConfig)//xis:application;
declare variable $adxfiles:patients             := collection($get:strXisHelperConfig)//hl7:Patient;

declare variable $adxfiles:_delete-all              := 'all';
declare variable $adxfiles:_delete-renderings-only  := 'renderings-only';
declare variable $adxfiles:_delete-validation-only  := 'validation-only';
declare variable $adxfiles:_delete-testseries-only  := 'testseries-only';
declare variable $adxfiles:_delete-types            := ($adxfiles:_delete-all,$adxfiles:_delete-renderings-only,$adxfiles:_delete-validation-only,$adxfiles:_delete-testseries-only);

(:~ eXist-db subdir under an account that holds uploaded/incoming files :)
declare variable $adxfiles:_filedir                 := 'files';
(:~ eXist-db subdir under an account that holds expanded attachments contained in a file combined with meta data and validation results. File format 'filename_i.ext'
    <file name="original_name_if_uploaded_or_generated name" created="date_added_to_account" mediaType="file_type">
        <meta> --all extracted summary information for message index-- </meta>
        <data> --the actual message data as-is if XML or base64 otherwise-- </data>
        <reports> --validation reports, 1 per base HL7 resource-- <reports>
    </file>
:)
declare variable $adxfiles:_attachmentdir           := 'attachments';
(:~ OLD: eXist-db subdir under an account that holds uploaded/incoming files as-is without metadata. When files are encountered here, they are moved to $adxfiles:_filedir, including information in $adxfiles:_reportsdir. Processed contents are then deleted here :)
declare variable $adxfiles:_messagedir              := 'messages';
(:~ OLD: eXist-db subdir under an account that holds reports for messages. When files are encountered here, they are moved to $adxfiles:_filedir :)
declare variable $adxfiles:_reportsdir              := 'reports';
(:~ eXist-db subdir under an account that holds temp files before zipping. :)
declare variable $adxfiles:_zipdir                  := 'zip';

declare variable $adxfiles:_filestructure           := <file name="" created="" mediaType="" username=""><meta/><data/><reports/></file>;

declare function adxfiles:getMessageTypeList($account as xs:string, $sort as xs:string?) as element(result) {
let $messages           := adxfiles:getIndex($account, ())/message

let $result             := 
    switch ($sort) 
    case 'root-element' return for $message in $messages order by $message/@rootelement return $message
    case 'root-name'    return for $message in $messages order by $message/@rootname return $message
    default             return for $message in $messages order by $message/parent::file/xs:dateTime(@created) descending return $message

let $totalcount         := count($result)
(:let $offset             := if ($offset > $totalcount) then 1 else $offset
let $max                := if ($max < 1) then $totalcount else $max
let $returnresults      := subsequence($result,$offset,$max):)
let $returnresults      := 
    for $groupedbyroot in $result
    let $rootelement    := $groupedbyroot/@rootelement
    group by $rootelement
    return $groupedbyroot[1]
    
let $currentcount       := count($returnresults)

return
(:<result page-current="{floor($offset div $max) + 1}" 
        page-total="{floor($totalcount div $max) + 1}" 
        current="{$currentcount}" 
        total="{$totalcount}" 
        offset="{$offset}" 
        max="{$max}" 
        sort="{$sort}">:)
<result account="{$account}"
        current="{$currentcount}" 
        total="{$totalcount}" 
        sort="{$sort}">
{
    for $message in $returnresults
    return
        <message>
        {
            $message/@rootelement, $message/@rootname
        }
        </message>
}
</result>
};

declare function adxfiles:getMessageList($account as xs:string, $sort as xs:string?, $offset as xs:integer, $max as xs:integer) as element(result) {
    adxfiles:getMessageList($account, $sort, $offset, $max, (), (), ())
};
declare function adxfiles:getMessageList($account as xs:string, $sort as xs:string?, $offset as xs:integer, $max as xs:integer, $searchTerms as xs:string*) as element(result) {
    adxfiles:getMessageList($account, $sort, $offset, $max, $searchTerms, (), ())
};
declare function adxfiles:getMessageList($account as xs:string, $sort as xs:string?, $offset as xs:integer, $max as xs:integer, $searchTerms as xs:string*, $beforeDate as xs:date?, $rootElement as xs:string?) as element(result) {
let $messages           := adxfiles:getIndex($account, ())/message

(: TODO extend search functionality to more than just filename :)
(: TODO @filename is currently not indexed :)
let $messages           :=
    if (empty($searchTerms)) then $messages else (
        $messages[contains(lower-case(@filename), $searchTerms[1])]
    )
let $messages           :=
    if (empty($beforeDate)) then $messages else (
        $messages[xs:date(substring(@created, 1, 10)) lt $beforeDate]
    )
let $messages           :=
    if (empty($rootElement)) then $messages else (
        $messages[starts-with(@rootelement, $rootElement)]
    )

let $result             := 
    switch ($sort) 
    case 'file'         return for $message in $messages order by $message/parent::file/@filename return $message
    case 'root-element' return for $message in $messages order by $message/@rootelement return $message
    case 'root-name'    return for $message in $messages order by $message/@rootname return $message
    case 'patient-name' return for $message in $messages order by $message/patient[1]/@name return $message
    case 'from'         return for $message in $messages order by $message/sender/@extension,$message/sender/@name return $message
    case 'to'           return for $message in $messages order by $message/receiver/@extension,$message/receiver/@name return $message
    default             return for $message in $messages order by $message/parent::file/xs:dateTime(@created) descending return $message

let $totalcount         := count($result)
let $offset             := if ($offset > $totalcount) then 1 else $offset
let $max                := if ($max < 1) then $totalcount else $max
let $returnresults      := subsequence($result,$offset,$max)
let $currentcount       := count($returnresults)

return
<result page-current="{floor($offset div $max) + 1}" 
        page-total="{floor($totalcount div $max) + 1}" 
        current="{$currentcount}" 
        total="{$totalcount}" 
        offset="{$offset}" 
        max="{$max}" 
        sort="{$sort}">
{
    for $message in $returnresults
    return
        <message>
        {
            $message/@*,$message/node(),
            for $validationReport in adxfiles:getReports($account, $message/@filename, $message/@xpath, ())/validationReport
            let $status := if ($validationReport//error) then 'error' else if ($validationReport//warning) then 'warning' else 'valid'
            return
                <validation path="{$validationReport/@validationBase}" status="{$status}"/>
            ,
            for $rendering in adxfiles:getRendering($account, $message/@filename, $message/@xpath, (), '*')[@viewer]
            return
                <rendering>{$rendering/@*}</rendering>
        }
        </message>
}
</result>
};

declare function adxfiles:getFileList($account as xs:string, $sort as xs:string?, $offset as xs:integer, $max as xs:integer) as element(result) {
    adxfiles:getFileList($account, $sort, $offset, $max, (), (), ())
};

declare function adxfiles:getFileList($account as xs:string, $sort as xs:string?, $offset as xs:integer, $max as xs:integer, $searchTerms as xs:string*) as element(result) {
    adxfiles:getFileList($account, $sort, $offset, $max, $searchTerms, (), ())
};

declare function adxfiles:getFileList($account as xs:string, $sort as xs:string?, $offset as xs:integer, $max as xs:integer, $searchTerms as xs:string*, $beforeDate as xs:date?, $rootElement as xs:string?) as element(result) {
let $files              := adxfiles:getIndex($account, ())

(: TODO extend search functionality to more than just filename :)
(: TODO @filename is currently not indexed :)
let $files              :=
    if (empty($searchTerms)) then $files else (
        $files[contains(lower-case(@filename), $searchTerms[1])]
    )
let $files              :=
    if (empty($beforeDate)) then $files else (
        $files[xs:date(substring(@created, 1, 10)) lt $beforeDate]
    )
let $files              :=
    if (empty($rootElement)) then $files else (
        $files[starts-with(@rootelement, $rootElement)] | $files[message[starts-with(@rootelement, $rootElement)]]
    )

let $result             :=
    switch ($sort) 
    case 'file'         return for $file in $files order by $file/@filename return <file>{$file/@*}</file>
    case 'root-element' return for $file in $files order by $file/@rootelement return <file>{$file/@*}</file>
    case 'root-name'    return for $file in $files order by $file/@rootname return <file>{$file/@*}</file>
    case 'type'         return for $file in $files order by $file/@mediaType return <file>{$file/@*}</file>
    default             return for $file in $files order by $file/xs:dateTime(@created) descending return <file>{$file/@*}</file>

let $totalcount         := count($result)
let $max                := if ($max < 1) then $totalcount else $max
let $returnresults      := subsequence($result,$offset,$max)
let $currentcount       := count($returnresults)

return
<result page-current="{floor($offset div $max) + 1}" 
        page-total="{floor($totalcount div $max) + 1}" 
        current="{$currentcount}" 
        total="{$totalcount}" 
        offset="{$offset}" 
        max="{$max}" 
        sort="{$sort}">
{
    $returnresults
}
</result>
};

declare function adxfiles:getMessage($account as xs:string, $filename as xs:string, $xpath as xs:string?) as element() {
    let $check              := adxfiles:checkArguments($account,'r--')
    let $accountdir         := concat($get:strXisAccounts, '/', $account)
    
    let $xsltParameters     :=
        <parameters>
            <param name="xpath" value="{$xpath}"/>
        </parameters>
    let $xsl                := doc(concat($get:strXisResources, '/stylesheets/add-namespace.xsl'))
    
    let $data               := collection(concat($accountdir,'/',$adxfiles:_filedir))/file[@name = $filename]/data
    let $message            := 
        try { transform:transform($data/*, $xsl, $xsltParameters) }
        catch * {
            if (string-length($xpath) gt 0) then util:eval(concat('$data/',$xpath)) else $data/*
        }
    
    (:let $data               :=
            collection(concat($accountdir,'/',$adxfiles:_filedir))/file[@name=$filename]/data
    
    let $message            :=
        if (string-length($xpath)>0) then 
            util:eval(concat('$data/',$xpath))
        else
            $data/*:)
    
    return
        if ($message[@representation='B64'] | $message[@xsi:type='xs:base64Binary']) then (
            fn:parse-xml(util:base64-decode($message))/*
        ) else 
        if ($message instance of element()) then (
            $message
        ) else (
            error(QName('http://art-decor.org/ns/error','ContentMissing'),concat('Could not find contents in file ',$filename,' in account ',$account,' using xpath expression ',$xpath))
        )
};

declare function adxfiles:getFile($account as xs:string, $filename as xs:string?) as node()* {
    let $check              := adxfiles:checkArguments($account,'r--')
    let $accountdir         := concat($get:strXisAccounts, '/', $account)
    
    let $files              :=
        if (string-length($filename) gt 0) then (
            let $d          := concat($accountdir,'/',$adxfiles:_filedir,'/',$filename)
            return
                if (doc-available($d)) then (doc($d)/file/data/node()) else ()
        )
        else (
            collection(concat($accountdir,'/',$adxfiles:_filedir))/file/data/node()
        )
    
    return
        if ($files) then (
            $files
        ) else (
            error(QName('http://art-decor.org/ns/error','ContentMissing'),concat('Could not find contents in file ',$filename,' in account ',$account))
        )
};

declare function adxfiles:saveFile($account as xs:string, $filename as xs:string, $filecontent as item()) as xs:boolean {
    let $accountdir         := concat($get:strXisAccounts, '/',$account)
    let $filesdir           := concat($accountdir,'/',$adxfiles:_messagedir)
    
    let $check              := adxfiles:checkArguments($account,'rw-')
    
    let $data-safe          :=
        if (empty($filecontent)) then (false()) else (
        (:if (empty($filecontent) or not($login)) then (false()) else ():)
            (:Hack alert: upload fails when content has UTF-8 Byte Order Marker. the UTF-8 representation of the BOM is the byte sequence 0xEF,0xBB,0xBF:)
            let $filecontent    := 
                if ($filecontent instance of xs:base64Binary) 
                then util:base64-decode($filecontent) 
                else if ($filecontent instance of xs:string) 
                then $filecontent
                else fn:serialize($filecontent,
                    <output:serialization-parameters xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization">
                        <output:method>xml</output:method>
                    </output:serialization-parameters>
                )
            let $content-no-bom := 
                if (string-to-codepoints(substring($filecontent,1,1))=65279) 
                then (substring($filecontent,2)) 
                else ($filecontent)
            let $store          :=
                xmldb:store($filesdir, encode-for-uri($filename), $content-no-bom)
            let $chgrp          := sm:chgrp(xs:anyURI($store),$account)
            let $chperm         := sm:chmod(xs:anyURI($store),'rw-rw----')
            
            return true()
        )
    
    let $index-safe         :=
        if ($data-safe) then (
            adxfiles:indexFile($account, $filename)
        ) else ()
    
    return exists($index-safe)
};

declare function adxfiles:cleanupFileName($filename as xs:string?) as xs:string? {
    if (string-length($filename) gt 0) then (
        replace($filename,'[^A-Za-z\d\.\-]','_') (:prevent white space:)
    ) else ()
};

declare function adxfiles:getReports($account as xs:string, $filename as xs:string, $xpath as xs:string?, $validationBase as xs:string?) as element()* {
    let $check              := adxfiles:checkArguments($account,'r--')
    let $accountdir         := concat($get:strXisAccounts, '/', $account)
    
    (:only check xpath when filename is given:)
    return
        if (string-length($filename)>0 and string-length($xpath)>0 and string-length($validationBase)>0) then (
            let $d          := concat($accountdir,'/',$adxfiles:_filedir,'/',$filename)
            return
                if (doc-available($d)) then (doc($d)/file/reports/message[@path=$xpath]/validationReport[@validationBase=$validationBase]) else ()
        )
        else if (string-length($filename)>0 and string-length($xpath)>0) then (
            let $d          := concat($accountdir,'/',$adxfiles:_filedir,'/',$filename)
            return
                if (doc-available($d)) then (doc($d)/file/reports/message[@path=$xpath]) else ()
        )
        else if (string-length($filename)>0 and string-length($validationBase)>0) then (
            let $d          := concat($accountdir,'/',$adxfiles:_filedir,'/',$filename)
            return
                if (doc-available($d)) then (doc($d)/file/reports/message/validationReport[@validationBase=$validationBase]) else ()
        )
        else if (string-length($filename)>0) then (
            let $d          := concat($accountdir,'/',$adxfiles:_filedir,'/',$filename)
            return
                if (doc-available($d)) then (doc($d)/file/reports/message) else ()
        )
        else if (string-length($validationBase)>0) then (
            let $d          := concat($accountdir,'/',$adxfiles:_filedir,'/',$filename)
            return
                if (doc-available($d)) then (doc($d)/file/reports/message/validationReport[@validationBase=$validationBase]) else ()
        )
        else (
            let $d          := concat($accountdir,'/',$adxfiles:_filedir,'/',$filename)
            return
                if (doc-available($d)) then (doc($d)/file/reports/message) else ()
        )
};

declare function adxfiles:saveReport($account as xs:string, $filename as xs:string, $xpath as xs:string, $validationBase as xs:string, $reportcontent as item()?) as xs:boolean {
    let $check                  := adxfiles:checkArguments($account,'rw-')
    let $accountdir             := concat($get:strXisAccounts, '/', $account)
    let $indexfile              := collection(concat($accountdir,'/',$adxfiles:_filedir))/file[@name=$filename]
    let $currentMessage         := $indexfile/meta/file/message[@xpath=$xpath]
    let $reports                := $indexfile/reports
    let $currentMessageReport   := $reports/message[@path=$xpath]
    let $currentVal             := $currentMessageReport/validationReport[@validationBase=$validationBase]
    
    let $updatepref             :=
        if ($currentMessage[@validationBase]) then
            update value $currentMessage/@validationBase with $validationBase
        else (
            update insert attribute validationBase {$validationBase} into $currentMessage
        )
    let $store              := 
        if ($reportcontent/node()) then (
            if ($currentVal) then 
                update replace $currentVal with $reportcontent/node()
            else if ($currentMessageReport) then
                update insert $reportcontent/node() into $currentMessageReport
            else (
                update insert $reportcontent into $reports
            )
        ) else ()
        
    return exists($reports)
};
declare function adxfiles:deleteAccountData($account as xs:string, $deletetype as xs:string, $filename as xs:string?, $xpath as xs:string?, $validationbase as xs:string?) as element() {
    adxfiles:deleteAccountData($param-account, $param-type, $param-file, $param-xpath, $param-base, (), ())
};

declare function adxfiles:deleteAccountData($account as xs:string, $deletetype as xs:string, $filename as xs:string?, $xpath as xs:string?, $validationbase as xs:string?, $rootElement as xs:string*, $beforeDate as xs:date?) as element() {
let $check              := adxfiles:checkArguments($account,'rw-')
(: only allow account that exist :)
let $accountdir         := concat($get:strXisAccounts, '/', $account)
let $attachmentdir      := concat($accountdir, '/', $adxfiles:_attachmentdir)
let $filesdir           := concat($accountdir, '/', $adxfiles:_filedir)
let $testseriesfile     := concat($accountdir, '/testseries.xml')

return
    try {
        let $dodocs     := (string-length($filename) gt 0 or string-length($rootElement) gt 0 or not(empty($beforeDate)))
        let $docs       := if ($dodocs) then collection($filesdir)/file else ()
        let $docs       := if (string-length($filename) gt 0) then $docs[@name = $filename] else $docs
        let $docs       := if (string-length($rootElement) gt 0) then $docs[meta/file[@rootelement = $rootElement]] | $docs[meta/file/message[@rootelement = $rootElement]] else $docs
        let $docs       := if (empty($beforeDate)) then $docs else $docs[xs:date(substring(@created, 1, 10)) lt $beforeDate]
        let $doccount   := count($docs)
        
        (: handle attachments :)
        let $deleteattachments  := 
            if ($deletetype = $adxfiles:_delete-all) then (
                (: delete all attachments (for file) :)
                if (xmldb:collection-available($attachmentdir)) then (
                    if ($dodocs) then (
                        (:attachments are called file_n where is a digit:)
                        for $doc in xmldb:get-child-resources($attachmentdir)
                        let $base   := string-join(tokenize($doc,'_')[not(position()=last())],'_')
                        let $ext    := string-join(tokenize($doc,'_')[last()],'_')
                        return
                            if ($docs[@name = $base] and matches($ext,'^\d+$')) then (
                                xmldb:remove($attachmentdir, $doc)
                            ) else ()
                    ) else (
                        xmldb:remove($attachmentdir)
                    )
                ) else ()
            ) else ()
        
        (: handle testseries :)
        let $deletetestseries   :=
            if ($deletetype = ($adxfiles:_delete-all, $adxfiles:_delete-testseries-only)) then (
                (: delete testseries validation data :)
                if (doc-available($testseriesfile)) then (
                    let $docTestSeries  := doc($testseriesfile)
                    return
                    if ($dodocs) then (
                        for $doc in $docs
                        return (
                            update delete $docTestSeries//xis:validation[xmldb:decode(@messageFile) = $doc/@name]//validationReport,
                            update delete $docTestSeries//xis:validation[xmldb:decode(@messageFile) = $doc/@name]//messageFile,
                            update value $docTestSeries//xis:validation[xmldb:decode(@messageFile) = $doc/@name]//xis:validation/@statusCode with '',
                            update value $docTestSeries//xis:validation[xmldb:decode(@messageFile) = $doc/@name]//xis:validation/@dateTime with ''
                        )
                    ) else (
                        update delete $docTestSeries//validationReport,
                        update delete $docTestSeries//messageFile,
                        update value $docTestSeries//xis:validation/@statusCode with '',
                        update value $docTestSeries//xis:validation/@dateTime with ''
                    )
                ) else ()
            ) else ()
        
        (: handle files :)
        let $deletefiles    :=
            switch ($deletetype)
            case $adxfiles:_delete-all return (
                (: delete all files (includes index and reports) :)
                if ($dodocs) then (
                    for $doc in $docs
                    return xmldb:remove(util:collection-name($doc), encode-for-uri(util:document-name($doc)))
                ) else (
                    xmldb:remove($filesdir)
                )
            )
            case ($adxfiles:_delete-validation-only) return (
                (: delete validation data :)
                if ($dodocs) then (
                    for $doc in $docs
                    return (
                        for $val in adxfiles:getReports($account, $doc/@name, $xpath, $validationbase)
                        return (
                            update delete $val/ancestor::file/meta/file/message[@xpath = $val/ancestor-or-self::message/@path]/@validationBase,
                            update delete $val
                        )
                    )
                ) else (
                    update delete collection($filesdir)/file/meta/file/message/@validationBase,
                    update delete collection($filesdir)/file/reports/node()
                )
            )
            case ($adxfiles:_delete-renderings-only) return (
                (: delete validation data :)
                if ($dodocs) then (
                    for $doc in $docs
                    return (
                        update delete adxfiles:getRendering($account, $doc/@name, $xpath, (), '*')
                        
                    )
                ) else (
                    update delete collection($filesdir)/file/render/node()
                )
            )
            default return ()
        
        let $dummy  := if (sm:is-dba(get:strCurrentUserName())) then adpfix:setXisAccountPermissions($account) else ()
        
        return 
            <result dodocs="{$dodocs}" filename="{$filename}" xpath="{$xpath}" rootelement="{$rootElement}" beforedate="{$beforeDate}" doccount="{$doccount}"
                names="{collection($filesdir)/file/meta/file/message/@rootelement}"
            >OK</result>
    } 
    catch * {
        <error>ERROR {$err:code} deleting results: {$err:description, "', module: ", $err:module, "(", $err:line-number, ",", $err:column-number, ")"}</error>
    }
};

declare function adxfiles:deleteIndex($account as xs:string, $deletetype as xs:string, $filename as xs:string?, $validationbase as xs:string?) as xs:boolean {
    let $check              := adxfiles:checkArguments($account,'rw-')
    
    let $data-safe          :=
        if ($deletetype=$adxfiles:_delete-all) then (
            if ($filename) then
                update delete adxfiles:getIndex($account, $filename)
            else
                update delete adxfiles:getIndex($account, ())
        )
        else if ($deletetype=$adxfiles:_delete-validation-only) then (
            if ($filename) then (
                if ($validationbase) then
                    update delete adxfiles:getIndex($account, $filename)/validation[@path=$validationbase]
                else
                    update delete adxfiles:getIndex($account, $filename)/validation
            ) else ()
        )
        else ()
        
    return true()
};

declare function adxfiles:reindexAccounts() as element(result) {
    <result>
    {
        for $account in doc($get:strTestAccounts)//xis:testAccount/@name
        return
            adxfiles:reindexAccount($account)/*
    }
    </result>
};

declare function adxfiles:reindexAccount($account as xs:string) as element(result) {
    let $check              := adxfiles:checkArguments($account,'rw-')
    
    let $accountdir         := concat($get:strXisAccounts,'/',$account)
    let $filesdir           := concat($accountdir,'/',$adxfiles:_filedir)
    let $accountfiles       := if (xmldb:collection-available($filesdir)) then xmldb:get-child-resources($filesdir) else ()
    
    let $update             :=
        for $filename in $accountfiles
        return adxfiles:reindexFile($account, $filename)
        
    return 
    <result>
        <reindexed>{$account}</reindexed>
    </result>
};

declare function adxfiles:reindexFile($account as xs:string, $filename as xs:string) as element(result) {
    let $check              := adxfiles:checkArguments($account,'rw-')
    let $supportedTypes     := ('application/xml','text/xml')
    let $accountdir         := concat($get:strXisAccounts, '/',$account)
    let $filesdir           := concat($accountdir,'/',$adxfiles:_filedir)
    
    let $fullfilepath       := concat($filesdir,'/',$filename)
    
    return
        if (doc-available($fullfilepath)) then (
            let $resultdoc      := doc($fullfilepath)/file
            let $fileCreation   := if ($resultdoc/@created castable as xs:dateTime) then ($resultdoc/xs:dateTime(@created)) else (current-dateTime())
            let $fileType       := $resultdoc/@mediaType
            
            return 
                if ($fileType = $supportedTypes) then (
                    let $addattribute   := update value $resultdoc/@created with $fileCreation
                    let $addindex       := update value $resultdoc/meta with adxfiles:indexContents($account, $resultdoc/@name, $fileCreation, $fileType, $resultdoc/data/*)
                    
                    return
                        <result status="INFO">File {$filename} successfully reindexed</result>
                )
                else (
                    <result status="ERROR">File {$filename} is not {$supportedTypes}. Found '{$fileType}'.</result>
                )
        ) else (
            <result status="ERROR">File {$filename} does not exist in account {$account}.</result>
        )
};

declare function adxfiles:indexAccounts() as element(result) {
    <result>
    {
        for $account in doc($get:strTestAccounts)//xis:testAccount/@name
        return
            adxfiles:indexAccount($account)/*
    }
    </result>
};

declare function adxfiles:indexAccount($account as xs:string) as element(result) {
    let $check              := adxfiles:checkArguments($account,'rw-')
    
    let $accountdir         := concat($get:strXisAccounts,'/',$account)
    let $messagesdir        := concat($accountdir,'/',$adxfiles:_messagedir)
    let $accountfiles       := if (xmldb:collection-available($messagesdir)) then xmldb:get-child-resources($messagesdir) else ()
    
    let $update             :=
        for $filename in $accountfiles
        return adxfiles:indexFile($account, $filename)
        
    return 
    <result>
        <reindexed>{$account}</reindexed>
    </result>
};

declare function adxfiles:indexFile($account as xs:string, $filename as xs:string) as element(result) {
    let $check              := adxfiles:checkArguments($account,'rw-')
    let $supportedTypes     := ('application/xml','text/xml')
    let $accountdir         := concat($get:strXisAccounts, '/',$account)
    let $filesdir           := concat($accountdir,'/',$adxfiles:_filedir)
    
    let $filesdir         := if (xmldb:collection-available($filesdir)) then ($filesdir) else (xmldb:create-collection($accountdir, $adxfiles:_filedir))
    
    let $messagedir         := concat($accountdir,'/',$adxfiles:_messagedir)
    let $reportdir          := concat($accountdir,'/',$adxfiles:_reportsdir)
    
    let $fullfilepath       := concat($messagedir,'/',$filename)
    let $fullreportpath     := concat($reportdir,'/',$filename)
    
    return
        if (xmldb:get-child-resources($messagedir)=$filename) then (
            let $resultFileName := $filename (:concat(util:uuid(),'.xml'):)
            let $fileCreation   := xmldb:created($messagedir, $filename)
            let $fileType       := xmldb:get-mime-type(xs:anyURI($fullfilepath))
            
            return 
                if ($fileType = $supportedTypes) then (
                    let $fileContents   := doc($fullfilepath)/(*|comment())
                    let $delete         := if (xmldb:get-child-resources($filesdir) = $resultFileName) then xmldb:remove($filesdir, $resultFileName) else ()
                    (:  NOTE!! need to avoid 'update insert' as this runs the risk of dissappearing xmlns declarations when the only usage of 
                        that declaration is inside an @xsi:type, e.g. xsi:type="hl7:II". Without the declaration the XML is invalid. 
                        Reported to the eXist-db list 2015-06-23
                    :)
                    (:let $resultdoc      := doc(xmldb:store($filesdir, $resultFileName, $adxfiles:_filestructure))/file:)
                    let $store          := xmldb:store($filesdir, $resultFileName, 
                        <file name="{$filename}" created="{$fileCreation}" mediaType="{$fileType}" username="{get:strCurrentUserName()}">
                            <meta/>
                            <data>{$fileContents}</data>
                            <reports/>
                        </file>)
                    let $chgrp          := sm:chgrp(xs:anyURI($store),$account)
                    let $chperm         := sm:chmod(xs:anyURI($store),'rw-rw----')
                    let $resultdoc      := doc($store)/file
                    
                    (:let $addattribute   := update value $resultdoc/@name with $filename:)
                    (:let $addattribute   := update value $resultdoc/@created with $fileCreation:)
                    (:let $addattribute   := update value $resultdoc/@mediaType with $fileType:)
                    (:let $addcontents    := update insert $fileContents into $resultdoc/data:)
                    let $addindex       := update insert adxfiles:indexContents($account, $filename, $fileCreation, $fileType, $resultdoc/data/*) into $resultdoc/meta
                    let $addreports     := 
                        if (doc-available($fullreportpath) and doc($fullreportpath)//message) then (
                            update insert doc($fullreportpath)//message into $resultdoc/reports
                        ) else ()
                    
                    let $remove         := xmldb:remove($messagedir, $filename)
                    let $remove         := if (doc-available($fullreportpath)) then (xmldb:remove($reportdir, $filename)) else ()
                    
                    return
                        <result status="INFO">File {$filename} successfully indexed</result>
                )
                else (
                    <result status="ERROR">File {$filename} is not {$supportedTypes}. Found '{$fileType}'.</result>
                )
        ) else (
            <result status="ERROR">File {$filename} does not exist in account {$account}.</result>
        )
};

declare %private function adxfiles:indexContents($account as xs:string, $filename as xs:string, $creationDate as xs:dateTime, $mediaType as xs:string, $data as element()?) as element(file)? {
    <file filename="{$filename}" created="{$creationDate}" mediaType="{$mediaType}">
    {
        try {
            let $rootElement        := if ($data) then $data/local-name() else ()
            let $rootName           := $adxfiles:interactions[@code = $rootElement]/@displayName
            let $rootName           := 
                if ($rootName) then (
                    $rootName
                    
                ) else if ($data[self::soap:Envelope/soap:Body/*]) then (
                    concat('SOAP Envelope (',$data/soap:Body/name(*[1]),')')
                
                ) else if ($data[self::soap:Envelope]) then (
                    'SOAP Envelope'
                    
                ) else if ($data[self::http:response]) then (
                    string-join(('HTTP Response: ',$data/*[1]/@status),'')
                    
                ) else if ($data[self::hl7:ClinicalDocument]) then (
                    if ($data[hl7:title]) then
                        $data/hl7:title
                    else if ($data/hl7:code[@displayName]) then
                        $data/hl7:code/@displayName
                    else if ($data/hl7:code[@code]) then
                        $data/hl7:code/@code
                    else (
                        $data/local-name()
                    )
                ) else (
                    $rootElement
                    
                )
            
            (: optimized for performance :)
            (: do not index ClinicalDocument elements if they are directly under an interaction. These should be checked in the context of that same interaction not separately. :)
            let $messages           := ($data[namespace-uri() = 'urn:hl7-org:v3'] |
                                        $data/descendant-or-self::*[hl7:interactionId] | 
                                        (:$data/descendant-or-self::hl7:ClinicalDocument[not(hl7:text | ancestor::*[hl7:interactionId])] |:)
                                        (: hack alert: we invented this interaction and we really don't care about the interaction part, only about the CDA :)
                                        (:$data/descendant-or-self::hl7:POCD_IN000040NL/descendant-or-self::hl7:ClinicalDocument | :)
                                        $data//*[@representation='B64'] |
                                        $data//*[@xsi:type='xs:base64Binary'] |
                                        $data/descendant::hl7:organizer[../../self::hl7:ControlActProcess] |
                                        $data/descendant-or-self::hl7:ClinicalDocument[not(hl7:text)]
                                       )
            
            return (
                attribute rootelement {$rootElement},
                attribute rootname {$rootName[1]},
                
                for $message in $messages
                let $xpath              := 
                    if ($data[parent::data/parent::file]) then 
                        (:get xpath taking file/data as level for granted. we start counting from the actual contents, excluding the packaging wrappers:)
                        for $node in $message/ancestor-or-self::*[position() <= (last() - 2)]
                        return 
                            concat('*[',if ($node/ancestor::*) then count($node/preceding-sibling::*)+1 else 1,']')
                    else (
                        for $node in $message/ancestor-or-self::*
                        return 
                            concat('*[',if ($node/ancestor::*) then count($node/preceding-sibling::*)+1 else 1,']')
                    )
                let $xpath              := if (empty($xpath)) then () else string-join($xpath,'/') 
                let $message            :=
                    if ($message[@representation='B64'] | $message[@xsi:type='xs:base64Binary']) then (
                        try { fn:parse-xml(util:base64-decode($message))/* } catch * {()}
                    ) else (
                        $message
                    )
                let $rootElement        := if ($message instance of element()) then ($message/local-name()) else ()
                let $rootName           := $adxfiles:interactions[@code = $rootElement]/@displayName
                let $rootName           := 
                    if ($rootName) then (
                        $rootName
                    ) else 
                    if ($message[self::hl7:organizer]) then (
                        string-join(('organizer', 
                            if ($message/hl7:code[@displayName]) then
                                $message/hl7:code/@displayName
                            else 
                            if ($message/hl7:code[@code]) then
                                $message/hl7:code/@code
                            else ()
                        ), ' ')
                    ) else
                    if ($message[self::hl7:ClinicalDocument]) then (
                        string-join(('CDA', 
                            if ($message[hl7:title]) then
                                $message/hl7:title
                            else 
                            if ($message/hl7:code[@displayName]) then
                                $message/hl7:code/@displayName
                            else 
                            if ($message/hl7:code[@code]) then
                                $message/hl7:code/@code
                            else (
                                $message/local-name()
                            )
                        ), ' ')
                    ) else (
                        $rootElement
                        
                    )
                
                (: get sender details :)
                let $senderId           := 
                    if ($message/hl7:sender/hl7:device/hl7:id) then (
                        $message/hl7:sender/hl7:device/hl7:id
                        
                    ) else if ($message[self::hl7:ClinicalDocument]/hl7:custodian) then (
                        $message/hl7:custodian/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:id
                        
                    ) else if ($message/hl7:author/hl7:*/hl7:id) then (
                        $message/hl7:author/hl7:*/hl7:id
                        
                    ) else ()
                let $senderNames        := (
                        $adxfiles:applications[@id = $senderId[@root = '2.16.840.1.113883.2.4.6.6']/@extension]/*:name |
                        $message/hl7:sender/hl7:device/hl7:name |
                        $message[self::hl7:ClinicalDocument]/hl7:custodian/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:name
                    )
                let $senderName         := adxfiles:getNameFromHL7Name($senderNames)
                
                (: get receiver details :)
                let $receiverId         := 
                    if ($message/hl7:receiver/hl7:device/hl7:id) then (
                        $message/hl7:receiver/hl7:device/hl7:id
                        
                    ) else if ($message/hl7:informationRecipient) then (
                        $message/hl7:informationRecipient/hl7:*/hl7:id
                        
                    ) else ()
                let $receiverNames      := (
                        $adxfiles:applications[@id = $receiverId[@root = '2.16.840.1.113883.2.4.6.6']/@extension]/*:name |
                        $message/hl7:receiver/hl7:device/hl7:name |
                        $message[self::hl7:ClinicalDocument]/hl7:informationRecipient/hl7:intendedRecipient//hl7:name
                    )
                let $receiverName       := adxfiles:getNameFromHL7Name($receiverNames)
                    
                (: get patient details :)
                let $patientId          := 
                    if ($message/hl7:ControlActProcess/hl7:queryByParameter) then (
                        $message/hl7:ControlActProcess/hl7:queryByParameter/*[lower-case(local-name())=('patientid','patient.id')]/hl7:value
                        
                    ) else if ($message[hl7:ControlActProcess]) then (
                        $message//hl7:subject/hl7:Patient/hl7:id |
                        $message//hl7:recordTarget/hl7:*/hl7:id |
                        $message//hl7:subject/hl7:patient/hl7:id
                        
                    ) else if ($message/hl7:recordTarget) then (
                        $message/hl7:recordTarget/hl7:*/hl7:id
                        
                    ) else ()
                let $patientNames       := (
                        $adxfiles:patients/hl7:id[@extension = $patientId/@extension][@root = $patientId/@root]/../*/hl7:name |
                        $patientId/../*/hl7:name
                    )
                let $patientName        := adxfiles:getNameFromHL7Name($patientNames)
                
                return
                    if (string-length($rootElement)>0) then (
                        <message filename="{$filename}" created="{$creationDate}" xpath="{$xpath}" rootelement="{$rootElement}" rootname="{$rootName[1]}">
                        {
                            if ($message[hl7:id]) then (
                                <id>{$message/hl7:id/@*}</id>
                            ) else ()
                            ,
                            if ($message[hl7:acknowledgement]) then (
                                <acknowledgement>
                                    {
                                        $message/hl7:acknowledgement/@typeCode,
                                        if ($message/hl7:acknowledgement[hl7:targetTransmission]) then
                                            <targetTransmission>
                                                <id>{$message/hl7:acknowledgement/hl7:targetTransmission/hl7:id[1]/@*}</id>
                                            </targetTransmission>
                                        else (
                                            <targetMessage>
                                                <id>{$message/hl7:acknowledgement/hl7:targetMessage/hl7:id[1]/@*}</id>
                                            </targetMessage>
                                        )
                                    }
                                </acknowledgement>
                            ) else ()
                            ,
                            if ($message[hl7:creationTime]) then (
                                <creationTime value="{adxfiles:getDateFromHL7Date($message/hl7:creationTime/@value)}"/>
                            ) else if ($message[hl7:effectiveDate]) then (
                                <creationTime value="{adxfiles:getDateFromHL7Date($message/hl7:effectiveTime/@value)}"/>
                            ) else ()
                            ,
                            <sender name="{$senderName}">{$senderId[1]/(@root|@extension)}</sender>
                            ,
                            <receiver name="{$receiverName}">{$receiverId[1]/(@root|@extension)}</receiver>
                            ,
                            if ($patientId) then (
                                <patient name="{$patientName}">{$patientId[1]/(@root|@extension)}</patient>
                            ) else ()
                            
                            (:validation elements are retrieved at runtime:)
                        }
                        </message>
                    ) else ()
            )
        }
        catch * {
            <de:error xmlns:de="http://art-decor.org/ns/error" account="{$account}" file="{$filename}">
            {
                concat('ERROR ', $err:code, $err:description, ', module: ', $err:module, '(', $err:line-number, ',', $err:column-number, ')')
            }
            </de:error>
        }
    }
    </file>
};

declare %private function adxfiles:getIndex($account as xs:string, $filename as xs:string?) as element(file)* {
    (:first process any files in /messages so they go to /files with proper indexes. 
    If this fails (permissions or whatever): tough luck, just return what is in /files:)
    let $update             := try { adxfiles:indexAccount($account) } catch * {()}
    (:let $update             := try { adpfix:setXisPermissions() } catch * {()}:)
    
    let $accountdir         := concat($get:strXisAccounts, '/', $account)
    
    return
        if (string-length($filename)>0) then (
            let $d          := concat($accountdir,'/',$adxfiles:_filedir,'/',$filename)
            return
                if (doc-available($d)) then (doc($d)/file/meta/file) else ()
        )
        else
            collection(concat($accountdir,'/',$adxfiles:_filedir))/file/meta/file
};

declare function adxfiles:getNameFromHL7Name ($name as element()*) as xs:string* {
    let $r :=
        for $namePart in $name[1]//text()
        return (
            if ($namePart[normalize-space()='']) then (
            ) else if ($namePart[parent::hl7:prefix]) then (
                $namePart
            ) else (
                concat($namePart,' ')
            )
        )
        
    return normalize-space(string-join($r,''))
};

declare function adxfiles:getDateFromHL7Date($dateString as xs:string?) as xs:string? {
    let $year           := substring($dateString,1,4)
    let $month          := substring($dateString,5,2)
    let $day            := substring($dateString,7,2)
    let $year           := if ($year castable as xs:integer and string-length($year)=4                        ) then $year  else ()
    let $month          := if ($month castable as xs:integer and string-length($month)=2 and number($month)<13) then $month else '01'
    let $day            := if ($day castable as xs:integer and string-length($day)=2     and number($day)<32  ) then $day   else '01'
    
    let $date           := concat($year,'-',$month,'-',$day)
    let $date           := if ($date castable as xs:date) then $date else ()
    
    let $hour           := substring($dateString,9,2)
    let $minute         := substring($dateString,11,2)
    let $second         := substring($dateString,13,2)
    let $hour           := if ($hour castable as xs:integer   and string-length($hour)=2 and number($hour)<24    ) then $hour   else '00'
    let $minute         := if ($minute castable as xs:integer and string-length($minute)=2 and number($minute)<60) then $minute else '00'
    let $second         := if ($second castable as xs:integer and string-length($second)=2 and number($second)<60) then $second else '00'
    
    let $time           := concat($hour,':',$minute,':',$second,substring($dateString,15))
    let $time           := if ($date castable as xs:date and $time castable as xs:time) then $time else ()
    
    return
        string-join(($date,$time),'T')
};

declare %private function adxfiles:checkArguments($account as xs:string, $access as xs:string) {
    let $accountPath        := concat($get:strXisAccounts, '/',$account)
    let $accountMessagePath := concat($accountPath,'/',$adxfiles:_messagedir)
    let $check              :=
        if (xmldb:get-child-collections($get:strXisAccounts)=$account) then () else (
            error(QName('http://art-decor.org/ns/error', 'AccountDoesNotExist'), concat('This account does not exist: ''',$account,''''))
        )
    let $check              :=
        if (sm:has-access(xs:anyURI($accountPath),$access)) then () else (
            error(QName('http://art-decor.org/ns/error', 'UnsufficientPermissions'), concat('You have insufficient permissions for this account: ''',$account,'''. You need ''',$access,''''))
        )
    return ()
};

declare function adxfiles:getRequestBody() as item()* {
    let $resource       := 
        if (request:is-multipart-content()) then (
            let $file   := request:get-uploaded-file-data('file')
            return if (empty($file)) then () else (util:base64-decode($file))
        )
        else ( 
            request:get-data()
        )
    let $resource       :=
        if ($resource instance of element()) then (
            (:exactly what we want:)
            $resource
        )
        else if ($resource instance of document-node()) then (
            $resource/*
        )
        else (
            (:Hack alert: upload fails when content has UTF-8 Byte Order Marker. 
            the UTF-8 representation of the BOM is the byte sequence 0xEF,0xBB,0xBF:)
            if (empty($resource))
            then ()
            else if (string-to-codepoints(substring($resource,1,1))=65279) 
            then (fn:parse-xml(substring($resource,2))/*)
            else (fn:parse-xml($resource)/*)
        )
    
    return $resource
};

declare function adxfiles:getRendering($account as xs:string, $filename as xs:string, $xpath as xs:string?, $resources as xs:string?, $viewer as xs:string?) as element(message)* {
    let $renderings := 
        if (string-length($xpath)=0) then 
            adxfiles:getFile($account, $filename)[1]/ancestor::file/render/message
        else (
            adxfiles:getFile($account, $filename)[1]/ancestor::file/render/message[@path=$xpath]
        )
    let $renderings :=
        if (string-length($viewer)=0) then 
            $renderings[empty(@viewer)]
        else
        if ($viewer = '*') then 
            $renderings
        else (
            $renderings[@renderBase=$resources][@viewer=$viewer]
        )
    
    return $renderings
};

declare function adxfiles:saveRendering($account as xs:string, $filename as xs:string, $xpath as xs:string, $resources as xs:string?, $viewer as xs:string?, $content as node()*) as xs:boolean {
    let $check              := adxfiles:checkArguments($account,'rw-')
    
    let $file               := adxfiles:getFile($account, $filename)[1]/ancestor::file
    
    let $update             := adxfiles:deleteRendering($account, $filename, $xpath, $resources, $viewer)
    let $update             := if ($file[render]) then () else (update insert <render/> into $file)
    let $rendering          :=
        <message path="{$xpath}">
        {
            if (string-length($viewer)>0) then ( attribute renderBase {$resources}, attribute viewer {$viewer} ) else (), 
            $content
        }
        </message>
    let $update             := update insert $rendering into $file/render
    
    return true()
};

declare function adxfiles:deleteRendering($account as xs:string, $filename as xs:string, $xpath as xs:string?, $resources as xs:string?, $viewer as xs:string?) as xs:boolean {
    let $check              := adxfiles:checkArguments($account,'rw-')
    
    let $file               := adxfiles:getFile($account, $filename)[1]/ancestor::file
    
    let $renderings         := adxfiles:getRendering($account, $filename, $xpath, $resources, $viewer)
    let $return             := exists($renderings)
    
    let $update             := update delete $renderings
    
    return $return
};

declare function adxfiles:saveTempMessage($account as xs:string, $message as element()) as xs:string {
    let $check              := adxfiles:checkArguments($account,'rw-')
    
    let $accountdir         := concat($get:strXisAccounts, '/', $account)
    let $tempdir            := xmldb:create-collection($accountdir, 'temp')
    
    return
        xmldb:store($tempdir, concat(util:uuid(), '.xml'), $message)
};
declare function adxfiles:deleteFile($account as xs:string, $filepath as xs:string) {
    let $check              := adxfiles:checkArguments($account,'rw-')
    
    let $collName           := string-join(tokenize($filepath, '/')[position() != last()], '/')
    let $resName            := tokenize($filepath, '/')[last()]
    
    let $delete             := xmldb:remove($collName, $resName)
    
    return ()
};