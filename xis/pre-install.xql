xquery version "3.0";
(:
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
declare namespace sm                = "http://exist-db.org/xquery/securitymanager";
declare namespace xmldb             = "http://exist-db.org/xquery/xmldb";
declare namespace repo              = "http://exist-db.org/xquery/repo";
declare namespace cfg               = "http://exist-db.org/collection-config/1.0";

(: The following external variables are set by the repo:deploy function :)

(: file path pointing to the exist installation directory :)
declare variable $home external;
(: path to the directory containing the unpacked .xar package :)
declare variable $dir external;
(: the target collection into which the app is deployed :)
declare variable $target external;
declare variable $root := repo:get-root();

(: helper function for creating top level database collection and index definitions required for Xis webapplication :)
declare %private function local:createTopCollections() {
    let $xisConf :=
        <collection xmlns="http://exist-db.org/collection-config/1.0" xmlns:hl7="urn:hl7-org:v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <index xmlns:xs="http://www.w3.org/2001/XMLSchema">
                <fulltext default="none" attributes="false"/>
                <!-- eXist-db 2.2 -->
                <!--<range>
                    <create qname="@code" type="xs:string"/>
                    <create qname="@codeSystem" type="xs:string"/>
                </range>-->
                <!-- eXist-db 2.1 -->
                <create qname="@code" type="xs:string"/>
                <create qname="@codeSystem" type="xs:string"/>
            </index>
        </collection>
    let $xisDataConf :=
        <collection xmlns="http://exist-db.org/collection-config/1.0" xmlns:hl7="urn:hl7-org:v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <index xmlns:xs="http://www.w3.org/2001/XMLSchema">
                <fulltext default="none" attributes="false"/>
                <lucene>
                    <analyzer class="org.apache.lucene.analysis.standard.StandardAnalyzer">
                        <param name="stopwords" type="org.apache.lucene.analysis.util.CharArraySet"/>
                    </analyzer>
                    <text qname="naam"/>
                    <text qname="hl7:name"/>
                </lucene>
                <!-- eXist-db 2.2 -->
                <!--<range>
                    <create qname="@atcode" type="xs:string"/>
                    <create qname="@atkode" type="xs:string"/>
                    <create qname="@code" type="xs:string"/>
                    <create qname="@codeSystem" type="xs:string"/>
                    <create qname="@displayName" type="xs:string"/>
                    <create qname="@extension" type="xs:string"/>
                    <create qname="@filename" type="xs:string"/>
                    <create qname="@gpkode" type="xs:string"/>
                    <create qname="@hpkode" type="xs:string"/>
                    <create qname="@id" type="xs:string"/>
                    <create qname="@mediaType" type="xs:string"/>
                    <create qname="@name" type="xs:string"/>
                    <create qname="@prkode" type="xs:string"/>
                    <create qname="@representation" type="xs:string"/>
                    <create qname="@schematron" type="xs:string"/>
                    <create qname="@root" type="xs:string"/>
                    <create qname="@rootelement" type="xs:string"/>
                    <create qname="@value" type="xs:string"/>
                    <create qname="@xsi:type" type="xs:string"/>
                </range>-->
                <!-- eXist-db 2.1 -->
                <create qname="@atcode" type="xs:string"/>
                <create qname="@atkode" type="xs:string"/>
                <create qname="@code" type="xs:string"/>
                <create qname="@codeSystem" type="xs:string"/>
                <create qname="@displayName" type="xs:string"/>
                <create qname="@extension" type="xs:string"/>
                <create qname="@filename" type="xs:string"/>
                <create qname="@gpkode" type="xs:string"/>
                <create qname="@hpkode" type="xs:string"/>
                <create qname="@id" type="xs:string"/>
                <create qname="@mediaType" type="xs:string"/>
                <create qname="@name" type="xs:string"/>
                <create qname="@prkode" type="xs:string"/>
                <create qname="@representation" type="xs:string"/>
                <create qname="@schematron" type="xs:string"/>
                <create qname="@root" type="xs:string"/>
                <create qname="@rootelement" type="xs:string"/>
                <create qname="@value" type="xs:string"/>
                <create qname="@xsi:type" type="xs:string"/>
            </index>
        </collection>
    
    return (
        for $coll in ('xis-data/accounts')
        return (
            sm:chmod(xs:anyURI(xmldb:create-collection($root, $coll)), 'rwxrwxr-x')
        )
        ,
        for $coll in ('xis','xis-data')
        return (
            if (not(xmldb:collection-available(concat('/db/system/config',$root,$coll)))) then (
                xmldb:create-collection(concat('/db/system/config',$root),$coll)
            ) else ()
        )
        ,
        let $index-file := concat('/db/system/config',$root,'xis/collection.xconf')
        return
        if (doc-available($index-file) and deep-equal(doc($index-file)/cfg:collection,$xisConf)) then () else (
            xmldb:store(concat('/db/system/config',$root,'xis'),'collection.xconf',$xisConf),
            xmldb:reindex(concat($root,'xis'))
        )
        ,
        let $index-file := concat('/db/system/config',$root,'xis-data/collection.xconf')
        return
        if (doc-available($index-file)) then () else (
            xmldb:store(concat('/db/system/config',$root,'xis-data'),'collection.xconf',$xisDataConf),
            xmldb:reindex(concat($root,'xis-data'))
        )
    )
};

(: store the collection configuration :)
let $update := local:createTopCollections()

return ()