<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" version="2.0" xml:space="default" xpath-default-namespace="">
    <xsl:output indent="no" method="xml"/>
    <xsl:key name="code" match="Class" use="@code"/>
    <xsl:param name="refsetId" select="11721000146100"/>
    <xsl:variable name="defaultLanguage" select="'en-US'"/>
    <xsl:variable name="version" select="'2022-09-30'"/>
    <xsl:template match="concepts">
        <valueSet id="{$refsetId}" oid="2.16.840.1.113883.6.96">
            <xsl:attribute name="url">
                <xsl:value-of select="'http://snomed.info/sct'"/>
            </xsl:attribute>
            <xsl:attribute name="version">
                <xsl:value-of select="$version"/>
            </xsl:attribute>
            <xsl:attribute name="statusCode">
                <xsl:value-of select="'active'"/>
            </xsl:attribute>
            <xsl:attribute name="experimental">
                <xsl:value-of select="'false'"/>
            </xsl:attribute>
            <xsl:attribute name="effectiveDate">
                <xsl:value-of select="$version"/>
            </xsl:attribute>
            <xsl:attribute name="defaultLanguage">
                <xsl:value-of select="$defaultLanguage"/>
            </xsl:attribute>
            <xsl:attribute name="count">
                <xsl:value-of select="count(concept[refsets/refset/@refsetId=$refsetId])"/>
            </xsl:attribute>
            <language complete="true">en-US</language>
            <language complete="true">en-GB</language>
            <language complete="false">nl-NL</language>
            <logo link="http://ihtsdo.org">ihtsdo-logo40.png</logo>
            <license>
                <text>
                    <p>In order to use the SNOMED CT Explorer, Browser, Refsets or ‘Referentielijsten’ please accept the following license agreement:</p>
                    <p>
                        <b>The Snomed Explorer, Browser, Referencesets and ‘Referentielijsten’</b>, hereafter called <b>SNOMED CT Products</b>, includes SNOMED Clinical Terms® (SNOMED CT®) which is used by permission of the SNOMED International.
                            All rights reserved. SNOMED CT® was originally created by the College of American Pathologists.“SNOMED”, “SNOMED CT” and “SNOMED Clinical Terms” are registered trademarks of the SNOMED International (<a
                            href="http://www.snomed.org">www.snomed.org</a>)</p>
                    <p>Use of SNOMED CT in <b>SNOMED CT Products</b> is governed by the conditions of the following SNOMED CT license issued by SNOMED International:</p>
                    <ol>
                        <li>
                            <p>The meaning of the terms “Affiliate”, or “Data Analysis System”, “Data Creation System”, “Derivative”, “End User”, “Extension”, “Member”, “Non-Member Territory”, “SNOMED CT” and “SNOMED CT Content” are as defined in the SNOMED International Affiliate License Agreement (<a href="http://www.snomed.org/resource/resource/117"
                                >see on the SNOMED International web site</a>).</p>
                        </li>
                        <li>
                            <p>Information about Affiliate Licensing is available at <a href="http://www.snomed.org/snomed-ct/get-snomed-ct">http://www.snomed.org/snomed-ct/get-snomed-ct</a>. 
                                    Individuals or organizations wishing to register as SNOMED International Affiliates can register at <a href="https://mlds.ihtsdotools.org/"
                                    >mlds.ihtsdotools.org</a>, subject to acceptance of the Affiliate License Agreement (<a href="http://www.snomed.org/resource/resource/117">see on the SNOMED International web site</a>).</p>
                        </li>
                        <li>
                            <p>The current list of SNOMED International Member Territories can be viewed at <a href="http://www.snomed.org/members">www.snomed.org/members</a>. Countries not included in that list are "Non-Member Territories".</p>
                        </li>
                        <li>
                            <p>End Users, that do not hold an SNOMED International Affiliate License, may access SNOMED CT® using <b>SNOMED CT Products</b> subject to acceptance of and adherence to the following sub-license limitations:
                                    <ol type="a">
                                    <li>The sub-licensee is only permitted to access SNOMED CT® using this software (or service) for the purpose of exploring and evaluating the terminology.</li>
                                    <li>The sub-licensee is not permitted the use of this software as part of a system that constitutes a SNOMED CT "Data Creation System" or "Data Analysis System", as defined in the SNOMED International Affiliate License. This means that the sub-licensee must not use <b>CT Products</b> to add or copy SNOMED CT identifiers into any type of record system, database or document.</li>
                                    <li>The sub-licensee is not permitted to translate or modify SNOMED CT Content or Derivatives.</li>
                                    <li>The sub-licensee is not permitted to distribute or share SNOMED CT Content or Derivatives.</li>
                                </ol>
                            </p>
                        </li>
                        <li>
                            <p>                        SNOMED International Affiliates may use <b>SNOMED CT Products</b> as part of a "Data Creation System" or "Data Analysis System" subject to the following conditions:
                                    <ol type="a">
                                    <li>The SNOMED International Affiliate, using <b>SNOMED CT Products</b> must accept full responsibility for any reporting and fees due for use or deployment of such a system in a Non-Member Territory.</li>
                                    <li>The SNOMED International Affiliate must not use <b>SNOMED International SNOMED CT Products</b> to access or interact with SNOMED CT in any way that is not permitted by the Affiliate License Agreement.</li>
                                    <li>In the event of termination of the Affiliate License Agreement, the use of <b>SNOMED CT Products</b> will be subject to the End User limitations noted in 4.</li>
                                </ol>
                            </p>
                        </li>
                    </ol>
                </text>
            </license>
            <identifier>
                <xsl:attribute name="system">
                    <xsl:value-of select="Identifier/@authority"/>
                </xsl:attribute>
                <xsl:attribute name="id">
                    <xsl:value-of select="'2.16.840.1.113883.6.96'"/>
                </xsl:attribute>
            </identifier>
            <name languageCode="{$defaultLanguage}">
                <xsl:value-of select="concept/refsets/refset[@refsetId=$refsetId][1]/@refset"/>
            </name>
            <publisher>
                <xsl:value-of select="'IHTSDO'"/>
            </publisher>
            <contact>
                <telecom>
                    <xsl:attribute name="system">
                        <xsl:value-of select="'url'"/>
                    </xsl:attribute>
                    <xsl:attribute name="value">
                        <xsl:value-of select="'http://ihtsdo.org'"/>
                    </xsl:attribute>
                </telecom>
            </contact>
            <description languageCode="{$defaultLanguage}"/>
            <xsl:apply-templates select="concept[refsets/refset/@refsetId=$refsetId]"/>
        </valueSet>
    </xsl:template>
    <xsl:template match="concept">
        <concept code="{@conceptId}" effectiveDate="{concat(substring(@effectiveTime,1,4),'-',substring(@effectiveTime,5,2),'-',substring(@effectiveTime,7,2))}" moduleId="{@moduleId}">
            <xsl:variable name="statusCode">
                <xsl:choose>
                    <xsl:when test="@active = '1'">
                        <xsl:value-of select="'active'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'retired'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:attribute name="statusCode">
                <xsl:value-of select="$statusCode"/>
            </xsl:attribute>
            <xsl:attribute name="sufficientlyDefined">
                <xsl:choose>
                    <xsl:when test="@definitionStatusId = '900000000000073002'">
                        <xsl:value-of select="'true'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'false'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="level">
                <xsl:choose>
                    <xsl:when test="ancestors/id">
                        <xsl:value-of select="max(ancestors/id/xs:integer(@distance))"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'0'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:for-each select="desc[@type = ('fsn', 'pref', 'syn')][@active='1']">
                <xsl:variable name="language">
                    <xsl:choose>
                        <xsl:when test="@languageCode = 'en' and @languageRefsetId = '900000000000509007'">
                            <xsl:value-of select="'en-US'"/>
                        </xsl:when>
                        <xsl:when test="@languageCode = 'en' and @languageRefsetId = '900000000000508004'">
                            <xsl:value-of select="'en-GB'"/>
                        </xsl:when>
                        <xsl:when test="@languageCode = 'nl'">
                            <xsl:value-of select="'nl-NL'"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="@languageCode"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="descStatusCode">
                    <xsl:choose>
                        <xsl:when test="@active = '1'">
                            <xsl:value-of select="'active'"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="'retired'"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="use">
                    <xsl:choose>
                        <xsl:when test="@languageRefsetId = '15551000146102'">
                            <xsl:value-of select="'prefPat'"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="@type"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <designation languageCode="{$language}" use="{@type}" statusCode="{$descStatusCode}" effectiveDate="{concat(substring(@effectiveTime,1,4),'-',substring(@effectiveTime,5,2),'-',substring(@effectiveTime,7,2))}" expirationDate="" lastEditDate="" count="{@count}" length="{@length}">
                    <xsl:value-of select="."/>
                </designation>
            </xsl:for-each>
            <xsl:for-each select="definition[@active='1'] | desc[@type = 'def'][@active='1']">
                <xsl:variable name="language">
                    <xsl:choose>
                        <xsl:when test="@languageCode = 'en' and @languageRefsetId = '900000000000509007'">
                            <xsl:value-of select="'en-US'"/>
                        </xsl:when>
                        <xsl:when test="@languageCode = 'en' and @languageRefsetId = '900000000000508004'">
                            <xsl:value-of select="'en-GB'"/>
                        </xsl:when>
                        <xsl:when test="@languageCode = 'nl'">
                            <xsl:value-of select="'nl-NL'"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="@languageCode"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="defStatusCode">
                    <xsl:choose>
                        <xsl:when test="@active = '1'">
                            <xsl:value-of select="'active'"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="'retired'"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <definition languageCode="{$language}" use="{@type}" statusCode="{$defStatusCode}" effectiveDate="{concat(substring(@effectiveTime,1,4),'-',substring(@effectiveTime,5,2),'-',substring(@effectiveTime,7,2))}">
                    <xsl:value-of select="."/>
                </definition>
            </xsl:for-each>
            <xsl:for-each select="dest[@active = '1']">
                <child cCode="{@sourceId}" subCount="{@subCount}">
                    <label languageCode="en-US">
                        <xsl:value-of select="."/>
                    </label>
                </child>
            </xsl:for-each>
            <xsl:for-each select="grp[@grp = '0']/src[@typeId = '116680003'][@active = '1']">
                <parent pCode="{@destinationId}">
                    <label languageCode="en-US">
                        <xsl:value-of select="."/>
                    </label>
                </parent>
            </xsl:for-each>
            <xsl:if test="ancestors/id">
                <ancestors>
                    <xsl:for-each select="ancestors/id">
                        <ancestor aCode="{.}" distance="{@distance}"/>
                    </xsl:for-each>
                </ancestors>
            </xsl:if>
            <xsl:for-each select="grp[@grp != '0' or (@grp = '0' and src/@typeId != '116680003' and src/@active = '1')]">
                <relGrp grpId="{@grp}">
                    <xsl:for-each select="src[@typeId != '116680003'][@active = '1']">
                        <relation destCode="{@destinationId}" typeCode="{@typeId}" type="{@type}" effectiveDate="{concat(substring(@effectiveTime,1,4),'-',substring(@effectiveTime,5,2),'-',substring(@effectiveTime,7,2))}">
                            <label languageCode="en-US">
                                <xsl:value-of select="."/>
                            </label>
                        </relation>
                    </xsl:for-each>
                </relGrp>
            </xsl:for-each>
            <xsl:if test="refsets">
                <refsets>
                    <xsl:for-each select="refsets/refset">
                        <refset refsetId="{@refsetId}">
                            <label languageCode="en-US">
                                <xsl:value-of select="@refset"/>
                            </label>
                        </refset>
                    </xsl:for-each>
                </refsets>
            </xsl:if>
            <xsl:if test="maps">
                <maps>
                    <xsl:for-each select="maps/map[@active = '1']">
                        <map refsetId="{@refsetId}" mapGroup="{@mapGroup}" mapPriority="{@mapPriority}" mapRule="{@mapRule}" mapAdvice="{@mapAdvice}" mapTarget="{@mapTarget}" correlationId="{@correlationId}" refset="{@refset}" correlation="{@correlation}"/>
                    </xsl:for-each>
                </maps>
            </xsl:if>
        </concept>
    </xsl:template>
</xsl:stylesheet>
