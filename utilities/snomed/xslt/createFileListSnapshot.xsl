<?xml version="1.0" encoding="UTF-8"?>
<!--
	Copyright (C) 2011 Nictiz
	
	Author: Gerrit Boers
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
	
	java -jar saxon9.jar -t -s:baseFileListSnapshot.xml -xsl:xslt/test.xsl -o:fileList.xml
	
	
	
	
	
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:file="http://expath.org/ns/file" version="2.0">
   <xsl:output method="xml" exclude-result-prefixes="#all" encoding="UTF-8" indent="yes"/>
   <!-- characterset of input files, incorrect setting will result in error: Failed to read input file .... (java.nio.charset.MalformedInputException) - Input length = 1 -->
   <xsl:variable name="charSet" select="'UTF-8'"/>
   <xsl:variable name="fileList">
      <xsl:choose>
         <xsl:when test="unparsed-text-available('../files.txt')">
            <xsl:variable name="fileText" select="unparsed-text('../files.txt', $charSet)"/>
            <files>
               <xsl:for-each select="tokenize($fileText, '\n')">
                  <xsl:choose>
                     <xsl:when test="starts-with(., 'SnomedCT')">
                        <file>
                           <xsl:attribute name="baseName">
                              <xsl:value-of select="concat(string-join(tokenize(., '_')[position() lt last()], '_'), '_')"/>
                           </xsl:attribute>
                           <xsl:attribute name="timestamp">
                              <xsl:value-of select="tokenize(., '_')[last()]"/>
                           </xsl:attribute>
                           <xsl:attribute name="date">
                              <xsl:value-of select="substring-before(tokenize(., '_')[last()], 'T')"/>
                           </xsl:attribute>
                           <xsl:value-of select="."/>
                        </file>
                     </xsl:when>
                     <xsl:when test="starts-with(., 'DT') or starts-with(., 'VT')">
                        <file>
                           <xsl:attribute name="baseName">
                              <xsl:value-of select="''"/>
                           </xsl:attribute>
                           <xsl:attribute name="timestamp">
                              <xsl:value-of select="''"/>
                           </xsl:attribute>
                           <xsl:attribute name="date">
                              <xsl:value-of select="substring-before(tokenize(., '-')[last()], '.')"/>
                           </xsl:attribute>
                           <xsl:value-of select="."/>
                        </file>
                     </xsl:when>
                     <xsl:when test="starts-with(., 'RF2-DHD')">
                        <file>
                           <xsl:attribute name="baseName">
                              <xsl:value-of select="'RF2-DHD-refsets'"/>
                           </xsl:attribute>
                           <xsl:attribute name="timestamp">
                              <xsl:value-of select="''"/>
                           </xsl:attribute>
                           <xsl:attribute name="date">
                              <xsl:value-of select="''"/>
                           </xsl:attribute>
                           <xsl:value-of select="."/>
                        </file>
                     </xsl:when>
                  </xsl:choose>
               </xsl:for-each>
            </files>
         </xsl:when>
         <xsl:otherwise>
            <missing>File list not available</missing>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:variable>
   <xsl:template match="/list">
      <xsl:variable name="snomedBase" select="$fileList//file[starts-with(., 'SnomedCT_ManagedServiceNL') or starts-with(., 'SnomedCT_InternationalRF2')]/@baseName"/>
      <xsl:variable name="snomedTimeStamp" select="$fileList//file[starts-with(., 'SnomedCT_ManagedServiceNL') or starts-with(., 'SnomedCT_InternationalRF2')]/@timestamp"/>
      <xsl:variable name="snomedDate" select="$fileList//file[starts-with(., 'SnomedCT_ManagedServiceNL') or starts-with(., 'SnomedCT_InternationalRF2')]/@date"/>
      <xsl:variable name="releaseType">
         <xsl:choose>
            <xsl:when test="contains($snomedBase, 'NL')">
               <xsl:value-of select="'NL'"/>
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="'INT'"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="fileSuffix">
         <xsl:choose>
            <xsl:when test="contains($snomedBase, 'NL')">
               <xsl:value-of select="'NL1000146_'"/>
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="'INT_'"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>

      <list base="{$snomedBase}" releaseType="{$releaseType}" date="{$snomedDate}">
         <!-- Always get International core release files -->
         <xsl:comment>International release core files</xsl:comment>
         <xsl:for-each select="snomedCore/file[@type = 'INT']">
            <file name="{concat(./@name,$fileSuffix,$snomedDate)}" path="{concat(./@base,$snomedTimeStamp,./@path)}" rows="{./@rows}" file="{./@file}"/>
         </xsl:for-each>
         <!-- Get NL files for NL core edition -->
         <xsl:comment>NL release core files</xsl:comment>
         <xsl:if test="$releaseType = 'NL'">
            <xsl:for-each select="snomedCore/file[@type = 'NL']">
               <file name="{concat(./@name,$fileSuffix,$snomedDate)}" path="{concat(./@base,$snomedTimeStamp,./@path)}" rows="{./@rows}" file="{./@file}"/>
            </xsl:for-each>
         </xsl:if>
         
         <!-- Refsets -->
         <xsl:comment>International edition external refset files</xsl:comment>
         <xsl:for-each select="external/file[@type = 'INT']">
            <xsl:variable name="fileBase" select="./@base"/>
            <xsl:variable name="refsetFile" select="$fileList//file[@baseName=$fileBase]"/>
            <xsl:copy-of select="."/>
         </xsl:for-each>
         <xsl:if test="$releaseType = 'NL'">
            <xsl:comment>NL edition external refset files</xsl:comment>
            <xsl:for-each select="external/file[@type = 'NL']">
               <xsl:variable name="fileBase" select="./@base"/>
               <xsl:variable name="refsetFile" select="$fileList//file[@baseName=$fileBase]"/>
               <xsl:copy-of select="."/>
            </xsl:for-each>
            <xsl:comment>DHD core files</xsl:comment>
            <xsl:for-each select="dhd/file">
               <xsl:copy-of select="."/>
            </xsl:for-each>
         </xsl:if>        
      </list>
   </xsl:template>
</xsl:stylesheet>
