<?xml version="1.0" encoding="UTF-8"?>
<!--
	Copyright (C) 2024 ADOT
		
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
	
	Stylesheet for generating dhd-icd10 mapping xml file
	input xml: fileListSnapshot.xml (not actually used during processing)
	input    :    DT-ICD10 mapping tab delimited text file
	output xml:   xml mapping file
	
	DHD file date is accepted as a parameter 'date' format yyyymmdd
	file must be in the following format
	DT-ICD10-map-yyyymmdd.txt
	Colum layout:
	SCTID	FSN	ICD10-code	ICD-10-beschrijving	Volgnummer
	
	example call:
	java -jar saxon9.jar -t -s:fileListSnapshot.xml -xsl:xslt/dhd-icd10-text-to-mapping.xsl -o:meta/DHD-ICD10-mapping.xml
	
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
   <xsl:output method="xml" exclude-result-prefixes="#all" encoding="UTF-8" indent="yes"/>
   <!-- characterset of input files, incorrect setting will result in error: Failed to read input file .... (java.nio.charset.MalformedInputException) - Input length = 1 -->
   <!-- variable for generated fileListSnapshot -->
   <xsl:variable name="fileListSnapshot" select="doc('../fileListSnapshot.xml')/list"/>
   
   <xsl:variable name="date" select="$fileListSnapshot/@date"/>
   <xsl:variable name="charSet" select="'UTF-8'"/>
   <xsl:template match="/">
      <dhd-icd10-mapping>
         <xsl:choose>
            <xsl:when test="unparsed-text-available(concat('../input/DT-ICD10-map-', $date, '.txt'))">
               <xsl:variable name="fileText" select="unparsed-text(concat('../input/DT-ICD10-map-', $date, '.txt'), $charSet)"/>
               <xsl:for-each select="tokenize($fileText, '\r\n')">
                  <xsl:if test="position() > 1 and string-length(tokenize(., '\t')[1]) > 1">
                     <xsl:variable name="columns" select="tokenize(., '\t')"/>
                     <xsl:element name="map">
                        <xsl:attribute name="conceptId">
                           <xsl:value-of select="$columns[1]"/>
                        </xsl:attribute>
                        <xsl:attribute name="effectiveTime">
                           <xsl:value-of select="'20230331'"/>
                        </xsl:attribute>
                        <xsl:attribute name="active">
                           <xsl:value-of select="'1'"/>
                        </xsl:attribute>
                        <xsl:attribute name="refsetId">
                           <xsl:value-of select="'dhd-icd10-map'"/>
                        </xsl:attribute>
                        <xsl:attribute name="refset">
                           <xsl:value-of select="'door RIVM geautoriseerde complexe nationale ‘mapping’ naar ICD-10 voor diagnosethesaurus'"/>
                        </xsl:attribute>
                        <xsl:attribute name="mapTarget">
                           <xsl:value-of select="$columns[3]"/>
                        </xsl:attribute>
                        <xsl:attribute name="mapGroup">
                           <xsl:value-of select="'1'"/>
                        </xsl:attribute>
                        <xsl:attribute name="correlation">
                           <xsl:value-of select="''"/>
                        </xsl:attribute>
                        <xsl:attribute name="mapPriority">
                           <xsl:value-of select="'1'"/>
                        </xsl:attribute>
                        <xsl:attribute name="icd10Term">
                           <xsl:value-of select="$columns[4]"/>
                        </xsl:attribute>
                     </xsl:element>
                  </xsl:if>
               </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
               <missing>DT-ICD10 mapping file</missing>
            </xsl:otherwise>
         </xsl:choose>
      </dhd-icd10-mapping>
   </xsl:template>
</xsl:stylesheet>
