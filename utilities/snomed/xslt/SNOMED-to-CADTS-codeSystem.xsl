<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" version="2.0" xml:space="default" xpath-default-namespace="">
    <xsl:output indent="no" method="xml"/>
    <xsl:key name="conceptKey" match="concept" use="@conceptId"/>
    <!-- variable for generated fileListSnapshot -->
    <xsl:variable name="fileListSnapshot" select="doc('../fileListSnapshot.xml')/list"/>
    
    <xsl:variable name="defaultLanguage" select="'en-US'"/>
    <xsl:variable name="version" select="concat(substring($fileListSnapshot/@date,1,4),'-',substring($fileListSnapshot/@date,5,2),'-',substring($fileListSnapshot/@date,7,2),'T00:00:00')"/>
    <xsl:template match="concepts">
        <browsableCodeSystem oid="2.16.840.1.113883.6.96">
            <xsl:attribute name="id">
                <xsl:value-of select="'snomedct'"/>
            </xsl:attribute>
            <xsl:attribute name="url">
                <xsl:value-of select="'http://snomed.info/sct'"/>
            </xsl:attribute>
            <xsl:attribute name="version">
                <xsl:value-of select="$version"/>
            </xsl:attribute>
            <xsl:attribute name="statusCode">
                <xsl:value-of select="'active'"/>
            </xsl:attribute>
            <xsl:attribute name="experimental">
                <xsl:value-of select="'false'"/>
            </xsl:attribute>
            <xsl:attribute name="effectiveDate">
                <xsl:value-of select="$version"/>
            </xsl:attribute>
            <xsl:attribute name="defaultLanguage">
                <xsl:value-of select="$defaultLanguage"/>
            </xsl:attribute>
            <xsl:attribute name="content">
                <xsl:value-of select="'complete'"/>
            </xsl:attribute>
<!--            <xsl:attribute name="type">
                <xsl:value-of select="'complex'"/>
            </xsl:attribute>-->
            <xsl:attribute name="structure">
                <xsl:value-of select="'network'"/>
            </xsl:attribute>
            <xsl:attribute name="count">
                <xsl:value-of select="count(concept)"/>
            </xsl:attribute>
            <language complete="true">en-US</language>
            <language complete="true">en-GB</language>
            <language complete="false">nl-NL</language>
            <identifier>
                <xsl:attribute name="system">
                    <xsl:value-of select="Identifier/@authority"/>
                </xsl:attribute>
                <xsl:attribute name="id">
                    <xsl:value-of select="'2.16.840.1.113883.6.96'"/>
                </xsl:attribute>
            </identifier>
            <name language="{$defaultLanguage}">
                <xsl:value-of select="'SNOMED Clinical Terms'"/>
            </name>
            <license>
                <text>
                    <p>In order to use the SNOMED CT Explorer, Browser, Refsets or ‘Referentielijsten’ please accept the following license agreement:</p>
                    <p>
                        <b>The Snomed Explorer, Browser, Referencesets and ‘Referentielijsten’</b>, hereafter called <b>SNOMED CT Products</b>, includes SNOMED Clinical Terms® (SNOMED CT®) which is used by permission of the SNOMED International.
                            All rights reserved. SNOMED CT® was originally created by the College of American Pathologists.“SNOMED”, “SNOMED CT” and “SNOMED Clinical Terms” are registered trademarks of the SNOMED International (<a
                            href="http://www.snomed.org">www.snomed.org</a>)</p>
                    <p>Use of SNOMED CT in <b>SNOMED CT Products</b> is governed by the conditions of the following SNOMED CT license issued by SNOMED International:</p>
                    <ol>
                        <li>
                            <p>The meaning of the terms “Affiliate”, or “Data Analysis System”, “Data Creation System”, “Derivative”, “End User”, “Extension”, “Member”, “Non-Member Territory”, “SNOMED CT” and “SNOMED CT Content” are as defined in the SNOMED International Affiliate License Agreement (<a href="http://www.snomed.org/resource/resource/117"
                                >see on the SNOMED International web site</a>).</p>
                        </li>
                        <li>
                            <p>Information about Affiliate Licensing is available at <a href="http://www.snomed.org/snomed-ct/get-snomed-ct">http://www.snomed.org/snomed-ct/get-snomed-ct</a>. 
                                    Individuals or organizations wishing to register as SNOMED International Affiliates can register at <a href="https://mlds.ihtsdotools.org/"
                                    >mlds.ihtsdotools.org</a>, subject to acceptance of the Affiliate License Agreement (<a href="http://www.snomed.org/resource/resource/117">see on the SNOMED International web site</a>).</p>
                        </li>
                        <li>
                            <p>The current list of SNOMED International Member Territories can be viewed at <a href="http://www.snomed.org/members">www.snomed.org/members</a>. Countries not included in that list are "Non-Member Territories".</p>
                        </li>
                        <li>
                            <p>End Users, that do not hold an SNOMED International Affiliate License, may access SNOMED CT® using <b>SNOMED CT Products</b> subject to acceptance of and adherence to the following sub-license limitations:
                                    <ol type="a">
                                    <li>The sub-licensee is only permitted to access SNOMED CT® using this software (or service) for the purpose of exploring and evaluating the terminology.</li>
                                    <li>The sub-licensee is not permitted the use of this software as part of a system that constitutes a SNOMED CT "Data Creation System" or "Data Analysis System", as defined in the SNOMED International Affiliate License. This means that the sub-licensee must not use <b>CT Products</b> to add or copy SNOMED CT identifiers into any type of record system, database or document.</li>
                                    <li>The sub-licensee is not permitted to translate or modify SNOMED CT Content or Derivatives.</li>
                                    <li>The sub-licensee is not permitted to distribute or share SNOMED CT Content or Derivatives.</li>
                                </ol>
                            </p>
                        </li>
                        <li>
                            <p>                        SNOMED International Affiliates may use <b>SNOMED CT Products</b> as part of a "Data Creation System" or "Data Analysis System" subject to the following conditions:
                                    <ol type="a">
                                    <li>The SNOMED International Affiliate, using <b>SNOMED CT Products</b> must accept full responsibility for any reporting and fees due for use or deployment of such a system in a Non-Member Territory.</li>
                                    <li>The SNOMED International Affiliate must not use <b>SNOMED International SNOMED CT Products</b> to access or interact with SNOMED CT in any way that is not permitted by the Affiliate License Agreement.</li>
                                    <li>In the event of termination of the Affiliate License Agreement, the use of <b>SNOMED CT Products</b> will be subject to the End User limitations noted in 4.</li>
                                </ol>
                            </p>
                        </li>
                    </ol>
                </text>
            </license>
            <publishingAuthority name="IHTSDO">
                <addrLine type="uri">http://ihtsdo.org</addrLine>
            </publishingAuthority>
            <description language="{$defaultLanguage}"/>
            <xsl:apply-templates select="concept"/>
        </browsableCodeSystem>
    </xsl:template>
    <xsl:template match="concept">
        <concept code="{@conceptId}" effectiveDate="{concat(substring(@effectiveTime,1,4),'-',substring(@effectiveTime,5,2),'-',substring(@effectiveTime,7,2))}" moduleId="{@moduleId}">
            <xsl:variable name="statusCode">
                <xsl:choose>
                    <xsl:when test="@active = '1'">
                        <xsl:value-of select="'active'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'retired'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="type">
                <xsl:choose>
                    <xsl:when test="$statusCode='retired'">
                        <xsl:value-of select="'D'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:choose>
                            <xsl:when test="dest[@active = '1']">
                                <xsl:value-of select="'S'"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="'L'"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:attribute name="statusCode">
                <xsl:value-of select="$statusCode"/>
            </xsl:attribute>
            <xsl:attribute name="sufficientlyDefined">
                <xsl:choose>
                    <xsl:when test="@definitionStatusId = '900000000000073002'">
                        <xsl:value-of select="'true'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'false'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="level">
                <xsl:choose>
                    <xsl:when test="ancestors/id">
                        <xsl:value-of select="max(ancestors/id/xs:integer(@distance))"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'0'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="type">
                        <xsl:value-of select="$type"/>
            </xsl:attribute>
            <xsl:for-each select="desc[@type = ('fsn', 'pref', 'syn')][@active = '1']">
                <xsl:variable name="language">
                    <xsl:choose>
                        <xsl:when test="@languageCode = 'en' and @languageRefsetId = '900000000000509007'">
                            <xsl:value-of select="'en-US'"/>
                        </xsl:when>
                        <xsl:when test="@languageCode = 'en' and @languageRefsetId = '900000000000508004'">
                            <xsl:value-of select="'en-GB'"/>
                        </xsl:when>
                        <xsl:when test="@languageCode = 'nl'">
                            <xsl:value-of select="'nl-NL'"/>
                        </xsl:when>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="descStatusCode">
                    <xsl:choose>
                        <xsl:when test="@active = '1'">
                            <xsl:value-of select="'active'"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="'retired'"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="use">
                    <xsl:choose>
                        <xsl:when test="@languageRefsetId = '15551000146102'">
                            <xsl:value-of select="'prefPat'"/>
                        </xsl:when>
                        <xsl:when test="@languageRefsetId = '160161000146108'">
                            <xsl:value-of select="'prefPatB1'"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="@type"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <designation lang="{$language}" use="{$use}" statusCode="{$descStatusCode}" effectiveDate="{concat(substring(@effectiveTime,1,4),'-',substring(@effectiveTime,5,2),'-',substring(@effectiveTime,7,2))}" expirationDate="" lastModifiedDate="" count="{@count}" length="{@length}">
                    <xsl:value-of select="."/>
                </designation>
            </xsl:for-each>
            <xsl:for-each select="definition[@active = '1'] | desc[@type = 'def'][@active = '1']">
                <xsl:variable name="language">
                    <xsl:choose>
                        <xsl:when test="@languageCode = 'en' and @languageRefsetId = '900000000000509007'">
                            <xsl:value-of select="'en-US'"/>
                        </xsl:when>
                        <xsl:when test="@languageCode = 'en' and @languageRefsetId = '900000000000508004'">
                            <xsl:value-of select="'en-GB'"/>
                        </xsl:when>
                        <xsl:when test="@languageCode = 'nl'">
                            <xsl:value-of select="'nl-NL'"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="@languageCode"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="defStatusCode">
                    <xsl:choose>
                        <xsl:when test="@active = '1'">
                            <xsl:value-of select="'active'"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="'retired'"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <definition lang="{$language}" statusCode="{$defStatusCode}" effectiveDate="{concat(substring(@effectiveTime,1,4),'-',substring(@effectiveTime,5,2),'-',substring(@effectiveTime,7,2))}">
                    <xsl:value-of select="."/>
                </definition>
            </xsl:for-each>
            <xsl:for-each select="annotation">
                <xsl:variable name="language">
                    <xsl:choose>
                        <xsl:when test="@languageCode = 'nl' and @typeId='480411000146103'">
                            <xsl:value-of select="'nl-NL'"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="@languageCode"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="annStatusCode">
                    <xsl:choose>
                        <xsl:when test="@active = '1'">
                            <xsl:value-of select="'active'"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="'retired'"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <annotation lang="{$language}" statusCode="{$annStatusCode}" effectiveDate="{concat(substring(@effectiveTime,1,4),'-',substring(@effectiveTime,5,2),'-',substring(@effectiveTime,7,2))}">
                    <xsl:value-of select="."/>
                </annotation>
            </xsl:for-each>
            <xsl:for-each select="dest[@active = '1']">
                <xsl:variable name="childConcept" select="key('conceptKey', @sourceId)"/>
                <xsl:variable name="childStatusCode">
                    <xsl:choose>
                        <xsl:when test="$childConcept/@active = '1'">
                            <xsl:value-of select="'active'"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="'retired'"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <child cCode="{@sourceId}" subCount="{@subCount}">                         
                    <xsl:variable name="childType">
                        <xsl:choose>
                            <xsl:when test="$childStatusCode='retired'">
                                <xsl:value-of select="'D'"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:choose>
                                    <xsl:when test="$childConcept/dest[@active = '1']">
                                        <xsl:value-of select="'S'"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="'L'"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    <xsl:attribute name="level">
                        <xsl:choose>
                            <xsl:when test="$childConcept/ancestors">
                                <xsl:value-of select="max($childConcept/ancestors/id/@distance)"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="'0'"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                    <xsl:attribute name="type">
                        <xsl:value-of select="$childType"/>
                    </xsl:attribute>
                    <xsl:for-each select="$childConcept/desc[@type = 'pref'][@active = '1']">
                        <xsl:variable name="language">
                            <xsl:choose>
                                <xsl:when test="@languageCode = 'en' and @languageRefsetId = '900000000000509007'">
                                    <xsl:value-of select="'en-US'"/>
                                </xsl:when>
                                <xsl:when test="@languageCode = 'en' and @languageRefsetId = '900000000000508004'">
                                    <xsl:value-of select="'en-GB'"/>
                                </xsl:when>
                                <xsl:when test="@languageCode = 'nl' and @languageRefsetId = '31000146106'">
                                    <xsl:value-of select="'nl-NL'"/>
                                </xsl:when>
                            </xsl:choose>
                        </xsl:variable>
                        <xsl:if test="string-length($language) gt 0">
                            <label lang="{$language}">
                                <xsl:value-of select="."/>
                            </label>
                        </xsl:if>
                    </xsl:for-each>
                </child>
            </xsl:for-each>
            <xsl:for-each select="grp[@grp = '0']/src[@typeId = '116680003'][@active = '1']">
                <xsl:variable name="parentSubCount" select="count(key('conceptKey', @destinationId)/dest[@active = '1'])"/>
                <xsl:variable name="parentConcept" select="key('conceptKey', @destinationId)"/>
                <parent pCode="{@destinationId}" subCount="{$parentSubCount}">
                    <xsl:for-each select="$parentConcept/desc[@type = 'pref'][@active = '1']">
                        <xsl:variable name="language">
                            <xsl:choose>
                                <xsl:when test="@languageCode = 'en' and @languageRefsetId = '900000000000509007'">
                                    <xsl:value-of select="'en-US'"/>
                                </xsl:when>
                                <xsl:when test="@languageCode = 'en' and @languageRefsetId = '900000000000508004'">
                                    <xsl:value-of select="'en-GB'"/>
                                </xsl:when>
                                <xsl:when test="@languageCode = 'nl' and @languageRefsetId = '31000146106'">
                                    <xsl:value-of select="'nl-NL'"/>
                                </xsl:when>
                            </xsl:choose>
                        </xsl:variable>
                        <xsl:if test="string-length($language) gt 0">
                            <label lang="{$language}">
                                <xsl:value-of select="."/>
                            </label>
                        </xsl:if>
                    </xsl:for-each>
                </parent>
            </xsl:for-each>
            <xsl:if test="ancestors/id">
                <xsl:for-each select="ancestors/id">
                    <ancestor distance="{@distance}">
                        <xsl:value-of select="."/>
                    </ancestor>
                </xsl:for-each>
                <ancSlf>
                    <xsl:value-of select="@conceptId"/>
                </ancSlf>
                <xsl:for-each select="ancestors/id">
                    <ancSlf>
                        <xsl:value-of select="."/>
                    </ancSlf>
                </xsl:for-each>
            </xsl:if>
            <!-- relationship groups -->
            <xsl:for-each select="grp[@grp != '0' or (@grp = '0' and src/@typeId != '116680003' and src/@active = '1')]">
                <relGrp grpId="{@grp}">
                    <xsl:for-each select="src[@typeId != '116680003'][@active = '1']">
                        <xsl:variable name="relatedConcept" select="key('conceptKey', @destinationId)"/>
                        <xsl:variable name="relatedTypeConcept" select="key('conceptKey', @typeId)"/>
                        <relation destCode="{@destinationId}" typeCode="{@typeId}" effectiveDate="{concat(substring(@effectiveTime,1,4),'-',substring(@effectiveTime,5,2),'-',substring(@effectiveTime,7,2))}">
                            <ancest>
                                <xsl:value-of select="@destinationId"/>
                            </ancest>
                            <xsl:for-each select="key('conceptKey', @destinationId)/ancestors/id">
                                <ancest>
                                    <xsl:value-of select="."/>
                                </ancest>
                            </xsl:for-each>
                            <xsl:for-each select="$relatedConcept/desc[@type = 'pref'][@active = '1']">
                                <xsl:variable name="language">
                                    <xsl:choose>
                                        <xsl:when test="@languageCode = 'en' and @languageRefsetId = '900000000000509007'">
                                            <xsl:value-of select="'en-US'"/>
                                        </xsl:when>
                                        <xsl:when test="@languageCode = 'en' and @languageRefsetId = '900000000000508004'">
                                            <xsl:value-of select="'en-GB'"/>
                                        </xsl:when>
                                        <xsl:when test="@languageCode = 'nl' and @languageRefsetId = '31000146106'">
                                            <xsl:value-of select="'nl-NL'"/>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:variable>
                                <xsl:if test="string-length($language) gt 0">
                                    <label lang="{$language}">
                                        <xsl:value-of select="."/>
                                    </label>
                                </xsl:if>
                                <!--                                <label lang="{$language}">
                                    <xsl:value-of select="."/>
                                </label>-->
                            </xsl:for-each>
                            <xsl:for-each select="$relatedTypeConcept/desc[@type = 'pref'][@active = '1']">
                                <xsl:variable name="language">
                                    <xsl:choose>
                                        <xsl:when test="@languageCode = 'en' and @languageRefsetId = '900000000000509007'">
                                            <xsl:value-of select="'en-US'"/>
                                        </xsl:when>
                                        <xsl:when test="@languageCode = 'en' and @languageRefsetId = '900000000000508004'">
                                            <xsl:value-of select="'en-GB'"/>
                                        </xsl:when>
                                        <xsl:when test="@languageCode = 'nl' and @languageRefsetId = '31000146106'">
                                            <xsl:value-of select="'nl-NL'"/>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:variable>
                                <xsl:if test="string-length($language) gt 0">
                                    <typeLabel lang="{$language}">
                                        <xsl:value-of select="."/>
                                    </typeLabel>
                                </xsl:if>
                            </xsl:for-each>
                        </relation>
                    </xsl:for-each>
                </relGrp>
            </xsl:for-each>
            <!-- refsets -->
            <xsl:if test="refsets">
                <refsets>
                    <xsl:for-each select="refsets/refset">
                        <refset refsetId="{@refsetId}">
                            <label lang="en-US">
                                <xsl:value-of select="@refset"/>
                            </label>
                        </refset>
                    </xsl:for-each>
                </refsets>
            </xsl:if>
            <!-- maps -->
            <xsl:if test="maps">
                <maps>
                    <xsl:for-each select="maps/map[@active = '1']">
                        <map refsetId="{@refsetId}" mapGroup="{@mapGroup}" mapPriority="{@mapPriority}" mapRule="{@mapRule}" mapAdvice="{@mapAdvice}" mapTarget="{@mapTarget}" correlationId="{@correlationId}" refset="{@refset}" correlation="{@correlation}"/>
                    </xsl:for-each>
                </maps>
            </xsl:if>
            <!-- associations -->
            <xsl:for-each select="association[@active = '1']">
                <xsl:variable name="associationRefset" select="key('conceptKey', @refsetId)"/>
                <xsl:variable name="associationTarget" select="key('conceptKey', @targetComponentId)"/>
                <association refsetId="{@refsetId}" targetComponentId="{@targetComponentId}">
                    <xsl:for-each select="$associationRefset/desc[@type = 'pref'][@active = '1']">
                        <xsl:variable name="language">
                            <xsl:choose>
                                <xsl:when test="@languageCode = 'en' and @languageRefsetId = '900000000000509007'">
                                    <xsl:value-of select="'en-US'"/>
                                </xsl:when>
                                <xsl:when test="@languageCode = 'en' and @languageRefsetId = '900000000000508004'">
                                    <xsl:value-of select="'en-GB'"/>
                                </xsl:when>
                                <xsl:when test="@languageCode = 'nl' and @languageRefsetId = '31000146106'">
                                    <xsl:value-of select="'nl-NL'"/>
                                </xsl:when>
                            </xsl:choose>
                        </xsl:variable>
                        <xsl:if test="string-length($language) gt 0">
                            <refsetLabel lang="{$language}">
                                <xsl:value-of select="."/>
                            </refsetLabel>
                        </xsl:if>
                    </xsl:for-each>

                    <xsl:for-each select="$associationTarget/desc[@type = 'pref'][@active = '1']">
                        <xsl:variable name="language">
                            <xsl:choose>
                                <xsl:when test="@languageCode = 'en' and @languageRefsetId = '900000000000509007'">
                                    <xsl:value-of select="'en-US'"/>
                                </xsl:when>
                                <xsl:when test="@languageCode = 'en' and @languageRefsetId = '900000000000508004'">
                                    <xsl:value-of select="'en-GB'"/>
                                </xsl:when>
                                <xsl:when test="@languageCode = 'nl' and @languageRefsetId = '31000146106'">
                                    <xsl:value-of select="'nl-NL'"/>
                                </xsl:when>
                            </xsl:choose>
                        </xsl:variable>
                        <xsl:if test="string-length($language) gt 0">
                            <targetLabel lang="{$language}">
                                <xsl:value-of select="."/>
                            </targetLabel>
                        </xsl:if>
                    </xsl:for-each>
                </association>
            </xsl:for-each>
            <!-- attribute values -->
            <xsl:for-each select="attributeValue[@active = '1']">
                <xsl:variable name="attributeValueRefset" select="key('conceptKey', @refsetId)"/>
                <xsl:variable name="attributeValue" select="key('conceptKey', @valueId)"/>
                <attributeValue refsetId="{@refsetId}" valueId="{@valueId}">
                    <xsl:for-each select="$attributeValueRefset/desc[@type = 'pref'][@active = '1']">
                        <xsl:variable name="language">
                            <xsl:choose>
                                <xsl:when test="@languageCode = 'en' and @languageRefsetId = '900000000000509007'">
                                    <xsl:value-of select="'en-US'"/>
                                </xsl:when>
                                <xsl:when test="@languageCode = 'en' and @languageRefsetId = '900000000000508004'">
                                    <xsl:value-of select="'en-GB'"/>
                                </xsl:when>
                                <xsl:when test="@languageCode = 'nl' and @languageRefsetId = '31000146106'">
                                    <xsl:value-of select="'nl-NL'"/>
                                </xsl:when>
                            </xsl:choose>
                        </xsl:variable>
                        <xsl:if test="string-length($language) gt 0">
                            <refsetLabel lang="{$language}">
                                <xsl:value-of select="."/>
                            </refsetLabel>
                        </xsl:if>
                    </xsl:for-each>
                    <xsl:for-each select="$attributeValue/desc[@type = 'pref'][@active = '1']">
                        <xsl:variable name="language">
                            <xsl:choose>
                                <xsl:when test="@languageCode = 'en' and @languageRefsetId = '900000000000509007'">
                                    <xsl:value-of select="'en-US'"/>
                                </xsl:when>
                                <xsl:when test="@languageCode = 'en' and @languageRefsetId = '900000000000508004'">
                                    <xsl:value-of select="'en-GB'"/>
                                </xsl:when>
                                <xsl:when test="@languageCode = 'nl' and @languageRefsetId = '31000146106'">
                                    <xsl:value-of select="'nl-NL'"/>
                                </xsl:when>
                            </xsl:choose>
                        </xsl:variable>
                        <xsl:if test="string-length($language) gt 0">
                            <valueLabel lang="{$language}">
                                <xsl:value-of select="."/>
                            </valueLabel>
                        </xsl:if>
                    </xsl:for-each>

                </attributeValue>
            </xsl:for-each>


        </concept>
    </xsl:template>
</xsl:stylesheet>
