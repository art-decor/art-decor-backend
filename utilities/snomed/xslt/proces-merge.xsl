<?xml version="1.0" encoding="UTF-8"?>
<!--
	Copyright (C) 2011-2016 ART-DECOR Expert Group (art-decor.org)
	
	Author: Gerrit Boers
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
-->

<!-- 



-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
   <xsl:output method="xml" indent="no" exclude-result-prefixes="#all" encoding="UTF-8"/>
   <xsl:key name="descriptionId" match="description" use="@conceptId"/>
   <xsl:key name="definitionId" match="definition" use="@conceptId"/>
   <xsl:key name="annotationId" match="annotation" use="@referencedComponentId"/>
   <xsl:key name="sourceId" match="relation" use="@sourceId"/>
   <xsl:key name="destinationId" match="relation" use="@destinationId"/>

   <xsl:template match="/concepts">
      <concepts>
         <xsl:for-each select="concept">
            <xsl:variable name="currentId" select="@id"/>
            <concept conceptId="{$currentId}" effectiveTime="{@effectiveTime}" active="{@active}" moduleId="{@moduleId}" definitionStatusId="{@definitionStatusId}"
               definitionStatus="{@definitionStatus}">
               <!-- 
                  order and sort descriptions
               English fsn - pref - syn
               Other languages grouped and sorted by languageCode
               
               -->
               <xsl:for-each-group select="key('descriptionId', $currentId)" group-by="@languageCode">
                  <xsl:sort select="@languagecode"/>
                  <!-- Only get the English FSN once, US language refset -->
                  <xsl:for-each select="current-group()[current-grouping-key()='en'][@typeId='900000000000003001'][@languageRefsetId='900000000000509007']">
                     <desc type="fsn" count="{@count}" length="{@length}" effectiveTime="{@effectiveTime}" active="{@active}" moduleId="{@moduleId}" languageCode="{@languageCode}"
                        typeId="{@typeId}"
                        acceptabilityId="{@acceptabilityId}" languageRefsetId="{@languageRefsetId}">
                        <xsl:value-of select="text"/>
                     </desc>
                  </xsl:for-each>
                  <xsl:for-each select="current-group()[current-grouping-key()='en'][@typeId='900000000000013009'][@acceptabilityId='900000000000548007']">
                     <desc type="pref" count="{@count}" length="{@length}" effectiveTime="{@effectiveTime}" active="{@active}" moduleId="{@moduleId}" languageCode="{@languageCode}"
                        typeId="{@typeId}"
                        acceptabilityId="{@acceptabilityId}" languageRefsetId="{@languageRefsetId}">
                        <xsl:value-of select="text"/>
                     </desc>
                  </xsl:for-each>
                  <xsl:for-each select="current-group()[current-grouping-key()='en'][@typeId='900000000000013009'][@acceptabilityId='900000000000549004']">
                     <desc type="syn" count="{@count}" length="{@length}" effectiveTime="{@effectiveTime}" active="{@active}" moduleId="{@moduleId}" languageCode="{@languageCode}"
                        typeId="{@typeId}"
                        acceptabilityId="{@acceptabilityId}" languageRefsetId="{@languageRefsetId}">
                        <xsl:value-of select="text"/>
                     </desc>
                  </xsl:for-each>
                  <!-- other languages -->
                  <xsl:for-each select="current-group()[current-grouping-key()!='en'][@typeId='900000000000003001']">
                     <desc type="fsn" count="{@count}" length="{@length}" effectiveTime="{@effectiveTime}" active="{@active}" moduleId="{@moduleId}" languageCode="{@languageCode}"
                        typeId="{@typeId}"
                        acceptabilityId="{@acceptabilityId}" languageRefsetId="{@languageRefsetId}">
                        <xsl:value-of select="text"/>
                     </desc>
                  </xsl:for-each>
                  <xsl:for-each select="current-group()[current-grouping-key()!='en'][@typeId='900000000000013009'][@acceptabilityId='900000000000548007']">
                     <desc type="pref" count="{@count}" length="{@length}" effectiveTime="{@effectiveTime}" active="{@active}" moduleId="{@moduleId}" languageCode="{@languageCode}"
                        typeId="{@typeId}"
                        acceptabilityId="{@acceptabilityId}" languageRefsetId="{@languageRefsetId}">
                        <xsl:value-of select="text"/>
                     </desc>
                  </xsl:for-each>
                  <xsl:for-each select="current-group()[current-grouping-key()!='en'][@typeId='900000000000013009'][@acceptabilityId='900000000000549004']">
                     <desc type="syn" count="{@count}" length="{@length}" effectiveTime="{@effectiveTime}" active="{@active}" moduleId="{@moduleId}" languageCode="{@languageCode}"
                        typeId="{@typeId}"
                        acceptabilityId="{@acceptabilityId}" languageRefsetId="{@languageRefsetId}">
                        <xsl:value-of select="text"/>
                     </desc>
                  </xsl:for-each>
               </xsl:for-each-group>
               <xsl:for-each select="key('definitionId',$currentId)">
                  <definition type="def" effectiveTime="{@effectiveTime}" active="{@active}" moduleId="{@moduleId}" languageCode="{@languageCode}"
                     typeId="{@typeId}" languageRefsetId="{@languageRefsetId}">
                     <xsl:value-of select="text"/>
                  </definition>
               </xsl:for-each>
               <xsl:for-each select="key('annotationId',$currentId)">
                  <annotation effectiveTime="{@effectiveTime}" active="{@active}" moduleId="{@moduleId}" languageCode="{@languageDialectCode}" typeId="{@typeId}">
                     <xsl:value-of select="@value"/>
                  </annotation>
               </xsl:for-each>
               <xsl:for-each select="key('sourceId', $currentId)">
                  <src effectiveTime="{@effectiveTime}" active="{@active}" moduleId="{@moduleId}" destinationId="{@destinationId}" relationshipGroup="{@relationshipGroup}"
                     typeId="{@typeId}" characteristicTypeId="{@characteristicTypeId}" modifierId="{@modifierId}"/>
               </xsl:for-each>
               <xsl:for-each select="key('destinationId', $currentId)">
                  <xsl:if test="@typeId='116680003'">
                     <dest effectiveTime="{@effectiveTime}" active="{@active}" moduleId="{@moduleId}" sourceId="{@sourceId}" relationshipGroup="{@relationshipGroup}" typeId="{@typeId}"
                        characteristicTypeId="{@characteristicTypeId}" modifierId="{@modifierId}"/>
                  </xsl:if>
               </xsl:for-each>
            </concept>
         </xsl:for-each>
      </concepts>
   </xsl:template>
</xsl:stylesheet>
