<?xml version="1.0" encoding="UTF-8"?>
<!--
	Copyright (C) 2011-2016 ART-DECOR Expert Group (art-decor.org)
	
	Author: Gerrit Boers
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
	
	 java -Xmx8096m -jar saxon9.jar -t -s:fileListSnapshot.xml -xsl:merge-refsets.xsl -o:refset-concept-merge.xml
	
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
   <xsl:output method="xml" exclude-result-prefixes="#all" encoding="UTF-8"/>
   <!-- characterset of input files, incorrect setting will result in error: Failed to read input file .... (java.nio.charset.MalformedInputException) - Input length = 1 -->
   <xsl:param name="charSet" select="'UTF-8'"/>
   <xsl:param name="releaseType" select="'Snapshot'"/>
   <xsl:template match="/list">
      <refsets>
         <!-- Refsets -->
         <xsl:for-each select="file[@rows = 'refset']">
            <xsl:variable name="refsets" select="doc(concat('../XML/', @path, '/', @name, '.xml'))/refsets"/>
            <xsl:for-each select="$refsets/refset">
               <xsl:copy-of select="."/>
            </xsl:for-each>
         </xsl:for-each>
         
         <!-- Simple Maps -->
         <xsl:for-each select="file[@rows = 'simpleMap']">
            <xsl:variable name="simpleMaps" select="doc(concat('../XML/', @path, '/', @name, '.xml'))/simpleMaps"/>
            <xsl:for-each select="$simpleMaps/simpleMap">
               <xsl:copy-of select="."/>
            </xsl:for-each>
         </xsl:for-each>
         
         
         <!-- Extended maps -->
         <xsl:for-each select="file[@rows = 'extendedMap']">
            <xsl:variable name="extendedMaps" select="doc(concat('../XML/', @path, '/', @name, '.xml'))/extendedMaps"/>
            <xsl:for-each select="$extendedMaps/extendedMap">
               <xsl:copy-of select="."/>
            </xsl:for-each>
         </xsl:for-each>


         <!-- Correlated maps -->
         <xsl:for-each select="file[@rows = 'correlatedMap']">
            <xsl:variable name="correlatedMaps" select="doc(concat('../XML/', @path, '/', @name, '.xml'))/correlatedMaps"/>
            <xsl:for-each select="$correlatedMaps/correlatedMap">
               <xsl:copy-of select="."/>
            </xsl:for-each>
         </xsl:for-each>
         
         
         <!-- Correlated Extended maps -->
         <xsl:for-each select="file[@rows = 'correlatedExtendedMap']">
            <xsl:variable name="correlatedExtendedMaps" select="doc(concat('../XML/', @path, '/', @name, '.xml'))/correlatedExtendedMaps"/>
            <xsl:for-each select="$correlatedExtendedMaps/correlatedExtendedMap">
               <xsl:copy-of select="."/>
            </xsl:for-each>
         </xsl:for-each>


         <!-- Asociation References -->
         <xsl:for-each select="file[@rows = 'associationReference']">
            <xsl:variable name="associationReferences" select="doc(concat('../XML/', @path, '/', @name, '.xml'))/associationReferences"/>
            <xsl:for-each select="$associationReferences/associationReference">
               <xsl:copy-of select="."/>
            </xsl:for-each>
         </xsl:for-each>
         
         
         <!-- Attribute Values -->
         <xsl:for-each select="file[@rows = 'attributeValue']">
            <xsl:variable name="attributeValues" select="doc(concat('../XML/', @path, '/', @name, '.xml'))/attributeValues"/>
            <xsl:for-each select="$attributeValues/attributeValue">
               <xsl:copy-of select="."/>
            </xsl:for-each>
         </xsl:for-each>




         <!-- DHD DT refset -->
         <xsl:variable name="refsetsNL" select="doc('../XML/RF2-DHD-refsets/refsets.xml')/refsets"/>
         <xsl:for-each select="$refsetsNL/refset">
            <xsl:copy-of select="."/>
         </xsl:for-each>

         <xsl:variable name="concepts" select="doc('../concepts_en-US_sorted_closed.xml')/concepts"/>
         <xsl:for-each select="$concepts/concept">
            <xsl:copy-of select="."/>
         </xsl:for-each>
      </refsets>
   </xsl:template>
</xsl:stylesheet>
