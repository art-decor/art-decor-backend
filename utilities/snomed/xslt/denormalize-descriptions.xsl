<?xml version="1.0" encoding="UTF-8"?>
<!--
	Copyright (C) 2011-2016 ART-DECOR Expert Group (art-decor.org)
	
	Author: Gerrit Boers
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
-->

<!--
	Denormalize descriptions for language reference sets
	Add language refsetId and acceptabilityId to description
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
	<xsl:output method="xml" exclude-result-prefixes="#all" encoding="UTF-8"/>
	<xsl:key name="descriptionId" match="description" use="@id"/>
	<xsl:key name="definitionId" match="textDefinition" use="@id"/>
	<xsl:template match="/list">
		<descriptions>
			<!-- exclude NL B1 refset -->
			<xsl:for-each select="language[@active='1'][not(@refsetId='160161000146108')]">
				<xsl:variable name="currentId" select="@referencedComponentId"/>
				<xsl:variable name="description" select="key('descriptionId', $currentId)"/>
				<xsl:variable name="definition" select="key('definitionId',$currentId)"/>
				<xsl:variable name="acceptabilityId" select="@acceptabilityId"/>
				<xsl:variable name="languageRefsetId" select="@refsetId"/>
				<xsl:for-each select="$description[@active='1']">                
					<xsl:variable name="descriptionLanguage">
					<xsl:choose>
						<xsl:when test="$languageRefsetId = '900000000000509007'">
							<xsl:value-of select="'en'"/>
						</xsl:when>
						<xsl:when test="$languageRefsetId = '900000000000508004'">
							<xsl:value-of select="'en'"/>
						</xsl:when>
						<xsl:when test="$languageRefsetId = '31000146106'">
							<xsl:value-of select="'nl'"/>
						</xsl:when>
						<xsl:when test="$languageRefsetId= '15551000146102'">
							<xsl:value-of select="'nl'"/>
						</xsl:when>
						<xsl:when test="$languageRefsetId = '160161000146108'">
							<xsl:value-of select="'nl'"/>
						</xsl:when>
					</xsl:choose>
				</xsl:variable>
					<description id="{@id}" count="{count(tokenize(@term,'\s'))}" length="{string-length(@term)}"
						effectiveTime="{@effectiveTime}" active="{@active}" moduleId="{@moduleId}" conceptId="{@conceptId}" languageCode="{$descriptionLanguage}"
						typeId="{@typeId}" acceptabilityId="{$acceptabilityId}" languageRefsetId="{$languageRefsetId}">
						<text><xsl:value-of select="@term"/></text>
					</description>
				</xsl:for-each>
				<xsl:for-each select="$definition[@active='1']">              
					<xsl:variable name="definitionLanguage">
						<xsl:choose>
							<xsl:when test="$languageRefsetId = '900000000000509007'">
								<xsl:value-of select="'en'"/>
							</xsl:when>
							<xsl:when test="$languageRefsetId = '900000000000508004'">
								<xsl:value-of select="'en'"/>
							</xsl:when>
							<xsl:when test="$languageRefsetId = '31000146106'">
								<xsl:value-of select="'nl'"/>
							</xsl:when>
							<xsl:when test="$languageRefsetId = '15551000146102'">
								<xsl:value-of select="'nl'"/>
							</xsl:when>
							<xsl:when test="$languageRefsetId = '160161000146108'">
								<xsl:value-of select="'nl'"/>
							</xsl:when>
						</xsl:choose>
					</xsl:variable>
					<definition effectiveTime="{@effectiveTime}" active="{@active}" moduleId="{@moduleId}" conceptId="{@conceptId}" languageCode="{$definitionLanguage}"
						typeId="{@typeId}" acceptabilityId="{$acceptabilityId}" languageRefsetId="{$languageRefsetId}">
						<text><xsl:value-of select="@term"/></text>
					</definition>
				</xsl:for-each>
			</xsl:for-each>
			<xsl:for-each select="annotation[@active='1']">
				<xsl:copy-of select="."/>
			</xsl:for-each>
		</descriptions>
	</xsl:template>
</xsl:stylesheet>
