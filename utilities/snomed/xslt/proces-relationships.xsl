<?xml version="1.0" encoding="UTF-8"?>
<!--
	Copyright (C) 2011-2016 ART-DECOR Expert Group (art-decor.org)
	
	Author: Gerrit Boers
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
   <xsl:output method="xml" indent="no" exclude-result-prefixes="#all" encoding="UTF-8"/>
   <xsl:key name="conceptId" match="concept" use="@conceptId"/>
   <!-- variable for generated fileListSnapshot -->
   <xsl:variable name="fileListSnapshot" select="doc('../fileListSnapshot.xml')/list"/>
   
   <!-- variable for selecting preferred terms to use for qualifiers, default is US language refset -->
   <xsl:variable name="preferredLanguageRefsetId" select="'900000000000509007'"/>
   
   <xsl:variable name="preferredSourceDestLanguageRefsetId">
      <xsl:choose>
         <!-- NL -->
         <xsl:when test="$fileListSnapshot/@releaseType='NL'">
            <xsl:value-of select="'31000146106'"/>
         </xsl:when>
         <!-- en US -->
         <xsl:otherwise>
            <xsl:value-of select="'900000000000509007'"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:variable>
   <xsl:variable name="preferredSourceDestLanguageCode">
      <xsl:choose>
         <!-- NL -->
         <xsl:when test="$fileListSnapshot/@releaseType='NL'">
            <xsl:value-of select="'nl'"/>
         </xsl:when>
         <!-- en US -->
         <xsl:otherwise>
            <xsl:value-of select="'en'"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:variable>
   <!-- EN -->
<!--   <xsl:variable name="preferredSourceDestLanguageRefsetId" select="'900000000000509007'"/>
   <xsl:variable name="preferredSourceDestLanguageCode" select="'en'"/>-->
   
   <xsl:variable name="fallbackSourceDestLanguageRefsetId" select="'900000000000509007'"/>
   <xsl:variable name="fallbackSourceDestLanguageCode" select="'en'"/>
   <xsl:template match="/concepts">
      <concepts>
         <xsl:for-each select="concept">
            <concept conceptId="{@conceptId}" effectiveTime="{@effectiveTime}" active="{@active}" moduleId="{@moduleId}" definitionStatusId="{@definitionStatusId}" definitionStatus="{key('conceptId', @definitionStatusId)/desc[@type='pref'][@languageRefsetId=$preferredLanguageRefsetId][@languageCode='en'][@active='1']/text()}">
               <xsl:choose>
                  <xsl:when test="count(desc[@active = '1']) > 0">
                     <xsl:for-each select="desc[@active = '1']">
                        <xsl:copy-of select="."/>
                     </xsl:for-each>
                  </xsl:when>
                  <xsl:otherwise>
                     <xsl:for-each select="desc">
                        <xsl:copy-of select="."/>
                     </xsl:for-each>
                  </xsl:otherwise>
               </xsl:choose>
               <xsl:for-each select="definition">
                  <xsl:copy-of select="."/>
               </xsl:for-each>
               <xsl:for-each select="annotation">
                  <xsl:copy-of select="."/>
               </xsl:for-each>
               <xsl:for-each select="src[@active='1']">
                  <xsl:variable name="srcName">
                     <xsl:choose>
                        <xsl:when test="key('conceptId', @destinationId)/desc[@type = 'pref'][@languageRefsetId = $preferredSourceDestLanguageRefsetId][@languageCode = $preferredSourceDestLanguageCode][@active = '1']">
                           <xsl:value-of select="key('conceptId', @destinationId)/desc[@type = 'pref'][@languageRefsetId = $preferredSourceDestLanguageRefsetId][@languageCode = $preferredSourceDestLanguageCode][@active = '1']"/>
                        </xsl:when>
                        <xsl:otherwise>
                           <xsl:choose>
                              <xsl:when test="key('conceptId', @destinationId)/desc[@type = 'pref'][@languageRefsetId = $fallbackSourceDestLanguageRefsetId][@languageCode = $fallbackSourceDestLanguageCode][@active = '1']">
                                 <xsl:value-of select="key('conceptId', @destinationId)/desc[@type = 'pref'][@languageRefsetId = $fallbackSourceDestLanguageRefsetId][@languageCode = $fallbackSourceDestLanguageCode][@active = '1']"/>
                              </xsl:when>
                              <xsl:otherwise>
                                 <xsl:value-of select="key('conceptId', @destinationId)/desc[@type = 'syn'][@languageCode = 'en'][@active = '1'][1]"/>
                              </xsl:otherwise>
                           </xsl:choose>
                        </xsl:otherwise>
                     </xsl:choose>
                  </xsl:variable>
                  <src effectiveTime="{@effectiveTime}" active="{@active}" moduleId="{@moduleId}" destinationId="{@destinationId}" relationshipGroup="{@relationshipGroup}" typeId="{@typeId}" type="{key('conceptId', @typeId)/desc[@type='pref'][@languageRefsetId=$preferredLanguageRefsetId][@languageCode='en'][@active='1']/text()}" characteristicTypeId="{@characteristicTypeId}"
                     modifierId="{@modifierId}">
                     <xsl:value-of select="$srcName"/>
                  </src>
               </xsl:for-each>
               <xsl:for-each select="dest[@active='1']">
                  <xsl:variable name="destName">
                     <xsl:choose>
                        <xsl:when test="key('conceptId', @sourceId)/desc[@type = 'pref'][@languageRefsetId = $preferredSourceDestLanguageRefsetId][@active = '1']">
                           <xsl:value-of select="key('conceptId', @sourceId)/desc[@type = 'pref'][@languageRefsetId = $preferredSourceDestLanguageRefsetId][@active = '1']"/>
                        </xsl:when>
                        <xsl:otherwise>
                           <xsl:choose>
                              <xsl:when test="key('conceptId', @sourceId)/desc[@type = 'pref'][@languageRefsetId = $fallbackSourceDestLanguageRefsetId][@active = '1']">
                                 <xsl:value-of select="key('conceptId', @sourceId)/desc[@type = 'pref'][@languageRefsetId = $fallbackSourceDestLanguageRefsetId][@active = '1']"/>
                              </xsl:when>
                              <xsl:otherwise>
                                 <xsl:value-of select="key('conceptId', @sourceId)/desc[@type = 'syn'][@languageCode = 'en'][@active = '1'][1]"/>
                              </xsl:otherwise>
                           </xsl:choose>
                        </xsl:otherwise>
                     </xsl:choose>
                  </xsl:variable>
                  <dest effectiveTime="{@effectiveTime}" active="{@active}" moduleId="{@moduleId}" sourceId="{@sourceId}" relationshipGroup="{@relationshipGroup}" typeId="{@typeId}" type="{key('conceptId', @typeId)/desc[@type='pref'][@languageRefsetId=$preferredLanguageRefsetId][@languageCode='en'][@active='1']/text()}" characteristicTypeId="{@characteristicTypeId}"
                     modifierId="{@modifierId}">
                     <xsl:value-of select="$destName"/>
                  </dest>
               </xsl:for-each>
            </concept>
         </xsl:for-each>
      </concepts>
   </xsl:template>
</xsl:stylesheet>
