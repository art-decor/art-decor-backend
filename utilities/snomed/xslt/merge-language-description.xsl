<?xml version="1.0" encoding="UTF-8"?>
<!--
	Copyright (C) 2011-2016 ART-DECOR Expert Group (art-decor.org)
	
	Author: Gerrit Boers
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
   <xsl:output method="xml" exclude-result-prefixes="#all" encoding="UTF-8"/>
   <!-- characterset of input files, incorrect setting will result in error: Failed to read input file .... (java.nio.charset.MalformedInputException) - Input length = 1 -->
   <xsl:param name="charSet" select="'UTF-8'"/>
   <xsl:param name="releaseType" select="'Snapshot'"/>
   <xsl:param name="version"/>
   <xsl:template match="/list">
      <list>
         <!-- Get all descriptions in INTERNATIONAL edition -->
         <xsl:variable name="descriptions" select="doc(concat('XML/SnomedCT_InternationalRF2_PRODUCTION_',$version,'/', $releaseType, '/Terminology/descriptions.xml'))/descriptions"/>
         <xsl:for-each select="$descriptions/description">
            <xsl:copy-of select="."/>
         </xsl:for-each>
         <!-- Get all descriptions in NL edition -->
<!--         <xsl:variable name="descriptions" select="doc(concat('XML/SnomedCT_Netherlands_EditionRelease/', $releaseType, '/Terminology/descriptions.xml'))/descriptions"/>
         <xsl:for-each select="$descriptions/description">
            <xsl:copy-of select="."/>
         </xsl:for-each>-->
         <!-- If present, get all descriptions in DHD folder -->
<!--         <xsl:if test="doc-available('XML/RF2-DHD-refsets/descriptions.xml')">
            <xsl:variable name="descriptions" select="doc('XML/RF2-DHD-refsets/descriptions.xml')/descriptions"/>
            <xsl:for-each select="$descriptions/description">
               <xsl:copy-of select="."/>
            </xsl:for-each>
         </xsl:if>-->
         <!-- If present, get all descriptions in Patient friendly terms folder -->
         
<!--         <xsl:variable name="descriptions" select="doc('XML/SnomedCT_Netherlands_PatientFriendlyExtensionRelease_20190331/Snapshot/Terminology/descriptions.xml')/descriptions"/>
            <xsl:for-each select="$descriptions/description">
               <xsl:copy-of select="."/>
            </xsl:for-each>-->


         <!-- Get all  language definitions in INTERNATIONAL edition -->
         <xsl:variable name="languages" select="doc(concat('XML/SnomedCT_InternationalRF2_PRODUCTION_',$version,'/', $releaseType, '/Refset/Language/languages.xml'))/languages"/>
         <xsl:for-each select="$languages/language">
            <xsl:copy-of select="."/>
         </xsl:for-each>
         <!-- Get all  language definitions in NL edition -->
<!--         <xsl:variable name="languages" select="doc(concat('XML/SnomedCT_Netherlands_EditionRelease/', $releaseType, '/Refset/Language/languages.xml'))/languages"/>
         <xsl:for-each select="$languages/language">
            <xsl:copy-of select="."/>
         </xsl:for-each>-->
         <!-- If present, get all languages in DHD folder -->
<!--         <xsl:if test="doc-available('XML/RF2-DHD-refsets/languages.xml')">
            <xsl:variable name="languages" select="doc('XML/RF2-DHD-refsets/languages.xml')/languages"/>
            <xsl:for-each select="$languages/language">
               <xsl:copy-of select="."/>
            </xsl:for-each>
         </xsl:if>-->
         <!-- If present, get all languages in Patient friendly terms folder -->
         
<!--         <xsl:variable name="languages" select="doc('XML/SnomedCT_Netherlands_PatientFriendlyExtensionRelease_20190331/Snapshot/Refset/Language/languages.xml')/languages"/>
            <xsl:for-each select="$languages/language">
               <xsl:copy-of select="."/>
            </xsl:for-each>-->
         
      </list>
   </xsl:template>
</xsl:stylesheet>
