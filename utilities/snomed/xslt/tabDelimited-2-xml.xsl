<?xml version="1.0" encoding="UTF-8"?>
<!--
	Copyright (C) 2011 Nictiz
	
	Author: Gerrit Boers
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
	<xsl:output method="xml" exclude-result-prefixes="#all" encoding="UTF-8" indent="yes"/>
	<!-- characterset of input files, incorrect setting will result in error: Failed to read input file .... (java.nio.charset.MalformedInputException) - Input length = 1 -->
	<xsl:variable name="charSet" select="'UTF-8'"/>
	<xsl:template match="/list">
		<result>
			<xsl:for-each select="file">
				<xsl:variable name="fileName" select="@name"/>
				<xsl:variable name="rowName" select="@rows"/>
				<xsl:variable name="rootName" select="@file"/>
				<xsl:value-of select="$fileName"/>
				<!-- open file if available -->
				<xsl:choose>
					<xsl:when test="unparsed-text-available(concat('../input/',@path, '/', $fileName, '.txt'))">
						<xsl:variable name="fileText" select="unparsed-text(concat('../input/',@path, '/', $fileName, '.txt'), $charSet)"/>
						<xsl:result-document indent="no" encoding="UTF-8" href="{concat('XML/',@path,'/',$fileName,'.xml')}">
							<xsl:element name="{$rootName}">
								<!-- get column descriptions from first row-->
								<xsl:variable name="columns" select="tokenize(tokenize($fileText, '\r\n')[1], '\t')"/>
								<xsl:for-each select="tokenize($fileText, '\r\n')">
									<xsl:if test="position() > 1">
										<xsl:element name="{$rowName}">
											<xsl:for-each select="tokenize(., '\t')">
												<xsl:variable name="position" select="position()"/>
												<xsl:attribute name="{$columns[$position]}">
													<xsl:value-of select="."/>
												</xsl:attribute>
											</xsl:for-each>
										</xsl:element>
									</xsl:if>
								</xsl:for-each>
							</xsl:element>
						</xsl:result-document>
					</xsl:when>
					<xsl:otherwise>
						<missing>
							<xsl:copy-of select="."/>
						</missing>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</result>
	</xsl:template>
</xsl:stylesheet>
