<?xml version="1.0" encoding="UTF-8"?>
<!--
	Copyright (C) 2024 ADOT
		
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
	
	Stylesheet for generating refsets xml file for DHD DT and VT
	input xml: fileListSnapshot.xml (not actually used during processing)
	input    :    DT and VT tab delimited text file without heading containing concept id in the first colum
	output xml:   refsets file containing DT and VT refset
	
	files must be in the following format
	DT-yyyymmdd.txt
	VT-yyyymmdd.txt
	
	example call:
	java -jar saxon9.jar -t -s:fileListSnapshot.xml -xsl:xslt/dhd-text-to-refsets.xsl -o:XML/RF2-DHD-refsets/refsets.xml
	
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:uuid="java.util.UUID" version="2.0" exclude-result-prefixes="uuid">
   <xsl:output method="xml" exclude-result-prefixes="#all" encoding="UTF-8" indent="yes"/>
   <!-- characterset of input files, incorrect setting will result in error: Failed to read input file .... (java.nio.charset.MalformedInputException) - Input length = 1 -->
   <!-- variable for generated fileListSnapshot -->
   <xsl:variable name="fileListSnapshot" select="doc('../fileListSnapshot.xml')/list"/>
   
   <xsl:variable name="date" select="$fileListSnapshot/@date"/>
   <xsl:variable name="charSet" select="'UTF-8'"/>
   <xsl:template match="/">
      <refsets>
         <!-- DHD DT -->
         <xsl:choose>
            <xsl:when test="unparsed-text-available(concat('../input/DT-', $date, '.txt'))">
               <xsl:variable name="fileText" select="unparsed-text(concat('../input/DT-', $date, '.txt'), $charSet)"/>
               <xsl:for-each select="tokenize($fileText, '\r\n')">
                  <xsl:if test="string-length(tokenize(., '\t')[1]) > 1">
                     <xsl:element name="refset">
                        <xsl:attribute name="id">
                           <xsl:value-of select="uuid:randomUUID()"/>
                        </xsl:attribute>
                        <xsl:attribute name="effectiveTime">
                           <xsl:value-of select="'20180316'"/>
                        </xsl:attribute>
                        <xsl:attribute name="active">
                           <xsl:value-of select="'1'"/>
                        </xsl:attribute>
                        <xsl:attribute name="moduleId">
                           <xsl:value-of select="'11000146104'"/>
                        </xsl:attribute>
                        <xsl:attribute name="refsetId">
                           <xsl:value-of select="'31000147101'"/>
                        </xsl:attribute>
                        <xsl:attribute name="referencedComponentId">
                           <xsl:value-of select="tokenize(., '\t')[1]"/>
                        </xsl:attribute>
                     </xsl:element>
                  </xsl:if>
               </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
               <missing>DT file</missing>
            </xsl:otherwise>
         </xsl:choose>
         <!-- DHD VT -->
         <xsl:choose>
            <xsl:when test="unparsed-text-available(concat('../input/VT-', $date, '.txt'))">
               <xsl:variable name="fileText" select="unparsed-text(concat('../input/VT-', $date, '.txt'), $charSet)"/>
               <xsl:for-each select="tokenize($fileText, '\r\n')">
                  <xsl:if test="string-length(tokenize(., '\t')[1]) > 1">
                     <xsl:element name="refset">
                        <xsl:attribute name="id">
                           <xsl:value-of select="uuid:randomUUID()"/>
                        </xsl:attribute>
                        <xsl:attribute name="effectiveTime">
                           <xsl:value-of select="'20200930'"/>
                        </xsl:attribute>
                        <xsl:attribute name="active">
                           <xsl:value-of select="'1'"/>
                        </xsl:attribute>
                        <xsl:attribute name="moduleId">
                           <xsl:value-of select="'11000146104'"/>
                        </xsl:attribute>
                        <xsl:attribute name="refsetId">
                           <xsl:value-of select="'41000147108'"/>
                        </xsl:attribute>
                        <xsl:attribute name="referencedComponentId">
                           <xsl:value-of select="tokenize(., '\t')[1]"/>
                        </xsl:attribute>
                     </xsl:element>
                  </xsl:if>
               </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
               <missing>VT file</missing>
            </xsl:otherwise>
         </xsl:choose>
      </refsets>
   </xsl:template>
</xsl:stylesheet>
