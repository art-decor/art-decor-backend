<?xml version="1.0" encoding="UTF-8"?>
<!--
	Copyright (C) 2011-2016 ART-DECOR Expert Group (art-decor.org)
	
	Author: Gerrit Boers
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
-->

<!-- 
   Merge concepts, descriptions and relationships into one big file for further processing
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
   <xsl:output method="xml" exclude-result-prefixes="#all" encoding="UTF-8"/>
   <!-- characterset of input files, incorrect setting will result in error: Failed to read input file .... (java.nio.charset.MalformedInputException) - Input length = 1 -->
   <xsl:param name="charSet" select="'UTF-8'"/>
   <xsl:param name="releaseType" select="'Snapshot'"/>
   <xsl:template match="/list">
      <concepts>
         
         <!-- get concepts -->
         <xsl:for-each select="file[@rows='concept']">
            <xsl:variable name="concepts" select="doc(concat('../XML/',@path,'/',@name,'.xml'))/concepts"/>
            <xsl:for-each select="$concepts/concept">
               <xsl:copy-of select="."/>
            </xsl:for-each>
         </xsl:for-each>

         <!-- get relations -->
         <xsl:for-each select="file[@rows='relation']">
            <xsl:variable name="relations" select="doc(concat('../XML/',@path,'/',@name,'.xml'))/relations"/>
            <xsl:for-each select="$relations/relation">
               <xsl:copy-of select="."/>
            </xsl:for-each>
         </xsl:for-each>
         

         
         <!-- get concepts from DHD folder -->
         <xsl:if test="doc-available('../XML/RF2-DHD-refsets/concepts.xml')">
            <xsl:variable name="concepts" select="doc('../XML/RF2-DHD-refsets/concepts.xml')/concepts"/>
            <xsl:for-each select="$concepts/concept">
               <xsl:copy-of select="."/>
            </xsl:for-each>
         </xsl:if>
         <!-- get relations from DHD folder -->
         <xsl:if test="doc-available('../XML/RF2-DHD-refsets/relations.xml')">
            <xsl:variable name="concepts" select="doc('../XML/RF2-DHD-refsets/relations.xml')/relations"/>
            <xsl:for-each select="$concepts/relation">
               <xsl:copy-of select="."/>
            </xsl:for-each>
         </xsl:if>
         
         <!-- get descriptions, definitions and annotations from generated descriptions XML file -->
         <xsl:variable name="descriptions" select="doc('../descriptions_definitions_denormalized.xml')/descriptions"/>
         <xsl:for-each select="$descriptions/description">
            <xsl:copy-of select="."/>
         </xsl:for-each>     
         <xsl:for-each select="$descriptions/definition">
            <xsl:copy-of select="."/>
         </xsl:for-each>
         <xsl:for-each select="$descriptions/annotation">
            <xsl:copy-of select="."/>
         </xsl:for-each>
        
      </concepts>
   </xsl:template>
</xsl:stylesheet>
