<?xml version="1.0" encoding="UTF-8"?>
<!--
	Copyright (C) 2011-2016 ART-DECOR Expert Group (art-decor.org)
	
	Author: Gerrit Boers
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
   <xsl:output method="xml" indent="no" exclude-result-prefixes="#all" encoding="UTF-8"/>
   <xsl:key name="conceptId" match="concept" use="@conceptId"/>
   <xsl:template match="/concepts">
      <concepts>
         <xsl:for-each select="concept">
            <concept conceptId="{@conceptId}" effectiveTime="{@effectiveTime}" moduleId="{@moduleId}" definitionStatusId="{@definitionStatusId}"
               definitionStatus="{@definitionStatus}">
               <xsl:if test="@active='1'">
                  <xsl:attribute name="active" select="'1'"/>
               </xsl:if>
               <xsl:for-each select="desc">
                  <desc type="{@type}" count="{@count}" length="{@length}" effectiveTime="{@effectiveTime}" moduleId="{@moduleId}" languageCode="{@languageCode}"
                     typeId="{@typeId}" caseSignificanceId="{@caseSignificanceId}" acceptabilityId="{@acceptabilityId}" languageRefsetId="{@languageRefsetId}">
                     <xsl:if test="@active='1'">
                        <xsl:attribute name="active" select="'1'"/>
                     </xsl:if>
                     <xsl:value-of select="."/>
                  </desc>
               </xsl:for-each>
               <xsl:for-each select="definition">
                  <xsl:copy-of select="."/>
               </xsl:for-each>
               <xsl:for-each select="annotation">
                  <xsl:copy-of select="."/>
               </xsl:for-each>
               <xsl:variable name="ancestors">
                  <xsl:for-each select="src[@typeId='116680003'][@active='1']">
                     <id distance="1">
                        <xsl:value-of select="./@destinationId"/>
                     </id>
                     <xsl:call-template name="resolveAncestors">
                        <xsl:with-param name="src" select="."/>
                        <xsl:with-param name="distance" select="2"/>
                     </xsl:call-template>
                  </xsl:for-each>
               </xsl:variable>
               <ancestors>
                  <xsl:for-each select="distinct-values($ancestors/id)">
                     <xsl:variable name="currentId" select="."/>
                     <xsl:variable name="minimumDistance" select="min($ancestors/id[.=$currentId]/@distance)"/>
                     <id distance="{$minimumDistance}">
                        <xsl:value-of select="."/>
                     </id>
                  </xsl:for-each>
               </ancestors>
               <xsl:for-each-group select="src" group-by="@relationshipGroup">
                  <grp grp="{current-grouping-key()}">
                     <xsl:for-each select="current-group()">
                        <xsl:sort select="@type"/>
                        <xsl:sort select="lower-case(.)"/>
                        <src effectiveTime="{@effectiveTime}" destinationId="{@destinationId}" relationshipGroup="{@relationshipGroup}" typeId="{@typeId}" type="{@type}">
                           <xsl:if test="@active='1'">
                              <xsl:attribute name="active" select="'1'"/>
                           </xsl:if>
                           <xsl:value-of select="."/>
                        </src>
                     </xsl:for-each>
                  </grp>
               </xsl:for-each-group>
               <xsl:for-each select="dest">
                  <xsl:sort select="@type"/>
                  <xsl:sort select="lower-case(.)"/>
                  <dest effectiveTime="{@effectiveTime}" sourceId="{@sourceId}" relationshipGroup="{@relationshipGroup}" typeId="{@typeId}" type="{@type}"
                     subCount="{count(key('conceptId', @sourceId)/dest[@typeId='116680003'][@active='1'])}">
                     <xsl:if test="@active='1'">
                        <xsl:attribute name="active" select="'1'"/>
                     </xsl:if>
                     <xsl:value-of select="."/>
                  </dest>
               </xsl:for-each>
            </concept>
         </xsl:for-each>
      </concepts>
   </xsl:template>
   <xsl:template name="resolveAncestors">
      <xsl:param name="src"/>
      <xsl:param name="distance"/>
      <xsl:for-each select="key('conceptId', @destinationId)/src[@typeId='116680003'][@active='1']">
         <id distance="{$distance}">
            <xsl:value-of select="./@destinationId"/>
         </id>

         <xsl:call-template name="resolveAncestors">
            <xsl:with-param name="src" select="."/>
            <xsl:with-param name="distance" select="$distance+1"/>
         </xsl:call-template>

      </xsl:for-each>
   </xsl:template>
</xsl:stylesheet>
