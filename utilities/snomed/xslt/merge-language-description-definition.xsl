<?xml version="1.0" encoding="UTF-8"?>
<!--
	Copyright (C) 2011-2016 ART-DECOR Expert Group (art-decor.org)
	
	Author: Gerrit Boers
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
   <xsl:output method="xml" exclude-result-prefixes="#all" encoding="UTF-8"/>
   <!-- characterset of input files, incorrect setting will result in error: Failed to read input file .... (java.nio.charset.MalformedInputException) - Input length = 1 -->
   <xsl:param name="charSet" select="'UTF-8'"/>
   <xsl:param name="releaseType" select="'Snapshot'"/>
   <xsl:template match="/list">
      <list>
         <!-- Get all descriptions -->
         <xsl:for-each select="file[@rows='description']">
            <xsl:variable name="descriptions" select="doc(concat('../XML/',@path,'/',@name,'.xml'))/descriptions"/>
            <xsl:for-each select="$descriptions/description">
               <xsl:copy-of select="."/>
            </xsl:for-each>
         </xsl:for-each>


         <!-- get text definitions -->
         <xsl:for-each select="file[@rows='textDefinition']">
            <xsl:variable name="textDefinitions" select="doc(concat('../XML/',@path,'/',@name,'.xml'))/textDefinitions"/>
            <xsl:for-each select="$textDefinitions/textDefinition">
               <xsl:copy-of select="."/>
            </xsl:for-each>
         </xsl:for-each>
         
         <!-- get annotations -->
         <xsl:for-each select="file[@rows='annotation']">
            <xsl:variable name="annotations" select="doc(concat('../XML/',@path,'/',@name,'.xml'))/annotations"/>
            <xsl:for-each select="$annotations/annotation[@typeId='480411000146103']">
               <xsl:copy-of select="."/>
            </xsl:for-each>
         </xsl:for-each>
         
         <!-- Get all  language definitions-->
         <xsl:for-each select="file[@rows='language']">
            <xsl:variable name="languages" select="doc(concat('../XML/',@path,'/',@name,'.xml'))/languages"/>
            <xsl:for-each select="$languages/language">
               <xsl:copy-of select="."/>
            </xsl:for-each>
         </xsl:for-each>

         
         <!-- If present, get all descriptions in DHD folder -->
         <xsl:if test="doc-available('../XML/RF2-DHD-refsets/descriptions.xml')">
            <xsl:variable name="descriptions" select="doc('../XML/RF2-DHD-refsets/descriptions.xml')/descriptions"/>
            <xsl:for-each select="$descriptions/description">
               <xsl:copy-of select="."/>
            </xsl:for-each>
         </xsl:if>

         <!-- If present, get all languages in DHD folder -->
         <xsl:if test="doc-available('../XML/RF2-DHD-refsets/languages.xml')">
            <xsl:variable name="languages" select="doc('../XML/RF2-DHD-refsets/languages.xml')/languages"/>
            <xsl:for-each select="$languages/language">
               <xsl:copy-of select="."/>
            </xsl:for-each>
         </xsl:if>
      </list>
   </xsl:template>
</xsl:stylesheet>
