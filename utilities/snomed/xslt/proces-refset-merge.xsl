<?xml version="1.0" encoding="UTF-8"?>
<!--
	Copyright (C) 2011-2013 Art Decor Expert Group
	
	Author: Gerrit Boers
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
-->

<!-- 

 java -Xmx8096m -jar saxon9.jar -t -s:refset-concept-merge.xml -xsl:proces-refset-merge.xsl -o:concepts_enhanced_en_GB.xml

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
   <xsl:output method="xml" indent="no" exclude-result-prefixes="#all" encoding="UTF-8"/>
   <xsl:key name="conceptId" match="concept" use="@conceptId"/>
   <xsl:key name="refsetId" match="refset" use="@referencedComponentId"/>
   <xsl:key name="simpleMapId" match="simpleMap" use="@referencedComponentId"/>
   <xsl:key name="complexMapId" match="complexMap" use="@referencedComponentId"/>
   <xsl:key name="extendedMapId" match="extendedMap" use="@referencedComponentId"/>
   <xsl:key name="correlatedMapId" match="correlatedMap" use="@referencedComponentId"/>
   <xsl:key name="correlatedExtendedMapId" match="correlatedExtendedMap" use="@referencedComponentId"/>
   <xsl:key name="correlatedOriginMapId" match="correlatedOriginMap" use="@referencedComponentId"/>
   <xsl:key name="associationId" match="associationReference" use="@referencedComponentId"/>
   <xsl:key name="attributeValueId" match="attributeValue" use="@referencedComponentId"/>
   <!-- variable for generated fileListSnapshot -->
   <xsl:variable name="fileListSnapshot" select="doc('../fileListSnapshot.xml')/list"/>
   
   <!-- variable for selecting preferred terms to use for qualifiers, default is US language refset -->
   <xsl:variable name="preferredLanguageRefsetId" select="'900000000000509007'"/>
   
   <xsl:variable name="preferredNameLanguageRefsetId">
      <xsl:choose>
         <!-- NL -->
         <xsl:when test="$fileListSnapshot/@releaseType='NL'">
            <xsl:value-of select="'31000146106'"/>
         </xsl:when>
         <!-- en US -->
         <xsl:otherwise>
            <xsl:value-of select="'900000000000509007'"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:variable>
   <!-- EN -->
<!--   <xsl:variable name="preferredNameLanguageRefsetId" select="'900000000000509007'"/>-->

   <xsl:template match="/refsets">
      <concepts>
         <xsl:for-each select="concept">
            <xsl:variable name="currentId" select="@conceptId"/>
            <concept conceptId="{$currentId}" effectiveTime="{@effectiveTime}" moduleId="{@moduleId}" definitionStatusId="{@definitionStatusId}" definitionStatus="{@definitionStatus}">
               <xsl:if test="@active = '1'">
                  <xsl:attribute name="active" select="'1'"/>
               </xsl:if>
               <xsl:for-each select="desc">
                  <xsl:copy-of select="."/>
               </xsl:for-each>
               <xsl:for-each select="definition">
                  <xsl:copy-of select="."/>
               </xsl:for-each>
               <xsl:for-each select="annotation">
                  <xsl:copy-of select="."/>
               </xsl:for-each>
               <xsl:copy-of select="ancestors"/>
               <xsl:for-each select="grp">
                  <xsl:copy-of select="."/>
               </xsl:for-each>
               <xsl:for-each select="dest">
                  <xsl:copy-of select="."/>
               </xsl:for-each>
               <refsets>
                  <xsl:for-each select="key('refsetId', $currentId)[@active='1']">
                     <xsl:variable name="refset">
                        <xsl:choose>
                           <xsl:when test="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredNameLanguageRefsetId][@active = '1']">
                              <xsl:value-of select="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredNameLanguageRefsetId][@active = '1']/text()"/>
                           </xsl:when>
                           <xsl:otherwise>
                              <xsl:choose>
                                 <xsl:when test="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredLanguageRefsetId][@active = '1']">
                                    <xsl:value-of select="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredLanguageRefsetId][@active = '1']/text()"/>
                                 </xsl:when>
                                 <xsl:otherwise>
                                    <xsl:value-of select="key('conceptId', @refsetId)/desc[@type = 'syn'][@active = '1'][1]/text()"/>
                                 </xsl:otherwise>
                              </xsl:choose>
                           </xsl:otherwise>
                        </xsl:choose>
                     </xsl:variable>
                     <refset id="{@id}" effectiveTime="{@effectiveTime}" active="{@active}" moduleId="{@moduleId}" refsetId="{@refsetId}" refset="{$refset}"/>
                  </xsl:for-each>
               </refsets>
               <maps>
                  <xsl:for-each select="key('simpleMapId', $currentId)[@active='1']">
                     <map>
                        <xsl:for-each select="@*[string-length(.) &gt; 0][not(name(.) = ('mapCategoryId', 'referencedComponentId'))]">
                           <xsl:attribute name="{name(.)}">
                              <xsl:value-of select="."/>
                           </xsl:attribute>
                        </xsl:for-each>
                        <xsl:attribute name="refset">
                           <xsl:choose>
                              <xsl:when test="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredNameLanguageRefsetId][@active = '1']">
                                 <xsl:value-of select="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredNameLanguageRefsetId][@active = '1'][1]/text()"/>
                              </xsl:when>
                              <xsl:otherwise>
                                 <xsl:value-of select="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][1]/text()"/>
                              </xsl:otherwise>
                           </xsl:choose>
                        </xsl:attribute>
                        <xsl:if test="string-length(@correlationId) &gt; 0">
                           <xsl:value-of select="key('conceptId', @correlationId)/desc[@type = 'pref'][@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][1]/text()"/>
                        </xsl:if>
                     </map>
                  </xsl:for-each>
                  <xsl:for-each select="key('complexMapId', $currentId)[@active='1']">
                     <map>
                        <xsl:for-each select="@*[string-length(.) &gt; 0][not(name(.) = ('mapCategoryId', 'referencedComponentId'))]">
                           <xsl:attribute name="{name(.)}">
                              <xsl:value-of select="."/>
                           </xsl:attribute>
                        </xsl:for-each>
                        <xsl:attribute name="refset">
                           <xsl:choose>
                              <xsl:when test="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredNameLanguageRefsetId][@active = '1']">
                                 <xsl:value-of select="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredNameLanguageRefsetId][@active = '1'][1]/text()"/>
                              </xsl:when>
                              <xsl:otherwise>
                                 <xsl:value-of select="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][1]/text()"/>
                              </xsl:otherwise>
                           </xsl:choose>
                        </xsl:attribute>
                        <xsl:if test="string-length(@correlationId) &gt; 0">
                           <xsl:attribute name="correlation">
                              <xsl:value-of select="key('conceptId', @correlationId)/desc[@type = 'pref'][@active = '1'][1]/text()"/>
                           </xsl:attribute>
                        </xsl:if>
                     </map>
                  </xsl:for-each>
                  <xsl:for-each select="key('extendedMapId', $currentId)[@active='1']">
                     <map>
                        <xsl:for-each select="@*[string-length(.) &gt; 0][not(name(.) = ('mapCategoryId', 'referencedComponentId'))]">
                           <xsl:attribute name="{name(.)}">
                              <xsl:value-of select="."/>
                           </xsl:attribute>
                        </xsl:for-each>
                        <xsl:attribute name="refsetId">
                           <xsl:value-of select="@refsetId"/>
                        </xsl:attribute>
                        <xsl:attribute name="refset">
                           <xsl:choose>
                              <xsl:when test="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredNameLanguageRefsetId][@active = '1']">
                                 <xsl:value-of select="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredNameLanguageRefsetId][@active = '1'][1]/text()"/>
                              </xsl:when>
                              <xsl:otherwise>
                                 <xsl:value-of select="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][1]/text()"/>
                              </xsl:otherwise>
                           </xsl:choose>
                        </xsl:attribute>
                        <xsl:if test="string-length(@correlationId) &gt; 0">
                           <xsl:attribute name="correlation">
                              <xsl:value-of select="key('conceptId', @correlationId)/desc[@type = 'pref'][@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][1]/text()"/>
                           </xsl:attribute>
                        </xsl:if>
                     </map>
                  </xsl:for-each>
                  <xsl:for-each select="key('correlatedMapId', $currentId)[@active='1']">
                     <map>
                        <xsl:for-each select="@*[string-length(.) &gt; 0][not(name(.) = ('mapCategoryId', 'referencedComponentId'))]">
                           <xsl:attribute name="{name(.)}">
                              <xsl:value-of select="."/>
                           </xsl:attribute>
                        </xsl:for-each>
                        <xsl:attribute name="refsetId">
                           <xsl:value-of select="@refsetId"/>
                        </xsl:attribute>
                        <xsl:attribute name="refset">
                           <xsl:choose>
                              <xsl:when test="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredNameLanguageRefsetId][@active = '1']">
                                 <xsl:value-of select="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredNameLanguageRefsetId][@active = '1'][1]/text()"/>
                              </xsl:when>
                              <xsl:otherwise>
                                 <xsl:value-of select="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][1]/text()"/>
                              </xsl:otherwise>
                           </xsl:choose>
                        </xsl:attribute>
                        <xsl:if test="string-length(@correlationId) &gt; 0">
                           <xsl:attribute name="mapCorrelation">
                              <xsl:value-of select="key('conceptId', @correlationId)/desc[@type = 'pref'][@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][1]/text()"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="string-length(@mapTargetQualifier) &gt; 0">
                           <xsl:attribute name="targetQualifier">
                              <xsl:value-of select="key('conceptId', @mapTargetQualifier)/desc[@type = 'pref'][@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][1]/text()"/>
                           </xsl:attribute>
                        </xsl:if>
                     </map>
                  </xsl:for-each>
                  <xsl:for-each select="key('correlatedExtendedMapId', $currentId)[@active='1']">
                     <map>
                        <xsl:for-each select="@*[string-length(.) &gt; 0][not(name(.) = ('mapCategoryId', 'referencedComponentId'))]">
                           <xsl:attribute name="{name(.)}">
                              <xsl:value-of select="."/>
                           </xsl:attribute>
                        </xsl:for-each>
                        <xsl:attribute name="refsetId">
                           <xsl:value-of select="@refsetId"/>
                        </xsl:attribute>
                        <xsl:attribute name="refset">
                           <xsl:choose>
                              <xsl:when test="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredNameLanguageRefsetId][@active = '1']">
                                 <xsl:value-of select="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredNameLanguageRefsetId][@active = '1'][1]/text()"/>
                              </xsl:when>
                              <xsl:otherwise>
                                 <xsl:value-of select="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][1]/text()"/>
                              </xsl:otherwise>
                           </xsl:choose>
                        </xsl:attribute>
                        <xsl:if test="string-length(@snomedCtSourceCodeToTargetMapCodeCorrelationValue) &gt; 0">
                           <xsl:attribute name="mapCorrelation">
                              <xsl:value-of select="key('conceptId', @snomedCtSourceCodeToTargetMapCodeCorrelationValue)/desc[@type = 'pref'][@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][1]/text()"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="string-length(@mapTargetQualifier) &gt; 0">
                           <xsl:attribute name="targetQualifier">
                              <xsl:value-of select="key('conceptId', @mapTargetQualifier)/desc[@type = 'pref'][@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][1]/text()"/>
                           </xsl:attribute>
                        </xsl:if>
                     </map>
                  </xsl:for-each>
                  <xsl:for-each select="key('correlatedOriginMapId', $currentId)[@active='1']">
                     <map>
                        <xsl:for-each select="@*[string-length(.) &gt; 0][not(name(.) = ('mapCategoryId', 'referencedComponentId'))]">
                           <xsl:attribute name="{name(.)}">
                              <xsl:value-of select="."/>
                           </xsl:attribute>
                        </xsl:for-each>
                        <xsl:attribute name="refsetId">
                           <xsl:value-of select="@refsetId"/>
                        </xsl:attribute>
                        <xsl:attribute name="refset">
                           <xsl:choose>
                              <xsl:when test="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredNameLanguageRefsetId][@active = '1']">
                                 <xsl:value-of select="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredNameLanguageRefsetId][@active = '1'][1]/text()"/>
                              </xsl:when>
                              <xsl:otherwise>
                                 <xsl:value-of select="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][1]/text()"/>
                              </xsl:otherwise>
                           </xsl:choose>
                        </xsl:attribute>
                        <xsl:if test="string-length(@attributeId) &gt; 0">
                           <xsl:attribute name="mapAttribute">
                              <xsl:value-of select="key('conceptId', @attributeId)/desc[@type = 'pref'][@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][1]/text()"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="string-length(@correlationId) &gt; 0">
                           <xsl:attribute name="mapCorrelation">
                              <xsl:value-of select="key('conceptId', @correlationId)/desc[@type = 'pref'][@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][1]/text()"/>
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="string-length(@contentOriginId) &gt; 0">
                           <xsl:attribute name="mapOrigin">
                              <xsl:value-of select="key('conceptId', @contentOriginId)/desc[@type = 'pref'][@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][1]/text()"/>
                           </xsl:attribute>
                        </xsl:if>
                     </map>
                  </xsl:for-each>
               </maps>
               <!-- Associations -->
               <xsl:for-each select="key('associationId', $currentId)[@active='1'][not(@refsetId='734138000' or @refsetId='734139008')]">
                  <xsl:variable name="refset">
                     <xsl:choose>
                        <xsl:when test="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredNameLanguageRefsetId][@active = '1']">
                           <xsl:value-of select="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredNameLanguageRefsetId][@active = '1']/text()"/>
                        </xsl:when>
                        <xsl:otherwise>
                           <xsl:choose>
                              <xsl:when test="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredLanguageRefsetId][@active = '1']">
                                 <xsl:value-of select="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredLanguageRefsetId][@active = '1']/text()"/>
                              </xsl:when>
                              <xsl:otherwise>
                                 <xsl:value-of select="key('conceptId', @refsetId)/desc[@type = 'syn'][@active = '1'][1]/text()"/>
                              </xsl:otherwise>
                           </xsl:choose>
                        </xsl:otherwise>
                     </xsl:choose>
                  </xsl:variable>
                  <xsl:variable name="target">
                     <xsl:choose>
                        <xsl:when test="key('conceptId', @targetComponentId)/desc[@type = 'pref'][@languageRefsetId = $preferredNameLanguageRefsetId][@active = '1']">
                           <xsl:value-of select="key('conceptId', @targetComponentId)/desc[@type = 'pref'][@languageRefsetId = $preferredNameLanguageRefsetId][@active = '1']/text()"/>
                        </xsl:when>
                        <xsl:otherwise>
                           <xsl:choose>
                              <xsl:when test="key('conceptId', @targetComponentId)/desc[@type = 'pref'][@languageRefsetId = $preferredLanguageRefsetId][@active = '1']">
                                 <xsl:value-of select="key('conceptId', @targetComponentId)/desc[@type = 'pref'][@languageRefsetId = $preferredLanguageRefsetId][@active = '1']/text()"/>
                              </xsl:when>
                              <xsl:otherwise>
                                 <xsl:value-of select="key('conceptId', @targetComponentId)/desc[@type = 'syn'][@active = '1'][1]/text()"/>
                              </xsl:otherwise>
                           </xsl:choose>
                        </xsl:otherwise>
                     </xsl:choose>
                  </xsl:variable>
                  <association effectiveTime="{@effectiveTime}" active="{@active}" refsetId="{@refsetId}" refset="{$refset}" targetComponentId="{@targetComponentId}" targetComponent="{$target}"/>
               </xsl:for-each>
               <!-- Attribute values -->
               <xsl:for-each select="key('attributeValueId', $currentId)[@active='1']">
                  <xsl:variable name="refset">
                     <xsl:choose>
                        <xsl:when test="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredNameLanguageRefsetId][@active = '1']">
                           <xsl:value-of select="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredNameLanguageRefsetId][@active = '1']/text()"/>
                        </xsl:when>
                        <xsl:otherwise>
                           <xsl:choose>
                              <xsl:when test="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredLanguageRefsetId][@active = '1']">
                                 <xsl:value-of select="key('conceptId', @refsetId)/desc[@type = 'pref'][@languageRefsetId = $preferredLanguageRefsetId][@active = '1']/text()"/>
                              </xsl:when>
                              <xsl:otherwise>
                                 <xsl:value-of select="key('conceptId', @refsetId)/desc[@type = 'syn'][@active = '1'][1]/text()"/>
                              </xsl:otherwise>
                           </xsl:choose>
                        </xsl:otherwise>
                     </xsl:choose>
                  </xsl:variable>
                  <xsl:variable name="value">
                     <xsl:choose>
                        <xsl:when test="key('conceptId', @valueId)/desc[@type = 'pref'][@languageRefsetId = $preferredNameLanguageRefsetId][@active = '1']">
                           <xsl:value-of select="key('conceptId', @valueId)/desc[@type = 'pref'][@languageRefsetId = $preferredNameLanguageRefsetId][@active = '1']/text()"/>
                        </xsl:when>
                        <xsl:otherwise>
                           <xsl:choose>
                              <xsl:when test="key('conceptId', @valueId)/desc[@type = 'pref'][@languageRefsetId = $preferredLanguageRefsetId][@active = '1']">
                                 <xsl:value-of select="key('conceptId', @valueId)/desc[@type = 'pref'][@languageRefsetId = $preferredLanguageRefsetId][@active = '1']/text()"/>
                              </xsl:when>
                              <xsl:otherwise>
                                 <xsl:value-of select="key('conceptId', @valueId)/desc[@type = 'syn'][@active = '1'][1]/text()"/>
                              </xsl:otherwise>
                           </xsl:choose>
                        </xsl:otherwise>
                     </xsl:choose>
                  </xsl:variable>
                  <attributeValue effectiveTime="{@effectiveTime}" active="{@active}" refsetId="{@refsetId}" refset="{$refset}" valueId="{@valueId}" value="{$value}"/>
<!--                  <attributeValue id="00000629-2697-5041-9438-3845fb99fe92" effectiveTime="20090731" active="1" moduleId="900000000000207008" refsetId="900000000000490003" referencedComponentId="2608473011" valueId="900000000000495008"/>-->
               </xsl:for-each>
            </concept>
         </xsl:for-each>
      </concepts>
   </xsl:template>
</xsl:stylesheet>
