# put the input directory listing into file.txt
ls -A1 input > files.txt

echo "----- Create fileListSnapshot.xml -----"
java -jar saxon9.jar -t -s:baseFileListSnapshot.xml -xsl:xslt/createFileListSnapshot.xsl -o:fileListSnapshot.xml

echo "----- Convert RF2 files to XML -----"
java -Xmx8096m -jar saxon9.jar -t -s:fileListSnapshot.xml -xsl:xslt/tabDelimited-2-xml.xsl -o:dummy.xml

echo "----- Convert DHD refset text files (DT, VT) to refset xml -----"
java -jar saxon9.jar -t -s:fileListSnapshot.xml -xsl:xslt/dhd-text-to-refsets.xsl -o:XML/RF2-DHD-refsets/refsets.xml

echo "----- Convert DHD-ICD10 mapping text file to mapping xml -----"
java -jar saxon9.jar -t -s:fileListSnapshot.xml -xsl:xslt/dhd-icd10-text-to-mapping.xsl -o:meta/DHD-ICD10-mapping.xml

echo "----- Merge Descriptions, Definitions & Languages -----"
java -Xmx12096m -jar saxon9.jar -t -s:fileListSnapshot.xml -xsl:xslt/merge-language-description-definition.xsl -o:language-description-definition.-merge.xml

echo "----- Generate denormalized descriptions & definitions file -----"
java -Xmx12096m -jar saxon9.jar -t -s:language-description-definition.-merge.xml -xsl:xslt/denormalize-descriptions.xsl -o:descriptions_definitions_denormalized.xml

echo "----- Merge Concepts, Descriptions, Languages and Relationships -----"
java -Xmx12096m -jar saxon9.jar -t -s:fileListSnapshot.xml -xsl:xslt/merge-concepts-descriptions-relations.xsl -o:merge_en-US.xml

echo "----- Put descriptions and relationship in concept elements -----"
java -Xmx12096m -jar saxon9.jar -t -s:merge_en-US.xml -xsl:xslt/proces-merge.xsl -o:merge_en-US_processed.xml

echo "----- Lookup type names and relationship names for all relationships -----"
java -Xmx12096m -jar saxon9.jar -t -s:merge_en-US_processed.xml -xsl:xslt/proces-relationships.xsl -o:concepts_en-US.xml

echo "----- Group relationships  & Sort relationships by type and name, and add string-lengths -----"
java -Xmx12096m -jar saxon9.jar -t -s:concepts_en-US.xml -xsl:xslt/sort-concepts-add-closure.xsl -o:concepts_en-US_sorted_closed.xml

echo "----- Merge Refsets into file -----"
java -Xmx12096m -jar saxon9.jar -t -s:fileListSnapshot.xml -xsl:xslt/merge-refsets.xsl -o:refset-concept-merge.xml

echo "----- Process refset merge -----"
java -Xmx12096m -jar saxon9.jar -t -s:refset-concept-merge.xml -xsl:xslt/proces-refset-merge.xsl -o:concepts_en-US_sorted_closed_enhanced.xml

echo "----- Convert to DECOR codesystem -----"
java -Xmx12096m -jar saxon9.jar -t -s:concepts_en-US_sorted_closed_enhanced.xml -xsl:xslt/SNOMED-to-CADTS-codeSystem.xsl -o:SNOMED-decor.xml
