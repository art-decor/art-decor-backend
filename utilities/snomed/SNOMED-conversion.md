# SNOMED xml files for AD2 and AD3 CADTS

To use SNOMED in the ART-DECOR terminology browsers (AD2 and AD3) it is necessary to convert the RF2 text files into one large xml file. The conversion denormalises the file so that each concept in the file contains all information of the concept and the names of the directly related concepts (parents, children and other relations). Concepts also contain a list ids of all ancestors (transitive closure).

The SNOMED xml file is generated through a series of xml transformations starting with the creation of a file list (fileListSnapshot.xml) from a listing of the input directory in combination with the base file list (baseFileListSnapshot.xml). A single shell script (convert-files.sh) takes care of all the necessary actions and transformations.

#### Directory structure

The following file are in the root:

- baseFileListSnapshot.xml
  Contains the list of files to convert to xml both for the International edition and Dutch edition, which files will be converted depends on the Snomed release in the input directory (International or Dutch). This file also contains listings for external refset releases.
- convert-files.sh
  Shell script containing all necessary actions and transformations.
- SNOMED-conversion.md
  This file.
- saxon9.jar
  Saxon xslt processor. Currently required in this location, change path in convert-files.sh if another location is to be used.

The following directories are in the root:

- input
  Contains all input files (directories). The RF2-DHD-refsets directory contains the files that define the DHD DT and VT reference sets (names, ids and descriptions).
- xslt
  Contains the xml stylesheets for transfomations
- meta
  Output directory for DHD-ICD10 mapping file for use in AD2 browser. This file should be placed in the meta directory of the AD2 xar package.

**Important** If there are new releases for any external refset or mapping the baseFileListSnapshot.xml must be edited manually. This cannot be done automatically because the release date and the dates of the individual file do not match so there's no way to automatically extract the dates from the release. The dates of the main Snomed release are extracted automatically.

 

#### Detailed description of the files involved

###### 1. createFileListSnapshot.xsl

Uses the baseFileListSnapshot.xml file and the input directory listing (files.txt) to create the fileListSnapshot.xml file. All necessary dates and timestamps are extracted from the directory listing and put in the fileListSnapshot.xml file. If the main Snomed release is the Dutch release then additional refsets and mapping files will be included (external Snomed and DHD)

###### 2. tabDelimited-2-xml.xsl

Creates an xml directory and transforms the tab delimited text files to xml files using the definitions in the generated fileListSnapshot.xml for the root and row names.

###### 3. dhd-text-to-refsets.xsl

Creates DT and VT refsets from text files in the input directory. Text files must be named DT-yyyymmdd.txt and VT-yyyymmdd.txt. The format must be tab delimited, no headers in the first row and the first column must contain the concept id.

###### 4. dhd-icd10-text-to-mapping.xsl

Creates the DHD ICD10 mapping xml file and put it in the meta directory. This is not an official Snomed mapping hence it is not included in the main xml file but handled separately in AD2. It is currently not supported in AD3.

###### 5. merge-language-description-definition.xsl

Merges all descriptions, text definitions, annotations* and language definitions into one large xml file for further processing.

\* The Netherlands edition uses annotations for patient friendly descriptions, only annotations of type '480411000146103 Explanation of at most four sentences on reading level B1, used to explain medical concepts to patients' are currently included.

###### 6. denormalize-descriptions.xsl

Walks through the language definitions and creates a large xml file containing elements for descriptions, definitions and annotations.

###### 7. merge-concepts-descriptions-relations.xsl

Merges concepts, descriptions, definitions, annotations en relations into one large xml file for further processing.

###### 8. proces-merge.xsl

Creates one large file with concepts containing descriptions, definitions, annotations en relations.

###### 9. proces-relationships.xsl

Puts type names and relationship names in relations. The AD2 version has a single language for relationship names. The stylesheet uses fileListSnapshot.xml to determine the release (International or Dutch) and selects the appropriate language.

###### 10. sort-concepts-add-closure.xsl

Sorts and groups relationships and calculates transitive closure.

###### 11. merge-resets.xsl

Merges refset and mapping xml files with concepts, uses fileListSnapshot.xml for list of refsets and mappings.

###### 12. proces-refset-merge.xsl

Looks up names for refsets, mappings etc. The AD2 version has a single language for names. The stylesheet uses fileListSnapshot.xml to determine the release (International or Dutch) and selects the appropriate language.

###### 13. SNOMED-to-CADTS-codeSystem.xsl

Converts the AD2 SNOMED file to AD3 CADTS format. Looks up multi-lingual descriptions for relationships. Based on the two letter language code and language reference set membership a four letter language code is generated for all descriptions, definitions and names.

The stylesheet uses fileListSnapshot.xml to determine the version (release date-time)



