<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    exclude-result-prefixes="xs xd"
    version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Mar 23, 2015</xd:p>
            <xd:p><xd:b>Author:</xd:b> ahenket</xd:p>
            <xd:p><xd:b>Purpose:</xd:b> Convert a directory of HL7 V3 schemas to DECOR templates</xd:p>
        </xd:desc>
    </xd:doc>
    
    <xsl:output indent="yes" omit-xml-declaration="yes"/>
    
    <xd:doc>
        <xd:desc>Unix path name to the directory</xd:desc>
    </xd:doc>
    <xsl:param name="schemaInputPath" select="'/Users/ahenket/Documents/Nictiz/AORTA/trunk/XML/schemas/'"/>
    <xd:doc>
        <xd:desc>Include any wrapper schemas in the directory. Default is false()</xd:desc>
    </xd:doc>
    <xsl:param name="includeWrappers" select="false()" as="xs:boolean"/>
    <xd:doc>
        <xd:desc>Include any interaction schemas in the directory. Default is false()</xd:desc>
    </xd:doc>
    <xsl:param name="includeInteractions" select="false()" as="xs:boolean"/>
    <xd:doc>
        <xd:desc>Base OID for the generated templates. Required</xd:desc>
    </xd:doc>
    <xsl:param name="templateBaseId" as="xs:string" select="'2.16.840.1.113883.2.4.6.10.100'"/>
    <xd:doc>
        <xd:desc>Base leaf node for the generated templates. Required</xd:desc>
    </xd:doc>
    <xsl:param name="templateStart" as="xs:integer" select="1"/>
    <xd:doc>
        <xd:desc>Fixed date time stamp as template/@effectiveDate for the generated templates. Required</xd:desc>
    </xd:doc>
    <xsl:param name="templateEffectiveDate" as="xs:dateTime?" select="xs:dateTime('2015-07-02T00:00:00')"/>
    <xd:doc>
        <xd:desc>Base OID for the generated template elements/attributes. Optional. Not implemented yet</xd:desc>
    </xd:doc>
    <xsl:param name="elementBaseId" as="xs:string" required="no"/>
    <xd:doc>
        <xd:desc>Base language cc-LL for the generated templates. Default is nl-NL</xd:desc>
    </xd:doc>
    <xsl:param name="language" as="xs:string" select="'nl-NL'"/>
    <xd:doc>
        <xd:desc>Base namespace prefix generated template elements/attributes. Required</xd:desc>
    </xd:doc>
    <xsl:param name="namespacePrefix" select="'hl7:'" as="xs:string"/>
    <xd:doc>
        <xd:desc>Default classification level for the generated templates. Optional. Default is ()</xd:desc>
    </xd:doc>
    <xsl:variable name="classificationLevel" as="xs:string?"/>
    <xsl:param name="resources" as="element(resources)*">
        <resources xml:lang="en-US">
            <defaultvalue>Default value</defaultvalue>
        </resources>
        <resources xml:lang="nl-NL">
            <defaultvalue>Standaardwaarde</defaultvalue>
        </resources>
        <resources xml:lang="de-DE">
            <defaultvalue>Default value</defaultvalue>
        </resources>
    </xsl:param>
    
    <xsl:variable name="effectiveDate">
        <xsl:choose>
            <xsl:when test="empty($templateEffectiveDate)">
                <xsl:value-of select="concat(substring(string(current-date()),1,10),'T00:00:00')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="substring(string($templateEffectiveDate),1,19)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="InfrastructureRootAttributes" as="element()">
        <xs:attributeGroup name="InfrastructureRootAttributes">
            <xs:attribute name="nullFlavor" type="NullFlavor" use="optional" xmlns="urn:hl7-org:v3"/>
        </xs:attributeGroup>
    </xsl:variable>
    <xsl:variable name="InfrastructureRootElements">
        <xs:group name="InfrastructureRootElements">
            <xs:sequence>
                <xs:element name="realmCode" type="CS" minOccurs="0" maxOccurs="unbounded" xmlns="urn:hl7-org:v3"/>
                <xs:element name="typeId" type="all.InfrastructureRoot.typeId" minOccurs="0" maxOccurs="1" xmlns="urn:hl7-org:v3"/>
                <xs:element name="templateId" type="II" minOccurs="0" maxOccurs="unbounded" xmlns="urn:hl7-org:v3"/>
            </xs:sequence>
        </xs:group>
    </xsl:variable>
    
    <xsl:variable name="schemaCollection" as="element(wrap)">
        <xsl:variable name="pattern">
            <xsl:choose>
                <xsl:when test="$includeInteractions">*</xsl:when>
                <xsl:otherwise>*_MT*</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <wrap>
            <xsl:for-each select="collection(iri-to-uri(concat($schemaInputPath,'?select=',$pattern,'.xsd;recurse=no')))">
                <xsl:sort select="tokenize(document-uri(.), '/')[last()]"/>
                <xsl:variable name="fileName" select="tokenize(document-uri(.), '/')[last()]"/>
                <xsl:choose>
                    <xsl:when test="starts-with($fileName,'MCCI') and $includeWrappers">
                        <xsl:copy-of select="xs:schema"/>
                    </xsl:when>
                    <xsl:when test="starts-with($fileName,'MCAI') and $includeWrappers">
                        <xsl:copy-of select="xs:schema"/>
                    </xsl:when>
                    <xsl:when test="starts-with($fileName,'MFMI') and $includeWrappers">
                        <xsl:copy-of select="xs:schema"/>
                    </xsl:when>
                    <xsl:when test="starts-with($fileName,'QUQI') and $includeWrappers">
                        <xsl:copy-of select="xs:schema"/>
                    </xsl:when>
                    <xsl:when test="not(starts-with($fileName,'REPC_MT004001NL_'))">
                        <xsl:copy-of select="xs:schema"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:for-each>
        </wrap>
    </xsl:variable>
    
    <xsl:template match="/">
        <xsl:variable name="templates" as="item()*">
        <xsl:for-each select="$schemaCollection/xs:schema">
            <xsl:apply-templates select="."/>
        </xsl:for-each>
        </xsl:variable>
        
        <xsl:apply-templates select="$templates" mode="replaceNameWithId">
            <xsl:with-param name="templates" select="$templates"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="element | include" mode="replaceNameWithId">
        <xsl:param name="templates"/>
        
        <xsl:copy>
            <xsl:for-each select="@*">
                <xsl:choose>
                    <xsl:when test="name()=('contains','ref')">
                        <xsl:variable name="ref" select="."/>
                        <xsl:attribute name="contains" select="$templates[@name=$ref]/@id"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:copy-of select="."/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
            <xsl:apply-templates select="node()" mode="replaceNameWithId">
                <xsl:with-param name="templates" select="$templates"/>
            </xsl:apply-templates>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="node()" mode="replaceNameWithId">
        <xsl:param name="templates"/>
        
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="node()" mode="replaceNameWithId">
                <xsl:with-param name="templates" select="$templates"/>
            </xsl:apply-templates>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="xs:schema/xs:element"/>
    
    <xsl:template match="xs:schema/xs:complexType | xs:schema/xs:group">
        <xsl:variable name="classification">
            <xsl:variable name="fileName" select="tokenize(document-uri(.), '/')[last()]"/>
            <xsl:choose>
                <xsl:when test="starts-with($fileName,'MCCI')">messagelevel</xsl:when>
                <xsl:when test="starts-with($fileName,'MCAI')">controlactlevel</xsl:when>
                <xsl:when test="starts-with($fileName,'MFMI')">controlactlevel</xsl:when>
                <xsl:when test="starts-with($fileName,'QUQI')">controlactlevel</xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$classificationLevel"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="seq" select="count(
                preceding-sibling::xs:group|
                preceding-sibling::xs:complexType|
                ancestor::xs:schema/preceding-sibling::xs:schema/xs:group|
                ancestor::xs:schema/preceding-sibling::xs:schema/xs:complexType
            )+$templateStart"/>
        <xsl:variable name="templateId" select="concat($templateBaseId,'.',$seq)"/>
        <xsl:variable name="templateName" select="@name"/>
            <!--<xsl:choose>
                <xsl:when test="self::xs:group">
                    <xsl:value-of select="concat((tokenize(../xs:complexType/@name,'\.')[1])[1],'.',@name)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@name"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>-->
        
        <xsl:comment> <xsl:value-of select="@name"/> </xsl:comment>
        <template id="{$templateId}" effectiveDate="{$effectiveDate}" name="{$templateName}" displayName="{$templateName}" statusCode="draft">
            <xsl:if test="string-length($classification)>0">
                <classification type="{$classification}" format="hl7v3xml1"/>
            </xsl:if>
            <relationship type="DRIV" model="{replace(tokenize(@name,'\.')[1],'_MT','_RM')}"/>
            <relationship type="DRIV" model="{tokenize(@name,'\.')[1]}"/>
            <xsl:if test="xs:annotation/xs:documentation">
                <desc language="{$language}">
                    <xsl:value-of select="xs:annotation/xs:documentation"/>
                </desc>
            </xsl:if>
            <xsl:apply-templates select="xs:attribute"/>
            <xsl:if test="xs:attributeGroup[@name='InfrastructureRootAttributes'] and not(xs:attribute[@name='nullFlavor'])">
                <xsl:apply-templates select="$InfrastructureRootAttributes"/>
            </xsl:if>
            <xsl:apply-templates select="xs:sequence/* | xs:choice"/>
        </template>
    </xsl:template>
    
    <!--
        <xs:attribute name="type" type="Classes" default="Observation"/>
        <xs:attribute name="classCode" type="ActClass" use="optional" default="OBS"/>
        <xs:attribute name="moodCode" type="ActMood" use="optional" default="EVN"/>
    -->
    <xsl:template match="xs:complexType/xs:attribute">
        <xsl:choose>
            <xsl:when test="@name=('type')"/>
            <xsl:when test="@name=('typeId','typeID')"/>
            <xsl:when test="@name=('realmCode')"/>
            <xsl:when test="@name=('templateId')"/>
            <xsl:otherwise>
                <attribute name="{@name}">
                    <xsl:if test="@fixed | @default">
                        <xsl:attribute name="value" select="@fixed |@default"/>
                    </xsl:if>
                    <xsl:if test="@type[matches(.,'^[a-z]+$')]">
                        <xsl:attribute name="datatype" select="@type"/>
                    </xsl:if>
                    <xsl:attribute name="isOptional" select="not(@use='required')"/>
                    <!--<xsl:if test="@default">
                        <desc language="{$language}">
                            <xsl:value-of select="$resources[@xml:lang=$language]/*[name()='defaultvalue']"/>
                            <xsl:text> = "</xsl:text>
                            <xsl:value-of select="@default"/>
                            <xsl:text>"</xsl:text>
                        </desc>
                    </xsl:if>-->
                    <xsl:choose>
                        <xsl:when test="@type[not(matches(.,'^[a-z]+$'))] and not(@fixed | @default)">
                            <vocabulary valueSet="{@type}"/>
                        </xsl:when>
                        <xsl:when test="@name='nullFlavor'">
                            <vocabulary valueSet="NullFlavor"/>
                        </xsl:when>
                    </xsl:choose>
                </attribute>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!--
        <xs:element name="ObservationIntolerance" type="COCT_MT120104NL.ObservationIntolerance"
            minOccurs="1" maxOccurs="1"/>
    -->
    <xsl:template match="xs:complexType//xs:element | xs:group//xs:element">
        <xsl:choose>
            <xsl:when test="@name=('type')"/>
            <xsl:when test="@name=('typeId','typeID')"/>
            <xsl:when test="@name=('realmCode')"/>
            <xsl:otherwise>
                <element name="{$namespacePrefix}{@name}">
                    <xsl:choose>
                        <xsl:when test=".[@name='contactEntityChoice']/ancestor::xs:complexType[@name=('COCT_MT030000.ContactParty')]">
                            <xsl:attribute name="contains" select="'COCT_MT030000.EntityChoice'"/>
                            <xsl:attribute name="flexibility" select="$effectiveDate"/>
                        </xsl:when>
                        <xsl:when test=".[@name='guarantor']/ancestor::xs:complexType[@name=('COCT_MT030000.Guarantor')]">
                            <xsl:attribute name="contains" select="'COCT_MT030000.EntityChoice'"/>
                            <xsl:attribute name="flexibility" select="$effectiveDate"/>
                        </xsl:when>
                        <xsl:when test=".[@name='guardian']/ancestor::xs:complexType[@name=('COCT_MT030000.Guardian')]">
                            <xsl:attribute name="contains" select="'COCT_MT030000.EntityChoice'"/>
                            <xsl:attribute name="flexibility" select="$effectiveDate"/>
                        </xsl:when>
                        <xsl:when test="xs:* except xs:annotation">
                            <xsl:message>
                                <xsl:text>Document </xsl:text>
                                <xsl:value-of select="tokenize(document-uri(.), '/')[last()]"/>
                                <xsl:text> </xsl:text>
                                <xsl:value-of select="ancestor::xs:complexType/name() , ancestor::xs:group/name()"/>
                                <xsl:text> </xsl:text>
                                <xsl:value-of select="ancestor::xs:complexType/@name | ancestor::xs:group/@name"/>
                                <xsl:text> bevat een element met inline gedefinieerde inhoud die we niet ondersteunen: </xsl:text>
                                <xsl:text>&#10;</xsl:text>
                                <xsl:copy-of select="."/>
                            </xsl:message>
                        </xsl:when>
                        <xsl:when test="@type[contains(.,'.')]">
                            <xsl:attribute name="contains" select="@type"/>
                            <xsl:attribute name="flexibility" select="$effectiveDate"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="datatype" select="@type"/>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:if test="not(parent::xs:choice[not(@minOccurs)])">
                        <xsl:attribute name="minimumMultiplicity">
                            <xsl:choose>
                                <xsl:when test="@minOccurs">
                                    <xsl:value-of select="@minOccurs"/>
                                </xsl:when>
                                <xsl:otherwise>1</xsl:otherwise>
                            </xsl:choose>
                        </xsl:attribute>
                    </xsl:if>
                    <xsl:choose>
                        <xsl:when test="@maxOccurs='unbounded'">
                            <xsl:attribute name="maximumMultiplicity" select="'*'"/>
                        </xsl:when>
                        <xsl:when test="@maxOccurs">
                            <xsl:attribute name="maximumMultiplicity" select="@maxOccurs"/>
                        </xsl:when>
                    </xsl:choose>
                </element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="xs:complexType//xs:choice | xs:group//xs:choice">
        <xsl:choose>
            <xsl:when test="parent::xs:choice">
                <xsl:apply-templates select="*"/>
            </xsl:when>
            <xsl:otherwise>
                <choice>
                    <xsl:choose>
                        <xsl:when test="not(@minOccurs)">
                            <xsl:attribute name="minimumMultiplicity" select="'1'"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="minimumMultiplicity" select="@minOccurs"/>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:choose>
                        <xsl:when test="not(@maxOccurs)">
                            <xsl:attribute name="maximumMultiplicity" select="'1'"/>
                        </xsl:when>
                        <xsl:when test="@maxOccurs='unbounded'">
                            <xsl:attribute name="maximumMultiplicity" select="'*'"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="minimumMultiplicity" select="@maxOccurs"/>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:apply-templates select="*"/>
                </choice>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="xs:complexType//xs:group[@ref='InfrastructureRootElements']">
        <xsl:apply-templates select="$InfrastructureRootElements/xs:sequence/*"/>
    </xsl:template>
    
    <xsl:template match="xs:complexType//xs:group[@ref][not(@ref='InfrastructureRootElements')]">
        <include ref="{@ref}">
            <xsl:attribute name="flexibility" select="$effectiveDate"/>
            <xsl:choose>
                <xsl:when test="@minOccurs">
                    <xsl:attribute name="minimumMultiplicity" select="@minOccurs"/>
                </xsl:when>
            </xsl:choose>
            <xsl:choose>
                <xsl:when test="@maxOccurs='unbounded'">
                    <xsl:attribute name="maximumMultiplicity" select="'*'"/>
                </xsl:when>
                <xsl:when test="@maxOccurs">
                    <xsl:attribute name="maximumMultiplicity" select="@maxOccurs"/>
                </xsl:when>
            </xsl:choose>
        </include>
    </xsl:template>
    
    <xsl:template match="text()"/>
</xsl:stylesheet>