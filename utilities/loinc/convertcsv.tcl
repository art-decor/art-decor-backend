#!/usr/bin/tclsh

proc getopt {_argv name {_var ""} {default ""}} {
     upvar 1 $_argv argv $_var var
     set pos [lsearch -regexp $argv ^$name]
     if {$pos>=0} {
         set to $pos
         if {$_var ne ""} {
             set var [lindex $argv [incr to]]
         }
         set argv [lreplace $argv $pos $to]
         return 1
     } else {
         if {[llength [info level 0]] == 5} {set var $default}
         return 0
     }
}
# findFiles
# basedir - the directory to start looking in
# pattern - A pattern, as defined by the glob command, that the files must match
proc findFiles {directory pattern} {

    # Fix the directory name, this ensures the directory name is in the
    # native format for the platform and contains a final directory seperator
    set directory [string trimright [file join [file normalize $directory] { }]]

    # Starting with the passed in directory, do a breadth first search for
    # subdirectories. Avoid cycles by normalizing all file paths and checking
    # for duplicates at each level.

    set directories [list $directory]
    set parents $directory
    while {[llength $parents] > 0} {

        # Find all the children at the current level
        set children [list]
        foreach parent $parents {
            set children [concat $children [glob -nocomplain -type {d r} -path $parent *]]
        }

        # Normalize the children
        set length [llength $children]
        for {set i 0} {$i < $length} {incr i} {
            lset children $i [string trimright [file join [file normalize [lindex $children $i]] { }]]
        }

        # Make the list of children unique
        set children [lsort -unique $children]

        # Find the children that are not duplicates, use them for the next level
        set parents [list]
        foreach child $children {
            if {[lsearch -sorted $directories $child] == -1} {
                lappend parents $child
            }
        }

        # Append the next level directories to the complete list
        set directories [lsort -unique [concat $directories $parents]]
    }

    # Get all the files in the passed in directory and all its subdirectories
    set result [list]
    foreach directory $directories {
        set result [concat $result [glob -nocomplain -type {f r} -path $directory -- $pattern]]
    }

    # Normalize the filenames
    set length [llength $result]
    for {set i 0} {$i < $length} {incr i} {
        lset result $i [file normalize [lindex $result $i]]
    }

    # Return only unique filenames
    return [lsort -unique $result]
}
proc readWrite { fin finencoding fout version effectiveDate debug } {
    set fi [open "${fin}" r]
    set fo [open "${fout}" w+]
    fconfigure $fi -encoding $finencoding
    fconfigure $fo -encoding utf-8
    
    # check for byte order marker (65279), introduced in version LOINC 2.58
    if { [scan [read $fi 1] %c] != 65279 } { seek $fi 0 }
    
    puts $fo "<root version=\"$version\" effectiveDate=\"$effectiveDate\">"
    set headerline      [gets $fi]
    
    # sepChar by default is a comma, but when exporting from Excel it is usually a semicolon
    # assume that if the header line has more than 5 columns based on semicolon that the
    # sepChar probably is a semicolon
    if {[llength [split $headerline ";"]] > 5} { set sepChar ";" } else { set sepChar "," }
    
    set headercolumns   [parseLine $fi [split $headerline ""] $sepChar false false "" {} $debug]
    while {[gets $fi line] >= 0} {
        if {[string length $line] > 0} {
            #puts $fo "    <row>[esc $line]</row>"
            puts $fo "    <row>"
            set columns     [parseLine $fi [split $line ""] $sepChar false false "" {} $debug]
            set collength   [llength $headercolumns]
            for {set i 0} {$i < $collength} {incr i} {
                set colname     [lindex $headercolumns $i]
                set colvalue    [lindex $columns $i]
                if {[string length $colvalue]>0} {
                    puts $fo "        <elem name=\"${colname}\">[esc ${colvalue}]</elem>"
                    #if {$colname == "RELATEDNAMES2"} {
                    #    #RELATEDNAMES2 does not need escaping as that was already done in 2.54
                    #    puts $fo "        <elem name=\"${colname}\">${colvalue}</elem>"
                    #} else {
                    #    puts $fo "        <elem name=\"${colname}\">[esc ${colvalue}]</elem>"
                    #}
                }
            }
            puts $fo "    </row>"
            #puts stdout "$line"
            #puts stdout "$columns"
            #exit
        }
    }
    puts $fo "</root>"
    
    close $fi
    close $fo
}
# "1006-6","","","","","","","","Erz--Ig, Verkleidung; ; Coombs (spezifische reagenz)","",""
# or (Excel export default)
# 10142;13361-1;Semen Analysis Pnl;10142;1;13361-1;Semen Analysis Pnl;;;;;;;;0;;Q;;;;;;;;;;;;
proc parseLine { fi chars {sepChar ,} {escapemode false} {datamode false} column {list {}} {debug false} } {
    while {[llength $chars] > 0} {
        set char        [lindex $chars 0]
        set nextchar    [lindex $chars 1]
        set chars       [lrange $chars 1 end]
        if {$debug == "true"} {puts stdout "Found $char with escapemode $escapemode, and datamode $datamode ..."}
        switch -exact $char {
            \" {
                if {$datamode == "false" || $datamode == "true"} {
                    set datamode "truequote"
                } elseif {$escapemode == "true"} {
                    append column $char
                    set escapemode "false"
                } elseif {$nextchar == "\""} {
                    set escapemode "true"
                } else {
                    lappend list $column
                    set column ""
                    set datamode "false"
                }
            }
            default {
                if {$char == $sepChar} {
                    if {$datamode == "truequote"} {
                        append column $char
                    } elseif {$datamode == "true"} {
                        lappend list $column
                        set column ""
                    } else {
                        set datamode "true"
                    }
                    set escapemode "false"
                } else {
                    if {$datamode == "false"} {
                        set datamode "true"
                    }
                    set escapemode "false"
                    
                    append column $char
                }
            }
        }
        if {$debug == "true"} {puts stdout "         result: column \"$column\", escapemode $escapemode, and datamode $datamode ..."}
    }
    
    #if {[regexp $sepChar $column]} { exit -1 }
    
    # Did we end with an unquoted value, then append that last column value
    if {$datamode == "true"} {
        lappend list $column
        set column ""
    }
    
    # Are we at the end of a line that ends with a column in quotes and an embedded newline?
    # Then continue reading the next line from the file as if part of the current
    if {$datamode == "truequote"} {
        #puts stdout "    [clock format [clock seconds] -format "%Y-%m-%d %T"] +++ Found embedded newline. Getting extra line."
        append column "\n"
        if {[gets $fi line] >= 0} {
            set list [parseLine $fi [split $line ""] $sepChar $escapemode $datamode $column $list $debug]
        } else {
            puts stdout "    [clock format [clock seconds] -format "%Y-%m-%d %T"] +++ We were not done parsing a line with an embedded newline but the file had no more data."
            exit 1
        }
    }
    
    return $list
}

#
# regsub {\&(?!(lt)|(gt)|(amp)|(quot)|(apos)|(#[\dA-Fa-f]{2});)} "aa&lt;bb&amp;cc&#0d;dd&s" "\\&amp;"
#
# yields: aa&lt;bb&amp;cc&#0d;dd&amp;s
#
proc esc { str } {
    regsub -all {\&(?!(lt)|(gt)|(amp)|(quot)|(apos)|(#[\dA-Fa-f]{2});)} $str "\\&amp;" str
    regsub -all {<}  $str "\\&lt;" str
    regsub -all {>}  $str "\\&gt;" str
    
    return $str
}

set usage "- A simple script to parse lines into xml rows.\nOptions:"
set parameters {
    {input      ""      "Directory with LOINC csv files"}
    {encoding   "utf-8" "Input file encoding. Default is utf-8. Output file encoding is always utf-8"}
    {output     ""      "Directory for the LOINC xml output files"}
    {debug      "false" "Activate (an abundance of) output"}
}

getopt argv -debug options(debug) "false"
getopt argv -input options(input) ""
getopt argv -output options(output) "LOINC-output"
getopt argv -encoding options(encoding) "utf-8"
getopt argv -version options(version) ""
getopt argv -effectiveDate options(effectiveDate) ""

puts stdout "    [clock format [clock seconds] -format "%Y-%m-%d %T"] Input=$options(input) - encoding $options(encoding) Output=$options(output) \
version=$options(version) effectiveDate=$options(effectiveDate) debug=$options(debug)\n"

set inputFiles  [findFiles $options(input) *.csv]
set total       [llength $inputFiles]

if {![file isdirectory $options(output)]} {
    puts stdout "    Creating output directory $options(output) …\n"
    file mkdir $options(output)
}
for {set i 0} {$i < $total} {incr i} {
    set file    [lindex $inputFiles $i]
    set output  "$options(output)/[file rootname [file tail $file]].xml"
    
    puts stdout "    [clock format [clock seconds] -format "%Y-%m-%d %T"] ([expr $i + 1]/$total) Processing [regsub "[pwd]/" $file ""] into $output …"
    
    readWrite $file $options(encoding) $output $options(version) $options(effectiveDate) $options(debug)
}

puts stdout "    [clock format [clock seconds] -format "%Y-%m-%d %T"] Done"