# coding=utf-8
from lxml import etree
import os
import csv
import pandas as pd
import settings


def lineToXml(line, headerlist, parent):
    # Convert CSV line to XML
    for index, col in enumerate(line):
        # Skip empty columns
        if (len(col) > 0):
            # Replace element names if wanted: <somename name=',...'>, else make <elem name=',...'>
            if (headerlist[index] in settings.replaces):
                tag = settings.replaces[headerlist[index]]
            else:
                tag = 'elem'
            if (headerlist[index] in ['SHORTNAME', 'LONG_COMMON_NAME']):
                elem = etree.Element(tag, length=str(len(col)), count=str(len(col.split(' '))), name=headerlist[index])
            else:
                elem = etree.Element(tag, name=headerlist[index])
            elem.text = col
            parent.append(elem)


def loincXml():
    # First we build a dictionary of language variants:
    # Key of outer dict is LOINC ID
    # {'11488-4':
    #   Key of inner dict is language. Value of key is the list of LOINC columns
    #     {'de_AT': ['11488-4', 'Befund', 'Ergebnis', ...]}
    #     {'nl_NL': ['11488-4', ...]}
    # ,
    # '11490-0':
    #     {'de_AT': ['11490-0', 'Entlassungsbrief Ärztlich',
    # ...
    print('Reading LinguisticVariants...')
    langfileheader = []
    langdict = {}
    for variant in os.listdir(os.path.join(settings.base, settings.lingdir)):
        if (variant[-21:] == 'LinguisticVariant.csv'):
            lang = variant[0:2] + '-' + variant[2:4]
            print('Reading ', lang, ' LinguisticVariant...')
            with open(os.path.join(settings.base, settings.lingdir, variant), encoding='utf-8') as csvfile:
                reader = csv.reader(csvfile, delimiter=',', quotechar='"')
                langfileheader = reader.__next__()
                # Contains the silly BOM, reset
                langfileheader[0] = "LOINC_NUM"
                # From AH:
                # Main LOINC db has TIME_ASPCT while Linguistic Variants in at least 2.54 and 2.56 have TIME_ASPECT
                # Question raised: https://forum.loinc.org/forums/topic/time_aspct-versus-time_aspect/
                # Fix as TIME_ASPCT so we do not burden the browser with this difference
                langfileheader[3] = "TIME_ASPCT"
                for line in reader:
                    if (line[0] not in langdict):
                        langdict[line[0]] = {}
                    langdict[line[0]][lang] = line

    # Second we build a dictionary of the map_to file:
    # Key of dict is LOINC ID, value is a list of mappings
    # {"10596-5": [["31630-7","Ordinal"], ["10596-5","5360-3","Quantitative"]]}
    # ...
    print('Reading ', settings.mapToFile, '...')
    mapdict = {}
    with open(os.path.join(settings.base, settings.indir, settings.mapToFile), encoding='utf-8') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        # Skip header
        reader.__next__()
        for line in reader:
            if (line[0] not in mapdict):
                mapdict[line[0]] = []
            mapdict[line[0]].append(line[1:3])

    # Now we build the LOINC XML
    # Open loinc.csv, write on a line by line basis, otherwise may run out of memory
    outname = os.path.join(settings.base, settings.loincOutdir, settings.loincXml)
    outfile = open(outname, 'wb')
    outfile.write(str('<loinc_db version="' + settings.version +
                      '" effectiveDate="' + settings.effectiveDate + '">\n').encode('utf-8'))
    with open(os.path.join(settings.base, settings.indir, settings.loincFile), encoding='utf-8') as csvfile:
        print('Reading ', settings.loincFile, '...')
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        header = reader.__next__()
        # Contains the silly BOM, reset
        header[0] = "LOINC_NUM"
        for line in reader:
            row = etree.Element("concept", loinc_num=line[0], status=line[11])
            # Append LOINC values from loinc.csv
            lineToXml(line, header, row)
            # Append mapdict values
            if (line[0] in mapdict):
                for mapping in mapdict[line[0]]:
                    # Make row with language attribute
                    attribs = {'from': line[0], 'to': mapping[0]}
                    attribs['comment'] = mapping[1]
                    maprow = etree.Element("map", attrib=attribs)
                    row.append(maprow)
            # Append LOINC values from all LinguisticVariants from langdict
            if (line[0] in langdict):
                for lang in langdict[line[0]]:
                    # Make row with language attribute
                    langrow = etree.Element("concept", language=lang)
                    lineToXml(langdict[line[0]][lang], langfileheader, langrow)
                    row.append(langrow)
            outfile.write(etree.tostring(row, pretty_print=True, encoding='utf-8'))
    print('Writing ', settings.loincXml, '...')
    outfile.write('</loinc_db>'.encode('utf-8'))
    outfile.close()


def buildXml():
    print('Updating build.xml')
    infile = os.path.join(settings.base, settings.versionOutdir, settings.buildXml)
    print(infile)
    tree = etree.parse(infile)
    versionNode = tree.xpath('//property[@name="project.version"]')[0]
    versionNode.attrib['value'] = settings.version
    # Write XML
    print('Writing ', settings.version, ' to ', settings.buildXml, '...')
    tree.write(infile, pretty_print=True, encoding='utf-8')


def versionXml():
    # LinguisticVariants.csv:
    # "ID","ISO_LANGUAGE","ISO_COUNTRY","LANGUAGE_NAME","PRODUCER"
    # "1","de","CH","German (SWITZERLAND)","CUMUL, Switzerland"
    # etc.
    print('Reading LinguisticVariant.csv')
    root = etree.Element("loinc_db", version=settings.version, effectiveDate=settings.effectiveDate)
    locale = etree.Element("localization", id="0", language="en-US", displayName="English (US)",
                           producer="Regenstrief, US")
    root.append(locale)
    for variant in os.listdir(os.path.join(settings.base, settings.lingdir)):
        if (variant[-22:] == 'LinguisticVariants.csv'):
            with open(os.path.join(settings.base, settings.lingdir, variant), encoding='utf-8') as csvfile:
                reader = csv.reader(csvfile, delimiter=',', quotechar='"')
                langfileheader = reader.__next__()
                # May contains the BOM, reset
                langfileheader[0] = "ID"
                for line in reader:
                    locale = etree.Element("localization", id=line[0], language=line[1] + "-" + line[2],
                                           displayName=line[3], producer=line[4])
                    root.append(locale)
    # Write XML
    outfile = os.path.join(settings.base, settings.versionOutdir, settings.versionXml)
    et = etree.ElementTree(root)
    print('Writing ', settings.versionXml, '...')
    et.write(outfile, pretty_print=True, encoding='utf-8')


def addConcepts(parent, data_xls, row):
    # Make concept element
    concept = etree.Element("concept")
    # Add colums as children
    for col in data_xls.columns:
        # Skip empty columns
        if pd.notna(row[col]):
            elem = etree.Element(col)
            elem.text = str(row[col])
            concept.append(elem)
    # Add child panels, if any
    for index, childRow in data_xls[data_xls['ParentId'] == row['ID']].iterrows():
        if childRow['ParentId'] != childRow['ID']:
            addConcepts(concept, data_xls, childRow)
    parent.append(concept)


# Convert Excel to XML
def panelsToXml():
    # Import Panels as dataframe
    print('Reading ', settings.panelsFile, '...')
    # data_xls = pd.read_excel(os.path.join(settings.base, settings.indir,
    #                                       settings.panelsFile), settings.panelsSheet, index_col=None)
    data_xls = pd.read_csv(os.path.join(settings.base, settings.indir,
                                        settings.panelsFile), low_memory=False, index_col=None)
    # Make XML root
    print('Building panels XML...')
    root = etree.Element("Panels", version=settings.version, effectiveDate=settings.effectiveDate)
    # Add top level panels (parentId == Id)
    for index, row in data_xls[data_xls['ParentId'] == data_xls['ID']].iterrows():
        addConcepts(root, data_xls, row)
    # Write XML
    outfile = os.path.join(settings.base, settings.panelsOutdir, settings.panelsXml)
    et = etree.ElementTree(root)
    print('Writing ', settings.panelsXml, '...')
    et.write(outfile, pretty_print=True, encoding='utf-8')


# Make output dir if not exists
for dir in [os.path.join(settings.base, settings.loincOutdir),
            os.path.join(settings.base, settings.panelsOutdir),
            os.path.join(settings.base, settings.versionOutdir)]:
    os.makedirs(dir, exist_ok=True)
# Remove existing files
# try:
#     os.remove(os.path.join(settings.loincOutdir, settings.loincXml))
#     os.remove(os.path.join(settings.versionOutdir, settings.versionXml))
#     os.remove(os.path.join(settings.panelsOutdir, settings.panelsXml))
# except OSError:
#     pass
if not(os.path.isfile(os.path.join(settings.base, settings.indir, settings.loincFile))):
    print('ERROR: LOINC file: ', settings.loincFile, ' not found at: ', settings.base + '/' + settings.indir)
elif not(os.path.isfile(os.path.join(settings.base, settings.indir, settings.mapToFile))):
    print('ERROR: MapTo file: ', settings.mapToFile, ' not found at: ', settings.base + '/' + settings.indir)
elif not(os.path.isfile(os.path.join(settings.base, settings.indir, settings.panelsFile))):
    print('ERROR: Panels file: ', settings.panelsFile, ' not found at: ', settings.base, '/', settings.indir)
elif not(os.path.isdir(os.path.join(settings.base, settings.lingdir))):
    print('ERROR: LinguisticVariants not found at: ', settings.base, '/', settings.lingdir)
elif not(os.path.isfile(os.path.join(settings.base, settings.versionOutdir, settings.buildXml))):
    print('ERROR: build.xml not found')
else:
    # Convert panels Excel to XML
    panelsToXml()
    # Convert loinc.csv to XML
    loincXml()
    # Write version-info
    versionXml()
    # Write build info
    buildXml()
    print("Finished")