#!/bin/bash

#### CHECK BEFORE RUNNING ####
export saxon=~/Development/lib/SaxonPE9-5-1-10J/saxon9pe.jar
export inputDir=LOINC261

# the version number
export loincVersion=2.61
# must be xs:date
export loincVersionDate="2017-06-23"
# file name of the file containing the language names after Tcl conversion
export languagesFile=LOINC_${loincVersion}_LinguisticVariants.xml
# debug
export debugTcl=false

#### DO NOT EDIT BELOW HERE ####
export outputDir1=temp/${inputDir}-output1
export outputDir2=temp/${inputDir}-output2
export intermediate=temp/loinc-preprocess.xml

export xslcreatexml=xsl/loinc_mergexml.xsl
export xslprocess=xsl/xml2classic.xsl
export xslmergexml=xsl/classic-merge-language.xsl
export xslpanelsandformsxml=xsl/doPanelsAndForms.xsl

export resultFile=data/en-US/LOINC_DB.xml
export versionFile=data/version-info.xml

export panelsAndFormsInput=${outputDir1}/PanelsAndForms.xml
export panelsAndFormsOutput=data/panels/data/LOINC_PanelsAndForms.xml

export allLinguisticVariants=${outputDir2}/LOINC_${loincVersion}_LinguisticVariants.xml

if [ ! -e ${saxon:?"Parameter jarDir not set. Example: export jarPath=/mypath/lib/saxon9/saxon9.jar"} ]; then
    echo "Parameter jarDir does not lead to saxon9.jar."
    exit 1
fi
echo ""

if [ -e "temp" ]; then
    echo "Removing temp directory to prevent mixing in older results"
    rm -r "temp"
fi
if [ -e "data" ]; then
    echo "Removing data directory to prevent mixing in older results"
    rm -r "data"
fi

# Build new project
echo ""
echo "**** LOINC version $loincVersion from $loincVersionDate ****"

# Using Tcl so we can process line by line, keeping memory footprint to around 5MB and have performance
echo "Building XML from CSV $input -- 23 variants + LOINC DB + Panels on SSD takes around 25 minutes …"
tclsh ./convertcsv.tcl -input "$inputDir" -output "$outputDir1" -encoding utf-8 -version "${loincVersion}" -effectiveDate "${loincVersionDate}" -debug "${debugTcl}"

echo ""
echo "Building LOINC_DB.xml from XML with mapping info as intermediate ${intermediate}"
echo "    This step adds language properties to the file (if applicable), like id / ISO code / ISO name / producer as present in the language index file"
echo "    Secondly it adds any available maps for concepts to other concepts."
java -jar ${saxon} -s:"${outputDir1}/loinc.xml" -xsl:"${xslcreatexml}" -o:"${intermediate}" mapToFile="../${outputDir1}/map_to.xml"

echo "Building result $resultFile from intermediate ${intermediate}"
java -jar ${saxon} -s:"${intermediate}" -xsl:"$xslprocess" -o:"$resultFile" codeSystemVersion="$loincVersion" codeSystemReleaseDate="$loincVersionDate"

echo "    `date "+%Y-%m-%d %H:%M:%S"` Removing intermediate ${intermediate}"
rm "${intermediate}"

echo "Building ${versionFile} …"
java -jar ${saxon} -s:"${outputDir1}/${languagesFile}" -xsl:"$xslprocess" -o:"$versionFile" codeSystemVersion="$loincVersion" codeSystemReleaseDate="$loincVersionDate"

echo "Building intermediate linguistic variants …"
echo "    This step adds language properties to the file (if applicable), like id / ISO code / ISO name / producer as present in the language index file"
for file in `ls ${outputDir1}/*_LinguisticVariant.xml`; do
    echo "    `date "+%Y-%m-%d %H:%M:%S"` Merging in language properties with linguistic variant `basename $file` from ${languagesFile}"
    java -jar ${saxon} -s:"${file}" -xsl:"$xslcreatexml" -o:"${intermediate}" languagesFile="../${outputDir1}/${languagesFile}"
   
    echo "    `date "+%Y-%m-%d %H:%M:%S"` Building result ${outputDir2}/`basename $file`.xml from intermediate ${intermediate}"
    java -jar ${saxon} -s:"${intermediate}" -xsl:"$xslprocess" -o:"${outputDir2}/`basename $file`" codeSystemVersion="$loincVersion" codeSystemReleaseDate="$loincVersionDate"
    
    if [ -e "${intermediate}" ]; then
        echo "    `date "+%Y-%m-%d %H:%M:%S"` Removing intermediate ${intermediate}"
        rm "${intermediate}"
    fi
    echo ""
done

echo "Merging separate linguistic variants into 1 file $allLinguisticVariants"
echo "<loinc_dbs>" > "$allLinguisticVariants"
for file in `ls ${outputDir2}/*_LinguisticVariant.xml`; do 
    cat "$file" >> "$allLinguisticVariants"
done
echo "</loinc_dbs>" >> "$allLinguisticVariants"

# This step creates full database file on a per language basis. We considered this, but decided to go with 1 databasefile and left this idea
echo "Merging base LOINC database with intermediate linguistic variants …"
#for file in `ls ${outputDir2}/*_LinguisticVariant.xml`; do
#    echo "    `date "+%Y-%m-%d %H:%M:%S"` Merging linguistic variant `basename $file`"
#    java -jar ${saxon} -s:"${resultFile}" -xsl:"$xslmergexml" languageFile="../${file}"
#done

# This creates that one database file containing all languages
    echo "    `date "+%Y-%m-%d %H:%M:%S"` Merging all linguistic variants into en-US"
    java -jar ${saxon} -s:"${resultFile}" -xsl:"$xslmergexml" languageFile="../${allLinguisticVariants}"

# This compiles PanelsAndForms into a hierarchical structure where children are actual child concepts of their parent instead a flat list
    echo "    `date "+%Y-%m-%d %H:%M:%S"` Preparing `basename $panelsAndFormsInput` as $panelsAndFormsOutput"
    java -jar ${saxon} -s:"$panelsAndFormsInput" -xsl:"$xslpanelsandformsxml" -o:"$panelsAndFormsOutput"

#if [ -e "temp" ]; then
#    echo "Removing temp directory to prevent mixing in older results"
#    rm -r "temp"
#fi

echo "Building result files done. See directory data."
echo "    `date "+%Y-%m-%d %H:%M:%S"` Move the whole data directory to the LOINC package, and then move ${versionFile} and panels to the root of the LOINC package"
echo "        LOINC package layout should look at least like this:"
echo "        loinc-data/"
echo "            data/"
echo "                universal/LOINC_DB_with_LinguisticVariants.xml (full db with languages)"
echo "                collection.xconf                               (index definition)"
echo "            panels/"
echo "                data/LOINC_PanelsAndForms.xml"
echo "                collection.xconf                               (index definition)"
echo "            version-info.xml                                   (version meta data)"
