This is the newer, Python based LOINC release maker.
The older shell script + TCL + XSLT does not support Panels.

First download LOINC. 

For LOINC 2.72 and later:
- There is a single download zip, unzip, settings reflects the default layout of this zip

Before LOINC 2.72:
Do not use the LOINC Table Core, but the LOINC Table File with all fields.
- You need:
  - Loinc.csv
  - MapTo.csv
- Gather all in a folder, i.e. {your_path_to_trunk}/utilities/loinc/Loinc266 (see settings.py)
- From Accessory Files:
  - LOINC Linguistic Variants
    - Put this in a subfolder, i.e. /LinguisticVariants
  - LOINC Panels and Forms File
    - From this: PanelsAndForms.csv (LOINC 2.65 and later)

loinc2xml.py is a Python program to make a LOINC XML release for ART-DECOR

Requirements: Python 3 and lxml
If pip is available, use:
  pip install lxml
  pip install xlrd
pip is always available for newer Python installations and on most *nix/nux flavors,
otherwise see https://pip.pypa.io/en/stable/installing/

For macOS go to https://www.python.org/downloads/ for the most recent Python version. macOS comes with python 2.7 on board. After installing the latest Python, the commands python and pip are now python3 and pip3.

Edit settings.py. This file contains all parameters, set version and effectiveDate,
if necessary, adapt other settings.

Run loinc2xml.py:
  python loinc2xml.py
Runs in about 2.5mins on a modern machine, shows what it's doing.
Writes all files directly to terminology-data.

After running, build xar for this version.
