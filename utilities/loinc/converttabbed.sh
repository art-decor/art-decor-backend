#!/bin/bash

#### CHECK BEFORE RUNNING ####
export input=../LOINC240/LOINCDB.TXT
# this file is optional but recommendable
export mapToFile=../LOINC240/MAP_TO.TXT
# the version number
export loincVersion=2.40
# must be xs:date
export loincVersionDate="2011-01-01"

#### DO NOT EDIT BELOW HERE ####
export xslcreatexml=xsl/loinc_tabbed2xml.xsl
export xslprocess=xsl/xml2classic.xsl
export resultFile=LOINC_DB.xml

if [ ! -e ${jarDir:?"Parameter jarDir not set. Example: export jarDir=/mypath/lib/saxon9"}/saxon9.jar ]; then
    echo "Parameter jarDir does not lead to saxon9.jar."
    exit 1
fi

# Build classic project
echo "**** LOINC version $loincVersion from $loincVersionDate ****"

echo "Building intermediate loinc-preprocess.xml from $input"
java -jar ${jarDir}/saxon9.jar -s:"$xslcreatexml" -xsl:"$xslcreatexml" -o:loinc-preprocess.xml loincFile="$input" mapToFile="$mapToFile"

echo "Building result $resultFile from intermediate loinc-preprocess.xml"
java -jar ${jarDir}/saxon9.jar -s:"loinc-preprocess.xml" -xsl:"$xslprocess" -o:"$resultFile" codeSystemVersion="$loincVersion" codeSystemReleaseDate="$loincVersionDate"
#java -jar ${jarDir}/saxon9.jar -s:"loinc-preprocess.xml" -xsl:"$xslprocess" -o:"$resultFile"

#echo "Removing intermediate loinc-preprocess.xml"
#rm loinc-preprocess.xml

echo "Building result file done"