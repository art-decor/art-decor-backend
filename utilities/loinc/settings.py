# Change if necessary, in the default layout this will be used to access terminology-data
base = '../..'
# Change to LOINC version - this is shown in the browser as LOINC version
version = '2.75'
effectiveDate = '2023-08-29'
# Change to point to right input files
indir = 'C:/Source/LOINC/Loinc_2.75'
lingdir = indir + '/AccessoryFiles/LinguisticVariants'
panelsFile = indir + '/AccessoryFiles/PanelsAndForms/PanelsAndForms.csv'
# Warning: do not use the LOINC Table Core CSV, it does not contain all fields (such as PanelType)
loincFile = 'LoincTable/Loinc.csv'
mapToFile = 'LoincTable/MapTo.csv'
# panelsSheet = 'FORMS'
# Locations for the result files. Usually, these won't need changing
loincOutdir = 'terminology-data/loinc-data/data/universal'
panelsOutdir = 'terminology-data/loinc-data/panels/data'
versionOutdir = 'terminology-data/loinc-data'
# Names of output files. Don't change, unless you change terminology app too.
loincXml = 'LOINC_DB_with_LinguisticVariants.xml'
panelsXml = 'LOINC_PanelsAndForms.xml'
versionXml = 'version-info.xml'
buildXml = 'build.xml'
# <elem name='COMPONENT'> will become <component name='COMPONENT'> et cetera for all below
replaces = {"COMPONENT": "component",
            "PROPERTY": "property",
            "TIME_ASPCT": "timing",
            "TIME_ASPECT": "timing",
            "SYSTEM": "system",
            "SCALE_TYP": "scale",
            "METHOD_TYP": "method",
            "CLASS": "class",
            "RELATEDNAMES2": "relatedNames2",
            "SHORTNAME": "shortName",
            "ORDER_OBS": "orderObs",
            "EXAMPLE_UNITS": "exUnits",
            "LONG_COMMON_NAME": "longName",
            "EXAMPLE_UCUM_UNITS": "exUCUMunits"
            }
