<?xml version="1.0" encoding="UTF-8"?>
<!--
	Copyright (C) 2011 Nictiz
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
	
	java -Xmx8096m -jar saxon9.jar -t -s:G-Standaard_2_XML.xsl -xsl:G-Standaard_2_XML.xsl -o:dummy.xml
	
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xsl:output method="xml" indent="yes" exclude-result-prefixes="#all" encoding="UTF-8"/>

    <!--
        <row LOINC_NUM="10013-1">
          <COMPONENT>R' wave amplitude.lead I</COMPONENT>
          <PROPERTY>Elpot</PROPERTY>
          <TIME_ASPCT>Pt</TIME_ASPCT>
          <SYSTEM>Heart</SYSTEM>
          <SCALE_TYP>Qn</SCALE_TYP>
          <METHOD_TYP>EKG</METHOD_TYP>
          <CLASS>EKG.MEAS</CLASS>
          <SOURCE>CH</SOURCE>
          <DATE_LAST_CHANGED>19980820</DATE_LAST_CHANGED>
          <CHNG_TYPE>MIN</CHNG_TYPE>
          <STATUS>ACTIVE</STATUS>
          <CLASSTYPE>2</CLASSTYPE>
          <UNITSREQUIRED>Y</UNITSREQUIRED>
          <RELATEDNAMES2>Cardiac; ECG; EKG.MEASUREMENTS; Electrical potential; Electrocardiogram; Electrocardiograph; Hrt; PB; Point in time; QNT; Quan; Quant; Quantitative; R prime; R' wave Amp L-I; R wave Amp L-I; Random; Right; Voltage</RELATEDNAMES2>
          <SHORTNAME>R' wave Amp L-I</SHORTNAME>
          <ORDER_OBS>Observation</ORDER_OBS>
          <EXAMPLE_UNITS>mV</EXAMPLE_UNITS>
          <LONG_COMMON_NAME>R' wave amplitude in lead I</LONG_COMMON_NAME>
          <HL7_V2_DATATYPE>NM</HL7_V2_DATATYPE>
          <HL7_V3_DATATYPE>PQ</HL7_V3_DATATYPE>
          <EXAMPLE_UCUM_UNITS>mV</EXAMPLE_UCUM_UNITS>
          <COMMON_TEST_RANK>0</COMMON_TEST_RANK>
          <COMMON_ORDER_RANK>0</COMMON_ORDER_RANK>
          <COMMON_SI_TEST_RANK>0</COMMON_SI_TEST_RANK>
       </row>
    -->
    <xsl:param name="codeSystemVersion" required="yes"/>
    <xsl:param name="codeSystemReleaseDate" required="yes"/>
    
    <xsl:variable name="theCodeSystem">2.16.840.1.113883.6.1</xsl:variable>
    <xsl:variable name="theCodeSystemName">LOINC</xsl:variable>
    <xsl:variable name="theCodeSystemStatus">active</xsl:variable>
    
    <xsl:variable name="custodianOrganisation">LOINC</xsl:variable>
    <xsl:variable name="custodianOrganisationLogo">LOINC.png</xsl:variable>
    <xsl:variable name="custodianOrganisationUrl">http://www.loinc.org</xsl:variable>
    
    <xsl:variable name="authorShort">Responsible</xsl:variable>
    <xsl:variable name="responsibleAuthor">Regenstrief</xsl:variable>
    
    <xsl:variable name="statusMapping" as="element()*">
        <map loinc="ACTIVE" claml="active"/>
        <map loinc="DEPRECATED" claml="deprecated"/>
        <map loinc="DISCOURAGED" claml="discouraged"/>
        <map loinc="TRIAL" claml="trial"/>
    </xsl:variable>
    
    <xsl:template match="/*" exclude-result-prefixes="#all">
        <ClaML version="2.0.0">
            <Meta name="statusCode" value="{$theCodeSystemStatus}"/>
            <Meta name="custodianOrganisation" value="{$custodianOrganisation}"/>
            <Meta name="custodianOrganisationLogo" value="{$custodianOrganisationLogo}"/>
            <Meta name="custodianOrganisationUrl" value="{$custodianOrganisationUrl}"/>
            <Identifier authority="{$custodianOrganisation}" uid="{$theCodeSystem}"/>
            <Title name="{$theCodeSystemName}" date="{$codeSystemReleaseDate}" version="{$codeSystemVersion}">
                <xsl:value-of select="$theCodeSystemName"/>
            </Title>
            <Authors>
                <Author name="{$authorShort}">
                    <xsl:value-of select="$responsibleAuthor"/>
                </Author>
            </Authors>
            <ClassKinds>
                <ClassKind name="abstract"/>
                <ClassKind name="concept"/>
            </ClassKinds>
            <!-- "LOINC_NUM",
                "COMPONENT",
                "PROPERTY",
                "TIME_ASPCT",
                "SYSTEM",
                "SCALE_TYP",
                "METHOD_TYP",
                "CLASS",
                "SOURCE",
                "DATE_LAST_CHANGED",
                "CHNG_TYPE",
                "COMMENTS",
                "STATUS",
                "CONSUMER_NAME",
                "MOLAR_MASS",
                "CLASSTYPE",
                "FORMULA",
                "SPECIES",
                "EXMPL_ANSWERS",
                "ACSSYM",
                "BASE_NAME",
                "NAACCR_ID",
                "CODE_TABLE",
                "SURVEY_QUEST_TEXT",
                "SURVEY_QUEST_SRC",
                "UNITSREQUIRED",
                "SUBMITTED_UNITS",
                "RELATEDNAMES2",
                "SHORTNAME",
                "ORDER_OBS",
                "CDISC_COMMON_TESTS",
                "HL7_FIELD_SUBFIELD_ID",
                "EXTERNAL_COPYRIGHT_NOTICE",
                "EXAMPLE_UNITS",
                "LONG_COMMON_NAME",
                "HL7_V2_DATATYPE",
                "HL7_V3_DATATYPE",
                "CURATED_RANGE_AND_UNITS",
                "DOCUMENT_SECTION",
                "EXAMPLE_UCUM_UNITS",
                "EXAMPLE_SI_UCUM_UNITS",
                "STATUS_REASON",
                "STATUS_TEXT",
                "CHANGE_REASON_PUBLIC",
                "COMMON_TEST_RANK",
                "COMMON_ORDER_RANK",
                "COMMON_SI_TEST_RANK",
                "HL7_ATTACHMENT_STRUCTURE",
                "EXTERNAL_COPYRIGHT_LINK" -->
            <RubricKinds>
                <!-- TODO -->
                <RubricKind name="definition"/>
                <RubricKind name="description"/>
                <RubricKind name="header"/>
                <RubricKind name="preferred"/>
                <RubricKind name="short"/>
            </RubricKinds>
            <xsl:for-each select="row">
                <xsl:variable name="theCode" select="@LOINC_NUM"/>
                <Class code="{$theCode}" kind="concept">
                    <!-- TODO -->
                </Class>
            </xsl:for-each>
        </ClaML>
    </xsl:template>
    
</xsl:stylesheet>
