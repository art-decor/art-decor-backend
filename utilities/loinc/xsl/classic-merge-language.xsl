<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    exclude-result-prefixes="xs xd"
    version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Feb 24, 2016</xd:p>
            <xd:p><xd:b>Author:</xd:b> ahenket</xd:p>
            <xd:p>Merge full LOINC DB with Linguistic Variant, input is the full LOINC_DB, required parameter is either the path+filename of the language file or the path to all language files</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:output indent="yes" omit-xml-declaration="yes"/>
    
    <!-- Example LOINC_2.61_de_AT_24_LinguisticVariant.xml or a compiled file of all LinguisticVariants e.g. LOINC_2.61_LinguisticVariants.xml -->
    <xsl:param name="languageFile" required="no" as="xs:string?"/>
    
    <!-- Expectation is that:
        a. The root element is loinc_db for one single LinguisticVariant or
        b. Under the root element there are as many loinc_db elements as there are LinguisticVariants -->
    <xsl:variable name="l10nContents" select="doc($languageFile)/*" as="element()"/>
    
    <xsl:key name="l10n" match="concept" use="@loinc_num"/>
    
    <xsl:variable name="language">
        <xsl:choose>
            <xsl:when test="count($l10nContents/*/@language) = 1">
                <xsl:value-of select="$l10nContents/*/@language"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>universal</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="outputFile">
        <xsl:choose>
            <xsl:when test="count($l10nContents/*/@language) = 1">
                <xsl:value-of select="tokenize($languageFile,'/')[last()]"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>LOINC_DB_with_LinguisticVariants.xml</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:template match="loinc_dbs">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="loinc_db">
        <xsl:if test="$l10nContents">
            <xsl:message>
                <xsl:text>    </xsl:text>
                <xsl:value-of select="replace(substring(string(current-dateTime()), 1, 19), 'T', ' ')"/>
                <xsl:text> Language file found with </xsl:text>
                <xsl:value-of select="count($l10nContents/descendant-or-self::loinc_db)"/>
                <xsl:text> languages: </xsl:text>
                <xsl:value-of select="$languageFile"/>
            </xsl:message>
        </xsl:if>
        <xsl:result-document href="data/{$language}/{$outputFile}" indent="yes">
            <xsl:copy>
                <xsl:copy-of select="@version"/>
                <xsl:copy-of select="@effectiveDate"/>
                <!--<xsl:attribute name="language" select="$language"/>-->
                <xsl:apply-templates/>
            </xsl:copy>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="concept">
        <xsl:variable name="conceptId" select="@loinc_num"/>
        
        <xsl:copy>
            <xsl:copy-of select="@* | node()"/>
            <xsl:for-each select="$l10nContents">
                <xsl:apply-templates select="key('l10n', $conceptId)" mode="l10n"/>
            </xsl:for-each>
            <!--<xsl:apply-templates select="$l10nContents/concept[@loinc_num=$conceptId]" mode="l10n"/>-->
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="concept" mode="l10n">
        <xsl:copy>
            <xsl:copy-of select="@* except @loinc_num"/>
            <xsl:copy-of select="node()"/>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>