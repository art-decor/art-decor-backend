<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    exclude-result-prefixes="xs xd"
    version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Aug 9, 2017</xd:p>
            <xd:p><xd:b>Author:</xd:b> ahenket</xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    
    <xsl:output indent="yes" omit-xml-declaration="yes"/>
    
    <xsl:key name="children" match="row" use="elem[@name='ParentId']"/>
    
    <xsl:template match="/">
        <Panels>
            <xsl:apply-templates select="root/row[elem[@name='ParentId'] = elem[@name='Id']]"/>
        </Panels>
    </xsl:template>
    
    <xsl:template match="row">
        <xsl:variable name="conceptId" select="elem[@name='Id']"/>
        <xsl:variable name="childConcepts" as="element()*" select="key('children', $conceptId)[not(elem[@name='Id'] = $conceptId)]"/>
        
        <concept>
            <xsl:apply-templates select="elem" mode="#current"/>
            <xsl:apply-templates select="$childConcepts"/>
        </concept>
    </xsl:template>
    
    <xsl:template match="elem" mode="#all">
        <xsl:element name="{@name}"><xsl:value-of select="."/></xsl:element>
    </xsl:template>
</xsl:stylesheet>