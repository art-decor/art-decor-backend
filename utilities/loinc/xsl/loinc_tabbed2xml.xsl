<?xml version="1.0" encoding="UTF-8"?>
<!--
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
    
    Converts ASCII to xml
    Column names are derived from first row
    LOINC_NUM will be attribute of row, all other columns become elements. 
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="fn" version="2.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xsl:output method="xml" indent="yes" exclude-result-prefixes="#all" encoding="iso-8859-1"/>
    <!-- characterset of input file, incorrect setting will result in error: Failed to read input file .... (java.nio.charset.MalformedInputException) - Input length = 1 -->
    <xsl:param name="charSet" select="'iso-8859-1'"/>
    <xsl:param name="loincFile" select="'../LOINC217/LOINCDB.TXT'"/>
    <xsl:param name="mapToFile" select="'../LOINC217/MAP_TO.TXT'"/>

    <!-- open file -->
    <xsl:variable name="loincdb">
        <xsl:value-of select="unparsed-text($loincFile,$charSet)"/>
        <!--<xsl:choose>
            <xsl:when test="unparsed-text-available($loincFile)">
                <xsl:value-of select="unparsed-text($loincFile,$charSet)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:message terminate="yes">
                    <xsl:text>ERROR Cannot locate: </xsl:text>
                    <xsl:value-of select="$loincFile"/>
                </xsl:message>
            </xsl:otherwise>
        </xsl:choose>-->
    </xsl:variable>
    
    <!-- open file -->
    <xsl:variable name="mapto">
        <xsl:choose>
            <xsl:when test="unparsed-text-available($mapToFile)">
                <xsl:message>
                    <xsl:text>INFO Using map file: </xsl:text>
                    <xsl:value-of select="$mapToFile"/>
                </xsl:message>
                <xsl:value-of select="unparsed-text($mapToFile,$charSet)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:message>
                    <xsl:text>WARNING Cannot locate: </xsl:text>
                    <xsl:value-of select="$mapToFile"/>
                </xsl:message>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="maptoxml" as="element(root)?">
        <xsl:if test="not(empty($mapto) or $mapto='')">
            <xsl:variable name="lines" select="tokenize($mapto, '\r?\n')" as="xs:string+"/>
            <xsl:variable name="elemNames" select="fn:getTokens($lines[1])" as="xs:string+"/>

            <root>
                <xsl:for-each select="$lines[position() > 1][string-length()>0]">
                    <row>
                        <xsl:variable name="lineItems" select="fn:getTokens(.)" as="xs:string*"/>

                        <xsl:for-each select="$elemNames">
                            <map name="{.}">
                                <xsl:variable name="pos" select="position()"/>
                                <xsl:value-of select="$lineItems[$pos]"/>
                            </map>
                        </xsl:for-each>
                    </row>
                </xsl:for-each>
            </root>
        </xsl:if>
    </xsl:variable>

    <xsl:template match="/" exclude-result-prefixes="#all">
        <xsl:if test="not(empty($loincdb) or $loincdb='')">
            <xsl:variable name="lines" select="tokenize($loincdb, '\r?\n')" as="xs:string+"/>
            <xsl:variable name="elemNames" select="fn:getTokens($lines[1])" as="xs:string+"/>
            <xsl:variable name="loincnumpos" select="index-of($elemNames,'LOINC_NUM')"/>

            <root>
                <xsl:for-each select="$lines[position() > 1][string-length()>0]">
                    <row>
                        <xsl:variable name="lineItems" select="fn:getTokens(.)" as="xs:string*"/>

                        <xsl:for-each select="$elemNames">
                            <xsl:variable name="pos" select="position()"/>
                            <xsl:choose>
                                <xsl:when test="empty($lineItems[$pos]) or $lineItems[$pos]=''"/>
                                <xsl:otherwise>
                                    <elem name="{.}">
                                        <xsl:value-of select="$lineItems[$pos]"/>
                                    </elem>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:for-each>

                        <xsl:for-each select="$maptoxml/row[map[@name='LOINC']=$lineItems[$loincnumpos]]">
                            <map from="{map[1]}" to="{map[2]}" comment="{map[3]}"/>
                        </xsl:for-each>
                    </row>
                </xsl:for-each>
            </root>
        </xsl:if>
    </xsl:template>

    <!-- Adapted from http://andrewjwelch.com/code/xslt/csv/csv-to-xml_v2.html -->
    <xsl:function name="fn:getTokens" as="xs:string+">
        <xsl:param name="str" as="xs:string"/>
        <xsl:analyze-string select="concat($str, '\t')" regex='(("[^"]*")+|[^\t]*)\t'>
            <xsl:matching-substring>
                <xsl:sequence select='replace(regex-group(1), "^""|""$|("")""", "$1")'/>
            </xsl:matching-substring>
        </xsl:analyze-string>
    </xsl:function>
</xsl:stylesheet>
