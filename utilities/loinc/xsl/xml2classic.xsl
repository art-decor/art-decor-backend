<?xml version="1.0" encoding="UTF-8"?>
<!--
    Copyright (C) 2011 Nictiz
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
    
    java -Xmx8096m -jar saxon9.jar -t -s:G-Standaard_2_XML.xsl -xsl:G-Standaard_2_XML.xsl -o:dummy.xml
    
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xsl:output method="xml" exclude-result-prefixes="#all" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>

    <!-- Example 2.50 -->
    <xsl:param name="codeSystemVersion" required="yes" as="xs:string"/>
    <!-- Example 2014-12-22 -->
    <xsl:param name="codeSystemReleaseDate" required="yes" as="xs:date"/>
    <!-- LinguisticVariant -->
    <!-- "ID","ISO_LANGUAGE","ISO_COUNTRY","LANGUAGE_NAME","PRODUCER" -->
    
    <xsl:template match="/*[row[1]/*[@name='LOINC_NUM']]" exclude-result-prefixes="#all">
        <loinc_db version="{$codeSystemVersion}" effectiveDate="{$codeSystemReleaseDate}">
            <!-- expect:    @id|          @language         | @displayName  | @producer -->
            <!-- maps to:   ID | ISO_LANGUAGE"_"ISO_COUNTRY | LANGUAGE_NAME | PRODUCER -->
            <xsl:copy-of select="@* except (@version|@effectiveDate)"/>
            <xsl:for-each select="row">
                
                <concept loinc_num="{elem[@name='LOINC_NUM']}">
                    <xsl:if test="elem[@name='STATUS']">
                        <xsl:attribute name="status" select="elem[@name='STATUS']"/>
                    </xsl:if>
                    <xsl:if test="elem[@name='HL7_V3_DATATYPE']">
                        <xsl:attribute name="hl7_v3_dataType" select="elem[@name='HL7_V3_DATATYPE']"/>
                    </xsl:if>
                    <xsl:copy-of select="parent::*/@language" copy-namespaces="no"/>
                    <xsl:for-each select="elem">
                        <xsl:choose>
                            <xsl:when test="@name='COMPONENT'">
                                <component name="{@name}" length="{string-length(.)}" count="{count(tokenize(.,'\s'))}">
                                    <xsl:value-of select="."/>
                                </component>
                            </xsl:when>
                            <xsl:when test="@name='PROPERTY'">
                                <property name="{@name}">
                                    <xsl:value-of select="."/>
                                </property>
                            </xsl:when>
                            <!-- Main LOINC db has TIME_ASPCT while Linguistic Variants in at least 2.54 and 2.56 have TIME_ASPECT
                                Question raised: https://forum.loinc.org/forums/topic/time_aspct-versus-time_aspect/
                                
                                Fix as TIME_ASPCT so we do not burden the browser with this difference
                            -->
                            <xsl:when test="@name=('TIME_ASPCT','TIME_ASPECT')">
                                <timing name="TIME_ASPCT">
                                    <xsl:value-of select="."/>
                                </timing>
                            </xsl:when>
                            <xsl:when test="@name='SYSTEM'">
                                <system name="{@name}">
                                    <xsl:value-of select="."/>
                                </system>
                            </xsl:when>
                            <xsl:when test="@name='SCALE_TYP'">
                                <scale name="{@name}">
                                    <xsl:value-of select="."/>
                                </scale>
                            </xsl:when>
                            <xsl:when test="@name='METHOD_TYP'">
                                <method name="{@name}">
                                    <xsl:value-of select="."/>
                                </method>
                            </xsl:when>
                            <xsl:when test="@name='EXAMPLE_UCUM_UNITS'">
                                <exUCUMunits name="{@name}">
                                    <xsl:value-of select="."/>
                                </exUCUMunits>
                            </xsl:when>
                            <xsl:when test="@name='EXAMPLE_UNITS'">
                                <exUnits name="{@name}">
                                    <xsl:value-of select="."/>
                                </exUnits>
                            </xsl:when>
                            <xsl:when test="@name='CLASS'">
                                <class name="{@name}">
                                    <xsl:value-of select="."/>
                                </class>
                            </xsl:when>
                            <xsl:when test="@name='LONG_COMMON_NAME'">
                                <longName name="{@name}" length="{string-length(.)}" count="{count(tokenize(.,'\s'))}">
                                    <xsl:value-of select="."/>
                                </longName>
                            </xsl:when>
                            <xsl:when test="@name='SHORTNAME'">
                                <shortName name="{@name}" length="{string-length(.)}" count="{count(tokenize(. ,'\s'))}">
                                    <xsl:value-of select="."/>
                                </shortName>
                            </xsl:when>
                            <!--               <type><xsl:value-of select="COMPONENT"/></type>-->
                            <xsl:when test="@name='ORDER_OBS'">
                                <orderObs name="{@name}">
                                    <xsl:value-of select="."/>
                                </orderObs>
                            </xsl:when>
                            <xsl:when test="@name='RELATEDNAMES2'">
                                <relatedNames2 name="{@name}">
                                    <xsl:value-of select="."/>
                                </relatedNames2>
                            </xsl:when>
                            <xsl:otherwise>
                                <elem name="{@name}">
                                    <xsl:value-of select="."/>
                                </elem>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:for-each>
                    <xsl:copy-of select="map" copy-namespaces="no"/>
                </concept>

            </xsl:for-each>

        </loinc_db>
    </xsl:template>
    
    <xsl:template match="/*[not(row[1]/*[@name='LOINC_NUM'])]" exclude-result-prefixes="#all">
        <loinc_db version="{$codeSystemVersion}" effectiveDate="{$codeSystemReleaseDate}">
            <!-- expect:    @id|          @language         | @displayName  | @producer -->
            <!-- maps to:   ID | ISO_LANGUAGE"_"ISO_COUNTRY | LANGUAGE_NAME | PRODUCER -->
            <xsl:copy-of select="@* except (@version|@effectiveDate)"/>
            
            <localization id="0" language="en-US" displayName="English (US)" producer="Regenstrief, US"/>
            <xsl:for-each select="row">
                <xsl:sort select="*[@name='LANGUAGE_NAME']/lower-case(.)"/>
                <!--
                    <row>
                        <elem name="ID">1</elem>
                        <elem name="ISO_LANGUAGE">de</elem>
                        <elem name="ISO_COUNTRY">CH</elem>
                        <elem name="LANGUAGE_NAME">German (SWITZERLAND)</elem>
                        <elem name="PRODUCER">CUMUL, Switzerland</elem>
                    </row>
                -->
                <!-- <localization version="2.54" effectiveDate="2015-12-21" id="22" language="nl-NL" displayName="Dutch (NETHERLANDS)" producer="NVKC, Dutch Society for Clinical Chemistry and Laboratory Medicine, The Netherlands"/> -->
                <localization>
                    <xsl:attribute name="id" select="*[@name='ID']"/>
                    <xsl:attribute name="language" select="string-join((*[@name='ISO_LANGUAGE'],*[@name='ISO_COUNTRY']),'-')"/>
                    <xsl:attribute name="displayName" select="*[@name='LANGUAGE_NAME']"/>
                    <xsl:attribute name="producer" select="*[@name='PRODUCER']"/>
                </localization>
                
            </xsl:for-each>
            
        </loinc_db>
    </xsl:template>
</xsl:stylesheet>
