<?xml version="1.0" encoding="UTF-8"?>
<!--
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
    
    Converts ASCII to xml
    Column names are derived from forst row
    LOINC_NUM will be attribute of row, all other columns become elements. 
-->
<!--
    Support LOINC's newer comma delimited CSV format for at least 2.50
    LOINC has double "" in quoted strings, 
    LOINC sometimes has double "" indicating empty value
    LOINC does not wrap all values in quotes ...
    
    "Column ""text in quotes"" rest",,,"","Column",,2
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xsl:output method="xml" indent="yes" omit-xml-declaration="yes" exclude-result-prefixes="#all" encoding="UTF-8"/>
    <!-- characterset of input file, incorrect setting will result in error: Failed to read input file .... (java.nio.charset.MalformedInputException) - Input length = 1 -->
    <xsl:param name="charSet" select="'UTF-8'"/>
    
    <!--<xsl:param name="mapToFile" select="'../LOINC254-output1/map_to.xml'"/>-->
    <xsl:param name="mapToFile"/>
    <!--<xsl:param name="languagesFile" select="'../LOINC254-output1/LOINC_2.54_LinguisticVariants.xml'"/>-->
    <xsl:param name="languagesFile"/>
    
    <!-- loinc.csv -->
    <!-- LOINC_2.54_de_AT_24_LinguisticVariant.csv -->
    <xsl:variable name="filename" select="tokenize(document-uri(.),'/')[last()]" as="xs:string"/>
    
    <!-- map_to.xml -->
    <!--
        <row>
            <elem name="﻿LOINC">10962-9</elem>
            <elem name="MAP_TO">7681-0</elem>
        </row>
        <row>
            <elem name="﻿LOINC">11022-1</elem>
            <elem name="MAP_TO">29161-7</elem>
            <elem name="COMMENT">Meconium testing should be reported in mass content (ex. Units ng/g)</elem>
        </row>
    -->
    <xsl:variable name="map_to" as="element()?" select="if (doc-available($mapToFile)) then doc($mapToFile)/* else ()"/>
    
    <xsl:key name="mapkey" match="row" use="elem[@name = 'LOINC'] | elem[@name = 'MAP_TO']"/>
    
    <xsl:template match="/" exclude-result-prefixes="#all">
        <xsl:apply-templates select="root" mode="processLOINC"/>
    </xsl:template>
    
    <xsl:template match="root" mode="processLOINC">
        <xsl:if test="string-length($languagesFile)>0 and not(doc-available($languagesFile))">
            <xsl:message terminate="yes">    Languages file configured, but not accessible: <xsl:value-of select="$languagesFile"/></xsl:message>
        </xsl:if>
        <xsl:if test="string-length($mapToFile)>0 and not(doc-available($mapToFile))">
            <xsl:message terminate="yes">    Map_to file configured, but not accessible: <xsl:value-of select="$mapToFile"/></xsl:message>
        </xsl:if>
        
        <root>
            <xsl:copy-of select="@*" copy-namespaces="no"/>
            <xsl:if test="doc-available($languagesFile)">
                <xsl:variable name="parts" select="tokenize(string-join(tokenize($filename,'\.')[position()!=last()],'.'),'_')"/>
                <xsl:if test="count($parts)>=5">
                    <xsl:variable name="id" select="$parts[5]"/>
                    <!--
                        "ID","ISO_LANGUAGE","ISO_COUNTRY","LANGUAGE_NAME","PRODUCER"
                        "1","de","CH","German (SWITZERLAND)","CUMUL, Switzerland"
                    -->
                    <xsl:variable name="lines" select="doc($languagesFile)/root/row" as="element()+"/>
                    <xsl:variable name="languageLine" select="$lines[*[@name='ID']=$id]/*" as="element()+"/>
                    
                    <xsl:attribute name="id" select="$id"/>
                    <xsl:attribute name="language" select="string-join(($languageLine[@name='ISO_LANGUAGE'],$languageLine[@name='ISO_COUNTRY']),'-')"/>
                    <xsl:attribute name="displayName" select="$languageLine[@name='LANGUAGE_NAME']"/>
                    <xsl:attribute name="producer" select="$languageLine[@name='PRODUCER']"/>
                </xsl:if>
            </xsl:if>
            <xsl:apply-templates select="row" mode="#current"/>
        </root>
    </xsl:template>
    <xsl:template match="row" mode="processLOINC">
        <xsl:variable name="LOINC_NUM" select="*[@name = 'LOINC_NUM']" as="xs:string"/>
        <xsl:copy copy-namespaces="no">
            <xsl:copy-of select="*" copy-namespaces="no"/>
            <xsl:for-each select="$map_to">
                <xsl:for-each select="key('mapkey', $LOINC_NUM)">
                    <!--<xsl:message> Map found for LOINC <xsl:value-of select="$LOINC_NUM"/></xsl:message>-->
                    <xsl:variable name="from" select="*[@name = 'LOINC']" as="xs:string"/>
                    <xsl:variable name="to" select="*[@name = 'MAP_TO']" as="xs:string"/>
                    <xsl:variable name="comment" select="*[@name = 'COMMENT']" as="element()?"/>
                    <map from="{$from}" to="{$to}">
                        <xsl:if test="$comment">
                            <xsl:attribute name="comment" select="$comment"/>
                        </xsl:if>
                    </map>
                </xsl:for-each>
            </xsl:for-each>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>
