<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:local="urn:local"
    exclude-result-prefixes="#all"
    version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Sep 4, 2017</xd:p>
            <xd:p><xd:b>Author:</xd:b> ahenket</xd:p>
            <xd:p><xd:b>Purpose:</xd:b> Convert a fully prepared LOINC database in ART-DECOR format into a FHIR STU3 CodeSystem</xd:p>
            <xd:p><xd:b>Background documentation:</xd:b> <xd:a href="https://www.hl7.org/STU3/fhir/codesystem.html">FHIR STU3 CodeSystem</xd:a><xd:br/>
                <xd:a href="https://www.hl7.org/fhir/loinc.html">LOINC FHIR page</xd:a></xd:p>
            <xd:p><xd:b>Mapping:</xd:b> <xd:ul>
                <xd:li>LONG_COMMON_NAME = en-US concept.display</xd:li>
                <xd:li>LONG_COMMON_NAME = <xd:i>other language</xd:i> concept.designation as Fully Specified Name</xd:li>
                <xd:li>SHORTNAME = en-US concept.designation as Synonym</xd:li>
                <xd:li>SHORTNAME = <xd:i>other language</xd:i> concept.designation as Synonym</xd:li>
                <xd:li>RELATEDNAMES2 = en-US concept.designation as Synonym. Terms split by semi colon</xd:li>
                <xd:li>RELATEDNAMES2 = <xd:i>other language</xd:i> concept.designation as Synonym. Terms split by semi colon</xd:li>
                <xd:li>All other columns are added as property. <xd:b>FIXME</xd:b>: Every <xd:i>other language</xd:i> columns get a language element as part of the property</xd:li>
                <xd:li>MAP_TO = property when current concept maps to other concept. When current concept is being mapped to, it is skipped</xd:li>
            </xd:ul></xd:p>
            <xd:p><xd:b>Example call: </xd:b> <xd:pre>java -jar saxon9pe.jar -s:data/universal/LOINC_DB_with_LinguisticVariants.xml -xsl:xsl/loinc2fhirCodeSystem.xsl -o:CodeSystem/CodeSystem_LOINC.xml maxConcepts=10</xd:pre>
                <xd:br/>NOTE: The parameter '<xd:ref name="maxConcepts" type="parameter"/>' determines the number concepts in the output is its value is &gt; 0. A value of &lt;= 0 yields all concepts.</xd:p>
        </xd:desc>
    </xd:doc>
    
    <xsl:output omit-xml-declaration="yes" indent="yes"/>
    
    <!-- DEBUG param. Allows fewer concepts than the max to test with. Default of 10 for now. Default should be 0 or -1 -->
    <xsl:param name="maxConcepts" select="10"/>
    <xsl:param name="codeSystem">http://loinc.org</xsl:param>
    <xsl:param name="codeSystemName">LOINC</xsl:param>
    <xsl:param name="codeSystemId" select="replace(lower-case($codeSystemName), '\s', '')"/>
    <xsl:param name="codeSystemVersion">2.61</xsl:param>
    <xsl:param name="codeSystemValueSet">http://art-decor.org/fhir/ValueSet/loinc</xsl:param>
    <xsl:param name="status">active</xsl:param>
    <xsl:param name="copyright">This content LOINC® is copyright © 1995 Regenstrief Institute, Inc. and the LOINC Committee, and available at no cost under the license at http://loinc.org/terms-of-use</xsl:param>
    <xsl:param name="publisher">Regenstrief Institute</xsl:param>
    <xsl:param name="publisherUrl">https://loinc.org</xsl:param>
    <xsl:param name="publisherDate" select="current-dateTime()"/>
    
    <xsl:variable name="columnMap" as="element()+">
        <row n="1" name="LOINC_NUM" type="Text" width="10" desc="The unique LOINC Code is a string in the format of nnnnnnnn-n."/>
        <row n="2" name="COMPONENT" type="Text" width="255" desc="First major axis-component or analyte"/>
        <row n="3" name="PROPERTY" type="Text" width="30" desc="Second major axis-property observed (e.g., mass vs. substance)"/>
        <row n="4" name="TIME_ASPCT" type="Text" width="15" desc="Third major axis-timing of the measurement (e.g., point in time vs 24 hours) "/>
        <row n="5" name="SYSTEM" type="Text" width="100" desc="Fourth major axis-type of specimen or system (e.g., serum vs urine) "/>
        <row n="6" name="SCALE_TYP" type="Text" width="30" desc="Fifth major axis-scale of measurement (e.g., qualitative vs. quantitative) "/>
        <row n="7" name="METHOD_TYP" type="Text" width="50" desc="Sixth major axis-method of measurement "/>
        <row n="8" name="CLASS" type="Text" width="20" desc="An arbitrary classification of the terms for grouping related observations together. The current classifications are listed in Table 30. We present the database sorted by the class field within class type (see field 23). Users of the database should feel free to re-sort the database in any way they find useful, and/or to add their own classifying fields to the database. The content of the laboratory test subclasses should be obvious from the subclass name. "/>
        <row n="9" name="SOURCE" type="Text" width="8" desc="This is for our internal use and should be ignored by database users. "/>
        <row n="9" name="VersionLastChanged" type="Text" width="10" desc="The LOINC version number in which the record has last changed. For new records, this field contains the same value as the loinc.FirstPublishedRelease field. Column added in version 2.54"/>
        <row n="10" name="DATE_LAST_CHANGED" type="Date/Time" width="10" desc="Date the concept was last changed last changed. Column deprecated in version 2.54"/>
        <row n="11" name="CHNG_TYPE" type="Text" width="3" desc="Change Type Code DEL = delete (deprecate) ADD = add NAM = change to Analyte/Component (field #2); MAJ = change to name field other than #2 (#3 - #7); MIN = change to field other than name UND = undelete "/>
        <row n="11" name="DefinitionDescription" type="Memo" width="-" desc="Narrative text that describes the LOINC term taken as a whole (i.e., taking all of the parts of the term together) or relays information speci c to the term, such as the context in which the term was requested or its clinical utility."/>
        <row n="12" name="COMMENTS" type="Memo" width="" desc="Narrative description or comments about a term. Column deprecated in version 2.56"/>
        <row n="13" name="STATUS" type="Text" width="11" desc="ACTIVE = Concept is active. Use at will. TRIAL = Concept is experimental in nature. Use with caution as the concept and associated attributes may change. DISCOURAGED = Concept is not recommended for current use. New mappings to this concept are discouraged; although existing may mappings may continue to be valid in context. Wherever possible, the superseding concept is indicated in the MAP_TO field in the MAP_TO table (see Table 29b) and should be used instead. DEPRECATED = Concept is deprecated. Concept should not be used, but it is retained in LOINC for historical purposes. Wherever possible, the superseding concept is indicated in the MAP_TO field (see Table 29b) and should be used both for new mappings and updating existing implementations."/>
        <row n="14" name="CONSUMER_NAME" type="Text" width="255" desc="An experimental (beta) consumer friendly name for this item. The intent is to provide a test name that health care consumers will recognize; it will be similar to the names that might appear on a lab report and is not guaranteed to be unique because some elements of the LOINC name are likely to be omitted. We will continue to modify these names in future release, so do not expect it to be stable (or perfect). Feedback is welcome. "/>
        <row n="15" name="MOLAR_MASS" type="Text" width="13" desc="Molecular weights: This field contains the molecular weights of chemical moieties when they are provided to us. This release contains values kindly contributed by IUPAC. "/>
        <row n="16" name="CLASSTYPE" type="Number" width="" desc="1=Laboratory class; 2=Clinical class; 3=Claims attachments; 4=Surveys "/>
        <row n="17" name="FORMULA" type="Text" width="255" desc="Contains the formula in human readable form, for calculating the value of any measure that is based on an algebraic or other formula except those for which the component expresses the formula. So Sodium/creatinine does not need a formula, but Free T3 index does."/>
        <row n="18" name="SPECIES" type="Text" width="20" desc="Codes detailing which non-human species the term applies to. If blank, “human” is assumed. "/>
        <row n="19" name="EXMPL_ANSWERS" type="Memo" width="" desc="For some tests and measurements, we have supplied examples of valid answers, such as “1:64”, “negative @ 1:16”, or “55”. "/>
        <row n="20" name="ACSSYM" type="Memo" width="" desc="Chemical name synonyms, alternative name synonyms, and chemical formulae supplied by the Chemical Abstract Society. "/>
        <row n="21" name="BASE_NAME" type="Text" width="50" desc="Chemical base name from CAS "/>
        <row n="22" name="NAACCR_ID" type="Text" width="20" desc="Maps to North American Association of Central Cancer Registries Identification Number "/>
        <row n="23" name="CODE_TABLE" type="Text" width="10" desc="Examples on CR0050 Cancer Registry "/>
        <row n="24" name="SURVEY_QUEST_TEXT" type="Memo" width="" desc="Verbatim question from the survey instrument "/>
        <row n="25" name="SURVEY_QUEST_SRC" type="Text" width="50" desc="Exact name of the survey instrument and the item/question number "/>
        <row n="26" name="UNITSREQUIRED" type="Text" width="1" desc="Y/N field that indicates that units are required when this LOINC is included as an OBX segment in a HIPAA attachment "/>
        <row n="27" name="SUBMITTED_UNITS" type="Text" width="30" desc="Units as received from person who requested this LOINC term. "/>
        <row n="28" name="RELATEDNAMES2" type="Memo" width="" desc="This is a new field introduced in version 2.05. It contains synonyms for each of the parts of the fully specified LOINC name (component, property, time, system, scale, method). It replaces #8, Relat_NMS. "/>
        <row n="29" name="SHORTNAME" type="Text" width="40" desc="Introduced in version 2.07, this field is a concatenation of the fully specified LOINC name. The field width may change in a future release. "/>
        <row n="30" name="ORDER_OBS" type="Text" width="15" desc="Defines term as order only, observation only, or both. A fourth category, Subset, is used for terms that are subsets of a panel but do not represent a package that is known to be orderable. We have defined them only to make it easier to maintain panels or other sets within the LOINC construct. This field reflects our best approximation of the terms intended use; it is not to be considered normative or a binding resolution. "/>
        <row n="31" name="CDISC_COMMON_TESTS" type="Text" width="1" desc="“Y” in this field means that the term is a part of subset of terms used by CDISC in clinical trials. "/>
        <row n="32" name="HL7_FIELD_SUBFIELD_ID" type="Text" width="50" desc="A value in this field means that the content should be delivered in the named field/subfield of the HL7 message. When NULL, the data for this data element should be sent in an OBX segment with this LOINC code stored in OBX-3 and with the value in the OBX-5."/>
        <row n="33" name="EXTERNAL_COPYRIGHT_NOTICE" type="Memo" width="" desc="External copyright holders copyright notice for this LOINC code. "/>
        <row n="34" name="EXAMPLE_UNITS" type="Text" width="255" desc="This field is populated with a combination of submitters units and units that people have sent us. Its purpose is to show users representative, but not necessarily recommended, units in which data could be sent for this term. "/>
        <row n="35" name="LONG_COMMON_NAME" type="Text" width="255" desc="This field contains the LOINC term in a more readable format than the fully specified name. The long common names have been created via a table driven algorithmic process. Most abbreviations and acronyms that are used in the LOINC database have been fully spelled out in English. "/>
        <row n="30" name="UnitsAndRange" type="Memo" width="-" desc="Units of measure (expressed using UCUM units) and normal ranges for physical quantities and survey scores. Intended as tailorable starter sets for applications that use LOINC forms as a way to capture data. Units are separated from normal ranges by a colon (:) and sets of unit:normal range pairs are separated by a semi-colon (;). Syntax for the normal range includes square brackets, which mean that the number adjacent to the bracket is included, and parentheses, which means that the number itself is not included. For example, [2,4] means “two to four”, while [2,4) means “two to less than four” and (2,4) means “between two and four but does not include two and four”."/>
        <row n="36" name="HL7_V2_DATATYPE" type="Text" width="255" desc="HL7 version 2.x data type that would be sent in OBX-2 when this data is delivered in an HL7 message. "/>
        <row n="37" name="HL7_V3_DATATYPE" type="Text" width="255" desc="HL7 version 3.0 data type that is compatible with this LOINC code. "/>
        <row n="38" name="CURATED_RANGE_AND_UNITS" type="Memo" width="" desc="A curated list of normal ranges and associated units (expressed as near UCUM codes) for physical quantities and survey scores. Intended as tailorable starter sets for applications that use LOINC forms as a way to capture data. Units are separated from normal ranges by XXX and sets of normal range/units pairs are separated by YYY. "/>
        <row n="39" name="DOCUMENT_SECTION" type="Text" width="255" desc="Classification of whether this LOINC code can be used a full document, a section of a document, or both. This field was created in the context of HL7 CDA messaging, and populated in collaboration with the HL7 Structured Documents Work 95 LOINC® Users’ Guide – December 2014  Group. "/>
        <row n="40" name="EXAMPLE_UCUM_UNITS" type="Text" width="255" desc="The Unified Code for Units of Measure (UCUM) is a code system intended to include all units of measures being contemporarily used in international science, engineering, and business. (www.unitsofmeasure.org ) This field contains example units of measures for this term expressed as UCUM units.  "/>
        <row n="41" name="EXAMPLE_SI_UCUM_UNITS" type="Text" width="255" desc="The Unified Code for Units of Measure (UCUM) is a code system intended to include all units of measures being contemporarily used in international science, engineering, and business. (www.unitsofmeasure.org) This field contains example units of measures for this term expressed as SI UCUM units.  "/>
        <row n="42" name="STATUS_REASON" type="Text" width="9" desc="Classification of the reason for concept status. This field will be Null for ACTIVE concepts, and optionally populated for terms in other status where the reason is clear. DEPRECATED or DISCOURAGED terms may take values of: AMBIGUOUS, DUPLICATE, or ERRONEOUS. "/>
        <row n="43" name="STATUS_TEXT" type="Memo" width="" desc="Explanation of concept status in narrative text. This field will be Null for ACTIVE concepts, and optionally populated for terms in other status. "/>
        <row n="43" name="PanelType" type="Text" width="255" desc="Describes a panel as a &quot;Convenience group&quot;, &quot;Organizer&quot;, or &quot;Panel&quot;. A &quot;Panel&quot; is an enumerated set of terms that are used together in direct clinical care. The package would typically be thought of as a single orderable item that contains a set of reported observations. A &quot;Convenience group&quot; is an enumerated set of terms used for a common purpose, but not typically orderable as a single unit. An &quot;Organizer&quot; is a subpanel (i.e. a child) within another panel that is only used to group together a set of terms, but is not an independently used entity. They often represent a header in a form, or serve as a navigation concept."/>
        <row n="44" name="CHANGE_REASON_PUBLIC" type="Memo" width="" desc="Detailed explanation about special changes to the term over time. "/>
        <row n="44" name="AskAtOrderEntry" type="Text" width="255" desc="A multivalued field, semicolon delimited list of LOINC codes that represent optional Ask at Order Entry (AOE) observations for a clinical observation or laboratory test. A LOINC term in this field may represent a single AOE observation or a panel containing several AOE observations."/>
        <row n="45" name="COMMON_TEST_RANK" type="Number" width="" desc="Ranking of approximately 2000 common tests performed by laboratories in USA."/>
        <row n="45" name="AssociatedObservations" type="Text" width="50" desc="A multi-valued, semicolon delimited list of LOINC codes that represent optional associated observation(s) for a clinical observation or laboratory test. A LOINC term in this field may represent a single associated observation or panel containing several associated observations."/>
        <row n="46" name="COMMON_ORDER_RANK" type="Number" width="" desc="Ranking of approximately 300 common orders performed by laboratories in USA."/>
        <row n="47" name="COMMON_SI_TEST_RANK" type="Number" width="" desc="Corresponding SI terms for 2000 common tests performed by laboratories in USA."/>
        <row n="48" name="HL7_ATTACHMENT_STRUCTURE" type="Text" width="15" desc="This field will be populated in collaboration with the HL7 Attachments Work Group as described in the HL7 Attachment Specification: Supplement to Consolidated CDA Templated Guide. Text will either be STRUCTURED or UNSTRUCTURED for relevant terms. The STRUCTURED terms are the allowed document type codes in the Consolidated CDA (C-CDA) Implementation guide. UNSTRUCTURED terms are approved by the HL7 Attachments WG for transmission using the Unstructured Document template of the C-CDA."/>
        <row n="49" name="EXTERNAL_COPYRIGHT_LINK" type="Text" width="255" desc="For terms that have a third party copyright, this field is populated with the COPYRIGHT_ID from the Source Organization table (see Table 29c below). It links a external copyright statement to a term."/>
        <row n="50" name="PanelType" type="Text" width="50" desc="Describes a panel as a “Convenience group”, “Organizer”, or “Panel”. A “Panel” is an enumerated set of terms that are used together in direct clinical care. The package would typically be thought of as a single orderable item that contains a set of reported observations. A “Convenience group” is an enumerated set of terms used for a common purpose, but not typically orderable as a single unit. An “Organizer” is a subpanel (i.e., a child) within another panel that is only used to group together a set of terms, but is not an independently used entity. They often represent a header in a form, or serve as a navigation concept."/>
        <row n="51" name="AskAtOrderEntry" type="Text" width="255" desc="A multi-valued, semicolon delimited list of LOINC codes that represent optional Ask at Order Entry (AOE) observations for a clinical observation or laboratory test. A LOINC term in this field may represent a single AOE observation or a panel containing several AOE observations."/>
        <row n="52" name="AssociatedObservations" type="Text" width="255" desc="A multi-valued, semicolon delimited list of LOINC codes that represent optional associated observation(s) for a clinical observation or laboratory test. A LOINC term in this field may represent a single associated observation or panel containing several associated observations."/>
        <row n="53" name="VersionFirstReleased" type="Text" width="10" desc="The LOINC version number in which the record was first released. For oldest records where the version released number is not known, this field will be null."/>
        <row n="54" name="ValidHL7AttachmentRequest" type="Text" width="50" desc="A value of ‘Y’ in this field indicates that this LOINC code can be sent by a payer as part of an HL7 attachment request for additional information."/>
    </xsl:variable>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="/">
        <xsl:message>
            <xsl:text>Processing </xsl:text>
            <xsl:value-of select="$codeSystemName"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="$codeSystemVersion"/>
            <xsl:if test="xs:integer($maxConcepts) gt 0">
                <xsl:text> [first </xsl:text>
                <xsl:value-of select="$maxConcepts"/>
                <xsl:text> concepts]</xsl:text>
            </xsl:if>
            <xsl:text> …</xsl:text>
        </xsl:message>
        <xsl:apply-templates select="loinc_db"/>
    </xsl:template>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="loinc_db">
        <xsl:processing-instruction name="xml-model">href="http://hl7.org/fhir/STU3/codesystem.sch" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"</xsl:processing-instruction>
        <CodeSystem xmlns="http://hl7.org/fhir" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://hl7.org/fhir http://hl7.org/fhir/STU3/codesystem.xsd">
            <id value="loinc"/>
            <language value="en-US"/>
            <text>
                <status value="additional"/>
                <div xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="$codeSystemName"/><xsl:text> </xsl:text><xsl:value-of select="$codeSystemVersion"/></div>
            </text>
            <url value="http://loinc.org"/>
            <!--<identifier>
                <system></system>
                <value></value>
            </identifier>-->
            <version value="{$codeSystemVersion}"/>
            <name value="{$codeSystemName}"/>
            <title value="{$codeSystemName}"/>
            <status value="{$status}"/>
            <experimental value="false"/>
            <date value="{$publisherDate}"/>
            <publisher value="{$publisher}"/>
            <contact>
                <name value="{$publisher}"/>
                <telecom>
                    <system value="url"/>
                    <value value="{$publisherUrl}"/>
                    <use value="work"/>
                </telecom>
            </contact>
            <description value="The international standard for identifying health measurements, observations, and documents."/>
            <!--<purpose/>-->
            <copyright value="{$copyright}"/>
            <caseSensitive value="false"/>
            <valueSet value="{$codeSystemValueSet}"/>
            <hierarchyMeaning value="grouped-by"/>
            <compositional value="false"/>
            <versionNeeded value="false"/>
            <content value="complete"/>
            <count value="{if (xs:integer($maxConcepts) gt 0) then $maxConcepts else count(concept)}"/>
            <!-- Filters we would allow on this codeSystem to build value sets with -->
            <!--<filter>
                <code value="status"/>
                <description value="The result of the filter are concepts that have a property code=STATUS. Matching is not case sensitive"/>
                <operator value="="/>
                <value value="A status code like ACTIVE, DEPRECATED, DISCOURAGED, TRIAL"/>
            </filter>-->
            <xsl:for-each select="'STATUS', 'COMPONENT', 'PROPERTY', 'TIME_ASPCT', 'SYSTEM', 'SCALE_TYP', 'METHOD_TYP', 'CLASS', 'CONSUMER_NAME', 'CLASSTYPE', 'ORDER_OBS', 'DOCUMENT_SECTION'">
                <xsl:variable name="colName" select="."/>
                <xsl:variable name="colInfo" select="$columnMap[@name = $colName]"/>
                <filter>
                    <code value="{$colName}"/>
                    <description value="{$colInfo/@desc}. The result of the filter are concepts that have a property code={$colName} or code that matches {$colName}. Matching is not case sensitive"/>
                    <operator value="="/>
                    <operator value="regex"/>
                    <value value="A term, or multiple terms."/>
                </filter>
            </xsl:for-each>
            <filter>
                <code value="copyright"/>
                <description value="Allows for the inclusion or exclusion of LOINC codes that include 3rd party copyright notices. LOINC = only codes with a sole copyright by Regenstrief. 3rdParty = only codes with a 3rd party copyright in addition to the one from Regenstrief."/>
                <operator value="="/>
                <value value="LOINC | 3rdParty"/>
            </filter>
            <!--<xsl:for-each select="distinct-values(concept/@status)">
                <property>
                    <code value="{.}"/>
                    <uri value="http://loinc.org/fhir/concept-status#{.}"/>
                    <description value="True if the concept is considered {.}"/>
                    <type value="boolean"/>
                </property>
            </xsl:for-each>-->
            <property>
                <code value="MAP_TO"/>
                <uri value="http://loinc.org/fhir/concept-mapto"/>
                <description value="value contains a concept id for a concept that this concept maps to"/>
                <type value="string"/>
            </property>
            <!--<property>
                <code value="MAP_FROM"/>
                <uri value="http://loinc.org/fhir/concept-mapfrom"/>
                <description value="value contains a concept id for a concept that maps to this concept"/>
                <type value="string"/>
            </property>-->
            <!-- Additional information supplied about each concept -->
            <xsl:for-each select="distinct-values(concept//@name)">
                <xsl:variable name="colName" select="."/>
                <xsl:choose>
                    <xsl:when test=". = ('RELATEDNAMES2', 'SHORTNAME', 'LONG_COMMON_NAME', 'LOINC_NUM')"/>
                    <xsl:otherwise>
                        <xsl:variable name="colInfo" select="$columnMap[@name = $colName][last()]" as="element()"/>
                        <property>
                            <code value="{.}"/>
                            <uri value="http://loinc.org/fhir/concept-properties#{.}"/>
                            <description value="{$colInfo/@desc}"/>
                            <type value="{local:loincDtToFHIRDt($colInfo/@type)}"/>
                        </property>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
            <xsl:choose>
                <xsl:when test="xs:integer($maxConcepts) gt 0">
                    <xsl:apply-templates select="concept[position() le $maxConcepts]"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="concept"/>
                </xsl:otherwise>
            </xsl:choose>
        </CodeSystem>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>Do a concept</xd:desc>
    </xd:doc>
    <xsl:template match="concept">
        <xsl:variable name="loinc_num" select="@loinc_num"/>
        <xsl:variable name="display" select="longName"/>
        <concept xmlns="http://hl7.org/fhir">
            <code value="{$loinc_num}"/>
            <display value="{$display}"/>
            <!--<definition/>-->
            <xsl:apply-templates select="shortName" mode="designationSYN"/>
            <xsl:apply-templates select="relatedNames2" mode="designationSYN"/>
            <xsl:apply-templates select="concept/longName" mode="designationFSN"/>
            <xsl:apply-templates select="concept/shortName" mode="designationSYN"/>
            <xsl:apply-templates select="concept/relatedNames2" mode="designationSYN"/>
            <!--<property>
                <code value="{@status}"/>
                <valueBoolean value="true"/>
            </property>-->
            <!-- <map from="1009-0" to="1007-4"/> -->
            <xsl:apply-templates select="map" mode="property">
                <xsl:with-param name="loinc_num" select="$loinc_num"/>
            </xsl:apply-templates>
            <xsl:apply-templates select="*" mode="property"/>
            <xsl:apply-templates select="concept/*" mode="property"/>
        </concept>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>Do RELATEDNAMES2 as designations of type synonym splitting by semicolon.</xd:desc>
    </xd:doc>
    <xsl:template match="relatedNames2" mode="designationSYN">
        <xsl:variable name="language" select="(parent::concept/@language, 'en-US')[1]"/>
        <xsl:for-each select="tokenize(., ';\s*')">
            <xsl:if test="string-length(.) gt 0">
                <designation xmlns="http://hl7.org/fhir">
                    <language value="{$language}"/>
                    <!-- Synonym -->
                    <use>
                        <system value="http://snomed.info/sct"/>
                        <code value="900000000000013009"/>
                    </use>
                    <value value="{.}"/>
                </designation>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>Do SHORTNAME as designation of type synonym</xd:desc>
    </xd:doc>
    <xsl:template match="shortName" mode="designationSYN">
        <xsl:variable name="language" select="(parent::concept/@language, 'en-US')[1]"/>
        <designation xmlns="http://hl7.org/fhir">
            <language value="{$language}"/>
            <!-- Synonym -->
            <use>
                <system value="http://snomed.info/sct"/>
                <code value="900000000000013009"/>
            </use>
            <value value="{.}"/>
        </designation>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>Do LONG_COMMON_NAME as designation of type fully specified name</xd:desc>
    </xd:doc>
    <xsl:template match="longName" mode="designationFSN">
        <xsl:variable name="language" select="(parent::concept/@language, 'en-US')[1]"/>
        <designation xmlns="http://hl7.org/fhir">
            <language value="{$language}"/>
            <!-- Fully specified name -->
            <use>
                <system value="http://snomed.info/sct"/>
                <code value="900000000000003001"/>
            </use>
            <value value="{.}"/>
        </designation>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>Do map[@from][@to] as MAP_TO property if current concept maps to another. Skip if this map is to current concept.</xd:desc>
        <xd:param name="loinc_num"/>
    </xd:doc>
    <xsl:template match="map" mode="property">
        <xsl:param name="loinc_num" select="ancestor-or-self::concept/@loinc_num"/>
        <xsl:choose>
            <xsl:when test="@from = $loinc_num">
                <property>
                    <code value="MAP_TO"/>
                    <valueString value="{@to}"/>
                </property>
            </xsl:when>
            <xsl:when test="@to = $loinc_num">
                <!--<property>
                    <code value="MAP_FROM"/>
                    <valueString value="{@from}"/>
                </property>-->
            </xsl:when>
            <xsl:otherwise>
                <!-- Huh? We have a map where 'to' nor 'from' equals our concept? -->
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>Do any column as property. <xd:b>FIXME</xd:b> Add language element with language if applicable. Skip elements map, concept, relatedNames2, shortName, longName, and LOINC_NUM as they are handled elsewhere</xd:desc>
        <xd:param name="language"/>
    </xd:doc>
    <xsl:template match="*" mode="property">
        <xsl:param name="language" select="parent::concept/@language" as="xs:string?"/>
        
        <xsl:choose>
            <xsl:when test="self::map | self::concept | self::relatedNames2 | self::shortName | self::longName | .[@name = 'LOINC_NUM']"/>
            <xsl:otherwise>
                <xsl:variable name="colName" select="@name"/>
                <xsl:variable name="colInfo" select="$columnMap[@name = $colName][last()]" as="element()"/>
                <xsl:variable name="datatype" select="local:loincDtToFHIRDt($colInfo/@type)"/>
                <property xmlns="http://hl7.org/fhir">
                    <xsl:if test="$language">
                        <language value="{$language}"/>
                    </xsl:if>
                    <code value="{@name}"/>
                    <xsl:choose>
                        <xsl:when test="$datatype = 'string'">
                            <valueString value="{.}"/>
                        </xsl:when>
                        <xsl:when test="$datatype = 'code'">
                            <valueCode value="{.}"/>
                        </xsl:when>
                        <xsl:when test="$datatype = 'integer'">
                            <valueInteger value="{.}"/>
                        </xsl:when>
                        <xsl:when test="$datatype = 'boolean'">
                            <valueBoolean value="{.}"/>
                        </xsl:when>
                        <xsl:when test="$datatype = 'dateTime'">
                            <valueDateTime value="{.}"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:message terminate="yes">LOINC concept '<xsl:value-of select="ancestor-or-self::concept/@loinc_num"/>' column '<xsl:value-of select="$colName"/>' has unsupported type value '<xsl:value-of select="$colInfo/@type"/>'. It was mapped to '<xsl:value-of select="$datatype"/>'</xsl:message>
                        </xsl:otherwise>
                    </xsl:choose>
                </property>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>Get FHIR CodeSystem.property.type based on LOINC column datatype [Text, Memo, Date/Time, Number]. The property type determines the datatype of the property value. Currently we always return 'string', but FHIR supports: <xd:ul>
                <xd:li>code</xd:li>
                <xd:li>Coding</xd:li>
                <xd:li>string</xd:li>
                <xd:li>integer</xd:li>
                <xd:li>boolean</xd:li>
                <xd:li>dateTime</xd:li>
            </xd:ul></xd:desc>
        <xd:param name="in"/>
    </xd:doc>
    <xsl:function name="local:loincDtToFHIRDt">
        <xsl:param name="in"/>
        
        <xsl:choose>
            <xsl:when test="$in = 'Text'">string</xsl:when>
            <xsl:when test="$in = 'Memo'">string</xsl:when>
            <xsl:when test="$in = 'Date/Time'">string</xsl:when>
            <xsl:when test="$in = 'Number'">string</xsl:when>
            <xsl:otherwise>string</xsl:otherwise>
        </xsl:choose>
    </xsl:function>
</xsl:stylesheet>