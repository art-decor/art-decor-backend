<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Copyright: to be decided, until then all rights reserved 
    
    Output: 
-->
<xsl:stylesheet version="2.0" xmlns:fun="http://art-decor.org/fun"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://hl7.org/fhir" exclude-result-prefixes="#all">
    <xsl:import href="fhir-common.xsl"/>
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:variable name="uri" select="concat($projectDiskRoot, 'map/map-', //@transactionRef, '--', translate(//@transactionEffectiveDate, 'T:-', ''), '.xml')"/>
    <xsl:variable name="map" select="doc($uri)"/>
    <xsl:key name="target" match="target" use="../source/@id"></xsl:key>
    
    <xsl:template match="/">
        <xsl:for-each select="//*[@transactionRef]">
            <xsl:variable name="href" select="concat($projectDiskRoot, 'QuestionnaireResponse/QR-', @shortName, '-', @id, '.xml')"/>
            <xsl:result-document href="{$href}">
                <xsl:apply-templates select="."/>
            </xsl:result-document>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="*[@transactionRef]">
        <QuestionnaireResponse xmlns="http://hl7.org/fhir" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <id value="{@id}"/>
            <meta>
                <profile value="http://www.iknl.fhir/mdo_verslag_diagnose_20161112"/>
            </meta>
            <language value="nl-NL"/>
            <!-- Example data -->
            <contained>
                <Patient>
                    <id value="patient1"/>
                    <identifier>
                        <system value="http://www.testziekenhuis.nl/patientnummers"/>
                        <value value="123456"/>
                    </identifier>
                    <gender value="female"/>
                    <birthDate value="1990-12-31"/>
                </Patient>
            </contained>
            <contained>
                <Organization>
                    <id value="organization1"/>
                    <name value="Test Laboratorium"/>
                </Organization>
            </contained>
            <contained>
                <Device>
                    <id value="device1"/>
                    <owner>
                        <reference value="#organization1"/>
                    </owner>
                </Device>
            </contained>
            <identifier>
                <system value="http://www.testziekenhuis.nl/mdo-verslagnummers"/>
                <value value="123456"/>
            </identifier>
            <questionnaire>
                <reference value="{$fhirCanonicalBase}Questionnaire/Q_{@shortName}"/>
            </questionnaire>
            <status value="completed"/>
            <subject>
                <reference value="#patient1"/>
            </subject>
            <author>
                <reference value="#device1"/>
            </author>
            <!-- TODO: tijdelijke oplossing om patient niet mee te nemen -->
            <xsl:apply-templates select="*[not(@conceptId=('2.16.840.1.113883.2.4.3.11.31.1.77.2.2.1', '2.16.840.1.113883.2.4.3.11.31.1.77.2.2.10'))]"/>
        </QuestionnaireResponse>
    </xsl:template>

    <xsl:template match="*[@conceptId][not(@value)][not(@hidden)]">
        <xsl:variable name="conceptId" select="@conceptId/string()"/>
        <xsl:if test=".//@value">
            <item>
                <linkId value="{$map/key('target', $conceptId)/@id}"/>
                <definition value="{$map/key('target', $conceptId)/@url}"/>
                <text value="{$map/key('target', $conceptId)/@text}"/>
                <xsl:apply-templates select="*"/>
            </item>
        </xsl:if>
    </xsl:template>

    <xsl:template match="*[@conceptId][@value]">
        <xsl:variable name="conceptId" select="@conceptId/string()"/>
        <!-- Answer are grouped in item, so we make <item> only for the first one -->
        <xsl:if test="not(preceding-sibling::*[@conceptId=$conceptId][not(@hidden)])">
            <item>
                <linkId value="{$map/key('target', $conceptId)/@id}"/>
                <definition value="{$map/key('target', $conceptId)/@url}"/>
                <text value="{$map/key('target', $conceptId)/@text}"/>
                <!-- Add answers for non-empty self and siblings with same concept id -->
                <xsl:for-each select="(self::* | following-sibling::*[@conceptId=$conceptId])[@value/string()]">
                    <answer>
                        <xsl:choose>
                            <xsl:when test="@code">
                                <valueCoding>
                                    <system value="{fun:codeSystemUrl(@codeSystem)}"/>
                                    <code value="{@code}"/>
                                    <display value="{@displayName}"/>
                                </valueCoding>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:variable name="answerType" select="concat('value', upper-case(substring($map/key('target', $conceptId)/@type, 1, 1)), substring($map/key('target', $conceptId)/@type, 2))"/> 
                                <xsl:element name="{$answerType}">
                                    <xsl:choose>
                                        <xsl:when test="$answerType = 'valueQuantity'">
                                            <xsl:element name="value">
                                                <xsl:copy-of select="@value"/>
                                            </xsl:element>
                                            <xsl:if test="@unit">
                                                <xsl:element name="unit">
                                                    <xsl:attribute name="value" select="@unit"/>
                                                </xsl:element>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:copy-of select="@value"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:element>
                            </xsl:otherwise>
                        </xsl:choose>
                    </answer>
                </xsl:for-each>
            </item>
        </xsl:if>            
    </xsl:template>

    <xsl:template match="@* | node()"/>

</xsl:stylesheet>
