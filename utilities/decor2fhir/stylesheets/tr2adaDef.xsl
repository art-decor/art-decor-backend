<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="dataset">
        <xsl:variable name="root" select="'../ada/projects/'"/>
        <xsl:variable name="path" select="concat(@shortName, '/definitions/', @shortName, '.xml')"/>
        <xsl:result-document href="{$root}{$path}">
            <ada xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../../../../ada/core/ada.xsd">
                <project prefix="{@shortName}-" language="{name[1]/@language}" versionDate="">
                    <release baseUri="http://decor.nictiz.nl/decor/services/RetrieveTransaction"/>
                </project>
                <applications>
                    <application version="1">
                        <views>
                            <view id="1" type="crud" target="xforms" transactionId="{@transactionId}" transactionEffectiveDate="{@transactionEffectiveDate}">
                                <name><xsl:value-of select="name[1]"/></name>
                                <concepts include="all"/>
                            </view>
                            <view id="2" type="index" target="xforms" transactionId="{@transactionId}" transactionEffectiveDate="{@transactionEffectiveDate}">
                                <name><xsl:value-of select="concat(name[1], ' Index')"/></name>
                                <indexOf ref="1"/>
                                <concepts include="only">
                                </concepts>
                                <controls>
                                    <button type="xml"/>
                                </controls>
                            </view>
                        </views>
                    </application>
                </applications>
            </ada>
        </xsl:result-document>
    </xsl:template>

    <xsl:template match="@*|node()"/>
</xsl:stylesheet>