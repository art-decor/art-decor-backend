<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Copyright: to be decided, until then all rights reserved 
    
    Output: 
-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fun="http://art-decor.org/fun" xmlns:fhir="http://hl7.org/fhir"
    xmlns="http://hl7.org/fhir" exclude-result-prefixes="#all" xmlns:sch="http://purl.oclc.org/dsdl/schematron">
    <xsl:import href="fhir-common.xsl"/>
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="fhir:Questionnaire">
        <sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
            <sch:ns uri="http://hl7.org/fhir" prefix="fhir"/>
            <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
            <sch:ns uri="http://www.w3.org/XML/1998/namespace" prefix="xml"/>
            
            <xsl:for-each select=".//fhir:item[fhir:linkId]">
                <sch:include href="{fhir:linkId/@value}.sch"/>
                <xsl:if test="fhir:item[fhir:required/@value='true']">
                    <sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="required-{fhir:linkId/@value}">
                        <sch:rule context="//fhir:item[fhir:linkId/@value='{fhir:linkId/@value}']">
                            <xsl:for-each select="fhir:item[fhir:required/@value='true']">
                                <xsl:variable name="requiredChildLinkId" select="fhir:linkId/@value/string()"/>
                                <sch:assert role="error" test="fhir:item/fhir:linkId/@value = '{$requiredChildLinkId}'"><xsl:value-of select="fhir:text/@value/string()"/>(<xsl:value-of select="$requiredChildLinkId"/>) must be present in <xsl:value-of select="../fhir:text/@value/string()"/> (<xsl:value-of select="../fhir:linkId/@value/string()"/>).</sch:assert>
                                <xsl:if test="fhir:type!='group'">
                                    <sch:assert role="error" test="fhir:item[fhir:linkId/@value = '{$requiredChildLinkId}']/fhir:answer"><xsl:value-of select="fhir:text/@value/string()"/>(<xsl:value-of select="$requiredChildLinkId"/>) must have an answer in <xsl:value-of select="../fhir:text/@value/string()"/> (<xsl:value-of select="../fhir:linkId/@value/string()"/>).</sch:assert>
                                </xsl:if>
                            </xsl:for-each>
                        </sch:rule>
                    </sch:pattern>
                </xsl:if>
                <xsl:if test="fhir:item[fhir:repeats/@value='false']">
                    <sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron" id="repeats-{fhir:linkId/@value}">
                        <sch:rule context="//fhir:item[fhir:linkId/@value='{fhir:linkId/@value}']">
                            <xsl:for-each select="fhir:item[fhir:repeats/@value='false']">
                                <xsl:variable name="repeatsFalseLinkId" select="fhir:linkId/@value/string()"/>
                                <xsl:choose>
                                    <xsl:when test="fhir:type='group'">
                                        <sch:assert role="error" test="not(fhir:item[fhir:linkId/@value = '{$repeatsFalseLinkId}'][2])"><xsl:value-of select="fhir:text/@value/string()"/>(<xsl:value-of select="$repeatsFalseLinkId"/>) may occur at most once in <xsl:value-of select="../fhir:text/@value/string()"/> (<xsl:value-of select="../fhir:linkId/@value/string()"/>).</sch:assert>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <sch:assert role="error" test="not(fhir:item[fhir:linkId/@value = '{$repeatsFalseLinkId}']/fhir:answer[2])"><xsl:value-of select="fhir:text/@value/string()"/>(<xsl:value-of select="$repeatsFalseLinkId"/>) may have at most one answer in <xsl:value-of select="../fhir:text/@value/string()"/> (<xsl:value-of select="../fhir:linkId/@value/string()"/>).</sch:assert>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:for-each>
                        </sch:rule>
                    </sch:pattern>
                </xsl:if>
            </xsl:for-each>
        </sch:schema>
    </xsl:template>

    <xsl:template match="@* | node()"/>

</xsl:stylesheet>
