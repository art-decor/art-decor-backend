<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fhir="http://hl7.org/fhir" xmlns="http://hl7.org/fhir" exclude-result-prefixes="fhir"
    version="2.0">
    <xsl:import href="fhir-common.xsl"/>
    <xsl:import href="xml-to-string.xsl"/>
    <xsl:output method="html" indent="yes" exclude-result-prefixes="#all"/>

    <xsl:template match="/">
        <xsl:apply-templates mode="overview"/>
    </xsl:template>

    <xsl:template match="map" mode="overview">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>
                    <xsl:value-of select="desc/string()"/>
                </title>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"></link>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
                <link href="../resources/css/default.css" rel="stylesheet" type="text/css"/>
                <style>
                    body {margin:8px;}
                    a[href] {color:blue;}
                    table {border-collapse: collapse; border: 1px solid black;}
                    td {border: 1px solid black; padding: 2px;}
                </style>
            </head>
            <body>
                <h1><xsl:value-of select="desc"/></h1>
                <table id="qTable">
                    <tbody>
                        <tr>
                            <th>Concept</th>
                            <th>DECOR id</th>
                            <th>DECOR effectiveDate</th>
                            <th>FHIR id</th>
                            <th>FHIR definition</th>
                            <th>FHIR type</th>
                        </tr>
                        <xsl:for-each select="group/mapping">
                            <tr>
                                <td><xsl:value-of select="source/@name"/></td>
                                <td><xsl:value-of select="source/@id"/></td>
                                <td><xsl:value-of select="source/@effectiveDate"/></td>
                                <td><xsl:value-of select="target/@id"/></td>
                                <td><xsl:value-of select="target/@url"/></td>
                                <td><xsl:value-of select="target/@type"/></td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="@* | node()" mode="overview"/> 

</xsl:stylesheet>
