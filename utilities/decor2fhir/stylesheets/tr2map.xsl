<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    
    XSLT to be included
-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fun="http://art-decor.org/fun" exclude-result-prefixes="#all">
    <xsl:include href="fhir-common.xsl"/>
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="/">
        <xsl:apply-templates mode="map"/>
    </xsl:template>

    <xsl:template match="dataset" mode="map">
        <map effectiveTime="{current-dateTime()}" statusCode="draft">
            <desc>Map for <xsl:value-of select="name"/></desc>
            <source>
                <xsl:copy-of select="@*"/>
            </source>
            <target>FHIR</target>
            <group>
                <xsl:apply-templates mode="map" select="//concept[@type = ('group', 'item')]"/>
            </group>
        </map>
    </xsl:template>

    <xsl:template match="concept" mode="map">
        <mapping>
            <xsl:comment><xsl:value-of select="name"/></xsl:comment>
            <source>
                <xsl:copy-of select="@id, @effectiveDate, @iddisplay"/>
                <xsl:attribute name="name" select="name"/>
            </source>
            <target>
                <xsl:choose>
                    <xsl:when test="$fhirIdStyle = 'idDate'">
                        <xsl:variable name="fhirId" select="concat(@id, '--', translate(@effectiveDate, 'T:-', ''))"/>
                        <xsl:attribute name="id" select="$fhirId"/>
                        <xsl:attribute name="url" select="concat($fhirCanonicalBase, 'DataElement/', $fhirId)"/>
                    </xsl:when>
                    <xsl:when test="$fhirIdStyle = 'idDisplay'">
                        <xsl:variable name="fhirId" select="@iddisplay"/>
                        <xsl:attribute name="id" select="$fhirId"/>
                        <xsl:attribute name="url" select="concat($fhirCanonicalBase, 'DataElement/', $fhirId)"/>
                    </xsl:when>
                    <xsl:when test="$fhirIdStyle = 'inheritId'">
                        <xsl:variable name="fhirId" select="if (inherit/@ref) then inherit/@ref else @id"/>
                        <xsl:attribute name="id" select="$fhirId"/>
                        <xsl:attribute name="url" select="concat($fhirCanonicalBase, 'DataElement/', $fhirId)"/>
                    </xsl:when>
                    <xsl:when test="$fhirIdStyle = 'inheritIdDisplay'">
                        <xsl:variable name="fhirId" select="if (inherit/@refdisplay) then inherit/@refdisplay else @iddisplay"/>
                        <xsl:attribute name="id" select="$fhirId"/>
                        <xsl:attribute name="url" select="concat($fhirCanonicalBase, 'DataElement/', $fhirId)"/>
                    </xsl:when>
                </xsl:choose>
                <xsl:attribute name="text" select="name"/>
                <xsl:attribute name="type" select="fun:decor2questionnaireType(.)"/>
            </target>
        </mapping>
    </xsl:template>

    <xsl:template match="@* | node()"/>

</xsl:stylesheet>
