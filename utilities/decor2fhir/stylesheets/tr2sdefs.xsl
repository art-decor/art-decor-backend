<?xml version="1.0" encoding="UTF-8"?>
<!-- 
-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:fun="http://art-decor.org/fun" xmlns="http://hl7.org/fhir" exclude-result-prefixes="#all">
    <xsl:import href="fhir-common.xsl"/>
    <xsl:output method="xml" indent="yes"/>
    <xsl:variable name="mapUri" select="concat($projectDiskRoot, 'map/map-', //dataset/@shortName, '.xml')"/>
    <xsl:variable name="map" select="doc($mapUri)"/>
    <xsl:variable name="configUri" select="concat($projectDiskRoot, 'config/files.xml')"/>
    <xsl:variable name="config" select="doc($configUri)"/>
    <xsl:key name="target" match="target" use="../source/@id"></xsl:key>
    
    <xsl:template match="/">
        <xsl:apply-templates/>
        <xsl:call-template name="generic-templates"/>
    </xsl:template>

    <xsl:template match="dataset">
        <xsl:variable name="transactionId" select="@transactionId/string()"/>
        <xsl:variable name="targets" select="$config//file[id=$transactionId]/targets/id/text()"/>
        <xsl:if test="count($targets) = 0">
            <xsl:message>ERROR: no targets in files.xml for this transaction</xsl:message>
        </xsl:if>
        <xsl:variable name="datasetRoot" select="."/>
        <xsl:for-each select="$targets">
            <xsl:variable name="conceptId" select="."/>
            <xsl:message select="$conceptId"></xsl:message>
            <xsl:apply-templates select="$datasetRoot//concept[@id=$conceptId]"/>
        </xsl:for-each>
        <xsl:call-template name="index">
            <xsl:with-param name="targets" select="$targets"/>
            <xsl:with-param name="datasetRoot" select="$datasetRoot"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="concept[@type='item' or @type = 'group']">
        <xsl:variable name="id" select="@id"/>
        <xsl:variable name="targetId" select="$map/key('target', $id)/@id"/>
        <xsl:variable name="doc">
            <StructureDefinition xmlns="http://hl7.org/fhir">
                <id value="{$targetId}"/>
                <language value="nl"/>
                <url value="http://decor.nictiz.nl/fhir/StructureDefinition/{$targetId}"/>
                <identifier>
                    <use value="official"/>
                    <system value="http://art-decor.org/ns/oids/sd"/>
                    <value value="{$targetId}"/>
                </identifier>
                <name value="{name}"/>
                <status value="draft"/>
                <description value="{if (desc/text()) then desc else name}"/>
                <fhirVersion value="3.0.0"/>
                <kind value="resource"/>
                <abstract value="false"/>
                <type value="Observation"/>
                <baseDefinition value="http://hl7.org/fhir/StructureDefinition/Observation"/>
                <derivation value="constraint"/>
                <differential>
                    <element id="Observation">
                        <path value="Observation"/>
                    </element>
                    <element id="Observation.code.coding.system">
                        <path value="Observation.code.coding.system"/>
                        <fixedUri value="http://decor.nictiz.nl/ns/oids/de"/>
                    </element>
                    <element id="Observation.code.coding.code">
                        <path value="Observation.code.coding.code"/>
                        <fixedCode value="{$targetId}"/>
                    </element>
                    <element id="Observation.subject">
                        <path value="Observation.subject"/>
                        <min value="1"/>
                        <type>
                            <code value="Reference"/>
                            <profile value="http://fhir.nl/fhir/STU3/StructureDefinition/nl-core-patient"/>
                            <targetProfile value="http://hl7.org/fhir/StructureDefinition/Patient"/>
                            <aggregation value="contained"/>
                        </type>
                    </element>
                    <xsl:if test="@type='group'">
                        <element id="Observation.related">
                            <path value="Observation.related"/>
                            <slicing>
                                <discriminator>
                                    <type value="value"/>
                                    <path value="target.code"/>
                                </discriminator>
                                <rules value="closed"/>
                            </slicing>
                        </element>
                        <xsl:for-each select="concept">
                            <xsl:variable name="childId" select="@id"/>
                            <xsl:variable name="childTargetId" select="$map/key('target', $childId)/@id"/>
                            <element id="related_{$childTargetId}">
                                <path value="Observation.related"/>
                                <!-- eld-16 says minus cannot be part of sliceName - probably FHIR bug but we need to fix it-->
                                <sliceName value="slice_{replace($childTargetId, '-', '_')}"/>
                                <min value="{@minimumMultiplicity}"/>
                                <max value="{@maximumMultiplicity}"/>
                            </element>
                        </xsl:for-each>
                    </xsl:if>
                    <xsl:if test="@type='item'">
                        <xsl:choose>
                            <xsl:when test="valueDomain/@type='code'">
                                <element id="Element">
                                    <path value="Observation.valueCodeableConcept"/>
                                    <min value="1"/>
                                    <type>
                                        <code value="CodeableConcept"/>
                                    </type>
                                    <binding>
                                        <strength value="required"/>
                                        <valueSetUri value="http://decor.nictiz.nl/fhir/ValueSet/{valueSet/@id/string()}--{valueSet/@effectiveDate/string()}"/>
                                    </binding>
                                </element>
                            </xsl:when>
                            <xsl:when test="valueDomain/@type='quantity'">
                                <element id="Element">
                                    <path value="Observation.valueQuantity"/>
                                    <min value="1"/>
                                    <type>
                                        <code value="Quantity"/>
                                    </type>
                                </element>
                            </xsl:when>
                            <xsl:when test="valueDomain/@type=('count', 'blob', 'complex', 'currency', 'date', 'decimal', 'identifier')">
                                <element id="Element">
                                    <xsl:comment>DECOR type <xsl:value-of select="valueDomain/@type"/> not supported in FHIR observations</xsl:comment>
                                </element>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:variable name="fhirType" select="fun:decor2observationType(.)"/>
                                <element id="Element">
                                    <path value="{concat('Observation.value', upper-case(substring($fhirType, 1, 1)), substring($fhirType, 2))}"/>
                                    <min value="1"/>
                                    <type>
                                        <code value="{$fhirType}"/>
                                    </type>
                                </element>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:if>
                </differential>
            </StructureDefinition>
        </xsl:variable>
        
        <xsl:variable name="href" select="concat($projectDiskRoot, 'StructureDefinition/', $targetId, '.xml')"/>
        <xsl:result-document href="{$href}" method="xml">
            <xsl:copy-of select="$doc"/>
        </xsl:result-document>
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template name="index">
        <xsl:param name="targets"/>
        <xsl:param name="datasetRoot"/>
        <xsl:variable name="href" select="concat($projectDiskRoot, $datasetRoot/@shortName/string(), '-index.html')"/>
        <xsl:variable name="body-maps" select="doc(concat($projectDiskRoot, 'body-maps.html'))"/>
        <xsl:variable name="body-valuesets" select="doc(concat($projectDiskRoot, 'body-valuesets.html'))"/>
        <xsl:result-document href="{$href}" method="html">
            <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <title>Index</title>
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"></link>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
                    <link href="resources/css/default.css" rel="stylesheet" type="text/css"/>
                    <style>
                        body {margin:8px;}
                        a[href] {color:blue;}
                        table {border-collapse: collapse; border: 1px solid black;}
                        td {border: 1px solid black; padding: 2px;}
                    </style>
                </head>
                <body>
                    <h1>Structure Definitions</h1>
                    <table id="stdefs">
                        <tbody>
                            <tr>
                                <th>DECOR concept</th>
                                <th>FHIR Structure Definition</th>
                            </tr>
                            <xsl:message select="/dataset/name/string()"/>
                            <tr>
                                <td><xsl:value-of select="/dataset/name/string()"/></td>
                                <td><a href="{concat('StructureDefinition/', /dataset/@iddisplay/string(), '.xml')}"><xsl:value-of select="/dataset/@iddisplay/string()"/>.xml</a></td>
                            </tr>
                            <xsl:for-each select="$targets">
                                <xsl:variable name="conceptId" select="."/>
                                <xsl:for-each select="$datasetRoot//concept[@id=$conceptId]">
                                    <xsl:variable name="targetId" select="$map/key('target', $conceptId)/@id"/>
                                    <xsl:variable name="filename" select="concat($targetId, '.xml')"/>
                                    <tr>
                                        <td><xsl:value-of select="name"/></td>
                                        <td><a href="{concat('StructureDefinition/', $filename)}"><xsl:value-of select="$filename"/></a></td>
                                    </tr>
                                    <xsl:for-each select=".//concept[@type='item' or @type = 'group']">
                                        <xsl:variable name="childId" select="@id"/>
                                        <xsl:variable name="targetId" select="$map/key('target', $childId)/@id"/>
                                        <xsl:variable name="filename" select="concat($targetId, '.xml')"/>
                                        <tr>
                                            <td><xsl:value-of select="name"/></td>
                                            <td><a href="{concat('StructureDefinition/', $filename)}"><xsl:value-of select="$filename"/></a></td>
                                        </tr>
                                    </xsl:for-each>
                                </xsl:for-each>
                            </xsl:for-each>
                        </tbody>
                    </table>
                    <h1>Maps</h1>
                    <xsl:copy-of select="$body-maps"/>
                    <h1>Valuesets</h1>
                    <xsl:copy-of select="$body-valuesets"/>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>

    <xsl:template match="@* | node()"/>

    <xsl:template match="@codeSystem">
        <system value="{fun:codeSystemUrl(.)}"/>
    </xsl:template>

    <xsl:template name="generic-templates">
        <xsl:variable name="href" select="concat($projectDiskRoot, 'StructureDefinition/' ,/dataset/@iddisplay, '.xml')"/>
        <xsl:result-document href="{$href}" method="xml">
            <StructureDefinition xmlns="http://hl7.org/fhir">
                <id value="{/dataset/@iddisplay}"/>
                <url value="http://www.iknl.nl/fhir/StructureDefinition/{/dataset/@iddisplay}"/>
                <name value="{/dataset/name}"/>
                <status value="draft"/>
                <date value="{current-dateTime()}"/>
                <fhirVersion value="3.0.0"/>
                <kind value="resource"/>
                <abstract value="false"/>
                <type value="DiagnosticReport"/>
                <baseDefinition value="http://hl7.org/fhir/StructureDefinition/DiagnosticReport"/>
                <derivation value="constraint"/>
                <differential>
                    <element id="DiagnosticReport">
                        <path value="DiagnosticReport"/>
                    </element>
                    <element id="DiagnosticReport.identifier">
                        <path value="DiagnosticReport.identifier"/>
                        <sliceName value="TnummerVerslag"/>
                        <min value="1"/>
                        <max value="1"/>
                    </element>
                    <element id="DiagnosticReport.code.coding.system">
                        <path value="DiagnosticReport.code.coding.system"/>
                        <fixedUri value="http://decor.nictiz.nl/ns/oids/de"/>
                    </element>
                    <element id="DiagnosticReport.code.coding.code">
                        <path value="DiagnosticReport.code.coding.code"/>
                        <fixedCode value="{/dataset/@iddisplay}"/>
                    </element>
                    <element id="DiagnosticReport.subject">
                        <path value="DiagnosticReport.subject"/>
                        <min value="1"/>
                        <type>
                            <code value="Reference"/>
                            <profile value="http://fhir.nl/fhir/StructureDefinition/nl-core-patient"/>
                            <targetProfile value="http://hl7.org/fhir/StructureDefinition/Patient"/>
                        </type>
                    </element>
                    <element id="DiagnosticReport.performer.actor">
                        <path value="DiagnosticReport.performer.actor"/>
                        <type>
                            <code value="Reference"/>
                            <targetProfile value="http://hl7.org/fhir/StructureDefinition/Organization"/>
                        </type>
                    </element>
                </differential>
            </StructureDefinition>
        </xsl:result-document>
    </xsl:template>

</xsl:stylesheet>