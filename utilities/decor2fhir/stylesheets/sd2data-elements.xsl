<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Copyright: to be decided, until then all rights reserved 
    
    Output: 
-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fun="http://art-decor.org/fun" xmlns:fhir="http://hl7.org/fhir" xmlns="http://hl7.org/fhir"  exclude-result-prefixes="#all">
    <xsl:import href="fhir-common.xsl"/>
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:variable name="mapUri" select="concat($projectDiskRoot, 'map/map-', //fhir:StructureDefinition/fhir:id/@value/string(), '.xml')"/>
    <xsl:variable name="map" select="doc($mapUri)"/>
    <xsl:key name="target" match="target" use="../source/@id"></xsl:key>

    <xsl:template match="/">
        <xsl:apply-templates select="//fhir:snapshot/fhir:element"/>
    </xsl:template>

    <!-- Slicing is not supported yet. Must exclude those, since they will lead to duplicate DataElement files -->
    <xsl:template match="fhir:element[not(fhir:slicing or fhir:sliceName)]">
        <xsl:variable name="id">
            <xsl:value-of select="substring-before(@id, '--')"/>
        </xsl:variable>
        <xsl:variable name="shortDate" select="substring-after(@id, '--')"/>
        <xsl:variable name="effectiveDate"
            select="fun:longDate($shortDate)"/>
        <xsl:variable name="targetUrl" select="$map/key('target', $id)/@url"/>
        <xsl:variable name="targetId" select="$map/key('target', $id)/@id"/>
        <xsl:if test="$targetUrl != ''">
        <xsl:variable name="doc">
            <DataElement xmlns="http://hl7.org/fhir" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <id value="{tokenize($targetUrl, '/')[last()]}"/>
                <url value="{$targetUrl}"/>
                <identifier>
                    <use value="official"/>
                    <system value="http://art-decor.org/ns/oids/de"/>
                    <value value="{substring-before(@id, '--')}"/>
                </identifier>
                <version value="{$effectiveDate}"/>
                <!-- DSTU2: name - status, STU3: status - name -->
                <status value="draft"/>
                <publisher value="{$publisher}"/>
                <name value="{fhir:short/@value}"/>
                <xsl:element name="element">
                    <xsl:copy-of select="@*"/>
                    <!-- Don't copy min/max, those are taken from Questionnaire / StructureDefinition -->
                    <xsl:copy-of select="(* except (fhir:min, fhir:max))"/>
                </xsl:element>
            </DataElement>
        </xsl:variable>
        <xsl:variable name="href" select="concat($projectDiskRoot, 'DataElement/', $targetId, '.xml')"/>
        <xsl:result-document href="{$href}" method="xml">
            <xsl:copy-of select="$doc"></xsl:copy-of>
        </xsl:result-document>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="@* | node()"/>

</xsl:stylesheet>
