<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fhir="http://hl7.org/fhir" xmlns="http://hl7.org/fhir" exclude-result-prefixes="#all"
    version="2.0">
    <xsl:import href="fhir-common.xsl"/>
    <xsl:import href="xml-to-string.xsl"/>
    <xsl:output method="html" indent="yes" exclude-result-prefixes="#all"/>
    
    <xsl:template match="/">
        <xsl:apply-templates mode="overview"/>
    </xsl:template>
    
    <xsl:template match="fhir:Questionnaire" mode="overview">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>
                    <xsl:value-of select="fhir:title/@value/string()"/>
                </title>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"></link>
                
                <!--<link rel="stylesheet" href="http://hl7.org/fhir/2017Jan/assets/css/bootstrap-fhir.css"></link>
                <link rel="stylesheet" href="http://hl7.org/fhir/2017Jan/assets/css/project.css"></link>
                <link rel="stylesheet" href="http://hl7.org/fhir/2017Jan/assets/css/pygments-manni.css"></link>-->
                
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
                <script src="../resources/scripts/jquery.treetable.js" type="text/javascript"/>
                <link href="../resources/css/default.css" rel="stylesheet" type="text/css"/>
                <style>
                    body {margin:8px;}
                    a[href] {color:blue;}
                    table {border-collapse: collapse;}
                </style>
            </head>
            <body>
                <h1>Questionnaire <xsl:value-of select="fhir:title/@value/string()"/></h1>
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#overview">Overview</a></li>
                    <!--<li><a data-toggle="tab" href="#details">Details</a></li>-->
                    <li><a data-toggle="tab" href="#xml">XML</a></li>
                </ul>
                <div class="tab-content">
                    <div id="overview" class="tab-pane fade in active">
                        <h3>Overview</h3>
                        <table id="qTable">
                            <tbody>
                                <tr>
                                    <th>Item</th>
                                    <th>linkId</th>
                                    <th>Repeats</th>
                                    <th>Required</th>
                                    <th>Type</th>
                                    <th style="width:30%">Options</th>
                                </tr>
                                <xsl:apply-templates mode="overview"/>
                            </tbody>
                        </table>
                        <h3>Details</h3>
                        <xsl:apply-templates mode="details" select="//fhir:item"/>
                    </div>
                    <!--<div id="details" class="tab-pane fade">
                        <h3>Details</h3>
                        <xsl:apply-templates mode="details" select="//fhir:item"/>
                    </div>-->
                    <div id="xml" class="tab-pane fade">
                        <h3>XML</h3>
                        <pre style="color: black; font-weight: bold;">
                            <xsl:call-template name="xml-to-string"/>
                        </pre>
                    </div>
                </div>
            </body>
            <script>
                $("#qTable").treetable();
            </script>
        </html>
    </xsl:template>
    
    <xsl:template match="fhir:item" mode="overview">
        <tr data-tt-id="{fhir:linkId/@value/string()}">
            <xsl:if test="parent::fhir:item">
                <xsl:attribute name="data-tt-parent-id">
                    <xsl:value-of select="../fhir:linkId/@value/string()"/>
                </xsl:attribute>
            </xsl:if>
            <td>
                <xsl:choose>
                    <xsl:when test="fhir:type/@value = 'group'">
                        <img src="../resources/images/icon_element.gif"/>
                    </xsl:when>
                    <xsl:when test="fhir:type/@value = 'choice'">
                        <img src="../resources/images/icon_datatype.gif"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <img src="../resources/images/icon_primitive.png"/>
                    </xsl:otherwise>
                </xsl:choose>
                <a href="#{fhir:linkId/@value/string()}">
                    <xsl:value-of select="fhir:text/@value/string()"/>
                </a>
            </td>
            <td>
                <xsl:value-of select="fhir:linkId/@value/string()"/>
            </td>
            <td>
                <xsl:value-of select="fhir:repeats/@value/string()"/>
            </td>
            <td>
                <xsl:value-of select="fhir:required/@value/string()"/>
            </td>
            <td>
                <xsl:if test="fhir:type/@value != 'group'">
                    <xsl:value-of select="fhir:type/@value/string()"/>
                </xsl:if>
            </td>
            <td>
                <xsl:if test="fhir:option">
                    <xsl:variable name="options">
                        <xsl:for-each select="fhir:option/fhir:valueCoding">
                            <xsl:value-of select="fhir:code/@value/string()"/>
                            <xsl:text> (</xsl:text>
                            <xsl:value-of select="fhir:display/@value/string()"/>
                            <xsl:text>)</xsl:text>
                            <xsl:if test="position() != last()">, </xsl:if>
                        </xsl:for-each>
                    </xsl:variable>
                    <xsl:value-of select="substring($options, 1, 50)"/>
                    <xsl:if test="string-length($options) &gt; 50">...</xsl:if>
                </xsl:if>
            </td>
        </tr>
        <xsl:apply-templates mode="overview"/>
    </xsl:template>
    
    <xsl:template match="fhir:item" mode="details">
        <xsl:variable name="href" select="concat($projectDiskRoot, replace(fhir:definition/@value/string(), $fhirCanonicalBase, ''), '.xml')"/>
        <div>
            <h4 id="{fhir:linkId/@value}"><xsl:value-of select="fhir:text/@value/string()"/></h4>
            <xsl:if test="fhir:definition">
                <xsl:variable name="definition" select="doc($href)"/>
                <table id="qTable">
                    <tbody>
                        <xsl:for-each select="$definition/fhir:DataElement/* | $definition/fhir:DataElement/fhir:element/*">
                            <xsl:choose>
                                <xsl:when test="local-name() = 'identifier'">
                                    <tr>
                                        <td style="width:25em"><xsl:value-of select="local-name()"/></td>
                                        <td><xsl:value-of select="fhir:value/@value/string()"/> [<xsl:value-of select="fhir:system/@value/string()"/>]</td>
                                    </tr>            
                                </xsl:when>
                                <xsl:when test="local-name() = 'url'">
                                    <tr>
                                        <td><xsl:value-of select="local-name()"/></td>
                                        <td><a href="{$href}"><xsl:value-of select="@value/string()"/></a></td>
                                    </tr>            
                                </xsl:when>
                                <xsl:when test="local-name() = 'min'">
                                    <tr>
                                        <td>cardinality</td>
                                        <td><xsl:value-of select="@value"/>..<xsl:value-of select="../fhir:max/@value"/></td>
                                    </tr>            
                                </xsl:when>
                                <xsl:when test="local-name() = 'type'">
                                    <tr>
                                        <td><xsl:value-of select="local-name()"/></td>
                                        <td><xsl:value-of select="fhir:code/@value/string()"/></td>
                                    </tr>            
                                </xsl:when>
                                <xsl:when test="local-name() = ('element', 'max')">
                                </xsl:when>
                                <xsl:when test="local-name() = 'binding'">
                                    <xsl:variable name="valueSetId" select="tokenize(fhir:valueSetReference/fhir:reference/@value, '/')[last()]"/>
                                    <tr>
                                        <td><xsl:value-of select="local-name()"/></td>
                                        <td>
                                            <a href="{concat('../ValueSet/', $valueSetId, '.xml')}"><xsl:value-of select="fhir:valueSetReference/fhir:display/@value"/></a>
                                            <a href="{fhir:valueSetReference/fhir:reference/@value}"><img src="../resources/images/download.png" alt="download"/></a>
                                        </td>
                                    </tr>            
                                </xsl:when>
                                <xsl:otherwise>
                                    <tr>
                                        <td><xsl:value-of select="local-name()"/></td>
                                        <td><xsl:value-of select="@value/string()"/></td>
                                    </tr>            
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:for-each>
                    </tbody>
                </table>
            </xsl:if>
        </div>
    </xsl:template>
    
    <xsl:template match="@* | node()" mode="overview"/> 
    
</xsl:stylesheet>
