<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:fhir="http://hl7.org/fhir"
    xmlns:fun="http://art-decor.org/fun"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:import href="fhir-common.xsl"/>
    <xsl:import href="tr2map.xsl"/>
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="/">
        <xsl:variable name="decorBase" select="files/decorBase/string()"/>
        <xsl:variable name="fhirBase" select="files/fhirBase/string()"/>
        <xsl:variable name="prefix" select="files/prefix/string()"/>
        
        <xsl:for-each select="//file">
            <xsl:variable name="uri" select="concat($fhirBase, $prefix, '/StructureDefinition/', id, '--', effectiveDate)"/>
            <xsl:variable name="doc" select="doc($uri)"/>
            <xsl:variable name="href" select="concat($projectDiskRoot, 'LogicalModel/LM-', fun:shortName(name), '-', id, '--', translate(effectiveDate, 'T:-', ''), '.xml')"/>
            <xsl:result-document href="{$href}" method="xml">
                <xsl:copy-of select="$doc"/>
            </xsl:result-document>
            <xsl:message>Writing <xsl:value-of select="$href"/>...</xsl:message>

            <xsl:variable name="uri" select="concat($decorBase, 'RetrieveTransaction?id=', id, '&amp;effectiveDate=', effectiveDate, '&amp;language=', $language, '&amp;format=xml', if(../version) then concat('&amp;version=', ../version/string()) else '' )"/>
            <xsl:variable name="doc" select="doc($uri)"/>
            <xsl:variable name="href" select="concat($projectDiskRoot, 'transactions/', $doc/dataset/@shortName, '.xml')"/>
            <xsl:result-document href="{$href}" method="xml">
                <xsl:copy-of select="$doc"/>
            </xsl:result-document>
            <xsl:message>Writing <xsl:value-of select="$href"/>...</xsl:message>
            
            <xsl:variable name="map">
                <xsl:apply-templates select="$doc" mode="map"/>
            </xsl:variable>
            <xsl:variable name="href" select="concat($projectDiskRoot, 'map/map-', $doc/dataset/@shortName, '.xml')"/>
            <xsl:result-document href="{$href}" method="xml">
                <xsl:copy-of select="$map"/>
            </xsl:result-document>
            <xsl:variable name="href" select="concat($projectDiskRoot, 'map/map-', $doc/dataset/@transactionId, '--', translate($doc/dataset/@transactionEffectiveDate, 'T:-', '') ,'.xml')"/>
            <xsl:result-document href="{$href}" method="xml">
                <xsl:copy-of select="$map"/>
            </xsl:result-document>
            <xsl:message>Writing <xsl:value-of select="$href"/>...</xsl:message>
        </xsl:for-each>

        <xsl:variable name="allValueSets">
            <xsl:for-each select="//file">
                <xsl:variable name="uri" select="concat($decorBase, 'RetrieveTransaction?id=', id, '&amp;effectiveDate=', effectiveDate, '&amp;language=', $language, '&amp;format=xml')"/>
                <xsl:variable name="doc" select="doc($uri)"/>
                <xsl:for-each select="$doc//valueSet">
                    <valueSet>
                        <xsl:copy-of select="@*"/>
                    </valueSet>
                </xsl:for-each>
            </xsl:for-each>
        </xsl:variable>

        <xsl:variable name="distinctValueSetIds" select="distinct-values($allValueSets/valueSet/@id)"/>
        <xsl:for-each select="$distinctValueSetIds">
            <xsl:variable name="vsId" select="."/>
            <xsl:variable name="distinctEffectiveDates" select="distinct-values($allValueSets/valueSet[@id=$vsId]/@effectiveDate)"/>
            <xsl:for-each select="$distinctEffectiveDates">
                <xsl:variable name="vsEffectiveDate" select="."/>
                <xsl:variable name="vsUri" select="concat($fhirBase, $prefix, '/ValueSet/', $vsId, '--', $vsEffectiveDate)"/>
                <xsl:variable name="vsDoc" select="doc($vsUri)"/>
                <xsl:variable name="vsHref" select="concat($projectDiskRoot, 'ValueSet/VS-', $vsDoc/fhir:ValueSet/fhir:name/@value/string(), '-', $vsId, '--', translate($vsEffectiveDate, ':-T', ''), '.xml')"/>
                <xsl:result-document href="{$vsHref}" method="xml">
                    <xsl:copy-of select="$vsDoc"/>
                </xsl:result-document>
                <xsl:variable name="vsHref" select="concat($projectDiskRoot, 'ValueSet/', $vsId, '--', translate($vsEffectiveDate, ':-T', ''), '.xml')"/>
                <xsl:result-document href="{$vsHref}" method="xml">
                    <xsl:copy-of select="$vsDoc"/>
                </xsl:result-document>
                <xsl:message>Writing <xsl:value-of select="$vsHref"/>...</xsl:message>
            </xsl:for-each>
        </xsl:for-each>

        <xsl:variable name="href" select="concat($projectDiskRoot, 'body-maps.html')"/>
        <xsl:variable name="body-maps">
            <table id="transactions">
                <tbody>
                    <tr>
                        <th>DECOR</th>
                        <th>DECOR to FHIR map</th>
                    </tr>
                    <xsl:for-each select="//file">
                        <xsl:variable name="uri" select="concat($decorBase, 'RetrieveTransaction?id=', id, '&amp;effectiveDate=', effectiveDate, '&amp;language=', $language, '&amp;format=xml')"/>
                        <xsl:variable name="shortName" select="doc($uri)/dataset/@shortName"/>
                        <tr>
                            <xsl:variable name="uri" select="concat($decorBase, 'RetrieveTransaction?id=', id, '&amp;effectiveDate=', effectiveDate, '&amp;language=', $language, '&amp;format=html')"/>
                            <td><a href="{$uri}"><xsl:value-of select="name"/></a></td>
                            <td><a href="html/map-{$shortName}.html">map_<xsl:value-of select="$shortName"/></a></td>
                        </tr>
                    </xsl:for-each>
                </tbody>
            </table>
        </xsl:variable>>
        <xsl:result-document href="{$href}" method="html">
            <xsl:copy-of select="$body-maps"/>
        </xsl:result-document>
        
        <xsl:variable name="href" select="concat($projectDiskRoot, 'body-valuesets.html')"/>
        <xsl:variable name="body-valuesets">
            <table id="valuesets">
                <tbody>
                    <tr>
                        <th>DECOR Valueset</th>
                        <th>FHIR ValueSet</th>
                    </tr>
                    <xsl:for-each select="$distinctValueSetIds">
                        <xsl:variable name="vsId" select="."/>
                        <xsl:variable name="distinctEffectiveDates" select="distinct-values($allValueSets/valueSet[@id=$vsId]/@effectiveDate)"/>
                        <xsl:for-each select="$distinctEffectiveDates">
                            <xsl:variable name="vsEffectiveDate" select="."/>
                            <tr>
                                <xsl:variable name="uri" select="concat($decorBase, 'RetrieveValueSet?id=', $vsId, '&amp;effectiveDate=', $vsEffectiveDate, '&amp;prefix=', $prefix, '&amp;language=', $language, '&amp;format=html')"/>
                                <td><a href="{$uri}"><xsl:value-of select="$allValueSets/valueSet[@id=$vsId][@effectiveDate=$vsEffectiveDate][1]/@displayName"/></a></td>
                                <xsl:variable name="vsUri" select="concat($vsId, '--', translate($vsEffectiveDate, 'T:-', ''), '.xml')"/>
                                <td><a href="ValueSet/{$vsUri}"><xsl:value-of select="$vsUri"/></a></td>
                            </tr>
                        </xsl:for-each>
                    </xsl:for-each>
                </tbody>
            </table>
        </xsl:variable>
        <xsl:result-document href="{$href}" method="html">
            <xsl:copy-of select="$body-valuesets"/>
        </xsl:result-document>
        
        <xsl:variable name="href" select="concat($projectDiskRoot, 'index.html')"/>
        <xsl:result-document href="{$href}" method="html">
            <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <title>Index</title>
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"></link>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
                    <link href="resources/css/default.css" rel="stylesheet" type="text/css"/>
                    <style>
                        body {margin:8px;}
                        a[href] {color:blue;}
                        table {border-collapse: collapse; border: 1px solid black;}
                        td {border: 1px solid black; padding: 2px;}
                    </style>
                </head>
                <body>
                    <h1>Questionnaires</h1>
                    <table id="questionnaires">
                        <tbody>
                            <tr>
                                <th>Questionnaire</th>
                            </tr>
                            <xsl:for-each select="//file">
                                <xsl:variable name="uri" select="concat($decorBase, 'RetrieveTransaction?id=', id, '&amp;effectiveDate=', effectiveDate, '&amp;language=', $language, '&amp;format=xml')"/>
                                <xsl:variable name="shortName" select="doc($uri)/dataset/@shortName"/>
                                <tr>
                                    <xsl:variable name="uri" select="concat($decorBase, 'RetrieveTransaction?id=', id, '&amp;effectiveDate=', effectiveDate, '&amp;language=', $language, '&amp;format=html')"/>
                                    <td><a href="html/Q_{$shortName}.html">Q_<xsl:value-of select="$shortName"/></a></td>
                                </tr>
                            </xsl:for-each>
                        </tbody>
                    </table>
                    <h1>Maps</h1>
                    <xsl:copy-of select="$body-maps"/>
                    <h1>Valuesets</h1>
                    <xsl:copy-of select="$body-valuesets"/>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="@* | node()"/>
</xsl:stylesheet>