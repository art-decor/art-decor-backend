<?xml version="1.0" encoding="UTF-8"?>
<!-- 
-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:fun="http://art-decor.org/fun" xmlns="http://hl7.org/fhir" exclude-result-prefixes="#all">
    <xsl:import href="fhir-common.xsl"/>
    <xsl:output method="xml" indent="yes"/>
    <xsl:variable name="mapUri" select="concat($projectDiskRoot, 'map/map-', //dataset/@shortName, '.xml')"/>
    <xsl:variable name="map" select="doc($mapUri)"/>
    <xsl:variable name="configUri" select="concat($projectDiskRoot, 'config/files.xml')"/>
    <xsl:variable name="config" select="doc($configUri)"/>
    <xsl:key name="target" match="target" use="../source/@id"></xsl:key>
    
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="dataset">
        <xsl:variable name="transactionId" select="@transactionId/string()"/>
        <xsl:variable name="targets" select="$config//file[id=$transactionId]/targets/id/text()"/>
        <xsl:if test="count($targets) = 0">
            <xsl:message>ERROR: no targets in files.xml for this transaction</xsl:message>
        </xsl:if>
        <Questionnaire xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns="http://hl7.org/fhir">
            <id value="194843"/>
            <text>
                <status value="generated"/>
                <div xmlns="http://www.w3.org/1999/xhtml">
                    <pre>Questionnaire for <xsl:value-of select="@shortName"/></pre>
                </div>
            </text>
            <url value="{$fhirCanonicalBase}Questionnaire/Q_{@shortName}"/>
            <!-- TODO: distinct valuesets -->
            <!-- For now, choice is explicitly listed in option/code -->
            <!--<xsl:apply-templates select="//valueSet"/>-->

            <name value="{@shortName}"/>
            <title value="{name}"/>
            <status value="draft"/>
            <date value="2016-04-14"/>
            <publisher value="{$publisher}"/>
            <subjectType value="Patient"/>
            <xsl:variable name="datasetRoot" select="."/>
            <xsl:for-each select="$targets">
                <xsl:variable name="conceptId" select="."/>
                <xsl:message select="$conceptId"></xsl:message>
                <xsl:apply-templates select="$datasetRoot//concept[@id=$conceptId]"/>
            </xsl:for-each>
        </Questionnaire>
    </xsl:template>

    <xsl:template match="concept[@type = 'group']">
        <xsl:variable name="id" select="@id"/>
        <xsl:variable name="targetUrl" select="$map/key('target', $id)/@url"/>
        <xsl:variable name="targetId" select="$map/key('target', $id)/@id"/>
        <item>
            <linkId value="{$targetId}"/>
            <!-- We use definitions for primitive data types only -->
            <definition value="{$targetUrl}"/>
            <text value="{name}"/>
            <type value="group"/>
            <xsl:choose>
                <xsl:when test="@minimumMultiplicity = '1'">
                    <required value="true"/>
                </xsl:when>
                <xsl:otherwise>
                    <required value="false"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
                <xsl:when test="@maximumMultiplicity != '1'">
                    <repeats value="true"/>
                </xsl:when>
                <xsl:otherwise>
                    <repeats value="false"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates/>
        </item>
    </xsl:template>

    <xsl:template match="concept[@type = 'item']">
        <xsl:variable name="id" select="@id"/>
        <xsl:variable name="targetUrl" select="$map/key('target', $id)/@url"/>
        <xsl:variable name="targetId" select="$map/key('target', $id)/@id"/>
        <item>
            <linkId value="{$targetId}"/>
            <definition value="{$targetUrl}"/>
            <text value="{name}"/>
            <type value="{fun:decor2questionnaireType(.)}"/>
            <xsl:choose>
                <xsl:when test="@minimumMultiplicity = '1'">
                    <required value="true"/>
                </xsl:when>
                <xsl:otherwise>
                    <required value="false"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
                <xsl:when test="@maximumMultiplicity != '1'">
                    <repeats value="true"/>
                </xsl:when>
                <xsl:otherwise>
                    <repeats value="false"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:for-each select="valueSet/conceptList/concept">
                <option>
                    <valueCoding>
                        <system value="{fun:codeSystemUrl(@codeSystem)}"/>
                        <code value="{@code}"/>
                        <display value="{@displayName}"/>
                    </valueCoding>
                </option>
            </xsl:for-each>
        </item>
    </xsl:template>

    <xsl:template match="valueSet">
        <contained>
            <ValueSet>
                <id value="{@name}"/>
                <name value="{@displayName}"/>
                <status value="{if(@statusCode='final') then active else if(@statusCode=('new', 'draft')) then 'draft' else 'retired'}"/>
                <codeSystem>
                    <system value="http://example.org/system/code/yesno"/>
                    <caseSensitive value="true"/>
                    <concept>
                        <code value="1"/>
                        <display value="Yes"/>
                    </concept>
                    <concept>
                        <code value="2"/>
                        <display value="No"/>
                    </concept>
                </codeSystem>
            </ValueSet>
        </contained>
    </xsl:template>
    <xsl:template match="@* | node()"/>

</xsl:stylesheet>
