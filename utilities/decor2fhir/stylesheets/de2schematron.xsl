<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Copyright ART-DECOR Expert Group 
    
    Output: 
-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fun="http://art-decor.org/fun" xmlns:fhir="http://hl7.org/fhir"
    xmlns="http://hl7.org/fhir" exclude-result-prefixes="#all" xmlns:sch="http://purl.oclc.org/dsdl/schematron">
    <xsl:import href="fhir-common.xsl"/>
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="fhir:DataElement">
        <sch:pattern id="{fhir:id/@value}" xmlns:fhir="http://hl7.org/fhir">
            <sch:rule context="//fhir:item[fhir:linkId/@value='{fhir:id/@value}']">
                <xsl:variable name="url" select="fhir:url/@value"/>
                <sch:assert test="fhir:definition/@value = '{$url}'">Definition <value-of
                        select="fhir:definition/@value"/> must be '<xsl:value-of select="$url"/>'.</sch:assert>
            </sch:rule>
            <sch:rule context="//fhir:item[fhir:linkId/@value='{fhir:id/@value}']/fhir:answer/*">
                <xsl:apply-templates select="fhir:element"/>
            </sch:rule>
        </sch:pattern>
    </xsl:template>

    <xsl:template match="fhir:element">
        <xsl:variable name="id" select="../fhir:id/@value/string()"/>
        <xsl:variable name="type" select="fhir:type/fhir:code/@value/string()"/>
        <xsl:choose>
            <xsl:when test="$type = 'BackboneElement'"/>
            <xsl:otherwise>
                <xsl:variable name="qType" select="fun:fhir2questionnaireType($type)"/>
                <xsl:variable name="answer" select="concat('value', upper-case(substring($qType, 1, 1)), substring($qType, 2))"/>
                <sch:assert test="local-name(.) = '{$answer}'">Answer must be of type '<xsl:value-of select="$answer"/>'.</sch:assert>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="$type = 'CodeableConcept'">
            <xsl:variable name="vsref" select="fhir:binding/fhir:valueSetReference/fhir:reference/@value"/>
            <sch:let name="valueset" value="doc(concat('../ValueSet/{tokenize($vsref, '/')[last()]}', '.xml'))"/>
            <sch:assert test="fhir:code/@value = $valueset//fhir:contains/fhir:code/@value" see="{$vsref}">
                Code <sch:value-of select="fhir:code/@value"/> of <xsl:value-of select="$id"/> must be from <xsl:value-of select="$vsref"/>.</sch:assert>
            <sch:assert test="fhir:system/@value = $valueset//fhir:contains/fhir:system/@value" see="{$vsref}">
                System <sch:value-of select="fhir:system/@value"/> of <xsl:value-of select="$id"/> must be from <xsl:value-of select="$vsref"/>.</sch:assert>
        </xsl:if>
    </xsl:template>

    <xsl:template match="@* | node()"/>

</xsl:stylesheet>
