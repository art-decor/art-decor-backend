To create the contents:

Start with project layout
-------------------------
- make [demo1] folder
- copy resources into [demo1] folder
- make config folder inside
- put files.xml in /config
- put params.xml in /config
- edit the latter two to fit your project

1. Getting all transaction definitions and Logical Models
---------------------------------------------------------
- make a file with all needed transactions, such as:
<files>
    <!-- Where the DECOR resources are -->
    <decorBase>http://localhost:8877/decor/services/</decorBase>
    <!-- Where the FHIR resources are -->
    <fhirBase>http://localhost:8877/fhir/stu3/</fhirBase>
    <!-- Project prefix -->
    <prefix>demo1-</prefix>
    <file>
        <name>Measurement/</name>
        <!-- Transaction @id and @effectiveDate -->
        <id>2.16.840.1.113883.3.1937.99.62.3.4.2</id>
        <effectiveDate>2012-09-05T16:59:35</effectiveDate>
    </file>
    ... More files...
</files>
- run get-resources.xsl on this file
- this will:
- get output from DECOR RetrieveTransaction and put it in /transactions
- get output from DECOR FHIR StructureDefinition (Logical Models) and put it in LogicalModel
- get output from DECOR FHIR ValueSets and put it in /valuesets
- use tr2map.xsl to make concept maps from all transactions and put a map file in /map
- make an index.html (the links will only work if the following steps are followed to generate html for Questionnaires and maps

2. Making DataElements
----------------------
- for DSTU2 FHIR resources: run fix-lm2.xsl on Logical Models in /LogicalModel and save to the same file, this will fix some errors and make the LM's STU3 compliant (tested for a limited set, not guaranteed to be universal)
- run sd2data-elements.xsl on all Logical Models in /LogicalModel
- this will put a DataElement file in /DataElement

3. Making Questionnaires
------------------------
- run tr2quest.xsl on all (desired) xml files in /transactions
- save as Q_[shortName].xml in /Questionnaire

4. Make example QuestionnaireResponse from ADA
----------------------------------------------
- Generate ADA project, make ADA XML for desired transactions
- Run ada2questResp.xsl on ADA XML
- this will put a QuestionnaireResponse file in /QuestionnaireResponse

5. Generate schematron
----------------------
- Run de2schematron on all DataElements
- Run q2schematron on all Questionnaires
- This will create schematron (now: only valueSet checks, more to come)

6. Generate more docs
---------------------
- Run q2html.xsl on all Questionnaires, this will generate documentation
- Run map2html.xsl on maps to generate a HTML view of the maps

Temp: Fix shortname
--------------------
Two shortnames are too long, these need to be fixed (in Logical Model) before upload to Simplifier:
uitslag_mammacarcinoomtotaal_naar_mdo_excisieprep_swk_okd_mari
to:
uitslag_mammacarcinoomtotaal_naar_mdo
And:
mdoverslag_postneoadjuvant_en_preoperatief__gemetastaseerde_ziekte_m1
to
mdoverslag_postneoadjuvant_en_preoperatief

Needs a permanent solution.

