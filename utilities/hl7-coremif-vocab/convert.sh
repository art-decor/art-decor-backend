#!/bin/bash

export xslcreate=create_decor_valueSets_from_coreMif.xsl
export xslmerge=merge_decor_valuesets.xsl

if [ ! -e ${jarPath:?"Parameter jarPath not set. Example: export jarPath=/mypath/lib/saxon9"} ]; then
    echo "Parameter jarPath does not lead to saxon9.jar."
    exit 1
fi

# Get current base project from art-decor.org
#echo "Retrieving base project ad2bbr-decor.xml from art-decor.org"
curl -s -o ad2bbr-decor.xml "http://art-decor.org/decor/services/modules/RetrieveProject.xquery?prefix=ad2bbr-&mode=verbatim&language=&download=false&format=xml"

# Build new project
for file in `ls DEFN=UV=VO=*.coremif` ; do
    echo "Building base project ${file%.*}.xml from ${file}"
    java -jar ${jarPath} -s:"$file" -xsl:"$xslcreate" -o:"${file%.*}.xml" includeDeprecatedValuesets=false
done

# Merge 1086 info into result of previous merge
#echo "Merging DEFN=UV=VO=1086-20110315.xml into DEFN=UV=VO=1086-20110315-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1086-20110315.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1086-20110315-merged.xml" baseLineProject=DEFN=UV=VO=1099-20110726.xml simpleMerge=false
# Merge 1098 info into result of previous merge
#echo "Merging DEFN=UV=VO=1098-20110718.xml into DEFN=UV=VO=1098-20110718-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1098-20110718.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1098-20110718-merged.xml" baseLineProject=DEFN=UV=VO=1086-20110315-merged.xml simpleMerge=false
# Merge 1099 info into result of previous merge
#echo "Merging DEFN=UV=VO=1099-20110726.xml into DEFN=UV=VO=1099-20110726-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1099-20110726.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1099-20110726-merged.xml" baseLineProject=DEFN=UV=VO=1098-20110718-merged.xml simpleMerge=false
# Merge 1150 info into result of previous merge
#echo "Merging DEFN=UV=VO=1150-20120331.xml into DEFN=UV=VO=1150-20120331-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1150-20120331.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1150-20120331-merged.xml" baseLineProject=DEFN=UV=VO=1099-20110726.xml simpleMerge=false
# Merge 1175 info into result of previous merge
#echo "Merging DEFN=UV=VO=1175-20120802.xml into DEFN=UV=VO=1175-20120802-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1175-20120802.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1175-20120802-merged.xml" baseLineProject=DEFN=UV=VO=1150-20120331-merged.xml simpleMerge=false
# Merge 1189 info into result of previous merge
#echo "Merging DEFN=UV=VO=1189-20121121.xml into DEFN=UV=VO=1189-20121121-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1189-20121121.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1189-20121121-merged.xml" baseLineProject=DEFN=UV=VO=1175-20120802-merged.xml simpleMerge=false
# Merge 1190 info into result of previous merge
#echo "Merging DEFN=UV=VO=1190-20121127.xml into DEFN=UV=VO=1190-20121127-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1190-20121127.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1190-20121127-merged.xml" baseLineProject=DEFN=UV=VO=1189-20121121-merged.xml simpleMerge=false
# Merge 1204 info into result of previous merge
#echo "Merging DEFN=UV=VO=1204-20130311.xml into DEFN=UV=VO=1204-20130311-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1204-20130311.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1204-20130311-merged.xml" baseLineProject=DEFN=UV=VO=1190-20121127-merged.xml simpleMerge=false
# Merge 1206 info into result of previous merge
#echo "Merging DEFN=UV=VO=1206-20130318.xml into DEFN=UV=VO=1206-20130318-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1206-20130318.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1206-20130318-merged.xml" baseLineProject=DEFN=UV=VO=1204-20130311-merged.xml simpleMerge=false
# Merge 1238 info into result of previous merge
#echo "Merging DEFN=UV=VO=1238-20130729.xml into DEFN=UV=VO=1238-20130729-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1238-20130729.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1238-20130729-merged.xml" baseLineProject=DEFN=UV=VO=1206-20130318-merged.xml simpleMerge=false
# Merge 1242 info into result of previous merge
#echo "Merging DEFN=UV=VO=1242-20130806.xml into DEFN=UV=VO=1242-20130806-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1242-20130806.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1242-20130806-merged.xml" baseLineProject=DEFN=UV=VO=1238-20130729-merged.xml simpleMerge=false
# Merge 1243 info into result of previous merge
#echo "Merging DEFN=UV=VO=1243-20130808.xml into DEFN=UV=VO=1243-20130808-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1243-20130808.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1243-20130808-merged.xml" baseLineProject=DEFN=UV=VO=1242-20130806-merged.xml simpleMerge=false
# Merge 1269 info into result of previous merge
#echo "Merging DEFN=UV=VO=1269-20131201.xml into DEFN=UV=VO=1269-20131201-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1269-20131201.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1269-20131201-merged.xml" baseLineProject=DEFN=UV=VO=1243-20130808-merged.xml simpleMerge=false
# Merge 1271 info into result of previous merge
#echo "Merging DEFN=UV=VO=1271-20131207.xml into DEFN=UV=VO=1271-20131207-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1271-20131207.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1271-20131207-merged.xml" baseLineProject=DEFN=UV=VO=1269-20131201-merged.xml simpleMerge=false
# Merge 1280 info into result of previous merge
#echo "Merging DEFN=UV=VO=1280-20140314.xml into DEFN=UV=VO=1280-20140314-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1280-20140314.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1280-20140314-merged.xml" baseLineProject=DEFN=UV=VO=1271-20131207-merged.xml simpleMerge=false
# Merge 1283 info into result of previous merge
#echo "Merging DEFN=UV=VO=1283-20140325.xml into DEFN=UV=VO=1283-20140325-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1283-20140325.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1283-20140325-merged.xml" baseLineProject=DEFN=UV=VO=1280-20140314-merged.xml simpleMerge=false
# Merge 1284 info into result of previous merge
#echo "Merging DEFN=UV=VO=1284-20140429.xml into DEFN=UV=VO=1284-20140429-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1284-20140429.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1284-20140429-merged.xml" baseLineProject=DEFN=UV=VO=1283-20140325-merged.xml simpleMerge=false
# Merge 1286 info into result of previous merge
#echo "Merging DEFN=UV=VO=1286-20140510.xml into DEFN=UV=VO=1286-20140510-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1286-20140510.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1286-20140510-merged.xml" baseLineProject=DEFN=UV=VO=1284-20140429-merged.xml simpleMerge=false
# Merge 1287 info into result of previous merge
#echo "Merging DEFN=UV=VO=1287-20140516.xml into DEFN=UV=VO=1287-20140516-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1287-20140516.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1287-20140516-merged.xml" baseLineProject=DEFN=UV=VO=1286-20140510-merged.xml simpleMerge=false
# Merge 1307 info into result of previous merge
#echo "Merging DEFN=UV=VO=1307-20140721.xml into DEFN=UV=VO=1307-20140721-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1307-20140721.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1307-20140721-merged.xml" baseLineProject=DEFN=UV=VO=1287-20140516-merged.xml simpleMerge=false
# Merge 1312 info into result of previous merge
#echo "Merging DEFN=UV=VO=1312-20140807.xml into DEFN=UV=VO=1312-20140807-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1312-20140807.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1312-20140807-merged.xml" baseLineProject=DEFN=UV=VO=1307-20140721-merged.xml simpleMerge=false
# Merge 1321 info into result of previous merge
#echo "Merging DEFN=UV=VO=1321-20141211.xml into DEFN=UV=VO=1321-20141211-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1321-20141211.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1321-20141211-merged.xml" baseLineProject=DEFN=UV=VO=1312-20140807-merged.xml simpleMerge=false
# Merge 1325 info into result of previous merge
#echo "Merging DEFN=UV=VO=1325-20150304.xml into DEFN=UV=VO=1325-20150304-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1325-20150304.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1325-20150304-merged.xml" baseLineProject=DEFN=UV=VO=1321-20141211-merged.xml simpleMerge=false
# Merge 1326 info into result of previous merge
#echo "Merging DEFN=UV=VO=1326-20150323.xml into DEFN=UV=VO=1326-20150323-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1326-20150323.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1326-20150323-merged.xml" baseLineProject=DEFN=UV=VO=1325-20150304-merged.xml simpleMerge=false
# Merge 1336 info into result of previous merge
#echo "Merging DEFN=UV=VO=1336-20150731.xml into DEFN=UV=VO=1336-20150731-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1336-20150731.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1336-20150731-merged.xml" baseLineProject=DEFN=UV=VO=1326-20150323-merged.xml simpleMerge=false
# Merge 1351 info into result of previous merge
#echo "Merging DEFN=UV=VO=1351-20151130.xml into DEFN=UV=VO=1351-20151130-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1351-20151130.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1351-20151130-merged.xml" baseLineProject=DEFN=UV=VO=1336-20150731-merged.xml simpleMerge=false
# Merge 1360 info into result of previous merge
#echo "Merging DEFN=UV=VO=1360-20160323.xml into DEFN=UV=VO=1360-20160323-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1360-20160323.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1360-20160323-merged.xml" baseLineProject=DEFN=UV=VO=1351-20151130-merged.xml simpleMerge=false
# Merge 1371 info into result of previous merge
#echo "Merging DEFN=UV=VO=1371-20160728.xml into DEFN=UV=VO=1371-20160728-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1371-20160728.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1371-20160728-merged.xml" baseLineProject=DEFN=UV=VO=1360-20160323-merged.xml simpleMerge=false

# Merge ad2bbr-decor into initial new result
echo "Merging base ad2bbr-decor.xml with DEFN=UV=VO=1371-20160728.xml into DEFN=UV=VO=1371-20160728-merged.xml"
java -jar ${jarPath} -s:"DEFN=UV=VO=1371-20160728.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1371-20160728-merged.xml" baseLineProject=ad2bbr-decor.xml simpleMerge=false

# Merge 1382 info into result of previous merge
echo "Merging DEFN=UV=VO=1382-20161126.xml into DEFN=UV=VO=1382-20161126-merged.xml"
java -jar ${jarPath} -s:"DEFN=UV=VO=1382-20161126.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1382-20161126-merged.xml" baseLineProject=DEFN=UV=VO=1371-20160728-merged.xml simpleMerge=false
# Merge 1390 info into result of previous merge
echo "Merging DEFN=UV=VO=1390-20170324.xml into DEFN=UV=VO=1390-20170324-merged.xml"
java -jar ${jarPath} -s:"DEFN=UV=VO=1390-20170324.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1390-20170324-merged.xml" baseLineProject=DEFN=UV=VO=1382-20161126-merged.xml simpleMerge=false
# Merge 1397 info into result of previous merge
echo "Merging DEFN=UV=VO=1397-20170731.xml into DEFN=UV=VO=1397-20170731-merged.xml"
java -jar ${jarPath} -s:"DEFN=UV=VO=1397-20170731.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1397-20170731-merged.xml" baseLineProject=DEFN=UV=VO=1390-20170324-merged.xml simpleMerge=false
# Merge 1402 info into result of previous merge
echo "Merging DEFN=UV=VO=1402-20171217.xml into DEFN=UV=VO=1402-20171217-merged.xml"
java -jar ${jarPath} -s:"DEFN=UV=VO=1402-20171217.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1402-20171217-merged.xml" baseLineProject=DEFN=UV=VO=1397-20170731-merged.xml simpleMerge=false
# Merge 1418 info into result of previous merge
echo "Merging DEFN=UV=VO=1418-20180401.xml into DEFN=UV=VO=1418-20180401-merged.xml"
java -jar ${jarPath} -s:"DEFN=UV=VO=1418-20180401.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1418-20180401-merged.xml" baseLineProject=DEFN=UV=VO=1402-20171217-merged.xml simpleMerge=false
# Merge 1422 info into result of previous merge
echo "Merging DEFN=UV=VO=1422-20180812.xml into DEFN=UV=VO=1422-20180812-merged.xml"
java -jar ${jarPath} -s:"DEFN=UV=VO=1422-20180812.xml" -xsl:"$xslmerge" -o:"DEFN=UV=VO=1422-20180812-merged.xml" baseLineProject=DEFN=UV=VO=1418-20180401-merged.xml simpleMerge=false

#echo "Merging ad2bbr-decor.xml with DEFN=UV=VO=1422-20180812-merged.xml"
#java -jar ${jarPath} -s:"DEFN=UV=VO=1422-20180812-merged.xml" -xsl:"$xslmerge" -o:"ad2bbr-decor-merged.xml" baseLineProject=ad2bbr-decor.xml simpleMerge=true

echo "Copying latest merge result DEFN=UV=VO=1422-20180812-merged.xml as ad2bbr-decor-merged.xml"
cp DEFN=UV=VO=1422-20180812-merged.xml ad2bbr-decor-merged.xml

# Sanity check of base project on art-decor.org against current generated result
echo "Sanity check of ad2bbr-decor-merged.xml against ad2bbr-decor.xml as uploaded on art-decor.org"
java -jar ${jarPath} -s:"ad2bbr-decor.xml" -xsl:"sanity-check.xsl" baseLineProject=ad2bbr-decor-merged.xml