<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    exclude-result-prefixes="xs xd"
    version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Aug 22, 2018</xd:p>
            <xd:p><xd:b>Author:</xd:b> ahenket</xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    
    <xsl:output method="text" omit-xml-declaration="yes"/>
    
    <xsl:param name="baseLineProject" select="'ad2bbr-decor-merged.xml'"/>
    <xsl:variable name="baseLineDocument" select="doc($baseLineProject)/decor" as="element(decor)"/>
    
    <xsl:template match="/">
        <xsl:text>Checking base project </xsl:text>
        <xsl:value-of select="tokenize(document-uri(.), '/')[last()]"/>
        <xsl:text> against newer version </xsl:text>
        <xsl:value-of select="$baseLineProject"/>
        <xsl:text>&#10;</xsl:text>
        <xsl:apply-templates select="decor/terminology/valueSet"/>
    </xsl:template>
    
    <xsl:template match="valueSet">
        <xsl:variable name="vs" select="."/>
        <xsl:variable name="vsid" select="@id"/>
        <xsl:variable name="vsed" select="@effectiveDate"/>
        <xsl:variable name="vsnm" select="@name"/>
        <xsl:variable name="vsvl" select="@versionLabel"/>
        <xsl:variable name="matchingValueSet" select="$baseLineDocument//valueSet[@id = $vsid][@effectiveDate = $vsed]" as="element(valueSet)*"/>
        
        <xsl:if test="count($matchingValueSet) gt 1">
            <xsl:text>Multiple valueSets (</xsl:text>
            <xsl:value-of select="count($matchingValueSet)"/>
            <xsl:text>) in new content for id </xsl:text><xsl:value-of select="$vsid"/><xsl:text> / effectiveDate </xsl:text><xsl:value-of select="$vsed"/><xsl:text> / name </xsl:text><xsl:value-of select="$vsnm"/><xsl:text>&#10;</xsl:text>
        </xsl:if>
        
        <xsl:choose>
            <xsl:when test="$matchingValueSet">
                <xsl:choose>
                    <xsl:when test="deep-equal(., $matchingValueSet)"/>
                    <!--<xsl:when test="not(count(completeCodeSystem) = count($matchingValueSet/completeCodeSystem))">
                        <xsl:message>Mismatched number of completeCodeSystems valueSet in new content id <xsl:value-of select="$vsid"/> / effectiveDate <xsl:value-of select="$vsed"/> / name <xsl:value-of select="$vsnm"/></xsl:message>
                    </xsl:when>
                    <xsl:when test="not(count(conceptList/concept) = count($matchingValueSet/conceptList/concept))">
                        <xsl:message>Mismatched number of concepts valueSet in new content id <xsl:value-of select="$vsid"/> / effectiveDate <xsl:value-of select="$vsed"/> / name <xsl:value-of select="$vsnm"/></xsl:message>
                    </xsl:when>
                    <xsl:when test="not(count(conceptList/include) = count($matchingValueSet/conceptList/include))">
                        <xsl:message>Mismatched number of concepts valueSet in new content id <xsl:value-of select="$vsid"/> / effectiveDate <xsl:value-of select="$vsed"/> / name <xsl:value-of select="$vsnm"/></xsl:message>
                    </xsl:when>-->
                    <xsl:otherwise>
                        <xsl:variable name="missingCodeSystems" select="completeCodeSystem[not(@codeSystem = $matchingValueSet/completeCodeSystem/@codeSystem)]" as="element()*"/>
                        <xsl:variable name="missingConcepts" select="conceptList/concept[not(concat(@code, @codeSystem) = $matchingValueSet/conceptList/concept/concat(@code, @codeSystem))]" as="element()*"/>
                        <xsl:variable name="missingIncludes" select="conceptList/include[not(concat(@ref, @flexibility) = $matchingValueSet/conceptList/include/concat(@ref, @flexibility))]" as="element()*"/>
                        <xsl:variable name="extraCodeSystems" select="$matchingValueSet/completeCodeSystem[not(@codeSystem = $vs/completeCodeSystem/@codeSystem)]" as="element()*"/>
                        <xsl:variable name="extraConcepts" select="$matchingValueSet/conceptList/concept[not(concat(@code, @codeSystem) = $vs/conceptList/concept/concat(@code, @codeSystem))]" as="element()*"/>
                        <xsl:variable name="extraIncludes" select="$matchingValueSet/conceptList/include[not(concat(@ref, @flexibility) = $vs/conceptList/include/concat(@ref, @flexibility))]" as="element()*"/>
                        <xsl:if test="$missingCodeSystems | $missingConcepts | $missingIncludes | $extraCodeSystems | $extraConcepts | $extraIncludes">
                            <xsl:text>Mismatched contents valueSet in new content id </xsl:text>
                            <xsl:value-of select="$vsid"/>
                            <xsl:text> / effectiveDate </xsl:text>
                            <xsl:value-of select="$vsed"/>
                            <xsl:text> / name </xsl:text>
                            <xsl:value-of select="$vsnm"/>
                            <xsl:text> (</xsl:text>
                            <xsl:value-of select="$vsvl"/>
                            <xsl:text> vs </xsl:text>
                            <xsl:value-of select="$matchingValueSet/@versionLabel"/>
                            <xsl:text>)</xsl:text>
                            <xsl:text>&#10;</xsl:text>
                            <xsl:for-each select="$missingCodeSystems">
                                <xsl:variable name="csid" select="@codeSystem"/>
                                <xsl:text>    completeCodeSystem </xsl:text><xsl:value-of select="$csid"/><xsl:text> missing&#10;</xsl:text>
                            </xsl:for-each>
                            <xsl:for-each select="$missingConcepts">
                                <xsl:variable name="cid" select="@code"/>
                                <xsl:variable name="csid" select="@codeSystem"/>
                                <xsl:text>    concept </xsl:text><xsl:value-of select="$cid"/><xsl:text> | </xsl:text><xsl:value-of select="$csid"/><xsl:text> missing&#10;</xsl:text>
                            </xsl:for-each>
                            <xsl:for-each select="$missingIncludes">
                                <xsl:variable name="iref" select="@ref"/>
                                <xsl:variable name="iflex" select="@flexibility"/>
                                <xsl:text>    include </xsl:text><xsl:value-of select="$iref"/><xsl:text> | </xsl:text><xsl:value-of select="$iflex"/><xsl:text> missing&#10;</xsl:text>
                            </xsl:for-each>
                            <xsl:for-each select="$extraCodeSystems">
                                <xsl:variable name="csid" select="@codeSystem"/>
                                <xsl:text>    completeCodeSystem </xsl:text><xsl:value-of select="$csid"/><xsl:text> extra&#10;</xsl:text>
                            </xsl:for-each>
                            <xsl:for-each select="$extraConcepts">
                                <xsl:variable name="cid" select="@code"/>
                                <xsl:variable name="csid" select="@codeSystem"/>
                                <xsl:text>    concept </xsl:text><xsl:value-of select="$cid"/><xsl:text> | </xsl:text><xsl:value-of select="$csid"/><xsl:text> extra&#10;</xsl:text>
                            </xsl:for-each>
                            <xsl:for-each select="$extraIncludes">
                                <xsl:variable name="iref" select="@ref"/>
                                <xsl:variable name="iflex" select="@flexibility"/>
                                <xsl:text>    include </xsl:text><xsl:value-of select="$iref"/><xsl:text> | </xsl:text><xsl:value-of select="$iflex"/><xsl:text> extra&#10;</xsl:text>
                            </xsl:for-each>
                        </xsl:if>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>Missing valueSet in new content id </xsl:text><xsl:value-of select="$vsid"/><xsl:text> / effectiveDate </xsl:text><xsl:value-of select="$vsed"/><xsl:text> / name </xsl:text><xsl:value-of select="$vsnm"/><xsl:text>&#10;</xsl:text>
                <xsl:text>    Other versions: </xsl:text>
                <xsl:value-of select="$baseLineDocument//valueSet[@id = $vsid]/@effectiveDate"/>
                <xsl:text>&#10;</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>