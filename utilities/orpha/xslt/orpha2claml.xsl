<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:owl="http://www.w3.org/2002/07/owl#"
    xmlns:oboInOwl="http://www.geneontology.org/formats/oboInOwl#"
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:efo="http://www.ebi.ac.uk/efo/"
    exclude-result-prefixes="rdf owl oboInOwl rdfs dc efo">

    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="rdf:RDF">
        <ClaML version="2.0.0">
            <xsl:for-each select="owl:Ontology">
                <Meta name="statusCode" value="active"/>
                <Meta name="custodianOrganisation" value="{dc:rights/text()}"/>
                <!--<Meta name="custodianOrganisationLogo" value=""/>-->
                <Meta name="custodianOrganisationUrl" value="https://www.orpha.net"/>
                <Meta name="license" value="Orphadata: Free access data from Orphanet. © INSERM 1999. Available on http://www.orphadata.org. Data version (XML data version)"/>
                <Identifier authority="ORPHA" uid="http://www.orpha.net/ORDO/"/>
                <Title name="ORPHA" date="{current-date()}" version="{owl:versionInfo}">ORPHA</Title>
                <Authors>
                    <xsl:for-each select="(dc:contributor | dc:creator)">
                        <Author name="{replace(lower-case(string()), ' ', '_')}"><xsl:copy-of select="text()"/></Author>
                    </xsl:for-each>
                </Authors>
                <ClassKinds>
                    <ClassKind name="Orpha_entity"/>
                </ClassKinds>
                <RubricKinds>
                    <RubricKind name="preferred"/>
                    <RubricKind name="synonym"/>
                    <RubricKind name="definition"/>
                    <RubricKind name="comment"/>
                </RubricKinds>
           </xsl:for-each>
            
            <xsl:for-each select="owl:Class[rdfs:label][not(owl:deprecated)]">
                <xsl:variable name="id" select="tokenize(@rdf:about, '_')[last()]"/>
                <Class code="{$id}" kind="Orpha_entity">
                    <xsl:for-each select="rdfs:subClassOf[@rdf:resource]">
                        <SuperClass code="{tokenize(@rdf:resource, '_')[last()]}"/>
                    </xsl:for-each>
                    <xsl:variable name="about" select="@rdf:about"/>
                    <xsl:for-each select="//rdfs:subClassOf[@rdf:resource=$about]">
                        <xsl:variable name="subId" select="tokenize(../@rdf:about, '_')[last()]"/>
                        <SubClass code="{$subId}"/>
                    </xsl:for-each>
                    <Rubric kind="preferred">
                        <Label xml:lang="en"><xsl:value-of select="rdfs:label/text()"/></Label>
                    </Rubric>
                    <xsl:for-each select="oboInOwl:hasExactSynonym">
                        <Rubric kind="synonym">
                            <Label xml:lang="en"><xsl:value-of select="text()"/></Label>
                        </Rubric>
                    </xsl:for-each>
                    <xsl:for-each select="efo:definition">
                        <Rubric kind="definition">
                            <Label xml:lang="en"><xsl:value-of select="text()"/></Label>
                        </Rubric>
                    </xsl:for-each>
                    <xsl:for-each select="rdfs:comment">
                        <Rubric kind="comment">
                            <Label xml:lang="en"><xsl:value-of select="text()"/></Label>
                        </Rubric>
                    </xsl:for-each>
                </Class>            
            </xsl:for-each>
        </ClaML>
    </xsl:template>

    <xsl:template match="@*|node()">
    </xsl:template>
</xsl:stylesheet>