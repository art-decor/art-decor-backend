<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="hl7v2.xml_2_templates.xsl"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:hl7v2="urn:hl7-org:v2xml"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:local="urn:local"
    exclude-result-prefixes="#all"
    version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> May 9, 2014</xd:p>
            <xd:p><xd:b>Author:</xd:b> ahenket</xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    
    <xsl:output indent="yes" omit-xml-declaration="yes"/>
    
    <xsl:variable name="classification-format" select="'hl7v2.5xml'"/>
    <xsl:variable name="template-default-id-root-messages" select="'2.16.840.1.113883.3.1937.99.60.4.12'"/>
    <xsl:variable name="template-default-id-root-segments" select="'2.16.840.1.113883.3.1937.99.60.4.13'"/>
    <xsl:variable name="template-default-id-root-fields" select="'2.16.840.1.113883.3.1937.99.60.4.14'"/>
    <xsl:variable name="template-default-id-root-datatypes" select="'2.16.840.1.113883.3.1937.99.60.4.15'"/>
    <xsl:variable name="template-default-status" select="'draft'"/>
    <xsl:variable name="template-default-effectiveDate" select="'2013-02-10T00:00:00'"/>
    <xsl:variable name="message-definitions" select="collection(iri-to-uri(concat('v25/xsd?select=', '???.xsd;recurse=no')))|collection(iri-to-uri(concat('v25/xsd?select=', '???_???.xsd;recurse=no')))"/>
    <xsl:variable name="segment-definitions" select="doc('v25/xsd/segments.xsd')/xs:schema/xs:element"/>
    <xsl:variable name="field-definitions" select="doc('v25/xsd/fields.xsd')/xs:schema/xs:element"/>
    <xsl:variable name="baseTypes" select="$field-definitions/ancestor::xs:schema//@base"/>
    <xsl:variable name="datatype-definitions" select="doc('v25/xsd/datatypes.xsd')/xs:schema"/>
    <!-- Source: fhir/trunk/build/source/v2/source.xml but with additions from the V2.5 guides for certain missing events -->
    <xsl:variable name="messageList">
        <version version="2.8.2" desc="Event Type">
            <item code="A01" desc="ADT/ACK - Admit/visit notification"/>
            <item code="A02" desc="ADT/ACK - Transfer a patient"/>
            <item code="A03" desc="ADT/ACK -  Discharge/end visit"/>
            <item code="A04" desc="ADT/ACK -  Register a patient"/>
            <item code="A05" desc="ADT/ACK -  Pre-admit a patient"/>
            <item code="A06" desc="ADT/ACK -  Change an outpatient to an inpatient"/>
            <item code="A07" desc="ADT/ACK -  Change an inpatient to an outpatient"/>
            <item code="A08" desc="ADT/ACK -  Update patient information"/>
            <item code="A09" desc="ADT/ACK -  Patient departing - tracking"/>
            <item code="A10" desc="ADT/ACK -  Patient arriving - tracking"/>
            <item code="A11" desc="ADT/ACK -  Cancel admit/visit notification"/>
            <item code="A12" desc="ADT/ACK -  Cancel transfer"/>
            <item code="A13" desc="ADT/ACK -  Cancel discharge/end visit"/>
            <item code="A14" desc="ADT/ACK -  Pending admit"/>
            <item code="A15" desc="ADT/ACK -  Pending transfer"/>
            <item code="A16" desc="ADT/ACK -  Pending discharge"/>
            <item code="A17" desc="ADT/ACK -  Swap patients"/>
            <item code="A18" desc="ADT/ACK -  Merge patient information" comments="Deprecated"/>
            <item code="A19" desc="QRY/ADR -  Patient query" comments="Deprecated"/>
            <item code="A20" desc="ADT/ACK -  Bed status update"/>
            <item code="A21" desc="ADT/ACK -  Patient goes on a &quot;leave of absence&quot;"/>
            <item code="A22" desc="ADT/ACK -  Patient returns from a &quot;leave of absence&quot;"/>
            <item code="A23" desc="ADT/ACK -  Delete a patient record"/>
            <item code="A24" desc="ADT/ACK -  Link patient information"/>
            <item code="A25" desc="ADT/ACK -  Cancel pending discharge"/>
            <item code="A26" desc="ADT/ACK -  Cancel pending transfer"/>
            <item code="A27" desc="ADT/ACK -  Cancel pending admit"/>
            <item code="A28" desc="ADT/ACK -  Add person information"/>
            <item code="A29" desc="ADT/ACK -  Delete person information"/>
            <item code="A30" desc="ADT/ACK -  Merge person information" comments="Deprecated"/>
            <item code="A31" desc="ADT/ACK -  Update person information"/>
            <item code="A32" desc="ADT/ACK -  Cancel patient arriving - tracking"/>
            <item code="A33" desc="ADT/ACK -  Cancel patient departing - tracking"/>
            <item code="A34" desc="ADT/ACK -  Merge patient information - patient ID only" comments="Deprecated"/>
            <item code="A35" desc="ADT/ACK -  Merge patient information - account number only" comments="Deprecated"/>
            <item code="A36" desc="ADT/ACK -  Merge patient information - patient ID and account number" comments="Deprecated"/>
            <item code="A37" desc="ADT/ACK -  Unlink patient information"/>
            <item code="A38" desc="ADT/ACK - Cancel pre-admit"/>
            <item code="A39" desc="ADT/ACK - Merge person - patient ID" comments="Deprecated"/>
            <item code="A40" desc="ADT/ACK - Merge patient - patient identifier list"/>
            <item code="A41" desc="ADT/ACK - Merge account - patient account number"/>
            <item code="A42" desc="ADT/ACK - Merge visit - visit number"/>
            <item code="A43" desc="ADT/ACK - Move patient information - patient identifier list"/>
            <item code="A44" desc="ADT/ACK - Move account information - patient account number"/>
            <item code="A45" desc="ADT/ACK - Move visit information - visit number"/>
            <item code="A46" desc="ADT/ACK - Change patient ID" comments="Deprecated"/>
            <item code="A47" desc="ADT/ACK - Change patient identifier list"/>
            <item code="A48" desc="ADT/ACK - Change alternate patient ID" comments="Deprecated"/>
            <item code="A49" desc="ADT/ACK - Change patient account number"/>
            <item code="A50" desc="ADT/ACK - Change visit number"/>
            <item code="A51" desc="ADT/ACK - Change alternate visit ID"/>
            <item code="A52" desc="ADT/ACK - Cancel leave of absence for a patient"/>
            <item code="A53" desc="ADT/ACK - Cancel patient returns from a leave of absence"/>
            <item code="A54" desc="ADT/ACK - Change attending doctor"/>
            <item code="A55" desc="ADT/ACK - Cancel change attending doctor"/>
            <item code="A60" desc="ADT/ACK - Update allergy information"/>
            <item code="A61" desc="ADT/ACK - Change consulting doctor"/>
            <item code="A62" desc="ADT/ACK - Cancel change consulting doctor"/>
            <item code="B01" desc="PMU/ACK - Add personnel record"/>
            <item code="B02" desc="PMU/ACK - Update personnel record"/>
            <item code="B03" desc="PMU/ACK - Delete personnel re cord"/>
            <item code="B04" desc="PMU/ACK - Active practicing person"/>
            <item code="B05" desc="PMU/ACK - Deactivate practicing person"/>
            <item code="B06" desc="PMU/ACK - Terminate practicing person"/>
            <item code="B07" desc="PMU/ACK - Grant Certificate/Permission"/>
            <item code="B08" desc="PMU/ACK - Revoke Certificate/Permission"/>
            <item code="C01" desc="CRM - Register a patient on a clinical trial"/>
            <item code="C02" desc="CRM - Cancel a patient registration on clinical trial (for clerical mistakes only)"/>
            <item code="C03" desc="CRM - Correct/update registration information"/>
            <item code="C04" desc="CRM - Patient has gone off a clinical trial"/>
            <item code="C05" desc="CRM - Patient enters phase of clinical trial"/>
            <item code="C06" desc="CRM - Cancel patient entering a phase (clerical mistake)"/>
            <item code="C07" desc="CRM - Correct/update phase information"/>
            <item code="C08" desc="CRM - Patient has gone off phase of clinical trial"/>
            <item code="C09" desc="CSU - Automated time intervals for reporting, like monthly"/>
            <item code="C10" desc="CSU - Patient completes the clinical trial"/>
            <item code="C11" desc="CSU - Patient completes a phase of the clinical trial"/>
            <item code="C12" desc="CSU - Update/correction of patient order/result information"/>
            <item code="E01" desc="Submit HealthCare Services Invoice"/>
            <item code="E02" desc="Cancel HealthCare Services Invoice"/>
            <item code="E03" desc="HealthCare Services Invoice Status"/>
            <item code="E04" desc="Re-Assess HealthCare Services Invoice Request"/>
            <item code="E10" desc="Edit/Adjudication Results"/>
            <item code="E12" desc="Request Additional Information"/>
            <item code="E13" desc="Additional Information Response"/>
            <item code="E15" desc="Payment/Remittance Advice"/>
            <item code="E20" desc="Submit Authorization Request"/>
            <item code="E21" desc="Cancel Authorization Request"/>
            <item code="E22" desc="Authorization Request Status"/>
            <item code="E24" desc="Authorization Response"/>
            <item code="E30" desc="Submit Health Document related to Authorization Request" comments="reserved for future/not yet defined"/>
            <item code="E31" desc="Cancel Health Document related to Authorization Request" comments="reserved for future/not yet defined"/>
            <item code="I01" desc="RQI/RPI - Request for insurance information"/>
            <item code="I02" desc="RQI/RPL - Request/receipt of patient selection display list"/>
            <item code="I03" desc="RQI/RPR - Request/receipt of patient selection list"/>
            <item code="I04" desc="RQD/RPI - Request for patient demographic data"/>
            <item code="I05" desc="RQC/RCI - Request for patient clinical information" comments="Deprecated"/>
            <item code="I06" desc="RQC/RCL - Request/receipt of clinical data listing" comments="Deprecated"/>
            <item code="I07" desc="PIN/ACK - Unsolicited insurance information"/>
            <item code="I08" desc="RQA/RPA - Request for treatment authorization information"/>
            <item code="I09" desc="RQA/RPA - Request for modification to an authorization"/>
            <item code="I10" desc="RQA/RPA - Request for resubmission of an authorization"/>
            <item code="I11" desc="RQA/RPA - Request for cancellation of an authorization"/>
            <item code="I12" desc="REF/RRI - Patient referral"/>
            <item code="I13" desc="REF/RRI - Modify patient referral"/>
            <item code="I14" desc="REF/RRI - Cancel patient referral"/>
            <item code="I15" desc="REF/RRI - Request patient referral status"/>
            <item code="I16" desc="Collaborative Care Referral"/>
            <item code="I17" desc="Modify Collaborative Care Referral" comments="Deprecated"/>
            <item code="I18" desc="Cancel Collaborative Care Referral" comments="Deprecated"/>
            <item code="I19" desc="Collaborative Care Query/Collaborative Care Query Update"/>
            <item code="I20" desc="Asynchronous Collaborative Care Update"/>
            <item code="I21" desc="Collaborative Care Message"/>
            <item code="I22" desc="Collaborative Care Fetch / Collaborative Care Information"/>
            <item code="J01" desc="QCN/ACK - Cancel query/acknowledge message"/>
            <item code="J02" desc="QSX/ACK - Cancel subscription/acknowledge message"/>
            <item code="K11" desc="RSP - Segment pattern response in response to QBP^Q11"/>
            <item code="K13" desc="RTB - Tabular response in response to QBP^Q13"/>
            <item code="K15" desc="RDY - Display response in response to QBP^Q15"/>
            <item code="K21" desc="RSP - Get person demographics response"/>
            <item code="K22" desc="RSP - Find candidates response"/>
            <item code="K23" desc="RSP - Get corresponding identifiers response"/>
            <item code="K24" desc="RSP - Allocate identifiers response"/>
            <item code="K25" desc="RSP - Personnel Information by Segment Response"/>
            <item code="K31" desc="RSP -Dispense History Response"/>
            <item code="K32" desc="Find Candidates including Visit Information Response"/>
            <item code="K33" desc="Get Donor Record Candidates Response Message"/>
            <item code="K34" desc="Segment Pattern Response Message"/>
            <item code="M01" desc="MFN/MFK - Master file not otherwise specified" comments="Deprecated"/>
            <item code="M02" desc="MFN/MFK - Master file - staff practitioner"/>
            <item code="M03" desc="MFN/MFK - Master file - test/observation" comments="Deprecated"/>
            <item code="M04" desc="MFN/MFK - Master files charge description"/>
            <item code="M05" desc="MFN/MFK - Patient location master file"/>
            <item code="M06" desc="MFN/MFK - Clinical study with phases and schedules master file"/>
            <item code="M07" desc="MFN/MFK - Clinical study without phases but with schedules master file"/>
            <item code="M08" desc="MFN/MFK - Test/observation (numeric) master file"/>
            <item code="M09" desc="MFN/MFK - Test/observation (categorical) master file"/>
            <item code="M10" desc="MFN/MFK - Test /observation batteries master file"/>
            <item code="M11" desc="MFN/MFK - Test/calculated observations master file"/>
            <item code="M12" desc="MFN/MFK - Master file notification message"/>
            <item code="M13" desc="MFN/MFK - Master file notification - general"/>
            <item code="M14" desc="MFN/MFK - Master file notification - site defined"/>
            <item code="M15" desc="MFN/MFK - Inventory item master file notification"/>
            <item code="M16" desc="MFN/MFK - Master File Notification Inventory Item Enhanced"/>
            <item code="M17" desc="DRG Master File Message"/>
            <item code="M18" desc="MFN/MFK - Master file notification - Test/Observation (Payer)"/>
            <item code="N01" desc="NMQ/NMR - Application management query message" comments="Deprecated"/>
            <item code="N02" desc="NMD/ACK - Application management data message (unsolicited)"/>
            <item code="O01" desc="ORM - Order message (also RDE, RDS, RGV, RAS)" comments="Deprecated"/>
            <item code="O02" desc="ORR - Order response (also RRE, RRD, RRG, RRA)" comments="Deprecated"/>
            <item code="O03" desc="OMD - Diet order"/>
            <item code="O04" desc="ORD - Diet order acknowledgment"/>
            <item code="O05" desc="OMS - Stock requisition order"/>
            <item code="O06" desc="ORS - Stock requisition acknowledgment"/>
            <item code="O07" desc="OMN - Non-stock requisition order"/>
            <item code="O08" desc="ORN - Non-stock requisition acknowledgment"/>
            <item code="O09" desc="OMP - Pharmacy/treatment order"/>
            <item code="O10" desc="ORP - Pharmacy/treatment order acknowledgment"/>
            <item code="O11" desc="RDE - Pharmacy/treatment encoded order"/>
            <item code="O12" desc="RRE - Pharmacy/treatment encoded order acknowledgment"/>
            <item code="O13" desc="RDS - Pharmacy/treatment dispense"/>
            <item code="O14" desc="RRD - Pharmacy/treatment dispense acknowledgment"/>
            <item code="O15" desc="RGV - Pharmacy/treatment give"/>
            <item code="O16" desc="RRG - Pharmacy/treatment give acknowledgment"/>
            <item code="O17" desc="RAS - Pharmacy/treatment administration"/>
            <item code="O18" desc="RRA - Pharmacy/treatment administration acknowledgment"/>
            <item code="O19" desc="OMG - General clinical order"/>
            <item code="O20" desc="ORG/ORL - General clinical order response"/>
            <item code="O21" desc="OML - Laboratory order"/>
            <item code="O22" desc="ORL - General laboratory order response message to any OML"/>
            <item code="O23" desc="OMI - Imaging order"/>
            <item code="O24" desc="ORI - Imaging order response message to any OMI"/>
            <item code="O25" desc="RDE - Pharmacy/treatment refill authorization request"/>
            <item code="O26" desc="RRE - Pharmacy/Treatment Refill Authorization Acknowledgement"/>
            <item code="O27" desc="OMB - Blood product order"/>
            <item code="O28" desc="ORB - Blood product order acknowledgment"/>
            <item code="O29" desc="BPS - Blood product dispense status"/>
            <item code="O30" desc="BRP - Blood product dispense status acknowledgment"/>
            <item code="O31" desc="BTS - Blood product transfusion/disposition"/>
            <item code="O32" desc="BRT - Blood product transfusion/disposition acknowledgment"/>
            <item code="O33" desc="OML - Laboratory order for multiple orders related to a single specimen"/>
            <item code="O34" desc="ORL - Laboratory order response message to a multiple order related to single specimen OML"/>
            <item code="O35" desc="OML - Laboratory order for multiple orders related to a single container of a specimen"/>
            <item code="O36" desc="ORL - Laboratory order response message to a single container of a specimen OML"/>
            <item code="O37" desc="OPL - Population/Location-Based Laboratory Order Message"/>
            <item code="O38" desc="OPR - Population/Location-Based Laboratory Order Acknowledgment Message"/>
            <item code="O39" desc="Specimen shipment centric laboratory order"/>
            <item code="O40" desc="Specimen Shipment Centric Laboratory Order Acknowledgment Message"/>
            <item code="O41" desc="DBC - Create Donor Record Message"/>
            <item code="O42" desc="DBU - Update Donor Record Message"/>
            <item code="O43" desc="General Order Message with Document Payload Acknowledgement Message"/>
            <item code="O44" desc="Donor Registration - Minimal Message"/>
            <item code="O45" desc="Donor Eligibility Observations Message"/>
            <item code="O46" desc="Donor Eligiblity Message"/>
            <item code="O47" desc="Donor Request to Collect Message"/>
            <item code="O48" desc="Donation Procedure Message"/>
            <item code="P01" desc="BAR/ACK - Add patient accounts"/>
            <item code="P02" desc="BAR/ACK - Purge patient accounts"/>
            <item code="P03" desc="DFT/ACK - Post detail financial transaction"/>
            <item code="P04" desc="QRY/DSP - Generate bill and A/R statements" comments="Deprecated"/>
            <item code="P05" desc="BAR/ACK - Update account"/>
            <item code="P06" desc="BAR/ACK - End account"/>
            <item code="P07" desc="PEX - Unsolicited initial individual product experience report"/>
            <item code="P08" desc="PEX - Unsolicited update individual product experience report"/>
            <item code="P09" desc="SUR - Summary product experience report" comments="Deprecated"/>
            <item code="P10" desc="BAR/ACK -Transmit Ambulatory Payment  Classification(APC)"/>
            <item code="P11" desc="DFT/ACK - Post Detail Financial Transactions - New"/>
            <item code="P12" desc="BAR/ACK - Update Diagnosis/Procedure"/>
            <item code="PC1" desc="PPR - PC/ problem add"/>
            <item code="PC2" desc="PPR - PC/ problem update"/>
            <item code="PC3" desc="PPR - PC/ problem delete"/>
            <item code="PC4" desc="QRY - PC/ problem query" comments="Deprecated"/>
            <item code="PC5" desc="PRR - PC/ problem response" comments="Deprecated"/>
            <item code="PC6" desc="PGL - PC/ goal add"/>
            <item code="PC7" desc="PGL - PC/ goal update"/>
            <item code="PC8" desc="PGL - PC/ goal delete"/>
            <item code="PC9" desc="QRY - PC/ goal query" comments="Deprecated"/>
            <item code="PCA" desc="PPV - PC/ goal response" comments="Deprecated"/>
            <item code="PCB" desc="PPP - PC/ pathway (problem-oriented) add"/>
            <item code="PCC" desc="PPP - PC/ pathway (problem-oriented) update"/>
            <item code="PCD" desc="PPP - PC/ pathway (problem-oriented) delete"/>
            <item code="PCE" desc="QRY - PC/ pathway (problem-oriented) query" comments="Deprecated"/>
            <item code="PCF" desc="PTR - PC/ pathway (problem-oriented) query response" comments="Deprecated"/>
            <item code="PCG" desc="PPG - PC/ pathway (goal-oriented) add"/>
            <item code="PCH" desc="PPG - PC/ pathway (goal-oriented) update"/>
            <item code="PCJ" desc="PPG - PC/ pathway (goal-oriented) delete"/>
            <item code="PCK" desc="QRY - PC/ pathway (goal-oriented) query" comments="Deprecated"/>
            <item code="PCL" desc="PPT - PC/ pathway (goal-oriented) query response" comments="Deprecated"/>
            <item code="Q01" desc="QRY/DSR - Query sent for immediate response" comments="Deprecated"/>
            <item code="Q02" desc="QRY/QCK - Query sent for deferred response" comments="Deprecated"/>
            <item code="Q03" desc="DSR/ACK - Deferred response to a query" comments="Deprecated"/>
            <item code="Q04" desc="EQQ - Embedded query language query" comments="Deprecated"/>
            <item code="Q05" desc="UDM/ACK - Unsolicited display update message"/>
            <item code="Q06" desc="OSQ/OSR - Query for order status" comments="Deprecated"/>
            <item code="Q07" desc="VQQ - Virtual table query" comments="Deprecated"/>
            <item code="Q08" desc="RQQ - event replay query" comments="Deprecated"/>
            <item code="Q09" desc="SPQ - Stored procedure request" comments="Deprecated"/>
            <item code="Q11" desc="QBP - Query by parameter requesting an RSP segment pattern response"/>
            <item code="Q13" desc="QBP - Query by parameter requesting an  RTB - tabular response"/>
            <item code="Q15" desc="QBP - Query by parameter requesting an RDY display response"/>
            <item code="Q16" desc="QSB - Create subscription"/>
            <item code="Q17" desc="QVR - Query for previous events"/>
            <item code="Q21" desc="QBP - Get person demographics"/>
            <item code="Q22" desc="QBP - Find candidates"/>
            <item code="Q23" desc="QBP - Get corresponding identifiers"/>
            <item code="Q24" desc="QBP - Allocate identifiers"/>
            <item code="Q25" desc="QBP - Personnel Information by Segment Query"/>
            <item code="Q26" desc="ROR - Pharmacy/treatment order response" comments="Deprecated"/>
            <item code="Q27" desc="RAR - Pharmacy/treatment administration information" comments="Deprecated"/>
            <item code="Q28" desc="RDR - Pharmacy/treatment dispense information" comments="Deprecated"/>
            <item code="Q29" desc="RER - Pharmacy/treatment encoded order information" comments="Deprecated"/>
            <item code="Q30" desc="RGR - Pharmacy/treatment dose information" comments="Deprecated"/>
            <item code="Q31" desc="QBP Query Dispense history"/>
            <item code="Q32" desc="Find Candidates including Visit Information"/>
            <item code="Q33" desc="QBP - Get Donor Record Candidates"/>
            <item code="Q34" desc="QBP - Get Donor Record"/>
            <item code="R01" desc="ORU/ACK - Unsolicited transmission of an observation message"/>
            <item code="R02" desc="QRY - Query for results of observation" comments="Deprecated"/>
            <item code="R03" desc="QRY/DSR Display-oriented results, query/unsol. update (for backward compatibility only) (Replaced by Q05)"/>
            <item code="R04" desc="ORF - Response to query; transmission of requested observation"/>
            <item code="R07" desc="EDR - Enhanced Display Response"/>
            <item code="R08" desc="TBR - Tabular Data Response"/>
            <item code="R09" desc="ERP - Event Replay Response"/>
            <item code="ROR" desc="ROR - Pharmacy prescription order query response" comments="Deprecated"/>
            <item code="R21" desc="OUL - Unsolicited laboratory observation" comments="Deprecated"/>
            <item code="R22" desc="OUL - Unsolicited Specimen Oriented Observation Message"/>
            <item code="R23" desc="OUL - Unsolicited Specimen Container Oriented Observation Message"/>
            <item code="R24" desc="OUL - Unsolicited Order Oriented Observation Message"/>
            <item code="R25" desc="OPU - Unsolicited Population/Location-Based Laboratory Observation Message"/>
            <item code="R26" desc="OSM - Unsolicited Specimen Shipment Manifest Message"/>
            <item code="R30" desc="ORU - Unsolicited Point-Of-Care Observation Message Without Existing Order - Place An Order"/>
            <item code="R31" desc="ORU - Unsolicited New Point-Of-Care Observation Message - Search For An Order"/>
            <item code="R32" desc="ORU - Unsolicited Pre-Ordered Point-Of-Care Observation"/>
            <item code="R33" desc="ORA - Observation Report Acknowledgement"/>
            <item code="R40" desc="ORU - Unsolicited Report Alarm"/>
            <item code="R41" desc="Observation Report Alert Acknowledgement"/>
            <item code="RAR" desc="Pharmacy administration information"/>
            <item code="RDR" desc="Pharmacy dispense information"/>
            <item code="RER" desc="Pharmacy encoded order information"/>
            <item code="RGR" desc="Pharmacy give information"/>
            <item code="S01" desc="SRM/SRR - Request new appointment booking"/>
            <item code="S02" desc="SRM/SRR - Request appointment rescheduling"/>
            <item code="S03" desc="SRM/SRR - Request appointment modification"/>
            <item code="S04" desc="SRM/SRR - Request appointment cancellation"/>
            <item code="S05" desc="SRM/SRR - Request appointment discontinuation"/>
            <item code="S06" desc="SRM/SRR - Request appointment deletion"/>
            <item code="S07" desc="SRM/SRR - Request addition of service/resource on appointment"/>
            <item code="S08" desc="SRM/SRR - Request modification of service/resource on appointment"/>
            <item code="S09" desc="SRM/SRR - Request cancellation of service/resource on appointment"/>
            <item code="S10" desc="SRM/SRR - Request discontinuation of service/resource on appointment"/>
            <item code="S11" desc="SRM/SRR - Request deletion of service/resource on appointment"/>
            <item code="S12" desc="SIU/ACK - Notification of new appointment booking"/>
            <item code="S13" desc="SIU/ACK - Notification of appointment rescheduling"/>
            <item code="S14" desc="SIU/ACK - Notification of appointment modification"/>
            <item code="S15" desc="SIU/ACK - Notification of appointment cancellation"/>
            <item code="S16" desc="SIU/ACK - Notification of appointment discontinuation"/>
            <item code="S17" desc="SIU/ACK - Notification of appointment deletion"/>
            <item code="S18" desc="SIU/ACK - Notification of addition of service/resource on appointment"/>
            <item code="S19" desc="SIU/ACK - Notification of modification of service/resource on appointment"/>
            <item code="S20" desc="SIU/ACK - Notification of cancellation of service/resource on appointment"/>
            <item code="S21" desc="SIU/ACK - Notification of discontinuation of service/resource on appointment"/>
            <item code="S22" desc="SIU/ACK - Notification of deletion of service/resource on appointment"/>
            <item code="S23" desc="SIU/ACK - Notification of blocked schedule time slot(s)"/>
            <item code="S24" desc="SIU/ACK - Notification of opened (&quot;unblocked&quot;) schedule time slot(s)"/>
            <item code="S25" desc="SQM/SQR - Schedule query message and response" comments="Deprecated"/>
            <item code="S26" desc="SIU/ACK Notification that patient did not show up for schedule appointment"/>
            <item code="S27" desc="SIU/ACK - Broadcast Notification of Scheduled Appointments"/>
            <item code="S28" desc="SLR/SLS - Request new sterilization lot"/>
            <item code="S29" desc="SLR/SLS - Request Sterilization lot deletion"/>
            <item code="S30" desc="STI/STS - Request item"/>
            <item code="S31" desc="SDR/SDS - Request anti-microbial device data"/>
            <item code="S32" desc="SMD/SMS - Request anti-microbial device cycle data"/>
            <item code="S33" desc="STC/ACK - Notification of sterilization configuration"/>
            <item code="S34" desc="SLN/ACK - Notification of sterilization lot"/>
            <item code="S35" desc="SLN/ACK - Notification of sterilization lot deletion"/>
            <item code="S36" desc="SDN/ACK - Notification of anti-microbial device data"/>
            <item code="S37" desc="SCN/ACK - Notification of anti-microbial device cycle data"/>
            <item code="T01" desc="MDM/ACK - Original document notification"/>
            <item code="T02" desc="MDM/ACK - Original document notification and content"/>
            <item code="T03" desc="MDM/ACK - Document status change notification"/>
            <item code="T04" desc="MDM/ACK - Document status change notification and content"/>
            <item code="T05" desc="MDM/ACK - Document addendum notification"/>
            <item code="T06" desc="MDM/ACK - Document addendum notification and content"/>
            <item code="T07" desc="MDM/ACK - Document edit notification"/>
            <item code="T08" desc="MDM/ACK - Document edit notification and content"/>
            <item code="T09" desc="MDM/ACK - Document replacement notification"/>
            <item code="T10" desc="MDM/ACK - Document replacement notification and content"/>
            <item code="T11" desc="MDM/ACK - Document cancel notification"/>
            <item code="T12" desc="QRY/DOC - Document query" comments="Deprecated"/>
            <item code="U01" desc="ESU/ACK - Automated equipment status update"/>
            <item code="U02" desc="ESR/ACK - Automated equipment status request"/>
            <item code="U03" desc="SSU/ACK - Specimen status update"/>
            <item code="U04" desc="SSR/ACK - specimen status request"/>
            <item code="U05" desc="INU/ACK  - Automated equipment inventory update"/>
            <item code="U06" desc="INR/ACK - Automated equipment inventory request"/>
            <item code="U07" desc="EAC/ACK - Automated equipment command"/>
            <item code="U08" desc="EAR/ACK - Automated equipment response"/>
            <item code="U09" desc="EAN/ACK - Automated equipment notification"/>
            <item code="U10" desc="TCU/ACK - Automated equipment test code settings update"/>
            <item code="U11" desc="TCR/ACK - Automated equipment test code settings request"/>
            <item code="U12" desc="LSU/ACK - Automated equipment log/service update"/>
            <item code="U13" desc="LSR/ACK - Automated equipment log/service request"/>
            <item code="V01" desc="VXQ - Query for vaccination record" comments="Deprecated"/>
            <item code="V02" desc="VXX - Response to vaccination query returning multiple PID matches" comments="Deprecated"/>
            <item code="V03" desc="VXR - Vaccination record response" comments="Deprecated"/>
            <item code="V04" desc="VXU - Unsolicited vaccination record update"/>
            <item code="Varies" desc="MFQ/MFR - Master files query (use event same as asking for e.g. M05 - location)"/>
            <item code="W01" desc="ORU - Waveform result, unsolicited transmission of requested information" comments="Deprecated"/>
            <item code="W02" desc="QRF - Waveform result, response to query" comments="Deprecated"/>
            <!-- additions -->
            <item code="ACK" desc="Acknowledgement Message"/>
            <item code="QRY" desc="Query Message"/>
            <item code="BATCH" desc="BATCH"/>
            <item code="MESSAGEBATCH" desc="MESSAGEBATCH"/>
            <item code="QRY" desc="Query Message"/>
            <item code="Z82" desc="RSP - Pharmacy Dispense Message"/>
            <item code="Z86" desc="RSP - Pharmacy Information Comprehensive"/>
            <item code="Z88" desc="RSP - Pharmacy Dispense Information"/>
            <item code="Z90" desc="RSP - Lab Results History"/>
            <item code="Z73" desc="QBP - Personnel Information Message by Phone Number"/>
            <item code="Z74" desc="RTB - Personnel Information Message by Phone Number"/>
            <!--<item code="MFN_Znn" desc="???"/>-->
        </version>
    </xsl:variable>
    <!-- Source: Appendix A (Word) 2.5 -->
    <xsl:variable name="messageStructureList">
        <version version="2.5" desc="Message structure">
            <item code="ACK" desc="Varies"/>
            <item code="ADR_A19" desc="A19"/>
            <item code="ADT_A01" desc="A01, A04, A08, A13"/>
            <item code="ADT_A02" desc="A02"/>
            <item code="ADT_A03" desc="A03"/>
            <item code="ADT_A05" desc="A05, A14, A28, A31"/>
            <item code="ADT_A06" desc="A06, A07"/>
            <item code="ADT_A09" desc="A09, A10, A11, A12"/>
            <item code="ADT_A15" desc="A15"/>
            <item code="ADT_A16" desc="A16"/>
            <item code="ADT_A17" desc="A17"/>
            <item code="ADT_A18" desc="A18"/>
            <item code="ADT_A20" desc="A20"/>
            <item code="ADT_A21" desc="A21, A22, A23, A25, A26, A27, A29, A32, A33"/>
            <item code="ADT_A24" desc="A24"/>
            <item code="ADT_A30" desc="A30, A34, A35, A36, A46, A47, A48, A49"/>
            <item code="ADT_A37" desc="A37"/>
            <item code="ADT_A38" desc="A38"/>
            <item code="ADT_A39" desc="A39, A40, A41, A42"/>
            <item code="ADT_A43" desc="A43, A44"/>
            <item code="ADT_A45" desc="A45"/>
            <item code="ADT_A50" desc="A50, A51"/>
            <item code="ADT_A52" desc="A52, A53, A55"/>
            <item code="ADT_A54" desc="A54"/>
            <item code="ADT_A60" desc="A60"/>
            <item code="ADT_A61" desc="A61, A62"/>
            <item code="BAR_P01" desc="P01"/>
            <item code="BAR_P02" desc="P02"/>
            <item code="BAR_P05" desc="P05"/>
            <item code="BAR_P06" desc="P06"/>
            <item code="BAR_P10" desc="P10"/>
            <item code="BAR_P12" desc="P12"/>
            <item code="BPS_O29" desc="O29"/>
            <item code="BRP_030" desc="O30" comments="this is a typo BRP_O30) and should be removed"/>
            <item code="BRP_O30" desc="O30" comments="added as a correction for BRP_030"/>
            <item code="BRT_O32" desc="O32"/>
            <item code="BTS_O31" desc="O31"/>
            <item code="CRM_C01" desc="C01, C02, C03, C04, C05, C06, C07, C08"/>
            <item code="CSU_C09" desc="C09, C10, C11, C12"/>
            <item code="DFT_P03" desc="P03"/>
            <item code="DFT_P11" desc="P11"/>
            <item code="DOC_T12" desc="T12"/>
            <item code="DSR_P04" desc="P04"/>
            <item code="DSR_Q01" desc="Q01"/>
            <item code="DSR_Q03" desc="Q03"/>
            <item code="EAC_U07" desc="U07"/>
            <item code="EAN_U09" desc="U09"/>
            <item code="EAR_U08" desc="U08"/>
            <item code="EDR_R07" desc="R07"/>
            <item code="EQQ_Q04" desc="Q04"/>
            <item code="ERP_R09" desc="R09"/>
            <item code="ESR_U02" desc="U02"/>
            <item code="ESU_U01" desc="U01"/>
            <item code="INR_U06" desc="U06"/>
            <item code="INU_U05" desc="U05"/>
            <item code="LSU_U12" desc="U12, U13"/>
            <item code="MDM_T01" desc="T01, T03, T05, T07, T09, T11"/>
            <item code="MDM_T02" desc="T02, T04, T06, T08, T10"/>
            <item code="MFD_MFA" desc="MFA"/>
            <item code="MFK_M01" desc="M01, M02, M03, M04, M05, M06, M07, M08, M09, M10, M11"/>
            <item code="MFN_M01" desc="M01"/>
            <item code="MFN_M02" desc="M02"/>
            <item code="MFN_M03" desc="M03"/>
            <item code="MFN_M04" desc="M04"/>
            <item code="MFN_M05" desc="M05"/>
            <item code="MFN_M06" desc="M06"/>
            <item code="MFN_M07" desc="M07"/>
            <item code="MFN_M08" desc="M08"/>
            <item code="MFN_M09" desc="M09"/>
            <item code="MFN_M10" desc="M10"/>
            <item code="MFN_M11" desc="M11"/>
            <item code="MFN_M12" desc="M12"/>
            <item code="MFN_M13" desc="M13"/>
            <item code="MFN_M15" desc="M15"/>
            <item code="MFQ_M01" desc="M01, M02, M03, M04, M05, M06"/>
            <item code="MFR_M01" desc="M01, M02, M03, M04, M05, M06"/>
            <item code="NMD_N02" desc="N02"/>
            <item code="NMQ_N01" desc="N01"/>
            <item code="NMR_N01" desc="N01"/>
            <item code="OMB_O27" desc="O27"/>
            <item code="OMD_O03" desc="O03"/>
            <item code="OMG_O19" desc="O19"/>
            <item code="OMI_O23" desc="O23"/>
            <item code="OML_O21" desc="O21"/>
            <item code="OML_O33" desc="O33"/>
            <item code="OML_O35" desc="O35"/>
            <item code="OMN_O07" desc="007"/>
            <item code="OMP_O09" desc="O09"/>
            <item code="OMS_O05" desc="O05"/>
            <item code="ORB_O28" desc="O28"/>
            <item code="ORD_O04" desc="O04"/>
            <item code="ORF_R04" desc="R04"/>
            <item code="ORG_O20" desc="O20"/>
            <item code="ORI_O24" desc="O24"/>
            <item code="ORL_O22" desc="022"/>
            <item code="ORL_O34" desc="O34"/>
            <item code="ORL_O36" desc="O36"/>
            <item code="ORM_O01" desc="O01"/>
            <item code="ORN_O08" desc="O08"/>
            <item code="ORP_O10" desc="O10"/>
            <item code="ORR_O02" desc="O02"/>
            <item code="ORS_O06" desc="O06"/>
            <item code="ORU_R01" desc="R01"/>
            <item code="ORU_R30" desc="R30"/>
            <item code="ORU_R31" desc="R31"/>
            <item code="ORU_R32" desc="R32"/>
            <item code="OSQ_Q06" desc="Q06"/>
            <item code="OSR_Q06" desc="Q06"/>
            <item code="OUL_R21" desc="R21"/>
            <item code="OUL_R22" desc="R22"/>
            <item code="OUL_R23" desc="R23"/>
            <item code="OUL_R24" desc="R24"/>
            <item code="PEX_P07" desc="P07, P08"/>
            <item code="PGL_PC6" desc="PC6, PC7, PC8"/>
            <item code="PMU_B01" desc="B01, B02"/>
            <item code="PMU_B03" desc="B03"/>
            <item code="PMU_B04" desc="B04, B05, B06"/>
            <item code="PMU_B07" desc="B07"/>
            <item code="PMU_B08" desc="B08"/>
            <item code="PPG_PCG" desc="PCC, PCG, PCH, PCJ"/>
            <item code="PPP_PCB" desc="PCB, PCD"/>
            <item code="PPR_PC1" desc="PC1, PC2, PC3"/>
            <item code="PPT_PCL" desc="PCL"/>
            <item code="PPV_PCA" desc="PCA"/>
            <item code="PRR_PC5" desc="PC5"/>
            <item code="PTR_PCF" desc="PCF"/>
            <item code="QBP_Q11" desc="Q11"/>
            <item code="QBP_Q13" desc="Q13"/>
            <item code="QBP_Q15" desc="Q15"/>
            <item code="QBP_Q21" desc="Q21, Q22, Q23,Q24, Q25"/>
            <item code="QCK_Q02" desc="Q02"/>
            <item code="QCN_J01" desc="J01, J02"/>
            <item code="QRY_A19" desc="A19"/>
            <item code="QRY_P04" desc="P04"/>
            <item code="QRY_PC4" desc="PC4, PC9, PCE, PCK"/>
            <item code="QRY_Q01" desc="Q01, Q26, Q27, Q28, Q29, Q30"/>
            <item code="QRY_Q02" desc="Q02"/>
            <item code="QRY_R02" desc="R02"/>
            <item code="QRY_T12" desc="T12"/>
            <item code="QSB_Q16" desc="Q16"/>
            <item code="QVR_Q17" desc="Q17"/>
            <item code="RAR_RAR" desc="RAR"/>
            <item code="RAS_O17" desc="O17"/>
            <item code="RCI_I05" desc="I05"/>
            <item code="RCL_I06" desc="I06"/>
            <item code="RDE_O01" desc="O01"/>
            <item code="RDE_O11" desc="O11, O25"/>
            <item code="RDR_RDR" desc="RDR"/>
            <item code="RDS_O13" desc="O13"/>
            <item code="RDY_K15" desc="K15"/>
            <item code="REF_I12" desc="I12, I13, I14, I15"/>
            <item code="RER_RER" desc="RER"/>
            <item code="RGR_RGR" desc="RGR"/>
            <item code="RGV_O15" desc="O15"/>
            <item code="ROR_ROR" desc="ROR"/>
            <item code="RPA_I08" desc="I08, I09. I10, I11"/>
            <item code="RPI_I01" desc="I01, I04"/>
            <item code="RPL_I02" desc="I02"/>
            <item code="RPR_I03" desc="I03"/>
            <item code="RQA_I08" desc="I08, I09, I10, I11"/>
            <item code="RQC_I05" desc="I05, I06"/>
            <item code="RQI_I01" desc="I01, I02, I03, I07"/>
            <item code="RQP_I04" desc="I04"/>
            <item code="RQQ_Q09" desc="Q09"/>
            <item code="RRA_O02" desc="O02"/>
            <item code="RRA_O18" desc="O18"/>
            <item code="RRD_O14" desc="O14"/>
            <item code="RRE_O12" desc="O12, O26"/>
            <item code="RRG_O16" desc="O16"/>
            <item code="RRI_I12" desc="I12, I13, I14, I15"/>
            <item code="RSP_K11" desc="K11"/>
            <item code="RSP_K21" desc="K21"/>
            <item code="RSP_K22" desc="K22"/>
            <item code="RSP_K23" desc="K23, K24"/>
            <item code="RSP_K25" desc="K25"/>
            <item code="RSP_K31" desc="K31"/>
            <item code="RTB_K13" desc="K13"/>
            <item code="SIU_S12" desc="S12, S13, S14, S15, S16, S17, S18, S19, S20, S21, S22, S23, S24, S26"/>
            <item code="SPQ_Q08" desc="Q08"/>
            <item code="SQM_S25" desc="S25"/>
            <item code="SQR_S25" desc="S25"/>
            <item code="SRM_S01" desc="S01, S02, S03, S04, S05, S06, S07, S08, S09, S10, S11"/>
            <item code="SRR_S01" desc="S01, S02, S03, S04, S05, S06, S07, S08, S09, S10, S11"/>
            <item code="SSR_U04" desc="U04"/>
            <item code="SSU_U03" desc="U03"/>
            <item code="SUR_P09" desc="P09"/>
            <item code="TBR_R08" desc="R08"/>
            <item code="TBR_R09" desc="R09"/>
            <item code="TCU_U10" desc="U10, U11"/>
            <item code="UDM_Q05" desc="Q05"/>
            <item code="VQQ_Q07" desc="Q07"/>
            <item code="VXQ_V01" desc="V01"/>
            <item code="VXR_V03" desc="V03"/>
            <item code="VXU_V04" desc="V04"/>
            <item code="VXX_V02" desc="V02"/>
            <item code="ORU_W01" desc="W01"/>
            <item code="QRF_W02" desc="W02"/>
            <item code="QRY" desc=""/>
            <item code="QBP_Z73" desc="Z73"/>
            <item code="RTB_Z74" desc="Z74"/>
            <item code="RSP_Z82" desc="Z82"/>
            <item code="RSP_Z86" desc="Z86"/>
            <item code="RSP_Z88" desc="Z88"/>
            <item code="RSP_Z90" desc="Z90"/>
            <item code="MFN_Znn" desc="Znn"/>
        </version>
    </xsl:variable>
    <!-- Source: Appendix A (Word) 2.5 -->
    <xsl:variable name="segmentList">
        <version version="2.8.2" desc="Segments">
            <item code="ABS" desc="Abstract"/>
            <item code="ACC" desc="Accident"/>
            <item code="ADD" desc="Addendum"/>
            <item code="AFF" desc="Professional Affiliation"/>
            <item code="AIG" desc="Appointment Information _ General Resource"/>
            <item code="AIL" desc="Appointment Information _ Location Resource"/>
            <item code="AIP" desc="Appointment Information _ Personnel Resource"/>
            <item code="AIS" desc="Appointment Information"/>
            <item code="AL1" desc="Patient Allergy Information"/>
            <item code="APR" desc="Appointment Preferences"/>
            <item code="ARQ" desc="Appointment Request"/>
            <item code="AUT" desc="Authorization Information"/>
            <item code="BHS" desc="Batch Header"/>
            <item code="BLC" desc="Blood Code"/>
            <item code="BLG" desc="Billing"/>
            <item code="BPO" desc="Blood product order"/>
            <item code="BPX" desc="Blood product dispense status"/>
            <item code="BTS" desc="Batch Trailer"/>
            <item code="BTX" desc="Blood Product Transfusion/Disposition"/>
            <item code="CDM" desc="Charge Description Master"/>
            <item code="CER" desc="Certificate Detail"/>
            <item code="CM0" desc="Clinical Study Master"/>
            <item code="CM1" desc="Clinical Study Phase Master"/>
            <item code="CM2" desc="Clinical Study Schedule Master"/>
            <item code="CNS" desc="Clear Notification"/>
            <item code="CON" desc="Consent Segment"/>
            <item code="CSP" desc="Clinical Study Phase"/>
            <item code="CSR" desc="Clinical Study Registration"/>
            <item code="CSS" desc="Clinical Study Data Schedule Segment"/>
            <item code="CTD" desc="Contact Data"/>
            <item code="CTI" desc="Clinical Trial Identification"/>
            <item code="DB1" desc="Disability"/>
            <item code="DG1" desc="Diagnosis"/>
            <item code="DRG" desc="Diagnosis Related Group"/>
            <item code="DSC" desc="Continuation Pointer"/>
            <item code="DSP" desc="Display Data"/>
            <item code="ECD" desc="Equipment Command"/>
            <item code="ECR" desc="Equipment Command Response"/>
            <item code="EDU" desc="Educational Detail"/>
            <item code="EQL" desc="Embedded Query Language"/>
            <item code="EQP" desc="Equipment/log Service"/>
            <item code="EQU" desc="Equipment Detail"/>
            <item code="ERQ" desc="Event replay query"/>
            <item code="ERR" desc="Error"/>
            <item code="EVN" desc="Event Type"/>
            <item code="FAC" desc="Facility"/>
            <item code="FHS" desc="File Header"/>
            <item code="FT1" desc="Financial Transaction"/>
            <item code="FTS" desc="File Trailer"/>
            <item code="GOL" desc="Goal Detail"/>
            <item code="GP1" desc="Grouping/Reimbursement - Visit"/>
            <item code="GP2" desc="Grouping/Reimbursement - Procedure Line Item"/>
            <item code="GT1" desc="Guarantor"/>
            <item code="IAM" desc="Patient Adverse Reaction Information"/>
            <item code="IIM" desc="Inventory Item Master"/>
            <item code="IN1" desc="Insurance"/>
            <item code="IN2" desc="Insurance Additional Information"/>
            <item code="IN3" desc="Insurance Additional Information, Certification"/>
            <item code="INV" desc="Inventory Detail"/>
            <item code="IPC" desc="Imaging Procedure Control Segment"/>
            <item code="ISD" desc="Interaction Status Detail"/>
            <item code="LAN" desc="Language Detail"/>
            <item code="LCC" desc="Location Charge Code"/>
            <item code="LCH" desc="Location Characteristic"/>
            <item code="LDP" desc="Location Department"/>
            <item code="LOC" desc="Location Identification"/>
            <item code="LRL" desc="Location Relationship"/>
            <item code="MFA" desc="Master File Acknowledgment"/>
            <item code="MFE" desc="Master File Entry"/>
            <item code="MFI" desc="Master File Identification"/>
            <item code="MRG" desc="Merge Patient Information"/>
            <item code="MSA" desc="Message Acknowledgment"/>
            <item code="MSH" desc="Message Header"/>
            <item code="NCK" desc="System Clock"/>
            <item code="NDS" desc="Notification Detail"/>
            <item code="NK1" desc="Next of Kin / Associated Parties"/>
            <item code="NPU" desc="Bed Status Update"/>
            <item code="NSC" desc="Application Status Change"/>
            <item code="NST" desc="Application control level statistics"/>
            <item code="NTE" desc="Notes and Comments"/>
            <item code="OBR" desc="Observation Request"/>
            <item code="OBX" desc="Observation/Result"/>
            <item code="ODS" desc="Dietary Orders, Supplements, and Preferences"/>
            <item code="ODT" desc="Diet Tray Instructions"/>
            <item code="OM1" desc="General Segment"/>
            <item code="OM2" desc="Numeric Observation"/>
            <item code="OM3" desc="Categorical Service/Test/Observation"/>
            <item code="OM4" desc="Observations that Require Specimens"/>
            <item code="OM5" desc="Observation Batteries (Sets)"/>
            <item code="OM6" desc="Observations that are Calculated from Other Observations"/>
            <item code="OM7" desc="Additional Basic Attributes"/>
            <item code="ORC" desc="Common Order"/>
            <item code="ORG" desc="Practitioner Organization Unit"/>
            <item code="OVR" desc="Override Segment"/>
            <item code="PCR" desc="Possible Causal Relationship"/>
            <item code="PD1" desc="Patient Additional Demographic"/>
            <item code="PDA" desc="Patient Death and Autopsy"/>
            <item code="PDC" desc="Product Detail Country"/>
            <item code="PEO" desc="Product Experience Observation"/>
            <item code="PES" desc="Product Experience Sender"/>
            <item code="PID" desc="Patient Identification"/>
            <item code="PR1" desc="Procedures"/>
            <item code="PRA" desc="Practitioner Detail"/>
            <item code="PRB" desc="Problem Details"/>
            <item code="PRC" desc="Pricing"/>
            <item code="PRD" desc="Provider Data"/>
            <item code="PSH" desc="Product Summary Header"/>
            <item code="PTH" desc="Pathway"/>
            <item code="PV1" desc="Patient Visit"/>
            <item code="PV2" desc="Patient Visit - Additional Information"/>
            <item code="QAK" desc="Query Acknowledgment"/>
            <item code="QID" desc="Query Identification"/>
            <item code="QPD" desc="Query Parameter Definition"/>
            <item code="QRD" desc="Original-Style Query Definition"/>
            <item code="QRF" desc="Original style query filter"/>
            <item code="QRI" desc="Query Response Instance"/>
            <item code="RCP" desc="Response Control Parameter"/>
            <item code="RDF" desc="Table Row Definition"/>
            <item code="RDT" desc="Table Row Data"/>
            <item code="RF1" desc="Referral Information"/>
            <item code="RGS" desc="Resource Group"/>
            <item code="RMI" desc="Risk Management Incident"/>
            <item code="ROL" desc="Role"/>
            <item code="RQ1" desc="Requisition Detail-1"/>
            <item code="RQD" desc="Requisition Detail"/>
            <item code="RXA" desc="Pharmacy/Treatment Administration"/>
            <item code="RXC" desc="Pharmacy/Treatment Component Order"/>
            <item code="RXD" desc="Pharmacy/Treatment Dispense"/>
            <item code="RXE" desc="Pharmacy/Treatment Encoded Order"/>
            <item code="RXG" desc="Pharmacy/Treatment Give"/>
            <item code="RXO" desc="Pharmacy/Treatment Order"/>
            <item code="RXR" desc="Pharmacy/Treatment Route"/>
            <item code="SAC" desc="Specimen Container detail"/>
            <item code="SCH" desc="Scheduling Activity Information"/>
            <item code="SFT" desc="Software Segment"/>
            <item code="SID" desc="Substance Identifier"/>
            <item code="SPM" desc="Specimen"/>
            <item code="SPR" desc="Stored Procedure Request Definition"/>
            <item code="STF" desc="Staff Identification"/>
            <item code="TCC" desc="Test Code Configuration"/>
            <item code="TCD" desc="Test Code Detail"/>
            <item code="TQ1" desc="Timing/Quantity"/>
            <item code="TQ2" desc="Timing/Quantity Relationship"/>
            <item code="TXA" desc="Transcription Document Header"/>
            <item code="UB1" desc="UB82"/>
            <item code="UB2" desc="UB92 Data"/>
            <item code="URD" desc="Results/update Definition"/>
            <item code="URS" desc="Unsolicited Selection"/>
            <item code="VAR" desc="Variance"/>
            <item code="VTQ" desc="Virtual Table Query Request"/>
            <!--<item code="anyHL7Segment" desc="???"/>-->
            <!--<item code="anyZSegment" desc="???"/>-->
        </version>
    </xsl:variable>
    <!-- Source: fhir/trunk/build/source/v2/source.xml -->
    <xsl:variable name="datatypeList">
        <version version="2.8.2" desc="Data Types">
            <item code="AD" desc="Address" comments="Replaced by XAD as of v 2.3"/>
            <item code="AUI" desc="Authorization information" comments="Replaces the CM data type used in sections 6.5.6.14 IN1-14, as of v 2.5."/>
            <item code="CCD" desc="Charge code and date" comments="Replaces the CM data type used in section 4.5.2.1 BLG-1, as of v 2.5."/>
            <item code="CCP" desc="Channel calibration parameters" comments="Replaces the CM data type used in 7.14.1.5 OBX-5.3 where OBX-5Observation value (*) is data type CD as of v 2.5."/>
            <item code="CD" desc="Channel definition" comments="For waveform data only;."/>
            <item code="CE" desc="Coded element" comments="WITHDRAWN"/>
            <item code="CF" desc="Coded element with formatted values"/>
            <item code="CK" desc="Composite ID with check digit" comments="WITHDRAWN"/>
            <item code="CM" desc="Composite" comments="WITHDRAWN Replaced by numerous new unambiguous data types in v 2.5"/>
            <item code="CN" desc="Composite ID number and name" comments="WITHDRAWN. Replaced by XCN as of v 2.3"/>
            <item code="CNE" desc="Coded with no exceptions"/>
            <item code="CNN" desc="Composite ID number and name simplified" comments="Restores the original data type CN as was initially implementable in the CM used in sections 4.5.3.32 and 7.4.1.32-(OBR-32), 4.5.3.33 and 7.4.1.33 - (OBR-33) 4.5.3.34 and 7.4.1.34 - (OBR-34) 4.5.3.35 and 7.4.1.35 - (OBR-35). Components 7 and 8, however, h"/>
            <item code="CP" desc="Composite price" comments="."/>
            <item code="CQ" desc="Composite quantity with units" comments="CQ cannot be legally expressed when embedded within another data type. Its use is constrained to a segment field."/>
            <item code="CSU" desc="Channel sensitivity and units" comments="Replaces the CM data type used in 7.14.1.5 OBX-5.3 where OBX-5Observation value (*) is data type CD as of v 2.5."/>
            <item code="CWE" desc="Coded with exceptions"/>
            <item code="CX" desc="Extended composite ID with check digit"/>
            <item code="DDI" desc="Daily deductible information" comments="Replaces the CM data type used in section 6.5.7.30  IN2-30, as of v 2.5."/>
            <item code="DIN" desc="Date and institution name" comments="Replaces the CM data type used in sections 15.4.6.12 STF-12 and 15.4.6.14 STF-13, as of v 2.5."/>
            <item code="DLD" desc="Discharge to location and date" comments="Replaces the CM data type used in section 8.8.4.9 - OM2-9, as of v 2.5"/>
            <item code="DLN" desc="Driver&#39;s license number"/>
            <item code="DLT" desc="Delta"/>
            <item code="DR" desc="Date/time range"/>
            <item code="DT" desc="Date"/>
            <item code="DTM" desc="Date/time"/>
            <item code="DTN" desc="Day type and number" comments="Replaces the CM data type used in section 6.5.8.11  IN3-11, as of v 2.5."/>
            <item code="ED" desc="Encapsulated data" comments="Supports ASCII MIME-encoding of binary data."/>
            <item code="EI" desc="Entity identifier"/>
            <item code="EIP" desc="Entity identifier pair" comments="Replaces the CM data type used in sections 4.5.1.8 - ORC-8, 4.5.3.29 - OBR-29, 7.3.1.29 - OBR-29, as of v 2.5."/>
            <item code="ELD" desc="Error location and description" comments="WITHDRAWN"/>
            <item code="ERL" desc="Error location"/>
            <item code="FC" desc="Financial class"/>
            <item code="FN" desc="Family name" comments="Appears ONLY in the PPN, XCN, and XPN."/>
            <item code="FT" desc="Formatted text"/>
            <item code="GTS" desc="General timing specification"/>
            <item code="HD" desc="Hierarchic designator"/>
            <item code="ICD" desc="Insurance certification definition" comments="Replaces the CM data type used in section 6.5.8.20  IN3-20, as of v 2.5."/>
            <item code="ID" desc="Coded values for HL7 tables"/>
            <item code="IS" desc="Coded value for user-defined tables"/>
            <item code="JCC" desc="Job code/class"/>
            <item code="LA1" desc="Location with address variation 1" comments="Replaces the CM data type used in 4.14.1.8 RXO-8 and 4.14.4.8 RXE-8 as of v 2.5. Retained for backward compatibility only as of v 2.5"/>
            <item code="LA2" desc="Location with address variation 2" comments="Replaces the CM data type used in 4.14.5.13 RXD-13, 4.14.6.11 RXG-11 and 4.14.7.11 RXA-11 as of v 2.5. Retained for backward compatibility only as of v 2.5,"/>
            <item code="MA" desc="Multiplexed array" comments="For waveform data only"/>
            <item code="MO" desc="Money"/>
            <item code="MOC" desc="Money and charge code" comments="Replaces the CM data type used in sections 4.5.3.23 OBR-23 and 7.4.1.23- OBR-23 as of v 2.5."/>
            <item code="MOP" desc="Money or percentage" comments="Replaces the CM data type used in section 6.5.8.5  IN3-5, as of v 2.5. This data type is restricted to this field."/>
            <item code="MSG" desc="Message type" comments="Replaces the CM data type used in 2.16.9.9 MSH-9 as of v 2.5."/>
            <item code="NA" desc="Numeric array" comments="For waveform data only"/>
            <item code="NDL" desc="Name with date and location" comments="Replaces the CM data type used in sections 4.5.3.32 and 7.4.1.32-( OBR-32) , 4.5.3.33 and 7.4.1.33  - ( OBR-33) 4.5.3.34 and 7.4.1.34 - ( OBR-34) 4.5.3.35 and 7.4.1.35 - ( OBR-35) as of v 2.5."/>
            <item code="NM" desc="Numeric"/>
            <item code="NR" desc="Numeric range" comments="Replaces the CM data type used in sections 8.8.4.6.1- OM2-6.1, 8.8.4.6.3- OM2-6.3and 8.8.4.6.4- OM2-6.4, as of v 2.5."/>
            <item code="OCD" desc="Occurrence code and date" comments="Replaces the CM data type used in sections 6.5.10.10 UB1-16 and 6.5.11.7 UB2-7, as of v 2.5."/>
            <item code="OSD" desc="Order sequence definition" comments="WITHDRAWN"/>
            <item code="OSP" desc="Occurrence span code and date" comments="Replaces the CM data type used in section 6.5.11.8 UB2-8, as of v 2.5."/>
            <item code="PIP" desc="Practitioner institutional privileges" comments="Replaces the CM data type used in 15.4.5.7 PRA-7 as of v 2.5."/>
            <item code="PL" desc="Person location"/>
            <item code="PLN" desc="Practitioner license or other ID number" comments="Replaces the CM data type used in 15.4.5.6 PRA-6, 11.6.3.7 PRD-7 and 11.6.4.7 CTD-7 as of v 2.5."/>
            <item code="PN" desc="Person name" comments="WITHDRAWN"/>
            <item code="PPN" desc="Performing person time stamp" comments="equivalent of an XCN joined with a TS"/>
            <item code="PRL" desc="Parent result link" comments="Replaces the CM data type used in sections 4.5.3.26 - OBR-26 and 7.4.1.26 - OBR-26 as of v 2.5."/>
            <item code="PT" desc="Processing type"/>
            <item code="PTA" desc="Policy type and amount" comments="Replaces the CM data type used in section 6.5.7.29  IN2-29, as of v 2.5."/>
            <item code="QIP" desc="Query input parameter list"/>
            <item code="QSC" desc="Query selection criteria"/>
            <item code="RCD" desc="Row column definition"/>
            <item code="RFR" desc="Reference range" comments="Replaces the CM data type used in sections 8.8.4.6 - OM2-6, 8.8.4.7 - OM2-7 and8.8.4.8 - OM2-8 as of v 2.5."/>
            <item code="RI" desc="Repeat interval"/>
            <item code="RMC" desc="Room coverage" comments="Replaces the CM data type used in section 6.5.7.28  IN2-28, as of v 2.5."/>
            <item code="RP" desc="Reference pointer"/>
            <item code="RPT" desc="Repeat pattern"/>
            <item code="SAD" desc="Street Address" comments="Appears ONLY in the XAD data type."/>
            <item code="SCV" desc="Scheduling class value pair" comments="For scheduling data only. See Chapter 10"/>
            <item code="SI" desc="Sequence ID"/>
            <item code="SN" desc="Structured numeric"/>
            <item code="SNM" desc="String of telephone number digits" comments="Definition: a string whose characters are limited to &quot;+&quot; and the decimal digits 0 through 9.  As a string, leading zeros are always considered significant.  Maximum length: Not specified for the type. May be specified in the context of use."/>
            <item code="SPD" desc="Specialty description" comments="Replaces the CM data type used in 15.4.5.5 PRA-5 as of v 2.5."/>
            <item code="SPS" desc="Specimen source" comments="WITHDRAWN"/>
            <item code="SRT" desc="Sort order"/>
            <item code="ST" desc="String data"/>
            <item code="TM" desc="Time"/>
            <item code="TN" desc="Telephone number" comments="WITHDRAWN"/>
            <item code="TQ" desc="Timing/quantity" comments="WITHDRAWN"/>
            <item code="TS" desc="Time stamp" comments="WITHDRAWN"/>
            <item code="TX" desc="Text data"/>
            <item code="UVC" desc="UB value code and amount" comments="Replaces the CM data type used in sections 6.5.10.10 UB1-10 and 6.5.11.6 UB2-6, as of v 2.5."/>
            <item code="VH" desc="Visiting hours"/>
            <item code="VID" desc="Version identifier"/>
            <item code="VR" desc="Value range" comments="Replaces the CM data type used in 5.10.5.3.11 QRD-11 as of v 2.5."/>
            <item code="WVI" desc="Channel Identifier" comments="Replaces the CM data type used in 7.14.1.3.1 OBX-5.1 where OBX-5 Observation value (*) is data type CD as of v 2.5."/>
            <item code="WVS" desc="Waveform source" comments="Replaces the CM data type used in 7.14.1.4 OBX-5.2 where OBX-5 Observation value (*) is data type CD as of v 2.5."/>
            <item code="XAD" desc="Extended address" comments="Replaces AD as of v 2.3"/>
            <item code="XCN" desc="Extended composite ID number and name for persons" comments="Replaces CN as of v 2.3"/>
            <item code="XON" desc="Extended composite name and ID number for organizations"/>
            <item code="XPN" desc="Extended person name" comments="Replaces PN as of v 2.3."/>
            <item code="XTN" desc="Extended telecommunications number" comments="Replaces TN as of v 2.3"/>
            <!--<item code="varies" desc="???"/>-->
        </version>
    </xsl:variable>
    
    <xsl:template match="/">
        <xsl:result-document href="../../decor/core/DECOR-supported-datatypes-hl7v2.5xml.xml" method="xml" indent="yes">
            <xsl:comment>
    DECOR-supported-datatypes
    Copyright (C) 2013-<xsl:value-of select="substring(string(current-date()),1,4)"/> Dr. Kai U. Heitmann, Alexander Henket
    
    List of supported data types in ART-DECOR
    
    reflects data types as &lt;dataType&gt; elements with the name
    name must be present (as of now) in coreschematrons directory as DTr1_{name}.sch
    
    data type elements are hierarchical representing inheritence, e.g.
    a SC is_a ST is_a ED is_a BIN 
    
    Also reflects possibility of demotion
    
    &lt;flavor&gt; elements may be added as immediate child elements of a data type
    to reflect a data type flavor (variant, data type with further constraint), 
    additionally shall carry a realm indicator, e.g. DE, NL, AT etc. or UV
    
    The mapping to one of the value domain data types of concepts in data sets 
    is also shown.
    So far the possible mapping is shown for the following value domain data types
      blob boolean code complex date datetime decimal duration identifier ordinal
      quantity string text count&#10;</xsl:comment>
            <xsl:text>&#10;</xsl:text>
            <xsl:processing-instruction name="xml-model">href="DECOR-supported-datatypes.sch" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"</xsl:processing-instruction>
            <xsl:text>&#10;</xsl:text>
            <supportedDataTypes type="{$classification-format}">
                <xsl:for-each select="$datatype-definitions/xs:*[@name=$baseTypes] | $datatype-definitions/xs:*[@name=$datatype-definitions//@base]">
                    <dataType name="{@name}"/>
                </xsl:for-each>
            </supportedDataTypes>
        </xsl:result-document>
        
        <!--xsl:for-each select="$datatype-definitions/xs:*[@name=$baseTypes] | $datatype-definitions/xs:*[@name=$datatype-definitions//@base]">
            <xsl:result-document href="file:/Users/ahenket/Development/sourceforge-exist/trunk/decor/core/coreschematrons-hl7v2.5xml/DTv25_{@name}.sch" method="xml" indent="yes">
                <xsl:comment>
                    <xsl:text>&#10;    </xsl:text>
                    <xsl:text>HL7 V2.5 - Datatype </xsl:text>
                    <xsl:value-of select="@name"/>
                    <xsl:text>&#10;    </xsl:text>
                    <xsl:text>Status: draft</xsl:text>
                    <xsl:text>&#10;</xsl:text>
                </xsl:comment>
                <xsl:text>&#10;</xsl:text>
                <rule abstract="true" id="{@name}" xmlns="http://purl.oclc.org/dsdl/schematron"/>
            </xsl:result-document>
        </xsl:for-each-->
        
        <rules>
            <template id="{$template-default-id-root-messages}.1" name="BATCH" displayName="BATCH" effectiveDate="2013-02-10T00:00:00" statusCode="draft">
                <classification type="messagelevel" format="{$classification-format}"/>
                <context path="/"/>
                <element name="hl7v2:BATCH">
                    <element name="hl7v2:FHS" contains="FHS_segment" minimumMultiplicity="0" maximumMultiplicity="1"/>
                    <include ref="MESSAGEBATCH" minimumMultiplicity="0" maximumMultiplicity="*"/>
                    <element name="hl7v2:FTS" contains="FTS_segment" minimumMultiplicity="0" maximumMultiplicity="1"/>
                </element>
            </template>
            
            <template id="{$template-default-id-root-messages}.2" name="MESSAGEBATCH" displayName="MESSAGEBATCH" effectiveDate="2013-02-10T00:00:00" statusCode="draft">
                <classification type="messagelevel" format="{$classification-format}"/>
                <context path="//"/>
                <element name="hl7v2:MESSAGEBATCH">
                    <element name="hl7v2:BHS" contains="BHS_segment" minimumMultiplicity="0" maximumMultiplicity="1"/>
                    <element name="hl7v2:QRD" contains="QRD_segment" minimumMultiplicity="0" maximumMultiplicity="1"/>
                    <element name="hl7v2:QRF" contains="QRF_segment" minimumMultiplicity="0" maximumMultiplicity="1"/>
                    <element name="hl7v2:MESSAGES" minimumMultiplicity="0" maximumMultiplicity="*">
                        <desc language="en-US">Any message type</desc>
                    </element>
                    <element name="hl7v2:BTS" contains="BTS_segment" minimumMultiplicity="0" maximumMultiplicity="1"/>
                </element>
            </template>
            
            <xsl:for-each select="$message-definitions">
                <xsl:call-template name="process-xsd">
                    <xsl:with-param name="id-start" select="2"/>
                    <xsl:with-param name="id-root" select="$template-default-id-root-messages"/>
                    <xsl:with-param name="template-classification" as="element(classification)">
                        <classification type="messagelevel" format="{$classification-format}"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            
            <template id="{$template-default-id-root-segments}.0" name="QVR_Q17_QBP_group" effectiveDate="2013-02-10T00:00:00" statusCode="draft">
                <classification type="segmentlevel" format="{$classification-format}"/>
                <element name="hl7v2:QVR_Q17.QBP" contains="QVR_Q17_QBP_group" minimumMultiplicity="0" maximumMultiplicity="1"/>
            </template>
            
            <xsl:for-each select="$segment-definitions">
                <xsl:call-template name="process-xsd">
                    <xsl:with-param name="id-root" select="$template-default-id-root-segments"/>
                    <xsl:with-param name="template-classification" as="element()">
                        <classification type="segmentlevel" format="{$classification-format}"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            
            <template id="{$template-default-id-root-datatypes}.0" name="escapeType_datatype" effectiveDate="2013-02-10T00:00:00" statusCode="draft">
                <classification type="datatypelevel" format="{$classification-format}"/>
                <element name="hl7v2:escape" contains="escapeType_datatype" minimumMultiplicity="0">
                    <attribute name="V"/>
                </element>
            </template>
            
            <xsl:for-each select="$datatype-definitions/xs:complexType[@name=$baseTypes] | $datatype-definitions/xs:complexType[@name=$datatype-definitions//@base]">
                <xsl:call-template name="process-xsd">
                    <xsl:with-param name="id-root" select="$template-default-id-root-datatypes"/>
                    <xsl:with-param name="template-classification" as="element()">
                        <classification type="datatypelevel" format="{$classification-format}"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
        </rules>
    </xsl:template>
    
    <xsl:template name="process-xsd">
        <xsl:param name="id-start" select="0"></xsl:param>
        <xsl:param name="id-root"/>
        <xsl:param name="template-classification" as="element(classification)"/>
        <xsl:variable name="template-id" select="$id-start + position()"/>
        <xsl:variable name="template-name" as="xs:string">
            <xsl:choose>
                <xsl:when test="$template-classification/@type='messagelevel'">
                    <xsl:value-of select="tokenize(tokenize(document-uri(.),'/')[last()],'\.')[1]"/>
                </xsl:when>
                <xsl:when test="$template-classification/@type='segmentlevel'">
                    <xsl:value-of select="concat(@name,'_segment')"/>
                </xsl:when>
                <xsl:when test="$template-classification/@type='datatypelevel'">
                    <xsl:value-of select="concat(@name,'_datatype')"/>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="template-display-name" as="xs:string">
            <xsl:choose>
                <xsl:when test="$template-classification/@type = 'messagelevel'">
                    <xsl:variable name="code" select="if (contains($template-name,'_')) then substring-after($template-name, '_') else $template-name"/>
                    <xsl:value-of select="$messageList//*[@code = $code]/@desc"/>
                </xsl:when>
                <xsl:when test="$template-classification/@type = 'segmentlevel'">
                    <xsl:variable name="code" select="@name"/>
                    <xsl:value-of select="$segmentList//*[@code = $code]/@desc"/>
                </xsl:when>
                <xsl:when test="$template-classification/@type = 'datatypelevel'">
                    <xsl:variable name="code" select="@name"/>
                    <xsl:value-of select="$datatypeList//*[@code = $code]/@desc"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$template-name"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <!-- Get message struture if we're at messagelevel. 
            If we can a code matching the message name, then structure == message name. 
            Else check if event is in @desc, and return structure code -->
        <xsl:variable name="template-structure-name" as="xs:string?">
            <xsl:variable name="code" select="if (contains($template-name,'_')) then substring-after($template-name, '_') else $template-name"/>
            <xsl:choose>
                <xsl:when test="$template-classification/@type = 'messagelevel'">
                    <xsl:choose>
                        <xsl:when test="$messageStructureList//*[@code = $template-name]">
                            <xsl:value-of select="$template-name"/>
                        </xsl:when>
                        <xsl:when test="$messageStructureList//*[tokenize(@desc,',\s*') = $code]">
                            <xsl:value-of select="$messageStructureList//*[tokenize(@desc,',\s*') = $code]/@code"/>
                        </xsl:when>
                    </xsl:choose>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:if test="string-length($template-name) = 0">
            <xsl:message>Missing template name for <xsl:value-of select="$template-classification/@type"/>&#160;<xsl:value-of select="@name"/></xsl:message>
        </xsl:if>
        <xsl:if test="string-length($template-display-name) = 0">
            <xsl:message>Missing template display name for <xsl:value-of select="$template-classification/@type"/>&#160;<xsl:value-of select="$template-name"/> (using @name as @displayName)</xsl:message>
        </xsl:if>
        <xsl:if test="string-length($template-structure-name) = 0 and $template-classification/@type='messagelevel'">
            <xsl:message>Missing template structure (relationship/@model) for <xsl:value-of select="$template-classification/@type"/>&#160;<xsl:value-of select="$template-name"/></xsl:message>
        </xsl:if>
        
        <template id="{$id-root}.{$template-id}" name="{$template-name}" displayName="{if (string-length($template-display-name) > 0) then $template-display-name else $template-name}" effectiveDate="{$template-default-effectiveDate}" statusCode="{$template-default-status}">
            <xsl:choose>
                <xsl:when test="$template-classification/@type='messagelevel'">
                    <xsl:copy-of select="$template-classification"/>
                    <xsl:if test="string-length($template-structure-name) > 0">
                        <relationship type="DRIV" model="{$template-structure-name}"/>
                    </xsl:if>
                    <context path="/"/>
                    <xsl:apply-templates select="/xs:schema/xs:element[@name=$template-name]" mode="process-root">
                        <xsl:with-param name="template-classification" select="$template-classification"/>
                    </xsl:apply-templates>
                </xsl:when>
                <xsl:when test="$template-classification/@type='segmentlevel'">
                    <xsl:copy-of select="$template-classification"/>
                    <xsl:apply-templates select="." mode="process-root">
                        <xsl:with-param name="template-classification" select="$template-classification"/>
                    </xsl:apply-templates>
                </xsl:when>
                <xsl:when test="$template-classification/@type='datatypelevel'">
                    <xsl:copy-of select="$template-classification"/>
                    <xsl:apply-templates select="xs:sequence/xs:element" mode="process-content">
                        <xsl:with-param name="template-classification" select="$template-classification"/>
                    </xsl:apply-templates>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:message terminate="yes">XSL template process-xsd <xsl:copy-of select="$template-classification"/> not handled.</xsl:message>
                </xsl:otherwise>
            </xsl:choose>
        </template>
    </xsl:template>
    
    <!--<xsd:element name="ACK" type="ACK.CONTENT"/>-->
    <xsl:template match="xs:element" mode="process-root">
        <xsl:param name="template-classification" as="element(classification)"/>
        <xsl:variable name="name" select="@name"/>
        <xsl:variable name="type" select="@type"/>
        <!--<element name="hl7v2:{$name}">-->
            <xsl:apply-templates select="/xs:schema/xs:complexType[@name=$type]/xs:sequence/xs:element" mode="process-content">
                <xsl:with-param name="template-classification" select="$template-classification"/>
            </xsl:apply-templates>
        <!--</element>-->
    </xsl:template>
    
    <!--
        <xsd:complexType name="ACK.CONTENT">
            <xsd:sequence>
              <xsd:element ref="MSH" minOccurs="1" maxOccurs="1" />
              <xsd:element ref="SFT" minOccurs="0" maxOccurs="unbounded" />
              <xsd:element ref="MSA" minOccurs="1" maxOccurs="1" />
              <xsd:element ref="ERR" minOccurs="0" maxOccurs="unbounded" />
            </xsd:sequence>
        </xsd:complexType>
    -->
    <xsl:template match="xs:element" mode="process-content">
        <xsl:param name="template-classification" as="element(classification)"/>
        <xsl:variable name="ref" select="@ref|@name"/>
        <xsl:variable name="type" select="@type"/>
        <xsl:variable name="min" select="@minOccurs"/>
        <xsl:variable name="max" select="if (@maxOccurs castable as xs:integer) then @maxOccurs else if (@maxOccurs='unbounded') then '*' else ()"/>
        <xsl:choose>
            <!-- QVR_Q17.QBP is a recursive definition -->
            <xsl:when test="$template-classification/@type='messagelevel' and $ref='QVR_Q17.QBP'">
                <element name="hl7v2:{$ref}" contains="QVR_Q17_QBP_group">
                    <xsl:if test="@minOccurs">
                        <xsl:attribute name="minimumMultiplicity" select="$min"/>
                    </xsl:if>
                    <xsl:if test="@maxOccurs">
                        <xsl:attribute name="maximumMultiplicity" select="$max"/>
                    </xsl:if>
                </element>
            </xsl:when>
            <xsl:when test="$template-classification/@type='datatypelevel' and $ref='escape'">
                <xsl:for-each select="/xs:schema/xs:complexType[@name=$type]/xs:sequence/xs:element">
                    <element name="hl7v2:{@name}" contains="escapeType_datatype">
                        <xsl:if test="@minOccurs">
                            <xsl:attribute name="minimumMultiplicity" select="$min"/>
                        </xsl:if>
                        <xsl:if test="@maxOccurs">
                            <xsl:attribute name="maximumMultiplicity" select="$max"/>
                        </xsl:if>
                        <attribute name="V"/>
                    </element>
                </xsl:for-each>
            </xsl:when>
            <xsl:when test="$template-classification/@type='datatypelevel'">
                <element name="hl7v2:{$ref}">
                    <xsl:if test="@minOccurs">
                        <xsl:attribute name="minimumMultiplicity" select="$min"/>
                    </xsl:if>
                    <xsl:if test="@maxOccurs">
                        <xsl:attribute name="maximumMultiplicity" select="$max"/>
                    </xsl:if>
                    <xsl:apply-templates select="$datatype-definitions/*[@name=$ref]" mode="process-field"/>
                </element>
            </xsl:when>
            <!-- If definition is inside same schema process it here -->
            <xsl:when test="$ref and /xs:schema/xs:element[@name=$ref]">
                <xsl:variable name="reftype" select="/xs:schema/xs:element[@name=$ref]/@type"/>
                <element name="hl7v2:{$ref}">
                    <xsl:if test="@minOccurs">
                        <xsl:attribute name="minimumMultiplicity" select="$min"/>
                    </xsl:if>
                    <xsl:if test="@maxOccurs">
                        <xsl:attribute name="maximumMultiplicity" select="$max"/>
                    </xsl:if>
                    <xsl:apply-templates select="/xs:schema/xs:complexType[@name=$reftype]/xs:sequence/xs:element" mode="process-content">
                        <xsl:with-param name="template-classification" select="$template-classification"/>
                    </xsl:apply-templates>
                </element>
            </xsl:when>
            <xsl:when test="$template-classification/@type='messagelevel'">
                <element name="{$ref}" contains="{$ref}_segment">
                    <xsl:if test="@minOccurs">
                        <xsl:attribute name="minimumMultiplicity" select="$min"/>
                    </xsl:if>
                    <xsl:if test="@maxOccurs">
                        <xsl:attribute name="maximumMultiplicity" select="$max"/>
                    </xsl:if>
                    <xsl:if test="number(@minOccurs)>0">
                        <xsl:attribute name="conformance" select="'R'"/>
                    </xsl:if>
                </element>
            </xsl:when>
            <xsl:when test="$template-classification/@type='segmentlevel'">
                <element name="hl7v2:{$ref}">
                    <xsl:if test="@minOccurs">
                        <xsl:attribute name="minimumMultiplicity" select="$min"/>
                    </xsl:if>
                    <xsl:if test="@maxOccurs">
                        <xsl:attribute name="maximumMultiplicity" select="$max"/>
                    </xsl:if>
                    <xsl:apply-templates select="$field-definitions[@name=$ref]" mode="process-field"/>
                </element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:message terminate="yes">XSL template process-content <xsl:copy-of select="$template-classification"/> not handled.</xsl:message>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="xs:element" mode="process-field">
        <xsl:variable name="ref" select="@ref"/>
        <xsl:variable name="type" select="@type"/>
        <xsl:variable name="min" select="@minOccurs"/>
        <xsl:variable name="max" select="if (@maxOccurs castable as xs:integer) then @maxOccurs else if (@maxOccurs='unbounded') then '*' else ()"/>
        <xsl:variable name="datatype" select="/xs:schema/xs:complexType[@name=$type]//xs:extension/@base"/>
        
        <xsl:attribute name="datatype" select="$datatype"/>
        <xsl:choose>
            <xsl:when test="$datatype-definitions/xs:complexType[@name=$datatype]">
                <xsl:attribute name="contains" select="concat($datatype,'_datatype')"/>
            </xsl:when>
        </xsl:choose>
        
        <xsl:for-each select="/xs:schema/xs:complexType[@name=$type]/xs:annotation/xs:documentation">
            <desc language="{local:handleLanguage(.)}">
                <xsl:copy-of select="node()"/>
            </desc>
        </xsl:for-each>
        <xsl:variable name="attributeGroupRef" select="/xs:schema/xs:complexType[@name=$type]//xs:attributeGroup/@ref"/>
        <xsl:for-each select="/xs:schema/xs:attributeGroup[@name=$attributeGroupRef]//xs:attribute">
            <attribute name="{@name}" value="{@fixed}" isOptional="{not(@required='true')}"/>
            <xsl:if test="not(@name and @fixed)">
                <xsl:message terminate="yes">XSL template process-field attribute missing @name or @fixed.</xsl:message>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:function name="local:handleLanguage" as="xs:string">
        <xsl:param name="element" as="element()?"/>
        <xsl:choose>
            <xsl:when test="$element/@xml:lang='en'">
                <xsl:value-of select="'en-US'"/>
            </xsl:when>
            <xsl:when test="lower-case($element/@xml:lang)='en-us'">
                <xsl:value-of select="'en-US'"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:message terminate="yes">XSL function local:handleLanguage <xsl:value-of select="$element/@xml:lang"/> not handled.</xsl:message>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
</xsl:stylesheet>