<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:fd="http://hl7.org/fhir/dev"
    xmlns:loc="urn"
    exclude-result-prefixes="xs xd fd loc"
    version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Jul 18, 2015</xd:p>
            <xd:p><xd:b>Author:</xd:b> ahenket</xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    
    <xsl:output indent="yes" omit-xml-declaration="yes"/>
    
    <xsl:param name="doref" select="'true'"/>
    <xsl:param name="skipEmptyTables" select="'false'"/>
    <xsl:param name="baseIdCS" select="'2.16.840.1.113883.3.1937.777.10.5'"/>
    <xsl:param name="baseIdVS" select="'2.16.840.1.113883.3.1937.777.10.11'"/>
    <xsl:param name="versionLabel" select="'2.5'"/>
    <xsl:param name="effectiveDate" select="'2013-02-10T00:00:00'"/>
    <xsl:param name="statusCode" select="'final'"/>
    
    <xsl:variable name="tableTypes" as="element()+">
        <table type="User" code="1" displayName="Administrative Sex"/>
        <table type="User" code="2" displayName="Marital Status"/>
        <table type="HL7" code="3" displayName="Event type"/>
        <table type="User" code="4" displayName="Patient Class"/>
        <table type="User" code="5" displayName="Race"/>
        <table type="User" code="6" displayName="Religion"/>
        <table type="User" code="7" displayName="Admission Type"/>
        <table type="HL7" code="8" displayName="Acknowledgment code"/>
        <table type="User" code="9" displayName="Ambulatory Status"/>
        <table type="User" code="10" displayName="Physician ID"/>
        <table type="User" code="17" displayName="Transaction Type"/>
        <table type="User" code="18" displayName="Patient Type"/>
        <table type="User" code="19" displayName="Anesthesia Code"/>
        <table type="User" code="21" displayName="Bad Debt Agency Code"/>
        <table type="User" code="22" displayName="Billing Status"/>
        <table type="User" code="23" displayName="Admit Source"/>
        <table type="User" code="24" displayName="Fee Schedule"/>
        <table type="HL7" code="27" displayName="Priority"/>
        <table type="User" code="32" displayName="Charge/Price Indicator"/>
        <table type="HL7" code="38" displayName="Order status"/>
        <table type="User" code="42" displayName="Company Plan Code"/>
        <table type="User" code="43" displayName="Condition Code"/>
        <table type="User" code="44" displayName="Contract Code"/>
        <table type="User" code="45" displayName="Courtesy Code"/>
        <table type="User" code="46" displayName="Credit Rating"/>
        <table type="HL7" code="48" displayName="What subject filter"/>
        <table type="User" code="49" displayName="Department Code"/>
        <table type="User" code="50" displayName="Accident Code"/>
        <table type="User" code="51" displayName="Diagnosis Code"/>
        <table type="User" code="52" displayName="Diagnosis Type"/>
        <table type="User" code="53" displayName="Diagnosis Coding Method"/>
        <table type="User" code="55" displayName="Diagnosis related group"/>
        <table type="User" code="56" displayName="DRG grouper review code"/>
        <table type="User" code="59" displayName="Consent Code"/>
        <table type="HL7" code="61" displayName="Check digit scheme"/>
        <table type="User" code="62" displayName="Event reason"/>
        <table type="User" code="63" displayName="Relationship"/>
        <table type="User" code="64" displayName="Financial class"/>
        <table type="HL7" code="65" displayName="Specimen Action Code"/>
        <table type="User" code="66" displayName="Employment Status"/>
        <table type="User" code="68" displayName="Guarantor Type"/>
        <table type="User" code="69" displayName="Hospital Service"/>
        <table type="HL7" code="70" displayName="Specimen Source Codes"/>
        <table type="User" code="72" displayName="Insurance Plan ID"/>
        <table type="User" code="73" displayName="Interest Rate Code"/>
        <table type="HL7" code="74" displayName="Diagnostic Service Section ID"/>
        <table type="HL7" code="76" displayName="Message type"/>
        <table type="User" code="78" displayName="Abnormal flags"/>
        <table type="HL7" code="80" displayName="Nature of Abnormal Testing"/>
        <table type="User" code="83" displayName="Outlier Type"/>
        <table type="User" code="84" displayName="Performed by"/>
        <table type="HL7" code="85" displayName="Observation result status codes interpretation"/>
        <table type="User" code="86" displayName="Plan ID"/>
        <table type="User" code="87" displayName="Pre-Admit Test Indicator"/>
        <table type="User" code="88" displayName="Procedure Code"/>
        <table type="User" code="89" displayName="Procedure Coding Method"/>
        <table type="HL7" code="91" displayName="Query priority"/>
        <table type="User" code="92" displayName="Re-Admission Indicator"/>
        <table type="User" code="93" displayName="Release Information"/>
        <table type="User" code="98" displayName="Type of Agreement"/>
        <table type="User" code="99" displayName="VIP Indicator"/>
        <table type="HL7" code="100" displayName="Invocation event"/>
        <table type="HL7" code="103" displayName="Processing ID"/>
        <table type="HL7" code="104" displayName="Version ID"/>
        <table type="HL7" code="105" displayName="Source of comment"/>
        <table type="HL7" code="106" displayName="Query/response format code"/>
        <table type="HL7" code="107" displayName="Deferred response type"/>
        <table type="HL7" code="108" displayName="Query results level"/>
        <table type="HL7" code="109" displayName="Report priority"/>
        <table type="User" code="110" displayName="Transfer to Bad Debt Code"/>
        <table type="User" code="111" displayName="Delete Account Code"/>
        <table type="User" code="112" displayName="Discharge Disposition"/>
        <table type="User" code="113" displayName="Discharged to location"/>
        <table type="User" code="114" displayName="Diet Type"/>
        <table type="User" code="115" displayName="Servicing Facility"/>
        <table type="User" code="116" displayName="Bed Status"/>
        <table type="User" code="117" displayName="Account Status"/>
        <table type="User" code="118" displayName="Major Diagnostic Category"/>
        <table type="HL7" code="119" displayName="Order control codes"/>
        <table type="HL7" code="121" displayName="Response flag"/>
        <table type="HL7" code="122" displayName="Charge type"/>
        <table type="HL7" code="123" displayName="Result Status"/>
        <table type="HL7" code="124" displayName="Transportation Mode"/>
        <table type="HL7" code="125" displayName="Value type"/>
        <table type="HL7" code="126" displayName="Quantity limited request"/>
        <table type="User" code="127" displayName="Allergen Type"/>
        <table type="User" code="128" displayName="Allergy Severity"/>
        <table type="User" code="129" displayName="Accommodation code"/>
        <table type="User" code="130" displayName="Visit User Code"/>
        <table type="User" code="131" displayName="Contact Role"/>
        <table type="User" code="132" displayName="Transaction Code"/>
        <table type="User" code="133" displayName="Procedure Practitioner Identifier Code Type"/>
        <table type="User" code="135" displayName="Assignment of Benefits"/>
        <table type="HL7" code="136" displayName="Yes/no indicator"/>
        <table type="User" code="137" displayName="Mail Claim Party"/>
        <table type="User" code="139" displayName="Employer Information Data"/>
        <table type="User" code="140" displayName="Military Service"/>
        <table type="User" code="141" displayName="Military Rank/Grade"/>
        <table type="User" code="142" displayName="Military Status"/>
        <table type="User" code="143" displayName="Non-covered Insurance Code"/>
        <table type="User" code="144" displayName="Eligibility Source"/>
        <table type="User" code="145" displayName="Room type"/>
        <table type="User" code="146" displayName="Amount type"/>
        <table type="User" code="147" displayName="Policy type"/>
        <table type="HL7" code="148" displayName="Money or percentage indicator"/>
        <table type="User" code="149" displayName="Day type"/>
        <table type="User" code="150" displayName="Certification patient type"/>
        <table type="User" code="151" displayName="Second Opinion Status"/>
        <table type="User" code="152" displayName="Second Opinion Documentation Received"/>
        <table type="HL7" code="153" displayName="Value code"/>
        <table type="HL7" code="155" displayName="Accept/application acknowledgment conditions"/>
        <table type="HL7" code="156" displayName="Which date/time qualifier"/>
        <table type="HL7" code="157" displayName="Which date/time status qualifier"/>
        <table type="HL7" code="158" displayName="Date/time selection qualifier"/>
        <table type="HL7" code="159" displayName="Diet Code Specification Type"/>
        <table type="HL7" code="160" displayName="Tray Type"/>
        <table type="HL7" code="161" displayName="Allow Substitution"/>
        <table type="HL7" code="162" displayName="Route of Administration"/>
        <table type="HL7" code="163" displayName="Body site"/>
        <table type="HL7" code="164" displayName="Administration Device"/>
        <table type="HL7" code="165" displayName="Administration Method"/>
        <table type="HL7" code="166" displayName="RX Component Type"/>
        <table type="HL7" code="167" displayName="Substitution Status"/>
        <table type="HL7" code="168" displayName="Processing priority"/>
        <table type="HL7" code="169" displayName="Reporting priority"/>
        <table type="HL7" code="170" displayName="Derived specimen"/>
        <table type="User" code="171" displayName="Citizenship"/>
        <table type="User" code="172" displayName="Veterans Military Status"/>
        <table type="User" code="173" displayName="Coordination of Benefits"/>
        <table type="User" code="174" displayName="Nature of Service/Test/Observation"/>
        <table type="HL7" code="175" displayName="Master file identifier code"/>
        <table type="User" code="177" displayName="Confidentiality code"/>
        <table type="HL7" code="178" displayName="File level event code"/>
        <table type="HL7" code="179" displayName="Response level"/>
        <table type="HL7" code="180" displayName="Record-level event code"/>
        <table type="User" code="181" displayName="MFN record-level error return"/>
        <table type="User" code="182" displayName="Staff type"/>
        <table type="HL7" code="183" displayName="Active/Inactive"/>
        <table type="User" code="184" displayName="Department"/>
        <table type="HL7" code="185" displayName="Preferred method of contact"/>
        <table type="User" code="186" displayName="Practitioner category"/>
        <table type="HL7" code="187" displayName="Provider billing"/>
        <table type="User" code="188" displayName="Operator ID"/>
        <table type="User" code="189" displayName="Ethnic Group"/>
        <table type="HL7" code="190" displayName="Address type"/>
        <table type="HL7" code="191" displayName="Type of referenced data"/>
        <table type="User" code="193" displayName="Amount class"/>
        <table type="HL7" code="200" displayName="Name type"/>
        <table type="HL7" code="201" displayName="Telecommunication use code"/>
        <table type="HL7" code="202" displayName="Telecommunication equipment type"/>
        <table type="HL7" code="203" displayName="Identifier type"/>
        <table type="User" code="204" displayName="Organizational name type"/>
        <table type="HL7" code="205" displayName="Price type"/>
        <table type="HL7" code="206" displayName="Segment action code"/>
        <table type="HL7" code="207" displayName="Processing mode"/>
        <table type="HL7" code="208" displayName="Query Response Status"/>
        <table type="HL7" code="209" displayName="Relational operator"/>
        <table type="HL7" code="210" displayName="Relational conjunction"/>
        <table type="HL7" code="211" displayName="Alternate character sets"/>
        <table type="User" code="212" displayName="Nationality"/>
        <table type="User" code="213" displayName="Purge Status Code"/>
        <table type="User" code="214" displayName="Special Program Code"/>
        <table type="User" code="215" displayName="Publicity Code"/>
        <table type="User" code="216" displayName="Patient Status Code"/>
        <table type="User" code="217" displayName="Visit Priority Code"/>
        <table type="User" code="218" displayName="Patient Charge Adjustment"/>
        <table type="User" code="219" displayName="Recurring Service Code"/>
        <table type="User" code="220" displayName="Living Arrangement"/>
        <table type="User" code="222" displayName="Contact Reason"/>
        <table type="User" code="223" displayName="Living Dependency"/>
        <table type="HL7" code="224" displayName="Transport Arranged"/>
        <table type="HL7" code="225" displayName="Escort Required"/>
        <table type="HL7" code="227" displayName="Manufacturers of Vaccines (code=MVX)"/>
        <table type="User" code="228" displayName="Diagnosis Classification"/>
        <table type="User" code="229" displayName="DRG Payor"/>
        <table type="User" code="230" displayName="Procedure Functional Type"/>
        <table type="User" code="231" displayName="Student Status"/>
        <table type="User" code="232" displayName="Insurance Company Contact Reason"/>
        <table type="User" code="233" displayName="Non-Concur Code/Description"/>
        <table type="HL7" code="234" displayName="Report timing"/>
        <table type="HL7" code="235" displayName="Report source"/>
        <table type="HL7" code="236" displayName="Event Reported To"/>
        <table type="HL7" code="237" displayName="Event Qualification"/>
        <table type="HL7" code="238" displayName="Event Seriousness"/>
        <table type="HL7" code="239" displayName="Event Expected"/>
        <table type="HL7" code="240" displayName="Event Consequence"/>
        <table type="HL7" code="241" displayName="Patient Outcome"/>
        <table type="HL7" code="242" displayName="Primary Observer's Qualification"/>
        <table type="HL7" code="243" displayName="Identity May Be Divulged"/>
        <table type="User" code="244" displayName="Single Use Device"/>
        <table type="User" code="245" displayName="Product Problem"/>
        <table type="User" code="246" displayName="Product Available for Inspection"/>
        <table type="HL7" code="247" displayName="Status of Evaluation"/>
        <table type="HL7" code="248" displayName="Product source"/>
        <table type="User" code="249" displayName="Generic Product"/>
        <table type="HL7" code="250" displayName="Relatedness Assessment"/>
        <table type="HL7" code="251" displayName="Action Taken in Response to the Event"/>
        <table type="HL7" code="252" displayName="Causality Observations"/>
        <table type="HL7" code="253" displayName="Indirect exposure mechanism"/>
        <table type="HL7" code="254" displayName="Kind of quantity"/>
        <table type="User" code="255" displayName="Duration categories"/>
        <table type="HL7" code="256" displayName="Time delay post challenge"/>
        <table type="HL7" code="257" displayName="Nature of challenge"/>
        <table type="HL7" code="258" displayName="Relationship modifier"/>
        <table type="User" code="259" displayName="Modality"/>
        <table type="User" code="260" displayName="Patient location type"/>
        <table type="User" code="261" displayName="Location Equipment"/>
        <table type="User" code="262" displayName="Privacy Level"/>
        <table type="User" code="263" displayName="Level of Care"/>
        <table type="User" code="264" displayName="Location Department"/>
        <table type="User" code="265" displayName="Specialty Type"/>
        <table type="HL7" code="267" displayName="Days of the week"/>
        <table type="User" code="268" displayName="Override"/>
        <table type="User" code="269" displayName="Charge On Indicator"/>
        <table type="User" code="270" displayName="Document Type"/>
        <table type="HL7" code="271" displayName="Document completion status"/>
        <table type="HL7" code="272" displayName="Document Confidentiality Status"/>
        <table type="HL7" code="273" displayName="Document Availability Status"/>
        <table type="HL7" code="275" displayName="Document Storage Status"/>
        <table type="User" code="276" displayName="Appointment reason codes"/>
        <table type="User" code="277" displayName="Appointment Type Codes"/>
        <table type="User" code="278" displayName="Filler status codes"/>
        <table type="User" code="279" displayName="Allow Substitution Codes"/>
        <table type="User" code="280" displayName="Referral priority"/>
        <table type="User" code="281" displayName="Referral type"/>
        <table type="User" code="282" displayName="Referral disposition"/>
        <table type="User" code="283" displayName="Referral status"/>
        <table type="User" code="284" displayName="Referral category"/>
        <table type="User" code="285" displayName="Insurance company ID codes"/>
        <table type="User" code="286" displayName="Provider role"/>
        <table type="HL7" code="287" displayName="Problem/goal action code"/>
        <table type="User" code="288" displayName="Census tract"/>
        <table type="User" code="289" displayName="County/parish"/>
        <table type="HL7" code="291" displayName="Subtype of referenced data"/>
        <table type="HL7" code="292" displayName="Vaccines administered (code = CVX)(parenteral, unless oral is noted)"/>
        <table type="User" code="293" displayName="Billing Category"/>
        <table type="User" code="294" displayName="Time selection criteria parameter class codes"/>
        <table type="User" code="295" displayName="Handicap"/>
        <table type="User" code="296" displayName="Primary Language"/>
        <table type="User" code="297" displayName="CN ID source"/>
        <table type="HL7" code="298" displayName="CP range type"/>
        <table type="HL7" code="299" displayName="Encoding"/>
        <table type="User" code="300" displayName="Namespace ID"/>
        <table type="HL7" code="301" displayName="Universal ID type"/>
        <table type="User" code="302" displayName="Point of care"/>
        <table type="User" code="303" displayName="Room"/>
        <table type="User" code="304" displayName="Bed"/>
        <table type="User" code="305" displayName="Person location type"/>
        <table type="User" code="306" displayName="Location status"/>
        <table type="User" code="307" displayName="Building"/>
        <table type="User" code="308" displayName="Floor"/>
        <table type="User" code="309" displayName="Coverage Type"/>
        <table type="User" code="311" displayName="Job Status"/>
        <table type="User" code="312" displayName="Policy Scope"/>
        <table type="User" code="313" displayName="Policy Source"/>
        <table type="User" code="315" displayName="Living Will Code"/>
        <table type="User" code="316" displayName="Organ Donor Code"/>
        <table type="User" code="317" displayName="Annotations"/>
        <table type="User" code="319" displayName="Department Cost Center"/>
        <table type="User" code="320" displayName="Item Natural Account Code"/>
        <table type="HL7" code="321" displayName="Dispense Method"/>
        <table type="HL7" code="322" displayName="Completion Status for valid values."/>
        <table type="HL7" code="323" displayName="Action Code"/>
        <table type="User" code="324" displayName="Location characteristic ID"/>
        <table type="User" code="325" displayName="Location Relationship ID"/>
        <table type="User" code="326" displayName="Visit Indicator"/>
        <table type="User" code="327" displayName="Job code"/>
        <table type="User" code="328" displayName="Employee classification"/>
        <table type="HL7" code="329" displayName="Quantity method"/>
        <table type="HL7" code="330" displayName="Marketing basis"/>
        <table type="HL7" code="331" displayName="Facility type"/>
        <table type="HL7" code="332" displayName="Source type"/>
        <table type="User" code="333" displayName="Driver_s license issuing authority"/>
        <table type="User" code="334" displayName="Disabled Person Code"/>
        <table type="User" code="335" displayName="Repeat pattern"/>
        <table type="User" code="336" displayName="Referral reason"/>
        <table type="HL7" code="337" displayName="Certification status"/>
        <table type="User" code="338" displayName="Practitioner ID number type"/>
        <table type="User" code="339" displayName="Advanced Beneficiary Notice Code"/>
        <table type="User" code="340" displayName="Procedure Code Modifier"/>
        <table type="User" code="341" displayName="Guarantor Credit Rating Code"/>
        <table type="User" code="342" displayName="Military Recipient"/>
        <table type="User" code="343" displayName="Military Handicapped Program Code"/>
        <table type="User" code="344" displayName="Patient_s Relationship to Insured"/>
        <table type="User" code="345" displayName="Appeal Reason"/>
        <table type="User" code="346" displayName="Certification Agency"/>
        <table type="User" code="347" displayName="State/province"/>
        <table type="User" code="348" displayName="Special Program Indicator"/>
        <table type="User" code="349" displayName="PSRO/UR Approval Indicator"/>
        <table type="HL7" code="350" displayName="Occurrence code"/>
        <table type="HL7" code="351" displayName="Occurrence span"/>
        <table type="HL7" code="353" displayName="CWE statuses"/>
        <table type="HL7" code="354" displayName="Message structure"/>
        <table type="HL7" code="355" displayName="Primary key value type"/>
        <table type="HL7" code="356" displayName="Alternate character set handling scheme"/>
        <table type="HL7" code="357" displayName="Message error condition codes"/>
        <table type="User" code="358" displayName="Practitioner group"/>
        <table type="HL7" code="359" displayName="Diagnosis Priority"/>
        <table type="User" code="360" displayName="Degree/license/certificate"/>
        <table type="User" code="361" displayName="Application"/>
        <table type="User" code="362" displayName="Facility"/>
        <table type="User" code="363" displayName="Assigning authority"/>
        <table type="User" code="364" displayName="Comment type"/>
        <table type="HL7" code="365" displayName="Equipment state"/>
        <table type="HL7" code="366" displayName="Local/remote control state"/>
        <table type="HL7" code="367" displayName="Alert level"/>
        <table type="User" code="368" displayName="Remote control command"/>
        <table type="User" code="369" displayName="Specimen Role"/>
        <table type="HL7" code="370" displayName="Container status"/>
        <table type="HL7" code="371" displayName="Additive/Preservative"/>
        <table type="User" code="372" displayName="Specimen component"/>
        <table type="HL7" code="373" displayName="Treatment"/>
        <table type="User" code="374" displayName="System induced contaminants"/>
        <table type="User" code="375" displayName="Artificial blood"/>
        <table type="HL7" code="376" displayName="Special Handling Code"/>
        <table type="User" code="377" displayName="Other environmental factors"/>
        <table type="User" code="378" displayName="Carrier type"/>
        <table type="User" code="379" displayName="Tray type"/>
        <table type="User" code="380" displayName="Separator type"/>
        <table type="User" code="381" displayName="Cap type"/>
        <table type="User" code="382" displayName="Drug interference"/>
        <table type="HL7" code="383" displayName="Substance status"/>
        <table type="HL7" code="384" displayName="Substance type"/>
        <table type="User" code="385" displayName="Manufacturer identifier"/>
        <table type="User" code="386" displayName="Supplier identifier"/>
        <table type="User" code="387" displayName="Command response"/>
        <table type="HL7" code="388" displayName="Processing type"/>
        <table type="HL7" code="389" displayName="Analyte repeat status"/>
        <table type="HL7" code="391" displayName="Segment group"/>
        <table type="User" code="392" displayName="Match reason"/>
        <table type="User" code="393" displayName="Match algorithms"/>
        <table type="HL7" code="394" displayName="Response modality"/>
        <table type="HL7" code="395" displayName="Modify indicator"/>
        <table type="HL7" code="396" displayName="Coding system"/>
        <table type="HL7" code="397" displayName="Sequencing"/>
        <table type="HL7" code="398" displayName="Continuation style code"/>
        <table type="HL7" code="399" displayName="Country code"/>
        <table type="User" code="401" displayName="Government reimbursement program"/>
        <table type="User" code="402" displayName="School type"/>
        <table type="HL7" code="403" displayName="Language Ability"/>
        <table type="HL7" code="404" displayName="Language Proficiency"/>
        <table type="User" code="405" displayName="Organization Unit"/>
        <table type="User" code="406" displayName="Organization unit type"/>
        <table type="User" code="409" displayName="Application change type"/>
        <table type="User" code="411" displayName="Supplemental Service Information Values"/>
        <table type="User" code="412" displayName="Category Identifier"/>
        <table type="User" code="413" displayName="Consent Identifier"/>
        <table type="User" code="414" displayName="Units of Time"/>
        <table type="User" code="415" displayName="DRG Transfer Type"/>
        <table type="User" code="416" displayName="Procedure DRG Type"/>
        <table type="User" code="417" displayName="Tissue Type Code"/>
        <table type="HL7" code="418" displayName="Procedure Priority"/>
        <table type="User" code="421" displayName="Severity of Illness Code"/>
        <table type="User" code="422" displayName="Triage Code"/>
        <table type="User" code="423" displayName="Case Category Code"/>
        <table type="User" code="424" displayName="Gestation Category Code"/>
        <table type="User" code="425" displayName="Newborn Code"/>
        <table type="User" code="426" displayName="Blood Product Code"/>
        <table type="User" code="427" displayName="Risk Management Incident Code"/>
        <table type="User" code="428" displayName="Incident Type Code"/>
        <table type="User" code="429" displayName="Production Class Code"/>
        <table type="User" code="430" displayName="Mode of Arrival Code"/>
        <table type="User" code="431" displayName="Recreational Drug Use Code"/>
        <table type="User" code="432" displayName="Admission Level of Care Code"/>
        <table type="User" code="433" displayName="Precaution Code"/>
        <table type="User" code="434" displayName="Patient Condition Code"/>
        <table type="User" code="435" displayName="Advance Directive Code"/>
        <table type="User" code="436" displayName="Sensitivity to Causative Agent Code"/>
        <table type="User" code="437" displayName="Alert Device Code"/>
        <table type="User" code="438" displayName="Allergy Clinical Status"/>
        <table type="HL7" code="440" displayName="Data types"/>
        <table type="User" code="441" displayName="Immunization Registry Status"/>
        <table type="User" code="442" displayName="Location Service Code"/>
        <table type="User" code="443" displayName="Provider role"/>
        <table type="HL7" code="444" displayName="Name assembly order"/>
        <table type="User" code="445" displayName="Identity Reliability Code"/>
        <table type="User" code="446" displayName="Species Code"/>
        <table type="User" code="447" displayName="Breed Code"/>
        <table type="User" code="448" displayName="Name context"/>
        <table type="HL7" code="450" displayName="Event type"/>
        <table type="User" code="451" displayName="Substance identifier"/>
        <table type="HL7" code="452" displayName="Health care provider type code"/>
        <table type="HL7" code="453" displayName="Health care provider classification"/>
        <table type="HL7" code="454" displayName="Health care provider area of specialization"/>
        <table type="User" code="455" displayName="Type of Bill Code"/>
        <table type="User" code="456" displayName="Revenue code"/>
        <table type="User" code="457" displayName="Overall Claim Disposition Code"/>
        <table type="User" code="458" displayName="OCE Edit Code"/>
        <table type="User" code="459" displayName="Reimbursement Action Code"/>
        <table type="HL7" code="460" displayName="Denial or Rejection Code"/>
        <table type="User" code="461" displayName="License Number"/>
        <table type="User" code="462" displayName="Location cost center"/>
        <table type="User" code="463" displayName="Inventory Number"/>
        <table type="User" code="464" displayName="Facility ID"/>
        <table type="HL7" code="465" displayName="Name/address representation"/>
        <table type="HL7" code="466" displayName="Ambulatory Payment Classification Code"/>
        <table type="HL7" code="467" displayName="Modifier Edit Code"/>
        <table type="HL7" code="468" displayName="Payment Adjustment Code"/>
        <table type="User" code="469" displayName="Packaging Status Code"/>
        <table type="HL7" code="470" displayName="Reimbursement Type Code"/>
        <table type="User" code="471" displayName="Query name"/>
        <table type="HL7" code="472" displayName="TQ conjunction ID"/>
        <table type="User" code="473" displayName="Formulary Status"/>
        <table type="User" code="474" displayName="Organization Unit Type"/>
        <table type="User" code="475" displayName="Charge Type Reason"/>
        <table type="User" code="476" displayName="Medically Necessary Duplicate Procedure Reason for suggested values. User-define"/>
        <table type="User" code="477" displayName="Controlled Substance Schedule*"/>
        <table type="HL7" code="478" displayName="Formulary Status"/>
        <table type="User" code="479" displayName="Pharmaceutical Substances"/>
        <table type="HL7" code="480" displayName="Pharmacy Order Types"/>
        <table type="HL7" code="482" displayName="Order Type"/>
        <table type="HL7" code="483" displayName="Authorization Mode"/>
        <table type="User" code="484" displayName="Dispense Type"/>
        <table type="User" code="485" displayName="Extended Priority Codes"/>
        <table type="HL7" code="487" displayName="Specimen Type"/>
        <table type="HL7" code="488" displayName="Specimen Collection Method"/>
        <table type="HL7" code="489" displayName="Risk Codes"/>
        <table type="HL7" code="490" displayName="Specimen Reject Reason"/>
        <table type="HL7" code="491" displayName="Specimen Quality"/>
        <table type="HL7" code="492" displayName="Specimen Appropriateness"/>
        <table type="User" code="493" displayName="Specimen Condition"/>
        <table type="HL7" code="494" displayName="Specimen Child Role"/>
        <table type="HL7" code="495" displayName="Body Site Modifier"/>
        <table type="User" code="496" displayName="Consent Type"/>
        <table type="HL7" code="497" displayName="Consent Mode"/>
        <table type="HL7" code="498" displayName="Consent Status"/>
        <table type="User" code="499" displayName="Consent Bypass Reason"/>
        <table type="HL7" code="500" displayName="Consent Disclosure Level"/>
        <table type="User" code="501" displayName="Consent Non-Disclosure Reason"/>
        <table type="User" code="502" displayName="Non-Subject Consenter Reason"/>
        <table type="HL7" code="503" displayName="Sequence/Results Flag"/>
        <table type="HL7" code="504" displayName="Sequence Condition Code"/>
        <table type="HL7" code="505" displayName="Cyclic Entry/Exit Indicator"/>
        <table type="HL7" code="506" displayName="Service Request Relationship"/>
        <table type="User" code="507" displayName="Observation Result Handling"/>
        <table type="User" code="508" displayName="Blood Product Processing Requirements"/>
        <table type="User" code="509" displayName="Indication For Use"/>
        <table type="HL7" code="510" displayName="Blood Product Dispense Status"/>
        <table type="HL7" code="511" displayName="BP Observation Status Codes Interpretation"/>
        <table type="User" code="512" displayName="Commercial Product"/>
        <table type="HL7" code="513" displayName="Blood Product Transfusion/Disposition Status"/>
        <table type="User" code="514" displayName="Transfusion Adverse Reaction"/>
        <table type="User" code="515" displayName="Transfusion Interrupted Reason"/>
        <table type="HL7" code="516" displayName="Error severity"/>
        <table type="User" code="517" displayName="Inform person code"/>
        <table type="User" code="518" displayName="Override type"/>
        <table type="User" code="519" displayName="Override reason"/>
        <table type="User" code="521" displayName="Override code"/>
        <table type="HL7" code="523" displayName="Computation type"/>
        <table type="HL7" code="524" displayName="Sequence condition"/>
        <table type="HL7" code="525" displayName="Privilege"/>
        <table type="HL7" code="526" displayName="Privilege class"/>
        <table type="HL7" code="527" displayName="Calendar alignment"/>
        <table type="HL7" code="528" displayName="Event related period"/>
        <table type="HL7" code="529" displayName="Precision"/>
        <table type="User" code="530" displayName="Organization, agency, department "/>
        <table type="HL7" code="531" displayName="Institution"/>
        <table type="HL7" code="532" displayName="Expanded yes/no indicator"/>
        <table type="User" code="533" displayName="Application error code"/>
        <table type="User" code="534" displayName="Notify Clergy Code"/>
        <table type="User" code="535" displayName="Signature Code"/>
        <table type="User" code="536" displayName="Certificate Status"/>
        <table type="User" code="537" displayName="Institution"/>
        <table type="User" code="538" displayName="Institution Relationship Type"/>
        <table type="User" code="539" displayName="Cost Center Code"/>
        <table type="User" code="540" displayName="Inactive Reason Code"/>
        <table type="User" code="541" displayName="Specimen Type Modifier"/>
        <table type="User" code="542" displayName="Specimen Source Type Modifier"/>
        <table type="User" code="543" displayName="Specimen Collection Site"/>
        <table type="User" code="544" displayName="Container Condition"/>
        <table type="User" code="545" displayName="Language Translated To"/>
        <table type="User" code="547" displayName="Jurisdictional Breadth"/>
        <table type="User" code="548" displayName="Signatory_s Relationship to Subject"/>
        <table type="User" code="549" displayName="NDC Codes"/>
        <table type="HL7" code="550" displayName="Body Parts"/>
        <table type="HL7" code="552" displayName="Advanced beneficiary notice override reason"/>
    </xsl:variable>
    
    <xsl:template match="fd:tables">
        <terminology>
            <xsl:choose>
                <xsl:when test="$doref = 'true' and $skipEmptyTables = 'true'">
                    <xsl:apply-templates select="fd:table[fd:version[fd:item]/@version = $versionLabel]" mode="ref"/>
                </xsl:when>
                <xsl:when test="$doref = 'true'">
                    <xsl:apply-templates select="fd:table[string(xs:integer(@id)) = $tableTypes/@code]" mode="ref"/>
                </xsl:when>
                <xsl:when test="$skipEmptyTables = 'true'">
                    <xsl:apply-templates select="fd:table[fd:version[fd:item]/@version = $versionLabel]"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="fd:table[string(xs:integer(@id)) = $tableTypes/@code]"/>
                </xsl:otherwise>
            </xsl:choose>
        </terminology>
    </xsl:template>
    
    <!-- <table id="1" state="include">
            <version version="2.1" desc="SEX">
                <item id="1" code="F" desc="Female"/>
    -->
    <xsl:template match="fd:table" mode="ref">
        <xsl:variable name="theID" select="@id"/>
        <xsl:variable name="theName" select="loc:getNameFromId(@id)"/>
        <xsl:variable name="theDisplayName" select="if (fd:version[@version=$versionLabel]) then loc:getDisplayNameFromDesc(fd:version[@version=$versionLabel]/@desc) else (loc:getDisplayNameFromDesc(fd:version[last()]/@desc))"/>
        <xsl:variable name="theCodeSystem" select="concat($baseIdCS,'.',string(xs:integer($theID)))"/>
        <xsl:variable name="theType" select="$tableTypes[@code = string(xs:integer($theID))]/@type" as="xs:string"/>
        <valueSet ref="{concat($baseIdVS,'.',string(xs:integer($theID)))}" name="{$theName}" displayName="{$theDisplayName}"/>
    </xsl:template>
    <xsl:template match="fd:table">
        <xsl:variable name="theID" select="@id"/>
        <xsl:variable name="theName" select="loc:getNameFromId(@id)"/>
        <xsl:variable name="theDisplayName" select="if (fd:version[@version=$versionLabel]) then loc:getDisplayNameFromDesc(fd:version[@version=$versionLabel]/@desc) else (loc:getDisplayNameFromDesc(fd:version[last()]/@desc))"/>
        <xsl:variable name="theCodeSystem" select="concat($baseIdCS,'.',string(xs:integer($theID)))"/>
        <xsl:variable name="theType" select="$tableTypes[@code = string(xs:integer($theID))]/@type" as="xs:string"/>
        <valueSet id="{concat($baseIdVS,'.',string(xs:integer($theID)))}" effectiveDate="{$effectiveDate}" name="{$theName}" displayName="{$theDisplayName}" statusCode="{$statusCode}" versionLabel="{$versionLabel}">
            <desc language="en-US"><div title="V2 TABLE TYPE DO NOT REMOVE. VALUES (See chapter 2): &#34;HL7&#34; (HL7 Table) or &#34;User&#34; (User-defined)" itemprop="HL7v2TableType"><xsl:value-of select="$theType"/></div></desc>
            <xsl:if test="fd:version[@version = $versionLabel]/fd:item[not(@code = ('...', '…'))]">
                <conceptList>
                    <xsl:apply-templates select="fd:version[@version = $versionLabel]/fd:item">
                        <xsl:with-param name="theCodeSystem" select="$theCodeSystem"/>
                    </xsl:apply-templates>
                </conceptList>
            </xsl:if>
        </valueSet>
    </xsl:template>
    
    <xsl:template match="fd:item">
        <xsl:param name="theCodeSystem" required="yes"/>
        <xsl:variable name="theDisplayName">
            <xsl:choose>
                <xsl:when test="@desc[string-length()>0]">
                    <xsl:value-of select="@desc"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@code"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <concept code="{@code}" codeSystem="{$theCodeSystem}" displayName="{$theDisplayName}" level="0" type="L">
            <xsl:for-each select="fd:desc[string-length(@value) > 0]">
                <xsl:variable name="lang" as="xs:string">
                    <xsl:choose>
                        <xsl:when test="@lang = 'nl'">nl-NL</xsl:when>
                        <xsl:when test="@lang = 'de'">de-DE</xsl:when>
                    </xsl:choose>
                </xsl:variable>
                <designation language="{$lang}" type="preferred" displayName="{@value}"/>
            </xsl:for-each>
            <xsl:for-each select="@comment[string-length(.) > 0] | fd:desc/@comments[string-length() > 0]">
                <xsl:variable name="lang" as="xs:string">
                    <xsl:choose>
                        <xsl:when test="ancestor::*[1]/@lang = 'nl'">nl-NL</xsl:when>
                        <xsl:when test="ancestor::*[1]/@lang = 'de'">de-DE</xsl:when>
                        <xsl:otherwise>en-US</xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <desc language="{$lang}"><xsl:value-of select="."/></desc>
            </xsl:for-each>
        </concept>
    </xsl:template>
    
    <xsl:function name="loc:getNameFromId">
        <xsl:param name="in"/>
        <xsl:if test="string-length($in) > 0">
            <xsl:variable name="fill" select="concat('000000', $in)"/>
            <xsl:value-of select="concat('HL7', substring($fill, string-length($fill) - 3))"/>
        </xsl:if>
    </xsl:function>
    
    <xsl:function name="loc:getDisplayNameFromDesc">
        <xsl:param name="in"/>
        
        <xsl:value-of select="$in"/>
    </xsl:function>
</xsl:stylesheet>