Commands:
    npm install jsonix-schema-compiler
    java -jar node_modules/jsonix-schema-compiler/lib/jsonix-schema-compiler-full.jar -generateJsonSchema -p DECOR ../../decor/core/DECOR.xsd

Add to your JSON instances as first line after opening curly brace: 
    "$schema": "file:/Users/ahenket/Development/ART-DECOR/trunk/decor/core/valueSets.jsonschema", 

XMLSchema.jsonschema downloaded 2019-05-29 from http://www.jsonix.org/jsonschemas/w3c/2001/XMLSchema.jsonschema
    Fixed dateTime, date and time to match DECOR json
    
DECOR.jsonschema:
    Fixed all references to XMLSchema.jsonschema from online to the local fixed version
    Fixed all singular element definitions to be an array in ValueSet
    Fixed VariableMixedContent.content to be a string
    
Related to:
    https://art-decor.atlassian.net/browse/ADISSUE-11