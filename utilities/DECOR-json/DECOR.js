var DECOR_Module_Factory = function () {
  var DECOR = {
    name: 'DECOR',
    typeInfos: [{
        localName: 'ProjectRelease',
        propertyInfos: [{
            name: 'note',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'note'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'date',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'date'
            },
            type: 'attribute'
          }, {
            name: 'by',
            attributeName: {
              localPart: 'by'
            },
            type: 'attribute'
          }, {
            name: 'versionLabel',
            attributeName: {
              localPart: 'versionLabel'
            },
            type: 'attribute'
          }, {
            name: 'statusCode',
            typeInfo: '.ReleaseStatusCodeLifeCycle',
            attributeName: {
              localPart: 'statusCode'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'ActorReference',
        propertyInfos: [{
            name: 'id',
            required: true,
            attributeName: {
              localPart: 'id'
            },
            type: 'attribute'
          }, {
            name: 'role',
            typeInfo: '.ActorType',
            attributeName: {
              localPart: 'role'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Context',
        typeName: null,
        propertyInfos: [{
            name: 'id',
            values: ['*', '**'],
            attributeName: {
              localPart: 'id'
            },
            type: 'attribute'
          }, {
            name: 'path',
            attributeName: {
              localPart: 'path'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Let',
        typeName: null,
        propertyInfos: [{
            name: 'name',
            required: true,
            attributeName: {
              localPart: 'name'
            },
            type: 'attribute'
          }, {
            name: 'value',
            required: true,
            attributeName: {
              localPart: 'value'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Project',
        typeName: null,
        propertyInfos: [{
            name: 'name',
            required: true,
            collection: true,
            elementName: {
              localPart: 'name'
            },
            typeInfo: '.BusinessNameWithLanguage'
          }, {
            name: 'desc',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'desc'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'copyright',
            required: true,
            collection: true,
            elementName: {
              localPart: 'copyright'
            },
            typeInfo: '.Copyright'
          }, {
            name: 'author',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'author'
            },
            typeInfo: '.Author'
          }, {
            name: 'reference',
            elementName: {
              localPart: 'reference'
            },
            typeInfo: '.Reference'
          }, {
            name: 'restURI',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'restURI'
            },
            typeInfo: '.RestURI'
          }, {
            name: 'defaultElementNamespace',
            elementName: {
              localPart: 'defaultElementNamespace'
            },
            typeInfo: '.DefaultElementNamespace'
          }, {
            name: 'contact',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'contact'
            },
            typeInfo: '.Contact'
          }, {
            name: 'buildingBlockRepository',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'buildingBlockRepository'
            },
            typeInfo: '.BuildingBlockRepository'
          }, {
            name: 'versionOrRelease',
            minOccurs: 0,
            collection: true,
            elementTypeInfos: [{
                elementName: {
                  localPart: 'version'
                },
                typeInfo: '.ProjectHistory'
              }, {
                elementName: {
                  localPart: 'release'
                },
                typeInfo: '.ProjectRelease'
              }],
            type: 'elements'
          }, {
            name: 'id',
            required: true,
            attributeName: {
              localPart: 'id'
            },
            type: 'attribute'
          }, {
            name: 'prefix',
            required: true,
            attributeName: {
              localPart: 'prefix'
            },
            type: 'attribute'
          }, {
            name: 'defaultLanguage',
            required: true,
            attributeName: {
              localPart: 'defaultLanguage'
            },
            type: 'attribute'
          }, {
            name: 'experimental',
            typeInfo: 'Boolean',
            attributeName: {
              localPart: 'experimental'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'IdentifierAssociation',
        typeName: null,
        propertyInfos: [{
            name: 'otherAttributes',
            type: 'anyAttribute'
          }, {
            name: 'conceptId',
            required: true,
            attributeName: {
              localPart: 'conceptId'
            },
            type: 'attribute'
          }, {
            name: 'conceptFlexibility',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'conceptFlexibility'
            },
            type: 'attribute'
          }, {
            name: 'ref',
            required: true,
            attributeName: {
              localPart: 'ref'
            },
            type: 'attribute'
          }, {
            name: 'refdisplay',
            attributeName: {
              localPart: 'refdisplay'
            },
            type: 'attribute'
          }, {
            name: 'effectiveDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'effectiveDate'
            },
            type: 'attribute'
          }, {
            name: 'expirationDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'expirationDate'
            },
            type: 'attribute'
          }, {
            name: 'officialReleaseDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'officialReleaseDate'
            },
            type: 'attribute'
          }, {
            name: 'versionLabel',
            attributeName: {
              localPart: 'versionLabel'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Cardinality',
        typeName: null,
        propertyInfos: [{
            name: 'conformance',
            typeInfo: '.ConformanceType',
            attributeName: {
              localPart: 'conformance'
            },
            type: 'attribute'
          }, {
            name: 'isMandatory',
            typeInfo: 'Boolean',
            attributeName: {
              localPart: 'isMandatory'
            },
            type: 'attribute'
          }, {
            name: 'minimumMultiplicity',
            required: true,
            typeInfo: 'NonNegativeInteger',
            attributeName: {
              localPart: 'minimumMultiplicity'
            },
            type: 'attribute'
          }, {
            name: 'maximumMultiplicity',
            required: true,
            attributeName: {
              localPart: 'maximumMultiplicity'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'TemplateProperties',
        propertyInfos: [{
            name: 'tag',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'tag'
            }
          }, {
            name: 'property',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'property'
            }
          }, {
            name: 'type',
            required: true,
            typeInfo: '.TemplateTypes',
            attributeName: {
              localPart: 'type'
            },
            type: 'attribute'
          }, {
            name: 'format',
            typeInfo: '.TemplateFormats',
            attributeName: {
              localPart: 'format'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Decor',
        typeName: null,
        propertyInfos: [{
            name: 'otherAttributes',
            type: 'anyAttribute'
          }, {
            name: 'project',
            required: true,
            elementName: {
              localPart: 'project'
            },
            typeInfo: '.Project'
          }, {
            name: 'datasets',
            required: true,
            elementName: {
              localPart: 'datasets'
            },
            typeInfo: '.Datasets'
          }, {
            name: 'scenarios',
            required: true,
            elementName: {
              localPart: 'scenarios'
            },
            typeInfo: '.Scenarios'
          }, {
            name: 'ids',
            required: true,
            elementName: {
              localPart: 'ids'
            },
            typeInfo: '.Ids'
          }, {
            name: 'terminology',
            required: true,
            elementName: {
              localPart: 'terminology'
            },
            typeInfo: '.Terminology'
          }, {
            name: 'rules',
            required: true,
            elementName: {
              localPart: 'rules'
            },
            typeInfo: '.Rules'
          }, {
            name: 'issues',
            required: true,
            elementName: {
              localPart: 'issues'
            },
            typeInfo: '.Issues'
          }, {
            name: 'repository',
            typeInfo: 'Boolean',
            attributeName: {
              localPart: 'repository'
            },
            type: 'attribute'
          }, {
            name: '_private',
            typeInfo: 'Boolean',
            attributeName: {
              localPart: 'private'
            },
            type: 'attribute'
          }, {
            name: 'release',
            typeInfo: 'NonNegativeInteger',
            attributeName: {
              localPart: 'release'
            },
            type: 'attribute'
          }, {
            name: 'versionDate',
            attributeName: {
              localPart: 'versionDate'
            },
            type: 'attribute'
          }, {
            name: 'versionLabel',
            attributeName: {
              localPart: 'versionLabel'
            },
            type: 'attribute'
          }, {
            name: 'compilationDate',
            attributeName: {
              localPart: 'compilationDate'
            },
            type: 'attribute'
          }, {
            name: 'language',
            typeInfo: {
              type: 'list'
            },
            attributeName: {
              localPart: 'language'
            },
            type: 'attribute'
          }, {
            name: 'deeplinkprefix',
            attributeName: {
              localPart: 'deeplinkprefix'
            },
            type: 'attribute'
          }, {
            name: 'deeplinkprefixservices',
            attributeName: {
              localPart: 'deeplinkprefixservices'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'DataSetConcept.Context',
        typeName: null,
        propertyInfos: [{
            name: 'otherAttributes',
            type: 'anyAttribute'
          }, {
            name: 'any',
            minOccurs: 0,
            collection: true,
            allowTypedObject: false,
            mixed: false,
            type: 'anyElement'
          }]
      }, {
        localName: 'TransactionTrigger',
        baseTypeInfo: '.FreeFormMarkupWithLanguage',
        propertyInfos: [{
            name: 'id',
            typeInfo: 'NCName',
            attributeName: {
              localPart: 'id'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Terminology',
        typeName: null,
        propertyInfos: [{
            name: 'terminologyAssociation',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'terminologyAssociation'
            },
            typeInfo: '.TerminologyAssociation'
          }, {
            name: 'codeSystem',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'codeSystem'
            },
            typeInfo: '.CodeSystem'
          }, {
            name: 'valueSet',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'valueSet'
            },
            typeInfo: '.ValueSet'
          }, {
            name: 'conceptAssociation',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'conceptAssociation'
            },
            typeInfo: '.ConceptAssociation'
          }]
      }, {
        localName: 'ConceptAssociation',
        typeName: null,
        propertyInfos: [{
            name: 'desc',
            elementName: {
              localPart: 'desc'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'source',
            required: true,
            elementName: {
              localPart: 'source'
            },
            typeInfo: '.SourceOrTargetValueSet'
          }, {
            name: 'target',
            required: true,
            elementName: {
              localPart: 'target'
            },
            typeInfo: '.SourceOrTargetValueSet'
          }, {
            name: 'group',
            required: true,
            collection: true,
            elementName: {
              localPart: 'group'
            },
            typeInfo: '.ConceptMapGroupDefinition'
          }, {
            name: 'id',
            required: true,
            attributeName: {
              localPart: 'id'
            },
            type: 'attribute'
          }, {
            name: 'displayName',
            required: true,
            attributeName: {
              localPart: 'displayName'
            },
            type: 'attribute'
          }, {
            name: 'statusCode',
            typeInfo: '.ItemStatusCodeLifeCycle',
            attributeName: {
              localPart: 'statusCode'
            },
            type: 'attribute'
          }, {
            name: 'effectiveDate',
            required: true,
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'effectiveDate'
            },
            type: 'attribute'
          }, {
            name: 'expirationDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'expirationDate'
            },
            type: 'attribute'
          }, {
            name: 'officialReleaseDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'officialReleaseDate'
            },
            type: 'attribute'
          }, {
            name: 'versionLabel',
            attributeName: {
              localPart: 'versionLabel'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'DefineVariable',
        typeName: null,
        propertyInfos: [{
            name: 'content',
            collection: true,
            allowDom: false,
            elementTypeInfos: [{
                elementName: {
                  localPart: 'use'
                },
                typeInfo: '.VarUse'
              }, {
                elementName: {
                  localPart: 'code'
                },
                typeInfo: '.VarCode'
              }],
            type: 'elementRefs'
          }, {
            name: 'name',
            required: true,
            typeInfo: 'NCName',
            attributeName: {
              localPart: 'name'
            },
            type: 'attribute'
          }, {
            name: 'path',
            required: true,
            attributeName: {
              localPart: 'path'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Example',
        typeName: null,
        propertyInfos: [{
            name: 'content',
            collection: true,
            allowTypedObject: false,
            type: 'anyElement'
          }, {
            name: 'type',
            typeInfo: '.ExampleType',
            attributeName: {
              localPart: 'type'
            },
            type: 'attribute'
          }, {
            name: 'caption',
            attributeName: {
              localPart: 'caption'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'VariousMixedContent',
        propertyInfos: [{
            name: 'content',
            collection: true,
            allowTypedObject: false,
            type: 'anyElement'
          }]
      }, {
        localName: 'SourceOrTargetValueSet',
        propertyInfos: [{
            name: 'ref',
            required: true,
            attributeName: {
              localPart: 'ref'
            },
            type: 'attribute'
          }, {
            name: 'flexibility',
            attributeName: {
              localPart: 'flexibility'
            },
            type: 'attribute'
          }, {
            name: 'displayName',
            attributeName: {
              localPart: 'displayName'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Reference',
        typeName: null,
        propertyInfos: [{
            name: 'url',
            attributeName: {
              localPart: 'url'
            },
            type: 'attribute'
          }, {
            name: 'logo',
            attributeName: {
              localPart: 'logo'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'ContainsDefinition',
        propertyInfos: [{
            name: 'ref',
            required: true,
            attributeName: {
              localPart: 'ref'
            },
            type: 'attribute'
          }, {
            name: 'flexibility',
            required: true,
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'flexibility'
            },
            type: 'attribute'
          }, {
            name: 'prefix',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'prefix'
            },
            type: 'attribute'
          }, {
            name: 'datasetId',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'datasetId'
            },
            type: 'attribute'
          }, {
            name: 'datasetEffectiveDate',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'datasetEffectiveDate'
            },
            type: 'attribute'
          }, {
            name: 'datasetExpirationDate',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'datasetExpirationDate'
            },
            type: 'attribute'
          }, {
            name: 'datasetStatusCode',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'datasetStatusCode'
            },
            type: 'attribute'
          }, {
            name: 'datasetVersionLabel',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'datasetVersionLabel'
            },
            type: 'attribute'
          }, {
            name: 'iType',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'iType'
            },
            type: 'attribute'
          }, {
            name: 'iStatusCode',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'iStatusCode'
            },
            type: 'attribute'
          }, {
            name: 'iEffectiveDate',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'iEffectiveDate'
            },
            type: 'attribute'
          }, {
            name: 'iVersionLabel',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'iVersionLabel'
            },
            type: 'attribute'
          }, {
            name: 'iExpirationDate',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'iExpirationDate'
            },
            type: 'attribute'
          }, {
            name: 'originalId',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'originalId'
            },
            type: 'attribute'
          }, {
            name: 'originalStatusCode',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'originalStatusCode'
            },
            type: 'attribute'
          }, {
            name: 'originalEffectiveDate',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'originalEffectiveDate'
            },
            type: 'attribute'
          }, {
            name: 'originalVersionLabel',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'originalVersionLabel'
            },
            type: 'attribute'
          }, {
            name: 'originalExpirationDate',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'originalExpirationDate'
            },
            type: 'attribute'
          }, {
            name: 'originalPrefix',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'originalPrefix'
            },
            type: 'attribute'
          }, {
            name: 'refdisplay',
            attributeName: {
              localPart: 'refdisplay'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Assert',
        typeName: null,
        propertyInfos: [{
            name: 'content',
            collection: true,
            allowTypedObject: false,
            type: 'anyElement'
          }, {
            name: 'flag',
            attributeName: {
              localPart: 'flag'
            },
            type: 'attribute'
          }, {
            name: 'see',
            attributeName: {
              localPart: 'see'
            },
            type: 'attribute'
          }, {
            name: 'role',
            required: true,
            typeInfo: '.AssertRole',
            attributeName: {
              localPart: 'role'
            },
            type: 'attribute'
          }, {
            name: 'test',
            required: true,
            attributeName: {
              localPart: 'test'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Ids',
        typeName: null,
        propertyInfos: [{
            name: 'baseId',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'baseId'
            },
            typeInfo: '.BaseId'
          }, {
            name: 'defaultBaseId',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'defaultBaseId'
            },
            typeInfo: '.DefaultBaseId'
          }, {
            name: 'id',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'id'
            },
            typeInfo: '.Id'
          }, {
            name: 'identifierAssociation',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'identifierAssociation'
            },
            typeInfo: '.IdentifierAssociation'
          }]
      }, {
        localName: 'TemplateRelationships',
        propertyInfos: [{
            name: 'type',
            required: true,
            typeInfo: '.RelationshipTypes',
            attributeName: {
              localPart: 'type'
            },
            type: 'attribute'
          }, {
            name: 'template',
            attributeName: {
              localPart: 'template'
            },
            type: 'attribute'
          }, {
            name: 'model',
            attributeName: {
              localPart: 'model'
            },
            type: 'attribute'
          }, {
            name: 'flexibility',
            attributeName: {
              localPart: 'flexibility'
            },
            type: 'attribute'
          }, {
            name: 'tmid',
            attributeName: {
              localPart: 'tmid'
            },
            type: 'attribute'
          }, {
            name: 'tmname',
            attributeName: {
              localPart: 'tmname'
            },
            type: 'attribute'
          }, {
            name: 'tmdisplayName',
            attributeName: {
              localPart: 'tmdisplayName'
            },
            type: 'attribute'
          }, {
            name: 'tmeffectiveDate',
            attributeName: {
              localPart: 'tmeffectiveDate'
            },
            type: 'attribute'
          }, {
            name: 'tmexpirationDate',
            attributeName: {
              localPart: 'tmexpirationDate'
            },
            type: 'attribute'
          }, {
            name: 'tmstatusCode',
            attributeName: {
              localPart: 'tmstatusCode'
            },
            type: 'attribute'
          }, {
            name: 'tmversionLabel',
            attributeName: {
              localPart: 'tmversionLabel'
            },
            type: 'attribute'
          }, {
            name: 'url',
            attributeName: {
              localPart: 'url'
            },
            type: 'attribute'
          }, {
            name: 'ident',
            attributeName: {
              localPart: 'ident'
            },
            type: 'attribute'
          }, {
            name: 'linkedartefactmissing',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'linkedartefactmissing'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'ActorDefinition',
        propertyInfos: [{
            name: 'name',
            required: true,
            collection: true,
            elementName: {
              localPart: 'name'
            },
            typeInfo: '.BusinessNameWithLanguage'
          }, {
            name: 'desc',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'desc'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'id',
            required: true,
            attributeName: {
              localPart: 'id'
            },
            type: 'attribute'
          }, {
            name: 'type',
            required: true,
            typeInfo: '.ScenarioActorType',
            attributeName: {
              localPart: 'type'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'ValueSetConcept',
        propertyInfos: [{
            name: 'designation',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'designation'
            },
            typeInfo: '.Designation'
          }, {
            name: 'desc',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'desc'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'code',
            required: true,
            attributeName: {
              localPart: 'code'
            },
            type: 'attribute'
          }, {
            name: 'codeSystem',
            required: true,
            attributeName: {
              localPart: 'codeSystem'
            },
            type: 'attribute'
          }, {
            name: 'codeSystemName',
            attributeName: {
              localPart: 'codeSystemName'
            },
            type: 'attribute'
          }, {
            name: 'codeSystemVersion',
            attributeName: {
              localPart: 'codeSystemVersion'
            },
            type: 'attribute'
          }, {
            name: 'displayName',
            required: true,
            attributeName: {
              localPart: 'displayName'
            },
            type: 'attribute'
          }, {
            name: 'ordinal',
            attributeName: {
              localPart: 'ordinal'
            },
            type: 'attribute'
          }, {
            name: 'level',
            required: true,
            typeInfo: 'Integer',
            attributeName: {
              localPart: 'level'
            },
            type: 'attribute'
          }, {
            name: 'type',
            required: true,
            typeInfo: '.VocabType',
            attributeName: {
              localPart: 'type'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Report',
        typeName: null,
        propertyInfos: [{
            name: 'content',
            collection: true,
            allowTypedObject: false,
            type: 'anyElement'
          }, {
            name: 'flag',
            attributeName: {
              localPart: 'flag'
            },
            type: 'attribute'
          }, {
            name: 'see',
            attributeName: {
              localPart: 'see'
            },
            type: 'attribute'
          }, {
            name: 'role',
            required: true,
            typeInfo: '.AssertRole',
            attributeName: {
              localPart: 'role'
            },
            type: 'attribute'
          }, {
            name: 'test',
            required: true,
            attributeName: {
              localPart: 'test'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'CodeSystemConceptList',
        propertyInfos: [{
            name: 'codedConcept',
            required: true,
            collection: true,
            elementName: {
              localPart: 'codedConcept'
            },
            typeInfo: '.CodedConcept'
          }]
      }, {
        localName: 'DataSetConcept',
        propertyInfos: [{
            name: 'implementation',
            elementName: {
              localPart: 'implementation'
            },
            typeInfo: '.DataSetConcept.Implementation'
          }, {
            name: 'context',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'context'
            },
            typeInfo: '.DataSetConcept.Context'
          }, {
            name: 'condition',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'condition'
            },
            typeInfo: '.DataSetConcept.Condition'
          }, {
            name: 'inherit',
            elementName: {
              localPart: 'inherit'
            },
            typeInfo: '.InheritDefinition'
          }, {
            name: 'contains',
            elementName: {
              localPart: 'contains'
            },
            typeInfo: '.ContainsDefinition'
          }, {
            name: 'name',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'name'
            },
            typeInfo: '.BusinessNameWithLanguage'
          }, {
            name: 'synonym',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'synonym'
            },
            typeInfo: '.BusinessNameWithLanguage'
          }, {
            name: 'desc',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'desc'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'source',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'source'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'rationale',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'rationale'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'comment',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'comment'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'property',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'property'
            },
            typeInfo: '.ArbitraryPropertyType'
          }, {
            name: 'relationship',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'relationship'
            },
            typeInfo: '.ObjectRelationships'
          }, {
            name: 'operationalization',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'operationalization'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'valueDomain',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'valueDomain'
            },
            typeInfo: '.DataSetConceptValue'
          }, {
            name: 'valueSet',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'valueSet'
            },
            typeInfo: '.DataSetConcept.ValueSet'
          }, {
            name: 'terminologyAssociation',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'terminologyAssociation'
            },
            typeInfo: '.TerminologyAssociation'
          }, {
            name: 'identifierAssociation',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'identifierAssociation'
            },
            typeInfo: '.IdentifierAssociation'
          }, {
            name: 'concept',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'concept'
            },
            typeInfo: '.DataSetConcept'
          }, {
            name: 'history',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'history'
            },
            typeInfo: '.DataSetConceptHistory'
          }, {
            name: 'id',
            required: true,
            attributeName: {
              localPart: 'id'
            },
            type: 'attribute'
          }, {
            name: 'canonicalUri',
            attributeName: {
              localPart: 'canonicalUri'
            },
            type: 'attribute'
          }, {
            name: 'statusCode',
            required: true,
            typeInfo: '.ItemStatusCodeLifeCycle',
            attributeName: {
              localPart: 'statusCode'
            },
            type: 'attribute'
          }, {
            name: 'type',
            typeInfo: '.DataSetConceptType',
            attributeName: {
              localPart: 'type'
            },
            type: 'attribute'
          }, {
            name: 'iddisplay',
            attributeName: {
              localPart: 'iddisplay'
            },
            type: 'attribute'
          }, {
            name: 'refdisplay',
            attributeName: {
              localPart: 'refdisplay'
            },
            type: 'attribute'
          }, {
            name: 'shortName',
            attributeName: {
              localPart: 'shortName'
            },
            type: 'attribute'
          }, {
            name: 'effectiveDate',
            required: true,
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'effectiveDate'
            },
            type: 'attribute'
          }, {
            name: 'expirationDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'expirationDate'
            },
            type: 'attribute'
          }, {
            name: 'officialReleaseDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'officialReleaseDate'
            },
            type: 'attribute'
          }, {
            name: 'versionLabel',
            attributeName: {
              localPart: 'versionLabel'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'EquivalencyTypeDefinition',
        propertyInfos: [{
            name: 'code',
            required: true,
            typeInfo: '.EquivalencyType',
            attributeName: {
              localPart: 'code'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Rules',
        typeName: null,
        propertyInfos: [{
            name: 'templateAssociationOrTemplateOrStructuredefinition',
            minOccurs: 0,
            collection: true,
            elementTypeInfos: [{
                elementName: {
                  localPart: 'templateAssociation'
                },
                typeInfo: '.TemplateAssociationDefinition'
              }, {
                elementName: {
                  localPart: 'template'
                },
                typeInfo: '.TemplateDefinition'
              }, {
                elementName: {
                  localPart: 'structuredefinition'
                },
                typeInfo: '.StructureDefinition'
              }],
            type: 'elements'
          }]
      }, {
        localName: 'Item',
        typeName: null,
        propertyInfos: [{
            name: 'desc',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'desc'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'label',
            required: true,
            attributeName: {
              localPart: 'label'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Contact',
        typeName: null,
        propertyInfos: [{
            name: 'email',
            attributeName: {
              localPart: 'email'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'TerminologyAssociation',
        typeName: null,
        propertyInfos: [{
            name: 'otherAttributes',
            type: 'anyAttribute'
          }, {
            name: 'conceptId',
            required: true,
            attributeName: {
              localPart: 'conceptId'
            },
            type: 'attribute'
          }, {
            name: 'conceptFlexibility',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'conceptFlexibility'
            },
            type: 'attribute'
          }, {
            name: 'code',
            attributeName: {
              localPart: 'code'
            },
            type: 'attribute'
          }, {
            name: 'codeSystem',
            attributeName: {
              localPart: 'codeSystem'
            },
            type: 'attribute'
          }, {
            name: 'codeSystemName',
            attributeName: {
              localPart: 'codeSystemName'
            },
            type: 'attribute'
          }, {
            name: 'displayName',
            attributeName: {
              localPart: 'displayName'
            },
            type: 'attribute'
          }, {
            name: 'valueSet',
            attributeName: {
              localPart: 'valueSet'
            },
            type: 'attribute'
          }, {
            name: 'flexibility',
            attributeName: {
              localPart: 'flexibility'
            },
            type: 'attribute'
          }, {
            name: 'strength',
            typeInfo: '.CodingStrengthType',
            attributeName: {
              localPart: 'strength'
            },
            type: 'attribute'
          }, {
            name: 'equivalence',
            typeInfo: '.EquivalencyType',
            attributeName: {
              localPart: 'equivalence'
            },
            type: 'attribute'
          }, {
            name: 'valueSetName',
            attributeName: {
              localPart: 'valueSetName'
            },
            type: 'attribute'
          }, {
            name: 'effectiveDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'effectiveDate'
            },
            type: 'attribute'
          }, {
            name: 'expirationDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'expirationDate'
            },
            type: 'attribute'
          }, {
            name: 'officialReleaseDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'officialReleaseDate'
            },
            type: 'attribute'
          }, {
            name: 'versionLabel',
            attributeName: {
              localPart: 'versionLabel'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'ScenarioTemplateCondition',
        propertyInfos: [{
            name: 'content',
            collection: true,
            allowDom: false,
            elementName: {
              localPart: 'desc'
            },
            typeInfo: '.FreeFormMarkupWithLanguage',
            type: 'elementRef'
          }, {
            name: 'conformance',
            typeInfo: '.ConformanceType',
            attributeName: {
              localPart: 'conformance'
            },
            type: 'attribute'
          }, {
            name: 'isMandatory',
            typeInfo: 'Boolean',
            attributeName: {
              localPart: 'isMandatory'
            },
            type: 'attribute'
          }, {
            name: 'minimumMultiplicity',
            typeInfo: 'NonNegativeInteger',
            attributeName: {
              localPart: 'minimumMultiplicity'
            },
            type: 'attribute'
          }, {
            name: 'maximumMultiplicity',
            attributeName: {
              localPart: 'maximumMultiplicity'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'ConceptMapGroupDefinition',
        propertyInfos: [{
            name: 'source',
            required: true,
            elementName: {
              localPart: 'source'
            },
            typeInfo: '.SourceOrTargetCodeSystem'
          }, {
            name: 'target',
            required: true,
            elementName: {
              localPart: 'target'
            },
            typeInfo: '.SourceOrTargetCodeSystem'
          }, {
            name: 'element',
            required: true,
            collection: true,
            elementName: {
              localPart: 'element'
            },
            typeInfo: '.ConceptMapElement'
          }]
      }, {
        localName: 'Id',
        typeName: null,
        propertyInfos: [{
            name: 'designation',
            required: true,
            collection: true,
            elementName: {
              localPart: 'designation'
            },
            typeInfo: '.Designation'
          }, {
            name: 'property',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'property'
            },
            typeInfo: '.ArbitraryPropertyType'
          }, {
            name: 'root',
            required: true,
            attributeName: {
              localPart: 'root'
            },
            type: 'attribute'
          }, {
            name: 'extension',
            attributeName: {
              localPart: 'extension'
            },
            type: 'attribute'
          }, {
            name: 'assigningAuthority',
            attributeName: {
              localPart: 'assigningAuthority'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'RepresentingTemplate',
        typeName: null,
        propertyInfos: [{
            name: 'concept',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'concept'
            },
            typeInfo: '.ScenarioTemplateConcept'
          }, {
            name: 'ref',
            attributeName: {
              localPart: 'ref'
            },
            type: 'attribute'
          }, {
            name: 'flexibility',
            attributeName: {
              localPart: 'flexibility'
            },
            type: 'attribute'
          }, {
            name: 'displayName',
            attributeName: {
              localPart: 'displayName'
            },
            type: 'attribute'
          }, {
            name: 'sourceDataset',
            attributeName: {
              localPart: 'sourceDataset'
            },
            type: 'attribute'
          }, {
            name: 'sourceDatasetFlexibility',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'sourceDatasetFlexibility'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'DataSetConceptList',
        propertyInfos: [{
            name: 'concept',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'concept'
            },
            typeInfo: '.DataSetConceptListConcept'
          }, {
            name: 'id',
            attributeName: {
              localPart: 'id'
            },
            type: 'attribute'
          }, {
            name: 'ref',
            attributeName: {
              localPart: 'ref'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'ArbitraryPropertyType',
        baseTypeInfo: '.VariousMixedContent',
        propertyInfos: [{
            name: 'name',
            attributeName: {
              localPart: 'name'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'BuildingBlockRepository',
        typeName: null,
        propertyInfos: [{
            name: 'url',
            required: true,
            attributeName: {
              localPart: 'url'
            },
            type: 'attribute'
          }, {
            name: 'ident',
            attributeName: {
              localPart: 'ident'
            },
            type: 'attribute'
          }, {
            name: 'licenseKey',
            attributeName: {
              localPart: 'licenseKey'
            },
            type: 'attribute'
          }, {
            name: 'format',
            typeInfo: '.BuildingBlockRepositoryFormat',
            attributeName: {
              localPart: 'format'
            },
            type: 'attribute'
          }, {
            name: 'displayName',
            attributeName: {
              localPart: 'displayName'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'ValueSetRefOrIntensionalOp',
        propertyInfos: [{
            name: 'designation',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'designation'
            },
            typeInfo: '.Designation'
          }, {
            name: 'desc',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'desc'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'url',
            attributeName: {
              localPart: 'url'
            },
            type: 'attribute'
          }, {
            name: 'ident',
            attributeName: {
              localPart: 'ident'
            },
            type: 'attribute'
          }, {
            name: 'referencedFrom',
            attributeName: {
              localPart: 'referencedFrom'
            },
            type: 'attribute'
          }, {
            name: 'op',
            typeInfo: '.IntensionalOperators',
            attributeName: {
              localPart: 'op'
            },
            type: 'attribute'
          }, {
            name: 'code',
            attributeName: {
              localPart: 'code'
            },
            type: 'attribute'
          }, {
            name: 'codeSystem',
            attributeName: {
              localPart: 'codeSystem'
            },
            type: 'attribute'
          }, {
            name: 'displayName',
            attributeName: {
              localPart: 'displayName'
            },
            type: 'attribute'
          }, {
            name: 'ref',
            attributeName: {
              localPart: 'ref'
            },
            type: 'attribute'
          }, {
            name: 'flexibility',
            attributeName: {
              localPart: 'flexibility'
            },
            type: 'attribute'
          }, {
            name: 'exception',
            typeInfo: 'Boolean',
            attributeName: {
              localPart: 'exception'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Author',
        typeName: null,
        propertyInfos: [{
            name: 'content',
            type: 'value'
          }, {
            name: 'id',
            required: true,
            typeInfo: 'PositiveInteger',
            attributeName: {
              localPart: 'id'
            },
            type: 'attribute'
          }, {
            name: 'username',
            attributeName: {
              localPart: 'username'
            },
            type: 'attribute'
          }, {
            name: 'email',
            attributeName: {
              localPart: 'email'
            },
            type: 'attribute'
          }, {
            name: 'notifier',
            typeInfo: '.NotifierOnOff',
            attributeName: {
              localPart: 'notifier'
            },
            type: 'attribute'
          }, {
            name: 'effectiveDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'effectiveDate'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'ValueSetConceptList',
        propertyInfos: [{
            name: 'conceptOrIncludeOrExclude',
            minOccurs: 0,
            collection: true,
            mixed: false,
            allowDom: false,
            elementTypeInfos: [{
                elementName: {
                  localPart: 'include'
                },
                typeInfo: '.ValueSetRefOrIntensionalOp'
              }, {
                elementName: {
                  localPart: 'concept'
                },
                typeInfo: '.ValueSetConcept'
              }, {
                elementName: {
                  localPart: 'exclude'
                },
                typeInfo: '.ValueSetRefOrIntensionalOp'
              }],
            type: 'elementRefs'
          }, {
            name: 'exception',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'exception'
            },
            typeInfo: '.ValueSetConcept'
          }]
      }, {
        localName: 'VarUse',
        propertyInfos: [{
            name: 'path',
            required: true,
            attributeName: {
              localPart: 'path'
            },
            type: 'attribute'
          }, {
            name: 'as',
            required: true,
            typeInfo: 'NMToken',
            attributeName: {
              localPart: 'as'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'ConceptMapTarget',
        propertyInfos: [{
            name: 'equivalence',
            required: true,
            collection: true,
            elementName: {
              localPart: 'equivalence'
            },
            typeInfo: '.EquivalencyTypeDefinition'
          }, {
            name: 'comment',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'comment'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'code',
            required: true,
            attributeName: {
              localPart: 'code'
            },
            type: 'attribute'
          }, {
            name: 'displayName',
            attributeName: {
              localPart: 'displayName'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Copyright',
        typeName: null,
        propertyInfos: [{
            name: 'addrLine',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'addrLine'
            },
            typeInfo: '.AddrLine'
          }, {
            name: 'years',
            required: true,
            typeInfo: {
              type: 'list'
            },
            attributeName: {
              localPart: 'years'
            },
            type: 'attribute'
          }, {
            name: 'by',
            attributeName: {
              localPart: 'by'
            },
            type: 'attribute'
          }, {
            name: 'logo',
            attributeName: {
              localPart: 'logo'
            },
            type: 'attribute'
          }, {
            name: 'type',
            typeInfo: '.CopyrightType',
            attributeName: {
              localPart: 'type'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Scenario',
        typeName: null,
        propertyInfos: [{
            name: 'name',
            required: true,
            collection: true,
            elementName: {
              localPart: 'name'
            },
            typeInfo: '.BusinessNameWithLanguage'
          }, {
            name: 'desc',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'desc'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'trigger',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'trigger'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'condition',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'condition'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'transaction',
            required: true,
            collection: true,
            elementName: {
              localPart: 'transaction'
            },
            typeInfo: '.Transaction'
          }, {
            name: 'id',
            required: true,
            attributeName: {
              localPart: 'id'
            },
            type: 'attribute'
          }, {
            name: 'statusCode',
            required: true,
            typeInfo: '.ItemStatusCodeLifeCycle',
            attributeName: {
              localPart: 'statusCode'
            },
            type: 'attribute'
          }, {
            name: 'effectiveDate',
            required: true,
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'effectiveDate'
            },
            type: 'attribute'
          }, {
            name: 'expirationDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'expirationDate'
            },
            type: 'attribute'
          }, {
            name: 'officialReleaseDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'officialReleaseDate'
            },
            type: 'attribute'
          }, {
            name: 'versionLabel',
            attributeName: {
              localPart: 'versionLabel'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Datasets',
        typeName: null,
        propertyInfos: [{
            name: 'dataset',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'dataset'
            },
            typeInfo: '.Dataset'
          }]
      }, {
        localName: 'ValueSet.SourceCodeSystem',
        typeName: null,
        propertyInfos: [{
            name: 'otherAttributes',
            type: 'anyAttribute'
          }, {
            name: 'any',
            minOccurs: 0,
            collection: true,
            allowTypedObject: false,
            mixed: false,
            type: 'anyElement'
          }]
      }, {
        localName: 'ValueSet',
        typeName: null,
        propertyInfos: [{
            name: 'desc',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'desc'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'sourceCodeSystem',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'sourceCodeSystem'
            },
            typeInfo: '.ValueSet.SourceCodeSystem'
          }, {
            name: 'publishingAuthority',
            elementName: {
              localPart: 'publishingAuthority'
            },
            typeInfo: '.AuthorityType'
          }, {
            name: 'purpose',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'purpose'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'copyright',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'copyright'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'completeCodeSystem',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'completeCodeSystem'
            },
            typeInfo: '.CodeSystemReference'
          }, {
            name: 'conceptList',
            elementName: {
              localPart: 'conceptList'
            },
            typeInfo: '.ValueSetConceptList'
          }, {
            name: 'id',
            attributeName: {
              localPart: 'id'
            },
            type: 'attribute'
          }, {
            name: 'canonicalUri',
            attributeName: {
              localPart: 'canonicalUri'
            },
            type: 'attribute'
          }, {
            name: 'ref',
            attributeName: {
              localPart: 'ref'
            },
            type: 'attribute'
          }, {
            name: 'name',
            required: true,
            typeInfo: 'NMToken',
            attributeName: {
              localPart: 'name'
            },
            type: 'attribute'
          }, {
            name: 'displayName',
            required: true,
            attributeName: {
              localPart: 'displayName'
            },
            type: 'attribute'
          }, {
            name: 'statusCode',
            typeInfo: '.ItemStatusCodeLifeCycle',
            attributeName: {
              localPart: 'statusCode'
            },
            type: 'attribute'
          }, {
            name: 'experimental',
            typeInfo: 'Boolean',
            attributeName: {
              localPart: 'experimental'
            },
            type: 'attribute'
          }, {
            name: 'flexibility',
            attributeName: {
              localPart: 'flexibility'
            },
            type: 'attribute'
          }, {
            name: 'effectiveDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'effectiveDate'
            },
            type: 'attribute'
          }, {
            name: 'expirationDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'expirationDate'
            },
            type: 'attribute'
          }, {
            name: 'officialReleaseDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'officialReleaseDate'
            },
            type: 'attribute'
          }, {
            name: 'versionLabel',
            attributeName: {
              localPart: 'versionLabel'
            },
            type: 'attribute'
          }, {
            name: 'url',
            attributeName: {
              localPart: 'url'
            },
            type: 'attribute'
          }, {
            name: 'ident',
            attributeName: {
              localPart: 'ident'
            },
            type: 'attribute'
          }, {
            name: 'referencedFrom',
            attributeName: {
              localPart: 'referencedFrom'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'DefaultBaseId',
        typeName: null,
        propertyInfos: [{
            name: 'id',
            required: true,
            attributeName: {
              localPart: 'id'
            },
            type: 'attribute'
          }, {
            name: 'type',
            required: true,
            typeInfo: '.DecorObjectType',
            attributeName: {
              localPart: 'type'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'AddrLine',
        typeName: null,
        propertyInfos: [{
            name: 'content',
            collection: true,
            allowDom: false,
            type: 'elementRefs'
          }, {
            name: 'type',
            typeInfo: '.AddressLineType',
            attributeName: {
              localPart: 'type'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'ActorsReference',
        propertyInfos: [{
            name: 'actor',
            required: true,
            collection: true,
            elementName: {
              localPart: 'actor'
            },
            typeInfo: '.ActorReference'
          }]
      }, {
        localName: 'ProjectHistory',
        propertyInfos: [{
            name: 'desc',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'desc'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'date',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'date'
            },
            type: 'attribute'
          }, {
            name: 'by',
            attributeName: {
              localPart: 'by'
            },
            type: 'attribute'
          }, {
            name: 'statusCode',
            typeInfo: '.ReleaseStatusCodeLifeCycle',
            attributeName: {
              localPart: 'statusCode'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'DataSetConceptValue',
        propertyInfos: [{
            name: 'propertyOrConceptListOrExample',
            minOccurs: 0,
            collection: true,
            elementTypeInfos: [{
                elementName: {
                  localPart: 'property'
                },
                typeInfo: '.DataSetValueProperty'
              }, {
                elementName: {
                  localPart: 'conceptList'
                },
                typeInfo: '.DataSetConceptList'
              }, {
                elementName: {
                  localPart: 'example'
                },
                typeInfo: '.Example'
              }],
            type: 'elements'
          }, {
            name: 'type',
            required: true,
            typeInfo: '.DataSetValueType',
            attributeName: {
              localPart: 'type'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'ObjectHistory',
        propertyInfos: [{
            name: 'desc',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'desc'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'date',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'date'
            },
            type: 'attribute'
          }, {
            name: 'by',
            attributeName: {
              localPart: 'by'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'BaseId',
        typeName: null,
        propertyInfos: [{
            name: 'id',
            required: true,
            attributeName: {
              localPart: 'id'
            },
            type: 'attribute'
          }, {
            name: 'type',
            required: true,
            typeInfo: '.DecorObjectType',
            attributeName: {
              localPart: 'type'
            },
            type: 'attribute'
          }, {
            name: 'prefix',
            required: true,
            attributeName: {
              localPart: 'prefix'
            },
            type: 'attribute'
          }, {
            name: '_default',
            typeInfo: 'Boolean',
            attributeName: {
              localPart: 'default'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Labels',
        typeName: null,
        propertyInfos: [{
            name: 'label',
            required: true,
            collection: true,
            elementName: {
              localPart: 'label'
            },
            typeInfo: '.IssueLabelDefinition'
          }]
      }, {
        localName: 'RuleDefinition',
        propertyInfos: [{
            name: 'desc',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'desc'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'item',
            elementName: {
              localPart: 'item'
            },
            typeInfo: '.Item'
          }, {
            name: 'example',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'example'
            },
            typeInfo: '.Example'
          }, {
            name: 'vocabulary',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'vocabulary'
            },
            typeInfo: '.Vocabulary'
          }, {
            name: 'property',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'property'
            },
            typeInfo: '.Property'
          }, {
            name: 'text',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'text'
            }
          }, {
            name: 'attribute',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'attribute'
            },
            typeInfo: '.Attribute'
          }, {
            name: 'letOrAssertOrReport',
            minOccurs: 0,
            collection: true,
            elementTypeInfos: [{
                elementName: {
                  localPart: 'let'
                },
                typeInfo: '.Let'
              }, {
                elementName: {
                  localPart: 'assert'
                },
                typeInfo: '.Assert'
              }, {
                elementName: {
                  localPart: 'report'
                },
                typeInfo: '.Report'
              }, {
                elementName: {
                  localPart: 'defineVariable'
                },
                typeInfo: '.DefineVariable'
              }, {
                elementName: {
                  localPart: 'element'
                },
                typeInfo: '.RuleDefinition'
              }, {
                elementName: {
                  localPart: 'include'
                },
                typeInfo: '.IncludeDefinition'
              }, {
                elementName: {
                  localPart: 'choice'
                },
                typeInfo: '.ChoiceDefinition'
              }, {
                elementName: {
                  localPart: 'constraint'
                },
                typeInfo: '.FreeFormMarkupWithLanguage'
              }],
            type: 'elements'
          }, {
            name: 'displayName',
            attributeName: {
              localPart: 'displayName'
            },
            type: 'attribute'
          }, {
            name: 'statusCode',
            typeInfo: '.ItemStatusCodeLifeCycle',
            attributeName: {
              localPart: 'statusCode'
            },
            type: 'attribute'
          }, {
            name: 'name',
            attributeName: {
              localPart: 'name'
            },
            type: 'attribute'
          }, {
            name: 'useWhere',
            typeInfo: 'Boolean',
            attributeName: {
              localPart: 'useWhere'
            },
            type: 'attribute'
          }, {
            name: 'contains',
            attributeName: {
              localPart: 'contains'
            },
            type: 'attribute'
          }, {
            name: 'flexibility',
            attributeName: {
              localPart: 'flexibility'
            },
            type: 'attribute'
          }, {
            name: 'conformance',
            typeInfo: '.ConformanceType',
            attributeName: {
              localPart: 'conformance'
            },
            type: 'attribute'
          }, {
            name: 'isMandatory',
            typeInfo: 'Boolean',
            attributeName: {
              localPart: 'isMandatory'
            },
            type: 'attribute'
          }, {
            name: 'id',
            attributeName: {
              localPart: 'id'
            },
            type: 'attribute'
          }, {
            name: 'datatype',
            typeInfo: 'QName',
            attributeName: {
              localPart: 'datatype'
            },
            type: 'attribute'
          }, {
            name: 'isClosed',
            typeInfo: 'Boolean',
            attributeName: {
              localPart: 'isClosed'
            },
            type: 'attribute'
          }, {
            name: 'strength',
            typeInfo: '.CodingStrengthType',
            attributeName: {
              localPart: 'strength'
            },
            type: 'attribute'
          }, {
            name: 'effectiveDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'effectiveDate'
            },
            type: 'attribute'
          }, {
            name: 'expirationDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'expirationDate'
            },
            type: 'attribute'
          }, {
            name: 'officialReleaseDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'officialReleaseDate'
            },
            type: 'attribute'
          }, {
            name: 'versionLabel',
            attributeName: {
              localPart: 'versionLabel'
            },
            type: 'attribute'
          }, {
            name: 'minimumMultiplicity',
            typeInfo: 'NonNegativeInteger',
            attributeName: {
              localPart: 'minimumMultiplicity'
            },
            type: 'attribute'
          }, {
            name: 'maximumMultiplicity',
            attributeName: {
              localPart: 'maximumMultiplicity'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'CodeSystemReference',
        propertyInfos: [{
            name: 'codeSystem',
            required: true,
            attributeName: {
              localPart: 'codeSystem'
            },
            type: 'attribute'
          }, {
            name: 'codeSystemName',
            attributeName: {
              localPart: 'codeSystemName'
            },
            type: 'attribute'
          }, {
            name: 'codeSystemVersion',
            attributeName: {
              localPart: 'codeSystemVersion'
            },
            type: 'attribute'
          }, {
            name: 'flexibility',
            attributeName: {
              localPart: 'flexibility'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'IssueLabelDefinition',
        propertyInfos: [{
            name: 'desc',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'desc'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'color',
            attributeName: {
              localPart: 'color'
            },
            type: 'attribute'
          }, {
            name: 'code',
            required: true,
            attributeName: {
              localPart: 'code'
            },
            type: 'attribute'
          }, {
            name: 'name',
            required: true,
            attributeName: {
              localPart: 'name'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Pathname',
        typeName: null,
        propertyInfos: [{
            name: 'path',
            required: true,
            attributeName: {
              localPart: 'path'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'DataSetValueProperty',
        propertyInfos: [{
            name: 'unit',
            attributeName: {
              localPart: 'unit'
            },
            type: 'attribute'
          }, {
            name: 'currency',
            attributeName: {
              localPart: 'currency'
            },
            type: 'attribute'
          }, {
            name: 'minInclude',
            attributeName: {
              localPart: 'minInclude'
            },
            type: 'attribute'
          }, {
            name: 'maxInclude',
            attributeName: {
              localPart: 'maxInclude'
            },
            type: 'attribute'
          }, {
            name: 'fractionDigits',
            attributeName: {
              localPart: 'fractionDigits'
            },
            type: 'attribute'
          }, {
            name: 'timeStampPrecision',
            values: ['Y', 'Y!', 'YM', 'YM!', 'YMD', 'YMD!', 'YMDHM'],
            attributeName: {
              localPart: 'timeStampPrecision'
            },
            type: 'attribute'
          }, {
            name: '_default',
            attributeName: {
              localPart: 'default'
            },
            type: 'attribute'
          }, {
            name: 'fixed',
            attributeName: {
              localPart: 'fixed'
            },
            type: 'attribute'
          }, {
            name: 'minLength',
            typeInfo: 'NonNegativeInteger',
            attributeName: {
              localPart: 'minLength'
            },
            type: 'attribute'
          }, {
            name: 'maxLength',
            typeInfo: 'NonNegativeInteger',
            attributeName: {
              localPart: 'maxLength'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'DataSetConceptHistory',
        propertyInfos: [{
            name: 'concept',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'concept'
            },
            typeInfo: '.DataSetConcept'
          }, {
            name: 'validTimeHigh',
            required: true,
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'validTimeHigh'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'TemplateAssociationDefinition',
        propertyInfos: [{
            name: 'concept',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'concept'
            },
            typeInfo: '.TemplateAssociationConcept'
          }, {
            name: 'templateId',
            required: true,
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'templateId'
            },
            type: 'attribute'
          }, {
            name: 'effectiveDate',
            required: true,
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'effectiveDate'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Issues',
        typeName: null,
        propertyInfos: [{
            name: 'labels',
            elementName: {
              localPart: 'labels'
            },
            typeInfo: '.Labels'
          }, {
            name: 'issue',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'issue'
            },
            typeInfo: '.Issue'
          }, {
            name: 'notifier',
            typeInfo: '.NotifierOnOff',
            attributeName: {
              localPart: 'notifier'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Actors',
        typeName: null,
        propertyInfos: [{
            name: 'actor',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'actor'
            },
            typeInfo: '.ActorDefinition'
          }]
      }, {
        localName: 'Vocabulary',
        typeName: null,
        propertyInfos: [{
            name: 'valueSet',
            attributeName: {
              localPart: 'valueSet'
            },
            type: 'attribute'
          }, {
            name: 'flexibility',
            attributeName: {
              localPart: 'flexibility'
            },
            type: 'attribute'
          }, {
            name: 'code',
            attributeName: {
              localPart: 'code'
            },
            type: 'attribute'
          }, {
            name: 'codeSystem',
            attributeName: {
              localPart: 'codeSystem'
            },
            type: 'attribute'
          }, {
            name: 'displayName',
            attributeName: {
              localPart: 'displayName'
            },
            type: 'attribute'
          }, {
            name: 'codeSystemName',
            attributeName: {
              localPart: 'codeSystemName'
            },
            type: 'attribute'
          }, {
            name: 'domain',
            attributeName: {
              localPart: 'domain'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'ConceptMapElement',
        propertyInfos: [{
            name: 'target',
            required: true,
            collection: true,
            elementName: {
              localPart: 'target'
            },
            typeInfo: '.ConceptMapTarget'
          }, {
            name: 'code',
            required: true,
            attributeName: {
              localPart: 'code'
            },
            type: 'attribute'
          }, {
            name: 'displayName',
            attributeName: {
              localPart: 'displayName'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'CodeSystem',
        typeName: null,
        propertyInfos: [{
            name: 'desc',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'desc'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'publishingAuthority',
            elementName: {
              localPart: 'publishingAuthority'
            },
            typeInfo: '.AuthorityType'
          }, {
            name: 'purpose',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'purpose'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'copyright',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'copyright'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'conceptList',
            elementName: {
              localPart: 'conceptList'
            },
            typeInfo: '.CodeSystemConceptList'
          }, {
            name: 'any',
            allowTypedObject: false,
            mixed: false,
            type: 'anyElement'
          }, {
            name: 'id',
            attributeName: {
              localPart: 'id'
            },
            type: 'attribute'
          }, {
            name: 'canonicalUri',
            attributeName: {
              localPart: 'canonicalUri'
            },
            type: 'attribute'
          }, {
            name: 'ref',
            attributeName: {
              localPart: 'ref'
            },
            type: 'attribute'
          }, {
            name: 'name',
            required: true,
            typeInfo: 'NMToken',
            attributeName: {
              localPart: 'name'
            },
            type: 'attribute'
          }, {
            name: 'displayName',
            required: true,
            attributeName: {
              localPart: 'displayName'
            },
            type: 'attribute'
          }, {
            name: 'statusCode',
            typeInfo: '.ItemStatusCodeLifeCycle',
            attributeName: {
              localPart: 'statusCode'
            },
            type: 'attribute'
          }, {
            name: 'experimental',
            typeInfo: 'Boolean',
            attributeName: {
              localPart: 'experimental'
            },
            type: 'attribute'
          }, {
            name: 'caseSensitive',
            typeInfo: 'Boolean',
            attributeName: {
              localPart: 'caseSensitive'
            },
            type: 'attribute'
          }, {
            name: 'flexibility',
            attributeName: {
              localPart: 'flexibility'
            },
            type: 'attribute'
          }, {
            name: 'effectiveDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'effectiveDate'
            },
            type: 'attribute'
          }, {
            name: 'expirationDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'expirationDate'
            },
            type: 'attribute'
          }, {
            name: 'officialReleaseDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'officialReleaseDate'
            },
            type: 'attribute'
          }, {
            name: 'versionLabel',
            attributeName: {
              localPart: 'versionLabel'
            },
            type: 'attribute'
          }, {
            name: 'url',
            attributeName: {
              localPart: 'url'
            },
            type: 'attribute'
          }, {
            name: 'ident',
            attributeName: {
              localPart: 'ident'
            },
            type: 'attribute'
          }, {
            name: 'referencedFrom',
            attributeName: {
              localPart: 'referencedFrom'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'IncludeDefinition',
        propertyInfos: [{
            name: 'desc',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'desc'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'item',
            elementName: {
              localPart: 'item'
            },
            typeInfo: '.Item'
          }, {
            name: 'example',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'example'
            },
            typeInfo: '.Example'
          }, {
            name: 'constraint',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'constraint'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'ref',
            required: true,
            attributeName: {
              localPart: 'ref'
            },
            type: 'attribute'
          }, {
            name: 'flexibility',
            attributeName: {
              localPart: 'flexibility'
            },
            type: 'attribute'
          }, {
            name: 'conformance',
            typeInfo: '.ConformanceType',
            attributeName: {
              localPart: 'conformance'
            },
            type: 'attribute'
          }, {
            name: 'isMandatory',
            typeInfo: 'Boolean',
            attributeName: {
              localPart: 'isMandatory'
            },
            type: 'attribute'
          }, {
            name: 'minimumMultiplicity',
            typeInfo: 'NonNegativeInteger',
            attributeName: {
              localPart: 'minimumMultiplicity'
            },
            type: 'attribute'
          }, {
            name: 'maximumMultiplicity',
            attributeName: {
              localPart: 'maximumMultiplicity'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'FreeFormMarkupWithLanguageCode',
        baseTypeInfo: '.VariousMixedContent',
        propertyInfos: [{
            name: 'language',
            attributeName: {
              localPart: 'language'
            },
            type: 'attribute'
          }, {
            name: 'lastTranslated',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'lastTranslated'
            },
            type: 'attribute'
          }, {
            name: 'mimeType',
            attributeName: {
              localPart: 'mimeType'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'TemplateId',
        typeName: null,
        propertyInfos: [{
            name: 'root',
            attributeName: {
              localPart: 'root'
            },
            type: 'attribute'
          }, {
            name: 'extension',
            attributeName: {
              localPart: 'extension'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Dataset',
        typeName: null,
        propertyInfos: [{
            name: 'otherAttributes',
            type: 'anyAttribute'
          }, {
            name: 'name',
            required: true,
            collection: true,
            elementName: {
              localPart: 'name'
            },
            typeInfo: '.BusinessNameWithLanguage'
          }, {
            name: 'desc',
            required: true,
            collection: true,
            elementName: {
              localPart: 'desc'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'property',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'property'
            },
            typeInfo: '.ArbitraryPropertyType'
          }, {
            name: 'relationship',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'relationship'
            },
            typeInfo: '.ObjectRelationships'
          }, {
            name: 'concept',
            required: true,
            collection: true,
            elementName: {
              localPart: 'concept'
            },
            typeInfo: '.DataSetConcept'
          }, {
            name: 'id',
            required: true,
            attributeName: {
              localPart: 'id'
            },
            type: 'attribute'
          }, {
            name: 'canonicalUri',
            attributeName: {
              localPart: 'canonicalUri'
            },
            type: 'attribute'
          }, {
            name: 'statusCode',
            required: true,
            typeInfo: '.ItemStatusCodeLifeCycle',
            attributeName: {
              localPart: 'statusCode'
            },
            type: 'attribute'
          }, {
            name: 'effectiveDate',
            required: true,
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'effectiveDate'
            },
            type: 'attribute'
          }, {
            name: 'expirationDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'expirationDate'
            },
            type: 'attribute'
          }, {
            name: 'officialReleaseDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'officialReleaseDate'
            },
            type: 'attribute'
          }, {
            name: 'versionLabel',
            attributeName: {
              localPart: 'versionLabel'
            },
            type: 'attribute'
          }, {
            name: 'iddisplay',
            attributeName: {
              localPart: 'iddisplay'
            },
            type: 'attribute'
          }, {
            name: 'shortName',
            attributeName: {
              localPart: 'shortName'
            },
            type: 'attribute'
          }, {
            name: 'prefix',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'prefix'
            },
            type: 'attribute'
          }, {
            name: 'url',
            attributeName: {
              localPart: 'url'
            },
            type: 'attribute'
          }, {
            name: 'ident',
            attributeName: {
              localPart: 'ident'
            },
            type: 'attribute'
          }, {
            name: 'referencedFrom',
            attributeName: {
              localPart: 'referencedFrom'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Instances',
        typeName: null,
        propertyInfos: [{
            name: 'any',
            minOccurs: 0,
            collection: true,
            allowTypedObject: false,
            mixed: false,
            type: 'anyElement'
          }, {
            name: 'displayName',
            attributeName: {
              localPart: 'displayName'
            },
            type: 'attribute'
          }, {
            name: 'effectiveDate',
            required: true,
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'effectiveDate'
            },
            type: 'attribute'
          }, {
            name: 'ref',
            required: true,
            attributeName: {
              localPart: 'ref'
            },
            type: 'attribute'
          }, {
            name: 'statusCode',
            required: true,
            typeInfo: '.ItemStatusCodeLifeCycle',
            attributeName: {
              localPart: 'statusCode'
            },
            type: 'attribute'
          }, {
            name: 'process',
            typeInfo: '.ProcessCode',
            attributeName: {
              localPart: 'process'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'CodedConcept',
        typeName: null,
        propertyInfos: [{
            name: 'designation',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'designation'
            },
            typeInfo: '.Designation'
          }, {
            name: 'desc',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'desc'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'code',
            required: true,
            attributeName: {
              localPart: 'code'
            },
            type: 'attribute'
          }, {
            name: 'statusCode',
            attributeName: {
              localPart: 'statusCode'
            },
            type: 'attribute'
          }, {
            name: 'level',
            required: true,
            typeInfo: 'Integer',
            attributeName: {
              localPart: 'level'
            },
            type: 'attribute'
          }, {
            name: 'type',
            required: true,
            typeInfo: '.VocabType',
            attributeName: {
              localPart: 'type'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Transaction',
        typeName: null,
        propertyInfos: [{
            name: 'otherAttributes',
            type: 'anyAttribute'
          }, {
            name: 'name',
            required: true,
            collection: true,
            elementName: {
              localPart: 'name'
            },
            typeInfo: '.BusinessNameWithLanguage'
          }, {
            name: 'desc',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'desc'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'trigger',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'trigger'
            },
            typeInfo: '.TransactionTrigger'
          }, {
            name: 'condition',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'condition'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'dependencies',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'dependencies'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'actors',
            required: true,
            elementName: {
              localPart: 'actors'
            },
            typeInfo: '.ActorsReference'
          }, {
            name: 'representingTemplate',
            elementName: {
              localPart: 'representingTemplate'
            },
            typeInfo: '.RepresentingTemplate'
          }, {
            name: 'transaction',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'transaction'
            },
            typeInfo: '.Transaction'
          }, {
            name: 'id',
            required: true,
            attributeName: {
              localPart: 'id'
            },
            type: 'attribute'
          }, {
            name: 'canonicalUri',
            attributeName: {
              localPart: 'canonicalUri'
            },
            type: 'attribute'
          }, {
            name: 'model',
            typeInfo: 'NCName',
            attributeName: {
              localPart: 'model'
            },
            type: 'attribute'
          }, {
            name: 'label',
            typeInfo: 'NCName',
            attributeName: {
              localPart: 'label'
            },
            type: 'attribute'
          }, {
            name: 'type',
            required: true,
            typeInfo: '.TransactionType',
            attributeName: {
              localPart: 'type'
            },
            type: 'attribute'
          }, {
            name: 'statusCode',
            typeInfo: '.ItemStatusCodeLifeCycle',
            attributeName: {
              localPart: 'statusCode'
            },
            type: 'attribute'
          }, {
            name: 'effectiveDate',
            required: true,
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'effectiveDate'
            },
            type: 'attribute'
          }, {
            name: 'expirationDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'expirationDate'
            },
            type: 'attribute'
          }, {
            name: 'officialReleaseDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'officialReleaseDate'
            },
            type: 'attribute'
          }, {
            name: 'versionLabel',
            attributeName: {
              localPart: 'versionLabel'
            },
            type: 'attribute'
          }, {
            name: 'iddisplay',
            attributeName: {
              localPart: 'iddisplay'
            },
            type: 'attribute'
          }, {
            name: 'shortName',
            attributeName: {
              localPart: 'shortName'
            },
            type: 'attribute'
          }, {
            name: 'prefix',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'prefix'
            },
            type: 'attribute'
          }, {
            name: 'url',
            attributeName: {
              localPart: 'url'
            },
            type: 'attribute'
          }, {
            name: 'ident',
            attributeName: {
              localPart: 'ident'
            },
            type: 'attribute'
          }, {
            name: 'referencedFrom',
            attributeName: {
              localPart: 'referencedFrom'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'DefaultElementNamespace',
        typeName: null,
        propertyInfos: [{
            name: 'ns',
            required: true,
            attributeName: {
              localPart: 'ns'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Issue',
        typeName: null,
        propertyInfos: [{
            name: 'object',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'object'
            },
            typeInfo: '.IssueObject'
          }, {
            name: 'trackingOrAssignment',
            minOccurs: 0,
            collection: true,
            elementTypeInfos: [{
                elementName: {
                  localPart: 'tracking'
                },
                typeInfo: '.IssueTracking'
              }, {
                elementName: {
                  localPart: 'assignment'
                },
                typeInfo: '.IssueAssignment'
              }],
            type: 'elements'
          }, {
            name: 'id',
            required: true,
            attributeName: {
              localPart: 'id'
            },
            type: 'attribute'
          }, {
            name: 'type',
            required: true,
            typeInfo: '.IssueType',
            attributeName: {
              localPart: 'type'
            },
            type: 'attribute'
          }, {
            name: 'priority',
            typeInfo: '.IssuePriority',
            attributeName: {
              localPart: 'priority'
            },
            type: 'attribute'
          }, {
            name: 'displayName',
            attributeName: {
              localPart: 'displayName'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Attribute',
        typeName: null,
        propertyInfos: [{
            name: 'desc',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'desc'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'item',
            elementName: {
              localPart: 'item'
            },
            typeInfo: '.Item'
          }, {
            name: 'vocabulary',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'vocabulary'
            },
            typeInfo: '.Vocabulary'
          }, {
            name: 'constraint',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'constraint'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'name',
            typeInfo: 'NMToken',
            attributeName: {
              localPart: 'name'
            },
            type: 'attribute'
          }, {
            name: 'value',
            attributeName: {
              localPart: 'value'
            },
            type: 'attribute'
          }, {
            name: 'classCode',
            attributeName: {
              localPart: 'classCode'
            },
            type: 'attribute'
          }, {
            name: 'contextConductionInd',
            typeInfo: 'Boolean',
            attributeName: {
              localPart: 'contextConductionInd'
            },
            type: 'attribute'
          }, {
            name: 'contextControlCode',
            typeInfo: 'NMToken',
            attributeName: {
              localPart: 'contextControlCode'
            },
            type: 'attribute'
          }, {
            name: 'determinerCode',
            typeInfo: 'NMToken',
            attributeName: {
              localPart: 'determinerCode'
            },
            type: 'attribute'
          }, {
            name: 'extension',
            attributeName: {
              localPart: 'extension'
            },
            type: 'attribute'
          }, {
            name: 'independentInd',
            typeInfo: 'Boolean',
            attributeName: {
              localPart: 'independentInd'
            },
            type: 'attribute'
          }, {
            name: 'institutionSpecified',
            typeInfo: 'Boolean',
            attributeName: {
              localPart: 'institutionSpecified'
            },
            type: 'attribute'
          }, {
            name: 'inversionInd',
            typeInfo: 'Boolean',
            attributeName: {
              localPart: 'inversionInd'
            },
            type: 'attribute'
          }, {
            name: 'mediaType',
            attributeName: {
              localPart: 'mediaType'
            },
            type: 'attribute'
          }, {
            name: 'moodCode',
            attributeName: {
              localPart: 'moodCode'
            },
            type: 'attribute'
          }, {
            name: 'negationInd',
            attributeName: {
              localPart: 'negationInd'
            },
            type: 'attribute'
          }, {
            name: 'nullFlavor',
            attributeName: {
              localPart: 'nullFlavor'
            },
            type: 'attribute'
          }, {
            name: 'operator',
            typeInfo: 'NMToken',
            attributeName: {
              localPart: 'operator'
            },
            type: 'attribute'
          }, {
            name: 'qualifier',
            attributeName: {
              localPart: 'qualifier'
            },
            type: 'attribute'
          }, {
            name: 'representation',
            attributeName: {
              localPart: 'representation'
            },
            type: 'attribute'
          }, {
            name: 'root',
            attributeName: {
              localPart: 'root'
            },
            type: 'attribute'
          }, {
            name: 'typeCode',
            attributeName: {
              localPart: 'typeCode'
            },
            type: 'attribute'
          }, {
            name: 'unit',
            attributeName: {
              localPart: 'unit'
            },
            type: 'attribute'
          }, {
            name: 'use',
            attributeName: {
              localPart: 'use'
            },
            type: 'attribute'
          }, {
            name: 'isOptional',
            typeInfo: 'Boolean',
            attributeName: {
              localPart: 'isOptional'
            },
            type: 'attribute'
          }, {
            name: 'prohibited',
            typeInfo: 'Boolean',
            attributeName: {
              localPart: 'prohibited'
            },
            type: 'attribute'
          }, {
            name: 'datatype',
            typeInfo: 'QName',
            attributeName: {
              localPart: 'datatype'
            },
            type: 'attribute'
          }, {
            name: 'id',
            attributeName: {
              localPart: 'id'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'BusinessNameWithLanguage',
        propertyInfos: [{
            name: 'value',
            type: 'value'
          }, {
            name: 'language',
            attributeName: {
              localPart: 'language'
            },
            type: 'attribute'
          }, {
            name: 'lastTranslated',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'lastTranslated'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Scenarios',
        typeName: null,
        propertyInfos: [{
            name: 'actors',
            required: true,
            elementName: {
              localPart: 'actors'
            },
            typeInfo: '.Actors'
          }, {
            name: 'scenario',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'scenario'
            },
            typeInfo: '.Scenario'
          }, {
            name: 'instances',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'instances'
            },
            typeInfo: '.Instances'
          }]
      }, {
        localName: 'TemplateAssociationConcept',
        propertyInfos: [{
            name: 'ref',
            required: true,
            attributeName: {
              localPart: 'ref'
            },
            type: 'attribute'
          }, {
            name: 'effectiveDate',
            required: true,
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'effectiveDate'
            },
            type: 'attribute'
          }, {
            name: 'elementId',
            attributeName: {
              localPart: 'elementId'
            },
            type: 'attribute'
          }, {
            name: 'elementPath',
            attributeName: {
              localPart: 'elementPath'
            },
            type: 'attribute'
          }, {
            name: 'url',
            attributeName: {
              localPart: 'url'
            },
            type: 'attribute'
          }, {
            name: 'ident',
            attributeName: {
              localPart: 'ident'
            },
            type: 'attribute'
          }, {
            name: 'referencedFrom',
            attributeName: {
              localPart: 'referencedFrom'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'InheritDefinition',
        propertyInfos: [{
            name: 'ref',
            required: true,
            attributeName: {
              localPart: 'ref'
            },
            type: 'attribute'
          }, {
            name: 'effectiveDate',
            required: true,
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'effectiveDate'
            },
            type: 'attribute'
          }, {
            name: 'prefix',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'prefix'
            },
            type: 'attribute'
          }, {
            name: 'datasetId',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'datasetId'
            },
            type: 'attribute'
          }, {
            name: 'datasetEffectiveDate',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'datasetEffectiveDate'
            },
            type: 'attribute'
          }, {
            name: 'datasetExpirationDate',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'datasetExpirationDate'
            },
            type: 'attribute'
          }, {
            name: 'datasetStatusCode',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'datasetStatusCode'
            },
            type: 'attribute'
          }, {
            name: 'datasetVersionLabel',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'datasetVersionLabel'
            },
            type: 'attribute'
          }, {
            name: 'iType',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'iType'
            },
            type: 'attribute'
          }, {
            name: 'iStatusCode',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'iStatusCode'
            },
            type: 'attribute'
          }, {
            name: 'iEffectiveDate',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'iEffectiveDate'
            },
            type: 'attribute'
          }, {
            name: 'iVersionLabel',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'iVersionLabel'
            },
            type: 'attribute'
          }, {
            name: 'iExpirationDate',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'iExpirationDate'
            },
            type: 'attribute'
          }, {
            name: 'originalId',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'originalId'
            },
            type: 'attribute'
          }, {
            name: 'originalStatusCode',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'originalStatusCode'
            },
            type: 'attribute'
          }, {
            name: 'originalEffectiveDate',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'originalEffectiveDate'
            },
            type: 'attribute'
          }, {
            name: 'originalVersionLabel',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'originalVersionLabel'
            },
            type: 'attribute'
          }, {
            name: 'originalExpirationDate',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'originalExpirationDate'
            },
            type: 'attribute'
          }, {
            name: 'originalPrefix',
            typeInfo: 'AnySimpleType',
            attributeName: {
              localPart: 'originalPrefix'
            },
            type: 'attribute'
          }, {
            name: 'refdisplay',
            attributeName: {
              localPart: 'refdisplay'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'IssueAssignment',
        propertyInfos: [{
            name: 'content',
            collection: true,
            allowDom: false,
            elementTypeInfos: [{
                elementName: {
                  localPart: 'desc'
                },
                typeInfo: '.FreeFormMarkupWithLanguage'
              }, {
                elementName: {
                  localPart: 'author'
                },
                typeInfo: '.Author'
              }],
            type: 'elementRefs'
          }, {
            name: 'to',
            required: true,
            typeInfo: 'PositiveInteger',
            attributeName: {
              localPart: 'to'
            },
            type: 'attribute'
          }, {
            name: 'name',
            required: true,
            attributeName: {
              localPart: 'name'
            },
            type: 'attribute'
          }, {
            name: 'effectiveDate',
            required: true,
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'effectiveDate'
            },
            type: 'attribute'
          }, {
            name: 'labels',
            typeInfo: {
              type: 'list',
              baseTypeInfo: 'NMToken'
            },
            attributeName: {
              localPart: 'labels'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'DataSetConcept.Condition',
        typeName: null,
        propertyInfos: [{
            name: 'otherAttributes',
            type: 'anyAttribute'
          }, {
            name: 'any',
            minOccurs: 0,
            collection: true,
            allowTypedObject: false,
            mixed: false,
            type: 'anyElement'
          }]
      }, {
        localName: 'DataSetConcept.Implementation',
        typeName: null,
        propertyInfos: [{
            name: 'otherAttributes',
            type: 'anyAttribute'
          }, {
            name: 'any',
            minOccurs: 0,
            collection: true,
            allowTypedObject: false,
            mixed: false,
            type: 'anyElement'
          }]
      }, {
        localName: 'AuthorityType',
        propertyInfos: [{
            name: 'content',
            collection: true,
            allowDom: false,
            elementName: {
              localPart: 'addrLine'
            },
            typeInfo: '.AddrLine',
            type: 'elementRef'
          }, {
            name: 'id',
            attributeName: {
              localPart: 'id'
            },
            type: 'attribute'
          }, {
            name: 'name',
            required: true,
            attributeName: {
              localPart: 'name'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'TemplateDefinition',
        propertyInfos: [{
            name: 'otherAttributes',
            type: 'anyAttribute'
          }, {
            name: 'desc',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'desc'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'classification',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'classification'
            },
            typeInfo: '.TemplateProperties'
          }, {
            name: 'relationship',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'relationship'
            },
            typeInfo: '.TemplateRelationships'
          }, {
            name: 'purpose',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'purpose'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'copyright',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'copyright'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'context',
            elementName: {
              localPart: 'context'
            },
            typeInfo: '.Context'
          }, {
            name: 'item',
            elementName: {
              localPart: 'item'
            },
            typeInfo: '.Item'
          }, {
            name: 'example',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'example'
            },
            typeInfo: '.Example'
          }, {
            name: 'attributeOrChoiceOrElement',
            minOccurs: 0,
            collection: true,
            elementTypeInfos: [{
                elementName: {
                  localPart: 'attribute'
                },
                typeInfo: '.Attribute'
              }, {
                elementName: {
                  localPart: 'choice'
                },
                typeInfo: '.ChoiceDefinition'
              }, {
                elementName: {
                  localPart: 'element'
                },
                typeInfo: '.RuleDefinition'
              }, {
                elementName: {
                  localPart: 'include'
                },
                typeInfo: '.IncludeDefinition'
              }, {
                elementName: {
                  localPart: 'let'
                },
                typeInfo: '.Let'
              }, {
                elementName: {
                  localPart: 'assert'
                },
                typeInfo: '.Assert'
              }, {
                elementName: {
                  localPart: 'report'
                },
                typeInfo: '.Report'
              }, {
                elementName: {
                  localPart: 'defineVariable'
                },
                typeInfo: '.DefineVariable'
              }, {
                elementName: {
                  localPart: 'constraint'
                },
                typeInfo: '.FreeFormMarkupWithLanguage'
              }],
            type: 'elements'
          }, {
            name: 'id',
            attributeName: {
              localPart: 'id'
            },
            type: 'attribute'
          }, {
            name: 'canonicalUri',
            attributeName: {
              localPart: 'canonicalUri'
            },
            type: 'attribute'
          }, {
            name: 'ref',
            attributeName: {
              localPart: 'ref'
            },
            type: 'attribute'
          }, {
            name: 'name',
            required: true,
            typeInfo: 'NMToken',
            attributeName: {
              localPart: 'name'
            },
            type: 'attribute'
          }, {
            name: 'statusCode',
            typeInfo: '.TemplateStatusCodeLifeCycle',
            attributeName: {
              localPart: 'statusCode'
            },
            type: 'attribute'
          }, {
            name: 'displayName',
            attributeName: {
              localPart: 'displayName'
            },
            type: 'attribute'
          }, {
            name: 'isClosed',
            typeInfo: 'Boolean',
            attributeName: {
              localPart: 'isClosed'
            },
            type: 'attribute'
          }, {
            name: 'effectiveDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'effectiveDate'
            },
            type: 'attribute'
          }, {
            name: 'expirationDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'expirationDate'
            },
            type: 'attribute'
          }, {
            name: 'officialReleaseDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'officialReleaseDate'
            },
            type: 'attribute'
          }, {
            name: 'versionLabel',
            attributeName: {
              localPart: 'versionLabel'
            },
            type: 'attribute'
          }, {
            name: 'url',
            attributeName: {
              localPart: 'url'
            },
            type: 'attribute'
          }, {
            name: 'ident',
            attributeName: {
              localPart: 'ident'
            },
            type: 'attribute'
          }, {
            name: 'referencedFrom',
            attributeName: {
              localPart: 'referencedFrom'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Designation',
        typeName: null,
        baseTypeInfo: '.FreeFormMarkupWithLanguageCode',
        propertyInfos: [{
            name: 'type',
            typeInfo: '.DesignationType',
            attributeName: {
              localPart: 'type'
            },
            type: 'attribute'
          }, {
            name: 'displayName',
            required: true,
            attributeName: {
              localPart: 'displayName'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'Property',
        typeName: null,
        propertyInfos: [{
            name: 'unit',
            attributeName: {
              localPart: 'unit'
            },
            type: 'attribute'
          }, {
            name: 'currency',
            attributeName: {
              localPart: 'currency'
            },
            type: 'attribute'
          }, {
            name: 'minInclude',
            attributeName: {
              localPart: 'minInclude'
            },
            type: 'attribute'
          }, {
            name: 'maxInclude',
            attributeName: {
              localPart: 'maxInclude'
            },
            type: 'attribute'
          }, {
            name: 'fractionDigits',
            attributeName: {
              localPart: 'fractionDigits'
            },
            type: 'attribute'
          }, {
            name: 'minLength',
            typeInfo: 'NonNegativeInteger',
            attributeName: {
              localPart: 'minLength'
            },
            type: 'attribute'
          }, {
            name: 'maxLength',
            typeInfo: 'NonNegativeInteger',
            attributeName: {
              localPart: 'maxLength'
            },
            type: 'attribute'
          }, {
            name: 'value',
            attributeName: {
              localPart: 'value'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'ScenarioTemplateConcept',
        propertyInfos: [{
            name: 'context',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'context'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'condition',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'condition'
            },
            typeInfo: '.ScenarioTemplateCondition'
          }, {
            name: 'terminologyAssociation',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'terminologyAssociation'
            },
            typeInfo: '.TerminologyAssociation'
          }, {
            name: 'identifierAssociation',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'identifierAssociation'
            },
            typeInfo: '.IdentifierAssociation'
          }, {
            name: 'ref',
            required: true,
            attributeName: {
              localPart: 'ref'
            },
            type: 'attribute'
          }, {
            name: 'flexibility',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'flexibility'
            },
            type: 'attribute'
          }, {
            name: 'conformance',
            typeInfo: '.ConformanceType',
            attributeName: {
              localPart: 'conformance'
            },
            type: 'attribute'
          }, {
            name: 'isMandatory',
            typeInfo: 'Boolean',
            attributeName: {
              localPart: 'isMandatory'
            },
            type: 'attribute'
          }, {
            name: 'minimumMultiplicity',
            typeInfo: 'NonNegativeInteger',
            attributeName: {
              localPart: 'minimumMultiplicity'
            },
            type: 'attribute'
          }, {
            name: 'maximumMultiplicity',
            attributeName: {
              localPart: 'maximumMultiplicity'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'DataSetConcept.ValueSet',
        typeName: null,
        propertyInfos: [{
            name: 'otherAttributes',
            type: 'anyAttribute'
          }, {
            name: 'any',
            minOccurs: 0,
            collection: true,
            allowTypedObject: false,
            mixed: false,
            type: 'anyElement'
          }]
      }, {
        localName: 'IssueTracking',
        propertyInfos: [{
            name: 'author',
            required: true,
            collection: true,
            elementName: {
              localPart: 'author'
            },
            typeInfo: '.Author'
          }, {
            name: 'desc',
            required: true,
            collection: true,
            elementName: {
              localPart: 'desc'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'effectiveDate',
            required: true,
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'effectiveDate'
            },
            type: 'attribute'
          }, {
            name: 'statusCode',
            required: true,
            typeInfo: '.IssueStatusCodeLifeCycle',
            attributeName: {
              localPart: 'statusCode'
            },
            type: 'attribute'
          }, {
            name: 'labels',
            typeInfo: {
              type: 'list',
              baseTypeInfo: 'NMToken'
            },
            attributeName: {
              localPart: 'labels'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'DataSetConceptListConcept',
        propertyInfos: [{
            name: 'name',
            required: true,
            collection: true,
            elementName: {
              localPart: 'name'
            },
            typeInfo: '.BusinessNameWithLanguage'
          }, {
            name: 'synonym',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'synonym'
            },
            typeInfo: '.BusinessNameWithLanguage'
          }, {
            name: 'desc',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'desc'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'id',
            attributeName: {
              localPart: 'id'
            },
            type: 'attribute'
          }, {
            name: 'exception',
            typeInfo: 'Boolean',
            attributeName: {
              localPart: 'exception'
            },
            type: 'attribute'
          }, {
            name: 'level',
            typeInfo: 'Integer',
            attributeName: {
              localPart: 'level'
            },
            type: 'attribute'
          }, {
            name: 'type',
            typeInfo: '.VocabType',
            attributeName: {
              localPart: 'type'
            },
            type: 'attribute'
          }, {
            name: 'ordinal',
            attributeName: {
              localPart: 'ordinal'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'ObjectRelationships',
        propertyInfos: [{
            name: 'type',
            required: true,
            typeInfo: '.RelationshipTypes',
            attributeName: {
              localPart: 'type'
            },
            type: 'attribute'
          }, {
            name: 'ref',
            required: true,
            attributeName: {
              localPart: 'ref'
            },
            type: 'attribute'
          }, {
            name: 'flexibility',
            attributeName: {
              localPart: 'flexibility'
            },
            type: 'attribute'
          }, {
            name: 'refdisplay',
            attributeName: {
              localPart: 'refdisplay'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'StructureDefinition',
        propertyInfos: [{
            name: 'concept',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'concept'
            },
            typeInfo: '.TemplateAssociationConcept'
          }, {
            name: 'publicationUrl',
            required: true,
            attributeName: {
              localPart: 'publicationUrl'
            },
            type: 'attribute'
          }, {
            name: 'canonicalUri',
            required: true,
            attributeName: {
              localPart: 'canonicalUri'
            },
            type: 'attribute'
          }, {
            name: 'id',
            required: true,
            attributeName: {
              localPart: 'id'
            },
            type: 'attribute'
          }, {
            name: 'strucdefId',
            attributeName: {
              localPart: 'strucdefId'
            },
            type: 'attribute'
          }, {
            name: 'strucdefVersion',
            attributeName: {
              localPart: 'strucdefVersion'
            },
            type: 'attribute'
          }, {
            name: 'displayName',
            required: true,
            attributeName: {
              localPart: 'displayName'
            },
            type: 'attribute'
          }, {
            name: 'referencedFrom',
            required: true,
            attributeName: {
              localPart: 'referencedFrom'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'SourceOrTargetCodeSystem',
        propertyInfos: [{
            name: 'codeSystem',
            required: true,
            attributeName: {
              localPart: 'codeSystem'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'RestURI',
        typeName: null,
        propertyInfos: [{
            name: 'content',
            type: 'value'
          }, {
            name: '_for',
            required: true,
            attributeName: {
              localPart: 'for'
            },
            type: 'attribute'
          }, {
            name: 'format',
            required: true,
            attributeName: {
              localPart: 'format'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'ChoiceDefinition',
        propertyInfos: [{
            name: 'desc',
            minOccurs: 0,
            collection: true,
            elementName: {
              localPart: 'desc'
            },
            typeInfo: '.FreeFormMarkupWithLanguage'
          }, {
            name: 'item',
            elementName: {
              localPart: 'item'
            },
            typeInfo: '.Item'
          }, {
            name: 'includeOrElementOrConstraint',
            minOccurs: 0,
            collection: true,
            elementTypeInfos: [{
                elementName: {
                  localPart: 'include'
                },
                typeInfo: '.IncludeDefinition'
              }, {
                elementName: {
                  localPart: 'element'
                },
                typeInfo: '.RuleDefinition'
              }, {
                elementName: {
                  localPart: 'constraint'
                },
                typeInfo: '.FreeFormMarkupWithLanguage'
              }],
            type: 'elements'
          }, {
            name: 'minimumMultiplicity',
            typeInfo: 'NonNegativeInteger',
            attributeName: {
              localPart: 'minimumMultiplicity'
            },
            type: 'attribute'
          }, {
            name: 'maximumMultiplicity',
            attributeName: {
              localPart: 'maximumMultiplicity'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'FreeFormMarkupWithLanguage',
        baseTypeInfo: '.VariousMixedContent',
        propertyInfos: [{
            name: 'language',
            attributeName: {
              localPart: 'language'
            },
            type: 'attribute'
          }, {
            name: 'lastTranslated',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'lastTranslated'
            },
            type: 'attribute'
          }, {
            name: 'mimeType',
            attributeName: {
              localPart: 'mimeType'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'VarCode',
        propertyInfos: [{
            name: 'code',
            required: true,
            attributeName: {
              localPart: 'code'
            },
            type: 'attribute'
          }, {
            name: 'codeSystem',
            required: true,
            attributeName: {
              localPart: 'codeSystem'
            },
            type: 'attribute'
          }]
      }, {
        localName: 'IssueObject',
        propertyInfos: [{
            name: 'id',
            attributeName: {
              localPart: 'id'
            },
            type: 'attribute'
          }, {
            name: 'name',
            attributeName: {
              localPart: 'name'
            },
            type: 'attribute'
          }, {
            name: 'type',
            required: true,
            typeInfo: '.IssueObjectType',
            attributeName: {
              localPart: 'type'
            },
            type: 'attribute'
          }, {
            name: 'effectiveDate',
            typeInfo: 'DateTime',
            attributeName: {
              localPart: 'effectiveDate'
            },
            type: 'attribute'
          }]
      }, {
        type: 'enumInfo',
        localName: 'ProcessCode',
        baseTypeInfo: 'NMToken',
        values: ['strict', 'lax']
      }, {
        type: 'enumInfo',
        localName: 'CopyrightType',
        baseTypeInfo: 'NMToken',
        values: ['author', 'contributor', 'reviewer']
      }, {
        type: 'enumInfo',
        localName: 'TemplateTypes',
        baseTypeInfo: 'NMToken',
        values: ['cdadocumentlevel', 'messagelevel', 'cdaheaderlevel', 'cdasectionlevel', 'cdaentrylevel', 'segmentlevel', 'clinicalstatementlevel', 'controlactlevel', 'payloadlevel', 'datatypelevel', 'notype']
      }, {
        type: 'enumInfo',
        localName: 'ScenarioActorType',
        baseTypeInfo: 'NMToken',
        values: ['person', 'organization', 'device']
      }, {
        type: 'enumInfo',
        localName: 'IssuePriority',
        baseTypeInfo: 'NMToken',
        values: ['HH', 'H', 'N', 'L', 'LL']
      }, {
        type: 'enumInfo',
        localName: 'NullFlavorPattern',
        baseTypeInfo: 'NMToken',
        values: ['nullFlavor']
      }, {
        type: 'enumInfo',
        localName: 'ReleaseStatusCodeLifeCycle',
        baseTypeInfo: 'NMToken',
        values: ['draft', 'pending', 'active', 'retired', 'cancelled', 'failed']
      }, {
        type: 'enumInfo',
        localName: 'TransactionType',
        values: ['group', 'initial', 'back', 'stationary']
      }, {
        type: 'enumInfo',
        localName: 'ConformanceType',
        baseTypeInfo: 'NMToken',
        values: ['R', 'C', 'NP']
      }, {
        type: 'enumInfo',
        localName: 'AddressLineType',
        values: ['phone', 'fax', 'email', 'uri', 'twitter', 'linkedin', 'facebook']
      }, {
        type: 'enumInfo',
        localName: 'ActorType',
        baseTypeInfo: 'NMToken',
        values: ['sender', 'receiver', 'stationary']
      }, {
        type: 'enumInfo',
        localName: 'VocabType',
        baseTypeInfo: 'NMToken',
        values: ['L', 'A', 'S', 'D']
      }, {
        type: 'enumInfo',
        localName: 'IntensionalOperators',
        baseTypeInfo: 'NMToken',
        values: ['is-a', 'descendent-of', 'is-not-a', 'regex']
      }, {
        type: 'enumInfo',
        localName: 'TemplateStatusCodeLifeCycle',
        baseTypeInfo: 'NMToken',
        values: ['draft', 'active', 'retired', 'rejected', 'cancelled', 'pending', 'review']
      }, {
        type: 'enumInfo',
        localName: 'IssueType',
        baseTypeInfo: 'NMToken',
        values: ['INC', 'RFC', 'FUT', 'CLF']
      }, {
        type: 'enumInfo',
        localName: 'EquivalencyType',
        baseTypeInfo: 'NMToken',
        values: ['relatedto', 'equivalent', 'equal', 'wider', 'subsumes', 'narrower', 'specializes', 'inexact', 'unmatched', 'disjoint']
      }, {
        type: 'enumInfo',
        localName: 'ExampleType',
        baseTypeInfo: 'NMToken',
        values: ['error', 'valid', 'neutral']
      }, {
        type: 'enumInfo',
        localName: 'CodingStrengthType',
        baseTypeInfo: 'NMToken',
        values: ['CNE', 'required', 'CWE', 'extensible', 'preferred', 'example']
      }, {
        type: 'enumInfo',
        localName: 'AffiliateKind',
        baseTypeInfo: 'NMToken',
        values: ['AR', 'AU', 'AT', 'BR', 'CA', 'CL', 'CN', 'HR', 'CZ', 'DK', 'FI', 'FR', 'DE', 'GR', 'IN', 'IE', 'IT', 'JP', 'KR', 'LT', 'MX', 'NL', 'NZ', 'RO', 'SOA', 'ES', 'SE', 'CH', 'TW', 'TR', 'UK', 'US', 'UY', 'UV']
      }, {
        type: 'enumInfo',
        localName: 'DataSetConceptType',
        baseTypeInfo: 'NMToken',
        values: ['group', 'item']
      }, {
        type: 'enumInfo',
        localName: 'AssertRole',
        baseTypeInfo: 'NMToken',
        values: ['fatal', 'error', 'warning', 'info']
      }, {
        type: 'enumInfo',
        localName: 'BuildingBlockRepositoryFormat',
        baseTypeInfo: 'NMToken',
        values: ['decor', 'fhir']
      }, {
        type: 'enumInfo',
        localName: 'IssueObjectType',
        baseTypeInfo: '.DecorObjectType',
        values: ['VS', 'DE', 'TM', 'TR', 'DS', 'SC', 'IS', 'CS', 'MP']
      }, {
        type: 'enumInfo',
        localName: 'DynamicFlexibility',
        values: ['dynamic']
      }, {
        type: 'enumInfo',
        localName: 'RelationshipTypes',
        baseTypeInfo: 'NMToken',
        values: ['REPL', 'SPEC', 'GEN', 'COPY', 'ADAPT', 'EQUIV', 'VERSION', 'BACKWD', 'DRIV']
      }, {
        type: 'enumInfo',
        localName: 'ItemStatusCodeLifeCycle',
        baseTypeInfo: 'NMToken',
        values: ['new', 'draft', 'pending', 'final', 'rejected', 'cancelled', 'deprecated']
      }, {
        type: 'enumInfo',
        localName: 'NotifierOnOff',
        baseTypeInfo: 'NMToken',
        values: ['on', 'off']
      }, {
        type: 'enumInfo',
        localName: 'FhirObjectType',
        baseTypeInfo: 'NMToken',
        values: ['FHIR']
      }, {
        type: 'enumInfo',
        localName: 'DataSetValueType',
        baseTypeInfo: 'NMToken',
        values: ['string', 'code', 'identifier', 'date', 'datetime', 'quantity', 'duration', 'boolean', 'count', 'ordinal', 'text', 'decimal', 'blob', 'complex']
      }, {
        type: 'enumInfo',
        localName: 'TemplateFormats',
        baseTypeInfo: 'NMToken',
        values: ['hl7v3xml1', 'hl7v2.5xml', 'fhirxml', 'vmrxml']
      }, {
        type: 'enumInfo',
        localName: 'DecorObjectFormat',
        baseTypeInfo: 'NMToken',
        values: ['HTML', 'XML', 'JSON', 'CSV', 'SQL', 'SVS']
      }, {
        type: 'enumInfo',
        localName: 'DecorObjectType',
        baseTypeInfo: 'NMToken',
        values: ['DS', 'DE', 'SC', 'TR', 'AC', 'VS', 'IS', 'RL', 'TM', 'CL', 'EL', 'SX', 'TX', 'EX', 'QX', 'CM', 'CS', 'MP']
      }, {
        type: 'enumInfo',
        localName: 'DesignationType',
        baseTypeInfo: 'NMToken',
        values: ['preferred', 'synonym', 'abbreviation', 'fsn']
      }, {
        type: 'enumInfo',
        localName: 'DevelopmentString',
        values: ['development']
      }, {
        type: 'enumInfo',
        localName: 'IssueStatusCodeLifeCycle',
        baseTypeInfo: 'NMToken',
        values: ['new', 'open', 'inprogress', 'feedback', 'closed', 'rejected', 'deferred', 'cancelled']
      }, {
        type: 'enumInfo',
        localName: 'LanguageCodeEnum',
        baseTypeInfo: 'Language',
        values: ['en-US', 'nl-NL', 'de-DE']
      }],
    elementInfos: [{
        typeInfo: '.Issue',
        elementName: {
          localPart: 'issue'
        }
      }, {
        typeInfo: '.Report',
        elementName: {
          localPart: 'report'
        }
      }, {
        typeInfo: '.Property',
        elementName: {
          localPart: 'property'
        }
      }, {
        typeInfo: '.VarUse',
        elementName: {
          localPart: 'use'
        },
        scope: '.DefineVariable'
      }, {
        typeInfo: '.DefaultBaseId',
        elementName: {
          localPart: 'defaultBaseId'
        }
      }, {
        typeInfo: '.BaseId',
        elementName: {
          localPart: 'baseId'
        }
      }, {
        typeInfo: '.CodeSystem',
        elementName: {
          localPart: 'codeSystem'
        }
      }, {
        typeInfo: '.Project',
        elementName: {
          localPart: 'project'
        }
      }, {
        typeInfo: '.RestURI',
        elementName: {
          localPart: 'restURI'
        }
      }, {
        typeInfo: '.Attribute',
        elementName: {
          localPart: 'attribute'
        }
      }, {
        typeInfo: '.Item',
        elementName: {
          localPart: 'item'
        }
      }, {
        typeInfo: '.TerminologyAssociation',
        elementName: {
          localPart: 'terminologyAssociation'
        }
      }, {
        typeInfo: '.Vocabulary',
        elementName: {
          localPart: 'vocabulary'
        }
      }, {
        typeInfo: '.Copyright',
        elementName: {
          localPart: 'copyright'
        }
      }, {
        typeInfo: '.Designation',
        elementName: {
          localPart: 'designation'
        }
      }, {
        typeInfo: '.Pathname',
        elementName: {
          localPart: 'pathname'
        }
      }, {
        typeInfo: '.Transaction',
        elementName: {
          localPart: 'transaction'
        }
      }, {
        typeInfo: '.AddrLine',
        elementName: {
          localPart: 'addrLine'
        }
      }, {
        typeInfo: '.Ids',
        elementName: {
          localPart: 'ids'
        }
      }, {
        typeInfo: '.Author',
        elementName: {
          localPart: 'author'
        }
      }, {
        typeInfo: '.CodedConcept',
        elementName: {
          localPart: 'codedConcept'
        }
      }, {
        typeInfo: '.Scenario',
        elementName: {
          localPart: 'scenario'
        }
      }, {
        typeInfo: '.Id',
        elementName: {
          localPart: 'id'
        }
      }, {
        typeInfo: '.Reference',
        elementName: {
          localPart: 'reference'
        }
      }, {
        typeInfo: '.ConceptAssociation',
        elementName: {
          localPart: 'conceptAssociation'
        }
      }, {
        typeInfo: '.DefineVariable',
        elementName: {
          localPart: 'defineVariable'
        }
      }, {
        typeInfo: '.ValueSet',
        elementName: {
          localPart: 'valueSet'
        }
      }, {
        typeInfo: '.IdentifierAssociation',
        elementName: {
          localPart: 'identifierAssociation'
        }
      }, {
        typeInfo: '.Labels',
        elementName: {
          localPart: 'labels'
        }
      }, {
        typeInfo: '.ValueSetRefOrIntensionalOp',
        elementName: {
          localPart: 'exclude'
        },
        scope: '.ValueSetConceptList'
      }, {
        typeInfo: '.Instances',
        elementName: {
          localPart: 'instances'
        }
      }, {
        typeInfo: '.Rules',
        elementName: {
          localPart: 'rules'
        }
      }, {
        typeInfo: '.TemplateId',
        elementName: {
          localPart: 'templateId'
        }
      }, {
        typeInfo: '.Example',
        elementName: {
          localPart: 'example'
        }
      }, {
        typeInfo: '.RepresentingTemplate',
        elementName: {
          localPart: 'representingTemplate'
        }
      }, {
        typeInfo: '.Decor',
        elementName: {
          localPart: 'decor'
        }
      }, {
        typeInfo: '.Context',
        elementName: {
          localPart: 'context'
        }
      }, {
        typeInfo: '.Issues',
        elementName: {
          localPart: 'issues'
        }
      }, {
        typeInfo: '.ValueSetRefOrIntensionalOp',
        elementName: {
          localPart: 'include'
        },
        scope: '.ValueSetConceptList'
      }, {
        typeInfo: '.Actors',
        elementName: {
          localPart: 'actors'
        }
      }, {
        typeInfo: '.Terminology',
        elementName: {
          localPart: 'terminology'
        }
      }, {
        typeInfo: '.Contact',
        elementName: {
          localPart: 'contact'
        }
      }, {
        typeInfo: '.DefaultElementNamespace',
        elementName: {
          localPart: 'defaultElementNamespace'
        }
      }, {
        typeInfo: '.Let',
        elementName: {
          localPart: 'let'
        }
      }, {
        typeInfo: '.Assert',
        elementName: {
          localPart: 'assert'
        }
      }, {
        typeInfo: '.FreeFormMarkupWithLanguage',
        elementName: {
          localPart: 'desc'
        },
        scope: '.ScenarioTemplateCondition'
      }, {
        typeInfo: '.FreeFormMarkupWithLanguage',
        elementName: {
          localPart: 'desc'
        },
        scope: '.IssueAssignment'
      }, {
        typeInfo: '.Datasets',
        elementName: {
          localPart: 'datasets'
        }
      }, {
        typeInfo: '.Dataset',
        elementName: {
          localPart: 'dataset'
        }
      }, {
        typeInfo: '.BuildingBlockRepository',
        elementName: {
          localPart: 'buildingBlockRepository'
        }
      }, {
        typeInfo: '.Cardinality',
        elementName: {
          localPart: 'cardinality'
        }
      }, {
        typeInfo: '.Scenarios',
        elementName: {
          localPart: 'scenarios'
        }
      }, {
        typeInfo: '.VarCode',
        elementName: {
          localPart: 'code'
        },
        scope: '.DefineVariable'
      }, {
        typeInfo: '.ValueSetConcept',
        elementName: {
          localPart: 'concept'
        },
        scope: '.ValueSetConceptList'
      }]
  };
  return {
    DECOR: DECOR
  };
};
if (typeof define === 'function' && define.amd) {
  define([], DECOR_Module_Factory);
}
else {
  var DECOR_Module = DECOR_Module_Factory();
  if (typeof module !== 'undefined' && module.exports) {
    module.exports.DECOR = DECOR_Module.DECOR;
  }
  else {
    var DECOR = DECOR_Module.DECOR;
  }
}