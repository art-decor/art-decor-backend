<?xml version="1.0" encoding="UTF-8"?>
<!--
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" 
    xmlns:o="urn:schemas-microsoft-com:office:office"
    xmlns:x="urn:schemas-microsoft-com:office:excel"
    xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"
    xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
    xmlns:html="http://www.w3.org/TR/REC-html40"
    xmlns:f="http://hl7.org/fhir"
    xmlns:local="urn:local"
    exclude-result-prefixes="#all" version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Oct 31, 2013</xd:p>
            <xd:p><xd:b>Author:</xd:b> ahenket</xd:p>
            <xd:p>Maps as much data as possible from the HL7 Registry in ISO 13582 Sharing of OIR Registries format.<xd:p>Prerequisite: export the HL7 Registry with all fields (http://www.hl7.org/oid/index.cfm - option "All OIDs" under "OID Excel Reports", use format XML). The export contains descriptions (3 fields) with things like &#233;. Could not find how to replace in xquery, so best to replace those before running this query.</xd:p></xd:p>
            <xd:p>The current mapping is as follows: <xd:ul>
                    <xd:li>Retrieve only OIDs with pattern [^[\d\.]+$]: This means we're skipping '0160/01/03 12:00:00', '2001/02/16 12:00:00', '655F67B1-2B11-4038-B82F-F6AB2F566F87', 'http://www.oid-info.com/get/2.16.840.1.113883.3.20'</xd:li>
                    <xd:li>Retrieve only OIDs with lower-case(assignment_status)=('complete','retired','deprecated','obsolete','proposed','pending')<br/> This means we're skipping status 'unknown', 'edited', 'rejected'</xd:li>
                    <xd:li>OID creation date is Entry_Timestamp, Date_begun, Date_finalized or nullFlavor NI</xd:li>
                    <xd:li>Registration authority is fixed to HL7 International</xd:li>
                </xd:ul></xd:p>
            <xd:pre>Is mapped?      HL7                         Remark
y               Comp_OID                    
y               Symbolic_name               adjusted to ISO 13582 format
y               CodingSystemName            
y               SubmitterFirst              
y               SubmitterLast               
y               Submitter_Email             
y               Submitter2                  Additional property
y               Contact_person_desc         
y               Contact_person_address      
y               Contact_person_phone        
y               Contact_person_email        
y               Contact_person_info         
y               Resp_body_name              
y               Resp_body_address           
y               Resp_body_phone             
y               Resp_body_email             
y               Resp_body_URL               
y               Resp_body_Type              
y               Resp_body_oid               
y               External_OID_flag           Additional property
y               externalOIDsubType          Additional property
y               replacedBy                  Additional property
y               Oid_Type                    Additional property and used to value category/@code
y               assignment_status           
y               AA_OID                      Additional property
y               AA_description              Additional property
y               Object_description          
y               Date_begun                  
y               Date_finalized              
y               Entry_Timestamp             
y               T396mnemonic                Additional property
y               Preferred_Realm             </xd:pre>
        </xd:desc>
    </xd:doc>
    
    <xsl:output indent="yes" omit-xml-declaration="yes"/>
    
    <xsl:param name="inputDate" select="'2004-03-08T12:13:48'" as="xs:string"/>
    <xsl:param name="registryFileName" select="'hl7nl-oids.xml'" as="xs:string"/>
    <xsl:param name="oidUriMapping" select="'uri-2-oid.xml'" as="xs:string"/>
    
    <xsl:variable name="oidRegistryDate" select="$inputDate"/>
    
    <xsl:variable name="oid2fhirPropertyNamePreferred">HL7-FHIR-System-URI-Preferred</xsl:variable>
    <xsl:variable name="oid2fhirPropertyName">HL7-FHIR-System-URI</xsl:variable>
    
    <xsl:variable name="docuri" as="element(map)*">
        <xsl:choose>
            <xsl:when test="doc-available($oidUriMapping)">
                <xsl:copy-of select="doc($oidUriMapping)//map"/>
            </xsl:when>
        </xsl:choose>
    </xsl:variable>
    <!--OID-->
    <xsl:variable name="oidText">OID</xsl:variable>
    <!--Coderingssysteem/identificatiesysteem-->
    <xsl:variable name="csidText">Coderingssysteem/identificatiesysteem</xsl:variable>
    <!--Description-->
    <xsl:variable name="descText">Description</xsl:variable>
    <!--Type-->
    <xsl:variable name="typeText">Type</xsl:variable>
    <!--Mnemonic-->
    <xsl:variable name="mnemText">Mnemonic</xsl:variable>
    <!--Status-->
    <xsl:variable name="statusText">Status</xsl:variable>
    <!--Assigning authority-->
    <xsl:variable name="aaText">Assigning authority</xsl:variable>
    <!--Systeemverantwoordelijke-->
    <xsl:variable name="respText">Systeemverantwoordelijke</xsl:variable>
    <!--URL-->
    <xsl:variable name="urlText">URL</xsl:variable>
    <!--Aanvrager-->
    <!--<xsl:variable name="reqText">Aanvrager</xsl:variable>-->
    <!--Contactpersoon-->
    <!--<xsl:variable name="contactText">Contactpersoon</xsl:variable>-->
    <!--Telefoonnummer-->
    <!--<xsl:variable name="phoneText">Telefoonnummer</xsl:variable>-->
    <!--E-mail-->
    <!--<xsl:variable name="emailText">E-mail</xsl:variable>-->
    <!--Postadres-->
    <!--<xsl:variable name="postalText">Postadres</xsl:variable>-->
    <!--Informatie-->
    <!--<xsl:variable name="infoText">Informatie</xsl:variable>-->
    <!--Acties-->
    <!--<xsl:variable name="actionText">Acties</xsl:variable>-->
    <!--Uitgifte-->
    <xsl:variable name="issueDateText">Uitgifte</xsl:variable>
    <!--Door-->
    <!--<xsl:variable name="issuerText">Door</xsl:variable>-->
    
    <xd:doc>
        <xd:desc></xd:desc>
    </xd:doc>
    <xsl:template match="/">
        <xsl:call-template name="check"/>
        <xsl:result-document href="{$registryFileName}" omit-xml-declaration="yes" indent="yes">
            <xsl:processing-instruction name="xml-model">href="../../tools/oids/core/DECORmyoidregistry.xsd" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"</xsl:processing-instruction>
            <myoidregistry xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" name="hl7nl" xsi:noNamespaceSchemaLocation="../../tools/oids/core/DECORmyoidregistry.xsd">
                <access>
                    <author username="kai" rights="rw"/>
                    <author username="maarten" rights="rw"/>
                    <author username="alexander" rights="rw"/>
                </access>
                <scopedAdditionalProperties>
                    <scopedAdditionalProperty code="Oid_Type">
                        <name language="en-US">This is the type of OID</name>
                        <valueDomain type="code">
                            <conceptList>
                                <concept code="1">
                                    <name language="en-US">OID for an HL7 Internal Object</name>
                                </concept>
                                <concept code="2">
                                    <name language="en-US">OID for an HL7 Body or Group</name>
                                </concept>
                                <concept code="3">
                                    <name language="en-US">Root to be a Registration Authority</name>
                                </concept>
                                <concept code="4">
                                    <name language="en-US">OID for a Registered Namespace</name>
                                </concept>
                                <concept code="5">
                                    <name language="en-US">OID for an HL7 Internal Code System</name>
                                </concept>
                                <concept code="6">
                                    <name language="en-US">OID for an External Code System</name>
                                </concept>
                                <concept code="7">
                                    <name language="en-US">OID for an HL7 Document</name>
                                </concept>
                                <concept code="8">
                                    <name language="en-US">OID for an HL7 Document Artifact</name>
                                </concept>
                                <concept code="9">
                                    <name language="en-US">OID for an HL7 Conformance Profile</name>
                                </concept>
                                <concept code="10">
                                    <name language="en-US">OID for an HL7 Template</name>
                                </concept>
                                <concept code="11">
                                    <name language="en-US">OID for an HL7 Internal Value Set</name>
                                </concept>
                                <concept code="12">
                                    <name language="en-US">OID for an Version 2.x Table</name>
                                </concept>
                                <concept code="13">
                                    <name language="en-US">OID for an External Value Set</name>
                                </concept>
                                <concept code="14">
                                    <name language="en-US">branch node subtype</name>
                                </concept>
                                <concept code="15">
                                    <name language="en-US">Defined external codesets</name>
                                </concept>
                                <concept code="17">
                                    <name language="en-US">Other Type OID</name>
                                </concept>
                                <concept code="18">
                                    <name language="en-US">OID for a Version 2.x Coding System</name>
                                </concept>
                                <concept code="19">
                                    <name language="en-US">OID for a published HL7 Example</name>
                                </concept>
                            </conceptList>
                        </valueDomain>
                    </scopedAdditionalProperty>
                    <scopedAdditionalProperty code="{$oid2fhirPropertyNamePreferred}">
                        <name language="en-US">The FHIR System URI. This system is to be the preferred system URI over the OID, or any other FHIR system URIs. Normally there's only 1 system URI, but at max there can be only 1 preferred system URI.</name>
                        <valueDomain type="string"/>
                    </scopedAdditionalProperty>
                    <scopedAdditionalProperty code="{$oid2fhirPropertyName}">
                        <name language="en-US">The FHIR System URI. When multiple FHIR System URIs exist (which should never happen), or if the OID is to be preferred over the FHIR System URI in FHIR communication, this is where that FHIR System URI goes.</name>
                        <valueDomain type="string"/>
                    </scopedAdditionalProperty>
                </scopedAdditionalProperties>
                <registry>
                    <validTime>
                        <!-- 2004-03-08T12:13:48 was found as creation date in the Excel spreadsheet -->
                        <low value="{replace(substring($oidRegistryDate,1,10),'-','')}"/>
                    </validTime>
                    <scopedOID value="2.16.840.1.113883.2.4"/>
                    <name value="The HL7 Netherlands Registry"/>
                    <description language="en-US" mediaType="text/plain" value="This is the HL7 Netherlands registry"/>
                    <description language="nl-NL" mediaType="text/plain" value="Dit is het HL7 Nederland register"/>
                    <person>
                        <name>
                            <part type="GIV" value="Helen"/>
                            <part type="FAM" value="Drijfhout-Wisse"/>
                        </name>
                    </person>
                    <hostingOrganization>
                        <name>
                            <part value="ART-DECOR"/>
                        </name>
                    </hostingOrganization>
                    <xsl:for-each select="/*/row[elem[@name = $oidText][matches(normalize-space(.), '^[\d\.]+$')]]">
                        <xsl:variable name="dotNotation" select="elem[@name = $oidText]/normalize-space()"/>
                        <xsl:variable name="symbolicName" select="local:getSymbolicName(elem[@name = $mnemText]/normalize-space())"/>
                        <xsl:variable name="category" select="local:getCategory(elem[@name = $typeText]/normalize-space())"/>
                        <xsl:variable name="oidType" select="local:getOidType(elem[@name = $typeText]/normalize-space())"/>
                        <xsl:variable name="statusCode" select="local:getStatus(elem[@name = $statusText]/normalize-space())"/>
                        <xsl:variable name="issueDate" select="local:convertDateTimeToTimestamp(elem[@name = $issueDateText]/normalize-space())"/>
                        <xsl:variable name="oidName" select="local:cleanDescription(elem[@name = $csidText]/normalize-space())"/>
                        <xsl:variable name="oidDescription" select="local:cleanDescription(elem[@name = $descText])[not(.='')]"/>
                        <!--<xsl:variable name="registrationName" select="local:cleanDescription(elem[@name = $issuerText])"/>-->
                        <xsl:variable name="registrationAuthority" select="local:cleanDescription(elem[@name = $aaText]/normalize-space())"/>
                        <xsl:variable name="responsibleAuthority" select="local:getResponsibleAuthority(., $dotNotation, $category, $oidName)[1]"/>
                        <xsl:variable name="responsibleAuthorityURL" select="elem[@name = $urlText]"/>
                        
                        <!--<xsl:variable name="contactName" select="local:getFieldFromIndex(.,$contactPos)/normalize-space()"/>
                        <xsl:variable name="contactAddr" select="local:getFieldFromIndex(.,$postalPos)/normalize-space()"/>
                        <xsl:variable name="contactAddressLine1" as="xs:string?">
                            <xsl:choose>
                                <xsl:when test="matches($contactAddr,'^Postbus')">
                                    <xsl:value-of select="replace($contactAddr,'^(Postbus\s+\d+)\s*.*','$1')"/>
                                </xsl:when>
                                <xsl:when test="matches($contactAddr,'\d{4}\s*[A-Z]{2}')">
                                    <xsl:variable name="contactPostalPart" select="replace($contactAddr,'[^,]+(,?\s*\d{4}\s*[A-Z]{2})','$1')"/>
                                    <xsl:value-of select="normalize-space(substring-before($contactAddr,$contactPostalPart))"/>
                                </xsl:when>
                                <xsl:when test="contains($contactAddr,',')">
                                    <xsl:value-of select="normalize-space(tokenize($contactAddr,',')[1])"/>
                                </xsl:when>
                            </xsl:choose>
                        </xsl:variable>
                        <xsl:variable name="contactAddressLine2" select="replace(substring-after($contactAddr,$contactAddressLine1),'^\s*,\s*','')"/>
                        <xsl:variable name="contactPostalCode" as="xs:string?">
                            <xsl:if test="matches($contactAddressLine2,'\d{4}\s*[A-Z]{2}')">
                                <xsl:value-of select="normalize-space(replace($contactAddressLine2,'(\d{4}\s*[A-Z]{2})\s.*','$1'))"/>
                            </xsl:if>
                        </xsl:variable>
                        <xsl:variable name="contactCity" select="normalize-space(substring-after($contactAddressLine2,$contactPostalCode))"/>
                        <xsl:variable name="contactPhone" select="local:getFieldFromIndex(.,$phonePos)/normalize-space()"/>
                        <xsl:variable name="contactEmail" select="local:getFieldFromIndex(.,$emailPos)/normalize-space()"/>-->
                        <oid>
                            <dotNotation value="{$dotNotation}"/>
                            <xsl:if test="string-length($symbolicName)>0">
                                <symbolicName value="{$symbolicName}"/>
                            </xsl:if>
                            <!-- 
                                N   - node
                                NRA - registration authority (RA)
                                NMN - structure for the management of OIDs (it is not good practice but we know that some of these nodes in some registries also identify objects, which should not be)
                                L   - leaf
                                LIO - identifies an instance of an object
                                LNS - a namespace identifier
                            -->
                            <category code="{$category}"/>
                            <status code="{$statusCode}"/>
                            <creationDate>
                                <xsl:choose>
                                    <xsl:when test="string-length($issueDate)>0">
                                        <xsl:attribute name="value" select="$issueDate"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:attribute name="nullFlavor" select="'NI'"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </creationDate>
                            <realm code="NL"/>
                            <description language="en-US" mediaType="text/plain" value="{if ($oidDescription[string-length()>0]) then $oidDescription else $oidName}">
                                <xsl:if test="$oidName[string-length()>0]">
                                    <thumbnail value="{$oidName}"/>
                                </xsl:if>
                            </description>
                            <description language="nl-NL" mediaType="text/plain" value="{if ($oidDescription[string-length()>0]) then $oidDescription else $oidName}">
                                <xsl:if test="$oidName[string-length()>0]">
                                    <thumbnail value="{$oidName}"/>
                                </xsl:if>
                            </description>
                            <registrationAuthority>
                                <code>
                                    <xsl:choose>
                                        <xsl:when test="matches(lower-case($registrationAuthority),'hl7.(nl)|(nld)|(nederland)|(netherlands)')">
                                            <xsl:attribute name="code">PRI</xsl:attribute>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:attribute name="code">OBO</xsl:attribute>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </code>
                                <scopingOrganization>
                                    <name>
                                        <part value="{$registrationAuthority}"/>
                                    </name>
                                </scopingOrganization>
                            </registrationAuthority>
                            <responsibleAuthority>
                                <code code="PRI"/>
                                <statusCode code="active"/>
                                <validTime nullFlavor="NI"/>
                                <scopingOrganization>
                                    <xsl:if test="matches(normalize-space($responsibleAuthority),'^[\d\.]+$')">
                                        <id value="{normalize-space($responsibleAuthority)}"/>
                                    </xsl:if>
                                    <xsl:choose>
                                        <xsl:when test="string-length($responsibleAuthority)>0">
                                            <name>
                                                <part value="{$responsibleAuthority}"/>
                                            </name>
                                        </xsl:when>
                                        <!--<xsl:when test="starts-with($externalFlag,'int')">
                                            <name>
                                                <part value="HL7 International"/>
                                            </name>
                                        </xsl:when>-->
                                        <xsl:otherwise>
                                            <name nullFlavor="NI"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <!--<xsl:if test="string-length(Resp_body_address/normalize-space(text()))>0">
                                        <addr>
                                            <xsl:for-each select="tokenize(Resp_body_address/text(),'\n')[not(normalize-space()=$responsibleAuthority)]">
                                                <part value="{normalize-space(.)}"/>
                                            </xsl:for-each>
                                        </addr>
                                    </xsl:if>-->
                                    <!--<xsl:if test="string-length(Resp_body_phone/normalize-space(text()))>0">
                                        <telecom value="{concat('tel:',replace(Resp_body_phone/normalize-space(text()),' ','%20'))}"/>
                                    </xsl:if>-->
                                    <!--<xsl:if test="string-length(Resp_body_email/normalize-space(text()))>0">
                                        <telecom value="{concat('mailto:',Resp_body_email/normalize-space(text()))}"/>
                                    </xsl:if>-->
                                    <!--<xsl:if test="string-length(Resp_body_URL/normalize-space(text()))>0">
                                        <telecom value="{Resp_body_URL/normalize-space(text())}"/>
                                    </xsl:if>-->
                                </scopingOrganization>
                            </responsibleAuthority>
                            
                            <!--<xsl:if test="$contactName[string-length()>0] or $contactAddr[string-length()>0] or $contactPhone[string-length()>0] or $contactEmail[string-length()>0]">
                                <submittingAuthority>
                                    <code code="PRI"/>
                                    <!-\-<applicationDate value="{$submissionDate}"/>-\->
                                        <person>
                                            <name>
                                                <xsl:choose>
                                                    <xsl:when test="$contactName[string-length()>0]">
                                                        <part value="{$contactName}"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:attribute name="nullFlavor" select="'NI'"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </name>
                                            <xsl:if test="$contactAddr[string-length()>0]">
                                                <addr>
                                                    <xsl:if test="$contactAddressLine1[string-length()>0]">
                                                        <part value="{normalize-space($contactAddressLine1)}" type="ADL"/>
                                                    </xsl:if>
                                                    <xsl:if test="$contactPostalCode[string-length()>0]">
                                                        <part value="{normalize-space($contactPostalCode)}" type="ZIP"/>
                                                    </xsl:if>
                                                    <xsl:if test="$contactCity[string-length()>0]">
                                                        <part value="{normalize-space($contactCity)}" type="CTY"/>
                                                    </xsl:if>
                                                </addr>
                                            </xsl:if>
                                            <xsl:if test="$contactPhone[string-length()>0]">
                                                <telecom value="{concat('tel:',replace($contactPhone,' ',''))}"/>
                                            </xsl:if>
                                            <xsl:for-each select="tokenize($contactEmail,'\r')">
                                                <telecom value="{concat('mailto:',.)}"/>
                                            </xsl:for-each>
                                        </person>
                                    <scopingOrganization>
                                        <name nullFlavor="NI"/>
                                    </scopingOrganization>
                                </submittingAuthority>
                            </xsl:if>-->
                            <additionalProperty>
                                <attribute value="Oid_Type"/>
                                <value value="{$oidType}"/>
                            </additionalProperty>
                            <xsl:for-each-group select="$docuri[@oid = $dotNotation][@uri]" group-by="concat(@oid, @uri)">
                                <additionalProperty>
                                    <attribute value="{if (current-group()[@preferred = 'uri']) then ($oid2fhirPropertyNamePreferred) else ($oid2fhirPropertyName)}"/>
                                    <value value="{current-group()[1]/@uri}"/>
                                </additionalProperty>
                            </xsl:for-each-group>
                        </oid>
                    </xsl:for-each>
                    <xsl:for-each select="/*/row/elem[@name = $oidText][not(matches(normalize-space(), '^[\d\.]+$'))]">
                        <xsl:message>Row with invalid OID not imported: "<xsl:value-of select="."/>"</xsl:message>
                    </xsl:for-each>
                </registry>
            </myoidregistry>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:function name="local:getResponsibleAuthority" as="xs:string*">
        <xsl:param name="in" as="element()*"/>
        <xsl:param name="oid" as="xs:string?"/>
        <xsl:param name="oidCategory" as="xs:string?"/>
        <xsl:param name="oidName" as="xs:string?"/>
        
        <xsl:if test="$in">
            <xsl:variable name="allRows" select="$in/../row" as="element()*"/>
            <xsl:variable name="t" select="$in/elem[@name = $respText]/normalize-space()"/>
            <xsl:choose>
                <xsl:when test="string-length($t) > 0">
                    <xsl:value-of select="$t"/>
                </xsl:when>
                <xsl:when test="$oidCategory = 'NRA' or $oid = '2.16.840.1.113883.2.4'">
                    <xsl:value-of select="$oidName"/>
                </xsl:when>
                <xsl:when test="$oidCategory = 'N'">
                    <xsl:variable name="nextOid" select="string-join(tokenize($oid, '\.')[not(position() = last())], '.')"/>
                    <xsl:variable name="oidRow" select="$allRows[elem[@name = $oidText]/normalize-space() = $nextOid]"/>
                    <xsl:variable name="nextCategory" select="local:getCategory($oidRow/elem[@name = $typeText]/normalize-space())"/>
                    <xsl:variable name="nextName" select="local:cleanDescription($oidRow/elem[@name = $csidText]/normalize-space())"/>
                    
                    <xsl:value-of select="local:getResponsibleAuthority($oidRow, $nextOid, $nextCategory, $nextName)"/>
                </xsl:when>
            </xsl:choose>
        </xsl:if>
    </xsl:function>
    
    <xd:doc>
        <xd:desc/>
        <xd:param name="input"/>
    </xd:doc>
    <xsl:function name="local:getSymbolicName" as="xs:string?">
        <xsl:param name="input" as="xs:string*"/>
        <xsl:variable name="output" select="normalize-space(string-join($input,''))"/>
        <xsl:choose>
            <xsl:when test="$output='ICPC-1-NL'">icpc-1-nl</xsl:when>
            <xsl:when test="$output='NL-Triage-standard'">nl-triage-standard</xsl:when>
            <xsl:when test="$output='IntensiteitVanDeConditie'">intensiteitVanDeConditiel</xsl:when>
            <xsl:when test="$output='big--nr'">big-nr</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="local:cleanSymbolicName($output)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xd:doc>
        <xd:desc>
            <xd:p>Return category</xd:p>
        </xd:desc>
        <xd:param name="type">The HL7 V3 Registry type from the field 'Oid_Type'</xd:param>
        <xd:return>ISO 13582 OID category</xd:return>
    </xd:doc>
    <xsl:function name="local:getCategory" as="xs:string">
        <xsl:param name="type" as="xs:string*"/>
        <xsl:variable name="output" select="normalize-space(string-join($type,''))"/>
        <!-- 
        C	Codesysteem
        I	Identificatiesysteem
        N	Node
        R	Organisatie Root
        O	Overig
        -->
        <xsl:choose>
            <xsl:when test="$output='C'">LNS</xsl:when>
            <xsl:when test="$output='I'">LNS</xsl:when>
            <xsl:when test="$output='N'">N</xsl:when>
            <xsl:when test="$output='R'">NRA</xsl:when>
            <xsl:otherwise>
                <!--  don't know. go to default value  -->
                <xsl:text>LNS</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    <xd:doc>
        <xd:desc/>
        <xd:param name="type"/>
    </xd:doc>
    <xsl:function name="local:getOidType" as="xs:string">
        <xsl:param name="type" as="xs:string*"/>
        <xsl:variable name="output" select="normalize-space(string-join($type,''))"/>
        <!-- 
        C	Codesysteem
        I	Identificatiesysteem
        N	Node
        R	Organisatie Root
        O	Overig
        -->
        <xsl:choose>
            <xsl:when test="$output='C'">codesystem</xsl:when>
            <xsl:when test="$output='I'">identifier</xsl:when>
            <xsl:when test="$output='N'">root</xsl:when>
            <xsl:when test="$output='R'">root</xsl:when>
            <xsl:otherwise>
                <!--  don't know. go to default value  -->
                <xsl:text>root</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xd:doc>
        <xd:desc>
            <xd:p>Return statusCode</xd:p>
        </xd:desc>
        <xd:param name="input">required. HL7 V3 Registry status code</xd:param>
        <xd:return>ISO 13582 status code</xd:return>
    </xd:doc>
    <xsl:function name="local:getStatus" as="xs:string">
        <xsl:param name="input" as="xs:string*"/>
        <xsl:variable name="output" select="normalize-space(string-join($input,''))"/>
        <xsl:variable name="cleanInput" select="lower-case($output)"/>
        <!--
            1	Active (Opnemen in register)
            2	Pending (Wachten met opnemen; voorafgaande actie vereist)
            3	Deprecated (Officieel sterk afgeraden)
            4	Niet opnemen in het register
        -->
        <xsl:choose>
            <xsl:when test="$cleanInput='1'">
                <xsl:text>completed</xsl:text>
            </xsl:when>
            <xsl:when test="$cleanInput='2'">
                <xsl:text>pending</xsl:text>
            </xsl:when>
            <xsl:when test="$cleanInput='3'">
                <xsl:text>retired</xsl:text>
            </xsl:when>
            <xsl:when test="$cleanInput='4'">
                <xsl:text>retired</xsl:text>
            </xsl:when>
            <xsl:when test="$cleanInput='complete'">
                <xsl:text>completed</xsl:text>
            </xsl:when>
            <xsl:when test="$cleanInput='unknown'">
                <xsl:text>unknown</xsl:text>
            </xsl:when>
            <xsl:when test="$cleanInput='retired'">
                <xsl:text>retired</xsl:text>
            </xsl:when>
            <xsl:when test="$cleanInput='deprecated'">
                <xsl:text>retired</xsl:text>
            </xsl:when>
            <xsl:when test="$cleanInput='obsolete'">
                <xsl:text>retired</xsl:text>
            </xsl:when>
            <xsl:when test="$cleanInput='pending'">
                <xsl:text>pending</xsl:text>
            </xsl:when>
            <xsl:when test="$cleanInput='rejected'">
                <xsl:text>unknown</xsl:text>
            </xsl:when>
            <xsl:when test="$cleanInput='proposed'">
                <xsl:text>pending</xsl:text>
            </xsl:when>
            <xsl:when test="$cleanInput='edited'">
                <xsl:text>unknown</xsl:text>
            </xsl:when>
            <!-- FHIR NamingSystem -->
            <!-- draft | active | retired -->
            <xsl:when test="$cleanInput='draft'">
                <xsl:text>pending</xsl:text>
            </xsl:when>
            <xsl:when test="$cleanInput='active'">
                <xsl:text>completed</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>unknown</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xd:doc>
        <xd:desc>
            <xd:p>Returns a valid ISO 21090 formatted timestamp or the empty string</xd:p>
        </xd:desc>
        <xd:param name="input">optional. String in the HL7 Registry with a US date time yyyy/mm/dd hh:mm:ss</xd:param>
        <xd:return>Valid ISO 21090 TS (yyyyMMddHHmmss) or empty string</xd:return>
    </xd:doc>
    <xsl:function name="local:convertUSDateTimeToTimestamp" as="xs:string?">
        <xsl:param name="input" as="xs:string*"/>
        <xsl:variable name="output" select="normalize-space(string-join($input,''))"/>
        <xsl:if test="matches(normalize-space($output),'^\d{4}/\d{2}/\d{2} \d{2}:\d{2}:\d{2}$')">
            <xsl:value-of select="replace(normalize-space($output),'[^\d]','')"/>
        </xsl:if>
    </xsl:function>
    
    <xd:doc>
        <xd:desc>
            <xd:p>Returns a valid ISO 21090 formatted timestamp or the empty string</xd:p>
        </xd:desc>
        <xd:param name="input">optional. String in a FHIR NamingSystem with a dateTime datatype yyyy-mm-ddThh:mm:ss</xd:param>
        <xd:return>Valid ISO 21090 TS (yyyyMMddHHmmss) or empty string</xd:return>
    </xd:doc>
    <xsl:function name="local:convertDateTimeToTimestamp" as="xs:string?">
        <xsl:param name="input" as="xs:string*"/>
        <xsl:variable name="output" select="normalize-space(string-join($input,''))"/>
        <xsl:variable name="d-m-yyyy"   select="replace($output, '^(\d{1})-(\d{1})-(\d{4})$', '$3-0$2-0$1')"/>
        <xsl:variable name="dd-m-yyyy"  select="replace($output, '^(\d{2})-(\d{1})-(\d{4})$', '$3-0$2-$1')"/>
        <xsl:variable name="d-mm-yyyy"  select="replace($output, '^(\d{2})-(\d{1})-(\d{4})$', '$3-$2-0$1')"/>
        <xsl:variable name="dd-mm-yyyy" select="replace($output, '(^\d{2})-(\d{2})-(\d{4})$', '$3-$2-$1')"/>
        
        <xsl:variable name="yyyy-m-d"   select="replace($output, '^(\d{4})-(\d{1})-(\d{1})$', '$3-0$2-0$1')"/>
        <xsl:variable name="yyyy-m-dd"  select="replace($output, '^(\d{4})-(\d{1})-(\d{2})$', '$3-0$2-$1')"/>
        <xsl:variable name="yyyy-mm-d"  select="replace($output, '^(\d{4})-(\d{2})-(\d{1})$', '$3-$2-0$1')"/>
        <xsl:choose>
            <xsl:when test="$output castable as xs:dateTime or $output castable as xs:date">
                <xsl:variable name="date" select="replace(substring(normalize-space($output),1,19),'[^\d]','')"/>
                <xsl:variable name="time" select="replace(substring(normalize-space($output),20),':','')"/>
                <xsl:value-of select="concat($date,$time)"/>
            </xsl:when>
            <xsl:when test="$d-m-yyyy castable as xs:date">
                <xsl:value-of select="replace($d-m-yyyy,'[^\d]','')"/>
            </xsl:when>
            <xsl:when test="$dd-m-yyyy castable as xs:date">
                <xsl:value-of select="replace($dd-m-yyyy,'[^\d]','')"/>
            </xsl:when>
            <xsl:when test="$d-mm-yyyy castable as xs:date">
                <xsl:value-of select="replace($dd-mm-yyyy,'[^\d]','')"/>
            </xsl:when>
            <xsl:when test="$dd-mm-yyyy castable as xs:date">
                <xsl:value-of select="replace($dd-mm-yyyy,'[^\d]','')"/>
            </xsl:when>
            <xsl:when test="$yyyy-m-d castable as xs:date">
                <xsl:value-of select="replace($yyyy-m-d,'[^\d]','')"/>
            </xsl:when>
            <xsl:when test="$yyyy-m-dd castable as xs:date">
                <xsl:value-of select="replace($yyyy-m-dd,'[^\d]','')"/>
            </xsl:when>
            <xsl:when test="$yyyy-m-dd castable as xs:date">
                <xsl:value-of select="replace($yyyy-m-dd,'[^\d]','')"/>
            </xsl:when>
            <xsl:when test="$yyyy-mm-d castable as xs:date">
                <xsl:value-of select="replace($yyyy-mm-d,'[^\d]','')"/>
            </xsl:when>
            <!--<xsl:otherwise>
                <xsl:variable name="date" select="replace(substring(string(current-dateTime()),1,19),'[^\d]','')"/>
                <xsl:variable name="time" select="replace(substring(string(current-dateTime()),20),':','')"/>
                <xsl:value-of select="concat($date,$time)"/>
            </xsl:otherwise>-->
        </xsl:choose>
    </xsl:function>
    
    <xd:doc>
        <xd:desc>
            <xd:p>The HL7 Registry contains symbolic names, but they do not match the pattern for it as layed out in ISO 13582:
                "A symbolic short name, unique among the siblings of the arc of the OID. The ISO rules on Secondary Arc Identifiers, 
                as laid out in Rec. ITU-T | ISO/IEC 9834-1 Procedures for Registration Authorities, section 6.2.2, apply: 
                <xd:ul><xd:li>identifiers of an arc are required to commence with a lowercase letter, and to contain only letters, digits, and hyphens.</xd:li> 
                <xd:li>the last characters shall not be a hyphen - there shall be no two consecutive hyphens in the name"</xd:li></xd:ul></xd:p>
            <xd:p>The function prefixes a lower-case x if the first character is not [a-zA-Z], the function lower-cases the first character
                if it is [A-Z]. The function replaces any character that is not in [a-zA-Z0-9-] with a hyphen ("-") and replaces all double
                hyphens with a singular hyphen. Finally if the string ends in a hyphen, it removes that last character.</xd:p>
            <xd:p>Examples of actual input and output:</xd:p>
            <xd:ul>
                <xd:li>CMET > cMET</xd:li>
                <xd:li>OklahomaDLN > oklahomaDLN</xd:li>
                <xd:li>Service actor type > service-actor-type</xd:li>
                <xd:li>x_ActMoodDefEvn > x-ActMoodDefEvn</xd:li>
                <xd:li>Test of Email Server ---Delete---- > test-of-Email-Server-Delete</xd:li>
            </xd:ul>
        </xd:desc>
        <xd:param name="input">optional. String in the HL7 Inc registry that holds the symbolic name</xd:param>
        <xd:return>Valid symbolic name or an empty string</xd:return>
    </xd:doc>
    <xsl:function name="local:cleanSymbolicName" as="xs:string?">
        <xsl:param name="input" as="xs:string?"/>
        <xsl:variable name="output" select="normalize-space($input)"/>
        
        <!-- first char must be a-z -->
        <xsl:variable name="output-xstart">
            <xsl:if test="string-length($output)>0 and not(matches($output,'^[a-zA-Z]'))">
                <xsl:text>x</xsl:text>
            </xsl:if>
            <xsl:value-of select="$output"/>
        </xsl:variable>
        <!-- first char must be lower case -->
        <xsl:variable name="output-firstchar">
            <xsl:if test="string-length($output-xstart)>0">
                <xsl:value-of select="concat(lower-case(substring($output-xstart,1,1)),substring($output-xstart,2))"/>
            </xsl:if>
        </xsl:variable>
        <!-- replace any character that is not in range [a-zA-Z0-9-] with a hyphen -->
        <xsl:variable name="output-legalchars" select="replace($output-firstchar,'[^a-zA-Z0-9-]','-')"/>
        <!-- replace double hyphens with a singular hyphen, need function -->
        <xsl:variable name="output-singlehyphen" select="local:replaceAll(replace($output-legalchars,'--','-'),'--','-')"/>
        <!-- last char must not be - -->
        <xsl:choose>
            <xsl:when test="ends-with($output-singlehyphen,'-')">
                <xsl:value-of select="substring($output-singlehyphen,1,string-length($output-singlehyphen)-1)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$output-singlehyphen"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xd:doc>
        <xd:desc>
            <xd:p>Recurses until there's positively no more occurence of the search string to replace. The regular
                replace() function will leave occurences if they are nested. Example: replace('---Test---','--','-')
                yields '--Test--'</xd:p>
        </xd:desc>
        <xd:param name="input">optional. String to replace contents in</xd:param>
        <xd:param name="search">required. Search string</xd:param>
        <xd:param name="replace">required. String to replace search string with</xd:param>
        <xd:return>Input string with all occurences of $search replaced with $replace</xd:return>
    </xd:doc>
    <xsl:function name="local:replaceAll" as="xs:string?">
        <xsl:param name="input" as="xs:string?"/>
        <xsl:param name="search" as="xs:string"/>
        <xsl:param name="replace" as="xs:string"/>
        
        <xsl:choose>
            <xsl:when test="contains($input,$search)">
                <xsl:value-of select="local:replaceAll(replace($input,$search,$replace),$search,$replace)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$input"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xd:doc>
        <xd:desc>
            <xd:p>Replaces all occurences of &amp;(quot|lt|gt|apos|amp); with a normal entity, and cleans up a couple of mangled others. These characters appear to be corruption from encoding conversion problems between the entry form and the underlying Registry database</xd:p>
        </xd:desc>
        <xd:param name="input">optional. String to replace contents in</xd:param>
        <xd:return>Input string with all occurences of escaped entities replaced with regular entities</xd:return>
    </xd:doc>
    <xsl:function name="local:cleanDescription" as="xs:string?">
        <xsl:param name="input" as="xs:string*"/>
        <xsl:variable name="output" select="normalize-space(string-join($input,''))"/>
        <xsl:variable name="a1" select="local:replaceAll(local:replaceAll($output,'&amp;amp;quot;','&quot;'),'&amp;quot;','&quot;')"/>
        <xsl:variable name="a2" select="local:replaceAll(local:replaceAll($a1,'&amp;amp;apos;',''''),'&amp;apos;','''')"/>
        <xsl:variable name="a3" select="local:replaceAll(local:replaceAll($a2,'&amp;amp;lt;','&lt;'),'&amp;lt;','&lt;')"/>
        <xsl:variable name="a4" select="local:replaceAll(local:replaceAll($a3,'&amp;amp;gt;','&gt;'),'&amp;gt;','>')"/>
        <xsl:variable name="a5" select="local:replaceAll(local:replaceAll($a4,'&amp;amp;','&amp;'),'&amp;amp;','&amp;')"/>
        
        <!-- Mangled UTF-8 in the original entry form probably, that looked like smart single quotes -->
        <xsl:variable name="a51" select="local:replaceAll($a5,'&amp;#226;&amp;#128;&amp;#153;','''')"/>
        <xsl:variable name="a52" select="local:replaceAll($a51,'&amp;#226;&amp;#128;&amp;#156;','''')"/>
        <xsl:variable name="a53" select="local:replaceAll($a52,'&amp;#226;&amp;#128;','''')"/>
        <!-- Handle any of these UTF-8 constituents in other contexts -->
        <xsl:variable name="a54" select="replace($a53,'&amp;#128;','')"/>
        <xsl:variable name="a55" select="replace($a54,'&amp;#153;','')"/>
        <xsl:variable name="a56" select="replace($a54,'&amp;#156;','')"/>
        <xsl:variable name="a57" select="replace($a55,'&amp;#226;','&#226;')"/>
        
        <xsl:variable name="a6" select="local:replaceAll($a57,'&amp;#146;','''')"/>
        <xsl:variable name="a7" select="local:replaceAll($a6,'&amp;#147;','&quot;')"/>
        <xsl:variable name="a8" select="local:replaceAll($a7,'&amp;#148;','&quot;')"/>
        <xsl:variable name="a9" select="replace($a8,'&amp;#150;','-')"/>
        <xsl:variable name="a10" select="local:replaceAll($a9,'&#732;','''')"/>
        <xsl:variable name="a11" select="replace($a10,'&amp;#8211;','-')"/>
        <xsl:variable name="a12" select="replace($a11,'&amp;#8226;','•')"/>
        <xsl:variable name="a13" select="local:replaceAll($a12,'&#x92;','''')"/>
        
        <xsl:value-of select="$a13"/>
    </xsl:function>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template name="check">
        <!-- count -->
        <!--<xsl:message>Number of OIDs found: <xsl:value-of select="count($oidRegistryItems)"/></xsl:message>-->
        <!--OID-->
        <xsl:message>Column: <xsl:value-of select="$oidText"/></xsl:message>
        <!--Coderingssysteem/identificatiesysteem-->
        <xsl:message>Column: <xsl:value-of select="$csidText"/></xsl:message>
        <!--Description-->
        <xsl:message>Column: <xsl:value-of select="$descText"/></xsl:message>
        <!--Type-->
        <xsl:message>Column: <xsl:value-of select="$typeText"/></xsl:message>
        <!--Mnemonic-->
        <xsl:message>Column: <xsl:value-of select="$mnemText"/></xsl:message>
        <!--Status-->
        <xsl:message>Column: <xsl:value-of select="$statusText"/></xsl:message>
        <!--Assigning authority-->
        <xsl:message>Column: <xsl:value-of select="$aaText"/></xsl:message>
        <!--Systeemverantwoordelijke-->
        <xsl:message>Column: <xsl:value-of select="$respText"/></xsl:message>
        <!--URL-->
        <xsl:message>Column: <xsl:value-of select="$urlText"/></xsl:message>
        <!--Aanvrager-->
        <!--<xsl:message>Column: <xsl:value-of select="$reqPos"/> - <xsl:value-of select="$reqText"/></xsl:message>
        <!-\-Contactpersoon-\->
        <xsl:message>Column: <xsl:value-of select="$contactPos"/> - <xsl:value-of select="$contactText"/></xsl:message>
        <!-\-Telefoonnummer-\->
        <xsl:message>Column: <xsl:value-of select="$phonePos"/> - <xsl:value-of select="$phoneText"/></xsl:message>
        <!-\-E-mail-\->
        <xsl:message>Column: <xsl:value-of select="$emailPos"/> - <xsl:value-of select="$emailText"/></xsl:message>
        <!-\-Postadres-\->
        <xsl:message>Column: <xsl:value-of select="$postalPos"/> - <xsl:value-of select="$postalText"/></xsl:message>
        <!-\-Informatie-\->
        <xsl:message>Column: <xsl:value-of select="$infoPos"/> - <xsl:value-of select="$infoText"/></xsl:message>
        <!-\-Acties-\->
        <xsl:message>Column: <xsl:value-of select="$actionPos"/> - <xsl:value-of select="$actionText"/></xsl:message>-->
        <!--Uitgifte-->
        <xsl:message>Column: <xsl:value-of select="$issueDateText"/></xsl:message>
        <!--Door-->
        <!--<xsl:message>Column: <xsl:value-of select="$issuerPos"/> - <xsl:value-of select="$issuerText"/></xsl:message>-->
    </xsl:template>
</xsl:stylesheet>