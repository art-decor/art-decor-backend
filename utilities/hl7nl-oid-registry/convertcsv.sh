#!/bin/bash

#### CHECK BEFORE RUNNING ####
export saxon=~/Development/lib/SaxonPE9-5-1-10J/saxon9pe.jar
export inputDir=input
export outputDir=input

# must be xs:date
export versionDate="2018-06-18"
# debug
export debugTcl=false

if [ ! -e ${saxon:?"Parameter jarDir not set. Example: export jarPath=/mypath/lib/saxon9/saxon9.jar"} ]; then
    echo "Parameter jarDir does not lead to saxon9.jar."
    exit 1
fi

if [ -e output ]; then
    echo "Removing output dir"
    rm -r output
fi

# Build new project
echo ""
echo "**** CSV conversion dated $versionDate ****"

# Building OID/URI mapping file
case "$1" in
    uri)  echo "Building OID/URI mapping file"; java -cp "${jarPath}" net.sf.saxon.Query -o:"uri-2-oid.xml" ../hl7-oid-registry/create-uri-2-oid.xquery ;;
    *) echo "Not building OID/URI mapping file. Call with parameter 'uri'"
esac

# Using Tcl so we can process line by line, keeping memory footprint to around 5MB and have performance
echo "Building XML from CSV $inputDir …"
tclsh ./convertcsv.tcl -input "$inputDir" -output "$outputDir" -encoding cp1252 -debug "${debugTcl}"

for file in "`ls ${outputDir}/*.xml`"; do
    echo "Converting XML into OID Registry: $file"
    java -jar "${jarPath}" -s:"${file}" -xsl:"create-myregistry-from-hl7nl-csvxml.xsl" oidUriMapping="uri-2-oid.xml"
done