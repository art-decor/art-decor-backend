(:
package net.aegis.fhir.service.formats;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.hl7.fhir.exceptions.FHIRFormatError;
import org.hl7.fhir.instance.formats.JsonParser;
import org.hl7.fhir.instance.formats.XmlParser;
import org.hl7.fhir.instance.model.Resource;

public class TestFHIRFormat {

	/**
	 * @param args
	 * @throws FHIRFormatError
	 */
	public static void main(String[] args) throws IOException, FHIRFormatError {

		try {
			File file = new File("[ReplaceWithBasePath]/alexander/vs-fhir.xml");
			byte fileContent[] = FileUtils.readFileToByteArray(file);

			JsonParser jsonP = new JsonParser();
			XmlParser xmlP = new XmlParser();

			ByteArrayInputStream iResource = new ByteArrayInputStream(fileContent);

			Resource testResource = xmlP.parse(iResource); // Parse into a base Resource object

			String jsonFormat = jsonP.composeString(testResource);

			System.out.println("JSON Format:");
			System.out.println(jsonFormat);

			String xmlFormat = xmlP.composeString(testResource);

			System.out.println("XML Format:");
			System.out.println(xmlFormat);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

}:)

declare namespace jBIS  = "java:java.io.ByteArrayInputStream";
declare namespace jFile  = "java:java.io.File";
declare namespace jException  = "java:java.io.IOException";

declare namespace aFileUtils = "java:org.apache.commons.io.FileUtils";
declare namespace fFormatError = "java:org.hl7.fhir.exceptions.FHIRFormatError";
declare namespace fJsonParser = "java:org.hl7.fhir.instance.formats.JsonParser";
declare namespace fXmlParser = "java:org.hl7.fhir.instance.formats.XmlParser";
declare namespace fModelResource = "java:org.hl7.fhir.instance.model.Resource";

declare namespace f             = "http://hl7.org/fhir";

let $xml            :=
<f:ValueSet xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://hl7.org/fhir ../../fhir/standards/0.4.0-DSTU2-for-comment/fhir-all-xsd/valueset.xsd" xmlns:f="http://hl7.org/fhir">
    <f:id value="v2-0354"/>
    <f:meta>
        <f:versionId value="1"/>
        <f:lastUpdated value="2015-01-19T08:13:24.974-06:00"/>
    </f:meta>
    <f:text>
        <f:status value="additional"/>
        <div xmlns="http://www.w3.org/1999/xhtml">
            <p>Message Structure</p>
            <table class="grid">
                <tr>
                    <td>
                        <b>Code</b>
                    </td>
                    <td>
                        <b>Description</b>
                    </td>
                    <td>
                        <b>Version</b>
                    </td>
                </tr>
                <tr>
                    <td>ACK <a name="ACK"/>
                    </td>
                    <td>Varies</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>ADR_A19 <a name="ADR_A19"/>
                    </td>
                    <td/>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>ADT_A01 <a name="ADT_A01"/>
                    </td>
                    <td>A01, A04, A08, A13</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>ADT_A02 <a name="ADT_A02"/>
                    </td>
                    <td>A02</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>ADT_A03 <a name="ADT_A03"/>
                    </td>
                    <td>A03</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>ADT_A05 <a name="ADT_A05"/>
                    </td>
                    <td>A05, A14, A28, A31</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>ADT_A06 <a name="ADT_A06"/>
                    </td>
                    <td>A06, A07</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>ADT_A09 <a name="ADT_A09"/>
                    </td>
                    <td>A09, A10, A11</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>ADT_A12 <a name="ADT_A12"/>
                    </td>
                    <td>A12</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>ADT_A15 <a name="ADT_A15"/>
                    </td>
                    <td>A15</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>ADT_A16 <a name="ADT_A16"/>
                    </td>
                    <td>A16</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>ADT_A17 <a name="ADT_A17"/>
                    </td>
                    <td>A17</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>ADT_A18 <a name="ADT_A18"/>
                    </td>
                    <td/>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>ADT_A20 <a name="ADT_A20"/>
                    </td>
                    <td>A20</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>ADT_A21 <a name="ADT_A21"/>
                    </td>
                    <td>A21, A22, A23, A25, A26, A27, A29, A32, A33</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>ADT_A24 <a name="ADT_A24"/>
                    </td>
                    <td>A24</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>ADT_A28 <a name="ADT_A28"/>
                    </td>
                    <td>A28, A31</td>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>ADT_A30 <a name="ADT_A30"/>
                    </td>
                    <td/>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>ADT_A37 <a name="ADT_A37"/>
                    </td>
                    <td>A37</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>ADT_A38 <a name="ADT_A38"/>
                    </td>
                    <td>A38</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>ADT_A39 <a name="ADT_A39"/>
                    </td>
                    <td>A39, A40, A41, A42</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>ADT_A43 <a name="ADT_A43"/>
                    </td>
                    <td>A43</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>ADT_A44 <a name="ADT_A44"/>
                    </td>
                    <td>A44</td>
                    <td>added v2.7</td>
                </tr>
                <tr>
                    <td>ADT_A45 <a name="ADT_A45"/>
                    </td>
                    <td>A45</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>ADT_A50 <a name="ADT_A50"/>
                    </td>
                    <td>A50, A51</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>ADT_A52 <a name="ADT_A52"/>
                    </td>
                    <td>A52, A53</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>ADT_A54 <a name="ADT_A54"/>
                    </td>
                    <td>A54, A55</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>ADT_A60 <a name="ADT_A60"/>
                    </td>
                    <td>A60</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>ADT_A61 <a name="ADT_A61"/>
                    </td>
                    <td>A61, A62</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>ARD_A19 <a name="ARD_A19"/>
                    </td>
                    <td>A19</td>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>BAR_P01 <a name="BAR_P01"/>
                    </td>
                    <td>P01</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>BAR_P02 <a name="BAR_P02"/>
                    </td>
                    <td>P02</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>BAR_P05 <a name="BAR_P05"/>
                    </td>
                    <td>P05</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>BAR_P06 <a name="BAR_P06"/>
                    </td>
                    <td>P06</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>BAR_P10 <a name="BAR_P10"/>
                    </td>
                    <td>P10</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>BAR_P12 <a name="BAR_P12"/>
                    </td>
                    <td>P12</td>
                    <td>added v2.5</td>
                </tr>
                <tr>
                    <td>BPS_O29 <a name="BPS_O29"/>
                    </td>
                    <td>O29</td>
                    <td>added v2.5</td>
                </tr>
                <tr>
                    <td>BRP_030 <a name="BRP_030"/>
                    </td>
                    <td>O30</td>
                    <td>added v2.5, removed after v2.5</td>
                </tr>
                <tr>
                    <td>BRP_O30 <a name="BRP_O30"/>
                    </td>
                    <td>O30</td>
                    <td>added v2.5.1</td>
                </tr>
                <tr>
                    <td>BRT_O32 <a name="BRT_O32"/>
                    </td>
                    <td>O32</td>
                    <td>added v2.5</td>
                </tr>
                <tr>
                    <td>BTS_O31 <a name="BTS_O31"/>
                    </td>
                    <td>O31</td>
                    <td>added v2.5</td>
                </tr>
                <tr>
                    <td>CCF_I22 <a name="CCF_I22"/>
                    </td>
                    <td>I22</td>
                    <td>added v2.7</td>
                </tr>
                <tr>
                    <td>CCI_I22 <a name="CCI_I22"/>
                    </td>
                    <td>I22</td>
                    <td>added v2.7</td>
                </tr>
                <tr>
                    <td>CCM_I21 <a name="CCM_I21"/>
                    </td>
                    <td>I21</td>
                    <td>added v2.7</td>
                </tr>
                <tr>
                    <td>CCQ_I19 <a name="CCQ_I19"/>
                    </td>
                    <td>I19</td>
                    <td>added v2.7</td>
                </tr>
                <tr>
                    <td>CCR_I16 <a name="CCR_I16"/>
                    </td>
                    <td>I16, |17, |18</td>
                    <td>added v2.7</td>
                </tr>
                <tr>
                    <td>CCU_I20 <a name="CCU_I20"/>
                    </td>
                    <td>I20</td>
                    <td>added v2.7</td>
                </tr>
                <tr>
                    <td>CQU_I19 <a name="CQU_I19"/>
                    </td>
                    <td>I19</td>
                    <td>added v2.7</td>
                </tr>
                <tr>
                    <td>CRM_C01 <a name="CRM_C01"/>
                    </td>
                    <td>C01, C02, C03, C04, C05, C06, C07, C08</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>CSU_C09 <a name="CSU_C09"/>
                    </td>
                    <td>C09, C10, C11, C12</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>DFT_P03 <a name="DFT_P03"/>
                    </td>
                    <td>P03</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>DFT_P11 <a name="DFT_P11"/>
                    </td>
                    <td>P11</td>
                    <td>added v2.5</td>
                </tr>
                <tr>
                    <td>DOC_T12 <a name="DOC_T12"/>
                    </td>
                    <td/>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>DSR_P04 <a name="DSR_P04"/>
                    </td>
                    <td>P04</td>
                    <td>added v2.4, removed after v2.5.1</td>
                </tr>
                <tr>
                    <td>DSR_Q01 <a name="DSR_Q01"/>
                    </td>
                    <td>Q01</td>
                    <td>added v2.3.1, removed after v2.6</td>
                </tr>
                <tr>
                    <td>DSR_Q03 <a name="DSR_Q03"/>
                    </td>
                    <td>Q03</td>
                    <td>added v2.3.1, removed after v2.6</td>
                </tr>
                <tr>
                    <td>EAC_U07 <a name="EAC_U07"/>
                    </td>
                    <td>U07</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>EAN_U09 <a name="EAN_U09"/>
                    </td>
                    <td>U09</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>EAR_U08 <a name="EAR_U08"/>
                    </td>
                    <td>U08</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>EDR_R07 <a name="EDR_R07"/>
                    </td>
                    <td>R07</td>
                    <td>added v2.3.1, removed after v2.5.1</td>
                </tr>
                <tr>
                    <td>EHC_E01 <a name="EHC_E01"/>
                    </td>
                    <td>E01</td>
                    <td>added v2.6</td>
                </tr>
                <tr>
                    <td>EHC_E02 <a name="EHC_E02"/>
                    </td>
                    <td>E02</td>
                    <td>added v2.6</td>
                </tr>
                <tr>
                    <td>EHC_E04 <a name="EHC_E04"/>
                    </td>
                    <td>E04</td>
                    <td>added v2.6</td>
                </tr>
                <tr>
                    <td>EHC_E10 <a name="EHC_E10"/>
                    </td>
                    <td>E10</td>
                    <td>added v2.6</td>
                </tr>
                <tr>
                    <td>EHC_E12 <a name="EHC_E12"/>
                    </td>
                    <td>E12</td>
                    <td>added v2.6</td>
                </tr>
                <tr>
                    <td>EHC_E13 <a name="EHC_E13"/>
                    </td>
                    <td>E13</td>
                    <td>added v2.6</td>
                </tr>
                <tr>
                    <td>EHC_E15 <a name="EHC_E15"/>
                    </td>
                    <td>E15</td>
                    <td>added v2.6</td>
                </tr>
                <tr>
                    <td>EHC_E20 <a name="EHC_E20"/>
                    </td>
                    <td>E20</td>
                    <td>added v2.6</td>
                </tr>
                <tr>
                    <td>EHC_E21 <a name="EHC_E21"/>
                    </td>
                    <td>E21</td>
                    <td>added v2.6</td>
                </tr>
                <tr>
                    <td>EHC_E24 <a name="EHC_E24"/>
                    </td>
                    <td>E24</td>
                    <td>added v2.6</td>
                </tr>
                <tr>
                    <td>EQQ_Q04 <a name="EQQ_Q04"/>
                    </td>
                    <td>Q04</td>
                    <td>added v2.3.1, removed after v2.5.1</td>
                </tr>
                <tr>
                    <td>ERP_R09 <a name="ERP_R09"/>
                    </td>
                    <td>R09</td>
                    <td>added v2.3.1, removed after v2.5.1</td>
                </tr>
                <tr>
                    <td>ESR_U02 <a name="ESR_U02"/>
                    </td>
                    <td>U02</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>ESU_U01 <a name="ESU_U01"/>
                    </td>
                    <td>U01</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>INR_U06 <a name="INR_U06"/>
                    </td>
                    <td>U06</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>INU_U05 <a name="INU_U05"/>
                    </td>
                    <td>U05</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>LSU_U12 <a name="LSU_U12"/>
                    </td>
                    <td>U12, U13</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>MDM_T01 <a name="MDM_T01"/>
                    </td>
                    <td>T01, T03, T05, T07, T09, T11</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>MDM_T02 <a name="MDM_T02"/>
                    </td>
                    <td>T02, T04, T06, T08, T10</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>MFD_MFA <a name="MFD_MFA"/>
                    </td>
                    <td>MFA</td>
                    <td>added v2.4, removed after v2.5.1</td>
                </tr>
                <tr>
                    <td>MFD_P09 <a name="MFD_P09"/>
                    </td>
                    <td>P09</td>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>MFK_M01 <a name="MFK_M01"/>
                    </td>
                    <td>M01, M02, M03, M04, M05, M06, M07, M08, M09, M10, M11</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>MFN_M01 <a name="MFN_M01"/>
                    </td>
                    <td/>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>MFN_M02 <a name="MFN_M02"/>
                    </td>
                    <td>M02</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>MFN_M03 <a name="MFN_M03"/>
                    </td>
                    <td>M03</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>MFN_M04 <a name="MFN_M04"/>
                    </td>
                    <td>M04</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>MFN_M05 <a name="MFN_M05"/>
                    </td>
                    <td>M05</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>MFN_M06 <a name="MFN_M06"/>
                    </td>
                    <td>M06</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>MFN_M07 <a name="MFN_M07"/>
                    </td>
                    <td>M07</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>MFN_M08 <a name="MFN_M08"/>
                    </td>
                    <td>M08</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>MFN_M09 <a name="MFN_M09"/>
                    </td>
                    <td>M09</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>MFN_M10 <a name="MFN_M10"/>
                    </td>
                    <td>M10</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>MFN_M11 <a name="MFN_M11"/>
                    </td>
                    <td>M11</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>MFN_M12 <a name="MFN_M12"/>
                    </td>
                    <td>M12</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>MFN_M13 <a name="MFN_M13"/>
                    </td>
                    <td>M13</td>
                    <td>added v2.5</td>
                </tr>
                <tr>
                    <td>MFN_M15 <a name="MFN_M15"/>
                    </td>
                    <td>M15</td>
                    <td>added v2.5</td>
                </tr>
                <tr>
                    <td>MFN_M16 <a name="MFN_M16"/>
                    </td>
                    <td>M16</td>
                    <td>added v2.6</td>
                </tr>
                <tr>
                    <td>MFN_M17 <a name="MFN_M17"/>
                    </td>
                    <td>M17</td>
                    <td>added v2.6</td>
                </tr>
                <tr>
                    <td>MFN_Znn <a name="MFN_Znn"/>
                    </td>
                    <td>Znn</td>
                    <td>added v2.7</td>
                </tr>
                <tr>
                    <td>MFQ_M01 <a name="MFQ_M01"/>
                    </td>
                    <td>M01, M02, M03, M04, M05, M06</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>MFR_M01 <a name="MFR_M01"/>
                    </td>
                    <td>M01, M02, M03,</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>MFR_M04 <a name="MFR_M04"/>
                    </td>
                    <td>M04</td>
                    <td>added v2.5.1</td>
                </tr>
                <tr>
                    <td>MFR_M05 <a name="MFR_M05"/>
                    </td>
                    <td>M05</td>
                    <td>added v2.5.1</td>
                </tr>
                <tr>
                    <td>MFR_M06 <a name="MFR_M06"/>
                    </td>
                    <td>M06</td>
                    <td>added v2.5.1</td>
                </tr>
                <tr>
                    <td>MFR_M07 <a name="MFR_M07"/>
                    </td>
                    <td>M07</td>
                    <td>added v2.5.1</td>
                </tr>
                <tr>
                    <td>NMD_N02 <a name="NMD_N02"/>
                    </td>
                    <td>N02</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>NMQ_N01 <a name="NMQ_N01"/>
                    </td>
                    <td>N01</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>NMR_N01 <a name="NMR_N01"/>
                    </td>
                    <td>N01</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>NUL <a name="NUL"/>
                    </td>
                    <td>Null</td>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>OMB_O27 <a name="OMB_O27"/>
                    </td>
                    <td>O27</td>
                    <td>added v2.5</td>
                </tr>
                <tr>
                    <td>OMD_O01 <a name="OMD_O01"/>
                    </td>
                    <td/>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>OMD_O03 <a name="OMD_O03"/>
                    </td>
                    <td>O03</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>OMG_O19 <a name="OMG_O19"/>
                    </td>
                    <td>O19</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>OMI_O23 <a name="OMI_O23"/>
                    </td>
                    <td>O23</td>
                    <td>added v2.5</td>
                </tr>
                <tr>
                    <td>OML_O21 <a name="OML_O21"/>
                    </td>
                    <td>O21</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>OML_O33 <a name="OML_O33"/>
                    </td>
                    <td>O33</td>
                    <td>added v2.5</td>
                </tr>
                <tr>
                    <td>OML_O35 <a name="OML_O35"/>
                    </td>
                    <td>O35</td>
                    <td>added v2.5</td>
                </tr>
                <tr>
                    <td>OML_O39 <a name="OML_O39"/>
                    </td>
                    <td>O39</td>
                    <td>added v2.7</td>
                </tr>
                <tr>
                    <td>OMN_O01 <a name="OMN_O01"/>
                    </td>
                    <td/>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>OMN_O07 <a name="OMN_O07"/>
                    </td>
                    <td>O07</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>OMP_O09 <a name="OMP_O09"/>
                    </td>
                    <td>O09</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>OMS_O01 <a name="OMS_O01"/>
                    </td>
                    <td/>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>OMS_O05 <a name="OMS_O05"/>
                    </td>
                    <td>O05</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>OPL_O37 <a name="OPL_O37"/>
                    </td>
                    <td>O37</td>
                    <td>added v2.6</td>
                </tr>
                <tr>
                    <td>OPR_O38 <a name="OPR_O38"/>
                    </td>
                    <td>O38</td>
                    <td>added v2.6</td>
                </tr>
                <tr>
                    <td>OPU_R25 <a name="OPU_R25"/>
                    </td>
                    <td>R25</td>
                    <td>added v2.6</td>
                </tr>
                <tr>
                    <td>ORA_R33 <a name="ORA_R33"/>
                    </td>
                    <td>R33</td>
                    <td>added v2.7</td>
                </tr>
                <tr>
                    <td>ORB_O28 <a name="ORB_O28"/>
                    </td>
                    <td>O28</td>
                    <td>added v2.5</td>
                </tr>
                <tr>
                    <td>ORD_O02 <a name="ORD_O02"/>
                    </td>
                    <td/>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>ORD_O04 <a name="ORD_O04"/>
                    </td>
                    <td>O04</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>ORF_R02 <a name="ORF_R02"/>
                    </td>
                    <td>R02, R04</td>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>ORF_R04 <a name="ORF_R04"/>
                    </td>
                    <td>R04</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>ORG_O20 <a name="ORG_O20"/>
                    </td>
                    <td>O20</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>ORI_O24 <a name="ORI_O24"/>
                    </td>
                    <td>O24</td>
                    <td>added v2.5</td>
                </tr>
                <tr>
                    <td>ORL_O22 <a name="ORL_O22"/>
                    </td>
                    <td>O22</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>ORL_O34 <a name="ORL_O34"/>
                    </td>
                    <td>O34</td>
                    <td>added v2.5</td>
                </tr>
                <tr>
                    <td>ORL_O36 <a name="ORL_O36"/>
                    </td>
                    <td>O36</td>
                    <td>added v2.5</td>
                </tr>
                <tr>
                    <td>ORL_O40 <a name="ORL_O40"/>
                    </td>
                    <td>O40</td>
                    <td>added v2.7</td>
                </tr>
                <tr>
                    <td>ORM_O01 <a name="ORM_O01"/>
                    </td>
                    <td>O01</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>ORM_Q06 <a name="ORM_Q06"/>
                    </td>
                    <td>Q06</td>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>ORN_O02 <a name="ORN_O02"/>
                    </td>
                    <td/>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>ORN_O08 <a name="ORN_O08"/>
                    </td>
                    <td>O08</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>ORP_O10 <a name="ORP_O10"/>
                    </td>
                    <td>O10</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>ORR_O02 <a name="ORR_O02"/>
                    </td>
                    <td>O02</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>ORR_Q06 <a name="ORR_Q06"/>
                    </td>
                    <td>Q06</td>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>ORS_O02 <a name="ORS_O02"/>
                    </td>
                    <td/>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>ORS_O06 <a name="ORS_O06"/>
                    </td>
                    <td>O06</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>ORU_R01 <a name="ORU_R01"/>
                    </td>
                    <td>R01</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>ORU_R30 <a name="ORU_R30"/>
                    </td>
                    <td>R30</td>
                    <td>added v2.5</td>
                </tr>
                <tr>
                    <td>ORU_R31 <a name="ORU_R31"/>
                    </td>
                    <td>R31</td>
                    <td>added v2.5, removed after v2.5</td>
                </tr>
                <tr>
                    <td>ORU_R32 <a name="ORU_R32"/>
                    </td>
                    <td>R32</td>
                    <td>added v2.5, removed after v2.5</td>
                </tr>
                <tr>
                    <td>ORU_W01 <a name="ORU_W01"/>
                    </td>
                    <td>W01</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>OSM_R26 <a name="OSM_R26"/>
                    </td>
                    <td>R26</td>
                    <td>added v2.7</td>
                </tr>
                <tr>
                    <td>OSQ_Q06 <a name="OSQ_Q06"/>
                    </td>
                    <td>Q06</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>OSR_Q06 <a name="OSR_Q06"/>
                    </td>
                    <td>Q06</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>OUL_R21 <a name="OUL_R21"/>
                    </td>
                    <td>R21</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>OUL_R22 <a name="OUL_R22"/>
                    </td>
                    <td>R22</td>
                    <td>added v2.5</td>
                </tr>
                <tr>
                    <td>OUL_R23 <a name="OUL_R23"/>
                    </td>
                    <td>R23</td>
                    <td>added v2.5</td>
                </tr>
                <tr>
                    <td>OUL_R24 <a name="OUL_R24"/>
                    </td>
                    <td>R24</td>
                    <td>added v2.5</td>
                </tr>
                <tr>
                    <td>PEX_P07 <a name="PEX_P07"/>
                    </td>
                    <td>P07, P08</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>PGL_PC6 <a name="PGL_PC6"/>
                    </td>
                    <td>PC6, PC7, PC8</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>PIN_I07 <a name="PIN_I07"/>
                    </td>
                    <td>I07</td>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>PMU_B01 <a name="PMU_B01"/>
                    </td>
                    <td>B01, B02</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>PMU_B03 <a name="PMU_B03"/>
                    </td>
                    <td>B03</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>PMU_B04 <a name="PMU_B04"/>
                    </td>
                    <td>B04, B05, B06</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>PMU_B07 <a name="PMU_B07"/>
                    </td>
                    <td>B07</td>
                    <td>added v2.5</td>
                </tr>
                <tr>
                    <td>PMU_B08 <a name="PMU_B08"/>
                    </td>
                    <td>B08</td>
                    <td>added v2.5</td>
                </tr>
                <tr>
                    <td>PPG_PCG <a name="PPG_PCG"/>
                    </td>
                    <td>PCC, PCG, PCH, PCJ</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>PPP_PCB <a name="PPP_PCB"/>
                    </td>
                    <td>PCB, PCD</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>PPR_PC1 <a name="PPR_PC1"/>
                    </td>
                    <td>PC1, PC2, PC3</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>PPT_PCL <a name="PPT_PCL"/>
                    </td>
                    <td>PCL</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>PPV_PCA <a name="PPV_PCA"/>
                    </td>
                    <td>PCA</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>PRR_PC5 <a name="PRR_PC5"/>
                    </td>
                    <td>PC5</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>PTR_PCF <a name="PTR_PCF"/>
                    </td>
                    <td>PCF</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>QBP_E03 <a name="QBP_E03"/>
                    </td>
                    <td>E03</td>
                    <td>added v2.6</td>
                </tr>
                <tr>
                    <td>QBP_E22 <a name="QBP_E22"/>
                    </td>
                    <td>E22</td>
                    <td>added v2.6</td>
                </tr>
                <tr>
                    <td>QBP_Q11 <a name="QBP_Q11"/>
                    </td>
                    <td>Q11</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>QBP_Q13 <a name="QBP_Q13"/>
                    </td>
                    <td>Q13</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>QBP_Q15 <a name="QBP_Q15"/>
                    </td>
                    <td>Q15</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>QBP_Q21 <a name="QBP_Q21"/>
                    </td>
                    <td>Q21, Q22, Q23,Q24, Q25</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>QBP_Qnn <a name="QBP_Qnn"/>
                    </td>
                    <td>Qnn</td>
                    <td>added v2.7</td>
                </tr>
                <tr>
                    <td>QBP_Z73 <a name="QBP_Z73"/>
                    </td>
                    <td>Z73</td>
                    <td>added v2.7</td>
                </tr>
                <tr>
                    <td>QCK_Q02 <a name="QCK_Q02"/>
                    </td>
                    <td>Q02</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>QCN_J01 <a name="QCN_J01"/>
                    </td>
                    <td>J01, J02</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>QRF_W02 <a name="QRF_W02"/>
                    </td>
                    <td>W02</td>
                    <td>added v2.5</td>
                </tr>
                <tr>
                    <td>QRY_A19 <a name="QRY_A19"/>
                    </td>
                    <td>A19</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>QRY_P04 <a name="QRY_P04"/>
                    </td>
                    <td>P04</td>
                    <td>added v2.5, removed after v2.5.1</td>
                </tr>
                <tr>
                    <td>QRY_PC4 <a name="QRY_PC4"/>
                    </td>
                    <td>PC4, PC9, PCE, PCK</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>QRY_Q01 <a name="QRY_Q01"/>
                    </td>
                    <td>Q01, Q26, Q27, Q28, Q29, Q30</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>QRY_Q02 <a name="QRY_Q02"/>
                    </td>
                    <td>Q02</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>QRY_R02 <a name="QRY_R02"/>
                    </td>
                    <td>R02</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>QRY_T12 <a name="QRY_T12"/>
                    </td>
                    <td>T12</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>QSB_Q16 <a name="QSB_Q16"/>
                    </td>
                    <td>Q16</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>QVR_Q17 <a name="QVR_Q17"/>
                    </td>
                    <td>Q17</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>RAR_RAR <a name="RAR_RAR"/>
                    </td>
                    <td>RAR</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>RAS_O01 <a name="RAS_O01"/>
                    </td>
                    <td>O01</td>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>RAS_O02 <a name="RAS_O02"/>
                    </td>
                    <td>O022</td>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>RAS_O17 <a name="RAS_O17"/>
                    </td>
                    <td>O17</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>RCI_I05 <a name="RCI_I05"/>
                    </td>
                    <td>I05</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>RCL_I06 <a name="RCL_I06"/>
                    </td>
                    <td>I06</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>RDE_O01 <a name="RDE_O01"/>
                    </td>
                    <td>O01</td>
                    <td>added v2.3.1, removed after v2.5</td>
                </tr>
                <tr>
                    <td>RDE_O11 <a name="RDE_O11"/>
                    </td>
                    <td>O11, O25</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>RDO_O01 <a name="RDO_O01"/>
                    </td>
                    <td/>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>RDR_RDR <a name="RDR_RDR"/>
                    </td>
                    <td>RDR</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>RDS_O01 <a name="RDS_O01"/>
                    </td>
                    <td>O01</td>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>RDS_O13 <a name="RDS_O13"/>
                    </td>
                    <td>O13</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>RDY_K11 <a name="RDY_K11"/>
                    </td>
                    <td>K11</td>
                    <td>added v2.4, removed after v2.4</td>
                </tr>
                <tr>
                    <td>RDY_K15 <a name="RDY_K15"/>
                    </td>
                    <td>K15</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>REF_I12 <a name="REF_I12"/>
                    </td>
                    <td>I12, I13, I14, I15</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>RER_RER <a name="RER_RER"/>
                    </td>
                    <td>RER</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>RGR_RGR <a name="RGR_RGR"/>
                    </td>
                    <td>RGR</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>RGV_O01 <a name="RGV_O01"/>
                    </td>
                    <td>O01</td>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>RGV_O15 <a name="RGV_O15"/>
                    </td>
                    <td>O15</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>ROR_ROR <a name="ROR_ROR"/>
                    </td>
                    <td>ROR</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>RPA_I08 <a name="RPA_I08"/>
                    </td>
                    <td>I08, I09. I10, I11</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>RPI_I01 <a name="RPI_I01"/>
                    </td>
                    <td>I01, I04</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>RPI_I04 <a name="RPI_I04"/>
                    </td>
                    <td>I04</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>RPI_I0I <a name="RPI_I0I"/>
                    </td>
                    <td>I01, I04</td>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>RPL_I02 <a name="RPL_I02"/>
                    </td>
                    <td>I02</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>RPR_I03 <a name="RPR_I03"/>
                    </td>
                    <td>I03</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>RQA_I08 <a name="RQA_I08"/>
                    </td>
                    <td>I08, I09, I10, I11</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>RQC_I05 <a name="RQC_I05"/>
                    </td>
                    <td>I05, I06</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>RQC_I06 <a name="RQC_I06"/>
                    </td>
                    <td>I06</td>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>RQI_I01 <a name="RQI_I01"/>
                    </td>
                    <td>I01, I02, I03, I07</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>RQI_I0I <a name="RQI_I0I"/>
                    </td>
                    <td>I01, I02, I03</td>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>RQP_I04 <a name="RQP_I04"/>
                    </td>
                    <td>I04</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>RQQ_Q09 <a name="RQQ_Q09"/>
                    </td>
                    <td>Q09</td>
                    <td>added v2.3.1, removed after v2.5.1</td>
                </tr>
                <tr>
                    <td>RRA_O02 <a name="RRA_O02"/>
                    </td>
                    <td>O02</td>
                    <td>added v2.3.1, removed after v2.5</td>
                </tr>
                <tr>
                    <td>RRA_O18 <a name="RRA_O18"/>
                    </td>
                    <td>O18</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>RRD_O02 <a name="RRD_O02"/>
                    </td>
                    <td>O02</td>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>RRD_O14 <a name="RRD_O14"/>
                    </td>
                    <td>O14</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>RRE_O02 <a name="RRE_O02"/>
                    </td>
                    <td>O02</td>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>RRE_O12 <a name="RRE_O12"/>
                    </td>
                    <td>O12, O26</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>RRG_O02 <a name="RRG_O02"/>
                    </td>
                    <td>O02</td>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>RRG_O16 <a name="RRG_O16"/>
                    </td>
                    <td>O16</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>RRI_I12 <a name="RRI_I12"/>
                    </td>
                    <td>I12, I13, I14, I15</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>RRO_O02 <a name="RRO_O02"/>
                    </td>
                    <td/>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>RSP_E03 <a name="RSP_E03"/>
                    </td>
                    <td>E03</td>
                    <td>added v2.6</td>
                </tr>
                <tr>
                    <td>RSP_E22 <a name="RSP_E22"/>
                    </td>
                    <td>E22</td>
                    <td>added v2.6</td>
                </tr>
                <tr>
                    <td>RSP_K11 <a name="RSP_K11"/>
                    </td>
                    <td>K11</td>
                    <td>added v2.5</td>
                </tr>
                <tr>
                    <td>RSP_K21 <a name="RSP_K21"/>
                    </td>
                    <td>K21</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>RSP_K22 <a name="RSP_K22"/>
                    </td>
                    <td>K22</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>RSP_K23 <a name="RSP_K23"/>
                    </td>
                    <td>K23, K24</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>RSP_K24 <a name="RSP_K24"/>
                    </td>
                    <td>K24</td>
                    <td>added v2.4, removed after v2.4</td>
                </tr>
                <tr>
                    <td>RSP_K25 <a name="RSP_K25"/>
                    </td>
                    <td>K25</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>RSP_K31 <a name="RSP_K31"/>
                    </td>
                    <td>K31</td>
                    <td>added v2.5.1</td>
                </tr>
                <tr>
                    <td>RSP_K32 <a name="RSP_K32"/>
                    </td>
                    <td>K32</td>
                    <td>added v2.7</td>
                </tr>
                <tr>
                    <td>RSP_Q11 <a name="RSP_Q11"/>
                    </td>
                    <td>Q11</td>
                    <td>added v2.5.1</td>
                </tr>
                <tr>
                    <td>RSP_Z82 <a name="RSP_Z82"/>
                    </td>
                    <td>Z82</td>
                    <td>added v2.7</td>
                </tr>
                <tr>
                    <td>RSP_Z86 <a name="RSP_Z86"/>
                    </td>
                    <td>Z86</td>
                    <td>added v2.7</td>
                </tr>
                <tr>
                    <td>RSP_Z88 <a name="RSP_Z88"/>
                    </td>
                    <td>Z88</td>
                    <td>added v2.7</td>
                </tr>
                <tr>
                    <td>RSP_Z90 <a name="RSP_Z90"/>
                    </td>
                    <td>Z90</td>
                    <td>added v2.7</td>
                </tr>
                <tr>
                    <td>RTB_K13 <a name="RTB_K13"/>
                    </td>
                    <td>K13</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>RTB_Knn <a name="RTB_Knn"/>
                    </td>
                    <td>Knn</td>
                    <td>added v2.7</td>
                </tr>
                <tr>
                    <td>RTB_Z74 <a name="RTB_Z74"/>
                    </td>
                    <td>Z74</td>
                    <td>added v2.7</td>
                </tr>
                <tr>
                    <td>SDR_S31 <a name="SDR_S31"/>
                    </td>
                    <td>S31, S36</td>
                    <td>added v2.6</td>
                </tr>
                <tr>
                    <td>SDR_S32 <a name="SDR_S32"/>
                    </td>
                    <td>S32, S37</td>
                    <td>added v2.6</td>
                </tr>
                <tr>
                    <td>SIU_S12 <a name="SIU_S12"/>
                    </td>
                    <td>S12, S13, S14, S15, S16, S17, S18, S19, S20, S21, S22, S23, S24, S26</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>SLR_S28 <a name="SLR_S28"/>
                    </td>
                    <td>S28, S29, S30, S34, S35</td>
                    <td>added v2.6</td>
                </tr>
                <tr>
                    <td>SPQ_Q08 <a name="SPQ_Q08"/>
                    </td>
                    <td>Q08</td>
                    <td>added v2.3.1, removed after v2.5.1</td>
                </tr>
                <tr>
                    <td>SQM_S25 <a name="SQM_S25"/>
                    </td>
                    <td>S25</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>SQR_S25 <a name="SQR_S25"/>
                    </td>
                    <td>S25</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>SRM_S01 <a name="SRM_S01"/>
                    </td>
                    <td>S01, S02, S03, S04, S05, S06, S07, S08, S09, S10, S11</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>SRM_T12 <a name="SRM_T12"/>
                    </td>
                    <td>T12</td>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>SRR_S01 <a name="SRR_S01"/>
                    </td>
                    <td>S01, S02, S03, S04, S05, S06, S07, S08, S09, S10, S11</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>SRR_T12 <a name="SRR_T12"/>
                    </td>
                    <td>T12</td>
                    <td>added v2.3.1, removed after v2.3.1</td>
                </tr>
                <tr>
                    <td>SSR_U04 <a name="SSR_U04"/>
                    </td>
                    <td>U04</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>SSU_U03 <a name="SSU_U03"/>
                    </td>
                    <td>U03</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>STC_S33 <a name="STC_S33"/>
                    </td>
                    <td>S33</td>
                    <td>added v2.6</td>
                </tr>
                <tr>
                    <td>SUR_P09 <a name="SUR_P09"/>
                    </td>
                    <td>P09</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>TBR_R08 <a name="TBR_R08"/>
                    </td>
                    <td>R08</td>
                    <td>added v2.4, removed after v2.5.1</td>
                </tr>
                <tr>
                    <td>TBR_R09 <a name="TBR_R09"/>
                    </td>
                    <td>R09</td>
                    <td>added v2.3.1, removed after v2.5.1</td>
                </tr>
                <tr>
                    <td>TCU_U10 <a name="TCU_U10"/>
                    </td>
                    <td>U10, U11</td>
                    <td>added v2.4</td>
                </tr>
                <tr>
                    <td>UDM_Q05 <a name="UDM_Q05"/>
                    </td>
                    <td>Q05</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>VQQ_Q07 <a name="VQQ_Q07"/>
                    </td>
                    <td>Q07</td>
                    <td>added v2.3.1, removed after v2.5.1</td>
                </tr>
                <tr>
                    <td>VXQ_V01 <a name="VXQ_V01"/>
                    </td>
                    <td>V01</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>VXR_V03 <a name="VXR_V03"/>
                    </td>
                    <td>V03</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>VXU_V04 <a name="VXU_V04"/>
                    </td>
                    <td>V04</td>
                    <td>added v2.3.1</td>
                </tr>
                <tr>
                    <td>VXX_V02 <a name="VXX_V02"/>
                    </td>
                    <td>V02</td>
                    <td>added v2.3.1</td>
                </tr>
            </table>
        </div>
    </f:text>
    <f:identifier value="http://hl7.org/fhir/v2/vs/0354"/>
    <f:name value="v2 Message Structure"/>
    <f:status value="active"/>
    <f:experimental value="false"/>
    <f:publisher value="HL7, Inc"/>
    <f:contact>
        <f:telecom>
            <f:system value="other"/>
            <f:value value="http://hl7.org"/>
        </f:telecom>
    </f:contact>
    <f:date value="2011-01-28"/>
    <f:description value="FHIR Value set/code system definition for HL7 v2 table 0354 ( Message Structure)"/>
    <f:copyright value="This content from LOINC® is copyright © 1995 Regenstrief Institute, Inc. and the LOINC Committee, and available at no cost under the license at http://loinc.org/terms-of-use"/>
    <f:extensible value="false"/>
    <f:codeSystem>
        <f:system value="http://hl7.org/fhir/v2/0354"/>
        <f:caseSensitive value="true"/>
        <f:concept>
            <f:code value="ACK"/>
            <f:display value="Varies"/>
        </f:concept>
        <f:concept>
            <f:code value="ADR_A19"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A01"/>
            <f:display value="A01, A04, A08, A13"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A02"/>
            <f:display value="A02"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A03"/>
            <f:display value="A03"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A05"/>
            <f:display value="A05, A14, A28, A31"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A06"/>
            <f:display value="A06, A07"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A09"/>
            <f:display value="A09, A10, A11"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A12"/>
            <f:display value="A12"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A15"/>
            <f:display value="A15"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A16"/>
            <f:display value="A16"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A17"/>
            <f:display value="A17"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A18"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A20"/>
            <f:display value="A20"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A21"/>
            <f:display value="A21, A22, A23, A25, A26, A27, A29, A32, A33"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A24"/>
            <f:display value="A24"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="ADT_A28"/>
            <f:display value="A28, A31"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A30"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A37"/>
            <f:display value="A37"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A38"/>
            <f:display value="A38"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A39"/>
            <f:display value="A39, A40, A41, A42"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A43"/>
            <f:display value="A43"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A44"/>
            <f:display value="A44"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A45"/>
            <f:display value="A45"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A50"/>
            <f:display value="A50, A51"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A52"/>
            <f:display value="A52, A53"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A54"/>
            <f:display value="A54, A55"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A60"/>
            <f:display value="A60"/>
        </f:concept>
        <f:concept>
            <f:code value="ADT_A61"/>
            <f:display value="A61, A62"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="ARD_A19"/>
            <f:display value="A19"/>
        </f:concept>
        <f:concept>
            <f:code value="BAR_P01"/>
            <f:display value="P01"/>
        </f:concept>
        <f:concept>
            <f:code value="BAR_P02"/>
            <f:display value="P02"/>
        </f:concept>
        <f:concept>
            <f:code value="BAR_P05"/>
            <f:display value="P05"/>
        </f:concept>
        <f:concept>
            <f:code value="BAR_P06"/>
            <f:display value="P06"/>
        </f:concept>
        <f:concept>
            <f:code value="BAR_P10"/>
            <f:display value="P10"/>
        </f:concept>
        <f:concept>
            <f:code value="BAR_P12"/>
            <f:display value="P12"/>
        </f:concept>
        <f:concept>
            <f:code value="BPS_O29"/>
            <f:display value="O29"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="BRP_030"/>
            <f:display value="O30"/>
        </f:concept>
        <f:concept>
            <f:code value="BRP_O30"/>
            <f:display value="O30"/>
        </f:concept>
        <f:concept>
            <f:code value="BRT_O32"/>
            <f:display value="O32"/>
        </f:concept>
        <f:concept>
            <f:code value="BTS_O31"/>
            <f:display value="O31"/>
        </f:concept>
        <f:concept>
            <f:code value="CCF_I22"/>
            <f:display value="I22"/>
        </f:concept>
        <f:concept>
            <f:code value="CCI_I22"/>
            <f:display value="I22"/>
        </f:concept>
        <f:concept>
            <f:code value="CCM_I21"/>
            <f:display value="I21"/>
        </f:concept>
        <f:concept>
            <f:code value="CCQ_I19"/>
            <f:display value="I19"/>
        </f:concept>
        <f:concept>
            <f:code value="CCR_I16"/>
            <f:display value="I16, |17, |18"/>
        </f:concept>
        <f:concept>
            <f:code value="CCU_I20"/>
            <f:display value="I20"/>
        </f:concept>
        <f:concept>
            <f:code value="CQU_I19"/>
            <f:display value="I19"/>
        </f:concept>
        <f:concept>
            <f:code value="CRM_C01"/>
            <f:display value="C01, C02, C03, C04, C05, C06, C07, C08"/>
        </f:concept>
        <f:concept>
            <f:code value="CSU_C09"/>
            <f:display value="C09, C10, C11, C12"/>
        </f:concept>
        <f:concept>
            <f:code value="DFT_P03"/>
            <f:display value="P03"/>
        </f:concept>
        <f:concept>
            <f:code value="DFT_P11"/>
            <f:display value="P11"/>
        </f:concept>
        <f:concept>
            <f:code value="DOC_T12"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="DSR_P04"/>
            <f:display value="P04"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="DSR_Q01"/>
            <f:display value="Q01"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="DSR_Q03"/>
            <f:display value="Q03"/>
        </f:concept>
        <f:concept>
            <f:code value="EAC_U07"/>
            <f:display value="U07"/>
        </f:concept>
        <f:concept>
            <f:code value="EAN_U09"/>
            <f:display value="U09"/>
        </f:concept>
        <f:concept>
            <f:code value="EAR_U08"/>
            <f:display value="U08"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="EDR_R07"/>
            <f:display value="R07"/>
        </f:concept>
        <f:concept>
            <f:code value="EHC_E01"/>
            <f:display value="E01"/>
        </f:concept>
        <f:concept>
            <f:code value="EHC_E02"/>
            <f:display value="E02"/>
        </f:concept>
        <f:concept>
            <f:code value="EHC_E04"/>
            <f:display value="E04"/>
        </f:concept>
        <f:concept>
            <f:code value="EHC_E10"/>
            <f:display value="E10"/>
        </f:concept>
        <f:concept>
            <f:code value="EHC_E12"/>
            <f:display value="E12"/>
        </f:concept>
        <f:concept>
            <f:code value="EHC_E13"/>
            <f:display value="E13"/>
        </f:concept>
        <f:concept>
            <f:code value="EHC_E15"/>
            <f:display value="E15"/>
        </f:concept>
        <f:concept>
            <f:code value="EHC_E20"/>
            <f:display value="E20"/>
        </f:concept>
        <f:concept>
            <f:code value="EHC_E21"/>
            <f:display value="E21"/>
        </f:concept>
        <f:concept>
            <f:code value="EHC_E24"/>
            <f:display value="E24"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="EQQ_Q04"/>
            <f:display value="Q04"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="ERP_R09"/>
            <f:display value="R09"/>
        </f:concept>
        <f:concept>
            <f:code value="ESR_U02"/>
            <f:display value="U02"/>
        </f:concept>
        <f:concept>
            <f:code value="ESU_U01"/>
            <f:display value="U01"/>
        </f:concept>
        <f:concept>
            <f:code value="INR_U06"/>
            <f:display value="U06"/>
        </f:concept>
        <f:concept>
            <f:code value="INU_U05"/>
            <f:display value="U05"/>
        </f:concept>
        <f:concept>
            <f:code value="LSU_U12"/>
            <f:display value="U12, U13"/>
        </f:concept>
        <f:concept>
            <f:code value="MDM_T01"/>
            <f:display value="T01, T03, T05, T07, T09, T11"/>
        </f:concept>
        <f:concept>
            <f:code value="MDM_T02"/>
            <f:display value="T02, T04, T06, T08, T10"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="MFD_MFA"/>
            <f:display value="MFA"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="MFD_P09"/>
            <f:display value="P09"/>
        </f:concept>
        <f:concept>
            <f:code value="MFK_M01"/>
            <f:display value="M01, M02, M03, M04, M05, M06, M07, M08, M09, M10, M11"/>
        </f:concept>
        <f:concept>
            <f:code value="MFN_M01"/>
        </f:concept>
        <f:concept>
            <f:code value="MFN_M02"/>
            <f:display value="M02"/>
        </f:concept>
        <f:concept>
            <f:code value="MFN_M03"/>
            <f:display value="M03"/>
        </f:concept>
        <f:concept>
            <f:code value="MFN_M04"/>
            <f:display value="M04"/>
        </f:concept>
        <f:concept>
            <f:code value="MFN_M05"/>
            <f:display value="M05"/>
        </f:concept>
        <f:concept>
            <f:code value="MFN_M06"/>
            <f:display value="M06"/>
        </f:concept>
        <f:concept>
            <f:code value="MFN_M07"/>
            <f:display value="M07"/>
        </f:concept>
        <f:concept>
            <f:code value="MFN_M08"/>
            <f:display value="M08"/>
        </f:concept>
        <f:concept>
            <f:code value="MFN_M09"/>
            <f:display value="M09"/>
        </f:concept>
        <f:concept>
            <f:code value="MFN_M10"/>
            <f:display value="M10"/>
        </f:concept>
        <f:concept>
            <f:code value="MFN_M11"/>
            <f:display value="M11"/>
        </f:concept>
        <f:concept>
            <f:code value="MFN_M12"/>
            <f:display value="M12"/>
        </f:concept>
        <f:concept>
            <f:code value="MFN_M13"/>
            <f:display value="M13"/>
        </f:concept>
        <f:concept>
            <f:code value="MFN_M15"/>
            <f:display value="M15"/>
        </f:concept>
        <f:concept>
            <f:code value="MFN_M16"/>
            <f:display value="M16"/>
        </f:concept>
        <f:concept>
            <f:code value="MFN_M17"/>
            <f:display value="M17"/>
        </f:concept>
        <f:concept>
            <f:code value="MFN_Znn"/>
            <f:display value="Znn"/>
        </f:concept>
        <f:concept>
            <f:code value="MFQ_M01"/>
            <f:display value="M01, M02, M03, M04, M05, M06"/>
        </f:concept>
        <f:concept>
            <f:code value="MFR_M01"/>
            <f:display value="M01, M02, M03,"/>
        </f:concept>
        <f:concept>
            <f:code value="MFR_M04"/>
            <f:display value="M04"/>
        </f:concept>
        <f:concept>
            <f:code value="MFR_M05"/>
            <f:display value="M05"/>
        </f:concept>
        <f:concept>
            <f:code value="MFR_M06"/>
            <f:display value="M06"/>
        </f:concept>
        <f:concept>
            <f:code value="MFR_M07"/>
            <f:display value="M07"/>
        </f:concept>
        <f:concept>
            <f:code value="NMD_N02"/>
            <f:display value="N02"/>
        </f:concept>
        <f:concept>
            <f:code value="NMQ_N01"/>
            <f:display value="N01"/>
        </f:concept>
        <f:concept>
            <f:code value="NMR_N01"/>
            <f:display value="N01"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="NUL"/>
            <f:display value="Null"/>
        </f:concept>
        <f:concept>
            <f:code value="OMB_O27"/>
            <f:display value="O27"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="OMD_O01"/>
        </f:concept>
        <f:concept>
            <f:code value="OMD_O03"/>
            <f:display value="O03"/>
        </f:concept>
        <f:concept>
            <f:code value="OMG_O19"/>
            <f:display value="O19"/>
        </f:concept>
        <f:concept>
            <f:code value="OMI_O23"/>
            <f:display value="O23"/>
        </f:concept>
        <f:concept>
            <f:code value="OML_O21"/>
            <f:display value="O21"/>
        </f:concept>
        <f:concept>
            <f:code value="OML_O33"/>
            <f:display value="O33"/>
        </f:concept>
        <f:concept>
            <f:code value="OML_O35"/>
            <f:display value="O35"/>
        </f:concept>
        <f:concept>
            <f:code value="OML_O39"/>
            <f:display value="O39"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="OMN_O01"/>
        </f:concept>
        <f:concept>
            <f:code value="OMN_O07"/>
            <f:display value="O07"/>
        </f:concept>
        <f:concept>
            <f:code value="OMP_O09"/>
            <f:display value="O09"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="OMS_O01"/>
        </f:concept>
        <f:concept>
            <f:code value="OMS_O05"/>
            <f:display value="O05"/>
        </f:concept>
        <f:concept>
            <f:code value="OPL_O37"/>
            <f:display value="O37"/>
        </f:concept>
        <f:concept>
            <f:code value="OPR_O38"/>
            <f:display value="O38"/>
        </f:concept>
        <f:concept>
            <f:code value="OPU_R25"/>
            <f:display value="R25"/>
        </f:concept>
        <f:concept>
            <f:code value="ORA_R33"/>
            <f:display value="R33"/>
        </f:concept>
        <f:concept>
            <f:code value="ORB_O28"/>
            <f:display value="O28"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="ORD_O02"/>
        </f:concept>
        <f:concept>
            <f:code value="ORD_O04"/>
            <f:display value="O04"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="ORF_R02"/>
            <f:display value="R02, R04"/>
        </f:concept>
        <f:concept>
            <f:code value="ORF_R04"/>
            <f:display value="R04"/>
        </f:concept>
        <f:concept>
            <f:code value="ORG_O20"/>
            <f:display value="O20"/>
        </f:concept>
        <f:concept>
            <f:code value="ORI_O24"/>
            <f:display value="O24"/>
        </f:concept>
        <f:concept>
            <f:code value="ORL_O22"/>
            <f:display value="O22"/>
        </f:concept>
        <f:concept>
            <f:code value="ORL_O34"/>
            <f:display value="O34"/>
        </f:concept>
        <f:concept>
            <f:code value="ORL_O36"/>
            <f:display value="O36"/>
        </f:concept>
        <f:concept>
            <f:code value="ORL_O40"/>
            <f:display value="O40"/>
        </f:concept>
        <f:concept>
            <f:code value="ORM_O01"/>
            <f:display value="O01"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="ORM_Q06"/>
            <f:display value="Q06"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="ORN_O02"/>
        </f:concept>
        <f:concept>
            <f:code value="ORN_O08"/>
            <f:display value="O08"/>
        </f:concept>
        <f:concept>
            <f:code value="ORP_O10"/>
            <f:display value="O10"/>
        </f:concept>
        <f:concept>
            <f:code value="ORR_O02"/>
            <f:display value="O02"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="ORR_Q06"/>
            <f:display value="Q06"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="ORS_O02"/>
        </f:concept>
        <f:concept>
            <f:code value="ORS_O06"/>
            <f:display value="O06"/>
        </f:concept>
        <f:concept>
            <f:code value="ORU_R01"/>
            <f:display value="R01"/>
        </f:concept>
        <f:concept>
            <f:code value="ORU_R30"/>
            <f:display value="R30"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="ORU_R31"/>
            <f:display value="R31"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="ORU_R32"/>
            <f:display value="R32"/>
        </f:concept>
        <f:concept>
            <f:code value="ORU_W01"/>
            <f:display value="W01"/>
        </f:concept>
        <f:concept>
            <f:code value="OSM_R26"/>
            <f:display value="R26"/>
        </f:concept>
        <f:concept>
            <f:code value="OSQ_Q06"/>
            <f:display value="Q06"/>
        </f:concept>
        <f:concept>
            <f:code value="OSR_Q06"/>
            <f:display value="Q06"/>
        </f:concept>
        <f:concept>
            <f:code value="OUL_R21"/>
            <f:display value="R21"/>
        </f:concept>
        <f:concept>
            <f:code value="OUL_R22"/>
            <f:display value="R22"/>
        </f:concept>
        <f:concept>
            <f:code value="OUL_R23"/>
            <f:display value="R23"/>
        </f:concept>
        <f:concept>
            <f:code value="OUL_R24"/>
            <f:display value="R24"/>
        </f:concept>
        <f:concept>
            <f:code value="PEX_P07"/>
            <f:display value="P07, P08"/>
        </f:concept>
        <f:concept>
            <f:code value="PGL_PC6"/>
            <f:display value="PC6, PC7, PC8"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="PIN_I07"/>
            <f:display value="I07"/>
        </f:concept>
        <f:concept>
            <f:code value="PMU_B01"/>
            <f:display value="B01, B02"/>
        </f:concept>
        <f:concept>
            <f:code value="PMU_B03"/>
            <f:display value="B03"/>
        </f:concept>
        <f:concept>
            <f:code value="PMU_B04"/>
            <f:display value="B04, B05, B06"/>
        </f:concept>
        <f:concept>
            <f:code value="PMU_B07"/>
            <f:display value="B07"/>
        </f:concept>
        <f:concept>
            <f:code value="PMU_B08"/>
            <f:display value="B08"/>
        </f:concept>
        <f:concept>
            <f:code value="PPG_PCG"/>
            <f:display value="PCC, PCG, PCH, PCJ"/>
        </f:concept>
        <f:concept>
            <f:code value="PPP_PCB"/>
            <f:display value="PCB, PCD"/>
        </f:concept>
        <f:concept>
            <f:code value="PPR_PC1"/>
            <f:display value="PC1, PC2, PC3"/>
        </f:concept>
        <f:concept>
            <f:code value="PPT_PCL"/>
            <f:display value="PCL"/>
        </f:concept>
        <f:concept>
            <f:code value="PPV_PCA"/>
            <f:display value="PCA"/>
        </f:concept>
        <f:concept>
            <f:code value="PRR_PC5"/>
            <f:display value="PC5"/>
        </f:concept>
        <f:concept>
            <f:code value="PTR_PCF"/>
            <f:display value="PCF"/>
        </f:concept>
        <f:concept>
            <f:code value="QBP_E03"/>
            <f:display value="E03"/>
        </f:concept>
        <f:concept>
            <f:code value="QBP_E22"/>
            <f:display value="E22"/>
        </f:concept>
        <f:concept>
            <f:code value="QBP_Q11"/>
            <f:display value="Q11"/>
        </f:concept>
        <f:concept>
            <f:code value="QBP_Q13"/>
            <f:display value="Q13"/>
        </f:concept>
        <f:concept>
            <f:code value="QBP_Q15"/>
            <f:display value="Q15"/>
        </f:concept>
        <f:concept>
            <f:code value="QBP_Q21"/>
            <f:display value="Q21, Q22, Q23,Q24, Q25"/>
        </f:concept>
        <f:concept>
            <f:code value="QBP_Qnn"/>
            <f:display value="Qnn"/>
        </f:concept>
        <f:concept>
            <f:code value="QBP_Z73"/>
            <f:display value="Z73"/>
        </f:concept>
        <f:concept>
            <f:code value="QCK_Q02"/>
            <f:display value="Q02"/>
        </f:concept>
        <f:concept>
            <f:code value="QCN_J01"/>
            <f:display value="J01, J02"/>
        </f:concept>
        <f:concept>
            <f:code value="QRF_W02"/>
            <f:display value="W02"/>
        </f:concept>
        <f:concept>
            <f:code value="QRY_A19"/>
            <f:display value="A19"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="QRY_P04"/>
            <f:display value="P04"/>
        </f:concept>
        <f:concept>
            <f:code value="QRY_PC4"/>
            <f:display value="PC4, PC9, PCE, PCK"/>
        </f:concept>
        <f:concept>
            <f:code value="QRY_Q01"/>
            <f:display value="Q01, Q26, Q27, Q28, Q29, Q30"/>
        </f:concept>
        <f:concept>
            <f:code value="QRY_Q02"/>
            <f:display value="Q02"/>
        </f:concept>
        <f:concept>
            <f:code value="QRY_R02"/>
            <f:display value="R02"/>
        </f:concept>
        <f:concept>
            <f:code value="QRY_T12"/>
            <f:display value="T12"/>
        </f:concept>
        <f:concept>
            <f:code value="QSB_Q16"/>
            <f:display value="Q16"/>
        </f:concept>
        <f:concept>
            <f:code value="QVR_Q17"/>
            <f:display value="Q17"/>
        </f:concept>
        <f:concept>
            <f:code value="RAR_RAR"/>
            <f:display value="RAR"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="RAS_O01"/>
            <f:display value="O01"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="RAS_O02"/>
            <f:display value="O022"/>
        </f:concept>
        <f:concept>
            <f:code value="RAS_O17"/>
            <f:display value="O17"/>
        </f:concept>
        <f:concept>
            <f:code value="RCI_I05"/>
            <f:display value="I05"/>
        </f:concept>
        <f:concept>
            <f:code value="RCL_I06"/>
            <f:display value="I06"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="RDE_O01"/>
            <f:display value="O01"/>
        </f:concept>
        <f:concept>
            <f:code value="RDE_O11"/>
            <f:display value="O11, O25"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="RDO_O01"/>
        </f:concept>
        <f:concept>
            <f:code value="RDR_RDR"/>
            <f:display value="RDR"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="RDS_O01"/>
            <f:display value="O01"/>
        </f:concept>
        <f:concept>
            <f:code value="RDS_O13"/>
            <f:display value="O13"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="RDY_K11"/>
            <f:display value="K11"/>
        </f:concept>
        <f:concept>
            <f:code value="RDY_K15"/>
            <f:display value="K15"/>
        </f:concept>
        <f:concept>
            <f:code value="REF_I12"/>
            <f:display value="I12, I13, I14, I15"/>
        </f:concept>
        <f:concept>
            <f:code value="RER_RER"/>
            <f:display value="RER"/>
        </f:concept>
        <f:concept>
            <f:code value="RGR_RGR"/>
            <f:display value="RGR"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="RGV_O01"/>
            <f:display value="O01"/>
        </f:concept>
        <f:concept>
            <f:code value="RGV_O15"/>
            <f:display value="O15"/>
        </f:concept>
        <f:concept>
            <f:code value="ROR_ROR"/>
            <f:display value="ROR"/>
        </f:concept>
        <f:concept>
            <f:code value="RPA_I08"/>
            <f:display value="I08, I09. I10, I11"/>
        </f:concept>
        <f:concept>
            <f:code value="RPI_I01"/>
            <f:display value="I01, I04"/>
        </f:concept>
        <f:concept>
            <f:code value="RPI_I04"/>
            <f:display value="I04"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="RPI_I0I"/>
            <f:display value="I01, I04"/>
        </f:concept>
        <f:concept>
            <f:code value="RPL_I02"/>
            <f:display value="I02"/>
        </f:concept>
        <f:concept>
            <f:code value="RPR_I03"/>
            <f:display value="I03"/>
        </f:concept>
        <f:concept>
            <f:code value="RQA_I08"/>
            <f:display value="I08, I09, I10, I11"/>
        </f:concept>
        <f:concept>
            <f:code value="RQC_I05"/>
            <f:display value="I05, I06"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="RQC_I06"/>
            <f:display value="I06"/>
        </f:concept>
        <f:concept>
            <f:code value="RQI_I01"/>
            <f:display value="I01, I02, I03, I07"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="RQI_I0I"/>
            <f:display value="I01, I02, I03"/>
        </f:concept>
        <f:concept>
            <f:code value="RQP_I04"/>
            <f:display value="I04"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="RQQ_Q09"/>
            <f:display value="Q09"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="RRA_O02"/>
            <f:display value="O02"/>
        </f:concept>
        <f:concept>
            <f:code value="RRA_O18"/>
            <f:display value="O18"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="RRD_O02"/>
            <f:display value="O02"/>
        </f:concept>
        <f:concept>
            <f:code value="RRD_O14"/>
            <f:display value="O14"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="RRE_O02"/>
            <f:display value="O02"/>
        </f:concept>
        <f:concept>
            <f:code value="RRE_O12"/>
            <f:display value="O12, O26"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="RRG_O02"/>
            <f:display value="O02"/>
        </f:concept>
        <f:concept>
            <f:code value="RRG_O16"/>
            <f:display value="O16"/>
        </f:concept>
        <f:concept>
            <f:code value="RRI_I12"/>
            <f:display value="I12, I13, I14, I15"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="RRO_O02"/>
        </f:concept>
        <f:concept>
            <f:code value="RSP_E03"/>
            <f:display value="E03"/>
        </f:concept>
        <f:concept>
            <f:code value="RSP_E22"/>
            <f:display value="E22"/>
        </f:concept>
        <f:concept>
            <f:code value="RSP_K11"/>
            <f:display value="K11"/>
        </f:concept>
        <f:concept>
            <f:code value="RSP_K21"/>
            <f:display value="K21"/>
        </f:concept>
        <f:concept>
            <f:code value="RSP_K22"/>
            <f:display value="K22"/>
        </f:concept>
        <f:concept>
            <f:code value="RSP_K23"/>
            <f:display value="K23, K24"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="RSP_K24"/>
            <f:display value="K24"/>
        </f:concept>
        <f:concept>
            <f:code value="RSP_K25"/>
            <f:display value="K25"/>
        </f:concept>
        <f:concept>
            <f:code value="RSP_K31"/>
            <f:display value="K31"/>
        </f:concept>
        <f:concept>
            <f:code value="RSP_K32"/>
            <f:display value="K32"/>
        </f:concept>
        <f:concept>
            <f:code value="RSP_Q11"/>
            <f:display value="Q11"/>
        </f:concept>
        <f:concept>
            <f:code value="RSP_Z82"/>
            <f:display value="Z82"/>
        </f:concept>
        <f:concept>
            <f:code value="RSP_Z86"/>
            <f:display value="Z86"/>
        </f:concept>
        <f:concept>
            <f:code value="RSP_Z88"/>
            <f:display value="Z88"/>
        </f:concept>
        <f:concept>
            <f:code value="RSP_Z90"/>
            <f:display value="Z90"/>
        </f:concept>
        <f:concept>
            <f:code value="RTB_K13"/>
            <f:display value="K13"/>
        </f:concept>
        <f:concept>
            <f:code value="RTB_Knn"/>
            <f:display value="Knn"/>
        </f:concept>
        <f:concept>
            <f:code value="RTB_Z74"/>
            <f:display value="Z74"/>
        </f:concept>
        <f:concept>
            <f:code value="SDR_S31"/>
            <f:display value="S31, S36"/>
        </f:concept>
        <f:concept>
            <f:code value="SDR_S32"/>
            <f:display value="S32, S37"/>
        </f:concept>
        <f:concept>
            <f:code value="SIU_S12"/>
            <f:display value="S12, S13, S14, S15, S16, S17, S18, S19, S20, S21, S22, S23, S24, S26"/>
        </f:concept>
        <f:concept>
            <f:code value="SLR_S28"/>
            <f:display value="S28, S29, S30, S34, S35"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="SPQ_Q08"/>
            <f:display value="Q08"/>
        </f:concept>
        <f:concept>
            <f:code value="SQM_S25"/>
            <f:display value="S25"/>
        </f:concept>
        <f:concept>
            <f:code value="SQR_S25"/>
            <f:display value="S25"/>
        </f:concept>
        <f:concept>
            <f:code value="SRM_S01"/>
            <f:display value="S01, S02, S03, S04, S05, S06, S07, S08, S09, S10, S11"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="SRM_T12"/>
            <f:display value="T12"/>
        </f:concept>
        <f:concept>
            <f:code value="SRR_S01"/>
            <f:display value="S01, S02, S03, S04, S05, S06, S07, S08, S09, S10, S11"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="SRR_T12"/>
            <f:display value="T12"/>
        </f:concept>
        <f:concept>
            <f:code value="SSR_U04"/>
            <f:display value="U04"/>
        </f:concept>
        <f:concept>
            <f:code value="SSU_U03"/>
            <f:display value="U03"/>
        </f:concept>
        <f:concept>
            <f:code value="STC_S33"/>
            <f:display value="S33"/>
        </f:concept>
        <f:concept>
            <f:code value="SUR_P09"/>
            <f:display value="P09"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="TBR_R08"/>
            <f:display value="R08"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="TBR_R09"/>
            <f:display value="R09"/>
        </f:concept>
        <f:concept>
            <f:code value="TCU_U10"/>
            <f:display value="U10, U11"/>
        </f:concept>
        <f:concept>
            <f:code value="UDM_Q05"/>
            <f:display value="Q05"/>
        </f:concept>
        <f:concept>
            <f:extension url="http://hl7.org/fhir/ExtensionDefinition/valueset-deprecated">
                <f:valueBoolean value="true"/>
            </f:extension>
            <f:code value="VQQ_Q07"/>
            <f:display value="Q07"/>
        </f:concept>
        <f:concept>
            <f:code value="VXQ_V01"/>
            <f:display value="V01"/>
        </f:concept>
        <f:concept>
            <f:code value="VXR_V03"/>
            <f:display value="V03"/>
        </f:concept>
        <f:concept>
            <f:code value="VXU_V04"/>
            <f:display value="V04"/>
        </f:concept>
        <f:concept>
            <f:code value="VXX_V02"/>
            <f:display value="V02"/>
        </f:concept>
    </f:codeSystem>
</f:ValueSet>

let $res            := fn:serialize($xml)

let $jsonP          := fJsonParser:new()
let $xmlP           := fXmlParser:new()

let $testResource   := xmlP.parse($res) (: Parse into a base Resource object :)

let $jsonFormat     := 
    try {
        jsonP.composeString($testResource)
    } 
    catch java:java.lang.reflect.InvocationTargetException {
        concat('ERROR ',$err:code,' : ',$err:description,', module: ', $err:module, '(', $err:line-number, ',', $err:column-number, ') ')
    }
    catch * {
        ()
    }

let $xmlFormat      := 
    try {
        xmlP.composeString($testResource)
    } 
    catch java:java.lang.reflect.InvocationTargetException {
        concat('ERROR ',$err:code,' : ',$err:description,', module: ', $err:module, '(', $err:line-number, ',', $err:column-number, ') ')
    }
    catch * {
        ()
    }

return
    <result>
    <json>{$jsonFormat}</json>
    <xml>{$xmlFormat}</xml>
    </result>
