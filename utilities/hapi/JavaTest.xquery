xquery version "1.0";
(:
: Need java support active in eXist-db/conf.xml
: See: http://exist-db.org/exist/apps/doc/xquery.xml#calling-java
:)

declare namespace fle="java:java.io.File";
<files>
{
    for $f in fle:list-files( fle:new(".") )
    let $n := fle:get-name($f)
    order by $n
    return
        if (fle:is-directory($f)) then
            <directory name="{ $n }"/>
        else
            <file name="{ $n }" size="{ fle:length($f) }"/>
}
</files>