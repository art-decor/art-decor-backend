xquery version "3.1";
(:
: Need java support active in eXist-db/conf.xml
: See: http://exist-db.org/exist/apps/doc/xquery.xml#calling-java
:
: /Applications/eXist-db.app/Contents/Resources/etc/conf.xml:902
:       <xquery enable-java-binding="yes"
:
: Need HAPI Jars loaded
: See: https://github.com/jamesagnew/hapi-fhir/releases
:  or: http://jamesagnew.github.io/hapi-fhir/download.html
:
:   
:
: Equivalent Java code
: package nl.nictiz.fhir.hapi;
: 
: import ca.uhn.fhir.context.FhirContext;
: import ca.uhn.fhir.model.dstu.resource.Patient;
: import ca.uhn.fhir.parser.IParser;
: 
: public class Main {
: 
:    public static void main(String[] args) {
:       FhirContext fc = new FhirContext();
:       IParser jsonp = fc.newJsonParser();
:       Patient patient = new Patient();
:       patient.addName().addFamily("Henket");
:       System.out.println(jsonp.encodeResourceToString(patient));
:    }
: }
:)
(:declare namespace patient       = "java:ca.uhn.fhir.model.dstu.resource.Patient";
declare namespace human-name-dt = "java:ca.uhn.fhir.model.dstu.composite.HumanNameDt";:)
declare namespace fhir-enum     = "java:ca.uhn.fhir.context.FhirVersionEnum";
declare namespace fhir-context  = "java:ca.uhn.fhir.context.FhirContext";
declare namespace xml-parser    = "java:ca.uhn.fhir.parser.IParser";
declare namespace json-parser   = "java:ca.uhn.fhir.parser.IParser";
declare namespace f             = "http://hl7.org/fhir";

(:let $vs             := valueset:new():)
(:let $vsc            := valueset:getClass($vs):)
let $fc             := fhir-context:forDstu3()
let $jp             := fhir-context:newJsonParser($fc)
let $xp             := fhir-context:newXmlParser($fc)
(:let $patient        := patient:new()
let $patient        := patient:add-identifier($patient, "urn:system", "123")
let $human-name-dt  := human-name-dt:add-family(patient:add-name($patient), "Surname")
let $json           := json-parser:encodeResourceToString($jp,$patient):)

(:let $xml            := doc('file:/Users/ahenket/Development/ART-DECOR/trunk/utilities/hapi/vs-fhir.xml')//f:*[1]:)
let $xml            := <ValueSet xmlns="http://hl7.org/fhir"><status value="unknown"/></ValueSet>

let $res            := fn:serialize($xml)
let $json           := 
    try {
        let $xmlp           := xml-parser:parseResource($xp, $res)
        let $jsonp          := json-parser:encodeResourceToString($jp, $xmlp)
        return $jsonp
    } 
    catch java:java.lang.reflect.InvocationTargetException {
        concat('ERROR ',$err:code,' : ',$err:description,', module: ', $err:module, '(', $err:line-number, ',', $err:column-number, ') ')
    }
    catch java:org.exist.xquery.XPathException {
        concat('ERROR ',$err:code,' : ',$err:description,', module: ', $err:module, '(', $err:line-number, ',', $err:column-number, ') ')
    }
    catch * {
        concat('ERROR ',$err:code,' : ',$err:description,', module: ', $err:module, '(', $err:line-number, ',', $err:column-number, ') ')
    }
return
    <result>
    {
        element {$xml/name()} {
            $xml/f:id,
            $xml/f:status,
            '...'
        }
    }
        <input>{$res}</input>
        <output>{$json}</output>
    </result>