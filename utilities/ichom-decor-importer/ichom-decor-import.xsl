<?xml version="1.0" encoding="UTF-8"?>
<!--
    Copyright © Nictiz
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" 
    xmlns:f="http://hl7.org/fhir"
    xmlns:local="urn:local"
    exclude-result-prefixes="#all" version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Version of this transform:</xd:b> 1.0.0beta1</xd:p>
            <xd:p><xd:b>Author of this transform:</xd:b> <xd:a href="https://www.linkedin.com/in/alexanderhenket/">Alexander Henket</xd:a> - HL7 Lead at <xd:a href="https://www.nictiz.nl">Nictiz</xd:a> - Developer @ <xd:a href="https://art-decor.org">ART-DECOR Expert Group</xd:a></xd:p>
            <xd:p>This transformation produces <xd:a href="https://assets.art-decor.org/ADAR/rv/DECOR.xsd">DECOR</xd:a> compatible output, from XML input produced from an Excel spreadsheet. This spreadsheet is maintained as intermediary medium for the initial work done for the International Consortium for Outcomes Measurement or <xd:a href="https://ichom.org">ICHOM</xd:a>.</xd:p>
            <xd:p><xd:b>Running the transform</xd:b></xd:p>
            <xd:p>The transform may be run from the command line using <xd:a href="http://www.saxonica.com/documentation/index.html#!using-xsl/commandline">Saxon</xd:a>, e.g. <xd:pre>java -jar saxon9pe.jar -s:input.xml -xsl:ichom-decor-import.xsl erroroncolumncheck=false</xd:pre></xd:p>
            <xd:p>The transform may be run through an <xd:a href="https://ant.apache.org">Ant</xd:a> task</xd:p>
            <xd:p><xd:b>Workflow</xd:b></xd:p>
            <xd:p>Spreadsheet maintainer produces XML file per worksheet tab based on <xd:a href="../ichom-xlsx.xsd">ichom-xlsx.xsd</xd:a>. This proposes the following structure. Root element <xd:i>xlsx</xd:i>, a <xd:i>row</xd:i> element per spreadsheet row containing a <xd:i>col</xd:i> element per spreadsheet column. Each col element shall have a @name attribute carrying the column name and have the cell data inside. The cell data can take on any XHTML compatible form. It might be simple text and/or contain XHTML markup.</xd:p>
            <xd:p>From the input a hierarchical tree structure is built. The order of the rows determines the order in the tree. A hierarchy up to 5 levels is achieved based on columns named tax_1 to tax_5. In the example above this would lead to the following input/output structures:</xd:p>
            <table xmlns="http://www.w3.org/1999/xhtml">
                <tr>
                    <th style="width: 25%;">Input</th>
                    <th style="width: 15%;">Logical</th>
                    <th style="width: 60%;">DECOR</th>
                </tr>
                <tr>
                    <td>
                        <pre>&lt;xlsx>
    <b style="color: green;">&lt;!-- row 1 under group A --></b>
    &lt;row>
        &lt;col name="variableid"><b style="color: red;">AGE</b>&lt;/col>
        &lt;col name="tax_1"><b style="color: red;">A</b>&lt;/col>
        &lt;col name="definition">Date of birth&lt;/col>
        &lt;col name="supportingdefinition">nan&lt;/col>
        &lt;col name="type">Date&lt;/col>
        &lt;col name="responseoptions">DD/MM/YYYY&lt;/col>
        &lt;col name="ICHOM_ID"><b style="color: red;">0</b>&lt;/col>
        &lt;!-- other columns -->
    &lt;/row>
    <b style="color: green;">&lt;!-- row 2 under group C --></b>
    &lt;row>
        &lt;col name="variableid"><b style="color: red;">COMORB</b>&lt;/col>
        &lt;col name="tax_1"><b style="color: red;">C</b>&lt;/col>
        &lt;col name="definition">Have you been told by a doctor that you have any of the following?&lt;/col>
        &lt;col name="supportingdefinition">Based upon the Self-administered Comorbidity Questionnaire (Sangha et al, 2003)&lt;/col>
        &lt;col name="type">Multiple answer
Separate multiple entries with ";"&lt;/col>
        &lt;col name="responseoptions">0 = I have no other diseases
1 = Heart disease (For example, angina, heart attack, or heart failure)
2 = High blood pressure
...&lt;/col>
        &lt;col name="ICHOM_ID"><b style="color: red;">18</b>&lt;/col>
        &lt;!-- other columns -->
    &lt;/row>
&lt;/xlsx>
                        </pre>
                    </td>
                    <td>
                        <xd:ul>
                            <xd:li>A<xd:ul>
                                    <xd:li>AGE</xd:li>
                                </xd:ul></xd:li>
                            <xd:li>C<xd:ul>
                                    <xd:li>COMORB</xd:li>
                                </xd:ul>
                            </xd:li>
                        </xd:ul>
                    </td>
                    <td>
                        <pre>&lt;concept id="2.16.840.1.113883.3.1937.777.37.2.11" effectiveDate="2019-06-05T00:00:00" type="<b>group</b>" statusCode="draft">
    &lt;name language="en-US"><b style="color: red;">A</b>&lt;/name>
    <b style="color: green;">&lt;!-- row 1 under group A --></b>
    &lt;concept id="2.16.840.1.113883.3.1937.777.37.2.6.<b style="color: red;">0</b>" effectiveDate="2019-06-05T00:00:00" type="item" statusCode="draft">
        &lt;name language="en-US"><b style="color: red;">AGE</b>&lt;/name>
        &lt;desc language="en-US">Date of birth&lt;/desc>
        &lt;valueDomain type="date">
            &lt;property timeStampPrecision="YMD!"/>
        &lt;/valueDomain>
    &lt;/concept>
&lt;/concept>
&lt;concept id="2.16.840.1.113883.3.1937.777.37.2.119" effectiveDate="2019-06-05T00:00:00" type="<b>group</b>" statusCode="draft">
    &lt;name language="en-US"><b style="color: red;">C</b>&lt;/name>
    <b style="color: green;">&lt;!-- row 2 under group C --></b>
    &lt;concept id="2.16.840.1.113883.3.1937.777.37.2.6.<b style="color: red;">18</b>" effectiveDate="2019-06-05T00:00:00" type="item" statusCode="draft">
        &lt;name language="en-US"><b style="color: red;">COMORB</b>&lt;/name>
        &lt;desc language="en-US">Have you been told by a doctor that you have any of the following?&lt;div class="supportingdefinition">Based upon the Self-administered Comorbidity Questionnaire (Sangha et al, 2003)&lt;/div>
        &lt;/desc>
        &lt;valueDomain type="code">
            &lt;conceptList id="2.16.840.1.113883.3.1937.777.37.2.6.18.20190605000000.0">
                &lt;concept id="2.16.840.1.113883.3.1937.777.37.2.6.18.20190605000000.1">
                    &lt;name language="en-US">0 = I have no other diseases&lt;/name>
                    &lt;desc language="en-US"/>
                &lt;/concept>
                &lt;concept id="2.16.840.1.113883.3.1937.777.37.2.6.18.20190605000000.2">
                    &lt;name language="en-US">1 = Heart disease (For example, angina, heart attack, or heart failure)&lt;/name>
                    &lt;desc language="en-US"/>
                &lt;/concept>
                &lt;concept id="2.16.840.1.113883.3.1937.777.37.2.6.18.20190605000000.3">
                    &lt;name language="en-US">2 = High blood pressure&lt;/name>
                    &lt;desc language="en-US"/>
                &lt;/concept>
            &lt;/conceptList>
        &lt;/valueDomain>
    &lt;/concept>
&lt;/concept>
                        </pre>
                    </td>
                </tr>
            </table>
            <xd:p>The column order is irrelevant. This routine works based on <xd:b>case sensitive</xd:b> column names. Some column contents are copied as-is, others are processed. Unsupported columns are reported. You may use <xd:ref name="erroroncolumncheck" type="parameter"/>=true to terminate on such errors. Default is report only.</xd:p>
            <xd:p>The last step is uploading the result to the ART-DECOR project file. This may only be one by someone with database privileges. Sending it that person directly is possible, but sending it to the group address support@art-decor.org with a request to upload it to the ICHOM project will do as well</xd:p>
            <xd:p><xd:b>Location</xd:b></xd:p>
            <xd:p>The location for the result at time of writing is <xd:a href="https://decor.nictiz.nl/art-decor/decor-datasets--ichom-">the ART-DECOR instance at Nictiz</xd:a>.</xd:p>
            <xd:p>-----------</xd:p>
            <xd:p><xd:b>License of this transform:</xd:b> This program is free software; you can redistribute it and/or modify it under the terms of the
                <xd:a href="http://www.gnu.org/copyleft/lesser.html">GNU Lesser General Public License</xd:a> as published by the Free Software Foundation; either version
                2.1 of the License, or (at your option) any later version.</xd:p>
            <xd:p><xd:b>Disclaimer for this transform:</xd:b> This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
                without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
                See the GNU Lesser General Public License for more details.</xd:p>
            <xd:p><xd:b>Revision History</xd:b></xd:p>
            <xd:ul>
                <xd:li>June 6, 2019 - 1.0.0beta1<br/>Initial public version based on talks with Mathias Blom, Boston Medical Group (BMG)</xd:li>
            </xd:ul>
        </xd:desc>
    </xd:doc>
    
    <xsl:output omit-xml-declaration="yes" indent="yes"></xsl:output>
    
    <xsl:param name="erroroncolumncheck">false</xsl:param>
    
    <xsl:variable name="now" select="'2019-07-17T00:00:00'"/>
    <xsl:variable name="now2" select="'2019-06-18T00:00:00'"/>
    <xsl:variable name="idproject" select="'2.16.840.1.113883.3.1937.777.37'"/>
    <xsl:variable name="iddatasettermbank" select="concat($idproject, '.1.1')"/>
    <xsl:variable name="iddatasetquestionnaires" select="concat($idproject, '.1.2')"/>
    <xsl:variable name="iddatasetstandardsets" select="concat($idproject, '.1.3')"/>
    <xsl:variable name="idconceptgroup1" select="concat($idproject, '.2.1')"/>
    <xsl:variable name="idconceptgroup2" select="concat($idproject, '.2.2')"/>
    <xsl:variable name="idconceptgroup3" select="concat($idproject, '.2.3')"/>
    <xsl:variable name="idconceptgroup4" select="concat($idproject, '.2.4')"/>
    <xsl:variable name="idconceptgroup5" select="concat($idproject, '.2.5')"/>
    <!-- What to use as prefix for concept with an ICHOMID -->
    <xsl:variable name="ichom-term-" select="concat($idproject, '.2.6')"/>
    <!-- What to use as prefix for concept without an ICHOMID (generates id here) -->
    <xsl:variable name="ichom-term-noichomid-" select="concat($idproject, '.2.7')"/>
    <!-- What to use as prefix for concept with an ICHOMID -->
    <xsl:variable name="ichom-term-consolidated-" select="concat($idproject, '.2.8')"/>
    <!-- What to use as prefix for concept without an ICHOMID (generates id here) -->
    <xsl:variable name="ichom-term-consolidated-noichomid-" select="concat($idproject, '.2.9')"/>
    <!-- What to use as prefix for concept with an ICHOMID in a questionnaire -->
    <xsl:variable name="ichom-quest-" select="concat($idproject, '.2.10')"/>
    <!-- What to use as prefix for concept without an ICHOMID in a questionnaire -->
    <xsl:variable name="ichom-quest-noichomid-" select="concat($idproject, '.2.11')"/>
    <!-- What to use as prefix for concept with an ICHOMID in a questionnaire -->
    <xsl:variable name="ichom-quest-consolidated-" select="concat($idproject, '.2.12')"/>
    <!-- What to use as prefix for concept without an ICHOMID in a questionnaire -->
    <xsl:variable name="ichom-quest-consolidated-noichomid-" select="concat($idproject, '.2.13')"/>
    <!-- What to use as prefix for concepts with an ICHOMID in standard sets -->
    <xsl:variable name="ichom-ss-" select="concat($idproject, '.2.12')"/>
    <!-- What to use as prefix for concepts without an ICHOMID in standard sets -->
    <xsl:variable name="ichom-ss-noichomid-" select="concat($idproject, '.2.13')"/>
    <!-- What to use as prefix for concepts with an ICHOMID in standard sets -->
    <xsl:variable name="ichom-ss-consolidated-" select="concat($idproject, '.2.12')"/>
    <!-- What to use as prefix for concepts without an ICHOMID in standard sets -->
    <xsl:variable name="ichom-ss-consolidated-noichomid-" select="concat($idproject, '.2.13')"/>
   
    
    <xsl:variable name="supportedColumns" as="element(col)+">
        <col name="variableid" required="true" datatype="xs:string">String. Name of the DECOR concept</col>
        <col name="item" required="false" datatype="xs:string">String. -- TODO, not currently handled</col>
        <col name="definition" required="false" datatype="xs:string">String. Is used as concept description with supportingdefinition</col>
        <col name="supportingdefinition" required="false" datatype="xs:string">String. Is used in a <div class="supportingoptions">...</div> in the concept description</col>
        <col name="type" required="false" datatype="xs:string">String. See function local:getDomainType for details</col>
        <col name="responseoptions" required="false" datatype="xs:string">String. Allows for semi-structured further specification of </col>
        <col name="tax_1" required="false" datatype="xs:string">String. Taxonomy level 1. Value nan (not a number) will be ignored</col>
        <col name="tax_2" required="false" datatype="xs:string">String. Taxonomy level 2. Value nan (not a number) will be ignored</col>
        <col name="tax_3" required="false" datatype="xs:string">String. Taxonomy level 3. Value nan (not a number) will be ignored</col>
        <col name="tax_4" required="false" datatype="xs:string">String. Taxonomy level 4. Value nan (not a number) will be ignored</col>
        <col name="tax_5" required="false" datatype="xs:string">String. Taxonomy level 5. Value nan (not a number) will be ignored</col>
        <col name="ICHOM_ID" required="true" datatype="xs:integer">Numeric. Final node of the OID that each concept will receive. Allows for reordering of the rows while keeping the OID stable across reruns of the transform. Shall be unqiue across all rows.</col>
        
        <col name="generic" required="false" datatype="xs:string">String. indicates whether a term or clinical concept touches more than one standard set, e.g. standard_set_specific.</col>
        <col name="questionnaire_abbreviation" required="false" datatype="xs:string">String. details the abbreviation of a questionnaire, that the term is part of, e.g. BREASTQBCT</col>
        <col name="disclaimer" required="false" datatype="xs:string">String. details a necessary disclaimer for terms that are part of questionnaires, e.g. Please contact license holder to obtain license for using the questionnaire. If you wish to participate in the ICHOM benchmarking program and have secured a license to use the tool, ICHOM will provide you with the technical specifications on how to collect the data.</col>
    </xsl:variable>
    
    <xsl:variable name="colIndex" as="element()*">
        <xsl:for-each select="/*/row[1]/col">
            <xsl:copy>
                <xsl:copy-of select="@name"/>
                <xsl:value-of select="position()"/>
            </xsl:copy>
        </xsl:for-each>
    </xsl:variable>
    
    <xd:doc>
        <xd:desc>Main logic. It all starts here. The structure based on the tax_1 to tax_5 column is built here, and each row is then sent off to the row template for concept creation</xd:desc>
    </xd:doc>
    <xsl:template match="/">
        <xsl:call-template name="checkColumns"/>
        
        <datasets xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:hl7="urn:hl7-org:v3" xsi:noNamespaceSchemaLocation="https://assets.art-decor.org/ADAR/rv/DECOR.xsd">
            <dataset id="{$iddatasettermbank}.1" effectiveDate="{$now}" statusCode="final">
                <name language="en-US">ICHOM Term Bank</name>
                <desc language="en-US">Term bank compiling terms as they exist in the various ICHOM sets until 2019. Each term has its origin set listed in the usage section. Many terms by the same name may exist and they even have equal definitions, but each is considered to be a unique term nonetheless. See the ICHOM Term Bank Consolidated for a more harmonized view.</desc>
                
                <xsl:for-each select="/*/row">
                    <xsl:apply-templates select="." mode="originalTerm"/>
                </xsl:for-each>
                
                <!--<xsl:for-each-group select="/*/row" group-by="col[position() = local:getColumn('tax_1')][not(. = 'nan' or . = '' or . = 'empty' or . = 'missing')]">
                    <xsl:variable name="tax_1" select="current-grouping-key()"/>
                    
                    <concept id="{$idconceptgroup1}{count(preceding-sibling::row) + 1}" effectiveDate="{$now}" type="group" statusCode="draft">
                        <name language="en-US"><xsl:value-of select="current-grouping-key()"/></name>
                        
                        <xsl:for-each-group select="current-group()" group-by="col[position() = local:getColumn('tax_2')][not(. = 'nan' or . = '' or . = 'empty' or . = 'missing')]">
                            <xsl:variable name="tax_2" select="current-grouping-key()"/>
                            
                            <concept id="{$idconceptgroup2}{count(preceding-sibling::row) + 1}" effectiveDate="{$now}" type="group" statusCode="draft">
                                <name language="en-US"><xsl:value-of select="current-grouping-key()"/></name>
                                
                                <!-\-<xsl:for-each select="current-group()">
                                    <xsl:apply-templates select="."/>
                                </xsl:for-each>-\->
                                
                                <xsl:for-each-group select="current-group()" group-by="col[position() = local:getColumn('tax_3')][not(. = 'nan' or . = '' or . = 'empty' or . = 'missing')]">
                                    <xsl:variable name="tax_3" select="current-grouping-key()"/>
                                    
                                    <concept id="{$idconceptgroup3}{count(preceding-sibling::row) + 1}" effectiveDate="{$now}" type="group" statusCode="draft">
                                        <name language="en-US"><xsl:value-of select="current-grouping-key()"/></name>
                                        
                                        <!-\-<xsl:for-each select="current-group()">
                                            <xsl:apply-templates select="."/>
                                        </xsl:for-each>-\->
                                        
                                        <xsl:for-each-group select="current-group()" group-by="col[position() = local:getColumn('tax_4')][not(. = 'nan' or . = '' or . = 'empty' or . = 'missing')]">
                                            <xsl:variable name="tax_4" select="current-grouping-key()"/>
                                            
                                            <concept id="{$idconceptgroup4}{count(preceding-sibling::row) + 1}" effectiveDate="{$now}" type="group" statusCode="draft">
                                                <name language="en-US"><xsl:value-of select="current-grouping-key()"/></name>
                                                
                                                <!-\-<xsl:for-each select="current-group()">
                                                    <xsl:apply-templates select="."/>
                                                </xsl:for-each>-\->
                                                
                                                <xsl:for-each-group select="current-group()" group-by="col[position() = local:getColumn('tax_5')][not(. = 'nan' or . = '' or . = 'empty' or . = 'missing')]">
                                                    <xsl:variable name="tax_5" select="current-grouping-key()"/>
                                                    
                                                    <concept id="{$idconceptgroup5}{count(preceding-sibling::row) + 1}" effectiveDate="{$now}" type="group" statusCode="draft">
                                                        <name language="en-US"><xsl:value-of select="current-grouping-key()"/></name>
                                                        
                                                        <xsl:for-each select="current-group()">
                                                            <xsl:apply-templates select="."/>
                                                        </xsl:for-each>
                                                    </concept>
                                                </xsl:for-each-group>
                                                <xsl:for-each select="current-group()[not(col[position() = local:getColumn('tax_5')][not(. = 'nan' or . = '' or . = 'empty' or . = 'missing')])]">
                                                    <xsl:apply-templates select="."/>
                                                </xsl:for-each>
                                            </concept>
                                        </xsl:for-each-group>
                                        <xsl:for-each select="current-group()[not(col[position() = local:getColumn('tax_4')][not(. = 'nan' or . = '' or . = 'empty' or . = 'missing')])]">
                                            <xsl:apply-templates select="."/>
                                        </xsl:for-each>
                                    </concept>
                                </xsl:for-each-group>
                                <xsl:for-each select="current-group()[not(col[position() = local:getColumn('tax_3')][not(. = 'nan' or . = '' or . = 'empty' or . = 'missing')])]">
                                    <xsl:apply-templates select="."/>
                                </xsl:for-each>
                            </concept>
                        </xsl:for-each-group>
                        <xsl:for-each select="current-group()[not(col[position() = local:getColumn('tax_2')][not(. = 'nan' or . = '' or . = 'empty' or . = 'missing')])]">
                            <xsl:apply-templates select="."/>
                        </xsl:for-each>
                    </concept>
                </xsl:for-each-group>-->
            </dataset>
            <dataset id="{$iddatasettermbank}.2" effectiveDate="{$now2}" statusCode="draft">
                <name language="en-US">ICHOM Term Bank Consolidated</name>
                <desc language="en-US">Term bank contained terms that consolidate compiling terms as they exist in the various ICHOM sets until 2019. Each term has its origin set listed in the usage section. Some terms have been unaltered, i.e. have only one parent term in the original term bank. Others have multiple parent terms. These are marked with a prefix [P] in the name.</desc>
                
                <xsl:for-each-group select="/*/row" group-by="col[@name = 'tax_1']">
                    <xsl:apply-templates select="current-group()[1]" mode="consolidatedTerm">
                        <xsl:with-param name="relationships" select="current-group()"/>
                    </xsl:apply-templates>
                </xsl:for-each-group>
            </dataset>
            
            <xsl:for-each-group select="/*/row/col[@name = 'standard_set']" group-by=".">
                <xsl:variable name="dsname">
                    <xsl:choose>
                        <xsl:when test="matches(current-group()[1], '_v\d+_\d+$')">
                            <xsl:value-of select="replace(current-group()[1], '_v\d+_\d+$', '')"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="current-group()[1]"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="dsversionLabel">
                    <xsl:choose>
                        <xsl:when test="matches(current-group()[1], '_v\d+_\d+$')">
                            <xsl:value-of select="replace(current-group()[1], '.*_v(\d+)_(\d+)$', '$1.$2')"/>
                        </xsl:when>
                    </xsl:choose>
                </xsl:variable>
                <dataset id="{$iddatasetstandardsets}.{position()}" effectiveDate="{$now}" statusCode="final">
                    <xsl:if test="not(empty($dsversionLabel))">
                        <xsl:attribute name="versionLabel" select="$dsversionLabel"/>
                    </xsl:if>
                    <name language="en-US">ICHOM <xsl:value-of select="$dsname"/></name>
                    <desc language="en-US"/>
                    <xsl:for-each select="current-group()/ancestor::row">
                        <xsl:variable name="conceptId">
                            <xsl:variable name="ichomid" select="col[position() = local:getColumn('ICHOM_ID')][not(. = 'nan' or . = '' or . = 'empty' or . = 'missing')]"/>
                            
                            <xsl:choose>
                                <xsl:when test="$ichomid castable as xs:integer">
                                    <xsl:value-of select="concat($ichom-ss-, '.', $ichomid)"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="concat($ichom-ss-noichomid-, '.', count(preceding-sibling::row) + 1)"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        <xsl:variable name="inheritConceptId">
                            <xsl:variable name="ichomid" select="col[position() = local:getColumn('ICHOM_ID')][not(. = 'nan' or . = '' or . = 'empty' or . = 'missing')]"/>
                            
                            <xsl:choose>
                                <xsl:when test="$ichomid castable as xs:integer">
                                    <xsl:value-of select="concat($ichom-term-, '.', $ichomid)"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="concat($ichom-term-noichomid-, '.', count(preceding-sibling::row) + 1)"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        <xsl:comment> <xsl:value-of select="col[position() = local:getColumn('tax_1')][not(. = 'nan' or . = '' or . = 'empty' or . = 'missing')]"/> </xsl:comment>
                        <concept id="{$conceptId}" effectiveDate="{$now}" type="item" statusCode="final">
                            <inherit ref="{$inheritConceptId}" effectiveDate="{$now}"/>
                        </concept>
                    </xsl:for-each>
                </dataset>
            </xsl:for-each-group>
        </datasets>
        
        
    </xsl:template>
    
    <xd:doc>
        <xd:desc>Row logic. Each row is turned into a concept with its inheritance</xd:desc>
        <xd:param name="relationships"/>
    </xd:doc>
    <xsl:template match="row" mode="consolidatedTerm">
        <xsl:param name="relationships" as="element(row)+"/>
        <xsl:variable name="conceptId">
            <xsl:variable name="ichomid" select="col[position() = local:getColumn('CONCEPT_ID')][not(. = 'nan' or . = '' or . = 'empty' or . = 'missing')]"/>
            
            <xsl:choose>
                <xsl:when test="$ichomid castable as xs:integer">
                    <xsl:value-of select="concat($ichom-term-consolidated-, '.', $ichomid)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat($ichom-term-consolidated-noichomid-, '.', count(preceding-sibling::row) + 1)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <concept id="{$conceptId}" effectiveDate="{$now}" type="item" statusCode="draft">
            <name language="en-US">
                <xsl:if test="count($relationships) gt 1">
                    <xsl:text>[P] </xsl:text>
                </xsl:if>
                <xsl:value-of select="col[position() = local:getColumn('tax_1')]"/>
            </name>
            <xsl:apply-templates select="." mode="doDescription"/>
            
            <xsl:apply-templates select="." mode="doSource"/>
            
            <xsl:for-each select="$relationships">
                <xsl:variable name="relatedConceptId">
                    <xsl:variable name="ichomid" select="col[position() = local:getColumn('ICHOM_ID')][not(. = 'nan' or . = '' or . = 'empty' or . = 'missing')]"/>
                    
                    <xsl:choose>
                        <xsl:when test="$ichomid castable as xs:integer">
                            <xsl:value-of select="concat($ichom-term-, '.', $ichomid)"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="concat($ichom-term-noichomid-, '.', count(preceding-sibling::row) + 1)"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <relationship type="REPL" ref="{$relatedConceptId}" flexibility="{$now}"/>
            </xsl:for-each>
            
            <xsl:apply-templates select="." mode="doOperationalization"/>
            
            <xsl:apply-templates select="col[position() = local:getColumn('type')]" mode="valueDomain">
                <xsl:with-param name="conceptId" select="$conceptId"/>
            </xsl:apply-templates>
        </concept>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>Row logic. Each row is turned into a concept with its properties</xd:desc>
    </xd:doc>
    <xsl:template match="row" mode="originalTerm">
        <xsl:variable name="conceptId">
            <xsl:variable name="ichomid" select="col[position() = local:getColumn('ICHOM_ID')][not(. = 'nan' or . = '' or . = 'empty' or . = 'missing')]"/>
            
            <xsl:choose>
                <xsl:when test="$ichomid castable as xs:integer">
                    <xsl:value-of select="concat($ichom-term-, '.', $ichomid)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat($ichom-term-noichomid-, '.', count(preceding-sibling::row) + 1)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <!--<xsl:variable name="conceptId" select="concat($idconcept, '.', count(preceding-sibling::row) + 1)"/>-->
        <xsl:variable name="supportingdefinition" select="col[position() = local:getColumn('supportingdefinition')][not(. = 'nan' or . = '' or . = 'empty' or . = 'missing')]"/>
        
        <concept id="{$conceptId}" effectiveDate="{$now}" type="item" statusCode="final">
            <name language="en-US"><xsl:value-of select="col[position() = local:getColumn('tax_1')]"/></name>
            <xsl:apply-templates select="." mode="doDescription"/>
            
            <xsl:apply-templates select="." mode="doSource"/>
            
            <xsl:apply-templates select="." mode="doOperationalization"/>
            
            <xsl:apply-templates select="col[position() = local:getColumn('type')]" mode="valueDomain">
                <xsl:with-param name="conceptId" select="$conceptId"/>
            </xsl:apply-templates>
        </concept>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>Pulls out item, definition and supportingdefinition to create a complex desc element</xd:desc>
    </xd:doc>
    <xsl:template match="row" mode="doDescription">
        <xsl:variable name="supportingdefinition" select="col[position() = local:getColumn('supportingdefinition')][not(. = 'nan' or . = '' or . = 'empty' or . = 'missing')]"/>
        
        <desc language="en-US">
            <div><b>Item: </b><xsl:value-of select="col[position() = local:getColumn('item')]"/></div>
            <div><b>Definition: </b><xsl:value-of select="col[position() = local:getColumn('definition')]"/></div>
            <xsl:if test="not(empty($supportingdefinition))">
                <div class="supportingdefinition">
                    <b>Supporting Definition: </b>
                    <xsl:value-of select="$supportingdefinition"/>
                </div>
            </xsl:if>
        </desc>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>Pulls out disclaimer and questionnaire_abbreviation to create a complex source element</xd:desc>
    </xd:doc>
    <xsl:template match="row" mode="doSource">
        <xsl:variable name="disclaimer" select="col[position() = local:getColumn('disclaimer')][not(. = 'nan' or . = '' or . = 'empty' or . = 'missing')]"/>
        <xsl:variable name="questionnaire" select="col[position() = local:getColumn('questionnaire_abbreviation')][not(. = 'nan' or . = '' or . = 'empty' or . = 'missing')]"/>
        
        <xsl:if test="$questionnaire | $disclaimer">
            <source language="en-US">
                <xsl:value-of select="$questionnaire"/>
                <xsl:if test="$disclaimer">
                    <p>
                        <b>Disclaimer</b>
                        <div class="disclaimer">
                            <xsl:value-of select="$disclaimer"/>
                        </div>
                    </p>
                </xsl:if>
            </source>
        </xsl:if>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>Pulls out generic to create a complex operationalization element</xd:desc>
    </xd:doc>
    <xsl:template match="row" mode="doOperationalization">
        <xsl:variable name="usagetype" select="col[position() = local:getColumn('generic')][not(. = 'nan' or . = '' or . = 'empty' or . = 'missing')]"/>
        
        <xsl:if test="$usagetype">
            <operationalization language="en-US">
                <xsl:text>Type of usage: </xsl:text>
                <xsl:value-of select="$usagetype"/>
            </operationalization>
        </xsl:if>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>Produces the valueDomain with its @type for a concept, and if relevant includes additional properties and discrete response option concepts.</xd:desc>
        <xd:param name="conceptId">The concept id is used as the basis for conceptList ids and any child concepts of that conceptList (responseoptions)</xd:param>
    </xd:doc>
    <xsl:template match="col" mode="valueDomain">
        <xsl:param name="conceptId"/>
        <xsl:variable name="conceptListId" select="concat($conceptId, '.', replace(substring(string($now), 1, 10), '[^\d]', ''), '000000')"/>
        <xsl:variable name="valuedomainType" select="local:getDomainType(., 'false')"/>
        <xsl:variable name="valueDomainOptions" select="ancestor-or-self::row[1]/col[position() = local:getColumn('responseoptions')]"/>
        <valueDomain>
            <xsl:attribute name="type" select="$valuedomainType"/>
            <xsl:if test="$valuedomainType = ('code', 'ordinal')">
                <conceptList id="{$conceptListId}.0">
                    <xsl:apply-templates select="$valueDomainOptions" mode="valueDomainOptions">
                        <xsl:with-param name="conceptListId" select="$conceptListId"/>
                    </xsl:apply-templates>
                </conceptList>
            </xsl:if>
            <xsl:choose>
                <xsl:when test="$valuedomainType = ('date', 'datetime') and $valueDomainOptions = 'YYYY'">
                    <property timeStampPrecision="Y!"/>
                </xsl:when>
                <xsl:when test="$valuedomainType = ('date', 'datetime') and matches($valueDomainOptions, '^(YYYY|MM).?(YYYY|MM)$')">
                    <property timeStampPrecision="YM!"/>
                </xsl:when>
                <xsl:when test="$valuedomainType = ('date', 'datetime') and matches($valueDomainOptions, '^(YYYY|MM|DD).?(YYYY|MM|DD).?(YYYY|MM|DD)$')">
                    <property timeStampPrecision="YMD!"/>
                </xsl:when>
            </xsl:choose>
        </valueDomain>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>Parses the value of the responseoptions cell into a list of DECOR valueDomain concepts by accepting each newline (\n) separated line as a concept.</xd:desc>
        <xd:param name="conceptListId">The conceptList base id is the actual DECOR conceptList id, without the final node 0. Every concept get a sequential leaf node, 1 2 3 etc.</xd:param>
    </xd:doc>
    <xsl:template match="col" mode="valueDomainOptions">
        <xsl:param name="conceptListId"/>
        <xsl:variable name="options" as="xs:string*">
            <xsl:choose>
                <xsl:when test="matches(., '\n')">
                    <xsl:for-each select="tokenize(., '\n')">
                        <xsl:value-of select="."/>
                    </xsl:for-each>
                </xsl:when>
                <xsl:when test="lower-case(.) = '0 = none 1 = 1-6 2 = 7-14 3 = >14'">
                    <xsl:text>0 = None</xsl:text>
                    <xsl:text>1 = 1-6</xsl:text>
                    <xsl:text>2 = 7-14</xsl:text>
                    <xsl:text>3 = >14</xsl:text>
                </xsl:when>
                <xsl:when test="lower-case(.) = '0 = no 1 = yes 999 = unknown'">
                    <xsl:text>0 = No</xsl:text>
                    <xsl:text>1 = Yes</xsl:text>
                    <xsl:text>999 = Unknown</xsl:text>
                </xsl:when>
                <xsl:when test="lower-case(.) = '0 = no 1 = yes'">
                    <xsl:text>0 = No</xsl:text>
                    <xsl:text>1 = Yes</xsl:text>
                </xsl:when>
                <xsl:when test="lower-case(.) = '0 = none 1 = primary 2 = secondary 3 = tertiary'">
                    <xsl:text>0 = None</xsl:text>
                    <xsl:text>1 = Primary</xsl:text>
                    <xsl:text>2 = Secondary</xsl:text>
                    <xsl:text>3 = Tertiary</xsl:text>
                </xsl:when>
                <xsl:when test="lower-case(.) = '1 = male 2 = female 999 = unknown'">
                    <xsl:text>1 = Male 2 = Female 999 = Unknown</xsl:text>
                </xsl:when>
                <xsl:when test="lower-case(.) = '1 = centimeters 2 = inches'">
                    <xsl:text>1 = centimeters</xsl:text>
                    <xsl:text>2 = inches</xsl:text>
                </xsl:when>
                <xsl:when test="lower-case(.) = '0 = never 1 = former 2 = current'">
                    <xsl:text>0 = Never</xsl:text>
                    <xsl:text>1 = Former</xsl:text>
                    <xsl:text>2 = Current</xsl:text>
                </xsl:when>
                <xsl:when test="lower-case(.) = '1 = kilograms 2 = pounds'">
                    <xsl:text>1 = kilograms</xsl:text>
                    <xsl:text>2 = pounds</xsl:text>
                </xsl:when>
                <xsl:when test="lower-case(.) = '1 = an acute myocardial infarction 2 = sudden cardiac death 3 = heart failure  4 = stroke 5 = cardiovascular procedure  6 = cardiovascular haemorrhage 7 = other cardiovascular causes e.g. peripheral arterial disease 8 = other cause of death (not cardiovascular) 999 = unknown'">
                    <xsl:text>1 = an acute myocardial infarction</xsl:text>
                    <xsl:text>2 = sudden cardiac death</xsl:text>
                    <xsl:text>3 = heart failure</xsl:text>
                    <xsl:text>4 = stroke</xsl:text>
                    <xsl:text>5 = cardiovascular procedure</xsl:text>
                    <xsl:text>6 = cardiovascular haemorrhage</xsl:text>
                    <xsl:text>7 = other cardiovascular causes e.g. peripheral arterial disease</xsl:text>
                    <xsl:text>8 = Other cause of death (not cardiovascular)</xsl:text>
                    <xsl:text>999 = Unknown</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="."/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:for-each select="$options">
            <concept id="{$conceptListId}.{position()}">
                <name language="en-US"><xsl:value-of select="."/></name>
                <desc language="en-US"/>
            </concept>
        </xsl:for-each>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>Processes the value of the type cell into a <xd:a href="https://art-decor.org/mediawiki/index.php?title=DECOR-dataset">DECOR supported datatype</xd:a></xd:desc>
        <xd:param name="in">The value of the Type column</xd:param>
        <xd:param name="emptyifunsupported">true|false</xd:param>
    </xd:doc>
    <xsl:function name="local:getDomainType" as="xs:string?">
        <xsl:param name="in"/>
        <xsl:param name="emptyifunsupported"/>
        
        <xsl:choose>
            <xsl:when test="lower-case($in) = 'date'">date</xsl:when>
            <xsl:when test="lower-case($in) = 'date by yyyy'">date</xsl:when>
            <xsl:when test="lower-case($in) = 'date by yyy'">date</xsl:when>
            <xsl:when test="lower-case($in) = 'yyyymmmdd'">date</xsl:when>
            <xsl:when test="lower-case($in) = 'string'">string</xsl:when>
            <xsl:when test="lower-case($in) = 'text'">text</xsl:when>
            <xsl:when test="starts-with(lower-case($in), 'date by dd/mm/yyyy')">date</xsl:when>
            <xsl:when test="starts-with(lower-case($in), 'date by mm/yyyy')">date</xsl:when>
            <xsl:when test="starts-with(lower-case($in), 'yyyymmd')">date</xsl:when>
            <xsl:when test="starts-with(lower-case($in), 'numeric')">decimal</xsl:when>
            <xsl:when test="starts-with(lower-case($in), 'multiple answer')">code</xsl:when>
            <xsl:when test="starts-with(lower-case($in), 'single answer')">code</xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="$emptyifunsupported = 'true'"/>
                    <xsl:otherwise>text</xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xd:doc>
        <xd:desc>The initial row is used to index columns based on their name. All subsequent rows are expected to have columns in the same order. Returns the column index.</xd:desc>
        <xd:param name="colName">Name of the column to get.</xd:param>
    </xd:doc>
    <xsl:function name="local:getColumn" as="xs:integer?">
        <xsl:param name="colName" as="xs:string"/>
        
        <xsl:value-of select="$colIndex[@name = $colName]/xs:integer(text())"/>
    </xsl:function>
    
    <xd:doc>
        <xd:desc>Checks unsupported / missing columns and outputs some statistics</xd:desc>
    </xd:doc>
    <xsl:template name="checkColumns">
        <xsl:variable name="outputlevel">
            <xsl:choose>
                <xsl:when test="$erroroncolumncheck = 'true'">ERROR</xsl:when>
                <xsl:otherwise>WARNING</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:variable name="representedColumnNames" select="distinct-values(//col/@name)" as="xs:string*"/>
        
        <xsl:variable name="unsupportedColumnNames" select="$representedColumnNames[not(. = $supportedColumns/@name)]" as="xs:string*"/>
        <xsl:variable name="missingColumnNames" select="$supportedColumns[@required = 'true'][not(@name = $representedColumnNames)]/@name" as="xs:string*"/>
        <xsl:variable name="requiredEmptyColumns" select="//row[col[@name = $supportedColumns[@required = 'true']/@name][. = ('nan', '', 'empty', 'missing')]]"/>
        <xsl:variable name="unsupportedDatatypes" select="//row[empty(local:getDomainType(col[@name = 'type'], 'true'))]"/>
        
        <xsl:message>Checking '<xsl:value-of select="tokenize(document-uri(.), '/')[last()]"/>' at <xsl:value-of select="current-dateTime()"/> ...</xsl:message>
        <xsl:message>    @version   : <xsl:value-of select="/*/@version"/></xsl:message>
        <xsl:message>    @exportDate: <xsl:value-of select="/*/@exportDate"/></xsl:message>
        <xsl:message>    @exportUser: <xsl:value-of select="/*/@exportUser"/></xsl:message>
        <xsl:message>    # of rows  : <xsl:value-of select="count(/*/row)"/></xsl:message>
        <xsl:message>    </xsl:message>
        
        <xsl:choose>
            <xsl:when test="empty($unsupportedColumnNames)">
                <xsl:message>INFO No unsupported columns found.</xsl:message>
            </xsl:when>
            <xsl:otherwise>
                <xsl:message><xsl:value-of select="$outputlevel"/> Unsupported columns found: <xsl:value-of select="$unsupportedColumnNames"/></xsl:message>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:choose>
            <xsl:when test="empty($missingColumnNames)">
                <xsl:message>INFO No missing required columns.</xsl:message>
            </xsl:when>
            <xsl:otherwise>
                <xsl:message><xsl:value-of select="$outputlevel"/> Missing required columns: <xsl:value-of select="$missingColumnNames"/></xsl:message>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:choose>
            <xsl:when test="empty($requiredEmptyColumns)">
                <xsl:message>INFO No empty required columns.</xsl:message>
            </xsl:when>
            <xsl:otherwise>
                <xsl:message><xsl:value-of select="$outputlevel"/> Empty required columns found:</xsl:message>
                <xsl:for-each select="$requiredEmptyColumns">
                    <xsl:message>- row ICHOM_ID: <xsl:value-of select="col[@name = 'ICHOM_ID']"/>, variableid: <xsl:value-of select="col[@name = 'variableid']"/>, empty columns: <xsl:value-of select="string-join(col[@name = $supportedColumns[@required = 'true']/@name][. = ('nan', '', 'empty')]/@name, ', ')"/></xsl:message>
                </xsl:for-each>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:choose>
            <xsl:when test="empty($unsupportedDatatypes)">
                <xsl:message>INFO No items with unsupported datatypes.</xsl:message>
            </xsl:when>
            <xsl:otherwise>
                <xsl:message><xsl:value-of select="$outputlevel"/> Items with unsupported datatypes found:</xsl:message>
                <xsl:for-each select="$unsupportedDatatypes">
                    <xsl:message>- row ICHOM_ID: <xsl:value-of select="col[@name = 'ICHOM_ID']"/>, variableid: <xsl:value-of select="col[@name = 'variableid']"/>, datatype claimed: "<xsl:value-of select="col[@name = 'type']"/>"</xsl:message>
                </xsl:for-each>
            </xsl:otherwise>
        </xsl:choose>
        
        <xsl:if test="$erroroncolumncheck = 'true' and not(empty(($unsupportedColumnNames, $missingColumnNames)))">
            <xsl:message>    </xsl:message>
            <xsl:message terminate="yes">Parameter erroroncolumncheck=true and problems found. Processing halted.</xsl:message>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>