package nl.nictiz.lucene;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

/** Index tokens by first and last character and by length; prefer longer tokens to split.
 *
 * @author Feikje Hielkema
 *
 */
public class CompoundPartIndex
{
	private static Logger log = Logger.getLogger(CompoundPartIndex.class);
	private Map<Character,Map<Integer,Set<String>>> firstLetterIndex = new HashMap<Character,Map<Integer,Set<String>>>();
	private Map<Character,Map<Integer,Set<String>>> lastLetterIndex = new HashMap<Character,Map<Integer,Set<String>>>();

	public CompoundPartIndex(Collection<String> terms)
	{
		for (String t : terms)
		{
			String term = SpellingErrorFilter.rewriteString(t);
			Integer length = term.length();
			Character first = term.charAt(0);
			Character last = term.charAt(length - 1);
			
			addToIndex(first, length, term, this.firstLetterIndex);
			addToIndex(last, length, term, this.lastLetterIndex);
		}
	}
	
	private void addToIndex(Character c, Integer length, String term, Map<Character,Map<Integer,Set<String>>> index)
	{
		if (index.containsKey(c))
		{
			Map<Integer,Set<String>> map = index.get(c);
			if (map.containsKey(length))
				map.get(length).add(term);
			else
			{
				Set<String> set = new HashSet<String>();
				set.add(term);
				map.put(length, set);
			}
		}
		else
		{
			Map<Integer,Set<String>> map = new HashMap<Integer,Set<String>>();
			Set<String> set = new HashSet<String>();
			set.add(term);
			map.put(length, set);
			index.put(c, map);
		}
	}
	
	public List<String> split(String token)
	{		
		int length = token.length();
		int maxLength = length - 1; //3;		//NB. allowing the latter part to be 1 character long may have consequences for efficiency!
		Character first = token.charAt(0);		//in +hematom +a, the 'a' is going to match with an awful lot of descriptions.
		Map<Integer,Set<String>> firstMap = this.firstLetterIndex.get(first);
				
		for (int i = maxLength; i > 2; i--)
		{	//find the longest known compound part to split by, providing the remaining part is at least 3 characters long
			List<String> split = split(token, firstMap, i);
			if (split != null)
				return split;
		}

		return null;
	}
		
	private List<String> split(String token, Map<Integer,Set<String>> map, Integer length)
	{
		if ((map == null) || (!map.containsKey(length)))
			return null;

		List<String> result = new ArrayList<String>();
		Set<String> set = map.get(length);
		for (String str : set)
		{
			if (!token.startsWith(str))
				continue;
				
			if (log.isDebugEnabled())
				log.debug(str);
			
			result.add(str);
			String other = token.substring(length);
			result.add(other);
			
			if (other.startsWith("s"))
			{
				result.add(other.substring(1));
				result.add(str + "s");
			}
			else if (other.startsWith("en"))
			{
				result.add(other.substring(2));
				result.add(other + "en");
			}
			
			return result;
		}
		
		return null;
	}
}