package nl.nictiz.lucene;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;

/** Uses a list of standard compound parts to split tokens.
 * It is sufficient for a token to start or end with such a part - 
 * it does not have to consist of two parts!
 * 
 * @author Feikje Hielkema
 *
 */
public class CompoundSplitterFilter extends TokenFilter
{
	private static Logger log = Logger.getLogger(CompoundSplitterFilter.class);
	
	private CompoundPartIndex index;
	private CharTermAttribute termAttribute;
	private final PositionIncrementAttribute posIncAtt = addAttribute(PositionIncrementAttribute.class);
	private State savedState;
	private List<String> extraTokens = new ArrayList<String>();
		
	public CompoundSplitterFilter(TokenStream input) throws IOException
	{
		super(input);
		this.termAttribute = addAttribute(CharTermAttribute.class);
		
		InputStream in = this.getClass().getClassLoader().getResourceAsStream("compoundparts-2.txt");
		List<String> terms = IOUtils.readLines(in);
		this.index = new CompoundPartIndex(terms);
		
		if (log.isDebugEnabled())
			log.debug("########################## " + terms.size());
	}

	@Override
	public boolean incrementToken() throws IOException
	{
		if (log.isDebugEnabled())
			log.debug(extraTokens);
		
		if (!extraTokens.isEmpty())
		{
			restoreState(savedState);
			posIncAtt.setPositionIncrement(0);
			termAttribute.setEmpty().append(extraTokens.remove(0));
			savedState = captureState();
			return true;
		}
			
		if (!input.incrementToken())
			return false;
		
		String term = new String(termAttribute.buffer(), 0, termAttribute.length());
		List<String> parts = index.split(term);
		
		if ((parts != null) && (!parts.isEmpty()))
		{
			if (log.isDebugEnabled())
				log.debug(parts);
			
			String part = parts.get(0);
			termAttribute.setEmpty();
			termAttribute.copyBuffer(part.toCharArray(), 0, part.length());
			
			for (int i = 1; i < parts.size(); i++)
				extraTokens.add(parts.get(i));

	        savedState = captureState();
		}
		
		return true;
	}
}
