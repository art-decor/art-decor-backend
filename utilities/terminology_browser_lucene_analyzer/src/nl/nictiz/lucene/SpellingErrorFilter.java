package nl.nictiz.lucene;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;

public class SpellingErrorFilter extends TokenFilter
{
	private static Logger log = Logger.getLogger(SpellingErrorFilter.class);
	private CharTermAttribute termAttribute;
	
	private static Map<String,String> substitutions = new HashMap<String,String>(); //, abbreviations = new HashMap<String,String>();
	private static Set<String> latinEndings = new HashSet<String>();
	private final boolean stem, abbreviate;
	
	private List<String> extraTokens = new ArrayList<String>();
	private final PositionIncrementAttribute posIncAtt = addAttribute(PositionIncrementAttribute.class);
	private State savedState;
	
	static
	{
		substitutions.put("ph", "f");
		substitutions.put("th", "t");
		substitutions.put("rh", "r");
		substitutions.put("k", "c");
		substitutions.put("ae", "e");
		
		//nieuw:
		substitutions.put("oe", "e");		
		substitutions.put("ue", "u");		
//		substitutions.put("pt", "ps");	
		substitutions.put("ie", "y");
		
		latinEndings.add("us");
//		latinEndings.add("es");
		latinEndings.add("is");
	}
	
	public SpellingErrorFilter(TokenStream input, boolean stem, boolean abbreviate)
	{
		super(input);
		this.stem = stem;
		this.abbreviate = abbreviate;
		this.termAttribute = addAttribute(CharTermAttribute.class);
	}

	@Override
	public boolean incrementToken() throws IOException
	{
		if (!extraTokens.isEmpty())
		{
			restoreState(savedState);
			posIncAtt.setPositionIncrement(0);
			termAttribute.setEmpty().append(extraTokens.remove(0));
			savedState = captureState();
		}
		else if (!input.incrementToken())
			return false;
		
		String term = new String(termAttribute.buffer(), 0, termAttribute.length());	//do not change spelling if term is an acronym
		String newTerm = NictizLowercaseFilter.isAcronym(term) ? term.toLowerCase() : rewrite(term);
		if (log.isDebugEnabled())
			log.debug(term + "\t" + newTerm);
		
		if (!newTerm.equals(term))
		{
			termAttribute.setEmpty();
			termAttribute.copyBuffer(newTerm.toCharArray(), 0, newTerm.length());
			
			if (!extraTokens.isEmpty())
		        savedState = captureState();
		}
		
		return true;
	}
	
	private String rewrite(String input)
	{
		if (input.isEmpty())
			return input;
		
		if (abbreviate)				//first replace abbreviations with full spelling, so they can then receive the rewrite
			input = abbreviate(input);
		
		input = rewriteString(input);	//then perform rewrite to catch frequent spelling mistakes
		
		if (stem)					//finally stem
			input = stem(input);	//TODO: add both stemmed and original version with OR?
		
		return input;
	}
	
	/** Rewrites the input to a 'simpler' sort of spelling,
	 * so frequently made spelling errors are compensated for.
	 */
	public static String rewriteString(String input)
	{
		StringBuffer sb = new StringBuffer();
		sb.append(input.charAt(0));
		for (int i = 1; i < input.length(); i++)
		{	//replace all double letters with single (rr -> r, cc -> c, tt -> t, ee -> e)
			char current = input.charAt(i);
			if (current != sb.charAt(sb.length() - 1))
				sb.append(current);
		}
		input = sb.toString();		
		
		for (String key : substitutions.keySet())
		{
			if (input.contains(key))
				input = input.replaceAll(key, substitutions.get(key));
		}
		return input;
	}
	
	private String abbreviate(String input)
	{
		if (input.equals("#"))
			return "fracture";
		else if (input.startsWith("#"))
		{
			extraTokens.add("fracture");
			return input.substring(1);
		}
		else if (input.endsWith("#"))
		{
			extraTokens.add("fracture");
			return input.substring(0, input.length() - 1);
		}
		else
			return input;
	}
	
	private String stem(String input)
	{
		int length = input.length();
		if (length <= 3)
			return input;
		
		String end = input.substring(length - 2, length);
		if (latinEndings.contains(end))
			return input.substring(0, length - 2);
		
		char last = input.charAt(length - 1);
		if ((last == 's') || (last == 'i') || (last == 'a') || (last == 'e') || last == 'u')
			return input.substring(0, length - 1);
		
		if (input.equalsIgnoreCase("children"))
			return input.substring(0, length - 3);
		
		return input;
	}
}
