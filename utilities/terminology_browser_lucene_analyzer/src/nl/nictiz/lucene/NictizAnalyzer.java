package nl.nictiz.lucene;


import java.io.IOException;
import java.io.Reader;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.miscellaneous.ASCIIFoldingFilter;
import org.apache.lucene.analysis.pattern.PatternTokenizer;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.util.Version;


public class NictizAnalyzer extends Analyzer
{
	private static Logger log = Logger.getLogger(NictizAnalyzer.class);
	public static enum Lowercase {All, FirstChar, Special, None};
	
	private Version version;
	private Lowercase lowercase;
	private final boolean stem, abbreviate, splitCompounds;
	private final Pattern pattern;
	
	public NictizAnalyzer(Version version, Lowercase lowercase, boolean stem, boolean abbreviate, boolean splitCompounds)
	{
		super();
		
		this.version = version;
		this.lowercase = lowercase;
		this.stem = stem;
		this.abbreviate = abbreviate;
		this.splitCompounds = splitCompounds;
		this.pattern = Pattern.compile("[\\s-!\"$&()+,./:;=?\\[\\]^_`{|}~\\\\]");
	}
	
	public NictizAnalyzer(Version version, Pattern tokenisePattern, boolean stem, boolean abbreviate, boolean splitCompounds)
	{
		super();
		
		this.version = version;
		this.pattern = tokenisePattern;
		this.lowercase = Lowercase.FirstChar;
		this.stem = stem;
		this.abbreviate = abbreviate;
		this.splitCompounds = splitCompounds;
	}
	
	public NictizAnalyzer(Version version, boolean stem, boolean abbreviate, boolean splitCompounds)
	{
		this(version, Lowercase.Special, stem, abbreviate, splitCompounds);
	}
	
	public NictizAnalyzer()
	{
		this(Version.LUCENE_44, Lowercase.Special, false, true, true);
	}
		
	public NictizAnalyzer(Version version)
	{
		this(version, Lowercase.Special, false, true, true);
	}

	@Override
	protected TokenStreamComponents createComponents(String field, Reader reader)
	{
		final Tokenizer source = new PatternTokenizer(reader, pattern, -1);
	    TokenStream sink = new StandardFilter(version, source);
	    
	    switch (lowercase)
	    {
	    	case All: sink = new LowerCaseFilter(version, sink); break;
	    	case FirstChar: sink = new FirstCharLowercaseFilter(sink); break;
	    	case Special: sink = new NictizLowercaseFilter(sink); break;
	    	default: break;
	    }
	
	    sink = new ASCIIFoldingFilter(sink);				//remove diacrits	
	    sink = new SpellingErrorFilter(sink, stem, abbreviate);		//change to simpler spelling
	    
	    try
	    {	//split compound words into parts
	    	if (splitCompounds)
	    		sink = new CompoundSplitterFilter(sink);
	    }
	    catch (IOException e)
	    {
	    	log.error("Could not initialise CompoundSplitterFilter: " + e.getMessage());
	    }
	    
	    return new TokenStreamComponents(source, sink);
	}
}
