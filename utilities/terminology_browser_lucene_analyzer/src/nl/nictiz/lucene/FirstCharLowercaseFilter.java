package nl.nictiz.lucene;


import java.io.IOException;

import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

public class FirstCharLowercaseFilter extends TokenFilter
{
	private CharTermAttribute termAttribute;
	
	public FirstCharLowercaseFilter(TokenStream input)
	{
		super(input);
		this.termAttribute = addAttribute(CharTermAttribute.class);
	}

	@Override
	public boolean incrementToken() throws IOException
	{
		if (!input.incrementToken())
			return false;
		
		String term = new String(termAttribute.buffer(), 0, termAttribute.length());
		if (term.isEmpty())
			return true;
		
		char c = term.charAt(0);
		if (Character.isLowerCase(c))
			return true;
		
		String newTerm = Character.toLowerCase(c) + term.substring(1);
		termAttribute.setEmpty();
		termAttribute.copyBuffer(newTerm.toCharArray(), 0, newTerm.length());
		return true;
	}
}