package nl.nictiz.lucene;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.PrefixQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.util.Version;

/**	NictizQueryParser parses the input using an analyzer, then
 * changes all clauses to PrefixQueries.
 * 
 * @author Feikje Hielkema
 * @version 1.0 07-03-2014
 */
public class NictizQueryParser extends QueryParser
{		
	public NictizQueryParser(Version matchVersion, String field, Analyzer analyzer)
	{
		super(matchVersion, field, analyzer);
	}

	@Override
	public Query parse(String searchTerm) throws ParseException
	{
		Query query = super.parse(searchTerm);	
		query = changeToPrefixQuery(query);
		return query;
	}
	
	private Query changeToPrefixQuery(Query query)
	{
		if (query instanceof BooleanQuery)
		{	//Replace each clause of the BooleanQuery with a PrefixQuery, using recursion
			BooleanQuery result = new BooleanQuery();
			for (BooleanClause clause : ((BooleanQuery) query).getClauses())
			{
				Query copy = changeToPrefixQuery(clause.getQuery());
				result.add(new BooleanClause(copy, BooleanClause.Occur.MUST));	//clause.getOccur()));
			}
			
			return result;
		}

		if (query instanceof TermQuery)
		{	//replace this TermQuery with a PrefixQuery
			Term term = ((TermQuery) query).getTerm();
			return new PrefixQuery(term);
		}
		
		return query;
	} 
}
