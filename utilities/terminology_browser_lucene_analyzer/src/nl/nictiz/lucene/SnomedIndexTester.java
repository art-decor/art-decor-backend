package nl.nictiz.lucene;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.IntField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;

public class SnomedIndexTester
{
	private static Logger log = Logger.getLogger(SnomedIndexTester.class);
	
	public static final Version version = Version.LUCENE_44;
	public static final String snomedIndex = "snomedIndex";
	
	private Set<String> activeConcepts = new HashSet<String>();
	private NictizAnalyzer analyzer = new NictizAnalyzer();
	private IndexReader reader;
	private IndexSearcher searcher;

	public void initActiveConcepts(File... conceptFiles) throws IOException
	{
		for (File conceptFile : conceptFiles)
		{
			List<String> lines = FileUtils.readLines(conceptFile);	
			for (int i = 1; i < lines.size(); i++)
			{
				String[] line = lines.get(i).split("\t");
				if (line[2].equals("1"))
					activeConcepts.add(line[0]);
			}
		}
	}
	
	public void createIndex(File... descriptionFiles) throws IOException
	{
		Directory index = new SimpleFSDirectory(new File(snomedIndex));
		IndexWriterConfig config = new IndexWriterConfig(version, analyzer);	//if an index already exists, it will be overwritten
		config.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
		IndexWriter writer = new IndexWriter(index, config);
		
		for (File file : descriptionFiles)
		{
			List<String> lines = FileUtils.readLines(file);						//load all descriptions
			for (int i = 1; i < lines.size(); i++)
			{
				String[] line = lines.get(i).split("\t");
				if (line[2].equals("0") || (!activeConcepts.contains(line[4])))	//skip inactive concepts and descriptions
					continue;
				
				Document doc = new Document();			//add the synonym as new entry, with retrieveable fields for id, fsn, synonym and length
				doc.add(new StringField(IndexCreator.idField, line[4], Field.Store.YES));
				doc.add(new TextField(IndexCreator.termField, line[7], Field.Store.YES));		//this is the only field we search on
				doc.add(new IntField(IndexCreator.lengthField, line[7].length(), Field.Store.YES));
				writer.addDocument(doc);
			}	
		}
			
		writer.close();
	}
	
	public void openIndex() throws IOException
	{
		Directory index = new SimpleFSDirectory(new File(snomedIndex));
		reader = DirectoryReader.open(index);
		searcher = new IndexSearcher(reader);
	}
		
	public List<Document> testIndex(String searchTerm, int maxResults) throws ParseException, IOException
	{
		NictizQueryParser parser = new NictizQueryParser(IndexCreator.version, IndexCreator.termField, analyzer);
		Query query = parser.parse(searchTerm);
		return search(query, maxResults);
	}
	
	private List<Document> search(Query query, int maxResults) throws IOException
	{
		log.info(query.toString());
		TopDocs docs = searcher.search(query, maxResults * 100);
		Map<Document,Float> map = new HashMap<Document,Float>();
		
		for (int i = 0; i < docs.scoreDocs.length; i++)
			map.put(searcher.doc(docs.scoreDocs[i].doc), docs.scoreDocs[i].score);
				
		List<Document> results = new ArrayList<Document>(map.keySet());
		Collections.sort(results, new LengthComparator(map));
		
		Set<String> unique = new HashSet<String>();
		for (Iterator<Document> it = results.iterator(); it.hasNext(); )
		{	//remove all double results
			Document doc = it.next();
			String id = doc.get(IndexCreator.idField);
			if (unique.contains(id))
				it.remove();
			else
				unique.add(id);
			
			if (unique.size() >= maxResults)
				break;
		}
				
		if (results.size() < maxResults)
			return results;
		return results.subList(0, maxResults);
	}
		
	private class LengthComparator implements Comparator<Document>
	{
		private Map<Document,Float> map;
		
		public LengthComparator(Map<Document,Float> map)
		{
			this.map = map;
		}
		
		@Override
		public int compare(Document doc1, Document doc2)
		{
			float f1 = map.get(doc1), f2 = map.get(doc2);
			int compare = Float.compare(f2, f1);
			if (compare != 0)
				return compare;
			
			int l1 = Integer.parseInt(doc1.get(IndexCreator.lengthField));
			int l2 = Integer.parseInt(doc2.get(IndexCreator.lengthField));
			return Integer.compare(l1, l2);
		}
	}
	
	public void close() throws IOException
	{
		reader.close();
	}
		
	public boolean compare(String[] test) throws ParseException, IOException
	{
		List<Document> results = testIndex(test[0], 50);
		
		if (results.isEmpty())
		{
			log.warn("No results found for " + test[0]);
			return false;
		}
		
		Document doc = results.get(0);
		String result = doc.get(IndexCreator.termField);
		if (result.equals(test[1]))
			return true;
		
		log.warn(test[0] + ": " + doc.get(IndexCreator.idField) + "\t" + result);
		return false;
	}
	
	public static void main(String[] arg)
	{	//Creates an index on SNOMED, in the same way that the terminology browser does (I hope)
		try
		{	
			SnomedIndexTester indexer = new SnomedIndexTester();
			
//			String dir = "D:/SnomedCT_RF2Release_INT_20150131/";
//			File coreConceptFile = new File(dir + "Snapshot/Terminology/sct2_Concept_Snapshot_INT_20150131.txt");
//			File extensionConceptFile = new File(dir + "NL_Extension/Snapshot/Terminology/sct2_Concept_Snapshot_NL_20150331.txt");
//			indexer.initActiveConcepts(coreConceptFile, extensionConceptFile);
//			File coreFile = new File(dir + "Snapshot/Terminology/sct2_Description_Snapshot-en_INT_20150131.txt");
//			File extensionFile = new File(dir + "NL_Extension/Snapshot/Terminology/sct2_Description_Snapshot_NL_20150331.txt");
//			log.info("Creating index");
//			indexer.createIndex(coreFile, extensionFile);
//			log.info("Finished indexing");
			
			
			indexer.openIndex();
			String[][] queries = {
					{"oesofag", "Esophagus"},
					{"oesofagu", "Esophagus"},
					{"oesofagus", "Esophagus"},
					{"esofagus", "Esophagus"},
					{"hematoma", "Hematoma"},
					{"hematoom", "Hematoma"},
					{"tuberculosis", "Tuberculosis"},
					{"tuberculose", "Tuberculosis"},		//werkt alleen met stemming
					{"eagle syndrome", "Eagle syndrome"},
					{"fractuur foot", "Fracture of foot"},
					{"# foot", "Fracture of foot"},
					{"#foot", "Fracture of foot"},
					{"ooglidnaevus", "ooglidnaevus"},
					{"ooglid naevus", "ooglidnaevus"},
					{"ooglid nevus", "ooglidnaevus"},
					{"ooglid nevu", "ooglidnaevus"},
					{"iriscyste", "iriscyste"},
					{"vit A", "Vitamin A"},
					{"vit a", "Vitamin A"},
					{"vitaminos", "Hypovitaminosis"}, 
					{"vit K", "Vitamin K"},
					{"WCC", "WCC - White blood cell count"},
					{"hb", "Hb"},
					{"Hb", "Hb"},
					{"HB", "Hb"},
					{"kamerhoekglaucoom", "open-kamerhoekglaucoom"},
					{"kamerhoek glaucoom", "open-kamerhoekglaucoom"},
					{"kamerhoek-glaucoom", "open-kamerhoekglaucoom"},
					{"kamerhoe glauco", "open-kamerhoekglaucoom"},
					{"openkamerhoek glaucoom", "open-kamerhoekglaucoom"},
					{"column", "Anal column"},
					{"trueperella", "Trueperella"},
					{"truperella", "Trueperella"},
					{"truepe", "Trueperella"},
					{"truperi", "Thiobaca trueperi"},	//werkt niet met stemming
					{"true", "True"},
					{"biopt", "Bioptic"},
					{"bioptie", "Bioptic"},				//onmogelijk?
					{"biopsie", "Biopsy"},
					{"oeso", "esoforie"}
					};
			
			int cntr = 0;
			for (String[] test : queries)
			{
				if (indexer.compare(test))
					cntr++;
			}					
			log.info(cntr + " successes out of " + queries.length);
			
//			for (Document doc : indexer.testIndex("oogli", 10))
//				log.info(doc.get(IndexCreator.idField) + "\t" + doc.get(IndexCreator.termField));
//			for (Document doc : indexer.testIndex("ooglid", 10))
//				log.info(doc.get(IndexCreator.idField) + "\t" + doc.get(IndexCreator.termField));
//			for (Document doc : indexer.testIndex("truep", 10))
//				log.info(doc.get(IndexCreator.idField) + "\t" + doc.get(IndexCreator.termField));
//			for (Document doc : indexer.testIndex("truepe", 10))
//				log.info(doc.get(IndexCreator.idField) + "\t" + doc.get(IndexCreator.termField));
//			for (Document doc : indexer.testIndex("bi-rad", 10))
//				log.info(doc.get(IndexCreator.idField) + "\t" + doc.get(IndexCreator.termField));
			
			indexer.close();
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}
}