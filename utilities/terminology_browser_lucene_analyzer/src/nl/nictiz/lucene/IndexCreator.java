package nl.nictiz.lucene;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import nl.nictiz.lucene.NictizAnalyzer.Lowercase;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.IntField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;

import au.com.bytecode.opencsv.CSVReader;

/** Creates an index on the DHD-translations.
 * 
 * @author Feikje Hielkema
 *
 */
public class IndexCreator
{
	private static Logger log = Logger.getLogger(IndexCreator.class);
	
	public static Version version = Version.LUCENE_44;
	public static final String compoundIndex = "index-with-compounds", 
							normalIndex = "index-without-compounds",
							termField = "description", 
							idField = "cid", 
							lengthField = "length";
	
	private Analyzer analyzer;
	private IndexWriter writer;
	
	public IndexCreator(boolean splitCompounds) throws IOException
	{
		this.analyzer = new NictizAnalyzer(version, Lowercase.FirstChar, false, false, splitCompounds);
		Directory index = splitCompounds ? new SimpleFSDirectory(new File(compoundIndex)) : new SimpleFSDirectory(new File(normalIndex));
		
		IndexWriterConfig config = new IndexWriterConfig(version, analyzer);	//if an index already exists, it will be overwritten
		config.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
		this.writer = new IndexWriter(index, config);
	}
	
	public void addDHDTerms(File file, int idColumn, int termColumn) throws IOException
	{
		CSVReader reader = new CSVReader(new FileReader(file));
		List<String[]> lines = reader.readAll();
		reader.close();
		
		for (String[] line : lines)
		{
			Document doc = new Document();			//add the synonym as new entry, with retrieveable fields for id, fsn, synonym and length
			doc.add(new StringField(idField, line[idColumn], Field.Store.YES));
			doc.add(new TextField(termField, line[termColumn], Field.Store.YES));		//this is the only field we search on
			doc.add(new IntField(lengthField, line[termColumn].length(), Field.Store.YES));
			writer.addDocument(doc);
		}
	}
	
	public void closeIndex() throws IOException
	{
		writer.close();
	}
	
	public static void main(String[] args)
	{
		try
		{
			File dir = new File("H:/Diagnosethesaurus/20140204/");
			IndexCreator creator = new IndexCreator(true);
			creator.addDHDTerms(new File(dir, "20140204_85049_HT_Thesaurus.csv"), 0, 1);
			creator.addDHDTerms(new File(dir, "20140204_85049_HT_Interface.csv"), 0, 2);
			creator.closeIndex();
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}
}
