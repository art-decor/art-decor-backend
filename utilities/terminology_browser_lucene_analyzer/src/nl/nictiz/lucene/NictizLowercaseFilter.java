package nl.nictiz.lucene;


import java.io.IOException;
import java.util.regex.Pattern;

import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

public class NictizLowercaseFilter extends TokenFilter
{
	private CharTermAttribute termAttribute;
	private static Pattern pattern = Pattern.compile("\\p{Upper}|(.+\\p{Upper}.*)");
	
	public NictizLowercaseFilter(TokenStream input)
	{
		super(input);
		this.termAttribute = addAttribute(CharTermAttribute.class);
	}

	@Override
	public boolean incrementToken() throws IOException
	{
		if (!input.incrementToken())
			return false;
		
		String term = new String(termAttribute.buffer(), 0, termAttribute.length());
		if (term.isEmpty() || isAcronym(term))
			return true;
		
		String newTerm = term.toLowerCase();
		if (newTerm.equals(term))
			return true;
		
		termAttribute.setEmpty();
		termAttribute.copyBuffer(newTerm.toCharArray(), 0, newTerm.length());
		return true;
	}
	
	/** Returns true if term contains capitals on other positions than the first,
	 * or consists solely of capitals.
	 * 
	 * @param term
	 * @return
	 */
	public static boolean isAcronym(String term)
	{
		return pattern.matcher(term).matches();
	}
}