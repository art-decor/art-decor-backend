package nl.nictiz.lucene.compound;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nl.nictiz.lucene.CompoundPartIndex;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVReader;

public class CompoundBuilder
{
	private static Logger log = Logger.getLogger(CompoundBuilder.class);	
	private static File dhdDir = new File("H:/Diagnosethesaurus/20130903/");
	private static String delimiter = "[\\p{Space}\\p{Punct}]+";
	
	private Set<String> terms = new HashSet<String>();
	private Set<String> stopwords;
	private CompoundPartIndex index;
	
	private static Pattern digitPattern = Pattern.compile("[\\p{Digit}\\p{Upper}]+");
	
	public CompoundBuilder() throws IOException
	{
		init();
	}
	
	public CompoundBuilder(Collection<String> terms) throws IOException
	{
		init();
		this.index = new CompoundPartIndex(terms);
	}
	
	private void init() throws IOException
	{
		InputStream in = this.getClass().getClassLoader().getResourceAsStream("stopwords-nl.txt");
		List<String> list = IOUtils.readLines(in, "UTF-8");
		in.close();
		
		this.stopwords = new HashSet<String>();
		stopwords.addAll(list);
	}
	
	public void addTerms(File file, int column, int row) throws IOException
	{
		CSVReader reader = new CSVReader(new FileReader(file));
		List<String[]> lines = reader.readAll();
		reader.close();
		
		for (int i = row; i < lines.size(); i++)
		{
			String[] line = lines.get(i);
			if (line.length <= column)
				continue; 
			
			String phrase = line[column];
			phrase = StringUtils.stripAccents(phrase);
			String[] tokens = phrase.split(delimiter);
			
			for (int j = 0; j < tokens.length; j++)
			{
				if (tokens[j].length() <= 3)
					continue;
				else if (stopwords.contains(tokens[j]))
					continue;
				else if (isAcronym(tokens[j], j))
					continue;
				else
					this.terms.add(tokens[j].toLowerCase());
			}
		}
	}
	
	public static boolean isAcronym(String token, int tokenNr)
	{
		String str = token;
		if ((tokenNr == 0) || (token.length() > 6))
			str = Character.toLowerCase(token.charAt(0)) + token.substring(1);
		
		Matcher m = digitPattern.matcher(str);
		return m.find();				
	}
		
	public void writeToFile(File file) throws IOException
	{
		List<String> list = new ArrayList<String>(this.terms);
		Collections.sort(list);
		FileUtils.writeLines(file, list);
		
//		for (String str : list)
//		{
//			if (str.length() == 4)
//				System.out.println(str);
//		}
	}
	
	public List<String> splitTerm(String phrase)
	{
		List<String> result = new ArrayList<String>();
		phrase = StringUtils.stripAccents(phrase.toLowerCase());
		
		for (String token : phrase.split(delimiter))
			result.addAll(index.split(token));
		
		return result;
	} 
	
	public void splitAllTerms()
	{
		for (String term : this.terms)
		{
			List<String> split = index.split(term);
			if (split != null)
				System.out.println(term + "\t->\t" + split.toString());
		}
	}
	
	public static void main(String[] arg)
	{
		try
		{
			CompoundBuilder builder = new CompoundBuilder(FileUtils.readLines(new File("resources/compoundparts.txt")));
			builder.addTerms(new File(dhdDir, "20130903_84238_HT_Thesaurus.csv"), 1, 2);
			builder.addTerms(new File(dhdDir, "20130903_84238_HT_Interface.csv"), 2, 1);
			builder.writeToFile(new File("compoundparts-2.txt"));
			
//			builder.splitAllTerms();
			
//			log.info(builder.split("aardbeihemangioom"));
//			log.info(builder.split("gezichtsverlies"));
//			log.info(builder.split("hemangioom"));
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}
}
