package nl.nictiz.lucene;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.nictiz.lucene.NictizAnalyzer.Lowercase;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.PrefixQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;

@SuppressWarnings("unused")
public class IndexQueryer
{
	private static Logger log = Logger.getLogger(IndexQueryer.class);
	
	private Analyzer analyzer;
	private IndexReader reader;
	private IndexSearcher searcher;
	
	public IndexQueryer(boolean splitCompounds) throws IOException
	{
		this.analyzer = new NictizAnalyzer(IndexCreator.version, Lowercase.FirstChar, false, true, splitCompounds);
		Directory index = splitCompounds ? new SimpleFSDirectory(new File(IndexCreator.compoundIndex)) : new SimpleFSDirectory(new File(IndexCreator.normalIndex));
		reader = DirectoryReader.open(index);
		searcher = new IndexSearcher(reader);
	}
	
	public List<Document> testIndex(String searchTerm, int maxResults) throws ParseException, IOException
	{
		NictizQueryParser parser = new NictizQueryParser(IndexCreator.version, IndexCreator.termField, analyzer);
		Query query = parser.parse(searchTerm);
		return search(query, maxResults);
	}
	
	private List<Document> search(Query query, int maxResults) throws IOException
	{
		log.info(query.toString());
		TopDocs docs = searcher.search(query, maxResults * 10);
		Map<Document,Float> map = new HashMap<Document,Float>();
		
		for (int i = 0; i < docs.scoreDocs.length; i++)
			map.put(searcher.doc(docs.scoreDocs[i].doc), docs.scoreDocs[i].score);
				
		List<Document> results = new ArrayList<Document>(map.keySet());
		Collections.sort(results, new LengthComparator(map));
				
		if (results.size() < maxResults)
			return results;
		return results.subList(0, maxResults);
	}
		
	private class LengthComparator implements Comparator<Document>
	{
		private Map<Document,Float> map;
		
		public LengthComparator(Map<Document,Float> map)
		{
			this.map = map;
		}
		
		@Override
		public int compare(Document doc1, Document doc2)
		{
			float f1 = map.get(doc1), f2 = map.get(doc2);
			int compare = Float.compare(f2, f1);
			if (compare != 0)
				return compare;
			
			int l1 = Integer.parseInt(doc1.get(IndexCreator.lengthField));
			int l2 = Integer.parseInt(doc2.get(IndexCreator.lengthField));
			return Integer.compare(l1, l2);
		}
	}
	
	public void close() throws IOException
	{
		reader.close();
	}
	
	public static void main(String[] args)
	{
		try
		{		
			
//			String query = "Herpes virus";
//			String[] queryArray = {"Herpes virus"};
			String[] queries = {"haematoma", "hematoma", "woun", "woun haematoma"};
			
//			log.info("************* NORMAL **********************");
//			IndexQueryer normal = new IndexQueryer(false);
//			for (Document doc : normal.testIndex(query, 10))
//				log.info(doc.get(IndexCreator.idField) + "\t" + doc.get(IndexCreator.termField));
//			normal.close();
			
			IndexQueryer compound = new IndexQueryer(true);
			
			for (String query : queries)
			{
				log.info("************* " + query + " **********************");
				for (Document doc : compound.testIndex(query, 10))
					log.info(doc.get(IndexCreator.idField) + "\t" + doc.get(IndexCreator.termField));
				System.out.println();
			}
			
//			log.info("************* COMPOUND - BUILD QUERY	**********************");
//			for (Document doc : compound.testIndex(queryArray, 10))
//				log.info(doc.get(IndexCreator.idField) + "\t" + doc.get(IndexCreator.termField));		
			
			compound.close();		
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

}
