This projects needs the following jars to compile & run:
	- commons-io-2.4.jar
	- commons-lang3-3.1.jar
	- log4j-1.2.17.jar
	- opencsv-1.8.jar
	- lucene-analyzers-common-4.4.0.jar
	- lucene-core-4.4.0.jar
	- lucene-queryparser-4.4.0.jar
	

These jars can be downloaded from 
	http://logging.apache.org/log4j/1.2/download.html
	http://commons.apache.org/proper/commons-io/download_io.cgi
	http://commons.apache.org/proper/commons-lang/download_lang.cgi
	https://sourceforge.net/projects/opencsv/
	http://lucene.apache.org/core/downloads.html

	

Configuring the Lucene Analyser in Exist-DB

To configure the analyser:

1.	Install the developer version of exist.
2.	Export this project to a jar and copy it to EXIST_HOME/lib/extensions/.
3.	Open 'db/apps/terminology-data/snomed-data/collection.xconf and replace 

	<analyzer class="org.apache.lucene.analysis.standard.StandardAnalyzer">
		<param name="stopwords" type="java.util.Set"/>
	</analyzer>

with

	<analyzer class="nl.nictiz.lucene.NictizAnalyzer"/>
	<parser class="org.apache.lucene.queryparser.analyzing.AnalyzingQueryParser"/>

The AnalyzingQueryParser is necessary to ensure that all queries, including wildcard queries, are sent through the analyser. But please 
heed Lucene's warning in AnalyzingQueryParser's documentation: 
"This class should only be used with analyzers that do not use stopwords or that add tokens."


Sending Queries from Art-Decor

Currently, search terms are sent as XML. For instance, if a  user enters 'cornea transplant', the following XML is sent:

	<query>
  	  <bool>
    	    <wildcard occur="must">cornea*</wildcard>
    	    <wildcard occur="must">transplant*</wildcard>
  	  </bool>
	</query>

Unfortunately, exist's XML-parser implements the same behaviour as Lucene's default QueryParser: i.e., wildcard queries are not sent through the analyser. 
However, if we sent the query as a string:
	'cornea* transplant*'
	
AnalyzingQueryParser splits it into two wildcard queries which are sent through the analyser and then combined to a Boolean query.

To send the queries as strings:

1.	Open the relevant xquery. For the Snomed terminology browser, this is 'db/apps/terminology/snomed/modules/search-snomed-description.xquery'.

2.	Copy the folllowing function into the file:
	
	declare function local:concat-wildcard($terms)
	{ 
	   	 let $term :=
   			if (matches($terms[1],'^[a-z|0-9]')) then concat($terms[1], '*')
   			else if (matches($terms[1],'^[A-Z]')) then lower-case($terms[1])
   			else()

   		let $result :=
   			 if (count($terms) = 1)  then $term
   			else if (count($terms) > 1) then  concat($term, " ", 
				local:concat-wildcard(subsequence($terms, 2, count($terms))))
      		else()

		return $result
	};
	
3.	In the assignment of $validSearch, add the else if -clause:

		else if (matches($searchTerms[1],'#') and 
			string-length($searchString)>3 and 
			string-length($searchString)<40) 
		then xs:boolean('true')
	
	This is to enable the replacement of '#' with 'fracture'.

4.	Replace the current assignments of $searchTerms, $options and $query with the following:
	
	let $searchTerms := tokenize($searchString, '[\s\-!"$&#38;()+,./:;=?\[\]^_`{|}~\\]')
	let $options := 
       		 <options>
           			 <default-operator>and</default-operator>
   			<filter-rewrite>yes</filter-rewrite>
       		 </options>
	let $query := local:concat-wildcard($searchTerms)

The 'default operator' option ensures that the query is sent as a conjunction, equivalent to the 'occur=must' attribute in the XML-query. 
The function 'local:concat-wildcard' concatenates all terms in the query, assigning a wildcard to those terms that are completely lowercased.
