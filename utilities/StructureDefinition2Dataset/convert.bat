@echo off

set xslcreate=convfhir2ds.xsl

if (%1 == "") (
    echo "FHIR version (1.0, or 3.0 or 4.0) not set. Example: convert 3.0"
    goto END
)

if (%jarPath% == ()) (
    echo Parameter jarPath does not lead to saxon9.jar.
    goto END
)

if exist temp\.lastrun (
    set /p Build=<temp\.lastrun
    echo Found temp\.lastrun: %Build%
)
if "%Build%" == "%1" (
    echo Tool has previously been run for the same FHIR version. Reusing temp files.
) else (
    echo Tool has previously been run for FHIR %Build%. Recreating temp files.
    rmdir /S /Q temp
)

if not exist temp (
    mkdir temp
    echo|set /p="%1" > temp\.lastrun
)

if "%1" == "1.0" (
    echo Retrieving DSTU2 Profiles, ValueSets, V2-Tables, V3-CodeSystems
    
    if not exist temp\profiles-resources.xml (
        echo [1/4] Retrieving profiles-resources.xml ...
        curl -o temp\profiles-resources.xml http://hl7.org/fhir/DSTU2/profiles-resources.xml
    )
    if not exist temp\valuesets.xml (
        echo [2/4] Retrieving valuesets.xml ...
        curl -o temp\valuesets.xml http://hl7.org/fhir/DSTU2/valuesets.xml
    )
    if not exist temp\v2-tables.xml (
        echo [3/4] Retrieving v2-tables.xml ...
        curl -o temp\v2-tables.xml http://hl7.org/fhir/DSTU2/v2-tables.xml
    )
    if not exist temp\v3-codesystems.xml (
        echo [4/4] Retrieving v3-codesystems.xml ...
        curl -o temp\v3-codesystems.xml http://hl7.org/fhir/DSTU2/v3-codesystems.xml
    )
) else if "%1" == "3.0" (
    echo Retrieving STU3 Profiles, ValueSets, V2-Tables, V3-CodeSystems
    
    if not exist temp\profiles-resources.xml (
        echo [1/4] Retrieving profiles-resources.xml ...
        curl -o temp\profiles-resources.xml http://hl7.org/fhir/STU3/profiles-resources.xml
    )
    if not exist temp\valuesets.xml (
        echo [2/4] Retrieving valuesets.xml ...
        curl -o temp\valuesets.xml http://hl7.org/fhir/STU3/valuesets.xml
    )
    if not exist temp\v2-tables.xml (
        echo [3/4] Retrieving v2-tables.xml ...
        curl -o temp\v2-tables.xml http://hl7.org/fhir/STU3/v2-tables.xml
    )
    if not exist temp\v3-codesystems.xml (
        echo [4/4] Retrieving v3-codesystems.xml ...
        curl -o temp\v3-codesystems.xml http://hl7.org/fhir/STU3/v3-codesystems.xml
    )
) else if "%1" == "4.0" (
    echo Retrieving R4 Profiles, ValueSets, V2-Tables, V3-CodeSystems
    
    if not exist temp\profiles-resources.xml (
        echo [1/4] Retrieving profiles-resources.xml ...
        curl -o temp\profiles-resources.xml http://hl7.org/fhir/R4/profiles-resources.xml
    )
    if not exist temp\valuesets.xml (
        echo [2/4] Retrieving valuesets.xml ...
        curl -o temp\valuesets.xml http://hl7.org/fhir/R4/valuesets.xml
    )
    if not exist temp\v2-tables.xml (
        echo [3/4] Retrieving v2-tables.xml ...
        curl -o temp\v2-tables.xml http://hl7.org/fhir/R4/v2-tables.xml
    )
    if not exist temp\v3-codesystems.xml (
        echo [4/4] Retrieving v3-codesystems.xml ...
        curl -o temp\v3-codesystems.xml http://hl7.org/fhir/R4/v3-codesystems.xml
    )
) else (
    echo Unsupported FHIR version %1
    goto END
)

REM Build combined bundle from profiles-resources + extra
echo Building combined bundle from temp\profiles-resources.xml and extra
java -jar "%jarPath%" -s:"temp/profiles-resources.xml" -o:"temp/combinedbundle.xml" -xsl:"%xslcreate%" fhirVersion=%1

REM Build new project
echo Building project file from temp\combinedbundle.xml
java -jar "%jarPath%" -s:"temp/combinedbundle.xml" -o:"decor-fhir%1.xml" -xsl:"%xslcreate%" fhirVersion=%1

:END
