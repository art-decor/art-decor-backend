<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:f="http://hl7.org/fhir"
    xmlns:f2d="urn:local:stuff"
    exclude-result-prefixes="#all"
    version="2.0">
    
    <xsl:include href="fhir-valueset2decor-valueset.xsl"/>
    
    <xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>
    
    <xsl:param name="debug" select="'true'"/>
    <xsl:variable name="DEBUGMODE" select="$debug castable as xs:boolean and xs:boolean($debug)" as="xs:boolean"/>
    
    <xsl:variable name="fhirVersion" select="'STU3'"/>
    
    <xsl:variable name="projectId" select="'1.2.3.4.5.6.77.1'"/>
    <xsl:variable name="projectPrefix" select="'ad6bbr-'"/>
    <xsl:variable name="projectLanguage" select="'en-US'"/>
    <xsl:variable name="experimental" select="true()"/>
    <!--<xsl:variable name="projectDate" select="concat(substring(string(current-date()),1,10),'T00:00:00')"/>-->
    <xsl:variable name="projectDate" select="'2016-01-12T00:00:00'"/>
    
    <xsl:variable name="extraValueSets" select="collection(iri-to-uri('extra?select=*.xml;recurse=no'))//f:ValueSet" as="element()*"/>
    <xsl:variable name="valuesets">
        <xsl:for-each select="doc('temp/valuesets.xml')//f:entry | doc('temp/v2-tables.xml')//f:entry | doc('temp/v3-codesystems.xml')//f:entry | $extraValueSets">
            <xsl:copy-of select="."/>
        </xsl:for-each>
    </xsl:variable>
    
    <xsl:variable name="extraCodeSystems" select="collection(iri-to-uri('extra?select=*.xml;recurse=no'))//f:CodeSystem" as="element()*"/>
    <xsl:variable name="codesystems">
        <xsl:for-each select="doc('temp/valuesets.xml')//f:entry | doc('temp/v2-tables.xml')//f:entry | doc('temp/v3-codesystems.xml')//f:entry | $extraCodeSystems">
            <xsl:copy-of select="."/>
        </xsl:for-each>
    </xsl:variable>
    
    <xsl:variable name="id-DS" select="concat($projectId,'.1')"/>
    <xsl:variable name="id-DE" select="concat($projectId,'.2')"/>
    <xsl:variable name="id-SC" select="concat($projectId,'.3')"/>
    <xsl:variable name="id-TR" select="concat($projectId,'.4')"/>
    <xsl:variable name="id-CS" select="concat($projectId,'.5')"/>
    <xsl:variable name="id-IS" select="concat($projectId,'.6')"/>
    <xsl:variable name="id-AC" select="concat($projectId,'.7')"/>
    <xsl:variable name="id-CL" select="concat($projectId,'.8')"/>
    <xsl:variable name="id-EL" select="concat($projectId,'.9')"/>
    <xsl:variable name="id-TM" select="concat($projectId,'.10')"/>
    <xsl:variable name="id-VS" select="concat($projectId,'.11')"/>
    <xsl:variable name="id-RL" select="concat($projectId,'.12')"/>
    <xsl:variable name="id-SX" select="concat($projectId,'.17')"/>
    <xsl:variable name="id-TX" select="concat($projectId,'.18')"/>
    <xsl:variable name="id-EX" select="concat($projectId,'.19')"/>
    <xsl:variable name="id-QX" select="concat($projectId,'.20')"/>
    <xsl:variable name="id-CM" select="concat($projectId,'.21')"/>
    
    <!-- 
        Primitive Types
        FHIR Name       Value Domain
        boolean         true | false
        integer         A signed 32-bit integer (for larger values, use decimal)
        string          A sequence of Unicode characters
        decimal         Rational numbers that have a decimal representation. See below about the precision of the number
        uri             A Uniform Resource Identifier Reference (RFC 3986 ). Note: URIs are case sensitive. For UUID (urn:uuid:53fefa32-fcbb-4ff8-8a92-55ee120877b7) use all lowercase
        base64Binary    A stream of bytes, base64 encoded (RFC 4648)
        instant         An instant in time - known at least to the second and always includes a time zone. Note: This is intended for precisely observed times (typically system logs etc.), and not human-reported times - for them, use date and dateTime. instant is a more constrained dateTime
        date            A date, or partial date (e.g. just year or year + month) as used in human communication. There is no time zone. Dates SHALL be valid dates
        dateTime        A date, date-time or partial date (e.g. just year or year + month) as used in human communication. If hours and minutes are specified, a time zone SHALL be populated. Seconds must be provided due to schema type constraints but may be zero-filled and may be ignored. Dates SHALL be valid dates. The time "24:00" is not allowed
        time            A time during the day, with no date specified (can be converted to a Duration since midnight). Seconds must be provided due to schema type constraints but may be zero-filled and may be ignored. The time "24:00" is not allowed, and neither is a time zone
        
        Simple Restrictions 
        In addition to the base primitive types, a few additional primitive types are defined as restrictions on one of the other primitive types.
        
        FHIR Name       Base FHIR Type      Description
        code            string              Indicates that the value is taken from a set of controlled strings defined elsewhere (see Using codes for further discussion). Technically, a code is restricted to a string which has at least one character and no leading or trailing whitespace, and where there is no whitespace other than single spaces in the contents 
        oid             uri                 An OID represented as a URI (RFC 3001 ); e.g. urn:oid:1.2.3.4.5 
        id              string              Any combination of upper or lower case ASCII letters ('A'..'Z', and 'a'..'z', numerals ('0'..'9'), '-' and '.', with a length limit of 64 characters. (This might be an integer, an un-prefixed OID, UUID or any other identifier pattern that meets these constraints.) 
        markdown        string              A string that may contain markdown syntax for optional processing by a markdown presentation engine
        unsignedInt     int                 Any non-negative integer (e.g. >= 0)
        positiveInt     int                 Any positive integer (e.g. > 0)
        
        Complex Types
        Element
        Identifier
        HumanName
        Address
        ContactPoint
        Timing
        Quantity
        SimpleQuantity
        Attachment
        Range
        Period
        Ratio
        CodeableConcept
        Coding
        SampledData
        Age
        Distance
        Duration
        Count
        Money
    -->
    <xsl:variable name="fhirdt2decordt" as="element()*">
        <map fhir="boolean" decor="boolean"/>
        <map fhir="integer" decor="count"/>
        <map fhir="string" decor="string"/>
        <map fhir="decimal" decor="decimal"/>
        <map fhir="uri" decor="string"/>
        <map fhir="base64Binary" decor="blob"/>
        <map fhir="instant" decor="datetime"/>
        <map fhir="date" decor="datetime"/>
        <map fhir="dateTime" decor="datetime"/>
        <map fhir="time" decor="string"/>
        <map fhir="code" decor="code"/>
        <map fhir="oid" decor="identifier"/>
        <map fhir="id" decor="identifier"/>
        <map fhir="markdown" decor="text"/>
        <map fhir="unsignedInt" decor="count" minInclude="0"/>
        <map fhir="positiveInt" decor="count" minInclude="1"/>
        <map fhir="Identifier" decor="identifier"/>
        <map fhir="HumanName" decor="complex"/>
        <map fhir="Address" decor="complex"/>
        <map fhir="ContactPoint" decor="complex"/>
        <map fhir="Timing" decor="complex"/>
        <map fhir="Quantity" decor="quantity"/>
        <map fhir="SimpleQuantity" decor="quantity"/>
        <map fhir="Attachment" decor="blob"/>
        <map fhir="Range" decor="complex"/>
        <map fhir="Period" decor="complex"/>
        <map fhir="Ratio" decor="complex"/>
        <map fhir="CodeableConcept" decor="code"/>
        <map fhir="Coding" decor="code"/>
        <map fhir="SampledData" decor="blob"/>
        <map fhir="Age" decor="quantity"/>
        <map fhir="Distance" decor="quantity"/>
        <map fhir="Duration" decor="quantity"/>
        <map fhir="Count" decor="quantity"/>
        <map fhir="Money" decor="quantity"/>
        
        <map fhir="DomainResource" decor="complex"/>
        <map fhir="Resource" decor="complex"/>
        <map fhir="Extension" decor="complex"/>
        <map fhir="Annotation" decor="complex"/>
        <map fhir="Signature" decor="complex"/>
        <map fhir="ElementDefinition" decor="complex"/>
        <map fhir="Reference" decor="complex"/>
        <map fhir="Meta" decor="complex"/>
        <map fhir="Narrative" decor="text"/>
        <map fhir="BackboneElement" decor="complex"/>
    </xsl:variable>
    
    <xsl:variable name="fhirsystems" as="element()*">
        <codeSystem oid="{$id-CS}.1" uri="http://example.com"/>
        <codeSystem oid="{$id-CS}.2" uri="https://www.census.gov/geo/reference/"/>
        <codeSystem oid="{$id-CS}.3" uri="https://www.usps.com/"/>
        <codeSystem oid="{$id-CS}.4" uri="http://hl7.org/fhir/restful-interaction"/>
        <codeSystem oid="{$id-CS}.5" uri="http://acme.com/config/fhir/codesystems/cholesterol"/>
        <codeSystem oid="{$id-CS}.6" uri="http://healthit.gov/nhin/purposeofuse"/>
        <codeSystem oid="{$id-CS}.7" uri="http://loinc.org/vs/LL2654-3"/>
        <codeSystem oid="{$id-CS}.8" uri="http://hl7.org/fhir/appropriateness-score"/>
        <codeSystem oid="{$id-CS}.9" uri="http://hl7.org/fhir/flagCategory"/>
        <codeSystem oid="{$id-CS}.10" uri="http://hl7.org/fhir/group-special-type"/>
        <codeSystem oid="{$id-CS}.11" uri="http://hl7.org/fhir/obs-kind"/>
        <codeSystem oid="{$id-CS}.12" uri="http://hl7.org/fhir/qicore-adverseevent-category"/>
        <codeSystem oid="{$id-CS}.13" uri="http://hl7.org/fhir/qicore-adverseevent-type"/>
        <codeSystem oid="{$id-CS}.14" uri="http://hl7.org/fhir/qicore-communication-medium"/>
        <codeSystem oid="{$id-CS}.15" uri="http://hl7.org/fhir/qicore-condition-criticality"/>
        <codeSystem oid="{$id-CS}.16" uri="http://hl7.org/fhir/qicore-diagnosticorder-precondition"/>
        <codeSystem oid="{$id-CS}.17" uri="http://hl7.org/fhir/qicore-military-service"/>
        <codeSystem oid="{$id-CS}.18" uri="http://hl7.org/fhir/qicore-observation-verification-method"/>
        <codeSystem oid="{$id-CS}.19" uri="http://hl7.org/fhir/uslab-event"/>
        <codeSystem oid="{$id-CS}.20" uri="http://hl7.org/fhir/v2/0136"/>
        <codeSystem oid="{$id-CS}.21" uri="http://hl7.org/fhir/v2/0353"/>
        <!--    ISO region codes    -->
        <codeSystem oid="{$id-CS}.22" uri="http://unstats.un.org/unsd/methods/m49/m49.htm"/>
        <codeSystem oid="{$id-CS}.23" uri="http://www.cms.gov/Medicare/Coding/ICD9ProviderDiagnosticCodes/codes.html"/>
        <codeSystem oid="{$id-CS}.24" uri="http://www.cms.gov/Medicare/Coding/ICD10/index.html"/>
    </xsl:variable>
    
    <xsl:variable name="extraStructureDefinitions" select="collection(iri-to-uri('extra?select=*.xml;recurse=no'))//f:StructureDefinition" as="element()*"/>
    <xsl:variable name="structuredefinitions" as="element()*">
        <xsl:choose>
            <xsl:when test="/root">
                <xsl:copy-of select="/root/f:StructureDefinition[f:snapshot]"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy-of select="//f:StructureDefinition[f:snapshot] | $extraStructureDefinitions[f:snapshot]"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="workdir" select="string-join(tokenize(document-uri(.),'/')[position()!=last()],'/')"/>
    
    <xsl:template match="/">
        <xsl:choose>
            <xsl:when test="/root">
                <xsl:call-template name="process"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="$DEBUGMODE">
                    <xsl:choose>
                        <xsl:when test="count($extraStructureDefinitions) = 1">
                            <xsl:message>There is <xsl:value-of select="count($extraStructureDefinitions)"/> additional StructureDefinition</xsl:message>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:message>There are <xsl:value-of select="count($extraStructureDefinitions)"/> additional StructureDefinitions</xsl:message>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:if>
                
                <root>
                    <xsl:copy-of select="$structuredefinitions"/>
                </root>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="process">
        <xsl:variable name="snapshots2datasets" as="element()">
            <root>
                <xsl:apply-templates select=".//f:snapshot">
                    <xsl:with-param name="cid" select="$id-DE"/>
                    <xsl:with-param name="ced" select="$projectDate"/>
                </xsl:apply-templates>
            </root>
        </xsl:variable>
        
        <xsl:if test="$DEBUGMODE">
            <xsl:result-document href="temp/snapshots2datasets.xml" indent="yes">
                <xsl:copy-of select="$snapshots2datasets"/>
            </xsl:result-document>
        </xsl:if>
        
        <xsl:processing-instruction name="xml-stylesheet">type="text/xsl" href="https://assets.art-decor.org/ADAR/rv/DECOR2schematron.xsl"</xsl:processing-instruction>
        <xsl:processing-instruction name="xml-model">href="https://assets.art-decor.org/ADAR/rv/DECOR.xsd" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"</xsl:processing-instruction>
        <xsl:text>&#10;</xsl:text>
        <decor xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="https://assets.art-decor.org/ADAR/rv/DECOR.xsd">
            <project id="{$projectId}" prefix="{$projectPrefix}" experimental="{$experimental}" defaultLanguage="{$projectLanguage}">
                <name language="en-US">FHIR <xsl:value-of select="$fhirVersion"/> StructureDefinitions</name>
                <name language="nl-NL">FHIR <xsl:value-of select="$fhirVersion"/> StructureDefinitions</name>
                <desc language="en-US">Conversion of FHIR <xsl:value-of select="$fhirVersion"/> StructureDefinitions as datasets/scenarios</desc>
                <desc language="nl-NL">Conversie van FHIR <xsl:value-of select="$fhirVersion"/> StructureDefinitions als datasets/scenario's</desc>
                <copyright years="2016-" by="The ART-DECOR expert group" logo="art-decor-logo-small.jpg">
                    <addrLine>The ART-DECOR expert group</addrLine>
                    <addrLine>E info@art-decor.org</addrLine>
                    <addrLine>E contact@art-decor.org</addrLine>
                </copyright>
                <author id="1" username="kai">dr Kai U. Heitmann</author>
                <author id="2" username="alexander">Alexander Henket</author>
                <reference logo="art-decor-logo-small.jpg"/>
                <version date="{$projectDate}" by="admin">
                    <desc language="en-US"/>
                </version>
            </project>
            <datasets>
                <xsl:for-each select="$snapshots2datasets/f:concepts">
                    <xsl:comment>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="*:name[1]"/>
                        <xsl:text> </xsl:text>
                    </xsl:comment>
                    <dataset id="{$id-DS}.{position()}" effectiveDate="{$projectDate}" statusCode="final">
                        <xsl:copy-of select="name" copy-namespaces="no"/>
                        <xsl:copy-of select="desc" copy-namespaces="no"/>
                        <xsl:apply-templates select="concept" mode="ccopy"/>
                    </dataset>
                </xsl:for-each>
            </datasets>
            <scenarios>
                <actors>
                    <actor id="{$id-AC}.1" type="person">
                        <name language="en-US">Person</name>
                        <name language="nl-NL">Person</name>
                    </actor>
                </actors>
                <xsl:for-each select="$snapshots2datasets/f:concepts">
                    <xsl:variable name="pos" select="position()"/>
                    <xsl:variable name="name" select="*:name[1]/node()"/>
                    <scenario id="{$id-SC}.{$pos}" effectiveDate="{$projectDate}" statusCode="final">
                        <name language="en-US"><xsl:value-of select="$name"/></name>
                        <name language="nl-NL"><xsl:value-of select="$name"/></name>
                        <desc language="en-US">-</desc>
                        <desc language="nl-NL">-</desc>
                        <transaction id="{$id-TR}.{1 + (2 * ($pos -1))}" effectiveDate="{$projectDate}" statusCode="final" type="group">
                            <name language="en-US">Registration</name>
                            <name language="nl-NL">Registratie</name>
                            <desc language="en-US">-</desc>
                            <desc language="nl-NL">-</desc>
                            <transaction id="{$id-TR}.{2 + (2 * ($pos -1))}" type="stationary" label="{replace($name, '\s', '')}" effectiveDate="{$projectDate}" statusCode="final">
                                <name language="en-US">Registration</name>
                                <name language="nl-NL">Registratie</name>
                                <desc language="en-US">-</desc>
                                <desc language="nl-NL">-</desc>
                                <actors>
                                    <actor id="{$id-AC}.1" role="sender"/>
                                </actors>
                                <representingTemplate sourceDataset="{$id-DS}.{$pos}" sourceDatasetFlexibility="{$projectDate}">
                                    <xsl:apply-templates select=".//concept[@id][not(@maximumMultiplicity = '0')]" mode="scopy"/>
                                </representingTemplate>
                            </transaction>
                        </transaction>
                    </scenario>
                </xsl:for-each>
            </scenarios>
            <ids>
                <baseId id="{$id-DS}" type="DS" prefix="{$projectPrefix}"/>
                <baseId id="{$id-DE}" type="DE" prefix="{$projectPrefix}dataelement-"/>
                <baseId id="{$id-SC}" type="SC" prefix="{$projectPrefix}scenario-"/>
                <baseId id="{$id-TR}" type="TR" prefix="{$projectPrefix}transactions-"/>
                <baseId id="{$id-CS}" type="CS" prefix="{$projectPrefix}codesystem-"/>
                <baseId id="{$id-IS}" type="IS" prefix="{$projectPrefix}issue-"/>
                <baseId id="{$id-AC}" type="AC" prefix="{$projectPrefix}actors-"/>
                <baseId id="{$id-CL}" type="CL" prefix="{$projectPrefix}conceptlist-"/>
                <baseId id="{$id-EL}" type="EL" prefix="{$projectPrefix}element-"/>
                <baseId id="{$id-TM}" type="TM" prefix="{$projectPrefix}template-"/>
                <baseId id="{$id-VS}" type="VS" prefix="{$projectPrefix}valueset-"/>
                <baseId id="{$id-RL}" type="RL" prefix="{$projectPrefix}regel-"/>
                <baseId id="{$id-SX}" type="SX" prefix="{$projectPrefix}example-scenario-"/>
                <baseId id="{$id-TX}" type="TX" prefix="{$projectPrefix}example-transaction-"/>
                <baseId id="{$id-EX}" type="EX" prefix="{$projectPrefix}example-instance-"/>
                <baseId id="{$id-QX}" type="QX" prefix="{$projectPrefix}qualification-test-instances-"/>
                <baseId id="{$id-CM}" type="CM" prefix="{$projectPrefix}mycommunity-"/>
                
                <!-- base id voor instances (examples and/or tests) toegevoegd -->
                <defaultBaseId id="{$id-DS}" type="DS"/>
                <defaultBaseId id="{$id-DE}" type="DE"/>
                <defaultBaseId id="{$id-SC}" type="SC"/>
                <defaultBaseId id="{$id-TR}" type="TR"/>
                <defaultBaseId id="{$id-CS}" type="CS"/>
                <defaultBaseId id="{$id-IS}" type="IS"/>
                <defaultBaseId id="{$id-AC}" type="AC"/>
                <defaultBaseId id="{$id-CL}" type="CL"/>
                <defaultBaseId id="{$id-EL}" type="EL"/>
                <defaultBaseId id="{$id-TM}" type="TM"/>
                <defaultBaseId id="{$id-VS}" type="VS"/>
                <defaultBaseId id="{$id-RL}" type="RL"/>
                <defaultBaseId id="{$id-SX}" type="SX"/>
                <defaultBaseId id="{$id-TX}" type="TX"/>
                <defaultBaseId id="{$id-EX}" type="EX"/>
                <defaultBaseId id="{$id-QX}" type="QX"/>
                <defaultBaseId id="{$id-CM}" type="CM"/>
            </ids>
            <terminology>
                <xsl:for-each select="$snapshots2datasets//terminologyAssociation">
                    <xsl:sort select="@conceptId"/>
                    <xsl:apply-templates select="."/>
                </xsl:for-each>
                <codeSystem ref="{$id-CS}.1" name="example.com" displayName="example.com">
                    <desc language="en-US">FHIR System URI: http://example.com</desc>
                    <desc language="nl-NL">FHIR System URI: http://example.com</desc>
                </codeSystem>
                <codeSystem ref="{$id-CS}.2" name="census-geo-reference" displayName="census-geo-reference">
                    <desc language="en-US">https://www.census.gov/geo/reference/</desc>
                    <desc language="nl-NL">https://www.census.gov/geo/reference/</desc>
                </codeSystem>
                <codeSystem ref="{$id-CS}.3" name="usps.com" displayName="usps.com">
                    <desc language="en-US">https://www.usps.com/</desc>
                    <desc language="nl-NL">https://www.usps.com/</desc>
                </codeSystem>
                <codeSystem ref="{$id-CS}.4" name="fhir-restful-interaction" displayName="fhir-restful-interaction">
                    <desc language="en-US">http://hl7.org/fhir/restful-interaction</desc>
                    <desc language="nl-NL">http://hl7.org/fhir/restful-interaction</desc>
                </codeSystem>
                <codeSystem ref="{$id-CS}.5" name="fhir-codesystems-cholesterol" displayName="fhir-codesystems-cholesterol">
                    <desc language="en-US">http://acme.com/config/fhir/codesystems/cholesterol</desc>
                    <desc language="nl-NL">http://acme.com/config/fhir/codesystems/cholesterol</desc>
                </codeSystem>
                <codeSystem ref="{$id-CS}.6" name="healthit-nhin-purposeofuse" displayName="healthit-nhin-purposeofuse">
                    <desc language="en-US">http://healthit.gov/nhin/purposeofuse</desc>
                    <desc language="nl-NL">http://healthit.gov/nhin/purposeofuse</desc>
                </codeSystem>
                <codeSystem ref="{$id-CS}.7" name="loinc.org-vs-LL2654-3" displayName="loinc.org-vs-LL2654-3">
                    <desc language="en-US">http://loinc.org/vs/LL2654-3</desc>
                    <desc language="nl-NL">http://loinc.org/vs/LL2654-3</desc>
                </codeSystem>
                <codeSystem ref="{$id-CS}.8" name="fhir-appropriateness-score" displayName="fhir-appropriateness-score">
                    <desc language="en-US">http://hl7.org/fhir/appropriateness-score</desc>
                    <desc language="nl-NL">http://hl7.org/fhir/appropriateness-score</desc>
                </codeSystem>
                <codeSystem ref="{$id-CS}.9" name="fhir-flagCategory" displayName="fhir-flagCategory">
                    <desc language="en-US">http://hl7.org/fhir/flagCategory</desc>
                    <desc language="nl-NL">http://hl7.org/fhir/flagCategory</desc>
                </codeSystem>
                <codeSystem ref="{$id-CS}.10" name="fhir-group-special-type" displayName="fhir-group-special-type">
                    <desc language="en-US">http://hl7.org/fhir/group-special-type</desc>
                    <desc language="nl-NL">http://hl7.org/fhir/group-special-type</desc>
                </codeSystem>
                <codeSystem ref="{$id-CS}.11" name="fhir-obs-kind" displayName="fhir-obs-kind">
                    <desc language="en-US">http://hl7.org/fhir/obs-kind</desc>
                    <desc language="nl-NL">http://hl7.org/fhir/obs-kind</desc>
                </codeSystem>
                <codeSystem ref="{$id-CS}.12" name="fhir-qicore-adverseevent-category" displayName="fhir-qicore-adverseevent-category">
                    <desc language="en-US">http://hl7.org/fhir/qicore-adverseevent-category</desc>
                    <desc language="nl-NL">http://hl7.org/fhir/qicore-adverseevent-category</desc>
                </codeSystem>
                <codeSystem ref="{$id-CS}.13" name="fhir-qicore-adverseevent-type" displayName="fhir-qicore-adverseevent-type">
                    <desc language="en-US">http://hl7.org/fhir/qicore-adverseevent-type</desc>
                    <desc language="nl-NL">http://hl7.org/fhir/qicore-adverseevent-type</desc>
                </codeSystem>
                <codeSystem ref="{$id-CS}.14" name="fhir-qicore-communication-medium" displayName="fhir-qicore-communication-medium">
                    <desc language="en-US">http://hl7.org/fhir/qicore-communication-medium</desc>
                    <desc language="nl-NL">http://hl7.org/fhir/qicore-communication-medium</desc>
                </codeSystem>
                <codeSystem ref="{$id-CS}.15" name="fhir-qicore-condition-criticality" displayName="fhir-qicore-condition-criticality">
                    <desc language="en-US">http://hl7.org/fhir/qicore-condition-criticality</desc>
                    <desc language="nl-NL">http://hl7.org/fhir/qicore-condition-criticality</desc>
                </codeSystem>
                <codeSystem ref="{$id-CS}.16" name="fhir-qicore-diagnosticorder-precondition" displayName="fhir-qicore-diagnosticorder-precondition">
                    <desc language="en-US">http://hl7.org/fhir/qicore-diagnosticorder-precondition</desc>
                    <desc language="nl-NL">http://hl7.org/fhir/qicore-diagnosticorder-precondition</desc>
                </codeSystem>
                <codeSystem ref="{$id-CS}.17" name="fhir-qicore-military-service" displayName="fhir-qicore-military-service">
                    <desc language="en-US">http://hl7.org/fhir/qicore-military-service</desc>
                    <desc language="nl-NL">http://hl7.org/fhir/qicore-military-service</desc>
                </codeSystem>
                <codeSystem ref="{$id-CS}.18" name="fhir-qicore-observation-verification-method" displayName="fhir-qicore-observation-verification-method">
                    <desc language="en-US">http://hl7.org/fhir/qicore-observation-verification-method</desc>
                    <desc language="nl-NL">http://hl7.org/fhir/qicore-observation-verification-method</desc>
                </codeSystem>
                <codeSystem ref="{$id-CS}.19" name="fhir-uslab-event" displayName="fhir-uslab-event">
                    <desc language="en-US">http://hl7.org/fhir/uslab-event</desc>
                    <desc language="nl-NL">http://hl7.org/fhir/uslab-event</desc>
                </codeSystem>
                <codeSystem ref="{$id-CS}.20" name="hl7v2-0136" displayName="HL7 Table 0136 Yes/no Indicator">
                    <desc language="en-US">http://hl7.org/fhir/v2/0136</desc>
                    <desc language="nl-NL">http://hl7.org/fhir/v2/0136</desc>
                </codeSystem>
                <codeSystem ref="{$id-CS}.21" name="hl7v2-0353" displayName="HL7 Table 0353 CWE statuses">
                    <desc language="en-US">http://hl7.org/fhir/v2/0353</desc>
                    <desc language="nl-NL">http://hl7.org/fhir/v2/0353</desc>
                </codeSystem>
                <!--    ISO region codes    -->
                <codeSystem ref="{$id-CS}.22" name="iso-region-codes" displayName="ISO Region Codes">
                    <desc language="en-US">http://unstats.un.org/unsd/methods/m49/m49.htm</desc>
                    <desc language="nl-NL">http://unstats.un.org/unsd/methods/m49/m49.htm</desc>
                </codeSystem>
                <codeSystem ref="{$id-CS}.23" name="medicare-icd9" displayName="ICD9 Provider Diagnostic Codes">
                    <desc language="en-US">http://www.cms.gov/Medicare/Coding/ICD9ProviderDiagnosticCodes/codes.html</desc>
                    <desc language="nl-NL">http://www.cms.gov/Medicare/Coding/ICD9ProviderDiagnosticCodes/codes.html</desc>
                </codeSystem>
                <codeSystem ref="{$id-CS}.24" name="medicare-icd10" displayName="medicare-icd10">
                    <desc language="en-US">http://www.cms.gov/Medicare/Coding/ICD10/index.html</desc>
                    <desc language="nl-NL">http://www.cms.gov/Medicare/Coding/ICD10/index.html</desc>
                </codeSystem>
                <xsl:apply-templates select="$valuesets//f:resource/f:ValueSet[f:url/@value = $snapshots2datasets//terminologyAssociation/@canonicalUri]" mode="preselect"/>
            </terminology>
            <rules/>
            <issues/>
        </decor>
    </xsl:template>
    
    <xsl:template match="terminologyAssociation">
        <terminologyAssociation>
            <xsl:copy-of select="@* except @canonicalUri"/>
        </terminologyAssociation>
    </xsl:template>
    
    <xsl:template match="f:snapshot">
        <xsl:param name="cid"/>
        <xsl:param name="ced"/>
        <xsl:variable name="precedingElementCount" select="count(ancestor::f:StructureDefinition/preceding-sibling::f:StructureDefinition/f:snapshot/f:element)" as="xs:integer"/>
        <xsl:variable name="t" as="element()*">
            <xsl:for-each select="f:element">
                <xsl:variable name="path" select="f:path/@value"/>
                <f:celement path="{$path}"
                            level="{count(tokenize($path, '\.'))}"
                            id="{generate-id()}" 
                            pos="{position() + $precedingElementCount}" 
                            sibling="{generate-id(preceding-sibling::f:element[1])}"
                            nextlevel="{generate-id(preceding-sibling::f:element[f:path[starts-with($path, concat(@value, '.'))]][1])}">
                    <xsl:if test="f:contentReference">
                        <xsl:attribute name="ref" select="f:contentReference/substring-after(@value, '#')"/>
                    </xsl:if>
                    <xsl:copy-of select="*"/>
                </f:celement>
            </xsl:for-each>
        </xsl:variable>
        
        <xsl:variable name="s" as="element()">
            <f:concepts>
                <name language="en-US">
                    <xsl:value-of select="ancestor::f:StructureDefinition/f:name/@value"/>
                </name>
                <name language="nl-NL">
                    <xsl:value-of select="ancestor::f:StructureDefinition/f:name/@value"/>
                </name>
                <desc language="en-US">
                    <xsl:value-of select="ancestor::f:StructureDefinition/f:description/@value"/>
                </desc>
                <desc language="nl-NL">
                    <xsl:value-of select="ancestor::f:StructureDefinition/f:description/@value"/>
                </desc>
                <xsl:apply-templates select="$t[1]">
                    <xsl:with-param name="t" select="$t"/>
                    <xsl:with-param name="cid" select="$cid"/>
                    <xsl:with-param name="ced" select="$ced"/>
                </xsl:apply-templates>
            </f:concepts>
        </xsl:variable>
        
        <xsl:copy-of select="$s"/>
    </xsl:template>
    
    <xsl:template match="f:celement">
        <xsl:param name="t"/>
        <xsl:param name="cid"/>
        <xsl:param name="ced"/>
        <xsl:variable name="thisElement" select="."/>
        <xsl:variable name="id" select="@id"/>
        <xsl:variable name="cidnew" select="concat($cid, '.', @pos)"/>
        <xsl:variable name="types" as="xs:string*">
            <xsl:for-each select="f:type/f:code/@value">
                <xsl:choose>
                    <xsl:when test=". = 'Reference'">
                        <xsl:value-of select="concat(.,'(',../../f:profile/tokenize(@value,'/')[last()],')')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="."/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </xsl:variable>
        
        <xsl:variable name="theRef" select="@ref"/>
        <xsl:variable name="reffedConcept" select="$t[@path = $theRef]"/>
        
        <concept id="{$cidnew}" effectiveDate="{$ced}" statusCode="final">
            <xsl:choose>
                <xsl:when test="$theRef">
                    <xsl:if test="not($reffedConcept)">
                        <xsl:message>Could not resolve Reference under "<xsl:value-of select="f:path/@value"/>": <xsl:value-of select="$theRef"/></xsl:message>
                    </xsl:if>
                    <contains ref="{concat($cid, '.', $reffedConcept/@pos)}" flexibility="{$ced}"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:if test="f:min/@value">
                        <xsl:attribute name="minimumMultiplicity" select="f:min/@value"/>
                    </xsl:if>
                    <xsl:if test="f:max/@value">
                        <xsl:attribute name="maximumMultiplicity" select="f:max/@value"/>
                    </xsl:if>
                    <xsl:if test="f:mustSupport[@value = 'true']">
                        <xsl:attribute name="conformance" select="'R'"/>
                    </xsl:if>
                    
                    <xsl:for-each select="f:code">
                        <terminologyAssociation conceptId="{$cidnew}" conceptFlexibility="{$ced}" code="{f:code/@value}" codeSystem="{f2d:getOid(f:system/@value)}" displayName="{f:display/@value}"/>
                    </xsl:for-each>
                    
                    <name language="en-US">
                        <xsl:choose>
                            <xsl:when test="f:sliceName">
                                <xsl:value-of select="f:sliceName/@value"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="replace(tokenize(f:path/@value, '\.')[last()],'\[x\]','')"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </name>
                    <name language="nl-NL">
                        <xsl:choose>
                            <xsl:when test="f:sliceName">
                                <xsl:value-of select="f:sliceName/@value"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="replace(tokenize(f:path/@value, '\.')[last()],'\[x\]','')"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </name>
                    <xsl:for-each select="f:alias">
                        <synonym language="en-US">
                            <xsl:value-of select="@value"/>
                        </synonym>
                    </xsl:for-each>
                    <desc language="en-US">
                        <xsl:value-of select="f:short/@value"/>
                        <xsl:if test="f:definition/@value">
                            <div>
                                <xsl:value-of select="f:definition/@value"/>
                            </div>
                        </xsl:if>
                    </desc>
                    <desc language="nl-NL">
                        <xsl:value-of select="f:short/@value"/>
                        <xsl:if test="f:definition/@value">
                            <div>
                                <xsl:value-of select="f:definition/@value"/>
                            </div>
                        </xsl:if>
                    </desc>
                    <xsl:if test="not(empty($types))">
                        <source language="en-US"><xsl:text>FHIR </xsl:text><xsl:value-of select="f:path/@value"/> (<xsl:value-of select="string-join($types, ' | ')"/>)</source>
                    </xsl:if>
                    <xsl:if test="f:requirements/@value">
                        <rationale language="en-US"><xsl:value-of select="f:requirements/@value"/></rationale>
                        <rationale language="nl-NL"><xsl:value-of select="f:requirements/@value"/></rationale>
                    </xsl:if>
                    <xsl:if test="f:comment/@value | f:comments/@value">
                        <comment language="en-US"><xsl:value-of select="f:comment/@value | f:comments/@value"/></comment>
                        <comment language="nl-NL"><xsl:value-of select="f:comment/@value | f:comments/@value"/></comment>
                    </xsl:if>
                    <xsl:if test="f:isSummary[@value = 'true']">
                        <property name="isSummary">true</property>
                    </xsl:if>
                    <xsl:if test="f:isModifier[@value = 'true']">
                        <property name="isModifier">true</property>
                    </xsl:if>
                    <xsl:if test="f:isModifierReason[@value = 'true']">
                        <property name="isModifierReason">
                            <xsl:value-of select="f:isModifierReason/@value"/>
                        </property>
                    </xsl:if>
                    <xsl:if test="f:meaningWhenMissing[@value = 'true']">
                        <operationalization language="en-US">
                            <xsl:value-of select="f:meaningWhenMissing/@value"/>
                        </operationalization>
                        <operationalization language="nl-NL">
                            <xsl:value-of select="f:meaningWhenMissing/@value"/>
                        </operationalization>
                    </xsl:if>
                    <xsl:choose>
                        <xsl:when test="count(f:type) gt 1">
                            <xsl:for-each select="f:type">
                                <concept id="{$cidnew}.{position()}" effectiveDate="{$ced}" statusCode="final">
                                    <xsl:if test="$thisElement/f:min/@value">
                                        <xsl:attribute name="minimumMultiplicity" select="$thisElement/f:min/@value"/>
                                    </xsl:if>
                                    <xsl:if test="$thisElement/f:max/@value">
                                        <xsl:attribute name="maximumMultiplicity" select="$thisElement/f:max/@value"/>
                                    </xsl:if>
                                    <xsl:if test="$thisElement/f:mustSupport[@value = 'true']">
                                        <xsl:attribute name="conformance" select="'R'"/>
                                    </xsl:if>
                                    <name language="en-US">
                                        <xsl:value-of select="replace(tokenize($thisElement/f:path/@value, '\.')[last()],'\[x\]','')"/>
                                        <xsl:text> </xsl:text>
                                        <xsl:value-of select="f:code/@value"/>
                                        <xsl:if test="f:profile | f:targetProfile">
                                            <xsl:text> (</xsl:text>
                                            <xsl:value-of select="tokenize((f:profile | f:targetProfile)[1]/@value, '/')[last()]"/>
                                            <xsl:text>)</xsl:text>
                                        </xsl:if>
                                    </name>
                                    <name language="nl-NL">
                                        <xsl:value-of select="replace(tokenize($thisElement/f:path/@value, '\.')[last()],'\[x\]','')"/>
                                        <xsl:text> </xsl:text>
                                        <xsl:value-of select="f:code/@value"/>
                                        <xsl:if test="f:profile | f:targetProfile">
                                            <xsl:text> (</xsl:text>
                                            <xsl:value-of select="tokenize((f:profile | f:targetProfile)[1]/@value, '/')[last()]"/>
                                            <xsl:text>)</xsl:text>
                                        </xsl:if>
                                    </name>
                                    <desc language="en-US">-</desc>
                                    <desc language="nl-NL">-</desc>
                                    <xsl:apply-templates select="." mode="doValueDomain">
                                        <xsl:with-param name="cidnew" select="concat($cidnew, '.', position())"/>
                                    </xsl:apply-templates>
                                </concept>
                            </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates select="f:type" mode="doValueDomain">
                                <xsl:with-param name="cidnew" select="$cidnew"/>
                            </xsl:apply-templates>
                        </xsl:otherwise>
                    </xsl:choose>
                    
                    <xsl:apply-templates select="$t[@nextlevel=$id]">
                        <xsl:with-param name="t" select="$t"/>
                        <xsl:with-param name="cid" select="$cid"/>
                        <xsl:with-param name="ced" select="$ced"/>
                    </xsl:apply-templates>
                </xsl:otherwise>
            </xsl:choose>
        </concept>
    </xsl:template>
    
    <xsl:template match="f:type" mode="doValueDomain">
        <xsl:param name="cidnew" as="xs:string"/>
        <xsl:variable name="fhirType" select="f:code/@value"/>
        <xsl:variable name="mapLine" as="element()*">
            <xsl:choose>
                <xsl:when test="$fhirdt2decordt[@fhir = $fhirType]">
                    <xsl:copy-of select="$fhirdt2decordt[@fhir = $fhirType]"/>
                </xsl:when>
                <xsl:otherwise>
                    <map fhir="{$fhirType}" decor="complex"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="clidnew" select="concat($cidnew, '.8.', position())"/>
        <xsl:if test="count($mapLine) != 1">
            <xsl:message terminate="no">Found <xsl:value-of select="count($mapLine)"/> mappings for type <xsl:value-of select="$fhirType"/> to handle <xsl:value-of select="ancestor::f:celement/f:path/@value"/>.</xsl:message>
        </xsl:if>
        <!--<binding>
            <strength value="required"/>
            <description value="The lifecycle status of a Value Set or Concept Map."/>
            <valueSetReference>
                <reference value="http://hl7.org/fhir/ValueSet/conformance-resource-status"/>
            </valueSetReference>
        </binding>-->
        <xsl:if test="$mapLine/@decor = ('code', 'ordinal')">
            <xsl:for-each select="ancestor::f:celement/f:binding/f:valueSetReference">
                <xsl:variable name="ref" select="f:reference/@value"/>
                <xsl:variable name="valueSet" select="$valuesets//f:resource/f:ValueSet[f:url/@value = $ref]"/>
                <xsl:choose>
                    <xsl:when test="$valueSet">
                        <xsl:comment><xsl:value-of select="$ref"/></xsl:comment>
                        <terminologyAssociation conceptId="{$clidnew}" valueSet="{f2d:getOidForValueSet($valueSet, count($valueSet/ancestor::f:entry/preceding-sibling::f:entry)+1)}" canonicalUri="{$valueSet/f:url/@value}" strength="{../f:strength/@value}"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:comment>Could not find referenced value set <xsl:value-of select="$ref"/> from concept <xsl:value-of select="$cidnew"/></xsl:comment>
                        <xsl:message terminate="no">Could not find referenced value set <xsl:value-of select="$ref"/> from concept <xsl:value-of select="$cidnew"/></xsl:message>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </xsl:if>
        <xsl:choose>
            <!--<xsl:when test="$fhirType='Reference' and ../f:profile/@value">
                <concept ref="{../f:profile/@value}" flexibility="{$projectDate}"/>
            </xsl:when>-->
            <xsl:when test="$mapLine[@decor]">
                <xsl:if test="../f:profile/@value | ../f:targetProfile/@value">
                    <xsl:comment>
                    <xsl:for-each select="../f:profile/@value | ../f:targetProfile/@value">
                        <xsl:text>&#x0a;</xsl:text>
                        <xsl:value-of select="."/>
                    </xsl:for-each>
                </xsl:comment>
                </xsl:if>
                <valueDomain type="{$mapLine/@decor}">
                    <xsl:if test="$mapLine/@decor = ('code', 'ordinal')">
                        <conceptList id="{$clidnew}"/>
                    </xsl:if>
                    <xsl:if test="$mapLine[@* except (@fhir | @decor)]">
                        <property>
                            <xsl:copy-of select="$mapLine/(@* except (@fhir | @decor))"/>
                            <xsl:if test="../*[starts-with(local-name(),'fixed')][@value]">
                                <xsl:attribute name="fixed" select="../*[starts-with(local-name(),'fixed')]/@value"/>
                            </xsl:if>
                            <xsl:if test="../*[starts-with(local-name(),'defaultValue')][@value]">
                                <xsl:attribute name="default" select="../*[starts-with(local-name(),'defaultValue')]/@value"/>
                            </xsl:if>
                            <xsl:if test="../f:minLength[@value]">
                                <xsl:attribute name="minLength" select="../f:minLength/@value"/>
                            </xsl:if>
                            <xsl:if test="../f:maxLength[@value]">
                                <xsl:attribute name="maxLength" select="../f:maxLength/@value"/>
                            </xsl:if>
                        </property>
                    </xsl:if>
                </valueDomain>
            </xsl:when>
            <xsl:otherwise/>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="concept" mode="ccopy">
        <xsl:variable name="type">
            <xsl:choose>
                <xsl:when test="count(concept)>0">
                    <xsl:text>group</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>item</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:variable name="currentPath" select="concat(name[1], ' :: ', source[1])"/>
        
        <concept>
            <xsl:copy-of select="(@* except (@minimumMultiplicity|@maximumMultiplicity|@conformance))" copy-namespaces="no"/>
            <xsl:if test="not(contains | inherit)">
                <xsl:attribute name="type" select="$type"/>
            </xsl:if>
            <xsl:copy-of select="* except (concept|terminologyAssociation|valueDomain)" copy-namespaces="no"/>
            <xsl:if test="not(concept)">
                <xsl:variable name="valueDomains" select="valueDomain" as="element()*"/>
                <xsl:for-each select="distinct-values($valueDomains/@type)">
                    <xsl:variable name="type" select="."/>
                    <xsl:copy-of select="$valueDomains[@type=$type][1]" copy-namespaces="no"/>
                </xsl:for-each>
            </xsl:if>
            
            <xsl:apply-templates select="concept" mode="ccopy"/>
        </concept>
    </xsl:template>
    
    <xsl:template match="concept" mode="scopy">
        <xsl:variable name="type">
            <xsl:choose>
                <xsl:when test="count(concept)>0">
                    <xsl:text>group</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>item</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:comment><xsl:value-of select="$type"/> :: <xsl:value-of select="name[1]"/></xsl:comment>
        <concept ref="{@id}">
            <xsl:copy-of select="@minimumMultiplicity|@maximumMultiplicity|@conformance" copy-namespaces="no"/>
        </concept>
    </xsl:template>
    
</xsl:stylesheet>