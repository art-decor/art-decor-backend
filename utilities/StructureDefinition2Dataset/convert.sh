#!/bin/bash

export xslcreate=convfhir2ds.xsl

if [ "$1" = "" ]; then
    echo "FHIR version (1.0, or 3.0 or 4.0) not set. Example: convert 1.0"
    exit 1
fi

if [ ! -e "${jarPath:?"Parameter jarPath not set. Example: export jarPath=/mypath/lib/saxon9/saxon9.jar"}" ]; then
    echo "Parameter jarPath does not lead to saxon9.jar."
    exit 1
fi

if [ -e "temp/.lastrun" ] && [ `cat temp/.lastrun` != "$1" ]; then
    echo "Tool has previously been run for FHIR `cat temp/.lastrun`. Recreating temp files."
    rm -rf temp
else
    echo "Tool has previously been run for the same FHIR version. Reusing temp files."
fi

if [ ! -e "temp" ]; then
    mkdir temp
    echo -n "$1" > temp/.lastrun
fi

if [ $1 = "1.0" ]; then
    echo "Retrieving DSTU2 Profiles, ValueSets, V2-Tables, V3-CodeSystems"
    
    if [ ! -e "temp/profiles-resources.xml" ]; then
        echo "[1/4] Retrieving profiles-resources.xml ..."
        echo ""
        curl -o temp/profiles-resources.xml http://hl7.org/fhir/DSTU2/profiles-resources.xml
    fi
    if [ ! -e "temp/valuesets.xml" ]; then
        echo "[2/4] Retrieving valuesets.xml ..."
        echo ""
        curl -o temp/valuesets.xml http://hl7.org/fhir/DSTU2/valuesets.xml
    fi
    if [ ! -e "temp/v2-tables.xml" ]; then
        echo "[3/4] Retrieving v2-tables.xml ..."
        echo ""
        curl -o temp/v2-tables.xml http://hl7.org/fhir/DSTU2/v2-tables.xml
    fi
    if [ ! -e "temp/v3-codesystems.xml" ]; then
        echo "[4/4] Retrieving v3-codesystems.xml ..."
        echo ""
        curl -o temp/v3-codesystems.xml http://hl7.org/fhir/DSTU2/v3-codesystems.xml
    fi
elif [ $1 = "3.0" ]; then
    echo "Retrieving STU3 Profiles, ValueSets, V2-Tables, V3-CodeSystems"
    
    if [ ! -e "temp/profiles-resources.xml" ]; then
        echo "[1/4] Retrieving profiles-resources.xml ..."
        echo ""
        curl -o temp/profiles-resources.xml http://hl7.org/fhir/STU3/profiles-resources.xml
    fi
    if [ ! -e "temp/valuesets.xml" ]; then
        echo "[2/4] Retrieving valuesets.xml ..."
        echo ""
        curl -o temp/valuesets.xml http://hl7.org/fhir/STU3/valuesets.xml
    fi
    if [ ! -e "temp/v2-tables.xml" ]; then
        echo "[3/4] Retrieving v2-tables.xml ..."
        echo ""
        curl -o temp/v2-tables.xml http://hl7.org/fhir/STU3/v2-tables.xml
    fi
    if [ ! -e "temp/v3-codesystems.xml" ]; then
        echo "[4/4] Retrieving v3-codesystems.xml ..."
        echo ""
        curl -o temp/v3-codesystems.xml http://hl7.org/fhir/STU3/v3-codesystems.xml
    fi
elif [ $1 = "4.0" ]; then
    echo "Retrieving R4 Profiles, ValueSets, V2-Tables, V3-CodeSystems"
    
    if [ ! -e "temp/profiles-resources.xml" ]; then
        echo "[1/4] Retrieving profiles-resources.xml ..."
        echo ""
        curl -o temp/profiles-resources.xml http://hl7.org/fhir/R4/profiles-resources.xml
    fi
    if [ ! -e "temp/valuesets.xml" ]; then
        echo "[2/4] Retrieving valuesets.xml ..."
        echo ""
        curl -o temp/valuesets.xml http://hl7.org/fhir/R4/valuesets.xml
    fi
    if [ ! -e "temp/v2-tables.xml" ]; then
        echo "[3/4] Retrieving v2-tables.xml ..."
        echo ""
        curl -o temp/v2-tables.xml http://hl7.org/fhir/R4/v2-tables.xml
    fi
    if [ ! -e "temp/v3-codesystems.xml" ]; then
        echo "[4/4] Retrieving v3-codesystems.xml ..."
        echo ""
        curl -o temp/v3-codesystems.xml http://hl7.org/fhir/R4/v3-codesystems.xml
    fi
else
    echo "Unsupported FHIR version $1"
    exit 1
fi

# Build combined bundle from profiles-resources + extra
echo "Building combined bundle from temp/profiles-resources.xml and extra"
java -jar "${jarPath}" -s:"temp/profiles-resources.xml" -o:"temp/combinedbundle.xml" -xsl:"$xslcreate" fhirVersion=$1

# Build new project
echo "Building project file from temp/combinedbundle.xml"
java -jar "${jarPath}" -s:"temp/combinedbundle.xml" -o:"decor-fhir$1.xml" -xsl:"$xslcreate" fhirVersion=$1
