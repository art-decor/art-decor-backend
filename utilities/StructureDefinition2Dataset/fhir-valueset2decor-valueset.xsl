<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:f="http://hl7.org/fhir"
    xmlns:f2d="urn:local:stuff"
    exclude-result-prefixes="xs xd f f2d"
    version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Oct 9, 2014</xd:p>
            <xd:p><xd:b>Author:</xd:b> ahenket</xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    
    <xsl:output indent="yes"/>
    
    <!--<xsl:variable name="fhirVersion" select="'STU3'"/>
    
    <xsl:variable name="projectId" select="'1.2.3.4.5.6.77.1'"/>
    <xsl:variable name="projectPrefix" select="'ad6bbr-'"/>
    <xsl:variable name="projectLanguage" select="'en-US'"/>
    <xsl:variable name="experimental" select="true()"/>
    <!-\-<xsl:variable name="projectDate" select="concat(substring(string(current-date()),1,10),'T00:00:00')"/>-\->
    <xsl:variable name="projectDate" select="'2016-01-12T00:00:00'"/>
    
    <xsl:variable name="extraValueSets" select="collection(iri-to-uri('extra?select=*.xml;recurse=no'))//f:ValueSet" as="element()*"/>
    <xsl:variable name="valuesets">
        <xsl:for-each select="doc('temp/valuesets.xml')//f:entry | doc('temp/v2-tables.xml')//f:entry | doc('temp/v3-codesystems.xml')//f:entry | $extraValueSets">
            <xsl:copy-of select="."/>
        </xsl:for-each>
    </xsl:variable>
    
    <xsl:variable name="extraCodeSystems" select="collection(iri-to-uri('extra?select=*.xml;recurse=no'))//f:CodeSystem" as="element()*"/>
    <xsl:variable name="codesystems">
        <xsl:for-each select="doc('temp/valuesets.xml')//f:entry | doc('temp/v2-tables.xml')//f:entry | doc('temp/v3-codesystems.xml')//f:entry | $extraCodeSystems">
            <xsl:copy-of select="."/>
        </xsl:for-each>
    </xsl:variable>
    
    <xsl:variable name="id-CS" select="concat($projectId,'.5')"/>
    <xsl:variable name="id-VS" select="concat($projectId,'.11')"/>
    
    <xsl:variable name="fhirsystems" as="element()*">
        <codeSystem oid="{$id-CS}.1" uri="http://example.com"/>
        <codeSystem oid="{$id-CS}.2" uri="https://www.census.gov/geo/reference/"/>
        <codeSystem oid="{$id-CS}.3" uri="https://www.usps.com/"/>
        <codeSystem oid="{$id-CS}.4" uri="http://hl7.org/fhir/restful-interaction"/>
        <codeSystem oid="{$id-CS}.5" uri="http://acme.com/config/fhir/codesystems/cholesterol"/>
        <codeSystem oid="{$id-CS}.6" uri="http://healthit.gov/nhin/purposeofuse"/>
        <codeSystem oid="{$id-CS}.7" uri="http://loinc.org/vs/LL2654-3"/>
        <codeSystem oid="{$id-CS}.8" uri="http://hl7.org/fhir/appropriateness-score"/>
        <codeSystem oid="{$id-CS}.9" uri="http://hl7.org/fhir/flagCategory"/>
        <codeSystem oid="{$id-CS}.10" uri="http://hl7.org/fhir/group-special-type"/>
        <codeSystem oid="{$id-CS}.11" uri="http://hl7.org/fhir/obs-kind"/>
        <codeSystem oid="{$id-CS}.12" uri="http://hl7.org/fhir/qicore-adverseevent-category"/>
        <codeSystem oid="{$id-CS}.13" uri="http://hl7.org/fhir/qicore-adverseevent-type"/>
        <codeSystem oid="{$id-CS}.14" uri="http://hl7.org/fhir/qicore-communication-medium"/>
        <codeSystem oid="{$id-CS}.15" uri="http://hl7.org/fhir/qicore-condition-criticality"/>
        <codeSystem oid="{$id-CS}.16" uri="http://hl7.org/fhir/qicore-diagnosticorder-precondition"/>
        <codeSystem oid="{$id-CS}.17" uri="http://hl7.org/fhir/qicore-military-service"/>
        <codeSystem oid="{$id-CS}.18" uri="http://hl7.org/fhir/qicore-observation-verification-method"/>
        <codeSystem oid="{$id-CS}.19" uri="http://hl7.org/fhir/uslab-event"/>
        <codeSystem oid="{$id-CS}.20" uri="http://hl7.org/fhir/v2/0136"/>
        <codeSystem oid="{$id-CS}.21" uri="http://hl7.org/fhir/v2/0353"/>
        <!-\-    ISO region codes    -\->
        <codeSystem oid="{$id-CS}.22" uri="http://unstats.un.org/unsd/methods/m49/m49.htm"/>
        <codeSystem oid="{$id-CS}.23" uri="http://www.cms.gov/Medicare/Coding/ICD9ProviderDiagnosticCodes/codes.html"/>
        <codeSystem oid="{$id-CS}.24" uri="http://www.cms.gov/Medicare/Coding/ICD10/index.html"/>
    </xsl:variable>
    
    <xsl:variable name="workdir" select="string-join(tokenize(document-uri(.),'/')[position()!=last()],'/')"/>-->
    
    <!--<xsl:import-schema namespace="http://hl7.org/fhir" schema-location="valueset.xsd"/>
    <xsl:import-schema schema-location="https://assets.art-decor.org/ADAR/rv/DECOR.xsd"/>-->
    
    <xsl:variable name="oidLookup" select="doc('hl7orgoids-lookup.xml')//oid" as="element()*"/>
    
    <xsl:variable name="fhirValueSetStatus2decorValueSetStatus" as="element()*">
        <map f="draft" d="draft">This valueset is still under development.</map>
        <map f="active" d="final">This valueset is ready for normal use.</map>
        <map f="retired" d="deprecated">This valueset has been withdrawn or superceded and should no longer be used.</map>
        <default d="draft">Fallback for unknown statusCodes in FHIR value set</default>
    </xsl:variable>
    
    <xsl:variable name="fhirContactSystem2decorAddrLineType" as="element()*">
        <map f="phone" d="phone">The value is a telephone number used for voice calls. Use of full international numbers starting with + is recommended to enable automatic dialing support but not required.</map>
        <map f="fax" d="fax">The value is a fax machine. Use of full international numbers starting with + is recommended to enable automatic dialing support but not required.</map>
        <map f="email" d="email">The value is an email address.</map>
        <map f="url" d="uri">The value is a url. This is intended for various personal contacts including blogs, Twitter, Facebook, etc. Do not use for email addresses.</map>
    </xsl:variable>
    
    <!--<xsl:template match="/">
        <xsl:variable name="valueSet" select="f2d:getExpandedValueSet(/f:ValueSet/f:url/@value)" as="element(f:ValueSet)?"/>
        <xsl:apply-templates select="$valueSet"/>
    </xsl:template>-->
    
    <xsl:template match="f:ValueSet" mode="preselect">
        <xsl:choose>
            <xsl:when test=".[not(f:expansion)][f:compose]">
                <!--<xsl:message>Requesting ValueSet expansion for <xsl:value-of select="f:url/@value"/></xsl:message>
                <xsl:variable name="valueSet" select="f2d:getExpandedValueSet(f:url/@value)" as="element(f:ValueSet)?"/>
                <xsl:apply-templates select="$valueSet"/>-->
                <xsl:apply-templates select="."/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="f:ValueSet">
        <xsl:variable name="vsid" select="f2d:getOidForValueSet(., count(ancestor::f:entry/preceding-sibling::f:entry)+1)"/>
        <xsl:variable name="vsnm" select="f2d:getValueSetName(.)"/>
        
        <!-- Patch input bug. See http://gforge.hl7.org/gf/project/fhir/tracker/?action=TrackerItemEdit&tracker_item_id=9222&start=0 -->
        <valueSet id="{$vsid}{if ($vsnm='daf-medication-route') then '.1' else ()}" name="{f2d:getValueSetName(.)}" displayName="{f2d:getValueSetDisplayName(.)}" effectiveDate="{f2d:getValueSetEffectiveDate()}" statusCode="{f2d:getValueSetStatusCode(f:status/@value)}" caonicalUri="{f:url/@value}">
            <!-- ===== START META STUFF ==== -->
            <xsl:if test="f:version">
                <xsl:attribute name="versionLabel" select="f:version/@value"/>
            </xsl:if>
            <!-- FHIR has a URI for id. Not supported in DECOR.
                 Alternate solution: add in ids section ...
                 <id root="decorVsID">
                    <designation language="en-US" displayName="fhirUri">fhirUri</designation>
                 </id>
            -->
            <!--<xsl:if test="f:identifier">
                <xsl:attribute name="f:identifier" select="f:identifier/f:value/@value"/>
            </xsl:if>-->
            <xsl:if test="f:experimental/@value = 'true'">
                <xsl:attribute name="experimental" select="f:experimental/@value"/>
            </xsl:if>
            <!--<xsl:if test="f:extensible">
                <xsl:attribute name="f:extensible" select="f:extensible/@value"/>
            </xsl:if>
            <xsl:if test="f:date">
                <xsl:attribute name="f:date" select="f:date/@value"/>
            </xsl:if>-->
            <desc language="{$projectLanguage}">
                <xsl:value-of select="f:description/@value"/>
                <!--<div>
                    <xsl:apply-templates select=".[f:compose]/f:text/*:div/node()" mode="copyintonamespace"/>
                </div>-->
            </desc>
            <!--<xsl:if test="f:publisher | f:telecom">
                <publishingAuthority name="{f:publisher/@value}">
                    <xsl:apply-templates select="f:telecom" mode="contact2addrLine"/>
                </publishingAuthority>
            </xsl:if>
            <xsl:if test="f:copyright">
                <copyright><xsl:value-of select="f:copyright/@value"/></copyright>
            </xsl:if>-->
            
            <!-- ===== END META STUFF ==== -->
            <!-- ===== START CONTENT STUFF ===== -->
            <xsl:apply-templates select="f:codeSystem[not(f:concept)]"/>
            
            <xsl:choose>
                <xsl:when test="f:expansion">
                    <conceptList>
                        <xsl:apply-templates select="f:expansion/f:contains"/>
                    </conceptList>
                </xsl:when>
                <xsl:when test="f:codeSystem[f:concept] | f:compose">
                    <xsl:for-each-group select="f:compose/f:include[f:system][not(f:concept | f:filter)]" group-by="concat(f:system/@value, f:version/@value)">
                        <!-- <completeCodeSystem codeSystem="" codeSystemName="" codeSystemVersion=""/> -->
                        <xsl:variable name="csuri" select="current-group()[1]/f:system/@value"/>
                        <xsl:variable name="codeSystem" select="$codesystems/descendant-or-self::f:CodeSystem[f:url/@value = $csuri]" as="element(f:CodeSystem)*"/>
                        <completeCodeSystem codeSystem="{f2d:getOidForValueSet($codeSystem, count(ancestor::f:entry/preceding-sibling::f:entry)+1)}">
                            <xsl:if test="current-group()[1]/f:version">
                                <xsl:attribute name="codeSystemVersion" select="current-group()[1]/f:version/@value"/>
                            </xsl:if>
                            <xsl:if test="current-group()[1]/f:caseSensitive">
                                <xsl:attribute name="f:caseSensitive" select="current-group()[1]/f:caseSensitive/@value"/>
                            </xsl:if>
                        </completeCodeSystem>
                    </xsl:for-each-group>
                    <xsl:variable name="conceptList" as="element(conceptList)">
                        <conceptList>
                            <xsl:apply-templates select="f:codeSystem/f:concept | f:compose"/>
                        </conceptList>
                    </xsl:variable>
                    <xsl:choose>
                        <xsl:when test="$conceptList/*">
                            <xsl:copy-of select="$conceptList" copy-namespaces="no"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:copy-of select="$conceptList/comment()"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:comment>Unknown contents. Could not find expansion | codeSystem | compose</xsl:comment>
                    <xsl:message terminate="no">Unknown contents. Could not find expansion | codeSystem | compose</xsl:message>
                </xsl:otherwise>
            </xsl:choose>
            <!-- ===== END CONTENT STUFF ===== -->
        </valueSet>
    </xsl:template>
    
    <xsl:template match="*" mode="copyintonamespace">
        <xsl:element name="{local-name()}">
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="node()" mode="copyintonamespace"/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="text()" mode="copyintonamespace">
        <xsl:copy/>
    </xsl:template>
    
    <!-- 
        <compose>
          <include>
            <system value="http://snomed.info/sct"/>
            <filter>
              <property value="concept"/>
              <op value="is-a"/>
              <value value="422096002"/>
            </filter>
          </include>
        </compose>
    -->
    <xsl:template match="f:compose">
        <xsl:for-each select="f:include | f:exclude">
            <xsl:choose>
                <xsl:when test="count(f:filter) gt 1">
                    <xsl:comment><xsl:value-of select="f2d:getValueSetName(ancestor::f:ValueSet)"/> has a compose/<xsl:value-of select="local-name(.)"/> with multiple filters. This is not supported.</xsl:comment>
                    <xsl:message terminate="no"><xsl:value-of select="f2d:getValueSetName(ancestor::f:ValueSet)"/> has a compose/<xsl:value-of select="local-name(.)"/> with multiple filters. This is not supported.</xsl:message>
                </xsl:when>
                <!--<xsl:when test=".[f:system[not(@value = 'http://snomed.info/sct')]][f:filter]">
                    <xsl:comment><xsl:value-of select="f2d:getValueSetName(ancestor::f:ValueSet)"/> has a compose/<xsl:value-of select="local-name(.)"/>/filter (for system <xsl:value-of select="f:system/@value"/>). This is not supported.</xsl:comment>
                    <xsl:message terminate="no"><xsl:value-of select="f2d:getValueSetName(ancestor::f:ValueSet)"/> has a compose/<xsl:value-of select="local-name(.)"/>/filter (for system <xsl:value-of select="f:system/@value"/>). This is not supported.</xsl:message>
                </xsl:when>-->
                <xsl:when test=".[f:filter/f:property[not(@value = 'concept')]]">
                    <xsl:comment><xsl:value-of select="f2d:getValueSetName(ancestor::f:ValueSet)"/> has a compose/<xsl:value-of select="local-name(.)"/>/filter (for system <xsl:value-of select="f:system/@value"/>) for property <xsl:value-of select="f:property/@value"/>. This is not supported.</xsl:comment>
                    <xsl:message terminate="no"><xsl:value-of select="f2d:getValueSetName(ancestor::f:ValueSet)"/> has a compose/<xsl:value-of select="local-name(.)"/>/filter (for system <xsl:value-of select="f:system/@value"/>) for property <xsl:value-of select="f:filter/f:property/@value"/>. This is not supported.</xsl:message>
                </xsl:when>
            </xsl:choose>
        </xsl:for-each>
        
        <!-- DSTU2 -->
        <xsl:apply-templates select="f:import"/>
        <!-- STU3 -->
        <xsl:apply-templates select="f:include/f:valueSet"/>
        
        <xsl:apply-templates select="f:include/f:filter | f:exclude/f:filter"/>
        <xsl:apply-templates select="f:include/f:concept | f:exclude/f:concept"/>
    </xsl:template>
    
    <xsl:template match="f:compose/f:include[not(f:concept | f:valueSet)][f:filter/f:property/@value = 'concept']/f:filter |
                         f:compose/f:exclude[not(f:concept | f:valueSet)][f:filter/f:property/@value = 'concept']/f:filter">
        <xsl:element name="{local-name(..)}">
            <xsl:attribute name="codeSystem" select="f2d:getOid(../f:system/@value)"/>
            <xsl:attribute name="op" select="f:op/@value"/>
            <xsl:attribute name="code" select="f:value/@value"/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="f:compose/f:include/f:concept">
        <xsl:variable name="displayName">
            <xsl:choose>
                <xsl:when test="f:display/@value">
                    <xsl:value-of select="f:display/@value"/>
                </xsl:when>
                <xsl:when test="f:extension[@url='http://hl7.org/fhir/StructureDefinition/valueset-definition']">
                    <xsl:value-of select="f:extension[@url='http://hl7.org/fhir/StructureDefinition/valueset-definition']/f:valueString/@value"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="f:code/@value"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <concept code="{f:code/@value}" codeSystem="{f2d:getOid(../f:system/@value)}" displayName="{$displayName}" type="L" level="0">
            <xsl:if test="f:version">
                <xsl:attribute name="codeSystemVersion" select="f:version/@value"/>
            </xsl:if>
            <xsl:if test="f:extension[@url='http://hl7.org/fhir/Profile/tools-extensions#definition']">
                <desc language="{$projectLanguage}">
                    <xsl:value-of select="f:extension[@url='http://hl7.org/fhir/Profile/tools-extensions#definition']/f:valueString/@value"/>
                </desc>
            </xsl:if>
        </concept>
    </xsl:template>
    
    <xsl:template match="f:compose/f:import | f:compose/f:include/f:valueSet">
        <xsl:variable name="ref" select="@value"/>
        <xsl:variable name="valueSet" select="$valuesets//f:resource/f:ValueSet[f:url/@value=$ref]"/>
        
        <xsl:choose>
            <xsl:when test="$valueSet">
                <xsl:comment><xsl:value-of select="$ref"/></xsl:comment>
                <include ref="{f2d:getOidForValueSet($valueSet, count($valueSet/ancestor::f:entry/preceding-sibling::f:entry)+1)}"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:comment>Could not determine the valueSet to import for <xsl:value-of select="$ref"/> in valueSet <xsl:value-of select="ancestor::f:ValueSet/f:url/@value"/>.</xsl:comment>
                <xsl:message terminate="no">Could not determine the valueSet to import for <xsl:value-of select="$ref"/> in valueSet <xsl:value-of select="ancestor::f:ValueSet/f:url/@value"/>.</xsl:message>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="f:codeSystem[not(f:concept)]">
        <!-- <completeCodeSystem codeSystem="" codeSystemName="" codeSystemVersion=""/> -->
        <completeCodeSystem codeSystem="{f2d:getOidForValueSet(., count(ancestor::f:entry/preceding-sibling::f:entry)+1)}">
            <xsl:if test="f:version">
                <xsl:attribute name="codeSystemVersion" select="f:version/@value"/>
            </xsl:if>
            <xsl:if test="f:caseSensitive">
                <xsl:attribute name="f:caseSensitive" select="f:caseSensitive/@value"/>
            </xsl:if>
        </completeCodeSystem>
    </xsl:template>
    
    <xsl:template match="f:codeSystem/f:concept">
        <!-- <concept code="P" codeSystem="2.16.840.1.113883.3.1937.99.62.3.5.1" displayName="Patiënt" level="0" type="L"/> -->
        <xsl:variable name="fhirSystem" select="ancestor::f:codeSystem/f:system/@value"/>
        <xsl:variable name="code" select="f:code/@value"/>
        <xsl:variable name="displayName">
            <xsl:choose>
                <xsl:when test="f:display/@value">
                    <xsl:value-of select="f:display/@value"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$code"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="level" select="count(ancestor-or-self::f:concept)"/>
        <concept code="{$code}" codeSystem="{f2d:getOid($fhirSystem)}" displayName="{$displayName}" level="{$level}" type="{f2d:getConceptType(.)}"/>
        <xsl:apply-templates select="f:concept"/>
    </xsl:template>
    
    <xsl:template match="f:expansion//f:contains">
        <!-- <concept code="P" codeSystem="2.16.840.1.113883.3.1937.99.62.3.5.1" displayName="Patiënt" level="0" type="L"/> -->
        <xsl:variable name="fhirSystem">
            <xsl:choose>
                <xsl:when test="f:system/@value">
                    <xsl:value-of select="f:system/@value"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="f:contains[1]/f:system/@value"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="code" select="(f:code/@value, '-')[1]"/>
        <xsl:variable name="displayName">
            <xsl:choose>
                <xsl:when test="f:display/@value">
                    <xsl:value-of select="f:display/@value"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$code"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="level" select="count(ancestor-or-self::f:contains)"/>
        <concept code="{$code}" codeSystem="{f2d:getOid($fhirSystem)}" displayName="{$displayName}" level="{$level}" type="{f2d:getConceptType(.)}"/>
        <xsl:apply-templates select="f:contains"/>
    </xsl:template>
    
    <xsl:function name="f2d:getConceptType" as="xs:string">
        <xsl:param name="fhirConcept" as="element()"/>
        
        <xsl:choose>
            <xsl:when test="$fhirConcept/f:abstract/@value='true'">A</xsl:when>
            <xsl:when test="$fhirConcept/f:concept">S</xsl:when>
            <xsl:otherwise>L</xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xsl:template match="f:telecom" mode="contact2addrLine">
        <addrLine>
            <xsl:if test="f:system/@value">
                <xsl:attribute name="type" select="f2d:getAddrLineType(f:system/@value)"/>
            </xsl:if>
            <xsl:if test="f:use">
                <xsl:attribute name="f:use" select="f:use/@value"/>
            </xsl:if>
            <xsl:if test="f:period/f:start">
                <xsl:attribute name="f:periodStart" select="f:period/f:start/@value"/>
            </xsl:if>
            <xsl:if test="f:period/f:end">
                <xsl:attribute name="f:periodEnd" select="f:period/f:end/@value"/>
            </xsl:if>
            <xsl:value-of select="f:value/@value"/>
        </addrLine>
    </xsl:template>
    
    <xsl:function name="f2d:getValueSetName" as="xs:string">
        <xsl:param name="valueSet" as="element(f:ValueSet)"/>
        <xsl:value-of select="tokenize($valueSet/f:url/@value,'/')[last()]"/>
    </xsl:function>
    <xsl:function name="f2d:getValueSetDisplayName" as="xs:string">
        <xsl:param name="valueSet" as="element(f:ValueSet)"/>
        <xsl:value-of select="$valueSet/f:name/@value"/>
    </xsl:function>
    
    <xsl:function name="f2d:getValueSetEffectiveDate" as="xs:string">
        <xsl:value-of select="$projectDate"/>
    </xsl:function>
    
    <xsl:function name="f2d:getValueSetStatusCode" as="xs:string">
        <xsl:param name="fhirStatus" as="xs:string"/>
        <xsl:choose>
            <xsl:when test="$fhirValueSetStatus2decorValueSetStatus[@f=$fhirStatus]">
                <xsl:value-of select="$fhirValueSetStatus2decorValueSetStatus[@f=$fhirStatus]/@d"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$fhirValueSetStatus2decorValueSetStatus[name()='default']/@d"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xsl:function name="f2d:getAddrLineType" as="xs:string">
        <xsl:param name="fhirType" as="xs:string"/>
        <xsl:choose>
            <xsl:when test="$fhirContactSystem2decorAddrLineType[@f=$fhirType]">
                <xsl:value-of select="$fhirContactSystem2decorAddrLineType[@f=$fhirType]/@d"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$fhirContactSystem2decorAddrLineType[name()='default']/@d"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xsl:function name="f2d:getOidForValueSet">
        <xsl:param name="valueSet" as="element()"/>
        <xsl:param name="pos"/>
        <xsl:variable name="extensions" select="$valueSet/f:extension[@url='http://hl7.org/fhir/StructureDefinition/valueset-oid'][not(f:valueUri/@value='urn:oid:2.16.840.1.113883.4.642.2.0')]" as="element()*"/>
        <xsl:choose>
            <xsl:when test="$valueSet/f:identifier[f:system/@value = 'urn:ietf:rfc:3986']">
                <xsl:value-of select="f2d:getOid($valueSet/f:identifier[f:system/@value = 'urn:ietf:rfc:3986'][1]/f:value/@value)"/>
            </xsl:when>
            <xsl:when test="$extensions[starts-with(f:valueUri/@value,'urn:oid:2.16.840.1.113883.4')]">
                <xsl:value-of select="replace($extensions[starts-with(f:valueUri/@value,'urn:oid:2.16.840.1.113883.4')][1]/f:valueUri/@value,'urn:oid:','')"/>
            </xsl:when>
            <xsl:when test="$extensions[starts-with(f:valueUri/@value,'urn:oid:2.16.840.1.113883.3')]">
                <xsl:value-of select="replace($extensions[starts-with(f:valueUri/@value,'urn:oid:2.16.840.1.113883.3')][1]/f:valueUri/@value,'urn:oid:','')"/>
            </xsl:when>
            <xsl:when test="$extensions[starts-with(f:valueUri/@value,'urn:oid:2.16.840.1.113883')]">
                <xsl:value-of select="replace($extensions[starts-with(f:valueUri/@value,'urn:oid:2.16.840.1.113883')][1]/f:valueUri/@value,'urn:oid:','')"/>
            </xsl:when>
            <xsl:when test="$extensions">
                <xsl:value-of select="replace($extensions[1]/f:valueUri/@value,'urn:oid:','')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="oid" select="f2d:getOid($valueSet/f:url/@value | $valueSet/f:system/@value)"/>
                <xsl:choose>
                    <xsl:when test="not($valueSet/f:url/@value = $oid)">
                        <xsl:value-of select="$oid"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat($id-VS, '.', $pos)"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xsl:function name="f2d:getOid">
        <xsl:param name="fhirSystem" as="xs:string"/>
        <!-- <property type="HL7-FHIR-System-URI-Preferred" value="http://snomed.info/sct"/> -->
        <xsl:variable name="oid" select="$oidLookup[property[@type='HL7-FHIR-System-URI-Preferred']/@value=$fhirSystem]/@oid"/>
        <xsl:choose>
            <xsl:when test="starts-with($fhirSystem,'urn:oid:')">
                <xsl:value-of select="replace($fhirSystem,'urn:oid:','')"/>
            </xsl:when>
            <xsl:when test="$oid">
                <xsl:value-of select="$oid[1]"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="oidLocal" select="$fhirsystems[@uri = $fhirSystem]/@oid"/>
                
                <xsl:if test="not($oidLocal)">
                    <xsl:message terminate="no">Could not determine OID for system <xsl:value-of select="$fhirSystem"/></xsl:message>
                </xsl:if>
                
                <xsl:value-of select="($oidLocal, $fhirSystem)[1]"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xsl:function name="f2d:getExpandedValueSet" as="element(f:ValueSet)?">
        <xsl:param name="identifier" as="xs:string"/>
        
        <xsl:copy-of select="doc(concat('http://fhir-dev.healthintersections.com.au/open/ValueSet/$expand/?identifier=',encode-for-uri($identifier)))/f:ValueSet"/>
    </xsl:function>
</xsl:stylesheet>