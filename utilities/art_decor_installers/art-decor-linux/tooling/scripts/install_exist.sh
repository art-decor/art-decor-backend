#!/bin/bash
# simple exist-db install script for ART-DECOR
# usage: run this script with sudo permissions: sudo ./<script>
# to install to a different path: sudo ./<script> --path /usr/local/exist_repo

# version
# 2012 08 22: initial version
# 2013 02 25: changed to more general setup
# 2013 09 28: changed to exist-db 2.1
# 2014 04 24: existdb does not work with shell nologin, so bash
# 2014 11 22: modified for exist 2.2 LTS
# 2015 10 27: remove old symlink in usr/local
# 2016 04 07: overwrite option for the location_symlink

# get common parameters from settings script
. ./settings

# set the user to run the process as
application_user=existdb
# the location where application will be symlinked
location_symlink=/usr/local/exist_atp
# version of application to be installed
# exist-db 2.2 release
version_application=eXist-db-setup-2.2-rev0000.jar

function setup_config_ports ()
{
        # function to select which ports we want to use
        # exist-db voor art-decor
        # the port that exist-db should run on
        exist_port=8877
        # port for exist-db confidential
        exist_port_conf=8499
        # the location where application will be installed
        location_application=${location_symlink}_22_$(date '+%Y%m%d')

# end of setup_config_ports
}

function install_application ()
{
        # create directory for exist
        mkdir $location_application
        cd $location_application
        # install application
        echo Starting exist-db installer. Press enter if settings are correct!
        java -jar ${tooling_location}/packages/${version_application}
        cd ${tooling_location}/packages

# end of download_application
}

function symlink_application ()
{
        # remove old symlink
        rm ${location_symlink}
        # set up symlink
        ln -s ${location_application} ${location_symlink}
        echo ln -s ${location_application}/ ${location_symlink}

# end of function symlink_application
}

function setup_permissions ()
{
        # set up the user and permissions
        # adduser to run the application as
        adduser --shell /bin/bash ${application_user}

        # set up permissions
        chown -h ${application_user}:${application_users} ${location_symlink}
        chown -R ${application_user}:${application_users} ${location_application}
}

function application_config ()
{
        # setup config
        echo ""
        echo Settings for exist-db:

        # set exist confidential port to $exist_port_conf
        # sed -i '/confidentialPort/s/<Set name="confidentialPort">.*/<Set name="confidentialPort">'${exist_port_conf}'<\/Set>/g' ${location_symlink}/tools/jetty/etc/jetty.xml

        # sed -i '/Set name="Port"/s/<Set name="Port">.*/<Set name="Port"><SystemProperty name="jetty.port.ssl" default="'${exist_port_conf}'"\/><\/Set>/g' ${location_symlink}/tools/jetty/etc/jetty.xml
        # echo Exist port confidential: $exist_port_conf

        # set the user to run exist as
        sed -i 's/^#RUN_AS_USER.*/RUN_AS_USER='${application_user}'/g' ${location_symlink}/tools/wrapper/bin/exist.sh

# end of function application_config
}

function etc_init ()
{
        # set up init script
        cd /etc/init.d
        # remove old symlink
        rm ./${location_symlink##*/}
        # ${location_symlink##*/}: get the last part from location symlink. so /usr/local/exist_atp = exist_atp
        ln -s ${location_application}/tools/wrapper/bin/exist.sh ./${location_symlink##*/}

        # dont start exist-db on server restart, since this might render the server uncontrollable, if exist-db does not start

# end of function etc_init
}

# main logic
# process arguments
  while [ "$1" != "" ]; do
    case $1 in
        -n | --number )           shift
                                term_number=$1
                                ;;
        -p | --path )           shift
                                MYPATHSYMLINK=$1
                                ;;
    esac
    shift
  done

# option to overwrite location_symlink
if [ -n "${MYPATHSYMLINK}" ]; then
   location_symlink=${MYPATHSYMLINK}
fi
setup_config_ports
install_application
symlink_application
setup_permissions
application_config
etc_init

echo Installation finished. You can start it with:
echo "   sudo service ${location_symlink##*/} start"
echo Check out the status with:
echo "   sudo service ${location_symlink##*/} status"
echo "   sudo netstat -anp|grep :8"
echo "   sudo ps aux|grep java"

# EOF