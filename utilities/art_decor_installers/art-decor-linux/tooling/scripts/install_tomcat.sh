#!/bin/bash
# simple tomcat install script
# usage: run this script with sudo permissions: sudo ./<script>
# CAUTION: this script will delete any previous Tomcat installation, but asks the user first to confirm the delete

# version
# 2012 06 27: initial version
# 2012 08 05: changed orbeon.war to art-decor.war
# 2012 09 14: run tomcat as user 'tomcat'
# 2012 11 05: differentiate between CentOS and Ubuntu for system_installer
# 2013 09 29: added chown for tomcat symlink
# 2014 03 24: corrected permissions for tomcat folder
# 2014 12 16: corrected layout, added check before removing old tomcat
# 2016 02 14: detect CentOS version 7+ in /etc/redhat-release
# 2016 05 23: detect CentOS version 7+ in /etc/redhat-release

# get common parameters from settings script
. ./settings

# set variables
# set the user to run the process as
application_user=tomcat
# the location where application will be symlinked
location_symlink=/usr/share/tomcat
# filename for the orbeon war in tomcat
filename_orbeon=art-decor.war

# check whether we are using Ubuntu or CentOS for apt-get or yum
# also used to toggle between update-rc.d and chkconfig
if $(cat /etc/issue|grep -q CentOS)
  then
     # on Centos 6
     system_installer=yum
     OS_VERSION=centos_6
elif $(cat /etc/redhat-release|grep -q CentOS)
  then
     # on Centos 7
     # install required software with yum
     system_installer=yum
     OS_VERSION=centos_7
elif $(cat /etc/issue|grep -q Ubuntu)
then
     # on Ubuntu
     system_installer=apt-get
fi


check_variable_has_content ()
{

        # check whether variable has content
        # echo variable to check = $1
        if [ "$1" != "" ]; then
                        # if variable has content return 1
                        return 1
                else
                        return 0
        fi

# end of function check_variable_has_content
}

function yum_prerequisites ()
{
        # install required software
        # install compiler for jsvc installation for tomcat
        $system_installer install gcc gcc-c++ make

        # specific for ubuntu
        if [ $system_installer == apt-get ]
        then
             $system_installer install build-essential
        fi

# end of function yum_prerequisites
}

function abort_installation ()
{
    echo Cancelling installation
    exit 0

# end of function abort_installation
}

function delete_application ()
{
       # delete previous application files
       rm -Rf /usr/share/tomcat*

# end of function delete_application
}

function unpack_application ()
{
       # download application
       echo cd /usr/share
       cd /usr/share
       cp ${tooling_location}/packages/apache-tomcat* .

       # unpack the tar.gz file
       tar xzf apache-tomcat* || echo error untarring
       # remove the downloaded tar.gz
       rm -f apache-tomcat*.tar.gz || echo error removing archive
       # rename the unpacked directory from apache-tomcat.x.x.x to tomcat.x.x.x
       # save the path where tomcat is installed to location_application
       for dir in `ls -d apache-tomcat*/`;
                do
                        location_application=${location_symlink/tomcat/}${dir/apache-/}
                        # echo dir = $dir
                        # echo loc_app = $location_application
                        # check whether location_application is empty
                        check_variable_has_content ${location_application} && exit 0
                        mv ${dir} ${location_application} || echo could not move
       done

# end of unpack_application
}

function symlink_application ()
{
        # set up symlink
        ln -s ${location_application} ${location_symlink}

# end of symlink_application
}

function jsvc_setup ()
{
        # Set up jsvc so that Tomcat can be run as a daemon
        # change dir to application location
        cd ${location_application}bin
        # unpack the jsvc package
        tar xzf commons-daemon-native.tar.gz
        # change directory to commons-daemon-x.x.x/unix
        for dir in `ls -d commons-daemon*/`; do cd ${dir}unix; done
        # run the jsvc configure script
        ./configure --with-java=${java_location}
        # run the install script
        make

# end of function jsvc_setup
}

function application_permissions ()
{
       # function to setup permissions
       # adduser to run the application as
       adduser --home ${location_symlink} --shell /sbin/nologin ${application_user}
       # set up permissions
       chown -R ${application_user} ${location_application}
       # chown the symlink
       chown -h  ${application_user}:${application_user} ${location_symlink}
       # start and stop scripts need to be executable by the owner
       chmod u+x ${location_symlink}/bin/*.sh

# end of function application_permissions ()
}

function env_script ()
{
        # function to write a script that sets some environment variables
        tee "/etc/profile.d/env.sh" > /dev/null <<EOF
export JAVA_HOME=${java_location}
export JRE_HOME=${java_location}/jre
export TOMCAT_HOME=${location_symlink}
export PATH=\$PATH:\$JAVA_HOME/bin
export CLASSPATH=.:\$JRE_HOME/lib:\$JAVA_HOME/lib:\$JAVA_HOME/lib/tools.jar
EOF

        # make the init script executable
        chmod +x /etc/profile.d/env.sh

# end of function env_script
}

function application_initscript_centos_6 ()
{
        # write an init script for tomcat
        tee "/etc/init.d/tomcat" > /dev/null <<EOF
#!/bin/bash
# chkconfig: 234 20 80
# description: Tomcat Server basic start/shutdown script
# processname: tomcat

export JAVA_HOME=${java_location}
export TOMCAT_HOME=${location_symlink}
export JEE_JAR=${java_location}
export JRE_HOME=${java_location}/jre
export PATH=\$PATH:\$JAVA_HOME/bin
export CLASSPATH=.:\$JAVA_HOME/jre/lib:\$JAVA_HOME/lib:\$JAVA_HOME/lib/tools.jar

case \$1 in
start)
su -l $application_user -s /bin/bash -c "/usr/share/tomcat/bin/startup.sh"
;;
stop)
su -l $application_user -s /bin/bash -c "/usr/share/tomcat/bin/shutdown.sh"
;;
restart)
su -l $application_user -s /bin/bash -c "/usr/share/tomcat/bin/shutdown.sh"
su -l $application_user -s /bin/bash -c "/usr/share/tomcat/bin/startup.sh"
;;
esac
exit 0
EOF

        # make the init script executable
        # set correct permissions for init script
        chmod 755 /etc/init.d/tomcat

        # start tomcat at boot time
        # for CentOS
        if [ $system_installer == yum ]
        then
             /sbin/chkconfig tomcat on
        fi

        # specific for ubuntu
        if [ $system_installer == apt-get ]
        then
               update-rc.d tomcat defaults
        fi

    # only for Centos 6, set the env script via a function
    env_script

# end of function application_initscript_centos_6 ()
}


function application_initscript_centos_7 ()
{
        # write an init script for tomcat
        tee "/etc/systemd/system/tomcat.service" > /dev/null <<EOF

# Systemd unit file for tomcat
[Unit]
Description=Apache Tomcat Web Application Container
After=syslog.target network.target

[Service]
Type=forking

Environment=JAVA_HOME=/usr/java/latest/jre
Environment=CATALINA_PID=/usr/share/tomcat/temp/tomcat.pid
Environment=CATALINA_HOME=/usr/share/tomcat
Environment=CATALINA_BASE=/usr/share/tomcat
Environment='CATALINA_OPTS=-Xms512M -Xmx8192M -server'
Environment='JAVA_OPTS=-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom'

ExecStart=/usr/share/tomcat/bin/startup.sh
ExecStop=/bin/kill -15 $MAINPID

User=tomcat
Group=tomcat

[Install]
WantedBy=multi-user.target
EOF

    # Now reload Systemd to load the Tomcat unit file:
    sudo systemctl daemon-reload

    # you want to enable the Tomcat service, so it starts on server boot, run this command:
    sudo systemctl enable tomcat


}

function application_initscript ()
{
    # based on the detected OS Version, we want to write different tomcat start files
    if [ $OS_VERSION == "centos_7" ]
       then
           application_initscript_centos_7
    elif [ $OS_VERSION == "centos_6" ]
       then
           application_initscript_centos_6
    fi
}


function copy_orbeon_war ()
{
        # copy the orbeon.war to tomcat directory
        cp ${tooling_location}/packages/${filename_orbeon}* ${location_symlink}/webapps/

        # check if orbeon.war is not already listed in config file
        if ! $(grep -q ${filename_orbeon} ${location_symlink}/conf/server.xml); then
                # add context to tomcat server config file
                sed -i '/<\/GlobalNamingResources/a\\n  <Context path="\/'${filename_orbeon/\.war/}'" docBase="'${filename_orbeon}'" reloadable="false" override="true" crossContext="true"><Parameter override="false" name="oxf.resources.priority.0" value="org.orbeon.oxf.resources.FilesystemResourceManagerFactory"/><Parameter override="false" name="oxf.resources.priority.0.oxf.resources.filesystem.sandbox-directory" value="/resources"/></Context>' ${location_symlink}/conf/server.xml
        fi

# end of function copy_orbeon_war
}


# main logic

echo Starting the Tomcat installation
echo OS Version detected = $OS_VERSION
yum_prerequisites

echo CAUTION: this script will delete any previous Tomcat installation
read -p "Continue (y/n)? " choice
case "$choice" in
  y|Y ) delete_application;;
  n|N ) abort_installation;;
  * ) abort_installation;;
esac

unpack_application
symlink_application
jsvc_setup
application_permissions
application_initscript
env_script
copy_orbeon_war

echo Installation of tomcat finished. You can start tomcat with: sudo service tomcat start
echo Check the status of tomcat with:
echo "  sudo netstat -anp|grep :8"
echo "  sudo ps aux|grep java"

# EOF
