package nl.nictiz.snomed.check;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.nictiz.snomed.check.ExtensionChecker.FileType;

import org.apache.commons.io.FileUtils;

/** AS SNOWOWL NOW PERFORMS THIS CHECK BEFORE IMPORT, THIS TEST IS NO LONGER NECESSARY. 
 * @version 2.0 02-07-2015
 * 
 * Finds all concepts that have become inactive in the latest release, then
 * checks whether any of them occur in the active relations exported by SnowOwl.
 * All matches are printed to the console.
 * 
 * @author hielkema
 * @version 1.1 25-07-2014
 */
public class InactiveConceptFinder
{
	/** Loads the FSN's for easier reading
	 */
	public Map<String,String> loadFSNs(File descriptionFile) throws IOException
	{
		Map<String,String> fsns = new HashMap<String,String>();
		List<String> lines = FileUtils.readLines(descriptionFile, "UTF-8");
		for (int i = 1; i < lines.size(); i++)
		{
			String[] line = lines.get(i).split("\t");
			if (line[2].equals("1") && line[6].equals("900000000000003001"))
				fsns.put(line[4], line[7]);
		}
		return fsns;
	}
	
	/** Detects all concepts that are inactive in the delta (and thus, 
	 * presumably, have just become inactive).
	 */
	public Map<String,String> findInactiveConcepts(File deltaConceptsFile, Map<String,String> fsns) throws IOException
	{
		Map<String,String> result = new HashMap<String,String>();
		List<String> lines = FileUtils.readLines(deltaConceptsFile, "UTF-8");
		for (int i = 1; i < lines.size(); i++)
		{
			String[] line = lines.get(i).split("\t");
			if (line[2].equals("0"))
				result.put(line[0], fsns.get(line[0]));
		}
		return result;
	}
	
	/** Checks the list of inactive concepts against the relations in the relationsFile. 
	 * All matches are printed to the console.
	 */
	public void printUses(File relationsFile, Map<String,String> inactiveConcepts) throws IOException
	{
		boolean foundMatch = false;
		List<String> lines = FileUtils.readLines(relationsFile, "UTF-8");
		
		for (int i = 1; i < lines.size(); i++)
		{
			String[] line = lines.get(i).split("\t");
			if (line[2].equals("0"))
				continue;
			
			if (inactiveConcepts.containsKey(line[4]))
			{
				System.out.println("Inactive concept " + line[4] + " | " + inactiveConcepts.get(line[4]) + "| is source of relation with " + line[5]);
				foundMatch = true;
			}
			if (inactiveConcepts.containsKey(line[5]))
			{
				foundMatch = true;
				System.out.println("Inactive concept " + line[5] + " | " + inactiveConcepts.get(line[5]) + "| is target of relation with " + line[4]);
			}
		}
		
		if (!foundMatch)
			System.out.println("No inactive concepts found");
	}
	
	/**	AS SNOWOWL NOW PERFORMS THIS CHECK BEFORE IMPORT, THIS TEST IS NO LONGER NECESSARY.  
	 * 
	 * To run this for a new release and/or SnowOwl-export:
	 * - Download the core release and unzip it
	 * - Unzip the relations-file in the SnowOwl-export
	 * - Open paths.properties and update the variables that point to the location and date of the core and SnowOwl files.
	 * 
	 * When you run the file, all inactive concepts used in SnowOwl should appear in the console, or the message 'no inactive concepts found'.
	 */
//	public static void main(String[] args)
//	{
//		try
//		{
//			File snapshotDescriptionFile = ExtensionChecker.getFile(FileType.Description, ExtensionChecker.coreSnapshotDir, ExtensionChecker.coreDate);	
//			File deltaConceptFile = ExtensionChecker.getFile(FileType.Concept, ExtensionChecker.coreDeltaDir, ExtensionChecker.coreDate);
//			File snowOwlRelationFile = new File(ExtensionChecker.snowOwlRelationFile);	
//			
//			InactiveConceptFinder finder = new InactiveConceptFinder();
//			Map<String,String> fsns = finder.loadFSNs(snapshotDescriptionFile);
//			Map<String,String> inactiveConcepts = finder.findInactiveConcepts(deltaConceptFile, fsns);
//			finder.printUses(snowOwlRelationFile, inactiveConcepts);
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//		}
//	}
}
