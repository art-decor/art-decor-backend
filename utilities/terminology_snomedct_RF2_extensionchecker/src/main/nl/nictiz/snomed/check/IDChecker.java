package nl.nictiz.snomed.check;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nl.nictiz.snomed.check.ExtensionChecker.FileType;
import nl.nictiz.snomed.util.HashMapWithList;

import org.apache.commons.io.FileUtils;
import org.apache.commons.validator.routines.checkdigit.VerhoeffCheckDigit;
import org.apache.log4j.Logger;

/**	Performs the checks relating to SCTID's and UUID's.
 * 
 * @author Feikje Hielkema
 * @version 0.1	16-01-2014
 */
public class IDChecker
{
	public static final String uuidRegex = "[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[1-5][a-fA-F0-9]{3}-[89aAbB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}";
	
	public static enum SCTIDType {Concept, Description, Relation};
	
	private static Logger log = Logger.getLogger(IDChecker.class);
	
	private static VerhoeffCheckDigit verhoeff = new VerhoeffCheckDigit();
//	private static Pattern uuidPattern = Pattern.compile(uuidRegex);
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	
	/**	Checks if the given string conforms to the RFC 4122 standard.
	 */
	public static boolean isValidUUID(String uuid)
	{
		try
		{
			UUID.fromString(uuid);
			return true;
		}
		catch (IllegalArgumentException e)
		{
			return false;
		}
		
//		Matcher m = uuidPattern.matcher(uuid);
//		return m.matches();
	}
	
	/** Checks if the given string conforms to the SCTID pattern
	 */
	public static boolean isValidSCTID(String sctid)
	{
		if (sctid.isEmpty())
			return false;
		if (isSnowOwlID(sctid))
			return false;
		return verhoeff.isValid(sctid);
	}
	
	/** Checks if the given string conforms to the SCTID extension pattern
	 * and resides in the Dutch namespace.
	 */
	public static boolean isValidDutchSCTID(String sctid, FileType type)
	{
		if (!isValidSCTID(sctid))
			return false;
		
		int length = sctid.length();
		if (!sctid.substring(0, length - 3).endsWith(ExtensionChecker.dutchNamespace))
			return false;
		
		String partition = sctid.substring(length - 3, length - 1);
		switch (type)
		{
			case Concept: return partition.equals("10");
			case Description: return partition.equals("11");
			case Definition: return partition.equals("11");
			case Relation: return partition.equals("12");
			case StatedRelation: return partition.equals("12");
			default: log.info("Unexpected component type " + sctid + " has partition " + partition); return false;
		}
	}
	
	public static boolean isSnowOwlID(String sctid)
	{
		int length = sctid.length();
		return sctid.substring(0, length - 3).endsWith(ExtensionChecker.b2iNamespace);
	}
	
	public boolean checkIDsSnapshot(String dir, String date, boolean dutch) throws IOException, ParseException
	{
		Map<String,String> ids = new HashMap<String,String>();
		boolean allClear = true;

		Map<FileType,File> files = ExtensionChecker.getAllFiles(dir, date);
		for (FileType type : files.keySet())
		{
			File file = files.get(type);
			log.debug("Checking file " + file.getName());
			try
			{
				BufferedReader reader = new BufferedReader(new FileReader(file));
				reader.readLine();

				for (int i = 0;; i++)
				{
					String line = reader.readLine();
					if (line == null)
						break;

					String[] tokens = line.split("\t");
					for (int j = 0; j < tokens.length; j++)
					{
						if (tokens[j].isEmpty())
						{
//							throw new ParseException("Empty token in column " + j + " of line " + i + ": " + line, j);
							log.error("Empty token in column " + j + " of line " + i + ": " + line);
							continue;
						}
					}

					if (dutch && (type != FileType.Relation))
					{	//in the Dutch extension, 
						if (!(isValidUUID(tokens[0]) || isValidDutchSCTID(tokens[0], type)))
						{	//the ID's in the first column must be in the Dutch namespace
							log.warn("ID " + tokens[0] + " is not a valid Dutch ID");
							allClear = false;
						}
						if (!tokens[3].equals(ExtensionChecker.dutchModuleId))
						{	//and the fourth column must refer to the Dutch module
							log.warn("Wrong module ID: " + line);
							allClear = false;
						}
					}

					String lineId = file.getName() + ":" + Integer.toString(i);
					if (tokens[0].isEmpty())
						continue;
					else if (ids.containsKey(tokens[0]))
					{
						log.warn(tokens[0] + " occurs at least twice: in " + ids.get(tokens[0]) + " and " + lineId);
						allClear = false;
					}
					else
						ids.put(tokens[0], lineId);
				}
				reader.close();
			}
			catch (FileNotFoundException e)
			{
				log.warn("Could not find file " + file.getAbsolutePath() + " to compare");
			}
		}

		boolean validIds = checkIDValidity(ids, null);		
		return (allClear && validIds);
	}

	private <V> boolean checkIDValidity(Map<String,V> ids, String fileName)
	{
		log.debug("Checking ID validity");
		boolean allClear = true;
		
		for (String id : ids.keySet())
		{
			if (id.length() >= 32)
			{
				if (!isValidUUID(id))
				{
					log.warn("Invalid UUID " + id + " in " + ((fileName != null) ? fileName : ids.get(id)));
					allClear = false;
				}
			}
			else if (!isValidSCTID(id))
			{
				log.warn("Invalid SCTID " + id + " in " + ((fileName != null) ? fileName : ids.get(id)));
				allClear = false;
			}
//			else if (isSnowOwlID(id))
//			{
//				log.warn("SCTID " + id + " in B2i namespace in " + ((fileName != null) ? fileName : ids.get(id)));
//				allClear = false;
//			}
		}
		
		return allClear;
	}
	
	public boolean checkIDsFull(String dir, String date, boolean dutch) throws IOException, ParseException
	{
		boolean allClear = true;
		
		for (File file : ExtensionChecker.getAllFiles(dir, date).values())
		{
			try
			{
				if (!checkIDUniquenessFull(file, dutch))
					allClear = false;
			}
			catch (FileNotFoundException e)
			{
				log.warn("Could not find file to compare " + file.getAbsolutePath());
			}
		}
		
		return allClear;
	}
	
	public boolean checkIDUniquenessFull(File file, boolean dutch) throws IOException, ParseException
	{
		log.debug("Checking file " + file.getName());
		boolean allClear = true;
		HashMapWithList<String,String[]> ids = new HashMapWithList<String,String[]>();
		BufferedReader reader = new BufferedReader(new FileReader(file));
		reader.readLine();
		int cntr = 0;
		boolean relation = file.getName().contains("sct2_Relationship");

		while (true)
		{
			String line = reader.readLine();
			cntr++;
			if (line == null)
				break;

			String[] tokens = line.split("\t");
			for (int j = 0; j < tokens.length; j++)
			{
				if (tokens[j].isEmpty())
				{
//					throw new ParseException("Empty token in column " + j + " of line " + cntr + ": " + line, j);
					log.error("Empty token in column " + j + " of line " + cntr + ": " + line + " (" + file.getName() + ")");
					continue;
				}
			}
			
			ids.add(tokens[0], tokens);
			if (dutch && (!relation) && (!tokens[3].equals(ExtensionChecker.dutchModuleId)))
			{
				log.warn("Wrong module ID: " + line);
				allClear = false;
			}
		}
		reader.close();

		for (String id : ids.keySet())
		{
			List<String[]> doubles = ids.get(id);
			if (doubles.size() == 1)
				continue;

			for (int i = 0; i < doubles.size(); i++)
			{
				String[] first = doubles.get(i);
				for (int j = (i + 1); j < doubles.size(); j++)
				{
					String[] second = doubles.get(j);
					if (identical(first, second, 1))
					{
						log.warn("Two entirely identical lines: -\t" + Arrays.toString(first));
						allClear = false;
					}
				}
			}
		}

		boolean validIds = checkIDValidity(ids, file.getName());		
		return (allClear && validIds);
	}
	
	private boolean identical(String[] first, String[] second, int idx)
	{
		boolean identical = true;
		for (int k = idx; k < first.length; k++)
		{
			if (!first[k].equals(second[k]))
				return false;
		}
		return true;
	}
	
	/** Returns the i'th column as a HashMap, where the column value is the key and the activity
	 * of the line is the value. If multiple lines occur with this key, the value is true if at
	 * least one of them is active.
	 * @throws IOException 
	 */
	public static Map<String,Boolean> readColumnFromFile(File file, int i) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(file));
		reader.readLine();
		Map<String,Boolean> result = new HashMap<String,Boolean>();

		while (true)
		{
			String line = reader.readLine();
			if (line == null)
				break;

			String[] tokens = line.split("\t");
			if ((!result.containsKey(tokens[i])) || (!result.get(tokens[i])))
				result.put(tokens[i], tokens[2].equals("1"));
		}
		return result;
	}
	
	public boolean checkDates(String dir, String date) throws IOException
	{
		boolean result = true;

		for (File file : ExtensionChecker.getAllFiles(dir, date).values())
		{
			log.debug("Checking file " + file.getName());
			try
			{
				BufferedReader reader = new BufferedReader(new FileReader(file));
				reader.readLine();

				for (int i = 0;; i++)
				{
					String line = reader.readLine();
					if (line == null)
						break;

					String[] tokens = line.split("\t");
					try
					{
						dateFormat.parse(tokens[1]);
					}
					catch (ParseException e)
					{
						log.warn("Unwellformed date " + tokens[1] + " in line " + i + " of file " + file.getAbsolutePath());
						result = false;
					}
				}
				reader.close();
			}
			catch (FileNotFoundException e)
			{
				log.warn("Could not find file to compare " + file.getAbsolutePath());
			}
		}

		return result;
	}
	
	public boolean checkForDoubleRefsToPromotedConcepts() throws IOException
	{
		boolean result = true;
		File coreConceptFile = ExtensionChecker.getFile(FileType.Concept, ExtensionChecker.coreSnapshotDir, ExtensionChecker.coreDate);
		Map<String,Boolean> coreConcepts = readColumnFromFile(coreConceptFile, 0);
		File extensionConceptFile = ExtensionChecker.getFile(FileType.Concept, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
//		Map<String,Boolean> extensionConcepts = readColumnFromFile(extensionConceptFile, 0);
		List<String> lines = FileUtils.readLines(extensionConceptFile);
		
//		for (String sctid : extensionConcepts.keySet())
		for (String line : lines)
		{
			String sctid = line.split("\t")[0];
			if (coreConcepts.containsKey(sctid))
			{
				result = false;
				log.warn(line);
//				log.warn("Concept " + sctid + " occurs both in core and extension!");
			}
		}
		
		return result;
	}
}
