package nl.nictiz.snomed.check;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;

public class Description
{
	public static enum Type {FSN, Synonym, Definition};
	public static final String fsnType = "900000000000003001", synonymType = "900000000000013009", definitionType = "900000000000550004";
	public static final String[] caseSignificanceValues = {"900000000000448009", "900000000000017005", "900000000000020002"};
	public static final String English = "en", Dutch = "nl";
	public static Set<String> caseSignificanceSet;
	
	public final String descriptionId, conceptId, description, languageCode, line;
	public final boolean active;
	public final Type type;
		
	public LanguageRefset.Type nlAcceptability = LanguageRefset.Type.None; 
	public LanguageRefset.Type usAcceptability = LanguageRefset.Type.None; 
	
	static
	{
		caseSignificanceSet = new HashSet<String>();
		for (String str : caseSignificanceValues)
			caseSignificanceSet.add(str);
	}
	
	public Description(String line) throws ParseException
	{
		this.line = line;
//		System.out.println(line);
		String[] tokens = line.split("\t");
			
		this.descriptionId = tokens[0];
		this.active = tokens[2].equals("1");
		this.conceptId = tokens[4];
		this.languageCode = tokens[5];
		
		if (tokens[6].equals(fsnType))
			this.type = Type.FSN;
		else if (tokens[6].equals(synonymType))
			this.type = Type.Synonym;
		else if (tokens[6].equals(definitionType))
			this.type = Type.Definition;
		else
			throw new ParseException("Unknown term type " + tokens[6], 6);
		
		this.description = tokens[7];
		if (!caseSignificanceSet.contains(tokens[8]))
			throw new ParseException("Unknown case significance value " + tokens[8], 8);
	}
	
	@Override
	public String toString()
	{
		return this.description;
	}
}
