package nl.nictiz.snomed.check;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nl.nictiz.snomed.util.HashMapWithList;

import org.apache.log4j.Logger;

public class LanguageRefset
{
	public static final String preferred = "900000000000548007", acceptable = "900000000000549004";
	
	public enum Type {Preferred, Acceptable, None};
	
	private static Logger log = Logger.getLogger(SimpleRefset.class);
	private HashMapWithList<String,Entry> entries = new HashMapWithList<String,Entry>();
	
	public LanguageRefset(File file) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(file));
		reader.readLine();

		while (true)
		{
			String line = reader.readLine();
			if (line == null)
				break;

			Entry entry = new Entry(line.split("\t"));
			entries.add(entry.referenceId, entry);
		}
		reader.close();
	}
	
	private class Entry
	{
		final String uuid, refsetId, referenceId;
		final boolean active;
		final Type type;
		
		public Entry(String[] line)
		{
//			System.out.println(line[0]);
			this.uuid = line[0];
			this.active = line[2].equals("1");
			this.refsetId = line[4];
			this.referenceId = line[5];
			this.type = line[6].equals(preferred) ? Type.Preferred : Type.Acceptable;
		}
	}
	
	public int getPreferred(List<Description> descriptions, String refsetId)
	{
		int result = 0;

		for (Description d : descriptions)
		{
			if (!entries.containsKey(d.descriptionId))
				continue;

			for (Entry e : entries.get(d.descriptionId))
			{
				if (e.active && e.refsetId.equals(refsetId) && e.type == Type.Preferred)	//if (d.descriptionId.equals(entry.referenceId))
				{
					result++;
					break;
				}
			}
		}
		
		return result;
	}
	
	public Set<String> getActiveReferenceIds()
	{
		Set<String> result = new HashSet<String>();
		for (Entry entry : entries.getAllValues())
		{
			if (entry.active)
				result.add(entry.referenceId);
		}
		return result;
	}
	
	public boolean checkReferenceUniquenessPerRefset()
	{
		boolean result = true;
		Map<String,Map<String,String>> references = new HashMap<String,Map<String,String>>();
		
		for (Entry entry : entries.getAllValues())
		{
			if (references.containsKey(entry.referenceId))
			{
				Map<String,String> map = references.get(entry.referenceId);
				if (map.containsKey(entry.refsetId) && (!map.get(entry.refsetId).equals(entry.uuid)))
				{	
					log.warn("Concept " + entry.referenceId + " occurs with different UUID's in refset " + entry.refsetId);
					result = false;
				}
				else
					map.put(entry.refsetId, entry.uuid);
			}
			else
			{
				Map<String,String> map = new HashMap<String,String>();
				map.put(entry.refsetId, entry.uuid);
				references.put(entry.referenceId, map);
			}
		}
		
		return result;
	}
	
	public Map<String,Boolean> getRefsetIds()
	{
		Map<String,Boolean> result = new HashMap<String,Boolean>();
		for (Entry entry : entries.getAllValues())
			result.put(entry.refsetId, entry.active);
		return result;
	}
	
	public Type getType(String descriptionId, String refsetId)
	{
		for (Entry entry : entries.get(descriptionId))
		{
			if (entry.refsetId.equals(refsetId))
				return entry.type;
		}
		return null;
	}
	
	public boolean accepts(String refsetId, String descriptionId)
	{
		if (!entries.containsKey(descriptionId))
			return false;
				
		for (Entry entry : entries.get(descriptionId))
		{
			if (entry.active && entry.refsetId.equals(refsetId))
				return true;
		}
		return false;
	}
	
	public boolean isPreferred(String refsetId, String descriptionId)
	{
		if (!entries.containsKey(descriptionId))
			return false;
				
		for (Entry entry : entries.get(descriptionId))
		{
			if (entry.active && entry.refsetId.equals(refsetId))
				return entry.type == Type.Preferred;
		}
		return false;
	}
}
