package nl.nictiz.snomed.check;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

/** This class takes an export that professes to be from a certain date,
 * and removes all entries that actually state they're from a later date.
 * Not very nice obviously, but sadly necessary at present.
 * 
 * @author hielkema
 * @version 1.0 15-08-2014
 */
public class DateAdjuster
{
	private static Logger log = Logger.getLogger(DateAdjuster.class);
	
	private SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
	private String dateStr;
	private Date endDate;
	
	public DateAdjuster(String date) throws ParseException
	{
		this.dateStr = date;
		this.endDate = format.parse(date);
	}
	
	public void processDir(File inDir, File outDir) throws ParseException, IOException
	{
		outDir.mkdir();
		for (File child : inDir.listFiles())
		{
			if (child.isDirectory())
				processDir(child, new File(outDir, child.getName()));
			else
				processFile(child, new File(outDir, child.getName()));
		}
	}
	
	public void processFile(File inFile, File outFile) throws ParseException, IOException
	{
		if (!inFile.getName().endsWith(dateStr + ".txt"))
			log.warn(inFile.getName() + " does not end with correct date.");
		else
			log.info("Processing " + inFile.getAbsolutePath());
			
		BufferedReader reader = new BufferedReader(new FileReader(inFile));
		PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(outFile)));	
		writer.println(reader.readLine());			//copy headers in first line

		while (true)
		{
			String line = reader.readLine();
			if (line == null)
				break;

			String[] tokens = line.split("\t");
			Date date = format.parse(tokens[1]);
			if (!date.after(endDate))			//only copy lines that are before the end date
				writer.println(line);
		}
		
		reader.close();
		writer.close();
	}
	
	public static void main(String[] args)
	{
		try
		{
			File inDir = new File("H:\\SNOMED/NL-Extensie/20140731/");
			File outDir = new File("NL_Extension");
			
			new DateAdjuster(inDir.getName()).processDir(inDir, outDir);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}
}
