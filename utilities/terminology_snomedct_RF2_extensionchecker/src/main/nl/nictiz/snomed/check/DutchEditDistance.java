package nl.nictiz.snomed.check;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVReader;

import com.aliasi.spell.WeightedEditDistance;

/**	This class uses the LingInfo package, which is governed by the following license:
 * 
 * Alias-i ROYALTY FREE LICENSE VERSION 1

Copyright c 2003-2007 Alias-i, Inc
All Rights Reserved

1.  This Alias-i Royalty Free License Version 1 ("License") governs
    the copying, modifying, and distributing of the computer program or
    work containing a notice stating that it is subject to the terms of
    this License and any derivative works of that computer program or
    work.  The computer program or work and any derivative works thereof
    are the "Software."  Your copying, modifying, or distributing of the
    Software constitutes acceptance of this License.  Although you are not
    required to accept this License, since you have not signed it, nothing
    else grants you permission to copy, modify, or distribute the
    Software.  If you wish to receive a license from Alias-i under
    different terms than those contained in this License, please contact
    Alias-i.  Otherwise, if you do not accept this License, any copying,
    modifying, or distributing of the Software is strictly prohibited by
    law.

2.  You may copy or modify the Software or use any output of the
    Software (i) for internal non-production trial, testing and evaluation
    of the Software, or (ii) in connection with any product or service you
    provide to third parties for free.  Copying or modifying the Software
    includes the acts of "installing", "running", "using", "accessing" or
    "deploying" the Software as those terms are understood in the software
    industry.  Therefore, those activities are only permitted under this
    License in the ways that copying or modifying are permitted.

3.  You may distribute the Software, provided that you: (i) distribute
    the Software only under the terms of this License, no more, no less;
    (ii) include a copy of this License along with any such distribution;
    (iii) include the complete corresponding machine-readable source code
    of the Software you are distributing; (iv) do not remove any copyright
    or other notices from the Software; and, (v) cause any files of the
    Software that you modified to carry prominent notices stating that you
    changed the Software and the date of any change so that recipients
    know that they are not receiving the original Software.

4.  Whether you distribute the Software or not, if you distribute any
    computer program that is not the Software, but that (a) is distributed
    in connection with the Software or contains any part of the Software,
    (b) causes the Software to be copied or modified (i.e., ran, used, or
    executed), such as through an API call, or (c) uses any output of the
    Software, then you must distribute that other computer program under a
    license defined as a Free Software License by the Free Software
    Foundation or an Approved Open Source License by the Open Source
    Initiative.

5.  You may not copy, modify, or distribute the Software except as
    expressly provided under this License, unless you receive a different
    written license from Alias-i to do so.  Any attempt otherwise to copy,
    modify, or distribute the Software is without Alias-i's permission, is
    void, and will automatically terminate your rights under this License.
    Your rights under this License may only be reinstated by a signed
    writing from Alias-i.

THE SOFTWARE IS PROVIDED "AS IS."  TO THE MAXIMUM EXTENT PERMITTED BY
APPLICABLE LAW, ALIAS-i DOES NOT MAKE, AND HEREBY EXPRESSLY DISCLAIMS,
ANY WARRANTIES, EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, CONCERNING
THE SOFTWARE OR ANY SUBJECT MATTER OF THIS LICENSE.  SPECIFICALLY, BUT
WITHOUT LIMITING THE FOREGOING, LICENSOR MAKES NO EXPRESS OR IMPLIED
WARRANTY OF MERCHANTABILITY, FITNESS (FOR A PARTICULAR PURPOSE OR
OTHERWISE), QUALITY, USEFULNESS, TITLE, OR NON-INFRINGEMENT.  TO THE
MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL LICENSOR
BE LIABLE TO YOU OR ANY THIRD PARTY FOR ANY DAMAGES OR IN RESPECT OF
ANY CLAIM UNDER ANY TORT, CONTRACT, STRICT LIABILITY, NEGLIGENCE OR
OTHER THEORY FOR ANY DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL,
PUNITIVE, SPECIAL OR EXEMPLARY DAMAGES, EVEN IF IT HAS BEEN ADVISED OF
THE POSSIBILITY OF SUCH DAMAGES, OR FOR ANY AMOUNTS IN EXCESS OF THE
AMOUNT YOU PAID ALIAS-i FOR THIS LICENSE.  YOU MUST PASS THIS ENTIRE
LICENSE, INCLUDING SPECIFICALLY THIS DISCLAIMER AND LIMITATION OF
LIABILITY, ON WHENEVER YOU DISTRIBUTE THE SOFTWARE.
 * 
 * DutchEditDistance extends LingInfo's WeightedEditDistance, customising it
 * to weigh common Dutch spelling errors, such as replacing 'ph' with 'f' or 
 * 'ei' with 'ij', less heavily than other errors.
 * 
 * @author hielkema
 * @version 1.0 13-11-2013
 */
public class DutchEditDistance extends WeightedEditDistance
{
	public static final String splitPattern = "[\\p{Space}]";
	
	private static Logger log = Logger.getLogger(DutchEditDistance.class);
	private static DutchEditDistance singleton;
	
	private Set<String> stopwords;
	private Map<String,String> substitutions = new HashMap<String,String>();
	private int acceptableDistance = 4;
	
	private DutchEditDistance() throws IOException
	{
		super();
		this.stopwords = DescriptionChecker.getStopwords();
		
		substitutions.put("ph", "f");
		substitutions.put("th", "t");
		substitutions.put("rh", "r");
		substitutions.put("rr", "r");
		substitutions.put("k", "c");
		substitutions.put("cc", "c");
		substitutions.put("ae", "e");
		substitutions.put("ie", "y");
		substitutions.put("ei", "ij");
	}
	
	public static DutchEditDistance getDutchEditDistance() throws IOException
	{
		if (singleton == null)
			singleton = new DutchEditDistance();
		return singleton;
	}
	
	public void setAcceptableDistance(int distance)
	{
		this.acceptableDistance = distance;
	}
	
	public boolean similar(String str1, String str2)
	{
		str1 = StringUtils.stripAccents(str1.toString().toLowerCase());
		str2 = StringUtils.stripAccents(str2.toString().toLowerCase());
		
		List<String> first = getAndCleanTokens(str1), second = getAndCleanTokens(str2);
		removeMatchedTokens(first, second);

		if (first.isEmpty() && second.isEmpty())
			return true;
		else if (first.isEmpty() || second.isEmpty())
			return false;
		else if ((first.size() > 3) || (second.size() > 3))
			return false;	//very unlikely that this will match
		
		List<String> options1 = getConcatenations(first), options2 = getConcatenations(second);
		for (String concat1 : options1)
		{
			for (String concat2 : options2)
				if (distance(concat1, concat2) <= acceptableDistance)
					return true;
		}

		return false;
	}
	
	private List<String> getConcatenations(List<String> list)
	{
		if (list.size() == 1)
			return list;
		
		List<String> result = new ArrayList<String>();
		if (list.size() == 2)
		{
			result.add(list.get(0) + list.get(1));
			result.add(list.get(1) + list.get(0));
		}
		else if (list.size() == 3)
		{
			result.add(list.get(0) + list.get(1) + list.get(2));
			result.add(list.get(0) + list.get(2) + list.get(1));
			result.add(list.get(1) + list.get(0) + list.get(2));
			result.add(list.get(1) + list.get(2) + list.get(0));
			result.add(list.get(2) + list.get(0) + list.get(1));
			result.add(list.get(2) + list.get(1) + list.get(0));
		}

		return result;
	}
	
	private void removeMatchedTokens(List<String> first, List<String> second)
	{
		for (Iterator<String> it1 = first.iterator(); it1.hasNext(); )
		{
			String token = it1.next();
			boolean found = false;
			for (Iterator<String> it2 = second.iterator(); it2.hasNext(); )
			{
				if (distance(token, it2.next()) <= acceptableDistance)
				{
					it1.remove();
					it2.remove();
					break;
				}
			}
		}		
	}
	 
	private List<String> getAndCleanTokens(String str)
	{
		List<String> list = new ArrayList<String>();
		for (String token : str.split(splitPattern))
		{
			token = token.replaceAll("\\p{Punct}", "");
			if (!(token.isEmpty() || stopwords.contains(token)))
			{
				for (String sub : substitutions.keySet())
					token = token.replaceAll(sub, substitutions.get(sub));
				list.add(token);
			}
		}
		return list;
	}
	
	@Override
	public double deleteWeight(char c)
	{	//ignore differences in punctuation: adding or missing a hyphen might be bad spelling, but is unlikely to influence the meaning
		 if (Character.isLetter(c) || Character.isDigit(c))
		    return -1;
		 else 
			 return 0;
	}

	@Override
	public double insertWeight(char c)
	{
			return -1;
	}

	@Override
	public double matchWeight(char c)
	{	//a matching (identical) character has no cost, obviously
		return 0;
	}

	@Override
	public double substituteWeight(char c1, char c2)
	{
		return -1;
	}

	@Override
	public double transposeWeight(char c1, char c2)
	{
		return -.5;
	}
	
	public static void main(String[] arg)
	{
		try
		{
			CSVReader reader = new CSVReader(new FileReader(new File("testResources/overlap.csv")), ';');
			List<String[]> list = reader.readAll();
			reader.close();
			
			DutchEditDistance distance = new DutchEditDistance();
			List<String[]> similar = new ArrayList<String[]>(), dissimilar = new ArrayList<String[]>();
	
			for (String[] line : list)
			{
				if (distance.similar(line[0], line[1]))
					similar.add(line);
				else
					dissimilar.add(line);
			}
			
			System.out.println("SIMILAR:");
			for (String[] line : similar)
				System.out.println(Arrays.toString(line));
			System.out.println("\n\nDISSIMILAR");
			for (String[] line : dissimilar)
				System.out.println(Arrays.toString(line));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}