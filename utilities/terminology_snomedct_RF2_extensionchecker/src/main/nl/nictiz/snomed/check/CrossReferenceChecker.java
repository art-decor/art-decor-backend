package nl.nictiz.snomed.check;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.nictiz.snomed.check.ExtensionChecker.FileType;

import org.apache.log4j.Logger;

public class CrossReferenceChecker
{
	private static Logger log = Logger.getLogger(CrossReferenceChecker.class);
	
	private Map<String,Boolean> coreConcepts, nlConcepts;
	
	public CrossReferenceChecker(String coreDate, String nlDate) throws IOException, ParseException
	{
		File coreConceptFile = ExtensionChecker.getFile(FileType.Concept, ExtensionChecker.coreSnapshotDir, coreDate);
		this.coreConcepts = ExtensionChecker.initConcepts(coreConceptFile);
		File dutchConceptFile = ExtensionChecker.getFile(FileType.Concept, ExtensionChecker.nlSnapshotDir, nlDate);
		this.nlConcepts = ExtensionChecker.initConcepts(dutchConceptFile);
	}
	
	public boolean checkDescriptionReferences(Collection<String> references, List<Description> descriptions)
	{
		boolean result = true;
		Map<String,Description> map = new HashMap<String,Description>();
		for (Description d : descriptions)
			map.put(d.descriptionId, d);
		
		for (String did : references)
		{
			if (map.containsKey(did))
			{
				if (!map.get(did).active)
				{
					result = false;
					log.warn("Reference to inactive Dutch description " + did);
				}
			}
			else
			{
				log.warn("Reference to unknown description " + did);
				result = false;
			}
		}
		return result;
	}
	
	public boolean checkActiveConceptReferences(Map<String,Boolean> references)
	{
		boolean result = true;
		for (String cid : references.keySet())
		{
			if (nlConcepts.containsKey(cid))
			{
				if (!references.get(cid))		//the reference is inactive so it doesn't matter whether the NL-concept
					continue;					//is active; we now know it used to exist
				
				if (!nlConcepts.get(cid))
				{
					result = false;
					log.warn("Reference to inactive Dutch concept " + cid);
				}
			}
			else if (coreConcepts.containsKey(cid))
			{
				if (!references.get(cid))		//the reference is inactive so it doesn't matter whether the core-concept
					continue;					//is active; we now know it used to exist
				
				if (!coreConcepts.get(cid))
				{
					result = false;
					log.warn("Reference to inactive core concept " + cid);
				}
			}
			else		//if the concept id cannot be found anywhere it is an error, whether the reference is active or not
			{
				log.warn("Reference to unknown concept " + cid);
				result = false;
			}
		}
		return result;
	}
	
	public boolean checkConceptReferences(Collection<String> references)
	{
		boolean result = true;
		for (String cid : references)
		{
			if (nlConcepts.containsKey(cid) || coreConcepts.containsKey(cid))
				continue;
			else		//if the concept id cannot be found anywhere it is an error, whether the reference is active or not
			{
				log.warn("Reference to unknown concept " + cid);
				result = false;
			}
		}
		return result;
	}
}
