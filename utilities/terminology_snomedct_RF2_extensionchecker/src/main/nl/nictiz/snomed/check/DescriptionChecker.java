package nl.nictiz.snomed.check;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nl.nictiz.snomed.check.Description.Type;
import nl.nictiz.snomed.check.ExtensionChecker.FileType;
import nl.nictiz.snomed.util.HashMapWithList;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVWriter;

/**	Performs the checks relating to descriptions.
 * 
 * @author Feikje Hielkema
 * @version 0.1	16-01-2014
 */
public class DescriptionChecker
{
	private static Logger log = Logger.getLogger(DescriptionChecker.class);
	
	private static DescriptionChecker singleton;
	
	private Pattern asciiPattern = Pattern.compile("\\p{ASCII}+");
	private char[] commonDiacrits = {'�', '�', '�', '�', '�'};
	private Set<String> stopwords;
	
	private DescriptionChecker() throws IOException
	{
		InputStream in = this.getClass().getClassLoader().getResourceAsStream("stopwords-nl.txt");
		List<String> list = IOUtils.readLines(in, "UTF-8");
		in.close();
		
		this.stopwords = new HashSet<String>();
		stopwords.addAll(list);
	} 
	
	public static DescriptionChecker getDescriptionChecker() throws IOException
	{
		if (singleton == null)
			singleton = new DescriptionChecker();
		return singleton;
	}
	
	public static Set<String> getStopwords() throws IOException
	{
		if (singleton == null)
			singleton = new DescriptionChecker();
		return singleton.stopwords;
	}
	
	/**	Reads all descriptions from the RF2-description file,
	 * collects those that contain characters which are neither ASCII nor
	 * common diacrits ({@link #commonDiacrits}
	 * and prints those descriptions to a different file.
	 * 
	 * @param descriptionFile	RF2-description file
	 * @param out	File to which descriptions with diacrits will be printed
	 * @return	The number of descriptions with diacrits
	 */
	public int collectDescriptionsWithCorruptDiacrits(File descriptionFile, File out) throws IOException
	{
		List<String> lines = FileUtils.readLines(descriptionFile, "UTF-8");
		List<String> corrupt = new ArrayList<String>();
		
		for (int i = 1; i < lines.size(); i++)
		{
			String line = lines.get(i);
			if (line.trim().isEmpty())
				continue;
			
			String description = line.split("\\t")[7];
			if (isCorrupt(description))
				corrupt.add(line);
		}
		
		FileUtils.writeLines(out, corrupt);
		return corrupt.size();
	}
	
	public int collectDescriptionsWithCorruptDiacrits(HashMapWithList<String,Description> descriptions, File out) throws IOException
	{
		List<String> corrupt = new ArrayList<String>();
		for (Description description : descriptions.getAllValues())
		{
			if (isCorrupt(description.description))
				corrupt.add(description.line);
		}
		
		FileUtils.writeLines(out, corrupt);
		return corrupt.size();
	}
	
	/** Returns true if the string contains a character
	 * which is neither ASCII nor a diacrit commonly used
	 * in Dutch.
	 */
	public boolean isCorrupt(String str)
	{
		Matcher m = asciiPattern.matcher(str);
		if (m.matches())
			return false;
		
		int i = 0;
		while (i < str.length())
		{
			m.find(i);
			i = m.end();
			if (i >= str.length())
				return false;
			
			if (!isCommonDiacrit(str.charAt(i)))
				return true;
			
			i++;
		}
		
		return false;
	}
	
	/** Returns true if the character is one of 
	 * @see #commonDiacrits
	 */
	public boolean isCommonDiacrit(char c)
	{
		for (char diacrit : this.commonDiacrits)
		{
			if (diacrit == c)
				return true;
		}
		return false;
	}
	
	public boolean checkDescriptionSimilarityWithinConcept(HashMapWithList<String,Description> descriptions, File out) throws IOException
	{
		CSVWriter writer = new CSVWriter(new FileWriter(out), ';');
		String[] header = {"ConceptID", "DescriptionID1", "Description1", "DescriptionId2", "Description2"};
		writer.writeNext(header);
		boolean result = true;
		
		DutchEditDistance editDistance = DutchEditDistance.getDutchEditDistance();
		
		for (List<Description> list : descriptions.values())
		{
			for (int i = 0; i < (list.size() - 1); i++)
			{
				Description desc = list.get(i);
				if (!(desc.active && desc.languageCode.equals("nl")))		//only check active Dutch descriptions
					continue;
								
				for (int j = (i + 1); j < list.size(); j++)
				{
					Description other = list.get(j);
					if (!(other.active && other.languageCode.equals("nl")))
						continue;
					
					if (editDistance.similar(desc.description, other.description))
					{
						String[] line = {desc.conceptId, desc.descriptionId, desc.description, other.descriptionId, other.description};
						writer.writeNext(line);
						result = false;
						log.warn("Similar descriptions '" + desc.description + "' and '" + other.description + "' of concept " + desc.conceptId);
					}
				}
			}
		}
		
		writer.close();
		return result;
	}
	
	public boolean checkForDoubleFSNs(HashMapWithList<String,Description> map)
	{
		boolean result = true;
		for (String fsn : map.keySet())
		{
			if (map.get(fsn).size() > 1)
			{
//				boolean promoted = false;
				StringBuffer sb = new StringBuffer();
				for (Description d : map.get(fsn))
				{
//					if (d.conceptId.contains(ExtensionChecker.dutchNamespace))
//						promoted = true;
					sb.append(d.conceptId + ", ");
				}
				
//				if (!promoted)
					log.warn("Identical FSNs " + fsn + " in concepts " + sb.toString().substring(0, sb.length() - 2));

				result = false;
			}
			else if (fsn.startsWith("Genus "))
			{
				String alternative = fsn.substring(6);
				if (map.containsKey(alternative))
					log.warn("Same genus? " + map.get(fsn).get(0) + " |" + fsn + "| and " + map.get(alternative).get(0) + " |" + alternative + "|");
				result = false;
			}
		}
		
		return result;
	}
	
	public boolean checkForDoppelgangersInCoreAndExtension(boolean skipOrganisms, int editDistance) throws IOException, ParseException
	{	//create index on core descriptions by hierarchy and first letter for both core and Dutch extension
		File coreConceptFile = ExtensionChecker.getFile(FileType.Concept, ExtensionChecker.coreSnapshotDir, ExtensionChecker.coreDate);
		File coreDescriptionFile = ExtensionChecker.getFile(FileType.Description, ExtensionChecker.coreSnapshotDir, ExtensionChecker.coreDate);
		AlphabeticIndex coreIndex = getIndex(coreConceptFile, coreDescriptionFile);
		
		//collect all active descriptions of active Dutch concepts
		File dutchConceptFile = ExtensionChecker.getFile(FileType.Concept, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		File dutchDescriptionFile = ExtensionChecker.getFile(FileType.Description, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		AlphabeticIndex dutchIndex = getIndex(dutchConceptFile, dutchDescriptionFile);
		
		//compare each description in extension against the descriptions in core - warn if descriptions are too similar for different concepts
		return dutchIndex.compare(coreIndex, skipOrganisms, editDistance);
	}
		
	private class AlphabeticIndex
	{	//keys: extension, first character of description, entire description
		private Map<String,Map<Character,HashMapWithList<String,Description>>> map = 
				new HashMap<String,Map<Character,HashMapWithList<String,Description>>>();
		private Map<String,String> extensions;
		
		public AlphabeticIndex(Map<String,String> extensions)
		{
			this.extensions = extensions;
		}
		
		public boolean compare(AlphabeticIndex other, boolean skipOrganism, int editDistance) throws IOException
		{
			boolean result = true;
			DutchEditDistance distance = DutchEditDistance.getDutchEditDistance();
			distance.setAcceptableDistance(editDistance);
			HashMapWithList<String,String> matches = new HashMapWithList<String,String>();
			
			for (String extension : map.keySet())
			{
				if (skipOrganism && extension.equals("organism"))
					continue;
				if (!other.map.containsKey(extension))
					continue;

				Map<Character,HashMapWithList<String,Description>> alph = map.get(extension), alphOther = other.map.get(extension);
				for (Character c : alph.keySet())
				{
					if (!alphOther.containsKey(c))
						continue;

					HashMapWithList<String,Description> map1 = alph.get(c), map2 = alphOther.get(c);
					for (String desc1 : map1.keySet())
					{
						for (String desc2 : map2.keySet())
						{
							if (!distance.similar(desc1, desc2))
								continue;

							result = false;
							for (Description d1 : map1.get(desc1))
							{
								for (Description d2 : map2.get(desc2))
								{
									if (matches.containsKey(d1.conceptId) && matches.get(d1.conceptId).contains(d2.conceptId))
										continue;
									log.warn(d1.conceptId + " |" + d1.description + "| is similar to " + d2.conceptId + " |" + d2.description + "|");
									matches.add(d1.conceptId, d2.conceptId);
								}
							}
						}
					}
				}
			}
			
			return result;
		}
		
		
		public void add(Description d)
		{
			String extension = extensions.get(d.conceptId);
			char c = d.description.charAt(0);
			
			if (map.containsKey(extension))
			{
				Map<Character,HashMapWithList<String,Description>> alph = map.get(extension);
				if (alph.containsKey(c))
					alph.get(c).add(d.description, d);
				else
				{
					HashMapWithList<String,Description> list = new HashMapWithList<String,Description>();
					list.add(d.description, d);
					alph.put(c, list);
				}
			}
			else
			{
				HashMapWithList<String,Description> list = new HashMapWithList<String,Description>();
				list.add(d.description, d);
				Map<Character,HashMapWithList<String,Description>> alph = new HashMap<Character,HashMapWithList<String,Description>>();
				alph.put(c, list);
				map.put(extension, alph);
			}
		}
	}
	
	private AlphabeticIndex getIndex(File conceptFile, File descriptionFile) throws IOException, ParseException
	{	//create index on core descriptions by hierarchy and first letter
		Map<String,Boolean> concepts = ExtensionChecker.initConcepts(conceptFile);
		HashMapWithList<String,Description> descriptions = ExtensionChecker.getActiveDescriptions(false, concepts, descriptionFile);
		
		Map<String,String> extensions = new HashMap<String,String>();
		for (List<Description> list : descriptions.values())
		{
			for (Description d : list)
			{
				if (d.type == Type.FSN)
					extensions.put(d.conceptId, d.description.substring(d.description.lastIndexOf('(') + 1, d.description.lastIndexOf(')')));
			}
		}
		
		AlphabeticIndex result = new AlphabeticIndex(extensions);
		for (List<Description> list : descriptions.values())
		{
			for (Description d : list)
				result.add(d);
		}
		
		return result;
	}
}
