package nl.nictiz.snomed.check;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import nl.nictiz.snomed.check.Description.Type;
import nl.nictiz.snomed.util.HashMapWithList;

import org.apache.commons.io.FileUtils;
import org.apache.commons.validator.routines.checkdigit.CheckDigitException;
import org.apache.commons.validator.routines.checkdigit.VerhoeffCheckDigit;
import org.apache.log4j.Logger;

/**	Checks a collection of RF2-files for internal consistency.
 * This involves, amongst other things, checking if all id's are valid
 * and not duplicate, if identical concepts or descriptions are not
 * referred to by two separate id's, etc.
 * 
 * All warnings are printed to the console.
 * 
 * @author Feikje Hielkema
 * @version 0.1 16-01-2014
 */
public class ExtensionChecker 
{
	public static enum FileType {Concept, Description, Definition, Relation, StatedRelation, SimpleRefset, LanguageRefset, 
								RefsetDescriptor, Module, AssocationRefset, ConceptInactivationRefset, AttributeValueRefset,
								SimpleMap};
	
	public static final String nlSnapshotDir, nlFullDir, nlDeltaDir, coreSnapshotDir, coreDeltaDir, prevNlSnapshotDir, prevNlFullDir;	
	public static final String nlDate, coreDate, prevNlDate;
	public static final String dutchLanguageRefsetId, usLanguageRefsetId, dutchNamespace, b2iNamespace, dutchModuleId;
	
	private static Logger log = Logger.getLogger(ExtensionChecker.class);
	
	static
	{
		ResourceBundle paths = ResourceBundle.getBundle("paths", Locale.ROOT);
		nlSnapshotDir = paths.getString("nlSnapshotDir");
		nlFullDir = paths.getString("nlFullDir");
		nlDeltaDir = paths.getString("nlDeltaDir");
		coreSnapshotDir = paths.getString("coreSnapshotDir");
		coreDeltaDir = paths.getString("coreDeltaDir");
		prevNlSnapshotDir = paths.getString("prevNlSnapshotDir");
		prevNlFullDir = paths.getString("prevNlFullDir");
		nlDate = paths.getString("nlDate");
		coreDate = paths.getString("coreDate");
		prevNlDate = paths.getString("prevNlDate");
		dutchLanguageRefsetId = paths.getString("dutchLanguageRefsetId");
		usLanguageRefsetId = paths.getString("usLanguageRefsetId");
		dutchNamespace = paths.getString("dutchNamespace");
		b2iNamespace = paths.getString("b2iNamespace");
		dutchModuleId = paths.getString("dutchModuleId");
	}
	
	public static Map<String,Boolean> initConcepts(File... files) throws IOException
	{
		Map<String,Boolean> map = new HashMap<String,Boolean>();
		
		for (File file : files)
		{
			BufferedReader reader = new BufferedReader(new FileReader(file));
			reader.readLine();

			while (true)
			{
				String line = reader.readLine();
				if (line == null)
					break;

				String[] tokens = line.split("\t");
				map.put(tokens[0], tokens[2].equals("1"));
			}

			reader.close();
		}
		return map;
	}
	
	public static Map<String,String> getFSNs(Map<String,Boolean> concepts, File... descriptionFiles) throws ParseException, IOException
	{
		Map<String,String> map = new HashMap<String,String>();
		for (File file : descriptionFiles)
		{
			List<String> lines = FileUtils.readLines(file, "UTF-8");
			for (int i = 1; i < lines.size(); i++)
			{
				Description d = new Description(lines.get(i));
				if (d.type != Type.FSN)
					continue;
				else if (!(d.active && concepts.containsKey(d.conceptId) && concepts.get(d.conceptId)))	//we want only active descriptions of active concepts
					continue;
				else
					map.put(d.conceptId, d.description);
			}
		}
		return map;
	}
	
	public static Map<String,String> getAllActiveFSNs(Map<String,Boolean> concepts, File... descriptionFiles) throws ParseException, IOException
	{
		Map<String,String> map = new HashMap<String,String>();
		for (File file : descriptionFiles)
		{
			List<String> lines = FileUtils.readLines(file, "UTF-8");
			for (int i = 1; i < lines.size(); i++)
			{
				Description d = new Description(lines.get(i));
				if (d.active && (d.type == Type.FSN))
					map.put(d.conceptId, d.description);
			}
		}
		return map;
	} 
	
	public static Map<String,String> getPreferredTerms(Map<String,Boolean> concepts,
			File descriptionFile, File languageRefsetFile, String languageRefsetId) 
			throws ParseException, IOException
	{
		LanguageRefset refset = new LanguageRefset(languageRefsetFile);
		log.debug("loaded language refset file");
		
		Map<String,String> map = new HashMap<String,String>();
		List<String> lines = FileUtils.readLines(descriptionFile, "UTF-8");
		for (int i = 1; i < lines.size(); i++)
		{
			Description d = new Description(lines.get(i));
			if ((!d.active) || (d.type == Type.FSN))
				continue;
			else if (!( concepts.containsKey(d.conceptId) && concepts.get(d.conceptId)))	//we want only active descriptions of active concepts
				continue;
			else if (refset.isPreferred(languageRefsetId, d.descriptionId))
				map.put(d.conceptId, d.description);
		}
		
		return map;
	}

	public static HashMapWithList<String,Description> getDescriptionsByConceptID(File... files) throws IOException, ParseException
	{
		HashMapWithList<String,Description> map = new HashMapWithList<String,Description>();
		
		for (File file : files)
		{
			List<String> lines = FileUtils.readLines(file, "UTF-8");
			for (int i = 1; i < lines.size(); i++)
			{
				Description d = new Description(lines.get(i));
				map.add(d.conceptId, d);
			}
		}
		
		return map;
	}
	
	public static Map<String,Description> getActiveDescriptionsByDescriptionID(Map<String,Boolean> concepts, File... files) throws ParseException, IOException
	{
		Map<String,Description> map = new HashMap<String,Description>();
		for (File file : files)
		{
			List<String> lines = FileUtils.readLines(file, "UTF-8");
			for (int i = 1; i < lines.size(); i++)
			{
				Description d = new Description(lines.get(i));
				if (!(d.active && concepts.containsKey(d.conceptId) && concepts.get(d.conceptId)))	//we want only active descriptions of active concepts
					continue;
				else
					map.put(d.descriptionId, d);
			}
		}
		return map;
	}
	
	public static HashMapWithList<String,Description> getActiveDescriptions(boolean fsnOnly, Map<String,Boolean> concepts, File... descriptionFiles) 
			throws IOException, ParseException
	{	
		HashMapWithList<String,Description> map = new HashMapWithList<String,Description>();
		for (File file : descriptionFiles)
		{
			List<String> lines = FileUtils.readLines(file, "UTF-8");
			for (int i = 1; i < lines.size(); i++)
			{
				Description d = new Description(lines.get(i));
				if (!(d.active && concepts.containsKey(d.conceptId) && concepts.get(d.conceptId)))	//we want only active descriptions of active concepts
					continue;
				else if (fsnOnly && (d.type != Type.FSN))
					continue;
				else
					map.add(d.description, d);
			}
		}
		return map;
	}
	
	public static HashMapWithList<String,String> getActiveLanguageRefsetDescriptions(
			Map<String,Boolean> concepts, File descriptionFile, File languageRefsetFile, String languageRefsetId) 
					throws ParseException, IOException
	{
		HashMapWithList<String,String> result = new HashMapWithList<String,String>();
		LanguageRefset refset = new LanguageRefset(languageRefsetFile);	
		List<String> lines = FileUtils.readLines(descriptionFile, "UTF-8");
		
		for (int i = 1; i < lines.size(); i++)
		{
			Description d = new Description(lines.get(i));
			if (!d.active)
				continue;
			else if (!( concepts.containsKey(d.conceptId) && concepts.get(d.conceptId)))	//we want only active descriptions of active concepts
				continue;
			else if (refset.accepts(languageRefsetId, d.descriptionId))
				result.add(d.conceptId, d.description);
		}
		
		return result;
	}
		
	public static File getFile(FileType type, String dir, String date)
	{
		String extension = "_NL_", language = "nl", version = "Snapshot";
		if (dir.equals(coreSnapshotDir) ||  dir.equals(coreDeltaDir))	// || dir.equals(coreFullDir)
		{
			extension = "_INT_";
			language = "en";
		}
		
		if (dir.equals(nlDeltaDir) || dir.equals(coreDeltaDir))
			version = "Delta";
		else if (dir.equals(nlFullDir) || dir.equals(prevNlFullDir))	// || dir.equals(coreFullDir) 
			version = "Full";
		
		File file = getFile(type, version, extension, date, dir, language);
		if ((file != null) && (!file.exists()))
			file = getFile(type, version, "_DutchExtension_", date,dir, language);
		if ((file != null) && (!file.exists()))
			file = getFile(type, version, extension, date,dir, "en");
		return file;
	}	
	
	private static File getFile(FileType type, String version, String extension, String date, String dir,
			String language)
	{
		switch (type)
		{
			case Concept : return new File(dir + "Terminology/sct2_Concept_" + version + extension + date + ".txt");
			case Description: {
				File file = new File(dir + "Terminology/sct2_Description_" + version + extension + date + ".txt");
				if (file.exists())
					return file;
				else
					return new File(dir + "Terminology/sct2_Description_" + version + "-" + language + extension + date + ".txt");
			}
			case Definition: {
				File file = new File(dir + "Terminology/sct2_TextDefinition_" + version + extension + date + ".txt");
				if (file.exists())
					return file;
				else
					return new File(dir + "Terminology/sct2_TextDefinition_" + version + "-" + language + extension + date + ".txt");
			}
			case Relation: return new File(dir + "Terminology/sct2_Relationship_" + version + extension + date + ".txt");
			case StatedRelation: return new File(dir + "Terminology/sct2_StatedRelationship_" + version + extension + date + ".txt");
			case SimpleRefset: return new File(dir + "Refset/Content/der2_Refset_Simple" + version + extension + date + ".txt");
			case LanguageRefset: {
				File file = new File(dir + "Refset/Language/der2_cRefset_Language"+ version  + "-" + language + extension + date + ".txt");
				if (file.exists())
					return file;
				else
					return new File(dir + "Refset/Language/der2_cRefset_Language" + version + extension + date + ".txt");
			}
			case RefsetDescriptor: return new File(dir + "Refset/Metadata/der2_cciRefset_RefsetDescriptor" + version  + extension + date + ".txt");
			case Module: return new File(dir + "Refset/Metadata/der2_ssRefset_ModuleDependency" + version + extension + date + ".txt");	
			case AssocationRefset: return new File(dir + "Refset/Content/der2_cRefset_AssociationReference" + version + extension + date + ".txt");
			case ConceptInactivationRefset: return new File(dir + "Refset/Content/der2_cRefset_ConceptInactivationIndicatorReferenceSet" + version + extension + date + ".txt");
			case AttributeValueRefset: return new File(dir + "Refset/Content/der2_cRefset_AttributeValue" + version + extension + date + ".txt");
			case SimpleMap: return new File(dir + "Refset/Map/der2_sRefset_SimpleMap" + version + extension + date + ".txt");
			default: return null;
		}
	}
	
	public static Map<FileType,File> getAllFiles(String dir, String date)
	{
		Map<FileType,File> result = new HashMap<FileType,File>();
		for (FileType type : FileType.values())
			result.put(type, getFile(type, dir, date));
		
		return result;
	}
	
	public static boolean compareRefsetIds(Map<String,Boolean> refsetIds, Map<String,Boolean> metadata)
	{
		boolean result = true;
		for (String id : refsetIds.keySet())
		{
			if (!metadata.containsKey(id))
			{
				log.warn("Undefined reference set " + id);
				result = false;
			}
			else if (refsetIds.get(id) && (!metadata.get(id)))
			{
				log.warn("Reference set " + id + " is inactive in metadata but actively used");
				result = false;
			}
		}
		return result;
	}
}
