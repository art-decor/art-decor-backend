package nl.nictiz.snomed.check;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nl.nictiz.snomed.check.ExtensionChecker.FileType;
import nl.nictiz.snomed.util.HashMapWithList;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

/** Provides methods to check that the different version types in a single export
 * match each other.
 * 
 * @author hielkema
 * @version 1.1 30-07-2014
 */
public class ExportTypeComparer
{
	private static Logger log = Logger.getLogger(ExportTypeComparer.class);
	
	private SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
	
	/** Checks whether the most recent version of any object in Full occurs in Snapshot 
	 * @throws ParseException */
	public boolean compareSnapshotAndFull(String snapshotDir, String fullDir, String date) throws IOException, ParseException
	{
		boolean result = true;
		List<String> promoted = FileUtils.readLines(new File("resources/gepromoveerd.txt"));
		
		for (FileType type : ExtensionChecker.FileType.values())
		{
			log.debug("Comparing filetype " + type);
			try
			{
				if (!compareSnapshotAndFullFile(type, snapshotDir, fullDir, date, promoted))
					result = false;		
			}
			catch (FileNotFoundException e)
			{
				log.warn("Could not find file to compare: " + e.getMessage());
			}
		}
		
		return result;
	}
	
	/** Checks whether all concepts, descriptions, relations etc. in Delta occur in Snapshot
	 * in the same state. Please note that this method assumes that Delta does NOT contain 
	 * changes on concepts that are neither active now, nor published in a previous release!
	 * @throws IOException 
	 */
	public boolean compareSnapshotAndDelta(String snapshotDir, String deltaDir, String date) throws IOException
	{
		boolean result = true;
		for (FileType type : ExtensionChecker.FileType.values())
		{
			try
			{
				if (!compareSnapshotAndDeltaFile(type, snapshotDir, deltaDir, date))
					result = false;		
			}
			catch (FileNotFoundException e)
			{
				log.warn("Could not find file to compare: " + e.getMessage());
			}
		}
				
		return result;
	}
	
	private boolean compareSnapshotAndFullFile(FileType type, String snapshotDir, String fullDir, String date, List<String> promoted) 
				throws IOException, ParseException
	{
		File fullFile = ExtensionChecker.getFile(type, fullDir, date);
		File snapshotFile = ExtensionChecker.getFile(type, snapshotDir, date);
		boolean result = true;
		
		//create a map with, for each object, the most recent version
		Map<String,String> fullMap = new HashMap<String,String>();
		List<String> lines = FileUtils.readLines(fullFile);
		for (int i = 0; i < lines.size(); i++)
		{
			String line = lines.get(i);
			if (line.trim().isEmpty())
				log.warn("Empty line at " + i + " in file " + fullFile.getName());
				
			String[] tokens = line.split("\t");
			if (!fullMap.containsKey(tokens[0]))
				fullMap.put(tokens[0], line);
			else
			{
				String[] otherLine = fullMap.get(tokens[0]).split("\t");
				Date currentDate = format.parse(tokens[1]);
				Date otherDate = format.parse(otherLine[1]);
				if (currentDate.after(otherDate) || currentDate.equals(otherDate))	//if dates are equal, rely on file order, use last line
					fullMap.put(tokens[0], line);				
			}
		}
		
		//now check if all those lines occur in snapshot and the other way around
		Set<String> fullLines = new HashSet<String>(fullMap.values());
		Set<String> snapshotLines = new HashSet<String>();
		List<String> list = FileUtils.readLines(snapshotFile);
		for (int i = 0; i < list.size(); i++)
		{
			String line = list.get(i);
			if (line.isEmpty())
			{
				log.warn("Emtpy line " + i + " in file " + snapshotFile.getName());
				result = false;
			}
			else
				snapshotLines.add(line);	
		}
		int conceptIdColumn = getConceptIdColumn(type);
		
		for (String line : fullLines)
		{
			if (!snapshotLines.contains(line))
			{
				String id = line.split("\t")[conceptIdColumn];
				if (promoted.contains(id) && (type == FileType.Concept))
					continue;	//ignore entries of promoted concepts in concept-file, they are supposed to cause a discrepancy
				else
				{
					log.warn("Full " + type +  "-file does not match snapshot in line: " + line);
					result = false;
				}
			}
		}
		
		for (String line : snapshotLines)
		{
			if (!fullLines.contains(line))
			{
				log.warn("Snapshot " + type +  "-file does not match full in line: " + line);
				result = false;
			}
		}
		
		return result;
	}
	
	private int getConceptIdColumn(FileType type)
	{
		switch (type)
		{
			case Concept: return 0;
			case Description: return 4;
			case Definition: return 4;
			case Relation: return 4;
			default: return 5;
		}
	}
	
	private boolean compareSnapshotAndDeltaFile(FileType type, String snapshotDir, String deltaDir, String date) throws IOException
	{
		File deltaFile = ExtensionChecker.getFile(type, deltaDir, date);
		File snapshotFile = ExtensionChecker.getFile(type, snapshotDir, date);
		List<String> deltaLines = FileUtils.readLines(deltaFile);
		Set<String> snapshotLines = new HashSet<String>(FileUtils.readLines(snapshotFile));
		boolean result = true;
		
		for (int i = 0; i < deltaLines.size(); i++)
		{
			String line = deltaLines.get(i);
			if (!snapshotLines.contains(line))
			{
				log.warn("Delta " + type +  "-file contains line not in snapshot: " + line + " (" + i + ")");
				result = false;
			}
		}

		return result;
	}

	/** Checks whether prev_full + curr_delta = curr_full. This means that the current full-version should contain
	 * every single line in the previous full version + the current delta.
	 * @throws IOException 
	 */
	public boolean compareAgainstPreviousFullRelease(String prevDir, String deltaDir, String fullDir, String currentDate, String prevDate) throws IOException
	{
		boolean result = true;
		for (FileType type : ExtensionChecker.FileType.values())
		{
			if (!compareAgainstPreviousFullFile(type, fullDir, deltaDir, prevDir, currentDate, prevDate))
				result = false;		
		}
		return result;
	}

	private boolean compareAgainstPreviousFullFile(FileType type, String fullDir, String deltaDir, String prevDir,
			String currentDate, String prevDate) throws IOException
	{
		boolean result = true;
		File prevFile = ExtensionChecker.getFile(type, prevDir, prevDate);
		List<String> prev = prevFile.exists() ? FileUtils.readLines(prevFile) : new ArrayList<String>();
		Set<String> prevConceptId = new HashSet<String>();
				
		File deltaFile = ExtensionChecker.getFile(type, deltaDir, currentDate);
		if (deltaFile.exists())
		{
			for (String line : FileUtils.readLines(deltaFile))
			{
				prev.add(line);
				prevConceptId.add(line.split("\t")[0]);
			}
		}
			
		File currentFile = ExtensionChecker.getFile(type, fullDir, currentDate);
		List<String> current = currentFile.exists() ? FileUtils.readLines(currentFile) : new ArrayList<String>();
		Map<String,Integer> differentVersionsId = new HashMap<String,Integer>();
		HashMapWithList<String,String> problems = new HashMapWithList<String,String>();
		
		//check whether the content of the map is identical to the content of the current snapshot
		for (String line : current)
		{
			String id = line.split("\t")[0];
			int nr = 1;
			if (differentVersionsId.containsKey(id))
				nr += differentVersionsId.get(id);
			differentVersionsId.put(id, nr);
			
			if (!prev.contains(line))
				problems.add(id, line);
		}
		
		for (String id : problems.keySet())
		{	//delta contains only most recent change; more can occur in full if they took place in the period between releases
			if (differentVersionsId.get(id) <= problems.get(id).size())
			{
				result = false;
				log.warn(type + "-file in current full contains entries for " + id + " which do not occur in previous snapshot or current delta:");
				for (String line : problems.get(id))
					System.out.println(line);
			}
		}

		for (String line : prev)
		{		
			if (!current.contains(line))
			{
				result = false;
				String id = line.split("\t")[0];
				if (!problems.containsKey(id))	//check if discrepancy has been recorded already
					log.warn(type + "-file in previous full or current delta contains line " + line + " which does not occur in current full");
			}
		}		

		return result;
	}

	/** Checks whether prev_snapshot + curr_delta - prev_version_of_curr_objects = curr_snapshot. This means that the
	 * current snapshot should contain all lines in delta and all lines in the previous snapshot that have not been
	 * replaced by delta (recognizable as they have the same identifier).
	 * @throws IOException 
	 */
	public boolean compareAgainstPreviousSnapshotRelease(String prevDir, String deltaDir, String snapshotDir, String currentDate, String prevDate) throws IOException
	{
		List<String> promoted = FileUtils.readLines(new File("resources/gepromoveerd.txt"));
		boolean result = true;
		for (FileType type : ExtensionChecker.FileType.values())
		{
			if (!compareAgainstPreviousSnapshotFile(type, snapshotDir, deltaDir, prevDir, currentDate, prevDate, promoted))
				result = false;		
		}
				
		return result;
	}
		
	private boolean compareAgainstPreviousSnapshotFile(FileType type, String snapshotDir, String deltaDir, String prevDir, 
			String currentDate, String prevDate, List<String> promoted) throws IOException
	{
		boolean result = true;
		//compile a map of lines in the previous snapshot, where the line id is the key
		File prevSnapshotFile = ExtensionChecker.getFile(type, prevDir, prevDate);	//getPrevExtensionFile(type, prevDir);
		Map<String,String> lineMap = new HashMap<String,String>();
		
		if (prevSnapshotFile.exists())
		{
			for (String line : FileUtils.readLines(prevSnapshotFile))
			{
				String[] tokens = line.split("\t");
				lineMap.put(tokens[0], line);
			}
		}
		
		//add all lines in the current delta to the map. Lines with the same identifier will be replaced
		File currentDeltaFile = ExtensionChecker.getFile(type, deltaDir, currentDate);
		if (currentDeltaFile.exists())
		{
			for (String line : FileUtils.readLines(currentDeltaFile))
			{
				String[] tokens = line.split("\t");
				lineMap.put(tokens[0], line);
			}
		}
		
		//load all lines in the current snapshot
		File currentSnapshotFile = ExtensionChecker.getFile(type, snapshotDir, currentDate);
		Map<String,String> snapshotMap = new HashMap<String,String>();
		if (currentSnapshotFile.exists())
		{
			for (String line : FileUtils.readLines(currentSnapshotFile))
			{
				String[] tokens = line.split("\t");
				snapshotMap.put(tokens[0], line);
			}
		}
		
		//check whether the content of the map is identical to the content of the current snapshot
		for (String id : snapshotMap.keySet())
		{
			if (!lineMap.containsKey(id))
			{
				log.warn(type + "-file in current snapshot contains entry for id " + id + " which does not occur in previous snapshot or current delta");
				result = false;
			}
			else if (!lineMap.get(id).equals(snapshotMap.get(id)))
			{
				log.warn(type + "-file contains different versions of " + id + " in current snapshot and prev. snapshot + delta");
				result = false;
			}
		}
		
		for (String id : lineMap.keySet())
		{
			if (!snapshotMap.containsKey(id))
			{
				String conceptId = lineMap.get(id).split("\t")[getConceptIdColumn(type)];
				if (promoted.contains(conceptId) && (type == FileType.Concept))
					continue;	//a promoted conceptId should feature in previous snapshot but not this one, so ignore the discrepancy it causes
				else
				{
					log.warn(type + "-file in previous snapshot or current delta contains entry for id " + id + " which does not occur in current snapshot");
					result = false;
				}
			}
		}
		
		return result;
	}
	
	private boolean onlyDateDiffers(String line1, String line2, String id)
	{
		int splitIdx = line1.indexOf('\t', id.length() + 2);
		if (!line1.substring(splitIdx).equals(line2.substring(splitIdx)))
			return false;
		
		if (!line1.substring(0, splitIdx).equals(line2.substring(0, splitIdx)))
			return true;
		
		log.warn("Identical?? " + splitIdx + "; " + line1 + "; " + line2);
		return false;
	}
}
