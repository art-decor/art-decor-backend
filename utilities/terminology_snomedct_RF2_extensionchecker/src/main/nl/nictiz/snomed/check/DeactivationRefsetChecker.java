package nl.nictiz.snomed.check;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import nl.nictiz.snomed.check.ExtensionChecker.FileType;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;

/** Checks if every concept in the concept inactivation refset is invalid
 * and the final column contains an ID in the inactivation list;
 * 
 * Checks if every active historical association is between an inactive and
 * active concept and of a valid type.
 * 
 * @author Feikje H-ielkema-Raadsveld
 * @version 1.1 19-08-2015
 */
public class DeactivationRefsetChecker
{
	private static Logger log = Logger.getLogger(DeactivationRefsetChecker.class);
	
	public static final String conceptInactivationRefset = "900000000000489007", descriptionInactivationRefset = "900000000000490003";
	public static final String[] conceptInactivationValues = {"900000000000484002", "900000000000482003", "900000000000485001", 
				"900000000000486000", "900000000000487009", "900000000000483008", "900000000000492006"};
	public static final String[] historicalAssociationRefsets = {"900000000000527005", "900000000000530003", "900000000000525002",
				"900000000000524003", "900000000000523009", "900000000000531004", "900000000000526001", "900000000000529008", "900000000000528000"};
	
	public boolean checkDeactivationRefsetSnapshot(FileType type) throws IOException
	{
		File file = ExtensionChecker.getFile(type, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		List<String> lines = FileUtils.readLines(file);
		log.info("Checking " + file.getName());
		boolean result = true;
		
		Map<String,Boolean> dutchConcepts = ExtensionChecker.initConcepts(ExtensionChecker.getFile(FileType.Concept, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate));
		Map<String,Boolean> coreConcepts = null;
		if (type == FileType.AssocationRefset) 
			coreConcepts = ExtensionChecker.initConcepts(ExtensionChecker.getFile(FileType.Concept, ExtensionChecker.coreSnapshotDir, ExtensionChecker.coreDate));
		
		for (int i = 1; i < lines.size(); i++)
		{	
			String[] line = lines.get(i).split("\t");
			if (coreConcepts != null)
			{
				if (!checkLineHistoricalAssocationRefset(line, i + 1, dutchConcepts, coreConcepts))
					result = false;
			}
			else if (line[4].equals(descriptionInactivationRefset))
				continue;
			else if (!checkLineConceptInactivationRefset(line, i + 1, dutchConcepts))
				result = false;			
		}
		
		return result;
	}
	
	private boolean checkLineConceptInactivationRefset(String[] line, int lineNr, Map<String,Boolean> dutchConcepts)
	{
		boolean result = true;
		if (!line[4].equals(conceptInactivationRefset))
		{
			log.warn("Line " + lineNr + " in wrong refset");
			result = false;
		}
		
		if (!dutchConcepts.containsKey(line[5]))
		{
			log.warn(line[5] + " in line " + lineNr + " is not an existing Dutch SCTID");
			result = false;
		}
		else if (line[2].equals("1") && dutchConcepts.get(line[5]))
		{
			log.warn(line[5] + " in line " + lineNr + " is active in concept-file but has an inactivation value!");
			result = false;
		}
		
		if (!ArrayUtils.contains(conceptInactivationValues, line[6]))
		{
			log.warn(line[6] + " in line " + lineNr + " is not a concept inactivation value");
			result = false;
		}
		
		return result;
	}
		
	//column 4 must be historicalAssociationRefset, 5 and 6 must be valid ID's, if column 2 is active then column 5 must be inactive and 6 active
	private boolean checkLineHistoricalAssocationRefset(String[] line, int lineNr, Map<String,Boolean> dutchConcepts, Map<String,Boolean> coreConcepts)
	{
		boolean result = true;
		if (!ArrayUtils.contains(historicalAssociationRefsets, line[4]))
		{
			log.warn("Line " + lineNr + " not in historical association refset");
			result = false;
		}
		
		if (!dutchConcepts.containsKey(line[5]))
		{
			log.warn(line[5] + " in line " + lineNr + " is not an existing Dutch SCTID");
			result = false;
		}
		else if (line[2].equals("1") && dutchConcepts.get(line[5]))
		{
			log.warn(line[5] + " in line " + lineNr + " is active in concept-file but has a historical association!");
			result = false;
		}
		
		if (coreConcepts.containsKey(line[6]) && coreConcepts.get(line[6]))
			return result;
		else if (dutchConcepts.containsKey(line[6]) && dutchConcepts.get(line[6]))
			return result;
		else if (line[2].equals("0") && IDChecker.isValidSCTID(line[6]))
			return result;
		
		log.warn(line[6] + " in line " + lineNr + " is inactive or does not exist.");
		return false;
	}
}
