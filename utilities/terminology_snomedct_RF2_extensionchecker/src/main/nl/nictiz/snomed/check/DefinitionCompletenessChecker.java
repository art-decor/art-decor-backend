package nl.nictiz.snomed.check;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import nl.nictiz.snomed.check.ExtensionChecker.FileType;
import nl.nictiz.snomed.tree.SnomedRelationReader;
import nl.nictiz.snomed.util.HashMapWithList;

public class DefinitionCompletenessChecker
{
	private static Logger log = Logger.getLogger(DefinitionCompletenessChecker.class);
	
	private String type, date;
	private Map<String,Boolean> concepts;
	
	public DefinitionCompletenessChecker(String type, String date) throws IOException
	{
		this.type = type;
		this.date = date;
		this.concepts = IDChecker.readColumnFromFile(
				ExtensionChecker.getFile(FileType.Concept, type, date), 0);
	}
	
	/* Check for each active concept it is possesses at least one active is-a-relation
	 * 
	 */
	public boolean checkMinimumOneIsARelation(File relationFile) throws IOException
	{
		SnomedRelationReader relations = new SnomedRelationReader(relationFile);
		
		boolean result = true;
		for (String conceptId : concepts.keySet())
		{
			if (!concepts.get(conceptId))
				continue;
			
			if (relations.getParents(conceptId).size() == 0)
			{
				log.warn(conceptId + " has no isA-relations!");
				result = false;
			}
		}
		
		return result;
	}
	
	public boolean checkFSNAndPrefTerm(LanguageRefset languageRefset, 
			HashMapWithList<String,Description> descriptions, String refsetId)
	{
		boolean result = true;
		
		for (String conceptId : concepts.keySet())
		{
			if (!concepts.get(conceptId))
				continue;	//skip inactive concepts
			
			if (!descriptions.containsKey(conceptId))
			{
				log.warn("No descriptions at all for " + conceptId);
				result = false;
				continue;
			}
			
			List<Description> descriptionList = descriptions.get(conceptId);	//get this concept's descriptions
			int fsnNr = getFSNNumber(descriptionList);
			
			if (fsnNr != 1)
			{	//number of English FSNs must be exactly one
				result = false;
				log.warn("Concept " + conceptId + " has " + fsnNr + " English FSNs");
			}
			
			int prefNr = languageRefset.getPreferred(descriptionList, refsetId);
			if (prefNr != 1)
			{	//number of preferred terms within language refset must be exactly one
				result = false;
				log.warn("Concept " + conceptId + " has " + prefNr + " preferred terms within the language reference set " + refsetId);
			}
		}
		
		return result;
	}
	
	private int getFSNNumber(List<Description> descriptions)
	{	//only  checks English FSNs!
		int nr = 0;
		for (Description d : descriptions)
		{
			if (d.active && d.languageCode.equals(Description.English) && d.type.equals(Description.Type.FSN))
				nr++;
		}
		return nr;
	}
}
