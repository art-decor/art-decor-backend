package nl.nictiz.snomed.check;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nl.nictiz.snomed.util.HashMapWithList;

import org.apache.log4j.Logger;

public class SimpleRefset
{
	private static Logger log = Logger.getLogger(SimpleRefset.class);
	private List<Entry> entries = new ArrayList<Entry>();
	
	public SimpleRefset(File file) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(file));
		reader.readLine();

		while (true)
		{
			String line = reader.readLine();
			if (line == null)
				break;

			entries.add(new Entry(line.split("\t")));
		}
	}
	
	private class Entry
	{
		final String uuid, refsetId, referenceId;
		final boolean active;
		
		public Entry(String[] line)
		{
			this.uuid = line[0];
			this.active = line[2].equals("1");
			this.refsetId = line[4];
			this.referenceId = line[5];
		}
	}
	
	public Map<String,Boolean> getReferenceIds()
	{
		Map<String,Boolean> result = new HashMap<String,Boolean>();
		for (Entry entry : entries)
			result.put(entry.referenceId, entry.active);
		return result;
	}
	
	public boolean checkReferenceUniquenessPerRefset()
	{
		boolean result = true;
		Map<String,Map<String,String>> references = new HashMap<String,Map<String,String>>();
		
		for (Entry entry : entries)
		{
			if (references.containsKey(entry.referenceId))
			{
				Map<String,String> map = references.get(entry.referenceId);
				if (map.containsKey(entry.refsetId) && (!map.get(entry.refsetId).equals(entry.uuid)))
				{	
					log.warn("Concept " + entry.referenceId + " occurs with different UUID's in refset " + entry.refsetId);
					result = false;
				}
				else
					map.put(entry.refsetId, entry.uuid);
			}
			else
			{
				Map<String,String> map = new HashMap<String,String>();
				map.put(entry.refsetId, entry.uuid);
				references.put(entry.referenceId, map);
			}
		}
		
		return result;
	}
	
	public boolean checkPreferredTerm(LanguageRefset languageRefset, HashMapWithList<String,Description> descriptions, 
			Map<String,Boolean> concepts, String refsetID)
	{
		boolean result = true;
		for (Entry entry : entries)
		{
			if (!entry.active)
				continue;
			
			if (!entry.referenceId.contains(ExtensionChecker.dutchNamespace))
				continue;
			
			if (!descriptions.containsKey(entry.referenceId))
			{
				log.warn("no descriptions at all for " + entry.referenceId);
				result = false;
				continue;
			}
			
			List<Description> descriptionList = descriptions.get(entry.referenceId);	//get this concept's descriptions
			int nr = languageRefset.getPreferred(descriptionList, refsetID);
			if (nr == 1)
				continue;
			else if ((nr == 0) && (!concepts.get(entry.referenceId)))
				continue;	//inactive concepts do not need a preferred term
			
			result = false;
			if (nr > 1)
				log.warn("Concept " + entry.referenceId + " has " + nr + " preferred terms within the language reference set " + refsetID);
			else
				log.warn("Concept " + entry.referenceId + " has no preferred terms in the language reference set " + refsetID + " (or is inactive?)");
		}
		return result;
	}
	
	public Map<String,Boolean> getRefsetIds()
	{
		Map<String,Boolean> result = new HashMap<String,Boolean>();
		for (Entry entry : entries)
			result.put(entry.refsetId, entry.active);
		return result;
	}
}
