package nl.nictiz.snomed.postcoordination;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class ParseData
{
	public static enum NodeType {Attribute, Other};
	
	public String sctid, term, attribute, source, target;
	public boolean success = true;
	public NodeType type = NodeType.Other;
	
	public Document doc;
	public Element currentNode;
	
	public void reset()
	{
		this.success = true;
		this.term = null;
		this.sctid = null;
		this.attribute = null;
		this.source = null;
		this.target = null;
		this.type = NodeType.Other;
	}
}
