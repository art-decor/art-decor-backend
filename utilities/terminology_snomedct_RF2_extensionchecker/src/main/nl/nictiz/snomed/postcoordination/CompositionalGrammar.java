package nl.nictiz.snomed.postcoordination;

import apg.Grammar;
import java.io.PrintStream;

public class CompositionalGrammar extends Grammar{

    // public API
    public static Grammar getInstance(){
        if(factoryInstance == null){
            factoryInstance = new CompositionalGrammar(getRules(), getUdts(), getOpcodes());
        }
        return factoryInstance;
    }

    // rule name enum
    public static int ruleCount = 38;
    public enum RuleNames{
        ANYNONESCAPEDCHAR("anyNonEscapedChar", 32, 144, 10),
        ATTRIBUTE("attribute", 12, 73, 6),
        ATTRIBUTEGROUP("attributeGroup", 10, 59, 6),
        ATTRIBUTENAME("attributeName", 13, 79, 1),
        ATTRIBUTESET("attributeSet", 11, 65, 8),
        ATTRIBUTEVALUE("attributeValue", 14, 80, 9),
        BS("BS", 27, 134, 1),
        CONCEPTID("conceptId", 7, 39, 1),
        CONCEPTREFERENCE("conceptReference", 6, 29, 10),
        CR("CR", 24, 131, 1),
        DECIMALVALUE("decimalValue", 19, 114, 5),
        DEFINITIONSTATUS("definitionStatus", 2, 16, 3),
        DIGIT("digit", 28, 135, 1),
        DIGITNONZERO("digitNonZero", 30, 137, 1),
        EQUIVALENTTO("equivalentTo", 3, 19, 1),
        ESCAPEDCHAR("escapedChar", 33, 154, 7),
        EXPRESSION("expression", 0, 0, 8),
        EXPRESSIONVALUE("expressionValue", 15, 89, 8),
        FOCUSCONCEPT("focusConcept", 5, 21, 8),
        HTAB("HTAB", 23, 130, 1),
        INTEGERVALUE("integerValue", 18, 104, 10),
        LF("LF", 25, 132, 1),
        NONWSNONPIPE("nonwsNonPipe", 31, 138, 6),
        NUMERICVALUE("numericValue", 17, 101, 3),
        QM("QM", 26, 133, 1),
        REFINEMENT("refinement", 9, 47, 12),
        SCTID("sctId", 20, 119, 4),
        SP("SP", 22, 129, 1),
        STRINGVALUE("stringValue", 16, 97, 4),
        SUBEXPRESSION("subExpression", 1, 8, 8),
        SUBTYPEOF("subtypeOf", 4, 20, 1),
        TERM("term", 8, 40, 7),
        UTF8_2("UTF8-2", 34, 161, 3),
        UTF8_3("UTF8-3", 35, 164, 17),
        UTF8_4("UTF8-4", 36, 181, 15),
        UTF8_TAIL("UTF8-tail", 37, 196, 1),
        WS("ws", 21, 123, 6),
        ZERO("zero", 29, 136, 1);
        private String name;
        private int id;
        private int offset;
        private int count;
        RuleNames(String string, int id, int offset, int count){
            this.name = string;
            this.id = id;
            this.offset = offset;
            this.count = count;
        }
        public  String ruleName(){return name;}
        public  int    ruleID(){return id;}
        private int    opcodeOffset(){return offset;}
        private int    opcodeCount(){return count;}
    }

    // UDT name enum
    public static int udtCount = 0;
    public enum UdtNames{
    }

    // private
    private static CompositionalGrammar factoryInstance = null;
    private CompositionalGrammar(Rule[] rules, Udt[] udts, Opcode[] opcodes){
        super(rules, udts, opcodes);
    }

    private static Rule[] getRules(){
    	Rule[] rules = new Rule[38];
        for(RuleNames r : RuleNames.values()){
            rules[r.ruleID()] = getRule(r.ruleID(), r.ruleName(), r.opcodeOffset(), r.opcodeCount());
        }
        return rules;
    }

    private static Udt[] getUdts(){
    	Udt[] udts = new Udt[0];
        return udts;
    }

        // opcodes
    private static Opcode[] getOpcodes(){
    	Opcode[] op = new Opcode[197];
        {int[] a = {1,2,6,7}; op[0] = getOpcodeCat(a);}
        op[1] = getOpcodeRnm(21, 123); // ws
        op[2] = getOpcodeRep((char)0, (char)1, 3);
        {int[] a = {4,5}; op[3] = getOpcodeCat(a);}
        op[4] = getOpcodeRnm(2, 16); // definitionStatus
        op[5] = getOpcodeRnm(21, 123); // ws
        op[6] = getOpcodeRnm(1, 8); // subExpression
        op[7] = getOpcodeRnm(21, 123); // ws
        {int[] a = {9,10}; op[8] = getOpcodeCat(a);}
        op[9] = getOpcodeRnm(5, 21); // focusConcept
        op[10] = getOpcodeRep((char)0, (char)1, 11);
        {int[] a = {12,13,14,15}; op[11] = getOpcodeCat(a);}
        op[12] = getOpcodeRnm(21, 123); // ws
        {char[] a = {58}; op[13] = getOpcodeTls(a);}
        op[14] = getOpcodeRnm(21, 123); // ws
        op[15] = getOpcodeRnm(9, 47); // refinement
        {int[] a = {17,18}; op[16] = getOpcodeAlt(a);}
        op[17] = getOpcodeRnm(3, 19); // equivalentTo
        op[18] = getOpcodeRnm(4, 20); // subtypeOf
        {char[] a = {61,61,61}; op[19] = getOpcodeTls(a);}
        {char[] a = {60,60,60}; op[20] = getOpcodeTls(a);}
        {int[] a = {22,23}; op[21] = getOpcodeCat(a);}
        op[22] = getOpcodeRnm(6, 29); // conceptReference
        op[23] = getOpcodeRep((char)0, Character.MAX_VALUE, 24);
        {int[] a = {25,26,27,28}; op[24] = getOpcodeCat(a);}
        op[25] = getOpcodeRnm(21, 123); // ws
        {char[] a = {43}; op[26] = getOpcodeTls(a);}
        op[27] = getOpcodeRnm(21, 123); // ws
        op[28] = getOpcodeRnm(6, 29); // conceptReference
        {int[] a = {30,31}; op[29] = getOpcodeCat(a);}
        op[30] = getOpcodeRnm(7, 39); // conceptId
        op[31] = getOpcodeRep((char)0, (char)1, 32);
        {int[] a = {33,34,35,36,37,38}; op[32] = getOpcodeCat(a);}
        op[33] = getOpcodeRnm(21, 123); // ws
        {char[] a = {124}; op[34] = getOpcodeTls(a);}
        op[35] = getOpcodeRnm(21, 123); // ws
        op[36] = getOpcodeRnm(8, 40); // term
        op[37] = getOpcodeRnm(21, 123); // ws
        {char[] a = {124}; op[38] = getOpcodeTls(a);}
        op[39] = getOpcodeRnm(20, 119); // sctId
        {int[] a = {41,42}; op[40] = getOpcodeCat(a);}
        op[41] = getOpcodeRnm(31, 138); // nonwsNonPipe
        op[42] = getOpcodeRep((char)0, Character.MAX_VALUE, 43);
        {int[] a = {44,46}; op[43] = getOpcodeCat(a);}
        op[44] = getOpcodeRep((char)0, Character.MAX_VALUE, 45);
        op[45] = getOpcodeRnm(22, 129); // SP
        op[46] = getOpcodeRnm(31, 138); // nonwsNonPipe
        {int[] a = {48,51}; op[47] = getOpcodeCat(a);}
        {int[] a = {49,50}; op[48] = getOpcodeAlt(a);}
        op[49] = getOpcodeRnm(11, 65); // attributeSet
        op[50] = getOpcodeRnm(10, 59); // attributeGroup
        op[51] = getOpcodeRep((char)0, Character.MAX_VALUE, 52);
        {int[] a = {53,54,58}; op[52] = getOpcodeCat(a);}
        op[53] = getOpcodeRnm(21, 123); // ws
        op[54] = getOpcodeRep((char)0, (char)1, 55);
        {int[] a = {56,57}; op[55] = getOpcodeCat(a);}
        {char[] a = {44}; op[56] = getOpcodeTls(a);}
        op[57] = getOpcodeRnm(21, 123); // ws
        op[58] = getOpcodeRnm(10, 59); // attributeGroup
        {int[] a = {60,61,62,63,64}; op[59] = getOpcodeCat(a);}
        {char[] a = {123}; op[60] = getOpcodeTls(a);}
        op[61] = getOpcodeRnm(21, 123); // ws
        op[62] = getOpcodeRnm(11, 65); // attributeSet
        op[63] = getOpcodeRnm(21, 123); // ws
        {char[] a = {125}; op[64] = getOpcodeTls(a);}
        {int[] a = {66,67}; op[65] = getOpcodeCat(a);}
        op[66] = getOpcodeRnm(12, 73); // attribute
        op[67] = getOpcodeRep((char)0, Character.MAX_VALUE, 68);
        {int[] a = {69,70,71,72}; op[68] = getOpcodeCat(a);}
        op[69] = getOpcodeRnm(21, 123); // ws
        {char[] a = {44}; op[70] = getOpcodeTls(a);}
        op[71] = getOpcodeRnm(21, 123); // ws
        op[72] = getOpcodeRnm(12, 73); // attribute
        {int[] a = {74,75,76,77,78}; op[73] = getOpcodeCat(a);}
        op[74] = getOpcodeRnm(13, 79); // attributeName
        op[75] = getOpcodeRnm(21, 123); // ws
        {char[] a = {61}; op[76] = getOpcodeTls(a);}
        op[77] = getOpcodeRnm(21, 123); // ws
        op[78] = getOpcodeRnm(14, 80); // attributeValue
        op[79] = getOpcodeRnm(6, 29); // conceptReference
        {int[] a = {81,82,86}; op[80] = getOpcodeAlt(a);}
        op[81] = getOpcodeRnm(15, 89); // expressionValue
        {int[] a = {83,84,85}; op[82] = getOpcodeCat(a);}
        op[83] = getOpcodeRnm(26, 133); // QM
        op[84] = getOpcodeRnm(16, 97); // stringValue
        op[85] = getOpcodeRnm(26, 133); // QM
        {int[] a = {87,88}; op[86] = getOpcodeCat(a);}
        {char[] a = {35}; op[87] = getOpcodeTls(a);}
        op[88] = getOpcodeRnm(17, 101); // numericValue
        {int[] a = {90,91}; op[89] = getOpcodeAlt(a);}
        op[90] = getOpcodeRnm(6, 29); // conceptReference
        {int[] a = {92,93,94,95,96}; op[91] = getOpcodeCat(a);}
        {char[] a = {40}; op[92] = getOpcodeTls(a);}
        op[93] = getOpcodeRnm(21, 123); // ws
        op[94] = getOpcodeRnm(1, 8); // subExpression
        op[95] = getOpcodeRnm(21, 123); // ws
        {char[] a = {41}; op[96] = getOpcodeTls(a);}
        op[97] = getOpcodeRep((char)1, Character.MAX_VALUE, 98);
        {int[] a = {99,100}; op[98] = getOpcodeAlt(a);}
        op[99] = getOpcodeRnm(32, 144); // anyNonEscapedChar
        op[100] = getOpcodeRnm(33, 154); // escapedChar
        {int[] a = {102,103}; op[101] = getOpcodeAlt(a);}
        op[102] = getOpcodeRnm(19, 114); // decimalValue
        op[103] = getOpcodeRnm(18, 104); // integerValue
        {int[] a = {105,113}; op[104] = getOpcodeAlt(a);}
        {int[] a = {106,110,111}; op[105] = getOpcodeCat(a);}
        op[106] = getOpcodeRep((char)0, (char)1, 107);
        {int[] a = {108,109}; op[107] = getOpcodeAlt(a);}
        {char[] a = {45}; op[108] = getOpcodeTls(a);}
        {char[] a = {43}; op[109] = getOpcodeTls(a);}
        op[110] = getOpcodeRnm(30, 137); // digitNonZero
        op[111] = getOpcodeRep((char)0, Character.MAX_VALUE, 112);
        op[112] = getOpcodeRnm(28, 135); // digit
        op[113] = getOpcodeRnm(29, 136); // zero
        {int[] a = {115,116,117}; op[114] = getOpcodeCat(a);}
        op[115] = getOpcodeRnm(18, 104); // integerValue
        {char[] a = {46}; op[116] = getOpcodeTls(a);}
        op[117] = getOpcodeRep((char)1, Character.MAX_VALUE, 118);
        op[118] = getOpcodeRnm(28, 135); // digit
        {int[] a = {120,121}; op[119] = getOpcodeCat(a);}
        op[120] = getOpcodeRnm(30, 137); // digitNonZero
        op[121] = getOpcodeRep((char)5, (char)17, 122);
        op[122] = getOpcodeRnm(28, 135); // digit
        op[123] = getOpcodeRep((char)0, Character.MAX_VALUE, 124);
        {int[] a = {125,126,127,128}; op[124] = getOpcodeAlt(a);}
        op[125] = getOpcodeRnm(22, 129); // SP
        op[126] = getOpcodeRnm(23, 130); // HTAB
        op[127] = getOpcodeRnm(24, 131); // CR
        op[128] = getOpcodeRnm(25, 132); // LF
        {char[] a = {32}; op[129] = getOpcodeTbs(a);}
        {char[] a = {9}; op[130] = getOpcodeTbs(a);}
        {char[] a = {13}; op[131] = getOpcodeTbs(a);}
        {char[] a = {10}; op[132] = getOpcodeTbs(a);}
        {char[] a = {34}; op[133] = getOpcodeTbs(a);}
        {char[] a = {92}; op[134] = getOpcodeTbs(a);}
        op[135] = getOpcodeTrg((char)48, (char)57);
        {char[] a = {48}; op[136] = getOpcodeTbs(a);}
        op[137] = getOpcodeTrg((char)49, (char)57);
        {int[] a = {139,140,141,142,143}; op[138] = getOpcodeAlt(a);}
        op[139] = getOpcodeTrg((char)33, (char)123);
        op[140] = getOpcodeTrg((char)125, (char)126);
        op[141] = getOpcodeRnm(34, 161); // UTF8-2
        op[142] = getOpcodeRnm(35, 164); // UTF8-3
        op[143] = getOpcodeRnm(36, 181); // UTF8-4
        {int[] a = {145,146,147,148,149,150,151,152,153}; op[144] = getOpcodeAlt(a);}
        op[145] = getOpcodeRnm(23, 130); // HTAB
        op[146] = getOpcodeRnm(24, 131); // CR
        op[147] = getOpcodeRnm(25, 132); // LF
        op[148] = getOpcodeTrg((char)32, (char)33);
        op[149] = getOpcodeTrg((char)35, (char)91);
        op[150] = getOpcodeTrg((char)93, (char)126);
        op[151] = getOpcodeRnm(34, 161); // UTF8-2
        op[152] = getOpcodeRnm(35, 164); // UTF8-3
        op[153] = getOpcodeRnm(36, 181); // UTF8-4
        {int[] a = {155,158}; op[154] = getOpcodeAlt(a);}
        {int[] a = {156,157}; op[155] = getOpcodeCat(a);}
        op[156] = getOpcodeRnm(27, 134); // BS
        op[157] = getOpcodeRnm(26, 133); // QM
        {int[] a = {159,160}; op[158] = getOpcodeCat(a);}
        op[159] = getOpcodeRnm(27, 134); // BS
        op[160] = getOpcodeRnm(27, 134); // BS
        {int[] a = {162,163}; op[161] = getOpcodeCat(a);}
        op[162] = getOpcodeTrg((char)194, (char)223);
        op[163] = getOpcodeRnm(37, 196); // UTF8-tail
        {int[] a = {165,169,173,177}; op[164] = getOpcodeAlt(a);}
        {int[] a = {166,167,168}; op[165] = getOpcodeCat(a);}
        {char[] a = {224}; op[166] = getOpcodeTbs(a);}
        op[167] = getOpcodeTrg((char)160, (char)191);
        op[168] = getOpcodeRnm(37, 196); // UTF8-tail
        {int[] a = {170,171}; op[169] = getOpcodeCat(a);}
        op[170] = getOpcodeTrg((char)225, (char)236);
        op[171] = getOpcodeRep((char)2, (char)2, 172);
        op[172] = getOpcodeRnm(37, 196); // UTF8-tail
        {int[] a = {174,175,176}; op[173] = getOpcodeCat(a);}
        {char[] a = {237}; op[174] = getOpcodeTbs(a);}
        op[175] = getOpcodeTrg((char)128, (char)159);
        op[176] = getOpcodeRnm(37, 196); // UTF8-tail
        {int[] a = {178,179}; op[177] = getOpcodeCat(a);}
        op[178] = getOpcodeTrg((char)238, (char)239);
        op[179] = getOpcodeRep((char)2, (char)2, 180);
        op[180] = getOpcodeRnm(37, 196); // UTF8-tail
        {int[] a = {182,187,191}; op[181] = getOpcodeAlt(a);}
        {int[] a = {183,184,185}; op[182] = getOpcodeCat(a);}
        {char[] a = {240}; op[183] = getOpcodeTbs(a);}
        op[184] = getOpcodeTrg((char)144, (char)191);
        op[185] = getOpcodeRep((char)2, (char)2, 186);
        op[186] = getOpcodeRnm(37, 196); // UTF8-tail
        {int[] a = {188,189}; op[187] = getOpcodeCat(a);}
        op[188] = getOpcodeTrg((char)241, (char)243);
        op[189] = getOpcodeRep((char)3, (char)3, 190);
        op[190] = getOpcodeRnm(37, 196); // UTF8-tail
        {int[] a = {192,193,194}; op[191] = getOpcodeCat(a);}
        {char[] a = {244}; op[192] = getOpcodeTbs(a);}
        op[193] = getOpcodeTrg((char)128, (char)143);
        op[194] = getOpcodeRep((char)2, (char)2, 195);
        op[195] = getOpcodeRnm(37, 196); // UTF8-tail
        op[196] = getOpcodeTrg((char)128, (char)191);
        return op;
    }

    public static void display(PrintStream out){
        out.println(";");
        out.println("; package.name.CompositionalGrammar");
        out.println(";");
        out.println("expression =  ws [definitionStatus ws] subExpression ws");
        out.println("subExpression = focusConcept [ws \":\" ws refinement]");
        out.println("definitionStatus = equivalentTo / subtypeOf");
        out.println("equivalentTo = \"===\"");
        out.println("subtypeOf = \"<<<\"");
        out.println("focusConcept = conceptReference *(ws \"+\" ws conceptReference) ");
        out.println("conceptReference = conceptId [ws \"|\" ws term ws \"|\"]");
        out.println("conceptId = sctId");
        out.println("term = nonwsNonPipe *( *SP nonwsNonPipe )");
        out.println("refinement =  (attributeSet / attributeGroup) *( ws [\",\" ws] attributeGroup )");
        out.println("attributeGroup = \"{\" ws attributeSet ws \"}\"");
        out.println("attributeSet = attribute *(ws \",\" ws attribute)");
        out.println("attribute = attributeName ws \"=\" ws attributeValue");
        out.println("attributeName = conceptReference");
        out.println("attributeValue =  expressionValue / QM stringValue QM / \"#\" numericValue ");
        out.println("expressionValue = conceptReference / \"(\" ws subExpression ws \")\"");
        out.println("stringValue = 1*(anyNonEscapedChar / escapedChar)");
        out.println("numericValue = decimalValue / integerValue");
        out.println("integerValue = ([\"-\"/\"+\"] digitNonZero *digit ) / zero ");
        out.println("decimalValue = integerValue  \".\" 1*digit");
        out.println("sctId = digitNonZero 5*17( digit )");
        out.println("ws = *( SP / HTAB / CR / LF ) ; optional white space");
        out.println("SP = %x20 ; space");
        out.println("HTAB = %x09 ; tab");
        out.println("CR = %x0D ; carriage return");
        out.println("LF = %x0A ; line feed");
        out.println("QM = %x22  ; quotation mark");
        out.println("BS = %x5C  ; back slash");
        out.println("digit = %x30-39");
        out.println("zero = %x30");
        out.println("digitNonZero = %x31-39 ");
        out.println("nonwsNonPipe = %x21-7B / %x7D-7E / UTF8-2 / UTF8-3 / UTF8-4");
        out.println("anyNonEscapedChar = HTAB / CR / LF / %x20-21 / %x23-5B / %x5D-7E / UTF8-2 / UTF8-3 / UTF8-4");
        out.println("escapedChar = BS QM /  BS BS");
        out.println("UTF8-2 = %xC2-DF UTF8-tail");
        out.println("UTF8-3 = %xE0 %xA0-BF UTF8-tail / %xE1-EC 2( UTF8-tail ) / %xED %x80-9F UTF8-tail / %xEE-EF 2( UTF8-tail )");
        out.println("UTF8-4 = %xF0 %x90-BF 2( UTF8-tail ) / %xF1-F3 3( UTF8-tail ) / %xF4 %x80-8F 2( UTF8-tail )");
        out.println("UTF8-tail = %x80-BF");
    }
}
