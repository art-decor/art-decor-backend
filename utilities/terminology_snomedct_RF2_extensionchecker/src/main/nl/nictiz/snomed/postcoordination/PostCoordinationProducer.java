package nl.nictiz.snomed.postcoordination;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.io.FileUtils;

import nl.nictiz.snomed.check.ExtensionChecker;
import nl.nictiz.snomed.check.ExtensionChecker.FileType;

/** Class that helps to formulate post-coordinations, to avoid
 * typo's.
 * 
 * @author Feikje Hielkema
 * @version 1.0 19-09-2014
 */
public class PostCoordinationProducer
{	
	public static String newLine = System.getProperty("line.separator");
	
	private Map<String,Boolean> concepts = new HashMap<String,Boolean>();
	private Map<String,String> fsns = new HashMap<String,String>();
	private Map<String,String> attributes = new HashMap<String,String>();
	
	public PostCoordinationProducer() throws IOException
	{
		ResourceBundle attributes = ResourceBundle.getBundle("attributes", Locale.ROOT);
		Enumeration<String> keys = attributes.getKeys();
		while (keys.hasMoreElements())
		{
			String key = keys.nextElement();
			this.attributes.put(key, attributes.getString(key));
		}
		
		File coreConceptFile = ExtensionChecker.getFile(FileType.Concept, ExtensionChecker.coreSnapshotDir, ExtensionChecker.coreDate);
		List<String> lines = FileUtils.readLines(coreConceptFile, "UTF-8");
		for (int i = 1; i < lines.size(); i++)
		{
			String[] line = lines.get(i).split("\t");
			concepts.put(line[0], line[2].equals("1"));
		}
		
		File extensionConceptFile = ExtensionChecker.getFile(FileType.Concept, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		lines = FileUtils.readLines(extensionConceptFile, "UTF-8");
		for (int i = 1; i < lines.size(); i++)
		{
			String[] line = lines.get(i).split("\t");
			concepts.put(line[0], line[2].equals("1"));
		}
		
		File coreDescriptionFile = ExtensionChecker.getFile(FileType.Description, ExtensionChecker.coreSnapshotDir, ExtensionChecker.coreDate);
		lines = FileUtils.readLines(coreDescriptionFile, "UTF-8");
		for (int i = 1; i < lines.size(); i++)
		{
			String[] line = lines.get(i).split("\t");
			if (line[2].equals("1") && line[6].equals("900000000000003001"))
				fsns.put(line[4], line[7]);
		}
		
		File extensionDescriptionFile = ExtensionChecker.getFile(FileType.Description, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		lines = FileUtils.readLines(extensionDescriptionFile, "UTF-8");
		for (int i = 1; i < lines.size(); i++)
		{
			String[] line = lines.get(i).split("\t");
			if (line[2].equals("1") && line[6].equals("900000000000003001"))
				fsns.put(line[4], line[7]);
		}
	}
	
	private boolean isActiveConcept(String conceptId)
	{
		if (!concepts.containsKey(conceptId))
		{
			System.out.println(conceptId + " does not exist");
			return false;
		}
		
		if (!concepts.get(conceptId))
		{
			System.out.println(conceptId + " is inactive");
			return false;
		}
		
		return true;
	}
	
	private String getAttribute(String property) throws PostCoordinationException
	{
		if (property.equals("q"))
			return null;
		else if (this.attributes.containsKey(property))
			return this.attributes.get(property);
		else
			throw new PostCoordinationException("Unknown attribute shorthand " + property);
	}
	
	private class PostCoordinationException extends Exception
	{
		private static final long serialVersionUID = 1L;

		public PostCoordinationException(String warning)
		{
			super(warning);
		}
	}
	
	private String appendLabel(String id)
	{
		return id + " |" + fsns.get(id) + "|";
	}
	
	private String[] buildExpression(BufferedReader reader) throws IOException
	{
		System.out.println("Which concept do you wish to refine?");
		String parentId = reader.readLine();
		if (parentId.equals("q"))
			return null;
		else if (!isActiveConcept(parentId))
		{
			System.out.println(parentId + " is not an active concept. Please try again.");
			return new String[0];
		}
		
		StringBuffer sbNoLabel = new StringBuffer(parentId + ":");
		StringBuffer sbWithLabel = new StringBuffer(appendLabel(parentId) + ":\n");
		
		while (true)
		{
			try
			{
				System.out.println("Please enter the next attribute, or 'q' to quit adding attributes.");			
				String attr = getAttribute(reader.readLine());
				if (attr == null)
					break;

				System.out.println("Please enter the target concept, or 'c' to specify a concept that should be refined.");
				String targetId = reader.readLine();
				
				if ("c".equals(targetId))
				{
					String[] subClause = buildExpression(reader);
					if (subClause == null)
						return null;
					
					System.out.println("You have created the subclause " + subClause[1] + ".\nWe will now proceed with the main clause.");
					sbNoLabel.append(attr + "=(" + subClause[0] + "),");
					sbWithLabel.append("\t" + appendLabel(attr) + " = (" + subClause[1] + "),\n");
				}
				else if (!isActiveConcept(targetId))
					System.out.println(targetId + " is not an active concept; this property cannot be added.");
				else
				{
					sbNoLabel.append(attr + "=" + targetId + ",");
					sbWithLabel.append("\t" + appendLabel(attr) + " = " + appendLabel(targetId) + ",\n");
				}
			}
			catch (PostCoordinationException e)
			{
				System.out.println(e.getMessage());				
			}
		}
		
		String[] result = {sbNoLabel.deleteCharAt(sbNoLabel.length() - 1).toString(), sbWithLabel.deleteCharAt(sbWithLabel.length() - 2).toString()};
		return result;
	}
	
	private static void writeToFile(String[] coordination, File out, BufferedReader reader) throws IOException
	{
		while (true)
		{
			try
			{
				FileWriter writer = new FileWriter(out);
				writer.write(coordination[1] + newLine + newLine);
				writer.write(coordination[0]);
				writer.close();
				break;
			}
			catch (IOException e)
			{
				System.out.println("I could not write to postcoordination.txt. Please close any editors that have opened this file, then press 'enter'.");
				reader.readLine();
			}
		}
	}
	
	public static void main(String[] arg)
	{
		try
		{
			System.out.println("Initialising...");
			PostCoordinationProducer pcp = new PostCoordinationProducer();
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			File out = new File("postcoordination.txt");
						
			while (true)
			{
				String[] coordination = pcp.buildExpression(reader);
				if (coordination == null)
					break;
				else if (coordination.length == 0)
					continue;
				
				System.out.println();
				System.out.println(coordination[1]);
				System.out.println(coordination[0]);
				System.out.println();
				writeToFile(coordination, out, reader);
			}
			reader.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}	
}
