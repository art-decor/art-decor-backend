package nl.nictiz.snomed.postcoordination;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import nl.nictiz.snomed.check.Description;
import nl.nictiz.snomed.check.ExtensionChecker;
import nl.nictiz.snomed.check.Description.Type;
import nl.nictiz.snomed.check.ExtensionChecker.FileType;
import nl.nictiz.snomed.postcoordination.ParseData.NodeType;
import nl.nictiz.snomed.util.HashMapWithList;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import apg.Ast;
import apg.Ast.AstCallback;

public class AstTranslator extends AstCallback
{
	private static Logger log = Logger.getLogger(AstTranslator.class);
	public static enum TranslatorType {Concept, Description, Attribute, AttributeName, AttributeValue, FocusConcept};
	
	private static Map<String,Boolean> conceptMap;
	private static HashMapWithList<String,Description> descriptionMap;
	
	private TranslatorType type;
	
	static
	{
		try
		{	//TODO: create maps for each hierarchy in domain or range
			conceptMap = ExtensionChecker.initConcepts(
					ExtensionChecker.getFile(FileType.Concept, ExtensionChecker.coreSnapshotDir, ExtensionChecker.coreDate), 
					ExtensionChecker.getFile(FileType.Concept, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate));

			descriptionMap = ExtensionChecker.getDescriptionsByConceptID(
					ExtensionChecker.getFile(FileType.Description, ExtensionChecker.coreSnapshotDir, ExtensionChecker.coreDate),
					ExtensionChecker.getFile(FileType.Description, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate));
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}
	
	public AstTranslator(Ast ast, TranslatorType type, ParseData data) throws ParserConfigurationException 
	{
		super(ast);
		this.type = type;
		this.callbackData.myData = data;
		
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		data.doc = dBuilder.newDocument();
		data.currentNode = data.doc.createElement("Tree");
		data.doc.appendChild(data.currentNode);
	}

	@Override
	public boolean preBranch(int offset, int length)
	{
		if ((type == TranslatorType.Concept) || (type == TranslatorType.Description))
			return super.preBranch(offset, length);
		
		ParseData data = getData();
		
		Element el;
		switch (type)
		{
			case FocusConcept: el = data.doc.createElement("FocusConcept"); data.currentNode.appendChild(el); data.currentNode = el; break;
			case Attribute: el = data.doc.createElement("Attribute"); data.currentNode.appendChild(el); data.currentNode = el; break;
		}
		return super.preBranch(offset, length);
	}
	
	@Override
	public void postBranch(int offset, int length)
	{
		String sub = new String(Arrays.copyOfRange(callbackData.inputString, offset, offset + length));
		
//		switch (type)
//		{
//			case Concept: getData().sctid = sub; break;
//			case Description: getData().term = sub; break;
//			case FocusConcept: 
//			case AttributeName:
//			case AttributeValue: currentNode = currentNode.getParentNode(); break;
//		}
		
		switch (type)
		{
			case Concept: handleConcept(sub); break;
			case Description: handleDescription(sub); break;
			case AttributeName: handleAttributeName(); break;	//TODO check that this sctid refers to an attribute and that the focus concept is in its domain
			case AttributeValue: handleAttributeValue(); break; 	//TODO check that this concept is in range of the attribute
			case FocusConcept: handleFocusConcept(); break;
		}
	}
		
	private void handleFocusConcept()
	{
		ParseData data = getData();
		if (data.type == NodeType.Other)
		{
			data.source = data.sctid;
			log.info("Focus concept = " + getData().sctid);
		}
		else if (data.type == NodeType.Attribute)
		{
			data.target = data.sctid;
		}
		
		data.sctid = null;
		data.type = null;
	}
	
	private void handleAttributeName()
	{	//set the id of the attribute in the XML-element
		ParseData data = getData();
//		data.type = NodeType.Attribute;
//		log.info("Checking attribute " + data.sctid + " with source " + data.source);
		data.currentNode.setAttribute("sctid", data.sctid);
		data.currentNode.setAttribute("name", data.term);
	}
	
	private void handleAttributeValue()
	{
		ParseData data = getData();
		if (data.sctid != null)
		{
			log.info("Checking attribute value " + getData().sctid);
			data.sctid = null;
		}
		else
		{
			log.info("Checking attribute value " + getData().target);
			data.target = null;
		}
	}
		
	private void handleConcept(String sctid)
	{
		ParseData data = getData();
		data.sctid = sctid;		//save sctid
			
		if (!conceptMap.containsKey(sctid))	
		{	//Check if sctid is valid and active
			log.error("Invalid sctid: " + sctid);
			getData().success = false;
			return;		//never mind further checks
		}
		else if (!conceptMap.get(sctid))
		{
			log.error("Inactive concept: " + sctid);
			getData().success = false;		//continue to check for attribute domain/range constraints
		}
	}
	
	private void handleDescription(String description)
	{
		boolean found = false;
		ParseData data = getData();
		String sctid = data.sctid;
		data.term = description;
		
		if (!this.descriptionMap.containsKey(sctid))
			return;	//id is invalid so term won't match and success is already false
		
		for (Description d : this.descriptionMap.get(getData().sctid))
		{
			if (d.active && d.description.equalsIgnoreCase(description))
			{
				found = true;
				break;
			}
		}
		
		if (!found)
		{
			log.error("Description '" + description + "' is not associated with sctid " + getData().sctid);
			data.success = false;
		}
	}
	
	private ParseData getData()
	{
		return (ParseData) this.callbackData.myData;
	}
}
