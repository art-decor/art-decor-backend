package nl.nictiz.snomed.postcoordination;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import nl.nictiz.snomed.postcoordination.AstTranslator.TranslatorType;

import org.apache.log4j.Logger;

import apg.Ast;
import apg.Grammar;
import apg.Parser;
import apg.Parser.Result;
import apg.Trace;
import apg.UdtLib;

/**	Validates whether a given expression conforms to: <ul>
 * <li>the SNOMED CT Compositional Grammar, using an APG-Grammar 
 * based on the ABNF definition supplied by the IHTSDO.</li>
 * <li>the Content Model, using the OWL content model??</li>
 * </ul>
 * 
 * TODO: 
 * - Build a parse tree from the components.
 * - Build OWL Content Model using Protege? Then use it to validate expressions against content model.
 * 
 * @author Feikje Hielkema-Raadveld
 * @version 1.0 12-08-2015
 */
public class PostCoordinationValidator
{
	private static Logger log = Logger.getLogger(PostCoordinationValidator.class);
	
	private Grammar grammar;
	private Parser parser;
	private Ast ast;
	private Trace trace;
	private ParseData data;
	
	public PostCoordinationValidator() throws Exception
	{
		grammar = CompositionalGrammar.getInstance();
		parser = new Parser(grammar);		
		int startRule = CompositionalGrammar.RuleNames.EXPRESSION.ruleID();
        parser.setStartRule(startRule);
        
//        trace = parser.enableTrace(true);
//        trace.setOut(System.out);
//        
//        for (int i = 21; i < 38; i++)
//        	trace.enableRule(false, i);
      
        ast = parser.enableAst(true);
        ast.enableRuleNode(CompositionalGrammar.RuleNames.EXPRESSION.ruleID(), true);
        ast.enableRuleNode(CompositionalGrammar.RuleNames.SUBEXPRESSION.ruleID(), true);
//        ast.enableRuleNode(CompositionalGrammar.RuleNames.DEFINITIONSTATUS.ruleID(), true);
        ast.enableRuleNode(CompositionalGrammar.RuleNames.FOCUSCONCEPT.ruleID(), true);
        ast.enableRuleNode(CompositionalGrammar.RuleNames.CONCEPTREFERENCE.ruleID(), true);
        ast.enableRuleNode(CompositionalGrammar.RuleNames.ATTRIBUTEGROUP.ruleID(), true);
        ast.enableRuleNode(CompositionalGrammar.RuleNames.ATTRIBUTESET.ruleID(), true);
        ast.enableRuleNode(CompositionalGrammar.RuleNames.ATTRIBUTE.ruleID(), true);
        ast.enableRuleNode(CompositionalGrammar.RuleNames.ATTRIBUTENAME.ruleID(), true);
        ast.enableRuleNode(CompositionalGrammar.RuleNames.ATTRIBUTEVALUE.ruleID(), true);
        ast.enableRuleNode(CompositionalGrammar.RuleNames.EXPRESSIONVALUE.ruleID(), true);
        ast.enableRuleNode(CompositionalGrammar.RuleNames.SCTID.ruleID(), true);
        ast.enableRuleNode(CompositionalGrammar.RuleNames.TERM.ruleID(), true);
        ast.enableRuleNode(CompositionalGrammar.RuleNames.REFINEMENT.ruleID(), true);
        
        data = new ParseData();
        ast.setRuleCallback(CompositionalGrammar.RuleNames.SCTID.ruleID(), new AstTranslator(ast, TranslatorType.Concept, data));
        ast.setRuleCallback(CompositionalGrammar.RuleNames.TERM.ruleID(), new AstTranslator(ast, TranslatorType.Description, data));
        ast.setRuleCallback(CompositionalGrammar.RuleNames.FOCUSCONCEPT.ruleID(), new AstTranslator(ast, TranslatorType.FocusConcept, data));
        ast.setRuleCallback(CompositionalGrammar.RuleNames.ATTRIBUTE.ruleID(), new AstTranslator(ast, TranslatorType.Attribute, data));
        ast.setRuleCallback(CompositionalGrammar.RuleNames.ATTRIBUTENAME.ruleID(), new AstTranslator(ast, TranslatorType.AttributeName, data));
        ast.setRuleCallback(CompositionalGrammar.RuleNames.ATTRIBUTEVALUE.ruleID(), new AstTranslator(ast, TranslatorType.AttributeValue, data));
	}
	
	public boolean parse(String expression) throws Exception
	{
		parser.setInputString(expression);
		Result result = parser.parse();
		
		if (!result.success())
		{
			log.info("Could not match expression beyond index " + result.getMatchedPhraseLength());
			return false;
		}
		
		ast.display(System.out, true);
		ast.translateAst();
		boolean success = data.success;
		data.reset();
		return success;
	}
	
	public static void main(String[] args)
	{	/*	Examples: 
				129152004 |Procedure on back (procedure)|: 260686004 |Method| = 129284003 |Surgical action|
				417662000 |History of clinical finding in subject (situation)|: 246090004 |Associated finding| = 284465006 |Finding related to psychosocial functioning (finding)|
				363346000 |Malignant neoplastic disease (disorder)|: 246454002 |Occurrence| = 255399007 |Congenital|
	 			162572001 |Suspected malignancy (situation)|: 246090004 |Associated finding| = 128462008 |Secondary malignant neoplastic disease|
	 			385413003 |Tumor extension finding (finding)|: 363698007 |Finding site| = 41178004 |Structure of sphincter ani muscle (body structure)|
	 			41769001 |Disease suspected (situation)|: 246090004 |Associated finding| = 301232003 |Lesion of lung (finding)|
	 			
	 			397956004 |prosthetic arthroplasty of the hip|: 363704007 |procedure site| = (24136001 |hip joint structure|: 272741003 |laterality| = 7771000 |left|), { 363699004 |direct device| = 304120007 |total hip replacement prosthesis|, 260686004 |method| = 257867005 |insertion - action|}
	 	*/
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Hello! Why don't you go and get some coffee while I load the data...");
			PostCoordinationValidator validator = new PostCoordinationValidator();
			System.out.println("Thanks for waiting!");
			
			while (true)
			{
				System.out.println("Please enter your postcoordination.");
				String expression = reader.readLine();
				if (expression.equalsIgnoreCase("q") || expression.equalsIgnoreCase("quit"))
					break;
				
				if (validator.parse(expression))
					System.out.println("Well done!");
				else
					System.out.println("I'm afraid you've got an error somewhere.");
			}
			
			System.out.println("Bye!");
			reader.close();
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}
}
