package nl.nictiz.snomed.util;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.nictiz.snomed.check.ExtensionChecker;
import nl.nictiz.snomed.check.ExtensionChecker.FileType;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVReader;

public class FsnFinder
{
	private static Logger log = Logger.getLogger(FsnFinder.class);
	
	private Map<String,Boolean> concepts = new HashMap<String,Boolean>();
	private Map<String,String> fsns = new HashMap<String,String>();
	
	public FsnFinder() throws IOException
	{
		initConcepts(ExtensionChecker.getFile(FileType.Concept, ExtensionChecker.coreSnapshotDir, ExtensionChecker.coreDate));
		initConcepts(ExtensionChecker.getFile(FileType.Concept, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate));
		initDescriptions(ExtensionChecker.getFile(FileType.Description, ExtensionChecker.coreSnapshotDir, ExtensionChecker.coreDate));
		initDescriptions(ExtensionChecker.getFile(FileType.Description, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate));
	}
	
	private void initConcepts(File conceptFile) throws IOException
	{
		List<String> lines2 = FileUtils.readLines(conceptFile, "UTF-8");
		for (int i = 1; i < lines2.size(); i++)
		{
			String[] line = lines2.get(i).split("\t");
			concepts.put(line[0], line[2].equals("1"));
		}
	}
	
	private void initDescriptions(File descriptionFile) throws IOException
	{
		List<String> lines = FileUtils.readLines(descriptionFile, "UTF-8");
		for (int i = 1; i < lines.size(); i++)
		{
			String[] line = lines.get(i).split("\t");
			if (line[2].equals("1") && line[6].equals("900000000000003001"))
				fsns.put(line[4], line[7]);
		}
	}
	
	public String getFSN(String sctid)
	{
		if (!concepts.containsKey(sctid))
			return "invalid sctid";
		else if (!concepts.get(sctid))
			return "inactive concept";
		else
			return fsns.get(sctid);
	}
	
	public static void main(String[] args)
	{
		try
		{
			FsnFinder fsnFinder = new FsnFinder();
			CSVReader reader = new CSVReader(new FileReader("file2.csv"), ';');
			
			for (String[] line : reader.readAll())
			{
				if (line[2].equals("Code"))
					continue;
				
				String sctid = line[4].trim();
				System.out.println(fsnFinder.getFSN(sctid));
			}
			
			reader.close();
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}
}
