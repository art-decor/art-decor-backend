package nl.nictiz.snomed.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HashMapWithList<K,V> extends HashMap<K,List<V>>
{
	private static final long serialVersionUID = -8998121137102702335L;

	public void add(K key, V value)
	{
		if (super.containsKey(key))
			super.get(key).add(value);
		else
		{
			List<V> list = new ArrayList<V>();
			list.add(value);
			super.put(key, list);
		}
	}
	
	public int keySize(K key)
	{
		if (super.containsKey(key))
			return super.get(key).size();
		return 0;
	}
	
	public List<V> getAllValues()
	{
		List<V> result = new ArrayList<V>();
		for (List<V> list : super.values())
			result.addAll(list);
		return result;
	} 
}
