package nl.nictiz.snomed.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.nictiz.snomed.check.Description;
import nl.nictiz.snomed.check.ExtensionChecker;
import nl.nictiz.snomed.check.Description.Type;
import nl.nictiz.snomed.check.ExtensionChecker.FileType;
import nl.nictiz.snomed.check.LanguageRefset;
import nl.nictiz.snomed.tree.Relation;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVWriter;

/** Compiles a list of concepts created in the Dutch Extension using
 * a particular (configurable) criterium. Original purpose: collecting
 * all micro-organism concepts created for Pieter-Jan Haas.
 * 
 * @author Feikje Hielkema
 *
 */
public class ConceptLister
{
	private static Logger log = Logger.getLogger(ConceptLister.class);
	
	private Map<String,Concept> concepts = new HashMap<String,Concept>();

	private class Concept
	{
		String sctid, fsn, prefTermEn, prefTermNL, parentId;
		List<String> synonyms = new ArrayList<String>();
		
		public Concept(String sctid)
		{
			this.sctid = sctid;
		}
				
		public String getFSN(boolean part)
		{
			if (!part)
				return fsn;
			
			return fsn.substring(0, fsn.lastIndexOf("(") - 1);
		}
	}
	
	public ConceptLister() throws IOException, ParseException
	{	//find all concepts with fsn ending in '(organism)'
		File conceptFile = ExtensionChecker.getFile(FileType.Concept, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		List<String> lines = FileUtils.readLines(conceptFile, "UTF-8");
		
		for (int i = 1; i < lines.size(); i++)
		{
			String line = lines.get(i);
			if (line.trim().isEmpty())
				continue;
			
			String[] tokens = line.split("\\t");
			if (tokens[2].equals("1"))
				concepts.put(tokens[0], new Concept(tokens[0]));
		}
		
		addDescriptions();
		addParents();
	}
	
	private void addDescriptions() throws IOException, ParseException
	{
		LanguageRefset languageRefset = new LanguageRefset(ExtensionChecker.getFile(FileType.LanguageRefset, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate));
				
		File descriptionFile = ExtensionChecker.getFile(FileType.Description, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		List<String> lines = FileUtils.readLines(descriptionFile, "UTF-8");
		
		for (int i = 1; i < lines.size(); i++)
		{
			String line = lines.get(i);
			if (line.trim().isEmpty())
				continue;
			
			Description desc = new Description(line);
			if (!(desc.active && concepts.containsKey(desc.conceptId)))
				continue;
			
			Concept c = concepts.get(desc.conceptId);
			if (desc.type == Type.FSN)
			{
				c.fsn = desc.description;
				if (!c.fsn.contains("("))
					log.info(c.fsn);
			}
			else
			{
				LanguageRefset.Type type = languageRefset.getType(desc.descriptionId, "31000146106");
				if (type == null)
				{
					type = languageRefset.getType(desc.descriptionId, "900000000000509007");
					if (type == LanguageRefset.Type.Preferred)
						c.prefTermEn = desc.description;
					else if (type == LanguageRefset.Type.Acceptable)
						c.synonyms.add(desc.description);
					else
						log.warn("Unknown description type " + desc.descriptionId);
				}
				else if (type == LanguageRefset.Type.Preferred)
					c.prefTermNL = desc.description;
				else if (type == LanguageRefset.Type.Acceptable)
					c.synonyms.add(desc.description);
				else
					log.warn("Unknown description type " + desc.descriptionId);
			}
		}
	}
	
	private void addParents() throws IOException
	{
		File relationFile = ExtensionChecker.getFile(FileType.Relation, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		List<String> lines = FileUtils.readLines(relationFile, "UTF-8");
		
		for (int i = 1; i < lines.size(); i++)
		{
			String line = lines.get(i);
			if (line.trim().isEmpty())
				continue;
			
			Relation relation = new Relation(line.split("\\t"));
			if (concepts.containsKey(relation.source))
				concepts.get(relation.source).parentId = relation.target;
		}
	}
	
	public void writeToBatchFile(File out) throws IOException
	{
		CSVWriter writer = new CSVWriter(new FileWriter(out), ';');
		int i = 1;
		
		List<String> keys = new ArrayList<String>(concepts.keySet());
		Collections.sort(keys);
		
		for (String key : keys)
		{
			Concept c = concepts.get(key);
			if (!c.fsn.endsWith("(organism)"))
				continue;
			
			List<String> line = new ArrayList<String>();
			line.add(Integer.toString(i));
			line.add("Organism");
			line.add(c.sctid);
			line.add((c.prefTermNL != null) ? c.prefTermNL : c.prefTermEn);
			line.add(c.getFSN(true));
			line.add("organism");
			line.add(c.prefTermEn);
			
			line.add(c.parentId.contains("1000146") ? "Current Batch Requests" : "SNOMED CT International");
			line.add(c.parentId);
			for (int j = 0; j < 7; j++)
				line.add("");

			if (!(c.fsn.startsWith("Genus") || c.fsn.startsWith("Family")))
				line.add("Organism detected in Dutch lab");
			
			line.add("");
			line.addAll(c.synonyms);
			writer.writeNext(line.toArray(new String[0]));
			i++;
		}
		writer.close();
	}
	
	public static void main(String[] args)
	{
		try
		{
			ConceptLister lister = new ConceptLister();
			lister.writeToBatchFile(new File("micro-organisms-SIRS-batch.csv"));
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}
}
