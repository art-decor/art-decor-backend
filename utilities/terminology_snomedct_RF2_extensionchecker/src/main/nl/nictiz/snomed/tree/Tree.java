package nl.nictiz.snomed.tree;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVWriter;

public class Tree
{
	private static Logger log = Logger.getLogger(Tree.class);
	
	private List<Node> roots = new ArrayList<Node>();
	private Map<String,String> fsnMap = new HashMap<String,String>();
	private SnomedRelationReader reader;
	private int nodes = 0;
	
	public Tree(SnomedRelationReader reader, Map<String,String> fsnMap)
	{
		this.reader = reader;
		this.fsnMap = fsnMap;
	}
	
	public void fillTopDown(String... rootIds) throws IOException
	{
		for (String rootId : rootIds)
		{
			Node root = new Node(rootId, fsnMap.get(rootId), null, true);
			roots.add(root);
			nodes++;
			addChildren(root);
		}
	}
	
	public void fillTopDownExclude(String rootId, String exclude) throws IOException
	{
		Node root = new Node(rootId, fsnMap.get(rootId), null, true);
		roots.add(root);
		nodes++;
		addChildrenExclude(root, exclude);
	}
	
	private void addChildren(Node parent) throws IOException
	{
		for (String child : reader.getChildren(parent.sctid))
		{
//			if (child.contains("1000146") || added.contains(child))
//				continue;
			
			Node childNode = new Node(child, fsnMap.get(child), null, true);
			nodes++;
			parent.addChild(childNode);
			addChildren(childNode);
		}
	}
	
	private void addChildrenExclude(Node parent, String exclude) throws IOException
	{
		for (String child : reader.getChildren(parent.sctid))
		{
			if (child.equals(exclude) || reader.isChild(child, exclude))
				continue;
			
			Node childNode = new Node(child, fsnMap.get(child), null, true);
			nodes++;
			parent.addChild(childNode);
			addChildrenExclude(childNode, exclude);
		}
	}
	
	public void addNode(Node node) throws IOException
	{
		if (node.sctid == null)
		{	//new concept, no id available yet
			if (node.sctids.isEmpty())
			{	//no known parents either, so just add it without id
				roots.add(node);
				nodes++;
			}
			else
			{	//postcoordination with parents: add parents to tree in case it has parents in common with other new nodes
				for (String id : node.sctids)
				{
					Node parent = new Node(id, fsnMap.get(id), null, false);
					addNode(parent);
					parent.addChild(node, reader);
				}
			}
			return;
		}
		
		boolean addAsRoot = true;
		for (Iterator<Node> it = roots.iterator(); it.hasNext(); )
		{
			Node root = it.next();
			if ((root.sctid != null) && node.sctid.equals(root.sctid))
			{	//node is already in tree as a root
				if (!node.inSet)
					return;				//better version already in tree, so stop right there
				else if (!root.inSet)	//better version (incl. refterm): remove root here, node will be added at end of method
					it.remove();		
				else 	//node already in tree (must occur twice in list?), so stop
				{
					log.info("Encountered existing root " + node.sctid);
					return;
				}
			}
			else
				nodes++;
			
			if (root.addDescendant(node, reader))		//node is added as a (grand)child of root
				addAsRoot = false;
			else if (node.addDescendant(root, reader))	//root is added as a child of node
				it.remove();							//so remove root from list of roots
		}
		
		if (addAsRoot)							
			roots.add(node);							//node has not been added as a descendant, so add it as a root

		addAllDescendantsInTree(node);
	}
	
	/*	Node has been added to tree, but tree may contain descendants now listed
	*	under other parents. Add those to node.children.
	*/
	private void addAllDescendantsInTree(Node node)
	{
		for (Node other : roots)
		{
			if (!node.sctid.equals(other.sctid))		//skip if same node
				other.addIfDescendant(node, reader);
		}
	}
	
	public void cleanDoubles()
	{
		for (Iterator<Node> it = roots.iterator(); it.hasNext(); )
		{	//for every root, check
			Node root = it.next();
			if (root.sctid == null)
				continue;
			
			for (Node other : roots)
			{
				if ((other.sctid == null) ||root.sctid.equals(other.sctid))
					continue;
				
				Node copy = other.findDescendant(root.sctid);
				if (copy != null)		//not found, so try another sibling
				{
					log.info("Removing " + root.sctid + " |" + root.prefTerm + "| from roots");
					it.remove();			//found, so remove this node from list of roots
					copy.copyChildren(root);
					break;					//Node has been removed, so stop checking its siblings; proceed to next root instead
				}
			}
		}
		
		for (Node root : roots)		//now do the same for all descendants of the roots
			root.cleanDoubles();
	}
	
	public void print(File out, boolean csv) throws IOException
	{
		log.info(roots.size() + " roots and " + nodes + " nodes");
		Collections.sort(roots);
		
		if (csv)
		{
			CSVWriter writer = new CSVWriter(new FileWriter(out), ';');
			for (Node root : roots)
				root.printToCsv(writer, 0);
			writer.close();
		}
		else
		{
			FileWriter writer = new FileWriter(out);
			for (Node root : roots)
			{
				root.print(writer, 0);
				writer.write("\n");
			}
			writer.close();
		}
	}
	
	public void printToHtml(File out) throws IOException
	{
		FileWriter writer = new FileWriter(out);
		for (Node root : roots)
			writer.write(root.printToHtml());
		writer.close();
	}
	
	public void printAsList(File out, boolean addParents) throws IOException
	{
		Set<String> set = new HashSet<String>();
		CSVWriter writer = new CSVWriter(new FileWriter(out), ';');
		Collections.sort(roots);
		
		for (Node root : roots)
		{
			if (set.contains(root.sctid))
				continue;
			
			set.add(root.sctid);
			root.printAsList(writer, set, addParents, this.reader, this.fsnMap);
		}
		
		writer.close();
	}
}
