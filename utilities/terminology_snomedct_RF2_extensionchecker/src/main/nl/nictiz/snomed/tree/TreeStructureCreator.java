package nl.nictiz.snomed.tree;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.nictiz.snomed.check.Description;
import nl.nictiz.snomed.check.Description.Type;
import nl.nictiz.snomed.check.ExtensionChecker;
import nl.nictiz.snomed.check.ExtensionChecker.FileType;
import nl.nictiz.snomed.check.LanguageRefset;
import nl.nictiz.snomed.util.HashMapWithList;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

/** Creates tree structure from a reference set.
 * 
 * @author hielkema
 *
 */
public class TreeStructureCreator
{
	private static Logger log = Logger.getLogger(TreeStructureCreator.class);
	
	private static final String usLanguageRefsetId = "900000000000509007";
	private SnomedRelationReader relations;
	private Map<String,Boolean> concepts;
	private Map<String,String> fsns;
	private HashMapWithList<String,Description> descriptions;
	
	public TreeStructureCreator() throws IOException, ParseException
	{
		this.concepts = ExtensionChecker.initConcepts(
				ExtensionChecker.getFile(FileType.Concept, ExtensionChecker.coreSnapshotDir, ExtensionChecker.coreDate));
//				ExtensionChecker.getFile(FileType.Concept, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate));
		log.info("Loaded concepts");
		
//		File nlDesc = ExtensionChecker.getFile(FileType.Description, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		File coreDesc = ExtensionChecker.getFile(FileType.Description, ExtensionChecker.coreSnapshotDir, ExtensionChecker.coreDate);
		this.fsns = ExtensionChecker.getFSNs(concepts, coreDesc);
//		this.fsns = ExtensionChecker.getPreferredTerms(concepts, coreDesc, 
//				ExtensionChecker.getFile(FileType.LanguageRefset, ExtensionChecker.coreSnapshotDir, ExtensionChecker.coreDate),
//				usLanguageRefsetId);
		log.info("Loaded descriptions");
		
		initRelations(coreDesc);//, nlDesc);
		log.info("Loaded relations");
	}
	
	private void initRelations(File... descriptionFiles) throws IOException
	{
		relations = new SnomedRelationReader(
				ExtensionChecker.getFile(FileType.Relation, ExtensionChecker.coreSnapshotDir, ExtensionChecker.coreDate),
				ExtensionChecker.getFile(FileType.Relation, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate));
	}
	
	public void createFromTxt(File file, File listOut, File treeOut) throws IOException
	{
		Tree tree = new Tree(relations, fsns);
		CSVWriter writer = new CSVWriter(new FileWriter(listOut), ';');
		String[] header = {"Sctid", "Preferred term (en-US)"};
		writer.writeNext(header);
		
		for (String sctid : FileUtils.readLines(file))
		{
			if (!concepts.containsKey(sctid))
				log.warn("Cannot find concept " + sctid);
			else if (!concepts.get(sctid))
				log.warn("Skipping inactive concept " + sctid);
			else
			{
				String fsn = fsns.get(sctid);
//				int idx = fsn.lastIndexOf(" (");
//				String prefTerm = fsn.substring(0, idx);
//				String tag = fsn.substring(idx + 2);
				String[] line = {sctid, fsn};
				writer.writeNext(line);
				Node node = new Node(sctid, fsn, fsn, true);
				tree.addNode(node);
			}
		}
		
		writer.close();
		tree.cleanDoubles();  	//purge the nodes that occur more often (at higher levels) than necessary
		tree.print(treeOut, true);
	}
	
	public Tree createFromCSV(File file) throws IOException
	{
		Tree tree = new Tree(relations, fsns);
		CSVReader reader = new CSVReader(new FileReader(file), ';');
		List<String[]> lines = reader.readAll();
		
		for (int i = 1; i < lines.size(); i++)
		{
			String[] line = lines.get(i);
			boolean inSet = !(line[0].isEmpty());
			Node node = new Node(line[3], fsns.get(line[3]), line[1], inSet);
			
//			if ((node.sctid == null) && node.sctids.isEmpty())
//				log.warn("Not adding node " + node.fsn + " (" + node.prefTerm + ")");
//			else if ((node.sctid != null) && concepts.containsKey(node.sctid) && (!concepts.get(node.sctid)))
//				log.warn("INACTIVE: " + node.sctid + " |" + node.fsn + "| (" + node.prefTerm + ")");
//			else
				tree.addNode(node);
//			log.info("Added " + ((node.sctid != null) ? node.sctid : node.sctids.toString()));
		}
		
		reader.close();
		
		tree.cleanDoubles();  	//purge the nodes that occur more often (at higher levels) than necessary
		return tree;
	}
	
	public void createTopDown(String name, String... topIds) throws IOException
	{
		Tree tree = new Tree(relations, fsns);
		tree.fillTopDown(topIds);
		tree.print(new File(name + "-tree.csv"), true);
//		tree.print(new File("bomen", name + "-boom.txt"), false);
//		tree.printAsList(new File("lijsten", name + "-lijst.csv"), true);
	}
	
	public void createTopDownExclude(String name, String ancestor, String exclude) throws IOException
	{
		Tree tree = new Tree(relations, fsns);
		tree.fillTopDownExclude(ancestor, exclude);
		tree.print(new File("bomen", name + "-boom.txt"), false);
		tree.printAsList(new File("lijsten", name + "-lijst.csv"), true);
	}
	
	public Tree getTree(String... topIds) throws IOException
	{
		Tree tree = new Tree(relations, fsns);
		tree.fillTopDown(topIds);
		return tree;
	}
	
	public void process(String name, String out, int output) throws IOException
	{
//		Tree tree = create(new File("testResources/tree-input/", name + ".csv"), false);
		Tree tree = createFromCSV(new File(name));
		log.info("Created " + out);
		
		switch (output)
		{
			case 0: tree.print(new File(out), false); break;
			case 1: tree.print(new File(out), true); break;
			case 2: tree.printToHtml(new File(out)); break;
		}
	}
		
	public static void main(String[] arg)
	{
		try
		{
			String dir = ExtensionChecker.coreSnapshotDir, date = ExtensionChecker.coreDate;
			TreeStructureCreator treeCreator = new TreeStructureCreator();
			log.info("Loaded SNOMED CT");
			
			treeCreator.createTopDown("Procedure", "71388002");
			File file = new File("C:\\Workspace/Miscellaneous/gmdn-refset.txt");
			treeCreator.createFromTxt(file, new File("gmdn-list.csv"), new File("gmdn-tree.csv"));
			
//			treeCreator.createTopDown("Oogheelkunde", "371548008", "36228007", "276342005", "410487001");
//			treeCreator.createTopDown("KNO", "118891001", "118781003", "118693000", "118670006", "359655004");
//			treeCreator.createTopDown("Heelkunde", "387713003");
//			treeCreator.createTopDown("PlastischeChirurgie", "3911000", "373351007", "304040003", "112746006", "392090004", "122465003", "24878005");
//			treeCreator.createTopDown("Orthopedie", "118666003", "118743008");
//			treeCreator.createTopDownExclude("Urologie", "118674002", "118675001");
//			treeCreator.createTopDown("Gynecologie", "118675001", "386637004", "4589007");
//			treeCreator.createTopDown("Neurochirurgie", "372246008", "392236004", "118879005");
//			treeCreator.createTopDown("Dermatologie", "447086009", "118858005", "426633009");
//			treeCreator.createTopDown("Allergologie", "395142003", "252512005", "15220000", "82078001");
//			treeCreator.createTopDown("Revalidatie", "52052004");	//exclusies leveren slechts 3 ge�xcludeerde concepten op, dus laat maar
//			treeCreator.createTopDown("Thoraxchirurgie", "22123002", "265045007", "112802009", "31168004");
//			treeCreator.createTopDown("ConsultatievePsychiatrie", "108311000", "31205005");
//			treeCreator.createTopDown("Radiotherapie", "108290001");
//			treeCreator.createTopDown("Radiologie", "363679005", "240917005");
//			treeCreator.createTopDown("NucleaireGeneeskunde", "371572003");
//			treeCreator.createTopDown("KlinischeChemie", "15220000");
//			treeCreator.createTopDown("MedischeMicrobiologie", "19851009");
//			treeCreator.createTopDown("PathologischeAnatomie", "108257001");
//			treeCreator.createTopDown("Anaesthesiologie", "399248000", "278414003");
//			treeCreator.createTopDown("KlinischeGenetica", "405824009", "79841006", "82078001");
//			treeCreator.createTopDown("Audiologie", "108249004");
//			treeCreator.createTopDown("MondKaakChirurgie", "239364005", "231686003", "118817003", "172880001");
//			treeCreator.createTopDown("Kindergeneeskunde", "265764009", "15220000", "82078001", "225105004", "57617002", "447241003", "84728005", "243788004", "443466001");	//lijst van ouders nogal onwaarschijnlijk...
			
////			treeCreator.createTopDown("InwendigeGeneeskunde", "384719006", "", "", "", "", "", "", "", "", "", "", "");	//is ingewikkeld, doen we als laatste
//			Tree t1 = treeCreator.getTree("265764009", "15220000", "82078001", "225105004", "57617002", "447241003", "119290003", "373205008");
//			t1.fillTopDownExclude("384719006", "386621005");
//			t1.print(new File("bomen", "InwendigeGeneeskunde-boom.txt"), false);
//			t1.printAsList(new File("lijsten", "InwendigeGeneeskunde-lijst.csv"), true);
//		
////			treeCreator.createTopDown("MaagDarmLeverZiekten", "", "", "", "", "", "", "", "", "", "", "", "");
//			Tree t3 = treeCreator.getTree("15220000", "82078001", "225105004", "57617002");
//			t3.fillTopDownExclude("384719006", "386621005");
//			t3.print(new File("bomen", "MaagDarmLeverZiekten-boom.txt"), false);
//			t3.printAsList(new File("lijsten", "MaagDarmLeverZiekten-lijst.csv"), true);
//			
////			treeCreator.createTopDown("Cardiologie", "", "", "", "", "", "", "", "", "", "", "", "");
//			Tree t4 = treeCreator.getTree("276341003");
//			t4.fillTopDownExclude("118797008", "64915003");
//			t4.print(new File("bomen", "Cardiologie-boom.txt"), false);
//			t4.printAsList(new File("lijsten", "Cardiologie-lijst.csv"), true);
//			
////			treeCreator.createTopDown("Longziekten", "", "", "", "", "", "", "", "", "", "", "", "");
//			Tree t5 = treeCreator.getTree("261479009", "178297005", "173206006", "50686003", "440145003", "119284002", "423692006", "15220000", "82078001");
//			t5.fillTopDownExclude("118794001", "387714009");
//			t5.fillTopDownExclude("118792002", "86800006");
//			t5.print(new File("bomen", "Longziekten-boom.txt"), false);
//			t5.printAsList(new File("lijsten", "Longziekten-lijst.csv"), true);
//			
////			treeCreator.createTopDown("Reumatologie", "", "", "", "", "", "", "", "", "", "", "", "");
//			Tree t6 = treeCreator.getTree("15220000", "82078001", "252464001");
//			t6.fillTopDownExclude("118745001", "112698002");
//			t6.print(new File("bomen", "Reumatologie-boom.txt"), false);
//			t6.printAsList(new File("lijsten", "Reumatologie-lijst.csv"), true);
//			
////			treeCreator.createTopDown("Neurologie", "118678004", "", "", "", "", "", "", "", "", "", "", "");
//			Tree t7 = treeCreator.getTree("15220000", "82078001");	//, "277762005"); //die laatste moet er al in zitten, controleer dat
//			t7.fillTopDownExclude("118678004", "16545005");
//			t7.print(new File("bomen", "Neurologie-boom.txt"), false);
//			t7.printAsList(new File("lijsten", "Neurologie-lijst.csv"), true);
//			
////			treeCreator.createTopDown("KlinischeGeriatrie", "32908001", "15220000", "82078001", "57617002", "225105004", "384719006"); //laatste heeft exclusie
//			Tree t8 = treeCreator.getTree("15220000", "82078001", "225105004", "57617002", "32908001");
//			t8.fillTopDownExclude("384719006", "386621005");
//			t8.print(new File("bomen", "KlinischeGeriatrie-boom.txt"), false);
//			t8.printAsList(new File("lijsten", "KlinischeGeriatrie-lijst.csv"), true);
			
//			treeCreator.process("dhd-oogheelkunde.csv", "oogheelkunde-boom.html", 2);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}
}
