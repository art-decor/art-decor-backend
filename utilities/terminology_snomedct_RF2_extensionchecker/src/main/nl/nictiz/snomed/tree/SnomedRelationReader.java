package nl.nictiz.snomed.tree;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nl.nictiz.snomed.check.ExtensionChecker;
import nl.nictiz.snomed.check.ExtensionChecker.FileType;
import nl.nictiz.snomed.util.HashMapWithList;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

public class SnomedRelationReader
{
	public static final String findingSite = "363698007", 
								procedureSite = "363704007", directProcedureSite = "405813007", indirectProcedureSite = "405814001",
							   associatedMorphology = "116676008", 
							   laterality = "272741003", 
							   side = "182353008",
							   left = "7771000",
							   right = "24028007",
							   directDevice = "363699004",
							   isa = "116680003",
							   implant = "40388003",
							   clinicalFinding = "404684003",
							   procedure = "71388002",
							   bodyStructure = "123037004",
							   organism = "410607006",
							   partOf = "123005000",
							   method = "260686004",
							   procedureWithContext = "129125009",
							   situationWithContext = "243796009",
							   pelvis = "281252001",
							   organCavity = "91812007",
							   rootConcept = "138875005",
							   substance = "105590001",
							   specimen = "123038009";
	
	private static Logger log = Logger.getLogger(SnomedRelationReader.class);
	
	private HashMapWithList<String,Relation> sourceIndex = new HashMapWithList<String,Relation>();	//index on the source objects
	private HashMapWithList<String,Relation> targetIndex = new HashMapWithList<String,Relation>();	//index on the target objects
	
	public SnomedRelationReader(File... files) throws IOException
	{
		for (File file : files)
		{
			BufferedReader reader = new BufferedReader(new FileReader(file));
			reader.readLine();
			int i = 1;
			
			while (true)
			{
				String line = reader.readLine();
				i++;
				if (line == null)
					break;
				if (line.trim().isEmpty())
					continue;
				
				String[] tokens = line.split("\t");
				if (tokens[2].equals("0"))
					continue;		//inactive relation
				
				Relation rel = new Relation(tokens);
				sourceIndex.add(rel.source, rel);
				targetIndex.add(rel.target, rel);
			}
			
			reader.close();
		}
	}
	
	public boolean isChild(String conceptId, String parentId)
	{
		if (!sourceIndex.containsKey(conceptId))
			return false;	//topmost node has no properties
		
		for (Relation relation : sourceIndex.get(conceptId))
		{
			if (relation.property.equals(isa) && relation.target.equals(parentId))
				return true;
		}
		return false;
	}
	
	public boolean isDescendant(String conceptId, String ancestorId)
	{
		if (conceptId.equals(ancestorId))
			return true;
		
		if (!sourceIndex.containsKey(conceptId))
			return false;	//topmost node has no properties
		
		for (Relation relation : sourceIndex.get(conceptId))
		{
			if (relation.property.equals(isa) && isDescendant(relation.target, ancestorId))
				return true;
		}
		return false;
	}
	
	public boolean isDescendant(String conceptId, String... ancestorIds)
	{
		for (String id : ancestorIds)
			if (id.equals(conceptId))
				return true;
		
		if (!sourceIndex.containsKey(conceptId))
			return false;	//topmost node has no properties
		
		for (Relation relation : sourceIndex.get(conceptId))
		{
			if (relation.property.equals(isa) && isDescendant(relation.target, ancestorIds))
				return true;
		}
		return false;
	}
	
	public List<Relation> getRelations(String conceptId, boolean defining)
	{
		List<Relation> result = new ArrayList<Relation>();
		if (!sourceIndex.containsKey(conceptId))
			return result;
		
		for (Relation relation : sourceIndex.get(conceptId))
		{
			if (defining ^ relation.property.equals(isa))
				result.add(relation);
		}
		return result;
	}
	
	public Set<String> getDescendants(String conceptId)
	{
		Set<String> result = new HashSet<String>();
		if (!targetIndex.containsKey(conceptId))
			return result;
		
		for (Relation relation : targetIndex.get(conceptId))
		{
			if (relation.property.equals(isa))
			{
				result.add(relation.source);
				result.addAll(getDescendants(relation.source));
			}
		}
		
		return result;
	}
	
	public Set<String> getDescendantsExcluding(String ancestorId, String... exclude)
	{
		Set<String> result = new HashSet<String>();
		if (!targetIndex.containsKey(ancestorId))
			return result;
		
		for (Relation relation : targetIndex.get(ancestorId))
		{
			if (!relation.property.equals(isa))
				continue;
			if (isDescendant(relation.source, exclude))
				continue;
			
			result.add(relation.source);
			result.addAll(getDescendantsExcluding(relation.source, exclude));
		}
		
		return result;
	}
	
	public Set<String> getChildren(String conceptId)
	{
		Set<String> result = new HashSet<String>();
		if (!targetIndex.containsKey(conceptId))
			return result;
		
		for (Relation relation : targetIndex.get(conceptId))
		{
			if (relation.property.equals(isa))
				result.add(relation.source);
		}
		
		return result;
	}
	
	public Set<String> getParents(String conceptId)
	{
		Set<String> result = new HashSet<String>();
		if (!sourceIndex.containsKey(conceptId))
			return result;

		for (Relation relation : sourceIndex.get(conceptId))
		{
			if (relation.property.equals(isa))
				result.add(relation.target);
		}
		return result;
	}
	
	public Set<String> getAncestors(String conceptId, int generationNr)
	{
		Set<String> result = new HashSet<String>();
		if (!sourceIndex.containsKey(conceptId))
			return result;

		for (Relation relation : sourceIndex.get(conceptId))
		{
			if (!relation.property.equals(isa))
				continue;
			if (generationNr > 1)
				result.addAll(getAncestors(relation.target, generationNr - 1));
			else
				result.add(relation.target);
		}
		
		return result;
	}
	
	public Set<String> getConceptsWithProperty(String property, String targetId)
	{
		Set<String> properties = getDescendants(property);
		properties.add(property);		
		Set<String> targets = getDescendants(targetId);
		targets.add(targetId);
		Set<String> result = new HashSet<String>();
		
		for (String conceptId : targets)
		{
			if (!targetIndex.containsKey(conceptId))
				continue;

			for (Relation relation : targetIndex.get(conceptId))
			{
				if (properties.contains(relation.property))
					result.add(relation.source);
			}
		}
		
		return result;
	}
	
	public Set<String> getConceptsWithInverseProperty(String property, String sourceId)
	{
		Set<String> properties = getDescendants(property);
		properties.add(property);		
		Set<String> sources = getDescendants(sourceId);
		sources.add(sourceId);
		Set<String> result = new HashSet<String>();
		
		for (String conceptId : sources)
		{
			if (!sourceIndex.containsKey(conceptId))
				continue;

			for (Relation relation : sourceIndex.get(conceptId))
			{
				if (properties.contains(relation.property))
					result.add(relation.target);
			}
		}
		
		return result;
	}
	
	/** Returns the conceptId of the subhierarchy that this concept belongs to;
	 * that is, the concept just below the SNOMED root concept.
	 * @param conceptId
	 * @return conceptId of subhierarchy
	 */
	public String getSubHierarchy(String conceptId, boolean distinguishMorphology)
	{	//Just go up until you find an is-a relation to the root concept; then return its source
		if (!sourceIndex.containsKey(conceptId))
		{
			log.warn(conceptId + " has no source relations");
			return null;
		}
		
		for (Relation rel : sourceIndex.get(conceptId))
		{
			if (rel.property.equals(isa))
			{
				if (distinguishMorphology && rel.target.equals("49755003"))		//morphologic abnormality is not a subhierarchy itself but we do wish to distinguish it from body structure
					return rel.target;
				else if (rel.target.equals(rootConcept))
					return conceptId;
				else
					return getSubHierarchy(rel.target, distinguishMorphology);
			}
		}
		//impossible to get here unless you're trying to trace the hierarchy of the root concept
		return null;
	}
	
	public static void main(String[] args)
	{
		try
		{
//			List<String> concepts = FileUtils.readLines(new File("nice.txt"));
			SnomedRelationReader relations = new SnomedRelationReader(ExtensionChecker.getFile(FileType.Relation, ExtensionChecker.coreSnapshotDir, ExtensionChecker.coreDate));
			
			log.info("Finished!");
//			Set<String> desc = new HashSet<String>();
//			for (String str : concepts)
//			{
//				Set<String> d = relations.getDescendants(str);
//				if (d.size() <= 100)
//					desc.addAll(d);
//				else
//					log.info("Skipping " + str);
////					if (desc.size() > 3000)
////						System.out.println(str);
//			}
//			
//			log.info(desc.size());
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}
}
