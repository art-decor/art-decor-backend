package nl.nictiz.snomed.tree;

import java.text.ParseException;
import java.util.Arrays;

public class SnomedTerm
{
	public enum TermType {fsn, synoniem, voorkeursterm};
	public static final String fsnProperty = "900000000000003001", synonymProperty = "900000000000013009";
	
	public final TermType type;
	public final String conceptId, termId, term, languageCode;
	
	public SnomedTerm(String[] line) throws ParseException
	{
		if ((line.length <= 1) || line[2].equals("0"))
			throw new ParseException("No active term defined in " + Arrays.toString(line), 0);	//skip inactive descriptions

		if (line[6].equals(fsnProperty))		//not the full specified name
			type = TermType.fsn;
		else if (line[6].equals(synonymProperty))
			type = TermType.synoniem;
		else
			throw new ParseException("Unsupported type " + line[6], 0);
		
		termId = line[0];
		conceptId = line[4];
		languageCode = line[5];
		term = line[7];
	}
	
	public SnomedTerm(String conceptId, String termId, String term, TermType type)
	{
		this.conceptId = conceptId;
		this.termId = termId;
		this.term = term;
		this.languageCode = "nl";
		this.type = type;
	}
}
