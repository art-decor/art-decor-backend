package nl.nictiz.snomed.tree;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import nl.nictiz.snomed.util.HashMapWithList;

public class Coordination
{
	List<String> parents = new ArrayList<String>();
	HashMapWithList<String,String> relations = new HashMapWithList<String,String>();
	
	@Override
	public String toString()
	{
		StringBuffer sb = new StringBuffer("Parents:");
		sb.append(parents.toString());
		sb.append("\nRelaties:");
		sb.append(relations.toString());
		return sb.toString();
	}
	
	/* 3434323241, 116680003|isA|= 34234324
	 * 116680003|isA|=  49793008 | hereditary motor neuron disease |, 116680003|isA|=  85995004 | autosomal recessive hereditary disorder | 
	 * 37340000 | motor neuron disease | : 246075003 | causative agent | = 311984009 | chemical or external agent | 
	 */
	public static Coordination parse(String coord) throws IOException
	{
		String[] tokens = coord.trim().split("\\D+");
		int cntr = 0;
		Coordination result = new Coordination();
		
		while (cntr < tokens.length)
		{
			if (tokens[cntr].equals("116680003"))
			{
				result.parents.add(tokens[cntr + 1]);
				cntr += 2;
			}
			else if (cntr == 0)
			{
				result.parents.add(tokens[cntr]);
				cntr++;
			}
			else if (tokens[cntr].length() <= 4)
				cntr++;
			else
			{
				result.relations.add(tokens[cntr], tokens[cntr+1]);
				cntr += 2;
			}
		}
		
		return result;
	}
	
	public static void main(String[] arg)
	{
		try	//116680003 | is a |= 
		{
			System.out.println(Coordination.parse("73211009 | diabetes mellitus | : 47429007 | associated with | = 116223007 | complication |"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
