package nl.nictiz.snomed.tree;

public class Relation
{
	public final String id, source, target, property, group;
	
	public Relation(String[] line)
	{
		this.id = line[0];
		this.source = line[4];
		this.target = line[5];
		this.group = line[6];
		this.property = line[7];	
	}
	
	public Relation(String id, String source, String property, String target)
	{
		this.source = source;
		this.property = property;
		this.target = target;
		this.id = id;
		this.group = "0";
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (o instanceof Relation)
			return this.id.equals(((Relation)o).id);
		return false;
	}
	
	@Override
	public int hashCode()
	{
		return this.id.hashCode();
	}
}
