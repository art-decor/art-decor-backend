package nl.nictiz.snomed.tree;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.aliasi.util.Arrays;

import au.com.bytecode.opencsv.CSVWriter;

public class Node implements Comparable
{
	private static Logger log = Logger.getLogger(Node.class);
	public final boolean inSet;
	public String sctid, fsn, coordination, prefTerm;
	public Set<String> sctids = new HashSet<String>();
	
	private List<Node> children = new ArrayList<Node>();
	
	public Node(String id, String fsn, String prefTerm, boolean inSet) throws IOException
	{
		if (id == null)
			throw new IOException("Trying to create concept with null id");
		
		this.sctid = id;
		if ((id != null) && id.isEmpty())
			this.sctid = null;
		this.fsn = fsn;
		if (fsn == null)
			log.warn("Cannot find FSN of " + id);
		this.prefTerm = prefTerm;
		this.inSet = inSet;
	}
	
	/* 3434323241, 116680003|isA|= 34234324
	 * 116680003|isA|=  49793008 | hereditary motor neuron disease |, 116680003|isA|=  85995004 | autosomal recessive hereditary disorder | 
	 * 37340000 | motor neuron disease | : 246075003 | causative agent | = 311984009 | chemical or external agent | 
	 */
	private void parseCoordation(String coord) throws IOException
	{
		String[] tokens = coord.trim().split("\\D+");
		int cntr = 0;
		
		while (cntr < tokens.length)
		{
			if (tokens[cntr].isEmpty())
				cntr++;
			else if (tokens[cntr].equals("116680003"))
			{
				sctids.add(tokens[cntr + 1]);
				cntr += 2;
			}
			else if (cntr == 0)
			{
				sctids.add(tokens[cntr]);
				cntr++;
			}
			else
				cntr += 2;
		}
		
		if (!sctids.isEmpty())
			return;

		String[] tokens2 = coord.split("\\s+");
		cntr = 0;

		while (cntr < tokens2.length)
		{
			if (tokens2[cntr].isEmpty())
				cntr++;
			else if (tokens2[cntr].equals("ISA"))
			{
				sctids.add(tokens2[cntr + 1]);
				cntr += 2;
			}
			else if (cntr == 0)
			{
				sctids.add(tokens2[cntr]);
				cntr++;
			}
			else
				cntr ++;
		}
	}
	
	public List<Node> getChildren()
	{
		return children;
	}
	
	public boolean hasChild(Node child)
	{
		if (child.sctid == null)
			return false;
		
		for (Node c : children)
		{
			if (child.sctid.equals(c.sctid))
				return true;
		}
		
		return false;
	}
	
	public void addChild(Node child)
	{
		this.children.add(child);
	}
	
	public boolean addChild(Node child, SnomedRelationReader reader)
	{
		if (child.sctid == null)
		{
			if (child.sctids.contains(this.sctid))
			{
				children.add(child);
				return true;
			}
			else
				return false;
		}
		else
			return addDescendant(child, reader);
	}
	
	public boolean addDescendant(Node node, SnomedRelationReader reader)
	{
		if (!hasDescendant(node, reader))
			return false;
		
		boolean added = false;
		for (Iterator<Node> it = children.iterator(); it.hasNext(); )
		{
			Node child = it.next();
			if ((node.sctid != null) && (child.sctid != null) && node.sctid.equals(child.sctid))
			{
				added = true;
				if (node.inSet && (!child.inSet))	//better version: replace root with node
					it.remove();
//				else
//					log.info("Identical nodes with id " + child.sctid);
			}
			else if (child.addDescendant(node, reader))		//node is successfully added as descendant of child
				added = true;
			else if (node.addDescendant(child, reader))		//child is successfully added as descendant of node, so remove child from this list
				it.remove();								//node will be added at end of method
		}
		
		if (!added)
			children.add(node);
		
		return true;
	}
	
	private boolean hasDescendant(Node node, SnomedRelationReader reader)
	{
		if (sctid == null)		//new concept, probably does not have children (excepting small-fiber neuropathy)
			return false;
		
		if (node.sctid != null)
		{
			if (node.sctid.equals(sctid))
				return false;
			return reader.isDescendant(node.sctid, sctid);
		}
		
		for (String childId : node.sctids)
		{
			if (reader.isDescendant(childId, sctid))
				return true;
		}
		
		return false;
	}
	
	//recursive; even if this node is not a descendant, some of its children might be due to polyhierarchic nature of snomed.
	public void addIfDescendant(Node parent, SnomedRelationReader reader)
	{	
		if (parent.children.contains(this))
			return;	//already added
		
		if (parent.hasDescendant(this, reader))
		{	//add this node to parent
			parent.children.add(this);
		}
		else
		{
			for (Node child : children)
				child.addIfDescendant(parent, reader);		//recurse
		}
	}
	
	public Node findDescendant(String sctid)
	{
		for (Node node : children)
		{
			if (node.sctid.equals(sctid))
				return node;
			
			Node result = node.findDescendant(sctid);
			if (result != null)
				return result;
		}
		return null;
	}
	
	public void copyChildren(Node node)
	{
		for (Node child : node.children)
		{
			if (!this.hasChild(child))
				this.children.add(child);
		}
	}
	
	/** For every child, check if it is not also a grandchild or deeper descendant.
	 * If it is, then remove it from the list of children. Recurse until you reach a leaf.
	 */
	public void cleanDoubles()
	{
		for (Iterator<Node> it = children.iterator(); it.hasNext(); )
		{	
			Node child = it.next();
			if (child.sctid == null)	//skip nodes without id
				continue;
			
			for (Node other : children)
			{
				if ((other.sctid == null) ||child.sctid.equals(other.sctid))
					continue;			//skip if other node has no id or is in fact the samen node
				
				Node copy = other.findDescendant(child.sctid);		//try to find this node among the descendants of its siblings
				if (copy != null)		//not found, so try another sibling
				{
					log.info("Removing " + child.sctid + " |" + child.prefTerm + "| from " + sctid + " |" + prefTerm + "|");
					it.remove();			//found, so remove node as child
					copy.copyChildren(child);
					break;	//Node has been removed, so stop checking its siblings; proceed to next child instead
				}
			}
		}
		
		for (Node child : children)
			child.cleanDoubles();   //now do the same for all child nodes
	}
	
	/* -	prefTerm (sctid; cramp; crampDesc)
	 * 		- prefTerm (sctid; cramp; crampDesc)
	 */
	public void print(FileWriter writer, int tabs) throws IOException
	{
		for (int i = 0; i < tabs; i++)
			writer.write("\t");
		writer.write("-\t");
		
		if (!inSet)
			writer.write("[" + fsn + "(" + sctid + ")]\n");
		else if (prefTerm == null)
			writer.write(sctid + "\t|" + fsn + "|\n");
		else
			writer.write(prefTerm + "\t\t" + sctid + "\t|" + fsn + "|\n");
		
		Collections.sort(children);
		for (Node child : children)
			child.print(writer, tabs + 1);
	}
	
	public void printToCsv(CSVWriter writer, int column)
	{
		String[] line = new String[column + 2];
		line[column] = sctid;
		line[column + 1] = fsn;//prefTerm + "\n" + sctid + " |" + fsn + "|";
		writer.writeNext(line);
		
		Collections.sort(children);
		for (Node child : children)
			child.printToCsv(writer, column + 1);
	}
	
	public String printToHtml()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("<div><table><tr>\n<td class=tdtree>");
		if (!children.isEmpty())
			sb.append("<span class=spantree onclick=\"toggle(this)\" oncontextmenu=\"toggle_recursive(this); return false;\">+</span>");
		
		sb.append("</td>\n<td class=tdtreecontent>\n<span class=\"spantreeitem\">");
		sb.append(prefTerm);
		if (sctid != null)
			sb.append(": " + sctid + " |" + fsn + "|");
		sb.append("</span>\n");
		
		if (!children.isEmpty())
		{
			sb.append("<div class=divchildrenhide>\n\n");
			Collections.sort(children);
			for (Node child : children)
				sb.append(child.printToHtml());
			sb.append("</div>\n");
		}
		sb.append("</td></tr></table></div>\n\n");
		return sb.toString();
	}
	
	public void printAsList(CSVWriter writer, Set<String> set, boolean addParents, SnomedRelationReader reader, Map<String,String> fsnMap)
	{
		String[] line = {sctid, fsn, prefTerm};
		if (addParents)
		{
			Set<String> parents = reader.getParents(sctid);
			String[] parentLine = new String[parents.size()];
			int i = 0;
			for (Iterator<String> it = parents.iterator(); it.hasNext(); )
			{
				String parentId = it.next();
				parentLine[i] = parentId + " |" + fsnMap.get(parentId) + "|";
				i++;
			}
			writer.writeNext(Arrays.concatenate(line, parentLine));
		}
		else
			writer.writeNext(line);
		Collections.sort(children);
		
		for (Node child : children)
		{
			if (set.contains(child.sctid))
				continue;
			
			set.add(child.sctid);
			child.printAsList(writer, set, addParents, reader, fsnMap);		
		}
	}
	
	@Override
	public int compareTo(Object o)
	{
		if (this.prefTerm != null)
			return this.prefTerm.compareTo(((Node) o).prefTerm);
		return this.fsn.compareTo(((Node)o).fsn);
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (sctid == null)
			return false;
		
		if (!(o instanceof Node))
			return false;
		
		Node other = (Node) o;
		return sctid.equals(other.sctid);
	}
}
