package nl.nictiz.snomed.check;

import static org.junit.Assert.*;
import nl.nictiz.snomed.check.ExtensionChecker.FileType;

import org.junit.Test;

/** Use @Test to run tests, @Ignore to skip them.
 * 
 * @author hielkema
 *
 */
public class IDCheckerTest
{
	private IDChecker idChecker;
	
	public IDCheckerTest()
	{
		idChecker = new IDChecker();
	}

	@Test
	public void checkValidUUID()
	{
		assertTrue(idChecker.isValidUUID("00005c54-d507-54fa-9350-75f24d9ece1c"));
		assertTrue(idChecker.isValidUUID("3a6f8872-26ca-4f89-b455-c718df294f40"));
		assertTrue(idChecker.isValidUUID("3A6f8872-26ca-4f89-b455-c718df294f40"));
	}
	
	@Test
	public void checkInvalidUUID()
	{
		assertFalse(idChecker.isValidUUID("00005c54-d507-54fa-9350-75f249ece1c"));
		assertFalse(idChecker.isValidUUID("3a6f887d2-26ca-4f89-b455-c718df294f40"));
		assertFalse(idChecker.isValidUUID("3a6f8k72-26ca-4f89-b455-c718df294f40"));
		assertFalse(idChecker.isValidUUID("3a6f8872-26ca-4f89-c455-c718df294f40"));
		assertFalse(idChecker.isValidUUID("3a6f8872-26ca-7f89-b455-c718df294f40"));
		assertFalse(idChecker.isValidUUID("3a6f8872-26ca-4f89-b455-c718dg294f40"));
	}
	
	@Test
	public void checkValidSCTID()
	{
		assertTrue(idChecker.isValidSCTID("405751000"));
		assertTrue(idChecker.isValidSCTID("31000146106"));
	}
	
	@Test
	public void checkInvalidSCTID()
	{
		assertFalse(idChecker.isValidSCTID("405751003"));	
		assertFalse(idChecker.isValidSCTID("31050146106"));
	}
	
	@Test
	public void checkValidDutchSCDTID()
	{
		assertTrue(idChecker.isValidDutchSCTID("31000146106", FileType.Concept));
		assertTrue(idChecker.isValidDutchSCTID("111000146114", FileType.Description));
		assertTrue(idChecker.isValidDutchSCTID("41000146126", FileType.Relation));
	}
	
	@Test
	public void checkInvalidDutchSCDTID()
	{
		assertFalse(idChecker.isValidDutchSCTID("405751000", FileType.Concept));		//id from core, not Dutch namespace
		assertFalse(idChecker.isValidDutchSCTID("31000146012", FileType.Concept));	//core partition
		assertFalse(idChecker.isValidDutchSCTID("31000146103", FileType.Concept));	//wrong Verhoeff check-digit
		assertFalse(idChecker.isValidDutchSCTID("31000146106", FileType.Description));	//wrong type of partition
	}
}
