package nl.nictiz.snomed.check;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

public class DescriptionCheckerTest
{
	private DescriptionChecker checker;
	
	public DescriptionCheckerTest() throws IOException
	{
		checker = DescriptionChecker.getDescriptionChecker();
	}
	
	@Test
	public void testCollectDescriptionsWithDiacrits() throws IOException
	{	//encoding correct
		assertEquals(0, checker.collectDescriptionsWithCorruptDiacrits(new File("testResources/descriptions.txt"), new File("testResources/out.txt")));
		//characters correct, but stored in Latin-1
		assertEquals(5, checker.collectDescriptionsWithCorruptDiacrits(new File("testResources/descriptions-latin1.txt"), new File("testResources/out2.txt")));
		//stored in UTF-8 but characters corrupted
		assertEquals(5, checker.collectDescriptionsWithCorruptDiacrits(new File("testResources/descriptions-corrupt-utf8.txt"), new File("testResources/out3.txt")));
		//characters corrupted and stored in Latin-1
		assertEquals(5, checker.collectDescriptionsWithCorruptDiacrits(new File("testResources/descriptions-corrupt.txt"), new File("testResources/out4.txt")));
	}
	
	@Test
	public void failIfFileDoesNotExist()
	{
		try
		{
			 checker.collectDescriptionsWithCorruptDiacrits(new File("noSuchFile.txt"), new File("testResources/out.txt"));
		}
		catch (IOException e)
		{
			assertTrue(true);
		}
	}
	
	@Test
	public void succeedIfCommonDiacrit()
	{
		assertTrue(checker.isCommonDiacrit('�'));
		assertTrue(checker.isCommonDiacrit('�'));
		assertTrue(checker.isCommonDiacrit('�'));
		assertTrue(checker.isCommonDiacrit('�'));
	}
	
	@Test
	public void failIfNotCommonDiacrit()
	{
		assertFalse(checker.isCommonDiacrit('�'));
		assertFalse(checker.isCommonDiacrit('�'));
	}
	
	@Test
	public void succeedIfCorrupted()
	{
		assertTrue(checker.isCorrupt("uve�tis"));
		assertTrue(checker.isCorrupt("m�bius-syndroom"));
		assertTrue(checker.isCorrupt("bacteri�le corneale ulcus"));
	}
	
	@Test
	public void failIfNotCorrupted()
	{
		assertFalse(checker.isCorrupt("acuut gesloten-kamerhoek glaucoom"));
		assertFalse(checker.isCorrupt("uve�tis"));
		assertFalse(checker.isCorrupt("m�bius-syndroom"));
		assertFalse(checker.isCorrupt("bacteri�le corneale ulcus"));
	}
}
