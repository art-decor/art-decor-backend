package nl.nictiz.snomed.check;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

public class DutchEditDistanceTest
{
	private DutchEditDistance edit;
	
	public DutchEditDistanceTest() throws IOException
	{
		edit = DutchEditDistance.getDutchEditDistance();
		edit.setAcceptableDistance(0);
	}

	@Test
	public void ignoreClassicalSpelling()
	{
		assertTrue(edit.similar("rinorroe", "rhinorrhoe"));
		assertTrue(edit.similar("asthma", "astma"));
		assertTrue(edit.similar("hypertrofie", "hypertrophy"));
		assertTrue(edit.similar("lymfokele", "lymphocele"));
		assertTrue(edit.similar("aether", "ether"));
	}
	
	@Test
	public void ignoreDiacrits()
	{
		assertTrue(edit.similar("lymfok�le", "lymfokele"));
		assertFalse(edit.similar("lymfok�le", "lymfokale"));
	}
	
	@Test
	public void ignoreCase()
	{
		assertTrue(edit.similar("Pneumonie", "pneumonie"));
	}
	
	@Test
	public void ignoreWordOrder()
	{
		assertTrue(edit.similar("glaucoom oog", "oog glaucoom"));
		assertTrue(edit.similar("glaucoom oog kamerhoek", "kamerhoek glaucoom oog"));
	}
	
	@Test
	public void ignorePunctuation()
	{
		assertTrue(edit.similar("kamerhoekglaucoom.", "kamerhoekglaucoom"));
		assertTrue(edit.similar("kamerhoekglaucoom, gesloten", "kamerhoekglaucoom gesloten"));
		assertFalse(edit.similar("kamerhoekglaucoom", "kamerhoekglaucoomG"));
	}
	
	@Test
	public void ignoreStopwords()
	{
		assertTrue(edit.similar("glaucoom van oog", "glaucoom van het oog"));
		assertFalse(edit.similar("glaucoom van oog", "glaucoom van blauw oog"));
	}
	
	@Test
	public void ignoreStopwordsAndWordOrder()
	{
		assertTrue(edit.similar("oogglaucoom", "glaucoom van oog"));
		assertFalse(edit.similar("oogglaucoom", "glaucoom open oog"));
	}
	
	@Test
	public void ignoreHyphen()
	{
		assertTrue(edit.similar("gesloten-kamerhoekglaucoom", "geslotenkamerhoekglaucoom"));
	}
	
	@Test
	public void matchContractions()
	{
		assertTrue(edit.similar("geslotenkamerhoekglaucoom", "gesloten kamerhoekglaucoom"));
		assertTrue(edit.similar("geslotenkamerhoekglaucoom", "gesloten kamerhoek glaucoom"));
		assertTrue(edit.similar("gesloten-kamerhoekglaucoom", "gesloten kamerhoekglaucoom"));
		assertFalse(edit.similar("geslotenkamerhoekglaucoom", "open kamerhoekglaucoom"));
	}
	
	@Test
	public void matchCombination()
	{
		assertTrue(edit.similar("geslotenkamerhoekglaucoom", "kamerhoek glaucoom, gesloten"));
		assertFalse(edit.similar("geslotenkamerhoekglaucoom", "kamerhoek glaucoom, open"));
	}
	
	@Test
	public void matchSimilar()
	{
		assertFalse(edit.similar("strabisme", "strabismus"));
		edit.setAcceptableDistance(2);
		assertTrue(edit.similar("strabisme", "strabismus"));
	}
	
	@Test
	public void checkOvermatching()
	{
		edit.setAcceptableDistance(2);
		assertFalse(edit.similar("Dickeya zeae", "Deer flies"));
		assertFalse(edit.similar("Medical doctor in nursing home (occupation)", "Metal rolling mill worker (occupation)"));
	}
	
	@Test
	public void checkEnglishDeterminers()
	{
		edit.setAcceptableDistance(0);
		assertTrue(edit.similar("furuncle of lip", "furuncle of the lip"));
		assertTrue(edit.similar("furuncle of ear", "furuncle of an ear"));
//		assertFalse(edit.similar("furuncle of lip", "furuncle of hip"));
	}
}
