package nl.nictiz.snomed.check;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Map;

import nl.nictiz.snomed.check.ExtensionChecker.FileType;
import nl.nictiz.snomed.util.HashMapWithList;

import org.apache.log4j.Logger;
import org.junit.Test;

/** This is not really a compile-test class. It is meant to validate
 * an RF2-export of the Dutch Extension for internal consistency.
 * 
* While the tests in CheckExtension have to succeed, the tests in this class
* may fail. When they do fail, the user must check manually whether the generated 
* warnings signal real problems (such as corrupted characters or equivalent concepts)
* or acceptable situations, such as similar but not identical concepts or the use 
* of uncorrupted diacrits.
* 
* All warnings are printed to the console.
*/
public class CheckExtensionDescriptions
{
	private static Logger log = Logger.getLogger(CheckExtensionDescriptions.class);
		
	/** Tests whether any characters with diacrits (e.g. �, �)  in Snapshot have become corrupted 
	 * due to character encoding problems. The files ought to be in UTF-8.
 	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void testDiacritsSnapshot() throws IOException, ParseException
	{
		log.info("Checking whether characters with diacrits have been corrupted in snapshot");
		File dutchDescriptionFile = ExtensionChecker.getFile(FileType.Description, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		HashMapWithList<String,Description> nlDescriptions = ExtensionChecker.getDescriptionsByConceptID(dutchDescriptionFile);
		DescriptionChecker descriptionChecker = DescriptionChecker.getDescriptionChecker();
		assertEquals(0, descriptionChecker.collectDescriptionsWithCorruptDiacrits(nlDescriptions, new File("corruptedDescriptionsSnapshot.txt")));
	}
	
	/** Tests whether any characters with diacrits (e.g. �, �)  in Full have become corrupted 
	 * due to character encoding problems. The files ought to be in UTF-8.
 	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void testDiacritsFull() throws IOException, ParseException
	{
		log.info("Checking whether characters with diacrits have been corrupted in full");
		File dutchDescriptionFile = ExtensionChecker.getFile(FileType.Description, ExtensionChecker.nlFullDir, ExtensionChecker.nlDate);
		HashMapWithList<String,Description> nlDescriptions = ExtensionChecker.getDescriptionsByConceptID(dutchDescriptionFile);
		DescriptionChecker descriptionChecker = DescriptionChecker.getDescriptionChecker();
		assertEquals(0, descriptionChecker.collectDescriptionsWithCorruptDiacrits(nlDescriptions, new File("corruptedDescriptionsFull.txt")));
	}
	
	/** Tests whether any characters with diacrits (e.g. �, �)  in Delta have become corrupted 
	 * due to character encoding problems. The files ought to be in UTF-8.
 	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void testDiacritsDelta() throws IOException, ParseException
	{
		log.info("Checking whether characters with diacrits have been corrupted in delta");
		File dutchDescriptionFile = ExtensionChecker.getFile(FileType.Description, ExtensionChecker.nlDeltaDir, ExtensionChecker.nlDate);
		HashMapWithList<String,Description> nlDescriptions = ExtensionChecker.getDescriptionsByConceptID(dutchDescriptionFile);
		DescriptionChecker descriptionChecker = DescriptionChecker.getDescriptionChecker();
		assertEquals(0, descriptionChecker.collectDescriptionsWithCorruptDiacrits(nlDescriptions, new File("corruptedDescriptionsDelta.txt")));
	}
	
	/** Checks whether any concept contains Dutch descriptions that are too much alike. 
	 * This test does not necessarily have to succeed: its results have to be checked by 
	 * a terminologist.
	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void checkForSimilarDescriptions() throws IOException, ParseException
	{
		File dutchDescriptionFile = ExtensionChecker.getFile(FileType.Description, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		HashMapWithList<String,Description> nlDescriptions = ExtensionChecker.getDescriptionsByConceptID(dutchDescriptionFile);
		DescriptionChecker descriptionChecker = DescriptionChecker.getDescriptionChecker();
		assertTrue(descriptionChecker.checkDescriptionSimilarityWithinConcept(nlDescriptions, new File("similarDescriptions.csv")));
	}
	
	/** Checks whether SNOMED (core + extension) contains concepts with identical FSNs - 
	 * which would be a definite error.
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void checkForDoubleFSNs() throws IOException, ParseException
	{
		Map<String,Boolean> concepts = ExtensionChecker.initConcepts(ExtensionChecker.getFile(FileType.Concept, ExtensionChecker.coreSnapshotDir, ExtensionChecker.coreDate));
		concepts.putAll(ExtensionChecker.initConcepts(ExtensionChecker.getFile(FileType.Concept, ExtensionChecker.nlDeltaDir, ExtensionChecker.nlDate)));
		
		File coreDescriptionFile = ExtensionChecker.getFile(FileType.Description, ExtensionChecker.coreSnapshotDir, ExtensionChecker.coreDate);
		File dutchDescriptionFile = ExtensionChecker.getFile(FileType.Description, ExtensionChecker.nlDeltaDir, ExtensionChecker.nlDate);
		HashMapWithList<String,Description> fsns = ExtensionChecker.getActiveDescriptions(true, concepts, coreDescriptionFile, dutchDescriptionFile);
		
		DescriptionChecker descriptionChecker = DescriptionChecker.getDescriptionChecker();
		assertTrue(descriptionChecker.checkForDoubleFSNs(fsns));
	}
	
	/** Checks for descriptions of different concepts in core and extension 
	 * that are very similar, as this may indicate that it they are doppelgangers.
	 * @throws IOException 
	 * @throws ParseException 
	 *
	@Test
	public void checkForDoppelgangersInCoreAndExtension() throws IOException, ParseException
	{
		DescriptionChecker descriptionChecker = DescriptionChecker.getDescriptionChecker();
		assertTrue(descriptionChecker.checkForDoppelgangersInCoreAndExtension(true, 1));	//skip organism hierarchy; any doppelgangers there should be found by fsn-check
	}*/
}
