package nl.nictiz.snomed.check;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Map;

import nl.nictiz.snomed.check.ExtensionChecker.FileType;
import nl.nictiz.snomed.util.HashMapWithList;

import org.apache.log4j.Logger;
import org.junit.Test;

/** This is not really a compile-test class. It is meant to validate
 * an RF2-export of the Dutch Extension for internal consistency.
 * 
* This involves, amongst other things, checking if all id's are valid
* and not duplicate, if identical concepts or descriptions are not
* referred to by two separate id's, etc.
* 
* All warnings are printed to the console.
*/
public class CheckExtension
{
	private static Logger log = Logger.getLogger(CheckExtension.class);
	
	private IDChecker idChecker;
	private CrossReferenceChecker referenceChecker;
	
	/** Constructor.
	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
	public CheckExtension() throws IOException, ParseException
	{
		idChecker = new IDChecker();
		referenceChecker = new CrossReferenceChecker(ExtensionChecker.coreDate, ExtensionChecker.nlDate);
	}
	
	/** Checks whether all sctid's in Snapshot are valid.
	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void testIDsSnapshot() throws IOException, ParseException
	{	//encoding correct
		log.info("Checking Snapshot IDs");
		assertTrue(idChecker.checkIDsSnapshot(ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate, true));
	}
	
	/** Checks whether all sctid's in Full are valid.
	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void testIDsFull() throws IOException, ParseException
	{	//encoding correct
		log.info("Checking Full IDs");
		assertTrue(idChecker.checkIDsFull(ExtensionChecker.nlFullDir, ExtensionChecker.nlDate, true));
	}
	
	/** Checks whether all sctid's in Delta are valid.
	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void testIDsDelta() throws IOException, ParseException
	{
		log.info("Checking Delta IDs");		//Uses same method as snapshot as we assume each id only occurs once in delta
		assertTrue(idChecker.checkIDsSnapshot(ExtensionChecker.nlDeltaDir, ExtensionChecker.nlDate, true));
	}
	
	/** Checks whether all dates in Snapshot are valid.
	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void testDatesSnapshot() throws IOException
	{	//encoding correct
		log.info("Checking Snapshot IDs");
		assertTrue(idChecker.checkDates(ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate));
	}
	
	/** Checks whether all dates in Full are valid.
	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void testDatesFull() throws IOException
	{	//encoding correct
		log.info("Checking Full IDs");
		assertTrue(idChecker.checkDates(ExtensionChecker.nlFullDir, ExtensionChecker.nlDate));
	}
	
	/** Checks whether all dates in Delta are valid.
	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void testDatesDelta() throws IOException
	{
		log.info("Checking Delta IDs");
		assertTrue(idChecker.checkDates(ExtensionChecker.nlDeltaDir, ExtensionChecker.nlDate));
	}
	
	/** Checks whether there are concept IDs that occur in the concept file of the core
	 * and the extension. This might happen if a concept is promoted but we fail to remove
	 * (not deactivate!) it from the extension. 
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCheckForDoubleRefsToPromotedConcepts() throws IOException
	{
		log.info("Checking for double references to promoted concepts");
		assertTrue(idChecker.checkForDoubleRefsToPromotedConcepts());
	}

	/** Checks whether all references to conceptIds in the snapshot/terminology/description file 
	 * refer to valid and existing conceptIds.
	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void testDescriptionReferencesSnapshot() throws IOException, ParseException
	{
		log.info("Checking references to concepts in Description file (snapshot)");
		File dutchDescriptionFile = ExtensionChecker.getFile(FileType.Description, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		HashMapWithList<String,Description> nlDescriptions = ExtensionChecker.getDescriptionsByConceptID(dutchDescriptionFile);
		assertTrue(referenceChecker.checkConceptReferences(nlDescriptions.keySet()));
	}
	
	/** Checks whether all sources of relations (which are references to conceptIds in the 
	 * snapshot/terminology/relation file) refer to valid and existing conceptIds.
	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void testRelationSourceReferencesSnapshot() throws IOException
	{
		log.info("Checking references to concepts in Relation file, source column (snapshot)");
		File relationFile = ExtensionChecker.getFile(FileType.Relation, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		Map<String,Boolean> relationIds = IDChecker.readColumnFromFile(relationFile, 4);
		assertTrue(referenceChecker.checkActiveConceptReferences(relationIds));
	}
	
	/** Checks whether all targets of relations (which are references to conceptIds in the 
	 * snapshot/terminology/relation file) refer to valid and existing conceptIds.
	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void testRelationTargetReferencesSnapshot() throws IOException
	{
		log.info("Checking references to concepts in Relation file, target column (snapshot)");
		File relationFile = ExtensionChecker.getFile(FileType.Relation, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		Map<String,Boolean> relationIds = IDChecker.readColumnFromFile(relationFile, 5);
		assertTrue(referenceChecker.checkActiveConceptReferences(relationIds));
	}
	
	/** Checks whether all references to conceptIds in the snapshot/terminology/description file 
	 * refer to valid conceptIds that existed once, even though they may no longer be active.
	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void testDescriptionReferencesFull() throws IOException, ParseException
	{
		log.info("Checking references to concepts in Description file (full)");
		File dutchDescriptionFile = ExtensionChecker.getFile(FileType.Description, ExtensionChecker.nlFullDir, ExtensionChecker.nlDate);
		HashMapWithList<String,Description> nlDescriptions = ExtensionChecker.getDescriptionsByConceptID(dutchDescriptionFile);
		assertTrue(referenceChecker.checkConceptReferences(nlDescriptions.keySet()));
	}
	
	/** Checks whether all sources of relations (which are references to conceptIds in the 
	 * snapshot/terminology/relation file) refer to valid conceptIds that existed once, even though 
	 * they may no longer be active.
	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void testRelationSourceReferencesFull() throws IOException
	{
		log.info("Checking references to concepts in Relation file, source column (full)");
		File relationFile = ExtensionChecker.getFile(FileType.Relation, ExtensionChecker.nlFullDir, ExtensionChecker.nlDate);
		Map<String,Boolean> relationIds = IDChecker.readColumnFromFile(relationFile, 4);
		assertTrue(referenceChecker.checkConceptReferences(relationIds.keySet()));
	}
	
	/** Checks whether all targets of relations (which are references to conceptIds in the 
	 * snapshot/terminology/relation file) refer to valid conceptIds that existed once, 
	 * even though they may no longer be active.
	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void testRelationTargetReferencesFull() throws IOException
	{
		log.info("Checking references to concepts in Relation file, target column (full)");
		File relationFile = ExtensionChecker.getFile(FileType.Relation, ExtensionChecker.nlFullDir, ExtensionChecker.nlDate);
		Map<String,Boolean> relationIds = IDChecker.readColumnFromFile(relationFile, 5);
		assertTrue(referenceChecker.checkConceptReferences(relationIds.keySet()));
	}
	
	/** Check that each Dutch concept has at least one active is-a relation.
	 */
	@Test
	public void testMinimumOneIsARelationSnapshot() throws IOException
	{
		log.info("Starting");
		DefinitionCompletenessChecker checker = new DefinitionCompletenessChecker(ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		File relationFile = ExtensionChecker.getFile(FileType.Relation, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		assertTrue(checker.checkMinimumOneIsARelation(relationFile));
	}
	
	/** Checks whether all conceptIds in all simple refsets are valid and existing
	 * conceptIds (in snapshot/refset/content/simplerefsetFile).
	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
//	@Test
	public void testSimpleRefsetConceptReferencesSnapshot() throws IOException
	{
		log.info("Checking references to concepts in simple refset file (snapshot)");
		SimpleRefset simpleRefset = new SimpleRefset(ExtensionChecker.getFile(FileType.SimpleRefset, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate));
		assertTrue(referenceChecker.checkActiveConceptReferences(simpleRefset.getReferenceIds()));
	}
	
	/** Checks whether no simple refset in snapshot contains multiple references to 
	 * the same conceptId (NB. in Full this would not necessarily be an error!)
	 * 
	 * @throws IOException
	 */
	@Test
	public void testSimpleRefsetUniqueReferencesSnapshot() throws IOException
	{
		log.info("Checking uniqueness of references in simple refset file (snapshot)");
		SimpleRefset simpleRefset = new SimpleRefset(ExtensionChecker.getFile(FileType.SimpleRefset, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate));
		assertTrue(simpleRefset.checkReferenceUniquenessPerRefset());
	}
	
	/** Checks whether all conceptIds in all simple refsets are valid and existing
	 * descriptionIds (in snapshot/refset/language/languagerefsetFile).
	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void testLanguageRefsetDescriptionReferencesSnapshot() throws IOException, ParseException
	{
		log.info("Checking references to descriptions in language refset file (snapshot)");
		File dutchDescriptionFile = ExtensionChecker.getFile(FileType.Description, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		HashMapWithList<String,Description> nlDescriptions = ExtensionChecker.getDescriptionsByConceptID(dutchDescriptionFile);
		File languageRefsetFile = ExtensionChecker.getFile(FileType.LanguageRefset, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		LanguageRefset languageRefset = new LanguageRefset(languageRefsetFile);	
		
		assertTrue(referenceChecker.checkDescriptionReferences(languageRefset.getActiveReferenceIds(), nlDescriptions.getAllValues()));
	}
	
	/** Checks whether no language refset in snapshot contains multiple references to 
	 * the same descriptionId (NB. in Full this would not necessarily be an error!)
	 * 
	 * @throws IOException
	 */
	@Test
	public void testLanguageRefsetUniqueReferencesSnapshot() throws IOException, ParseException
	{
		log.info("Checking uniqueness of references in language refset file (snapshot)");
		File languageRefsetFile = ExtensionChecker.getFile(FileType.LanguageRefset, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		LanguageRefset languageRefset = new LanguageRefset(languageRefsetFile);	
		assertTrue(languageRefset.checkReferenceUniquenessPerRefset());
	}
	
	/** Checks whether each concept in the extension has a FSN and US preferred term
	 * @throws ParseException 
	 * @throws IOException 
	 * 
	 */
//	@Test
	public void testFSNAndUSPrefTermSnapshot() throws IOException, ParseException
	{
		log.info("Checking FSNs in Dutch Extension (snapshot)");
		File dutchDescriptionFile = ExtensionChecker.getFile(FileType.Description, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		HashMapWithList<String,Description> nlDescriptions = ExtensionChecker.getDescriptionsByConceptID(dutchDescriptionFile);
		File languageRefsetFile = ExtensionChecker.getFile(FileType.LanguageRefset, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		LanguageRefset languageRefset = new LanguageRefset(languageRefsetFile);	
		
		DefinitionCompletenessChecker checker = new DefinitionCompletenessChecker(ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		assertTrue(checker.checkFSNAndPrefTerm(languageRefset, nlDescriptions, ExtensionChecker.usLanguageRefsetId));
	}
	
	/** Checks whether each concept in a simple refset has exactly one Dutch preferred term
	 * (in snapshot).
	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
//	@Test
	public void testDutchLanguageRefsetPreferredTermSnapshot() throws IOException, ParseException
	{
		log.info("Checking preferred terms in Dutch language refset (snapshot)");
		File dutchDescriptionFile = ExtensionChecker.getFile(FileType.Description, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		HashMapWithList<String,Description> nlDescriptions = ExtensionChecker.getDescriptionsByConceptID(dutchDescriptionFile);
		File languageRefsetFile = ExtensionChecker.getFile(FileType.LanguageRefset, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		LanguageRefset languageRefset = new LanguageRefset(languageRefsetFile);	
		SimpleRefset simpleRefset = new SimpleRefset(ExtensionChecker.getFile(FileType.SimpleRefset, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate));
		
		Map<String,Boolean> concepts = IDChecker.readColumnFromFile(ExtensionChecker.getFile(FileType.Concept, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate), 0);
		
		assertTrue(simpleRefset.checkPreferredTerm(languageRefset, nlDescriptions, concepts, ExtensionChecker.dutchLanguageRefsetId));
	}
	
	/** Checks whether each concept in a simple refset has exactly one US preferred term
	 * (in snapshot).
	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
//	@Test		Blijkt dubbel met testFSNAndUSPrefTermSnapshot
	public void testAmericanLanguageRefsetPreferredTermSnapshot() throws IOException, ParseException
	{
		log.info("Checking preferred terms in US language refset (snapshot)");
		File dutchDescriptionFile = ExtensionChecker.getFile(FileType.Description, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		HashMapWithList<String,Description> nlDescriptions = ExtensionChecker.getDescriptionsByConceptID(dutchDescriptionFile);
		
		File nlLanguageRefsetFile = ExtensionChecker.getFile(FileType.LanguageRefset, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		LanguageRefset languageRefset = new LanguageRefset(nlLanguageRefsetFile);	
		SimpleRefset simpleRefset = new SimpleRefset(ExtensionChecker.getFile(FileType.SimpleRefset, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate));
		
		Map<String,Boolean> concepts = IDChecker.readColumnFromFile(ExtensionChecker.getFile(FileType.Concept, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate), 0);
		
		assertTrue(simpleRefset.checkPreferredTerm(languageRefset, nlDescriptions, concepts, ExtensionChecker.usLanguageRefsetId));
	}
	
	/** Checks (in snapshot) whether each reference set is defined in the metadata
	 * (in snapshot/refset/metadata/refsetDescriptorFile)
	 * 
	 * @throws IOException
	 */
	@Test
	public void testMetadataSnapshot() throws IOException
	{
		log.info("Checking whether all refsets are defined in metadata (snapshot)");
		SimpleRefset simpleRefset = new SimpleRefset(ExtensionChecker.getFile(FileType.SimpleRefset, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate));
		Map<String,Boolean> refsetIds = simpleRefset.getRefsetIds();
		
		LanguageRefset languageRefset = new LanguageRefset(ExtensionChecker.getFile(FileType.LanguageRefset, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate));
		refsetIds.putAll(languageRefset.getRefsetIds());
		
		File nlRefsetDescFile = ExtensionChecker.getFile(FileType.RefsetDescriptor, ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDate);
		File coreRefsetDescFile = ExtensionChecker.getFile(FileType.RefsetDescriptor, ExtensionChecker.coreSnapshotDir, ExtensionChecker.coreDate);
		Map<String,Boolean> refsetsInMetadata = IDChecker.readColumnFromFile(nlRefsetDescFile, 5);
		refsetsInMetadata.putAll(IDChecker.readColumnFromFile(coreRefsetDescFile, 5));
		
		assertTrue(ExtensionChecker.compareRefsetIds(refsetIds, refsetsInMetadata));
	}
	
	/** Checks whether all columns in the conceptInactivationRefset-file are valid
	 * 
	 * @throws IOException
	 */
	@Test
	public void testConceptInactivationRefsetSnapshot() throws IOException
	{
		log.info("Checking concept inactivation refset file (snapshot)");
		assertTrue(new DeactivationRefsetChecker().checkDeactivationRefsetSnapshot(FileType.AttributeValueRefset));
	}
	
	/** Checks whether all columns in the historicalAssocationRefset-file are valid
	 * 
	 * @throws IOException
	 */
	@Test
	public void testHistoricalAssociationRefsetSnapshot() throws IOException
	{
		log.info("Checking concept inactivation refset file (snapshot)");
		assertTrue(new DeactivationRefsetChecker().checkDeactivationRefsetSnapshot(FileType.AssocationRefset));
	}
	
	/** Checks whether all concepts, descriptions, relations etc. in Delta occur in Snapshot
	 * in the same state. Please note that this method assumes that Delta does NOT contain 
	 * changes that were made since the previous release, that have already been undone!
	 * It does not check whether the delta matches this snapshot minus the previously released
	 * snapshot.
	 * 
	 * @throws IOException 
	 */
	@Test
	public void testDeltaSubsetOfSnapshot() throws IOException
	{
		log.info("Checking whether Delta is a proper subset of Snapshot");
		ExportTypeComparer etc = new ExportTypeComparer();
		assertTrue(etc.compareSnapshotAndDelta(ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlDeltaDir, ExtensionChecker.nlDate));
	}
	
	/** Checks whether the most recent version of any object in Full occurs in Snapshot.
	 * (curr_snapshot = curr_full - prev_version_of_curr_objects)
	 * 
	 * @throws IOException 
	 * @throws ParseException */
	@Test
	public void testMostRecentFullIsSnapshot() throws IOException, ParseException
	{
		log.info("Checking whether the most recent version of each object in full matches snapshot");
		ExportTypeComparer etc = new ExportTypeComparer();
		assertTrue(etc.compareSnapshotAndFull(ExtensionChecker.nlSnapshotDir, ExtensionChecker.nlFullDir, ExtensionChecker.nlDate));
	}
	
	/** Checks whether prev_full + curr_delta = curr_full 
	 * @throws IOException 
	 */
	@Test
	public void compareAgainstPreviousReleaseFull() throws IOException
	{
		log.info("Checking the contents of the delta against the previous release with snapshot");
		ExportTypeComparer etc = new ExportTypeComparer();
		assertTrue(etc.compareAgainstPreviousFullRelease(ExtensionChecker.prevNlFullDir, ExtensionChecker.nlDeltaDir, ExtensionChecker.nlFullDir, 
				ExtensionChecker.nlDate, ExtensionChecker.prevNlDate));
	}
	
	/** Checks whether prev_snapshot + curr_delta - prev_version_of_curr_objects = curr_snapshot
	 * @throws IOException 
	 */
	@Test
	public void compareAgainstPreviousReleaseSnapshot() throws IOException
	{
		log.info("Checking the contents of the delta against the previous release with snapshot");
		ExportTypeComparer etc = new ExportTypeComparer();
		assertTrue(etc.compareAgainstPreviousSnapshotRelease(ExtensionChecker.prevNlSnapshotDir, ExtensionChecker.nlDeltaDir, ExtensionChecker.nlSnapshotDir, 
				ExtensionChecker.nlDate, ExtensionChecker.prevNlDate));
	}
}
