This projects needs the following jars to compile & run:
	- log4j-1.2.17.jar
	- commons-io-2.4.jar
	- commons-lang3-3.1.jar
	- commons-validator-1.4.0.jar
	- opencsv-1.8.jar
	- lingpipe-4.1.0.jar
	- apg.jar
	
These jars can be downloaded from 
	http://logging.apache.org/log4j/1.2/download.html
	http://commons.apache.org/proper/commons-io/download_io.cgi
	http://commons.apache.org/proper/commons-lang/download_lang.cgi
	http://commons.apache.org/proper/commons-validator/download_validator.cgi
	https://sourceforge.net/projects/opencsv/
	http://alias-i.com/lingpipe/web/download.html

apg.jar must be built first; download source and build files from https://github.com/ldthomas/apg-java

Instructions on how to check the RF2-extension can be found in https://service.projectplace.com/pp/pp.cgi/r1031395921