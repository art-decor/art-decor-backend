declare namespace f                 = "http://hl7.org/fhir";
declare namespace saxon             = "http://saxon.sf.net/";
declare namespace array             = "http://www.w3.org/2005/xpath-functions/array";
declare option saxon:output "indent=yes";

declare %private function local:sanitizeOID($oid as xs:string?, $uri as xs:string*) as xs:string? {
    let $oid    := normalize-space($oid)
    let $oid    := replace($oid, '^urn:oid:', '')
    (: STU3 http://hl7.org/fhir/ValueSet/dicom-cid29 has urn:oid:1.2.840.10008.6.&#x200B;1.&#x200B;19 :)
    let $oid    := replace($oid, '&#x200B;', '')
    
    return
        if (matches($oid,'^[0-2](\.(0|[1-9]\d*))*$')) then (
            if ($uri[starts-with(., 'http://hl7.org/fhir/v2/')] and starts-with($oid, '2.16.840.1.133883.18.')) then (
                replace($oid, '^2\.16\.840\.1\.133883\.18\.', '2.16.840.1.113883.18.') (:input error, was fixed after DSTU2 (2015-11-06):)
            ) else (
                $oid
            )
        ) else
        if (starts-with($oid, '.1.113883')) then 
            concat('2.16.840',$oid)
        else ()
};
(:~ Check if input string is an ISO OID
    @param $oid - optional. If empty result is false
    @return true if complies with OID pattern or false otherwise
:)
declare %private function local:isOid($oid as xs:string?) as xs:boolean {
    matches($oid, "^[0-2](\.(0|[1-9][0-9]*))*$")
};

declare %private function local:buildMaps($uriSet as item()*, $fhirVersion as xs:string?, $utg as element()*, $utg-v2-conceptdomains as element(f:CodeSystem)?, $v2Maps as element(map)*) as element()* {
    let $maps           := 
        for $uris in $uriSet
        (: For example 1.2.276.0.76.5.409 appears in plain sight in DSTU2/STU3 NamingSystems and is thus an invalid uri :)
        let $uri        := if (local:isOid($uris)) then concat('urn:oid:',$uris) else $uris
        group by $uri
        order by $uri
        return (
            let $type       := 
                if ($uris/ancestor::f:ValueSet[f:url/@value = $uri]) then 'valueset'
                else
                if ($uris/ancestor::f:CodeSystem[f:url/@value = $uri]) then 'codesystem'
                else
                if ($uris/parent::f:valueSet | $uris/parent::f:import[ancestor::f:compose]) then 'valueset'
                else
                if ($uris/parent::f:system[ancestor::f:compose | ancestor::f:codeSystem]) then 'codesystem'
                else
                if ($uris/../@type[not(. = '')]) then $uris/../@type
                else
                if ($uris/ancestor::f:NamingSystem/f:kind/@value) then $uris/ancestor::f:NamingSystem/f:kind/@value
                else (
                    'identifier'
                )
            (: some uris are used as CodeSystem/url or ValueSet/url while there is also a NamingSystem that claims that a different uri or the oid is the preferred identifier
               For these cases we disregard NamingSystems. We only consider NamingSystem if it is not a code system or value set.
            :)
            let $preferred  := $uris/parent::f:url[parent::f:CodeSystem | parent::f:ValueSet] | $uris[1]/parent::*[@preferred = 'uri']
            let $preferred  := if ($preferred) then $preferred else $uris/ancestor::f:uniqueId[1]/f:preferred[@value = 'true']
            (: 2022-06-10 GTIN is the only relevant system that does not have any preferred indicator :)
            let $preferred  := $uri = 'https://www.gs1.org/gtin' or exists($preferred)
            
            (: https://jira.hl7.org/browse/UP-181 :)
            let $searchuri  :=
                if (starts-with($uri, 'ttp://')) then 'h' || $uri else $uri
            (: HL7 changed its mind a number of times on uris. 
                In R4 it's http://terminology.hl7.org/CodeSystem/v2-0009 and in DSTU2 it's http://hl7.org/fhir/v2/0009
                In R4 it's http://terminology.hl7.org/ValueSet/v2-0389 and in DSTU2 it's http://hl7.org/fhir/ValueSet/v2-0389
                
                ConceptDomains don't have a separate entry anymore in R4, e.g. http://terminology.hl7.org/CodeSystem/v2-0460
                
                In STU3 there 3 typos www.hl7.org instead of hl7.org
                http://www.hl7.org/fhir/contractaction
                http://www.hl7.org/fhir/contractactorrole
                http://www.hl7.org/fhir/contractsignertypecodes
            :)
            let $searchuri  := 
                if ($type = 'codesystem') then 
                    if (matches($searchuri, 'http://hl7.org/fhir/v2/')) then 
                        concat('http://terminology.hl7.org/CodeSystem/v2-', tokenize($searchuri, '/')[last()])
                    else
                    if (matches($searchuri, 'http://hl7.org/fhir/v3/')) then  
                        concat('http://terminology.hl7.org/CodeSystem/v3-', tokenize($searchuri, '/')[last()])
                    else
                    if (matches($searchuri, 'http://(www\.)?hl7.org/fhir/')) then  
                        concat('http://terminology.hl7.org/CodeSystem/', tokenize($searchuri, '/')[last()])
                    else
                        $searchuri
                else
                if ($type = 'valueset') then 
                    if (matches($searchuri, 'http://hl7.org/fhir/ValueSet/')) then
                        concat('http://terminology.hl7.org/ValueSet/', tokenize($searchuri, '/')[last()])
                    else
                        $searchuri
                else (
                    $searchuri
                )
            (: check utg with new uri and old uri. prefer oid from actual object above an oid in a namingsystem for codesystems and valuesets :)
            let $oids       := 
                switch ($type)
                case 'codesystem' return $utg/descendant-or-self::f:CodeSystem[f:url/@value = ($searchuri, $uri)]/f:identifier[f:system/@value='urn:ietf:rfc:3986']/f:value/@value[not(.='n/a' or .='urn:oid:required')]
                case 'valueset'   return $utg/descendant-or-self::f:ValueSet[f:url/@value = ($searchuri, $uri)]/f:identifier[f:system/@value='urn:ietf:rfc:3986']/f:value/@value[not(.='n/a' or .='urn:oid:required')]
                default           return $utg/descendant-or-self::f:NamingSystem[f:uniqueId[f:type/@value='uri']/f:value/@value = ($searchuri, $uri)]/f:uniqueId[f:type/@value='oid']/f:value/@value[not(.='n/a' or .='urn:oid:required')]
            (: conceptdomains have a very awkward place in utg. they are documented in the text of a ValueSet :)
            let $oids       := 
                if (empty($oids)) then
                    switch ($type)
                    case 'codesystem' return $utg-v2-conceptdomains/f:text/*:div/*:table[@class = 'codes'][*:tr[1]/*:td[5][.//text()[. = 'V2 Code System OID']]]/*:tr[*:td[1][.//text()[. = ($searchuri, $uri)]]]/*:td[5]
                    case 'valueset'   return $utg-v2-conceptdomains/f:text/*:div/*:table[@class = 'codes'][*:tr[1]/*:td[7][.//text()[. = 'V2 ValueSet OID']]]/*:tr[*:td[1][.//text()[. = ($searchuri, $uri)]]]/*:td[7]
                    default           return ()
                else (
                    $oids
                )
            (: in DSTU2 there was a ValueSet extension for documenting oids instead of using identifier. There also was no Codesystem yet so we need to check NamingSystem :)
            let $oids       := 
                if (empty($oids)) then 
                    if ($fhirVersion = 'DSTU2') then 
                        switch ($type)
                        case 'codesystem' return $uris/ancestor::f:NamingSystem[f:uniqueId[f:type/@value='uri']/f:value/@value = $uri]/f:uniqueId[f:type/@value='oid']/f:value/@value[not(.='n/a' or .='urn:oid:required')]
                        case 'valueset'   return (
                            $uris/ancestor::f:ValueSet[f:url/@value = $uri]/f:extension[@url="http://hl7.org/fhir/StructureDefinition/valueset-oid"]/f:valueUri/@value,
                            $uris/ancestor::f:NamingSystem[f:uniqueId[f:type/@value='uri']/f:value/@value = $uri]/f:uniqueId[f:type/@value='oid']/f:value/@value[not(.='n/a' or .='urn:oid:required')]
                        )[1]
                        default           return $uris/ancestor::f:NamingSystem[f:uniqueId[f:type/@value='uri']/f:value/@value = $uri]/f:uniqueId[f:type/@value='oid']/f:value/@value[not(.='n/a' or .='urn:oid:required')]
                    else (
                        $uris/ancestor::f:NamingSystem[f:uniqueId[f:type/@value='uri']/f:value/@value = $uri]/f:uniqueId[f:type/@value='oid']/f:value/@value[not(.='n/a' or .='urn:oid:required')]
                    )
                else (
                    $oids
                )
            (: if all else fails assume that the uri is not represented in utg and just check the local codesystem/valueset/namingsystem for an oid :)
            let $oids       :=
                if (empty($oids)) then
                    switch ($type)
                    case 'codesystem' return $uris/ancestor::f:CodeSystem[f:url/@value = $uri]/f:identifier[f:system/@value='urn:ietf:rfc:3986']/f:value/@value[not(.='n/a' or .='urn:oid:required')]
                    case 'valueset'   return $uris/ancestor::f:ValueSet[f:url/@value = $uri]/f:identifier[f:system/@value='urn:ietf:rfc:3986']/f:value/@value[not(.='n/a' or .='urn:oid:required')]
                    default           return $uris/ancestor::f:NamingSystem[f:uniqueId[f:type/@value='uri']/f:value/@value = $uri]/f:uniqueId[f:type/@value='oid']/f:value/@value[not(.='n/a' or .='urn:oid:required')]
                else (
                    $oids
                )
            let $oids       :=
                if (empty($oids)) then (
                    $uris/ancestor::map/@oid
                )
                else (
                    $oids
                )
            (: some URIs are actually the oid starting with 'urn:oid:' :)
            let $oids       :=
                if (empty($oids)) then
                    local:sanitizeOID($uri, $uris)
                else (
                    $oids
                )
            let $check      := 
                if (empty($oids)) then 
                    switch ($uri)
                    case 'http://acme.com/config/fhir/codesystems/cholesterol'
                    case 'http://hl7.org/fhir/CodeSystem/example'
                    case 'http://hl7.org/fhir/CodeSystem/example-supplement'
                    case 'http://hl7.org/fhir/CodeSystem/summary'
                    case 'http://hl7.org/fhir/hacked'
                    case 'http://hl7.org/fhir/ValueSet/example-expansion'
                    case 'http://hl7.org/fhir/ValueSet/example-extensional'
                    case 'http://hl7.org/fhir/ValueSet/example-filter'
                    case 'http://hl7.org/fhir/ValueSet/example-hierarchical'
                    case 'http://hl7.org/fhir/ValueSet/example-inline'
                    case 'http://hl7.org/fhir/ValueSet/example-intensional'
                    case 'http://example.com'
                    case '#hacked' return ()
                    default return 
                        if (empty($fhirVersion[not(. = '')])) then () else (
                            <unmatched>
                            {
                                if (empty($fhirVersion[not(. = '')])) then () else attribute fhirVersion {$fhirVersion},
                                attribute uri {$uri},
                                attribute type {$type}
                            }
                            {
                                let $s := (
                                    $uris/ancestor::f:CodeSystem[f:url[@value = $uri] | f:valueSet[@value = $uri]], 
                                    $uris/ancestor::f:ValueSet[f:url/@value = $uri], 
                                    $uris/ancestor::f:NamingSystem[f:uniqueId[f:type/@value='uri']/f:value/@value = $uri]
                                )
                                let $name := ($s/f:name, $s/f:title)[1]/@value
                                return (
                                    if ($name) then attribute name {$name} else (),
                                    if ($s/f:description) then <description>{data(($s/f:description)[1]/@value)}</description> else ()
                                    ,
                                    let $paths  := 
                                        for $u in $uris
                                        return string-join(
                                            for $n in $u/ancestor::*[not(self::f:Bundle | self::f:entry | self::f:resource)]
                                            return
                                                local-name($n)
                                            , '.'
                                        )
                                    
                                    return
                                    for $p in distinct-values($paths) return 
                                        <source count="{count($paths[. = $p])}">{$p}</source>
                                )
                            }
                            </unmatched>
                        )
                else ()
            return (
                $check,
                (: e.g. http://hl7.org/fhir/ValueSet/daf-medication-codes for example has multiple oids attached :)
                for $oid in $oids
                group by $oid
                return (
                    if (empty(local:sanitizeOID($oid, $uri))) then
                        error(xs:QName('f:NOT_AN_OID'), $fhirVersion || ' URI ' || $uri || ' type ' || $type || ' points to incorrectly formatted oid: ' || $oid)
                    else
                    (: some uris are actually an oid. this makes for a non-sensical self mapping. skip :)
                    if ($oid = local:sanitizeOID($uri, $uri)) then
                        ()
                    else
                    (: '2.16.840.1.113883.6.' is a codesystem, not a valueset. This is an issue for R4 http://hl7.org/fhir/ValueSet/genenames for example :)
                    if (starts-with(local:sanitizeOID($oid, $uri), '2.16.840.1.113883.6.') and $type = 'valueset') then
                        ()
                    else (
                        <map>
                        {
                            if (empty($fhirVersion[not(. = '')])) then () else attribute fhirVersion {$fhirVersion},
                            attribute oid {local:sanitizeOID($oid, $uri)},
                            attribute uri {$uri},
                            attribute type {$type[1]},
                            attribute preferred {$preferred},
                            $v2Maps[@oid = local:sanitizeOID($oid, $uri)]/@hl70396
                        }
                        {
                            let $contents :=
                                for $ns in $uris/ancestor::f:NamingSystem[not(f:date) or f:date/xs:date(substring(@value, 1, 10)) = max($uris/ancestor::f:NamingSystem/f:date/xs:date(substring(@value, 1, 10)))] 
                                return local:sanitizeNamingSystem($ns, $v2Maps[@oid = local:sanitizeOID($oid, $uri)]/@hl70396)
                            let $contents :=
                                if ($contents) then $contents else
                                    for $vs in $uris/parent::f:url/parent::f:ValueSet[not(f:date) or f:date/xs:date(substring(@value, 1, 10)) = max($uris/parent::f:url/parent::f:ValueSet/f:date/xs:date(substring(@value, 1, 10)))] 
                                    return local:buildNamingSystemFromValueSet($vs, local:sanitizeOID($oid, $uri), $v2Maps)
                            let $contents :=
                                if ($contents) then $contents else
                                    for $vs in $uris/parent::f:url/parent::f:CodeSystem[not(f:date) or f:date/xs:date(substring(@value, 1, 10)) = max($uris/parent::f:url/parent::f:CodeSystem/f:date/xs:date(substring(@value, 1, 10)))] 
                                    return local:buildNamingSystemFromValueSet($vs, local:sanitizeOID($oid, $uri), $v2Maps)
                            let $contents :=
                                if ($contents) then $contents else
                                    for $mp in $uris/parent::map 
                                    return local:buildNamingSystem($mp, $fhirVersion, $v2Maps)/f:NamingSystem
                            
                            return 
                                (if (count($contents) gt 1) then comment {' duplicate '} else (), $contents)
                        }
                        </map>
                    )
                )
            )
        )
    let $check    :=
        for $oid in $maps/@oid
        let $oidmappings    := $maps[@oid = $oid]
        return
            if (count($oidmappings) = 1) then () else (
                error(xs:QName('f:OID_AMBIGUOUS'), $fhirVersion || ' Multiple mappings for oid ' || $oid || '. Found:' || string-join($oidmappings/@uri, ' '))
            )
        
    return 
        $maps
};

(: https://hl7-fhir.github.io/valueset-namingsystem-type.html
Code	Display	Definition
codesystem	Code System	The naming system is used to define concepts and symbols to represent those concepts; e.g. UCUM, LOINC, NDC code, local lab codes, etc.
identifier	Identifier	The naming system is used to manage identifiers (e.g. license numbers, order numbers, etc.).
root	Root	The naming system is used as the root for other identifiers and naming systems.
:)
declare %private function local:buildNamingSystem($map as element(map)?, $fhirVersion as xs:string?, $v2Maps as element(map)*) as element(map)? {
if ($map) then 
    let $id                 := $map/@id
    let $oid                := $map/@oid
    let $uri                := $map/@uri
    (: HL7 V2 Table 0396 Code for Code Systems :)
    let $hl70396            := $map/@hl70396
    let $type               := $map/@type
    let $preferred          := $map/@preferred
    let $displayName        := $map/@displayName
    let $publisher          := $map/@publisher
    let $publicationDate    := $map/@approvalDate
    return
    <map>
    { 
        if (empty($fhirVersion) or $fhirVersion = '') then () else attribute fhirVersion {$fhirVersion},
        attribute oid {$oid},
        attribute uri {$uri},
        attribute type {$type},
        attribute preferred {$preferred}
    }
        <NamingSystem xmlns="http://hl7.org/fhir">
            <id value="{$id}"/>
            <name value="{$displayName}"/>
            <status value="draft"/>
            <kind value="{$type}"/>
            <publisher value="{$publisher}"/>
            <date value="{$publicationDate}"/>
            <description value="{$displayName}"/>
            <uniqueId>
                <type value="uri"/>
                <value value="{$uri}"/>
                <preferred value="{$preferred = 'uri'}"/>
            </uniqueId>
            <uniqueId>
                <type value="oid"/>
                <value value="{$oid}"/>
                <preferred value="{not($preferred = 'uri')}"/>
            </uniqueId>
        {
            if ($hl70396) then
                <uniqueId>
                    <type value="other"/>
                    <value value="{$hl70396}"/>
                    <preferred value="{not($preferred = 'hl70396')}"/>
                    <comment value="HL7 V2 Table 0396 Code"/>
                </uniqueId>
            else ()
        }
        </NamingSystem>
    </map>
else ()
};
declare %private function local:buildNamingSystemFromValueSet($valueSet as element()?, $oid as xs:string, $v2Maps as element(map)*) as element(f:NamingSystem)? {
if ($valueSet) then 
    <NamingSystem xmlns="http://hl7.org/fhir">
    {
        comment { ' generated from ' || local-name($valueSet) || ' ' },
        $valueSet/f:id,
        $valueSet/f:extension[@url="http://hl7.org/fhir/StructureDefinition/structuredefinition-ballot-status"],
        $valueSet/f:extension[@url="http://hl7.org/fhir/StructureDefinition/structuredefinition-fmm"],
        $valueSet/f:extension[@url="http://hl7.org/fhir/StructureDefinition/structuredefinition-wg"],
        $valueSet/f:name,
        $valueSet/f:status,
        <kind value="{lower-case(local-name($valueSet))}"/>,
        $valueSet/f:date,
        $valueSet/f:publisher,
        $valueSet/f:contact,
        $valueSet/f:description,
        <uniqueId>
            <type value="uri"/>
            <value value="{$valueSet/f:url/@value}"/>
            <preferred value="true"/>
        </uniqueId>
        ,
        <uniqueId>
            <type value="oid"/>
            <value value="{$oid}"/>
            <preferred value="false"/>
        </uniqueId>
        ,
        let $hl70396 := $v2Maps[@oid = $oid]/@hl70396
        return
        if ($hl70396) then
            <uniqueId>
                <type value="other"/>
                <value value="{$hl70396}"/>
                <preferred value="false"/>
                <comment value="HL7 V2 Table 0396 Code"/>
            </uniqueId>
        else ()
    }
    </NamingSystem>
else ()
};
declare %private function local:sanitizeNamingSystem($namingSystem as element(f:NamingSystem)?, $hl70396 as xs:string?) as element(f:NamingSystem)? {
if ($namingSystem) then 
    <NamingSystem xmlns="http://hl7.org/fhir">
    {
        for $node in $namingSystem/*
        return
            if ($node[self::f:uniqueId][f:type/@value = 'oid']) then 
                <uniqueId>
                {
                    for $subnode in $node/*
                    return
                        if ($subnode[self::f:value]) then
                            <value value="{local:sanitizeOID($subnode/@value, $namingSystem/f:uniqueId[f:type/@value = 'uri']/f:value/@value)}"/>
                        else
                            $subnode
                }
                </uniqueId>
            else 
                $node
        ,
        if ($hl70396) then
            <uniqueId>
                <type value="other"/>
                <value value="{$hl70396}"/>
                <preferred value="false"/>
                <comment value="HL7 V2 Table 0396 Code"/>
            </uniqueId>
        else ()
    }
    </NamingSystem>
else ()
};

(:let $utg-col-STU3                       := doc('utg-STU3.xml')/f:Bundle:)
let $utg-col-R4                         := doc('utg-R4.xml')/f:Bundle

let $v2Maps     := 
    for $concept in $utg-col-R4//f:CodeSystem[f:url/@value = 'http://terminology.hl7.org/CodeSystem/v2-tables']/f:concept[f:property[f:code/@value = 'v2-table-oid'][f:valueString/@value]]
    return
        <map oid="{$concept/f:property[f:code/@value = 'v2-table-oid']/f:valueString/@value}" hl70396="HL7{$concept/f:code/@value}" preferred="uri" type="codesystem" displayName="{$concept/f:display/@value}"/>

let $v2Maps     := array:flatten(
      $v2Maps
    | <map oid="2.16.840.1.113883.5.4"    hl70396="ACTCODE" preferred="uri" type="codesystem" displayName="HL7 ActCode"/>
    | <map oid="2.16.840.1.113883.5.110"  hl70396="ROLECLASS" preferred="uri" type="codesystem" displayName="HL7 RoleClass"/>
    | <map oid="2.16.840.1.113883.5.111"  hl70396="ROLECODE" preferred="uri" type="codesystem" displayName="HL7 RoleCode"/>
    | <map oid="2.16.840.1.113883.5.1008" hl70396="NULLFL" preferred="uri" type="codesystem" displayName="SNOMED CT"/>
    | <map oid="2.16.840.1.113883.6.1"    hl70396="LN" preferred="uri" type="codesystem" displayName="LOINC"/>
    | <map oid="2.16.840.1.113883.6.8"    hl70396="UCUM" preferred="uri" type="codesystem" displayName="UCUM"/>
    | <map oid="2.16.840.1.113883.6.15"   hl70396="NIC" preferred="uri" type="codesystem" displayName="NIC"/>
    | <map oid="2.16.840.1.113883.6.20"   hl70396="NDA" preferred="uri" type="codesystem" displayName="North American Nursing Diagnosis Association"/>
    | <map oid="2.16.840.1.113883.6.43.1" hl70396="ICDO3" preferred="uri" type="codesystem" displayName="International Classification of Disease for Oncology Third Edition"/>
    | <map oid="2.16.840.1.113883.6.73"   hl70396="WC" preferred="uri" type="codesystem" displayName="WHO ATC"/>
    | <map oid="2.16.840.1.113883.6.96"   hl70396="SCT" preferred="uri" type="codesystem" displayName="SNOMED CT"/>
    | <map oid="1.0.3166.1.2.2"           hl70396="ISO3166_1" preferred="uri" type="codesystem" displayName="ISO 3166 Part 1 Country Codes, 2nd Edition, Alpha-2"/>
    | <map oid="1.0.4217"                 hl70396="ISO4217" preferred="uri" type="codesystem" displayName="ISO 4217 currency code"/>
    | <map oid="2.16.840.1.113883.6.3.2"  hl70396="ICD10" preferred="uri" type="codesystem" displayName="ICD 10 NL"/>
)

let $extraMaps  :=
      <map id="dsm5" oid="2.16.840.1.113883.6.344" uri="http://hl7.org/fhir/sid/dsm5" preferred="uri" type="codesystem" displayName="DSM 5" publisher="American Psychiatric Association (APA)" creationDate="2018-02-01" approvalDate="2018-02-01"/>
    | <map id="icd9-cm-diagnosis" oid="2.16.840.1.113883.6.103" uri="http://hl7.org/fhir/sid/icd-9-cm/diagnosis" preferred="uri" type="codesystem" displayName="ICD-9 CM Diagnosis" publisher="National Center for Health Statistics"/>
    | <map id="icd9-cm-procedure" oid="2.16.840.1.113883.6.104" uri="http://hl7.org/fhir/sid/icd-9-cm/procedure" preferred="uri" type="codesystem" displayName="ICD-9 CM Procedure" publisher="National Center for Health Statistics"/>
    | <map id="icd10-de" oid="1.2.276.0.76.5.409" uri="http://hl7.org/fhir/sid/icd-10-de" preferred="uri" type="codesystem" displayName="ICD-10 International (WHO) (German Variant)" publisher="DIMDI"/>
    | <map id="icf-nl" oid="2.16.840.1.113883.6.254" uri="http://hl7.org/fhir/sid/icf-nl" preferred="uri" type="codesystem" displayName="ICF NL" publisher="WHO" creationDate="2007-02-20" approvalDate="2010-11-16"/>
    | <map id="icpc-1-nl" oid="2.16.840.1.113883.2.4.4.31.1" uri="http://hl7.org/fhir/sid/icpc-1-nl" preferred="uri" type="codesystem" displayName="ICPC-1 NL" publisher="NHG" creationDate="2010-01-01" approvalDate="2010-01-01"/>
    | <map id="HealthcareProviderTaxonomyHIPAA" oid="2.16.840.1.113883.6.101" uri="http://nucc.org/provider-taxonomy" preferred="uri" type="codesystem" displayName="NUCC Provider Taxonomy" publisher="HL7 International" creationDate="2017-04-19" approvalDate="2017-04-19"/>
    | <map id="nubc-patDiscStatus-cs" oid="2.16.840.1.113883.6.301.5" uri="http://www.nubc.org/patient-discharge" preferred="uri" type="codesystem" displayName="NUBC Patient Discharge Status" publisher="National Uniform Billing Committee" creationDate="2013-04-09" approvalDate="2013-04-10"/>
    | <map id="cdt-ADAcodes" oid="2.16.840.1.113883.6.13" uri="http://ada.org/cdt" preferred="uri" type="codesystem" displayName="CDT for dental procedures" publisher="ADA" creationDate="2009-10-14" approvalDate="2009-10-14"/>
    | <map id="ada-snodent" oid="2.16.840.1.113883.3.3150" uri="http://ada.org/snodent" preferred="uri" type="codesystem" displayName="SNODENT for dental conditions, findings, disorders. Used for claims and clinical care" publisher="ADA" creationDate="2013-05-10" approvalDate="2013-05-10"/>
    | <map id="cosmic" oid="2.16.840.1.113883.3.912" uri="http://cancer.sanger.ac.uk/cancergenome/projects/cosmic" preferred="uri" type="codesystem" displayName="COSMIC : Catalogue Of Somatic Mutations In Cancer" publisher="COSMIS" creationDate="2011-05-22" approvalDate="2011-06-06"/>
    | <map id="fipsPUB6-4" oid="2.16.840.1.113883.6.93" uri="https://www.census.gov/geo/reference/" preferred="uri" type="codesystem" displayName="FIPS PUB 6-4 COUNTY CODES" publisher="NIST"/>
    | <map id="clinicaltrials-gov" oid="2.16.840.1.113883.3.1077" uri="http://clinicaltrials.gov" preferred="uri" type="codesystem" displayName="ClinicalTrials.gov" publisher="US NLM" creationDate="2011-07-26" approvalDate="2011-07-26"/>
    | <map id="UNII" oid="2.16.840.1.113883.4.9" uri="http://fdasis.nlm.nih.gov" preferred="uri" type="codesystem" displayName="Unique Ingredient Identifier (UNII)" publisher="US FDA" creationDate="2005-09-12" approvalDate="2005-09-12"/>
    | <map id="NDFRT" oid="2.16.840.1.113883.6.209" uri="http://hl7.org/fhir/ndfrt" preferred="uri" type="codesystem" displayName="NDF-RT (National Drug File – Reference Terminology)" publisher="NLM" creationDate="2005-01-25" approvalDate="2005-01-25"/>
    | <map id="icd-9-cm" oid="2.16.840.1.113883.6.42" uri="http://hl7.org/fhir/sid/icd-9-cm" preferred="uri" type="codesystem" displayName="ICD-9 Clinical Modification" publisher="WHO" creationDate="2019-03-20" approvalDate="2019-03-20"/>
    | <map id="loincAHRQMedication" oid="1.3.6.1.4.1.12009.10.1.1714" uri="http://loinc.org/vs/LL2654-3" preferred="uri" type="valueset" displayName="AHRQ Medication Q1" publisher="Regenstrief"/>    
    (:| <map id="CVX" oid="2.16.840.1.113883.6.59" uri="http://terminology.hl7.org/CodeSystem/CVX" preferred="uri" type="codesystem" displayName="Vaccines administered (CVX)" publisher="US CDC" creationDate="1999-12-30" approvalDate="2013-01-22"/>:)
    (:| <map id="ICD10dut" oid="2.16.840.1.113883.6.3.2" uri="http://terminology.hl7.org/CodeSystem/ICD10dut" preferred="uri" type="codesystem" displayName="ICD10 NL" publisher="RIVM" creationDate="2005-01-25" approvalDate="2009-10-07"/>:)
    (:| <map id="atc" oid="2.16.840.1.113883.6.73" uri="http://terminology.hl7.org/CodeSystem/atc" preferred="uri" type="codesystem" displayName="ATC, Anatomical Therapeutic Chemical classification" publisher="WHO" creationDate="1999-12-30" approvalDate="2011-02-30"/>:)
    | <map id="gtin" oid="1.3.160" uri="https://www.gs1.org/gtin" preferred="uri" type="identifier" displayName="GTIN Global Trade Item Number" publisher="GS1" creationDate="2007-06-27" approvalDate="2009-12-23"/>
    | <map id="gtin" oid="1.3.160" uri="http://terminology.hl7.org/CodeSystem/gtin" preferred="otheruri" type="identifier" displayName="GTIN Global Trade Item Number" publisher="GS1" creationDate="2007-06-27" approvalDate="2009-12-23"/>
    | <map id="nanda" oid="2.16.840.1.113883.6.20" uri="http://www.nanda.org/" preferred="uri" type="codesystem" displayName="NANDA" publisher="North American Nursing Diagnosis Association" creationDate="" approvalDate=""/>
    | <map id="nic" oid="2.16.840.1.113883.6.15" uri="https://nursing.uiowa.edu/cncce/nursing-interventions-classification-overview" preferred="uri" type="codesystem" displayName="NIC" publisher="University of Iowa" creationDate="" approvalDate=""/>
    | <map id="hgvs" oid="2.16.840.1.113883.6.282" uri="http://varnomen.hgvs.org/" preferred="uri" type="codesystem" displayName="Sequence Variant Nomenclature" publisher="HGNC" creationDate="2009-05-21" approvalDate="2009-05-27"/>
    | <map id="icd10PCS" oid="2.16.840.1.113883.6.4" uri="http://www.cms.gov/Medicare/Coding/ICD10/index.html" preferred="uri" type="codesystem" displayName="ICD10 Procedure Codes" publisher="Medicare"/>
    | <map id="hlaAllele" oid="2.16.840.1.113883.6.341" uri="http://www.ebi.ac.uk/ipd/imgt/hla" preferred="uri" type="codesystem" displayName="Human leukocyte antigen nomenclature" publisher="National Center for Biomedical Communications" creationDate="2017-07-28" approvalDate="2017-07-28"/>
    | <map id="ensembl-G" oid="2.16.840.1.113883.6.324" uri="http://www.ensembl.org" preferred="uri" type="codesystem" displayName="ENSEMBL reference sequence identifiers"/>
    | <map id="hgnc" oid="2.16.840.1.113883.6.281" uri="http://www.genenames.org" preferred="uri" type="codesystem" displayName="HUGO Gene Nomenclature" publisher="HGNC" creationDate="2009-05-21" approvalDate="2009-05-27"/>
    | <map id="pubmed" oid="2.16.840.1.113883.13.191" uri="http://www.ncbi.nlm.nih.gov/pubmed" preferred="uri" type="codesystem" displayName="PubMed" publisher="National Center for Biomedical Communications" creationDate="2011-12-01" approvalDate="2011-12-09"/>
    | <map id="refSeq" oid="2.16.840.1.113883.6.280" uri="http://www.ncbi.nlm.nih.gov/refseq" preferred="uri" type="codesystem" displayName="NCBI Reference Sequence (RefSeq)" publisher="National Center for Biotechnology Information" creationDate="2009-05-21" approvalDate="2009-05-27"/>
    | <map id="MIM" oid="2.16.840.1.113883.6.174" uri="http://www.omim.org" preferred="uri" type="codesystem" displayName="Online Mendelian Inheritance in Man 1993" publisher="Johns Hopkins University" creationDate="2005-01-25" approvalDate="2005-01-25"/>
    | <map id="pharmgkb" oid="2.16.840.1.113883.3.913" uri="http://www.pharmgkb.org" preferred="uri" type="codesystem" displayName="PHARMGKB : Pharmacogenomic Knowledge Base" publisher="HHS" creationDate="2021-03-18" approvalDate="2021-03-18"/>
    | <map id="radLex" oid="2.16.840.1.113883.6.256" uri="http://www.radlex.org" preferred="uri" type="codesystem" displayName="RadLex" publisher="RSNA" creationDate="2007-04-18" approvalDate="2009-12-30"/>
    (:| <map id="whoATCLevel1" oid="2.16.840.1.113883.6.73" uri="http://www.whocc.no/atc" preferred="uri" type="codesystem" displayName="Anatomical Therapeutic Chemical Classification System (WHO)" publisher="WHO" creationDate="2011-02-23" approvalDate="2011-03-31"/>:)
    | <map id="uspostalcodes" oid="2.16.840.1.113883.6.231" uri="https://www.usps.com/" preferred="uri" type="codesystem" displayName="US Two Letter Alphabetic Zip Codes" publisher="US Postal Services" creationDate="2005-03-03" approvalDate="2015-12-31"/>
    | <map id="dicomuid" oid="1.2.840.10008.2.6.1" uri="urn:dicom:uid" preferred="uri" type="identifier" displayName="DICOM unique ID" publisher="NEMA"/>
    | <map id="mime" oid="2.16.840.1.113883.6.10" uri="urn:ietf:bcp:13" preferred="uri" type="codesystem" displayName="IETF MIME" publisher="IANA"/>
    | <map id="rfc5646" oid="2.16.840.1.113883.6.316" uri="urn:ietf:bcp:47" preferred="uri" type="codesystem" displayName="RFC5646 Tags for Identifying Languages" publisher="IETF" creationDate="2014-11-10" approvalDate="2014-11-12"/>
    | <map id="uri" oid="2.16.840.1.113883.4.873" uri="urn:ietf:rfc:3986" preferred="uri" type="identifier" displayName="OID for URIs created subject to IETF rules" publisher="IETF"/>
    | <map id="iso3166-1edition2alpha2" oid="1.0.3166.1.2.2" uri="urn:iso:std:iso:3166" preferred="uri" type="codesystem" displayName="ISO 3166 Part 1 Country Codes, 2nd Edition, Alpha-2" publisher="ISO" creationDate="2008-03-31" approvalDate="2008-03-31"/>
    | <map id="iso4217-HL7" oid="1.0.4217" uri="urn:iso:std:iso:4217" preferred="uri" type="codesystem" displayName="ISO 4217 currency code" publisher="ISO" creationDate="2009-10-14" approvalDate="2009-10-14"/>
    
let $namingsystem-terminologies-DSTU2   := doc('namingsystem-terminologies-DSTU2.xml')/f:Bundle
let $namingsystem-identifiers-DSTU2     := doc('namingsystem-registry-DSTU2.xml')/f:Bundle
let $namingsystem-valuesets-DSTU2       := doc('valuesets-v2-DSTU2.xml')/f:Bundle | doc('valuesets-v3-DSTU2.xml')/f:Bundle | doc('valuesets-fhir-DSTU2.xml')/f:Bundle

let $namingsystem-terminologies-STU3    := doc('namingsystem-terminologies-STU3.xml')/f:Bundle
let $namingsystem-identifiers-STU3      := doc('namingsystem-registry-STU3.xml')/f:Bundle
let $namingsystem-valuesets-STU3        := doc('valuesets-v2-STU3.xml')/f:Bundle | doc('valuesets-v3-STU3.xml')/f:Bundle | doc('valuesets-fhir-STU3.xml')/f:Bundle

let $namingsystem-terminologies-R4      := doc('namingsystem-terminologies-R4.xml')/f:Bundle
let $namingsystem-identifiers-R4        := doc('namingsystem-registry-R4.xml')/f:Bundle
let $namingsystem-valuesets-R4          := doc('valuesets-v2-R4.xml')/f:Bundle | doc('valuesets-v3-R4.xml')/f:Bundle | doc('valuesets-fhir-R4.xml')/f:Bundle

let $uri-DSTU2                          := $namingsystem-terminologies-DSTU2//f:NamingSystem/f:uniqueId[f:type/@value='uri']/f:value/@value | 
                                           $namingsystem-identifiers-DSTU2//f:NamingSystem/f:uniqueId[f:type/@value='uri']/f:value/@value |
                                           $namingsystem-valuesets-DSTU2//f:ValueSet/f:url/@value |
                                           $namingsystem-valuesets-DSTU2//f:ValueSet/f:compose/f:import/@value |
                                           $namingsystem-valuesets-DSTU2//f:ValueSet/f:compose//f:system/@value |
                                           $extraMaps/@uri

let $uri-STU3                           := $namingsystem-valuesets-STU3//f:CodeSystem/f:url/@value | 
                                           $namingsystem-valuesets-STU3//f:CodeSystem/f:valueSet/@value | 
                                           $namingsystem-valuesets-STU3//f:ValueSet/f:url/@value |
                                           $namingsystem-valuesets-STU3//f:ValueSet/f:compose//f:system/@value |
                                           $namingsystem-valuesets-STU3//f:ValueSet/f:compose//f:valueSet/@value |
                                           $namingsystem-terminologies-STU3//f:NamingSystem/f:uniqueId[f:type/@value='uri']/f:value/@value |
                                           $namingsystem-identifiers-STU3//f:NamingSystem/f:uniqueId[f:type/@value='uri']/f:value/@value
                                           (:$utg-col-STU3//f:CodeSystem/f:url/@value | 
                                           $utg-col-STU3//f:CodeSystem/f:valueSet/@value | 
                                           $utg-col-STU3//f:ValueSet/f:url/@value |
                                           $utg-col-STU3//f:ValueSet/f:compose//f:system/@value |
                                           $utg-col-STU3//f:NamingSystem/f:uniqueId[f:type/@value='uri']/f:value/@value |:)

let $uri-STU3                           := $uri-STU3 | $extraMaps/@uri[not(. = $uri-STU3)]

let $uri-R4                             := $namingsystem-valuesets-R4//f:CodeSystem/f:url/@value | 
                                           $namingsystem-valuesets-R4//f:CodeSystem/f:valueSet/@value | 
                                           $namingsystem-valuesets-R4//f:ValueSet/f:url/@value |
                                           $namingsystem-valuesets-R4//f:ValueSet/f:compose//f:system/@value |
                                           $namingsystem-valuesets-R4//f:ValueSet/f:compose//f:valueSet/@value |
                                           $namingsystem-terminologies-R4//f:NamingSystem/f:uniqueId[f:type/@value='uri']/f:value/@value |
                                           $namingsystem-identifiers-R4//f:NamingSystem/f:uniqueId[f:type/@value='uri']/f:value/@value
                                           (:$utg-col-R4//f:CodeSystem/f:url/@value | 
                                           $utg-col-R4//f:CodeSystem/f:valueSet/@value | 
                                           $utg-col-R4//f:ValueSet/f:url/@value |
                                           $utg-col-R4//f:ValueSet/f:compose//f:system/@value |
                                           $utg-col-R4//f:NamingSystem/f:uniqueId[f:type/@value='uri']/f:value/@value |:)

let $uri-R4                             := $uri-R4 | $extraMaps/@uri[not(. = $uri-R4)]

let $utg-v2-conceptdomains              := $utg-col-R4//f:CodeSystem[f:url/@value = 'http://terminology.hl7.org/CodeSystem/v2-tables']

(: an oid in STU3 could be wrong and we should assume every new version of FHIR is better in that respect. Not everything is represented in UTG. 
    So for any CodeSystem|ValueSet|NamingSystem not represented in UTG we add that to our lookup list :)
let $utg-current                        := $utg-col-R4 | $uri-R4[not(. = (
                                              $utg-col-R4//f:CodeSystem/f:url/@value | 
                                              $utg-col-R4//f:CodeSystem/f:valueSet/@value | 
                                              $utg-col-R4//f:ValueSet/f:url/@value |
                                              $utg-col-R4//f:ValueSet/f:compose//f:system/@value |
                                              $utg-col-R4//f:ValueSet/f:compose//f:valueSet/@value |
                                              $utg-col-R4//f:NamingSystem/f:uniqueId[f:type/@value='uri']/f:value/@value |
                                              $utg-col-R4//f:NamingSystem/f:uniqueId[f:type/@value='uri']/f:value/@value
                                           ))]/(ancestor::f:CodeSystem | ancestor::f:ValueSet | ancestor::f:NamingSystem)

return
    <x>
    {
        local:buildMaps($uri-DSTU2[not( contains(., 'http://hl7.org/fhir/ValueSet/daf-') or 
                                        contains(., 'http://hl7.org/fhir/ValueSet/qicore-') or
                                        contains(., 'http://hl7.org/fhir/ValueSet/uslab-') or
                                        contains(., 'http://hl7.org/fhir/ValueSet/sdc-') or
                                        contains(., 'http://hl7.org/fhir/ValueSet/sdcde-'))], 'DSTU2', $utg-current, $utg-v2-conceptdomains, ())
    }
    {
        local:buildMaps($uri-STU3, 'STU3', $utg-current, $utg-v2-conceptdomains, ())
    }
    {
        local:buildMaps($uri-R4, 'R4', $utg-current, $utg-v2-conceptdomains, ())
    }
    {
        local:buildMaps($uri-R4, (), $utg-current, $utg-v2-conceptdomains, $v2Maps)
    }
    {
        $v2Maps
    }
    </x>