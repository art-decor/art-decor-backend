xquery version "3.1";

declare namespace transform = "http://exist-db.org/xquery/transform";
declare namespace fn        = "http://www.w3.org/2005/xpath-functions";
declare variable $f external;

declare function local:getMapAsXml($request-body as item()?, $root as xs:string?, $namespace as xs:string?) as element()* {
    let $root       := ($request-body[self::fn:map]/fn:string[@key = 'resourceType'], $root)[1]

    let $contents   := (
        for $elm in $request-body/((fn:string | fn:boolean | fn:number | fn:null) except fn:*[@key = 'resourceType'])
        return
            local:getItemAsXml($elm, $root, $namespace)
        ,
        for $elm in $request-body/((fn:array | fn:map) except fn:*[@key = '#text'])
        return
            local:getMapAsXml($elm, $root, $namespace)
        ,
        for $elm in $request-body/fn:*[@key = '#text']
        return
            if ($elm[self::fn:array | self::fn:map]) then local:getMapAsXml($elm, $root, $namespace) else local:getItemAsXml($elm, $root, $namespace)
    )
    
    return
    if ($request-body[self::fn:array]) then $contents
    else
    if ($request-body[self::fn:map[@key = 'resource']][fn:string[@key = 'resourceType']]) then
        element {QName($namespace, $request-body/@key)} {
            element {QName($namespace, $root)} {
                $contents
            }
        }
    else
    if (count($request-body/ancestor::*) = 0) then
        if ($request-body[empty(fn:map[@key]) or count(*) gt 1]) then
            element {QName($namespace, $root)} {
                $contents
            }
        else (
            $contents
        )
    else (
        element {QName($namespace, $request-body/(parent::fn:array/@key, @key))} {
            if ($request-body/(parent::fn:array/@key, @key) = 'extension') then
                attribute url {data($request-body/fn:string[@key = 'url'])}
            else (),
            $contents
        }
    )
};
declare function local:getItemAsXml($request-body as item()?, $root as xs:string?, $namespace as xs:string?) as item()? {
    let $elmname := ($request-body/@key, $request-body/parent::fn:array/@key)[1]
    
    return
        if ($request-body[@key = 'url']/parent::fn:map/parent::fn:array[@key = 'extension']) then
            ()(:attribute url {data($request-body)}:)
        else
        if ($request-body[@key = 'div']/parent::fn:map[@key = 'text']/../fn:string[@key = 'resourceType']) then
            fn:parse-xml('&lt;div&gt;' || data($request-body) || '&lt;/div&gt;')/div/node()
        else
            element {QName($namespace, $elmname)} {
                attribute value {data($request-body)}
            }
};

declare function local:getBodyAsXml($request-body as item()?, $root as xs:string?, $namespace as xs:string?) as item()? {
    if (empty($request-body)) then () else (
        typeswitch ($request-body)
        case element() return $request-body
        case document-node() return $request-body/*
        default return (
            let $map := fn:serialize($request-body, map{'method':'json', 'indent': true(), 'json-ignore-whitespace-text-nodes':'yes'}) => json-to-xml()
            
            return (:$map:)local:getMapAsXml($map/*, ($map/fn:map/fn:string[@key = 'resourceType'], 'UnknownRoot')[1], 'http://hl7.org/fhir')
        )
    )
};

(:let $f    := 'utg-terminology-stu3/Bundle-changed.json'

return:)
local:getBodyAsXml(json-doc($f), (), ())