<?xml version="1.0" encoding="UTF-8"?>
<!--
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" 
    xmlns:local="urn:local"
    exclude-result-prefixes="#all" version="2.0">
    
    <xsl:output indent="yes" omit-xml-declaration="yes"/>
    
    <xsl:param name="registryFileName" select="'hl7org-oids.xml'"/>
    <xsl:param name="registryFileNameCurrent" select="'hl7org-oids-current.xml'"/>
    <xsl:param name="oidUriMapping" select="'uri-2-oid.xml'"/>
    
    <xsl:variable name="docoid" as="element(oid)*">
        <xsl:choose>
            <xsl:when test="doc-available($registryFileName)">
                <xsl:copy-of select="doc($registryFileName)//oid"/>
            </xsl:when>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="docoidcurrent" as="element(oid)*">
        <xsl:choose>
            <xsl:when test="doc-available($registryFileNameCurrent)">
                <xsl:copy-of select="doc($registryFileNameCurrent)//oid"/>
            </xsl:when>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="docuri" as="element(map)*">
        <xsl:choose>
            <xsl:when test="doc-available($oidUriMapping)">
                <xsl:copy-of select="doc($oidUriMapping)//map"/>
            </xsl:when>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="input" select="."/>
    
    <xsl:template match="/">
        <xsl:message>
            <xsl:text>    OID Export contains              </xsl:text>
            <xsl:value-of select="count(distinct-values(*/OID_root/Comp_OID))"/>
            <xsl:text> unique entries</xsl:text>
        </xsl:message>
        <xsl:message>
            <xsl:text>    OID / URI mapping contains       </xsl:text>
            <xsl:value-of select="count(distinct-values($docuri/@oid))"/>
            <xsl:text> unique entries</xsl:text>
        </xsl:message>
        <xsl:message>
            <xsl:text>    OID Registry contains            </xsl:text>
            <xsl:value-of select="count(distinct-values($docoid/dotNotation/@value))"/>
            <xsl:text> unique entries</xsl:text>
        </xsl:message>
        <xsl:for-each-group select="$docuri" group-by="@oid">
            <xsl:if test="not(current-grouping-key() = $docoid/dotNotation/@value)">
                <xsl:message>    *** <xsl:value-of select="current-grouping-key()"/> (<xsl:value-of select="distinct-values(current-group()/@uri)"/>) from OID/URI mapping table not found in HL7 OID Registry.</xsl:message>
            </xsl:if>
        </xsl:for-each-group>
        <xsl:message>
            <xsl:text>    Previous OID Registry contains   </xsl:text>
            <xsl:value-of select="count(distinct-values($docoidcurrent/dotNotation/@value))"/>
            <xsl:text> unique entries</xsl:text>
        </xsl:message>
        
        <!-- oids that don't have an additionalPorperty with a FHIR uri at all, or those that carry something other our Dutch oid that should never have been in there in the first place -->
        <xsl:variable name="oids-missing-in-new" select="$docoidcurrent[not(additionalProperty[attribute[starts-with(@value, 'HL7-FHIR')]]) or not(additionalProperty[attribute[starts-with(@value, 'HL7-FHIR')]][value[starts-with(@value, 'http://nictiz.nl') or starts-with(@value, 'http://fhir.nl') or starts-with(@value, 'https://referentiemodel.nhg.org')]])]/dotNotation[not(@value = $docoid/dotNotation/@value)]" as="element()*"/>
        
        <xsl:variable name="calc" as="element()*">
            <xsl:for-each select="$oids-missing-in-new/..">
                <xsl:variable name="s" select="."/>
                <xsl:variable name="oldoid" select="dotNotation/@value"/>
                <xsl:variable name="fhirUri" select="additionalProperty[attribute[starts-with(@value, 'HL7-FHIR')]]/value/@value" as="item()*"/>
                
                <xsl:for-each select="distinct-values($fhirUri)">
                    <xsl:variable name="uri" select="."/>
                    <xsl:variable name="newoid" select="$docoid[additionalProperty[attribute[starts-with(@value, 'HL7-FHIR')]][value[@value = $uri]]]/dotNotation/@value"/>
                    
                    <xsl:element name="{if ($newoid) then 'different' else 'missing'}">
                        <xsl:attribute name="oldoid" select="$oldoid"/>
                        <xsl:attribute name="uri" select="."/>
                        <xsl:attribute name="fhirVersion" select="string-join($fhirUri[. = $uri]/ancestor::additionalProperty/attribute/tokenize(@value, '-')[last()][not(. = ('Preferred', 'URI'))], ' ')"/>
                        <xsl:if test="$newoid">
                            <xsl:attribute name="newoid" select="$newoid"/>
                        </xsl:if>
                        <description>
                            <xsl:value-of select="($s/*:description/@value, $s/*:description/*:thumbnail/@value)[1]"/>
                        </description>
                    </xsl:element>
                </xsl:for-each>
            </xsl:for-each>
        </xsl:variable>
        
        <xsl:message>
            <xsl:text>    New OID Registry is missing      </xsl:text>
            <xsl:value-of select="count(distinct-values($calc[self::missing]/@oldoid))"/>
            <xsl:text> entries compared to the old OID Registry</xsl:text>
        </xsl:message>
        <xsl:for-each-group select="$calc[self::missing]" group-by="@oldoid">
            <xsl:message>
                <xsl:text>                                     </xsl:text>
                <xsl:value-of select="current-grouping-key()"/>
            </xsl:message>
            <xsl:for-each select="current-group()/@uri">
                <xsl:message>
                    <xsl:text>                                         </xsl:text>
                    <xsl:value-of select="../@fhirVersion"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="."/>
                </xsl:message>
            </xsl:for-each>
        </xsl:for-each-group>
        <xsl:message>
            <xsl:text>                        and has      </xsl:text>
            <xsl:value-of select="count(distinct-values($calc[self::different]/@oldoid))"/>
            <xsl:text> different entries for the same uris compared to the old OID Registry</xsl:text>
        </xsl:message>
        <xsl:for-each select="('DSTU2', 'STU3', 'R4', ())">
            <xsl:variable name="fhirVersion" select="if (string-length(.) gt 0) then concat('-', .) else (.)"/>
            <xsl:variable name="duplicates" select="$docoid[count(additionalProperty[attribute/@value = concat('HL7-FHIR-System-URI-Preferred', $fhirVersion)]) gt 1]"/>
            <xsl:message>
                <xsl:text>                        and has      </xsl:text>
                <xsl:value-of select="count(distinct-values($duplicates/dotNotation/@value))"/>
                <xsl:choose>
                    <xsl:when test="string-length(.) gt 0">
                        <xsl:text> entries with more than 1 preferred entry for </xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text> entries with more than 1 generically preferred entry</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:value-of select="."/>
            </xsl:message>
            <xsl:for-each-group select="$duplicates" group-by="dotNotation/@value">
                <xsl:sort select="current-grouping-key()"/>
                <xsl:message>
                    <xsl:text>                                     </xsl:text>
                    <xsl:value-of select="current-grouping-key()"/>
                </xsl:message>
                <xsl:for-each select="current-group()[1]/additionalProperty[attribute/@value = concat('HL7-FHIR-System-URI-Preferred', $fhirVersion)]/value/@value">
                    <xsl:sort select="."/>
                    <xsl:message>
                        <xsl:text>                                         </xsl:text>
                        <xsl:value-of select="."/>
                    </xsl:message>
                </xsl:for-each>
            </xsl:for-each-group>
        </xsl:for-each>
        <xsl:result-document href="oids-missing-in-new.xml">
            <x>
                <xsl:attribute name="count" select="count($oids-missing-in-new/..)"/>
                <xsl:attribute name="missing" select="count($calc[self::missing])"/>
                <xsl:attribute name="different-oid" select="count($calc[self::different])"/>
                <xsl:copy-of select="$calc[self::missing]"/>
                <xsl:copy-of select="$calc[not(self::missing)]"/>
            </x>
        </xsl:result-document>
    </xsl:template>
    
</xsl:stylesheet>