(: run me on art-decor.org after making sure the HL7 Org OID registry is present :)

xquery version "3.0";
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "/db/apps/art/modules/art-decor-settings.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at "/db/apps/art/modules/art-decor.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "/db/apps/art/api/api-server-settings.xqm";
import module namespace snomed      = "http://art-decor.org/ns/terminology/snomed" at "/db/apps/terminology/snomed/api/api-snomed.xqm";
import module namespace adloinc     = "http://art-decor.org/ns/terminology/loinc" at "/db/apps/terminology/loinc/api/api-loinc.xqm";
import module namespace claml       = "http://art-decor.org/ns/terminology/claml" at "/db/apps/terminology/claml/api/api-claml.xqm";

declare namespace f                 = "http://hl7.org/fhir";
declare namespace http              = "http://expath.org/ns/http-client";

declare variable $artDeepLinkServices   := adserver:getServerURLServices();
declare variable $artDeepLink           := adserver:getServerURLArt();
declare variable $artDeepLinkTerminology:= 'https://terminologie.nictiz.nl/terminology/';
(:declare variable $artDeepLinkTerminology:= replace(replace($artDeepLink,'/art-decor/', '/terminology/'), ':8080/', ':8877/');:)
declare variable $codeSystemSNOMED      := '2.16.840.1.113883.6.96';
declare variable $codeSystemLOINC       := '2.16.840.1.113883.6.1';
declare variable $codeSystemUCUM        := '2.16.840.1.113883.6.8';
declare variable $codeSystemNullFlavor  := '2.16.840.1.113883.6.1008';
declare variable $snomedgpscheck        := if (request:exists()) then request:get-parameter('snomedgps',())[1] = ('on', 'true') else true();
declare variable $localizationcheck     := if (request:exists()) then request:get-parameter('l10n',())[1] = ('on', 'true') else true();

(: need to check prefix/id for other versions before adding it here :)
declare variable $supportedVersions     := ('4.0');
declare variable $fhirVersion           := if (request:exists()) then request:get-parameter('fhirVersion','4.0') else '4.0';
declare variable $effectiveDate         :=
    switch ($fhirVersion)
    case '1.0' return '2015-10-24T00:00:00' (:DSTU2 1.0.2:)
    case '3.0' return '2019-10-24T00:00:00' (:STU3 3.0.2:)
    case '4.0' return '2019-10-30T00:00:00' (:R4 4.0.1:)
    default return substring(string(current-date()), 1,10) || 'T00:00:00'
;
declare variable $requestHeaders        := 
    <http:request method="GET">
        <http:header name="Content-Type" value="text/xml"/>
        <http:header name="Cache-Control" value="no-cache"/>
        <http:header name="Max-Forwards" value="1"/>
    </http:request>
;
declare variable $snomedRefsets         :=
    try { doc(concat(repo:get-root(), 'terminology-data/snomed-data/meta/refsets.xml'))/refsets }
    catch * {
        <refsets fallback="true">
            <refset id="98051000146103" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor contactallergenen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch contact allergen simple reference set</desc>
            </refset>
            <refset id="98011000146102" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch food allergen simple reference set</desc>
            </refset>
            <refset id="52801000146101" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset van landelijk implantatenregister</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch implant registry simple reference set</desc>
            </refset>
            <refset id="98021000146107" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch inhalation allergen simple reference set</desc>
            </refset>
            <refset id="98031000146109" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch insect venom allergen simple reference set</desc>
            </refset>
            <refset id="98041000146101" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch occupational allergen simple reference set</desc>
            </refset>
            <refset id="98061000146100" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor allergenen uitgezonderd medicatie</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch total non-drug allergen simple reference set</desc>
            </refset>
            <refset id="2581000146104" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor micro-organismen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch microorganism simple reference set</desc>
            </refset>
            <refset id="55451000146109" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch radio-allergosorbent test result simple reference set</desc>
            </refset>
            <refset id="97801000146108" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Netherlands microscopic ordinal test result simple reference set</desc>
            </refset>
            <refset id="46231000146109" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Netherlands ordinal test result simple reference set</desc>
            </refset>
            <refset id="110891000146105" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing intervention for delirium simple reference set</desc>
            </refset>
            <refset id="99051000146107" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor verpleegkundige interventies</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing intervention simple reference set</desc>
            </refset>
            <refset id="110881000146108" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing interventions for fall risk simple reference set</desc>
            </refset>
            <refset id="110861000146100" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing interventions for pain simple reference set</desc>
            </refset>
            <refset id="110911000146108" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing interventions for psychosocial care simple reference set</desc>
            </refset>
            <refset id="110901000146106" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing interventions for suicide simple reference set</desc>
            </refset>
            <refset id="110871000146106" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing interventions for wound simple reference set</desc>
            </refset>
            <refset id="117711000146107" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor patiëntproblemen inclusief secties van e-overdracht</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Simple reference set of Dutch nursing problems with sections of e-transfer</desc>
            </refset>
            <refset id="11721000146100" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">nationale kernset patiëntproblemen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing problem simple reference set</desc>
            </refset>
            <refset id="41000146103" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor optometrische diagnosen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch optometric diagnoses simple reference set</desc>
            </refset>
            <refset id="231000146105" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor optometrische verrichtingen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch optometric procedures simple reference set</desc>
            </refset>
            <refset id="8721000146106" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor optometrische bezoekredenen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch optometric reason for visit simple reference set</desc>
            </refset>
            <refset id="2551000146109" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor zeldzame neuromusculaire aandoeningen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Dutch rare neuromuscular disorders simple reference set</desc>
            </refset>
            <refset id="31000147101" moduleId="11000146104">
                <desc languageCode="nl" languageRefsetId="31000146106">DHD Diagnosethesaurus-referentieset</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">DHD Diagnosis thesaurus reference set</desc>
            </refset>
            <refset id="110851000146103" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">PALGA thesaurus simple reference set for pathology</desc>
            </refset>
            <refset id="721144007" moduleId="900000000000012004">
                <desc languageCode="en" languageRefsetId="900000000000509007">General dentistry diagnostic reference set</desc>
            </refset>
            <refset id="450970008" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor huisartsen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">General Practice / Family Practice reference set</desc>
            </refset>
            <refset id="711112009" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset met eenvoudige 'mapping' naar ICNP-diagnosen</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">ICNP diagnoses simple map reference set</desc>
            </refset>
            <refset id="712505008" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset met eenvoudige 'mapping' naar ICNP-interventies</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">ICNP interventions simple map reference set</desc>
            </refset>
            <refset id="723264001" moduleId="900000000000012004">
                <desc languageCode="en" languageRefsetId="900000000000509007">Lateralizable body structure reference set</desc>
            </refset>
            <refset id="733990004" moduleId="900000000000012004">
                <desc languageCode="en" languageRefsetId="900000000000509007">Nursing Activities Reference Set</desc>
            </refset>
            <refset id="733991000" moduleId="900000000000012004">
                <desc languageCode="en" languageRefsetId="900000000000509007">Nursing Health Issues Reference Set</desc>
            </refset>
            <refset id="721145008" moduleId="900000000000012004">
                <desc languageCode="en" languageRefsetId="900000000000509007">Odontogram reference set</desc>
            </refset>
            <refset id="32321000146103" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">SNOMED CT to ICF correlated extended map reference set</desc>
            </refset>
            <refset id="31451000146105" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">SNOMED CT to NANDA correlated map reference set</desc>
            </refset>
            <refset id="32311000146108" moduleId="11000146104">
                <desc languageCode="en" languageRefsetId="900000000000509007">SNOMED CT to Omaha correlated extended map reference set</desc>
            </refset>
            <refset id="467614008" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset met eenvoudige 'mapping' naar GMDN</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">GMDN simple map reference set</desc>
            </refset>
            <refset id="447562003" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset met complexe 'mapping' naar ICD-10</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">ICD-10 complex map reference set</desc>
            </refset>
            <refset id="446608001" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset met eenvoudige 'mapping' naar ICD-O</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">ICD-O simple map reference set</desc>
            </refset>
            <refset id="450993002" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset met complexe 'mapping' naar ICPC-2</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">International Classification of Primary Care, Second edition complex map reference set</desc>
            </refset>
            <refset id="705112009" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset met 'mapping' naar LOINC Part-codes</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">LOINC Part map reference set</desc>
            </refset>
        </refsets>
    }
;
declare variable $snomedAssociations    :=
    try { doc(concat(repo:get-root(), 'terminology-data/snomed-data/meta/associationsValues.xml'))/associationsValues }
    catch * {
        <associationsValues fallback="true">
            <refset id="900000000000524003" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende geëxporteerde concepten</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">MOVED TO association reference set</desc>
            </refset>
            <refset id="900000000000523009" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende mogelijk equivalente concepten</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">POSSIBLY EQUIVALENT TO association reference set</desc>
            </refset>
            <refset id="900000000000526001" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende vervangende concepten</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">REPLACED BY association reference set</desc>
            </refset>
            <refset id="900000000000527005" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende identieke concepten</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">SAME AS association reference set</desc>
            </refset>
            <refset id="900000000000528000" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende voormalige ouderconcepten</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">WAS A association reference set</desc>
            </refset>
            <refset id="900000000000530003" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende alternatieve concepten</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">ALTERNATIVE association reference set</desc>
            </refset>
            <refset id="900000000000525002" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende geïmporteerde concepten</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">MOVED FROM association reference set</desc>
            </refset>
            <refset id="900000000000489007" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">referentieset met attribuutwaarden voor reden voor deactivatie van concept</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Concept inactivation indicator reference set</desc>
            </refset>
            <value id="900000000000487009" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">component verplaatst naar andere module</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Moved elsewhere</desc>
            </value>
            <value id="900000000000484002" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">ambigu component</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Ambiguous</desc>
            </value>
            <value id="900000000000482003" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">dubbel component</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Duplicate</desc>
            </value>
            <value id="900000000000483008" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">obsoleet component</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Outdated</desc>
            </value>
            <value id="723277005" moduleId="900000000000012004">
                <desc languageCode="en" languageRefsetId="900000000000509007">Nonconformance to editorial policy component</desc>
            </value>
            <value id="900000000000485001" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">foutief component</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Erroneous</desc>
            </value>
            <value id="900000000000486000" moduleId="900000000000012004">
                <desc languageCode="nl" languageRefsetId="31000146106">component van beperkte waarde</desc>
                <desc languageCode="en" languageRefsetId="900000000000509007">Limited</desc>
            </value>
        </associationsValues>
    }
; 
declare %private function local:prep() as element()* {
    switch ($fhirVersion)
    case '1.0' return (
        let $fhir1.0collection    := xmldb:create-collection($get:strDecorTemp, '1.0')
        
        let $data1                :=
            for $uri in ('http://hl7.org/fhir/DSTU2/namingsystem-terminologies.xml','http://hl7.org/fhir/DSTU2/v2-tables.xml', 'http://hl7.org/fhir/DSTU2/v3-codesystems.xml', 'http://hl7.org/fhir/DSTU2/valuesets.xml')
            let $f    := tokenize($uri, '/')[last()]
            let $ff   := concat($fhir1.0collection, '/', $f)
            return
                if (doc-available($ff)) then $ff else (
                    xmldb:store($fhir1.0collection, $f, doc($uri))
                )
        
        return doc($data1[last()])/f:Bundle | doc($data1[2])//f:ValueSet[f:url/@value = 'http://hl7.org/fhir/ValueSet/v2-0136']
    )
    case '3.0' return (
        let $fhir3.0collection    := xmldb:create-collection($get:strDecorTemp, '3.0')
    
        let $data3                :=
            for $uri in ('http://hl7.org/fhir/STU3/namingsystem-terminologies.xml','http://hl7.org/fhir/STU3/v2-tables.xml', 'http://hl7.org/fhir/STU3/v3-codesystems.xml', 'http://hl7.org/fhir/STU3/valuesets.xml')
            let $f    := tokenize($uri, '/')[last()]
            let $ff   := concat($fhir3.0collection, '/', $f)
            return
                if (doc-available($ff)) then $ff else (
                    xmldb:store($fhir3.0collection, $f, doc($uri))
                )
        
        return doc($data3[last()])/f:Bundle | doc($data3[2])//f:ValueSet[f:url/@value = 'http://hl7.org/fhir/ValueSet/v2-0136'] | doc($data3[2])//f:CodeSystem[f:url/@value = 'http://hl7.org/fhir/v2/0136']
    )
    case '4.0' return (
        let $fhir4.0collection    := xmldb:create-collection($get:strDecorTemp, '4.0')
    
        let $data4                :=
            for $uri in ('http://hl7.org/fhir/R4/namingsystem-terminologies.xml','http://hl7.org/fhir/R4/v2-tables.xml', 'http://hl7.org/fhir/R4/v3-codesystems.xml', 'http://hl7.org/fhir/R4/valuesets.xml')
            let $f    := tokenize($uri, '/')[last()]
            let $ff   := concat($fhir4.0collection, '/', $f)
            return
                if (doc-available($ff)) then $ff else (
                    xmldb:store($fhir4.0collection, $f, doc($uri))
                )
        
        return doc($data4[last()])/f:Bundle | doc($data4[2])//f:ValueSet[f:url/@value = 'http://terminology.hl7.org/ValueSet/v2-0136'] | doc($data4[2])//f:CodeSystem[f:url/@value = 'http://terminology.hl7.org/CodeSystem/v2-0136']
    )
    default return ()
};

declare %private function local:convertCodeSystem2DECOR($cs as element(f:CodeSystem), $sequence as xs:string) as element(codeSystem) {
    let $isref    := $cs/f:content/@value = 'not-present'
    let $csuri    := $cs/f:url/@value
    let $csid     := art:getOIDForCanonicalUri($csuri, (), (), $fhirVersion)
    let $csid     := 
        if ($cs/f:identifier[f:system/@value = 'urn:ietf:rfc:3986']) then 
            replace($cs/f:identifier[f:system/@value = 'urn:ietf:rfc:3986']/f:value/@value, 'urn:oid:', '')
        else
        if (starts-with($cs/f:url/@value, 'urn:oid:')) then
            replace($cs/f:url/@value, 'urn:oid:', '')
        else
        if (string-length($csid)) then
            $csid
        else (
            ($get:colDecorData//codeSystem[@canonicalUri = $csuri]/@id, $sequence)[1]
        )
    
    let $csed     := 
        if ($cs/f:date/@value castable as xs:date) then concat($cs/f:date/@value, 'T00:00:00') else
        if ($cs/f:date/@value castable as xs:dateTime) then substring($cs/f:date/@value, 1, 19) else (
            $effectiveDate
        )
    let $csnm     := ($cs/f:name/@value, $cs/f:title/@value, tokenize($csuri, '/')[last()])[1]
    let $csnm     := replace(replace(normalize-space($csnm), '[/\s]', '_'), '[\(\)'',]', '')
    let $csdn     := ($cs/f:title/@value, $cs/f:name/@value, tokenize($csuri, '/')[last()])[1]
    
    let $csvers   := $cs/f:version/@value
    let $csst     := 
        switch ($cs/f:status/@value)
        case 'draft' return 'draft'
        case 'active' return 'final'
        case 'retired' return 'deprecated'
        default return $cs/f:status/@value (: e.g. 'unknown' - purposefully illegal DECOR status. TODO?:)
    let $csexp    := $cs/f:experimental/@value = 'true'
    let $cscs     := $cs/f:caseSensitive/@value = 'true'
    let $csvl     := $cs/f:version/@value
    
    return
    <codeSystem id="{$csid}" name="{$csnm}" displayName="{$csdn}" effectiveDate="{$csed}" statusCode="{$csst}">
    {
        if (empty($csvl)) then () else attribute versionLabel {$csvl},
        attribute canonicalUri {$csuri},
        if ($csexp) then attribute experimental {$csexp} else (),
        attribute caseSensitive {$cscs}
    }
      <desc language="en-US">{data($cs/f:description/@value)}</desc>
    {
      if ($cs/f:publisher) then
          <publishingAuthority name="{(data($cs/f:publisher/@value), 'HL7 International')[1]}">
          {
              for $contactDetail in $cs/f:contact
              return (
                  if ($contactDetail/f:name) then <addrLine>{data($contactDetail/f:name/@value)}</addrLine> else (),
                  for $telecom in $contactDetail/f:telecom
                  return
                      <addrLine>
                      {
                          switch ($telecom/f:system/@value)
                          case 'phone'
                          case 'fax'
                          case 'email' return attribute type {$telecom/f:system/@value}
                          case 'url' return attribute type {'uri'}
                          case 'sms' return attribute type {'phone'}
                          default return ()
                          ,
                          data($telecom/f:value/@value)
                      }
                      </addrLine>
              )
          }
          </publishingAuthority>
      else ()
      ,
      if ($cs/f:purpose) then
          <purpose language="en-US">{data($cs/f:purpose/@value)}</purpose>
      else ()
      ,
      if ($cs/f:copyright) then
          <copyright language="en-US">{data($cs/f:copyright/@value)}</copyright>
      else (),
      if ($cs/f:concept) then
          <conceptList>
          {
              for $concept in $cs/f:concept
              return
                  local:convertCodeSystemConcept2DECOR($concept)
          }
          </conceptList>
      else ()
    }
    </codeSystem>
};
declare %private function local:convertCodeSystemConcept2DECOR($cs as element(f:concept)) as element(codedConcept)* {
    let $lvl    := count($cs/ancestor::f:concept)
    let $typ    := if ($cs/f:property[f:code/@value = ('abstract', 'notSelectable')]/f:valueBoolean/@value = 'true') then 'A' else if ($cs/f:concept) then 'S' else 'L'
    (:inactive	Inactive	True if the concept is not considered active - e.g. not a valid concept any more. Property type is boolean, default value is false:)
    (:deprecated	Deprecated	The date at which a concept was deprecated. Concepts that are deprecated but not inactive can still be used, but their use is discouraged, and they should be expected to be made inactive in a future release. Property type is dateTime:)
    let $sts    := 
        if ($cs/f:property[f:code/@value = 'deprecated']/f:valueBoolean/@value = 'true') then 'deprecated' else
        if ($cs/f:property[f:code/@value = 'inactive']/f:valueBoolean/@value = 'true') then 'inactive' else
        if ($cs/f:property[f:code/@value = 'deprecationDate'][f:valueDateTime]) then 'deprecated'
        else 'active'
    return (
        <codedConcept code="{$cs/f:code/@value}" level="{$lvl}" type="{$typ}" statusCode="{$sts}">
          <designation displayName="{($cs/f:display/@value, $cs/f:code/@value)[1]}" language="en-US" type="preferred"/>
        {
            let $d    := $cs/f:property[f:code/@value = 'deprecationDate']/f:valueDateTime/@value | $cs/f:property[f:code/@value = 'deprecation']/f:valueDateTime/@value
            (: kill timezone if any :)
            let $d    := if (empty($d)) then () else tokenize($d[1], '[+-Z]')[1]
            let $d    :=
                if (empty($d)) then () else (
                    if ($d castable as xs:date) then format-date(xs:date($d), '[Y0001]-[M01]-[D01]') else
                    if ($d castable as xs:dateTime) then substring($d, 1, 19) else
                    if (concat($d, '01-01T00:00:00') castable as xs:dateTime) then concat($d, '01-01T00:00:00') else
                    if (concat($d, '01T00:00:00') castable as xs:dateTime) then concat($d, '01T00:00:00') else
                    if (concat($d, 'T00:00:00') castable as xs:dateTime) then concat($d, 'T00:00:00') else
                    if (concat($d, ':00:00') castable as xs:dateTime) then concat($d, ':00:00') else
                    if (concat($d, ':00') castable as xs:dateTime) then concat($d, ':00') else ()
                )
            return
            if (empty($d)) then () else attribute expirationDate {$d}
        }
        {
            for $designation in $cs/f:designation[not(starts-with(f:use/f:system/@value, 'http://acme.com'))]
            let $destype    := 
                if ($designation/f:use[f:system/@value = 'http://snomed.info/sct']/f:code/@value = '900000000000003001') then 'fsn' else
                if ($designation/f:use[f:system/@value = 'http://snomed.info/sct']/f:code/@value = '900000000000013009') then 'synonym' else (
                    'preferred'
                )
            return
                <designation displayName="{$designation/f:value/@value}" language="{$designation/f:language/@value}" type="{$destype}"/>
        }
        {
            if ($cs/f:definition) then
                <desc language="en-US">{data($cs/f:definition/@value)}</desc>
            else ()
        }
        </codedConcept>
        ,
        for $subconcept in $cs/f:concept
        return
            local:convertCodeSystemConcept2DECOR($subconcept)
    )
};

declare %private function local:convertValueSet2DECOR($cs as element(f:ValueSet), $sequence as xs:string, $urioidmap as map(*)) as element(valueSet)? {
    let $csuri    := $cs/f:url/@value
    let $csid     := local:getValueSetIDFromURI($csuri, $urioidmap)
    let $csid     := if (art:isOid($csid)) then $csid else $sequence
    let $csed     := 
        if ($cs/f:date/@value castable as xs:date) then concat($cs/f:date/@value, 'T00:00:00') else
        if ($cs/f:date/@value castable as xs:dateTime) then substring($cs/f:date/@value, 1, 19) else (
            $effectiveDate
        )
    let $csnm     := ($cs/f:name/@value, $cs/f:title/@value, tokenize($csuri, '/')[last()])[1]
    let $csnm     := replace(replace(normalize-space($csnm), '[/\s]', '_'), '[\(\)'',]', '')
    let $csdn     := ($cs/f:title/@value, $cs/f:name/@value, tokenize($csuri, '/')[last()])[1]
    
    let $csvers   := $cs/f:version/@value
    let $csst     := 
        switch ($cs/f:status/@value)
        case 'draft' return 'draft'
        case 'active' return 'final'
        case 'retired' return 'deprecated'
        default return $cs/f:status/@value (: e.g. 'unknown' - purposefully illegal DECOR status. TODO?:)
    let $csexp    := $cs/f:experimental/@value = 'true'
    let $csvl     := $cs/f:version/@value
    
    return
    <valueSet id="{$csid}" name="{$csnm}" displayName="{$csdn}" effectiveDate="{$csed}" statusCode="{$csst}">
    {
        if (empty($csvl)) then () else attribute versionLabel {$csvl},
        attribute canonicalUri {$csuri},
        if ($csexp) then attribute experimental {$csexp} else ()
    }
      <desc language="en-US">{data($cs/f:description/@value)}</desc>
    {
      if ($cs/f:publisher) then
          <publishingAuthority name="{(data($cs/f:publisher/@value), 'HL7 International')[1]}">
          {
              for $contactDetail in $cs/f:contact
              return (
                  if ($contactDetail/f:name) then <addrLine>{data($contactDetail/f:name/@value)}</addrLine> else (),
                  for $telecom in $contactDetail/f:telecom
                  return
                      <addrLine>
                      {
                          switch ($telecom/f:system/@value)
                          case 'phone'
                          case 'fax'
                          case 'email' return attribute type {$telecom/f:system/@value}
                          case 'url' return attribute type {'uri'}
                          case 'sms' return attribute type {'phone'}
                          default return ()
                          ,
                          data($telecom/f:value/@value)
                      }
                      </addrLine>
              )
          }
          </publishingAuthority>
      else ()
      ,
      if ($cs/f:purpose) then
          <purpose language="en-US">{data($cs/f:purpose/@value)}</purpose>
      else ()
      ,
      if ($cs/f:copyright) then
          <copyright language="en-US">{data($cs/f:copyright/@value)}</copyright>
      else ()
      ,
      for $include in $cs/f:compose/f:include[not(f:concept|f:filter[not(f:property/@value = ('class', 'SCALE_TYP','acme-plasma'))]|f:valueSet)][f:system]
      let $csoid    := map:get($urioidmap, $include/f:system/@value)
      let $csoid    := if (empty($csoid)) then art:getOIDForCanonicalUri($include/f:system/@value, (), (), $fhirVersion) else $csoid
      let $csoid    := if (empty($csoid)) then $include/f:system/@value else $csoid
      let $csoid    := replace($csoid, 'urn:oid:', '')
      return
          if ($include/f:system/@value = 'urn:iso:std:iso:3166:-2') then () else (
              <completeCodeSystem codeSystem="{$csoid}">{if ($include/f:version) then attribute codeSystemVersion {$include/f:version/@value} else ()}</completeCodeSystem>
          )
      ,
      let $csliststuff  := $cs/f:compose/f:include[f:concept|f:filter[not(f:property/@value = ('class', 'SCALE_TYP','acme-plasma'))]|f:valueSet] | $cs/f:compose/f:exclude
      return
      if ($csliststuff) then
          <conceptList>
          {
              for $part in $csliststuff
              let $cssys    := if ($part/f:system/@value = '#hacked') then 'http://hl7.org/fhir/hacked' else $part/f:system/@value  
              let $csoid    := if ($cssys) then map:get($urioidmap, $cssys) else ()
              let $csoid    := if (empty($csoid)) then art:getOIDForCanonicalUri($cssys, (), (), $fhirVersion) else $csoid
              let $csoid    := if (empty($csoid)) then replace($cssys, 'urn:oid:', '') else replace($csoid, 'urn:oid:', '')
              let $check    := if ($part[self::exclude]/f:filter) then error(xs:QName('f:UNSUPPORTED'), 'Found filter on exclusion. This is not supported') else ()
              let $check    := if ($part[count(f:filter) gt 1]) then error(xs:QName('f:UNSUPPORTED'), concat('Found multiple filters on ', local-name($part),'. This is not supported')) else ()
              return
                  if (local-name($part) = 'include') then local:convertInclusion2DECOR($part, $csoid, $urioidmap) else local:convertExclusion2DECOR($part, $csoid, $urioidmap)
          }
          </conceptList>
      else ()
    }
    </valueSet>
};

declare %private function local:convertInclusion2DECOR($part as element(f:include), $csoid as xs:string?, $urioidmap as map(*)) {
    for $valueSet in $part/f:valueSet
    let $vsid   := local:getValueSetIDFromURI($valueSet/@value, $urioidmap)
    return
        <include ref="{$vsid}"/>
    ,
    for $system in $part[f:system][not(f:concept | f:filter)]
    return
        <include codeSystem="{$csoid}">
        {
            if ($system/f:version) then attribute codeSystemVersion {$system/f:version/@value} else ()
        }
        </include>
    ,
    for $filter in $part[f:system][not(f:concept)]/f:filter
    let $op       :=
        if ($filter/f:property/@value = 'parent' and $filter/f:op/@value = '=') then 'descendent-of' else (
            $filter/f:op/@value[not(. = '=')]
        )
    let $cssys    := if ($part/f:system/@value = '#hacked') then 'http://hl7.org/fhir/hacked' else $part/f:system/@value
    let $cscode   := 
        if ($filter/f:value[@value = 'tbrottled'] and $part/f:system/@value = '#hacked') then 'throttled' else
        if ($filter/f:value[@value = 'lock'] and $part/f:system/@value = '#hacked') then 'lock-error' else
            $filter/f:value/@value
    let $cscpt    := collection(util:collection-name($part))//f:CodeSystem[f:url/@value = $cssys]//f:concept[f:code/@value = $cscode]
    let $cdn      := $cscpt/f:display/@value
    let $cdn      := 
        if (empty($cdn)) then (
            let $tt := local:getDisplayNameAndStatus($cscode, $csoid, 'en-US', 'en-US')/desc
            return ($tt[@preferredDisplayName = 'true'], $tt)[1]/@originalDisplayName
        ) 
        else $cdn
    let $cptlevel := count($cscpt/ancestor::f:concept[not(descendant::f:CodeSystem)])
    let $cpttype  :=
        if ($cscpt/f:property[f:code/@value = 'deprecated']/f:valueBoolean/@value = 'true') then 'D' else
        if ($cscpt/f:property[f:code/@value = 'inactive']/f:valueBoolean/@value = 'true') then 'D' else
        if ($cscpt/f:property[f:code/@value = ('abstract', 'notSelectable')]/f:valueBoolean/@value = 'true') then 'A' else 
        if ($cscpt/f:concept) then 'S' else 'L'
    let $elmname  := if (empty($op)) then (if ($csoid = $codeSystemNullFlavor) then 'exception' else 'concept') else 'include'
    return
        element {$elmname}
        {
            if (empty($op)) then () else attribute op {$op},
            attribute code {$cscode},
            attribute codeSystem {$csoid},
            if ($cdn) then attribute displayName {$cdn} else ()
            ,
            if ($part/f:version) then attribute codeSystemVersion {$part/f:version/@value} else (),
            if ($elmname = 'concept') then (
                attribute level {$cptlevel},
                attribute type {$cpttype}
            ) else ()
        }
    ,
    for $concept in $part[not(f:filter)]/f:concept
    let $cssys    := if ($part/f:system/@value = '#hacked') then 'http://hl7.org/fhir/hacked' else $part/f:system/@value
    let $cscode   := 
        if ($concept/f:code[@value = 'tbrottled'] and $part/f:system/@value = '#hacked') then 'throttled' else
        if ($concept/f:code[@value = 'lock'] and $part/f:system/@value = '#hacked') then 'lock-error' else
            $concept/f:code/@value
    let $cscpt    := collection(util:collection-name($part))//f:CodeSystem[f:url/@value = $cssys]//f:concept[f:code/@value = $cscode]
    let $cdn      := $concept/f:display/@value
    let $cdn      := if (empty($cdn)) then $cscpt/f:display/@value else $cdn
    let $cdn      := 
        if (empty($cdn)) then (
            let $tt := local:getDisplayNameAndStatus($cscode, $csoid, 'en-US', 'en-US')/desc
            return ($tt[@preferredDisplayName = 'true'], $tt)[1]/@originalDisplayName
        ) 
        else $cdn
    let $cptlevel := count($cscpt/ancestor::f:concept[not(descendant::f:CodeSystem)])
    let $cpttype  :=
        if ($cscpt/f:property[f:code/@value = 'deprecated']/f:valueBoolean/@value = 'true') then 'D' else
        if ($cscpt/f:property[f:code/@value = 'inactive']/f:valueBoolean/@value = 'true') then 'D' else
        if ($cscpt/f:property[f:code/@value = ('abstract', 'notSelectable')]/f:valueBoolean/@value = 'true') then 'A' else 
        if ($cscpt/f:concept) then 'S' else 'L'
    let $elmname  := if ($csoid = $codeSystemNullFlavor) then 'exception' else 'concept'
    return
        element {$elmname} {
            attribute code {$cscode},
            attribute codeSystem {$csoid},
            if ($part/f:version) then attribute codeSystemVersion {$part/f:version/@value} else ()
            ,
            if ($cdn) then attribute displayName {$cdn} else ()
            ,
            attribute level {$cptlevel},
            attribute type {$cpttype}
        }
};

declare %private function local:convertExclusion2DECOR($part as element(f:exclude), $csoid as xs:string?, $urioidmap as map(*)) {
    for $valueSet in $part/f:valueSet
    let $vsid   := local:getValueSetIDFromURI($valueSet/@value, $urioidmap)
    return
        <exclude ref="{$vsid}"/>
    ,
    for $concept in $part/f:concept
    return
        <exclude code="{$concept/f:code/@value}" codeSystem="{$csoid}">
        {
            if ($part/f:version) then attribute codeSystemVersion {$part/f:version/@value} else ()
            ,
            let $cdn      := ($concept/f:display/@value, collection(util:collection-name($part))//f:CodeSystem[f:url/@value = $part/f:system/@value]//f:concept[f:code/@value = $concept/f:code/@value]/f:display/@value)[1]
            let $cdn    := 
                if (empty($cdn)) then (
                    let $tt := local:getDisplayNameAndStatus($concept/f:code/@value, $csoid, 'en-US', 'en-US')/desc
                    return ($tt[@preferredDisplayName = 'true'], $tt)[1]/@originalDisplayName
                ) 
                else $cdn
            return
                if ($cdn) then attribute displayName {$cdn} else ()
        }
        </exclude>
    ,
    for $system in $part[f:system][not(f:concept)]
    return
        <exclude codeSystem="{$csoid}">{if ($system/f:version) then attribute codeSystemVersion {$system/f:version/@value} else ()}</exclude>
};

declare %private function local:getValueSetIDFromURI($vsuri as attribute(), $urioidmap as map(*)) as xs:string {
    let $vsid   := art:getOIDForCanonicalUri($vsuri, (), (), $fhirVersion)
    let $vsid   := if (art:isOid($vsid)) then $vsid else map:get($urioidmap, $vsuri)
    let $vsid   := 
        if (art:isOid($vsid)) then $vsid else (
            let $vs   := collection(util:collection-name($vsuri))//f:ValueSet[f:url/@value = $vsuri]
            return 
            if ($vs/f:identifier[f:system/@value = 'urn:ietf:rfc:3986'][art:isOid(replace(f:value/@value, '[^\.\d]', ''))]) then 
                replace($vs/f:identifier[f:system/@value = 'urn:ietf:rfc:3986']/f:value/@value, '[^\.\d]', '')
            else
            if (starts-with($vs/f:url/@value, 'urn:oid:') and art:isOid(replace(f:url/@value, '[^\.\d]', ''))) then
                replace($vs/f:url/@value, '[^\.\d]', '')
            else (
                $vsuri
            )
        )
    return $vsid
};

declare %private function local:getDisplayNameAndStatus($code as xs:string, $codeSystem as xs:string, $defaultLanguage as xs:string, $allLanguages as xs:string+) as element()* {
    if ($codeSystem = $codeSystemUCUM) then (
        let $concept        := doc(concat($get:strDecorCore, '/DECOR-ucum.xml'))//ucum[@unit = $code][@message = 'OK']
        let $displayNames   := 
            for $d in $concept/@displayName[not(. = '')]
            let $g := $d
            group by $g
            return $g
        return
        <codeSystem id="{$codeSystem}">
        {
            if (empty($concept)) then 
                attribute missing {'true'} 
            else (
                attribute originalStatusCode {'active'},
                attribute originalStatusText {'active'},
                for $displayName in $displayNames
                return
                    <desc language="en-US" originalDisplayName="{$displayName}" type="preferred"/>
            )
        }
        </codeSystem>
    )
    else
    if ($codeSystem = $codeSystemSNOMED) then (
        let $lang                       := tokenize($defaultLanguage, '-')[1]
        let $languageRefsetId           := 
            switch ($defaultLanguage)
            case 'nl-NL' return '31000146106'
            case 'de-DE' return '722130004'
            default return '900000000000509007'
        let $fallbackLanguageRefsetId   := '900000000000509007'
        let $service-uri        := concat($artDeepLinkTerminology,'snomed/getConcept/',encode-for-uri($code))
        (: protection against empty responses. This leads to 
            exerr:ERROR Unable to add Source to result:Premature end of file. [source: String] :)
        let $server-response    := try { http:send-request($requestHeaders, $service-uri) } catch * {()}
        
        let $concept        := $server-response//concept[@conceptId = $code]
        
        (:let $concept        := $snomed:colDataBase//@conceptId[. = $code]/parent::concept:)
        let $displayNames   := $concept/desc[@active]
        let $statusCode     := if ($concept[@active]) then 'active' else if ($concept) then 'deprecated' else ()
        let $statusText     := $statusCode
        
        let $deprecation    := $concept/attributeValue[@active][@refsetId = '900000000000489007']
        let $deprCode       := $deprecation/@valueId
        let $deprText       := $snomedAssociations//value[@id = $deprCode]
        let $deprText       := ($deprText/desc[@languageRefsetId = $languageRefsetId], $deprText/desc[@languageRefsetId = $fallbackLanguageRefsetId])
        let $deprText       := if ($deprText) then $deprText[1] else $deprecation[1]/@refset
        let $l10nMissing    := empty($displayNames[@languageRefsetId = $languageRefsetId])
        return
            <codeSystem id="{$codeSystem}">
            {
                if (empty($concept)) then attribute missing {'true'} else (),
                if ($localizationcheck and $l10nMissing) then attribute missingTranslation {$l10nMissing} else (),
                if (empty($statusCode)) then () else attribute originalStatusCode {$statusCode},
                if (empty($statusText)) then () else attribute originalStatusText {$statusText},
                if ($deprCode) then attribute deprecationCode {$deprCode} else (),
                if ($deprText) then attribute deprecationText {$deprText} else ()
            }
            {
                for $displayName in $displayNames
                return
                    <desc language="{$displayName/@languageCode}" originalDisplayName="{$displayName}">
                    {
                        attribute type {
                            switch ($displayName/@type) 
                            case 'fsn' return 'fsn' 
                            case 'pref' return 'preferred' 
                            case 'syn' return 'synonym' 
                            default return $displayName/@type
                        },
                        attribute preferredDisplayName {
                            (: preferred term if in the right language refset and fsn :)
                            if ($displayNames[@languageRefsetId = $languageRefsetId][@type = 'fsn']) then 
                                $displayName[@languageRefsetId = $languageRefsetId]/@type = 'fsn'
                            else
                            (: preferred term if in the right language refset that has no fsn, but has preferred :)
                            if ($displayNames[@languageRefsetId = $languageRefsetId][@type = 'pref']) then 
                                $displayName[@languageRefsetId = $languageRefsetId]/@type = 'pref'
                            else
                            (: preferred term if in the fallback language refset and fsn :)
                            if ($displayNames[@languageRefsetId = $fallbackLanguageRefsetId][@type = 'fsn']) then
                                $displayName[@languageRefsetId = $fallbackLanguageRefsetId]/@type = 'fsn'
                            else
                            (: preferred term if in the fallback language refset that has no fsn, but has preferred :)
                            if ($displayNames[@languageRefsetId = $fallbackLanguageRefsetId][@type = 'pref']) then 
                                $displayName[@languageRefsetId = $fallbackLanguageRefsetId]/@type = 'pref'
                            else (
                                (: preferred term if not available in the right refset, but at least in the core or the right language as fsn :)
                                $displayName[empty(@languageRefsetId)]/@type = 'fsn' or $displayName[@languageCode = $lang]/@type = 'fsn'
                            )
                        },
                        $displayName/(@* except (@language | @originalDisplayName | @type | @preferred))
                    }
                    </desc>
            }
            {
                (:<!-- attribueValue | associations -->:)
                (:<!-- The refsets MOVE TO (900000000000524003) and MOVED FROM (900000000000525002) refer to a module that a concept was moved into or out of -->:)
                (:<!-- REFERS TO is for descriptions and may be ignored (900000000000531004) -->:)
                (:<!-- SIMILAR TO is no longer in use (900000000000529008) -->:)
                for $assocs in $concept/association[@active][not(@refsetId = '900000000000531004')]
                let $refsetId   := $assocs/@refsetId
                group by $refsetId
                return (
                    let $refset     := ($snomedAssociations//refset[@id = $refsetId], $snomedRefsets//refset[@id = $refsetId])
                    let $refsetName := ($refset/desc[@languageRefsetId = $languageRefsetId], $refset/desc[@languageRefsetId = $fallbackLanguageRefsetId])
                    let $refsetName := if ($refsetName) then $refsetName[1] else $assocs[1]/@refset
                    
                    for $assoc in $assocs 
                    return 
                        <association>{$assoc/(@* except @refset), attribute refset {$refsetName}, attribute uri {concat($artDeepLink,'snomed-ct?conceptId=',encode-for-uri($assoc/@targetComponentId))}}</association>
                )
            }
            </codeSystem>
    )
    else 
    if ($codeSystem = $codeSystemLOINC and starts-with($code, 'LA')) then () else
    if ($codeSystem = $codeSystemLOINC) then (
        let $service-uri        := concat($artDeepLinkTerminology,'loinc/searchLOINC/',encode-for-uri($code))
        (: protection against empty responses. This leads to 
            exerr:ERROR Unable to add Source to result:Premature end of file. [source: String] :)
        let $server-response    := try { http:send-request($requestHeaders, $service-uri) } catch * {()}
        
        let $concept        := $server-response//concept[@loinc_num = $code]
        
        (:let $concept        := collection(concat($adloinc:strLoincData,$adloinc:anyLanguage))//concept[@loinc_num = $code][1]:)
        let $displayNames   := $concept//longName | $concept//shortName
        let $statusText     := $concept/@status
        let $statusCode     := 
            if (empty($statusText)) then () else 
            if (lower-case($statusText)='active')                       then 'active' else 
            if (lower-case($statusText)='trial')                        then 'pending' else 
            if (lower-case($statusText)=('deprecated','discouraged'))   then 'deprecated' else (
                lower-case($statusText)
            )
        
        let $deprecation    := $concept/elem[@name = 'STATUS_REASON']
        let $deprCode       := $deprecation
        let $deprText       := $deprecation
        let $l10nlang       := $displayNames/ancestor::concept[1]/@language
        let $l10nMissing    := if (empty($l10nlang)) then not($defaultLanguage = 'en-US') else not($defaultLanguage = $l10nlang)
        return
            <codeSystem id="{$codeSystem}">
            {
                if (empty($concept)) then attribute missing {'true'} else (),
                (:if ($localizationcheck and $l10nMissing) then attribute missingTranslation {$l10nMissing} else (),:)
                if (empty($statusCode)) then () else attribute originalStatusCode {$statusCode},
                if (empty($statusText)) then () else attribute originalStatusText {$statusText},
                if ($deprCode) then attribute deprecationCode {$deprCode} else (),
                if ($deprText) then attribute deprecationText {$deprText} else ()
            }
            {
                for $displayName in $displayNames
                let $lang   := $displayName/ancestor::concept[1]/@language
                return
                    <desc language="{if ($lang) then $lang else 'en-US'}" originalDisplayName="{$displayName}"/>
            }
            {
                for $assoc in $concept/map[@from = $code]
                return
                    <association targetComponentId="{$assoc/@to}" uri="{concat($artDeepLink,'loinc?conceptId=',encode-for-uri($assoc/@to))}"/>
            }
            </codeSystem>
    )
    else (
        let $service-uri        := concat($artDeepLinkTerminology,'claml/RetrieveClass?mode=xml&amp;classificationId=',$codeSystem,'&amp;code=',encode-for-uri($code))
        (: protection against empty responses. This leads to 
            exerr:ERROR Unable to add Source to result:Premature end of file. [source: String] :)
        let $server-response    := try { http:send-request($requestHeaders, $service-uri) } catch * {()}
        
        let $concept        := $server-response//Class[@code=$code][@classificationId=$codeSystem]
        
        (:let $concept        := claml:getPreparedClass((), $codeSystem, $code, ''):)
        let $displayNames   := $concept/Rubric[@kind='preferred']/Label
        let $statusCode     := $concept/Meta[@name='statusCode']/@value
        let $statusText     := $statusCode
        let $l10nMissing    := empty($displayNames[@xml:lang = $defaultLanguage])
        return
            <codeSystem id="{$codeSystem}">
            {
                if (empty($concept)) then attribute missing {'true'} else (),
                (:if ($localizationcheck and $l10nMissing) then attribute missingTranslation {$l10nMissing} else (),:)
                if (empty($statusCode)) then () else attribute originalStatusCode {$statusCode},
                if (empty($statusText)) then () else attribute originalStatusText {$statusText}
            }
            {
                for $displayName in $displayNames
                return
                    <desc>
                    {
                        if ($displayName[@xml:lang]) then (attribute language {$displayName/@xml:lang}) else (),
                        attribute originalDisplayName {$displayName},
                        attribute preferredDisplayName {$displayName/@xml:lang = $defaultLanguage}
                    }
                    </desc>
            }
            </codeSystem>
    )
};

let $projectId          := if (request:exists()) then request:get-parameter('id', ()) else ()
let $projectPrefix      := if (request:exists()) then request:get-parameter('prefix', ()) else ()
let $projectName        := if (request:exists()) then request:get-parameter('name', ()) else ()

let $fhirVersion        :=
    if ($fhirVersion = $supportedVersions) then $fhirVersion else (
        error(xs:QName('f:UNKNOWN_VERSION'), concat('Unknown version of FHIR ''', $fhirVersion, '''. Expected one of ', string-join($supportedVersions, ', ')))
    )

let $projectId          := 
    if (empty($projectId)) then
        switch ($fhirVersion)
        case '3.0' return '2.16.840.1.113883.3.1937.777.33'
        case '4.0' return '2.16.840.1.113883.3.1937.777.34'
        default return ()
    else ($projectId)
let $projectPrefix      := 
    if (empty($projectPrefix)) then
        switch ($fhirVersion)
        case '3.0' return 'ad6bbr-'
        case '4.0' return 'ad5bbr-'
        default return ()
    else ($projectPrefix)
let $projectName        := 
    if (empty($projectName)) then
        switch ($fhirVersion)
        case '3.0' return 'FHIR STU3 value sets / code systems'
        case '4.0' return 'FHIR R4 value sets / code systems'
        default return ()
    else ($projectName)
let $fhirDataUrl        :=
    switch ($fhirVersion)
    case '3.0' return 'https://hl7.org/fhir/STU3/valuesets.xml'
    case '4.0' return 'https://hl7.org/fhir/R4/valuesets.xml'
    default return ()
let $fhirDataName       :=
    switch ($fhirVersion)
    case '3.0' return 'STU3-FHIR-valuesets.xml'
    case '4.0' return 'R4-FHIR-valuesets.xml'
    default return ()

let $fhirTerminology    := local:prep()

let $codeSystems        :=
    for $codeSystem at $i in $fhirTerminology/descendant-or-self::f:CodeSystem
    return
        local:convertCodeSystem2DECOR($codeSystem, concat($projectId,'.5.',$i))

(:let $miscellaneous      := (
    (\:<codeSystem id="2.16.840.1.113883.1.11.14914" canonicalUri="http://terminology.hl7.org/ValueSet/v3-Race"/> |
    <codeSystem id="2.16.840.1.113883.5.1138" canonicalUri="http://terminology.hl7.org/ValueSet/v3-ActUSPrivacyLaw"/> |
    <codeSystem id="2.16.840.1.113883.21.72" canonicalUri="http://terminology.hl7.org/ValueSet/v2-0136"/>:\)
):)

let $urioidmap          :=
    map:merge((
        for $c in $codeSystems
        let $csuri := $c/@canonicalUri
        group by $csuri
        return
            map:entry($csuri, $c[1]/@id)
        (:,
        for $c in $miscellaneous
        let $csuri := $c/@canonicalUri
        group by $csuri
        return
            map:entry($csuri, $c[1]/@id):)
    ))
    
let $valueSets          :=
    for $valueSet at $i in $fhirTerminology/descendant-or-self::f:ValueSet
    return
        local:convertValueSet2DECOR($valueSet, concat($projectId,'.11.',$i), $urioidmap)

let $store              := xmldb:create-collection($get:strDecorData || '/projects', replace($projectPrefix, '-$', ''))
let $store              := xmldb:store($store, 'decor-' || replace($projectPrefix, '-$', '') || '.xml', document {
    <?xml-model href="https://assets.art-decor.org/ADAR/rv/DECOR.sch" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>,
    <?xml-stylesheet type="text/xsl" href="https://assets.art-decor.org/ADAR/rv/DECOR2schematron.xsl"?>,
    <decor xmlns:cda="urn:hl7-org:v3" xmlns:hl7="urn:hl7-org:v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="https://assets.art-decor.org/ADAR/rv/DECOR.xsd" repository="true" private="false" cda:dummy-1="urn:hl7-org:v3" hl7:dummy-2="urn:hl7-org:v3" xsi:dummy-3="http://www.w3.org/2001/XMLSchema-instance">
      <project id="{$projectId}" prefix="{$projectPrefix}" defaultLanguage="en-US" experimental="false">
        <name language="en-US">{$projectName}</name>
        <desc language="en-US">Automated conversion from {$fhirDataUrl}</desc>
        <copyright by="HL7 International" years="2019-" type="author"/>
        <author id="1" username="admin" notifier="off">Administrator</author>
        <author id="2" username="kai" email="info@kheitmann.de" notifier="off">Dr. Kai Heitmann</author>
        <author id="3" username="alexanderhenket" email="henket@nictiz.nl" notifier="off">Alexander Henket</author>
        <reference/>
        <defaultElementNamespace ns="hl7:"/>
      </project>
      <datasets/>
      <scenarios>
        <actors/>
      </scenarios>
      <ids>
        <baseId id="{$projectId}.1" type="DS" prefix="{$projectPrefix}dataset-"/>
        <baseId id="{$projectId}.2" type="DE" prefix="{$projectPrefix}dataelement-"/>
        <baseId id="{$projectId}.3" type="SC" prefix="{$projectPrefix}scenario-"/>
        <baseId id="{$projectId}.4" type="TR" prefix="{$projectPrefix}transaction-"/>
        <baseId id="{$projectId}.5" type="CS" prefix="{$projectPrefix}codesystem-"/>
        <baseId id="{$projectId}.6" type="IS" prefix="{$projectPrefix}issue-"/>
        <baseId id="{$projectId}.7" type="AC" prefix="{$projectPrefix}actor-"/>
        <baseId id="{$projectId}.8" type="CL" prefix="{$projectPrefix}conceptlist-"/>
        <baseId id="{$projectId}.9" type="EL" prefix="{$projectPrefix}template-element-"/>
        <baseId id="{$projectId}.10" type="TM" prefix="{$projectPrefix}template-"/>
        <baseId id="{$projectId}.11" type="VS" prefix="{$projectPrefix}valueset-"/>
        <baseId id="{$projectId}.16" type="RL" prefix="{$projectPrefix}rule-intern-"/>
        <baseId id="{$projectId}.17" type="TX" prefix="{$projectPrefix}test-transaction-"/>
        <baseId id="{$projectId}.18" type="SX" prefix="{$projectPrefix}test-scenario-"/>
        <baseId id="{$projectId}.19" type="EX" prefix="{$projectPrefix}example-instance-"/>
        <baseId id="{$projectId}.20" type="QX" prefix="{$projectPrefix}qualification-test-instance-"/>
        <baseId id="{$projectId}.21" type="CM" prefix="{$projectPrefix}community-"/>
        <baseId id="{$projectId}.22" type="MP" prefix="{$projectPrefix}mapping-"/>
        <defaultBaseId id="{$projectId}.1" type="DS"/>
        <defaultBaseId id="{$projectId}.2" type="DE"/>
        <defaultBaseId id="{$projectId}.3" type="SC"/>
        <defaultBaseId id="{$projectId}.4" type="TR"/>
        <defaultBaseId id="{$projectId}.5" type="CS"/>
        <defaultBaseId id="{$projectId}.6" type="IS"/>
        <defaultBaseId id="{$projectId}.7" type="AC"/>
        <defaultBaseId id="{$projectId}.8" type="CL"/>
        <defaultBaseId id="{$projectId}.9" type="EL"/>
        <defaultBaseId id="{$projectId}.10" type="TM"/>
        <defaultBaseId id="{$projectId}.11" type="VS"/>
        <defaultBaseId id="{$projectId}.16" type="RL"/>
        <defaultBaseId id="{$projectId}.17" type="TX"/>
        <defaultBaseId id="{$projectId}.18" type="SX"/>
        <defaultBaseId id="{$projectId}.19" type="EX"/>
        <defaultBaseId id="{$projectId}.20" type="QX"/>
        <defaultBaseId id="{$projectId}.21" type="CM"/>
        <defaultBaseId id="{$projectId}.22" type="MP"/>
      </ids>
      <terminology>
      {
          $codeSystems
          ,
          $valueSets
      }
      </terminology>
      <rules/>
      <issues notifier="on"/>
    </decor>
})

return <done/>