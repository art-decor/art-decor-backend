#!/bin/bash

export xslcreate=create-myregistry-from-hl7-export.xsl
export xslcheck=check-myregistry-from-hl7-export.xsl

if [ ! -e "${1:?"Input not set. Example: convert OID_Report-20141030213600.xml"}" ]; then
    echo "Input file does not exist."
    exit 1
fi

if [ ! -e "${jarPath:?"Parameter jarPath not set. Example: export jarPath=/mypath/lib/saxon9/saxon9.jar"}" ]; then
    echo "Parameter jarPath does not lead to saxon9.jar."
    exit 1
fi

if [ ! -e "namingsystem-terminologies-DSTU2.xml" ]; then
    echo "Retrieving DSTU2 Terminology NamingSystems"
    curl -o namingsystem-terminologies-DSTU2.xml http://hl7.org/fhir/DSTU2/namingsystem-terminologies.xml
fi
if [ ! -e "namingsystem-registry-DSTU2.xml" ]; then
    echo "Retrieving DSTU2 Identifier NamingSystems"
    curl -o namingsystem-registry-DSTU2.xml http://hl7.org/fhir/DSTU2/namingsystem-registry.xml
fi
if [ ! -e "valuesets-v2-DSTU2.xml" ]; then
    echo "Retrieving DSTU2 ValueSets V2"
    curl -o valuesets-v2-DSTU2.xml http://hl7.org/fhir/DSTU2/v2-tables.xml
fi
if [ ! -e "valuesets-v3-DSTU2.xml" ]; then
    echo "Retrieving DSTU2 ValueSets V3"
    curl -o valuesets-v3-DSTU2.xml http://hl7.org/fhir/DSTU2/v3-codesystems.xml
fi
if [ ! -e "valuesets-fhir-DSTU2.xml" ]; then
    echo "Retrieving DSTU2 ValueSets FHIR"
    curl -o valuesets-fhir-DSTU2.xml http://hl7.org/fhir/DSTU2/valuesets.xml
fi

if [ ! -e "namingsystem-terminologies-STU3.xml" ]; then
    echo "Retrieving STU3 Terminology NamingSystems"
    curl -o namingsystem-terminologies-STU3.xml http://hl7.org/fhir/STU3/namingsystem-terminologies.xml
fi
if [ ! -e "namingsystem-registry-STU3.xml" ]; then
    echo "Retrieving STU3 Identifier NamingSystems"
    curl -o namingsystem-registry-STU3.xml http://hl7.org/fhir/STU3/namingsystem-registry.xml
fi
if [ ! -e "valuesets-v2-STU3.xml" ]; then
    echo "Retrieving STU3 ValueSets V2"
    curl -o valuesets-v2-STU3.xml http://hl7.org/fhir/STU3/v2-tables.xml
fi
if [ ! -e "valuesets-v3-STU3.xml" ]; then
    echo "Retrieving STU3 ValueSets V3"
    curl -o valuesets-v3-STU3.xml http://hl7.org/fhir/STU3/v3-codesystems.xml
fi
if [ ! -e "valuesets-fhir-STU3.xml" ]; then
    echo "Retrieving STU3 ValueSets FHIR"
    curl -o valuesets-fhir-STU3.xml http://hl7.org/fhir/STU3/valuesets.xml
fi

if [ ! -e "namingsystem-terminologies-R4.xml" ]; then
    echo "Retrieving R4 Terminology NamingSystems"
    curl -o namingsystem-terminologies-R4.xml http://hl7.org/fhir/R4/namingsystem-terminologies.xml
fi
if [ ! -e "namingsystem-registry-R4.xml" ]; then
    echo "Retrieving R4 Identifier NamingSystems"
    curl -o namingsystem-registry-R4.xml http://hl7.org/fhir/R4/namingsystem-registry.xml
fi
if [ ! -e "valuesets-v2-R4.xml" ]; then
    echo "Retrieving R4 ValueSets V2"
    curl -o valuesets-v2-R4.xml http://hl7.org/fhir/R4/v2-tables.xml
fi
if [ ! -e "valuesets-v3-R4.xml" ]; then
    echo "Retrieving R4 ValueSets V3"
    curl -o valuesets-v3-R4.xml http://hl7.org/fhir/R4/v3-codesystems.xml
fi
if [ ! -e "valuesets-fhir-R4.xml" ]; then
    echo "Retrieving R4 ValueSets FHIR"
    curl -o valuesets-fhir-R4.xml http://hl7.org/fhir/R4/valuesets.xml
fi

#d=utg-terminology-stu3
#if [ ! -e "utg-STU3.xml" ]; then
#    echo "Retrieving STU3 UTG package"
#    curl -o hl7.terminology.r3.tgz https://terminology.hl7.org/hl7.terminology.r3.tgz
#    rm -rf $d; mkdir $d
#    tar -xvf hl7.terminology.r3.tgz --directory $d
#    
#    echo "{ \"resourceType\": \"Bundle\", \"type\": \"collection\", \"entry\": [" > $d/Bundle.json
#    for file in `find $d -name "CodeSystem-*.json" | sort;find $d -name "NamingSystem-*.json" | sort;find $d -name "ValueSet-*.json" | sort` ; do
#        echo "{ \"resource\": " >> $d/Bundle.json
#        cat "$file" >> $d/Bundle.json
#        echo "}," >> $d/Bundle.json
#    done
#    echo "] }" >> $d/Bundle.json
#    
    # remove final comma from for loop
#    sed -e ':a' -e 'N' -e '$!ba' -e 's/,\n\]/\n]/g' $d/Bundle.json > $d/Bundle-changed.json
#    java -cp "${jarPath}" net.sf.saxon.Query -o:"utg-STU3.xml" ./convertJson2Xml.xquery f="$d/Bundle-changed.json"
#    
#    rm -rf $d; rm -f hl7.terminology.r3.tgz
#fi
d=utg-terminology-r4
if [ ! -e "utg-R4.xml" ]; then
    echo "Retrieving R4 UTG package"
    curl -o hl7.terminology.r4.tgz https://terminology.hl7.org/hl7.terminology.r4.tgz
    rm -rf $d; mkdir $d
    tar -xvf hl7.terminology.r4.tgz --directory $d
    
    echo "{ \"resourceType\": \"Bundle\", \"type\": \"collection\", \"entry\": [" > $d/Bundle.json
    for file in `find $d -name "CodeSystem-*.json" | sort;find $d -name "NamingSystem-*.json" | sort;find $d -name "ValueSet-*.json" | sort` ; do
        echo "{ \"resource\": " >> $d/Bundle.json
        cat "$file" >> $d/Bundle.json
        echo "}," >> $d/Bundle.json
    done
    echo "] }" >> $d/Bundle.json
    
    # remove final comma from for loop
    sed -e ':a' -e 'N' -e '$!ba' -e 's/,\n\]/\n]/g' $d/Bundle.json > $d/Bundle-changed.json
    java -cp "${jarPath}" net.sf.saxon.Query -o:"utg-R4.xml" ./convertJson2Xml.xquery f="$d/Bundle-changed.json"
    
    rm -rf $d; rm -f hl7.terminology.r4.tgz
fi
#d=utg-terminology-r5
#if [ ! -e "$d" ]; then
#    echo "Retrieving R5 UTG package"
#    curl -o hl7.terminology.r5.tgz https://terminology.hl7.org/hl7.terminology.r5.tgz
#    rm -rf $d; mkdir $d
#    tar -xvf hl7.terminology.r5.tgz --directory $d
#    
#    echo "{ \"resourceType\": \"Bundle\", \"type\": \"collection\", \"entry\": [" > $d/Bundle.json
#    for file in `find $d -name "CodeSystem-*.json" | sort;find $d -name "NamingSystem-*.json" | sort;find $d -name "ValueSet-*.json" | sort` ; do
#        echo "{ \"resource\": " >> $d/Bundle.json
#        cat "$file" >> $d/Bundle.json
#        echo "}," >> $d/Bundle.json
#    done
#    echo "] }" >> $d/Bundle.json
    
    # remove final comma from for loop
#    sed -e ':a' -e 'N' -e '$!ba' -e 's/,\n\]/\n]/g' $d/Bundle.json > $d/Bundle-changed.json
#    java -cp "${jarPath}" net.sf.saxon.Query -o:"$d/Bundle-changed.xml" ./convertJson2Xml.xquery f="$d/Bundle-changed.json"
    
#    rm -rf $d; rm -f hl7.terminology.r5.tgz
#fi

# Build new project
echo "Building OID/URI mapping file"
java -cp "${jarPath}" net.sf.saxon.Query -o:uri-2-oid.xml ./create-uri-2-oid.xquery

echo "Building from $1"
java -jar "${jarPath}" -s:"$1" -xsl:"$xslcreate" oidUriMapping=uri-2-oid.xml unmatchedFileName=uri-2-unmatched.xml

echo "Checking hl7org-oids.xml ..."
xmllint --schema ../../tools/oids/core/DECORmyoidregistry.xsd --noout hl7org-oids.xml
java -jar "${jarPath}" -s:"$1" -xsl:"$xslcheck"

#echo "Creating STU3 BBR ..."
#java -cp "${jarPath}" net.sf.saxon.Query -o:decor-fhir3.0bbr.xml ./createFhirTerminologyBBR.xquery fhirVersion=3.0 id=2.16.840.1.113883.3.1937.777.33 prefix=fhir3.0bbr- name="FHIR Core 3.0 Building Block Repository"

#echo "Creating R4 BBR ..."
#java -cp "${jarPath}" net.sf.saxon.Query -o:decor-fhir4.0bbr.xml ./createFhirTerminologyBBR.xquery fhirVersion=4.0 id=2.16.840.1.113883.3.1937.777.34 prefix=fhir4.0bbr- name="FHIR Core 4.0 Building Block Repository"