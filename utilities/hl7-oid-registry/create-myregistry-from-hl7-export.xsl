<?xml version="1.0" encoding="UTF-8"?>
<!--
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" 
    xmlns:f="http://hl7.org/fhir"
    xmlns:local="urn:local"
    exclude-result-prefixes="#all" version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Oct 31, 2013</xd:p>
            <xd:p><xd:b>Author:</xd:b> ahenket</xd:p>
            <xd:p>Maps as much data as possible from the HL7 Registry in ISO 13582 Sharing of OIR Registries format.<xd:p>Prerequisite: export the HL7 Registry with all fields (http://www.hl7.org/oid/index.cfm - option "All OIDs" under "OID Excel Reports", use format XML). The export contains descriptions (3 fields) with things like &#233;. Could not find how to replace in xquery, so best to replace those before running this query.</xd:p></xd:p>
            <xd:p>The current mapping is as follows: <xd:ul>
                    <xd:li>Retrieve only OIDs with pattern [^[\d\.]+$]: This means we're skipping '0160/01/03 12:00:00', '2001/02/16 12:00:00', '655F67B1-2B11-4038-B82F-F6AB2F566F87', 'http://www.oid-info.com/get/2.16.840.1.113883.3.20'</xd:li>
                    <xd:li>Retrieve only OIDs with lower-case(assignment_status)=('complete','retired','deprecated','obsolete','proposed','pending')<br/> This means we're skipping status 'unknown', 'edited', 'rejected'</xd:li>
                    <xd:li>OID creation date is Entry_Timestamp, Date_begun, Date_finalized or nullFlavor NI</xd:li>
                    <xd:li>Registration authority is fixed to HL7 International</xd:li>
                </xd:ul></xd:p>
            <xd:pre>Is mapped?      HL7                         Remark
y               Comp_OID                    
y               Symbolic_name               adjusted to ISO 13582 format
y               CodingSystemName            
y               SubmitterFirst              
y               SubmitterLast               
y               Submitter_Email             
y               Submitter2                  Additional property
y               Contact_person_desc         
y               Contact_person_address      
y               Contact_person_phone        
y               Contact_person_email        
y               Contact_person_info         
y               Resp_body_name              
y               Resp_body_address           
y               Resp_body_phone             
y               Resp_body_email             
y               Resp_body_URL               
y               Resp_body_Type              
y               Resp_body_oid               
y               External_OID_flag           Additional property
y               externalOIDsubType          Additional property
y               replacedBy                  Additional property
y               Oid_Type                    Additional property and used to value category/@code
y               assignment_status           
y               AA_OID                      Additional property
y               AA_description              Additional property
y               Object_description          
y               Date_begun                  
y               Date_finalized              
y               Entry_Timestamp             
y               T396mnemonic                Additional property
y               Preferred_Realm             </xd:pre>
        </xd:desc>
    </xd:doc>
    
    <xsl:output indent="yes" omit-xml-declaration="yes"/>
    
    <xsl:param name="registryFileName" select="'hl7org-oids.xml'"/>
    <xsl:param name="oidUriMapping" select="'uri-2-oid.xml'"/>
    <xsl:param name="unmatchedFileName" select="'uri-2-unmatched.xml'"/>
    
    <xsl:variable name="oid2fhirPropertyNamePreferred">HL7-FHIR-System-URI-Preferred</xsl:variable>
    <xsl:variable name="oid2fhirPropertyNamePreferredDSTU2">HL7-FHIR-System-URI-Preferred-DSTU2</xsl:variable>
    <xsl:variable name="oid2fhirPropertyNamePreferredSTU3">HL7-FHIR-System-URI-Preferred-STU3</xsl:variable>
    <xsl:variable name="oid2fhirPropertyNamePreferredR4">HL7-FHIR-System-URI-Preferred-R4</xsl:variable>
    <xsl:variable name="oid2fhirPropertyName">HL7-FHIR-System-URI</xsl:variable>
    <xsl:variable name="oid2hl7v2PropertyName">HL7-V2-Table-0396-Code</xsl:variable>
    
    <xsl:variable name="doc" as="document-node()?">
        <xsl:if test="doc-available($oidUriMapping)">
            <xsl:copy-of select="doc($oidUriMapping)"/>
        </xsl:if>
    </xsl:variable>
    <xsl:variable name="docuri" as="element(map)*" select="$doc//map[not(@fhirVersion)]"/>
    <xsl:variable name="docuridstu2" as="element(map)*" select="$doc//map[@fhirVersion = 'DSTU2']"/>
    <xsl:variable name="docuristu3" as="element(map)*" select="$doc//map[@fhirVersion = 'STU3']"/>
    <xsl:variable name="docurir4" as="element(map)*" select="$doc//map[@fhirVersion = 'R4']"/>
    <xsl:variable name="oidRegistryItems" select="dataroot/OID_root[Comp_OID[matches(text(),'^[\d\.]+$')]][assignment_status[lower-case(.)=('complete','retired','deprecated','obsolete','proposed','pending')]]"/>
    <xsl:variable name="oidRegistryOIDs" select="$oidRegistryItems/Comp_OID/normalize-space(text())"/>
    <xsl:variable name="unmatched" select="$doc//unmatched"/>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match="/">
        <xsl:result-document href="{$unmatchedFileName}" omit-xml-declaration="yes" indent="yes">
            <x count="{count($unmatched)}">
                <xsl:for-each select="distinct-values($unmatched/@fhirVersion)">
                    <xsl:attribute name="countVersion{.}" select="count($unmatched[@fhirVersion = current()])"/>
                </xsl:for-each>
                <xsl:copy-of select="$unmatched"/>
            </x>
        </xsl:result-document>
        <xsl:result-document href="{$registryFileName}" omit-xml-declaration="yes" indent="yes">
            <xsl:processing-instruction name="xml-model">href="../../tools/oids/core/DECORmyoidregistry.xsd" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"</xsl:processing-instruction>
            <myoidregistry xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" name="hl7org" xsi:noNamespaceSchemaLocation="../../tools/oids/core/DECORmyoidregistry.xsd">
                <access>
                    <author username="kai" rights="rw"/>
                    <author username="maarten" rights="rw"/>
                    <author username="alexander" rights="rw"/>
                </access>
                <scopedAdditionalProperties>
                    <scopedAdditionalProperty code="AA_OID">
                        <name language="en-US">This is the string value of the OID or GUID being registered, if it was issued by someone other than HL7.</name>
                        <valueDomain type="string"/>
                    </scopedAdditionalProperty>
                    <scopedAdditionalProperty code="AA_description">
                        <name language="en-US">This is a name and/or textual description of the Assigning Authority for the OID. If this is an External OID, this describes or names the Registration Authority that created the OID. If this is a Type 4 OID (registered identifier namespaces), this describes the body that manages the namespace identified by the OID, and assigns the identifiers in that namespace.</name>
                        <valueDomain type="string"/>
                    </scopedAdditionalProperty>
                    <scopedAdditionalProperty code="External_OID_flag">
                        <name language="en-US">When you select 'Internal', HL7 will generate an OID automatically in the HL7 tree for you. When you select 'External', you must have an OID already assigned from another Registration Authority (which may be yourself if you are one) and you enter it into the form field. This OID will be registered in the HL7 registry with the descriptive metadata you are entering on these pages. Note that a GUID may alternatively be registered. Has values like 'EXT', 'Ext', 'INT', 'Int', 'Pxy', 'int'</name>
                        <valueDomain type="string">
                            <property maxLength="8"/>
                        </valueDomain>
                    </scopedAdditionalProperty>
                    <scopedAdditionalProperty code="externalOIDsubType">
                        <name language="en-US">This is used only when an externally created OID in a tree of OIDs maintained by a Registration Authority outside of HL7 has a branch node that serves to help organize the tree, but does not itself identify any object. If the node is used to help organize objects of a specific type, like a sub-tree of identifier namespaces or local coding systems, then this should be set to the type of object being organized.. See Oid_Type for semantics</name>
                        <valueDomain type="string">
                            <property maxLength="16"/>
                        </valueDomain>
                    </scopedAdditionalProperty>
                    <scopedAdditionalProperty code="Oid_Type">
                        <name language="en-US">This is the type of OID</name>
                        <valueDomain type="code">
                            <conceptList>
                                <concept code="1">
                                    <name language="en-US">OID for an HL7 Internal Object</name>
                                </concept>
                                <concept code="2">
                                    <name language="en-US">OID for an HL7 Body or Group</name>
                                </concept>
                                <concept code="3">
                                    <name language="en-US">Root to be a Registration Authority</name>
                                </concept>
                                <concept code="4">
                                    <name language="en-US">OID for a Registered Namespace</name>
                                </concept>
                                <concept code="5">
                                    <name language="en-US">OID for an HL7 Internal Code System</name>
                                </concept>
                                <concept code="6">
                                    <name language="en-US">OID for an External Code System</name>
                                </concept>
                                <concept code="7">
                                    <name language="en-US">OID for an HL7 Document</name>
                                </concept>
                                <concept code="8">
                                    <name language="en-US">OID for an HL7 Document Artifact</name>
                                </concept>
                                <concept code="9">
                                    <name language="en-US">OID for an HL7 Conformance Profile</name>
                                </concept>
                                <concept code="10">
                                    <name language="en-US">OID for an HL7 Template</name>
                                </concept>
                                <concept code="11">
                                    <name language="en-US">OID for an HL7 Internal Value Set</name>
                                </concept>
                                <concept code="12">
                                    <name language="en-US">OID for an Version 2.x Table</name>
                                </concept>
                                <concept code="13">
                                    <name language="en-US">OID for an External Value Set</name>
                                </concept>
                                <concept code="14">
                                    <name language="en-US">branch node subtype</name>
                                </concept>
                                <concept code="15">
                                    <name language="en-US">Defined external codesets</name>
                                </concept>
                                <concept code="17">
                                    <name language="en-US">Other Type OID</name>
                                </concept>
                                <concept code="18">
                                    <name language="en-US">OID for a Version 2.x Coding System</name>
                                </concept>
                                <concept code="19">
                                    <name language="en-US">OID for a published HL7 Example</name>
                                </concept>
                                <concept code="21">
                                    <name language="en-US">V2.x Value Sets</name>
                                </concept>
                            </conceptList>
                        </valueDomain>
                    </scopedAdditionalProperty>
                    <scopedAdditionalProperty code="replacedBy">
                        <name language="en-US">Replacing OID</name>
                        <valueDomain type="string">
                            <property maxLength="255"/>
                        </valueDomain>
                    </scopedAdditionalProperty>
                    <scopedAdditionalProperty code="Resp_body_Type">
                        <name language="en-US">A type for the organization responsible for the OID entry and the object identified by the OID. This is used to help filter when searching for organizations having registered OIDs.</name>
                        <valueDomain type="code">
                            <conceptList>
                                <concept code="Academic">
                                    <name language="en-US">Academic</name>
                                </concept>
                                <concept code="Govt body">
                                    <name language="en-US">Government body</name>
                                </concept>
                                <concept code="HL7 body">
                                    <name language="en-US">HL7 body</name>
                                </concept>
                                <concept code="Prof Soc">
                                    <name language="en-US">Professional Society</name>
                                </concept>
                                <concept code="Provider">
                                    <name language="en-US">Provider</name>
                                </concept>
                                <concept code="SDO">
                                    <name language="en-US">Standards Development Org.</name>
                                </concept>
                                <concept code="Vendor">
                                    <name language="en-US">Vendor</name>
                                </concept>
                                <concept code="Payor">
                                    <name language="en-US">Payor</name>
                                </concept>
                                <concept code="Exchange">
                                    <name language="en-US">Exchange</name>
                                </concept>
                            </conceptList>
                        </valueDomain>
                    </scopedAdditionalProperty>
                    <scopedAdditionalProperty code="Submitter2">
                        <name language="en-US">The name of a secondary submitter. If the OID registration is being submitted by proxy (someone other than the person wanting the OID to be registered, such as an HL7 cochair) then this is the name of the person needed the registration (rather than the person actually doing the registration).</name>
                        <valueDomain type="string"/>
                    </scopedAdditionalProperty>
                    <scopedAdditionalProperty code="T396mnemonic">
                        <name language="en-US">The name for Version 2.x Table 0396 for this code system (if registering a type 5 or type 6 OID) if codes from this terminology will be used in Version 2 messaging.</name>
                        <valueDomain type="string">
                            <property maxLength="20"/>
                        </valueDomain>
                    </scopedAdditionalProperty>
                    <scopedAdditionalProperty code="{$oid2fhirPropertyNamePreferred}">
                        <name language="en-US">The FHIR System URI. This system is to be the preferred system URI over the OID, or any other FHIR system URIs. Normally there's only 1 system URI, but at max there can be only 1 preferred system URI.</name>
                        <valueDomain type="string"/>
                    </scopedAdditionalProperty>
                    <scopedAdditionalProperty code="{$oid2fhirPropertyNamePreferredDSTU2}">
                        <name language="en-US">The FHIR System URI preferred until and including DSTU2. This system is to be the preferred system URI over the OID, or any other FHIR system URIs. Normally there's only 1 system URI, but at max there can be only 1 preferred system URI.</name>
                        <valueDomain type="string"/>
                    </scopedAdditionalProperty>
                    <scopedAdditionalProperty code="{$oid2fhirPropertyNamePreferredSTU3}">
                        <name language="en-US">The FHIR System URI preferred until and including STU3. This system is to be the preferred system URI over the OID, or any other FHIR system URIs. Normally there's only 1 system URI, but at max there can be only 1 preferred system URI.</name>
                        <valueDomain type="string"/>
                    </scopedAdditionalProperty>
                    <scopedAdditionalProperty code="{$oid2fhirPropertyNamePreferredR4}">
                        <name language="en-US">The FHIR System URI preferred until and including R4. This system is to be the preferred system URI over the OID, or any other FHIR system URIs. Normally there's only 1 system URI, but at max there can be only 1 preferred system URI.</name>
                        <valueDomain type="string"/>
                    </scopedAdditionalProperty>
                    <scopedAdditionalProperty code="{$oid2fhirPropertyName}">
                        <name language="en-US">The FHIR System URI. When multiple FHIR System URIs exist (which should never happen), or if the OID is to be preferred over the FHIR System URI in FHIR communication, this is where that FHIR System URI goes.</name>
                        <valueDomain type="string"/>
                    </scopedAdditionalProperty>
                    <scopedAdditionalProperty code="{$oid2hl7v2PropertyName}">
                        <name language="en-US">The HL7 V2 Table 0396 Coding Systems code equivalent for this OID.</name>
                        <valueDomain type="string"/>
                    </scopedAdditionalProperty>
                </scopedAdditionalProperties>
                <registry>
                    <validTime>
                        <low value="{replace(substring(string(current-dateTime()),1,10),'-','')}"/>
                    </validTime>
                    <scopedOID value="2.16.840.1.113883"/>
                    <name value="The HL7 International Registry"/>
                    <description language="en-US" mediaType="text/plain" value="This is the HL7 International registry"/>
                    <description language="nl-NL" mediaType="text/plain" value="Dit is het HL7 International register"/>
                    <person>
                        <name>
                            <part type="GIV" value="Ted"/>
                            <part type="FAM" value="Klein"/>
                        </name>
                    </person>
                    <hostingOrganization>
                        <name>
                            <part value="ART-DECOR"/>
                        </name>
                    </hostingOrganization>
                    <!-- Group by OID + status -->
                    <xsl:for-each-group select="$oidRegistryItems" group-by="string-join((Comp_OID/normalize-space(text()), local:getStatus(assignment_status/text())),'')">
                        <xsl:sort select="replace(replace(Comp_OID/normalize-space(text()) || '.', '\.', '.0000000000'), '.0*([0-9]{9,})', '.$1')"/>
                        <xsl:for-each select="current-group()[last()]">
                            <xsl:variable name="dotNotation" select="Comp_OID/normalize-space(text())"/>
                            <xsl:variable name="dateBegun" select="local:convertUSDateTimeToTimestamp(Date_begun/normalize-space(text()))"/>
                            <xsl:variable name="dateFinal" select="local:convertUSDateTimeToTimestamp(Date_finalized/normalize-space(text()))"/>
                            <xsl:variable name="dateEntry" select="local:convertUSDateTimeToTimestamp(Date_finalized/normalize-space(text()))"/>
                            <xsl:variable name="externalFlag" select="lower-case(External_OID_flag/normalize-space(text()))"/>
                            <xsl:variable name="symbolicName" select="local:cleanSymbolicName(Symbolic_name/normalize-space(text()))"/>
                            
                            <oid>
                                <dotNotation value="{$dotNotation}"/>
                                <xsl:if test="string-length($symbolicName) > 0">
                                    <symbolicName value="{$symbolicName}"/>
                                </xsl:if>
                                <!-- 
                                N   - node
                                NRA - registration authority (RA)
                                NMN - structure for the management of OIDs (it is not good practice but we know that some of these nodes in some registries also identify objects, which should not be)
                                L   - leaf
                                LIO - identifies an instance of an object
                                LNS - a namespace identifier
                            -->
                                <category code="{local:getCategory(Comp_OID/normalize-space(text()),Oid_Type/normalize-space(text()))}"/>
                                <status code="{local:getStatus(assignment_status/text())}"/>
                                <creationDate>
                                    <xsl:choose>
                                        <xsl:when test="string-length($dateEntry) > 0">
                                            <xsl:attribute name="value" select="$dateEntry"/>
                                        </xsl:when>
                                        <xsl:when test="string-length($dateBegun) > 0">
                                            <xsl:attribute name="value" select="$dateBegun"/>
                                        </xsl:when>
                                        <xsl:when test="string-length($dateFinal) > 0">
                                            <xsl:attribute name="value" select="$dateFinal"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:attribute name="nullFlavor" select="'NI'"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </creationDate>
                                <realm code="{Preferred_Realm/normalize-space(text())}"/>
                                <xsl:variable name="oidDescription" select="local:cleanDescription(Object_description/text())"/>
                                <description language="en-US" mediaType="text/plain" value="{$oidDescription}">
                                    <xsl:if test="CodingSystemName[string-length() > 0]">
                                        <thumbnail value="{CodingSystemName/local:cleanDescription(normalize-space(text()))}"/>
                                    </xsl:if>
                                </description>
                                <registrationAuthority>
                                    <code code="PRI"/>
                                    <scopingOrganization>
                                        <name>
                                            <part value="HL7 International"/>
                                        </name>
                                    </scopingOrganization>
                                </registrationAuthority>
                                <responsibleAuthority>
                                    <code code="PRI"/>
                                    <statusCode code="active"/>
                                    <validTime>
                                        <xsl:choose>
                                            <xsl:when test="string-length($dateEntry) > 0">
                                                <low value="{$dateEntry}"/>
                                            </xsl:when>
                                            <xsl:when test="string-length($dateBegun) > 0">
                                                <low value="{$dateBegun}"/>
                                            </xsl:when>
                                            <xsl:when test="string-length($dateFinal) > 0">
                                                <low value="{$dateFinal}"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:attribute name="nullFlavor" select="'NI'"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </validTime>
                                    <xsl:variable name="contactName" select="Contact_person_desc/local:cleanDescription(normalize-space(text()))"/>
                                    <xsl:variable name="contactAddr" select="Contact_person_address/local:cleanDescription(string())"/>
                                    <xsl:variable name="contactPhone" select="Contact_person_phone/normalize-space(text())"/>
                                    <xsl:variable name="contactEmail" select="Contact_person_email/normalize-space(text())[not(. = ('masked', 'unk@unknown.org'))]"/>
                                    <xsl:variable name="contactTitle" select="Contact_person_info/normalize-space(text())"/>
                                    <xsl:if test="string-length($contactName) > 0 or string-length($contactAddr) > 0 or string-length($contactPhone) > 0 or string-length($contactEmail) > 0 or string-length($contactTitle) > 0">
                                        <person>
                                            <name>
                                                <xsl:choose>
                                                    <xsl:when test="string-length($contactName) > 0">
                                                        <part value="{$contactName}"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:attribute name="nullFlavor" select="'NI'"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </name>
                                            <xsl:if test="string-length($contactAddr) > 0">
                                                <addr>
                                                    <xsl:for-each select="tokenize($contactAddr, '\n')[not(normalize-space() = $contactName)]">
                                                        <part value="{normalize-space(.)}"/>
                                                    </xsl:for-each>
                                                </addr>
                                            </xsl:if>
                                            <xsl:if test="string-length($contactPhone) > 0">
                                                <telecom value="{concat('tel:',replace($contactPhone,' ','%20'))}"/>
                                            </xsl:if>
                                            <xsl:if test="string-length($contactEmail) > 0">
                                                <telecom value="{concat('mailto:',$contactEmail)}"/>
                                            </xsl:if>
                                        </person>
                                    </xsl:if>
                                    <scopingOrganization>
                                        <xsl:variable name="responsibleBodyId" select="Resp_body_oid/normalize-space(text())"/>
                                        <xsl:variable name="responsibleBodyName">
                                            <xsl:variable name="tmpName" select="Resp_body_name/local:cleanDescription(normalize-space(text()))"/>
                                            <xsl:choose>
                                                <xsl:when test="$responsibleBodyId = '2.16.840.1.113883' and (contains($tmpName, 'unknown') or empty($tmpName))">HL7 International</xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:value-of select="Resp_body_name/local:cleanDescription(normalize-space(text()))"/>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:variable>
                                        <xsl:variable name="responsibleBodyAddr" select="Resp_body_address"/>
                                        <xsl:variable name="responsibleBodyPhone" select="Resp_body_phone/normalize-space(text())"/>
                                        <xsl:variable name="responsibleBodyEmail" select="Resp_body_email/normalize-space(text())[not(. = ('masked', 'unk@unknown.org'))]"/>
                                        <xsl:variable name="responsibleBodyURL" select="Resp_body_URL/normalize-space(text())"/>
                                        <xsl:if test="string-length(Resp_body_oid[matches(normalize-space(text()), '^[\d\.]+$')]) > 0">
                                            <id value="{Resp_body_oid/normalize-space(text())}"/>
                                        </xsl:if>
                                        <xsl:choose>
                                            <xsl:when test="string-length($responsibleBodyName) > 0">
                                                <name>
                                                    <part value="{$responsibleBodyName}"/>
                                                </name>
                                            </xsl:when>
                                            <xsl:when test="starts-with($externalFlag, 'int')">
                                                <name>
                                                    <part value="HL7 International"/>
                                                </name>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <name nullFlavor="NI"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                        <xsl:if test="string-length($responsibleBodyAddr/normalize-space(text())) > 0">
                                            <addr>
                                                <xsl:for-each select="tokenize($responsibleBodyAddr, '\n')[not(normalize-space() = $responsibleBodyName)]">
                                                    <part value="{local:cleanDescription(normalize-space(.))}"/>
                                                </xsl:for-each>
                                            </addr>
                                        </xsl:if>
                                        <xsl:if test="string-length($responsibleBodyPhone) > 0">
                                            <telecom value="{concat('tel:',replace($responsibleBodyPhone,' ','%20'))}"/>
                                        </xsl:if>
                                        <xsl:if test="string-length($responsibleBodyEmail) > 0">
                                            <telecom value="{concat('mailto:',$responsibleBodyEmail)}"/>
                                        </xsl:if>
                                        <xsl:if test="string-length($responsibleBodyURL) > 0">
                                            <telecom value="{$responsibleBodyURL}"/>
                                        </xsl:if>
                                    </scopingOrganization>
                                </responsibleAuthority>
                                <xsl:variable name="submissionDate" select="local:convertUSDateTimeToTimestamp(Date_begun/normalize-space(text()))"/>
                                <xsl:variable name="submissionFirst" select="SubmitterFirst/normalize-space(text())"/>
                                <xsl:variable name="submissionLast" select="SubmitterLast/normalize-space(text())"/>
                                <xsl:variable name="submissionEmail" select="Submitter_Email/normalize-space(text())[not(. = ('masked', 'unk@unknown.org'))]"/>
                                <xsl:if test="string-length($submissionDate) > 0 and string-length(concat($submissionFirst, $submissionLast)) > 0">
                                    <submittingAuthority>
                                        <code code="PRI"/>
                                        <applicationDate value="{$submissionDate}"/>
                                        <person>
                                            <name>
                                                <xsl:if test="string-length($submissionFirst) > 0">
                                                    <part value="{local:cleanDescription($submissionFirst)}" type="GIV"/>
                                                </xsl:if>
                                                <xsl:if test="string-length($submissionLast) > 0">
                                                    <part value="{local:cleanDescription($submissionLast)}" type="FAM"/>
                                                </xsl:if>
                                            </name>
                                            <xsl:if test="string-length($submissionEmail) > 0">
                                                <telecom value="{concat('mailto:',$submissionEmail)}"/>
                                            </xsl:if>
                                        </person>
                                        <scopingOrganization>
                                            <name nullFlavor="NI"/>
                                        </scopingOrganization>
                                    </submittingAuthority>
                                </xsl:if>
                                <xsl:if test="Oid_Type[string-length(normalize-space()) > 0]">
                                    <additionalProperty>
                                        <attribute value="Oid_Type"/>
                                        <value value="{Oid_Type/text()}"/>
                                    </additionalProperty>
                                </xsl:if>
                                <xsl:if test="Submitter2[string-length(normalize-space()) > 0]">
                                    <additionalProperty>
                                        <attribute value="Submitter2"/>
                                        <value value="{Submitter2/local:cleanDescription(text())}"/>
                                    </additionalProperty>
                                </xsl:if>
                                <xsl:if test="Resp_body_Type[string-length(normalize-space()) > 0]">
                                    <additionalProperty>
                                        <attribute value="Resp_body_Type"/>
                                        <value>
                                            <xsl:attribute name="value">
                                                <xsl:choose>
                                                    <xsl:when test="Resp_body_Type/text() = 'HL7 Body'">HL7 body</xsl:when>
                                                    <xsl:when test="Resp_body_Type/text() = 'Govt Body'">Govt body</xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="Resp_body_Type/text()"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:attribute>
                                        </value>
                                    </additionalProperty>
                                </xsl:if>
                                <xsl:if test="External_OID_flag[string-length(normalize-space()) > 0]">
                                    <additionalProperty>
                                        <attribute value="External_OID_flag"/>
                                        <value value="{External_OID_flag/lower-case(text())}"/>
                                    </additionalProperty>
                                </xsl:if>
                                <xsl:if test="externalOIDsubType[string-length(normalize-space()) > 0]">
                                    <additionalProperty>
                                        <attribute value="externalOIDsubType"/>
                                        <value value="{externalOIDsubType/text()}"/>
                                    </additionalProperty>
                                </xsl:if>
                                <xsl:if test="AA_OID[string-length(normalize-space()) > 0]">
                                    <additionalProperty>
                                        <attribute value="AA_OID"/>
                                        <value value="{AA_OID/text()}"/>
                                    </additionalProperty>
                                </xsl:if>
                                <xsl:if test="AA_description[string-length(normalize-space()) > 0]">
                                    <additionalProperty>
                                        <attribute value="AA_description"/>
                                        <value value="{local:cleanDescription(AA_description/text())}"/>
                                    </additionalProperty>
                                </xsl:if>
                                <xsl:if test="T396mnemonic[string-length(normalize-space()) > 0]">
                                    <additionalProperty>
                                        <attribute value="T396mnemonic"/>
                                        <value value="{T396mnemonic/text()}"/>
                                    </additionalProperty>
                                </xsl:if>
                                <xsl:if test="replacedBy[string-length(normalize-space()) > 0]">
                                    <additionalProperty>
                                        <attribute value="replacedBy"/>
                                        <value value="{replacedBy/text()}"/>
                                    </additionalProperty>
                                </xsl:if>
                                <!-- If there is a fhirVersion="DSTU2" assume this URI was true at the time -->
                                <xsl:for-each-group select="$docuridstu2[@oid = $dotNotation][@uri][@preferred = 'true']" group-by="@uri">
                                    <additionalProperty>
                                        <attribute value="{$oid2fhirPropertyNamePreferredDSTU2}"/>
                                        <value value="{current-grouping-key()}"/>
                                    </additionalProperty>
                                </xsl:for-each-group>
                                <!-- If there is a fhirVersion="STU3" assume this URI was true at the time -->
                                <xsl:for-each-group select="$docuristu3[@oid = $dotNotation][@uri][@preferred = 'true']" group-by="@uri">
                                    <additionalProperty>
                                        <attribute value="{$oid2fhirPropertyNamePreferredSTU3}"/>
                                        <value value="{current-grouping-key()}"/>
                                    </additionalProperty>
                                </xsl:for-each-group>
                                <!-- If there is a fhirVersion="R4" assume this URI was true at the time -->
                                <xsl:for-each-group select="$docurir4[@oid = $dotNotation][@uri][@preferred = 'true']" group-by="@uri">
                                    <additionalProperty>
                                        <attribute value="{$oid2fhirPropertyNamePreferredR4}"/>
                                        <value value="{current-grouping-key()}"/>
                                    </additionalProperty>
                                </xsl:for-each-group>
                                <xsl:for-each-group select="$docuri[@oid = $dotNotation][@uri]" group-by="@uri">
                                    <additionalProperty>
                                        <attribute value="{if (current-group()[1]/@preferred='true') then ($oid2fhirPropertyNamePreferred) else ($oid2fhirPropertyName)}"/>
                                        <value value="{current-grouping-key()}"/>
                                    </additionalProperty>
                                </xsl:for-each-group>
                                <xsl:for-each-group select="$docuri[@oid = $dotNotation][@hl70396]" group-by="@hl70396">
                                    <additionalProperty>
                                        <attribute value="{$oid2hl7v2PropertyName}"/>
                                        <value value="{current-grouping-key()}"/>
                                    </additionalProperty>
                                </xsl:for-each-group>
                            </oid>
                        </xsl:for-each>
                    </xsl:for-each-group>
                    <xsl:comment> Additions from FHIR, not present in the OID registry </xsl:comment>
                    <xsl:for-each-group select="($docuridstu2 , $docuristu3 , $docurir4 , $docuri)[not(@oid = $oidRegistryOIDs)]" group-by="@oid">
                        <xsl:sort select="replace(replace(@oid || '.', '\.', '.0000000000'), '.0*([0-9]{9,})', '.$1')"/>
                        <xsl:variable name="dotNotation" select="current-grouping-key()"/>
                        <xsl:variable name="creationDate" select="local:convertDateTimeToTimestamp(current-group()[1]/*[1]/f:date/@value)"/>
                        <xsl:variable name="status" select="local:getStatus((current-group()/*[1]/f:status/@value)[last()])"/>
                        <xsl:variable name="description" select="if ((current-group()/*[1]/f:description/@value)[last()]) then (current-group()/*[1]/f:description/@value)[last()] else (current-group()/*[1]/f:name/@value)[last()]"/>
                        <xsl:variable name="descriptionThumbnail" select="(current-group()/*[1]/f:name/@value)[last()]"/>
                        <!--
                            <NamingSystem xmlns="http://hl7.org/fhir">
                                <id value="tx-dicom"/>
                                <name value="DICOM Code Definitions"/>
                                <status value="draft"/>
                                <kind value="codesystem"/>
                                <publisher value="HL7, Inc"/>
                                <date value="2015-08-21"/>
                                <description value="The meanings of codes defined in DICOM, either explicitly or by reference to another part of DICOM or an external reference document or standard"/>
                                <uniqueId>
                                    <type value="uri"/>
                                    <value value="http://nema.org/dicom/dicm"/>
                                    <preferred value="true"/>
                                </uniqueId>
                                <uniqueId>
                                    <type value="oid"/>
                                    <value value="1.2.840.10008.2.16.4"/>
                                    <preferred value="false"/>
                                </uniqueId>
                              </NamingSystem>
                        -->
                        <oid>
                            <dotNotation value="{$dotNotation}"/>
                            <category code="LNS"/>
                            <status code="{$status}"/>
                            <creationDate value="{$creationDate}"/>
                            <realm code="UV"/>
                            <description language="en-US" mediaType="text/plain" value="{local:cleanDescription($description)}">
                                <thumbnail value="{local:cleanDescription($descriptionThumbnail)}"/>
                            </description>
                            <registrationAuthority>
                                <code code="PRI"/>
                                <scopingOrganization>
                                    <name>
                                        <part value="HL7 International"/>
                                    </name>
                                </scopingOrganization>
                            </registrationAuthority>
                            <responsibleAuthority>
                                <code code="PRI"/>
                                <statusCode code="active"/>
                                <validTime nullFlavor="NI"/>
                                <scopingOrganization>
                                    <name>
                                        <part value="HL7 International"/>
                                    </name>
                                </scopingOrganization>
                            </responsibleAuthority>
                            <additionalProperty>
                                <attribute value="Oid_Type"/>
                                <value value="4">
                                    <xsl:choose>
                                        <!-- 4 - OID for a Registered Namespace -->
                                        <xsl:when test="@type = 'identifier'">
                                            <xsl:attribute name="value">4</xsl:attribute>
                                        </xsl:when>
                                        <!-- 5 - OID for an HL7 Internal Code System -->
                                        <!-- HL7 V2 -->
                                        <xsl:when test="@type = 'codesystem' and (starts-with(@uri, 'http://hl7.org/fhir/v2') or starts-with(@uri, 'http://terminology.hl7.org/CodeSystem/v2-'))">
                                            <xsl:attribute name="value">5</xsl:attribute>
                                        </xsl:when>
                                        <!-- HL7 V3 -->
                                        <xsl:when test="@type = 'codesystem' and (starts-with(@uri, 'http://hl7.org/fhir/v3') or starts-with(@uri, 'http://terminology.hl7.org/CodeSystem/v3-'))">
                                            <xsl:attribute name="value">5</xsl:attribute>
                                        </xsl:when>
                                        <!-- HL7 FHIR -->
                                        <xsl:when test="@type = 'codesystem' and (matches(@uri, 'http://hl7.org/fhir/[^/]+') or starts-with(@uri, 'http://terminology.hl7.org/CodeSystem'))">
                                            <xsl:attribute name="value">5</xsl:attribute>
                                        </xsl:when>
                                        <!-- 6 - OID for an External Code System -->
                                        <xsl:when test="@type = 'codesystem'">
                                            <xsl:attribute name="value">6</xsl:attribute>
                                        </xsl:when>
                                        <!-- 21 V2.x Value Sets -->
                                        <!-- HL7 V2 -->
                                        <xsl:when test="@type = 'valueset' and (starts-with(@uri, 'http://hl7.org/fhir/v2-') or starts-with(@uri, 'http://terminology.hl7.org/ValueSet/v2-'))">
                                            <xsl:attribute name="value">21</xsl:attribute>
                                        </xsl:when>
                                        <!-- 11 OID for an HL7 Internal Value Set -->
                                        <!-- HL7 V2 -->
                                        <xsl:when test="@type = 'valueset' and (starts-with(@uri, 'http://hl7.org/fhir/') or starts-with(@uri, 'http://terminology.hl7.org/'))">
                                            <xsl:attribute name="value">11</xsl:attribute>
                                        </xsl:when>
                                        <xsl:when test="@type = 'valueset'">
                                            <xsl:attribute name="value">13</xsl:attribute>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:message> **** ERROR Unsupported OID/URI type in mapping table oid="<xsl:value-of select="@oid"/>" uri="<xsl:value-of select="@uri"/>" type="<xsl:value-of select="@type"/>"</xsl:message>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </value>
                            </additionalProperty>
                            <!--<additionalProperty>
                                <attribute value="External_OID_flag"/>
                                <value value="int"/>
                            </additionalProperty>
                            <additionalProperty>
                                <attribute value="AA_description"/>
                                <value value="HL7"/>
                            </additionalProperty>-->
                            <!-- If there is a fhirVersion="DSTU2" assume this URI was true at the time -->
                            <xsl:for-each-group select="$docuridstu2[@oid = $dotNotation][@uri][@preferred = 'true']" group-by="@uri">
                                <additionalProperty>
                                    <attribute value="{$oid2fhirPropertyNamePreferredDSTU2}"/>
                                    <value value="{current-grouping-key()}"/>
                                </additionalProperty>
                            </xsl:for-each-group>
                            <!-- If there is a fhirVersion="STU3" assume this URI was true at the time -->
                            <xsl:for-each-group select="$docuristu3[@oid = $dotNotation][@uri][@preferred = 'true']" group-by="@uri">
                                <additionalProperty>
                                    <attribute value="{$oid2fhirPropertyNamePreferredSTU3}"/>
                                    <value value="{current-grouping-key()}"/>
                                </additionalProperty>
                            </xsl:for-each-group>
                            <!-- If there is a fhirVersion="R4" assume this URI was true at the time -->
                            <xsl:for-each-group select="$docurir4[@oid = $dotNotation][@uri][@preferred = 'true']" group-by="@uri">
                                <additionalProperty>
                                    <attribute value="{$oid2fhirPropertyNamePreferredR4}"/>
                                    <value value="{current-grouping-key()}"/>
                                </additionalProperty>
                            </xsl:for-each-group>
                            <xsl:for-each-group select="$docuri[@oid = $dotNotation][@uri]" group-by="@uri">
                                <additionalProperty>
                                    <attribute value="{if (current-group()[1]/@preferred='true') then ($oid2fhirPropertyNamePreferred) else ($oid2fhirPropertyName)}"/>
                                    <value value="{current-grouping-key()}"/>
                                </additionalProperty>
                            </xsl:for-each-group>
                        </oid>
                    </xsl:for-each-group>
                </registry>
            </myoidregistry>
        </xsl:result-document>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>
            <xd:p>Return category</xd:p>
        </xd:desc>
        <xd:param name="oid">The OID from the field 'Comp_OID'</xd:param>
        <xd:param name="oidtype">The HL7 V3 Registry type from the field 'Oid_Type'</xd:param>
        <xd:return>ISO 13582 OID category</xd:return>
    </xd:doc>
    <xsl:function name="local:getCategory" as="xs:string">
        <xsl:param name="oid" as="xs:string?"/>
        <xsl:param name="oidtype" as="xs:string?"/>
        
        <xsl:choose>
            <xsl:when test="$oid = concat('2.16.840.1.113883.',$oidtype)">
                <xsl:text>NMN</xsl:text>
            </xsl:when>
            <xsl:when test="1">
                <!-- 1 - OID for an HL7 Internal Object -->
                <xsl:text>LNS</xsl:text>
            </xsl:when>
            <xsl:when test="2">
                <!-- 2 - OID for an HL7 Body or Group -->
                <xsl:text>LNS</xsl:text>
            </xsl:when>
            <xsl:when test="3">
                <!-- 3 - Root to be a Registration Authority -->
                <xsl:text>NRA</xsl:text>
            </xsl:when>
            <xsl:when test="4">
                <!-- 4 - OID for a Registered Namespace -->
                <xsl:text>LNS</xsl:text>
            </xsl:when>
            <xsl:when test="5">
                <!-- 5 - OID for an HL7 Internal Code System -->
                <xsl:text>LNS</xsl:text>
            </xsl:when>
            <xsl:when test="6">
                <!-- 6 - OID for an External Code System -->
                <xsl:text>L</xsl:text>
            </xsl:when>
            <xsl:when test="7">
                <!-- 7 - OID for an HL7 Document -->
                <xsl:text>LNS</xsl:text>
            </xsl:when>
            <xsl:when test="8">
                <!-- 8 - OID for an HL7 Document Artifact -->
                <xsl:text>LNS</xsl:text>
            </xsl:when>
            <xsl:when test="9">
                <!-- 9 - OID for an HL7 Conformance Profile -->
                <xsl:text>LNS</xsl:text>
            </xsl:when>
            <xsl:when test="10">
                <!-- 10 - OID for an HL7 Template -->
                <xsl:text>LNS</xsl:text>
            </xsl:when>
            <xsl:when test="11">
                <!-- 11 - OID for an HL7 Internal Value Set -->
                <xsl:text>L</xsl:text>
            </xsl:when>
            <xsl:when test="12">
                <!-- 12 - OID for an Version 2.x Table -->
                <xsl:text>L</xsl:text>
            </xsl:when>
            <xsl:when test="13">
                <!-- 13 - OID for an External Value Set -->
                <xsl:text>L</xsl:text>
            </xsl:when>
            <xsl:when test="14">
                <!-- 14 - branch node subtype -->
                <xsl:text>N</xsl:text>
            </xsl:when>
            <xsl:when test="15">
                <!-- 15 - Defined external codesets -->
                <xsl:text>L</xsl:text>
            </xsl:when>
            <xsl:when test="17">
                <!-- 17 - Other Type OID -->
                <xsl:text>LNS</xsl:text>
            </xsl:when>
            <xsl:when test="18">
                <!-- 18 - OID for a Version 2.x Coding System -->
                <xsl:text>L</xsl:text>
            </xsl:when>
            <xsl:when test="19">
                <!-- 19 - OID for a published HL7 Example -->
                <xsl:text>NMN</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <!--  don't know. go to default value  -->
                <xsl:text>LNS</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xd:doc>
        <xd:desc>
            <xd:p>Return statusCode</xd:p>
        </xd:desc>
        <xd:param name="input">required. HL7 V3 Registry status code</xd:param>
        <xd:return>ISO 13582 status code</xd:return>
    </xd:doc>
    <xsl:function name="local:getStatus" as="xs:string">
        <xsl:param name="input" as="xs:string?"/>
        <xsl:variable name="cleanInput" select="lower-case($input)"/>
        <xsl:choose>
            <xsl:when test="$cleanInput='complete'">
                <xsl:text>completed</xsl:text>
            </xsl:when>
            <xsl:when test="$cleanInput='unknown'">
                <xsl:text>unknown</xsl:text>
            </xsl:when>
            <xsl:when test="$cleanInput='retired'">
                <xsl:text>retired</xsl:text>
            </xsl:when>
            <xsl:when test="$cleanInput='deprecated'">
                <xsl:text>retired</xsl:text>
            </xsl:when>
            <xsl:when test="$cleanInput='obsolete'">
                <xsl:text>retired</xsl:text>
            </xsl:when>
            <xsl:when test="$cleanInput='pending'">
                <xsl:text>pending</xsl:text>
            </xsl:when>
            <xsl:when test="$cleanInput='rejected'">
                <xsl:text>unknown</xsl:text>
            </xsl:when>
            <xsl:when test="$cleanInput='proposed'">
                <xsl:text>pending</xsl:text>
            </xsl:when>
            <xsl:when test="$cleanInput='edited'">
                <xsl:text>unknown</xsl:text>
            </xsl:when>
            <!-- FHIR NamingSystem -->
            <!-- draft | active | retired -->
            <xsl:when test="$cleanInput='draft'">
                <xsl:text>pending</xsl:text>
            </xsl:when>
            <xsl:when test="$cleanInput='active'">
                <xsl:text>completed</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>unknown</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xd:doc>
        <xd:desc>
            <xd:p>Returns a valid ISO 21090 formatted timestamp or the empty string</xd:p>
        </xd:desc>
        <xd:param name="input">optional. String in the HL7 Registry with a US date time yyyy/mm/dd hh:mm:ss</xd:param>
        <xd:return>Valid ISO 21090 TS (yyyyMMddHHmmss) or empty string</xd:return>
    </xd:doc>
    <xsl:function name="local:convertUSDateTimeToTimestamp" as="xs:string?">
        <xsl:param name="input" as="xs:string?"/>
        <xsl:if test="matches(normalize-space($input),'^\d{4}/\d{2}/\d{2} \d{2}:\d{2}:\d{2}$')">
            <xsl:value-of select="replace(normalize-space($input),'[^\d]','')"/>
        </xsl:if>
    </xsl:function>
    
    <xd:doc>
        <xd:desc>
            <xd:p>Returns a valid ISO 21090 formatted timestamp or the empty string</xd:p>
        </xd:desc>
        <xd:param name="input">optional. String in a FHIR NamingSystem with a dateTime datatype yyyy-mm-ddThh:mm:ss</xd:param>
        <xd:return>Valid ISO 21090 TS (yyyyMMddHHmmss) or empty string</xd:return>
    </xd:doc>
    <xsl:function name="local:convertDateTimeToTimestamp" as="xs:string?">
        <xsl:param name="input" as="xs:string?"/>
        <xsl:variable name="dt">
            <xsl:choose>
                <xsl:when test="normalize-space($input) castable as xs:dateTime or normalize-space($input) castable as xs:date">
                    <xsl:value-of select="normalize-space($input)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="string(current-dateTime())"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="$dt castable as xs:dateTime">
                <xsl:value-of select="concat(replace(substring($dt, 1, 19), '[^\d]', ''),replace(substring($dt, 20), ':', ''))"/>
            </xsl:when>
            <xsl:when test="$dt castable as xs:date">
                <xsl:value-of select="concat(replace(substring($dt, 1, 10), '[^\d]', ''),replace(substring($dt, 11), ':', ''))"/>
            </xsl:when>
        </xsl:choose>
    </xsl:function>
    
    <xd:doc>
        <xd:desc>
            <xd:p>The HL7 Registry contains symbolic names, but they do not match the pattern for it as layed out in ISO 13582:
                "A symbolic short name, unique among the siblings of the arc of the OID. The ISO rules on Secondary Arc Identifiers, 
                as laid out in Rec. ITU-T | ISO/IEC 9834-1 Procedures for Registration Authorities, section 6.2.2, apply: 
                <xd:ul><xd:li>identifiers of an arc are required to commence with a lowercase letter, and to contain only letters, digits, and hyphens.</xd:li> 
                <xd:li>the last characters shall not be a hyphen - there shall be no two consecutive hyphens in the name"</xd:li></xd:ul></xd:p>
            <xd:p>The function prefixes a lower-case x if the first character is not [a-zA-Z], the function lower-cases the first character
                if it is [A-Z]. The function replaces any character that is not in [a-zA-Z0-9-] with a hyphen ("-") and replaces all double
                hyphens with a singular hyphen. Finally if the string ends in a hyphen, it removes that last character.</xd:p>
            <xd:p>Examples of actual input and output:</xd:p>
            <xd:ul>
                <xd:li>CMET > cMET</xd:li>
                <xd:li>OklahomaDLN > oklahomaDLN</xd:li>
                <xd:li>Service actor type > service-actor-type</xd:li>
                <xd:li>x_ActMoodDefEvn > x-ActMoodDefEvn</xd:li>
                <xd:li>Test of Email Server ---Delete---- > test-of-Email-Server-Delete</xd:li>
            </xd:ul>
        </xd:desc>
        <xd:param name="input">optional. String in the HL7 Inc registry that holds the symbolic name</xd:param>
        <xd:return>Valid symbolic name or an empty string</xd:return>
    </xd:doc>
    <xsl:function name="local:cleanSymbolicName" as="xs:string?">
        <xsl:param name="input" as="xs:string?"/>
        <xsl:variable name="output" select="normalize-space($input)"/>
        
        <!-- first char must be a-z -->
        <xsl:variable name="output-xstart">
            <xsl:if test="string-length($output)>0 and not(matches($output,'^[a-zA-Z]'))">
                <xsl:text>x</xsl:text>
            </xsl:if>
            <xsl:value-of select="$output"/>
        </xsl:variable>
        <!-- first char must be lower case -->
        <xsl:variable name="output-firstchar">
            <xsl:if test="string-length($output-xstart)>0">
                <xsl:value-of select="concat(lower-case(substring($output-xstart,1,1)),substring($output-xstart,2))"/>
            </xsl:if>
        </xsl:variable>
        <!-- replace any character that is not in range [a-zA-Z0-9-] with a hyphen -->
        <xsl:variable name="output-legalchars" select="replace($output-firstchar,'[^a-zA-Z0-9-]','-')"/>
        <!-- replace double hyphens with a singular hyphen, need function -->
        <xsl:variable name="output-singlehyphen" select="local:replaceAll(replace($output-legalchars,'--','-'),'--','-')"/>
        <!-- last char must not be - -->
        <xsl:choose>
            <xsl:when test="ends-with($output-singlehyphen,'-')">
                <xsl:value-of select="substring($output-singlehyphen,1,string-length($output-singlehyphen)-1)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$output-singlehyphen"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xd:doc>
        <xd:desc>
            <xd:p>Recurses until there's positively no more occurence of the search string to replace. The regular
                replace() function will leave occurences if they are nested. Example: replace('---Test---','--','-')
                yields '--Test--'</xd:p>
        </xd:desc>
        <xd:param name="input">optional. String to replace contents in</xd:param>
        <xd:param name="search">required. Search string</xd:param>
        <xd:param name="replace">required. String to replace search string with</xd:param>
        <xd:return>Input string with all occurences of $search replaced with $replace</xd:return>
    </xd:doc>
    <xsl:function name="local:replaceAll" as="xs:string?">
        <xsl:param name="input" as="xs:string?"/>
        <xsl:param name="search" as="xs:string"/>
        <xsl:param name="replace" as="xs:string"/>
        
        <xsl:choose>
            <xsl:when test="contains($input,$search)">
                <xsl:value-of select="local:replaceAll(replace($input,$search,$replace),$search,$replace)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$input"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xd:doc>
        <xd:desc>
            <xd:p>Replaces all occurences of &amp;(quot|lt|gt|apos|amp); with a normal entity, and cleans up a couple of mangled others. These characters appear to be corruption from encoding conversion problems between the entry form and the underlying Registry database</xd:p>
        </xd:desc>
        <xd:param name="input">optional. String to replace contents in</xd:param>
        <xd:return>Input string with all occurences of escaped entities replaced with regular entities</xd:return>
    </xd:doc>
    <xsl:function name="local:cleanDescription" as="xs:string?">
        <xsl:param name="input" as="xs:string?"/>
        <!--<xsl:variable name="a0" select="replace($input,'&amp;amp;#','&amp;#')"/>-->
        <xsl:variable name="a1" select="local:replaceAll(local:replaceAll($input,'&amp;amp;quot;','&quot;'),'&amp;quot;','&quot;')"/>
        <xsl:variable name="a2" select="local:replaceAll(local:replaceAll($a1,'&amp;amp;apos;',''''),'&amp;apos;','''')"/>
        <xsl:variable name="a3" select="local:replaceAll(local:replaceAll($a2,'&amp;amp;lt;','&lt;'),'&amp;lt;','&lt;')"/>
        <xsl:variable name="a4" select="local:replaceAll(local:replaceAll($a3,'&amp;amp;gt;','&gt;'),'&amp;gt;','>')"/>
        <xsl:variable name="a5" select="local:replaceAll(local:replaceAll($a4,'&amp;amp;','&amp;'),'&amp;amp;','&amp;')"/>
        
        <!-- Mangled UTF-8 in the original entry form probably, that looked like smart single quotes -->
        <xsl:variable name="a5a" select="local:replaceAll($a5,'&amp;#226;&amp;#8364;&amp;#8482;s', '''s')"/>
        <xsl:variable name="a6" select="local:replaceAll($a5a,'&amp;#226;&amp;#128;&amp;#153;','''')"/>
        <xsl:variable name="a7" select="local:replaceAll($a6,'&amp;#226;&amp;#128;&amp;#156;','''')"/>
        <xsl:variable name="a8" select="local:replaceAll($a7,'&amp;#226;&amp;#128;','''')"/>
        <xsl:variable name="a9" select="local:replaceAll($a8,'&#x92;','''')"/>
        <!-- Handle any of these UTF-8 constituents in other contexts -->
        
        <xsl:variable name="a10" select="local:replaceAll($a9, '&amp;#128;','')"/>
        <xsl:variable name="a11" select="local:replaceAll($a10,'&amp;#146;','''')"/>
        <xsl:variable name="a12" select="local:replaceAll($a11,'&amp;#147;','&quot;')"/>
        <xsl:variable name="a13" select="local:replaceAll($a12,'&amp;#148;','&quot;')"/>
        <xsl:variable name="a14" select="local:replaceAll($a13,'&amp;#150;','-')"/>
        <xsl:variable name="a15" select="local:replaceAll($a14,'&amp;#153;','')"/>
        <xsl:variable name="a16" select="local:replaceAll($a15,'&amp;#156;','')"/>
        <xsl:variable name="a17" select="local:replaceAll($a16,'&amp;#160;','&#160;')"/>
        <xsl:variable name="a17a" select="local:replaceAll($a17,'&amp;#167;','§—')"/>
        <xsl:variable name="a18" select="local:replaceAll($a17a,'&amp;#169;','©')"/>
        <xsl:variable name="a19" select="local:replaceAll($a18,'&amp;#170;','ª')"/>
        
        <xsl:variable name="a20" select="local:replaceAll($a19,'&amp;#171;','«')"/>
        <xsl:variable name="a20a" select="local:replaceAll($a20,'&amp;#173;','­')"/>
        <xsl:variable name="a21" select="local:replaceAll($a20a,'&amp;#174;','®')"/>
        <xsl:variable name="a22" select="local:replaceAll($a21,'&amp;#176;','°')"/>
        <xsl:variable name="a23" select="local:replaceAll($a22,'&amp;#180;','´')"/>
        <xsl:variable name="a23a" select="local:replaceAll($a23,'&amp;#183;','·')"/>
        <xsl:variable name="a24" select="local:replaceAll($a23a,'&amp;#186;','º')"/>
        <xsl:variable name="a25" select="local:replaceAll($a24,'&amp;#187;','»')"/>
        <xsl:variable name="a26" select="local:replaceAll($a25,'&amp;#192;','À')"/>
        <xsl:variable name="a27" select="local:replaceAll($a26,'&amp;#193;','Á')"/>
        <xsl:variable name="a28" select="local:replaceAll($a27,'&amp;#194;','Â')"/>
        <xsl:variable name="a29" select="local:replaceAll($a28,'&amp;#195;','Ã')"/>
        
        <xsl:variable name="a30" select="local:replaceAll($a29,'&amp;#196;','Ä')"/>
        <xsl:variable name="a31" select="local:replaceAll($a30,'&amp;#197;','Å')"/>
        <xsl:variable name="a32" select="local:replaceAll($a31,'&amp;#198;','Æ')"/>
        <xsl:variable name="a33" select="local:replaceAll($a32,'&amp;#199;','Ç')"/>
        <xsl:variable name="a34" select="local:replaceAll($a33,'&amp;#200;','È')"/>
        <xsl:variable name="a35" select="local:replaceAll($a34,'&amp;#201;','É')"/>
        <xsl:variable name="a36" select="local:replaceAll($a35,'&amp;#205;','Í')"/>
        <xsl:variable name="a37" select="local:replaceAll($a36,'&amp;#209;','Ñ')"/>
        <xsl:variable name="a38" select="local:replaceAll($a37,'&amp;#210;','Ò')"/>
        <xsl:variable name="a39" select="local:replaceAll($a38,'&amp;#211;','Ó')"/>
        
        <xsl:variable name="a40" select="local:replaceAll($a39,'&amp;#214;','Ö')"/>
        <xsl:variable name="a41" select="local:replaceAll($a40,'&amp;#216;','Ø')"/>
        <xsl:variable name="a42" select="local:replaceAll($a41,'&amp;#217;','Ù')"/>
        <xsl:variable name="a43" select="local:replaceAll($a42,'&amp;#218;','Ú')"/>
        <xsl:variable name="a44" select="local:replaceAll($a43,'&amp;#219;','Ů')"/>
        <xsl:variable name="a45" select="local:replaceAll($a44,'&amp;#220;','Ü')"/>
        <xsl:variable name="a46" select="local:replaceAll($a45,'&amp;#223;','ß')"/>
        <xsl:variable name="a47" select="local:replaceAll($a46,'&amp;#224;','à')"/>
        <xsl:variable name="a48" select="local:replaceAll($a47,'&amp;#225;','á')"/>
        <xsl:variable name="a49" select="local:replaceAll($a48,'&amp;#226;','â')"/>
        
        <xsl:variable name="a50" select="local:replaceAll($a49,'&amp;#227;','ã')"/>
        <xsl:variable name="a51" select="local:replaceAll($a50,'&amp;#228;','ä')"/>
        <xsl:variable name="a52" select="local:replaceAll($a51,'&amp;#229;','å')"/>
        <xsl:variable name="a53" select="local:replaceAll($a52,'&amp;#230;','æ')"/>
        <xsl:variable name="a54" select="local:replaceAll($a53,'&amp;#231;','ç')"/>
        <xsl:variable name="a55" select="local:replaceAll($a54,'&amp;#232;','è')"/>
        <xsl:variable name="a56" select="local:replaceAll($a55,'&amp;#233;','é')"/>
        <xsl:variable name="a57" select="local:replaceAll($a56,'&amp;#234;','ê')"/>
        <xsl:variable name="a58" select="local:replaceAll($a57,'&amp;#235;','ë')"/>
        <xsl:variable name="a59" select="local:replaceAll($a58,'&amp;#236;','ì')"/>
        
        <xsl:variable name="a60" select="local:replaceAll($a59,'&amp;#237;','í')"/>
        <xsl:variable name="a61" select="local:replaceAll($a60,'&amp;#238;','î')"/>
        <xsl:variable name="a62" select="local:replaceAll($a61,'&amp;#239;','ï')"/>
        <xsl:variable name="a63" select="local:replaceAll($a62,'&amp;#240;','ð')"/>
        <xsl:variable name="a64" select="local:replaceAll($a63,'&amp;#241;','ñ')"/>
        <xsl:variable name="a65" select="local:replaceAll($a64,'&amp;#242;','ò')"/>
        <xsl:variable name="a66" select="local:replaceAll($a65,'&amp;#243;','ó')"/>
        <xsl:variable name="a67" select="local:replaceAll($a66,'&amp;#244;','ô')"/>
        <xsl:variable name="a68" select="local:replaceAll($a67,'&amp;#245;','õ')"/>
        <xsl:variable name="a69" select="local:replaceAll($a68,'&amp;#246;','ö')"/>
        
        <xsl:variable name="a70" select="local:replaceAll($a69,'&amp;#248;','ø')"/>
        <xsl:variable name="a71" select="local:replaceAll($a70,'&amp;#249;','ù')"/>
        <xsl:variable name="a72" select="local:replaceAll($a71,'&amp;#250;','ú')"/>
        <xsl:variable name="a73" select="local:replaceAll($a72,'&amp;#251;','û')"/>
        <xsl:variable name="a74" select="local:replaceAll($a73,'&amp;#252;','ü')"/>
        <xsl:variable name="a75" select="local:replaceAll($a74,'&amp;#269;','č')"/>
        <xsl:variable name="a75a" select="local:replaceAll($a75,'&amp;#321;','Ł')"/>
        <xsl:variable name="a75b" select="local:replaceAll($a75a,'&amp;#322;','ł')"/>
        <xsl:variable name="a76" select="local:replaceAll($a75b,'&amp;#323;','Ń')"/>
        <xsl:variable name="a77" select="local:replaceAll($a76,'&amp;#324;','ń')"/>
        <xsl:variable name="a78" select="local:replaceAll($a77,'&amp;#331;','ŋ')"/>
        <xsl:variable name="a79" select="local:replaceAll($a78,'&amp;#332;','Ō')"/>
        
        <xsl:variable name="a80" select="local:replaceAll($a79,'&amp;#333;','ō')"/>
        <xsl:variable name="a81" select="local:replaceAll($a80,'&amp;#334;','Ŏ')"/>
        <xsl:variable name="a82" select="local:replaceAll($a81,'&amp;#335;','ŏ')"/>
        <xsl:variable name="a83" select="local:replaceAll($a82,'&amp;#336;','Ő')"/>
        <xsl:variable name="a84" select="local:replaceAll($a83,'&amp;#337;','ő')"/>
        <xsl:variable name="a85" select="local:replaceAll($a84,'&amp;#338;','Œ')"/>
        <xsl:variable name="a86" select="local:replaceAll($a85,'&amp;#339;','œ')"/>
        <xsl:variable name="a86a" select="local:replaceAll($a86,'&amp;#378;','ź')"/>
        <xsl:variable name="a87" select="local:replaceAll($a86a,'&amp;#618;','ɪ')"/>
        <xsl:variable name="a88" select="local:replaceAll($a87,'&amp;#643;','ʃ')"/>
        <xsl:variable name="a89" select="local:replaceAll($a88,'&amp;#712;','ˈ')"/>
        
        <xsl:variable name="a90" select="local:replaceAll($a89,'&amp;#720;','ː')"/>
        <xsl:variable name="a91" select="local:replaceAll($a90,'&amp;#732;','˜')"/>
        <xsl:variable name="a92" select="local:replaceAll($a91,'&amp;#8208;','‐')"/>
        <xsl:variable name="a93" select="local:replaceAll($a92,'&amp;#8211;','-')"/>
        <xsl:variable name="a94" select="local:replaceAll($a93,'&amp;#8212;','—')"/>
        <xsl:variable name="a95" select="local:replaceAll($a94,'&amp;#8217;','&#8217;')"/>
        <xsl:variable name="a96" select="local:replaceAll($a95,'&amp;#8226;','&#8226;')"/>
        <xsl:variable name="a97" select="local:replaceAll($a96,'&amp;#8232;',' &#8232;')"/>
        <xsl:variable name="a98" select="local:replaceAll($a97,'&amp;#x2028;',' &#8232;')"/>
        <xsl:variable name="a99" select="local:replaceAll($a98,'&amp;#8364;',' &#8364;')"/>
        
        <xsl:variable name="a100" select="local:replaceAll($a99,'&amp;#8480;',' &#8480;')"/>
        <xsl:variable name="a101" select="local:replaceAll($a100,'&amp;#8482;',' &#8482;')"/>
        <xsl:variable name="a102" select="local:replaceAll($a101,'&amp;#65292;','&#65292;')"/>
        
        <xsl:value-of select="$a102"/>
    </xsl:function>
</xsl:stylesheet>