<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
   <xsl:output indent="yes" media-type="xml"/>
   <xsl:template match="/ClaML">
      <ClaML version="2.0.0">
         <Meta name="custodianOrganisation" value="WHO-FIC"/>
         <Meta name="custodianOrganisationLogo" value="who-logo40.png"/>
         <Meta name="custodianOrganisationUrl" value="http://www.who-fic.nl/Familie_van_Internationale_Classificaties/Verwante_classificaties/ATC_DDD_Anatomische_Therapeutische_Chemische_classificatie_met_gedefinieerde_dagdoses"/>
         <Identifier authority="hl7.org" uid="2.16.840.1.113883.6.73"/>
         <Title name="ATC-NL" date="2017-01-01" version="2017-01-01">ATC-NL</Title>
         <Authors>
            <Author name="htn">Z-Index</Author>
         </Authors>
         <ClassKinds>
            <ClassKind name="anatomical_main_group"/>
            <ClassKind name="therapeutic_subgroup"/>
            <ClassKind name="pharmacological_subgroup"/>
            <ClassKind name="chemical_subgroup"/>
            <ClassKind name="chemical_substance"/>
         </ClassKinds>
         <RubricKinds>
            <RubricKind name="preferred"/>
         </RubricKinds>
         <xsl:for-each select="//Class">
            <Class code="{@code}" kind="{@kind}">
               <xsl:if test="parent::Class">
                  <SuperClass code="{parent::Class/@code}"/>
               </xsl:if>
               <Rubric id="{generate-id()}" kind="{Rubric/@kind}">
                  <xsl:for-each select="Rubric/Label[@xml:lang='nl']">
                     <Label xml:lang="{./@xml:lang}">
                        <xsl:value-of select="."/>
                     </Label>
                  </xsl:for-each>
               </Rubric>
               <xsl:for-each select="Class">
                  <SubClass code="{@code}"/>
               </xsl:for-each>
            </Class>
         </xsl:for-each>
      </ClaML>
   </xsl:template>
</xsl:stylesheet>
