<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
   <xsl:output indent="yes" media-type="xml"/>
   <xsl:template match="/BST801T">
      <ClaML version="2.0.0">
         <ClassKinds>
            <ClassKind name="anatomical_main_group"/>
            <ClassKind name="therapeutic_subgroup"/>
            <ClassKind name="pharmacological_subgroup"/>
            <ClassKind name="chemical_subgroup"/>
            <ClassKind name="chemical_substance"/>
         </ClassKinds>
         <RubricKinds>
            <RubricKind name="preferred"/>
         </RubricKinds>
         <xsl:for-each-group select="row" group-starting-with="row[string-length(@ATCODE) = 1]">
            <Class code="{@ATCODE}" kind="anatomical_main_group">
               <Rubric id="_010-0202-1533-3956" kind="preferred">
                  <Label xml:lang="nl">
                     <xsl:value-of select="@ATOMS"/>
                  </Label>
                  <Label xml:lang="en">
                     <xsl:value-of select="@ATOMSE"/>
                  </Label>
               </Rubric>
               <xsl:for-each-group select="current-group()[string-length(@ATCODE) &gt; 1]" group-starting-with="row[string-length(@ATCODE) = 3]">
                  <Class code="{@ATCODE}" kind="therapeutic_subgroup">
                     <Rubric id="_010-0202-1533-3956" kind="preferred">
                        <Label xml:lang="nl">
                           <xsl:value-of select="@ATOMS"/>
                        </Label>
                        <Label xml:lang="en">
                           <xsl:value-of select="@ATOMSE"/>
                        </Label>
                     </Rubric>
                     <xsl:for-each-group select="current-group()[string-length(@ATCODE) &gt; 3]" group-starting-with="row[string-length(@ATCODE) = 4]">
                        <Class code="{@ATCODE}" kind="pharmacological_subgroup">
                           <Rubric id="_010-0202-1533-3956" kind="preferred">
                              <Label xml:lang="nl">
                                 <xsl:value-of select="@ATOMS"/>
                              </Label>
                              <Label xml:lang="en">
                                 <xsl:value-of select="@ATOMSE"/>
                              </Label>
                           </Rubric>
                           <xsl:for-each-group select="current-group()[string-length(@ATCODE) &gt; 4]" group-starting-with="row[string-length(@ATCODE) = 5]">
                              <Class code="{@ATCODE}" kind="chemical_subgroup">
                                 <Rubric id="_010-0202-1533-3956" kind="preferred">
                                    <Label xml:lang="nl">
                                       <xsl:value-of select="@ATOMS"/>
                                    </Label>
                                    <Label xml:lang="en">
                                       <xsl:value-of select="@ATOMSE"/>
                                    </Label>
                                 </Rubric>
                                 <xsl:for-each select="current-group()[string-length(@ATCODE) &gt; 5]">
                                    <Class code="{@ATCODE}" kind="chemical_substance">
                                       <Rubric id="_010-0202-1533-3956" kind="preferred">
                                          <Label xml:lang="nl">
                                             <xsl:value-of select="@ATOMS"/>
                                          </Label>
                                          <Label xml:lang="en">
                                             <xsl:value-of select="@ATOMSE"/>
                                          </Label>
                                       </Rubric>
                                    </Class>
                                 </xsl:for-each>
                              </Class>
                           </xsl:for-each-group>
                        </Class>

                     </xsl:for-each-group>
                  </Class>

               </xsl:for-each-group>
            </Class>
         </xsl:for-each-group>
      </ClaML>
   </xsl:template>
</xsl:stylesheet>
