xquery version "3.0";

import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "/db/apps/art/modules/art-decor-settings.xqm";
import module namespace art     = "http://art-decor.org/ns/art" at "/db/apps/art/modules/art-decor.xqm";
import module namespace vs      = "http://art-decor.org/ns/decor/valueset" at "/db/apps/art/api/api-decor-valueset.xqm";

(:<model xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" exportDate="9-4-2015 10:40:01" xmlns="http://www.umcg.nl/MAX">:)
declare namespace UML           = "omg.org/UML1.3";
declare namespace max           = "http://www.umcg.nl/MAX";
declare namespace error         = "http://art-decor.org/ns/decor/import/error";
declare namespace xs            = "http://www.w3.org/2001/XMLSchema";

(:~Project prefix:)
declare variable $hcimReleaseYear           := '2020'; (:year-from-date(current-date()):)
declare variable $projectPrefix             := concat('zib',$hcimReleaseYear,'bbr-');
(:~Projectid:)
declare variable $oidProject                := '2.16.840.1.113883.2.4.3.11.60.121';
declare variable $xmicollection             := '/db/apps/decor/staging/hynekkruzik';

(:~Base for a Code System:)
declare variable $oidBaseCS                 := '2.16.840.1.113883.2.4.3.11.60.40.4';
(:~Base for a Building Block:)
declare variable $oidBaseZIB                := '2.16.840.1.113883.2.4.3.11.60.40.3';
(:~NL-CM-VS Base for a Value Set:)
declare variable $oidBaseVS                 := '2.16.840.1.113883.2.4.3.11.60.40.2';
(:~NL-CM Base for Concepts in a Building Block:)
declare variable $oidBaseEL                 := '2.16.840.1.113883.2.4.3.11.60.40.1';
declare variable $oidBaseDS                 := concat($oidProject,'.1');
declare variable $oidBaseDE                 := concat($oidProject,'.2');
declare variable $oidBaseDE2015             := concat($oidBaseDE,'.2');
declare variable $oidBaseDE2016             := concat($oidBaseDE,'.3');
declare variable $oidBaseDE2017             := concat($oidBaseDE,'.4');
declare variable $oidBaseDE2018             := concat($oidBaseDE,'.5');
declare variable $oidBaseDE2019             := concat($oidBaseDE,'.6');
declare variable $oidBaseDE2020             := concat($oidBaseDE,'.7');
declare variable $oidBaseDEdefault          := 
    switch (string($hcimReleaseYear))
    case '2015' return $oidBaseDE2015
    case '2016' return $oidBaseDE2016
    case '2017' return $oidBaseDE2017
    case '2018' return $oidBaseDE2018
    case '2019' return $oidBaseDE2019
    case '2020' return $oidBaseDE2020
    default return error(xs:QName('error:ParameterError'),concat('Your release year in \$hcimReleaseYear ''', $hcimReleaseYear, ''' does not lead to a base id yet. Add new global variable, and add in <ids> section to continue.'));
declare variable $oidBaseSC                 := concat($oidProject,'.3');
declare variable $oidBaseTRgroup            := concat($oidProject,'.4.1');
declare variable $oidBaseTRitem             := concat($oidProject,'.4.2');
declare variable $oidBaseIS                 := concat($oidProject,'.6');
declare variable $oidBaseAC                 := concat($oidProject,'.7');

(:declare variable $scenariostartid           := 1;
declare variable $transactionstartid        := 2;:)

declare variable $decorServiceUrl           := 'http://decor.nictiz.nl/decor/services/';
declare variable $defaultLanguage           := 'nl-NL';
declare variable $otherLanguages            := ('en-US');

declare variable $xmimodels                 := for $model in collection($xmicollection)//UML:Model order by lower-case(local:getdcmname($model)[1]) return $model;
declare variable $resultcollection          := '/db/apps/decor/staging';
declare variable $resultfile                := concat($projectPrefix,'decor-xmi.xml');
declare variable $imagecollection           := local:getimagecollection();
declare variable $imagehttpbase             := concat('http://decor.nictiz.nl/decor/data/projects/',substring($projectPrefix,1,string-length($projectPrefix)-1),'/',tokenize($imagecollection,'/')[last()]);
declare variable $preparedvalsets           := doc(concat('/db/apps/decor/staging/',$projectPrefix,'decor-valuesets.xml'))//valueSet;

declare variable $objectTypes               := ('Package','Class','Artifact');
declare variable $objectStereotypes         := ('DCM','rootconcept','data','container','document','context');
declare variable $relationshipTypes         := ('Aggregation','Dependency','Generalization','NoteLink');
declare variable $relationshipStereotypes   := ('trace');
declare variable $dcmdescheadings           := ('Concept','Purpose','Evidence Base','Example Instances','Instructions','Patient Population',
                                                'Interpretation','Issues','References','Traceability to other Standards',
                                                'Revision History');
declare variable $dcmscenheadings           := ('Concept','Purpose');
declare variable $dcmprojheadings           := ('Disclaimer','Terms of Use','Copyrights');
declare variable $dcmtags                   := ('DCM::AssigningAuthority','DCM::ConceptId','DCM::DefinitionCode','DCM::ExampleValue',
                                                'DCM::Id','DCM::LifecycleStatus','DCM::Name','DCM::PublicationDate',
                                                'DCM::ReferencedConceptId','DCM::ReferencedDefinitionCode','DCM::ValueSet','DCM::ValueSetId',
                                                'DCM::Version');
declare variable $TimeIntervalDSname        := 'nl.zorg.part.TimeInterval';

declare %private function local:getimagecollection() as xs:string {
    let $coll       := concat($projectPrefix,'images')
    let $fullcoll   := concat($resultcollection,'/',$coll)
    let $delete     := if (xmldb:collection-available($fullcoll)) then xmldb:remove($fullcoll) else ()
    
    return
        xmldb:create-collection($resultcollection,$coll)
};

declare %private function local:xmi2dataset($object as element(UML:Class)) as element(dataset) {
let $xmiroot            := local:getdcmroot($object)
let $dcmname            := local:getdcmname($xmiroot)
(:  <UML:TaggedValue tag="DCM::Superseeds" xmi.id="EAID_437C82AF_0FE2_4e8f_B27D_B5E4C2311C4F" value="nl.nfu.OverdrachtAlert-v1.1 (2.16.840.1.113883.2.4.3.11.60.40.3.8.1)" modelElement="MX_EAID_279935E4_7548_49ed_A213_AC6542006E73"/>:)
(:let $dcmsuperseeds      := local:getTaggedValue($xmiroot/ancestor-or-self::UML:Model,'DCM::Superseeds'):)
return
    <dataset>
    {
        (:dataset attribute properties:)
        local:getdcmid($xmiroot),
        local:getdcmeffectivedate($xmiroot),
        local:getdcmstatuscode($xmiroot),
        local:getdcmversionlabel($xmiroot),
        local:getdcmpublicationdate($xmiroot),
        local:getdcmdeprecationdate($xmiroot),
        $dcmname,
        for $lang in $otherLanguages
        return
            if ($dcmname[@language = $lang]) then () else <name language="{$lang}">{$dcmname/node()}</name>
        ,
        local:getdcmdesc($xmiroot, $dcmdescheadings)
        ,
        local:getdcmtags($xmiroot)
        (:,
        if (exists($dcmsuperseeds)) then (
            <relationship type="REPL" ref="{$dcmsuperseeds}"/>
        ) else ():)
        (:,
        local:getdcmproperties($xmiroot):)
    }
    {
        (:dataset root concept:)
        local:xmi2concept($xmiroot,$object,())
    }
    </dataset>
};

declare %private function local:xmi2concept($xmiroot as element(UML:Package), $object as element(UML:Class), $parent as element(UML:Class)?) as element(concept) {
let $conceptid          := local:getid($object)
let $concepteff         := local:getdcmeffectivedate($xmiroot)
let $concepttype        := local:gettype($object)
let $dcmdatatype        := local:getdcmdatatype($object)
let $valuedomaintype    := if ($concepttype='item') then local:getValueDomainType($object) else ()

(: pre 2017 style UML:Collaboration :)
(:let $boundary           := local:getcollaborationforobject($object, $parent)
let $otherobjects       := if ($boundary) then local:getothercollaborationobjects($boundary, $object, $parent) else ():)
(: 2017 style UML:ClassifierRole onder een UML:Collaboration :)
let $boundary           := local:getassociatedboundaries($object, $parent)
let $otherobjects       := if ($boundary) then local:getotherboundaryobjects($boundary, $object, $parent) else ()

(: TODO: Boundary may include nested Boundary with Classes :)
let $choiceobjects      :=
    for $otherobject in $otherobjects
    let $otherparents   := local:getallparents($otherobject)
    return
        if ($parent and $otherparents) then (
            if ($otherparents[not(@xmi.id = $parent/@xmi.id)]) then (
                let $parentName := if ($parent) then local:getname($parent) else ()
                return
                error(xs:QName('error:ClassParentError'),concat('ZIB "',local:getname(local:getdcmroot($object)),'" current object/id "',local:getTaggedValue($object,'ea_localid'),'" ',local:getname($object),' and other object/id "',local:getTaggedValue($otherobject,'ea_localid'),'" ',local:getname($otherobject),' share the same boundary/collaboration, but have a different parent. (',$parentName,' versus ',local:getTaggedValue($otherparents[not(@xmi.id=$parent/@xmi.id)],'ea_localid')))
            ) else (
                $otherobject
            )
        ) else ()
(:  assumption: when an object is in the same boundary as the current object, but not connected to the same parent as the current object
    then this signifies an object that belongs in a referenced package. This package should be referenced from one of the choiceobjects,
    hence one of the other objects within the same boundary that has the same parent as the current object.
    
    when there's no override we can just reference the other package. when there is an override we actually need to retrieve that other 
    package and add inherits in our current context except for the override and all its parents up to the package root.
    
    this is complex stuff to do here, so we delay that for a second processing step on the dataset, and just convert the override to a valid 
    concept. The concept/@id shall be matched in the second step to the correct concept and by then will be inserted into the correct 
    location.
    
    First step:
        <concept id="1.2.3" effectiveDate="2015-04-01T00:00:00" type="group" statusCode="final">
            <concept id="1.2.7" effectiveDate="2015-04-01T00:00:00" type="item" statusCode="final">
                <name/> | <desc/> | <operationalization/> | <valueDomain/>
            </concept>
        </concept>
        
    Second step:
        <concept id="1.2.3" effectiveDate="2015-04-01T00:00:00" type="group" statusCode="final">
            <name/> | <desc/><!-- referenced concept group from package that holds the overridden concept -->
            <concept ref="1.2.4" flexibility="2015-04-01T00:00:00">
                <!-- concept from package that is not overridden -->
            </concept>
            <concept id="1.2.5" effectiveDate="2015-04-01T00:00:00" type="item" statusCode="final">
                <name/> | <desc/><!-- concept group from package that holds the overridden concept -->
                
                <concept id="1.2.7" effectiveDate="2015-04-01T00:00:00" type="item" statusCode="final">
                    <name/> | <desc/> | <operationalization/> | <valueDomain/>
                </concept>
            </concept>
        </concept>
:)
let $overrideobjects    := $otherobjects[not(@xmi.id = ($choiceobjects/@xmi.id | $boundary/@xmi.id))](:[self::UML:Class]:)

(:if we are in a choice, it is very possible that our parent has a cardinality that spans its choice options. In this case our current object may not have 
    have it's own cardinality, but will instead rely on the max cardinality of its parent. The minimum cardinality cannot be determined this way and there's
    no way to say anything else but 0:)
let $card               := local:getMultiplicity($object, $parent, exists($choiceobjects))
let $min                := tokenize($card,'\.\.')[1]
let $min                := if (string-length($min)>0) then $min else (0)
let $max                := if ($card castable as xs:integer) then $card else tokenize($card,'\.\.')[2]
let $max                := if (string-length($max)>0) then $max else ('*')

return
    <concept minimumMultiplicity="{$min}" maximumMultiplicity="{$max}">
    {
        $conceptid,
        $concepteff,                            (:assume DCM effectiveDate  == concept effectiveDate:)
        local:getdcmstatuscode($xmiroot),       (:assume DCM status         == concept status:)
        $concepttype,
        local:getname($object),
        let $synonym    := local:getsynonym($object)
        return
            if ($synonym[starts-with(.,'EN:')]) then (
                <name language="en-US">{tokenize(normalize-space($synonym/substring-after(.,'EN:')),'::?')[1]}</name>
            )
            else (
                $synonym
            )
        ,
        local:getdesc($object),
        (:even if the object is a reference to another object, this object will still have it's own name e.g. "Reason to Prescribe" might reference "Problem". 
        To support this we insert this referencing object as concept group, and do a true inherit underneath that group. The second expand phase will handle ids etc.:)
        if ($concepttype) then () else (
            <concept  minimumMultiplicity="1" maximumMultiplicity="1">
            {
                $concepteff,   (:assume DCM effectiveDate  == concept effectiveDate:)
                local:getdcmstatuscode($xmiroot),      (:assume DCM status         == concept status:)
                $concepttype
            }
                <inherit ref="{local:getinheritid($object)}" effectiveDate="{$concepteff}"/>
            {
                for $overrideobject in $overrideobjects
                return
                    local:xmi2concept($xmiroot, $overrideobject, ())
            }
            </concept>
        )
    }
    {
        if ($concepttype) then (
            let $operationalizations    := local:getoperationalization($object)
            
            return (
                $operationalizations,
                if ($dcmdatatype = 'TimeInterval') then (
                    <valueDomain type="TimeInterval"/>
                ) else
                if ($concepttype='item') then (
                    <valueDomain type="{$valuedomaintype}">
                    {if ($valuedomaintype=('code','ordinal')) then
                        <conceptList id="{concat($conceptid,'.',replace($concepteff,'[^\d]',''),'.0')}"/>
                     else ()}
                    {local:getexamples($object)}
                    {local:getvaluedomainproperties($operationalizations[1], $conceptid, $concepteff, $valuedomaintype)}
                    </valueDomain>
                ) else ()
                ,
                (:dataset root concept:)
                for $childobject in local:getchildconcepts($object)
                return 
                    local:xmi2concept($xmiroot, $childobject, $object)
            )
        ) else ()
    }
    {
        (:These get moved to scenarios in xmi2scenario:)
        for $constraint in (local:getconstraints($object) | local:getassociatedconstraints($object, $parent))
        return
            <condition>{local:getdesc($constraint)}</condition>
        ,
        (:if there is a boundary/collaboration object: get any constraints from that level:)
        if ($boundary) then (
            for $constraint in local:getconstraints($boundary/descendant-or-self::UML:ClassifierRole[local:getTaggedValue(.,'ea_eleType')='element'])
            return
                <condition>{local:getdesc($constraint, if ($choiceobjects) then concat(' [',string-join(for $obj in ($object | $choiceobjects) return local:getname($obj),' | '),']') else ())}</condition>
        ) else ()
    }
    </concept>
};

declare %private function local:expanddatasets($decorProject as element(decor), $datasets as element(datasets)) as element(datasets) {
    <datasets>
    {
        text {'&#x0a;        '},comment {' ================= '},
        text {'&#x0a;        '},comment {concat(' === ZIBS ',$hcimReleaseYear,' === ')},
        text {'&#x0a;        '},comment {' ================= '},
        
        for $dataset in $datasets/dataset
        return
            <dataset>
            {
                $dataset/@*,
                $dataset/(* except concept)
                ,
                for $concept in $dataset/concept
                return local:expandconcept($decorProject, $datasets, $concept, (), false(), $dataset/@effectiveDate)
            }
            </dataset>
    }
    </datasets>
};
declare %private function local:expandconcept($decorProject as element(decor), $datasets as element(datasets), $concept as element(concept), $overrideconcepts as element(concept)*, $overridemode as xs:boolean, $effectiveDate as item()) as element(concept)+ {
    let $originalconcept    := 
        if ($concept[inherit]) then (
            if ($datasets//concept[@id=$concept/inherit/@ref]) then
                $datasets//concept[@id=$concept/inherit/@ref]
            else (
                (:error(xs:QName('error:ReferenceNotFound'),concat('DCM::Id "',$concept/ancestor::dataset/@id,'" ',$concept/ancestor::dataset/name[1],
                        ' concept "',$concept/name[1],'" references a concept with id="',$concept/inherit/@ref,
                        '", but it cannot be found. This means we are unable to apply the specialization on concept ',
                        string-join($concept/concept/concat(name[1],' (',@id,')'),' / '),'. This concept/these concepts will not be imported.')):)
            
                (:  get previously created/imported concept from decor/data 
                    have to compare without @effectiveDate as it might/will differ from previous imports. Without effectiveDate, we'll get the latest version:)
                art:getOriginalForConcept(<concept><inherit ref="{$concept/inherit/@ref}"/></concept>)
            )
        ) 
        else if ($overridemode) then (
            $concept
        )
        else ()
    let $originalispart     := local:inheritispart(local:getOriginalForConcept($originalconcept, $datasets))
    
    (:if the original concept is not in our current set we also need to get the @min/@max/conf from somewhere else:)
    let $transactionconcept :=
        if ($concept[@minimumMultiplicity|@maximumMultiplicity] or empty($originalconcept)) then () else (
            art:getTransactionsByDataset($originalconcept/ancestor::dataset/@id, $originalconcept/ancestor::dataset/@effectiveDate)//concept[@ref=$originalconcept/@id]
        )
    
    return
    
        (:this is the concept that is being overridden so we must be in overridemode:)
        if ($concept[@id=$overrideconcepts/@id]) then (
            <concept>
            {
                
                let $overridingconcept  := $overrideconcepts[@id=$concept/@id]
                let $newid              := local:getnewid($decorProject, 'DE')
                return (
                    (:attribute ttt {1},:)
                    attribute id {$newid},
                    $effectiveDate,
                    attribute oldid {$concept/@id},
                    $overridingconcept/(@* except (@id|@effectiveDate|@minimumMultiplicity|@maximumMultiplicity)),
                    if ($overridingconcept[@minimumMultiplicity|@maximumMultiplicity]) then (
                        $overridingconcept/(@minimumMultiplicity|@maximumMultiplicity),
                        $concept/condition
                    )
                    else (
                        $transactionconcept//@minimumMultiplicity[1],
                        $transactionconcept//@maximumMultiplicity[1],
                        $transactionconcept/condition
                    )
                    ,
                    (:get name/operationalization/conditions from original. assume that only valueDomain is constrained and possibly only desc is overridden:)
                    $concept/name,
                    $concept/synonym,
                    if ($overridingconcept[desc]) then $overridingconcept/desc else $concept/desc,
                    $concept/(* except (name|synonym|desc|condition|concept|inherit|relationship|operationalization|valueDomain)),
                    <relationship type="SPEC" ref="{$concept/@id}" flexibility="{$concept/@effectiveDate}">
                    {
                        (:get terminology associations on concept from original, if any:)
                        if ($concept[@minimumMultiplicity|@maximumMultiplicity] or empty($originalconcept)) then () else (
                            $originalconcept/ancestor::decor/project/@prefix,
                            art:getConceptAssociations($originalconcept)[@conceptId=$originalconcept/@id]
                        )
                    }
                    </relationship>
                    ,
                    $concept/(relationship|operationalization),
                    for $valueDomain in $overridingconcept/valueDomain
                    return
                        <valueDomain>
                        {
                            $valueDomain/@*,
                            if ($valueDomain/conceptList) then (
                                <conceptList id="{concat($newid,'.',replace($concept/@effectiveDate,'[^\d]',''),'.0')}" oldid="{$valueDomain/conceptList/@id}"/>
                            ) else (),
                            $valueDomain/(* except conceptList)
                        }
                        </valueDomain>
                    ,
                    (:this is unlikely but should cater for it:)
                    for $childconcept in $overridingconcept/concept
                    return local:expandconcept($decorProject, $datasets, $childconcept, $overrideconcepts, $overridemode, $effectiveDate)
                )
            }
            </concept>
        )
        else 
        (:this is an ancestor of an overridden concept so we must be in overridemode:)
        if ($concept[.//@id=$overrideconcepts/@id]) then (
            <concept>
            {
                (:attribute ttt {2},:)
                attribute id {local:getnewid($decorProject, 'DE')},
                $effectiveDate,
                attribute oldid {$concept/@id},
                $concept/(@* except (@id|@effectiveDate|@minimumMultiplicity|@maximumMultiplicity)),
                if ($concept[@minimumMultiplicity|@maximumMultiplicity]) then (
                    $concept/(@minimumMultiplicity|@maximumMultiplicity),
                    $concept/condition
                )
                else (
                    $transactionconcept//@minimumMultiplicity[1],
                    $transactionconcept//@maximumMultiplicity[1],
                    $transactionconcept/condition
                )
                ,
                $concept/(* except (condition|concept|inherit|relationship|operationalization|valueDomain)),
                <relationship type="SPEC" ref="{$concept/@id}" flexibility="{$concept/@effectiveDate}">
                {
                    (:get terminology associations on concept from original, if any:)
                    if ($concept[@minimumMultiplicity|@maximumMultiplicity] or empty($originalconcept)) then () else (
                        $originalconcept/ancestor::decor/project/@prefix,
                        art:getConceptAssociations($originalconcept)[@conceptId=$originalconcept/@id]
                    )
                }
                </relationship>
                ,
                $concept/relationship,
                for $childconcept in $concept/concept
                return local:expandconcept($decorProject, $datasets, $childconcept, $overrideconcepts, $overridemode, $effectiveDate)
            }
            </concept>
        )
        else 
        (:this is the starting point for inheritance optionally with override, but ... we've just encountered a circular reference :)
        if ($concept/ancestor::concept[@id = $concept/inherit/@ref][@effectiveDate = $concept/inherit/@effectiveDate]) then (
            <concept>
            {
                
                if ($overrideconcepts | $concept/concept) then (
                    (: this really cannot be handled. Apparently we have a circular reference that includes override :)
                    error(xs:QName('error:CircularReferenceFound'),concat('DCM::Id "',$concept/ancestor::dataset/@id,'" ',$concept/ancestor::dataset/name[1],
                            ' concept "',$concept/name[1],'" has a circular reference to a concept with id="',$concept/inherit/@ref,
                            '". This circular reference would be handled as a concept[contains] if it did not have override concepts too. This is not supported.'))
                )
                else (
                    attribute id {local:getnewid($decorProject, 'DE')},
                    $effectiveDate,
                    $concept/(@* except (@id|@effectiveDate|@minimumMultiplicity|@maximumMultiplicity|@type)),
                    if ($concept[@minimumMultiplicity|@maximumMultiplicity]) then (
                        $concept/(@minimumMultiplicity|@maximumMultiplicity)
                    ) else (
                        $transactionconcept//@minimumMultiplicity[1],
                        $transactionconcept//@maximumMultiplicity[1]
                    )
                    ,
                    <contains ref="{$concept/inherit/@ref}" flexibility="{$concept/inherit/@effectiveDate}"/>
                )
            }
            </concept>
        )
        else 
        (:this is the starting point for inheritance optionally with override.:)
        if ($concept[inherit]) then (
            let $check              := 
                if ($originalconcept or $concept[not(concept)]) then () else (
                    error(xs:QName('error:ReferenceNotFound'),concat('DCM::Id "',$concept/ancestor::dataset/@id,'" ',$concept/ancestor::dataset/name[1],
                        ' concept "',$concept/name[1],'" references a concept with id="',$concept/inherit/@ref,
                        '", but it cannot be found. This means we are unable to apply the specialization on concept ',
                        string-join($concept/concept/concat(name[1],' (',@id,')'),' / '),'. This concept/these concepts will not be imported.'))
                )
            
            return (
                if ($originalispart) then (
                    (: if the target concept is an nl.zorg.part.* creates inherits for concepts contained by the root concept. :)
                    if ($overridemode) then () else (
                        <relationship type="SPEC" ref="{$originalconcept/@id}" flexibility="{$originalconcept/@effectiveDate}"/>
                    )
                    ,
                    for $child in $originalconcept/concept
                    return
                        local:inheritconcept($child, $effectiveDate, $datasets, $decorProject)
                ) else (
                    <concept>
                    {
                        (:attribute ttt {3,$overridemode},:)
                        attribute id {local:getnewid($decorProject, 'DE')},
                        $effectiveDate,
                        attribute oldid {$originalconcept/@id},
                        $concept/(@* except (@id|@effectiveDate|@minimumMultiplicity|@maximumMultiplicity|@type)),
                        if ($concept[@minimumMultiplicity|@maximumMultiplicity]) then (
                            $concept/(@minimumMultiplicity|@maximumMultiplicity)
                        ) else (
                            $transactionconcept//@minimumMultiplicity[1],
                            $transactionconcept//@maximumMultiplicity[1]
                        )
                        ,
                        if ($concept[concept]) then (
                            (:entering overridemode:)
                            attribute type {'group'},
                            $originalconcept/(* except (concept|inherit|relationship|operationalization|valueDomain)),
                            <relationship type="SPEC" ref="{$concept/inherit/@ref}" flexibility="{$concept/inherit/@effectiveDate}"/>,
                            $originalconcept/relationship
                            ,
                            for $childconcept in $originalconcept/concept
                            return local:expandconcept($decorProject, $datasets, $childconcept, ($overrideconcepts | $concept/concept), true(), $effectiveDate)
                        ) else (
                            (: just handle as contains :)
                            if ($originalconcept) then
                                $originalconcept/condition
                            else (
                                $transactionconcept/condition
                            )
                            ,
                            (:if ($originalconcept) then (
                                <inherit ref="{$originalconcept/@id}" effectiveDate="{$originalconcept/@effectiveDate}">
                                {
                                    if ($concept[@minimumMultiplicity|@maximumMultiplicity] or empty($originalconcept)) then () else (
                                        $originalconcept/ancestor::decor/project/@prefix,
                                        art:getConceptAssociations($originalconcept)
                                    )
                                }
                                </inherit>
                            )
                            else (
                                $concept/inherit,
                                <comment language="{$defaultLanguage}">Referentie niet gevonden tijdens import. ({substring(string(current-dateTime()),1,19)})</comment>,
                                for $lang in $otherLanguages
                                return
                                    <comment language="{$lang}">Reference not found during import. ({substring(string(current-dateTime()),1,19)})</comment>
                            ):)
                            <contains ref="{$originalconcept/@id}" flexibility="{$originalconcept/@effectiveDate}"/>
                            ,
                            $originalconcept/comment
                        )
                    }
                    </concept>
                )
            )
        )
        else 
        if ($overridemode) then (
            <concept>
            {
                let $check              := 
                    if ($concept[@minimumMultiplicity|@maximumMultiplicity] | $transactionconcept) then () else (
                        error(xs:QName('error:ReferenceNotFound'),concat('DCM::Id "',$concept/ancestor::dataset/@id,'" ',
                        $concept/ancestor::dataset/name[1],' concept "',$concept/@id,
                        '", does not have any transaction information that we can find.'))
                    )
                
                return (
                    (:attribute ttt {4},:)
                    attribute id {local:getnewid($decorProject, 'DE')},
                    $effectiveDate,
                    attribute oldid {$concept/@id},
                    $concept/(@* except (@id|@effectiveDate|@minimumMultiplicity|@maximumMultiplicity|@type)),
                    if ($concept[@minimumMultiplicity|@maximumMultiplicity]) then (
                        $concept/(@minimumMultiplicity|@maximumMultiplicity),
                        $concept/condition
                    ) else (
                        $transactionconcept//@minimumMultiplicity[1],
                        $transactionconcept//@maximumMultiplicity[1],
                        $transactionconcept/condition
                    )
                    ,
                    <inherit ref="{$concept/@id}" effectiveDate="{$concept/@effectiveDate}">
                    {
                        if ($concept[@minimumMultiplicity|@maximumMultiplicity] or empty($originalconcept)) then () else (
                            $originalconcept/ancestor::decor/project/@prefix,
                            art:getConceptAssociations($originalconcept)
                        )
                    }
                    </inherit>
                    ,
                    $concept/comment,
                    if ($originalconcept[valueDomain[@type = 'TimeInterval']]) then (
                        let $TimeIntervalDS     := $datasets/dataset[name = $TimeIntervalDSname]
                        
                        for $c in $TimeIntervalDS/concept/concept
                        return
                            <concept>
                            {
                                $c/@minimumMultiplicity , $c/@maximumMultiplicity , attribute id {local:getnewid($decorProject, 'DE')}, $effectiveDate, $concept/@statusCode,
                                <inherit ref="{$c/@id}" effectiveDate="{$c/@effectiveDate}"/>,
                                $c/comment,
                                for $cc in $c/concept return local:getconceptinherit($decorProject, $cc, $effectiveDate, $concept/@statusCode)
                            }
                            </concept>
                    ) else ()
                    ,
                    for $childconcept in $concept/concept
                    return local:expandconcept($decorProject, $datasets, $childconcept, $overrideconcepts, $overridemode, $effectiveDate)
                )

            }
            </concept>
        ) else 
        if ($concept[valueDomain[@type = 'TimeInterval']]) then (
            <concept>
            {
                $concept/@id,
                $effectiveDate,
                attribute type {'group'},
                $concept/(@* except (@id|@effectiveDate|@minimumMultiplicity|@maximumMultiplicity|@type)),
                if ($concept[@minimumMultiplicity|@maximumMultiplicity]) then (
                    $concept/(@minimumMultiplicity|@maximumMultiplicity),
                    $concept/condition
                ) else (
                    $transactionconcept//@minimumMultiplicity[1],
                    $transactionconcept//@maximumMultiplicity[1],
                    $transactionconcept/condition
                )
                ,
                $concept/(* except (valueDomain|concept|condition|operationalization))
                ,
                let $TimeIntervalDS     := $datasets/dataset[name = $TimeIntervalDSname]
                return (
                    <relationship type="SPEC" ref="{$TimeIntervalDS/concept/@id}" flexibility="{$TimeIntervalDS/concept/@effectiveDate}"/>,
                    $concept/operationalization,
                    for $c in $TimeIntervalDS/concept/concept
                    return
                        <concept>
                        {
                            $c/@minimumMultiplicity , $c/@maximumMultiplicity , attribute id {local:getnewid($decorProject, 'DE')}, $effectiveDate, $concept/@statusCode,
                            <inherit ref="{$c/@id}" effectiveDate="{$c/@effectiveDate}"/>,
                            $c/comment,
                            for $cc in $c/concept return local:getconceptinherit($decorProject, $cc, $effectiveDate, $concept/@statusCode)
                        }
                        </concept>
                )
            }
            </concept>
        ) else (
            <concept>
            {
                (:attribute ttt {5},:)
                $concept/@id,
                $effectiveDate,
                if ($concept[inherit]) then () else if ($concept[concept]) then (attribute type {'group'}) else (attribute type {'item'}),
                $concept/(@* except (@id|@effectiveDate|@minimumMultiplicity|@maximumMultiplicity|@type)),
                if ($concept[@minimumMultiplicity|@maximumMultiplicity]) then (
                    $concept/(@minimumMultiplicity|@maximumMultiplicity),
                    $concept/condition
                ) else (
                    $transactionconcept//@minimumMultiplicity[1],
                    $transactionconcept//@maximumMultiplicity[1],
                    $transactionconcept/condition
                )
                ,
                $concept/(* except (concept|condition))
                ,
                for $childconcept in $concept/concept
                return local:expandconcept($decorProject, $datasets, $childconcept, $overrideconcepts, $overridemode, $effectiveDate)
            }
            </concept>
        )
};

declare %private function local:inheritconcept($concept as element(concept), $effectiveDate as item(), $datasets as element(datasets), $decorProject as element(decor)) as element(concept)+ {
    let $originalconcept    := 
        if ($concept[inherit]) then (
            if ($datasets//concept[@id=$concept/inherit/@ref]) then
                $datasets//concept[@id=$concept/inherit/@ref]
            else (
                error(xs:QName('error:ReferenceNotFound'),concat('DCM::Id "',$concept/ancestor::dataset/@id,'" ',$concept/ancestor::dataset/name[1],
                        ' concept "',$concept/name[1],'" references a concept with id="',$concept/inherit/@ref,
                        '", but it cannot be found. This means we are unable to apply the specialization on concept ',
                        string-join($concept/concept/concat(name[1],' (',@id,')'),' / '),'. This concept/these concepts will not be imported.'))
                
                (:  get previously created/imported concept from decor/data 
                    have to compare without @effectiveDate as it might/will differ from previous imports. Without effectiveDate, we'll get the latest version:)
                (:art:getOriginalForConcept(<concept><inherit ref="{$concept/inherit/@ref}"/></concept>):)
            )
        ) else ()
    let $originalispart     := local:inheritispart(local:getOriginalForConcept($originalconcept, $datasets))

    return
        if ($originalispart) then (
            for $child in $originalconcept/concept
            return
                local:inheritconcept($child, $effectiveDate, $datasets, $decorProject)
        ) else (
            <concept>
            {
                attribute id {local:getnewid($decorProject, 'DE')},
                $effectiveDate,
                $concept/(@* except (@id|@type|@effectiveDate)),
                <inherit ref="{$concept/@id}" effectiveDate="{$concept/@effectiveDate}"/>,
                for $child in $concept/concept
                return
                    local:inheritconcept($child, $effectiveDate, $datasets, $decorProject)
            }
            </concept>
        )
};

declare %private function local:getOriginalForConcept($concept as element(concept)?, $datasets as element(datasets)) as element(concept)? {
    if ($concept[inherit]) then (
        let $concepts   := $datasets//concept[@id=$concept/inherit/@ref]
        let $error      := 
            if (count($concepts) le 1) then () else (
                error(xs:QName('error:ClassParentError'),concat('Concept id="',$concept/inherit/@ref,'" effectiveDate="',$concept/inherit/@effectiveDate,'" gave multiple hits. Expected exactly 1'))
            )
        return
            local:getOriginalForConcept($concepts, $datasets)
    )
    else 
    if ($concept[contains]) then (
        let $concepts   := $datasets//concept[@id=$concept/contains/@ref]
        let $error      := 
            if (count($concepts) le 1) then () else (
                error(xs:QName('error:ClassParentError'),concat('Concept id="',$concept/contains/@ref,'" effectiveDate="',$concept/contains/@flexibility,'" gave multiple hits. Expected exactly 1'))
            )
        return
            local:getOriginalForConcept($concepts, $datasets)
    )
    else (
        $concept
    )
};

(: we want to know if this is referencing an nl.zorg.part, except FarmaceutischProduct (2.16.840.1.113883.2.4.3.11.60.40.3.9.7) and 
        GebruiksInstructie (2.16.840.1.113883.2.4.3.11.60.40.3.9.12), so we can expand those, saving use cases a lot of work :)
declare %private function local:inheritispart($concept as element(concept)?) as xs:boolean {
    (:exists($originalconcept/ancestor::dataset[not(@id = ('2.16.840.1.113883.2.4.3.11.60.40.3.9.7', '2.16.840.1.113883.2.4.3.11.60.40.3.9.12'))]/name[starts-with(., 'nl.zorg.part.')]):)
    exists($concept/ancestor::dataset[not(@id = ('2.16.840.1.113883.2.4.3.11.60.40.3.9.7'))]/name[starts-with(., 'nl.zorg.part.')])
};

(: relevant for TimeInterval child concepts :)
declare %private function local:getconceptinherit($decorProject as element(decor), $concept as element(concept), $effectiveDate as item(), $statusCode as item()) as element(concept) {
    <concept>
    {
        $concept/@minimumMultiplicity , $concept/@maximumMultiplicity , attribute id {local:getnewid($decorProject, 'DE')}, $effectiveDate, $statusCode,
        <inherit ref="{$concept/@id}" effectiveDate="{$concept/@effectiveDate}"/>,
        $concept/comment,
        for $cc in $concept/concept return local:getconceptinherit($decorProject, $cc, $effectiveDate, $statusCode)
    }
    </concept>
};
declare %private function local:getnewid($decorProject as element(decor), $type as xs:string) as xs:string {
    let $currid := $decorProject/project/@newid
    let $newid  := string(xs:integer($currid[1]) + 1)
    let $update := update value $currid[1] with $newid
    
    return concat($oidBaseDE,'.',$newid)
};
(: ==== DCM LEVEL FUNCTIONS ==== :)

declare %private function local:getdcmroot($object as element()) as element(UML:Package) {
    $object/ancestor-or-self::UML:Model/UML:Namespace.ownedElement/UML:Package[local:getTaggedValue(.,'stereotype')='DCM']
};

(:  <UML:TaggedValue tag="DCM::Id" xmi.id="EAID_4574860E_541F_403a_9C8B_E2421FE545A7" value="2.16.840.1.113883.2.4.3.11.60.40.3.12.5" modelElement="MX_EAID_A42E3995_0040_494e_ABEF_9AE12D626CC7"/>:)
declare %private function local:getdcmid($object as element()) as item() {
    (:1-4-2015#NOTES#The date this version was made.:)
    let $id     := local:getidfromstring(local:getTaggedValue($object/ancestor-or-self::UML:Model,'DCM::Id'))
    
    return
        if ($id) then (
            attribute id {$id}
        ) else (
            error(xs:QName('error:UnsupportedValue'),concat('ZIB "',local:getname(local:getdcmroot($object)),'" DCM::Id "',$id,'" not a valid OID.'))
        )
};

(:  <UML:TaggedValue tag="DCM::PublicationDate" xmi.id="EAID_CF6EF5C7_0A05_4185_A49F_E1A60A916E79" value="1-4-2015#NOTES#The date this version was made." modelElement="MX_EAID_A42E3995_0040_494e_ABEF_9AE12D626CC7"/>
    <UML:TaggedValue tag="DCM::CreationDate" xmi.id="EAID_CA0E4293_E76D_4878_9BFF_EF3BC1606AB6" modelElement="MX_EAID_A42E3995_0040_494e_ABEF_9AE12D626CC7"/>
:)
declare %private function local:getdcmeffectivedate($object as element()) as item() {
    let $date       := local:xmidate2datetime(local:getTaggedValue($object/ancestor-or-self::UML:Model,'DCM::PublicationDate'))
    (:let $date       := concat(substring(string(current-date()), 1, 10), 'T00:00:00'):)
    return
        if ($date) then (
            attribute effectiveDate {$date}
        ) else (
            error(xs:QName('error:UnsupportedValue'),concat('ZIB "',local:getname(local:getdcmroot($object)),'" DCM::PublicationDate "',$date,'" not a valid date stamp dd-mm-yyyy or d-m-yyyy.'))
        )
};

(:  <UML:TaggedValue tag="DCM::PublicationDate" xmi.id="EAID_CF6EF5C7_0A05_4185_A49F_E1A60A916E79" value="1-4-2015#NOTES#The date this version was made." modelElement="MX_EAID_A42E3995_0040_494e_ABEF_9AE12D626CC7"/>:)
declare %private function local:getdcmpublicationdate($object as element()) as item()? {
    let $date       := local:xmidate2datetime(local:getTaggedValue($object/ancestor-or-self::UML:Model,'DCM::PublicationDate'))
    
    return
        if ($date) then (
            attribute officialReleaseDate {$date}
        ) else ()
};

(:  <UML:TaggedValue tag="DCM::DeprecatedDate" xmi.id="EAID_76EE629F_2C29_4c40_9589_28A990C1FBDE" modelElement="MX_EAID_A42E3995_0040_494e_ABEF_9AE12D626CC7"/>:)
declare %private function local:getdcmdeprecationdate($object as element()) as item()? {
    let $date       := local:xmidate2datetime(local:getTaggedValue($object/ancestor-or-self::UML:Model,'DCM::DeprecatedDate'))
    
    return
        if ($date) then (
            attribute expirationDate {$date}
        ) else ()
};

(:  <tag name="DCM::LifecycleStatus" value="Final" />
    
    <tag name="DCM::PublicationStatus" value="Published">The status of the DCM on that moment. Strike out the other status. All status levels are mentioned after the process of development and review are clear (concept).
        Author Draft(en);
        Committee Draft(en);
        Organisation Draft(en);
        Submitted(en);
        Withdrawn
        Rejected(en) 
        Obsolete
        Approved for testing
        Approved for Production Use
        Superseded</tag>
    <UML:TaggedValue tag="DCM::PublicationStatus" xmi.id="EAID_33A08AE6_B44E_422a_A4C1_2399B41F1D16" value="Prepublished#NOTES#The status of the DCM on that moment. Strike out the other status.
        All status levels are mentioned after the process of development and review are clear (concept).
        
        Author Draft(en);
        Committee Draft(en);
        Organisation Draft(en);
        Submitted(en);
        Withdrawn
        Rejected(en) 
        Obsolete
        Approved for testing
        Approved for Production Use
        Superseded" modelElement="MX_EAID_356EAE3F_22FC_4b97_9BEC_6E34D661FC2C"/>
:)
declare %private function local:getdcmstatuscode($object as element()) as item() {
    let $lifecyclestatus    := local:getTaggedValue($object/ancestor-or-self::UML:Model,'DCM::LifecycleStatus')
    let $publicationstatus  := local:getTaggedValue($object/ancestor-or-self::UML:Model,'DCM::PublicationStatus')
    
    return
    switch ($lifecyclestatus) 
    case 'Final'        return attribute statusCode {'final'}
    case 'Draft'        return attribute statusCode {'draft'}
    default             return error(xs:QName('error:UnsupportedValue'),concat('ZIB "',local:getname(local:getdcmroot($object)), '" DCM::LifecycleStatus "',$lifecyclestatus,'" not mapped yet.'))
};

(:  <name>nl.nfu.Ademhaling-v1.0</name>:)
declare %private function local:getdcmversionlabel($object as element()) as item()? {
    let $versionlabel   := local:getTaggedValue($object/ancestor-or-self::UML:Model,'DCM::Version')
    
    return
        if (matches($versionlabel,'^v?\d+\.\d+')) then
            attribute versionLabel {$versionlabel}
        else ()
};

(:  <UML:TaggedValue tag="DCM::Name" xmi.id="EAID_93434A5A_A47B_4ccd_A140_60A7144E4507" value="nl.nfu.Ademhaling" modelElement="MX_EAID_A42E3995_0040_494e_ABEF_9AE12D626CC7"/>:)
declare %private function local:getdcmname($xmiroot as element()) as element(name)+ {
    let $name           := local:getTaggedValue($xmiroot/ancestor-or-self::UML:Model,'DCM::Name')
    let $name           := <name language="{$defaultLanguage}">{data($name)}</name>
    
    let $aliasnames     := local:getTaggedValue(($xmiroot/ancestor-or-self::UML:Model/UML:Namespace.ownedElement/UML:Package)[1],'alias')
    let $aliasnames     := 
        for $aliasname in $aliasnames
        return
            if ($aliasname[starts-with(.,'EN:')]) then (
                <name language="en-US">{normalize-space(substring-after($aliasname,'EN:'))}</name>
            )
            else ( (:no idea what language this would be:) )
    
    return
        $name | $aliasnames
};

(:  <notes>Rootconcept van de bouwsteen Ademhaling. Dit rootconcept bevat alle gegevenselementen van de bouwsteen Ademhaling.</notes>:)
declare %private function local:getdcmdesc($xmiroot as element(), $headings as xs:string*) as element(desc)* {
    let $paragraphs     := 
        for $p in $headings
        let $pkg        := local:getPackageByName($xmiroot,$p)
        let $par        := local:linktext(local:cleanuptext(local:getTaggedValue($pkg,'documentation')))
        return
            <par head="{$p}">{local:parseLanguage($par)}</par>
    
    let $langs          := if ($paragraphs[*]) then distinct-values($paragraphs/*/name()) else ()
    
    let $descriptions   :=
        for $lang in $langs
        return
            <desc language="{$lang}">
            {
                for $p in $headings
                let $par        := $paragraphs[@head=$p]/*[name()=$lang]
                let $pkg        := local:getPackageByName($xmiroot,$p)
                let $imgs       := if ($pkg) then local:getImages($pkg) else ()
                return
                    if ($par | $imgs) then (
                        <p><b>{$p}</b></p> | <p>{if ($par | $imgs) then $par/node() else ('-'), $imgs}</p>
                    ) else (
                        (:skip empty paragraphs:)
                    )
            }
            </desc>
    
    return $descriptions
};
declare %private function local:getprojectdesc($xmiroot as element()) as element(desc)* {
    let $paragraphs     := 
        for $p in $dcmprojheadings
        let $pkg        := local:getPackageByName($xmiroot,$p)
        let $par        := local:linktext(local:cleanuptext(local:getTaggedValue($pkg,'documentation')))
        return
            <par head="{$p}">{local:parseLanguage($par)}</par>
    
    let $langs          := distinct-values($paragraphs/*/name())
    
    let $descriptions   :=
        for $lang in $langs
        return
            <desc language="{$lang}">
            {
                if ($lang = 'nl-NL') then (
                    <p><b>Algemeen</b></p> | <p>--</p>
                ) else (
                    <p><b>General</b></p> | <p>--</p>
                )
                ,
                for $p in $dcmprojheadings
                let $par        := $paragraphs[@head=$p]/*[name()=$lang]
                let $pkg        := local:getPackageByName($xmiroot,$p)
                let $imgs       := 
                    if ($pkg) then local:getImages($pkg) else (
                        (:error(xs:QName('error:MissingPackage'),concat('ZIB "',local:getname(local:getdcmroot($xmiroot)), '" missing package "', $p, '"')):)
                    )
                return
                    if ($par | $imgs) then (
                        <p><b>{$p}</b></p> | <p>{if ($par | $imgs) then $par/node() else ('-'), $imgs}</p>
                    ) else (
                        (:skip empty paragraphs:)
                    )
            }
            </desc>
    
    return $descriptions
};

(: Example return
<nl-NL>Om geneesmiddelallergieën in het werkproces van voorschrijvers en apothekers te ...</nl-NL>
<en-US>For all allergies in the Netherlands, the codes available in the G standard are ...</en-US>

&lt;languages xml:space=&#34;preserve&#34;&gt;&#xA;
    &lt;nl-NL&gt;Het observeren van de spontane ... pulmonaire systeem van de patiënt.&lt;/nl-NL&gt;&#xA;
    &lt;en-US&gt;Observing a person’s spontaneous ... pulmonary system.&lt;/en-US&gt;&#xA;
&lt;/languages&gt;
:)
declare %private function local:parseLanguage($str as xs:string?) as element()* {
let $str    := if (starts-with($str, '&amp;lt;')) then replace(replace($str, '&amp;gt;', '>'), '&amp;lt;', '<') else $str
let $t      := 
    if (string-length($str) = 0) then () else (
        if (matches($str, '&lt;languages xml:space=&#34;preserve&#34;&gt;')) then
            art:parseNode(<x>{$str}</x>)/*
        else (
            art:parseNode(
                <x>
                {
                    if (matches($str, '^\[([a-z]{2}-[A-Z]{2})\]')) then
                        replace(replace($str,'\[([a-z]{2}-[A-Z]{2})\]','<$1>'), '\[\\([a-z]{2}-[A-Z]{2})\]','</$1>')
                    else 
                        concat('<',$defaultLanguage,'>',$str,'</',$defaultLanguage,'>')
                }
                </x>
            )
        )
    )
(:handle empty a hrefs:)
let $t      :=
    for $n in $t/*[matches(name(), '^[a-z]{2}-[A-Z]{2}$')]
    return local:patchLanguage($n)

return
    $t

};
declare %private function local:patchLanguage($node as item()?) as item()* {
    if ($node instance of element()) then (
        if ($node[self::*:a][@href = '']) then (
            let $link   := normalize-space(string-join(data($node),''))
            return
            <a href="{$link}">{$node/(@* except @href), $link}</a>
        )
        else
        if ($node[self::*:a][*:font/*:u]) then (
            let $link   := normalize-space(string-join(data($node),''))
            return
            <a href="{$link}">{$node/(@* except @href), $link}</a>
        )
        else (
            element {name($node)} {$node/@*, for $subnode in $node/node() return local:patchLanguage($subnode)}
        )
    )
    else ($node)
};

(:  <UML:DiagramElement geometry="Left=32;Top=30;Right=491;Bottom=285;" subject="EAID_AEDE6B76_65D3_46f1_9320_B110CCC553BC" seqno="1" style="DUID=5EC58C6E;ImageID=1664747453;"/>:)
(:  <EAImage xmlns:dt="urn:schemas-microsoft-com:datatypes" name="Image9131" type="ENHMetafile" imageID="389291236" dt:dt="bin.base64">..</EAImage>:)
(:  <EAImage xmlns:dt="urn:schemas-microsoft-com:datatypes" name="Image11840" type="Bitmap" imageID="1664747453" dt:dt="bin.base64">..</EAImage>:)
declare %private function local:getImages($package as element()) as element(img)* {
    let $diagram    := $package/ancestor::XMI.content/UML:Diagram[@owner=$package/@xmi.id]
    let $xmiroot    := local:getdcmroot($package)
    
    for $diagramelement in $diagram/UML:Diagram.element/UML:DiagramElement
    let $imageid    := tokenize($diagramelement/tokenize(@style,';')[starts-with(.,'ImageID')],'=')[2]
    let $image      := $package/ancestor::XMI/EAModel.image/EAImage[@imageID=$imageid]
    let $imagename  := string-join((local:getdcmname($xmiroot)[1],local:getdcmversionlabel($xmiroot),$package/@name,$imageid,$diagramelement/@seqno),'-')
    let $imagename  := concat(replace($imagename,'[^A-Za-z0-9\.\-]',''),'.',local:getImageExtensionByType($image/@type))
    let $store      := xmldb:store($imagecollection,$imagename,xs:base64Binary($image))
    let $geometry   := local:getdiagramboundaries($diagramelement/@geometry)
    order by $diagramelement/xs:integer(@seqno)
    return 
        <img src="{concat($imagehttpbase,'/',$imagename)}" alt="{$imagename}" style="width:{$geometry[3] - $geometry[1]}px; height:{$geometry[4] - $geometry[2]}px;"/>
    
};
(:this is a weak point in our import. defaults to png because that is mostly true. emf is not a websupported format but happens to be supported by EA:)
declare %private function local:getImageExtensionByType($type as xs:string) as xs:string {
    switch ($type)
    case ('Bitmap') return 'png'
    case ('ENHMetafile') return 'emf'
    default return 'png'
};

(:  <UML:TaggedValue tag="DCM::CoderList" xmi.id="EAID_499B3150_B9D8_4fd8_A35C_8EBB66651167" value="Kerngroep Registratie aan de Bron" modelElement="MX_EAID_A42E3995_0040_494e_ABEF_9AE12D626CC7"/>:)
declare %private function local:getdcmtags($xmiroot as element()) as element(tag)* {
    let $tags           := $xmiroot/ancestor::XMI.content/UML:TaggedValue[@modelElement=../UML:Model/@xmi.id][not(@tag=$dcmtags)]
    
    for $tag in $tags[starts-with(@tag,'DCM::')][@value[not(.='')]]
    let $value          := tokenize($tag/@value,'#')[1]
    let $notes          := <property name="{$tag/@tag}">{local:cleanuptext($value)}</property>
    return
        art:parseNode($notes)
};

(: ==== DCM LEVEL FUNCTIONS ==== :)
(: ==== HELPER FUNCTIONS    ==== :)
declare %private function local:getallrelationships($object as element()) as element(UML:Association.connection)* {
    $object/../UML:Association/UML:Association.connection[UML:AssociationEnd[@isNavigable = 'true'][@type = $object/@xmi.id]]
};
declare %private function local:getallchildren($object as element()) as element()* {
    let $allrelationships   := local:getallrelationships($object)
    return
        $object/ancestor::UML:Package//*[@xmi.id = $allrelationships/UML:AssociationEnd[@isNavigable='false']/@type]
};
declare %private function local:getallparents($object as element()) as element()* {
    let $allrelationships   := $object/../UML:Association[local:getTaggedValue(.,'ea_sourceType')='Class'][local:getTaggedValue(.,'ea_targetType')='Class']/UML:Association.connection[UML:AssociationEnd[@isNavigable='false'][@type=$object/@xmi.id]]
    return
        $object/ancestor::UML:Package//*[@xmi.id=$allrelationships/UML:AssociationEnd[@isNavigable='true']/@type]
};

declare %private function local:getchildconcepts($object as element()) as element()* {
    let $allchildren   := local:getallchildren($object)
    
    for $child in $allchildren[self::UML:Class]
    order by xs:integer(local:getTaggedValue($child,'tpos'))
    return $child[local:getTaggedValue(.,'stereotype')=('container','data','context','reference')]
};

declare %private function local:getgeneralizations($object as element()) as element(UML:Generalization)* {
    $object/../UML:Generalization[@subtype=$object/@xmi.id]
};
declare %private function local:getdependencies($object as element()) as element(UML:Dependency)* {
    $object/../UML:Dependency[@supplier=$object/@xmi.id]
};
declare %private function local:getassociatedconstraints($object as element(), $parent as element()?) as element(UML:Constraint)* {
    if ($parent) then (
        let $association  := $object/../UML:Association[UML:Association.connection[UML:AssociationEnd[@isNavigable='false'][@type=$object/@xmi.id]][UML:AssociationEnd[@type=$parent/@xmi.id]]]
        
        return
            if ($association) then 
                $object/../UML:Constraint[contains(local:getTaggedValue(.,'relatedlinks'),$association/@xmi.id)]
            else ()
    ) else ()
};
declare %private function local:getassociatedboundaries($object as element(), $parent as element()?) as element(UML:ClassifierRole)* {
    if ($parent) then (
        let $association  := $object/../UML:Association[local:getTaggedValue(.,'ea_targetType') = 'Boundary'][UML:Association.connection[UML:AssociationEnd[@isNavigable='false'][@type=$object/@xmi.id]]]
        
        return
            if ($association) then 
                $object/../UML:Collaboration/UML:Namespace.ownedElement/UML:ClassifierRole[@xmi.id = $association/UML:Association.connection/UML:AssociationEnd[@isNavigable='true']/@type]
            else ()
    ) else ()
};
declare %private function local:getconstraints($object as element()+) as element(UML:Constraint)* {
    (:<EANoteLink xmi.id="EAID_409CA25D_E703_4f09_A833_10286BC4B1DE" source="EAID_882590D2_80A1_4edc_BF17_DB5F1E1495BB" target="EAID_9C9C3F9D_FB38_4528_B088_FDFCD867EA84">:)
    (:<UML:Constraint xmi.id="EAID_9C9C3F9D_FB38_4528_B088_FDFCD867EA84" visibility="public" namespace="EAPK_D1A4F12C_2E43_4670_AC69_814BB21A8E8A">
        <UML:ModelElement.taggedValue>
            <UML:TaggedValue tag="documentation" value="Als Toegestaan = Ja, maar met beperkingen (JA_MAAR)"/>
            ...
        </UML:ModelElement.taggedValue>
    </UML:Constraint>:)
    (
        for $link in $object/ancestor::XMI//EANoteLink[@source=$object/@xmi.id]
        return $object/ancestor::UML:Package[1]/UML:Namespace.ownedElement/UML:Constraint[@xmi.id=$link/@target]
        ,
        for $link in $object/ancestor::XMI//EANoteLink[@target=$object/@xmi.id]
        return $object/ancestor::UML:Package[1]/UML:Namespace.ownedElement/UML:Constraint[@xmi.id=$link/@source]
    )
};

(: Pre 2017: Is this object (Class) part of a Collaboration object like a ChoiceBox :)
declare %private function local:getcollaborationforobject($object as element(), $parent as element()?) as element(UML:Collaboration)? {
    let $geometry       := local:getobjectgeometry($object, $parent)
    let $oleft          := $geometry[1]
    let $otop           := $geometry[2]
    let $oright         := $geometry[3]
    let $obottom        := $geometry[4]
    let $collaborations := local:getcollaborations($object)
    
    return
        (:
            <object-geometry>461 172 591 242</object-geometry> AdemHaling
            <object-geometry>234 171 383 241</object-geometry> AdemhalingDatumTijd
            <object-geometry>393 457 514 527</object-geometry> ToedieningHulpmiddel::MedischHulpmiddel
            <collab-geometry>241 433 520 538</collab-geometry>
        :)
        for $collaboration in $collaborations
        let $collaborationgeometry  := local:getcollaborationgeometry($collaboration, $parent)
        let $cleft      := $collaborationgeometry[1]
        let $ctop       := $collaborationgeometry[2]
        let $cright     := $collaborationgeometry[3]
        let $cbottom    := $collaborationgeometry[4]
        return
            (:assume that if left and top are within boundary it is a match:)
            if ($oleft >= $cleft and $oleft <= $cright) then (
                if ($otop >= $ctop and $otop <= $cbottom) then (
                    $collaboration
                ) else ()
            ) else ()
};
(: Pre 2017 :)
declare %private function local:getothercollaborationobjects($collaboration as element(UML:Collaboration), $object as element(), $parent as element()?) as element()* {
    let $otherobjects   := $object/../UML:Class[not(@xmi.id=$object/@xmi.id)]
    
    for $otherobject in $otherobjects
    let $coll   := local:getcollaborationforobject($otherobject, $parent)
    return
        if ($coll and $coll[@xmi.id=$collaboration/@xmi.id]) then $otherobject else ($coll)
};
(: Since 2017 :)
declare %private function local:getotherboundaryobjects($boundary as element(UML:ClassifierRole), $object as element(), $parent as element()?) as element(UML:Class)* {
    let $otherobjects   := $object/../*[not(@xmi.id = ($object/@xmi.id | $boundary/ancestor::UML:Collaboration/@xmi.id))]
    
    for $otherobject in $otherobjects
    let $otherobjectboundaries      := local:getassociatedboundaries($otherobject, $parent)
    return
        (: if object has a connection to the same boundary ....:)
        if ($otherobjectboundaries[@xmi.id = $boundary/@xmi.id]) then 
            (: we only support other Classes in a Boundary :)
            if ($otherobject[not(self::UML:Class)]) then
            
                error(xs:QName('error:ClassBoundaryError'),concat('ZIB "',local:getname(local:getdcmroot($object)),
                    '" current object/id "',                        local:getTaggedValue($object,'ea_localid'),'" ',local:getname($object), 
                    ' is connected to a boundary id=',              local:getTaggedValue($boundary,'ea_localid'),
                    ' that contains a ', name($otherobject),' id=', local:getTaggedValue($otherobject,'ea_localid'), '. We only support other Class objects.'))
            
            else ($otherobject)
        else ()
};

declare %private function local:getcollaborationgeometry($object as element(), $parent as element()?) as xs:integer+ {
    local:getdiagramboundaries(local:getcollaborationdiagram($object, $parent)/@geometry)
};
declare %private function local:getobjectgeometry($object as element(), $parent as element()?) as xs:integer+ {
    local:getdiagramboundaries(local:getobjectdiagram($object, $parent)/@geometry)
};
declare %private function local:getcollaborations($object as element()) as element(UML:Collaboration)* {
    $object/../UML:Collaboration[UML:Namespace.ownedElement/UML:ClassifierRole[local:getTaggedValue(.,'ea_eleType')='element']]
};
declare %private function local:getcollaborationdiagram($object as element(UML:Collaboration), $parent as element()?) as element(UML:DiagramElement) {
    let $diagram    :=
        for $classifierRole in $object/../UML:Collaboration/UML:Namespace.ownedElement/UML:ClassifierRole[local:getTaggedValue(.,'ea_eleType')='element']
        return
            local:getobjectdiagram($classifierRole, $parent)
    
    return
        if (count($diagram)=1) then (
            $diagram
        )
        else if (count($diagram)>1) then (
            error(xs:QName('error:CollaborationDiagramError'),concat('ZIB "',local:getname(local:getdcmroot($object)), '" object/id "', local:getTaggedValue($object,'ea_localid'),'" ',local:getname($object), ' has more than one diagram. (',$object/name(),' ',$object/@xmi.id,')'))
        )
        else (
            error(xs:QName('error:CollaborationDiagramError'),concat('ZIB "',local:getname(local:getdcmroot($object)), '" object/id "', local:getTaggedValue($object,'ea_localid'),'" ',local:getname($object), ' does not have a diagram. (',$object/name(),' ',$object/@xmi.id,')'))
        )
};
declare %private function local:getobjectdiagram($object as element(), $parent as element()?) as element(UML:DiagramElement) {
    let $diagram    :=  
        if ($parent) then (
            $object/ancestor-or-self::XMI.content//UML:Diagram[@owner=$parent/ancestor-or-self::UML:Package[1]/@xmi.id]//UML:DiagramElement[@subject=$object/@xmi.id]
        ) else (
            $object/ancestor-or-self::XMI.content//UML:DiagramElement[@subject=$object/@xmi.id]
        )
    
    return
        if (count($diagram)=1) then (
            $diagram
        )
        else if (count($diagram)>1) then (
            error(xs:QName('error:ObjectDiagramError'),concat('ZIB "',local:getname(local:getdcmroot($object)),
                '" object/id "', local:getTaggedValue($object,'ea_localid'),'" ',local:getname($object),
                ' has more than one diagram. (',$object/name(),' ',$object/@xmi.id,')'))
        )
        else (
            error(xs:QName('error:ObjectDiagramError'),concat('ZIB "',local:getname(local:getdcmroot($object)),
                '" object/id "', local:getTaggedValue($object,'ea_localid'),'" ',local:getname($object),
                ' does not have a diagram. (',$object/name(),' ',$object/@xmi.id,')'))
        )
};
(:Left=438;Top=206;Right=716;Bottom=315;:)
declare %private function local:getdiagramboundaries($geometry as xs:string) as xs:integer+ {
    let $parts  := tokenize($geometry,';')
    let $left   := tokenize($parts[starts-with(.,'Left')],'=')[2]
    let $top    := tokenize($parts[starts-with(.,'Top')],'=')[2]
    let $right  := tokenize($parts[starts-with(.,'Right')],'=')[2]
    let $bottom := tokenize($parts[starts-with(.,'Bottom')],'=')[2]
    
    let $check  := 
        if ($left castable as xs:integer and
            $right castable as xs:integer and
            $top castable as xs:integer and
            $bottom castable as xs:integer) then () else (
            error(xs:QName('error:UnsupportedValue'),concat('Diagram boundaries are not all integers left="',$left,'", right="',$right,'", top="',$top,'", bottom="',$bottom,'". Original: ',$geometry))
        )
    
    return (xs:integer($left), xs:integer($top), xs:integer($right), xs:integer($bottom))
};

(: Input string that either -is- an OID or -contains- an OID. Output is just the OID :)
declare %private function local:getidfromstring($idstring as xs:string?) as xs:string? {
    let $idstring   := tokenize(normalize-space($idstring), '#')[1]
    
    let $id         :=
        if (matches($idstring,'^[0-2](\.(0|[1-9]\d*))*$')) then (
            $idstring
        )
        else if (starts-with($idstring,'OID')) then (
            tokenize(replace($idstring,'OID\s*:?\s*',''),'\s')[1]
        )
        else if (matches($idstring,'NL-CM-VS[:\-]\s*\d')) then (
            tokenize(replace($idstring,'NL-CM-VS:\s*',concat($oidBaseVS,'.')),'\s')[1]
        )
        else if (matches($idstring,'^NL-CM[:\-]\s*\d')) then (
            tokenize(replace($idstring,'NL-CM[:\-]\s*',concat($oidBaseEL,'.')),'\s')[1]
        )
        else ()
    return
        if (matches($id,'^[0-2](\.(0|[1-9]\d*))*$')) then ($id) else ()
};

(: Get TaggedValue by @tag name from an element, or nothing :)
declare %private function local:getTaggedValue($object as element()?,$tag as xs:string) as xs:string* {
    $object/UML:ModelElement.taggedValue/UML:TaggedValue[@tag=$tag]/@value |
    $object/ancestor-or-self::XMI.content/UML:TaggedValue[@tag=$tag][@modelElement=$object/@xmi.id]/@value
};

(: Get Package by @name from an element, or nothing :)
declare %private function local:getPackageByName($object as element()?,$name as xs:string) as element()? {
    $object/ancestor-or-self::UML:Package//UML:Package[@name=$name]
};

(: Get the @multiplicity from the non-navigable counterpart of an association :)
declare %private function local:getMultiplicity($object as element(), $parent as element()?, $objectIsPartOfChoice as xs:boolean) as xs:string? {
    let $allparentrelationships := if ($parent) then local:getallrelationships($parent) else ()
    let $objectmultiplicity     := $allparentrelationships/UML:AssociationEnd[@type=$object/@xmi.id][local:getTaggedValue(.,'ea_end')='source']/@multiplicity
    
    return
        if ($objectmultiplicity or not($objectIsPartOfChoice)) then ($objectmultiplicity) else (
            let $parentOfParent     := local:getallparents($parent)
             (:note: will lead to error if multiple parents exist:)
            let $parentmultiplicity := local:getMultiplicity($parent, $parentOfParent, false())
            let $min                := '0'
            let $max                := if ($parentmultiplicity castable as xs:integer) then $parentmultiplicity else tokenize($parentmultiplicity,'\.\.')[2]
            let $max                := if (string-length($max)>0) then $max else ('*')
            return
                concat($min,'..',$max)
        )
};

(: Input string d-m-yyyy (single or double digits allowed for day and month. Output date/time string YYYY-MM-DDT00:00:00 or nothing :)
declare %private function local:xmidate2datetime($date as xs:string?) as xs:string? {
    let $date       := tokenize($date, '#')[1]
    let $year       := tokenize($date, '-')[3]
    let $month      := tokenize($date, '-')[2]
    let $month      := if (string-length($month)=1) then concat('0',$month) else ($month)
    let $day        := tokenize($date, '-')[1]
    let $day        := if (string-length($day)=1) then concat('0',$day) else ($day)
    
    let $newdate    := string-join(($year,$month,$day),'-')
    
    return
        if ($newdate castable as xs:date) then (concat($newdate,'T00:00:00')) else ()
};

declare %private function local:cleanuptext($text as xs:string?) as xs:string? {
    replace(replace($text,'(\[\\[a-z]{2}-[A-Z]{2}\])\s*','$1'),'(&#xD;)?&#xA;','<br/>')
};
declare %private function local:linktext($text as xs:string?) as xs:string? {
    let $r  := replace($text,'(ZIB-\d+)','<a href="https://bits.nictiz.nl/browse/$1" target="_new">$1</a>')
    let $r  := replace($r, '\$inet:/?/?', '')
    
    return $r
};
(: ====         HELPER FUNCTIONS        ==== :)
(: ==== CLASS/CONTAINER LEVEL FUNCTIONS ==== :)

(: Get DECOR valueDomain type based on Generalization of input element:)
declare %private function local:getdcmdatatype($object as element(UML:Class)) as xs:string? {
    let $generalizations    := local:getgeneralizations($object)
    (:<UML:TaggedValue tag="ea_localid" value="7891"/>:)
    let $commondatatype     := $object/ancestor::XMI//EAStub[@xmi.id=$generalizations/@supertype]/@name
    let $commondatatype     :=
        if (not($commondatatype=('CD','CO')) and exists(local:getTaggedValue($object,'DCM::ValueSet'))) then 'CD' else ($commondatatype)

    return if (count($generalizations)=1 and count($commondatatype)=1) then ($commondatatype) else ()
};
declare %private function local:getValueDomainType($object as element(UML:Class)) as xs:string {
    let $generalizations    := local:getgeneralizations($object)
    (:<UML:TaggedValue tag="ea_localid" value="7891"/>:)
    let $commondatatype     := $object/ancestor::XMI//EAStub[@xmi.id=$generalizations/@supertype]/@name
    let $commondatatype     :=
        if (not($commondatatype=('CD','CO')) and exists(local:getTaggedValue($object,'DCM::ValueSet'))) then 'CD' else ($commondatatype)
    
    return
    if (count($generalizations)=1 and count($commondatatype)=1) then (
        switch ($commondatatype)
        case 'ANY'  return 'complex'
        case 'BL'   return 'boolean'
        case 'CD'   return 'code'
        case 'CO'   return 'ordinal'
        case 'ED'   return 'blob'
        case 'II'   return 'identifier'
        case 'INT'  return 'count'
        case 'PQ'   return 'quantity'
        case 'ST'   return 'string'
        case 'TS'   return 'datetime'
        case 'UCUM' return 'code'
        case 'TimeInterval' return 'complex' (: nieuw in 2017 ... :)
        default     return 
        error(xs:QName('error:ClassDatatypeError'),concat('ZIB "',local:getname(local:getdcmroot($object)),'" object/id "',$object/@xmi.id,'" ',local:getname($object),' has a Generalization relationship to "',$commondatatype,'", hence we do not know what the datatype should be.')) 
    )
    else if (count($generalizations)=0) then (
        error(xs:QName('error:ClassDatatypeError'),concat('ZIB "',local:getname(local:getdcmroot($object)),'" object/id "',$object/@xmi.id,'" ',local:getname($object),' does not have a Generalization relationship, hence we do not know what the datatype should be.'))
    )
    else if (count($generalizations)>1) then (
        error(xs:QName('error:ClassDatatypeError'),concat('ZIB "',local:getname(local:getdcmroot($object)),'" object/id "',$object/@xmi.id,'" ',local:getname($object),' has multiple Generalization relationships, hence we do not know what the datatype should be.'))
    )
    else if (count($commondatatype)=0) then (
        error(xs:QName('error:ClassDatatypeError'),concat('ZIB "',local:getname(local:getdcmroot($object)),'" object/id "',$object/@xmi.id,'" ',local:getname($object),' does not have a corresponding UML:Class in Common.xmi for id=',$generalizations/destId,', hence we do not know what the datatype should be.'))
    )
    else (
        error(xs:QName('error:ClassDatatypeError'),concat('ZIB "',local:getname(local:getdcmroot($object)),'" object/id "',$object/@xmi.id,'" ',local:getname($object),' maps onto multiple datatypes in Common.xmi, hence we do not know what the datatype should be.'))
    )
};

declare %private function local:getoperationalization($object as element()) as element(operationalization)* {
    let $tags       := (local:getTaggedValue($object,'DCM::AssigningAuthority') , $object/UML:ModelElement.constraint/UML:Constraint/@name)
    let $content    :=
        for $tag at $i in $tags
        let $tagParts   := tokenize($tag,'#')
        let $value      := $tagParts[1]
        let $notes      := $tagParts[3]
        order by lower-case($value)
        return (
            if ($notes) then concat($value,' (',string-join($notes,'#'),')') else ($value)
        )
    
    return
        if (exists($content)) then (
            for $lang in ($defaultLanguage, $otherLanguages)
            return
                <operationalization language="{$lang}">
                {
                    if (count($content)=1) then ($content) else (
                        <ul>{for $c in $content return <li>{$c}</li>}</ul>
                    )
                }
                </operationalization>
        ) else ()
};

(: Type = count or quantity + Operationalization "Waardebereik: 0-20" --> 
        <property minInclude="0" maxInclude="20"/>
   Type = count or quantity + Operationalization "Waardebereik: 0-" --> 
        <property minInclude="0"/>
   Type = count or quantity + Operationalization "Waardebereik: -10,00 - 0,00" --> 
        <property minInclude="-10.00" maxInclude="0.00"/>
   Type = count or quantity + Operationalization "Waardebereik: -25,00 - +25,00" -->
        <property minInclude="-25.00" maxInclude="25.00"/>        
   Type = count or quantity + Operationalization "Waardebereik: 0,00 and 40,00" -->
        ignore. unclear if this is a range or just two valid values
   Type = count or quantity + Operationalization "Waardebereik: -20" --> 
        ignore. unclear if this is a negative value or a max range
   Type = count or quantity  + Operationalization <operationalization language="nl-NL"><ul>
                                        <li>Waardebereik 0-100</li>
                                        <li>Waardebereik: 0-10</li>
                                     </ul></operationalization>
        ignore, because you cannot make that computable without additional rules when to do what
   Type = identifier + Operationalization "UZOVI (OID: 2.16.840.1.113883.2.4.6.4)" --> 
        <identifierAssociation conceptId="id" conceptFlexibility="effectiveDate" ref="2.16.840.1.113883.2.4.6.4"/>
   Type = identifier + Operationalization <operationalization language="nl-NL"><ul>
                                            <li>UZI register abonneenummer (URA) (OID: 2.16.528.1.1007.3.3)</li>
                                            <li>Vektis AGB-zorgverlener tabel (OID: 2.16.840.1.113883.2.4.6.1)</li>
                                          </ul></operationalization>
        <identifierAssociation conceptId="id" conceptFlexibility="effectiveDate" ref="2.16.528.1.1007.3.3"/>
        <identifierAssociation conceptId="id" conceptFlexibility="effectiveDate" ref="2.16.840.1.113883.2.4.6.1"/>
:)
declare %private function local:getvaluedomainproperties($operationalization as element(operationalization)?, $conceptId as xs:string, $conceptEff as xs:string, $valuedomaintype as xs:string) as element()* {
    if ($valuedomaintype = ('count', 'quantity')) then (
        if (count($operationalization//text()) = 1) then (
            for $str in $operationalization//text()/normalize-space()
            let $range  := 
                if (matches($str, '^Waarden?bereik:\s([+\-]?\d+([,\.]\d+)?)\s?-\s?([+\-]?\d+([,\.]\d+)?)?$')) then (
                    replace($str, '^Waarden?bereik:\s([+\-]?\d+([,\.]\d+)?)\s?-\s?([+\-]?\d+([,\.]\d+)?)?$', '$1#$3')
                ) else ()
            let $min    := replace(replace(tokenize($range, '\s?#\s?')[1], '^\+', ''), ',', '.')
            let $max    := replace(replace(tokenize($range, '\s?#\s?')[2], '^\+', ''), ',', '.')
            return 
                if (string-length($min) = 0) then () else (
                    <property minInclude="{$min}">
                    {
                        if (string-length($max) = 0) then () else attribute maxInclude {$max}
                    }
                    </property>
                )
        ) else ()
    ) else
    if ($valuedomaintype = 'identifier') then (
        if ($operationalization) then (
            for $str in $operationalization//text()/normalize-space()
            let $oid    := if (matches($str, '^.*\(OID:\s?([\d\.]+)\s?\)$')) then replace($str, '^.*\(OID:\s?([\d\.]+)\s?\)$', '$1') else ()
            return
                if (string-length($oid) = 0) then () else (
                    <identifierAssociation conceptId="{$conceptId}" conceptFlexibility="{$conceptEff}" ref="{$oid}"/>
                )
        ) else ()
    )
    else ()
};

(:  <UML:TaggedValue tag="DCM::DefinitionCode" xmi.id="EAID_BBCB97CE_7F9D_4338_B583_F47617DDCEDB" value="NL-CM:12.5.1" modelElement="EAID_2B69B2EB_E972_4de7_BE79_ABC52DDD7350"/>
    Vanaf release 2017:
    <UML:TaggedValue tag="DCM::ConceptId" xmi.id="EAID_BBCB97CE_7F9D_4338_B583_F47617DDCEDB" value="NL-CM:12.5.1" modelElement="EAID_2B69B2EB_E972_4de7_BE79_ABC52DDD7350"/>
    :)
declare %private function local:getid($object as element()) as item() {
    let $ids     := 
        for $id in (local:getTaggedValue($object, 'DCM::ConceptId'), local:getTaggedValue($object,'DCM::DefinitionCode'))
        return local:getidfromstring($id)
    
    return
    attribute id {$ids}
};

(:  <UML:TaggedValue tag="DCM::ReferencedDefinitionCode" xmi.id="EAID_9F338B8D_AB87_4ad1_8983_36A8F66AA6BD" value="NL-CM:10.1.1#NOTES#Dit is een verwijzing naar het concept MedischHulpmiddel in de bouwsteen MedischHulpmiddel." modelElement="EAID_8ABF3E56_806E_4c07_8BDA_D2384089D7A3"/>:)
(:  Vanaf Release 2017:
    <UML:TaggedValue tag="DCM::ReferencedConceptId"      xmi.id="EAID_9F338B8D_AB87_4ad1_8983_36A8F66AA6BD" value="NL-CM:10.1.1#NOTES#&lt;languages xml:space=&#34;preserve&#34;&gt;&#xA;&lt;nl-NL&gt;Dit is een verwijzing naar het rootconcept van de bouwsteen MedischHulpmiddel.&lt;/nl-NL&gt;&#xA;&lt;en-US&gt;This is a reference to rootconcept of information model MedicalDevice.&lt;/en-US&gt;&#xA;&lt;/languages&gt;" modelElement="EAID_8ABF3E56_806E_4c07_8BDA_D2384089D7A3"/>:)
declare %private function local:getinheritid($object as element()) as item() {
    let $ids     := 
        for $id in ($object/ancestor::XMI.content/UML:TaggedValue[@tag='DCM::ReferencedDefinitionCode'][@modelElement=$object/@xmi.id]/@value |
                    $object/ancestor::XMI.content/UML:TaggedValue[@tag='DCM::ReferencedConceptId'][@modelElement=$object/@xmi.id]/@value)
        return local:getidfromstring($id)
    
    return
    attribute ref {$ids}
};

(:  <name>Ademhaling</name>:)
declare %private function local:getname($object as element()) as element(name) {
    <name language="{$defaultLanguage}">{data(tokenize($object/@name,'::?')[1])}</name>
};

(:  <alias>Ademhaling</alias>:)
declare %private function local:getsynonym($object as element()) as element(synonym)* {
    for $alias in local:getTaggedValue($object,'alias')
    return
    <synonym language="{$defaultLanguage}">{normalize-space($alias)}</synonym>
};

(:  <notes>Rootconcept van de bouwsteen Ademhaling. Dit rootconcept bevat alle gegevenselementen van de bouwsteen Ademhaling.</notes>:)
(:  <UML:TaggedValue tag="documentation" value="&amp;lt;languages xml:space=&#34;preserve&#34;&amp;gt;&#xA;&amp;lt;nl-NL&amp;gt;Hulpmiddel dat gebruikt wordt bij de toediening van extra zuurstof aan de patiënt.&amp;lt;/nl-NL&amp;gt;&#xA;&amp;lt;en-US&amp;gt;Device used for administering extra oxygen to the patient.&amp;lt;/en-US&amp;gt;&#xA;&amp;lt;/languages&amp;gt;"/>:)
declare %private function local:getdesc($object as element()) as element(desc)* {
    local:getdesc($object, ())
};
declare %private function local:getdesc($object as element(), $extraText as xs:string?) as element(desc)* {
    let $paragraphs     := <par>{local:parseLanguage(local:cleanuptext(local:getTaggedValue($object,'documentation')))}</par>
    
    let $langs          := distinct-values($paragraphs/*/name())
    
    (: skip <br/> that came from local:cleanuptext :)
    let $descriptions   :=
        for $lang in $langs[not(. = 'br')]
        return
            <desc language="{$lang}">{$paragraphs/*[name()=$lang]/node(), $extraText}</desc>
    
    return $descriptions
};

(:  Get concept type from its relationships:)
declare %private function local:gettype($object as element()) as item()? {
    let $childconcepts  := local:getchildconcepts($object)
    let $inherits       := local:conceptinherits($object)
    
    return
        if ($inherits=true()) then ()
        else if ($childconcepts) then 
            attribute type {'group'}
        else
            attribute type {'item'}
};

(: Vanaf Release 2017 is ReferencedDefinitionCode veranderd in ReferencedConceptId :)
declare %private function local:conceptinherits($object as element()) as xs:boolean {
    exists($object/ancestor::XMI.content/UML:TaggedValue[@tag='DCM::ReferencedDefinitionCode'][@modelElement=$object/@xmi.id][@value] | 
           $object/ancestor::XMI.content/UML:TaggedValue[@tag='DCM::ReferencedConceptId'][@modelElement=$object/@xmi.id][@value])
};

(:  <UML:TaggedValue tag="DCM::ExampleValue" xmi.id="EAID_BA56B245_BC3C_40e8_AF79_F928B28E518F" value="15/min" modelElement="EAID_D2607CE5_7DB7_47c3_A870_277BC5AE4360"/>
    <UML:TaggedValue tag="DCM::ExampleValue" xmi.id="EAID_BA56B245_BC3C_40e8_AF79_F928B28E518F" value="ATC C09BA01#NOTES#CAPTOPRIL" modelElement="EAID_D2607CE5_7DB7_47c3_A870_277BC5AE4360"/>
:)
declare %private function local:getexamples($object as element()) as element(example)* {
    for $tag in local:getTaggedValue($object,'DCM::ExampleValue')
    let $tagParts   := tokenize($tag,'#')
    let $value      := $tagParts[1]
    let $notes      := $tagParts[3]
    order by lower-case($value)
    return
        <example>{if ($notes) then concat($value,' (',string-join($notes,'#'),')') else ($value)}</example>
};
(: ==== CLASS/CONTAINER LEVEL FUNCTIONS ==== :)

(: ==== DECOR FUNCTIONS ==== :)
declare %private function local:xmi2terminology($xmimodels as element(UML:Model)+, $datasets as element(datasets)) as item()* {
    text {'&#x0a;        '},comment {' ================= '},
    text {'&#x0a;        '},comment {concat(' === ZIBS ',$hcimReleaseYear,' === ')},
    text {'&#x0a;        '},comment {' ================= '},
    
    (:do terminologyAssociations:)
    for $xmimodel in $xmimodels
    let $xmiroot    := local:getdcmroot($xmimodel)
    let $xmipkgroot := $xmimodel//UML:Class[local:getTaggedValue(.,'stereotype')='rootconcept']
    let $dcmid      := local:getdcmid($xmiroot)
    let $dcmeff     := local:getdcmeffectivedate($xmiroot)
    let $dcmname    := local:getdcmname($xmimodel)[1]
    return (
        '&#10;',
        comment {$dcmname/text()},
        '&#10;',
        for $object in $xmipkgroot/..//UML:Class
        let $objectid       := local:getid($object)
        order by local:getTaggedValue($object,'ea_localid')
        return (
            (: <UML:TaggedValue tag="DCM::DefinitionCode" xmi.id="EAID_FAAC8396_9438_4d41_AFBD_8751179069A9" value="SNOMED CT: 250774007 Inspired oxygen concentration" modelElement="EAID_1A60E309_3CAE_4ce3_A004_B6CA46D49BD7"/>
               Vanaf release 2017 wordt DCM::DefinitionCode niet meer vor dit doel gebruikt. In principe hoeft dus niet meer op starts-with(., 'NL-CM:') te worden gecontroleerd. De id's zitten vanaf 2017 in DCM::ConceptId
            :)
            (:
                <UML:TaggedValue tag="DCM::DefinitionCode" xmi.id="EAID_1F10F1B0_EF3B_4871_B5B3_34BE90E557BE" value="SNOMED CT: 273302005 Barthel index (assessment scale)"...
                <UML:TaggedValue tag="DCM::DefinitionCode" xmi.id="EAID_3A1DAC1B_85EE_40c4_8D73_225A1057B7E3" value="ScoreObservaties: 12017008 ChecklistPijnGedrag Paniek#NOTES#OID: 2.16.840.1.113883.2.4.3.11.60.40.4.0.1"...
                
                <terminologyAssociation conceptId="..." conceptFlexibility="..." code="12017008" codeSystem="2.1" displayName="ChecklistPijnGedrag Paniek"/>
            :)
            for $tag in local:getTaggedValue($object,'DCM::DefinitionCode')[not(starts-with(.,'NL-CM:'))]
            let $codeSystemName := normalize-space(substring-before($tag,':'))
            (: Deal with possible bug in input where the colon is missing but a space exists, or where the space after the colon is missing :)
            let $codeSystemName := if (string-length($codeSystemName) = 0) then (substring-before($tag,' ')) else $codeSystemName
            let $codeDisp       := replace(normalize-space(substring-after($tag, $codeSystemName)),'^[:\s]+', '')
            let $code           := tokenize($codeDisp,'\s')[1]
            let $displayName    := tokenize(normalize-space(substring-after($codeDisp,$code)), '#')[1]
            let $valueseteff    := local:getdcmeffectivedate($xmiroot)
            let $codeSystem     := if (contains($tag, 'OID: ')) then replace($tag, '.*OID:\s*(\d+(\.\d+)*)\s*', '$1') else ()
            let $codeSystem     :=
                switch ($codeSystemName)
                case 'ICF'                  return '2.16.840.1.113883.6.254'
                case 'LNC'                  return '2.16.840.1.113883.6.1'
                case 'LOINC'                return '2.16.840.1.113883.6.1'
                case 'SNOMED CT'            return '2.16.840.1.113883.6.96'
                case 'SNOMED'               return '2.16.840.1.113883.6.96'
                (:case 'ScoreObservaties'     return '2.16.840.1.113883.2.4.3.11.60.40.4.0.1':)
                default                     return if (art:isOid($codeSystem)) then $codeSystem else (
                        error(xs:QName('error:UnsupportedValue'),concat('ZIB "',local:getname(local:getdcmroot($object)), '" DCM::DefinitionCode "',$tag,'". Could not determine codeSystem OID from "',$codeSystemName,'"'))
                    )
            order by lower-case($tag)
            return (
                for $c in ($datasets/dataset[@id=$dcmid]//*[@id=$objectid] | $datasets/dataset[@id=$dcmid]//*[@oldid=$objectid])
                let $id := $c/@id
                group by $id
                return
                <terminologyAssociation conceptId="{$id[1]}" conceptFlexibility="{$valueseteff}" code="{$code}" codeSystem="{$codeSystem}" displayName="{$displayName}"/>
            )
            ,
            (: <UML:TaggedValue tag="DCM::ValueSet" xmi.id="EAID_6D4E9BEF_4746_4671_B0C9_315CB6E806DF" value="DiepteCodelijst#NOTES#OID: 2.16.840.1.113883.2.4.3.11.60.40.2.12.5.2" modelElement="EAID_5A276647_F413_4de9_AC28_A0F753211A2D"/>
               Vanaf release 2017 is er een nieuwe property bijgekomen DCM::ValueSetId waar de OID direct is te lezen. Hierdoor zou parsing van bovenstaande overbodig kunnen worden:
               <UML:TaggedValue tag="DCM::ValueSetId" xmi.id="EAID_80E8ADEA_8E8C_404c_BAC3_211CA51B34DF" value="2.16.840.1.113883.2.4.3.11.60.40.2.12.5.2" modelElement="EAID_367DA6F9_7516_4d31_91B0_6F412A0C9D03"/>
               De DCM::ValueSetId hangt aan de Class voor de waardelijst/valueSet en is dus niet direct benaderbaar. Parsing van de DCM::ValueSet is dus efficiënter op dit moment. Ook besproken met Albert-Jan
            :)
            for $tag in local:getTaggedValue($object,'DCM::ValueSet')
            let $tagparts       := tokenize($tag,'#')
            let $valuesetname   := $tagparts[1]
            let $valuesetclass  := 
                for $dependency in local:getdependencies($object)
                return
                    $object/../UML:Class[@xmi.id = $dependency/@client][@name = $valuesetname]
            let $vsid           := 
                for $s in local:getTaggedValue($valuesetclass, 'DCM::ValueSetId') 
                return if (string-length($s) gt 0) then normalize-space($s) else ()
            let $strength       := 
                for $s in local:getTaggedValue($valuesetclass, 'DCM::ValueSetBinding') 
                return if (string-length($s) gt 0) then normalize-space($s) else ()
            let $conceptListId  := concat($objectid,'.',replace($dcmeff,'[^\d]',''),'.0')
            let $valuesetid     := local:getidfromstring($tagparts[3])
            let $valueseteff    := local:getdcmeffectivedate($xmiroot)
            let $check          :=
                if ($vsid = $valuesetid) then () else (
                    (:error(xs:QName('error:UnsupportedValue'),concat('ZIB "',local:getname(local:getdcmroot($object)), '" DCM::ValueSet "',$tag,'" holds a different value set id "', $valuesetid,'" than DCM::ValueSetId ', $vsid,'.')):)
                )
            order by $valuesetid
            return (
                for $c in ($datasets/dataset[@id=$dcmid]//*[@id=$conceptListId] | $datasets/dataset[@id=$dcmid]//*[@oldid=$conceptListId])
                let $id := $c/@id
                group by $id
                return
                <terminologyAssociation conceptId="{$id[1]}" valueSet="{$valuesetid}" flexibility="{$valueseteff}">
                {
                    switch (lower-case($strength))
                    case 'required'   return attribute strength {'required'}
                    case 'extensible' return attribute strength {'extensible'}
                    case 'preferred'  return attribute strength {'preferred'}
                    case 'example'    return attribute strength {'example'}
                    case ''           return ()
                    default           return error(xs:QName('error:UnsupportedValue'),concat('ZIB "',local:getname(local:getdcmroot($object)), '" DCM::ValueSetBinding "',$strength,'" for DCM::ValueSet ', $tag,' is not a valid value. Valid values are ''required'', ''extensible'', ''preferred'', ''example''.'))
                }
                </terminologyAssociation>
            )
        )
    )
    ,
    (:get unique terminology associations that we carried over from another project:)
    for $associations in $datasets//*[not(@prefix=$projectPrefix)]/terminologyAssociation
    let $conceptId  := $associations/@conceptId
    let $valueSet   := $associations/@valueSet
    let $code       := $associations/@code
    let $codeSystem := $associations/@codeSystem
    group by $conceptId, $valueSet, $code, $codeSystem
    return
        $associations[1]
    ,
    
    text {'&#x0a;        '},comment {' ================= '},
    text {'&#x0a;        '},comment {concat(' === ZIBS ',$hcimReleaseYear,' === ')},
    text {'&#x0a;        '},comment {' ================= '},
    
    (:do valueSets:)
    for $xmimodel in $xmimodels
    let $xmiroot    := local:getdcmroot($xmimodel)
    let $xmipkgroot := $xmimodel//UML:Class[local:getTaggedValue(.,'stereotype')='rootconcept']
    return (
        for $object in $xmipkgroot/..//UML:Class
        let $objectid       := local:getid($object)
        order by local:getTaggedValue($object,'ea_localid')
        return (
            (:  <UML:TaggedValue tag="DCM::ValueSet" xmi.id="EAID_D1EFFE18_8E40_4e05_9300_39B6B5499FA1" value="ToedieningHulpmiddelCodelijst#NOTES#OID: 2.16.840.1.113883.2.4.3.11.60.40.2.12.5.4" modelElement="EAID_43073D9E_F79A_4b23_B4A2_AE7D95A61E47"/>
                <UML:TaggedValue tag="DCM::ValueSet" xmi.id="EAID_40EACFFA_3D11_4e3d_82A2_DEEB5F3DC845" value="VeroorzakendeStofAllergeneStoffenCodelijst#NOTES#OID: 2.16.840.1.113883.2.4.3.11.60.40.2.8.2.17&#xA;&#xA;Allergene stoffen niet gerelateerd aan medicatiebewaking." modelElement="EAID_DA05F464_3690_43da_968F_ED280FBB5C6C"/>:)
            for $tag in local:getTaggedValue($object,'DCM::ValueSet')
            let $tagparts       := tokenize($tag,'#')
            let $valuesetid     := local:getidfromstring($tagparts[3])
            let $valuesetname   := normalize-space($tagparts[1])
            let $valuesetdisp   := normalize-space(substring-after($tagparts[3], $valuesetid))
            let $valuesetdisp   := if (string-length($valuesetdisp)>0) then $valuesetdisp else $valuesetname
            let $valueseteff    := local:getdcmeffectivedate($xmiroot)
            let $valuesetsts    := local:getdcmstatuscode($xmiroot)
            order by $valuesetid
            return
                local:getdecorvalueset($object, $valuesetid, $valuesetname)
                (:<valueSet id="{$valuesetid}" name="{$valuesetname}" displayName="{$valuesetdisp}" effectiveDate="{$valueseteff}" statusCode="{$valuesetsts}">
                {
                    local:getxmivalueset($object, $valuesetid, $valuesetname)
                }
                </valueSet>:)
        )
    )
    ,
    (:get unique terminology associations that we carried over from another project:)
    for $associations in $datasets//*[not(@prefix = $projectPrefix)]/terminologyAssociation[@valueSet]
    let $conceptId      := $associations/@conceptId
    let $valueSetRef    := $associations/@valueSet
    group by $conceptId, $valueSetRef
    return (
        let $valueSet   := (vs:getValueSetByRef($valueSetRef,$associations[1]/@flexibility,$associations[1]/parent::*/@prefix, false())//valueSet[@id])[1]
        return (
            <valueSet ref="{$valueSet/@id}" name="{$valueSet/@name}" displayName="{if ($valueSet/@displayName[not(.='')]) then $valueSet/@displayName else $valueSet/@name}"/>,
            if ($valueSet/parent::*/@url) then
                <buildingBlockRepository url="{$valueSet/parent::*/@url}" ident="{$valueSet/parent::*/@ident}"/>
            else (
                <buildingBlockRepository url="{$decorServiceUrl}" ident="{$valueSet/parent::*/@ident}"/>
            )
        )
    )
};

declare %private function local:getdecorvalueset($object as element(), $id as xs:string?, $name as xs:string?) as element(valueSet)? {
    let $valueSet   := 
        if (empty($id)) then $preparedvalsets[lower-case(@name) = lower-case($name)] else $preparedvalsets[@id = $id]
    
    return
    if (count($valueSet) > 1) then (
        error(xs:QName('error:ValueSetError'),concat('ZIB "',local:getname(local:getdcmroot($object)),'" object/id "',local:getTaggedValue($object,'ea_localid'),'" ',local:getname($object),' refers to valueset id=',$id,' name=',$name,' but this valueSet has multiple hits in the ',count($preparedvalsets),' prepared value sets'))
    ) else
    if ($valueSet) then (
        <valueSet>
        {
            $valueSet/@id,
            $valueSet/@name,
            $valueSet/@displayName,
            attribute effectiveDate {local:getdcmeffectivedate($object)},
            attribute statusCode {local:getdcmstatuscode($object)},
            $valueSet/(@* except (@id|@name|@displayName|@effectiveDate|@statusCode)),
            $valueSet/node()
        }
        </valueSet>
    ) else (
        (:error(xs:QName('error:ValueSetError'),concat('ZIB "',local:getname(local:getdcmroot($object)),'" object/id "',local:getTaggedValue($object,'ea_localid'),'" ',local:getname($object),' refers to valueset id=',$id,' name=',$name,' but this valueSet is not found in the ',count($preparedvalsets),' prepared value sets')):)
    )
};

declare %private function local:xmi2scenario($xmimodel as element(UML:Model), $i as xs:integer, $datasets as element(dataset)*) as element(scenario) {
let $xmiroot            := local:getdcmroot($xmimodel)
let $datasetid          := local:getdcmid($xmiroot)
let $dataset            := $datasets[@id=$datasetid]
let $transactionlabel   := replace(string-join(($dataset/name[1],$dataset/@versionLabel),'-'),'\+','_')

let $check          :=
    if (count($dataset)=1) then () 
    else if (count($dataset)=0) then (
        error(xs:QName('error:InternalScenarioError'),concat('ZIB "',local:getname(local:getdcmroot($xmimodel)),'" does not have a corresponding dataset with id ',$datasetid))
    )
    else (
        error(xs:QName('error:InternalScenarioError'),concat('ZIB "',local:getname(local:getdcmroot($xmimodel)),'" has multiple datasets with id ',$datasetid))
    )

return
    (:<scenario id="2.16.840.1.113883.2.4.3.11.60.3000.3.2" effectiveDate="" expirationDate="" officialReleaseDate="" statusCode="" versionLabel="">:)
    <scenario id="{$oidBaseSC}{substring-after($datasetid, $oidBaseZIB)}">
    {
        $dataset/@effectiveDate,
        $dataset/@statusCode,
        $dataset/@versionLabel,
        $dataset/@officialReleaseDate,
        $dataset/@expirationDate,
        $dataset/name,
        local:getdcmdesc($xmiroot, $dcmscenheadings)
    }
        <transaction id="{$oidBaseTRgroup}{substring-after($datasetid, $oidBaseZIB)}" type="group">
        {
            $dataset/@effectiveDate,
            $dataset/@statusCode
        }
        {
            for $lang in ($defaultLanguage, $otherLanguages)
            return
                if ($lang = 'nl-NL') then 
                    <name language="{$lang}">Transactiegroep</name>
                else (
                    <name language="{$lang}">Transaction Group</name>
                )
        }
            <transaction id="{$oidBaseTRitem}{substring-after($datasetid, $oidBaseZIB)}" type="stationary" model="ClinicalDocument" label="{$transactionlabel}">
            {
                $dataset/@effectiveDate,
                $dataset/@statusCode
            }
            {
                for $lang in ($defaultLanguage, $otherLanguages)
                return
                    if ($lang = 'nl-NL') then 
                        <name language="{$lang}">Registratie</name>
                    else (
                        <name language="{$lang}">Registration</name>
                    )
            }
            {
                for $lang in ($defaultLanguage, $otherLanguages)
                return
                    <desc language="{$lang}">-</desc>
            }
                <actors>
                    <actor id="{$oidBaseAC}.1" role="sender"/>
                </actors>
                <representingTemplate sourceDataset="{$datasetid}" sourceDatasetFlexibility="{$dataset/@effectiveDate}">
                {
                    for $concept in $dataset//concept[@id]
                    return
                        <concept ref="{$concept/@id}" flexibility="{$concept/@effectiveDate}">
                        {
                            if ($concept[condition]) then (
                                attribute conformance {'C'},
                                for $condition in $concept/condition
                                return
                                    <condition>
                                    {
                                        $concept/@minimumMultiplicity,
                                        $concept/@maximumMultiplicity,
                                        $concept/@conformance,
                                        $concept/@isMandatory,
                                        $condition/node()
                                    }
                                    </condition>
                                ,
                                if ($concept/condition[@conformance='NP']) then () else (<condition conformance="NP"/>)
                            ) else (
                                $concept/@minimumMultiplicity,
                                $concept/@maximumMultiplicity,
                                $concept/@conformance,
                                $concept/@isMandatory
                            )
                        }
                        </concept>
                }
                </representingTemplate>
            </transaction>
        </transaction>
    </scenario>
};

declare %private function local:main() as item()* {
(:let $remove         := if (doc-available(concat($resultcollection,'/',$resultfile))) then xmldb:remove($resultcollection,$resultfile) else ():)
let $xml            := 
    document {
        <?xml-stylesheet type="text/xsl" href="https://assets.art-decor.org/ADAR/rv/DECOR2schematron.xsl"?> |
        <?xml-model href="https://assets.art-decor.org/ADAR/rv/DECOR.sch" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?> |
        <decor  xmlns:cda="urn:hl7-org:v3" xmlns:hl7="urn:hl7-org:v3" xmlns:nfu="urn:urn:nictiz-nl:v3/nfu" xmlns:pharm="urn:ihe:pharm:medication" xmlns:sdtc="urn:hl7-org:sdtc"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="https://assets.art-decor.org/ADAR/rv/DECOR.xsd" private="false" repository="true" 
                nfu:dummy-1="urn:urn:nictiz-nl:v3/nfu" xsi:dummy-2="http://www.w3.org/2001/XMLSchema-instance" pharm:dummy-3="urn:ihe:pharm:medication" sdtc:dummy-4="urn:hl7-org:sdtc">
            <!-- Project -->
        </decor>
    }
let $projectFile    := xmldb:store($resultcollection,$resultfile, $xml)
let $decorProject   := doc($projectFile)/decor

(:let $error          := error(xs:QName('error:BreakError'), 'Tot hier en niet verder'):)

let $xml            := 
    <project id="{$oidProject}" prefix="{$projectPrefix}" experimental="false" defaultLanguage="{$defaultLanguage}" newid="0">
    {
        for $lang in ($defaultLanguage, $otherLanguages)
        return
            if ($lang = 'nl-NL') then 
                <name language="{$lang}">Zorginformatiebouwstenen (ZIB) {$hcimReleaseYear}</name>
            else (
                <name language="{$lang}">Health and Care Information Models (HCIM) {$hcimReleaseYear}</name>
            )
    }
    {
        (:get project description based on first model:)
        (:local:getprojectdesc(local:getdcmroot($xmimodels[not(util:document-name(.)='Common.xmi')][1])):)
        ()
    }
        <desc language="nl-NL">
            <p>
                <b>Algemeen</b>
            </p>
            <p>Zorginformatiebouwstenen worden gebruikt om inhoudelijke (niet technische) afspraken vast te leggen ten behoeve van het standaardiseren van informatie, die gebruikt wordt in het zorgproces. Het doel van de standaardisatie is dat deze informatie uit het zorgproces wordt hergebruikt voor andere doeleinden zoals kwaliteitsregistraties, overdracht of patiëntgebonden onderzoek. Een zorginformatiebouwsteen is een informatiemodel, waarin een zorginhoudelijk concept wordt beschreven in termen van de gegevenselementen waaruit dat concept bestaat, de datatypes van die gegevenselementen
                etc. Zorginformatiebouwstenen zijn informatiemodellen van minimale klinische concepten, die elk meerdere gegevens in zich herbergen met een afgesproken inhoud, structuur en onderlinge relatie.</p>
            <p>
                <b>Disclaimer</b>
            </p>
            <p>Het programma ‘Registratie aan de Bron’ is een samenwerking tussen de NFU (de Nederlandse Federatie van Universitair Medische Centra) en Nictiz, hierna de samenwerkingspartijen genoemd. Aan ‘Registratie aan de Bron’ hebben ook andere partijen buiten de umc’s en Nictiz een belangrijke bijdrage geleverd en daarbinnen samengewerkt. De samenwerkingspartijen besteden de grootst mogelijke zorg aan de betrouwbaarheid en actualiteit van de gegevens in deze Zorginformatiebouwstenen (Klinische Bouwsteen). Onjuistheden en onvolledigheden kunnen echter voorkomen. De samenwerkingspartijen
                zijn niet aansprakelijk voor schade als gevolg van onjuistheden of onvolledigheden in de aangeboden informatie, noch voor schade die het gevolg is van problemen veroorzaakt door, of inherent aan het verspreiden van informatie via het internet, zoals storingen of onderbrekingen van of fouten of vertraging in het verstrekken van informatie of diensten door de samenwerkingspartijen of door u aan de samenwerkingspartijen via een website of via e-mail, of anderszins langs elektronische weg.<br/>
                <br/>Tevens aanvaarden de samenwerkingspartijen geen aansprakelijkheid voor eventuele schade die geleden wordt als gevolg van het gebruik van gegevens, adviezen of ideeën verstrekt door of namens de samenwerkingspartijen via deze Zorginformatiebouwsteen.<br/>De samenwerkingspartijen aanvaarden geen verantwoordelijkheid voor de inhoud van informatie in deze Zorginformatiebouwsteen waarnaar of waarvan met een hyperlink of anderszins wordt verwezen.<br/>In geval van tegenstrijdigheden in de genoemde Zorginformatiebouwsteen documenten en bestanden geeft de meest recente en hoogste
                versie van de vermelde volgorde in de revisies de prioriteit van de desbetreffende documenten weer. Indien informatie die in de elektronische versie van deze Zorginformatiebouwsteen is opgenomen ook schriftelijk wordt verstrekt, zal in geval van tekstverschillen de schriftelijke versie bepalend zijn. Dit geldt indien de versieaanduiding en datering van beiden gelijk is. Een definitieve versie heeft prioriteit echter boven een conceptversie. Een gereviseerde versie heeft prioriteit boven een eerdere versie.</p>
            <p>
                <b>Terms of Use</b>
            </p>
            <p>De gebruiker mag de informatie van deze Zorginformatiebouwsteen zonder beperking gebruiken. Voor het kopiëren, verspreiden en doorgeven van de informatie van deze Zorginformatiebouwsteen gelden de copyrightbepalingen uit de betreffende paragraaf.</p>
            <p>
                <b>Copyrights</b>
            </p>
            <p>De gebruiker mag de informatie van deze Zorginformatiebouwsteen kopiëren, verspreiden en doorgeven, onder de voorwaarden, die gelden voor Creative Commons licentie <u>Naamsvermelding-NietCommercieel-GelijkDelen 3.0 Nederland (CC BY-NC-SA-3.0)</u>, <br/>
                <u>
                    <br/>
                </u>De inhoud is beschikbaar onder de Creative Commons Naamsvermelding-NietCommercieel-GelijkDelen 3.0 <br/>(zie ook <a href="http://creativecommons.org/licenses/by-nc-sa/3.0/nl/">
                    <font color="#0000ff">
                        <u>http://creativecommons.org/licenses/by-nc-sa/3.0/nl/</u>
                    </font>
                </a>)</p>
            <p>
                <b>Conversie naar ART-DECOR</b>
            </p>
            <p>Bij iedere publicatie van de Zorginformatiebouwstenen wordt de set geautomatiseerd geïmporteerd in ART-DECOR voor verdere verwerking informatiestandaarden. Een ZIB is een functioneel model met gedefinieerde elementen in een bepaalde verhouding ten opzichte van elkaar (kardinaliteit) die bepaalt of deze elementen optioneel zijn of verplicht en of ze kunnen herhalen. In ART-DECOR valt dit uiteen in deze delen: <ul>
                    <li>Een dataset met de definitie en de (boom)structuur. Onder definitie worden ook concept- en waardelijstkoppelingen gerekend</li>
                    <li>Een transactie met de kardinaliteit en eventuele condities zoals bij keuzeboxen</li>
                    <li>Voor iedere ZIB is er 1 dataset en 1 transactie van het type registratie die onder 1 transactiegroep, onder 1 scenario zit.</li>
                </ul>
                <table>
                    <tr style="text-align: left; background-color: lightgrey;">
                        <th style="padding-right: 10px;">Zorginformatiebouwsteen eigenschap</th>
                        <th style="padding-right: 10px;">Voorbeeld ZIB</th>
                        <th style="padding-right: 10px;">ART-DECOR</th>
                        <th style="padding-right: 10px;">Voorbeeld ART-DECOR</th>
                    </tr>
                    <tr>
                        <td style="padding-right: 10px;">ZIB id (DCM::Id)<br/>
                            <br/>
                            <br/>
                            <br/>Waardelijst id</td>
                        <td style="padding-right: 10px;">2.16.840.1.113883.2.4.3.11.60.40.3.<b>12.5</b>
                            <br/>
                            <br/>
                            <br/>
                            <br/>2.16.840.1.113883.2.4.3.11.60.40.2.12.5.1</td>
                        <td style="padding-right: 10px;">Dataset id<br/>Scenario id<br/>Transactiegroep id<br/>Transactie id<br/>Waardelijst id</td>
                        <td style="padding-right: 10px;">
                            <i>idem</i>
                            <br/>
                            <i>{$oidBaseSC}</i>.<b>12.5</b>
                            <br/>
                            <i>{$oidBaseTRgroup}</i>.<b>12.5</b>
                            <br/>
                            <i>{$oidBaseTRitem}</i>.<b>12.5</b>
                            <br/>
                            <i>idem</i>
                        </td>
                    </tr>
                    <tr style="background-color: GhostWhite ;">
                        <td style="padding-right: 10px;">ZIB publicatiedatum (DCM::PublicationDate)</td>
                        <td style="padding-right: 10px;">1-4-2015</td>
                        <td style="padding-right: 10px;">Dataset versie<br/>Scenario versie<br/>Transactiegroep versie<br/>Transactie versie<br/>Waardelijst versie</td>
                        <td style="padding-right: 10px;">2015-04-01T00:00:00</td>
                    </tr>
                    <tr>
                        <td style="padding-right: 10px;">ZIB versie (DCM::Version)</td>
                        <td style="padding-right: 10px;">3.1</td>
                        <td style="padding-right: 10px;">Dataset versielabel<br/>Scenario versielabel<br/>Transactiegroep versielabel<br/>Transactie versie<br/>Waardelijst versielabel</td>
                        <td style="padding-right: 10px;">
                            <i>idem</i>
                        </td>
                    </tr>
                    <tr style="background-color: GhostWhite ;">
                        <td style="padding-right: 10px;">ZIB versie (DCM::LifecycleStatus)</td>
                        <td style="padding-right: 10px;">Final</td>
                        <td style="padding-right: 10px;">Dataset status<br/>Scenario status<br/>Transactiegroep status<br/>Transactie status<br/>Waardelijst status</td>
                        <td style="padding-right: 10px;">final</td>
                    </tr>
                </table>
            </p>
        </desc>
        <desc language="en-US">
            <p>
                <b>General</b>
            </p>
            <p>Health and Care Information models (HCIM), or Clinical Building Blocks (CBBs) or Zorginformatiebouwstenen (zib's) are used to capture functional, semantic (non technical) agreements for the standardization of information used in the care process. The purpose of the standardization is that this information from the care process is reused for other purposes such as quality registration, transfer or patient-related research. A HCIM is an information model in which a care-based concept is described in terms of the data elements from which that concept exists, the data types of those
                data elements, etc. Care information building blocks are information models of minimal clinical concepts, each containing multiple data with agreed content, structure and mutual relationship.</p>
            <p>
                <b>Disclaimer</b>
            </p>
            <p>The programme ‘Registratie aan de Bron’ [Registration at the Source] is a collaboration between the NFU (Nederlandse Federatie van Universitair Medische Centra, [Netherlands Federation of University Medical Centers]) and Nictiz, hereinafter referred to as the partners. Other parties outside the university medical centers and Nictiz also made important contributions to and worked within ‘Registration at the Source’. The parties pay the utmost attention to the reliability and topicality of the data in these Healthcare Information Building Blocks (Clinical Building Block).
                Omissions and inaccuracies may however occur. The parties are not liable for any damages resulting from omissions or inaccuracies in the information provided, nor are they liable for damages resulting from problems caused by or inherent to distributing information on the internet, such as malfunctions, interruptions, errors or delays in information or services provided by the parties to you or by you to the parties via a website or via e-mail, or any other digital means. <br/>
                <br/>The parties will also not accept liability for any damages resulting from the use of data, advice or ideas provided by or on behalf of the parties by means of this Healthcare Information Building Block. <br/>The parties will not accept any liability for the content of information in this Healthcare Information Building Block to which or from which a hyperlink is referred. <br/>In the event of contradictions in said Healthcare Information Building Block documents and files, the most recent and highest version of the listed order in the revisions will indicate the priority
                of the documents in question. If information included in the digital version of this Healthcare Information Building Block is also distributed in writing, the written version will be leading in cases of textual differences. This will apply if both have the same version number and date. A definitive version has priority over a draft version. A revised version has priority over previous versions.</p>
            <p>
                <b>Terms of Use</b>
            </p>
            <p>The user may use the information in this Healthcare Information Building Block without limitations. The copyright provisions in the paragraph in question apply to copying, distributing and passing on information from this Healthcare Information Building Block.</p>
            <p>
                <b>Copyrights</b>
            </p>
            <p>The user may copy, distribute and pass on the information in this Healthcare Information Building Block under the conditions that apply for Creative Commons license <u>Attribution-NonCommercial-ShareAlike 3.0 Netherlands (CC BY-NC-SA-3.0)</u>, <u> </u>The content is available under Creative Commons Attribution-NonCommercial-ShareAlike 3.0 (also see <a href="http://creativecommons.org/licenses/by-nc-sa/3.0/nl/">
                    <font color="#0000ff">
                        <u>http://creativecommons.org/licenses/by-nc-sa/3.0/nl/</u>
                    </font>
                </a>)</p>
            <p>
                <b>Conversion to ART-DECOR</b>
            </p>
            <p>The set of Health and Care Information Models is followed up by an automated import into ART-DECOR upon every publication for further processing into information standards. An HCIM is a functional model with well defined elements in a certain relationship to eachother (cardinality) determining if the elements are optional, required and if they may repeat. In ART-DECOR this lands in the following sections: <ul>
                    <li>A dataset with the definition and the (tree) structure. Concept and value set associations are part of the definition</li>
                    <li>A transaction with the cardinality and potentially conditions like for choice boxes.</li>
                    <li>For every ZIB there 1 dataset and 1 transaction of type stationary that is under 1 transaction group, under 1 scenario</li>
                </ul>
                <table>
                    <tr style="text-align: left; background-color: lightgrey;">
                        <th style="padding-right: 10px;">Health and Care Information Models property</th>
                        <th style="padding-right: 10px;">Example HCIM</th>
                        <th style="padding-right: 10px;">ART-DECOR</th>
                        <th style="padding-right: 10px;">Example ART-DECOR</th>
                    </tr>
                    <tr>
                        <td style="padding-right: 10px;">HCIM id (DCM::Id)<br/>
                            <br/>
                            <br/>
                            <br/>Value set id</td>
                        <td style="padding-right: 10px;">2.16.840.1.113883.2.4.3.11.60.40.3.<b>12.5</b>
                            <br/>
                            <br/>
                            <br/>
                            <br/>2.16.840.1.113883.2.4.3.11.60.40.2.12.5.1</td>
                        <td style="padding-right: 10px;">Dataset id<br/>Scenario id<br/>Transaction group id<br/>Transaction id<br/>Value set id</td>
                        <td style="padding-right: 10px;">
                            <i>idem</i>
                            <br/>
                            <i>{$oidBaseSC}</i>.<b>12.5</b>
                            <br/>
                            <i>{$oidBaseTRgroup}</i>.<b>12.5</b>
                            <br/>
                            <i>{$oidBaseTRitem}</i>.<b>12.5</b>
                            <br/>
                            <i>idem</i>
                        </td>
                    </tr>
                    <tr style="background-color: GhostWhite ;">
                        <td style="padding-right: 10px;">HCIM publicatiedatum (DCM::PublicationDate)</td>
                        <td style="padding-right: 10px;">1-4-2015</td>
                        <td style="padding-right: 10px;">Dataset version<br/>Scenario version<br/>Transaction group version<br/>Transaction version<br/>Value set version</td>
                        <td style="padding-right: 10px;">2015-04-01T00:00:00</td>
                    </tr>
                    <tr>
                        <td style="padding-right: 10px;">HCIM versie (DCM::Version)</td>
                        <td style="padding-right: 10px;">3.1</td>
                        <td style="padding-right: 10px;">Dataset version label<br/>Scenario version label<br/>Transactie group version label<br/>Transaction versie<br/>Value set version label</td>
                        <td style="padding-right: 10px;">
                            <i>idem</i>
                        </td>
                    </tr>
                    <tr style="background-color: GhostWhite ;">
                        <td style="padding-right: 10px;">HCIM versie (DCM::LifecycleStatus)</td>
                        <td style="padding-right: 10px;">Final</td>
                        <td style="padding-right: 10px;">Dataset status<br/>Scenario status<br/>Transaction group status<br/>Transaction status<br/>Value set status</td>
                        <td style="padding-right: 10px;">final</td>
                    </tr>
                </table>
            </p>
        </desc>
        <copyright by="Registratie aan de bron" logo="logo_registratieaandebron.png" years="2013-" type="author">
            <addrLine type="uri">https://www.registratieaandebron.nl</addrLine>
            <addrLine type="uri">https://www.zibs.nl</addrLine>
        </copyright>
        <copyright by="Nictiz" logo="nictiz-logo.png" years="2013-" type="author"/>
        <!--<author id="1" username="tessa" email="stijn@nictiz.nl" notifier="on">Tessa van Stijn</author>-->
        <author id="2" username="maarten">Maarten Ligtvoet</author>
        <!--<author id="3" username="andraschmohl" notifier="on">Andra Schmohl</author>-->
        <author id="4" username="alexander" email="henket@nictiz.nl" notifier="on">Alexander Henket</author>
        <!--<author id="5" username="albert-jan" email="spruyt@nictiz.nl" notifier="on">Albert-Jan Spruyt</author>-->
        <author id="6" username="nasrakhan" email="khan@nictiz.nl">Nasra Khan</author>
        <reference url="http://decor.nictiz.nl/pub/{substring($projectPrefix,1,string-length($projectPrefix)-1)}/" logo="nictiz-logo.png"/>
        <restURI for="VS" format="CSV">http://decor.nictiz.nl/decor/services/RetrieveValueSet?prefix=__PFX__&amp;language=__LANG__&amp;id=__ID__&amp;effectiveDate=__ED__&amp;format=csv</restURI>
        <restURI for="VS" format="XML">http://decor.nictiz.nl/decor/services/RetrieveValueSet?prefix=__PFX__&amp;language=__LANG__&amp;id=__ID__&amp;effectiveDate=__ED__&amp;format=xml</restURI>
        <restURI for="DS" format="HTML">http://decor.nictiz.nl/decor/services/RetrieveTransaction?prefix=__PFX__&amp;language=__LANG__</restURI>
        <restURI for="FHIR" format="4.0">http://decor.nictiz.nl/fhir/</restURI>
        <defaultElementNamespace ns="hl7:"/>
        <buildingBlockRepository url="http://art-decor.org/decor/services/" ident="ad1bbr-"/>
        <buildingBlockRepository url="http://art-decor.org/decor/services/" ident="ad2bbr-"/>
        <buildingBlockRepository url="http://art-decor.org/decor/services/" ident="ccda-"/>
        <buildingBlockRepository url="http://decor.nictiz.nl/decor/services/" ident="mp-"/>
        <!--<buildingBlockRepository url="http://decor.nictiz.nl/decor/services/" ident="zib2015bbr-"/>-->
        <!--<buildingBlockRepository url="http://decor.nictiz.nl/decor/services/" ident="zib2016bbr-"/>-->
        <!--<buildingBlockRepository url="http://decor.nictiz.nl/decor/services/" ident="zib2017bbr-"/>-->
        <buildingBlockRepository url="https://art-decor.ihe-europe.net/decor/services/" ident="XDLAB-"/>
        <buildingBlockRepository url="https://art-decor.ihe-europe.net/decor/services/" ident="IHE-PCC-"/>
        <version date="{substring(string(current-dateTime()),1,19)}" by="{get:strCurrentUserName()}">
        {
            if ($defaultLanguage = 'nl-NL') then 
            <desc language="{$defaultLanguage}">Automatische import van Zorginformatiebouwstenen uit XMI. De volgende zibs zijn geïmporteerd:
                <ul>
                {
                    (:<b>nl.zorg.Patient versie: 3.1.</b> Deze versie vervangt:   nl.zorg.Patient-v3.1:)
                    (:<UML:TaggedValue tag="DCM::Superseeds" xmi.id="EAID_437C82AF_0FE2_4e8f_821E_589B6F1E0347" value="nl.zorg.MedicatieToediening-v3.0" modelElement="MX_EAID_69EAFB89_9900_4cb3_AEDF_B00BD7024B18"/>:)
                    for $xmimodel at $i in $xmimodels[not(util:document-name(.)='Common.xmi')]
                    let $xmiroot            := local:getdcmroot($xmimodel)
                    let $dcmnames           := local:getdcmname($xmiroot)
                    let $dcmname            := if ($dcmnames[@language = 'nl-NL']) then $dcmnames[@language = 'nl-NL'] else $dcmnames[1]
                    let $dcmversion         := local:getdcmversionlabel($xmiroot)
                    let $dcmsuperseeds      := local:getTaggedValue($xmiroot/ancestor-or-self::UML:Model,'DCM::Superseeds')
                    return
                        <li><b>{data($dcmname)} versie: {data($dcmversion)}.</b> 
                            {if ($dcmsuperseeds and not($dcmsuperseeds = $xmiroot/@name)) then (' Deze versie vervangt:',$dcmsuperseeds) else ()}
                        </li>
                }
                </ul>
            </desc>
            else (
            <desc language="{$defaultLanguage}">Automatic import of health and care information models from XMI. The following HCIMs have been imported:
                <ul>
                {
                    (:<b>nl.zorg.Patient versie: 3.1.</b> Deze versie vervangt:   nl.zorg.Patient-v3.1:)
                    (:<UML:TaggedValue tag="DCM::Superseeds" xmi.id="EAID_437C82AF_0FE2_4e8f_821E_589B6F1E0347" value="nl.zorg.MedicatieToediening-v3.0" modelElement="MX_EAID_69EAFB89_9900_4cb3_AEDF_B00BD7024B18"/>:)
                    for $xmimodel at $i in $xmimodels[not(util:document-name(.)='Common.xmi')]
                    let $xmiroot            := local:getdcmroot($xmimodel)
                    let $dcmnames           := local:getdcmname($xmiroot)
                    let $dcmname            := if ($dcmnames[@language = 'en-US']) then $dcmnames[@language = 'en-US'] else $dcmnames[1]
                    let $dcmversion         := local:getdcmversionlabel($xmiroot)
                    let $dcmsuperseeds      := local:getTaggedValue($xmiroot/ancestor-or-self::UML:Model,'DCM::Superseeds')
                    return
                        <li><b>{data($dcmname)} version: {data($dcmversion)}.</b> 
                            {if ($dcmsuperseeds and not($dcmsuperseeds = $xmiroot/@name)) then (' This version superseeds:',$dcmsuperseeds) else ()}
                        </li>
                }
                </ul>
            </desc>
            )
        }
        </version>
    </project>
let $store          := update insert $xml into $decorProject

(:let $error          := error(xs:QName('error:BreakError'), 'Tot hier en niet verder'):)

let $datasets       := 
    <datasets>
    {
        text {'&#x0a;        '},comment {' ================= '},
        text {'&#x0a;        '},comment {concat(' === ZIBS ',$hcimReleaseYear,' === ')},
        text {'&#x0a;        '},comment {' ================= '}
    }
    {
        for $xmimodel at $i in $xmimodels[not(util:document-name(.)='Common.xmi')]
        return
        local:xmi2dataset($xmimodel//UML:Class[local:getTaggedValue(.,'stereotype')='rootconcept'])
    }
    {
        if ($xmimodels[local:getdcmname(local:getdcmroot(.)) = 'nl.zorg.part.TimeInterval'] or not($xmimodels/ancestor-or-self::XMI//EAStub[@name = 'TimeInterval'])) then () else (
            let $partdt     := concat(format-date(current-date(),'[Y0001]-[M01]-[D01]'),'T00:00:00')
            
            return
            <dataset id="{$oidBaseDS}.1" effectiveDate="{$partdt}" statusCode="final">
                <name language="nl-NL">{$TimeIntervalDSname}</name>
                <name language="en-US">{$TimeIntervalDSname}</name>
                <desc language="nl-NL">
                    <p>
                        <b>Concept</b>
                    </p>
                    <p>Het TimeInterval geeft een combinatie van ingangsdatum, duur en/of einddatum om een tijdperiode te duiden. <br/>Dit is een subbouwsteen.<br/>NB: In de set Zorginformatiebouwstenen is TimeInterval een complex datatype en geen zelfstandige subbouwsteen. Ten behoeve van de import is er echter voor gekozen om de onderdelen van TimeInterval expliciet bereikbaar te maken, zoals dat ook met andere complexe structuren als naam en adres is gedaan in de oorspronkelijke set.</p>
                    <p>
                        <b>Purpose</b>
                    </p>
                    <p>Het TimeInterval wordt gebruikt om bijvoorbeeld een gebruiksduur van medicatie aan te duiden, of een periode waarin een zorgverlener onderdeel van  een behandelteam was</p>
                    <p>
                        <b>Evidence Base</b>
                    </p>
                    <p>-</p>
                    <p>
                        <b>Example Instances</b>
                    </p>
                    <p>-</p>
                    <p>
                        <b>Instructions</b>
                    </p>
                    <p>-</p>
                    <p>
                        <b>Interpretation</b>
                    </p>
                    <p>-</p>
                    <p>
                        <b>Issues</b>
                    </p>
                    <p>-</p>
                    <p>
                        <b>References</b>
                    </p>
                    <p>-</p>
                    <p>
                        <b>Traceability to other Standards</b>
                    </p>
                    <p>-</p>
                    <p>
                        <b>Revision History</b>
                    </p>
                    <p>
                        <u>Publicatieversie 1.0</u> (nn-nn-nnnn)</p>
                </desc>
                <desc language="en-US">
                    <p>
                        <b>Concept</b>
                    </p>
                    <p>The TimeInterval allows for a combination of start date, duration and/or end date to delimit a period of time. <br/>This is a partial information model.<br/>NOTE: TimeInterval is a complex datatype in the set Healthcare Information Models and not a distinct partial information model. For the purposes of the import the properties of the TimeInterval are explicitly accessible, comparable to other complex structures as name and address that are part of the official set.</p>
                    <p>
                        <b>Purpose</b>
                    </p>
                    <p>Het TimeInterval wordt gebruikt om bijvoorbeeld een gebruiksduur van medicatie aan te duiden, of een periode waarin een zorgverlener onderdeel van  een behandelteam was</p>
                    <p>
                        <b>Evidence Base</b>
                    </p>
                    <p>-</p>
                    <p>
                        <b>Example Instances</b>
                    </p>
                    <p>-</p>
                    <p>
                        <b>Instructions</b>
                    </p>
                    <p>-</p>
                    <p>
                        <b>Interpretation</b>
                    </p>
                    <p>-</p>
                    <p>
                        <b>Issues</b>
                    </p>
                    <p>-</p>
                    <p>
                        <b>References</b>
                    </p>
                    <p>-</p>
                    <p>
                        <b>Traceability to other Standards</b>
                    </p>
                    <p>-</p>
                    <p>
                        <b>Revision History</b>
                    </p>
                    <p>-</p>
                </desc>
                <property name="DCM::CreationDate">{format-date(current-date(),'[D01]/[M01]/[Y0001]')}</property>
                <property name="DCM::DescriptionLanguage">nl</property>
                <property name="DCM::PublicationStatus">Unpublished</property>
                <concept minimumMultiplicity="0" maximumMultiplicity="1" id="{local:getnewid($decorProject, 'DE')}" effectiveDate="{$partdt}" statusCode="final" type="group">
                    <name language="nl-NL">TimeInterval</name>
                    <name language="en-US">TimeInterval</name>
                    <desc language="nl-NL">GebruiksInterval</desc>
                    <desc language="en-US">UsageInterval</desc>
                    <concept minimumMultiplicity="0" maximumMultiplicity="1" id="{local:getnewid($decorProject, 'DE')}" effectiveDate="{$partdt}" statusCode="final" type="item">
                        <name language="nl-NL">IngangsDatum</name>
                        <name language="en-US">StartDate</name>
                        <desc language="nl-NL">Dit is het tijdstip waarop het gebruik zou zijn gestart (of gestart is of zal starten). </desc>
                        <desc language="en-US">This is the time at which the use was to take effect (or took effect or will take effect). </desc>
                        <valueDomain type="datetime">
                            <example>12-12-2012</example>
                        </valueDomain>
                    </concept>
                    <concept minimumMultiplicity="0" maximumMultiplicity="1" id="{local:getnewid($decorProject, 'DE')}" effectiveDate="{$partdt}" statusCode="final" type="item">
                        <name language="nl-NL">EindDatum</name>
                        <name language="en-US">EndDate</name>
                        <desc language="nl-NL">Het tijdstip waarop het gebruik zijn zijn geëindigd (of geëindigd is of zal eindigen). Om verwarring te voorkomen tussen 'tot' en 'tot en met' is het meegeven van de tijd altijd verplicht bij einddatum.</desc>
                        <desc language="en-US">The time at which the period of use was to end (or ended or will end). To avoid confusion between 'to' and 'up to', the submission of time is always mandatory for the end date. </desc>
                        <valueDomain type="datetime">
                            <example>31-12-2013</example>
                            <property timeStampPrecision="YMDHM"/>
                        </valueDomain>
                    </concept>
                    <concept minimumMultiplicity="0" maximumMultiplicity="1" id="{local:getnewid($decorProject, 'DE')}" effectiveDate="{$partdt}" statusCode="final" type="item">
                        <name language="nl-NL">Duur</name>
                        <name language="en-US">Duration</name>
                        <desc language="nl-NL">De beoogde gebruiksduur. Bijvoorbeeld 5 dagen of 8 weken. Het is niet toegestaan om de gebruiksduur in maanden aan te geven, omdat verschillende maanden een variabele duur in dagen hebben.</desc>
                        <desc language="en-US">The intended duration of use. E.g. 5 days or 8 weeks. It is not allowed to indicate the duration in months, because different months have a variable duration in days. </desc>
                        <valueDomain type="duration"/>
                    </concept>
                </concept>
            </dataset>
        )
    }
    </datasets>
      
let $store          := update insert $datasets into $decorProject

(:let $error          := error(xs:QName('error:BreakError'), 'Tot hier en niet verder'):)

let $datasets       := local:expanddatasets($decorProject, $decorProject/datasets)
let $store          := update replace $decorProject/datasets with $datasets

(:let $error          := error(xs:QName('error:BreakError'), 'Tot hier en niet verder'):)

let $xml            :=
    <scenarios>
        <actors>
            <actor type="person" id="{$oidProject}.7.1">
            {
                for $lang in ($defaultLanguage, $otherLanguages)
                return
                    if ($lang = 'nl-NL') then 
                        <name language="{$lang}">Verwijzer Instelling A</name>
                    else (
                        <name language="{$lang}">Referrer HCI A</name>
                    )
                ,
                for $lang in ($defaultLanguage, $otherLanguages)
                return
                    if ($lang = 'nl-NL') then 
                        <desc language="{$lang}">Verwijzer in instelling A</desc>
                    else (
                        <desc language="{$lang}">Referrer in healthcare institution A</desc>
                    )
            }
            </actor>
            <actor type="organization" id="{$oidProject}.7.2">
            {
                for $lang in ($defaultLanguage, $otherLanguages)
                return
                    if ($lang = 'nl-NL') then 
                        <name language="{$lang}">Verwijzer Instelling B</name>
                    else (
                        <name language="{$lang}">Referrer HCI B</name>
                    )
                ,
                for $lang in ($defaultLanguage, $otherLanguages)
                return
                    if ($lang = 'nl-NL') then 
                        <desc language="{$lang}">Verwijzer in instelling B</desc>
                    else (
                        <desc language="{$lang}">Referrer in healthcare institution B</desc>
                    )
            }
            </actor>
        </actors>
    {
        text {'&#x0a;        '},comment {' ================= '},
        text {'&#x0a;        '},comment {concat(' === ZIBS ',$hcimReleaseYear,' === ')},
        text {'&#x0a;        '},comment {' ================= '}
    }
    {
        for $xmimodel at $i in $xmimodels[not(util:document-name(.)='Common.xmi')]
        return
        local:xmi2scenario($xmimodel, $i, $datasets/dataset)
    }
    </scenarios>
let $store          := update insert $xml into $decorProject

(:let $error          := error(xs:QName('error:BreakError'), 'Tot hier en niet verder'):)

let $xml            := 
    <ids>
        <baseId id="{$oidBaseZIB}" type="DS" prefix="NL-BS-"/>
        <baseId id="{$oidBaseEL}" type="DE" prefix="NL-CM-"/>
        <baseId id="{$oidBaseVS}" type="VS" prefix="NL-CM-VS-"/>
        <baseId id="{$oidBaseDS}" type="DS" prefix="{$projectPrefix}dataset-" default="true"/>
        <baseId id="{$oidProject}.2" type="DE" prefix="{$projectPrefix}dataelementen-" default="false"/>
        <!--<baseId id="{$oidProject}.2.1" type="DE" prefix="{$projectPrefix}dataelement-" default="true"/>-->
        <!-- Verpleegkundige set - historisch overblijfsel van toen het nog 2 projecten waren -->
        <baseId id="2.16.840.1.113883.2.4.3.11.60.4.2.2" type="DE" prefix="NL-CM-2015V-" default="false"/>
        <baseId id="{$oidBaseDE2015}" type="DE" prefix="NL-CM-2015-" default="{$oidBaseDE2015 = $oidBaseDEdefault}"/>
        <baseId id="{$oidBaseDE2016}" type="DE" prefix="NL-CM-2016-" default="{$oidBaseDE2016 = $oidBaseDEdefault}"/>
        <baseId id="{$oidBaseDE2017}" type="DE" prefix="NL-CM-2017-" default="{$oidBaseDE2017 = $oidBaseDEdefault}"/>
        <baseId id="{$oidBaseDE2018}" type="DE" prefix="NL-CM-2018-" default="{$oidBaseDE2018 = $oidBaseDEdefault}"/>
        <baseId id="{$oidBaseDE2019}" type="DE" prefix="NL-CM-2019-" default="{$oidBaseDE2019 = $oidBaseDEdefault}"/>
        <baseId id="{$oidBaseSC}" type="SC" prefix="NL-BS-SC-" default="true"/>
        <baseId id="{$oidBaseTRgroup}" type="TR" prefix="NL-BS-TR-group-" default="false"/>
        <baseId id="{$oidBaseTRitem}" type="TR" prefix="NL-BS-TR-" default="true"/>
        <baseId id="{$oidProject}.5" type="CS" prefix="{$projectPrefix}codesystem-" default="true"/>
        <baseId id="{$oidBaseIS}" type="IS" prefix="{$projectPrefix}issue-" default="true"/>
        <baseId id="{$oidBaseAC}" type="AC" prefix="{$projectPrefix}actor-" default="true"/>
        <baseId id="{$oidProject}.8" type="CL" prefix="{$projectPrefix}conceptlist-" default="true"/>
        <baseId id="{$oidProject}.9" type="EL" prefix="{$projectPrefix}template-element-" default="true"/>
        <baseId id="{$oidProject}.10" type="TM" prefix="{$projectPrefix}template-" default="false"/>
        <!--<baseId id="{$oidProject}.10.1" type="TM" prefix="{$projectPrefix}template-" default="true"/>
        <baseId id="{$oidProject}.10.2" type="TM" prefix="{$projectPrefix}template-" default="false"/>
        <baseId id="{$oidProject}.10.3" type="TM" prefix="{$projectPrefix}template-" default="false"/>
        <baseId id="2.16.840.1.113883.2.4.6.10.21" type="TM" prefix="{$projectPrefix}template-" default="false"/>
        <baseId id="2.16.840.1.113883.2.4.6.10.21.1" type="TM" prefix="{$projectPrefix}template-" default="false"/>
        <baseId id="2.16.840.1.113883.2.4.6.10.21.2" type="TM" prefix="{$projectPrefix}template-" default="false"/>
        <baseId id="2.16.840.1.113883.2.4.6.10.21.3" type="TM" prefix="{$projectPrefix}template-" default="false"/>-->
        <baseId id="{$oidProject}.11" type="VS" prefix="{$projectPrefix}valueset-" default="true"/>
        <baseId id="{$oidProject}.16" type="RL" prefix="{$projectPrefix}rule-" default="true"/>
        <baseId id="{$oidProject}.17" type="TX" prefix="{$projectPrefix}test-data-element-" default="true"/>
        <baseId id="{$oidProject}.18" type="SX" prefix="{$projectPrefix}test-scenario-" default="true"/>
        <baseId id="{$oidProject}.19" type="EX" prefix="{$projectPrefix}example-instance-" default="true"/>
        <baseId id="{$oidProject}.20" type="QX" prefix="{$projectPrefix}test-requirement-" default="true"/>
        <baseId id="{$oidProject}.21" type="CM" prefix="{$projectPrefix}community-" default="true"/>
        <baseId id="{$oidProject}.22" type="MP" prefix="{$projectPrefix}mapping-" default="true"/>
        <defaultBaseId id="{$oidBaseZIB}" type="DS"/>
        <defaultBaseId id="{$oidBaseDEdefault}" type="DE"/>
        <defaultBaseId id="{$oidBaseSC}" type="SC"/>
        <defaultBaseId id="{$oidBaseTRitem}" type="TR"/>
        <defaultBaseId id="{$oidProject}.5" type="CS"/>
        <defaultBaseId id="{$oidBaseIS}" type="IS"/>
        <defaultBaseId id="{$oidBaseAC}" type="AC"/>
        <defaultBaseId id="{$oidProject}.8" type="CL"/>
        <defaultBaseId id="{$oidProject}.9" type="EL"/>
        <defaultBaseId id="{$oidProject}.10" type="TM"/>
        <defaultBaseId id="{$oidProject}.11" type="VS"/>
        <defaultBaseId id="{$oidProject}.16" type="RL"/>
        <defaultBaseId id="{$oidProject}.17" type="TX"/>
        <defaultBaseId id="{$oidProject}.18" type="SX"/>
        <defaultBaseId id="{$oidProject}.19" type="EX"/>
        <defaultBaseId id="{$oidProject}.20" type="QX"/>
        <defaultBaseId id="{$oidProject}.21" type="CM"/>
        <defaultBaseId id="{$oidProject}.22" type="MP"/>
        {
            (:<sourceCodeSystem id="{$codeSystem}" identifierName="{$contentrow[1]/@CodeSystemName}"/>:)
            for $sourceCodeSystem in $preparedvalsets//sourceCodeSystem
            let $codeSystem     := $sourceCodeSystem[1]/normalize-space(@id)
            group by $codeSystem
            order by count(index-of($codeSystem, '.')), string-join(tokenize($codeSystem, '\.')[position() != last()], '.'), number(tokenize($codeSystem, '\.')[last()])
            return
                let $codeSystemName     := distinct-values($sourceCodeSystem/replace(normalize-space(@identifierName), '\s*\[DEPRECATED\]\s*', ''))
                let $codeSystemNameL    := distinct-values($sourceCodeSystem/lower-case(replace(normalize-space(@identifierName), '\s*\[DEPRECATED\]\s*', '')))
                let $check              := 
                    if (count($codeSystemNameL)=1) then () else (
                        error(xs:QName('error:InternalCodeSystemError'),concat('CodeSystem "',$codeSystem,'" has multiple names across valueSets: ', string-join($codeSystemName, ', ')))
                    )
                return
                if (string-length($codeSystem) = 0 or string-length($codeSystemName[1]) = 0) then () else (
                    <id root="{$codeSystem}">
                        <designation language="{$defaultLanguage}" type="preferred" displayName="{$codeSystemName[1]}">{data($codeSystemName[1])}</designation>
                    </id>
                )
        }
        {
            $datasets//identifierAssociation
        }
    </ids>
let $store          := update insert $xml into $decorProject

(:let $error          := error(xs:QName('error:BreakError'), 'Tot hier en niet verder'):)

let $xml            := 
    <terminology>
    {
        local:xmi2terminology($xmimodels[not(util:document-name(.)='Common.xmi')], $datasets)
    }
    </terminology>
let $store          := update insert $xml into $decorProject

(:let $error          := error(xs:QName('error:BreakError'), 'Tot hier en niet verder'):)

let $xml            :=
    for $bbrs in $xml//buildingBlockRepository
    let $url    := $bbrs/@url
    let $ident  := $bbrs/@ident
    group by $url, $ident
    return
        if ($decorProject/project/buildingBlockRepository[@url=$url][@ident=$ident]) then () else (
            update insert $bbrs[1] preceding $decorProject/project/(version|release)[1]
        )

let $xml            := <rules/>
let $store          := update insert $xml into $decorProject

let $xml            := <issues/>
let $store          := update insert $xml into $decorProject

let $update         := update delete $decorProject//project/@newid
let $update         := update delete $decorProject//datasets//condition
let $update         := update delete $decorProject//datasets//@oldid
let $update         := update delete $decorProject//datasets//@minimumMultiplicity
let $update         := update delete $decorProject//datasets//@maximumMultiplicity
let $update         := update delete $decorProject//datasets//inherit/@prefix
let $update         := update delete $decorProject//datasets//relationship/@prefix
let $update         := update delete $decorProject//datasets//terminologyAssociation
let $update         := update delete $decorProject//datasets//identifierAssociation
let $update         := update delete $decorProject//ul/br
let $update         := update delete $decorProject//ol/br
let $update         := update delete $decorProject//valueSet/sourceCodeSystem
let $update         := update delete $decorProject//valueSet/linkInfo
let $update         := update delete $decorProject//terminology/buildingBlockRepository

let $doc            := doc($projectFile)
let $check          := $preparedvalsets[not(@id=$doc//valueSet/@id)]

return 
    if ($check) then (
        <missing-valuesets saved="{count($doc//valueSet)}" prepared="{count($preparedvalsets)}">
        {
            text {'&#x0a;    '},comment {' These value sets were generated but they do not appear to be used in the models '}, 
            $check
        }
        </missing-valuesets>
    ) else ($doc)

};

declare %private function local:teststuff() as item()* {
(:let $remove         := 
    if (doc-available(concat($resultcollection,'/',$resultfile))) then xmldb:remove($resultcollection,$resultfile) else ()
let $projectFile    := xmldb:store($resultcollection,$resultfile, $xml)
let $decorProject   := doc($projectFile)/decor

let $xml            :=
    (<?xml-stylesheet type="text/xsl" href="https://assets.art-decor.org/ADAR/rv/DECOR2schematron.xsl"?> |
    <?xml-model href="https://assets.art-decor.org/ADAR/rv/DECOR.xsd" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>)
let $store          := update insert $xml into $decorProject/document-node()

let $xml            := 
    <project id="{$oidProject}" prefix="{$projectPrefix}" experimental="false" defaultLanguage="{$defaultLanguage}" newid="0">
        <name language="nl-NL">Zorginformatiebouwstenen (ZIB's) 1</name>
        <name language="en-US">Healthcare Information Models (HCIMs) 1</name>
        {
            (\:get project description based on first model:\)
            local:getprojectdesc(local:getdcmroot($xmimodels[not(util:document-name(.)='Common.xmi')][1]))
        }
        <copyright by="NFU" logo="NFU.jpg" years="2013-{format-date(current-date(),'[Y0001]')}"/>
        <copyright by="AMC" logo="AMC.jpg" years="2013-{format-date(current-date(),'[Y0001]')}"/>
        <copyright by="Erasmus MC" logo="Erasmus.jpg" years="2013-{format-date(current-date(),'[Y0001]')}"/>
        <copyright by="LUMC" logo="LUMC.jpeg" years="2013-{format-date(current-date(),'[Y0001]')}"/>
        <copyright by="MUMC" logo="MUMC.jpg" years="2013-{format-date(current-date(),'[Y0001]')}"/>
        <copyright by="Radboud UMC" logo="Radboud.jpg" years="2013-{format-date(current-date(),'[Y0001]')}"/>
        <copyright by="UMCG" logo="UMCG.jpg" years="2013-{format-date(current-date(),'[Y0001]')}"/>
        <copyright by="UMCU" logo="UMCU.jpeg" years="2013-{format-date(current-date(),'[Y0001]')}"/>
        <copyright by="VUMC" logo="VUMC.jpg" years="2013-{format-date(current-date(),'[Y0001]')}"/>
        <copyright by="Nictiz" logo="nictiz-logo.png" years="2013-{format-date(current-date(),'[Y0001]')}"/>
        <author id="1" username="tessa" email="stijn@nictiz.nl" notifier="on">Tessa van Stijn</author>
        <author id="2" username="maarten">Maarten Ligtvoet</author>
        <author id="3" username="andraschmohl" email="schmohl@nictiz.nl" notifier="on">Andra Schmohl</author>
        <author id="4" username="alexander" email="henket@nictiz.nl" notifier="on">Alexander Henket</author>
        <author id="5" username="albert-jan" email="spruyt@nictiz.nl" notifier="on">Albert-Jan Spruyt</author>
        <author id="6" username="nasrakhan" email="khan@nictiz.nl">Nasra Khan</author>
        <reference url="http://decor.nictiz.nl/pub/{substring($projectPrefix,1,string-length($projectPrefix)-1)}/" logo="nictiz-logo.png"/>
        <restURI for="VS" format="CSV">http://decor.nictiz.nl/decor/services/RetrieveValueSet?prefix=__PFX__&amp;language=__LANG__&amp;id=__ID__&amp;effectiveDate=__ED__&amp;format=csv</restURI>
        <restURI for="VS" format="XML">http://decor.nictiz.nl/decor/services/RetrieveValueSet?prefix=__PFX__&amp;language=__LANG__&amp;id=__ID__&amp;effectiveDate=__ED__&amp;format=xml</restURI>
        <restURI for="DS" format="HTML">http://decor.nictiz.nl/decor/services/RetrieveTransaction?prefix=__PFX__&amp;language=__LANG__</restURI>
        <defaultElementNamespace ns="hl7:"/>
        <buildingBlockRepository url="http://art-decor.org/decor/services/" ident="ad1bbr-"/>
        <buildingBlockRepository url="http://art-decor.org/decor/services/" ident="ad2bbr-"/>
        <buildingBlockRepository url="http://art-decor.org/decor/services/" ident="ccda-"/>
        <buildingBlockRepository url="http://decor.nictiz.nl/decor/services/" ident="zib2bbr-"/>
        <buildingBlockRepository url="http://decor.nictiz.nl/decor/services/" ident="hg-"/>
        <buildingBlockRepository url="http://decor.nictiz.nl/decor/services/" ident="naw-"/>
        <buildingBlockRepository url="http://decor.nictiz.nl/decor/services/" ident="nictiz2bbr-"/>
        <buildingBlockRepository url="http://decor.nictiz.nl/decor/services/" ident="mp-"/>
        <version date="{substring(string(current-dateTime()),1,19)}" by="{get:strCurrentUserName()}">
            <desc language="{$defaultLanguage}">Automatische import van bouwstenen uit XMI. De volgende bouwstenen zijn geïmporteerd:
                <ul>
                {
                    (\:<b>nl.zorg.Patient versie: 3.1.</b> Deze versie vervangt:   nl.zorg.Patient-v3.1:\)
                    (\:<UML:TaggedValue tag="DCM::Superseeds" xmi.id="EAID_437C82AF_0FE2_4e8f_821E_589B6F1E0347" value="nl.zorg.MedicatieToediening-v3.0" modelElement="MX_EAID_69EAFB89_9900_4cb3_AEDF_B00BD7024B18"/>:\)
                    for $xmimodel at $i in $xmimodels[not(util:document-name(.)='Common.xmi')]
                    let $xmiroot            := local:getdcmroot($xmimodel)
                    let $dcmname            := local:getdcmname($xmiroot)
                    let $dcmversion         := local:getdcmversionlabel($xmiroot)
                    let $dcmsuperseeds      := local:getTaggedValue($xmiroot/ancestor-or-self::UML:Model,'DCM::Superseeds')
                    return
                        <li><b>{data($dcmname)} versie: {data($dcmversion)}.</b> 
                            {if ($dcmsuperseeds and not($dcmsuperseeds = $xmiroot/@name)) then (' Deze versie vervangt: ',$dcmsuperseeds) else ()}
                        </li>
                }
                </ul>
            </desc>
        </version>
    </project>
let $store          := update insert $xml into $decorProject

let $datasets       := 
    <datasets>
    {
        for $xmimodel at $i in $xmimodels[not(util:document-name(.)='Common.xmi')]
        return
        local:xmi2dataset($xmimodel//UML:Class[local:getTaggedValue(.,'stereotype')='rootconcept'])
    }
    </datasets>
let $store          := update insert $datasets into $decorProject
(\:let $datasets       := local:expanddatasets($decorProject, $decorProject/datasets):\)

return
    $datasets:)

(: Ademhaling v3.1 <UML:Class name="ToedieningHulpmiddel::MedischHulpmiddel" xmi.id="EAID_8ABF3E56_806E_4c07_8BDA_D2384089D7A3" :)
    let $object             := $xmimodels//UML:Class[@xmi.id = 'EAID_8ABF3E56_806E_4c07_8BDA_D2384089D7A3']
    let $parent             := local:getallparents($object)[self::UML:Class]
    let $xmiroot            := local:getdcmroot($object)
    let $conceptid          := local:getid($object)
    let $concepteff         := local:getdcmeffectivedate($xmiroot)
    let $concepttype        := local:gettype($object)
    let $valuedomaintype    := if ($concepttype='item') then local:getValueDomainType($object) else ()
    
    (: pre 2017 style UML:Collaboration :)
    (:let $boundary           := local:getcollaborationforobject($object, $parent)
    let $otherobjects       := if ($boundary) then local:getothercollaborationobjects($boundary, $object, $parent) else ():)
    (: 2017 style UML:ClassifierRole onder een UML:Collaboration :)
    let $boundary           := local:getassociatedboundaries($object, $parent)
    let $otherobjects       := if ($boundary) then local:getotherboundaryobjects($boundary, $object, $parent) else ()
    
    return
        $otherobjects
};

local:main()
(:local:teststuff():)
