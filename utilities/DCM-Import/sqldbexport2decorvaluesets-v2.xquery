xquery version "3.0";

import module namespace art     = "http://art-decor.org/ns/art" at "/db/apps/art/modules/art-decor.xqm";

(:<model xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" exportDate="9-4-2015 10:40:01" xmlns="http://www.umcg.nl/MAX">:)
declare namespace UML           = "omg.org/UML1.3";
declare namespace max           = "http://www.umcg.nl/MAX";
declare namespace error         = "http://art-decor.org/ns/decor/import/error";
declare namespace xs            = "http://www.w3.org/2001/XMLSchema";

(:~Project prefix:)
declare variable $projectPrefix     := 'zib1bbr-';

declare variable $language          := 'nl-NL';
declare variable $vscollection      := '/db/apps/decor/staging/Testrun/GOG';
declare variable $resultcollection  := '/db/apps/decor/staging';
declare variable $resultfile        := concat($projectPrefix,'decor-valuesets.xml');

let $terminology        :=
    <terminology>
    {
        '&#10;',
        comment {
            '&#10;',
            '    Gereserveerde valueSet attributen: id, name, displayName, effectiveDate, statusCode, expirationDate, officialReleaseDate, versionLabel&#10;',
            '    Extra attributen zijn geen bezwaar&#10;',
            '&#10;',
            '    valueSet heeft 0..* desc elementen. 1 desc element per taal, te zien aan het @language attribuut. De transformatie genereert nu het desc &#10;',
            '    element uit de omschrijving(en) in het name element bij completeCodeSystem koppelingen. Dit is vooral omdat er op dit bewuste element zelf &#10;',
            '    geen desc mogelijkheid zit. Als er slechts 1 omschrijving is, komt die er ''plat'' in, anders komt iedere omschrijving in een <div/> element.&#10;',
            '&#10;',
            '    valueSet sourceCodeSystem bevat de id en naam van ieder codeSystem dat ergens in de valueSet wordt gebruikt. Denormalisatie voor performance.&#10;',
            '&#10;',
            '    valueSet conceptList komt alleen voor als er gecodeerde concepten/excepties zijn. Ieder gecodeerd concept dat niet is gekoppeld aan NullFlavor&#10;',
            '    wordt een <concept/>. Anders wordt het een <exception/>. Eerst komen de <concept/> elementen en dan de <exception/> elementen. De opbouw is &#10;',
            '    hetzelfde. @level en @type hebben vaste waarden (resp. 0 en L) aangezien de ZIB waardelijsten hiervoor geen ondersteuning kennen.&#10;'
        }
        ,
        (:<valueset name="VeroorzakendeStofSNKCodelijst" OID="2.16.840.1.113883.2.4.3.11.60.40.2.8.2.14" Element_EA_guid="{DA05F464-3690-43da-968F-ED280FBB5C6C}" ElementID="11833" Document_EA_guid="{241FAEBD-DA01-4e79-AC5C-CD8FA4618F50}" DocumentID="11849">:)
        for $row in collection($vscollection)//valueset
        let $vsid           := $row/@OID
        let $vsnm           := $row/@name
        let $vsdn           := $row/@name
        let $vsef           := substring(string(current-dateTime()),1,19)
        let $contentrows    := $row/concept
        return
            <valueSet id="{$vsid}" name="{$vsnm}" displayName="{$vsdn}" effectiveDate="{$vsef}" statusCode="final">
            {
                let $descriptions   :=
                    for $completeCodeSystem in $row/concept[not(code)]
                    return string-join(($completeCodeSystem/systemname , $completeCodeSystem/name),' - ')
                return
                if (count($descriptions) = 0) then () else if (count($descriptions) = 1) then
                    <desc language="{$language}">{$descriptions}</desc>
                else (
                    <desc language="{$language}">{for $d in $descriptions return <div>{$d}</div>}</desc>
                )
                ,
                (:
                    <concept nummer="1">
                        <name>Alle waarden</name>
                        <systemname>G-standaard Stofnaamcode (SNK)</systemname>
                        <systemOID>2.16.840.1.113883.2.4.4.1.750</systemOID>
                    </concept>
                :)
                for $codeSystem in $row//systemOID
                let $codeSystemOID := $codeSystem
                group by $codeSystemOID
                return
                    <sourceCodeSystem id="{$codeSystem[1]}" identifierName="{$codeSystem[1]/../systemname}"/>
                ,
                for $completeCodeSystem in $row/concept[not(code)]
                order by xs:integer($completeCodeSystem/@nummer)
                return
                    <completeCodeSystem codeSystem="{$completeCodeSystem/systemOID}" codeSystemName="{$completeCodeSystem/systemname}"/>
                ,
                if ($row[concept/code]) then 
                    <conceptList>
                    {
                        for $concept in $row/concept[code][not(systemOID='2.16.840.1.113883.5.1008')]
                        order by xs:integer($concept/@nummer)
                        return
                            <concept code="{$concept/code}" codeSystem="{$concept/systemOID}" displayName="{$concept/name}">
                            {
                                if ($concept[value]) then attribute ordinal {$concept/ordinal} else (),
                                attribute level {'0'},
                                attribute type {'L'},
                                if ($concept[description]) then <desc language="{$language}">{$concept/description/node()}</desc> else ()
                            }
                            </concept>
                        ,
                        for $concept in $row/concept[code][systemOID='2.16.840.1.113883.5.1008']
                        order by xs:integer($concept/@nummer)
                        return
                            <exception code="{$concept/code}" codeSystem="{$concept/systemOID}" displayName="{$concept/name}">
                            {
                                if ($concept[value]) then attribute ordinal {$concept/ordinal} else (),
                                attribute level {'0'},
                                attribute type {'L'},
                                if ($concept[description]) then <desc language="{$language}">{$concept/description/node()}</desc> else ()
                            }
                            </exception>
                    }
                    </conceptList>
                else ()
            }
            </valueSet>
    }
    </terminology>
let $remove         := if (doc-available(concat($resultcollection,'/',$resultfile))) then xmldb:remove($resultcollection,$resultfile) else ()
let $update         := xmldb:store($resultcollection,$resultfile,$terminology)

return $terminology