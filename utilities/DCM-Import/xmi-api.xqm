xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

module namespace x2d            = "http://art-decor.org/ns/xmi2decor";
import module namespace art     = "http://art-decor.org/ns/art" at "/db/apps/art/modules/art-decor.xqm";
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "/db/apps/art/modules/art-decor-settings.xqm";

declare namespace UML           = "omg.org/UML1.3";
declare namespace error         = "http://art-decor.org/ns/decor/import/error";
declare namespace xs            = "http://www.w3.org/2001/XMLSchema";

declare variable $x2d:objectTypes               := ('Package','Class','Artifact');
declare variable $x2d:objectStereotypes         := ('DCM','rootconcept','data','container','document','context');
declare variable $x2d:relationshipTypes         := ('Aggregation','Dependency','Generalization','NoteLink');
declare variable $x2d:relationshipStereotypes   := ('trace');
declare variable $x2d:dcmdescheadings           := ('Concept','Purpose','Evidence Base','Example Instances','Instructions',
                                                    'Interpretation','Issues','References','Traceability to other Standards',
                                                    'Revision History');
declare variable $x2d:dcmscenheadings           := ('Concept','Purpose');
declare variable $x2d:dcmprojheadings           := ('Disclaimer','Terms of Use','Copyrights');
declare variable $x2d:dcmtags                   := ('DCM::AssigningAuthority','DCM::DefinitionCode','DCM::ExampleValue',
                                                    'DCM::Id','DCM::LifecycleStatus','DCM::Name','DCM::PublicationDate',
                                                    'DCM::ReferencedDefinitionCode','DCM::ValueSet','DCM::Version');

(:~Base for a Building Block:)
declare variable $x2d:oidBaseBS             := '2.16.840.1.113883.2.4.3.11.60.40.3';
(:~NL-CM-VS Base for a Value Set:)
declare variable $x2d:oidBaseVS             := '2.16.840.1.113883.2.4.3.11.60.40.2';
(:~NL-CM Base for Concepts in a Building Block:)
declare variable $x2d:oidBaseEL             := '2.16.840.1.113883.2.4.3.11.60.40.1';

declare function x2d:getprojectdesc($xmiroot as element(), $language as xs:string) as element(desc) {
    let $paragraphs     := 
        for $p in $x2d:dcmprojheadings
        let $par        := x2d:getLanguageValue(x2d:getTaggedValue(x2d:getPackageByName($xmiroot,$p),'documentation'),$language)
        return
            concat('<p><b>',$p,'</b></p><p>',if (exists($par)) then x2d:cleanuptext($par) else ('-'),'</p>')
    
    let $general        := 
        switch ($language)
        case ('en-US') return '<p><b>General</b></p><p>--</p>'
        default return        '<p><b>Algemeen</b></p><p>--</p>'
        
    let $desc           := <desc language="{$language}">{string-join(($general,$paragraphs),'')}</desc>
    
    return
        art:parseNode($desc)
};
declare function x2d:getimagecollection($resultcollection as xs:string, $projectPrefix as xs:string, $recreate as xs:boolean) as xs:string {
    let $coll       := concat($projectPrefix,'images')
    let $fullcoll   := concat($resultcollection,'/',$coll)
    let $delete     := 
        if ($recreate) then (
            if (xmldb:collection-available($fullcoll)) then xmldb:remove($fullcoll) else ()
        ) else ()
    
    return
        xmldb:create-collection($resultcollection,$coll)
};
declare function x2d:getimagehttpbase($projectPrefix as xs:string, $resultcollection as xs:string) as xs:string {
    let $imagecollection    := x2d:getimagecollection($resultcollection, $projectPrefix, false())
    return
    concat('http://decor.nictiz.nl/decor/data/projects/',substring($projectPrefix,1,string-length($projectPrefix)-1),'/',tokenize($imagecollection,'/')[last()])
};

declare function x2d:getdcmroot($object as element()) as element(UML:Package) {
    $object/ancestor-or-self::UML:Model/UML:Namespace.ownedElement/UML:Package[x2d:getTaggedValue(.,'stereotype')='DCM']
};
(:  <UML:TaggedValue tag="DCM::Id" xmi.id="EAID_4574860E_541F_403a_9C8B_E2421FE545A7" value="2.16.840.1.113883.2.4.3.11.60.40.3.12.5" modelElement="MX_EAID_A42E3995_0040_494e_ABEF_9AE12D626CC7"/>:)
declare function x2d:getdcmid($object as element()) as item() {
    (:1-4-2015#NOTES#The date this version was made.:)
    let $id     := local:getidfromstring(x2d:getTaggedValue($object/ancestor-or-self::UML:Model,'DCM::Id'))
    
    return
        if ($id) then (
            attribute id {$id}
        ) else (
            error(xs:QName('error:UnsupportedValue'),concat('DCM::Id "',$id,'" not a valid OID.'))
        )
};
(:  <UML:TaggedValue tag="DCM::Name" xmi.id="EAID_93434A5A_A47B_4ccd_A140_60A7144E4507" value="nl.nfu.Ademhaling" modelElement="MX_EAID_A42E3995_0040_494e_ABEF_9AE12D626CC7"/>:)
declare function x2d:getdcmname($xmiroot as element(), $language as xs:string) as element(name) {
    let $name           := x2d:getTaggedValue($xmiroot/ancestor-or-self::UML:Model,'DCM::Name')
    
    return
    <name language="{$language}">{data($name)}</name>
};
(:  <name>nl.nfu.Ademhaling-v1.0</name>:)
declare function x2d:getdcmversionlabel($object as element()) as item()? {
    let $versionlabel   := x2d:getTaggedValue($object/ancestor-or-self::UML:Model,'DCM::Version')
    
    return
        if (matches($versionlabel,'^v?\d+\.\d+')) then
            attribute versionLabel {$versionlabel}
        else ()
};
(:  <notes>Rootconcept van de bouwsteen Ademhaling. Dit rootconcept bevat alle gegevenselementen van de bouwsteen Ademhaling.</notes>:)
declare function x2d:getdcmdesc($xmiroot as element(), $language as xs:string, $projectPrefix as xs:string, $resultcollection as xs:string) as element(desc) {
    let $paragraphs     := 
        for $p in $x2d:dcmdescheadings
        let $pkg        := x2d:getPackageByName($xmiroot,$p)
        let $par        := x2d:getLanguageValue(x2d:getTaggedValue($pkg,'documentation'),$language)
        let $imgs       := x2d:getImages($pkg, $language, $projectPrefix, $resultcollection)
        return
            concat('<p><b>',$p,'</b></p><p>',if (exists($par) or $imgs) then x2d:linktext(x2d:cleanuptext($par)) else ('-'),art:serializeNode(<x>{$imgs}</x>)/node(),'</p>')
    
    let $desc           := <desc language="{$language}">{string-join($paragraphs,'')}</desc>
    
    return
        art:parseNode($desc)
};

(:  <notes>Rootconcept van de bouwsteen Ademhaling. Dit rootconcept bevat alle gegevenselementen van de bouwsteen Ademhaling.</notes>:)
declare function x2d:getdesc($object as element(),$language as xs:string) as element(desc) {
    let $desc   := x2d:cleanuptext(x2d:getLanguageValue(x2d:getTaggedValue($object, 'documentation'),$language))
    
    return
        art:parseNode(<desc language="{$language}">{$desc}</desc>)
};

(: Get TaggedValue by @tag name from an element, or nothing :)
declare function x2d:getTaggedValue($object as element()?,$tag as xs:string) as xs:string* {
    $object/UML:ModelElement.taggedValue/UML:TaggedValue[@tag=$tag]/@value |
    $object/ancestor-or-self::XMI.content/UML:TaggedValue[@tag=$tag][@modelElement=$object/@xmi.id]/@value
};
declare function x2d:getLanguageValue($tag as xs:string?, $language as xs:string) as xs:string? {
    switch ($language)
    case ('en-US') return (
        if (matches($tag,'\[en-US\]')) then (
            replace(substring-after($tag,'[en-US]'),'\[?[/\\]?en-US\]\s*','')
        )
        else
        if (matches($tag,'(^|\n)EN:')) then (
            normalize-space(tokenize(substring-after($tag,'EN:'),'\n')[1])
        )
        else ()
    )
    default return (
        if (matches($tag,'\[nl-NL\]')) then (
            replace(substring-after($tag,'[nl-NL]'),'\[?[/\\]?nl-NL\]\s*','')
        )
        else
        if (matches($tag,'(^|\n)EN:')) then (
            normalize-space(substring-before($tag,'EN:'))
        )
        else (
            $tag
        )
    )
};

(: Get Package by @name from an element, or nothing :)
declare function x2d:getPackageByName($object as element()?,$name as xs:string) as element()? {
    $object/ancestor-or-self::UML:Package//UML:Package[@name=$name]
};

(:  <UML:DiagramElement geometry="Left=32;Top=30;Right=491;Bottom=285;" subject="EAID_AEDE6B76_65D3_46f1_9320_B110CCC553BC" seqno="1" style="DUID=5EC58C6E;ImageID=1664747453;"/>:)
(:  <EAImage xmlns:dt="urn:schemas-microsoft-com:datatypes" name="Image9131" type="ENHMetafile" imageID="389291236" dt:dt="bin.base64">..</EAImage>:)
(:  <EAImage xmlns:dt="urn:schemas-microsoft-com:datatypes" name="Image11840" type="Bitmap" imageID="1664747453" dt:dt="bin.base64">..</EAImage>:)
declare function x2d:getImages($package as element(), $language as xs:string, $projectPrefix as xs:string, $resultcollection as xs:string) as element(img)* {
    let $imagecollection    := x2d:getimagecollection($resultcollection, $projectPrefix, false())
    let $imagehttpbase      := x2d:getimagehttpbase($projectPrefix, $resultcollection)
    let $diagram            := $package/ancestor::XMI.content/UML:Diagram[@owner=$package/@xmi.id]
    let $xmiroot            := x2d:getdcmroot($package)
    
    for $diagramelement in $diagram/UML:Diagram.element/UML:DiagramElement
    let $imageid    := tokenize($diagramelement/tokenize(@style,';')[starts-with(.,'ImageID')],'=')[2]
    let $image      := $package/ancestor::XMI/EAModel.image/EAImage[@imageID=$imageid]
    let $imagename  := string-join((x2d:getdcmname($xmiroot, $language),x2d:getdcmversionlabel($xmiroot),$package/@name,$imageid,$diagramelement/@seqno),'-')
    let $imagename  := concat(replace($imagename,'[^A-Za-z0-9\.\-]',''),'.',x2d:getImageExtensionByType($image/@type))
    let $store      := xmldb:store($imagecollection,$imagename,xs:base64Binary($image))
    let $geometry   := x2d:getdiagramboundaries($diagramelement/@geometry)
    order by $diagramelement/xs:integer(@seqno)
    return 
        <img src="{concat($imagehttpbase,'/',$imagename)}" alt="{$imagename}" style="width:{$geometry[3] - $geometry[1]}px; height:{$geometry[4] - $geometry[2]}px;"/>
    
};
(:this is a weak point in our import. defaults to png because that is mostly true. emf is not a websupported format but happens to be supported by EA:)
declare function x2d:getImageExtensionByType($type as xs:string) as xs:string {
    switch ($type)
    case ('Bitmap') return 'png'
    case ('ENHMetafile') return 'emf'
    default return 'png'
};

(:Left=438;Top=206;Right=716;Bottom=315;:)
declare function x2d:getdiagramboundaries($geometry as xs:string) as xs:integer+ {
    let $parts  := tokenize($geometry,';')
    let $left   := tokenize($parts[starts-with(.,'Left')],'=')[2]
    let $top    := tokenize($parts[starts-with(.,'Top')],'=')[2]
    let $right  := tokenize($parts[starts-with(.,'Right')],'=')[2]
    let $bottom := tokenize($parts[starts-with(.,'Bottom')],'=')[2]
    
    let $check  := 
        if ($left castable as xs:integer and
            $right castable as xs:integer and
            $top castable as xs:integer and
            $bottom castable as xs:integer) then () else (
            error(xs:QName('error:UnsupportedValue'),concat('Diagram boundaries are not all integers left="',$left,'", right="',$right,'", top="',$top,'", bottom="',$bottom,'". Original: ',$geometry))
        )
    
    return (xs:integer($left), xs:integer($top), xs:integer($right), xs:integer($bottom))
};
declare function x2d:cleanuptext($text as xs:string?) as xs:string? {
    replace(replace(replace(replace($text,'\s*$',''),'(<[^>]+)[“”]','$1"'),'>\s*\r?\n\s*','>'),'\r?\n','<br/>')
};
declare function x2d:linktext($text as xs:string?) as xs:string? {
    replace($text,'(GOG-\d+)','<a href="https://bits.nictiz.nl/browse/$1" target="_new">$1</a>')
};

(: Input string that either -is- an OID or -contains- an OID. Output is just the OID :)
declare %private function local:getidfromstring($idstring as xs:string?) as xs:string? {
    let $idstring   := tokenize(normalize-space($idstring), '#')[1]
    
    let $id         :=
        if (matches($idstring,'^[0-2](\.(0|[1-9]\d*))*$')) then (
            $idstring
        )
        else if (starts-with($idstring,'OID')) then (
            tokenize(replace($idstring,'OID\s*:?\s*',''),'\s')[1]
        )
        else if (starts-with($idstring,'NL-CM-VS:')) then (
            tokenize(replace($idstring,'NL-CM-VS:\s*',concat($oidBaseVS,'.')),'\s')[1]
        )
        else if (starts-with($idstring,'NL-CM:')) then (
            tokenize(replace($idstring,'NL-CM:\s*',concat($oidBaseEL,'.')),'\s')[1]
        )
        else ()
    return
        if (matches($id,'^[0-2](\.(0|[1-9]\d*))*$')) then ($id) else ()
};
