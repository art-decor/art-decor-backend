
Author:         Alexander Henket
Creation date:  2015-04-17
Updated:        2024-04-23, 2024-08-28

=== Background: ===

Detailed Clinical Models are ISO TS 13972. They are normally created in Enterprise Architect and are on the level of 
DECOR Dataset/Scenario/ValueSet. For conversion to DECOR to happen smoothly you need extra information in the source
models on top of what DCMs will have by default.

Enterprise Architect supports XMI export of these zibs

Zibs vary in quality and consistency depending on the governance group that created them. It is highly likely that you
need additional information to make a successful conversion.

=== Purpose: ===

The xmi2decor-nfu.xquery and sqldbexport2decorvaluesets-v3.xquery scripts attempt import of the relevant parts of a directory of DCMs into a full DECOR project.

=== Prerequisites: ===

- Need to be admin with full db access

- Make sure your server hosts any previous zib project releases at the very least

- Check that XMI is UTF-8 or convert it to UTF-8
  Windows powershell
  https://superuser.com/questions/1163753/converting-text-file-to-utf-8-on-windows-command-prompt
  
  foreach ($file in get-ChildItem *.xmi) {
    Echo $file.name
    Get-Content $file | Set-Content -Encoding utf8 ("$file.name" +".xmi")
  }
  
  macOS
  for f in *.xmi; do iconv -f Windows-1252 -t UTF-8 $f | sed -e 1d > $f.bak; mv $f.bak $f; done

- Upload XMI + ValueSet files to /db/apps/decor/staging/XMI

- Check variables in the header of the relevant script. xmi2decor-nfu.xquery and sqldbexport2decorvaluesets-v3.xquery

=== Dependencies: ===

- art:parseNode in /dp/apps/art/modules/art-decor.xqm
- /db/apps/art/modules/art-decor-settings.xqm
- /db/apps/art/api/api-decor-valueset.xqm 

=== Run: ===

- Use a non-production server !! ART-DECOR is very sensitive to id-clashes when you deploy the project file

- Execute the script of choice on you server and check the result in /db/apps/decor/staging/
    - sqldbexport2decorvaluesets-v3.xquery produces prefix-decor-valuesets.xml
    - xmi2decor-nfu.xquery produces: prefix-decor-xmi.xml in around a couple of minutes
    
- The script will error at best of its abilities whenever it finds something it cannot handle. Upon successful completion
  of the script you may be reasonably sure that the result is consistent as standalone project, but the next steps are:
  
  - check using DECOR.xsd schema and schematron
  - add to your /db/apps/decor/data/ <...> to check in ART. Look for id clashes and other weird behavior

=== Mapping at a glance: ===

- Project version is written with date of running the script and containing DCM::Superseeds and revision history for every model

- Each model is a dataset
    DCM::Id                         = dataset/@id
    DCM::PublicationDate            = dataset/@effectiveTime,       concept/@effectiveTime,       scenario/@effectiveTime,       transaction/@effectiveTime,       valueSet/@effectiveTime
                                                                    concept/inherit/@effectiveTime
    DCM::Version                    = dataset/@versionLabel,        concept/@versionLabel,        scenario/@versionLabel,        transaction/@versionLabel
    DCM::LifecycleStatus            = dataset/@statusCode,          concept/@statusCode,          scenario/@statusCode,          transaction/@statusCode
    DCM::PublicationDate            = dataset/@officialReleaseDate, concept/@officialReleaseDate, scenario/@officialReleaseDate, transaction/@officialReleaseDate
    DCM::DeprecatedDate             = dataset/@expirationDate,      concept/@expirationDate,      scenario/@expirationDate,      transaction/@expirationDate

- Concepts
    DCM::DefinitionCode             = concept/@id e.g. "NL-CM:12.5.7" -- Note these NL-CM like abbreviations need specific configuration/agreement!
    DCM::ReferencedDefinitionCode   = concept/inherit/@ref
    
                                    = concept/@type is omitted when DCM::ReferencedDefinitionCode, is 'group' when concept is a parent, otherwise 'item'
    UML:Class/@name                 = concept/name
    UML:Class/UML:ModelElement.taggedValue/UML:TaggedValue[@tag='documentation']
                                    = concept/desc
    UML:Class/UML:ModelElement.taggedValue/UML:TaggedValue[@tag='documentation']
    XMI.content/UML:TaggedValue[@tag='DCM::AssigningAuthority']
                                    = concept/operationalization
    
    UML:Class Generalization determines concept/valueDomain/type
    DCM::ValueSet                   = concept/valueDomain/conceptList/@id e.g. "DiepteCodelijst#NOTES#OID: 2.16.840.1.113883.2.4.3.11.60.40.2.12.5.2"
                                                                    -- Note these OID notations need specific configuration/agreement!
    DCM::ExampleValue               = concept/valueDomain/example

- Scenarios/transactions
    Each model is mapped into 1 scenario with 1 transaction group, containing 1 transaction of type "Stationary" linking all dataset concepts to their 
    cardinalities and constraints. When there are no constraints, the cardinalities are as-is. Otherwise the concept is considered conditional and the 
    cardinalities move to the condition/constraint
  
- Terminology
    DCM::DefinitionCode             = terminologyAssociation[@code][@codeSystem] e.g. "SNOMED CT: 278907009 Respiratory pattern"
                                                                    -- Note these named systems like SNOMED CT need specific configuration/agreement!
    DCM::ValueSet                   = terminologyAssociation[@valueSet][@flexibility='DCM::PublicationDate'] e.g. "DiepteCodelijst#NOTES#OID: 2.16.840.1.113883.2.4.3.11.60.40.2.12.5.2"
                                                                    -- Note these OID notations need specific configuration/agreement!
    
    valueSets are retrieved from a prepared file. valueSet/@effectiveDate and valueSet/@statusCode are overwritten in this process to match the terminologyAssociation/@flexibility
                                                                    -- Note that the terminologyAssociation/valueSet connection probably needs refinement.