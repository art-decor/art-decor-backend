<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="#all"
    version="2.0">
    
    <xsl:output indent="yes"/>
    
    <!-- release for these zibs. Usually on any dataset/transaction/valueSet from this release -->
    <xsl:param name="releasedate">2020-09-01T00:00:00</xsl:param>
    
    <xsl:template match="/">
        <xsl:variable name="theDECOR" select="."/>
        <terminology>
        <xsl:for-each select="$systems[not(contains(lower-case(@name), 'deprecated'))]">
            <xsl:variable name="theId" select="@id"/>
            <xsl:variable name="theConcepts" select="$theDECOR//valueSet/conceptList/*[@codeSystem = $theId]" as="element()*"/>
            
            <xsl:choose>
                <xsl:when test="$theConcepts">
                    <codeSystem id="{@id}" name="{@name}" displayName="{@name}" effectiveDate="{$releasedate}" statusCode="final">
                        <conceptList>
                            <xsl:for-each select="$theConcepts">
                                <codedConcept>
                                    <xsl:copy-of select="@code" copy-namespaces="no"/>
                                    <xsl:copy-of select="@level" copy-namespaces="no"/>
                                    <xsl:copy-of select="@type" copy-namespaces="no"/>
                                    <xsl:copy-of select="@ordinal" copy-namespaces="no"/>
                                    <xsl:attribute name="statusCode">
                                        <xsl:choose>
                                            <xsl:when test="@type = 'D'">deprecated</xsl:when>
                                            <xsl:otherwise>active</xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:attribute>
                                    <xsl:if test="empty(designation)">
                                        <designation language="nl-NL" type="preferred" displayName="{@displayName}"/>
                                    </xsl:if>
                                    <xsl:copy-of select="designation" copy-namespaces="no"/>
                                    <xsl:copy-of select="desc[not(. = preceding-sibling::designation/@displayName)]" copy-namespaces="no"/>
                                </codedConcept>
                            </xsl:for-each>
                        </conceptList>
                    </codeSystem>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:comment>WARNING: no contents found for <xsl:value-of select="$theId"/><xsl:text> </xsl:text><xsl:value-of select="@name"/></xsl:comment>
                    <xsl:message>WARNING: no contents found for <xsl:value-of select="$theId"/><xsl:text> </xsl:text><xsl:value-of select="@name"/></xsl:message>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
        </terminology>
    </xsl:template>
    
    <xsl:variable name="systems" as="element()*">
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4" name="Weescodes"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.23.1" name="ExtraRolcodes"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.23.2" name="JuridischeStatus"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.23.3" name="JuridischeVertegenwoordiging"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.24.1" name="CBS-SOI2016AGG3"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.24.2" name="CBS-CTO2016"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.22.1" name="TelecomDeviceTypes"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.1.1" name="BarthelDarm"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.1.2" name="BarthelBlaas"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.1.3" name="BarthelUiterlijkeVerzorging"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.1.4" name="BarthelToiletgebruik"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.1.5" name="BarthelEten"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.1.6" name="BarthelTransfer"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.1.7" name="BarthelMobiliteit"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.1.8" name="BarthelAanUitkleden"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.1.9" name="BarthelTrappenLopen"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.1.10" name="BarthelBadenDouchen"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.3.1" name="BristolStoolTypes"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.4.1" name="MUST_BMI"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.4.2" name="MUST_Gewichtsverlies"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.4.3" name="MUST_Ziekte"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.5.1" name="SNAQ_Gewichtsverlies"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.5.2" name="SNAQ_Eetlust"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.5.3" name="SNAQ_Voeding"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.17.1" name="SK_Gewichtsverlies"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.17.2" name="SK_Ziektebeeld"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.17.3" name="SK_Voeding"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.17.4" name="SK_Voedingstoestand"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.18.1" name="SNAQ65+_Gewichtsverlies"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.18.2" name="SNAQ65+_Eetlust"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.18.3" name="SNAQ65+_Inspanning"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.18.4" name="SNAQ65+_BovenarmOmtrek"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.19.1" name="SNAQrc_Gewichtsverlies"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.19.2" name="SNAQrc_Eetlust"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.19.3" name="SNAQrc_Voeding"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.19.4" name="SNAQrc_HulpBijEten"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.10.1" name="DEPRECATED"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.10.2" name="DEPRECATED"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.10.3" name="M_Afstandsmetastase"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.10.4" name="V_VeneuzeInvasie"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.10.5" name="R_ResidueleTumor"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.10.6" name="AnatomischStadium"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.10.7" name="PrognostischStadium"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.10.8" name="TNMVersie"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.10.9" name="L_LymfatischeInvasie"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.10.10" name="T_PrimaireTumor"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.10.11" name="N_RegionaleLymfeklieren"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.10.12" name="m_AantalPrimaireTumoren"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.13.1" name="WoningType"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.14.1" name="WilsverklaringType"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.12.1" name="LijnStatus"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.15.1" name="ManchetType"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.2.1" name="GCS_Eyes"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.2.2" name="GCS_Motor"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.2.3" name="GCS_Verbal"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.20.1" name="CN_Alertheid"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.20.2" name="CN_Kalmte_Agitatie"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.20.3" name="CN_Ademhalingsreactie"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.20.4" name="CN_Huilen"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.20.5" name="CN_Lichaamsbeweging"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.20.6" name="CN_Gezichtsspanning"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.20.7" name="CN_Spierspanning"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.21.1" name="FLACC_Gezicht"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.21.2" name="FLACC_Benen"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.21.3" name="FLACC_Activiteit"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.21.4" name="FLACC_Huilen"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.21.5" name="FLACC_Troostbaar"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.8.1" name="ContextKleding"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.9.1" name="PijnMeetmethode"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.27" name="VisusMeetkaart"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.16.1" name="ResultaatStatus"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.6.1" name="InstemmingStatus"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.25.1" name="BehandelBesluiten"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.11.1" name="PatientbesprekingType"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.7.1" name="OncoUlcerWeefsel"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.7.2" name="WondWeefselWCS"/>
        <codeSystem id="2.16.840.1.113883.2.4.3.11.60.40.4.7.3" name="WondVochtigheid"/>
    </xsl:variable>
</xsl:stylesheet>