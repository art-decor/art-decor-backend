xquery version "3.0";

import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "/db/apps/art/modules/art-decor-settings.xqm";
import module namespace art     = "http://art-decor.org/ns/art" at "/db/apps/art/modules/art-decor.xqm";

(:<model xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" exportDate="9-4-2015 10:40:01" xmlns="http://www.umcg.nl/MAX">:)
declare namespace UML           = "omg.org/UML1.3";
declare namespace max           = "http://www.umcg.nl/MAX";
declare namespace error         = "http://art-decor.org/ns/decor/import/error";
declare namespace xs            = "http://www.w3.org/2001/XMLSchema";

(:~Project prefix:)
declare variable $projectPrefix     := 'nfu-';
(:~Projectid:)
declare variable $oidProject        := '2.16.840.1.113883.2.4.3.11.60.39';
(:~Base for a Building Block:)
declare variable $oidBaseBS         := '2.16.840.1.113883.2.4.3.11.60.40.3';
(:~NL-CM-VS Base for a Value Set:)
declare variable $oidBaseVS         := '2.16.840.1.113883.2.4.3.11.60.40.2';
(:~NL-CM Base for Concepts in a Building Block:)
declare variable $oidBaseEL         := '2.16.840.1.113883.2.4.3.11.60.40.1';
declare variable $oidBaseDS         := '2.16.840.1.113883.2.4.3.11.60.39.1';
declare variable $oidBaseDE         := '2.16.840.1.113883.2.4.3.11.60.39.2';
declare variable $oidBaseSC         := '2.16.840.1.113883.2.4.3.11.60.39.3';
declare variable $oidBaseTR         := '2.16.840.1.113883.2.4.3.11.60.39.4';
declare variable $oidBaseAC         := '2.16.840.1.113883.2.4.3.11.60.39.7';

declare variable $language          := 'nl-NL';
declare variable $maxcollection     := '/db/apps/decor/staging/MAX';
declare variable $maxmodels         := collection($maxcollection)/max:model;
declare variable $xmicollection     := '/db/apps/decor/staging/XMI';
declare variable $xmimodels         := collection($xmicollection)//UML:Model;
declare variable $resultcollection  := '/db/apps/decor/staging';
declare variable $resultfile        := concat($projectPrefix,'decor-max.xml');
declare variable $preparedvalsets   := doc('/db/apps/decor/staging/nfu-decor-valuesets.xml')//valueSet;

declare variable $objectTypes               := ('Package','Class','Artifact');
declare variable $objectStereotypes         := ('DCM','rootconcept','data','container','document','context');
declare variable $relationshipTypes         := ('Aggregation','Dependency','Generalization','NoteLink');
declare variable $relationshipStereotypes   := ('trace');

declare variable $remove            := if (doc-available(concat($resultcollection,'/',$resultfile))) then xmldb:remove($resultcollection,$resultfile) else ();

declare variable $xml               := 
    <decor xmlns:nfu="urn:urn:nictiz-nl:v3/nfu" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="https://assets.art-decor.org/ADAR/rv/DECOR.xsd"/>;
declare variable $doc               := xmldb:store($resultcollection,$resultfile, $xml);
declare variable $resultdoc         := doc($doc)/decor;

declare %private function local:max2dataset($object as element(object)) as element(dataset) {
let $maxroot           := local:getdcmroot($object)

return
    <dataset>
    {
        (:dataset attribute properties:)
        local:getdcmid($maxroot),
        local:getdcmeffectivedate($maxroot),
        local:getdcmstatuscode($maxroot),
        local:getdcmversionlabel($maxroot),
        local:getdcmpublicationdate($maxroot),
        local:getdcmdeprecationdate($maxroot),
        local:getdcmname($maxroot),
        local:getdcmdesc($maxroot)
        (:,
        local:getdcmtags($maxroot),
        local:getdcmproperties($maxroot):)
    }
    {
        (:dataset root concept:)
        local:max2concept($maxroot,$object,())
    }
    </dataset>
};

declare %private function local:max2concept($maxroot as element(object), $object as element(object), $parent as element()?) as element(concept) {
let $conceptid          := local:getid($object)
let $concepteff         := local:getdcmeffectivedate($maxroot)
let $concepttype        := local:gettype($object)
let $valuedomaintype    := if ($concepttype='item') then local:getValueDomainType($object) else ()

let $boundary           := local:getcollaborationforobject($object, $parent)
let $otherobjects       := if ($boundary) then local:getothercollaborationobjects($boundary, $object, $parent) else ()

let $choiceobjects      :=
    for $otherobject in $otherobjects
    let $otherparents   := local:getallparents($otherobject)
    return
        if ($parent and $otherparents) then (
            if ($otherparents[not(@xmi.id=$parent/@xmi.id)]) then (
                let $parentName := if ($parent) then local:getname($parent) else ()
                return
                error(xs:QName('error:ClassParentError'),concat('BS "',local:getname(local:getdcmroot($object)),'" current object/id "',local:getTaggedValue($object,'ea_localid'),'" ',local:getname($object),' and other object/id "',local:getTaggedValue($otherobject,'ea_localid'),'" ',local:getname($otherobject),' share the same boundary/collaboration, but have a different parent. (',$parentName,' versus ',local:getTaggedValue($otherparents[not(@xmi.id=$parent/@xmi.id)],'ea_localid')))
            ) else (
                $otherobject
            )
        ) else ()
(:  assumption: when an object is in the same boundary as the current object, but not connected to the same parent as the current object
    then this signifies an object that belongs in a referenced package. This package should be referenced from one of the choiceobjects,
    hence one of the other objects within the same boundary that has the same parent as the current object.
    
    when there's no override we can just reference the other package. when there is an override we actually need to retrieve that other 
    package and add inherits in our current context except for the override and all it parents up to the package root.
    
    this is complex stuff to do here, so we delay that for a second processing step on the dataset, and just convert the override to a valid 
    concept. The concept/@id shall be matched in the second step to the correct concept and by then will be inserted into the correct 
    location.
    
    First step:
        <concept id="1.2.3" effectiveDate="2015-04-01T00:00:00" type="group" statusCode="final">
            <concept id="1.2.7" effectiveDate="2015-04-01T00:00:00" type="item" statusCode="final">
                <name/> | <desc/> | <operationalization/> | <valueDomain/>
            </concept>
        </concept>
        
    Second step:
        <concept id="1.2.3" effectiveDate="2015-04-01T00:00:00" type="group" statusCode="final">
            <name/> | <desc/><!-- referenced concept group from package that holds the overridden concept -->
            <concept ref="1.2.4" flexibility="2015-04-01T00:00:00">
                <!-- concept from package that is not overridden -->
            </concept>
            <concept id="1.2.5" effectiveDate="2015-04-01T00:00:00" type="item" statusCode="final">
                <name/> | <desc/><!-- concept group from package that holds the overridden concept -->
                
                <concept id="1.2.7" effectiveDate="2015-04-01T00:00:00" type="item" statusCode="final">
                    <name/> | <desc/> | <operationalization/> | <valueDomain/>
                </concept>
            </concept>
        </concept>
:)
let $overrideobjects    := $otherobjects[not(@xmi.id=$choiceobjects/@xmi.id)]

let $card               := local:getMultiplicity($object, $parent)
let $min                := tokenize($card,'\.\.')[1]
let $min                := if (string-length($min)>0) then $min else (0)
let $max                := if ($card castable as xs:integer) then $card else tokenize($card,'\.\.')[2]
let $max                := if (string-length($max)>0) then $max else ('*')

return
    <concept minimumMultiplicity="{$min}" maximumMultiplicity="{$max}">
    {
        $conceptid,
        $concepteff,   (:assume DCM effectiveDate  == concept effectiveDate:)
        local:getdcmstatuscode($xmiroot),      (:assume DCM status         == concept status:)
        $concepttype,
        if ($concepttype) then () else (
            <inherit ref="{local:getinheritid($object)}" effectiveDate="{$concepteff}"/>
            ,
            for $overrideobject in $overrideobjects
            return
                local:xmi2concept($xmiroot, $overrideobject, ())
        )
    }
    {
        (:These get moved to scenarios in max2scenario:)
        for $constraint in (local:getconstraints($object) | local:getassociationconstraints($object, $parent))
        return
            <condition>{local:getTaggedValue($constraint,'documentation')}</condition>
        ,
        (:if there is a boundary/collaboration object: get any constraints from that level:)
        if ($boundary) then (
            for $constraint in local:getconstraints($boundary/UML:Namespace.ownedElement/UML:ClassifierRole[local:getTaggedValue(.,'ea_eleType')='element'])
            return
                <condition>{$constraint/notes/node(), if ($choiceobjects) then ('[',string-join(for $obj in ($object | $choiceobjects) return local:getname($obj),' | '),']') else ()}</condition>
        ) else ()
    }
    {
        if ($concepttype) then (
            local:getname($object),
            local:getsynonym($object),
            local:getdesc($object),
            local:getoperationalization($object),
            local:getdcmtags($object),
            if ($concepttype='item') then (
                <valueDomain type="{$valuedomaintype}">
                {if ($valuedomaintype=('code','ordinal')) then
                    <conceptList id="{$conceptid}.0"/>
                 else ()}
                {local:getexamples($object)}
                </valueDomain>
            ) else ()
            ,
            (:dataset root concept:)
            for $childobject in local:getchildconcepts($object)
            return 
                local:max2concept($xmiroot, $childobject, $object)
        ) else ()
    }
    </concept>
};

declare %private function local:expanddatasets($datasets as element(datasets)) as element(datasets) {
    <datasets>
    {
        for $dataset in $datasets/dataset
        return
            <dataset>
            {
                $dataset/@*,
                $dataset/(* except concept),
                for $concept in $dataset/concept
                return local:expandconcept($datasets, $concept, (), false())
            }
            </dataset>
    }
    </datasets>
};
declare %private function local:expandconcept($datasets as element(datasets), $concept as element(concept), $overrideconcepts as element(concept)*, $overridemode as xs:boolean) as element(concept) {
    <concept>
    {
        if ($concept[@id=$overrideconcepts/@id]) then (
            (:this is the concept that is being overridden so we must be in overridemode:)
            
            let $overridingconcept  := $overrideconcepts[@id=$concept/@id]
            let $newid              := local:getnewid('DE')
            return (
                attribute id {$newid},
                $overridingconcept/(@* except @id),
                $overridingconcept/(* except (concept|valueDomain)),
                for $valueDomain in $overridingconcept/valueDomain
                return
                    <valueDomain>
                    {
                        $valueDomain/@*,
                        if ($valueDomain/conceptList) then (
                            <conceptList id="{concat($newid,'.0')}"/>
                        ) else (),
                        $valueDomain/(* except conceptList)
                    }
                    </valueDomain>
                ,
                (:this is unlikely but should cater for it:)
                for $childconcept in $overridingconcept/concept
                return local:expandconcept($datasets, $childconcept, $overrideconcepts, $overridemode)
            )
        )
        else if ($concept[.//@id=$overrideconcepts/@id]) then (
            (:this is an ancestor of an overridden concept so we must be in overridemode:)
            
            attribute id {local:getnewid('DE')},
            $concept/(@* except @id),
            $concept/(* except concept),
            for $childconcept in $concept/concept
            return local:expandconcept($datasets, $childconcept, $overrideconcepts, $overridemode)
        )
        else if ($concept[inherit][concept]) then (
            (:this is the starting point for inheritance with override. assume it already has its proper 
                id+effectiveDate, but pick others from original:)
            
            let $originalconcept    := $datasets//concept[@id=$concept/inherit/@ref]
            
            return (
                $concept/(@id|@effectiveDate),
                $originalconcept/(@* except (@id|@effectiveDate)),
                $originalconcept/(* except concept),
                for $childconcept in $originalconcept/concept
                return local:expandconcept($datasets, $childconcept, ($overrideconcepts | $concept/concept), true())
            )
        )
        else if ($overridemode) then (
            attribute id {local:getnewid('concept')},
            $concept/(@* except @id),
            if ($concept/inherit) then (
                $concept/inherit,
                for $childconcept in $datasets//concept[parent::concept/@id=$concept/inherit/@ref]
                return local:expandconcept($datasets, $childconcept, $overrideconcepts, true())
            ) else (
                <inherit ref="{$concept/@id}" effectiveDate="{$concept/@effectiveDate}"/>,
                for $childconcept in $concept/concept
                return local:expandconcept($datasets, $childconcept, $overrideconcepts, $overridemode)
            )
        ) else (
            $concept/@*,
            $concept/(* except concept),
            if ($concept/inherit) then (
                for $childconcept in $datasets//concept[parent::concept/@id=$concept/inherit/@ref]
                return local:expandconcept($datasets, $childconcept, $overrideconcepts, true())
            ) else (
                for $childconcept in $concept/concept
                return local:expandconcept($datasets, $childconcept, $overrideconcepts, $overridemode)
            )
        )
    }
    </concept>
};
declare %private function local:getnewid($type as xs:string) as xs:string {
    let $currid := $resultdoc/project/@newid
    let $newid  := string(xs:integer($currid[1]) + 1)
    let $update := update value $currid[1] with $newid
    
    return concat($oidBaseDE,'.',$newid)
};
(: ==== DCM LEVEL FUNCTIONS ==== :)

declare %private function local:getdcmroot($object as element()) as element(object) {
    $object/ancestor-or-self::max:model/objects/object[stereotype='DCM']
};

(:  <tag name="DCM::Id" value="2.16.840.1.113883.2.4.3.11.60.40.3.12.5" />:)
declare %private function local:getdcmid($object as element()) as item() {
    (:1-4-2015#NOTES#The date this version was made.:)
    let $id     := local:getidfromstring($object/ancestor-or-self::objects/object[stereotype='DCM']/tag[@name='DCM::Id']/@value)
    
    return
        if ($id) then (
            attribute id {$id}
        ) else (
            error(xs:QName('error:UnsupportedValue'),concat('DCM::Id "',$id,'" not a valid OID.'))
        )
};

(:  <tag name="DCM::PublicationDate" value="1-4-2015">The date this version was made.</tag>
    <tag name="DCM::CreationDate" />
:)
declare %private function local:getdcmeffectivedate($object as element()) as item() {
    let $date       := local:maxdate2datetime($object/ancestor-or-self::objects/object[stereotype='DCM']/tag[@name='DCM::PublicationDate']/@value)
    
    return
        if ($date) then (
            attribute effectiveDate {$date}
        ) else (
            error(xs:QName('error:UnsupportedValue'),concat('DCM::PublicationDate "',$date,'" not a valid date stamp dd-mm-yyyy or d-m-yyyy.'))
        )
};

(:  <tag name="DCM::PublicationDate" value="1-4-2015">The date this version was made.</tag>:)
declare %private function local:getdcmpublicationdate($object as element()) as item()? {
    let $date       := local:maxdate2datetime($object/ancestor-or-self::objects/object[stereotype='DCM']/tag[@name='DCM::PublicationDate']/@value)
    
    return
        if ($date) then (
            attribute officialReleaseDate {$date}
        ) else ()
};

(:  <tag name="DCM::DeprecatedDate" />:)
declare %private function local:getdcmdeprecationdate($object as element()) as item()? {
    let $date       := local:maxdate2datetime($object/ancestor-or-self::objects/object[stereotype='DCM']/tag[@name='DCM::DeprecatedDate']/@value)
    
    return
        if ($date) then (
            attribute expirationDate {$date}
        ) else ()
};

(:  <tag name="DCM::LifecycleStatus" value="Final" />
    
    <tag name="DCM::PublicationStatus" value="Published">The status of the DCM on that moment. Strike out the other status. All status levels are mentioned after the process of development and review are clear (concept).
        Author Draft(en);
        Committee Draft(en);
        Organisation Draft(en);
        Submitted(en);
        Withdrawn
        Rejected(en) 
        Obsolete
        Approved for testing
        Approved for Production Use
        Superseded</tag>
:)
declare %private function local:getdcmstatuscode($object as element()) as item() {
    let $status     := $object/ancestor-or-self::objects/object[stereotype='DCM']/tag[@name='DCM::LifecycleStatus']/@value
    
    return
    switch ($status) 
    case 'Final'        return attribute statusCode {'final'}
    case 'Draft'        return attribute statusCode {'draft'}
    default             return error(xs:QName('error:UnsupportedValue'),concat('DCM::LifecycleStatus "',$status,'" not mapped yet.'))
};

(:  <name>nl.nfu.Ademhaling-v1.0</name>:)
declare %private function local:getdcmversionlabel($object as element()) as item() {
    let $versionlabel   := $object/ancestor-or-self::objects/object[stereotype='DCM']/tag[@name='DCM::Version']/@value
    
    return
        if (matches($versionlabel,'^v?\d+\.\d+')) then
            attribute versionLabel {$versionlabel}
        else ()
};

(:  <UML:TaggedValue tag="DCM::Name" xmi.id="EAID_93434A5A_A47B_4ccd_A140_60A7144E4507" value="nl.nfu.Ademhaling" modelElement="MX_EAID_A42E3995_0040_494e_ABEF_9AE12D626CC7"/>:)
declare %private function local:getdcmname($object as element()) as element(name) {
    let $name           := $object/ancestor-or-self::objects/object[stereotype='DCM']/tag[@name='DCM::Name']/@value
    
    return
    <name language="{$language}">{data($name)}</name>
};

(:  <notes>Rootconcept van de bouwsteen Ademhaling. Dit rootconcept bevat alle gegevenselementen van de bouwsteen Ademhaling.</notes>:)
declare %private function local:getdcmdesc($maxroot as element()) as element(desc) {
    let $purpose        := $maxroot/ancestor::max:model/objects/object[parentId=$maxroot/id][name='Purpose']/notes
    let $descElement    := <documentation>{replace($purpose,'(&#xD;)?&#xA;','<br/>')}</documentation>
    let $desc           := art:parseNode($descElement)
    
    return
    <desc language="{$language}">{$desc/node()}</desc>
};

(:  <tag name="DCM::CoderList" value="Kerngroep Registratie aan de Bron" />:)
declare %private function local:getdcmtags($maxroot as element()) as element(tag)* {
    let $tags           := $maxroot/tag[@value[not(.='')]]
    
    for $tag in $tags[not(@name=('DCM::AssigningAuthority','DCM::DefinitionCode','DCM::ExampleValue','DCM::Id','DCM::ReferencedDefinitionCode','DCM::ValueSet'))]
    return
    <tag name="{$tag/@name}" language="{$language}">{data($tag/@value)}</tag>
};

(:  <notes>Rootconcept van de bouwsteen Ademhaling. Dit rootconcept bevat alle gegevenselementen van de bouwsteen Ademhaling.</notes>:)
declare %private function local:getdcmproperties($maxroot as element()) as element(property)* {
    let $tags           := $maxroot/ancestor::max:model/objects/object[parentId=$maxroot/id][not(name='Purpose')]
    
    for $tag in $tags[notes[not(.='')]]
    let $notes          := <notes>{replace($notes,'(&#xD;)?&#xA;','<br/>')}</notes>
    return
    <property name="{$tag/name}" language="{$language}">{art:parseNode($tag/notes)/node()}</property>
};

(: ==== DCM LEVEL FUNCTIONS ==== :)
(: ==== HELPER FUNCTIONS    ==== :)
declare %private function local:getallrelationships($object as element()) as element()* {
    $object/ancestor::max:model/relationships/relationship[destId=$object/id]
};
declare %private function local:getallchildren($object as element(object)) as element(object)* {
    let $allrelationships   := local:getallrelationships($object)[type='Aggregation']
    return
        $object/ancestor::max:model/objects/object[id=$allrelationships/sourceId]
};
declare %private function local:getallparents($object as element()) as element()* {
    let $allrelationships   := $object/../UML:Association[local:getTaggedValue(.,'ea_sourceType')='Class'][local:getTaggedValue(.,'ea_targetType')='Class']/UML:Association.connection[UML:AssociationEnd[@isNavigable='false'][@type=$object/@xmi.id]]
    return
        $object/ancestor::UML:Package//*[@xmi.id=$allrelationships/UML:AssociationEnd[@isNavigable='true']/@type]
};

declare %private function local:getchildconcepts($object as element(object)) as element(object)* {
    let $allchildren   := local:getallchildren($object)
    return $allchildren[type='Class'][stereotype=('container','data','context')]
};

declare %private function local:getgeneralizations($object as element()) as element(UML:Generalization)* {
    $object/../UML:Generalization[@subtype=$object/@xmi.id]
};
declare %private function local:getdependencies($object as element()) as element(UML:Dependency)* {
    $object/../UML:Dependency[@supplier=$object/@xmi.id]
};
declare %private function local:getassociationconstraints($object as element(), $parent as element()?) as element(UML:Constraint)* {
    if ($parent) then (
        let $parentassociation  := $object/../UML:Association[UML:Association.connection[UML:AssociationEnd[@isNavigable='false'][@type=$object/@xmi.id]][UML:AssociationEnd[@type=$parent/@xmi.id]]]
        
        return
            $object/../UML:Constraint[contains(local:getTaggedValue(.,'relatedlinks'),$parentassociation/@xmi.id)]
    ) else ()
};
declare %private function local:getconstraints($object as element()) as element(UML:Constraint)* {
    (:<EANoteLink xmi.id="EAID_409CA25D_E703_4f09_A833_10286BC4B1DE" source="EAID_882590D2_80A1_4edc_BF17_DB5F1E1495BB" target="EAID_9C9C3F9D_FB38_4528_B088_FDFCD867EA84">:)
    (:<UML:Constraint xmi.id="EAID_9C9C3F9D_FB38_4528_B088_FDFCD867EA84" visibility="public" namespace="EAPK_D1A4F12C_2E43_4670_AC69_814BB21A8E8A">
        <UML:ModelElement.taggedValue>
            <UML:TaggedValue tag="documentation" value="Als Toegestaan = Ja, maar met beperkingen (JA_MAAR)"/>
            ...
        </UML:ModelElement.taggedValue>
    </UML:Constraint>:)
    (
        for $link in $object/ancestor::XMI//EANoteLink[@source=$object/@xmi.id]
        return $object/ancestor::UML:Package[1]/UML:Namespace.ownedElement/UML:Constraint[@xmi.id=$link/@target]
        ,
        for $link in $object/ancestor::XMI//EANoteLink[@target=$object/@xmi.id]
        return $object/ancestor::UML:Package[1]/UML:Namespace.ownedElement/UML:Constraint[@xmi.id=$link/@source]
    )
};

declare %private function local:getotherchildren($object as element(object)) as element(object)* {
    let $allchildren   := local:getallchildren($object)
    return $allchildren[not(type='Class' and stereotype=('container','data','context'))]
};

(: Input string that either -is- an OID or -contains- an OID. Output is just the OID :)
declare %private function local:getidfromstring($idstring as xs:string?) as xs:string? {
    let $idstring   := tokenize(normalize-space($idstring), '#')[1]
    
    let $id         :=
        if (matches($idstring,'^[0-2](\.(0|[1-9]\d*))*$')) then (
            $idstring
        )
        else if (starts-with($idstring,'OID')) then (
            tokenize(replace($idstring,'OID\s*:?\s*',''),'\s')[1]
        )
        else if (starts-with($idstring,'NL-CM-VS:')) then (
            tokenize(replace($idstring,'NL-CM-VS:\s*',concat($oidBaseVS,'.')),'\s')[1]
        )
        else if (starts-with($idstring,'NL-CM:')) then (
            tokenize(replace($idstring,'NL-CM:\s*',concat($oidBaseEL,'.')),'\s')[1]
        )
        else ()
    return
        if (matches($id,'^[0-2](\.(0|[1-9]\d*))*$')) then ($id) else ()
};

(: Get the @multiplicity from the non-navigable counterpart of an association :)
declare %private function local:getMultiplicity($object as element(), $parent as element()?) as xs:string? {
    $object/ancestor-or-self::max:model/relationships/relationship[sourceId=$object/id]/sourceCard
};

(: Input string d-m-yyyy (single or double digits allowed for day and month. Output date/time string YYYY-MM-DDT00:00:00 or nothing :)
declare %private function local:maxdate2datetime($date as xs:string?) as xs:string? {
    let $date       := tokenize($date, '#')[1]
    let $year       := tokenize($date, '-')[3]
    let $month      := tokenize($date, '-')[2]
    let $month      := if (string-length($month)=1) then concat('0',$month) else ($month)
    let $day        := tokenize($date, '-')[1]
    let $day        := if (string-length($day)=1) then concat('0',$day) else ($day)
    
    let $newdate    := string-join(($year,$month,$day),'-')
    
    return
        if ($newdate castable as xs:date) then (concat($newdate,'T00:00:00')) else ()
};

(: ====         HELPER FUNCTIONS        ==== :)
(: ==== CLASS/CONTAINER LEVEL FUNCTIONS ==== :)

(: Get DECOR valueDomain type based on Generalization of input element:)
declare %private function local:getValueDomainType($object as element(object)) as xs:string {
    let $generalizations    := $object/ancestor::max:model/relationships/relationship[sourceId=$object/id][type='Generalization']
    (:<UML:TaggedValue tag="ea_localid" value="7891"/>:)
    let $commondatatype     := $xmimodels//UML:TaggedValue[@tag='ea_localid'][@value=$generalizations/destId]/ancestor::UML:Class[last()]/@name
    
    return
    if (count($generalizations)=1 and count($commondatatype)=1) then (
        switch ($commondatatype)
        case 'ANY'  return 'complex'
        case 'BL'   return 'boolean'
        case 'CD'   return 'code'
        case 'CO'   return 'ordinal'
        case 'ED'   return 'blob'
        case 'II'   return 'identifier'
        case 'INT'  return 'count'
        case 'PQ'   return 'quantity'
        case 'ST'   return 'string'
        case 'TS'   return 'datetime'
        case 'UCUM' return 'code'
        default     return 
        error(xs:QName('error:ClassDatatypeError'),concat('BS "',local:getname(local:getdcmroot($object)),'" object/id "',$object/id,'" ',local:getname($object),' has a Generalization relationship to "',$commondatatype,'", hence we do not know what the datatype should be.')) 
    )
    else if (count($generalizations)=0) then (
        error(xs:QName('error:ClassDatatypeError'),concat('BS "',local:getname(local:getdcmroot($object)),'" object/id "',$object/id,'" ',local:getname($object),' does not have a Generalization relationship, hence we do not know what the datatype should be.'))
    )
    else if (count($generalizations)>1) then (
        error(xs:QName('error:ClassDatatypeError'),concat('BS "',local:getname(local:getdcmroot($object)),'" object/id "',$object/id,'" ',local:getname($object),' has multiple Generalization relationships, hence we do not know what the datatype should be.'))
    )
    else if (count($commondatatype)=0) then (
        error(xs:QName('error:ClassDatatypeError'),concat('BS "',local:getname(local:getdcmroot($object)),'" object/id "',$object/id,'" ',local:getname($object),' does not have a corresponding UML:Class in Common.xmi for id=',$generalizations/destId,', hence we do not know what the datatype should be.'))
    )
    else (
        error(xs:QName('error:ClassDatatypeError'),concat('BS "',local:getname(local:getdcmroot($object)),'" object/id "',$object/id,'" ',local:getname($object),' maps onto multiple datatypes in Common.xmi, hence we do not know what the datatype should be.'))
    )
};

(:  <tag name="DCM::AssigningAuthority" value="UZI register abonneenummer (URA)">OID: 2.16.528.1.1007.3.3</tag>:)
declare %private function local:getoperationalization($object as element(object)) as element(operationalization)? {
    let $tags       := ($object/tag[@name='DCM::AssigningAuthority'], $object/UML:ModelElement.constraint/UML:Constraint/@name)
    let $content    :=
        for $tag at $i in $tags
        let $value      := $tag/@value/string()
        let $notes      := $tag/text()[string-length()>0]
        order by lower-case($value)
        return (
            if ($notes) then concat($value,' (',string-join($notes,'#'),')') else ($value)
        )
    
    return
        if (exists($content)) then (
            <operationalization language="{$language}">
            {
                if (count($content)=1) then ($content) else (
                    <ul>{for $c in $content return <li>{$c}</li>}</ul>
                )
            }
            </operationalization>
        ) else ()
};

(:  <tag name="DCM::Id" value="2.16.840.1.113883.2.4.3.11.60.40.3.12.5" />:)
declare %private function local:getid($object as element()) as item() {
    let $ids     := 
        for $id in $object/tag[@name='DCM::DefinitionCode'][starts-with(@value,'NL-CM:')]/@value
        return local:getidfromstring($id)
    
    return
    attribute id {$ids}
};

(:  <tag name="DCM::ReferencedDefinitionCode" value="NL-CM:10.1.1">Dit is een verwijzing naar het concept MedischHulpmiddel in de bouwsteen MedischHulpmiddel.</tag>:)
declare %private function local:getinheritid($object as element()) as item() {
    let $ids     := 
        for $id in $object/tag[@name='DCM::ReferencedDefinitionCode'][starts-with(@value,'NL-CM:')]/@value
        return local:getidfromstring($id)
    
    return
    attribute ref {$ids}
};

(:  <name>Ademhaling</name>:)
declare %private function local:getname($object as element()) as element(name) {
    <name language="{$language}">{data($object/name)}</name>
};

(:  <alias>Ademhaling</alias>:)
declare %private function local:getsynonym($object as element()) as element(synonym)* {
    for $alias in $object/alias
    return
    <synonym language="{$language}">{normalize-space($alias)}</synonym>
};

(:  <notes>Rootconcept van de bouwsteen Ademhaling. Dit rootconcept bevat alle gegevenselementen van de bouwsteen Ademhaling.</notes>:)
declare %private function local:getdesc($object as element()) as element(desc) {
    let $descElement    := <notes>{replace($object/notes,'(&#xD;)?&#xA;','<br/>')}</notes>
    let $desc           := art:parseNode($descElement)
    
    return
    <desc language="{$language}">{$desc/node()}</desc>
};

(:  Get concept type from its relationships:)
declare %private function local:gettype($object as element()) as item()? {
    let $childconcepts  := local:getchildconcepts($object)
    let $inherits       := local:conceptinherits($object)
    
    return
        if ($inherits=true()) then ()
        else if ($childconcepts) then 
            attribute type {'group'}
        else
            attribute type {'item'}
};

declare %private function local:conceptinherits($object as element(object)) as xs:boolean {
    $object/tag/@name='DCM::ReferencedDefinitionCode'
};

(:  <tag name="DCM::ExampleValue" value="Officieel adres"/>:)
declare %private function local:getexamples($object as element()) as element(example)* {
    for $tag in $object/tag[@name='DCM::ExampleValue']
    let $value      := $tag/@value/string()
    let $notes      := $tag/text()[string-length()>0]
    order by lower-case($value)
    return
        <example>{if ($notes) then concat($value,' (',string-join($notes,'#'),')') else ($value)}</example>
};

(: ==== CLASS/CONTAINER LEVEL FUNCTIONS ==== :)
(: ==== DECOR FUNCTIONS ==== :)

declare %private function local:max2terminology($maxmodels as element(max:model)*) as item()* {
    (:do terminologyAssociations:)
    for $maxmodel in $maxmodels
    let $maxroot    := local:getdcmroot($maxmodel)
    return (
        '&#10;',
        comment {local:getdcmname($maxroot)/text()},
        '&#10;',
        for $object in $maxmodel/objects/object
        let $objectid       := local:getid($object)
        order by $object/id
        return (
            (:<tag name="DCM::DefinitionCode" value="SNOMED CT: 250774007 Inspired oxygen concentration" />:)
            for $tag in $object/tag[@name='DCM::DefinitionCode'][not(starts-with(@value,'NL-CM:'))]/@value
            let $codeSystemName := substring-before($tag,':')
            let $code           := tokenize(normalize-space(substring-after($tag,': ')),'\s')[1]
            let $displayName    := normalize-space(substring-after($tag,$code))
            let $codeSystem     :=
                switch ($codeSystemName)
                case 'LNC'          return '2.16.840.1.113883.6.1'
                case 'LOINC'        return '2.16.840.1.113883.6.1'
                case 'SNOMED CT'    return '2.16.840.1.113883.6.96'
                default             return error(xs:QName('error:UnsupportedValue'),concat('DCM::DefinitionCode "',$tag,'". Could not determine codeSystem OID from "',$codeSystemName,'"'))
            order by lower-case($tag)
            return 
                <terminologyAssociation conceptId="{$objectid}" code="{$code}" codeSystem="{$codeSystem}" displayName="{$displayName}"/>
            ,
            (:<tag name="DCM::ValueSet" value="RitmeCodelijst">OID: 2.16.840.1.113883.2.4.3.11.60.40.2.12.5.1</tag>:)
            for $tag in $object/tag[@name='DCM::ValueSet']
            let $valuesetid     := local:getidfromstring($tag)
            let $valueseteff    := local:getdcmeffectivedate($maxroot)
            order by $valuesetid
            return
                <terminologyAssociation conceptId="{$objectid}.0" valueSet="{$valuesetid}" flexibility="{$valueseteff}"/>
        )
    )
    ,
    (:do valueSets:)
    for $maxmodel in $maxmodels
    let $maxroot    := local:getdcmroot($maxmodel)
    return (
        for $object in $maxmodel/objects/object
        let $objectid       := local:getid($object)
        order by $object/id
        return (
            (:  <tag name="DCM::ValueSet" value="VeroorzakendeStofHPKCodelijst">OID: 2.16.840.1.113883.2.4.3.11.60.40.2.8.2.19</tag>
                <tag name="DCM::ValueSet" value="VeroorzakendeStofAllergeneStoffenCodelijst">OID: 2.16.840.1.113883.2.4.3.11.60.40.2.8.2.17&#10;&#10;Allergene stoffen niet gerelateerd aan medicatiebewaking.</tag>:)
            for $tag in $object/tag[@name='DCM::ValueSet']
            let $valuesetid     := local:getidfromstring($tag)
            let $valuesetname   := normalize-space($tag/@value)
            let $valuesetdisp   := normalize-space(substring-after($tag, $valuesetid))
            let $valuesetdisp   := if (string-length($valuesetdisp)>0) then $valuesetdisp else $valuesetname
            let $valueseteff    := local:getdcmeffectivedate($maxroot)
            let $valuesetsts    := local:getdcmstatuscode($maxroot)
            order by $valuesetid
            return
                local:getdecorvalueset($object, $valuesetid, $valuesetname)
                (:<valueSet id="{$valuesetid}" name="{$valuesetname}" displayName="{$valuesetdisp}" effectiveDate="{$valueseteff}" statusCode="{$valuesetsts}">
                {
                    local:getxmivalueset($object, $valuesetid, $valuesetname)
                }
                </valueSet>:)
        )
    )
};

declare %private function local:getdecorvalueset($object as element(), $id as xs:string?, $name as xs:string?) as element(valueSet) {
    let $valueSet   := $preparedvalsets[@id=$id] | $preparedvalsets[lower-case(@name)=lower-case($name)]
    
    return
    if ($valueSet) then (
        <valueSet>
        {
            $valueSet/@id,
            $valueSet/@name,
            $valueSet/@displayName,
            attribute effectiveDate {local:getdcmeffectivedate($object)},
            attribute statusCode {local:getdcmstatuscode($object)},
            $valueSet/(@* except (@id|@name|@displayName|@effectiveDate|@statusCode)),
            $valueSet/node()
        }
        </valueSet>
    ) else (
        error(xs:QName('error:ValueSetError'),concat('BS "',local:getname(local:getdcmroot($object)),'" object/id "',$object/id,'" ',local:getname($object),' refers to valueset id=',$id,' name=',$name,' but this valueSet is not found in the ',count($preparedvalsets),' prepared value sets'))
    )
};

declare %private function local:getxmivalueset($object as element(object), $valuesetid as xs:string, $valuesetname as xs:string) as item()* {
    let $storecoll      := '/db/apps/decor/staging'
    let $storeres1      := 'test.zip'
    let $storeres2      := 'test.rtf'
    
    let $dependencies   := ($object/ancestor::max:model/relationships/relationship[type='Dependency'][destId=$object/id] | 
                            $object/ancestor::max:model/relationships/relationship[type='Dependency'][sourceId=$object/id])
                            
    let $check          :=
        if ($dependencies) then () else (
            error(xs:QName('error:ValueSetError'),concat('BS "',local:getname(local:getdcmroot($object)),'" object/id "',$object/id,'" ',local:getname($object),' does not have relations of type Dependency, hence we do not know what the valueset should be.'))
        ) 
    
    let $dependobjects  := ($object/ancestor::max:model/objects/object[stereotype='document'][type='Artifact'][id=$dependencies/sourceId] |
                            $object/ancestor::max:model/objects/object[stereotype='document'][type='Artifact'][id=$dependencies/destId])
    let $dependobject   := $dependobjects[name=$valuesetname]
    
    let $check          :=
        if ($dependobject) then () else (
            error(xs:QName('error:ValueSetError'),concat('BS "',local:getname(local:getdcmroot($object)),'" object/id "',$object/id,'" ',local:getname($object),' does not relate to an Artifact object of type document named "',$valuesetname,'" (found ',string-join($dependobjects/name,'/'),'), hence we do not know what the valueset should be.'))
        )
    
    let $xmiobject      := $xmimodels//UML:Class[@name=$dependobject/name][*/UML:TaggedValue[@tag="ea_localid"][@value=$dependobject/id]]/*/UML:TaggedValue[@tag="modeldocument"]
    
    let $check          :=
        if (count($xmiobject)=1) then ()
        else if (count($xmiobject)>1) then (
            error(xs:QName('error:ValueSetError'),concat('BS "',local:getname(local:getdcmroot($object)),'" object/id "',$object/id,'" ',local:getname($object),' is connected to multiple value sets "',string-join($dependobject/concat(id,' ',name),' / '),', hence we do not know what the valueset should be.'))
        )
        else (
            error(xs:QName('error:ValueSetError'),concat('BS "',local:getname(local:getdcmroot($object)),'" object/id "',$object/id,'" ',local:getname($object),' is connected to a value set under id "',$dependobject/id,'" ',$dependobject/name,' but we cannot find it in XMI, hence we do not know what the valueset should be.'))
        )
    
    let $storedzip      := xmldb:store($storecoll, $storeres1, xs:base64Binary($xmiobject[1]))
    let $entryCb        := function($path as xs:anyURI, $type as xs:string, $params as item()*) { $type='resource' }
    let $dataCb         := function($path as xs:anyURI, $type as xs:string, $data as item()?, $params as item()*) { $data }
    let $unzipped-stuff := compression:unzip(util:binary-doc($storedzip), $entryCb, (), $dataCb, ())
    let $storedrtf      := xmldb:store($storecoll, $storeres2, $unzipped-stuff)
    let $rtfstuff       := util:binary-doc($storedrtf)
    let $rtf2html       := contentextraction:get-metadata-and-content($rtfstuff)
    
    let $rtfid          := 
        if ($rtf2html//text()[normalize-space(.)=$valuesetid]) then ($valuesetid) else (
            local:getidfromstring($rtf2html//text()[contains(.,'OID')][1])
        )
    let $check          :=
        if ($valuesetid=($rtfid,'2.16.840.1.113883.2.4.3.11.60.40.2.8.2.2','2.16.840.1.113883.2.4.3.11.60.40.2.9.5.4')) then ()
        else (
            error(xs:QName('error:ValueSetError'),concat('BS "',local:getname(local:getdcmroot($object)),'" object/id "',$object/id,'" ',local:getname($object),' is connected to a value set document that has a different id "',$rtfid,'" than the DCM::ValueSet id "',$valuesetid,'" hence we cannot be sure about that id.'))
        )
    
    let $decorvalueset  := local:getdecorvaluesetfromxhtml($rtf2html)
    let $descelements   := $decorvalueset[self::desc]
    return (
        if ($descelements) then (
            <desc language="{$descelements[1]/@language}">{$descelements/node()}</desc>
        ) else (),
        $decorvalueset[not(self::desc)]
    )
};
(:
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta name="Content-Type" content="application/rtf"/>
            <title/>
        </head>
        <body>
            <p>
                <b>RitmeCodelijst</b>
            </p>
            <p>
                <b/>
                <b>OID: 2.16.840.1.113883.2.4.3.11.60.40.2.12.5.1</b>
            </p>
            <p>
                <b/>Concept Name</p>
            <p>Concept Code</p>
            <p>CodeSys. Name</p>
            <p>CodeSystem OID</p>
            <p>Description</p>
            <p>Normal respiratory rhythm</p>
            <p>5467003</p>
            <p>SNOMED CT</p>
            <p>2.16.840.1.113883.6.96</p>
            <p>Normaal ademhalingsritme</p>
            <p>Abnormal respiratory rhythm</p>
            <p>58617008</p>
            <p>SNOMED CT</p>
            <p>2.16.840.1.113883.6.96</p>
            <p>Abnormaal ademhalingsritme</p>
        </body>
    </html>
:)
declare %private function local:getdecorvaluesetfromxhtml($html as item()) as element()* {
    let $body           := $html//*:body
    
    let $vsname         := normalize-space(string-join($body/*:p[1],''))
    let $vsoid          := normalize-space(string-join($body/*:p[2],''))
    
    (:/position() doesn't get the desired result:)
    let $end-hdr-mrkr   := count($body/*:p[.//text()='Description']/preceding-sibling::*:p) + 1
    
    let $stt-hdr        := 3
    let $end-hdr        := if ($end-hdr-mrkr > 1) then $end-hdr-mrkr else 5
    let $stt-bdy        := $end-hdr + 1
    let $end-bdy        := count($body/*:p)
    
    let $check          :=
        if ($stt-hdr < $end-hdr) then (
            if ($stt-bdy < $end-bdy) then (
            )
            else (
                error(xs:QName('error:ValueSetParsingError'),concat('Value Set "',$vsname,'" id="',$vsoid,'" has an unparseable structure. Body start "',$stt-bdy,'" is not before body end "',$end-bdy,'".'))
            )
        )
        else (
            error(xs:QName('error:ValueSetParsingError'),concat('Value Set "',$vsname,'" id="',$vsoid,'" has an unparseable structure. Header start "',$stt-hdr,'" is not before header end "',$end-hdr,'".'))
        )
    
    let $col-count  := $end-hdr - $stt-hdr + 1
    let $row-count  := ($end-bdy - $stt-bdy + 1) div $col-count
    
    let $row-count  :=
        if ($row-count castable as xs:integer) then (xs:integer($row-count)) else (
            error(xs:QName('error:ValueSetParsingError'),concat('Value Set "',$vsname,'" id="',$vsoid,'" has an unparseable structure. The number of columns "',$col-count,'" is not equal across all body rows.'))
        )
    
    let $vsmode     := 
        switch ($col-count)
        case 3  return 'codeSystems'
        case 5  return 'codes'
        case 6  return 'codedOrdinals'
        default return (
            error(xs:QName('error:ValueSetParsingError'),concat('Value Set "',$vsname,'" id="',$vsoid,'" has an unparseable structure. We support 3 columns (complete codesystems) and 5 columns (list of codes). Found ',$col-count,' columns.'))
        )
    
    return (
        (:$body/*:p[.//text()='Description'],
        <header columns="{$col-count}" body-rows="{$row-count}" end-header-marker="{$end-hdr-mrkr}" mode="{$vsmode}">{
            for $i in (3 to $end-hdr)
            return <column>{normalize-space(data($body/*:p[$i]))}</column>
            ,:)
            switch ($vsmode)
            case 'codeSystems' return (
                for $i in (1 to $row-count)
                let $stt            := $stt-bdy + (($i -1) * $col-count)
                let $codes          := normalize-space(string-join($body/*:p[$stt],''))
                let $codeSystemName := normalize-space(string-join($body/*:p[$stt + 1],''))
                let $codeSystem     := normalize-space(string-join($body/*:p[$stt + 2],''))
                return (
                    if (string-length($codes)=0) then () else (
                        <desc language="{$language}">{$codes}</desc>
                    ),
                    <completeCodeSystem codeSystem="{$codeSystem}">
                    {
                        if (string-length($codeSystemName)=0) then () else (
                            attribute codeSystemName {$codeSystemName}
                        )
                    }
                    </completeCodeSystem>
                )
            )
            case 'codes'      return (
                <conceptList>
                {
                    for $i in (1 to $row-count)
                    let $stt            := $stt-bdy + (($i -1) * $col-count)
                    let $displayName    := normalize-space(string-join($body/*:p[$stt],''))
                    let $code           := normalize-space(string-join($body/*:p[$stt + 1],''))
                    let $codeSystemName := normalize-space(string-join($body/*:p[$stt + 2],''))
                    let $codeSystem     := normalize-space(string-join($body/*:p[$stt + 3],''))
                    let $description    := normalize-space(string-join($body/*:p[$stt + 4],''))
                    return
                        <concept level="0" type="L" code="{$code}" codeSystem="{$codeSystem}">
                        {
                            if (string-length($displayName)=0) then () else (
                                attribute displayName {$displayName}
                            ),
                            if (string-length($codeSystemName)=0) then () else (
                                attribute codeSystemName {$codeSystemName}
                            ),
                            if (string-length($description)=0) then () else (
                                <desc language="{$language}">{$description}</desc>
                            )
                        }
                        </concept>
                }
                </conceptList>
            )
            case 'codedOrdinals'      return (
                <conceptList>
                {
                    for $i in (1 to $row-count)
                    let $stt            := $stt-bdy + (($i -1) * $col-count)
                    let $displayName    := normalize-space(string-join($body/*:p[$stt],''))
                    let $code           := normalize-space(string-join($body/*:p[$stt + 1],''))
                    let $value          := normalize-space(string-join($body/*:p[$stt + 2],''))
                    let $codeSystemName := normalize-space(string-join($body/*:p[$stt + 3],''))
                    let $codeSystem     := normalize-space(string-join($body/*:p[$stt + 4],''))
                    let $description    := normalize-space(string-join($body/*:p[$stt + 5],''))
                    return
                        <concept level="0" type="L" code="{$code}" codeSystem="{$codeSystem}">
                        {
                            if (string-length($displayName)=0) then () else (
                                attribute displayName {$displayName}
                            ),
                            if (string-length($value)=0) then () else (
                                attribute value {$value}
                            ),
                            if (string-length($codeSystemName)=0) then () else (
                                attribute codeSystemName {$codeSystemName}
                            ),
                            if (string-length($description)=0) then () else (
                                <desc language="{$language}">{$description}</desc>
                            )
                        }
                        </concept>
                }
                </conceptList>
            )
            default           return (
                error(xs:QName('error:ValueSetParsingError'),concat('Value Set "',$vsname,'" id="',$vsoid,'" has an unparseable structure. Internal failure. Got unsupported $vsmode:"',$vsmode,'".'))
            )
        (:}</header>:)
    )
};

declare %private function local:max2scenario($maxmodel as element(max:model), $i as xs:integer, $datasets as element(dataset)*) as element(scenario) {
let $maxroot            := local:getdcmroot($maxmodel)
let $datasetid          := local:getdcmid($maxroot)
let $dataset            := $datasets[@id=$datasetid]

let $check          :=
    if (count($dataset)=1) then () 
    else if (count($dataset)=0) then (
        error(xs:QName('error:InternalScenarioError'),concat('BS "',local:getname(local:getdcmroot($maxmodel)),'" does not have a corresponding dataset with id',$datasetid))
    )
    else (
        error(xs:QName('error:InternalScenarioError'),concat('BS "',local:getname(local:getdcmroot($maxmodel)),'" has multiple datasets with id',$datasetid))
    )

return
    (:<scenario id="2.16.840.1.113883.2.4.3.11.60.39.3.2" effectiveDate="" expirationDate="" officialReleaseDate="" statusCode="" versionLabel="">:)
    <scenario id="{$oidBaseSC}.{$i}">
    {
        $dataset/@effectiveDate,
        $dataset/@statusCode,
        $dataset/@versionLabel,
        $dataset/@officialReleaseDate,
        $dataset/@expirationDate,
        $dataset/name,
        $dataset/desc
    }
        <transaction id="{$oidBaseTR}.{$i + $i - 1}" type="group">
        {
            $dataset/@effectiveDate,
            $dataset/@statusCode
        }
            <name language="nl-NL">Transactiegroep</name>
            <transaction id="{$oidBaseTR}.{$i + $i}" type="stationary" model="ClinicalDocument">
            {
                $dataset/@effectiveDate,
                $dataset/@statusCode
            }
                <name language="nl-NL">Registratie</name>
                <desc language="nl-NL"/>
                <actors>
                    <actor id="{$oidBaseAC}.1" role="sender"/>
                </actors>
                <representingTemplate sourceDataset="{$datasetid}">
                {
                    for $concept in $dataset//concept
                    return
                        <concept ref="{$concept/@id}">
                        {
                            if ($concept[condition]) then (
                                attribute conformance {'C'},
                                for $condition in $concept/condition
                                return
                                    <condition>
                                    {
                                        $concept/@minimumMultiplicity,
                                        $concept/@maximumMultiplicity,
                                        data($condition)
                                    }
                                    </condition>
                                ,
                                <condition conformance="NP"/>
                            ) else (
                                $concept/@minimumMultiplicity,
                                $concept/@maximumMultiplicity
                            )
                        }
                        </concept>
                }
                </representingTemplate>
            </transaction>
        </transaction>
    </scenario>
};

let $xml        :=
    (<?xml-stylesheet type="text/xsl" href="https://assets.art-decor.org/ADAR/rv/DECOR2schematron.xsl"?> |
    <?xml-model href="https://assets.art-decor.org/ADAR/rv/DECOR.xsd" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>)
let $store      := update insert $xml into $resultdoc/document-node()

let $xml        := 
    <project id="{$oidProject}" prefix="{$projectPrefix}" defaultLanguage="{$language}" newid="0">
        <name language="{$language}">Generieke bouwstenen</name>
        <desc language="{$language}"></desc>
        <copyright years="2013" by="NFU" logo="NFU.jpg"/>
        <copyright years="2013" by="AMC" logo="AMC.jpg"/>
        <copyright years="2013" by="Erasmus MC" logo="Erasmus.jpg"/>
        <copyright years="2013" by="LUMC" logo="LUMC.jpeg"/>
        <copyright years="2013" by="MUMC" logo="MUMC.jpg"/>
        <copyright years="2013" by="Radboud UMC" logo="Radboud.jpg"/>
        <copyright years="2013" by="UMCG" logo="UMCG.jpg"/>
        <copyright years="2013" by="UMCU" logo="UMCU.jpeg"/>
        <copyright years="2013" by="VUMC" logo="VUMC.jpg"/>
        <copyright years="2013" by="Nictiz" logo="nictiz.jpg"/>
        <author id="1" username="alexander">Alexander Henket</author>
        <version date="{substring(string(current-dateTime()),1,19)}" by="{get:strCurrentUserName()}">
            <desc language="{$language}">Automatische import van bouwstenen uit XMI. De volgende gegevens zijn de versiegegevens uit de bouwstenen zelf:
                <ul>
                {
                    for $maxmodel at $i in $maxmodels
                    let $maxroot            := local:getdcmroot($maxmodel)
                    let $dcmname            := local:getdcmname($maxroot)
                    let $dcmversion         := local:getdcmversionlabel($maxroot)
                    let $dcmsuperseeds      := $object/ancestor-or-self::objects/object[stereotype='DCM']/tag[@name='DCM::Superseeds']/@value
                    let $dcmrevhistraw      := $maxroot/ancestor::max:model/objects/object[parentId=$maxroot/id][not(name='Revision History')]
                    let $dcmrevhistparsed   := if ($dcmrevhistraw) then (art:parseNode(<div>{replace($dcmrevhistraw/notes,'(&#xD;)?&#xA;','<br/>')}</div>)) else ()
                    return
                        <li><b>{data($dcmname)} versie: {data($dcmversion)}.</b> 
                            {if ($dcmsuperseeds) then (' Deze versie vervangt: ',$dcmsuperseeds) else ()}
                            {$dcmrevhistparsed}
                        </li>
                }
                </ul>
            </desc>
        </version>
    </project>
let $store      := update insert $xml into $resultdoc

let $datasets   := 
    <datasets>
    {
        for $maxmodel at $i in $maxmodels
        return
        local:max2dataset($maxmodel/objects/object[stereotype='rootconcept'])
    }
    </datasets>
let $datasets   := local:expanddatasets($datasets)
let $store      := update insert $datasets into $resultdoc

let $xml        :=
    <scenarios>
        <actors>
            <actor type="person" id="2.16.840.1.113883.2.4.3.11.60.39.7.1">
                <name language="nl-NL">Verwijzer ZH A</name>
                <desc language="nl-NL">Verwijzer in Ziekenhuis A</desc>
            </actor>
            <actor type="organization" id="2.16.840.1.113883.2.4.3.11.60.39.7.2">
                <name language="nl-NL">ZH B</name>
                <desc language="nl-NL">Ziekenhuis B</desc>
            </actor>
        </actors>
    {
        for $maxmodel at $i in $maxmodels
        return
        local:max2scenario($maxmodel, $i, $datasets/dataset)
    }
    </scenarios>
let $store      := update insert $xml into $resultdoc

let $xml        := 
    <ids>
        <baseId id="{$oidBaseBS}" type="DS" prefix="NL-BS-"/>
        <baseId id="{$oidBaseEL}" type="DE" prefix="NL-CM-"/>
        <baseId id="{$oidBaseVS}" type="VS" prefix="NL-CM-VS-"/>
        <baseId id="{$oidBaseDS}" type="DS" prefix="nfu-dataset-" default="true"/>
        <baseId id="{$oidBaseDE}" type="DE" prefix="nfu-dataelement-" default="false"/>
        <baseId id="{$oidBaseSC}" type="SC" prefix="nfu-scenario-" default="true"/>
        <baseId id="{$oidBaseTR}" type="TR" prefix="nfu-transaction-" default="true"/>
        <baseId id="2.16.840.1.113883.2.4.3.11.60.39.5" type="CS" prefix="nfu-codesystem-" default="true"/>
        <baseId id="2.16.840.1.113883.2.4.3.11.60.39.6" type="IS" prefix="nfu-issue-" default="true"/>
        <baseId id="{$oidBaseAC}" type="AC" prefix="nfu-actor-" default="true"/>
        <baseId id="2.16.840.1.113883.2.4.3.11.60.39.8" type="CL" prefix="nfu-conceptlist-" default="true"/>
        <baseId id="2.16.840.1.113883.2.4.3.11.60.39.9" type="EL" prefix="nfu-template-element-" default="true"/>
        <baseId id="2.16.840.1.113883.2.4.3.11.60.39.10" type="TM" prefix="nfu-template-" default="false"/>
        <baseId id="2.16.840.1.113883.2.4.6.10.21" type="TM" prefix="nfu-template-" default="false"/>
        <baseId id="2.16.840.1.113883.2.4.6.10.21.1" type="TM" prefix="nfu-template-" default="true"/>
        <baseId id="2.16.840.1.113883.2.4.6.10.21.2" type="TM" prefix="nfu-template-" default="false"/>
        <baseId id="2.16.840.1.113883.2.4.6.10.21.3" type="TM" prefix="nfu-template-" default="false"/>
        <baseId id="2.16.840.1.113883.2.4.3.11.60.39.11" type="VS" prefix="nfu-valueset-" default="true"/>
        <baseId id="2.16.840.1.113883.2.4.3.11.60.39.16" type="RL" prefix="nfu-rule-" default="true"/>
        <baseId id="2.16.840.1.113883.2.4.3.11.60.39.17" type="TX" prefix="nfu-test-data-element-" default="true"/>
        <baseId id="2.16.840.1.113883.2.4.3.11.60.39.18" type="SX" prefix="nfu-test-scenario-" default="true"/>
        <baseId id="2.16.840.1.113883.2.4.3.11.60.39.19" type="EX" prefix="nfu-example-instance-" default="true"/>
        <baseId id="2.16.840.1.113883.2.4.3.11.60.39.20" type="QX" prefix="nfu-test-requirement-" default="true"/>
        <baseId id="2.16.840.1.113883.2.4.3.11.60.39.21" type="CM" prefix="nfu-community-" default="true"/>
        <defaultBaseId id="{$oidBaseBS}" type="DS"/>
        <defaultBaseId id="{$oidBaseDE}" type="DE"/>
        <defaultBaseId id="{$oidBaseSC}" type="SC"/>
        <defaultBaseId id="{$oidBaseTR}" type="TR"/>
        <defaultBaseId id="2.16.840.1.113883.2.4.3.11.60.39.5" type="CS"/>
        <defaultBaseId id="2.16.840.1.113883.2.4.3.11.60.39.6" type="IS"/>
        <defaultBaseId id="{$oidBaseAC}" type="AC"/>
        <defaultBaseId id="2.16.840.1.113883.2.4.3.11.60.39.8" type="CL"/>
        <defaultBaseId id="2.16.840.1.113883.2.4.3.11.60.39.9" type="EL"/>
        <defaultBaseId id="2.16.840.1.113883.2.4.6.10.21.1" type="TM"/>
        <defaultBaseId id="2.16.840.1.113883.2.4.3.11.60.39.11" type="VS"/>
        <defaultBaseId id="2.16.840.1.113883.2.4.3.11.60.39.16" type="RL"/>
        <defaultBaseId id="2.16.840.1.113883.2.4.3.11.60.39.17" type="TX"/>
        <defaultBaseId id="2.16.840.1.113883.2.4.3.11.60.39.18" type="SX"/>
        <defaultBaseId id="2.16.840.1.113883.2.4.3.11.60.39.19" type="EX"/>
        <defaultBaseId id="2.16.840.1.113883.2.4.3.11.60.39.20" type="QX"/>
        <defaultBaseId id="2.16.840.1.113883.2.4.3.11.60.39.21" type="CM"/>
    </ids>
let $store      := update insert $xml into $resultdoc

let $xml        := 
    <terminology>
    {
        local:max2terminology($maxmodels)
    }
    </terminology>
let $store      := update insert $xml into $resultdoc

let $xml        := <rules/>
let $store      := update insert $xml into $resultdoc

let $xml        := <issues/>
let $store      := update insert $xml into $resultdoc

let $update     := update delete $resultdoc//project/@newid
let $update     := update delete $resultdoc//datasets//condition
let $update     := update delete $resultdoc//datasets//@minimumMultiplicity
let $update     := update delete $resultdoc//datasets//@maximumMultiplicity
let $update     := update delete $resultdoc//ul/br
let $update     := update delete $resultdoc//ol/br

let $doc        := doc($doc)
let $check      := $preparedvalsets[not(@id=$doc//valueSet/@id)]

return 
    if ($check) then (
        <missing-valuesets saved="{count($doc//valueSet)}" prepared="{count($preparedvalsets)}">{$check}</missing-valuesets>
    ) else ($doc)