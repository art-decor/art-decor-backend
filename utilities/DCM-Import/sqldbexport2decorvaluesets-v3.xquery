xquery version "3.0";

import module namespace art     = "http://art-decor.org/ns/art" at "/db/apps/art/modules/art-decor.xqm";
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "/db/apps/art/modules/art-decor-settings.xqm";

(:<model xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" exportDate="9-4-2015 10:40:01" xmlns="http://www.umcg.nl/MAX">:)
declare namespace UML           = "omg.org/UML1.3";
declare namespace max           = "http://www.umcg.nl/MAX";
declare namespace error         = "http://art-decor.org/ns/decor/import/error";
declare namespace xs            = "http://www.w3.org/2001/XMLSchema";

(:~Project prefix:)
declare variable $projectPrefix       := 'zib2024bbr-';
declare variable $vscollection        := '/db/apps/decor/staging/ZIBS2024.3(XMI)-20242910';

declare variable $pubdate2024.1       := '2024-04-15T00:00:00';
declare variable $pubdate             := $pubdate2024.1;

declare variable $language            := 'nl-NL';
declare variable $resultcollection    := '/db/apps/decor/staging';
declare variable $resultfile          := concat($projectPrefix, 'decor-valuesets.xml');

declare variable $refsetPattern       := '.*SNOMED[- ]?CT: \^(\d+)\s*\|?\s*(.*)\s*\|?.*';
declare variable $nederlandseLabCodes := '2.16.840.1.113883.2.4.3.11.51.1';

let $valueSets          := 
    (:<valueset name="VeroorzakendeStofSNKCodelijst" OID="2.16.840.1.113883.2.4.3.11.60.40.2.8.2.14" Element_EA_guid="{DA05F464-3690-43da-968F-ED280FBB5C6C}" ElementID="11833" Document_EA_guid="{241FAEBD-DA01-4e79-AC5C-CD8FA4618F50}" DocumentID="11849">:)
    for $valueSet in collection($vscollection)//valueSet
    let $deprecatedItems    := ($valueSet/completeCodeSystem/@codeSystemName | $valueSet/conceptList/*/@displayName)[matches(., 'DEPRECATED', 'i')]
    let $activeItems        := ($valueSet/completeCodeSystem/@codeSystemName | $valueSet/conceptList/*/@displayName)[not(matches(., 'DEPRECATED', 'i'))]
    return (
        comment { util:document-name($valueSet) }
        ,
        <valueSet>
        {
            for $att in $valueSet/@*
            let $name   := $att/name()
            let $value  :=
                switch ($name)
                case 'name' return replace($att,'\+','_')
                case 'effectiveDate' return if ($att castable as xs:dateTime) then $att else $pubdate
                case 'statusCode' return 
                    if ($att = 'final') then 
                        if (empty($activeItems) and $deprecatedItems) then 'deprecated' else $att
                    else $att
                default return normalize-space($att)
            return
                attribute {$name} {$value}
        }
        {
            for $desc in $valueSet/desc[@language] | $valueSet/completeCodeSystem/desc[@language]
            let $lang   := $desc/@language
            group by $lang
            return 
                element {$desc[1]/name()} {
                    $desc[1]/@language,
                    if (count($desc) = 1) then normalize-space($desc) else (<ul>{for $d in $desc return <li>{normalize-space($d)}</li>}</ul>)
                }
        }
        {
            (: <sourceCodeSystem id="2.16.840.1.113883.6.96" identifierName="SNOMED CT"/> :)
            (: skip empty members :)
            for $sourceCodesystem in $valueSet/sourceCodeSystem[string-length(@id) gt 0]
            let $id    := replace($sourceCodesystem/(@id | @codeSystem)/normalize-space(),'^2\.16\.840\.1\.13883','2.16.840.1.113883')
            group by $id
            return
                <sourceCodeSystem>
                {
                    attribute id {$id},
                    attribute identifierName {$sourceCodesystem[1]/(@identifierName | @codeSystemName)/normalize-space()}
                }
                </sourceCodeSystem>
        }
        {
            let $completeCodeSystems  :=
                (:<completeCodeSystem codeSystem="2.16.840.1.113883.6.96" codeSystemName="SNOMED CT"/>:)
                (:<completeCodeSystem codeSystem="2.16.840.1.113883.6.96" codeSystemName="SNOMED CT"><desc language="nl-NL">SNOMED CT - SNOMED CT: ^11721000146100 | RefSet Patiëntproblemen V&amp;VN</desc></completeCodeSystem>:)
                (: skip empty members and refsets :)
                for $node in $valueSet/completeCodeSystem[string-length(@codeSystem) gt 0][empty(desc[matches(upper-case(.), $refsetPattern)])]
                let $codeSystem     := replace($node/@codeSystem/normalize-space(),'^2\.16\.840\.1\.13883','2.16.840.1.113883')
                let $codeSystemName := $node/@codeSystemName/normalize-space()
                
                let $codeSystem     :=
                    switch ($codeSystem)
                    case $nederlandseLabCodes return (
                        (: Van de OnderzoekCodelijst en de TestCodeLOINCCodelijst weten we dat deze LOINC bedoelen. Van ander gebruik zou het ook SNOMED CT of UCUM kunnen zijn :)
                        if ($valueSet/@id = ('2.16.840.1.113883.2.4.3.11.60.40.2.13.1.3', '2.16.840.1.113883.2.4.3.11.60.40.2.13.1.5')) then
                            '2.16.840.1.113883.6.1' (:'Nederlandse Labcodeset':)
                        else (
                            error(xs:QName('error:UnsupportedCodesystem'),concat('ZIB value set resource "',util:document-name($node), '" contains valueSet "',$valueSet/@displayName,'" with reference to "',$node/@codeSystem,'" which is not a codeSystem.'))
                        )
                    )
                    default return $codeSystem
                let $codeSystemName :=
                    switch ($codeSystem)
                    case '2.16.840.1.113883.6.1' return 'LOINC'
                    default return $codeSystemName
                
                return
                    <completeCodeSystem codeSystem="{$codeSystem}" codeSystemName="{$codeSystemName}">
                    {
                        if ($node/@type[. = 'D']) then 
                            $node/@type 
                        else
                        if (contains(upper-case($codeSystemName), 'DEPRECATED')) then 
                            attribute type {'D'} 
                        else ()
                        ,
                        $node/designation
                        ,
                        switch ($node/@codeSystem)
                        case $nederlandseLabCodes return (
                            let $d      := $node/@codeSystemName/normalize-space()
                            let $d-nl   := if (matches(lower-case($d), 'labterminologie.nl')) then $d else replace($d, '\.$', '') || '. Zie https://labterminologie.nl voor de relevante subset van LOINC'
                            let $d-en   := if (matches(lower-case($d), 'labterminologie.nl')) then $d else replace($d, '\.$', '') || '. See https://labterminologie.nl for the relevant subset of LOINC'
                            return
                                <desc language="nl-NL">{data($d-nl)}</desc> | <desc language="en-US">{data($d-en)}</desc>
                        )
                        default return $node/desc
                        ,
                        $node/filter
                    }
                    </completeCodeSystem>
            let $refsets              := $valueSet/completeCodeSystem[string-length(@codeSystem) gt 0][desc[matches(upper-case(.), $refsetPattern)]]
            
            return
            if ($valueSet/conceptList | $completeCodeSystems | $refsets) then 
                <conceptList>
                {
                    for $completeCodeSystem in $completeCodeSystems
                    let $displayName  := ($completeCodeSystem/designation[@language = 'nl-NL']/@displayName[not(. = '')])[1]
                    return
                        <include>
                        {
                            $completeCodeSystem/(@* except @displayName),
                            if ($completeCodeSystem/@displayName) then
                                if (empty($displayName)) then $completeCodeSystem/@displayName else attribute displayName {$displayName}
                            else (),
                            $completeCodeSystem/designation,
                            $completeCodeSystem/desc,
                            $completeCodeSystem/filter
                        }
                        </include>
                }
                {
                    for $refset in $refsets
                    let $conceptId    := replace($refset/desc[matches(upper-case(.), $refsetPattern)][1], $refsetPattern, '$1')
                    let $displayName  := replace($refset/desc[matches(upper-case(.), $refsetPattern)][1], $refsetPattern, '$2')
                    return
                        <include op="in" code="{$conceptId}">
                        {
                            $refset/@codeSystem, 
                            if ($displayName = '') then () else attribute displayName {$displayName}, 
                            $refset/designation, 
                            $refset/desc
                        }
                        </include>
                }
                {
                    (: skip empty conceptList members :)
                    let $set := 
                        $valueSet/conceptList/(*[string-length(string-join(@code | @codeSystem | @ref, '')) gt 0] except exception) |
                        $valueSet/conceptList/exception[string-length(string-join(@code | @codeSystem | @ref, '')) gt 0]
                    
                    for $node in $set
                    let $displayName  := ($node/designation[@language = 'nl-NL']/@displayName[not(. = '')])[1]
                    return
                        element {$node/name()} {
                            $node/@code,
                            if ($node[matches(@codeSystem,'[^\.\d]')] and $node[@systemOID]) then
                                attribute codeSystem {replace($node/@systemOID/normalize-space(),'^2\.16\.840\.1\.13883','2.16.840.1.113883')}
                            else (
                                attribute codeSystem {replace($node/@codeSystem/normalize-space(),'^2\.16\.840\.1\.13883','2.16.840.1.113883')}
                            ),
                            if ($node/@displayName) then
                                if (empty($displayName)) then $node/@displayName else attribute displayName {$displayName}
                            else (),
                            $node/@ref[string-length() gt 0],
                            $node/@flexibility[not(. = 'dynamic')],
                            if ($node/name() = 'concept' or $node/name() = 'exception') then (
                                $node/@level,
                                if (matches($node/@displayName, '\s*\[?\s*DEPRECATED\s*\]?\s*', 'i')) then attribute type {'D'} else $node/@type
                            ) else (),
                            $node/@ordinal,
                            if ($node[name() = 'include'][@code]) then (
                                if ($node/@op = 'descendant-of') then
                                    attribute op {'descendent-of'}
                                else
                                if ($node/@op) then
                                    $node/@op
                                else
                                if ($node/desc[matches(., '\s<\s*\d')]) then 
                                    attribute op {'descendent-of'}
                                else (
                                    attribute op {'is-a'}
                                )
                            ) else (),
                            $node/designation,
                            for $desc in $node/desc
                            return
                                <desc>{$desc/@language, replace($desc, 'SNOMED\s*CT\s*-\s*SNOMED\s*CT', 'SNOMED CT')}</desc>
                            (:for $desc in $node/desc[@language][.//text()[string-length(normalize-space()) gt 0]]:)
                            (:for $desc in $node/desc[@language][text()[string-length(normalize-space()) gt 0]]
                            return 
                                (\:element {$desc/name()} {
                                    $desc/@*,
                                    normalize-space($desc)
                                }:\)
                                <designation language="{$desc/@language}" type="preferred" displayName="{data($desc)}"/>
                            ,
                            if ($node/desc[@language][text()[string-length(normalize-space()) gt 0]]) then () else (
                                <designation language="{$language}" type="preferred" displayName="{$node/@displayName}"/>
                            ):)
                        }
                }
                </conceptList>
            else ()
        }
        </valueSet>
    )

let $codeSystems        :=
    for $concepts in $valueSets/conceptList/*[starts-with(@codeSystem, '2.16.840.1.113883.2.4.3.11.60.40.4') or starts-with(@codeSystem, '2.16.840.1.113883.2.4.3.11.60.101.5')]
    let $csid     := $concepts/@codeSystem
    let $csdn     := $concepts[1]/ancestor::valueSet/sourceCodeSystem[@id = $concepts[1]/@codeSystem]/@identifierName
    let $csdn     := if ($csdn = 'NL-CM-CS') then 'Weescodes' else $csdn
    let $csnm     := replace($csdn,'\+','plus')
    let $csed     := $concepts[1]/ancestor::valueSet/@effectiveDate
    let $csed     := if ($csed castable as xs:dateTime) then $csed else $pubdate
    let $csst     := $concepts[1]/ancestor::valueSet/@statusCode
    group by $csid
    order by $csid
    return
        <codeSystem id="{$csid}" name="{($csnm)[1]}" displayName="{($csdn)[1]}" effectiveDate="{($csed)[1]}" statusCode="final">
            <conceptList>
            {
                for $concept in $concepts
                let $code           := $concept/@code
                let $displayName    := $concept/@displayName
                let $ordinal        := $concept/@ordinal
                group by $code, $displayName
                order by $code
                return
                    <codedConcept code="{$code}" level="0" type="L">
                    {
                        if (empty($ordinal)) then () else attribute ordinal {$ordinal[1]},
                         attribute statusCode {'active'},
                        if ($concept[1][@displayName = designation/@displayName]) then () else (
                            <designation language="{if ($concept[1]/designation) then 'en-US' else 'nl-NL'}" type="preferred">{$concept[1]/@displayName}</designation>
                        ),
                        $concept[1]/designation,
                        $concept[1]/desc[not(. = $concept[1]//@displayName)]
                    }
                    </codedConcept>
            }
            </conceptList>
        </codeSystem>

let $terminology        :=
    <terminology xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../core/DECOR.xsd">
    {
        '&#10;',
        comment {
            '&#10;',
            '    Gereserveerde valueSet attributen: id, name, displayName, effectiveDate, statusCode, expirationDate, officialReleaseDate, versionLabel&#10;',
            '    Extra attributen zijn geen bezwaar&#10;',
            '&#10;',
            '    valueSet heeft 0..* desc elementen. 1 desc element per taal, te zien aan het @language attribuut. De transformatie genereert nu het desc &#10;',
            '    element uit de omschrijving(en) in het name element bij completeCodeSystem koppelingen. Dit is vooral omdat er op dit bewuste element zelf &#10;',
            '    geen desc mogelijkheid zit. Als er slechts 1 omschrijving is, komt die er ''plat'' in, anders komt iedere omschrijving in een <div/> element.&#10;',
            '&#10;',
            '    valueSet sourceCodeSystem bevat de id en naam van ieder codeSystem dat ergens in de valueSet wordt gebruikt. Denormalisatie voor performance.&#10;',
            '&#10;',
            '    valueSet conceptList komt alleen voor als er gecodeerde concepten/excepties zijn. Ieder gecodeerd concept dat niet is gekoppeld aan NullFlavor&#10;',
            '    wordt een <concept/>. Anders wordt het een <exception/>. Eerst komen de <concept/> elementen en dan de <exception/> elementen. De opbouw is &#10;',
            '    hetzelfde. @level en @type hebben vaste waarden (resp. 0 en L) aangezien de ZIB waardelijsten hiervoor geen ondersteuning kennen.&#10;'
        }
        ,
        $codeSystems,
        $valueSets
    }
    </terminology>
let $remove             := if (doc-available(concat($resultcollection,'/',$resultfile))) then xmldb:remove($resultcollection,$resultfile) else ()
(: validate against DECOR schema :)
let $xsdreport          := validation:jaxv-report($terminology, $get:docDecorSchema)
let $update             := xmldb:store($resultcollection, $resultfile, document {
        <?xml-stylesheet type="text/xsl" href="../core/DECOR2schematron.xsl"?>,
        <?xml-model href="../core/DECOR.sch" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>,
        $terminology
    })

return 
    if ($xsdreport/status[. = 'invalid']) then ($xsdreport) else ($terminology)
