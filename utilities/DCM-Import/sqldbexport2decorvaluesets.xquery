xquery version "3.0";

import module namespace art     = "http://art-decor.org/ns/art" at "/db/apps/art/modules/art-decor.xqm";

(:<model xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" exportDate="9-4-2015 10:40:01" xmlns="http://www.umcg.nl/MAX">:)
declare namespace UML           = "omg.org/UML1.3";
declare namespace max           = "http://www.umcg.nl/MAX";
declare namespace error         = "http://art-decor.org/ns/decor/import/error";
declare namespace xs            = "http://www.w3.org/2001/XMLSchema";

(:~Project prefix:)
declare variable $projectPrefix     := 'zib2bbr-';

declare variable $language          := 'nl-NL';
declare variable $vscollection      := '/db/apps/decor/staging/eOverdracht-ValueSets-20150910';
declare variable $resultcollection  := '/db/apps/decor/staging';
declare variable $resultfile        := concat($projectPrefix,'decor-valuesets.xml');

(:<row Valueset_ID="10520" Valueset="ManchetTypeCodelijst" OID="2.16.840.1.113883.2.4.3.11.60.40.2.12.4.3"/>:)
(:<row Entry_ID="7001" Valueset_ID="12298" Object_ID="10787" ConceptName="Tegenover" ConceptCode="to" ConceptValue="0" 
    CodeSystemName="AanduidingBijHuisnummer" CodeSystemOID="2.16.840.1.113883.2.4.3.11.60.101.5.3" 
    Description="Tegenover" TPos="1" MaxColumn="5"/>:)
declare %private function local:buildValueSet($row as element(row), $contentrows as element(row)+) as element()+ {
    let $vsid           := $row/@OID
    let $vsnm           := $row/@Valueset
    
    return (
        for $contentrow in $contentrows[string-length(@CodeSystemName)>0]
        let $codeSystem    := normalize-space($contentrow/@CodeSystemOID)
        group by $codeSystem
        return
            <sourceCodeSystem id="{$codeSystem}" identifierName="{normalize-space($contentrow[1]/@CodeSystemName)}"/>
        ,
        for $contentrow in $contentrows
        let $maxcolumn      := normalize-space($contentrow/@MaxColumn)
        let $code           := normalize-space($contentrow/@ConceptCode)
        let $value          := normalize-space($contentrow/@ConceptValue)
        let $displayName    := normalize-space($contentrow/@ConceptName)
        let $description    := normalize-space($contentrow/@Description)
        let $codeSystem     := normalize-space($contentrow/@CodeSystemOID)
        let $codeSystemName := normalize-space($contentrow/@CodeSystemName)
        order by $contentrow/xs:integer(@TPos)
        return
            if ($maxcolumn='3') then (
                if (string-length($displayName)=0) then () else (
                    <desc language="{$language}">{concat($codeSystemName,' (',$codeSystem,'): ',$displayName)}</desc>
                ),
                <completeCodeSystem codeSystem="{$codeSystem}" codeSystemName="{$codeSystemName}"/>
            )
            else if ($maxcolumn=('5','6')) then (
                element {if ($codeSystem='2.16.840.1.113883.5.1008') then 'exception' else 'concept'} {
                    attribute code {$code},
                    attribute codeSystem {$codeSystem},
                    if (string-length($displayName)=0) then () else (
                        attribute displayName {$displayName}
                    ),
                    (:if (string-length($codeSystemName)=0) then () else (
                        attribute codeSystemName {$codeSystemName}
                    ),:)
                    attribute level {"0"},
                    attribute type {"L"},
                    if (string-length($description)=0) then () else (
                        <desc language="{$language}">
                        {
                            if (string-length($value)=0 or $contentrow[not(@MaxColumn='6')]) then () else (
                                concat('Waarde: ', $value,'. ')
                            ),
                            data($description)
                        }
                        </desc>
                    )
                }
            )
            else ( 
                error(xs:QName('error:ValueSetContentError'),concat('VS "',$vsnm,'" id "',$vsid,'" has a content row with @MaxColumn value ',$maxcolumn,'. We support value 3, 5, and 6.', string-join(for $a in $contentrow/@* return concat($a/name(),'=',$a),'')))
            )
    )
};

let $terminology        :=
    <terminology>
    {
        for $row in doc(concat($vscollection,'/Valuesets.xml'))//row
        let $id             := $row/@Valueset_ID
        let $vsid           := $row/@OID
        let $vsnm           := $row/@Valueset
        let $vsdn           := $row/@Valueset
        let $vsef           := substring(string(current-dateTime()),1,19)
        let $contentrows    := doc(concat($vscollection,'/ValueSetEntries.xml'))//row[@Valueset_ID=$id]
        let $vscontents     := local:buildValueSet($row, $contentrows)
        return
            <valueSet id="{$vsid}" name="{$vsnm}" displayName="{$vsdn}" effectiveDate="{$vsef}" statusCode="final">
            {
                if (count($vscontents[self::desc])=0) then () 
                else if (count($vscontents[self::desc])=1) then (
                    $vscontents[self::desc]
                )
                else (
                    <desc language="{$vscontents[self::desc][1]/@language}">
                        <ul>
                        {
                            for $desc in $vscontents[self::desc]
                            return <li>{$desc/node()}</li>
                        }
                        </ul>
                    </desc>
                ),
                $vscontents[self::sourceCodeSystem],
                $vscontents[self::completeCodeSystem],
                if ($vscontents[self::concept|self::exception]) then (
                    <conceptList>{$vscontents[self::concept],$vscontents[self::exception]}</conceptList>
                ) else ()
            }
            </valueSet>
    }
    </terminology>
let $remove         := if (doc-available(concat($resultcollection,'/',$resultfile))) then xmldb:remove($resultcollection,$resultfile) else ()
let $update         := xmldb:store($resultcollection,$resultfile,$terminology)

return $terminology