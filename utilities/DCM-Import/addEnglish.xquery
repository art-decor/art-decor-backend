xquery version "3.0";

import module namespace art     = "http://art-decor.org/ns/art" at "/db/apps/art/modules/art-decor.xqm";
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "/db/apps/art/modules/art-decor-settings.xqm";
import module namespace x2d     = "http://art-decor.org/ns/xmi2decor" at "file:/Users/ahenket/Development/ART-DECOR/trunk/utilities/DCM-Import/xmi-api.xqm";

declare namespace UML           = "omg.org/UML1.3";
declare namespace max           = "http://www.umcg.nl/MAX";
declare namespace error         = "http://art-decor.org/ns/decor/import/error";
declare namespace xs            = "http://www.w3.org/2001/XMLSchema";

(:~Project prefix:)
declare variable $projectPrefix             := 'zib1bbr-';
declare variable $xmicollection             := '/db/apps/decor/staging/20151127-Zorginformatiebouwstenen-XMI-EngelsNederlands-UMC_XMI';
declare variable $xmimodels                 := collection($xmicollection)//UML:Model;
declare variable $language                  := 'en-US';
declare variable $resultcollection          := '/db/apps/decor/staging';

declare %private function local:addDatasetLanguage($concept as element(), $xmiobject as element(UML:Class)) as element(concept) {
    if ($concept[inherit])
    then ($concept)
    else
    if ($concept[name/@language='en-US']) 
    then ($concept)
    else (
        let $xmiobject      := 
            if ($concept[relationship][not($concept/ancestor::concept/relationship)]) then (
                let $originalConcept    := art:getOriginalForConcept(<concept><inherit ref="{$concept/relationship/@ref}" effectiveDate="{$concept/relationship/@flexibility}"/></concept>)
                return
                $xmimodels[@xmi.id=../UML:TaggedValue[@tag='DCM::Id'][@value=$originalConcept/ancestor::dataset/@id]/@modelElement]/..//UML:Class[x2d:getTaggedValue(.,'stereotype')='rootconcept']
            ) else ($xmiobject)
        
        let $idmnem         := 
            if (starts-with($concept/relationship[1]/@ref,$x2d:oidBaseEL)) then
                concat('^NL-CM:\s*',substring-after($concept/relationship[1]/@ref,concat($x2d:oidBaseEL,'.')),'\s*(#|$)')
            else 
            if (starts-with($concept/relationship[1]/@ref,$x2d:oidBaseBS)) then
                concat('^NL-BS:\s*',substring-after($concept/relationship[1]/@ref,concat($x2d:oidBaseBS,'.')),'\s*(#|$)')
            else
            if (starts-with($concept/@id,$x2d:oidBaseEL)) then 
                concat('^NL-CM:\s*',substring-after($concept/@id,concat($x2d:oidBaseEL,'.')),'\s*(#|$)')
            else (
                concat('^NL-BS:\s*',substring-after($concept/@id,concat($x2d:oidBaseBS,'.')),'\s*(#|$)')
            )
        let $taggedValue    := $xmiobject/ancestor::XMI//UML:TaggedValue[@tag='DCM::DefinitionCode'][matches(@value,$idmnem)]
        let $xmiConcept     := $xmiobject/ancestor::XMI//*[@xmi.id=$taggedValue/@modelElement]
        let $c              :=
            if (count($xmiConcept)=1) then () else if (count($xmiConcept)=0) then (
                error(xs:QName('error:ClassNotFound'),concat('Dataset "',$concept/ancestor::dataset/@id,'" (',$concept/ancestor::dataset/name[1],') Concept "',$concept/@id,'" (',$concept/name[1],') using "',$idmnem,'" could not be found. Based on "',string-join($taggedValue/@value,' / '),'"'))
            ) else (
                error(xs:QName('error:ClassOverload'),concat('Dataset "',$concept/ancestor::dataset/@id,'" (',$concept/ancestor::dataset/name[1],') Concept "',$concept/@id,'" (',$concept/name[1],') using "',$idmnem,'" yielded multiple hits instead of exactly 1. Based on "',string-join($taggedValue/@value,' / '),'" gave "',string-join($xmiConcept/concat(name(),' (',@name,')'),', '),'"'))
            )
        return
        <concept>
        {
            $concept/@*,
            $concept/name,
            if ($concept/name[@language=$language]) then () else (
                local:getAliasFromXmiAsSynonym($xmiConcept, $language)
            ),
            $concept/synonym,
            $concept/desc,
            x2d:getdesc($xmiConcept, $language),
            $concept/(* except (name|synonym|desc|concept)),
            for $childconcept in $concept/concept
            return
                local:addDatasetLanguage($childconcept, $xmiobject)
        }
        </concept>
    )
};

declare %private function local:getAliasFromXmiAsSynonym($xmiconcept as element()?, $language as xs:string) as element()* {
    let $synonym        := x2d:getLanguageValue(x2d:getTaggedValue($xmiconcept, 'alias'),$language)
    
    return
        if ($synonym) then 
            <name language="{$language}">{tokenize($synonym,'::')[1]}</name>
        else ()
};

declare %private function local:getDescFromXmi($xmiconcept as element()?, $language as xs:string) as element() {
    let $desc           := x2d:getLanguageValue(x2d:getTaggedValue($xmiconcept, 'documentation'),$language)
    
    return
        if ($desc) then 
            art:parseNode(<desc language="{$language}">{$desc}</desc>)
        else ()
};

let $prefix         := 'zib1bbr-'
let $decorProject   := art:getDecorByPrefix($prefix)

return
    <decor>
    {
        $decorProject/@*
    }
    <project>
    {
        $decorProject/project/@*,
        $decorProject/project/name,
        $decorProject/project/desc,
        x2d:getprojectdesc(x2d:getdcmroot($xmimodels[not(util:document-name(.)='Common.xmi')][1]), $language),
        $decorProject/project/(* except (name|desc))
    }
    </project>
    <datasets>
    {
        for $dataset in $decorProject//datasets/dataset
        let $taggedValue    := $xmimodels/ancestor::XMI//UML:TaggedValue[@tag='DCM::Id'][@value=$dataset/@id]
        let $xmiobject      := $xmimodels[@xmi.id=$taggedValue/@modelElement]/..//UML:Class[x2d:getTaggedValue(.,'stereotype')='rootconcept']
        let $c              :=
            if (count($xmiobject)=1) then () else if (count($xmiobject)=0) then (
                error(xs:QName('error:ClassNotFound'),concat('Dataset "',$dataset/@id,'" (',$dataset/name[1],') could not be found. Based on "',string-join($taggedValue/@value,' / '),'"'))
            ) else (
                error(xs:QName('error:ClassOverload'),concat('Dataset "',$dataset/@id,'" (',$dataset/name[1],') could not be found. Based on "',string-join($taggedValue/@value,' / '),'" yielded multiple hits instead of exactly 1. Based on "',string-join($taggedValue/@value,' / '),'" gave "',string-join($xmiobject/concat(name(),' (',@name,')'),', '),'"'))
            )
        return
            <dataset>
            {
                $dataset/@*,
                $dataset/name,
                if ($dataset/name[@language=$language]) then () else (
                    <name language="{$language}">{$dataset/name[1]/node()}</name>
                ),
                $dataset/synonym,
                $dataset/desc,
                x2d:getdcmdesc($xmiobject, $language, $projectPrefix, $resultcollection),
                $dataset/(* except (name|synonym|desc|concept)),
                for $concept in $dataset/concept
                return
                    local:addDatasetLanguage($concept, $xmiobject)
            }
            </dataset>
    }
    </datasets>
    {
        $decorProject/(* except (project|datasets))
    }
    </decor>