Marc de Graauw, 15-7-2016

There is an old orbeon XForms schema (sometimes used in our XForms): 
xsi:schemaLocation="http://www.w3.org/1999/xhtml https://raw.github.com/orbeon/orbeon-forms/master/src/main/resources/org/orbeon/oxf/xml/schemas/xhtml1-transitional-orbeon.xsd"

The schema referred to isn't valid itself (maybe due to improved Saxon checks, because it used to be.) I informed Orbeon, and since they don't use it themselves, and it is not maintained any more (it was only provided for validation and content completion in editors for the benefit of others), they decided to remove this schema. It was useless to us already, since validating an XForm with it would lead to the message that the schema itself was not valid.

Oxygen does provide a schema for XHTML+XForms:
<?xml-model href="http://www.oxygenxml.com/1999/xhtml/xhtml-xforms.nvdl" schematypens="http://purl.oclc.org/dsdl/nvdl/ns/structure/1.0"?>
(ADA generated forms already used it.)

This schema does not recognize Orbeon-specific extensions (xxforms) and is a bit too strict in places (i.e. requiring xf:label where we don't use it and Orbeon doesn't complain), so I've edited the underlying schemas to be a bit more relaxed.

Placing attached schemas in:
{where-xoygen-lives}\Oxygen XML Editor 18\frameworks\xforms
and inserting the xml-model statement before doc root in a XForm does provide validation and (quite good) context-aware content completion in Oxygen.

Not that validating an XForm with the above may still lead to errors. Partly those are xhtml errors (to fix that, we'd need to patch the Oxygen XHTML schema), and partly genuine XForms errors (@appearance on hint is one) it seems. 