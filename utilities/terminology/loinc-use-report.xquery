xquery version "3.0";
(:
Query to list all LOINC codes used in terminology associations and value sets in decor projects in the project directory

Can be placed in any collection to run

:)

import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "/db/apps/art/modules/art-decor-settings.xqm";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "text";


let $loincOid := '2.16.840.1.113883.6.1'
let $loinc := collection(concat($get:strCodesystemStableData,'/external/regenstrief'))/browsableCodeSystem
let $projects := collection(concat($get:strDecorData,'/projects'))/decor


let $loincCodes := distinct-values($projects//*[@codeSystem=$loincOid]/@code)

let $nonNLcodes :=    for $code in $loincCodes
                        let $loincConceptCode := $loinc/concept[@code=$code][not(designation[@use='pref'][@lang='nl-NL'])]/@code
                        return
                           $loincConceptCode/string()

return
$nonNLcodes
