xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
module namespace adloinc            = "http://art-decor.org/ns/terminology/loinc";
import module namespace adsearch    = "http://art-decor.org/ns/terminology/search" at "../../api/api-terminology-search.xqm";
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";

declare variable $adloinc:defaultLanguage   := 'en-US';
declare variable $adloinc:anyLanguage       := 'universal';

declare variable $adloinc:strLoincData      := concat($get:strTerminologyData,'/loinc-data/data/');
declare variable $adloinc:strLoincPanels    := concat($get:strTerminologyData,'/loinc-data/panels/');
declare variable $adloinc:colLoincPanels    := collection($adloinc:strLoincPanels)//Panels;
declare variable $adloinc:strLoincVersion   := concat($get:strTerminologyData,'/loinc-data/version-info.xml');
declare variable $adloinc:colLoincDb        := collection(adloinc:getLoincDbCollection(()))/loinc_db;


(:~
    There have been several layouts for the terminology-data/loinc-data collection
    Current 2.56:
        loinc-data/data/universal/LOINC_DB_with_LinguisticVariants.xml containing en-US + any linguistic variant/translation
    Previous 2.54.1:
        loinc-data/data/de-AT/LOINC_DB_de_AT_24_LinguisticVariant.xml containing en-US + de_AT linguistic variant/translation
        loinc-data/data/de-CH/LOINC_DB_de_CH_1_LinguisticVariant.xml containing en-US + de_CH linguistic variant/translation
        loinc-data/data/en-US/LOINC_DB.xml containing en-US only
        etc...
    Previous 2.54.0:
        loinc-data/localizations/ containing localizations only in resources named as above, e.g. LOINC_DB_de_AT_24_LinguisticVariant.xml
        loinc-data/concepts/LOINC_DB.xml
    Previous < 2.54.0:
        loinc-data/concepts/LOINC_DB.xml
        
    Returns either
        loinc-data/data/universal if it exists
    or
        loinc-data/data/en-US if universal does not exist and there is no value for language
    or 
        loinc-data/data/{language} if universal does not exist and there is a value for language
:)
declare function adloinc:getLoincDbCollection($language as xs:string?) as xs:string {
    if (xmldb:collection-available(concat($adloinc:strLoincData,$adloinc:anyLanguage))) then
        concat($adloinc:strLoincData,$adloinc:anyLanguage)
    else if (empty($language)) then
        concat($adloinc:strLoincData,$adloinc:defaultLanguage)
    else (
        concat($adloinc:strLoincData,$language)
    )
};

(:~
:   Returns
:   <loinc_db>
:       <localization id="0" language="en-US" displayName="English (US)" producer="Regenstrief, US"/>
:       <localization id="22" language="nl-NL" displayName="Dutch (NETHERLANDS)" producer="NVKC, Dutch Society for Clinical Chemistry and Laboratory Medicine, The Netherlands"/>
:       <localization id="10" language="et-EE" displayName="Estonian (ESTONIA)" producer="Estonian E-Health Foundation"/>
:       <localization id="23" language="fr-BE" displayName="French (BELGIUM)" producer="Jean M. Prevost, MD, Biopathology"/>
:       <localization id="8" language="fr-CA" displayName="French (CANADA)" producer="Canada Health Infoway Inc."/>
:       ...
:   </loinc_db>
:
:   As of LOINC 2.54.1 this info is in loinc-data/version-info.xml. In version <= 2.54.0 this info needed to be built from the various db resources.
:)
declare function adloinc:getVersionInfo() as element() {
    if (doc-available($adloinc:strLoincVersion)) then
        doc($adloinc:strLoincVersion)/loinc_db
    else (
        <loinc_db>
            <localization id="0" language="en-US" displayName="English (US)" producer="Regenstrief, US"/>
        {
            for $localization in collection($adloinc:strLoincData)/loinc_db[@language]
            return
                <localization>{$localization/@*}</localization>
        }
        </loinc_db>
    )
};

(:~
:   Returns boolean true() of search string is valid or false() otherwise
:   
:   A valid search string is any string of 2 characters or more
:)
declare function adloinc:isValidSearch($searchString as xs:string?) as xs:boolean {
    if (string-length($searchString) >= 2) then
        true()
    else (
        false()
    )
};

(:~
:   Returns LOINC concepts wrapped in a <result/> element. The results are paginated by default based the max number of results to return
:   This element returns several attributes:
:   <result 
:       page-current="{floor($offsetResults div $maxResults) + 1}"  -- The page you are currently on based on total count / count per page
:       page-total="{floor($totalcount div $maxResults) + 1}"       -- The total number of pages available in this set
:       current="{$currentcount}"                                   -- The current number of LOINC concepts in the result element
:       total="{$totalcount}"                                       -- The total number of LOINC concepts for this search
:       offset="{$offsetResults}"                                   -- The requested index number for the first LOINC concept in the result element
:       max="{$maxResults}"                                         -- The requested max number of results in the set, or () for all results
:       sort="{$sortBy}"                                            -- The requested sort column
:       language="{$language}"/>                                    -- The requested language to search in
:
:   Searches always go through en-US and optionally through in another language.
:   Sorting is done based on length of en-US longName by default or optionally on column name. Supported column names are the same 
:   as the LOINC_DB CSV columnnames. Examples: LOINC_NUM, LONG_COMMON_NAME, COMPONENT, PROPERTY, TIME_ASPCT, SYSTEM, SCALE_TYPE, METHOD_TYP
:
:   Note that sorting on columns LONG_COMMON_NAME, COMPONENT will not be done based on contents but on content length as per the search 
:   recommendations from SNOMED-CT
:
:   Filtering by status is possible. Use any LOINC supported status codes, like 'ACTIVE' or 'DEPRECATED'. See LOINC documentation for the specific 
:   version that is loaded which ones are present. The packages contain exactly those.
:)
declare function adloinc:searchConcept($searchString as xs:string, $offsetResults as xs:integer, $maxResults as xs:integer?, $sortBy as xs:string?) as element(result) {
    adloinc:searchConcept($searchString, $offsetResults, $maxResults, $sortBy, ())
};

declare function adloinc:searchConcept($searchString as xs:string, $offsetResults as xs:integer, $maxResults as xs:integer?, $sortBy as xs:string?, $statusCodes as xs:string*) as element(result) {
    adloinc:searchConcept($searchString, $offsetResults, $maxResults, $sortBy, $statusCodes, ())
};
declare function adloinc:searchConcept($searchString as xs:string, $offsetResults as xs:integer, $maxResults as xs:integer?, $sortBy as xs:string?, $statusCodes as xs:string*, $language as xs:string?) as element(result) {
let $result             := adloinc:searchConcept($searchString, $statusCodes, $language)

let $result             := 
    switch ($sortBy)
    case 'LOINC_NUM' return (
        for $concept in $result
        order by $concept/@loinc_num
        return $concept
    )
    case 'LONG_COMMON_NAME' return (
        for $concept in $result
        order by $concept/longName[1]/xs:integer(@length)
        return $concept
    )
    case 'COMPONENT' return (
        for $concept in $result
        order by $concept/component[1]/xs:integer(@length)
        return $concept
    )
    case 'PROPERTY' return (
        for $concept in $result
        order by $concept/property[1]
        return $concept
    )
    case 'TIME_ASPCT' return (
        for $concept in $result
        order by $concept/timing[1]
        return $concept
    )
    case 'SYSTEM' return (
        for $concept in $result
        order by $concept/system[1]
        return $concept
    )
    case 'SCALE_TYPE' return (
        for $concept in $result
        order by $concept/scale[1]
        return $concept
    )
    case 'METHOD_TYP' return (
        for $concept in $result
        order by $concept/method[1]
        return $concept
    )
    default return (
        if (empty($sortBy)) then (
            for $concept in $result
            order by $concept/longName[1]/xs:integer(@length)
            return $concept
        )
        else (
            for $concept in $result
            order by $concept/*[@name=$sortBy][1]
            return $concept
        )
    )

let $totalcount         := count($result)
let $returnresults      := if ($maxResults) then subsequence($result,$offsetResults,$maxResults) else subsequence($result,$offsetResults)
let $currentcount       := count($returnresults)

return
<result page-current="{floor($offsetResults div $maxResults) + 1}" 
        page-total="{floor($totalcount div $maxResults) + 1}" 
        current="{$currentcount}" 
        total="{$totalcount}" 
        offset="{$offsetResults}" 
        max="{$maxResults}" 
        sort="{$sortBy}"
        language="{$language}">
{
    $returnresults
}
</result>
};

(:~
:   (DEPRECATED) Performs search. The search string is tokenized based on whitespace into terms. If there is 1 term and this matches the pattern of a 
:   LOINC code then only this attribute is considered in search. Otherwise other properties are searched.
:
:   @param $columnKeys limits scope to requested columns (see LOINC documentation for valid column names like LONG_COMMON_NAME and SYSTEM)
:   @param $statusCodes limits scope to concepts with requested status (see LOINC documentation for valid values, but 'ACTIVE' and "DEPRECATED' are normally supported)
:   @param $language limits scope to requested language with pattern ll-CC, e.g. de-DE (see LOINC documentation for which linguistic variants the current version supports).

declare function adloinc:searchConceptByColumn($searchString as xs:string, $columnKeys as xs:string*) as element(concept)* {
    adloinc:searchConceptByColumn($searchString, $columnKeys, (), ())
};
declare function adloinc:searchConceptByColumn($searchString as xs:string, $columnKeys as xs:string*, $statusCodes as xs:string*, $language as xs:string?) as element(concept)* {
let $colLoincDb         := collection(adloinc:getLoincDbCollection($language))

let $result             := 
    if (adloinc:isValidSearch($searchString)) then (
        if ($searchString[matches(.,'^[0-9]+-[0-9]$')]) then 
            $colLoincDb//concept[@loinc_num = $searchString][1]
        else (
            let $searchTerms    := tokenize(lower-case($searchString),'\s|\[|\]|\(|\)|&amp;|\\|\+|-|\$|\*')
            let $options        := adsearch:getSimpleLuceneOptions()
            let $query          := adsearch:getSimpleLuceneQuery($searchTerms)
            let $result         :=
                if ($columnKeys) then (
                    $colLoincDb//concept[ft:query(*[@name=$columnKeys], $query, $options)][@loinc_num]
                ) else (
                    $colLoincDb//concept[ft:query(*[@name=$columnKeys], $query, $options)][@loinc_num or @language = $language]/ancestor-or-self::concept[@loinc_num]
                )
            return
                if ($statusCodes) then (
                    $result[@status=$statusCodes]
                ) else (
                    $result
                )
        )
    ) else ()

return $result
};
:)

(:~
:   Performs search. The search string is tokenized based on whitespace into terms. If there is 1 term and this matches the pattern of a 
:   LOINC code then only this attribute is considered in search. Otherwise other properties are searched.
:
:   @param $statusCodes limits scope to concepts with requested status (see LOINC documentation for valid values, but 'ACTIVE' and "DEPRECATED' are normally supported)
:   @param $language limits scope to requested language with pattern ll-CC, e.g. de-DE (see LOINC documentation for which linguistic variants the current version supports).
:)
declare function adloinc:searchConcept($searchString as xs:string, $statusCodes as xs:string*, $language as xs:string?) as element(concept)* {
let $colLoincDb         := collection(adloinc:getLoincDbCollection($language))

let $result             := 
    if (adloinc:isValidSearch($searchString)) then (
        if ($searchString[matches(.,'^[0-9]+-[0-9]$')]) then 
            $colLoincDb//concept[@loinc_num = $searchString][1]
        else (
             (: Replace Lucene special characters - I coouldn't make escaping work - the former tokenize didn't include all Lucene special characters :)
            let $searchString   := translate($searchString, '+-&amp;|!(){}[]^"~*?:\/', '                    ')
            let $searchTerms    := tokenize(lower-case($searchString),'\s|\[|\]|\(|\)|&amp;|\\|\+|-|\$|\*')
            let $options        := adsearch:getSimpleLuceneOptions()
            let $query          := adsearch:getSimpleLuceneQuery($searchTerms)
            let $result         :=
                if (empty($language)) then
                    $colLoincDb//concept[ft:query(., $query, $options)][@loinc_num]
                else (
                    $colLoincDb//concept[ft:query(., $query, $options)][@loinc_num | @language[. = $language]]/ancestor-or-self::concept[@loinc_num]
                )
            return
                if ($statusCodes) then (
                    $result[@status=$statusCodes]
                ) else (
                    $result
                )
        )
    ) else ()

return $result
};

(:~ Get the tree of a panel :)
declare function adloinc:getPanel($loincId as xs:string) as node()? {
    $adloinc:colLoincPanels//concept[Loinc=$loincId][1]
};

(:~ Get panel node with LOINC concepts:)
declare function adloinc:getPanelWithConcepts($panelChild as node()) as node()* {
    let $concept := $adloinc:colLoincDb/concept[@loinc_num=$panelChild/Loinc/string()]
    return 
    (
    $panelChild/(* except concept),
    $concept, 
    for $child in $panelChild/concept
    order by xs:integer($child/Sequence) ascending
    return <panelMember>{adloinc:getPanelWithConcepts($child)}</panelMember>
    )
};

(:~ Get the tree of a panel with LOINC concepts:)
declare function adloinc:getPanelTreeWithConcepts($loincId as xs:string) as node()? {
    (: There may be multiple panels with the same Loinc, i.e. organizers which appear in multiple panels. They should all be the same, so just pick the first :)
    let $panelTree  := $adloinc:colLoincPanels//concept[Loinc=$loincId][1]
    let $count := count($panelTree//concept)
    return if ($panelTree) then
        <result total="{$count}" current="{$count}">
            <panel>{adloinc:getPanelWithConcepts($panelTree)}</panel>
        </result>
            else <result current="0" total="0"/>
};