xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adloinc            = "http://art-decor.org/ns/terminology/loinc" at "../api/api-loinc.xqm";

let $searchString   := if (request:exists()) then util:unescape-uri(request:get-parameter('string',('sys bloo')),'UTF-8') else '74835-0'
let $searchData     := if (request:exists()) then request:get-data()/root else ()

(:let $searchString   := '10597-3'
let $searchData     := 
    <root 
        xmlns:xforms="http://www.w3.org/2002/xforms" 
        xmlns:ev="http://www.w3.org/2001/xml-events" 
        xmlns:xi="http://www.w3.org/2001/XInclude" 
        xmlns:widget="http://orbeon.org/oxf/xml/widget" 
        xmlns:f="http://orbeon.org/oxf/xml/formatting" 
        xmlns:xs="http://www.w3.org/2001/XMLSchema" 
        xmlns:atp="urn:nictiz.atp" 
        xmlns:fr="http://orbeon.org/oxf/xml/form-runner" 
        xmlns:xxforms="http://orbeon.org/oxf/xml/xforms" 
        xmlns:xhtml="http://www.w3.org/1999/xhtml" 
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
        sort="LONG_COMMON_NAME" 
        offset="1" 
        max="50">
        <search>10597-3</search>
    </root>:)

let $panelMode      := if ($searchData/panel='true') then true() else false()
let $offsetResults  := if ($searchData/@offset castable as xs:integer) then $searchData/xs:integer(@offset) else 1
let $maxResults     := if ($searchData/@count castable as xs:integer) then $searchData/xs:integer(@count) else 50
let $sortBy         := $searchData/@sort
let $statusCodes    := tokenize($searchData/@statusCodes,'\s')
let $language       := 
    if ($searchData) then 
        $searchData/@language 
    else if (request:exists()) then 
        request:get-parameter('language',())[string-length()>0]
    else ()

return
    if ($panelMode) then
        adloinc:getPanelTreeWithConcepts($searchString)
    else
        adloinc:searchConcept($searchString, $offsetResults, $maxResults, $sortBy, $statusCodes, $language)
