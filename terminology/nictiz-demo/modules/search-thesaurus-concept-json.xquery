xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace snomed              = "http://art-decor.org/ns/terminology/snomed" at "../../snomed/api/api-snomed.xqm";
declare namespace       json     =  "http://www.json.org";
declare namespace       output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option          output:method "json";
declare option          output:media-type "application/json";

declare variable $maxResults     := xs:integer('50');

declare %private function local:isValidSearch($searchString as xs:string?) as xs:boolean {
    if (matches($searchString,'^[a-z|0-9]') and string-length($searchString)>2 and string-length($searchString)<40) 
    then true()
    else if (matches($searchString,'^[A-Z]') and string-length($searchString)>1 and string-length($searchString)<40) 
    then true()
    else false()
};

let $searchString   := replace(util:unescape-uri(request:get-parameter('search',('')),'UTF-8'),"[\)\(\-]"," ")
let $thesaurus      := request:get-parameter('thesaurus','dt')
(:let $searchString   := 'gona':)

let $validSearch    := local:isValidSearch($searchString)
let $searchTerms    := tokenize($searchString,'\s')
let $options        := snomed:getSimpleLuceneOptions()
let $query          :=     
                        <query>
                             <bool>
                             {
                                 for $term in $searchTerms
                                 return
                                 <wildcard occur="must">{concat(lower-case($term),'*')}</wildcard>
                             }
                             </bool>
                         </query>
                        
(:raw query result:)
let $result         :=
    if ($validSearch) then
      collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib/',$thesaurus))//desc[ft:query(.,$query,$options)]
    else ()

(:order result by count and length:)
let $result         := 
    for $desc in $result
    order by xs:integer($desc/@count),xs:integer($desc/@length)
    return $desc
  
let $count          := count($result)
let $current        := if ($count>$maxResults) then $maxResults else ($count)
return
    <result current="{$current}" count="{$count}">
    {
        for $res in subsequence($result,1,$maxResults)
        let $conceptId  := $res/parent::concept/@conceptId
        return
        <designation conceptId="{$conceptId}" displayName="{$res/text()}" json:array="true"/>
    }
    </result>
    