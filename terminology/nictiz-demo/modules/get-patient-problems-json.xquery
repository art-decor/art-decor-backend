xquery version "3.1";
(:
    Copyright (C) 2011-2015 ART-DECOR Expert Group (art-decor.org)
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
(:
   
         for $item in $nodes/fn:map
      return
      element {$item/@key}{
         for $string in $item/fn:string
         return
         attribute {$string/@key} {
            $string/text()
         }
      }
   
:)
import module namespace get      = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";

declare namespace output         = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace fn             ="http://www.w3.org/2005/xpath-functions";
declare namespace       json     =  "http://www.json.org";

declare option output:method "json";
declare option output:media-type "application/json";
declare option exist:serialize "json-ignore-whitespace-text-nodes=yes";

let $patientId := request:get-parameter('patientId','')

(:let $patientId := '471601742':)
let $patientRecord := collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib/nursing-problems'))/record[@patientId=$patientId]

let $patientProblems := $patientRecord/problem

return
<problems>
   {
   for $problem in $patientProblems
   order by $problem/@createDate descending
   return
   <problem json:array="true">
   {
      $problem/@*,
      $problem/*[not(name()='treatmentObjective')],
      for $treatmentObjective in $problem/treatmentObjective
      return
      <treatmentObjective json:array="true">
         {
            $treatmentObjective/@*,
            $treatmentObjective/*[not(name()=('intervention','outcome'))]
            ,
            for $intervention in $treatmentObjective/intervention
            return
            <intervention  json:array="true">
            {
               $intervention/@*,
               $intervention/*[not(name()='outcome')],
               for $outcome in $intervention/outcome
               return
               <outcome json:array="true">
               {
               $outcome/@*,
               $outcome/*
               }
               </outcome>
            }
            </intervention>,
            for $painScore in $treatmentObjective/painScore
            return
            <painScore json:array="true">
               {
               $painScore/@*,
               $painScore/*
               }
            </painScore>
         }
      </treatmentObjective>
   }
   </problem>
   }
</problems>