xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";

declare namespace       json     =  "http://www.json.org";
declare namespace       output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option          output:method "json";
declare option          output:media-type "application/json";

let $episodeId := request:get-parameter('id','')
(:let $episodeId := '90559397-f3be-48a0-9593-97f3be58a0d2':)



let $episode := collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib'))//episode[@id=$episodeId]


let $problems     :=
         for $item in $episode/problem 
         let $omahaMapping := collection(concat($get:strTerminologyData,'/nictiz-demo-data/omaha/'))/omaha-problem-map/row[SNOMED_CT_code=$item/name/@code]
         let $interventions := $episode/intervention[@problemId=$item/@id]
         return
            <problem createDate="{$item/@createDate}" id="{$item/@id}" json:array="true">
            {
               $item/name,
               $item/type,
               <omahaProblem name="{$omahaMapping/OMAHA_term/text()}" areaName="{$omahaMapping/OMAHA_areaTerm/text()}"/>
               ,
               for $intervention in $interventions
                  let $omahaInterventionMapping := collection(concat($get:strTerminologyData,'/nictiz-demo-data/omaha/'))/omaha-intervention-map/row[Kernset_SNOMED_code=$intervention/name/@code]
                  return
                  <intervention createDate="{$intervention/@createDate}" id="{$intervention/@id}" json:array="true">
                  {
                     $intervention/name,
                     for $interventionMapping in $omahaInterventionMapping
                     return
                     <omahaAction name="{$interventionMapping/Term_Omaha_actievlak}" actionType="{$interventionMapping/term_Omaha_soort_actie}"/>
                     ,
                     for $outcome in $intervention/outcome
                     let $omahaOutcomeMapping := collection(concat($get:strTerminologyData,'/nictiz-demo-data/omaha/'))/omaha-conclusion-map/row[Kernset_SNOMED_code=$outcome/name/@code]
                     return
                     <outcome createDate="{$outcome/@createDate}" id="{$outcome/@id}" json:array="true">
                     {
                     $outcome/name,
                     for $outcomeMapping in $omahaOutcomeMapping
                     return
                     <omahaConclusion name="{$outcomeMapping/term_Omaha_}" id="{util:uuid()}"/>
                     }
                     </outcome>
                  }
                  </intervention>
            }
            </problem>

return
<mappings>
{
 $problems
}
</mappings>