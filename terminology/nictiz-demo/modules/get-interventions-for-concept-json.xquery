xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get      = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace snomed   = "http://art-decor.org/ns/terminology/snomed" at "../../snomed/api/api-snomed.xqm";
declare namespace       json     = "http://www.json.org";
declare namespace       output   = "http://www.w3.org/2010/xslt-xquery-serialization";

declare option          output:method "json";
declare option          output:media-type "application/json";


let $conceptId        := util:unescape-uri(request:get-parameter('code',''),'UTF-8')

(:let $conceptId        := '22253000':)

let $coreInterventionSet    :=
      let $interventionMap := collection(concat($get:strTerminologyData,'/nictiz-demo-data/core/'))/interventions/map[@conceptId=$conceptId]/@refsetId
      return
      $snomed:colDataBase//refset[@refsetId=$interventionMap][@active]/ancestor::concept[@active]

return
<interventions>
{   
   for $concept in $coreInterventionSet
   let $displayName := if ($concept/desc[@languageRefsetId='31000146106'][@type='pref']) then $concept/desc[@languageRefsetId='31000146106'][@type='pref'] else $concept/desc[@type='pref'][1]
   order by $concept/desc[@languageRefsetId='31000146106'][1]
   return
   <concept code="{$concept/@conceptId}" codeSystem="2.16.840.1.113883.6.96" displayName="{$displayName}"/>
}
</interventions>

