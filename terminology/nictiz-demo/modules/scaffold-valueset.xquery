xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get      = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace snomed   = "http://art-decor.org/ns/terminology/snomed" at "../../snomed/api/api-snomed.xqm";
declare option exist:serialize "charset=utf-8 indent=yes";



let $name := 'PijnPatroonCodelijst'
let $oid := '2.16.840.1.113883.2.4.3.11.60.40.2.12.21.3'
let $conceptList := ('255238004','26593000','255341006')








return
   <valueSet url="http://art-decor.org/fhir/ValueSet/{$oid}--2020-12-31T000000" oid="{$oid}" experimental="true" id="{$oid}" name="{$name}" displayName="{$name}" effectiveDate="2020-12-31T00:00:00" statusCode="final">
   <name languageCode="nl-NL" count="{count(tokenize($name,'\s'))}" length="{string-length($name)}">{$name}</name>
   <publisher language="nl-NL">Zorginformatiebouwstenen (ZIB) 2020</publisher>
   <publisher language="en-US">Health and Care Information Models (HCIM) 2020</publisher>
   <contact>
      <telecom system="" value=""/>
   </contact>
   <description></description>
   <compose>
      <include>
         <system oid="2.16.840.1.113883.6.96" url=""/>
         {
         for $conceptId in $conceptList
         let $concept := $snomed:colDataBase//concept[@conceptId=$conceptId]
         let $enPref := $concept/desc[@type='pref'][@languageRefsetId='900000000000509007']
         let $nlPref := $concept/desc[@type='pref'][@languageRefsetId='31000146106']
         return
         <concept code="{$conceptId}" codeSystem="2.16.840.1.113883.6.96" displayName="{$enPref}" level="0" type="L">
            <designation use="pref" languageCode="en-US" count="{$enPref/@count}" length="{$enPref/@length}">{$enPref/text()}</designation>
            <designation use="pref" languageCode="nl-NL" count="{$nlPref/@count}" length="{$nlPref/@length}">{$nlPref/text()}</designation>
         </concept>
         }
      </include>
   </compose>
</valueSet>