xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace snomed              = "http://art-decor.org/ns/terminology/snomed" at "../../snomed/api/api-snomed.xqm";

declare variable $maxResults     := xs:integer('50');

let $searchString   := replace(util:unescape-uri(request:get-parameter('string',('')),'UTF-8'),"[\)\(-/]"," ")

(:let $searchString   := replace('Probleem met agressie van ouders/familie',"[\)\(-/]"," "):)

let $validSearch    :=
    if (matches($searchString,'^[a-z|0-9]') and string-length($searchString)>2 and string-length($searchString)<60) 
    then true()
    else if (matches($searchString,'^[A-Z]') and string-length($searchString)>1 and string-length($searchString)<60) 
    then true()
    else false()
let $searchTerms    := tokenize($searchString,'\s')
let $options        := snomed:getSimpleLuceneOptions()
let $query          :=     
                        <query>
                             <bool>
                             {
                                 for $term in $searchTerms
                                 return
                                 <wildcard occur="must">{concat(lower-case($term),'*')}</wildcard>
                             }
                             </bool>
                         </query>
                         
let $codeSearch :=matches($searchString,'^[A-Z]?[0-9|\.|/]*$')

(:raw query result:)
let $result         :=
    if ($validSearch) then
      if ($codeSearch) then
            collection(concat($get:strTerminologyData,'/nictiz-demo-data/nhg'))//desc[starts-with(../@code,$searchString)]
      else
            collection(concat($get:strTerminologyData,'/nictiz-demo-data/nhg'))//desc[ft:query(.,$query,$options)]
(:            collection(concat($get:strTerminologyData,'/nictiz-demo-data'))//desc[starts-with(.,$searchString)]:)
    else ()

(:order result by count and length:)
let $result         := 
    for $desc in $result
    order by xs:integer($desc/@count),xs:integer($desc/@length)
    return $desc

  
let $count          := count($result)
let $current        := if ($count>$maxResults) then $maxResults else ($count)
return
    <result current="{$current}" count="{$count}">
    {
        for $res in subsequence($result,1,$maxResults)
        let $conceptId  := $res/parent::concept/@conceptId
        let $code       := $res/parent::concept/@code
        return
        <description code="{$code}" conceptId="{$conceptId}">{$res/@type,$res/text()}</description>
    }
    </result>
    