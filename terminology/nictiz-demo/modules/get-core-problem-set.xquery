xquery version "3.0";
(:
	Copyright (C) 2011-2017 Art Decor Expert Group art-decor.org
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace snomed      = "http://art-decor.org/ns/terminology/snomed" at "../../snomed/api/api-snomed.xqm";

let $omahaCode    := util:unescape-uri(request:get-parameter('omaha',('')),'UTF-8')
let $nandaCode    := util:unescape-uri(request:get-parameter('nanda',('')),'UTF-8')
(:let $omahaCode  := ''
let $nandaCode :='':)

let $coreProblemSet    :=
   if(string-length($omahaCode) gt 0) then 
      let $omahaMapping := collection(concat($get:strTerminologyData,'/nictiz-demo-data/omaha/'))/omaha-problem-map/row[OMAHA_code=$omahaCode]
      for $mapping in $omahaMapping/SNOMED_CT_code
      return
      $snomed:colDataBase//concept[@active][@conceptId=$mapping]
   else if (string-length($nandaCode) gt 0) then
      let $nandaMapping := collection(concat($get:strTerminologyData,'/nictiz-demo-data/nanda/'))/nanda-problem-map/row[NANDA_code=$nandaCode]
      for $mapping in $nandaMapping/SNOMED_CT_code
      return
      $snomed:colDataBase//concept[@active][@conceptId=$mapping]
   else
      $snomed:colDataBase//refset[@refsetId='11721000146100']/ancestor::concept[@active]




return
<problems omaha="{$omahaCode}" nanda="{$nandaCode}">
<concept conceptId="">
   <desc languageRefsetId="31000146106"/><omaha code=""/>
</concept>
{
   for $concept in $coreProblemSet
   order by $concept/desc[@languageRefsetId='31000146106'][1]
   return
   <concept conceptId="{$concept/@conceptId}">
   {
   for $desc in $concept/desc[@languageRefsetId='31000146106'][@type='pref']
   return
   <desc>{$desc/text()}</desc>
   ,
   $concept/refsets,
   for $map in $concept//map
   return
   if ($map/@refsetId='32311000146108') then
      <omaha code="{$map/@mapTarget}"/>
   else if ($map/@refsetId='31451000146105') then
      <nanda code="{$map/@mapTarget}"/>
   else()
   }
   </concept>
}
</problems>



