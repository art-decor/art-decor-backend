xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace snomed              = "http://art-decor.org/ns/terminology/snomed" at "../../snomed/api/api-snomed.xqm";

let $searchString   := replace(util:unescape-uri(request:get-parameter('string',('')),'UTF-8'),"[\)\(-]"," ")
let $toplevels      := tokenize(util:unescape-uri(request:get-parameter('toplevels',''),'UTF-8'),'\s')
let $refsets        := tokenize(util:unescape-uri(request:get-parameter('refsets',''),'UTF-8'),'\s')

(:let $searchString :='endo':)
(:let $toplevels    :=  ():)
(:let $refsets      :=  ():)
let $validSearch    :=
    if (matches($searchString,'^[a-z|0-9]') and string-length($searchString)>2 and string-length($searchString)<60) 
    then true()
    else if (matches($searchString,'^[A-Z]') and string-length($searchString)>1 and string-length($searchString)<60) 
    then true()
    else false()
let $searchTerms    := tokenize(lower-case($searchString),'\s')
let $options        := snomed:getSimpleLuceneOptions()
let $query          :=     
   <query>
        <bool>
        {
            for $term in $searchTerms
            return
            <wildcard occur="must">{concat(lower-case($term),'*')}</wildcard>
        }
        </bool>
    </query>
let $toplevels      := $toplevels[not(.='')]
let $refsets        := $refsets[not(.='')]
let $maxResults     := $snomed:maxResults
(:raw query result:)
let $result         :=
    if ($validSearch) then
        if (empty($toplevels) and empty($refsets)) then
            $snomed:colDataBase//desc[ft:query(.,$query,$options)][@active][../@active]
        else
        if (empty($toplevels)) then
            $snomed:colDataBase//desc[ft:query(.,$query,$options)][..//@refsetId=$refsets][@active][../@active]
        else
        if (empty($refsets)) then
            $snomed:colDataBase//desc[ft:query(.,$query,$options)][../ancestors/id=$toplevels][@active][../@active]
        else (
            $snomed:colDataBase//desc[ft:query(.,$query,$options)][../ancestors/id=$toplevels][..//@refsetId=$refsets][@active][../@active]
        )
    else ()

(:order result by count and length:)
let $result         := 
    for $desc in $result
    order by xs:integer($desc/@count),xs:integer($desc/@length)
    return $desc[@languageRefsetId='15551000146102']

(:group result by conceptId:)
(:let $result         :=
    for $desc in $result
    let $cc := $desc/parent::concept/@conceptId
    group by $cc    
    order by xs:integer($desc[1]/@count),xs:integer($desc[1]/@length)
    return $desc[1]:)
  
let $count          := count($result)
let $current        := if ($count>$maxResults) then $maxResults else ($count)
return
    <result current="{$current}" count="{$count}">
    {
        for $res in subsequence($result,1,$maxResults)
        let $conceptId  := $res/parent::concept/@conceptId
        let $fsn        := $snomed:colDataBase//@conceptId[.=$conceptId]/parent::concept/desc[@active][@type='fsn'][@languageCode = 'en']/text()
        return
        <description conceptId="{$conceptId}" fullName="{$fsn}">{$res/@type,$res/text()}</description>
    }
    </result>
