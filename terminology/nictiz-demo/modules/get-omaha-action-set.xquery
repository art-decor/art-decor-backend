xquery version "3.0";
(:
	Copyright (C) 2011-2017 Art Decor Expert Group art-decor.org
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";



let $conceptId      := util:unescape-uri(request:get-parameter('conceptId',('')),'UTF-8')

(:let $areaCode  := '1':)

let $omahaActionSet    :=
      if (string-length($conceptId) gt 0) then         
         let $omahaMapping := collection(concat($get:strTerminologyData,'/nictiz-demo-data/omaha/'))/omaha-intervention-map/row[Kernset_SNOMED_code=$conceptId]
         for $mapping in distinct-values($omahaMapping/Code_omaha_actievlak)
         return
         collection(concat($get:strTerminologyData,'/nictiz-demo-data/omaha/'))/actions/concept[@code=$mapping]
      else
         collection(concat($get:strTerminologyData,'/nictiz-demo-data/omaha/'))/actions/concept


return
<problems>
{
   for $action in $omahaActionSet
   order by $action/desc
   return
   $action
}
</problems>



