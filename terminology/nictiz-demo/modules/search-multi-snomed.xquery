xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace snomed              = "http://art-decor.org/ns/terminology/snomed" at "../../snomed/api/api-snomed.xqm";

let $searchString   := util:unescape-uri(request:get-parameter('string',('')),'UTF-8')

(:let $searchString :='hyper':)
(:let $toplevels    :=  ():)
(:let $refsets      :=  ():)
let $validSearch    := snomed:isValidSearch($searchString)
let $searchTerms    := tokenize(tokenize($searchString,'\s'),'-')
let $options        := snomed:getSimpleLuceneOptions()
let $query          :=     
   <query>
        <bool>
        {
            for $term in $searchTerms
            return
            <wildcard occur="must">{concat(lower-case($term),'*')}</wildcard>
        }
        </bool>
    </query>
let $maxResults     := $snomed:maxResults
(:raw query result:)
let $result         :=
    if ($validSearch) then
      collection(concat($get:strTerminologyData,'/nictiz-demo-data/snomed'))//desc[ft:query(.,$query,$options)][@languageCode='nl']
    else ()

(:order result by count and length:)
let $result         := 
    for $desc in $result
    order by xs:integer($desc/@count),xs:integer($desc/@length)
    return $desc

(:group result by conceptId:)
(:let $result         :=
    for $desc in $result
    let $cc := $desc/parent::concept/@conceptId
    group by $cc    
    order by xs:integer($desc[1]/@count),xs:integer($desc[1]/@length)
    return $desc[1]:)
  
let $count          := count($result)
let $current        := if ($count>$maxResults) then $maxResults else ($count)
return
    <result current="{$current}" count="{$count}">
    {
        for $res in subsequence($result,1,$maxResults)
        let $conceptId  := $res/parent::concept/@conceptId
        return
        <description conceptId="{$conceptId}">{$res/@type,$res/text()}</description>
    }
    </result>
