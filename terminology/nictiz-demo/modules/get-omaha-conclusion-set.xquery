xquery version "3.0";
(:
	Copyright (C) 2011-2017 Art Decor Expert Group art-decor.org
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";



let $valueset      := util:unescape-uri(request:get-parameter('valueset',('')),'UTF-8')

(:let $areaCode  := '1':)

let $omahaConclusionSet    := collection(concat($get:strTerminologyData,'/nictiz-demo-data/omaha/'))/conclusions/valueset[@id=$valueset]/concept


return
<conclusions>
   <concept code=""><desc></desc></concept>
{
   for $conclusion in $omahaConclusionSet
   order by $conclusion/desc
   return
   $conclusion
}
</conclusions>



