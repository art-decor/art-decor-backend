xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace snomed              = "http://art-decor.org/ns/terminology/snomed" at "../../snomed/api/api-snomed.xqm";

let $concepts  := collection(concat($get:strTerminologyData,'/nictiz-demo-data/snomed'))//concept


return
<concepts count="{count($concepts)}" nl="{count($concepts[desc/@languageCode='nl'])}" de="{count($concepts[desc/@languageCode='de'])}" pl="{count($concepts[desc/@languageCode='pl'])}" fr="{count($concepts[desc/@languageCode='fr'])}" es="{count($concepts[desc/@languageCode='es'])}"></concepts>