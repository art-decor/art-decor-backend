xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get      = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace snomed   = "http://art-decor.org/ns/terminology/snomed" at "../../snomed/api/api-snomed.xqm";

declare namespace       json     =  "http://www.json.org";
declare namespace       output   = "http://www.w3.org/2010/xslt-xquery-serialization";

declare option          output:method "json";
declare option          output:media-type "application/json";

let $valueSetId   := request:get-parameter('oid','')
let $language     :=request:get-parameter('lang','nl-NL')

let $valueSet := collection($get:strValuesetStableData)//valueSet[@oid=$valueSetId]

return
<valueSet>
{
   for $concept in $valueSet//include/concept
   let $codeSystem := $concept/parent::include/system/@oid
   return
   if ($concept/designation[@languageCode=$language][@use='displ']) then
      <concept code="{$concept/@code}" codeSystem="{$codeSystem}" displayName="{$concept/designation[@languageCode=$language][@use='displ']/text()}"/>
   else if ($concept/designation[@languageCode=$language][@use='pref']) then
      <concept code="{$concept/@code}" codeSystem="{$codeSystem}" displayName="{$concept/designation[@languageCode=$language][@use='pref']/text()}"/>
   else <concept code="{$concept/@code}" codeSystem="{$codeSystem}" displayName="{$concept/designation[@use='pref']/text()}"/>
}
</valueSet>