xquery version "3.0";
(:
	Copyright (C) 2011-2017 Art Decor Expert Group art-decor.org
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace snomed      = "http://art-decor.org/ns/terminology/snomed" at "../../snomed/api/api-snomed.xqm";

let $conceptId   := util:unescape-uri(request:get-parameter('conceptId',('')),'UTF-8')
(:let $areaCode  := '1':)


let $nandaProblemSet    :=
   if(string-length($conceptId) gt 0) then 
         let $nandaMapping := collection(concat($get:strTerminologyData,'/nictiz-demo-data/nanda/'))/nanda-problem-map/row[SNOMED_CT_code=$conceptId]
         for $mapping in distinct-values($nandaMapping/NANDA_code)
         return
         collection(concat($get:strTerminologyData,'/nictiz-demo-data/nanda/'))/problems/concept[@code=$mapping]
   else 
      collection(concat($get:strTerminologyData,'/nictiz-demo-data/nanda/'))/problems/concept





return
<problems core="{$conceptId}">
   <concept code="" domain="" class="">
      <desc/>
   </concept>
   {
      for $problem in $nandaProblemSet
      order by $problem/desc
      return
      $problem
   }
</problems>



