xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get      = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace aduser   = "http://art-decor.org/ns/art-decor-users" at "../../../art/api/api-user-settings.xqm";

declare namespace request        = "http://exist-db.org/xquery/request";
declare namespace response       = "http://exist-db.org/xquery/response";
declare namespace sm             = "http://exist-db.org/xquery/securitymanager";
declare namespace output         = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace fn             = "http://www.w3.org/2005/xpath-functions";

declare option    output:method "json";
declare option    output:media-type "application/json";


let $set := request:get-parameter('set','a')

(:let $set := 'a':)

let $restoreCollection :=
      if ($set='a') then 'episodes-A'
      else 'episodes-B'

let $delete :=
   for $episode in collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib/episodes'))//episode
   let $file := util:document-name($episode)
   return
   xmldb:remove(concat($get:strTerminologyData,'/nictiz-demo-data/zib/episodes'),$file)

let $restoreEpisodes := 
   for $file in xmldb:get-child-resources(xs:anyURI(concat($get:strTerminologyData,'/nictiz-demo-data/zib/',$restoreCollection)))
   return
   xmldb:copy-resource(concat($get:strTerminologyData,'/nictiz-demo-data/zib/',$restoreCollection),$file,concat($get:strTerminologyData,'/nictiz-demo-data/zib/episodes'),$file,xs:boolean('true'))
   



  
return
<result>
   {$restoreEpisodes}
</result>