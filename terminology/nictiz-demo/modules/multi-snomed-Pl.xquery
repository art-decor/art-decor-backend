xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace snomed              = "http://art-decor.org/ns/terminology/snomed" at "../../snomed/api/api-snomed.xqm";

let $descriptions :=collection(concat($get:strTerminologyData,'/nictiz-demo-data/import'))//row

for $description in $descriptions
let $multiConcept :=collection(concat($get:strTerminologyData,'/nictiz-demo-data/import'))//concept[@conceptId=$description/SCTID]
let $desc :=
   <desc type="pref" languageCode="pl">{normalize-space($description/PTN_-_Pools/text())}</desc>

return
if ($multiConcept) then
   update insert $desc into $multiConcept
else
   let $snomedConcept := $snomed:colDataBase//concept[@conceptId=$description/@conceptId]
   let $newConcept :=
              <concept>
        {
            $snomedConcept[1]/@*,
            $snomedConcept[1]/desc,
            $desc

        }
        </concept>
    return
    update insert $newConcept into collection(concat($get:strTerminologyData,'/nictiz-demo-data/import'))//concepts
    