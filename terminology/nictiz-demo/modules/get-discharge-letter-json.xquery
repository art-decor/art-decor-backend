xquery version "3.1";
(:
    Copyright (C) 2011-2015 ART-DECOR Expert Group (art-decor.org)
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)

import module namespace get      = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";

declare namespace output         = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace fn             ="http://www.w3.org/2005/xpath-functions";
declare namespace       json     =  "http://www.json.org";

declare option output:method     "json";
declare option output:media-type "application/json";
declare option exist:serialize   "json-ignore-whitespace-text-nodes=yes";

let $id := request:get-parameter('id','')

let $episode := collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib/episodes'))//episode[@id=$id]

let $linkList := collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib/patientTerms'))//linkList

let $patientFriendlyLanguageRefsetId := '15551000146102'
let $preferredLanguageRefsetId := '31000146106'
let $fallbackLanguageRefsetId :='900000000000509007'


return
<letter>
   {
   for $complaint in $episode/problem[type/@code='409586006']
   let $concept := collection(concat($get:strTerminologyData,'/snomed-data/en-GB'))//concept[@conceptId=$complaint/name/@code]
   let $patientTerm := 
        if ($concept/desc[@languageRefsetId = $patientFriendlyLanguageRefsetId][@active = '1'][@type='pref']) then
            $concept/desc[@languageRefsetId = $patientFriendlyLanguageRefsetId][@active = '1'][@type='pref']/text()
        else if ($concept/desc[@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][@type='pref']) then
            $concept/desc[@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][@type='pref']/text()
        else
            $concept/desc[@languageRefsetId = $fallbackLanguageRefsetId][@active = '1'][@type='pref']/text()
   return
   <complaint json:array="true">
      {
         $complaint/@*,
         $complaint/*,
         if ($linkList/row/SNOMED_ID = $complaint/name/@code) then <link json:literal="true">true</link> else ()
      }
      <patientTerm>{$patientTerm}</patientTerm>
      <patientDescription>{$concept/definition[@languageRefsetId = $patientFriendlyLanguageRefsetId][@active = '1']/text()}</patientDescription>
   </complaint>
   ,
   for $diagnosis in $episode/problem[type/@code='282291009']
   let $concept := collection(concat($get:strTerminologyData,'/snomed-data/en-GB'))//concept[@conceptId=$diagnosis/name/@code]
   let $patientTerm := 
        if ($concept/desc[@languageRefsetId = $patientFriendlyLanguageRefsetId][@active = '1'][@type='pref']) then
            $concept/desc[@languageRefsetId = $patientFriendlyLanguageRefsetId][@active = '1'][@type='pref']/text()
        else if ($concept/desc[@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][@type='pref']) then
            $concept/desc[@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][@type='pref']/text()
        else
            $concept/desc[@languageRefsetId = $fallbackLanguageRefsetId][@active = '1'][@type='pref']/text()
   return
   <diagnosis json:array="true">
      {
         $diagnosis/@*,
         $diagnosis/*,
         if ($linkList/row/SNOMED_ID = $diagnosis/name/@code) then <link json:literal="true">true</link> else ()   
      }
      <patientTerm>{$patientTerm}</patientTerm>
      <patientDescription>{$concept/definition[@languageRefsetId = $patientFriendlyLanguageRefsetId][@active = '1']/text()}</patientDescription>
   </diagnosis>
   ,
   for $complication in $episode/problem[type/@code='116223007']
   let $concept := collection(concat($get:strTerminologyData,'/snomed-data/en-GB'))//concept[@conceptId=$complication/name/@code]
   let $patientTerm := 
        if ($concept/desc[@languageRefsetId = $patientFriendlyLanguageRefsetId][@active = '1'][@type='pref']) then
            $concept/desc[@languageRefsetId = $patientFriendlyLanguageRefsetId][@active = '1'][@type='pref']/text()
        else if ($concept/desc[@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][@type='pref']) then
            $concept/desc[@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][@type='pref']/text()
        else
            $concept/desc[@languageRefsetId = $fallbackLanguageRefsetId][@active = '1'][@type='pref']/text()
   return
   <complication json:array="true">
      {
         $complication/@*,
         $complication/*,
         if ($linkList/row/SNOMED_ID = $complication/name/@code) then <link json:literal="true">true</link> else ()
      }
      <patientTerm>{$patientTerm}</patientTerm>
      <patientDescription>{$concept/definition[@languageRefsetId = $patientFriendlyLanguageRefsetId][@active = '1']/text()}</patientDescription>
   </complication>
   }
</letter>