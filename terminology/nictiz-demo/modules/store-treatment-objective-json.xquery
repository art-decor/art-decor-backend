xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:  
  
      if ($action='store') then
      try {
      xmldb:store(concat($get:strTerminologyData,'/nictiz-demo-data/zib/nursing-problems'),concat($problemId,'.xml'),$problem)
  
      }
      catch * {()}
   else if ($action='update' and $problem) then
      try {
      update replace collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib/nursing-problems'))//problem[@id=$problemId]  with $problem
      } 
      catch * {()}
   else()
    
:)

import module namespace get      = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace aduser   = "http://art-decor.org/ns/art-decor-users" at "../../../art/api/api-user-settings.xqm";

declare namespace request        = "http://exist-db.org/xquery/request";
declare namespace response       = "http://exist-db.org/xquery/response";
declare namespace sm             = "http://exist-db.org/xquery/securitymanager";
declare namespace output         = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace fn             = "http://www.w3.org/2005/xpath-functions";

declare option    output:method "json";
declare option    output:media-type "application/json";

let $inputData := util:binary-to-string(request:get-data())

let $nodes     := json-to-xml($inputData)/fn:map
let $id        := util:uuid()
let $problemId := $nodes/fn:string[@key='problemId']/text()

let $treatmentObjective   :=
                  <treatmentObjective id="{$id}" problemId="{$nodes/fn:string[@key='problemId']/text()}" createDate="{current-dateTime()}">
                     <text>{$nodes/fn:string[@key='text']/text()}</text>
                  </treatmentObjective>
           





return
<result>
{
   try {
   if (collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib/nursing-problems'))//problem[@id=$problemId]/treatmentObjective) then
      update insert $treatmentObjective preceding collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib/nursing-problems'))//problem[@id=$problemId]/treatmentObjective[1]
   else
      update insert $treatmentObjective into collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib/nursing-problems'))//problem[@id=$problemId]
   }
   catch * {('error')}
}
</result>