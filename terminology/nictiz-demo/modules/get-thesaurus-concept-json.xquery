xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";

declare namespace       json     =  "http://www.json.org";
declare namespace       output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option          output:method "json";
declare option          output:media-type "application/json";

let $conceptId := request:get-parameter('id','')

let $concept := collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib'))//concept[@conceptId=$conceptId]

return
<concept>
   {
   $concept/@*,
   for $desc in $concept/desc
   return
   <desc json:array="true">{$desc/@*,$desc/text()}</desc>
   ,
   for $icd in $concept/icd10
   order by $icd/@code
   return
   <icd10 json:array="true">{$icd/@*}</icd10>
   ,
   for $dbc in $concept/dbc
   order by $dbc/@specialisme,$dbc/@code
   return
   <dbc json:array="true">{$dbc/@*}</dbc>
   ,
   for $za in $concept/za
   return
   <za json:array="true">{$za/@*}</za>
   ,
   for $concilium in $concept/concilium
   return
   <concilium json:array="true">{$concilium/@*}</concilium>
   ,
   for $cbv in $concept/cbv
   return
   <cbv json:array="true">{$cbv/@*}</cbv>
   }
</concept>