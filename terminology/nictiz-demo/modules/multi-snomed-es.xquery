xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace snomed              = "http://art-decor.org/ns/terminology/snomed" at "../../snomed/api/api-snomed.xqm";




let $concepts :=collection(concat($get:strTerminologyData,'/nictiz-demo-data/import'))//concept

for $concept in $concepts
let $descriptions :=collection(concat($get:strTerminologyData,'/nictiz-demo-data/import'))//description[@conceptId=$concept/@conceptId]
for $description in $descriptions

let $acceptability   :=  collection(concat($get:strTerminologyData,'/nictiz-demo-data/import'))//language[@referencedComponentId=$description/@id]/@acceptabilityId
let $type            := 
      if ($acceptability='900000000000548007' and $description/@typeId='900000000000003001') then
         'fsn'
      else if ($acceptability='900000000000548007' and $description/@typeId='900000000000013009') then
         'pref'
      else 'syn'
let $desc :=
   <desc type="{$type}" languageCode="{$description/@languageCode}">{$description/@term/string()}</desc>

return
if ($description) then
   update insert $desc into $concept
else()
    