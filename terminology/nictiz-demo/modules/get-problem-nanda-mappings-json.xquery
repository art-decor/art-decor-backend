xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";

declare namespace       json     =  "http://www.json.org";
declare namespace       output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option          output:method "json";
declare option          output:media-type "application/json";

let $problemId := request:get-parameter('id','')
(:let $problemId := 'df14c34b-ad3e-4cbb-94c3-4bad3eecbbda':)



let $problem := collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib/nursing-problems'))//problem[@id=$problemId]


let $problemMappings     :=
         let $nandaMapping := collection(concat($get:strTerminologyData,'/nictiz-demo-data/nanda/'))/nanda-problem-map/row[SNOMED_CT_code=$problem/name/@code]
         let $interventions := collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib/nursing-problems'))//intervention[@problemId=$problem/@id]
         return
            <problem createDate="{$problem/@createDate}" id="{$problem/@id}" json:array="true">
            {
               $problem/name,
               $problem/type,
               <nandaProblem name="{$nandaMapping/NANDA_term/text()}"/>
               ,
               for $intervention in $interventions
                  let $nandaInterventionMapping := collection(concat($get:strTerminologyData,'/nictiz-demo-data/nanda/'))/nanda-intervention-map/row[Kernset_SNOMED_code=$intervention/name/@code]
                  return
                  <intervention createDate="{$intervention/@createDate}" id="{$intervention/@id}" json:array="true">
                  {
                     $intervention/name,
                     for $interventionMapping in $nandaInterventionMapping
                     return
                     <nandaAction name="{$interventionMapping/Term_Omaha_actievlak}" actionType="{$interventionMapping/term_Omaha_soort_actie}"/>
                     ,
                     for $outcome in $intervention/outcome
                     let $nandaOutcomeMapping := collection(concat($get:strTerminologyData,'/nictiz-demo-data/nanda/'))/nanda-conclusion-map/row[Kernset_SNOMED_code=$outcome/name/@code]
                     return
                     <outcome createDate="{$outcome/@createDate}" id="{$outcome/@id}" json:array="true">
                     {
                     $outcome/name,
                     for $outcomeMapping in $nandaOutcomeMapping
                     return
                     <nandaConclusion name="{$outcomeMapping/term_Omaha_}" id="{util:uuid()}"/>
                     }
                     </outcome>
                  }
                  </intervention>
            }
            </problem>

return
<mappings>
{
 $problemMappings
}
</mappings>