xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

declare namespace sm             = "http://exist-db.org/xquery/securitymanager";
declare namespace request        = "http://exist-db.org/xquery/request";
declare namespace response       = "http://exist-db.org/xquery/response";
declare namespace json           = "http://www.json.org";
declare namespace output         = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace fn             = "http://www.w3.org/2005/xpath-functions";

declare option          output:method "json";
declare option          output:media-type "application/json";

    let $logout             := session:invalidate()
    return
        <user name="" logged-in="false">
            <defaultLanguage></defaultLanguage>
            <displayName/>
            <groups/>
        </user>


