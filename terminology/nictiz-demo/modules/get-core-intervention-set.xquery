xquery version "3.0";
(:
	Copyright (C) 2011-2017 Art Decor Expert Group art-decor.org
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace snomed      = "http://art-decor.org/ns/terminology/snomed" at "../../snomed/api/api-snomed.xqm";

let $coreCode     := util:unescape-uri(request:get-parameter('conceptId',('')),'UTF-8')
let $nandaCode    := util:unescape-uri(request:get-parameter('nanda',('')),'UTF-8')
let $omahaCode    := util:unescape-uri(request:get-parameter('omaha',('')),'UTF-8')


let $coreInterventionSet    :=
   if(string-length($coreCode) gt 0) then 
      let $interventionMap := collection(concat($get:strTerminologyData,'/nictiz-demo-data/core/'))/interventions/map[@conceptId=$coreCode]/@refsetId
      return
      $snomed:colDataBase//refset[@refsetId=$interventionMap]/ancestor::concept[@active]
   else if (string-length($nandaCode) gt 0) then
      let $nandaMapping := collection(concat($get:strTerminologyData,'/nictiz-demo-data/nanda/'))/nanda-intervention-map/row[Nanda_code_interventie=$nandaCode]
      for $mapping in $nandaMapping/Kernset_SNOMED_code
      return
      $snomed:colDataBase//concept[@active][@conceptId=$mapping]
   else if(string-length($omahaCode) gt 0) then 
      let $omahaMapping := collection(concat($get:strTerminologyData,'/nictiz-demo-data/omaha/'))/omaha-intervention-map/row[Code_omaha_actievlak=$omahaCode]
      for $mapping in $omahaMapping/Kernset_SNOMED_code
      return
      $snomed:colDataBase//concept[@active][@conceptId=$mapping]
   else ()



return
<problems omaha="{$omahaCode}" nanda="{$nandaCode}">
{   
   if (count($coreInterventionSet) gt 1) then
      <concept conceptId=""><desc></desc></concept>
   else(),
   for $concept in $coreInterventionSet
   order by $concept/desc[@languageRefsetId='31000146106'][1]
   return
   <concept conceptId="{$concept/@conceptId}">
   {
   for $desc in $concept/desc[@languageRefsetId='31000146106'][@type='pref']
   return
   <desc>{$desc/text()}</desc>
   ,
   $concept/refsets,
   for $map in $concept//map
   return
   if ($map/@refsetId='32311000146108') then
      <omaha code="{$map/@mapTarget}"/>
   else if ($map/@refsetId='31451000146105') then
      <nanda code="{$map/@mapTarget}"/>
   else()
   }
   </concept>
}
</problems>



