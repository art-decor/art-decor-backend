xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
declare namespace       json     =  "http://www.json.org";
declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "json";
declare option output:media-type "application/json";

let $patients := collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib'))/patients


for $patient in $patients/patient
let $record := <record patientId="{$patient/@bsn}" name="{$patient/name/last}"><description/></record>
return
try {
   xmldb:store(concat($get:strTerminologyData,'/nictiz-demo-data/zib/nursing-problems'),concat($patient/@id,'.xml'),$record)
}
catch * {('error')}