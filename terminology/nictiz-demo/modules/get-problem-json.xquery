xquery version "3.1";
(:
    Copyright (C) 2011-2015 ART-DECOR Expert Group (art-decor.org)
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)

import module namespace get      = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";

declare namespace output         = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace fn             ="http://www.w3.org/2005/xpath-functions";
declare namespace       json     =  "http://www.json.org";

declare option output:method     "json";
declare option output:media-type "application/json";
declare option exist:serialize   "json-ignore-whitespace-text-nodes=yes";

let $id := request:get-parameter('id','')
(:let $id := '7ef7fa0a-b9c0-4c69-b7fa-0ab9c04c69ca':)

let $problem := collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib/episodes'))//problem[@id=$id] | collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib/nursing-problems'))//problem[@id=$id]

return
<problems>
   {$problem}
</problems>