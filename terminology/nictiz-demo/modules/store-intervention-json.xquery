xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get      = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace aduser   = "http://art-decor.org/ns/art-decor-users" at "../../../art/api/api-user-settings.xqm";

declare namespace request        = "http://exist-db.org/xquery/request";
declare namespace response       = "http://exist-db.org/xquery/response";
declare namespace sm             = "http://exist-db.org/xquery/securitymanager";
declare namespace output         = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace fn             = "http://www.w3.org/2005/xpath-functions";

declare option    output:method "json";
declare option    output:media-type "application/json";

let $inputData := util:binary-to-string(request:get-data())

let $nodes     := json-to-xml($inputData)/fn:map
let $patientId := $nodes/fn:string[@key='patientId']/text()
let $treatmentObjectiveId := $nodes/fn:string[@key='treatmentObjectiveId']/text()

let $intervention   := 
                  <intervention id="{util:uuid()}" problemId="{$nodes/fn:string[@key='problemId']/text()}" createDate="{current-dateTime()}" startDate="{$nodes/fn:string[@key='startDate']/text()}" 
                           endDate="{$nodes/fn:string[@key='endDate']/text()}">
                     <description>{$nodes/fn:string[@key='description']/text()}</description>
                     <instruction>{$nodes/fn:string[@key='instruction']/text()}</instruction>
                     {
                         for $item in $nodes/fn:map
                        return
                        element {$item/@key}{
                           for $string in $item/fn:string
                           return
                           attribute {$string/@key} {
                              $string/text()
                           }
                        }
                     }
                  </intervention>

return
<result>
{
   if (collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib/nursing-problems'))//treatmentObjective[@id=$treatmentObjectiveId]/intervention) then
      update insert $intervention preceding collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib/nursing-problems'))//treatmentObjective[@id=$treatmentObjectiveId]/intervention[1]
   else
      update insert $intervention into collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib/nursing-problems'))//treatmentObjective[@id=$treatmentObjectiveId]
}
</result>