xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:
    
      if ($action='store') then
      try {
      xmldb:store(concat($get:strTerminologyData,'/nictiz-demo-data/zib/nursing-problems'),concat($problemId,'.xml'),$problem)
  
      }
      catch * {()}
   else if ($action='update' and $problem) then
      try {
      update replace collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib/nursing-problems'))//problem[@id=$problemId]  with $problem
      } 
      catch * {()}
   else()
    
:)

import module namespace get      = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace aduser   = "http://art-decor.org/ns/art-decor-users" at "../../../art/api/api-user-settings.xqm";

declare namespace request        = "http://exist-db.org/xquery/request";
declare namespace response       = "http://exist-db.org/xquery/response";
declare namespace sm             = "http://exist-db.org/xquery/securitymanager";
declare namespace output         = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace fn             = "http://www.w3.org/2005/xpath-functions";

declare option    output:method "json";
declare option    output:media-type "application/json";

let $inputData := util:binary-to-string(request:get-data())

let $nodes     := json-to-xml($inputData)/fn:map
let $problemId := if (string-length($nodes/fn:string[@key='id']/text()) gt 0) then
                     $nodes/fn:string[@key='id']/text()
                  else util:uuid()
let $patientId := $nodes/fn:string[@key='patientId']/text()
let $action    := if (string-length($nodes/fn:string[@key='id']/text()) gt 0) then
                     'update'
                  else 'store'
let $problem   :=            <problem id="{$problemId}" patientId="{$nodes/fn:string[@key='patientId']/text()}" createDate="{current-dateTime()}" startDate="{$nodes/fn:string[@key='startDate']/text()}" 
                           endDate="{$nodes/fn:string[@key='endDate']/text()}" 
                           woundBeginDate="{$nodes/fn:string[@key='woundBeginDate']/text()}" 
                           lastBandageChangeDate="{$nodes/fn:string[@key='lastBandageChangeDate']/text()}"
                           woundInfection="{$nodes/fn:boolean[@key='woundInfection']/text()}">
                     <description>{$nodes/fn:string[@key='description']/text()}</description>
                     {
                         for $item in $nodes/fn:map
                        return
                        element {$item/@key}{
                           for $string in $item/fn:string
                           return
                           attribute {$string/@key} {
                              $string/text()
                           }
                        }
                     }
                  </problem>
           



let $record   :=
            if ($action='store') then
               collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib/nursing-problems'))/record[@patientId=$patientId]
            else if ($action='update') then
               collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib/nursing-problems'))//problem[@id=$problemId]/parent::record
            else()



return
<result>
{
   if ($action='store') then
      try {
      update insert $problem into $record 
      }
      catch * {()}
   else if ($action='update' and $problem) then
      try {
      update replace $record/problem[@id=$problemId]  with $problem
      } 
      catch * {()}
   else()
}
</result>