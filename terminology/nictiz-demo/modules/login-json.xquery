xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get      = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace aduser   = "http://art-decor.org/ns/art-decor-users" at "../../../art/api/api-user-settings.xqm";

declare namespace sm             = "http://exist-db.org/xquery/securitymanager";
declare namespace request        = "http://exist-db.org/xquery/request";
declare namespace response       = "http://exist-db.org/xquery/response";
declare namespace json           = "http://www.json.org";
declare namespace output         = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace fn             = "http://www.w3.org/2005/xpath-functions";

declare option          output:method "json";
declare option          output:media-type "application/json";
(:<user>
    <username>username</username>
    <password>password</password>
</user>:)
(:Before login:

<sm:id xmlns:sm="http://exist-db.org/xquery/securitymanager">
    <sm:real>
        <sm:username>admin</sm:username>
        <sm:groups>
            <sm:group>dba</sm:group>
        </sm:groups>
    </sm:real>
</sm:id>:)
(:After login for different user:

<sm:id xmlns:sm="http://exist-db.org/xquery/securitymanager">
    <sm:real>
        <sm:username>admin</sm:username>
        <sm:groups>
            <sm:group>dba</sm:group>
        </sm:groups>
    </sm:real>
    <sm:effective>
        <sm:username>alexander</sm:username>
        <sm:groups>
            <sm:group>dba</sm:group>
            <sm:group>debug</sm:group>
            <sm:group>decor</sm:group>
            <sm:group>decor-admin</sm:group>
            <sm:group>editor</sm:group>
            <sm:group>issues</sm:group>
            <sm:group>terminology</sm:group>
            <sm:group>tools</sm:group>
        </sm:groups>
    </sm:effective>
</sm:id>:)

let $user := get:strCurrentUserName()

let $userLanguage       := aduser:getUserLanguage($user)
let $userDisplayName    := aduser:getUserDisplayName($user)
let $groups             := sm:get-user-groups($user)
return
<response>
   <user name="{$user}" logged-in="true">
      <defaultLanguage>{$userLanguage}</defaultLanguage>
      <displayName>{$userDisplayName}</displayName>
      <groups>{for $group in $groups return <group json:array="true">{$group}</group>}</groups>
   </user>
</response>


