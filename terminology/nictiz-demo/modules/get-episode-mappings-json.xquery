xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";

declare namespace       json     =  "http://www.json.org";
declare namespace       output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option          output:method "json";
declare option          output:media-type "application/json";

let $episodeId := request:get-parameter('id','')
(:let $episodeId := 'afc6eafb-0973-4da4-86ea-fb09735da4a3':)

let $problemTypeDiagnosis := '282291009'

let $episode := collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib'))//episode[@id=$episodeId]
let $problems := $episode//problem[type/@code=$problemTypeDiagnosis]
let $concepts     :=
      for $problem in $problems
      return
      collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib'))//concept[@snomedId=$problem/name/@code]

return
<mappings>
{
 for $concept in $concepts
 return
   <concept json:array="true">
      {
      $concept/@*,
      for $desc in $concept/desc
      return
      <desc json:array="true">{$desc/@*,$desc/text()}</desc>
      ,
      for $icd in $concept/icd10
      order by $icd/@code
      return
      <icd10 id="{util:uuid()}" json:array="true">{$icd/@*}</icd10>
      ,
      for $dbc in $concept/dbc
      order by $dbc/@specialisme,$dbc/@code
      return
      <dbc id="{util:uuid()}" json:array="true">{$dbc/@*}</dbc>
      ,
      for $za in $concept/za
      return
      <za id="{util:uuid()}" json:array="true">{$za/@*}</za>
      ,
      for $concilium in $concept/concilium
      return
      <concilium id="{util:uuid()}" json:array="true">{$concilium/@*}</concilium>
      ,
      for $cbv in $concept/cbv
      return
      <cbv id="{util:uuid()}" json:array="true">{$cbv/@*}</cbv>
      }
   </concept>
}
</mappings>