xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get      = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace aduser   = "http://art-decor.org/ns/art-decor-users" at "../../../art/api/api-user-settings.xqm";

declare namespace request        = "http://exist-db.org/xquery/request";
declare namespace response       = "http://exist-db.org/xquery/response";
declare namespace sm             = "http://exist-db.org/xquery/securitymanager";
declare namespace output         = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace fn             = "http://www.w3.org/2005/xpath-functions";

declare namespace       json     =  "http://www.json.org";

declare option    output:method "json";
declare option    output:media-type "application/json";

let $inputData := util:binary-to-string(request:get-data())

let $nodes     := json-to-xml($inputData)/fn:map
let $problemId := $nodes/fn:string[@key='problemId']/text()
let $interventionId := $nodes/fn:string[@key='interventionId']/text()
let $context   := $nodes/fn:string[@key='context']/text()

let $observation   := 
                  <observation id="{util:uuid()}" createDate="{current-dateTime()}" json:array="true">
                     {  
                        for $string in $nodes/fn:string[not(@key=('id','createDate'))]
                        return
                        element {$string/@key} {
                              $string/text()
                           },
                        for $bool in $nodes/fn:boolean
                        return
                        element {$bool/@key} {
                              $bool/text()
                           },
                        for $number in $nodes/fn:number
                        return
                        element {$number/@key} {
                              $number/text()
                           },
                        for $item in $nodes/fn:map
                        return
                        element {$item/@key}{
                           for $string in $item/fn:string
                           return
                           attribute {$string/@key} {
                              $string/text()
                           }
                        },
                        for $array in $nodes/fn:array
                        return
                        for $item in $array/fn:map
                        return
                        element {$array/@key}{
                           for $string in $item/fn:string
                           return
                           attribute {$string/@key} {
                              $string/text()
                           }
                        }
                     }
                  </observation>

return
<result>
{
    try {
    if ($context ='problem') then
      if (collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib/nursing-problems'))//problem[@id=$problemId]/observation) then
         update insert $observation preceding collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib/nursing-problems'))//problem[@id=$problemId]/observation[1]
      else
         update insert $observation into collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib/nursing-problems'))//problem[@id=$problemId]
     else if ($context = 'intervention') then
      if (collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib/nursing-problems'))//intervention[@id=$interventionId]/observation) then
         update insert $observation preceding collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib/nursing-problems'))//intervention[@id=$interventionId]/observation[1]
      else
         update insert $observation into collection(concat($get:strTerminologyData,'/nictiz-demo-data/zib/nursing-problems'))//intervention[@id=$interventionId]
     else()
   }
   catch * {('error')}
}
</result>