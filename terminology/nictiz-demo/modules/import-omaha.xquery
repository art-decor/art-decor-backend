xquery version "3.0";
(:
	Copyright (C) 2011-2017 Art Decor Expert Group art-decor.org
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";


let $omahaProblems      := collection(concat($get:strTerminology,'/nictiz-demo/import'))/problemen
let $omahaActions       := collection(concat($get:strTerminology,'/nictiz-demo/import'))/acties
let $omahaConclusions   := collection(concat($get:strTerminology,'/nictiz-demo/import'))/conclusies

let $omahaAreaSet    :=
   for $code in distinct-values($omahaProblems//Code_gebieden)
   order by $code
   return
   <concept code="{$code}">
      <desc>{$omahaProblems/row[Code_gebieden=$code][1]/Term_gebieden/text()}</desc>
   </concept>

let $omahaProblemSet :=
   for $row in $omahaProblems/row
   return
   <concept code="{$row/Code_signaal_symptoom}" area="{$row/Code_gebieden}">
      <desc>{$row/Term_signaal_symptoom/text()}</desc>
   </concept>

let $omahaActionTypeSet  :=
   for $row in $omahaActions/row[string-length(code_soort_actie) gt 0]
   return
   <concept code="{$row/code_soort_actie}">
      <desc>{$row/term_soort_actie/text()}</desc>
   </concept>

let $omahaActionSet  :=
   for $row in $omahaActions/row[string-length(code_actievlak) gt 0]
   return
   <concept code="{$row/code_actievlak}">
      <desc>{$row/term_actievlak/text()}</desc>
   </concept>

let $omahaConclusionSet  :=
   for $row in $omahaConclusions/row
   return
   <concept code="{$row/Status}">
      <desc>{$row/Heading1/text()}</desc>
   </concept>

return
(:xmldb:store(concat($get:strTerminologyData,'/nictiz-demo-data/omaha/'),'areas.xml',<areas>{$omahaAreaSet}</areas>):)
(:xmldb:store(concat($get:strTerminologyData,'/nictiz-demo-data/omaha/'),'problems.xml',<problems>{$omahaProblemSet}</problems>):)
(:xmldb:store(concat($get:strTerminologyData,'/nictiz-demo-data/omaha/'),'actionTypes.xml',<actionTypes>{$omahaActionTypeSet}</actionTypes>):)
(:xmldb:store(concat($get:strTerminologyData,'/nictiz-demo-data/omaha/'),'actions.xml',<actions>{$omahaActionSet}</actions>):)
xmldb:store(concat($get:strTerminologyData,'/nictiz-demo-data/omaha/'),'conclusions.xml',<conclusions>{$omahaConclusionSet}</conclusions>)



