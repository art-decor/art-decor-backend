xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace snomed              = "http://art-decor.org/ns/terminology/snomed" at "../../snomed/api/api-snomed.xqm";

declare variable $maxResults     := xs:integer('50');


let $mappings := collection(concat($get:strTerminologyData,'/nictiz-demo-data/nhg'))//concept

return
    for $concept in $mappings
      let $snomedConcepts :=
         for $id in $concept/conceptId
         let $snomedConcept := $snomed:colDataBase//concept[@conceptId=$id]
         let $patientFriendly := if ($snomedConcept/desc[@languageRefsetId='15551000146102']) then
            'true'
            else 'false'
         return
         <conceptId patientFriendly="{$patientFriendly}">
            {$id/text()}
         </conceptId>
      let $snomedId :=
         if (count($snomedConcepts) gt 1) then
            if ($snomedConcepts[@patientFriendly='true']) then
               $snomedConcepts[@patientFriendly='true'][1]/text()
            else $snomedConcepts[1]/text()
         else $snomedConcepts/text()
    return
    <concept code="{$concept/@code}" conceptId="{$snomedId}">
      {
      
      $snomedConcepts
      ,
      $concept/desc}
    </concept>