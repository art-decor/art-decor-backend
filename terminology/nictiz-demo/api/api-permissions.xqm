xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
module namespace terminologydemopfix     = "http://art-decor.org/ns/terminologydemo-permissions";
declare namespace sm            = "http://exist-db.org/xquery/securitymanager";

(:  Mode            Octal
 :  rw-r--r--   ==  0644
 :  rw-rw-r--   ==  0664
 :  rwxr-xr--   ==  0754
 :  rwxr-xr-x   ==  0755
 :  rwxrwxr-x   ==  0775
 :)

(:install path for art (normally /db/apps/), includes trailing slash :)
declare variable $terminologydemopfix:root                   := repo:get-root();
declare variable $terminologydemopfix:strDemoModules       := concat($terminologydemopfix:root,'terminology/nictiz-demo/modules');

declare function terminologydemopfix:setTerminologyDemoQueryPermissions() {
    terminologydemopfix:checkIfUserDba(),
    
    for $query in xmldb:get-child-resources(xs:anyURI(concat('xmldb:exist:///',$terminologydemopfix:strDemoModules)))
    return (
        sm:chown(xs:anyURI(concat('xmldb:exist:///',$terminologydemopfix:strDemoModules,'/',$query)),'admin'),
        sm:chgrp(xs:anyURI(concat('xmldb:exist:///',$terminologydemopfix:strDemoModules,'/',$query)),'terminology'),
        if (starts-with($query,('check','get','is-','retrieve','search','view','validate','patient','specialist'))) then
            sm:chmod(xs:anyURI(concat('xmldb:exist:///',$terminologydemopfix:strDemoModules,'/',$query)),sm:octal-to-mode('0755'))
        else (
            sm:chmod(xs:anyURI(concat('xmldb:exist:///',$terminologydemopfix:strDemoModules,'/',$query)),sm:octal-to-mode('0754'))
        )
        ,
        sm:clear-acl(xs:anyURI(concat('xmldb:exist:///',$terminologydemopfix:strDemoModules,'/',$query)))
    )
};


(:
:   Helper function with recursion for terminologypfix:setSCTExtensionCollectionPermissions()
:)
declare %private function terminologydemopfix:setPermissions($path as xs:string, $collown as xs:string, $collmode as xs:string, $resown as xs:string, $resmode as xs:string) {
    sm:chown(xs:anyURI($path),$collown),
    sm:chmod(xs:anyURI($path),$collmode),
    sm:clear-acl(xs:anyURI($path)),
    for $res in xmldb:get-child-resources(xs:anyURI($path))
    return (
        sm:chown(xs:anyURI(concat($path,'/',$res)),$resown),
        sm:chmod(xs:anyURI(concat($path,'/',$res)),$resmode),
        sm:clear-acl(xs:anyURI(concat($path,'/',$res)))
    )
    ,
    for $collection in xmldb:get-child-collections($path)
    return
        terminologydemopfix:setPermissions(concat($path,'/',$collection), $collown, $collmode, $resown, $resmode)
};

declare %private function terminologydemopfix:checkIfUserDba() {
    if (sm:is-dba((sm:id()//sm:real/sm:username/text())[1])) then () else (
        error(QName('http://art-decor.org/ns/art-decor-permissions', 'NotAllowed'), concat('Only dba user can use this module. ',(sm:id()//sm:real/sm:username/text())[1]))
    )
};