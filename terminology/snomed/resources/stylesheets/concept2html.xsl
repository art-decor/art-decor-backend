<!--
    Copyright (C) 2011-2016 ART-DECOR Expert Group (art-decor.org)
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
--><!-- 
    DISCLAIMER
    Deze stylesheet en de resulterende html weergave van xml berichten zijn uitsluitend bedoeld voor testdoeleinden.
    Zij zijn uitdrukkelijk niet bedoeld voor gebruik in de medische praktijk.
     
   
    Boxover javascript door http://boxover.swazz.org
    (BoxOver is free and distributed under the GNU license) 
--><xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:hl7="urn:hl7-org:v3" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" exclude-result-prefixes="hl7" version="2.0">
    <xsl:output method="html" exclude-result-prefixes="#all" encoding="UTF-8"/> 
    <xsl:param name="xslt.language" select="'en-US'"/>
    <xsl:variable name="languageRefsetId">
        <xsl:choose>
            <xsl:when test="$xslt.language = 'nl-NL'">
                <xsl:value-of select="'31000146106'"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="'900000000000509007'"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable> 
    <!-- fallback language refset is en-US -->
    <xsl:variable name="fallbackLanguageRefsetId" select="'900000000000509007'"/>
    <xsl:variable name="refsetList">
        <xi:include href="/db/apps/terminology-data/snomed-data/meta/refsetsDisplay.xml">
            <xi:fallback>
                <refsets>
                    <refset id="98061000146100" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met allergenen uitgezonderd medicatie</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">Dutch total non-drug allergen simple reference set</desc>
                    </refset>
                    <refset id="98041000146101" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met beroepsallergenen</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">Dutch occupational allergen simple reference set</desc>
                    </refset>
                    <refset id="98051000146103" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met contactallergenen</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">Dutch contact allergen simple reference set</desc>
                    </refset>
                    <refset id="98021000146107" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met inhalatieallergenen</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">Dutch inhalation allergen simple reference set</desc>
                    </refset>
                    <refset id="98031000146109" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met insectengifallergenen</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">Dutch insect venom allergen simple reference set</desc>
                    </refset>
                    <refset id="98011000146102" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met voedselallergenen</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">Dutch food allergen simple reference set</desc>
                    </refset>
                    <refset id="52801000146101" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met implantaten uit landelijk implantatenregister</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">Dutch implant registry simple reference set</desc>
                    </refset>
                    <refset id="146481000146103" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met obstetrische verrichtingen</desc>
                    <desc languageCode="en" languageRefsetId="900000000000509007">Dutch obstetric procedures simple reference set</desc>
                    </refset>
                    <refset id="110851000146103" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor pathologie</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">PALGA thesaurus simple reference set for pathology</desc>
                    </refset>
                    <refset id="140741000146104" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met verrichtingen betreffende geestelijke gezondheidszorg</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">Dutch mental health procedures simple reference set</desc>
                    </refset>
                    <refset id="2551000146109" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met zeldzame neuromusculaire aandoeningen</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">Dutch rare neuromuscular disorders simple reference set</desc>
                    </refset>
                    <refset id="2581000146104" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met micro-organismen</desc>
                    <desc languageCode="en" languageRefsetId="900000000000509007">Dutch microorganism simple reference set</desc>
                    </refset>
                    <refset id="145871000146106" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met typen mengflora</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">Dutch mixed flora finding simple reference set</desc>
                    </refset>
                    <refset id="46231000146109" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met ordinale resultaten</desc>
                    <desc languageCode="en" languageRefsetId="900000000000509007">Dutch ordinal test result simple reference set</desc>
                    </refset>
                    <refset id="97801000146108" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met ordinale microscopieresultaten</desc>
                    <desc languageCode="en" languageRefsetId="900000000000509007">Dutch microscopic ordinal test result simple reference set</desc>
                    </refset>
                    <refset id="140301000146101" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met ordinale uitslagen antibioticagevoeligheidbepalingen</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">Dutch simple reference set for ordinal antimicrobial susceptibility test results</desc>
                    </refset>
                    <refset id="55451000146109" moduleId="11000146104">
                        <desc languageCode="en" languageRefsetId="900000000000509007">Dutch radio-allergosorbent test result simple reference set</desc>
                    </refset>
                    <refset id="41000146103" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met optometrische diagnosen</desc>
                    <desc languageCode="en" languageRefsetId="900000000000509007">Dutch optometric diagnoses simple reference set</desc>
                    </refset>
                    <refset id="8721000146106" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met optometrische bezoekredenen</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">Dutch optometric reason for visit simple reference set</desc>
                        </refset>
                    <refset id="231000146105" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met optometrische verrichtingen</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">Dutch optometric procedures simple reference set</desc>
                    </refset>
                    <refset id="11721000146100" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">nationale kernset patiëntproblemen</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing problem simple reference set</desc>
                    </refset>
                    <!--   <refset id="117711000146107" moduleId="11000146104">
      <desc languageCode="nl" languageRefsetId="31000146106">nationale kernset patiëntproblemen inclusief secties ter ordening op basis van de SNOMED-hiërarchie</desc>
      <desc languageCode="en" languageRefsetId="900000000000509007">Simple reference set of Dutch nursing problems with sections of e-transfer</desc>
   </refset>-->
                    <refset id="99051000146107" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met verpleegkundige interventies</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing intervention simple reference set</desc>
                    </refset>
                    <refset id="110861000146100" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met verpleegkundige interventies voor thema pijn</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing interventions for pain simple reference set</desc>
                    </refset>
                    <refset id="110891000146105" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met verpleegkundige interventies voor thema delier</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing intervention for delirium simple reference set</desc>
                    </refset>
                    <refset id="110911000146108" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met verpleegkundige interventies voor thema psychosociale zorg</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing interventions for psychosocial care simple reference set</desc>
                    </refset>
                    <refset id="110881000146108" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met verpleegkundige interventies voor thema risico op vallen</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing interventions for fall risk simple reference set</desc>
                    </refset>
                    <refset id="110901000146106" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met verpleegkundige interventies voor thema risico op suïcide</desc>
                    <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing interventions for suicide simple reference set</desc>
                    </refset>
                    <refset id="140991000146106" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met verpleegkundige observaties</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing observation simple reference set</desc>
                        </refset>
                    <refset id="140961000146101" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met verpleegkundige observaties voor thema risico op delier</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing observations for delirium simple reference set</desc>
                        </refset>
                    <refset id="140971000146107" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met verpleegkundige observaties voor thema risico op suïcide</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing observations for suicide simple reference set</desc>
                        </refset>
                    <refset id="140981000146109" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met verpleegkundige observaties voor thema risico op vallen</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing observations for fall risk simple reference set</desc>
                    </refset>
                    <refset id="110871000146106" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met verpleegkundige interventies voor thema wond</desc>
                    <desc languageCode="en" languageRefsetId="900000000000509007">Dutch nursing interventions for wound simple reference set</desc>
                    </refset>
                    <refset id="148521000146104" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met gecorreleerde 'mapping' van SNOMED CT allergeen naar hulpstof uit G-standaard</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">SNOMED CT allergen to drug excipient map reference set</desc>
                    </refset>
                    <refset id="711112009" moduleId="900000000000012004">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met eenvoudige 'mapping' naar ICNP-diagnosen</desc>
                    <desc languageCode="en" languageRefsetId="900000000000509007">ICNP diagnoses simple map reference set</desc>
                    </refset>
                    <refset id="712505008" moduleId="900000000000012004">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met eenvoudige 'mapping' naar ICNP-interventies</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">ICNP interventions simple map reference set</desc>
                    </refset>
                    <refset id="31451000146105" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met gecorreleerde 'mapping' van patiëntproblemen van SNOMED CT naar NANDA-diagnose</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">SNOMED CT to NANDA correlated map reference set of patient problems</desc>
                    </refset>
                    <refset id="32321000146103" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met uitgebreide gecorreleerde 'mapping' van patiëntproblemen van SNOMED CT naar ICF</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">SNOMED CT to ICF correlated extended map reference set of patient problems</desc>
                    </refset>
                    <refset id="32311000146108" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met uitgebreide gecorreleerde 'mapping' van patiëntproblemen van SNOMED CT naar Omaha System</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">SNOMED CT to Omaha correlated extended map reference set of patient problems</desc>
                    </refset>
                    <refset id="467614008" moduleId="900000000000012004">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met eenvoudige 'mapping' naar GMDN</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">GMDN simple map reference set</desc>
                    </refset>
                    <refset id="447562003" moduleId="900000000000012004">
                        <desc languageCode="nl" languageRefsetId="31000146106">complexe internationale ‘mapping’ naar ICD-10</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">ICD-10 complex map reference set</desc>
                    </refset>
                    <refset id="446608001" moduleId="900000000000012004">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset met eenvoudige 'mapping' naar ICD-O</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">ICD-O simple map reference set</desc>
                    </refset>
                    <!--   <refset id="705112009" moduleId="900000000000012004">
      <desc languageCode="nl" languageRefsetId="31000146106">referentieset met 'mapping' naar LOINC Part-codes</desc>
      <desc languageCode="en" languageRefsetId="900000000000509007">LOINC Part map reference set</desc>
   </refset>-->
                    <refset id="723264001" moduleId="900000000000012004">
                        <desc languageCode="en" languageRefsetId="900000000000509007">Lateralizable body structure reference set</desc>
                        </refset>
                    <refset id="733990004" moduleId="900000000000012004">
                        <desc languageCode="en" languageRefsetId="900000000000509007">Nursing Activities Reference Set</desc>
                    </refset>
                    <refset id="733991000" moduleId="900000000000012004">
                        <desc languageCode="en" languageRefsetId="900000000000509007">Nursing Health Issues Reference Set</desc>
                    </refset>
                    <refset id="721144007" moduleId="900000000000012004">
                        <desc languageCode="en" languageRefsetId="900000000000509007">General dentistry diagnostic reference set</desc>
                        </refset>
                <refset id="721145008" moduleId="900000000000012004">
                        <desc languageCode="en" languageRefsetId="900000000000509007">Odontogram reference set</desc>
                    </refset>
                    <refset id="31000147101" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">DHD Diagnosethesaurus-referentieset</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">DHD Diagnosis thesaurus reference set</desc>
                    </refset>
                    <refset id="41000147108" moduleId="11000146104">
                        <desc languageCode="nl" languageRefsetId="31000146106">DHD Verrichtingenthesaurus-referentieset</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">DHD Procedure thesaurus reference set</desc>
                    </refset>
                    <refset id="450970008" moduleId="900000000000012004">
                        <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor huisartsen</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">General Practice / Family Practice reference set</desc>
                    </refset>
                    <refset id="dhd-icd10-map" moduleId="900000000000012004">
                        <desc languageCode="nl" languageRefsetId="31000146106">door RIVM geautoriseerde complexe nationale ‘mapping’ naar ICD-10 voor diagnosethesaurus</desc>
                        <desc languageCode="en" languageRefsetId="900000000000509007">RIVM authorized national diagnosis thesaurus to ICD10 complex mapping reference set</desc>
                    </refset>
                </refsets>
            </xi:fallback>
        </xi:include>
    </xsl:variable>
    <!-- https://confluence.ihtsdotools.org/display/DOCRELFMT/5.2.5.1+Historical+Association+Reference+Sets -->
    <xsl:variable name="associationList" as="element()?">
        <xi:include href="/db/apps/terminology-data/snomed-data/meta/associationsValues.xml">
            <xi:fallback>
                <associationsValues>
                    <refset id="900000000000524003" moduleId="900000000000012004">
                    <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende geëxporteerde concepten</desc>
                    <desc languageCode="en" languageRefsetId="900000000000509007">MOVED TO association reference set</desc>
                </refset>
                <refset id="900000000000523009" moduleId="900000000000012004">
                    <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende mogelijk equivalente concepten</desc>
                    <desc languageCode="en" languageRefsetId="900000000000509007">POSSIBLY EQUIVALENT TO association reference set</desc>
                </refset>
                <refset id="900000000000526001" moduleId="900000000000012004">
                    <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende vervangende concepten</desc>
                    <desc languageCode="en" languageRefsetId="900000000000509007">REPLACED BY association reference set</desc>
                </refset>
                <refset id="900000000000527005" moduleId="900000000000012004">
                    <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende identieke concepten</desc>
                    <desc languageCode="en" languageRefsetId="900000000000509007">SAME AS association reference set</desc>
                </refset>
                <refset id="900000000000528000" moduleId="900000000000012004">
                    <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende voormalige ouderconcepten</desc>
                    <desc languageCode="en" languageRefsetId="900000000000509007">WAS A association reference set</desc>
                </refset>
                <refset id="900000000000530003" moduleId="900000000000012004">
                    <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende alternatieve concepten</desc>
                    <desc languageCode="en" languageRefsetId="900000000000509007">ALTERNATIVE association reference set</desc>
                </refset>
                <refset id="900000000000525002" moduleId="900000000000012004">
                    <desc languageCode="nl" languageRefsetId="31000146106">referentieset voor associaties betreffende geïmporteerde concepten</desc>
                    <desc languageCode="en" languageRefsetId="900000000000509007">MOVED FROM association reference set</desc>
                </refset>
                <refset id="900000000000489007" moduleId="900000000000012004">
                    <desc languageCode="nl" languageRefsetId="31000146106">referentieset met attribuutwaarden voor reden voor deactivatie van concept</desc>
                    <desc languageCode="en" languageRefsetId="900000000000509007">Concept inactivation indicator reference set</desc>
                </refset>
                <value id="900000000000487009" moduleId="900000000000012004">
                    <desc languageCode="nl" languageRefsetId="31000146106">component verplaatst naar andere module</desc>
                    <desc languageCode="en" languageRefsetId="900000000000509007">Moved elsewhere</desc>
                </value>
                <value id="900000000000484002" moduleId="900000000000012004">
                    <desc languageCode="nl" languageRefsetId="31000146106">ambigu component</desc>
                    <desc languageCode="en" languageRefsetId="900000000000509007">Ambiguous</desc>
                </value>
                <value id="900000000000482003" moduleId="900000000000012004">
                    <desc languageCode="nl" languageRefsetId="31000146106">dubbel component</desc>
                    <desc languageCode="en" languageRefsetId="900000000000509007">Duplicate</desc>
                </value>
                <value id="900000000000483008" moduleId="900000000000012004">
                    <desc languageCode="nl" languageRefsetId="31000146106">obsoleet component</desc>
                    <desc languageCode="en" languageRefsetId="900000000000509007">Outdated</desc>
                </value>
                <value id="723277005" moduleId="900000000000012004">
                    <desc languageCode="en" languageRefsetId="900000000000509007">Nonconformance to editorial policy component</desc>
                </value>
                <value id="900000000000485001" moduleId="900000000000012004">
                    <desc languageCode="nl" languageRefsetId="31000146106">foutief component</desc>
                    <desc languageCode="en" languageRefsetId="900000000000509007">Erroneous</desc>
                </value>
                <value id="900000000000486000" moduleId="900000000000012004">
                    <desc languageCode="nl" languageRefsetId="31000146106">component van beperkte waarde</desc>
                    <desc languageCode="en" languageRefsetId="900000000000509007">Limited</desc>
                </value>
            </associationsValues>
            </xi:fallback>
        </xi:include>
    </xsl:variable>
    <xsl:variable name="languageList">
        <xi:include href="/db/apps/terminology-data/snomed-data/meta/languages.xml">
            <xi:fallback>
                <languages preferred="31000146106">
                    <!-- nl-NL -->
                    <language languageRefsetId="31000146106"/>
                    <!-- nl patient friendly terms -->
                    <language languageRefsetId="15551000146102"/>
                    <!-- nl patient friendly terms b1 -->
                    <language languageRefsetId="160161000146108"/>
                    <!-- en-US -->
                    <language languageRefsetId="900000000000509007"/>
                    <!-- en-GB -->
                    <!--<language languageRefsetId="900000000000508004"/>-->
                </languages>
            </xi:fallback>
        </xi:include>
    </xsl:variable>
    <xsl:template match="/">
        <html>
            <head>
                <!-- Javascript voor tonen/verbergen van elementen
                     toggled is de id van het te tonen/verbergen element
                     toggler de id van het element dat als trigger dient
                -->
                <script type="text/javascript">
               function toggle(toggled, toggler) {
                  if (document.getElementById) {
                     var currentStyle = document.getElementById(toggled).style;
                     var togglerStyle = document.getElementById(toggler).style;
                     if (currentStyle.display == "block") {
                        currentStyle.display = "none";
                        togglerStyle.backgroundImage = "url(/exist/terminology/resources/images/trClosed.gif)";
                     } else {
                        currentStyle.display = "block";
                        togglerStyle.backgroundImage = "url(/exist/terminology/resources/images/triangleOpen.gif)";
                     }
                     return false;
                  } else {
                     return true;
                  }
               }</script>
                <style type="text/css" media="print, screen">
                    body {
                       font-family: Verdana;
                       font-size: 12px;
                    }
                    table {
                       width: 100%;
                       border-spacing: 0px;
                       font-family: Verdana;
                       font-size: 12px;
                    }
                    td {
                       text-align: left;
                       vertical-align: top;
                    }
                    td.parent {
                       text-align: center;
                       vertical-align: top;
                       padding-left: 1em;
                       padding-right: 1em;
                       padding-top: 0em;
                       padding-bottom: 0em;
                    }
                    td.child {
                       text-align: left;
                       vertical-align: top;
                       padding-left: 1em;
                       padding-right: 1em;
                       padding-top: 0.3em;
                       padding-bottom: 0.3em;
                    }
                    td.vertical-line {
                       text-align: center;
                       vertical-align: middle;
                    }
                    td.toggler {
                       background-image: url(/exist/terminology/resources/images/trClosed.gif);
                       background-repeat: no-repeat;
                       padding-left: 15px;
                    }
                    td.toggler:hover {
                       cursor: pointer;
                       /*    	color : #e16e22;*/
                    }
                    td.toggle {
                       display: none;
                    }
                    div.navigate {
                       border-radius: 5px 5px;
                       -moz-border-radius: 5px;
                       -webkit-border-radius: 5px;
                       box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.1);
                       -webkit-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
                       -moz-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
                       background: #ebe7e1;
                       border: 1px solid #e6aa83;
                       padding: 0.5em;
                       width: 80%;
                       margin: auto;
                    }
                    div.navigate-child {
                       border-radius: 5px 5px;
                       -moz-border-radius: 5px;
                       -webkit-border-radius: 5px;
                       box-shadow: 3px 3px 3px rgba(0, 0, 0, 0.1);
                       -webkit-box-shadow: 3px 3px rgba(0, 0, 0, 0.1);
                       -moz-box-shadow: 3px 3px rgba(0, 0, 0, 0.1);
                       background: #ebe7e1;
                       border: 1px solid #e6aa83;
                       padding: 0.2em;
                       padding-left: 1em;
                       width: 50%;
                       margin: auto;
                    }
                    div.concept {
                       border-radius: 5px 5px;
                       -moz-border-radius: 5px;
                       -webkit-border-radius: 5px;
                       box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.1);
                       -webkit-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
                       -moz-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
                       background: #ebe7e1;
                       border: 1px solid #e6aa83;
                       padding: 0.5em;
                       margin: auto;
                    }
                    div.refset {
                       border-radius: 5px 5px;
                       -moz-border-radius: 5px;
                       -webkit-border-radius: 5px;
                       background: #e3dfd9;
                       border: 1px solid #bbb;
                       padding: 0.2em;
                       padding-left: 0.5em;
                       margin-left: 1em;
                       margin-top: 0.5em;
                       margin-bottom: 0.4em;
                    }
                    div.refsetGroup {
                       border-radius: 5px 5px;
                       -moz-border-radius: 5px;
                       -webkit-border-radius: 5px;
                       background: #ebe7e1;
                       border: 1px solid #bbb;
                       padding-top: 0.2em;
                       padding-left: 0.4em;
                       padding-bottom: 0.2em;
                       padding-right: 0.4em;
                       margin-top: 0.2em;
                       margin-bottom: 0.4em;
                    }
                    div.map-rule:first-child {
                       background: #e3dfd9;
                       margin-top: 0.2em;
                       margin-bottom: 0.2em;
                       margin-left: 0.2em;
                       margin-right: 0.2em;
                       padding-left: 0.5em;
                    }
                    div.map-rule {
                       background: #e3dfd9;
                       margin-top: 0.8em;
                       margin-bottom: 0.2em;
                       margin-left: 0.2em;
                       margin-right: 0.2em;
                       padding-left: 0.5em;
                    }
                    div.grp {
                       border-radius: 5px 5px;
                       -moz-border-radius: 5px;
                       -webkit-border-radius: 5px;
                       background: #e3dfd9;
                       border: 1px solid #e6aa83;
                       padding: 0.2em;
                       padding-left: 0.5em;
                       margin-left: 1em;
                       margin-bottom: 0.5em;
                    }
                    div.syn {
                       border-radius: 5px 5px;
                       -moz-border-radius: 5px;
                       -webkit-border-radius: 5px;
                       background: #dad6d0;
                       padding: 0.2em;
                       padding-left: 0.5em;
                       margin-top: 0.5em;
                       margin-bottom: 0.5em;
                    }
                    span.subcount {
                       border-radius: 5px 5px;
                       -moz-border-radius: 5px;
                       -webkit-border-radius: 5px;
                       background: #dad6d0;
                       margin-right: 0.2em;
                       padding-left: 0.2em;
                       padding-right: 0.2em;
                       float: right;
                    }
                    td.normal {
                       text-align: left;
                       vertical-align: top;
                       padding-left: 15px;
                       display: block;
                    }
                    td.indent {
                       text-align: left;
                       vertical-align: top;
                       padding-left: 15px;
                    }</style>
            </head>
            <body>
                <xsl:apply-templates/>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="concept">
        <xsl:variable name="descriptions">
            <xsl:for-each select="desc">
                <xsl:copy-of select="."/>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="definitions">
            <xsl:for-each select="definition">
                <xsl:copy-of select="."/>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="annotations">
            <xsl:for-each select="annotation">
                <xsl:copy-of select="."/>
            </xsl:for-each>
        </xsl:variable>
        <table>
            <!-- parents -->
            <tr>
                <td>
                    <table>
                        <tr>
                            <xsl:for-each select="grp[@grp = '0']/src[@typeId = '116680003'][@active = '1']">
                                <td class="parent">
                                    <a href="{@destinationId}">
                                        <div class="navigate">
                                            <xsl:value-of select="."/>
                                        </div>
                                    </a>
                                </td>
                            </xsl:for-each>
                        </tr>
                        <tr>
                            <xsl:for-each select="grp[@grp = '0']/src[@typeId = '116680003'][@active = '1']">
                                <td class="vertical-line">
                                    <xsl:text>|</xsl:text>
                                </td>
                            </xsl:for-each>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- focus concept -->
            <tr>
                <td>
                    <div class="concept">
                        <table>
                            <tr>
                                <!-- name, synonyms -->
                                <td width="40%">
                                    <table width="100%">
                                        <!-- FSN -->
                                        <tr>
                                            <td colspan="2">
                                                <xsl:choose>
                                                    <xsl:when test="@conceptId">
                                                        <xsl:choose>
                                                            <xsl:when test="@active">
                                                                <span style="font-size:110%;font-weight:bold;">
                                                                    <xsl:choose>
                                                                        <xsl:when test="desc[@active = '1'][@type = 'fsn'][@languageRefsetId = $languageList/languages/@preferred]">
                                                                            <xsl:value-of select="desc[@active = '1'][@type = 'fsn'][@languageRefsetId = $languageList/languages/@preferred]"/>
                                                                        </xsl:when>
                                                                        <xsl:otherwise>
                                                                            <xsl:value-of select="desc[@active = '1'][@type = 'fsn'][@languageCode = 'en']"/>
                                                                        </xsl:otherwise>
                                                                    </xsl:choose>
                                                                </span>
                                                                </xsl:when>
                                                        <xsl:otherwise>
                                                                <span style="font-size:110%;font-weight:bold;color:red;">
                                                                    <xsl:choose>
                                                                        <xsl:when test="desc[@active = '1'][@type = 'fsn'][@languageRefsetId = $languageList/languages/@preferred]">
                                                                            <xsl:value-of select="desc[@active = '1'][@type = 'fsn'][@languageRefsetId = $languageList/languages/@preferred]"/>
                                                                        </xsl:when>
                                                                        <xsl:otherwise>
                                                                            <xsl:value-of select="desc[@active = '1'][@type = 'fsn'][@languageCode = 'en']"/>
                                                                        </xsl:otherwise>
                                                                    </xsl:choose>
                                                                    <xsl:text> DEPRECATED</xsl:text>
                                                                </span>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <span style="font-size:110%;font-weight:bold;color:red;">
                                                            <xsl:choose>
                                                                <xsl:when test="$xslt.language = 'nl-NL'">
                                                                    <xsl:text> ID niet gevonden</xsl:text>
                                                                </xsl:when>
                                                                <xsl:otherwise>
                                                                    <xsl:text> ID not found</xsl:text>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                            </span>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </td>
                                        </tr>
                                        <!-- Descriptions -->
                                        <xsl:for-each select="$languageList//language">
                                            <xsl:variable name="refsetId" select="@languageRefsetId"/>
                                            <xsl:if test="$descriptions/desc[@languageRefsetId = $refsetId][@active = '1'][@type = 'pref']">
                                                <tr>
                                                    <td style="vertical-align:middle;" width="5%">
                                                        <div>
                                                            <img src="/exist/terminology/resources/images/languageRefsets/{$refsetId}.png"/>
                                                        </div>
                                                    </td>
                                                    <td style="font-size:110%;vertical-align:middle;">
                                                        <div title="{if($xslt.language ='nl-NL') then 'Voorkeursterm' else 'Preferred term'}">
                                                            <xsl:value-of select="$descriptions/desc[@languageRefsetId = $refsetId][@active = '1'][@type = 'pref']"/>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </xsl:if>
                                            <!-- synonyms -->
                                            <xsl:if test="$descriptions/desc[@languageRefsetId = $refsetId][@active = '1'][@type = 'syn']">
                                                <tr>
                                                    <td style="vertical-align:middle;" width="5%">
                                                        <div/>
                                                    </td>
                                                    <td style="font-size:110%;vertical-align:middle;">
                                                        <div class="syn" title="{if($xslt.language ='nl-NL') then 'Synoniemen' else 'Synonyms'}">
                                                            <xsl:for-each select="$descriptions/desc[@languageRefsetId = $refsetId][@active = '1'][@type = 'syn']">
                                                                <xsl:value-of select="."/>
                                                                <br/>
                                                            </xsl:for-each>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </xsl:if>
                                            <!-- definition -->
                                            <xsl:if test="$definitions/definition[@active = '1'][@languageRefsetId = $refsetId]">
                                                <tr>
                                                    <td style="vertical-align:middle;" width="5%">
                                                        <xsl:choose>
                                                            <xsl:when test="$descriptions/desc[@languageRefsetId = $refsetId][@active = '1'][@type = 'pref']">
                                                                <div/>
                                                            </xsl:when>
                                                            <xsl:otherwise>
                                                                <img src="/exist/terminology/resources/images/languageRefsets/{$refsetId}.png"/>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                    </td>
                                                    <td style="font-size:110%;vertical-align:middle;">
                                                        <div class="syn" title="{if($xslt.language ='nl-NL') then 'Tekstdefinitie' else 'Text definition'}">
                                                            <i>
                                                                <xsl:value-of select="$definitions/definition[@active = '1'][@languageRefsetId = $refsetId]"/>
                                                            </i>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </xsl:if>
                                        <!-- special case for NL patient friendly (B1) terms in annotation -->
                                            <xsl:if test="$refsetId='160161000146108' and $annotations/annotation[@typeId='480411000146103']">
                                                <tr>
                                                    <td style="vertical-align:middle;" width="5%">
                                                        <img src="/exist/terminology/resources/images/languageRefsets/{$refsetId}.png"/>
                                                    </td>
                                                    <td style="font-size:110%;vertical-align:middle;">
                                                        <div class="syn" title="{if($xslt.language ='nl-NL') then 'Tekstdefinitie' else 'Text definition'}">
                                                            <i>
                                                                <xsl:value-of select="$annotations/annotation[@active = '1'][@typeId='480411000146103']"/>
                                                            </i>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </xsl:if>
                                        </xsl:for-each>
                                    </table>
                                </td>
                                <td width="60%">
                                    <!-- id, status -->
                                    <div class="grp">
                                        <table>
                                            <tr>
                                                <td width="40%">
                                                    <xsl:value-of select="'Id'"/>
                                                </td>
                                                <td>
                                                    <xsl:value-of select="@conceptId"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="40%">
                                                    <xsl:value-of select="'Status'"/>
                                                </td>
                                                <td>
                                                    <xsl:value-of select="@definitionStatus"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!-- group -->
                                    <xsl:for-each select="grp[@grp = '0'][src[@active = '1']/@typeId != '116680003']">
                                        <div class="grp">
                                            <table>
                                                <xsl:for-each select="src[@typeId != '116680003'][@active = '1']">
                                                    <tr>
                                                        <td width="40%">
                                                            <xsl:value-of select="@type"/>
                                                        </td>
                                                        <td>
                                                            <a href="{@destinationId}">
                                                                <xsl:value-of select="."/>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </xsl:for-each>
                                            </table>
                                        </div>
                                    </xsl:for-each>
                                    <!-- group -->
                                    <xsl:for-each select="grp[@grp != '0'][src/@active]">
                                        <div class="grp">
                                            <table>
                                                <xsl:for-each select="src[@active = '1']">
                                                    <tr>
                                                        <td width="40%">
                                                            <xsl:value-of select="@type"/>
                                                        </td>
                                                        <td>
                                                            <a href="{@destinationId}">
                                                                <xsl:value-of select="."/>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </xsl:for-each>
                                            </table>
                                        </div>
                                    </xsl:for-each>
                                    <!-- attribueValue | associations -->
                                    <!-- The refsets MOVE TO (900000000000524003) and MOVED FROM (900000000000525002) refer to a module that a concept was moved into or out of -->
                                    <!-- REFERS TO is for descriptions and may be ignored (900000000000531004) -->
                                    <!-- SIMILAR TO is no longer in use (900000000000529008) -->
                                    <xsl:for-each-group select="attributeValue[@active][not(@refsetId = '900000000000531004')], association[@active][not(@refsetId = '900000000000531004')]" group-by="@refsetId">
                                        <xsl:variable name="refsetName">
                                            <xsl:choose>
                                                <xsl:when test="($associationList//refset[@id = current-grouping-key()], $refsetList//refset[@id = current-grouping-key()], @refset)/desc[@languageRefsetId = $languageRefsetId]">
                                                    <xsl:value-of select="($associationList//refset[@id = current-grouping-key()], $refsetList//refset[@id = current-grouping-key()], @refset)/desc[@languageRefsetId = $languageRefsetId]"/>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:value-of select="($associationList//refset[@id = current-grouping-key()], $refsetList//refset[@id = current-grouping-key()], @refset)/desc[@languageRefsetId = $fallbackLanguageRefsetId]"/>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:variable>
                                        <div class="refset">
                                            <table>
                                                <xsl:choose>
                                                    <!-- simple maps -->
                                                    <xsl:when test="count(current-group()) = 1">
                                                        <tr>
                                                            <td width="40%">
                                                                <xsl:value-of select="$refsetName"/>
                                                            </td>
                                                            <td>
                                                                <xsl:for-each select="current-group()">
                                                                    <xsl:variable name="effectiveDate">
                                                                        <xsl:choose>
                                                                            <xsl:when test="matches(@effectiveTime, '^\d{8}$')">
                                                                                <xsl:value-of select="concat(' (', replace(@effectiveTime, '^(\d{4})(\d{2})(\d{2})$', '$1-$2-$3'), ')')"/>
                                                                            </xsl:when>
                                                                            <xsl:otherwise>
                                                                                <xsl:value-of select="concat(' (', @effectiveTime, ')')"/>
                                                                            </xsl:otherwise>
                                                                        </xsl:choose>
                                                                    </xsl:variable>
                                                                    <xsl:variable name="targetText">
                                                                        <xsl:value-of select="self::attributeValue/@valueId | self::association/@targetComponentId"/>
                                                                        <xsl:text> | </xsl:text>
                                                                        <xsl:value-of select="self::attributeValue/@value | self::association/@targetComponent"/>
                                                                        <xsl:text> |</xsl:text>
                                                                    </xsl:variable>
                                                                    <xsl:choose>
                                                                        <xsl:when test="@refsetId = '467614008'">
                                                                            <!-- empty -->
                                                                        </xsl:when>
                                                                        <xsl:when test="@refsetId = '900000000000524003' or @refsetId = '900000000000525002'">
                                                                            <!-- Cannot link to a module -->
                                                                            <xsl:text>Module: </xsl:text>
                                                                            <xsl:copy-of select="$targetText"/>
                                                                        </xsl:when>
                                                                        <xsl:otherwise>
                                                                            <a href="{@targetComponentId}">
                                                                                <xsl:copy-of select="$targetText"/>
                                                                            </a>
                                                                            <xsl:value-of select="$effectiveDate"/>
                                                                        </xsl:otherwise>
                                                                    </xsl:choose>
                                                                </xsl:for-each>
                                                            </td>
                                                        </tr>
                                                    </xsl:when>
                                                    <!-- multiple in same refset -->
                                                    <xsl:when test="count(current-group()) gt 1">
                                                        <xsl:variable name="id" select="generate-id()"/>
                                                        <xsl:variable name="id-toggler" select="concat($id, '-toggler')"/>
                                                        <tr>
                                                            <td colspan="2" class="toggler" id="{$id-toggler}" onclick="{concat('return toggle(&#34;',$id,'&#34;,&#34;',$id-toggler,'&#34;)')}">
                                                                <xsl:value-of select="$refsetName"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="toggle" id="{$id}">
                                                                <table>
                                                                    <xsl:for-each select="current-group()">
                                                                        <xsl:variable name="effectiveDate">
                                                                            <xsl:choose>
                                                                                <xsl:when test="matches(@effectiveTime, '^\d{8}$')">
                                                                                    <xsl:value-of select="concat(' (', replace(@effectiveTime, '^(\d{4})(\d{2})(\d{2})$', '$1-$2-$3'), ')')"/>
                                                                                </xsl:when>
                                                                                <xsl:otherwise>
                                                                                    <xsl:value-of select="concat(' (', @effectiveTime, ')')"/>
                                                                                </xsl:otherwise>
                                                                            </xsl:choose>
                                                                        </xsl:variable>
                                                                        <xsl:choose>
                                                                            <xsl:when test="@refsetId = '467614008'">
                                                                                <!-- empty -->
                                                                            </xsl:when>
                                                                            <xsl:otherwise>
                                                                                <tr>
                                                                                    <td>
                                                                                        <a href="{@targetComponentId}">
                                                                                            <xsl:value-of select="@targetComponentId"/>
                                                                                            <xsl:text> | </xsl:text>
                                                                                            <xsl:value-of select="@targetComponent"/>
                                                                                            <xsl:text> |</xsl:text>
                                                                                        </a>
                                                                                        <xsl:value-of select="$effectiveDate"/>
                                                                                    </td>
                                                                                </tr>
                                                                            </xsl:otherwise>
                                                                        </xsl:choose>
                                                                    </xsl:for-each>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </xsl:when>
                                                </xsl:choose>
                                            </table>
                                        </div>
                                    </xsl:for-each-group>
                                    <!-- simple refsets -->
                                    <xsl:for-each select="refsets/refset[@refsetId = $refsetList//refset/@id][@active = '1']">
                                        <xsl:variable name="simpleRefsetId" select="@refsetId"/>
                                        <div class="refset">
                                            <table>
                                                <tr>
                                                    <td colspan="2">
                                                        <xsl:choose>
                                                            <xsl:when test="$refsetList//refset[@id = $simpleRefsetId]/desc[@languageRefsetId=$languageRefsetId]">
                                                                <xsl:value-of select="$refsetList//refset[@id = $simpleRefsetId]/desc[@languageRefsetId=$languageRefsetId]"/>
                                                            </xsl:when>
                                                            <xsl:otherwise>
                                                                <xsl:value-of select="$refsetList//refset[@id = $simpleRefsetId]/desc[@languageCode = 'en']"/>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </xsl:for-each>
                                    <!-- maps -->
                                    <xsl:for-each-group select="maps/map[@refsetId = $refsetList//refset/@id]" group-by="@refsetId">
                                        <xsl:variable name="refsetId" select="@refsetId"/>
                                        <div class="refset">
                                            <table>
                                                <xsl:choose>
                                                    <!-- simple maps -->
                                                    <xsl:when test="count(current-group()) = 1 and not(current-group()[1]/@correlation) and not(current-group()[1]/@snomedCtSourceCodeToTargetMapCodeCorrelationValue) and not(current-group()[1]/@correlationId)">
                                                        <tr>
                                                            <td width="40%">
                                                                <xsl:choose>
                                                                    <xsl:when test="$refsetList//refset[@id = $refsetId]/desc[@languageRefsetId=$languageRefsetId]">
                                                                        <xsl:value-of select="$refsetList//refset[@id = $refsetId]/desc[@languageRefsetId=$languageRefsetId]"/>
                                                                    </xsl:when>
                                                                    <xsl:otherwise>
                                                                        <xsl:value-of select="$refsetList//refset[@id = $refsetId]/desc[@languageCode = 'en']"/>
                                                                    </xsl:otherwise>
                                                                </xsl:choose>
                                                            </td>
                                                            <td>
                                                                <xsl:choose>
                                                                    <xsl:when test="@refsetId = '467614008'">
                                                                        <!-- empty -->
                                                                    </xsl:when>
                                                                    <xsl:otherwise>
                                                                        <xsl:value-of select="@mapTarget"/>
                                                                    </xsl:otherwise>
                                                                </xsl:choose>
                                                            </td>
                                                        </tr>
                                                    </xsl:when>
                                                    <!-- complex maps, extended maps and correlated maps  -->
                                                    <xsl:otherwise>
                                                        <xsl:variable name="id" select="generate-id()"/>
                                                        <xsl:variable name="id-toggler" select="concat($id, '-toggler')"/>
                                                        <tr>
                                                            <td colspan="2" class="toggler" id="{$id-toggler}" onclick="{concat('return toggle(&#34;',$id,'&#34;,&#34;',$id-toggler,'&#34;)')}">
                                                                <xsl:choose>
                                                                    <xsl:when test="$refsetList//refset[@id = $refsetId]/desc[@languageRefsetId=$languageRefsetId]">
                                                                        <xsl:value-of select="$refsetList//refset[@id = $refsetId]/desc[@languageRefsetId=$languageRefsetId]"/>
                                                                    </xsl:when>
                                                                    <xsl:otherwise>
                                                                        <xsl:value-of select="$refsetList//refset[@id = $refsetId]/desc[@languageCode = 'en']"/>
                                                                    </xsl:otherwise>
                                                                </xsl:choose>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="toggle" id="{$id}">
                                                                <!-- extended maps with groups -->
                                                                <xsl:choose>
                                                                    <xsl:when test="current-group()[1]/@correlation">
                                                                        <xsl:for-each-group select="current-group()" group-by="@mapGroup">
                                                                            <xsl:sort select="current-grouping-key()"/>
                                                                            <div class="refsetGroup">
                                                                                <xsl:for-each select="current-group()">
                                                                                    <xsl:sort select="@mapPriority"/>
                                                                                    <div class="map-rule">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td width="20%">Target</td>
                                                                                                <td>
                                                                                                    <xsl:choose>
                                                                                                        <xsl:when test="@refsetId = '447562003'">
                                                                                                            <xsl:choose>
                                                                                                                <xsl:when test="$xslt.language = 'nl-NL'">
                                                                                                                    <a target="_blank" href="https://terminologie.nictiz.nl/art-decor/claml?collection=icd10-nl-data&amp;conceptId={@mapTarget}">
                                                                                                                        <xsl:value-of select="@mapTarget"/>
                                                                                                                    </a>
                                                                                                                </xsl:when>
                                                                                                                <xsl:otherwise>
                                                                                                                    <a target="_blank" href="https://terminologie.nictiz.nl/art-decor/claml?collection=icd10-us-data&amp;conceptId={@mapTarget}">
                                                                                                                        <xsl:value-of select="@mapTarget"/>
                                                                                                                    </a>
                                                                                                                </xsl:otherwise>
                                                                                                            </xsl:choose>
                                                                                                        </xsl:when>
                                                                                                        <xsl:when test="@refsetId = 'dhd-icd10-map'">
                                                                                                                    <a target="_blank" href="https://terminologie.nictiz.nl/art-decor/claml?collection=icd10-nl-data&amp;conceptId={@mapTarget}">
                                                                                                                        <xsl:value-of select="@mapTarget"/>
                                                                                                                    </a>
                                                                                                        </xsl:when>
                                                                                                        <xsl:otherwise>
                                                                                                            <xsl:value-of select="@mapTarget"/>
                                                                                                        </xsl:otherwise>
                                                                                                    </xsl:choose>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <xsl:if test="string-length(@mapRule) &gt; 0">
                                                                                                <tr>
                                                                                                    <td width="20%">Rule</td>
                                                                                                    <td>
                                                                                                        <xsl:value-of select="@mapRule"/>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </xsl:if>
                                                                                            <xsl:if test="string-length(@mapAdvice) &gt; 0">
                                                                                                <tr>
                                                                                                    <td width="20%">Advice</td>
                                                                                                    <td>
                                                                                                        <xsl:value-of select="@mapAdvice"/>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </xsl:if>
                                                                                            <xsl:if test="string-length(@correlation) &gt; 0">
                                                                                                <tr>
                                                                                                    <td width="20%">Correlation</td>
                                                                                                    <td>
                                                                                                        <xsl:value-of select="@correlation"/>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </xsl:if>
                                                                                            <xsl:if test="string-length(@icd10Term) &gt; 0">
                                                                                                <tr>
                                                                                                    <td width="20%">Term</td>
                                                                                                    <td>
                                                                                                        <xsl:value-of select="@icd10Term"/>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </xsl:if>
                                                                                        </table>
                                                                                    </div>
                                                                                </xsl:for-each>
                                                                            </div>
                                                                        </xsl:for-each-group>
                                                                    </xsl:when>
                                                                    <!-- correlated maps -->
                                                                    <xsl:otherwise>
                                                                        <div class="refsetGroup">
                                                                            <xsl:for-each select="current-group()">
                                                                            <div class="map-rule">
                                                                                <table>
                                                                                    <tr>
                                                                                        <td width="20%">Target</td>
                                                                                        <td>
                                                                                            <xsl:value-of select="@mapTarget"/>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <xsl:if test="string-length(@mapAttribute) &gt; 0">
                                                                                        <tr>
                                                                                            <td width="20%">Attribute</td>
                                                                                            <td>
                                                                                                <xsl:value-of select="@mapAttribute"/>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </xsl:if>
                                                                                    <xsl:if test="string-length(@mapOrigin) &gt; 0">
                                                                                        <tr>
                                                                                            <td width="20%">Origin</td>
                                                                                            <td>
                                                                                                <xsl:value-of select="@mapOrigin"/>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </xsl:if>
                                                                                    <xsl:if test="string-length(@mapCorrelation) &gt; 0">
                                                                                        <tr>
                                                                                            <td width="20%">Correlation</td>
                                                                                            <td>
                                                                                                <xsl:value-of select="@mapCorrelation"/>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </xsl:if>
                                                                                    <xsl:if test="string-length(@targetQualifier) &gt; 0">
                                                                                        <tr>
                                                                                            <td width="20%">Target Qualifier</td>
                                                                                            <td>
                                                                                                <xsl:value-of select="@targetQualifier"/>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </xsl:if>
                                                                                </table>
                                                                            </div>
                                                                            </xsl:for-each>
                                                                        </div>
                                                                    </xsl:otherwise>
                                                                </xsl:choose>
                                                            </td>
                                                        </tr>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </table>
                                        </div>
                                    </xsl:for-each-group>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <xsl:if test="dest[@typeId = '116680003'][@active = '1']">
                <tr>
                    <td class="vertical-line">|</td>
                </tr>
            </xsl:if>
            <!-- children -->
            <tr>
                <td>
                    <tr>
                        <xsl:for-each select="dest[@typeId = '116680003'][@active = '1']">
                            <tr>
                                <td class="child">
                                    <div class="navigate-child">
                                        <a href="{@sourceId}">
                                            <xsl:value-of select="."/>
                                        </a>
                                        <xsl:if test="@subCount &gt; 0">
                                            <span class="subcount">
                                                <xsl:value-of select="@subCount"/>
                                            </span>
                                        </xsl:if>
                                    </div>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </tr>
                </td>
            </tr>
        </table>
    </xsl:template>
    <xsl:template name="doDescType">
        <xsl:param name="type"/>
        <span style="text-align:center;background-color:#ebe7e1;padding-right:0.4em;padding-left:0.4em;margin-right:0.5em; border: 1px solid gainsboro; border-bottom-left-radius: 5px; border-bottom-right-radius: 5px; border-collapse: separate; border-top-left-radius: 5px; border-top-right-radius: 5px;" title="F = Fully Specified Name, P = Preferred, S = Synonym">
            <xsl:choose>
                <xsl:when test="$type = 'pref'">P</xsl:when>
                <xsl:when test="$type = 'fsn'">F</xsl:when>
                <xsl:otherwise>S</xsl:otherwise>
            </xsl:choose>
        </span>
    </xsl:template>
</xsl:stylesheet>