xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace snomed              = "http://art-decor.org/ns/terminology/snomed" at "../api/api-snomed.xqm";

let $conceptId  := request:get-parameter('id','138875005')
(:let $refsetId   := request:get-parameter('refsetId',()):)

(:let $conceptId := '138875005':)
(:let $conceptId := '38102005':)

return
<subConcepts>{snomed:getSubConcepts($conceptId)}</subConcepts>