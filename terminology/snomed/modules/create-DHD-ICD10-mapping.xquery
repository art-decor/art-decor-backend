xquery version "3.0";
(:
	Copyright (C) 2011-2016 Art-Decor Expert Group
	
	Author: Gerrit Boers
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";

let $rows := collection(concat($get:strTerminologyData,'/nictiz-demo-data/import'))//row
let $mappings:=
   for $map in $rows
   return
 <map conceptId="{$map/SCTID}" effectiveTime="20230331" active="1" refsetId="dhd-icd10-map" refset="door RIVM geautoriseerde complexe nationale ‘mapping’ naar ICD-10 voor diagnosethesaurus" mapTarget="{$map/ICD10-code}" mapGroup="1" correlation="" mapPriority="1" icd10Term="{$map/ICD-10-beschrijving}"/>
return
<dhd-icd10-mapping>{$mappings}</dhd-icd10-mapping>