xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace snomed              = "http://art-decor.org/ns/terminology/snomed" at "../api/api-snomed.xqm";

let $conceptId      := request:get-parameter('id',())
let $refsetId       := request:get-parameter('refsetId',())

(:let $conceptId := '251000146104'
let $refsetId :='231000146105':)
(:let $refsetEffectiveDate :='2012-12-03':)
(:let $conceptId := '38102005':)

let $concept := $snomed:colDataBase//concept[@conceptId=$conceptId]

return
<concepts>
{
        <concept>
        {
            $concept[1]/@*,
            $concept[1]/*[not(name()='maps')],
            <simpleMaps>
            {
                for $map in $concept[1]/maps/map[empty(@mapGroup)]
                return $map
            }
            </simpleMaps>
            ,
            <complexMaps>
            {
                for $map in $concept[1]/maps/map[@mapGroup]
                let $mapping := $map/@refsetId 
                group by $mapping
                return
                    <map refsetId="{$mapping}" refset="{$map[1]/@refset}">
                    {
                        for $group in $map
                        let $grp := $group/@mapGroup
                        group by $grp
                        order by $grp
                        return
                            <group>
                            {
                                for $item in $group
                                order by $item/@mapPriority
                                return $item
                            }
                            </group>
                    }
                    </map>
            }
            </complexMaps>
        }
        </concept>
}
</concepts>