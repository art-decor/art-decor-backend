xquery version "1.0";
(:
    Copyright (C) 2011-2015 ART-DECOR Expert Group (art-decor.org)
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art ="http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";
import module namespace snomed              = "http://art-decor.org/ns/terminology/snomed" at "../api/api-snomed.xqm";
(:declare option exist:serialize "method=text media-type=text/xml charset=utf-8";:)
(:let $refsetId:= request:get-parameter('id',''):)
let $refsetId:= '117711000146107'
(:let $language :=request:get-parameter('language','nl'):)
let $language := 'nl'
let $languageRefset :=
      if ($language='nl') then
         '31000146106'
      else
         '900000000000509007'

let $fallbackLanguageRefset := '900000000000509007'

let $refset          := collection(concat($get:strTerminologyData,'/snomed-data/en-GB'))//concept[.//@refsetId=$refsetId]
let $refsetConcept   := collection(concat($get:strTerminologyData,'/snomed-data/en-GB'))//concept[@conceptId=$refsetId]
let $refsetIdList    := collection(concat($get:strTerminologyData,'/snomed-data/en-GB'))//concept[.//@refsetId=$refsetId]/@conceptId

let $level1 :=
   for $concept in $refset[refsets/refset[@refsetId=$refsetId][@active='1']][not(ancestors/id=$refsetIdList)]
   return
   $concept


let $orderedList := 
   for $concept in $level1
   let $subConcepts := $refset[ancestors/id=$concept/@conceptId]
   return
      <concept conceptId="{$concept/@conceptId}">
      {
      if ($concept/desc[@type='pref'][@active='1'][@languageRefsetId=$languageRefset]) then
         $concept/desc[@type='pref'][@active='1'][@languageRefsetId=$languageRefset]
      else
         $concept/desc[@type='pref'][@active='1'][@languageRefsetId=$fallbackLanguageRefset]
      ,
      if ($concept/desc[@type='fsn'][@active='1'][@languageRefsetId=$languageRefset]) then
         $concept/desc[@type='fsn'][@active='1'][@languageRefsetId=$languageRefset]
      else
         $concept/desc[@type='fsn'][@active='1'][@languageRefsetId=$fallbackLanguageRefset]
         ,
      if ($concept/definition[@active='1'][@languageRefsetId=$languageRefset]) then
         $concept/definition[@active='1'][@languageRefsetId=$languageRefset]
      else
         $concept/definition[@active='1'][@languageRefsetId=$fallbackLanguageRefset]
      ,
      for $subConcept in $subConcepts
      return
      
      <concept conceptId="{$subConcept/@conceptId}">
      {
      if ($subConcept/desc[@type='pref'][@active='1'][@languageRefsetId=$languageRefset]) then
         $subConcept/desc[@type='pref'][@active='1'][@languageRefsetId=$languageRefset]
      else
         $subConcept/desc[@type='pref'][@active='1'][@languageRefsetId=$fallbackLanguageRefset]
      ,
      if ($subConcept/desc[@type='fsn'][@active='1'][@languageRefsetId=$languageRefset]) then
         $subConcept/desc[@type='fsn'][@active='1'][@languageRefsetId=$languageRefset]
      else
         $subConcept/desc[@type='fsn'][@active='1'][@languageRefsetId=$fallbackLanguageRefset]
         ,
      if ($subConcept/definition[@active='1'][@languageRefsetId=$languageRefset]) then
         $subConcept/definition[@active='1'][@languageRefsetId=$languageRefset]
      else
         $subConcept/definition[@active='1'][@languageRefsetId=$fallbackLanguageRefset]
       }
      </concept>
      }
   </concept>

return
<refset refsetId="{$refsetId}" pref="{$refsetConcept/desc[@languageCode=$language][@type='pref'][@active='1']}">
   {
   for $concept1 in $orderedList[concept]
   order by lower-case($concept1/desc[@type='pref'])
   return
   <concept>
      {
      $concept1/@*,
      $concept1/desc,
      for $subconcept in $concept1/concept
      order by lower-case($subconcept/desc[@type='pref'])
      return
      $subconcept
      }
   </concept>,
   for $concept2 in $orderedList[not(concept)]
   order by lower-case($concept2/desc[@type='pref'])
   return
   $concept2
   
   }
</refset>
