xquery version "1.0";
(:
    Copyright (C) 2011-2015 ART-DECOR Expert Group (art-decor.org)
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
    
    mapping   International Classification of Primary Care, Second edition complex map reference set
    
    
    
             
      <refset id="146481000146103" moduleId="11000146104">Dutch obstetric procedures simple reference set</refset>
      <refset id="110851000146103" moduleId="11000146104">PALGA thesaurus simple reference set for pathology</refset>
      <refset id="2551000146109" moduleId="11000146104">Dutch rare neuromuscular disorders simple reference set</refset>
      <refset id="46231000146109" moduleId="11000146104">Netherlands ordinal test result simple reference set</refset>
      <refset id="97801000146108" moduleId="11000146104">Netherlands microscopic ordinal test result simple reference set</refset>
      <refset id="140301000146101" moduleId="11000146104">Dutch simple reference set for ordinal antimicrobial susceptibility test results</refset>
      <refset id="55451000146109" moduleId="11000146104">Dutch radio-allergosorbent test result simple reference set</refset>
      <refset id="41000146103" moduleId="11000146104">Dutch optometric diagnoses simple reference set</refset>
      <refset id="8721000146106" moduleId="11000146104">Dutch optometric reason for visit simple reference set</refset>
      <refset id="231000146105" moduleId="11000146104">Dutch optometric procedures simple reference set</refset>
      <refset id="117711000146107" moduleId="11000146104">Simple reference set of Dutch nursing problems with sections of e-transfer</refset>
      <refset id="148521000146104" moduleId="11000146104">SNOMED CT allergen to drug excipient map reference set</refset>
      <refset id="711112009" moduleId="900000000000012004">ICNP diagnoses simple map reference set</refset>
      <refset id="712505008" moduleId="900000000000012004">ICNP interventions simple map reference set</refset>
      <refset id="31451000146105" moduleId="11000146104">SNOMED CT to NANDA correlated map reference set</refset>
      <refset id="32321000146103" moduleId="11000146104">SNOMED CT to ICF correlated extended map reference set</refset>
      <refset id="32311000146108" moduleId="11000146104">SNOMED CT to Omaha correlated extended map reference set</refset>
      <refset id="467614008" moduleId="900000000000012004">GMDN simple map reference set</refset>
      <refset id="705112009" moduleId="900000000000012004">LOINC Part map reference set</refset>
      <refset id="723264001" moduleId="900000000000012004">Lateralisable body structure reference set</refset>
      <refset id="721144007" moduleId="900000000000012004">General dentistry diagnostic reference set</refset>
      <refset id="721145008" moduleId="900000000000012004">Odontogram reference set</refset>
      <refset id="31000147101" moduleId="11000146104">DHD Diagnosis thesaurus reference set</refset>
      <refset id="41000147108" moduleId="11000146104">DHD Procedure thesaurus reference set</refset>
    
:)
(:
    Creates list of refsets for use by search filter.
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";

let $refsets := distinct-values(collection(concat($get:strTerminologyData,'/snomed-data/en-GB'))//refset/@refsetId)
let $maps := distinct-values(collection(concat($get:strTerminologyData,'/snomed-data/en-GB'))//map/@refsetId)
let $concepts:= collection(concat($get:strTerminologyData,'/snomed-data/en-GB'))//concept
let $preferredLanguageRefsetId := '31000146106'
let $fallbackLanguageRefsetId :='900000000000509007'

let $groupedList :=
   <refsets>
      <refset id="98051000146103" moduleId="11000146104">Dutch contact allergen simple reference set</refset>
      <refset id="98011000146102" moduleId="11000146104">Dutch food allergen simple reference set</refset>
      <refset id="98021000146107" moduleId="11000146104">Dutch inhalation allergen simple reference set</refset>
      <refset id="98031000146109" moduleId="11000146104">Dutch insect venom allergen simple reference set</refset>
      <refset id="42931000146101" moduleId="11000146104">Dutch non-drug allergen simple reference set for Dutch clinical building blocks version 2017</refset>
      <refset id="98041000146101" moduleId="11000146104">Dutch occupational allergen simple reference set</refset>
      <refset id="260181000146102" moduleId="11000146104">Dutch simple reference set for undefined allergens</refset>
      <refset id="98061000146100" moduleId="11000146104">Dutch total non-drug allergen simple reference set</refset>
      <refset id="140741000146104" moduleId="11000146104">Dutch mental health procedures simple reference set</refset>
      <refset id="52801000146101" moduleId="11000146104">Dutch implant registry simple reference set</refset>    
      <refset id="450970008" moduleId="900000000000012004">General Practice / Family Practice reference set</refset>
      <refset id="733990004" moduleId="900000000000012004">Nursing Activities Reference Set</refset>
      <refset id="733991000" moduleId="900000000000012004">Nursing Health Issues Reference Set</refset>
      <refset id="787778008" moduleId="900000000000012004">Global Patient Set</refset>
      <refset id="816080008" moduleId="900000000000012004">International Patient Summary</refset>
      <refset id="260131000146101" moduleId="11000146104">Dutch laboratory test method simple reference set</refset>
      <refset id="2581000146104" moduleId="11000146104">Dutch microorganism simple reference set</refset>
      <refset id="145871000146106" moduleId="11000146104">Dutch mixed flora finding simple reference set</refset>
      <refset id="447562003" moduleId="900000000000012004">ICD-10 complex map reference set</refset>
      <refset id="446608001" moduleId="900000000000012004">ICD-O simple map reference set</refset>
      <refset id="784008009" moduleId="900000000000012004">SNOMED CT to Orphanet simple map</refset>
      <refset id="110851000146103" moduleId="11000146104">PALGA thesaurus simple reference set for pathology</refset>
      <refset id="110891000146105" moduleId="11000146104">Dutch nursing intervention for delirium simple reference set</refset>
      <refset id="99051000146107" moduleId="11000146104">Dutch nursing intervention simple reference set</refset>
      <refset id="110881000146108" moduleId="11000146104">Dutch nursing interventions for fall risk simple reference set</refset>
      <refset id="110861000146100" moduleId="11000146104">Dutch nursing interventions for pain simple reference set</refset>
      <refset id="110911000146108" moduleId="11000146104">Dutch nursing interventions for psychosocial care simple reference set</refset>
      <refset id="110901000146106" moduleId="11000146104">Dutch nursing interventions for suicide simple reference set</refset>
      <refset id="110871000146106" moduleId="11000146104">Dutch nursing interventions for wound simple reference set</refset>
      <refset id="140991000146106" moduleId="11000146104">Dutch nursing observation simple reference set</refset>
      <refset id="140961000146101" moduleId="11000146104">Dutch nursing observations for delirium simple reference set</refset>
      <refset id="140981000146109" moduleId="11000146104">Dutch nursing observations for fall risk simple reference set</refset>
      <refset id="140971000146107" moduleId="11000146104">Dutch nursing observations for suicide simple reference set</refset>
      <refset id="11721000146100" moduleId="11000146104">Dutch nursing problem simple reference set</refset>
   </refsets>

let $unsortedList :=
      <refsets>
      {
      for $set in $refsets
      let $refsetConcept :=$concepts[@conceptId=$set]
      return
      <refset id="{$set}" moduleId="{$refsetConcept/@moduleId}">
      {
      if ($refsetConcept/desc[@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][@type='pref']) then
         let $description := $refsetConcept/desc[@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][@type='pref']
         return <desc languageCode="{$description/@languageCode}" languageRefsetId="{$description/@languageRefsetId}">{$description/text()}</desc>
      else ()
      ,
      if ($refsetConcept/desc[@type = 'pref'][@languageRefsetId = $fallbackLanguageRefsetId][@active = '1']) then
         let $description := $refsetConcept/desc[@type = 'pref'][@languageRefsetId = $fallbackLanguageRefsetId][@active = '1']
         return <desc languageCode="{$description/@languageCode}" languageRefsetId="{$description/@languageRefsetId}">{$description/text()}</desc>
      else
         let $description := $refsetConcept/desc[@type = 'syn'][@languageRefsetId = $fallbackLanguageRefsetId][@active = '1'][1]
         return <desc languageCode="{$description/@languageCode}" languageRefsetId="{$description/@languageRefsetId}">{$description/text()}</desc>
      }
      </refset>
      ,
      for $set in $maps
      let $refsetConcept :=$concepts[@conceptId=$set]
      return
      <refset id="{$set}" moduleId="{$refsetConcept/@moduleId}">
      {
      if ($refsetConcept/desc[@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][@type='pref']) then
         let $description := $refsetConcept/desc[@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][@type='pref']
         return <desc languageCode="{$description/@languageCode}" languageRefsetId="{$description/@languageRefsetId}">{$description/text()}</desc>
      else ()
      ,
      if ($refsetConcept/desc[@type = 'pref'][@languageRefsetId = $fallbackLanguageRefsetId][@active = '1']) then
         let $description := $refsetConcept/desc[@type = 'pref'][@languageRefsetId = $fallbackLanguageRefsetId][@active = '1']
         return <desc languageCode="{$description/@languageCode}" languageRefsetId="{$description/@languageRefsetId}">{$description/text()}</desc>
      else
         let $description := $refsetConcept/desc[@type = 'syn'][@languageRefsetId = $fallbackLanguageRefsetId][@active = '1'][1]
         return <desc languageCode="{$description/@languageCode}" languageRefsetId="{$description/@languageRefsetId}">{$description/text()}</desc>
      }
      </refset>
      
      }
      </refsets>
return
<refsets>
{
for $item in $groupedList/refset
let $refset := $unsortedList/refset[@id=$item/@id]
return
$refset
}
</refsets>