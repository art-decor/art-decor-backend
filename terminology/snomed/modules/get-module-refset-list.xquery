xquery version "1.0";
(:
    Copyright (C) 2011-2015 ART-DECOR Expert Group (art-decor.org)
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art ="http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";
import module namespace snomed              = "http://art-decor.org/ns/terminology/snomed" at "../api/api-snomed.xqm";
(:declare option exist:serialize "method=text media-type=text/xml charset=utf-8";:)
(:let $refsetId:= request:get-parameter('id',''):)
let $moduleId:= '11000146104'

let $refsetList :=
   for $refsetId in collection(concat($get:strTerminologyData,'/snomed-data/meta'))//refset[@moduleId=$moduleId]/@id
      let $refsetConcept   := collection(concat($get:strTerminologyData,'/snomed-data/en-GB'))//concept[@conceptId=$refsetId]
      let $preferred := if ($refsetConcept/desc[@languageCode='nl'][@type='pref'][@active='1']) then 
                              $refsetConcept/desc[@languageCode='nl'][@type='pref'][@active='1']
                        else $refsetConcept/desc[@languageCode='en'][@type='pref'][@active='1']
      order by $preferred
      return
      <refset refsetId="{$refsetId}" pref="{$preferred}"/>

return
<refsetList>
{$refsetList}
</refsetList>