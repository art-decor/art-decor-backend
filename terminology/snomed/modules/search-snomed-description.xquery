xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace snomed              = "http://art-decor.org/ns/terminology/snomed" at "../api/api-snomed.xqm";

let $searchString   := util:unescape-uri(request:get-parameter('string',('')),'UTF-8')
let $toplevels      := tokenize(util:unescape-uri(request:get-parameter('toplevels',''),'UTF-8'),'\s')
let $refsets        := tokenize(util:unescape-uri(request:get-parameter('refsets',''),'UTF-8'),'\s')

(:let $searchString :='endo':)
(:let $toplevels    :=  ():)
(:let $refsets      :=  ():)

return
    snomed:searchDescription($searchString,$snomed:maxResults,$toplevels,$refsets)