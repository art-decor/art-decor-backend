xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace snomed              = "http://art-decor.org/ns/terminology/snomed" at "../api/api-snomed.xqm";

let $conceptId  := request:get-parameter('id','38102005')
let $op         := request:get-parameter('op','is-a')
let $maxstring  := request:get-parameter('max','0')

let $concept    := snomed:getRawConcept($conceptId)
let $max        := if ($maxstring castable as xs:int) then xs:int($maxstring) else 0

return
<conceptHierarchy>{snomed:getConceptHierarchy($op, $concept, $max)}</conceptHierarchy>