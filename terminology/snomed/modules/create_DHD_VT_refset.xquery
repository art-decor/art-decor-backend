xquery version "3.0";
(:
	Copyright (C) 2011-2016 Art-Decor Expert Group
	
	Author: Gerrit Boers
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";

let $concepts := collection(concat($get:strTerminologyData,'/nictiz-demo-data/import'))//row
let $refset:=
   for $concept in $concepts
   return
   <refset id="{util:uuid()}" effectiveTime="20200930" active="1" moduleId="11000146104" refsetId="41000147108" referencedComponentId="{$concept/Snomed_CT_ID}"/>

return
<refsets>{$refset}</refsets>