xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:
   Simple query for retrieving and combining search filter lists for use in SNOMED Explorer xform
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";

let $toplevels:= collection(concat($get:strTerminologyData,'/snomed-data/meta'))/topLevels
let $refsets:= doc(concat($get:strTerminologyData,'/snomed-data/meta/refsets.xml'))/refsets
return
<filters>
{
$toplevels,
$refsets
}
</filters>