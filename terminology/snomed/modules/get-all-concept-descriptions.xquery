xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace snomed              = "http://art-decor.org/ns/terminology/snomed" at "../api/api-snomed.xqm";

let $conceptId      := request:get-parameter('conceptId','138875005')
(:let $conceptId    := '13645005':)

let $desc           := snomed:getAllDescriptionsForId($conceptId)
return
    <result current="1" count="1">
    {
        for $res in $desc
        return
            <description type="{$res/@type}" conceptId="{$res/../@conceptId}" fullName="{$res/../desc[@type='fsn'][@languageCode='en']/text()}">{$res/text()}</description>
    }
    </result>