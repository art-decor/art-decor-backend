xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace snomed              = "http://art-decor.org/ns/terminology/snomed" at "../api/api-snomed.xqm";
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";

let $conceptId      := request:get-parameter('id','138875005')
(:let $conceptId    := '302619004':)
(:let $conceptId    := '1083711000119100':)
let $languageCode := substring(request:get-parameter('language','en-US'),1,2)
let $concept :=$snomed:colDataBase//concept[@conceptId=$conceptId]
let $dhdMaps    := collection($get:strTerminologyData,'/snomed-data/meta')//map[@conceptId=$conceptId]
return
<concepts>
{
   if ($languageCode='en') then
        <concept>
        {
           $concept/@*,
           $concept/*[not(name()='maps')]
        }
           <maps>
           {
              for $map in $concept/maps/map|$dhdMaps
              return
              $map
           }
           </maps>
        </concept>
    else (
        let $statusConcept  := $snomed:colDataBase//concept[@conceptId=$concept/@definitionStatusId]
        let $status         :=
            if ($statusConcept/desc[@type='pref'][@languageCode=$languageCode][@languageRefsetId='31000146106']) then
                $statusConcept/desc[@type='pref'][@languageCode=$languageCode][@languageRefsetId='31000146106']/text()
             else
                $statusConcept/desc[@type='pref'][@languageCode='en'][@languageRefsetId='900000000000509007']/text()
        return
        <concept definitionStatus="{$status}">
        {
            $concept/@*[not(name()='definitionStatus')],
            $concept/desc,
            $concept/definition,
            $concept/annotation,
            $concept/dest,
            for $grp in $concept/grp
            return
            <grp>
            {
                $grp/@*,
                for $src in $grp/src
                let $typeConcept := $snomed:colDataBase//concept[@conceptId=$src/@typeId]
                let $type :=
                    if ($typeConcept/desc[@type='pref'][@languageCode=$languageCode][@languageRefsetId='31000146106']) then
                        $typeConcept/desc[@type='pref'][@languageCode=$languageCode][@languageRefsetId='31000146106']/text()
                    else
                        $typeConcept/desc[@type='pref'][@languageCode='en'][@languageRefsetId='900000000000509007']/text()
                return
                <src type="{$type}">
                {
                   $src/@*[not(name()='type')],
                   $src/text()
                }
                </src>
            }
            </grp>
            ,
            $concept/refsets,
            $concept/association,
            $concept/attributeValue,
            $concept/simpleMaps,
            $concept/complexMaps
            }
            <maps>
            {
                for $map in $concept/maps/map|$dhdMaps
                return
                $map
            }
            </maps>
            {
                $concept/ancestors,
                $dhdMaps
            }
            <test/>
        </concept>
    )
}
</concepts>