xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art ="http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";
(:declare option exist:serialize "method=text media-type=text/xml charset=utf-8";:)
let $refsetId:= request:get-parameter('id','')

let $refset :=collection(concat($get:strTerminologyData,'/snomed-extension/refsets'))//refset[@id=$refsetId]
let $refsetProject := collection(concat($get:strTerminologyData,'/snomed-extension/meta'))//project[@ref=$refsetId]
return
<refset private="{$refsetId}">
   {
   $refset/@*,
   $refsetProject/name,
   $refsetProject/moduleDependency
   }
   <members>
      {
         for $member in $refset/member[@statusCode='active']
         return
         $member
      }
   </members>
</refset>
