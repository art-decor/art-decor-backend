xquery version "1.0";
(:
    Copyright (C) 2011-2015 ART-DECOR Expert Group (art-decor.org)
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
(:
    Creates list of refsets for use by search filter.
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";


let $concepts:= collection(concat($get:strTerminologyData,'/snomed-data/en-GB'))//concept[@conceptId='138875005']/dest
let $preferredLanguageRefsetId := '31000146106'
let $fallbackLanguageRefsetId :='900000000000509007'
let $toplevelList :=
      <topLevels>
      {
      for $dest in $concepts
      let $concept :=collection(concat($get:strTerminologyData,'/snomed-data/en-GB'))//concept[@conceptId=$dest/@sourceId]
      return
      <concept conceptId="{$concept/@conceptId}">
      {
      if ($concept/desc[@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][@type='pref']) then
         let $description := $concept/desc[@languageRefsetId = $preferredLanguageRefsetId][@active = '1'][@type='pref']
         return <desc languageCode="{$description/@languageCode}" languageRefsetId="{$description/@languageRefsetId}">{$description/text()}</desc>
      else ()
      ,
      if ($concept/desc[@type = 'pref'][@languageRefsetId = $fallbackLanguageRefsetId][@active = '1']) then
         let $description := $concept/desc[@type = 'pref'][@languageRefsetId = $fallbackLanguageRefsetId][@active = '1']
         return <desc languageCode="{$description/@languageCode}" languageRefsetId="{$description/@languageRefsetId}">{$description/text()}</desc>
      else
         let $description := $concept/desc[@type = 'syn'][@languageRefsetId = $fallbackLanguageRefsetId][@active = '1'][1]
         return <desc languageCode="{$description/@languageCode}" languageRefsetId="{$description/@languageRefsetId}">{$description/text()}</desc>
      }
      </concept>
      
      }
      </topLevels>

      
return
$toplevelList