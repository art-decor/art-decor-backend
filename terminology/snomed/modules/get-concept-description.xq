xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace snomed              = "http://art-decor.org/ns/terminology/snomed" at "../api/api-snomed.xqm";

let $conceptId      := request:get-parameter('conceptId','138875005')
(:let $conceptId    := '302619004':)

let $desc           := snomed:getActiveDescriptionsForId($conceptId)
let $current        := if ($desc) then 1 else 0

return
    <result current="{$current}" count="{$current}">
    {
        if ($desc) then 
            <description type="pref" conceptId="{$conceptId}" fullName="{$desc[@type='fsn']/text()}">{$desc[@type='pref'][@languageCode='en'][1]/text()}</description>
        else ()
    }
    </result>