xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace snomedpfix     = "http://art-decor.org/ns/snomedct-permissions" at "../api/api-permissions.xqm";

let $fixQueries     := snomedpfix:setSCTExtensionCollectionPermissions()
let $fixFiles       := snomedpfix:setSCTQueryPermissions()

return
<ok/>