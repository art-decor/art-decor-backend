xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
module namespace snomed     = "http://art-decor.org/ns/terminology/snomed";
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";

declare variable $snomed:root               := repo:get-root();
declare variable $snomed:strDataBase        := concat($get:strTerminologyData,'/snomed-data/en-GB');

declare variable $snomed:colDataBase        := collection($snomed:strDataBase);


(:~
:   All functions support their own override, but this is the fallback for the maximum number of results returned on a search
:)
declare variable $snomed:maxResults     := xs:integer('50');

declare function snomed:getPreferredActiveDescriptionForId($conceptId as xs:string?) as element(desc)? {
    snomed:getAllDescriptionsForId($conceptId)[@type='pref'][@active='1']
};
declare function snomed:getActiveDescriptionsForId($conceptId as xs:string?) as element(desc)* {
    snomed:getAllDescriptionsForId($conceptId)[@active='1']
};
declare function snomed:getAllDescriptionsForId($conceptId as xs:string?) as element(desc)* {
    snomed:getRawConcept($conceptId)/desc
};
declare function snomed:getRawConcept($conceptId as xs:string?) as element(concept)* {
    let $baseConcept    := $snomed:colDataBase//@conceptId[.=$conceptId]/parent::concept
    
    return
    $baseConcept
};

declare function snomed:getSubConcepts($conceptId as xs:string?) as element(concept)* {
let $concept    := $snomed:colDataBase//@conceptId[.=$conceptId]/parent::concept

return

        for $dest in $concept/dest[@active]
        let $subconcept       := $snomed:colDataBase//@conceptId[.=$dest/@sourceId]/parent::concept
        order by $subconcept/desc[@languagecode='en'][1][@type='pref']
        return
        $subconcept

};
(:~
    This function returns all SNOMED concepts in the hierarchy where
    op       is   'is-a'
                  :: includes all concept ids that have a transitive is-a relationship with the concept Id 
                   provided in concept, including the provided concept itself (i.e. include child codes)
                   this is the default
                  'descendent-of'
                  :: includes all concept ids that have a transitive is-a relationship with the concept Id 
                   provided in concept, excluding the provided concept itself (i.e. include child codes)
    concept  is   the concept carrying the SNOMED concpet in question, as returned by the api function snomed:getRawConcept($conceptId)
    max      is   (not yet implemented) the maximum number of concepts to return, by default limited to 10, if unlimited set to 0

:)
declare function snomed:getConceptHierarchy($concept as element(concept)?) as element(concept)* {
    snomed:getConceptHierarchy('is-a', $concept, 0)
};
declare function snomed:getConceptHierarchy($op as xs:string?, $concept as element(concept)?, $max as xs:int?) as element(concept)* {

if ($concept)
    then
        if ($op = 'descendent-of')
        then
            for $dest in $concept/dest[@active]
            let $subconcept := snomed:getRawConcept($dest/@sourceId)
            return snomed:getConceptHierarchy('is-a', $subconcept, $max)
         else
            <concept>
            {
                $concept/@*,
                $concept/desc[@type='pref'][@active='1'],
                for $dest in $concept/dest[@active]
                let $subconcept := snomed:getRawConcept($dest/@sourceId)
                return snomed:getConceptHierarchy('is-a', $subconcept, $max)
            }
            </concept>
    else ()

};

declare function snomed:isValidSearch($searchString as xs:string?) as xs:boolean {
    if (matches($searchString,'^[a-z|0-9]') and string-length($searchString)>2 and string-length($searchString)<40) 
    then true()
    else if (matches($searchString,'^[A-Z]') and string-length($searchString)>1 and string-length($searchString)<40) 
    then true()
    else false()
};


declare function snomed:searchDescription($searchString as xs:string?, $maxResults as xs:integer?, $toplevels as xs:string*, $refsets as xs:string*) as element() {
let $validSearch    := snomed:isValidSearch($searchString)
let $searchTerms    := tokenize($searchString,'\s')
let $options        := snomed:getSimpleLuceneOptions()
let $query          := snomed:getSimpleLuceneQuery($searchTerms)
let $toplevels      := $toplevels[not(.='')]
let $refsets        := $refsets[not(.='')]

(:raw query result:)
let $result         :=
    if ($validSearch and matches($searchString,'^\d+')) then
        let $res    := $snomed:colDataBase//@conceptId[.=$searchString]/parent::concept/desc[@languageCode='en'][@type='pref']
        return
        if ($res) then ($res) else (
            $snomed:colDataBase//@conceptId[.=$searchString]/parent::concept/desc[@languageCode='en'][1]
        )
    else 
    if ($validSearch) then
        if (empty($toplevels) and empty($refsets)) then
            $snomed:colDataBase//desc[ft:query(.,$query,$options)][@active][../@active]
        else
        if (empty($toplevels)) then
            $snomed:colDataBase//desc[ft:query(.,$query,$options)][..//@refsetId=$refsets][@active][../@active]
        else
        if (empty($refsets)) then
            $snomed:colDataBase//desc[ft:query(.,$query,$options)][../ancestors/id=$toplevels][@active][../@active]
        else (
            $snomed:colDataBase//desc[ft:query(.,$query,$options)][../ancestors/id=$toplevels][..//@refsetId=$refsets][@active][../@active]
        )
    else ()

(:order result by count and length:)
let $result         := 
    for $desc in $result
    order by xs:integer($desc/@count),xs:integer($desc/@length)
    return $desc

(:group result by conceptId:)
let $result         :=
    for $desc in $result
    let $cc := $desc/parent::concept/@conceptId
    group by $cc    
    order by xs:integer($desc[1]/@count),xs:integer($desc[1]/@length)
    return $desc[1]
  
let $count          := count($result)
let $current        := if ($count>$maxResults) then $maxResults else ($count)
return
    <result current="{$current}" count="{$count}">
    {
        for $res in subsequence($result,1,$maxResults)
        let $conceptId  := $res/parent::concept/@conceptId
        let $fsn        := $snomed:colDataBase//@conceptId[.=$conceptId]/parent::concept/desc[@active][@type='fsn'][@languageCode = 'en']/text()
        return
        <description conceptId="{$conceptId}" fullName="{$fsn}">{$res/@type,$res/text()}</description>
    }
    </result>
};



(:~
:   Returns lucene config xml for a sequence of strings. The search will find yield results that match all terms+trailing wildcard in the sequence
:   Example output:
:   <query>
:       <bool>
:           <wildcard occur="must">term1*</wildcard>
:           <wildcard occur="must">term2*</wildcard>
:       </bool>
:   </query>
:
:   @param $searchTerms required sequence of terms to look for
:   @return lucene config
:   @since 2014-06-06
:)
declare function snomed:getSimpleLuceneQuery($searchTerms as xs:string*) as element() {
    <query>
        <bool>
        {
            for $term in $searchTerms
            return
                if (matches($term,'^[a-z|0-9]')) then
                    <wildcard occur="must">{concat($term,'*')}</wildcard>
                else if (matches($term,'^[A-Z]')) then
                    <term occur="must">{lower-case($term)}</term>
                else()
        }
        </bool>
    </query>
};

(:~
:   Returns lucene options xml that instruct filter-rewrite=yes
:)
declare function snomed:getSimpleLuceneOptions() as element() {
    <options>
        <filter-rewrite>yes</filter-rewrite>
    </options>
};

(: copied from functx, turns a string into a sequence of characters :)
declare %private function snomed:chars( $arg as xs:string? )  as xs:string* {
   for $ch in string-to-codepoints($arg)
   return codepoints-to-string($ch)
 } ;





