xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
module namespace snomedpfix     = "http://art-decor.org/ns/snomedct-permissions";
declare namespace sm            = "http://exist-db.org/xquery/securitymanager";

(:  Mode            Octal
 :  rw-r--r--   ==  0644
 :  rw-rw-r--   ==  0664
 :  rwxr-xr--   ==  0754
 :  rwxr-xr-x   ==  0755
 :  rwxrwxr-x   ==  0775
 :)

(:install path for art (normally /db/apps/), includes trailing slash :)
declare variable $snomedpfix:root                   := repo:get-root();
declare variable $snomedpfix:strSnomedModules       := concat($snomedpfix:root,'terminology/snomed/modules');
declare variable $snomedpfix:strSnomedExtensionData := concat($snomedpfix:root,'terminology-data/snomed-extension');

declare function snomedpfix:setSCTQueryPermissions() {
    snomedpfix:checkIfUserDba(),
    
    for $query in xmldb:get-child-resources(xs:anyURI(concat('xmldb:exist:///',$snomedpfix:strSnomedModules)))
    return (
        sm:chown(xs:anyURI(concat('xmldb:exist:///',$snomedpfix:strSnomedModules,'/',$query)),'admin'),
        sm:chgrp(xs:anyURI(concat('xmldb:exist:///',$snomedpfix:strSnomedModules,'/',$query)),'terminology'),
        if (starts-with($query,('check','get','is-','retrieve','search','view'))) then
            sm:chmod(xs:anyURI(concat('xmldb:exist:///',$snomedpfix:strSnomedModules,'/',$query)),sm:octal-to-mode('0755'))
        else (
            sm:chmod(xs:anyURI(concat('xmldb:exist:///',$snomedpfix:strSnomedModules,'/',$query)),sm:octal-to-mode('0754'))
        )
        ,
        sm:clear-acl(xs:anyURI(concat('xmldb:exist:///',$snomedpfix:strSnomedModules,'/',$query)))
    )
};

declare function snomedpfix:setSCTExtensionCollectionPermissions() {
    snomedpfix:checkIfUserDba(),
    if (xmldb:collection-available($snomedpfix:strSnomedExtensionData)) then (
        snomedpfix:setPermissions($snomedpfix:strSnomedExtensionData,'admin:terminology',sm:octal-to-mode('0775'),'admin:terminology',sm:octal-to-mode('0775'))
    ) else ()
};

(:
:   Helper function with recursion for snomedpfix:setSCTExtensionCollectionPermissions()
:)
declare %private function snomedpfix:setPermissions($path as xs:string, $collown as xs:string, $collmode as xs:string, $resown as xs:string, $resmode as xs:string) {
    sm:chown(xs:anyURI($path),$collown),
    sm:chmod(xs:anyURI($path),$collmode),
    sm:clear-acl(xs:anyURI($path)),
    for $res in xmldb:get-child-resources(xs:anyURI($path))
    return (
        sm:chown(xs:anyURI(concat($path,'/',$res)),$resown),
        sm:chmod(xs:anyURI(concat($path,'/',$res)),$resmode),
        sm:clear-acl(xs:anyURI(concat($path,'/',$res)))
    )
    ,
    for $collection in xmldb:get-child-collections($path)
    return
        snomedpfix:setPermissions(concat($path,'/',$collection), $collown, $collmode, $resown, $resmode)
};

declare %private function snomedpfix:checkIfUserDba() {
    if (sm:is-dba((sm:id()//sm:real/sm:username/text())[1])) then () else (
        error(QName('http://art-decor.org/ns/art-decor-permissions', 'NotAllowed'), concat('Only dba user can use this module. ',(sm:id()//sm:real/sm:username/text())[1]))
    )
};