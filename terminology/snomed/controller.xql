(:     This is the main controller for the web application. It is called from the
    XQueryURLRewrite filter configured in web.xml. :)
xquery version "3.0";
declare variable $exist:path external;
declare variable $exist:controller external;
declare variable $exist:resource external;
declare variable $exist:root external;

if ($exist:path eq "/") then
    (: forward root path to index.xql :)
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <redirect url="index.html"/>
    </dispatch>
	else if (matches($exist:path, "/getConcept")) then
			<dispatch xmlns="http://exist.sourceforge.net/NS/exist">
				<forward url="{$exist:controller}/modules/get-snomed-concept.xquery">
					<add-parameter name="id" value="{$exist:resource}"/>
				</forward>
			</dispatch>
	else if (matches($exist:path, "/viewConcept")) then
			<dispatch xmlns="http://exist.sourceforge.net/NS/exist">
				<forward url="{$exist:controller}/modules/retrieve-concept-for-language.xquery">
					<add-parameter name="id" value="{$exist:resource}"/>
				</forward>
				<view>
					<forward servlet="XSLTServlet">
						<set-attribute name="xslt.stylesheet" value="{$exist:root}/snomed/resources/stylesheets/concept2html.xsl"/>
						<set-attribute name="xslt.output.media-type" value="text/html"/>
    					<set-attribute name="xslt.output.doctype-public" value="-//W3C//DTD XHTML 1.0 Transitional//EN"/>
    					<set-attribute name="xslt.output.doctype-system" value="resources/xhtml1-transitional.dtd"/>
    					<set-attribute name="xslt.language" value="{request:get-parameter('language','en-US')}"/>
					</forward>
				</view>
			</dispatch>
	else if (matches($exist:path, "/getDescription")) then
			<dispatch xmlns="http://exist.sourceforge.net/NS/exist">
				<forward url="{$exist:controller}/modules/get-concept-description.xq">
					<add-parameter name="conceptId" value="{$exist:resource}"/>
				</forward>
			</dispatch>
	else if (matches($exist:path, "/searchDescription")) then
			<dispatch xmlns="http://exist.sourceforge.net/NS/exist">
				<forward url="{$exist:controller}/modules/search-snomed-description.xquery">
					<add-parameter name="string" value="{$exist:resource}"/>
				</forward>
			</dispatch>
	else if (matches($exist:path, "/retrieveRefsetASCII")) then
			<dispatch xmlns="http://exist.sourceforge.net/NS/exist">
				<forward url="{$exist:controller}/modules/retrieve-refset.xquery">
					<add-parameter name="string" value="{$exist:resource}"/>
				</forward>
			</dispatch>
	else if (matches($exist:path, "/retrieveRefsetXML")) then
			<dispatch xmlns="http://exist.sourceforge.net/NS/exist">
				<forward url="{$exist:controller}/modules/retrieve-refset-xml.xquery">
					<add-parameter name="id" value="{$exist:resource}"/>
				</forward>
			</dispatch>
	else if (matches($exist:path, "/viewRefset")) then
			<dispatch xmlns="http://exist.sourceforge.net/NS/exist">
				<forward url="{$exist:controller}/modules/view-refset.xquery">
				</forward>
				<view>
					<forward servlet="XSLTServlet">
						<set-attribute name="xslt.stylesheet" value="{$exist:root}/snomed/resources/stylesheets/refset2html.xsl"/>
						<set-attribute name="xslt.output.media-type" value="text/html"/>
    					<set-attribute name="xslt.output.doctype-public" value="-//W3C//DTD XHTML 1.0 Transitional//EN"/>
    					<set-attribute name="xslt.output.doctype-system" value="resources/xhtml1-transitional.dtd"/>
    					<set-attribute name="xslt.show" value="{request:get-parameter('show','fsn')}"/>
    					<set-attribute name="xslt.language" value="{request:get-parameter('language','nl')}"/>
					</forward>
				</view>
			</dispatch>
	else if (matches($exist:path, "/viewOrderedRefset")) then
			<dispatch xmlns="http://exist.sourceforge.net/NS/exist">
				<forward url="{$exist:controller}/modules/view-ordered-refset.xquery">
				</forward>
				<view>
					<forward servlet="XSLTServlet">
						<set-attribute name="xslt.stylesheet" value="{$exist:root}/snomed/resources/stylesheets/orderedRefset2html.xsl"/>
						<set-attribute name="xslt.output.media-type" value="text/html"/>
    					<set-attribute name="xslt.output.doctype-public" value="-//W3C//DTD XHTML 1.0 Transitional//EN"/>
    					<set-attribute name="xslt.output.doctype-system" value="resources/xhtml1-transitional.dtd"/>
					</forward>
				</view>
			</dispatch>
	else if (matches($exist:path, "/refsetList")) then
			<dispatch xmlns="http://exist.sourceforge.net/NS/exist">
				<forward url="{$exist:controller}/modules/get-module-refset-list.xquery">
					<add-parameter name="id" value="{$exist:resource}"/>
				</forward>
				<view>
					<forward servlet="XSLTServlet">
						<set-attribute name="xslt.stylesheet" value="{$exist:root}/snomed/resources/stylesheets/refsetList2html.xsl"/>
						<set-attribute name="xslt.output.media-type" value="text/html"/>
    					<set-attribute name="xslt.output.doctype-public" value="-//W3C//DTD XHTML 1.0 Transitional//EN"/>
    					<set-attribute name="xslt.output.doctype-system" value="resources/xhtml1-transitional.dtd"/>
					</forward>
				</view>
			</dispatch>
	else if (matches($exist:path, "/isDescendant")) then
			<dispatch xmlns="http://exist.sourceforge.net/NS/exist">
				<forward url="{$exist:controller}/modules/is-descendant.xquery">
				</forward>
			</dispatch>
	else if (matches($exist:path, "/getRelease")) then
			<dispatch xmlns="http://exist.sourceforge.net/NS/exist">
				<forward url="{$exist:controller}/modules/get-zipped-release.xquery">
					<add-parameter name="release" value="{$exist:resource}"/>
				</forward>
			</dispatch>	
else
    (: everything else is passed through :)
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <cache-control cache="yes"/>
    </dispatch>