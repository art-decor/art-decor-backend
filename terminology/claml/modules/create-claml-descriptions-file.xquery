xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace claml = "http://art-decor.org/ns/terminology/claml" at "xmldb:exist:///db/apps/terminology/claml/api/api-claml.xqm";

(:let $clamlDataCollection := 'atc-data':)

(:let $clamlDataCollection := 'icf-data':)

(:let $clamlDataCollection := 'icd10-data':)

(:let $clamlDataCollection := 'hl7-data':)

(:let $clamlDataCollection := 'loinc-claml':)

let $clamlDataCollection        := if (request:exists()) then request:get-parameter('package',()) else ('icpc-1-nl-data')
return
    claml:createDescriptionsFile($clamlDataCollection)