xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get   ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace claml = "http://art-decor.org/ns/terminology/claml" at "../api/api-claml.xqm";

let $mode                   := if (request:exists()) then request:get-parameter('mode','html') else ('html')
let $statusCodes            := if (request:exists()) then tokenize(normalize-space(request:get-parameter('statusCode','active')),'\s') else ()
let $classificationId       := if (request:exists()) then request:get-parameter('classificationId','') else ('2.16.840.1.113883.5.1119')
let $code                   := if (request:exists()) then request:get-parameter('code','') else ('HP')
let $language               := if (request:exists()) then request:get-parameter('language','') else ('nl-NL')
(:let $classificationId     := '2.16.840.1.113883.6.73'
let $code                   := '':)

let $preparedClass          := claml:getPreparedClass($statusCodes, $classificationId, $code, $language)

return
    if ($mode='xml') then (
        $preparedClass
    )
    else if (empty($preparedClass)) then (
        response:set-header('Content-Type','text/html; charset=utf-8'),
        claml:classToHtml(<Class/>)
    )
    else (
        response:set-header('Content-Type','text/html; charset=utf-8'),
        claml:classToHtml($preparedClass)
    )