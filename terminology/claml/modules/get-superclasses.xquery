xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
(:let $classification := request:get-parameter('classification','')
let $code := request:get-parameter('code',''):)
let $classification := 'ATC-NL'
let $request := 'N05BA06 N05BA21'
let $codes := tokenize($request,'\s')
let $classifications :=
   <classifications>
   {
      for $child in xmldb:get-child-collections($get:strTerminologyData)
      let $title := collection(concat($get:strTerminologyData,'/',$child))//ClaML/Title
      return
      if ($title) then
      <classification collection="{$child}">
      {$title}
      </classification>
      else()
   }
   </classifications>

let $collection := 
   if (string-length($classification)>0) then
      concat($get:strTerminologyData,'/',$classifications/classification[Title/@name=$classification]/@collection,'/hierarchy')
   else(
      concat($get:strTerminologyData,'/',$classifications/classification[1]/@collection,'/hierarchy')
   )

let $parents :=  collection($collection)//Class[SubClass/@code=$codes]


return
<parents>
{
   for $parent in $parents
   return
   if (every $id in $codes satisfies $id=$parent/SubClass/@code) then $parent else ()
}
</parents>


