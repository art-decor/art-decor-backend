xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adsearch       = "http://art-decor.org/ns/terminology/search" at "../../api/api-terminology-search.xqm";

let $classificationId   := if (request:exists()) then request:get-parameter('classificationId','') else ('2.16.840.1.113883.6.3.2')
let $searchLanguage     := if (request:exists()) then request:get-parameter('language','nl-NL') else ('nl-NL')
let $searchString       := if (request:exists()) then util:unescape-uri(request:get-parameter('string',''),'UTF-8') else ()
let $statusCodes        := if (request:exists()) then tokenize(normalize-space(request:get-parameter('statusCode','active')),'\s') else ()
let $searchScope        := if (request:exists()) then tokenize(normalize-space(request:get-parameter('scope','description')),'\s') else ()
(:let $classification   :='':)
(:let $searchString     :='ana eig':)

return
    adsearch:searchClamlConcept($classificationId, $searchLanguage, $searchString, $adsearch:maxResults, $statusCodes, $searchScope)