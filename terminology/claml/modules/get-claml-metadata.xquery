xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
let $classificationId := request:get-parameter('classificationId','')
let $language :='nl-NL'
(:let $classificationId := '2.16.840.1.113883.6.73':)



   
let $classificationIndex := doc(concat($get:strTerminology,'/claml/classification-index.xml'))/classificationIndex
let $classification :=$classificationIndex//classification[@id=$classificationId]


let $collection := 
   if (count($classification)>1) then
      if ($classification/@language=$language) then
         concat($classification[@language=$language]/@collection,'/denormalized')
      else(concat($classification[1]/@collection,'/denormalized'))
   else(concat($classification/@collection,'/denormalized'))

let $claml :=collection($collection)//ClaML-denormalized[Identifier/@uid=$classificationId]
return
<info>
{
$claml/Title,
$claml/Meta,
$claml/Identifier
}
</info>
(:$classification:)


