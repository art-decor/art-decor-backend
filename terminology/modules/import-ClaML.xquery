xquery version "3.0";
(:
	Copyright (C) 2011-2017 Art Decor Expert Group art-decor.org
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
	

      <claml governanceGroup="who" path="/icnp-data/nl-NL/claml" file="icnp"/>,
      <claml governanceGroup="ihe" path="/pathlex-data/en-US/claml" file="pathlex"/>,
:)
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";

declare namespace fhir= "http://hl7.org/fhir";


let $xsltParameters     :=  <parameters></parameters>

let $clamlFiles:= 
   (
      <claml governanceGroup="who" path="/icd10-who-data/en-US/claml" file="icd10-who"/>,
      <claml governanceGroup="who" path="/atc-data/nl-NL/claml" file="atc"/>,
      <claml governanceGroup="rsna" path="/radlex-data/en-US/claml" file="radlex"/>,
      <claml governanceGroup="orphanet" path="/orpha-data/en-US/claml" file="orpha"/>
   )

               

for $clamlFile in $clamlFiles
   let $claml   := collection(concat($get:strTerminologyData,$clamlFile/@path))/ClaML
   let $governanceGroupId := $clamlFile/@governanceGroup
   let $fileName := $clamlFile/@file
   let $codeSystem :=transform:transform($claml, xs:anyURI(concat('xmldb:exist://',$get:strTerminology,'/resources/stylesheets/ClaML-to-CADTS-codeSystem.xsl')), $xsltParameters)
   let $resolvedAncestors :=transform:transform($codeSystem, xs:anyURI(concat('xmldb:exist://',$get:strTerminology,'/resources/stylesheets/resolve-ancestors-for-CADTS-codeSystem.xsl')), $xsltParameters)
   return
      xmldb:store(concat($get:strCodesystemStableData,'/external/',$governanceGroupId),concat($fileName,'.xml'),transform:transform($resolvedAncestors, xs:anyURI(concat('xmldb:exist://',$get:strTerminology,'/resources/stylesheets/add-metadata-to-CADTS-codeSystem.xsl')), $xsltParameters))
   
