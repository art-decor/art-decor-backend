xquery version "3.0";
(:
	Copyright (C) 2011-2017 Art Decor Expert Group art-decor.org
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";

import module namespace adterminology       = "http://art-decor.org/ns/terminology" at "../api/api-terminology.xqm";



(:let $codeSystemId := '2.16.840.1.113883.6.96'
let $code   := '138875005':)

let $loinc := collection(concat($get:strTerminologyData,'/loinc-data/data/universal'))/loinc_db

let $total := count($loinc/concept)
let $long := count($loinc/concept/longName)
let $short := count($loinc/concept/shortName)

let $languages := distinct-values($loinc//concept/@language)

let $langReport :=
   for $lang in $languages
      let $lng := count($loinc//concept[@language=$lang]/longName)
      let $shrt := count($loinc//concept[@language=$lang]/shortName)
      let $comp := count($loinc//concept[@language=$lang]/component)
      return
      <language code="{$lang}" component="{$comp}" long="{$lng}" short="{$shrt}"/>

return
<result count="{$total}" long="{$long}" short="{$short}">
{$langReport}
</result>