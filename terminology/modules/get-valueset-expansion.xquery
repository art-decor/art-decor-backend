xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adterminology       = "http://art-decor.org/ns/terminology" at "../api/api-terminology.xqm";
import module namespace get      = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";

let $compose := request:get-data()/compose

(:let $compose :=
  <compose>
    <include>
      <system value="http://loinc.org" oid="2.16.840.1.113883.6.1"/>
      <filter>
        <property value="parent"/>
        <op value="="/>
        <value value="LP43571-6"/>
      </filter>
    </include>
    <exclude>
      <system value="http://loinc.org" oid="2.16.840.1.113883.6.1"/>
      <concept>
        <code value="5932-9"/>
        <display value="Cholesterol [Presence] in Blood by Test strip"/>
      </concept>
      <concept>
        <code value="9342-7"/>
        <display value="Cholesterol [Percentile]"/>
      </concept>
    </exclude>
  </compose>:)

let $maxResults   := xs:integer('10')
let $query        := adterminology:getQueryForCompose($compose)
let $result       := util:eval($query)
let $resultCount  := count($result)
let $current      := if ($resultCount>$maxResults) then $maxResults else ($resultCount)

let $output       :=subsequence($result,1,$maxResults)

return
   <result current="{$current}" count="{$resultCount}" query="{$query}">
   {
     for $concept in $output
     return
     <concept>{$concept/@*,$concept/designation[@use='pref'][1]}</concept>
   }
   </result>