xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adterminology       = "http://art-decor.org/ns/terminology" at "../api/api-terminology.xqm";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "json";
declare option output:media-type "application/json";

let $context       := if (request:exists()) then request:get-parameter('context','') else ('')
let $codeSystemIds       := if (request:exists()) then request:get-parameter('codeSystemIds','') else ('')
let $code          := if (request:exists()) then util:unescape-uri(request:get-parameter('code',''),'UTF-8') else ()
let $statusCodes        := if (request:exists()) then tokenize(normalize-space(request:get-parameter('statusCode','active')),'\s') else ()
let $searchScope        := if (request:exists()) then tokenize(normalize-space(request:get-parameter('scope','description')),'\s') else ()




(:let $codeSystemIds       := ''
let $context     := ''
let $code       := '135640007'
let $statusCodes        := ''
let $searchScope        := '':)


let $maxResults        :=xs:integer('50')


return
    adterminology:searchCode($context,$codeSystemIds, $code,$maxResults, $statusCodes, $searchScope)