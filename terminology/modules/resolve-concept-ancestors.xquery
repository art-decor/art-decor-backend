xquery version "3.0";
(:
	Copyright (C) 2011-2015 Art Decor Expert Group art-decor.org
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
	
      if ($ancestors/ancestor) then
         if ($concept/ancestors) then
            update replace $concept/ancestors with $ancestors
         else 
            update insert $ancestors into $concept
      else ()
:)
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";



declare %private function local:resolveAncestors ($codeSystem as item(), $code as xs:string,$distance as xs:integer) as item()*{
   let $parentConcept := $codeSystem/concept[@code=$code]
   return
   (
    <ancestor code="{$parentConcept/@code/string()}" distance="{$distance}"/>
   ,
   if ($distance lt 11) then
      for $parent in $parentConcept/parent
      return
      local:resolveAncestors($codeSystem,$parent/@code/string(),$distance+1)
   else()
   )
   
};

declare %private function local:resolveAncestorsForConcept($codeSystem as element(),$concept as element()) as item()*{
         let $ancestorList :=
         <ancestors>
         {
         for $parent in $concept/parent
         return
         local:resolveAncestors($codeSystem,$parent/@code,1)
         }
         </ancestors>
      let $ancestors :=
      <ancestors>
      {
         for $code in distinct-values($ancestorList/ancestor/@code)
            let $minDistance := min($ancestorList/ancestor[@code=$code]/@distance)
            return
            <ancestor code="{$code}" distance="{$minDistance}"/>
       }
       </ancestors>
      return
      if ($ancestors/ancestor) then
         if ($concept/ancestors) then
            update replace $concept/ancestors with $ancestors
         else 
            update insert $ancestors into $concept
      else ()
};

(:let $statusCode         := tokenize(normalize-space(request:get-parameter('statusCode','active')),'\s'):)
(:let $classificationId   := request:get-parameter('classificationId','')
let $code               := request:get-parameter('code',''):)

let $governanceGroupId :='who'
(:let $codeSystemId   := 'v3-ObservationInterpretation':)
let $codeSystemId   := 'v3-ActCode'
let $codeSystems   := collection(concat($get:strCodesystemAuthoringData,'/',$governanceGroupId,'/data'))/codeSystem

for $codeSystem in $codeSystems
   return
   for $concept in $codeSystem/concept
   return
   local:resolveAncestorsForConcept($codeSystem,$concept)




