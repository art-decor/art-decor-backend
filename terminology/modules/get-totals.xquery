xquery version "3.0";
(:
	Copyright (C) 2011-2017 Art Decor Expert Group art-decor.org
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";

import module namespace adterminology       = "http://art-decor.org/ns/terminology" at "../api/api-terminology.xqm";



(:let $codeSystemId := '2.16.840.1.113883.6.96'
let $code   := '138875005':)

let $concepts := count(collection($get:strCodesystemStableData)//concept)
let $designations := count(collection($get:strCodesystemStableData)//designation)
let $valueSets := count(collection('/db/apps/terminology-data/valueset-stable-data/')//valueSet)
let $valueSetConcepts :=count(collection('/db/apps/terminology-data/valueset-stable-data/')//concept)
return

<totals concepts="{$concepts}" designations="{$designations}" valueSets="{$valueSets}" valueSetConcepts="{$valueSetConcepts}" smallValueSets="{count(collection('/db/apps/terminology-data/valueset-stable-data/')//valueSet[count(.//concept) lt 50])}">
</totals>