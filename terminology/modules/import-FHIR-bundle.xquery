xquery version "3.0";
(:
	Copyright (C) 2011-2017 Art Decor Expert Group art-decor.org
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";

declare namespace fhir= "http://hl7.org/fhir";





let $governanceGroupId := 'hl7'

let $xsltParameters     :=  <parameters></parameters>

let $bundle   := collection(concat($get:strTerminologyData,'/codesystem-authoring-data/import'))/fhir:Bundle

for $codeSystem in $bundle/fhir:entry/fhir:resource/fhir:CodeSystem
let $codeSystemId := $codeSystem/fhir:id/@value/string()
let $decorCodeSystem :=transform:transform($codeSystem ,xs:anyURI(concat('xmldb:exist://',$get:strTerminology,'/resources/stylesheets/FHIR-codeSystem-to-CADTS-codeSystem.xsl')), $xsltParameters)
let $resolvedAncestors :=transform:transform($decorCodeSystem ,xs:anyURI(concat('xmldb:exist://',$get:strTerminology,'/resources/stylesheets/resolve-ancestors-for-CADTS-codeSystem.xsl')), $xsltParameters)
return
xmldb:store(concat($get:strCodesystemStableData,'/external/',$governanceGroupId),concat($codeSystemId,'.xml'),transform:transform($resolvedAncestors ,xs:anyURI(concat('xmldb:exist://',$get:strTerminology,'/resources/stylesheets/add-metadata-to-CADTS-codeSystem.xsl')), $xsltParameters))



