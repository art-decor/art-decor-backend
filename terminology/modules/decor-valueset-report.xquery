xquery version "3.0";
(:
	Copyright (C) 2011-2017 Art Decor Expert Group art-decor.org
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
	
	<result count="{count($loinc/concept)}">
      <component count="{count(distinct-values($loinc/concept/component))}">R wave duration.lead V3</component>
      <property count="{count(distinct-values($loinc/concept/property))}">Time</property>
      <timing count="{count(distinct-values($loinc/concept/timing))}">Pt</timing>
      <system count="{count(distinct-values($loinc/concept/system))}">Heart</system>
      <scale count="{count(distinct-values($loinc/concept/scale))}">{distinct-values($loinc/concept/scale)}</scale>
      <method count="{count(distinct-values($loinc/concept/method))}">EKG</method>
      <class count="{count(distinct-values($loinc/concept/class))}">
         {
         for $class in distinct-values($loinc/concept/class)
         order by $class
         return
         <class>{$class}</class>
         }
      </class>
</result>
	
:)
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";

import module namespace adterminology       = "http://art-decor.org/ns/terminology" at "../api/api-terminology.xqm";

let $decorData := $get:colDecorData

let $distinctCodeSystem := distinct-values($decorData//terminology/valueSet/conceptList/concept/@codeSystem)
let $maxValueSetCount := max($decorData//decor/count(terminology/valueSet))

return
<report count="{count($distinctCodeSystem)}" maxValueSetCount="{$maxValueSetCount}">
<projects>
{
for $project in $decorData//decor
return
<project prefix="{$project/project/@prefix}" name="{$project/project/name[1]}" valuesetCount="{count($project/terminology/valueSet)}"/>
}
</projects>
<codeSystems>
{
for $codeSystem in $distinctCodeSystem
let $valueSetCount := count($decorData//terminology/valueSet[.//concept/@codeSystem=$codeSystem])
let $codeSystemName := collection($get:strCodesystemStableData)/codeSystem[@oid=$codeSystem]/name[1]
order by $valueSetCount descending
return
<codeSystem name="{$codeSystemName/text()}" oid="{$codeSystem}" valueSetCount="{$valueSetCount}"></codeSystem>
}
</codeSystems>
</report>