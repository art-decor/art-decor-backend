xquery version "3.0";
(:
	Copyright (C) 2011-2017 Art Decor Expert Group art-decor.org
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
	
	<result count="{count($loinc/concept)}">
      <component count="{count(distinct-values($loinc/concept/component))}">R wave duration.lead V3</component>
      <property count="{count(distinct-values($loinc/concept/property))}">Time</property>
      <timing count="{count(distinct-values($loinc/concept/timing))}">Pt</timing>
      <system count="{count(distinct-values($loinc/concept/system))}">Heart</system>
      <scale count="{count(distinct-values($loinc/concept/scale))}">{distinct-values($loinc/concept/scale)}</scale>
      <method count="{count(distinct-values($loinc/concept/method))}">EKG</method>
      <class count="{count(distinct-values($loinc/concept/class))}">
         {
         for $class in distinct-values($loinc/concept/class)
         order by $class
         return
         <class>{$class}</class>
         }
      </class>
</result>
	
:)
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";

import module namespace adterminology       = "http://art-decor.org/ns/terminology" at "../api/api-terminology.xqm";

let $decorData := $get:colDecorData

let $decorRoot := $get:strDecorData
let $govGroupCollections :=xmldb:get-child-collections($decorRoot)


let $distinctCodeSystem := distinct-values($decorData//terminology/valueSet/conceptList/concept/@codeSystem)
let $maxValueSetCount := max($decorData//decor/count(terminology/valueSet))

return
<report maxValueSetCount="{$maxValueSetCount}">
<collections>
{
for $col in $govGroupCollections

   let $govCollection := concat($decorRoot,'/',$col)
   for $projectCollection in xmldb:get-child-collections($govCollection)
      let $decorFile := collection(concat($decorRoot,'/',$col,'/',$projectCollection))/decor
      let $projetCollectionCheck:=
        if ($decorFile/terminology/valueSet[@id] and not(xmldb:collection-available(concat('/db/apps/terminology-data/valueset-stable-data/',$projectCollection)))) then
            xmldb:create-collection('/db/apps/terminology-data/valueset-stable-data/',$projectCollection)
        else()
   return
   if ($decorFile/terminology/valueSet[@id]) then
      for $valueSet in $decorFile/terminology/valueSet[@id]
      let $valueSetComplete := 
            <valueSet url="http://art-decor.org/fhir/ValueSet/{$valueSet/@id}--{replace($valueSet/@effectiveDate,':','')}" oid="{$valueSet/@id}" experimental="true">
               {$valueSet/@*}
               <name languageCode="{$decorFile/project/@defaultLanguage}" count="{count(tokenize($valueSet/@displayName,'\s'))}" length="{string-length($valueSet/@displayName)}">{$valueSet/@displayName/string()}</name>
               {
               for $name in $decorFile/project/name
               return
               <publisher>{$name/@*,$name/text()}</publisher>
               }
               <contact>
               <telecom system="" value=""/>
               </contact>
               <description>TEST DECOR valueSet transformation</description>
               <compose>
               
               {
               if ($valueSet/completeCodeSystem) then
                  for $codeSystem in $valueSet/completeCodeSystem
                  return
                   <include> 
                     <system oid="{$codeSystem/@codeSystem}" url=""/> 
                   </include> 
               else()
               ,
               if ($valueSet/conceptList) then
                  for $item in $valueSet/conceptList/*
                  let $codeSystemId := $item/@codeSystem
                  group by $codeSystemId
                  return
                  <include>
                     <system oid="{$codeSystemId}" url=""/>
                     {
                        for $groupItem in $item
                        return
                        <concept>
                           {
                           $groupItem/@*,
                           if ($groupItem/name()='exception') then
                              attribute exception {'true'}
                           else()
                           ,
                           <designation use="pref" languageCode="{$decorFile/project/@defaultLanguage}" count="{count(tokenize($groupItem/@displayName,'\s'))}" length="{string-length($groupItem/@displayName)}">{$groupItem/@displayName/string()}</designation>
                           }
                        </concept>
                       }
                  </include>
               else()
               }
               </compose>
            </valueSet>
         return
         (
         <valueSetInfo project="{$projectCollection}" name="{$valueSet/@name}"/>,
          xmldb:store(concat('/db/apps/terminology-data/valueset-stable-data/',$projectCollection),concat(replace($valueSet/@name,' ',''),'.xml'),$valueSetComplete) 
         )
   else()

}
</collections>
</report>