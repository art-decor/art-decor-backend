xquery version "3.0";
(:
	Copyright (C) 2011-2017 Art Decor Expert Group art-decor.org
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";

declare namespace fhir= "http://hl7.org/fhir";





let $governanceGroupId := 'ihtsdo'

let $xsltParameters     :=  <parameters></parameters>

let $snomed   := collection(concat($get:strCodesystemStableData,'/ihtsdo'))/codeSystem

let $refsetList := distinct-values(collection(concat($get:strCodesystemStableData,'/ihtsdo'))//refset/@refsetId)



return
for $refsetId in $refsetList
let $refsetName := collection(concat($get:strCodesystemStableData,'/ihtsdo'))//concept[@code=$refsetId]/designation[@use='pref'][1]
let $refsetComplete := 
   <valueSet id="{$refsetId}" url="http://art-decor.org/fhir/ValueSet/4711.{$refsetId}" oid="4711.{$refsetId}" statusCode="active" effectiveDate="2017-09-30" experimental="true" version="2017-09-30">
      <name>{$refsetName/text()}</name>
      <publisher>SNOMED International</publisher>
      <contact>
      <telecom system="url" value="http://ihtsdo.org"/>
      </contact>
      <description>This is a test conversion of SNOMED simple refsets to FHIR valuesets</description>
      <compose>
         <include>
            {collection(concat($get:strCodesystemStableData,'/external/snomed'))//concept[refsets//refset/@refsetId=$refsetId]}
         </include>
      </compose>
   </valueSet>
return
xmldb:store('/db/apps/terminology-data/valueset-stable-data/external/snomed',concat($refsetId,'.xml'),$refsetComplete)



