xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adterminology       = "http://art-decor.org/ns/terminology" at "../api/api-terminology.xqm";

let $codeSystemId       := if (request:exists()) then request:get-parameter('codeSystem','') else ('')
let $code               := if (request:exists()) then util:unescape-uri(request:get-parameter('code',''),'UTF-8') else ()
let $languages          := if (request:exists()) then request:get-parameter('languages','') else ('')
let $preferred          := if (request:exists()) then request:get-parameter('preferred','') else ('')
let $length          := if (request:exists()) then request:get-parameter('length','') else ('')
let $compose := request:get-data()/compose



(:let $codeSystemId       := '2.16.840.1.113883.6.96'
let $code               := '320141001'
let $languages          := ''
let $preferred          := ''
let $length             := ''

let $compose :=
<compose><include>
                    <system value="http://snomed.info/sct" oid="2.16.840.1.113883.6.96"/><filter>
                    <property value="concept" uri="http://snomed.info/id/127489000"/>
                    <op value="descendent-of"/>
                    <value value="386098006" display="Respiratory sympathomimetic agent"/>
                </filter><filter>
                    <property value="concept" uri="http://snomed.info/id/127489000"/>
                    <op value="descendent-of"/>
                    <value value="320073005" display="Selective beta-2 adrenoceptor stimulants"/>
                </filter><filter>
                    <property value="Has manufactured dose form" uri="http://snomed.info/id/411116001"/>
                    <op value="="/>
                    <value value="385288002" display="Inhalation dosage form"/>
                </filter><filter>
                    <property value="Has manufactured dose form" uri="http://snomed.info/id/411116001"/>
                    <op value="="/>
                    <value value="422151007" display="Breath activated powder inhaler"/>
                </filter>
                </include><exclude>
                    <system value="http://snomed.info/sct" oid="2.16.840.1.113883.6.96"/>
                </exclude></compose>:)

return
  adterminology:getConceptForLanguages($codeSystemId,$code,$preferred,$languages,$length,$compose)