xquery version "3.0";
(:
	Copyright (C) 2011-2017 Art Decor Expert Group art-decor.org
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";

declare namespace fhir= "http://hl7.org/fhir";





let $governanceGroupId := request:get-parameter('id','')
let $collection   := collection(concat($get:strCodesystemAuthoringData,'/',$governanceGroupId,'/data'))

let $codeSystemList :=
   for $codeSystem in $collection/codeSystem
   order by $codeSystem/name[1]
   return
   <codeSystem>
   {
   $codeSystem/@*,
   $codeSystem/name
   }</codeSystem>

return
<codeSystemList>{$codeSystemList}</codeSystemList>