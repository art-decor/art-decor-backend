xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adterminology       = "http://art-decor.org/ns/terminology" at "../api/api-terminology.xqm";

let $context       := if (request:exists()) then request:get-parameter('context','') else ('')
let $codeSystemIds       := if (request:exists()) then request:get-parameter('codeSystemIds','') else ('')
let $searchLanguages     := if (request:exists()) then request:get-parameter('languages','') else ('')
let $searchString       := if (request:exists()) then util:unescape-uri(request:get-parameter('string',''),'UTF-8') else ()
let $statusCodes        := if (request:exists()) then tokenize(normalize-space(request:get-parameter('statusCode','active')),'\s') else ()
let $searchScope        := if (request:exists()) then tokenize(normalize-space(request:get-parameter('scope','description')),'\s') else ()
let $ancestors          := if (request:exists()) then tokenize(util:unescape-uri(request:get-parameter('ancestors',''),'UTF-8'),'\s') else ()
let $refsets            := if (request:exists()) then tokenize(util:unescape-uri(request:get-parameter('refsets',''),'UTF-8'),'\s') else ()



(:
let $codeSystemId       := ''
let $searchLanguage     := ''
let $searchString       := 'lo pa ba'
let $statusCodes        := ''
let $searchScope        := ''
let $ancestors          := ''
let $refsets            := '':)


let $maxResults        :=xs:integer('20')


return
    adterminology:searchDesignation($context,$codeSystemIds, $searchLanguages, $searchString,$maxResults, $statusCodes, $searchScope,$ancestors,$refsets,())