xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adterminology       = "http://art-decor.org/ns/terminology" at "../api/api-terminology.xqm";

let $oid               := if (request:exists()) then util:unescape-uri(request:get-parameter('oid',''),'UTF-8') else ()
let $languages          := if (request:exists()) then request:get-parameter('languages','') else ('')
let $preferred          := if (request:exists()) then request:get-parameter('preferred','') else ('')

let $url :=''
(:let $codeSystemId       := '2.16.840.1.113883.6.96'
let $language           := ''
let $code               := '372897005':)

return
  (:  adterminology:getRawConcept($codeSystemId,$code):)
  adterminology:getValueSetForLanguages($url,$oid,$preferred,$languages)