xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adterminology       = "http://art-decor.org/ns/terminology" at "../api/api-terminology.xqm";

let $codeSystemId       := if (request:exists()) then request:get-parameter('codeSystem','') else ('')
let $code               := if (request:exists()) then util:unescape-uri(request:get-parameter('code',''),'UTF-8') else ()
let $languages          := if (request:exists()) then request:get-parameter('languages','') else ('')
let $preferred          := if (request:exists()) then request:get-parameter('preferred','') else ('')
let $length          := if (request:exists()) then request:get-parameter('length','') else ('')

(:let $codeSystemId       := '2.16.840.1.113883.6.96'
let $language           := ''
let $code               := '372897005':)
let $compose         := ()
return
  (:  adterminology:getRawConcept($codeSystemId,$code):)
  adterminology:getConceptForLanguages($codeSystemId,$code,$preferred,$languages,$length,$compose)