xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adterminology       = "http://art-decor.org/ns/terminology" at "../api/api-terminology.xqm";
import module namespace get      = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";


let $codeSystemId       := '2.16.840.1.113883.6.96'
let $code               := '320141001'
let $languages          := ''
let $preferred          := ''
let $length             := ''

let $compose :=
<compose>
   <include home="138875005">
      <system value="http://snomed.info/sct" oid="2.16.840.1.113883.6.96"/>
      <filter>
      <property value="concept" uri=""/>
      <op value="is-a"/>
      <value value="123037004" display="Body structure"/>
      </filter>
   </include>
   <include home="138875005">
      <system value="http://snomed.info/sct" oid="2.16.840.1.113883.6.96"/>
      <filter>
      <property value="concept" uri=""/>
      <op value="is-a"/>
      <value value="71388002" display="Procedure"/>
      </filter>
   </include>
   <include home="138875005">
      <system value="http://snomed.info/sct" oid="2.16.840.1.113883.6.96"/>
      <concept code="734146004" display="OWL ontology namespace"/>
   </include>
</compose>


let $query :=
               (: handle per codesystem :)
               for $includes in $compose/include
               let $codeSystemOid   := $includes/system/@oid
               group by $codeSystemOid
               return
                  for $include in $includes
                     let $collectionPath     := util:collection-name(collection($get:strTerminologyStableData)//codeSystem[@oid=$codeSystemOid])
                     let $codeSystemPredicate  := 
                                                if (count(collection($collectionPath)/codeSystem) gt 1 and not($include/filter or $include/concept)) then
                                                   concat('codeSystem[@oid=''',$codeSystemOid,''']')
                                                else ()
                     let $ancestorPredicate     := if (count(collection($collectionPath)/codeSystem) gt 1 and $include/filter) then
                                                      concat('[ancestor::codeSystem/@oid=''',$codeSystemOid,''']')
                                                      else()
                     (: matching excludes i.e. same codesystem:)                       
                     let $exclusions      := $compose/exclude[system/@oid=$codeSystemOid]
                     let $exclPredicates  :=
                                             if ($exclusions/concept) then
                                                concat('[not(@code=(''',string-join($exclusions/concept/@code,''','''),'''))]')
                                             else
                                                adterminology:getPredicatesForExclude($exclusions)
                     return
                     (:concepts:)
                     if ($include/concept) then
                        let $concepts        := $include/concept/@code
                        let $conceptSelect   := if ($concepts) then concat('[@code=(''',string-join($concepts,''','''),''')]') else()                    
                        return
                           concat('collection(''',$collectionPath,''')/',$codeSystemPredicate,'/concept',$conceptSelect,string-join($exclPredicates,''),$ancestorPredicate)
                     else                           
                     (:filters:)
                        let $predicates      := adterminology:getPredicatesForInclude($include)
                        return
                              concat('collection(''',$collectionPath,''')/',$codeSystemPredicate,'/concept',string-join($predicates,''),string-join($exclPredicates,''),$ancestorPredicate)

return
<result>{$query}</result>