xquery version "3.0";
(:
	Copyright (C) 2011-2017 Art Decor Expert Group art-decor.org
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";





let $codeSystemId := request:get-parameter('codeSystem','')
(:let $codeSystemId :='2.16.840.1.113883.5.1057':)
let $codeSystem   := collection($get:strCodesystemStableData)//codeSystem[@oid=$codeSystemId]

let $xsltParameters     :=  <parameters></parameters>

let $response :=
   if ($codeSystem/@structure='network') then
      transform:transform($codeSystem ,xs:anyURI(concat('xmldb:exist://',$get:strTerminology,'/resources/stylesheets/expand-CADTS-codeSystem.xsl')), $xsltParameters)
   else
      $codeSystem

return
$response