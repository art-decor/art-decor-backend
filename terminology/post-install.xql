xquery version "3.0";
(:
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace art               = "http://art-decor.org/ns/art" at "../art/modules/art-decor.xqm";
import module namespace adserver          = "http://art-decor.org/ns/art-decor-server" at "../art/api/api-server-settings.xqm";
import module namespace adpfix            = "http://art-decor.org/ns/art-decor-permissions" at "../art/api/api-permissions.xqm";
import module namespace xmldb             = "http://exist-db.org/xquery/xmldb";
import module namespace sm                = "http://exist-db.org/xquery/securitymanager";
import module namespace repo              = "http://exist-db.org/xquery/repo";
import module namespace claml             = "http://art-decor.org/ns/terminology/claml" at "claml/api/api-claml.xqm";
import module namespace snomedpfix        = "http://art-decor.org/ns/snomedct-permissions" at "snomed/api/api-permissions.xqm";
import module namespace icapfix           = "http://art-decor.org/ns/ica-permissions" at "ica/api/api-permissions.xqm";
import module namespace terminologypfix   = "http://art-decor.org/ns/terminology-permissions" at "api/api-permissions.xqm" ;
import module namespace terminologydemopfix     = "http://art-decor.org/ns/terminologydemo-permissions" at "nictiz-demo/api/api-permissions.xqm";

(: The following external variables are set by the repo:deploy function :)

(: file path pointing to the exist installation directory :)
declare variable $home external;
(: path to the directory containing the unpacked .xar package :)
declare variable $dir external;
(: the target collection into which the app is deployed :)
declare variable $target external;
(:install path for art (/db, /db/apps), no trailing slash :)
declare variable $root := repo:get-root();

(: explicitly set the Orbeon version on servers that did not have the setting yet. This facilitates the ability to configure it in the art-settings :)
let $updatexforms           := adserver:setServerOrbeonVersion(adserver:getServerOrbeonVersion())
(: upgrade or downgrade the XForms depending on Orbeon version :)
let $updatexforms           := if (adserver:getServerOrbeonVersion() = '3.9') then adpfix:updateXForms('downgrade') else adpfix:updateXForms('upgrade')

return
(: check if message collection exists, if not then create and set permissions :)
claml:createClaMLIndex(),
icapfix:setICACollectionPermissions(),
icapfix:setICAQueryPermissions(),
terminologypfix:setTerminologyQueryPermissions(),
terminologypfix:setTerminologyStableCollectionPermissions(),
terminologypfix:setTerminologyAuthoringCollectionPermissions(),
terminologydemopfix:setTerminologyDemoQueryPermissions(),
try { art:saveFormResources('terminology',art:mergeLocalLanguageUpdates('terminology')) } catch * {()}
