xquery version "3.0";
(:
:   Copyright (C) 2013-2016 ART-DECOR expert group art-decor.org
:   
:   This program is free software; you can redistribute it and/or modify it under the terms of the
:   GNU Lesser General Public License as published by the Free Software Foundation; either version
:   2.1 of the License, or (at your option) any later version.
:   
:   This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
:   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
:   See the GNU Lesser General Public License for more details.
:   
:   The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:
:)
module namespace terminologypfix     = "http://art-decor.org/ns/terminology-permissions";
declare namespace sm            = "http://exist-db.org/xquery/securitymanager";

(:  Mode            Octal
 :  rw-r--r--   ==  0644
 :  rw-rw-r--   ==  0664
 :  rwxr-xr--   ==  0754
 :  rwxr-xr-x   ==  0755
 :  rwxrwxr-x   ==  0775
 :)

(:install path for art (normally /db/apps/), includes trailing slash :)
declare variable $terminologypfix:root                      := repo:get-root();
declare variable $terminologypfix:strTerminology            := concat($terminologypfix:root,'/terminology');
declare variable $terminologypfix:strTerminologyData        := concat($terminologypfix:root,'/terminology-data');
declare variable $terminologypfix:strAuthoringModules       := concat($terminologypfix:strTerminology,'/modules');
declare variable $terminologypfix:strAuthoringCollections   := ('codesystem-authoring-data','valueset-authoring-data','conceptmap-authoring-data');
declare variable $terminologypfix:strStableCollections      := ('codesystem-stable-data','valueset-stable-data','conceptmap-stable-data', 'valueset-expansion-data');

declare function terminologypfix:setTerminologyQueryPermissions() {
    terminologypfix:checkIfUserDba(),
    
    terminologypfix:setPermissions($terminologypfix:strTerminology, 'admin:terminology', sm:octal-to-mode('0775'), 'admin:terminology', sm:octal-to-mode('0755'))
    ,
    sm:chmod(xs:anyURI($terminologypfix:strTerminology), 'rwxrwsr-x')
    ,
    for $query in xmldb:get-child-resources(xs:anyURI($terminologypfix:strAuthoringModules))
    return (
        if (starts-with($query,('check','get','is-','retrieve','search','view','validate'))) then () else (
            sm:chmod(xs:anyURI(concat($terminologypfix:strAuthoringModules,'/',$query)), sm:octal-to-mode('0754'))
        )
    )
};

declare function terminologypfix:setTerminologyAuthoringCollectionPermissions() {
    terminologypfix:checkIfUserDba(),
    for $authoringCollection in $terminologypfix:strAuthoringCollections
    return
        terminologypfix:setPermissions(concat($terminologypfix:strTerminologyData,'/',$authoringCollection), 'admin:terminology', 'rwsrwsr-x', 'admin:terminology', 'rwsrwsr-x')
};

declare function terminologypfix:setTerminologyStableCollectionPermissions() {
    terminologypfix:checkIfUserDba(),
    for $stableCollection in $terminologypfix:strStableCollections
    let $coll1  := xmldb:create-collection($terminologypfix:strTerminologyData, concat($stableCollection, '/cache'))
    let $coll2  := xmldb:create-collection($terminologypfix:strTerminologyData, concat($stableCollection, '/projects'))
    return (
        terminologypfix:setPermissions($coll1, 'admin:terminology', 'rwsrwsr-x', 'admin:terminology', 'rwsrwsr-x'),
        terminologypfix:setPermissions($coll2, 'admin:terminology', 'rwsrwsr-x', 'admin:terminology', 'rwsrwsr-x')
    )
};

(:
:   Helper function with recursion for terminologypfix:setSCTExtensionCollectionPermissions()
:)
declare %private function terminologypfix:setPermissions($path as xs:string, $collown as xs:string, $collmode as xs:string, $resown as xs:string, $resmode as xs:string) {
    sm:chown(xs:anyURI($path),$collown),
    sm:chmod(xs:anyURI($path),$collmode),
    sm:clear-acl(xs:anyURI($path)),
    for $res in xmldb:get-child-resources(xs:anyURI($path))
    return (
        sm:chown(xs:anyURI(concat($path,'/',$res)),$resown),
        sm:chmod(xs:anyURI(concat($path,'/',$res)),$resmode),
        sm:clear-acl(xs:anyURI(concat($path,'/',$res)))
    )
    ,
    for $collection in xmldb:get-child-collections($path)
    return
        terminologypfix:setPermissions(concat($path,'/',$collection), $collown, $collmode, $resown, $resmode)
};

declare %private function terminologypfix:checkIfUserDba() {
    if (sm:is-dba((sm:id()//sm:real/sm:username/text())[1])) then () else (
        error(QName('http://art-decor.org/ns/art-decor-permissions', 'NotAllowed'), concat('Only dba user can use this module. ',(sm:id()//sm:real/sm:username/text())[1]))
    )
};