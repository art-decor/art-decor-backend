xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
module namespace adterminology   = "http://art-decor.org/ns/terminology";
declare namespace       json     =  "http://www.json.org";
import module namespace art      = "http://art-decor.org/ns/art" at "../../art/modules/art-decor.xqm";
import module namespace get      = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace claml    = "http://art-decor.org/ns/terminology/claml" at "../claml/api/api-claml.xqm";
import module namespace snomed   = "http://art-decor.org/ns/terminology/snomed" at "../snomed/api/api-snomed.xqm";

(:~
:   All functions support their own override, but this is the fallback for the maximum number of results returned on a search
:)
declare variable $adterminology:maxResults := xs:integer('50');
(:~
:   Returns lucene config xml for a sequence of strings. The search will find yield results that match all terms+trailing wildcard in the sequence
:   Example output:
:   <query>
:       <bool>
:           <wildcard occur="must">term1*</wildcard>
:           <wildcard occur="must">term2*</wildcard>
:       </bool>
:   </query>
:
:   @param $searchTerms required sequence of terms to look for
:   @return lucene config
:   @since 2014-06-06
:   @see http://exist-db.org/exist/apps/doc/lucene.xml?q=lucene&field=all&id=D2.2.4.28#D2.2.4.28
:)
declare function adterminology:getSimpleLuceneQuery($searchTerms as xs:string+) as element() {
    <query>
        <bool>
        {
            for $term in $searchTerms
            return
                if (matches($term,'^[a-z|0-9]')) then
                    <wildcard occur="must">{concat($term,'*')}</wildcard>
                else if (matches($term,'^[A-Z]')) then
                    <term occur="must">{lower-case($term)}</term>
                else()
        }
        </bool>
    </query>
};

(:~
:   Returns lucene options xml that instruct filter-rewrite=yes
:)
declare function adterminology:getSimpleLuceneOptions() as element() {
    <options>
        <default-operator>and</default-operator>
        <phrase-slop>0</phrase-slop>
        <leading-wildcard>no</leading-wildcard>
        <filter-rewrite>yes</filter-rewrite>
    </options>
};

declare function adterminology:isValidSearch($searchString as xs:string?) as xs:boolean {
    if (matches($searchString,'^[a-z|0-9]') and string-length($searchString)>3 and string-length($searchString)<40) 
    then true()
    else if (matches($searchString,'^[A-Z]') and string-length($searchString)>1 and string-length($searchString)<40) 
    then true()
    else false()
};

declare function adterminology:searchDesignation($context as xs:string?, $loincContext as xs:string?, $codeSystemIdList as xs:string?, $languageList as xs:string?,$searchString as xs:string?, $maxResults as xs:integer?, $statusCodes as xs:string*, $ancestors as xs:string*, $isa as xs:string?, $refsets as xs:string*,$compose as element()?) as element() {
   let $maxResults         := if ($maxResults) then $maxResults else $adterminology:maxResults
   let $validSearch        := adterminology:isValidSearch($searchString)
   let $searchTerms        := tokenize($searchString,'\s')
   let $options            := adterminology:getSimpleLuceneOptions()
   let $query              := adterminology:getSimpleLuceneQuery($searchTerms)
   let $ancestors          := $ancestors[not(.='')]
   let $refsets            := $refsets[not(.='')]
   let $isa                := $isa[not(.='')]
   let $codeSystemIds      := tokenize($codeSystemIdList,'\s')
   let $languages          := tokenize($languageList,'\s')

   let $dataCollectionStr  := if (string-length($context) gt 1) then concat($get:strCodesystemStableData,'/',$context) else $get:strCodesystemStableData

   let $loincPredicate     := if ($loincContext='codes') then '[../loinc/@isLoinc]'
                              else if ($loincContext='parts') then '[../loinc/@isPart]'
                              else if ($loincContext='answers') then '[../loinc/@isAnswer]'
                              else ''
   
(:raw query result:)
let $q              := 'collection($dataCollectionStr)//designation[ft:query(., $query, $options)]'
let $q              := if ($context='external/regenstrief') then $q || $loincPredicate else $q
let $q              := if (empty($ancestors)) then $q else $q || '[../ancestor = $ancestors]'
let $q              := if (empty($isa)) then $q else $q || '[../ancSlf = $isa]'
let $q              := if (empty($refsets)) then $q else $q || '[..//@refsetId = $refsets ]'
let $q              := if (empty($statusCodes)) then $q || '[../@statusCode = (''active'', ''draft'')]' else $q || '[../@statusCode = $statusCodes]'
let $result         :=
    if ($validSearch) then (
        let $r      := util:eval($q)
        return
            if (count($r) le 25000) then $r else (
                <designation>Over 25000 results, please refine search</designation>
            )
    ) else ()

(:order result by count and length:)
let $result         := 
      if (empty($languages)) then
       for $desc in $result
       order by xs:integer($desc/@count),xs:integer($desc/@length)
       return $desc
      else
       for $desc in $result[@lang=$languages]
       order by xs:integer($desc/@count),xs:integer($desc/@length)
       return $desc

(:group result by conceptId:)
let $result         :=
    for $desc in $result
    let $cc := $desc/parent::concept
    group by $cc    
    order by xs:integer($desc[1]/@count),xs:integer($desc[1]/@length)
    return $desc[1]
  
let $count          := count($result)
let $current        := if ($count>$maxResults) then $maxResults else ($count)
return
    <result current="{$current}" count="{$count}">
    {
     if ($codeSystemIdList='2.16.840.1.113883.6.96') then
        for $res in subsequence($result,1,$maxResults)
        let $concept :=$res/parent::concept
        return
        <designation codeSystem="2.16.840.1.113883.6.96" codeSystemName="{$concept/designation[@use='fsn'][1]/text()}" code="{$concept/@code}" json:array="true">{$res/text()}</designation>
     else
        for $res in subsequence($result,1,$maxResults)
        let $codeSystem := $res/ancestor::browsableCodeSystem
        let $concept := $res/parent::concept
        let $text := if ($res/component) then string-join($res/*/text(),' ') else if ($res/@use='rel') then $concept/designation[@use='pref'][1]/text() else $res/text()
        return
        <designation codeSystem="{$codeSystem/@oid}" codeSystemName="{$codeSystem/name[1]}" code="{$concept/@code}" json:array="true">
        {
         if ($compose) then
            if (adterminology:validateAgainstCompose($codeSystem/@oid,$concept/@code,$compose)) then
               attribute inSet {'true'}
            else()
         else (),
         $text
        }
        </designation>
    }
    </result>
};

declare function adterminology:getRawConcept($codeSystemId as xs:string?,$code as xs:string?) as element(concept)* {
    let $concept     := collection($get:strCodesystemStableData)//concept[@code=$code][ancestor::browsableCodeSystem/@oid=$codeSystemId]
    
    return
    $concept
};

declare function adterminology:searchCode($context as xs:string?,$codeSystemIdList as xs:string?,$code as xs:string?, $maxResults as xs:integer?, $statusCodes as xs:string*) as element() {
   let $maxResults         := if ($maxResults) then $maxResults else $adterminology:maxResults
   let $codeSystemIds      := tokenize($codeSystemIdList,'\s')

   let $dataCollectionStr  := if(string-length($context) gt 1) then
                                 concat($get:strCodesystemStableData,'/',$context)
                              else
                                 $get:strCodesystemStableData

(:raw query result:)
let $result         :=
    if (string-length($code) gt 0) then (
        (:collection($dataCollectionStr)//concept[@code=$code][../@statusCode = ('active', 'draft')]:)
        let $r  := collection($dataCollectionStr)//concept[@code=$code]
        return $r[ancestor::browsableCodeSystem[1]/@statusCode = ('active', 'draft')]
    )
    else ()
  
let $count          := count($result)
let $current        := if ($count>$maxResults) then $maxResults else ($count)
return
    <result current="{$current}" count="{$count}">
    {
     if ($codeSystemIdList='2.16.840.1.113883.6.96') then
        for $concept in subsequence($result,1,$maxResults)
        let $designationText := $concept/designation[@use='pref'][1]/text()
        order by $designationText ascending
        return
        <designation codeSystem="2.16.840.1.113883.6.96" codeSystemName="{$concept/designation[@use='fsn'][1]/text()}" code="{$concept/@code}" json:array="true">{$designationText}</designation>
     else
        for $concept in subsequence($result,1,$maxResults)
        let $codeSystem := $concept/parent::browsableCodeSystem
        let $designationText := $concept/designation[@use='pref'][1]/text()
        order by $designationText ascending
        return
        <designation codeSystem="{$codeSystem/@oid}" codeSystemName="{$codeSystem/name[1]}" code="{$concept/@code}" json:array="true">{$designationText}</designation>
    }
    </result>
};

declare function adterminology:validateAgainstCompose($codeSystemId as xs:string,$code as xs:string,$compose as element()) as xs:boolean {
   let $collectionPath     := util:collection-name(collection($get:strCodesystemStableData)//browsableCodeSystem[@oid=$codeSystemId])
   let $ancestorPredicate  := if (count(collection($collectionPath)/browsableCodeSystem) gt 1) then
                                 concat('[ancestor::browsableCodeSystem/@oid=''',$codeSystemId,''']')
                              else()
   return
   (:first check if there's an include with the specified codesystem:)
   if ($compose/include/system/@oid=$codeSystemId) then
         (:check if the concept is explicitely included first:)
         if ($compose/include[system/@oid=$codeSystemId]/concept/@code=$code) then
               xs:boolean('true')
         else
               (: next check if the concept is excluded :)
               if ($compose/exclude[system/@oid=$codeSystemId]/concept or $compose/exclude[system/@oid=$codeSystemId]/filter) then
                     (:check if concept is explicitely excluded:)
                     if ($compose/exclude[system/@oid=$codeSystemId]/concept/@code=$code) then
                           xs:boolean('false')
                     else
                     (:check exclusion filters:)
                     let $exclusions      := $compose/exclude[system/@oid=$codeSystemId][filter]
                     return
                     if ($exclusions) then
                           let $exclPredicates  := adterminology:getPredicatesForInclude($exclusions)
                           return
                                if (util:eval(concat('collection(''',$collectionPath,''')//concept[@code=''',$code,''']',string-join($exclPredicates,''),$ancestorPredicate))) then 
                                    xs:boolean('false')
                                else
                                    (:check include filters:)
                                    adterminology:validateInclude($codeSystemId,$code,$compose,$collectionPath,$ancestorPredicate)
                      else
                           (:check include filters:)
                           adterminology:validateInclude($codeSystemId,$code,$compose,$collectionPath,$ancestorPredicate)
                          
               else        
                     (:check include filters:)
                     adterminology:validateInclude($codeSystemId,$code,$compose,$collectionPath,$ancestorPredicate)
   else xs:boolean('false')
};

declare function adterminology:validateSearchResult($result as element()) as element(){
   let $compose := $result/compose
   return
   <result>
      {
      $result/@*,
      for $designation in $result/designation
      return
      <designation json:array="true">
      {
      $designation/@*[not(name()='inSet')],
      if ($compose) then
      if (adterminology:validateAgainstCompose($designation/@codeSystem,$designation/@code,$compose)) then
      attribute inSet {'true'}
      else()
      else (),
      $designation/text()
      }
      </designation>
      
      }
   </result>
};

declare %private function adterminology:validateInclude($codeSystemId as xs:string,$code as xs:string?,$compose as element(),$collectionPath as xs:string,$ancestorPredicate as xs:string?) as xs:boolean {
   (:concept includes only first:)
   if ($compose/include[system/@oid=$codeSystemId]/concept and not($compose/include[system/@oid=$codeSystemId]/filter)) then
         if ($code=$compose/include/concept/@code) then
            xs:boolean('true')
         else xs:boolean('false')
   (:filters and possible concepts:)
   else if ($compose/include[system/@oid=$codeSystemId]/filter) then
         (:check if there're also concepts:)
         if ($compose/include[system/@oid=$codeSystemId]/concept) then
            if ($code=$compose/include/concept/@code) then
               xs:boolean('true')
            else
               (: now check filters:)
               let $includes        := $compose/include[system/@oid=$codeSystemId]
               let $queries         := 
                                       for $include in $includes[filter]
                                       let $predicates      :=adterminology:getPredicatesForInclude($include)
                                       return
                                       concat('collection(''',$collectionPath,''')//concept[@code=''',$code,''']',string-join($predicates,''),$ancestorPredicate)
               return
                    if( util:eval(string-join($queries,'|'))) then
                        xs:boolean('true') 
                    else 
                        xs:boolean('false')
         (:just filters:)
         else
            (: check filters:)
            let $includes        := $compose/include[system/@oid=$codeSystemId]
            let $queries         := 
                                    for $include in $includes
                                    let $predicates      :=adterminology:getPredicatesForInclude($include)
                                    return
                                    concat('collection(''',$collectionPath,''')//concept[@code=''',$code,''']',string-join($predicates,''),$ancestorPredicate)
            return
                 if( util:eval(string-join($queries,'|'))) then
                     xs:boolean('true') 
                 else 
                     xs:boolean('false')

   else
         if( util:eval(concat('collection(''',$collectionPath,''')//concept[@code=''',$code,''']',$ancestorPredicate))) then 
               xs:boolean('true')
         else xs:boolean('false')
};

declare function adterminology:getCodeSystemInfo($codeSystemId as xs:string){
   let $codeSystem   := (collection($get:strCodesystemStableData)//browsableCodeSystem[@oid=$codeSystemId])[not(@content='not-present')][1]
   let $iconpath     := util:collection-name($codeSystem)
   let $icon         := util:binary-doc(concat($iconpath, '/icon.png')) || util:binary-doc(concat($iconpath, '../icon.png')) || util:binary-doc(concat($iconpath, '../../icon.png'))
   return
   <browsableCodeSystem>
   {
      $codeSystem/@*,
      $codeSystem/name,
      $codeSystem/desc,
      $codeSystem/language,
      $codeSystem/license,
      if (string-length($icon) > 0) then <logo>{$icon}</logo> else ()
   }
   </browsableCodeSystem>
};

declare function adterminology:getConceptForLanguages($codeSystemId as xs:string?,$code as xs:string?,$preferredLanguage as xs:string?,$languageList as xs:string?,$children as xs:string?,$compose as element()?) as element(concept)* {
    let $concepts    := collection($get:strCodesystemStableData)//concept[@code=$code]
    let $concept     := $concepts[ancestor::browsableCodeSystem/@oid=$codeSystemId]
    let $languageIds := tokenize($languageList,'\s')
    let $numberOfChildren := if ($children castable as xs:integer) then xs:integer($children) else xs:integer('20') 
                           
    return
    <concept>
    {
        $concept/@*,
        if ($compose) then
          if (adterminology:validateAgainstCompose($codeSystemId,$code,$compose)) then
             attribute inSet {'true'}
          else()
        else ()
        ,
        for $designation in $concept/designation[@lang=$preferredLanguage]
        return
            <designation json:array="true">
             {
             $designation/@*,
             $designation/*,
             $designation/text()
             }
           </designation>
       ,
       for $designation in $concept/designation[@lang=$languageIds][not(@lang=$preferredLanguage)]
       return
           <designation json:array="true">
             {
             $designation/@*,
             $designation/*,
             $designation/text()
             }
           </designation>
       ,
       $concept/definition[@lang=$preferredLanguage]
       ,
       $concept/definition[@lang=$languageIds][not(@lang=$preferredLanguage)]
       ,
       for $relgrp in $concept/relGrp
       order by $relgrp/@grpId
       return
         <relGrp json:array="true">
         {
           $relgrp/@*,
           for $relation in $relgrp/relation
           return
           <relation json:array="true">
              {
              $relation/@*,
              $relation/ancest,
              if($relation/label[@lang=$preferredLanguage]) then
                 $relation/label[@lang=$preferredLanguage]
              else
                 $relation/label[1],
              if($relation/typeLabel[@lang=$preferredLanguage]) then
                 $relation/typeLabel[@lang=$preferredLanguage]
              else
                 $relation/typeLabel[1]
              }
           </relation>
          }
         </relGrp>
       ,
       $concept/loinc
       ,
       $concept/ancestor
       ,
       <children count="{count($concept/child)}" start="1">
       {
            for $child in subsequence($concept/child,1,$numberOfChildren)
            return
                <child json:array="true">
                  {
                  $child/@*,
                   if ($compose) then
                     if (adterminology:validateAgainstCompose($codeSystemId,$child/@cCode,$compose)) then
                        attribute inSet {'true'}
                     else()
                   else (),
                  if($child/label[@lang=$preferredLanguage]) then
                     $child/label[@lang=$preferredLanguage][1]
                  else
                     $child/label[1]
                  }
                </child>
       }
       </children>
       ,
       for $parent in $concept/parent
       return
            <parent json:array="true">
            {
                $parent/@*,
                if ($compose) then
                   if (adterminology:validateAgainstCompose($codeSystemId,$parent/@pCode,$compose)) then
                      attribute inSet {'true'}
                   else()
                else (),
                if($parent/label[@lang=$preferredLanguage]) then
                   $parent/label[@lang=$preferredLanguage][1]
                else
                   $parent/label[1]
             }
            </parent>
       ,
       for $association in $concept/association
       return
         <association json:array="true">
           {
              $association/@*,
              if($association/refsetLabel[@lang=$preferredLanguage]) then
                 $association/refsetLabel[@lang=$preferredLanguage]
              else
                 $association/refsetLabel[1],
              if($association/targetLabel[@lang=$preferredLanguage]) then
                 $association/targetLabel[@lang=$preferredLanguage]
              else
                 $association/targetLabel[1]
           }
         </association>
       ,
       for $attributeValue in $concept/attributeValue
       return
        <attributeValue json:array="true">
          {
             $attributeValue/@*,
             if($attributeValue/refsetLabel[@lang=$preferredLanguage]) then
                $attributeValue/refsetLabel[@lang=$preferredLanguage]
             else
                $attributeValue/refsetLabel[1],
             if($attributeValue/valueLabel[@lang=$preferredLanguage]) then
                $attributeValue/valueLabel[@lang=$preferredLanguage]
             else
                $attributeValue/valueLabel[1]
          }
        </attributeValue>
       ,
       for $props in $concept/property
       return
            <property json:array="true">
            {
                   $props/@*,
                   $props/*
            }
            </property>
       ,
       for $valueSetLink in $concept/valueSet
       order by $valueSetLink/@type
       let $valueSet := collection($get:strValuesetStableData)//valueSet[@code=$valueSetLink/@id]
       return
            <valueSetLink type="{$valueSetLink/@type}" json:array="true">
            {
               $valueSetLink/context,
               $valueSet
            }
            </valueSetLink>
       ,
       for $refset in $concept//refset
       return
            <refset refsetId="{$refset/@refsetId}" json:array="true"/>
       ,
       for $refsetId in distinct-values($concept//map/@refsetId)
       let $maps := $concept//map[@refsetId=$refsetId]
       return
            if (string-length($maps[1]/@correlationId) = 0) then
              <simpleMap refsetId="{$maps[1]/@refsetId}" mapTarget="{$maps[1]/@mapTarget}" json:array="true"/>
            else
              for $mapGroup in distinct-values($maps/@mapGroup)
              let $mapGroups := $maps[@mapGroup=$mapGroup]
              order by $mapGroup
              return
               <mapGroup refsetId="{$refsetId}" json:array="true">
               {
                    for $map in $mapGroups
                    order by $map/@mapPriority
                    return
                      <map json:array="true">
                         {$map/@*}
                      </map>
               }
               </mapGroup>
       ,
       for $smaps in $concept/mapping
           return
               <mapping json:array="true">
               {
                   $smaps/@*,
                   $smaps/*
               }
               </mapping>
    }
    </concept>
};

declare function adterminology:getConceptChildrenForLanguage($codeSystemId as xs:string?,$code as xs:string?,$preferredLanguage as xs:string?,$start as xs:string?,$length as xs:string?,$compose as element()?) as element(concept)* {
    let $concepts    := collection($get:strCodesystemStableData)//concept[@code=$code]
    let $concept     := $concepts[ancestor::browsableCodeSystem/@oid=$codeSystemId]
    let $childStart  := if ($start castable as xs:integer) then xs:integer($start) else xs:integer('1') 
    let $numberOfChildren := if ($length castable as xs:integer) then xs:integer($length) else xs:integer('20') 
    return
    <children count="{count($concept/child)}" start="{$childStart}">
    {
    for $child in subsequence($concept/child,$childStart,$numberOfChildren)
    return
    <child json:array="true">
      {
      $child/@*,
       if ($compose) then
         if (adterminology:validateAgainstCompose($codeSystemId,$child/@cCode,$compose)) then
            attribute inSet {'true'}
         else()
       else (),
      if($child/label[@lang=$preferredLanguage]) then
         $child/label[@lang=$preferredLanguage][1]
      else
         $child/label[1]
      }
    </child>
    }
    </children>
};


declare function adterminology:searchValueSet($context as xs:string?,$projectIdList as xs:string?, $languageList as xs:string?,$searchString as xs:string?, $maxResults as xs:integer?, $statusCodes as xs:string*) as element() {
   let $maxResults         := if ($maxResults) then $maxResults else $adterminology:maxResults
   let $validSearch        :=     if (matches($searchString,'^[a-z|0-9]') and string-length($searchString)>1 and string-length($searchString)<40)  then true()
                                  else if (matches($searchString,'^[A-Z]') and string-length($searchString)>0 and string-length($searchString)<40) then true()
                                  else false()
   let $searchTerms        := tokenize($searchString,'\s')
   let $options            := adterminology:getSimpleLuceneOptions()
   let $query              := adterminology:getSimpleLuceneQuery($searchTerms)
   let $projectIds      := tokenize($projectIdList,'\s')
   let $languages          := tokenize($languageList,'\s')

   let $dataCollectionStr  := if(string-length($context) gt 1) then
                                 concat($get:strTerminologyData,'/valueset-stable-data/',$context)
                              else
                                 concat($get:strTerminologyData,'/valueset-stable-data')

(:raw query result:)
let $result         :=
    if ($validSearch) then
        if (empty($projectIds)) then
            collection($dataCollectionStr)//name[ft:query(.,$query,$options)]
        else
            collection($dataCollectionStr)//name[ft:query(.,$query,$options)]
              
    else ()

(:order result by count and length:)
let $result         := 
      if (empty($languages)) then
       for $desc in $result
       order by xs:integer($desc/@count),xs:integer($desc/@length)
       return $desc
      else
       for $desc in $result[@lang=$languages]
       order by xs:integer($desc/@count),xs:integer($desc/@length)
       return $desc

  
let $count          := count($result)
let $current        := if ($count>$maxResults) then $maxResults else ($count)
return
    <result current="{$current}" count="{$count}">
    {
        for $res in subsequence($result,1,$maxResults)
        let $valueSet := $res/parent::valueSet
        return
        <name valueSet="{$valueSet/@oid}" publisherName="{$valueSet/publisher[1]}">{$valueSet/name[1]/text()}</name>
    }
    </result>
};

declare function adterminology:getValueSetForLanguages($url as xs:string?,$oid as xs:string?,$preferredLanguage as xs:string?,$languageList as xs:string?) as element(concept)* {
    let $valueSet     := collection(concat($get:strTerminologyData,'/valueset-stable-data'))//valueSet[@oid=$oid][1]
    let $languageIds := tokenize($languageList,'\s')
    let $count :=count($valueSet//concept)
    return
    <valueSet count="{$count}">
    {
    $valueSet/@*,
    $valueSet/name[1],
    $valueSet/publisher[1],
    $valueSet/description,
    <compose>
    {
    for $include in $valueSet/compose/include
    return
    <include json:array="true">
      {
      $include/@*,
      $include/system,
      $include/version,
      $include/filter,
      $include/valueSet,
      if ($count lt 100) then
         $include/concept
      else()
      }
    </include>,
     $valueSet/exclude
     }
     </compose>
    }
    </valueSet>
};

declare function adterminology:getQueryForCompose($compose as element()) as xs:string*{
   (:  includes :)
   (: handle per codesystem :)
   let $queries :=
                  (: handle per codesystem :)
                  for $includes in $compose/include
                  let $codeSystemOid   := $includes/system/@oid
                  group by $codeSystemOid
                  return
                     for $include in $includes
                        let $collectionPath      := util:collection-name(collection($get:strCodesystemStableData)//browsableCodeSystem[@oid=$codeSystemOid])
                        let $codeSystemPredicate := 
                            if (count(collection($collectionPath)/browsableCodeSystem) gt 1 and not($include/filter or $include/concept)) then
                                concat('codeSystem[@oid=''',$codeSystemOid,''']')
                            else ()
                        let $ancestorPredicate   :=
                            if (count(collection($collectionPath)/browsableCodeSystem) gt 1 and $include/filter) then
                                concat('[ancestor::browsableCodeSystem/@oid=''',$codeSystemOid,''']')
                            else()
                        (: matching excludes i.e. same codesystem:)                       
                        let $exclusions      := $compose/exclude[system/@oid=$codeSystemOid]
                        let $exclPredicates  :=
                            if ($exclusions/concept) then
                                concat('[not(@code=(''',string-join($exclusions/concept/code/@value,''','''),'''))]')
                            else
                                adterminology:getPredicatesForExclude($exclusions)
                        return
                            (:concepts:)
                            if ($include/concept) then
                               let $concepts        := $include/concept/@code
                               let $conceptSelect   := if ($concepts) then concat('[@code=(''',string-join($concepts,''','''),''')]') else()                    
                               return
                                  concat('collection(''',$collectionPath,''')/',$codeSystemPredicate,'/concept',$conceptSelect,string-join($exclPredicates,''),$ancestorPredicate)
                            else                           
                            (:filters:)
                               let $predicates      := adterminology:getPredicatesForInclude($include)
                               return
                                     concat('collection(''',$collectionPath,''')/',$codeSystemPredicate,'/concept',string-join($predicates,''),string-join($exclPredicates,''),$ancestorPredicate)
   return
   string-join($queries,'|')
      
};

declare function adterminology:getPredicatesForInclude($include as element()*) as xs:string*{
   for $filter in $include/filter
      return
         if  ($filter/property/@value='parent') then (
            if ($filter/op/@value='=') then
                concat('[parent/@pCode=''',$filter/value/@value,''']')
            else if ($filter/op/@value='in') then
                concat('[parent/@pCode=(',string-join(tokenize($filter/value/@value,','),''','''),')]')
            else ('[false]')
        ) else if ($filter/property/@value='concept') then (
            if ($filter/op/@value='is-a') then
                concat('[ancSlf=''',$filter/value/@value,''']')
            else if ($filter/op/@value='descendent-of') then
                concat('[ancestor=''',$filter/value/@value,''']')
            else ('[false]')
        ) else if ($filter/property/@value='ancestor') then (
            if ($filter/op/@value='=') then
                concat('[ancestor=''',$filter/value/@value,''']')
            else if ($filter/op/@value='in') then
                concat('[ancestor=(',string-join(tokenize($filter/value/@value,','),''','''),')]')
            else ('[false]')
        ) else if ($filter/property[starts-with(@uri,'http://snomed.info/id/')]) then (
            if ($filter/op/@value='=') then
                concat('[relGrp/relation[@typeCode=''',$filter/property/substring-after(@uri,'http://snomed.info/id/'),''' and @destCode=''',$filter/value/@value,''']]')
            else if ($filter/op/@value='in') then
                concat('[relGrp/relation[@typeCode=''',$filter/property/substring-after(@uri,'http://snomed.info/id/'),''' and @destCode=(''',string-join(tokenize($filter/value/@value,','),''','''),''')]]')
            else if ($filter/op/@value='is-a') then
                concat('[relGrp/relation[ancest=''',$filter/value/@value,''' and @typeCode=''',$filter/property/substring-after(@uri,'http://snomed.info/id/'),''']]')
            else ('[false]')
        ) else ('[false]')
      
      (:
      if ($filter/property/@value='parent' and $filter/op/@value='=') then
         concat('[parent/@pCode=''',$filter/value/@value,''']')
      else if ($filter/property/@value='parent' and $filter/op/@value='in') then
         concat('[parent/@pCode=(',string-join(tokenize($filter/value/@value,','),''','''),')]')
      else if ($filter/property/@value='concept' and $filter/op/@value='is-a') then
         concat('[ancSlf=''',$filter/value/@value,''']')
      else if (($filter/property/@value='ancestor' and $filter/op/@value='=') or ($filter/property/@value='concept' and $filter/op/@value='descendent-of')) then
         concat('[ancestor=''',$filter/value/@value,''']')
      else if ($filter/property/@value='ancestor' and $filter/op/@value='in') then
         concat('[ancestor=(',string-join(tokenize($filter/value/@value,','),''','''),')]')
      else if ($filter/property[starts-with(@uri,'http://snomed.info/id/')] and $filter/op/@value='=') then
         concat('[relGrp/relation[@typeCode=''',$filter/property/substring-after(@uri,'http://snomed.info/id/'),''' and @destCode=''',$filter/value/@value,''']]')
      else if ($filter/property[starts-with(@uri,'http://snomed.info/id/')] and $filter/op/@value='in') then
         concat('[relGrp/relation[@typeCode=''',$filter/property/substring-after(@uri,'http://snomed.info/id/'),''' and @destCode=(''',string-join(tokenize($filter/value/@value,','),''','''),''')]]')
      else if ($filter/property[starts-with(@uri,'http://snomed.info/id/')] and $filter/op/@value='is-a') then
         concat('[relGrp/relation[ancest=''',$filter/value/@value,''' and @typeCode=''',$filter/property/substring-after(@uri,'http://snomed.info/id/'),''']]')
      else(concat('[false]', '[', $filter/property/@value, '_', $filter/op/@value, ']'))
      :)
};

declare function adterminology:getPredicatesForExclude($exclude as element()*) as xs:string*{
   for $filter in $exclude/filter
   return
   if ($filter/property/@value='parent' and $filter/op/@value='=') then
      concat('[not(parent/@pCode=''',$filter/value/@value,''')]')
   else if ($filter/property/@value='parent' and $filter/op/@value='in') then
      concat('[not(parent/@pCode=(',string-join(tokenize($filter/value/@value,','),''','''),'))]')
   else if ($filter/property/@value='concept' and $filter/op/@value='is-a') then
      concat('[not(ancSlf=''',$filter/value/@value,''')]')
   else if (($filter/property/@value='ancestor' and $filter/op/@value='=') or ($filter/property/@value='concept' and $filter/op/@value='descendent-of')) then
      concat('[not(ancestor=''',$filter/value/@value,''')]')
   else if ($filter/property/@value='ancestor' and $filter/op/@value='in') then
      concat('[not(ancestor=(',string-join(tokenize($filter/value/@value,','),''','''),'))]')
   else if ($filter/property[starts-with(@uri,'http://snomed.info/id/')] and $filter/op/@value='=') then
      concat('[not(relGrp/relation[@typeCode=''',$filter/property/substring-after(@uri,'http://snomed.info/id/'),''' and @destCode=''',$filter/value/@value,'''])]')
   else if ($filter/property[starts-with(@uri,'http://snomed.info/id/')] and $filter/op/@value='in') then
      concat('[not(relGrp/relation[@typeCode=''',$filter/property/substring-after(@uri,'http://snomed.info/id/'),''' and @destCode=(''',string-join(tokenize($filter/value/@value,','),''','''),''')])]')
   else if ($filter/property[starts-with(@uri,'http://snomed.info/id/')] and $filter/op/@value='is-a') then
      concat('[not(relGrp/relation[ancest=''',$filter/value/@value,''' and @typeCode=''',$filter/property/substring-after(@uri,'http://snomed.info/id/'),'''])]')
   else()
};

(: Terminology report section :)

(: Traverse datasets :)
declare %private function adterminology:traverseDatasets($decorproject as element()) as element()* {
   for $dataset in $decorproject/datasets/dataset
   let $concepts := $dataset//concept[not(parent::conceptList)][@statusCode=('draft','final')]
   return
   <dataset conceptCount="{count($concepts)}" json:array="true">
   {
      $dataset/name,
      for $concept in $concepts
      let $terminologyAssociations := $decorproject/terminology/terminologyAssociation[@conceptId=$concept/@id]
      return
         <concept json:array="true">
   {
      $concept/@*,
      $concept/name,
      if ($terminologyAssociations) then
         for $association in $terminologyAssociations
         return
         adterminology:handleTerminologyAssociation($decorproject,$association)
      else
      <message type="noAssociation">No terminology association defined</message>
       ,
       if ($concept/valueDomain/conceptList) then
         let $conceptListAssociation:= $decorproject/terminology/terminologyAssociation[@conceptId=$concept/valueDomain/conceptList/@id]
         return
         <conceptList>
            {
            (
            if ($conceptListAssociation) then
               for $association in $conceptListAssociation
               return
               adterminology:handleConceptListAssociation($decorproject,$association)
            else
            <message type="noValuesetAssociated">No valueset associated with conceptlist</message>
            ,
            for $listConcept in $concept/valueDomain/conceptList/concept
               let $listConceptAssociations := $decorproject/terminology/terminologyAssociation[@conceptId=$listConcept/@id]
               return
               if($listConceptAssociations) then
                  for $association in $listConceptAssociations
                  return
                  adterminology:handleTerminologyAssociation($decorproject,$association)
               
               else
               <message type="noValuesetItem">No valueset item associated with concept in list</message>
             )
             }
          </conceptList>
       else()

    }  
   </concept>
    }  
   </dataset>
};


(: handle terminology association :)
declare %private function adterminology:handleTerminologyAssociation($decorproject as element(),$terminologyAssociation as element()) as element(){
   (: check if codesystem is in project :)
   if (starts-with($terminologyAssociation/@codeSystem,$decorproject/project/@id)) then
      let $localConcept :=  $decorproject//codedConcept[@code=$terminologyAssociation/@code][ancestor::codeSystem/@id=$terminologyAssociation/@codeSystem]
      return
      if ($localConcept) then
         adterminology:checkConcept($terminologyAssociation/@displayName,$localConcept)
      else
         (: check if code system is present :)
         if ($decorproject//codeSystem[@id=$terminologyAssociation/@codeSystem]) then
            <message type="conceptNotFound">Concept not found</message>
         else
            let $codeSystemName := if ($terminologyAssociation/@codeSystemName) then $terminologyAssociation/@codeSystemName else '?'
            return
            <message type="codesystemNotfound">Codesystem not found</message>
   else
      (: handle SNOMED post coordinated codes :)
      if ($terminologyAssociation/@codeSystem='2.16.840.1.113883.6.96') then
         if ($terminologyAssociation/@code castable as xs:integer) then
            let $snomedConcept := collection($get:strCodesystemStableData)//concept[@code=$terminologyAssociation/@code][ancestor::browsableCodeSystem/@oid=$terminologyAssociation/@codeSystem]
            return
            if ($snomedConcept) then
               adterminology:checkConcept($terminologyAssociation/@displayName,$snomedConcept)
            else
               (: check if code system is present :)
               if (collection($get:strCodesystemStableData)//browsableCodeSystem/@oid=$terminologyAssociation/@codeSystem) then
                  <message type="conceptNotFound">Concept not found</message>
               else
                  <message type="codesystemNotfound">Codesystem not found</message>
         else<nop/>
         
      else
         let $codeSystemConcept := collection($get:strCodesystemStableData)//concept[@code=$terminologyAssociation/@code][ancestor::browsableCodeSystem/@oid=$terminologyAssociation/@codeSystem]
         return
         if ($codeSystemConcept) then
            adterminology:checkConcept($terminologyAssociation/@displayName,$codeSystemConcept)
         else
            (: check if code system is present :)
            if (collection($get:strCodesystemStableData)//browsableCodeSystem/@oid=$terminologyAssociation/@codeSystem) then
               <message type="conceptNotFound">Concept not found</message>
            else
               let $codeSystemName := if ($terminologyAssociation/@codeSystemName) then $terminologyAssociation/@codeSystemName else '?'
               return
               <message type="codesystemNotfound">Codesystem not found</message>
};


(: handle conceptlist association :)
declare %private function adterminology:handleConceptListAssociation($decorproject as element(), $conceptListAssociation as element()) as element(){
   (: check if valueset is in project :)
   if (starts-with($conceptListAssociation/@valueSet,$decorproject/project/@id)) then
      let $valueSet := $decorproject/terminology/valueSet[@id=$conceptListAssociation/@valueSet]
      return
      if ($valueSet) then
         <message type="ok" json:array="true">OK</message>
      else
         <message type="valuesetNotFound" json:array="true">Valueset not found</message>
   else()

};

declare %private function adterminology:checkConcept($displayName as xs:string, $concept as element()) as element(){
   let $message :=
      if ($concept/@statusCode='retired') then
         <message type="conceptRetired" json:array="true">Concept retired</message>
      else if ($concept/@statusCode='draft') then
         <message type="conceptDraft" json:array="true">Concept is in draft</message>
      else if ($concept/@statusCode='experimental') then
         <message type="conceptExperimental" json:array="true">Concept is experimental</message>
      else
         if ($displayName=$concept/designation) then
            <message type="ok" json:array="true">OK</message>
         else 
            let $designations := $concept/designation
            return
            <message type="noMatchingDesignation" json:array="true">Display name does not match concept designation
            {
            for $designation in $designations
            return
            <designation json:array="true">
            {$designation/@*,$designation/text()}
            </designation>
            }
            </message>
            

   return
   $message
};


(: Traverse valuesets :)
declare %private function adterminology:traverseValuesets($decorproject as element()) as element()*{
   for $valueSet in $decorproject/terminology/valueSet[@id]
   return
   <valueSet conceptCount="{count($valueSet/conceptList/concept)}" json:array="true">
      {
      $valueSet/@displayName,
      for $concept in $valueSet/conceptList/concept
         return
         <concept>
         {
         $concept/@*,
         (: check if codesystem is in project :)
         if (starts-with($concept/@codeSystem,$decorproject/project/@id)) then
            let $localConcept :=  $decorproject//codedConcept[@code=$concept/@code][ancestor::codeSystem/@id=$concept/@codeSystem]
            return
            if ($localConcept) then
               adterminology:checkConcept($concept/@displayName,$localConcept)
            else
               (: check if code system is present :)
               if ($decorproject//codeSystem[@id=$concept/@codeSystem]) then
                  <message type="conceptNotFound" json:array="true">Concept not found</message>
               else
                  let $codeSystemName := if ($concept/@codeSystemName) then $concept/@codeSystemName else '?'
                  return
                  <message type="codesystemNotfound" json:array="true">Codesystem not found</message>
         else
            (: handle SNOMED post coordinated codes :)
            if ($concept/@codeSystem='2.16.840.1.113883.6.96') then
               if ($concept/@code castable as xs:integer) then
                  let $snomedConcept := collection($get:strCodesystemStableData)//concept[@code=$concept/@code][ancestor::browsableCodeSystem/@oid=$concept/@codeSystem]
                  return
                  if ($snomedConcept) then
                     adterminology:checkConcept($concept/@displayName,$snomedConcept)
                  else
                     (: check if code system is present :)
                     if (collection($get:strCodesystemStableData)//browsableCodeSystem/@oid=$concept/@codeSystem) then
                        <message type="conceptNotFound" json:array="true">Concept not found</message>
                     else
                        <message type="codesystemNotfound" json:array="true">Codesystem not found</message>
               else adterminology:parseSnomedExpression(replace($concept/@code,'\|(.*?)\|',''))
               
            else
               let $codeSystemConcept := collection($get:strCodesystemStableData)//concept[@code=$concept/@code][ancestor::browsableCodeSystem/@oid=$concept/@codeSystem]
               return
               if ($codeSystemConcept) then
                  adterminology:checkConcept($concept/@displayName,$codeSystemConcept)
               else
                  (: check if code system is present :)
                  if (collection($get:strCodesystemStableData)//browsableCodeSystem/@oid=$concept/@codeSystem) then
                     <message type="conceptNotFound" json:array="true">Concept not found</message>
                  else
                     let $codeSystemName := if ($concept/@codeSystemName) then $concept/@codeSystemName else '?'
                     return
                     <message type="codesystemNotfound" json:array="true">Codesystem not found</message>
             }
            </concept>         
            }
   </valueSet>

};

declare function adterminology:parseSnomedExpression ($expression as xs:string) {
   (: check for multiple focus concepts :)
   if (normalize-space(substring-before($expression,'+')) castable as xs:integer) then
      let $focusCode := normalize-space(substring-before($expression,'+'))
      return
      (
      <concept code="{$focusCode}">{adterminology:getSnomedConceptDesignations($focusCode)}</concept>,
      <focus/>,
      adterminology:parseSnomedExpression(normalize-space(substring-after($expression,'+')))
      )
   else if (normalize-space(substring-before($expression,':')) castable as xs:integer) then
      let $conceptCode := normalize-space(substring-before($expression,':'))
      let $refinements := tokenize(normalize-space(substring-after($expression,':')),',')
      return
      (
      <concept code="{$conceptCode}">{adterminology:getSnomedConceptDesignations($conceptCode)}</concept>,
         
         for $refinement in $refinements
         return
         if (normalize-space(substring-before($refinement,'=')) castable as xs:integer) then
         let $refinementCode := normalize-space(substring-before($refinement,'='))
         return
         <refinement>
         <concept code="{$refinementCode}">{adterminology:getSnomedConceptDesignations($refinementCode)}</concept>
            {
            if (normalize-space(substring-after($refinement,'=')) castable as xs:integer) then
               let $equalsCode :=normalize-space(substring-after($refinement,'='))
               return
               <equals code="{$equalsCode}">{adterminology:getSnomedConceptDesignations($equalsCode)}</equals>
            else
            <message type="cannotParseExpression">Expression cannot be parsed</message>
            }
         </refinement>
         else <message type="cannotParseExpression">Expression cannot be parsed</message>
         
      
      )
   else 
      <message type="cannotParseExpression">Expression cannot be parsed</message>
};

declare function adterminology:getSnomedConceptDesignations($code as xs:integer) as element()*{
   let $concept := collection(concat($get:strCodesystemStableData,'/external/snomed'))//concept[@code=$code]
   return
   if ($concept) then
       if ($concept/@statusCode='retired') then
         <message type="conceptRetired">Concept retired</message>
      else if ($concept/@statusCode='draft') then
         <message type="conceptDraft">Concept is in draft</message>
      else if ($concept/@statusCode='experimental') then
         <message type="conceptExperimental">Concept is experimental</message>
      else  
         let $designations := $concept/designation
         return
         $designations[@use='pref']
   else
      <message type="conceptNotFound">Concept not found</message>
};