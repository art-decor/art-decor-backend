xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
module namespace adsearch       = "http://art-decor.org/ns/terminology/search";
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace claml   = "http://art-decor.org/ns/terminology/claml" at "../claml/api/api-claml.xqm";
import module namespace snomed  = "http://art-decor.org/ns/terminology/snomed" at "../snomed/api/api-snomed.xqm";

(:~
:   All functions support their own override, but this is the fallback for the maximum number of results returned on a search
:)
declare variable $adsearch:maxResults := xs:integer('50');

(:~
:   Returns lucene config xml for a sequence of strings. The search will find yield results that match all terms+trailing wildcard in the sequence
:   Example output:
:   <query>
:       <bool>
:           <wildcard occur="must">term1*</wildcard>
:           <wildcard occur="must">term2*</wildcard>
:       </bool>
:   </query>
:
:   @param $searchTerms required sequence of terms to look for
:   @return lucene config
:   @since 2014-06-06
:)
declare function adsearch:getSimpleLuceneQuery($searchTerms as xs:string+) as element() {
    adsearch:getSimpleLuceneQuery($searchTerms, 'wildcard')
};

(:~
:   http://exist-db.org/exist/apps/doc/lucene.xml?q=lucene&field=all&id=D2.2.4.28#D2.2.4.28
:)
declare function adsearch:getSimpleLuceneQuery($searchTerms as xs:string+, $searchType as xs:string) as element() {
    <query>
        <bool>
        {
            for $term in $searchTerms
            return
                if ($searchType='fuzzy') then
                    <fuzzy occur="must">{concat($term,'~')}</fuzzy>
                
                else if ($searchType='fuzzy-not') then
                    <fuzzy occur="not">{concat($term,'~')}</fuzzy>
                    
                else if ($searchType='regex') then
                    <regex occur="must">{$term}</regex>
                    
                else if ($searchType='regex-not') then
                    <regex occur="not">{$term}</regex>
                    
                else if ($searchType='phrase') then
                    <phrase occur="must">{$term}</phrase>
                    
                else if ($searchType='phrase-not') then
                    <phrase occur="not">{$term}</phrase>
                    
                else if ($searchType='term') then
                    <term occur="must">{$term}</term>
                    
                else if ($searchType='term-not') then
                    <term occur="not">{$term}</term>
                    
                else if ($searchType='wildcard') then
                    <wildcard occur="must">{concat($term,'*')}</wildcard>
                    
                else if ($searchType='wildcard-not') then
                    <wildcard occur="not">{concat($term,'*')}</wildcard>
                    
                else ()
        }
        </bool>
    </query>
};

(:~
:   Returns lucene options xml that instruct filter-rewrite=yes
:)
declare function adsearch:getSimpleLuceneOptions() as element() {
    <options>
        <default-operator>and</default-operator>
        <phrase-slop>0</phrase-slop>
        <leading-wildcard>no</leading-wildcard>
        <filter-rewrite>yes</filter-rewrite>
    </options>
};

(:~
:   Returns description elements for a ClaML based terminology
:   Example output:
:   <result current="1" count="1">
:       <description count="3" length="28" conceptId="A17" type="pref" language="nl" superClasses="tuberculose" classificationId="2.16.840.1.113883.6.3.2" classificationName="ICD-10-2014-NL-web" statusCode="active" usageMark="+">tuberculose van zenuwstelsel</description>
:   ...
:   </result>
:
:   @param $classificationId required. OID for the terminology
:   @param $language optional. language for the terminology. format ll-CC, example en-US. If omitted then the first available language is picked
:   @param $searchString required. white delimited sequence of terms to look for
:   @param $maxResults optional. maximum number of results to return, defaults to $adsearch:maxResults
:   @param $statusCodes optional. Status codes in the terminology to match. Normally 'active' and/or 'deprecated'. Returns all matches if empty
:   @param $searchScope optional. Search scope for matching. 'code' and/or 'description'. Matches 'descrition' if empty
:   @return resultset with max $maxResults results
:   @since 2015-07-02
:)
declare function adsearch:searchClamlConcept($classificationId as xs:string, $language as xs:string?, $searchString as xs:string, $maxResults as xs:integer?, $statusCodes as xs:string*, $searchScope as xs:string*) as element(result) {
    let $maxResults         := if ($maxResults) then $maxResults else $adsearch:maxResults
    let $searchType         := 'wildcard'
    
    let $clamlPath          := adsearch:getClamlDescriptionsPath($classificationId, $language)
    let $clamlCollection    := if ($clamlPath) then collection($clamlPath) else ()
    
    let $validSearchCode    := 
        if ($searchScope='code') then true() else(false())
    let $validSearchDesc    := 
        if (matches($searchString,'^[a-z|0-9]') and string-length($searchString)>2 and string-length($searchString)<40) then
            true()
        else if (matches($searchString,'^[A-Z]') and string-length($searchString)>1 and string-length($searchString)<40) then
            true()
        else(false())
    let $searchTerms        := tokenize($searchString,'[\s\-]')
    
    (:let $searchTerms := tokenize('ast','\s'):)
    let $maxResults         := xs:integer('50')
    let $luceneOptions      := adsearch:getSimpleLuceneOptions()
    let $luceneQuery        := adsearch:getSimpleLuceneQuery($searchTerms, $searchType)
    
    let $result             :=
        if ($validSearchCode and $validSearchDesc and $searchScope='code' and $searchScope='description') then 
            $clamlCollection//description[@conceptId=$searchTerms] |
            $clamlCollection//description[ft:query(.,$luceneQuery,$luceneOptions)]
        else if ($validSearchCode and $searchScope='code') then (
            $clamlCollection//description[@conceptId=$searchTerms]
        ) else if ($validSearchDesc) then (
            $clamlCollection//description[ft:query(.,$luceneQuery,$luceneOptions)]
        ) else (
        )
    
    let $result             := if (empty($statusCodes)) then ($result) else ( $result[empty(@statusCode)] | $result[@statusCode = $statusCodes] )
    
    let $count := count($result)
    
return
    <result current="{if ($count<=$maxResults) then $count else $maxResults}" total="{$count}">
    {
        for $object in subsequence($result,1,$maxResults)
        order by xs:integer($object/@count),xs:integer($object/@length)
        return 
            $object
    }
    </result>
};

(:~
:   Returns string description elements for a ClaML based terminology
:   Example output:
:       /db/apps/terminology-data/icd10-nl-data/nl-NL/descriptions
:
:   @param $classificationId required. OID for the terminology
:   @param $language optional. language for the terminology. format ll-CC, example en-US. If omitted then the first available language is picked
:   @return resultset with max $maxResults results
:   @since 2015-07-02
:)
declare function adsearch:getClamlDescriptionsPath($classificationId as xs:string, $language as xs:string?) as xs:string? {
    let $classifications        :=
        <classifications>
        {
            for $child in xmldb:get-child-collections($get:strTerminologyData)
            let $clamls := collection(concat($get:strTerminologyData,'/',$child))//ClaML
            return
            if ($clamls) then
                <classification collection="{$child}">
                {
                    for $claml in $clamls
                    return
                    <Title language="{substring-after(substring-before(util:collection-name($claml),'/claml'),concat($get:strTerminologyData,'/',$child,'/'))}">
                    {
                        $claml/Title/@*,
                        $claml/Identifier
                    }
                    </Title>
                }
                </classification>
            else()
        }
        </classifications>
    
    let $classification         := $classifications/classification[Title/Identifier/@uid=$classificationId]
    let $classificationTitles   := $classification/Title[Identifier/@uid=$classificationId]
    let $collectionPath         := 
        if (count($classificationTitles)>1) then
            if ($classificationTitles/@language=$language) then
                concat($get:strTerminologyData,'/',$classification/@collection,'/',$language,'/descriptions')
            else (
                concat($get:strTerminologyData,'/',$classification/@collection,'/',$classificationTitles[1]/@language,'/descriptions')
            )
        else if (count($classificationTitles)=1) then (
            concat($get:strTerminologyData,'/',$classification/@collection,'/',$classificationTitles/@language,'/descriptions')
        ) else ()
    
    return $collectionPath
};
