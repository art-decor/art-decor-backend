<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0" xml:space="default">
    <xsl:output indent="yes" method="xml"/>
    <xsl:key name="code" match="concept" use="@code"/>
    <xsl:template match="codeSystem">
        <codeSystem>
            <id>
                <xsl:attribute name="value">
                    <xsl:value-of select="@id"/>
                </xsl:attribute>
            </id>
            <url>
                <xsl:attribute name="value">
                    <xsl:value-of select="@url"/>
                </xsl:attribute>
            </url>
            <name>
                <xsl:attribute name="value">
                    <xsl:value-of select="name"/>
                </xsl:attribute>
            </name>
            <description>
                <xsl:attribute name="value">
                    <xsl:value-of select="description"/>
                </xsl:attribute>
            </description>
            <xsl:for-each select="concept[not(parent)]">
                <xsl:apply-templates/>
            </xsl:for-each>
        </codeSystem>
    </xsl:template>
    <xsl:template match="concept">
        <concept>
            <code>
                <xsl:attribute name="value">
                    <xsl:value-of select="@code"/>
                </xsl:attribute>
            </code>
            <display>
                <xsl:attribute name="value">
                    <xsl:value-of select="@displayName"/>
                </xsl:attribute>
            </display>
            <definition>
                <xsl:attribute name="value">
                    <xsl:value-of select="definition"/>
                </xsl:attribute>
            </definition>
         <!-- test if abstract -->
            <xsl:if test="@abstract='true'">
                <property>
                    <code value="notSelectable"/>
                    <valueBoolean value="true"/>
                </property>
            </xsl:if>
         <!-- test if deprecated -->
            <xsl:if test="@statusCode='deprecated'">
                <property>
                    <code value="deprecated"/>
                    <valueDateTime>
                        <xsl:attribute name="value">
                            <xsl:value-of select="@expirationDate"/>
                        </xsl:attribute>
                    </valueDateTime>
                </property>
            </xsl:if>
            <xsl:for-each select="concept">
                <xsl:apply-templates select="key('code',@code)"/>
            </xsl:for-each>
            <xsl:for-each select="parent[position()&gt;1]">
                <modifierExtension url="http://hl7.org/fhir/StructureDefinition/codesystem-subsumes">
                    <valueCode>
                        <xsl:attribute name="value">
                            <xsl:value-of select="@code"/>
                        </xsl:attribute>
                    </valueCode>
                </modifierExtension>
            </xsl:for-each>
        </concept>
    </xsl:template>
</xsl:stylesheet>