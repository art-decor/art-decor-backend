<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" version="2.0" xml:space="default" xpath-default-namespace="">
    <xsl:output indent="yes" method="xml"/>
    <xsl:key name="code" match="Class" use="@code"/>
    <xsl:variable name="defaultLanguage" select="'en-US'"/>
    <xsl:template match="ClaML">
        <browsableCodeSystem>
            <xsl:attribute name="id">
                <xsl:value-of select="Title/@name"/>
            </xsl:attribute>
            <xsl:attribute name="url">
                <xsl:value-of select="'URL'"/>
            </xsl:attribute>
            <xsl:attribute name="oid">
                <xsl:value-of select="Identifier/@uid"/>
            </xsl:attribute>
            <xsl:attribute name="version">
                <xsl:value-of select="Title/@version"/>
            </xsl:attribute>
            <xsl:attribute name="statusCode">
                <xsl:value-of select="'active'"/>
            </xsl:attribute>
            <xsl:attribute name="experimental">
                <xsl:value-of select="'false'"/>
            </xsl:attribute>
            <xsl:attribute name="effectiveDate">
                <xsl:value-of select="Title/@date"/>
            </xsl:attribute>
            <xsl:attribute name="defaultLanguage">
                <xsl:value-of select="$defaultLanguage"/>
            </xsl:attribute>
            <xsl:attribute name="content">
                <xsl:value-of select="'complete'"/>
            </xsl:attribute>
            <identifier>
                <xsl:attribute name="system">
                    <xsl:value-of select="Identifier/@authority"/>
                </xsl:attribute>
                <xsl:attribute name="id">
                    <xsl:value-of select="Identifier/@uid"/>
                </xsl:attribute>
            </identifier>
            <name language="{$defaultLanguage}">
                <xsl:value-of select="Title/@name"/>
            </name>
            <!--            <logo link="{Meta[@name = 'custodianOrganisationUrl']/@value}">
                <xsl:value-of select="Meta[@name = 'custodianOrganisationLogo']/@value"/>
            </logo>-->
            <license><text>License text</text></license>
            <publishingAuthority name="{Meta[@name = 'custodianOrganisation']/@value}">
                <addrLine type="uri"><xsl:value-of select="Meta[@name = 'custodianOrganisationUrl']/@value"/></addrLine>
            </publishingAuthority>
            <description language="{$defaultLanguage}"/>
            <xsl:apply-templates select="Class"/>
        </browsableCodeSystem>
    </xsl:template>
    <xsl:template match="Class">
        <concept code="{@code}" statusCode="active">
            <xsl:apply-templates select="Rubric"/>
            <xsl:for-each select="SubClass">
                <child cCode="{@code}">
                    <xsl:variable name="child" select="key('code', @code)"/>
                    <xsl:for-each select="$child/Rubric[@kind = 'preferred']">
                        <xsl:for-each select="Label">
                            <xsl:variable name="language">
                                <xsl:choose>
                                    <xsl:when test="@xml:lang = 'en'">
                                        <xsl:value-of select="'en-US'"/>
                                    </xsl:when>
                                    <xsl:when test="@xml:lang = 'es'">
                                        <xsl:value-of select="'es-ES'"/>
                                    </xsl:when>
                                    <xsl:when test="@xml:lang = 'de'">
                                        <xsl:value-of select="'de-DE'"/>
                                    </xsl:when>
                                    <xsl:when test="@xml:lang = 'nl'">
                                        <xsl:value-of select="'nl-NL'"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="@xml:lang"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:variable>
                            <label lang="{$language}">
                                <xsl:value-of select="."/>
                            </label>
                        </xsl:for-each>
                    </xsl:for-each>
                </child>
            </xsl:for-each>
            <xsl:for-each select="SuperClass">
                <parent pCode="{@code}">
                    <xsl:variable name="parent" select="key('code', @code)"/>
                    <xsl:for-each select="$parent/Rubric[@kind = 'preferred']">
                        <xsl:for-each select="Label">
                            <xsl:variable name="language">
                                <xsl:choose>
                                    <xsl:when test="@xml:lang = 'en'">
                                        <xsl:value-of select="'en-US'"/>
                                    </xsl:when>
                                    <xsl:when test="@xml:lang = 'es'">
                                        <xsl:value-of select="'es-ES'"/>
                                    </xsl:when>
                                    <xsl:when test="@xml:lang = 'de'">
                                        <xsl:value-of select="'de-DE'"/>
                                    </xsl:when>
                                    <xsl:when test="@xml:lang = 'nl'">
                                        <xsl:value-of select="'nl-NL'"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="@xml:lang"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:variable>
                            <label lang="{$language}">
                                <xsl:value-of select="."/>
                            </label>
                        </xsl:for-each>
                    </xsl:for-each>
                </parent>
            </xsl:for-each>
        </concept>
    </xsl:template>
    <xsl:template match="Rubric">
        <xsl:variable name="rubricKind" select="@kind"/>
        <xsl:for-each select="Label">
            <xsl:variable name="language">
                <xsl:choose>
                    <xsl:when test="@xml:lang = 'en'">
                        <xsl:value-of select="'en-US'"/>
                    </xsl:when>
                    <xsl:when test="@xml:lang = 'es'">
                        <xsl:value-of select="'es-ES'"/>
                    </xsl:when>
                    <xsl:when test="@xml:lang = 'de'">
                        <xsl:value-of select="'de-DE'"/>
                    </xsl:when>
                    <xsl:when test="@xml:lang = 'nl'">
                        <xsl:value-of select="'nl-NL'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="@xml:lang"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="$rubricKind = 'preferred'">
                    <designation lang="{$language}" use="pref" statusCode="active" effectiveDate="" expirationDate="" lastModifiedDate="">
                        <xsl:attribute name="count">
                            <xsl:value-of select="count(tokenize(., '\s'))"/>
                        </xsl:attribute>
                        <xsl:attribute name="length">
                            <xsl:value-of select="string-length(.)"/>
                        </xsl:attribute>
                        <xsl:value-of select="."/>
                    </designation>
                </xsl:when>
                <xsl:when test="$rubricKind = 'definition'">
                    <definition lang="{$language}" statusCode="active" effectiveDate="" expirationDate="" lastModifiedDate="">
                        <xsl:choose>
                            <xsl:when test="para">
                                <xsl:value-of select="para"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="."/>
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:value-of select="."/>
                    </definition>
                </xsl:when>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>