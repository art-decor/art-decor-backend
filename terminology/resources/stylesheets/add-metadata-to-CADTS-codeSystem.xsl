<!--
	Copyright (C) 2011-2017 ART-DECOR Expert Group (art-decor.org)
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html

--><xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" version="2.0">
    <xsl:output method="xml" indent="no" exclude-result-prefixes="#all" encoding="UTF-8"/>
    <xsl:key name="code" match="concept" use="@code"/>
    <xsl:template match="browsableCodeSystem">
        <browsableCodeSystem>
            <xsl:variable name="conceptCount" select="count(concept)"/>
            <xsl:copy-of select="@*"/>
            <xsl:attribute name="structure">
                <xsl:choose>
                    <xsl:when test="concept[count(parent) &gt; 1]">
                        <xsl:value-of select="'network'"/>
                    </xsl:when>
                    <xsl:when test="concept[count(parent) = 1]">
                        <xsl:value-of select="'tree'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'list'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <!--            <xsl:attribute name="type">
                <xsl:choose>
                    <xsl:when test="count(distinct-values(concept/designation/@use)) = 1 and concept/designation/@use = 'pref'">
                        <xsl:value-of select="'simple'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'complex'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>-->
            <xsl:attribute name="count">
                <xsl:value-of select="$conceptCount"/>
            </xsl:attribute>
            <xsl:for-each select="distinct-values(concept/designation/@languageCode)">
                <language complete="">
                    <xsl:value-of select="."/>
                </language>
            </xsl:for-each>
            <xsl:copy-of select="*[not(name() = 'concept')]"/>
            <xsl:for-each select="concept">
                <xsl:variable name="type">
                    <xsl:choose>
                        <xsl:when test="@statusCode='retired'">
                            <xsl:value-of select="'D'"/>
                        </xsl:when>
                        <xsl:when test="@abstract='true'">
                            <xsl:value-of select="'A'"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="child">
                                    <xsl:value-of select="'S'"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="'L'"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <concept>
                    <xsl:copy-of select="@*[not(name()='abstract')]"/>
                    <xsl:attribute name="level">
                        <xsl:choose>
                            <xsl:when test="ancestor">
                                <xsl:value-of select="max(ancestor/@distance)"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="'0'"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                    <xsl:attribute name="type">
                        <xsl:value-of select="$type"/>
                    </xsl:attribute>
                    <xsl:copy-of select="*[not(name()='child')]"/>
                    <xsl:for-each select="child">
                        <child>
                            <xsl:copy-of select="@*"/>
                            <xsl:variable name="childConcept" select="key('code',@cCode)"/>
                            <xsl:variable name="childType">
                                <xsl:choose>
                                    <xsl:when test="$childConcept/@statusCode='retired'">
                                        <xsl:value-of select="'D'"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:choose>
                                            <xsl:when test="$childConcept/child">
                                                <xsl:value-of select="'S'"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:value-of select="'L'"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:variable>
                            <xsl:attribute name="level">
                                <xsl:choose>
                                    <xsl:when test="$childConcept/ancestor">
                                        <xsl:value-of select="max($childConcept/ancestor/@distance)"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="'0'"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:attribute>
                            <xsl:attribute name="type">
                                <xsl:value-of select="$childType"/>
                            </xsl:attribute>
                            <xsl:copy-of select="*"/>
                        </child>
                    </xsl:for-each>
                </concept>
            </xsl:for-each>
        </browsableCodeSystem>
    </xsl:template>
</xsl:stylesheet>