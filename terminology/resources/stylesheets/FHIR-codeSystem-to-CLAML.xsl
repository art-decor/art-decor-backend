<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fhir="http://hl7.org/fhir" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs fhir" version="2.0" xml:space="default" xpath-default-namespace="">
   <xsl:output indent="yes" method="xml"/>
   <xsl:key name="code" match="fhir:concept" use="fhir:code/@value"/>
   <xsl:key name="child" match="fhir:concept" use="fhir:property[fhir:code/@value = 'subsumedBy']/fhir:valueCode/@value"/>
   <xsl:variable name="defaultLanguage" select="'en-US'"/>
   <xsl:template match="fhir:CodeSystem">
      <ClaML version="2.0.0">
         <Meta name="statusCode" value="{fhir:status/@value}"/>
         <Meta name="custodianOrganisation" value="HL7"/>
         <Meta name="custodianOrganisationLogo" value="HL7NL-logo40.png"/>
         <Meta name="custodianOrganisationUrl" value="http://www.hl7.nl"/>
         <Identifier authority="hl7.org" uid="{fhir:identifier/fhir:value/substring-after(@value, 'urn:oid:')}"/>
         <Title name="{fhir:name/@value}" version="1.000">
            <xsl:attribute name="date">
               <xsl:choose>
                  <xsl:when test="fhir:date/@value castable as xs:dateTime">
                     <xsl:value-of select="format-dateTime(fhir:date/@value, '[Y0001]-[M01]-[D01]')"/>
                  </xsl:when>
                  <xsl:when test="fhir:date/@value castable as xs:date">
                     <xsl:value-of select="format-date(fhir:date/@value, '[Y0001]-[M01]-[D01]')"/>
                  </xsl:when>
               </xsl:choose>
            </xsl:attribute>
            <xsl:value-of select="normalize-space(fhir:description/@value)"/>
         </Title>                        
         <Authors>
            <Author name="{fhir:publisher/@value}">
               <xsl:value-of select="fhir:publisher/@value"/>
            </Author>
         </Authors>
         <ClassKinds>
            <ClassKind name="abstract"/>
            <ClassKind name="concept"/>
         </ClassKinds>
         <RubricKinds>
            <RubricKind name="definition"/>
            <RubricKind name="description"/>
            <RubricKind name="header"/>
            <RubricKind name="preferred"/>
            <RubricKind name="short"/>
         </RubricKinds>
         <xsl:for-each select="//fhir:concept[not(fhir:property[fhir:code/@value = 'status']/fhir:valueCode/@value='retired')]">
            <xsl:call-template name="concept"/>
         </xsl:for-each>
      </ClaML>
   </xsl:template>
   <xsl:template name="concept">
      <Class code="{fhir:code/@value}" kind="{if (fhir:property/fhir:code[@value = 'notSelectable']) then 'abstract' else ('concept')}">
         <xsl:if test="fhir:property/fhir:code[@value = 'notSelectable']">
            <Meta name="isSelectable" value="false"/>
         </xsl:if>
         <xsl:for-each select="fhir:property">
            <Meta name="{fhir:code/@value}" value="{fhir:valueCode/@value}">
               <xsl:attribute name="value">
                  <xsl:choose>
                     <xsl:when test="fhir:valueCode">
                        <xsl:value-of select="fhir:valueCode/@value"/>
                     </xsl:when>
                     <xsl:when test="fhir:valueBoolean">
                        <xsl:value-of select="fhir:valueBoolean/@value"/>
                     </xsl:when>
                     <xsl:when test="fhir:valueCoding">
                        <xsl:value-of select="fhir:valueCoding/fhir:code/@value"/>
                     </xsl:when>
                  </xsl:choose>
               </xsl:attribute>
            </Meta>
         </xsl:for-each>
         <!-- designation from display name -->
         
         <Rubric id="{generate-id()}-2" kind="preferred">
            <Label xml:lang="{$defaultLanguage}">
               <xsl:value-of select="fhir:display/@value"/>
            </Label>
         </Rubric>
         <!-- designations  -->
         <xsl:for-each select="fhir:designation">
            <xsl:variable name="designationLanguage">
               <xsl:choose>
                  <xsl:when test="fhir:language/@value = 'de'">
                     <xsl:value-of select="'de-DE'"/>
                  </xsl:when>
                  <xsl:otherwise>
                     <xsl:value-of select="$defaultLanguage"/>
                  </xsl:otherwise>
               </xsl:choose>
            </xsl:variable>
            <xsl:variable name="rubricKind">
               <xsl:choose>
                  <xsl:when test="fhir:use/fhir:code/@value = 'preferredForLanguage'">
                     <xsl:value-of select="'preferred'"/>
                  </xsl:when>
                  <xsl:otherwise>
                     <xsl:value-of select="'short'"/>
                  </xsl:otherwise>
               </xsl:choose>
            </xsl:variable>
            <Rubric id="{generate-id()}-1" kind="short">
               <Label xml:lang="{$designationLanguage}">
                  <xsl:value-of select="fhir:value/@value"/>
               </Label>
            </Rubric>
         </xsl:for-each>
         <xsl:if test="string-length(normalize-space(fhir:definition/@value)) &gt; 0">
            <Rubric id="{generate-id()}" kind="definition">
               <Label xml:lang="{$defaultLanguage}">
                  <xsl:value-of select="normalize-space(fhir:definition/@value)"/>
               </Label>
            </Rubric>
         </xsl:if>
         <!-- children definied by extension -->
         <xsl:for-each select="fhir:modifierExtension[@url = 'http://hl7.org/fhir/StructureDefinition/codesystem-subsumes']">
            <SubClass code="{fhir:valueCode/@value}"/>
         </xsl:for-each>
         <!-- children defined by hierarchy -->
         <xsl:for-each select="fhir:concept">
            <SubClass code="{fhir:code/@value}"/>
         </xsl:for-each>
         <!-- children defined by subsumption -->
         <xsl:for-each select="key('child', fhir:code/@value)">
            <SubClass code="{fhir:code/@value}"/>
         </xsl:for-each>
         <!-- parents definied by subsumedBy -->
         <xsl:for-each select="fhir:property[fhir:code/@value = 'subsumedBy']">
            <SuperClass code="{fhir:valueCode/@value}"/>
         </xsl:for-each>
      </Class>
   </xsl:template>
</xsl:stylesheet>