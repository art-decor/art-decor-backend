<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0" xml:space="default">
    <xsl:output indent="yes" method="xml"/>
    <xsl:key name="code" match="concept" use="@code"/>
    <xsl:template match="codeSystem">
        <codeSystem>
            <xsl:copy-of select="@*"/>
            <xsl:copy-of select="*[not(name() = 'concept')]"/>
            <xsl:apply-templates select="concept"/>
        </codeSystem>
    </xsl:template>
    <xsl:template match="concept">
        <xsl:variable name="followingList" select="following-sibling::concept/@code"/>
        <concept>
            <xsl:copy-of select="@*"/>
            <xsl:copy-of select="*"/>
        </concept>
        <xsl:for-each select="child[not(@cCode=$followingList)]">
            <xsl:copy-of select="key('code',@cCode)"/>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
