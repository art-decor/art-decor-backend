<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="#all"
    version="2.0">
    <!-- 
        Purpose: create a Centralized ART-DECOR Terminology Services codeSystem from a DECOR project codeSystem
        
        Skips cancelled and rejected codedConcepts
        
        Input: DECOR codeSystem from a project 
        Output: CADTS codeSystem for view/expand
    -->
    
    <xsl:output indent="yes" method="xml" exclude-result-prefixes="#all"/>
    <xsl:key name="code" match="codedConcept[not(@statusCode = ('cancelled', 'rejected'))]" use="@code"/>
    <xsl:key name="parentcode" match="codedConcept[not(@statusCode = ('cancelled', 'rejected'))]/parent" use="@code"/>
    
    <xsl:param name="language" as="xs:string?"/>
    <xsl:param name="canonicalUri" select="@canonicalUri" as="xs:string"/>
    
    <xsl:template match="/">
        <xsl:apply-templates select="//codeSystem"/>
    </xsl:template>
    
    <xsl:template match="codeSystem">
        <xsl:variable name="language" select="($language, @defaultLanguage, .//@language)[1]" as="xs:string"/>
        <xsl:variable name="checkParentChild" select="exists(//codedConcept[parent | child])"/>
        <!-- TODO: check. are these the right values? -->
        <xsl:variable name="structure">
            <xsl:choose>
                <xsl:when test="//codedConcept[parent[2]]">network</xsl:when>
                <xsl:when test="//codedConcept[parent]">tree</xsl:when>
                <xsl:when test="not($checkParentChild) and //codedConcept[@level != '0']">tree</xsl:when>
                <xsl:otherwise>list</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="codedConcepts" select=".//codedConcept[not(@statusCode = ('cancelled', 'rejected'))]" as="element()*"/>
        <xsl:variable name="codedConceptCount" select="count($codedConcepts)"/>
        <browsableCodeSystem xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../../../../decor/core/DECORbrowsableCodeSystemOnly.xsd">
            <xsl:attribute name="id" select="@name"/>
            <xsl:attribute name="oid" select="@id"/>
            <xsl:attribute name="url" select="($canonicalUri, 'urn:oid:' || @id)[1]"/>
            <!-- Just the data part -->
            <xsl:attribute name="version" select="substring(string((@officialReleaseDate, current-date())[1]), 1, 10)"/>
            <!--decor    : new, draft, final, deprecated, rejected, pending,        cancelled -->
            <!--browsable:      draft, active,   retired, rejected, pending/review, cancelled -->
            <xsl:attribute name="statusCode">
                <xsl:choose>
                    <xsl:when test="@statusCode = ('new', 'draft')">draft</xsl:when>
                    <xsl:when test="@statusCode = ('final', 'active')">active</xsl:when>
                    <xsl:when test="@statusCode = ('deprecated', 'retired')">retired</xsl:when>
                    <xsl:when test="@statusCode = 'rejected'">rejected</xsl:when>
                    <xsl:when test="@statusCode = ('pending', 'review')">pending</xsl:when>
                    <xsl:when test="@statusCode = 'cancelled'">cancelled</xsl:when>
                    <xsl:otherwise>draft</xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="experimental" select="@experimental = 'true'"/>
            <!-- abandonned 2024-07-29 
            <xsl:attribute name="effectiveDate" select="substring(string((@effectiveDate, current-date())[1]), 1, 10)"/>
            -->
            <xsl:attribute name="effectiveDate">
                <xsl:choose>
                    <xsl:when test="@effectiveDate castable as xs:dateTime">
                        <xsl:value-of select="@effectiveDate"/>
                    </xsl:when>
                    <xsl:when test="@effectiveDate castable as xs:date">
                        <xsl:value-of select="concat(@effectiveDate, 'T00:00:00')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="format-date(current-date(), '[Y0001]-[M01]-[D01]T00:00:00')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="defaultLanguage" select="$language"/>
            <!-- TODO: how to determine? can we assume a DECOR system normally is complete? ... -->
            <xsl:attribute name="content" select="'complete'"/>
            <xsl:attribute name="structure" select="$structure"/>
            <!-- TODO: don't know what this is ... -->
            <xsl:attribute name="type" select="'simple'"/>
            <xsl:attribute name="count" select="count($codedConcepts)"/>
            
            <xsl:for-each select="distinct-values(.//@language)">
                <xsl:sort select="."/>
                <xsl:variable name="theLanguage" select="."/>
                <xsl:variable name="completeCount" select="count($codedConcepts[designation[@language = $theLanguage]])"/>
                <language complete="{$codedConceptCount = $completeCount}"><xsl:value-of select="."/></language>
            </xsl:for-each>
            <!-- TODO: check. this is correct if this is mimicing FHIR -->
            <identifier system="urn:ietf:rfc:3986" id="urn:oid:{@id}"/>
            <name language="en-US">
                <xsl:value-of select="@name"/>
            </name>
            <!-- TODO: check. FHIR supports this but CADTS does not seem to -->
            <!--<title language="en-US"><xsl:value-of select="(@displayName, @name)[1]"/></title>-->
            <!-- TODO: check. do we need this coming from a DECOR project? -->
            <!--<logo link="http://www.hl7.nl">HL7NL-logo40.png</logo>-->
            <!-- TODO: check. does CADTS support multiple languages? -->
            <xsl:for-each select="copyright">
                <license>
                    <xsl:copy-of select="@language | node()" copy-namespaces="no"/>
                </license>
            </xsl:for-each>
            <xsl:copy-of select="publishingAuthority"/>
            <!--<xsl:for-each select="publishingAuthority">
                <publisher>
                    <xsl:value-of select="@name"/>
                </publisher>
            </xsl:for-each>
            <xsl:for-each select="publishingAuthority[addrLine]">
                <contact>
                    <xsl:for-each select="addrLine">
                        <telecom system="{@type}" value="{.}"/>
                    </xsl:for-each>
                </contact>
            </xsl:for-each>-->
            <xsl:apply-templates select="desc"/>
            <!-- https://www.hl7.org/fhir/codesystem.html#status -->
            <xsl:apply-templates select="property"/>
            <xsl:choose>
                <xsl:when test="property[@code = 'notSelectable']"/>
                <xsl:when test="$codedConcepts[@type = 'A'] | $codedConcepts[@abstract = 'true'] | $codedConcepts/property[@code = 'notSelectable']/valueBoolean[@value = 'true']">
                    <property code="notSelectable" type="boolean" description="This concept is a grouping concept and not intended to be used in the normal use of the code system (though may be used for filters etc.). This is also known as 'Abstract'" uri="http://hl7.org/fhir/concept-properties#notSelectable"/>
                </xsl:when>
            </xsl:choose>
            <xsl:apply-templates select="$codedConcepts">
                <xsl:with-param name="checkParentChild" select="$checkParentChild"/>
                <xsl:with-param name="language" select="$language"/>
            </xsl:apply-templates>
        </browsableCodeSystem>
    </xsl:template>
    
    <xsl:template match="desc">
        <description language="{@language}"><xsl:copy-of select="node()"/></description>
    </xsl:template>
    
    <xsl:template match="property">
        <property>
            <xsl:copy-of select="@code"/>
            <xsl:copy-of select="@type"/>
            <xsl:copy-of select="@description"/>
            <xsl:copy-of select="@uri"/>
        </property>
    </xsl:template>
    
    <xsl:template match="codedConcept">
        
        <xsl:param name="checkParentChild" as="xs:boolean"/>
        <xsl:param name="language" as="xs:string"/>
        
        <xsl:variable name="theCode" select="@code"/>
        <xsl:variable name="theLevel" select="if (@level castable as xs:integer) then xs:integer(@level) else ()" as="xs:integer?"/>
        <xsl:variable name="isAbstract" select="@type = 'A' or property[@code = 'notSelectable']/valueBoolean/@value = 'true'" as="xs:boolean"/>
        <xsl:variable name="effectiveDate" select="@effectiveDate"/>
        <xsl:variable name="expirationDate" select="(property[@code = 'deprecationDate']/valueDateTime/@value, property[@code = 'retirementDate']/valueDateTime/@value)[1]"/>
        <xsl:variable name="lastModifiedDate" select="@lastModifiedDate"/>
        
        <!-- Based on level a codedConcept is only a direct child of the current concept if it has current level+1, and is following the current concept, and does not also follow a concept with the same or lower level than the current concept -->
        <xsl:variable name="nextFirstOtherHierarchy" select="if ($checkParentChild) then () else (following-sibling::codedConcept[@level castable as xs:integer][xs:integer(@level) le $theLevel])[1]"/>
        <xsl:variable name="children" select="if ($checkParentChild) then key('parentcode', $theCode)/parent::codedConcept else following-sibling::codedConcept[@level castable as xs:integer][xs:integer(@level) = ($theLevel + 1)][not(preceding-sibling::codedConcept[@code = $nextFirstOtherHierarchy/@code])]" as="element(codedConcept)*"/>
        
        <xsl:variable name="parents" select="if ($checkParentChild) then key('code', parent/@code) else (preceding-sibling::codedConcept[@level castable as xs:integer][xs:integer(@level) = ($theLevel - 1)][1])" as="element(codedConcept)*"/>
        <xsl:variable name="ancestors" as="element()*">
            <xsl:apply-templates select="." mode="getAncestors">
                <xsl:with-param name="checkParentChild" select="$checkParentChild"/>
            </xsl:apply-templates>
        </xsl:variable>
        
        <xsl:variable name="theDisplayName" as="xs:string">
            <xsl:choose>
                <xsl:when test="designation[@language = $language]">
                    <xsl:value-of select="(designation[@language = $language][@type = 'preferred'], designation[@language = $language][@type = 'fsn'], designation[@language = $language])[1]/@displayName"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="(designation[@type = 'preferred'], designation[@type = 'fsn'], designation)[1]/@displayName"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:variable name="theType" as="xs:string">
            <xsl:choose>
                <xsl:when test="@statusCode = ('cancelled', 'rejected', 'deprecated', 'retired')">D</xsl:when>
                <xsl:when test="$isAbstract">A</xsl:when>
                <xsl:when test="exists($children)">S</xsl:when>
                <xsl:otherwise>L</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <!--<xsl:if test="empty(@statusCode)">
            <xsl:message terminate="yes">CodedConcept <xsl:value-of select="@code"/> SHALL have a status</xsl:message>
        </xsl:if>-->
        
        <!-- Input: CodedConceptStatusCodeLifeCycle , Output: CodedConceptStatusCodeLifeCycleStable
            "draft"                                 > draft
            "active"                                > active
            "deprecated"                            > retired
            "retired"                               > retired
            "experimental"                          > experimental
            
            Not expected to be in the set (should be filtered earlier) but for completeness sake
            "cancelled"                             > retired
            "rejected"                              > retired
        -->
        <concept>
            <!--<xsl:if test="$isAbstract = true()">
                <xsl:attribute name="abstract" select="$isAbstract"/>
            </xsl:if>-->
            <xsl:attribute name="code" select="$theCode"/> 
            <xsl:choose>
                <xsl:when test="@statusCode = 'deprecated'">
                    <xsl:attribute name="statusCode" select="'retired'"/>
                </xsl:when>
                <xsl:when test="@statusCode = 'cancelled'">
                    <xsl:attribute name="statusCode" select="'retired'"/>
                </xsl:when>
                <xsl:when test="@statusCode = 'rejected'">
                    <xsl:attribute name="statusCode" select="'retired'"/>
                </xsl:when>
                <xsl:when test="@statusCode">
                    <xsl:attribute name="statusCode" select="@statusCode"/>
                </xsl:when>
                <xsl:when test="ancestor::codeSystem/@statusCode = 'final'">
                    <xsl:attribute name="statusCode">active</xsl:attribute>
                </xsl:when>
                <xsl:when test="ancestor::codeSystem/@statusCode = 'rejected'">
                    <xsl:attribute name="statusCode">rejected</xsl:attribute>
                </xsl:when>
                <xsl:when test="ancestor::codeSystem/@statusCode = 'cancelled'">
                    <xsl:attribute name="statusCode">cancelled</xsl:attribute>
                </xsl:when>
                <xsl:when test="ancestor::codeSystem/@statusCode = 'deprecated'">
                    <xsl:attribute name="statusCode">deprecated</xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="statusCode">draft</xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:if test="$effectiveDate">
                <xsl:attribute name="effectiveDate" select="$effectiveDate"/>
            </xsl:if>
            <xsl:if test="$expirationDate">
                <xsl:attribute name="expirationDate" select="$expirationDate"/>
            </xsl:if>
            <xsl:if test="$lastModifiedDate">
                <xsl:attribute name="lastModifiedDate" select="$lastModifiedDate"/>
            </xsl:if>
            <xsl:if test="$theLevel">
                <xsl:attribute name="level" select="$theLevel"/>
            </xsl:if>
            <xsl:if test="$theType">
                <xsl:attribute name="type" select="$theType"/>
            </xsl:if>
            
            <xsl:for-each select="designation">
                <xsl:sort select="@language"/>
                <xsl:apply-templates select="."/>
            </xsl:for-each>
            
            <xsl:for-each select="desc">
                <xsl:sort select="@language"/>
                <xsl:apply-templates select="." mode="doDefinition"/>
            </xsl:for-each>
            
            <xsl:apply-templates select="$parents" mode="doParent">
                <xsl:with-param name="checkParentChild" select="$checkParentChild"/>
                <xsl:with-param name="theLevel" select="$theLevel"/>
            </xsl:apply-templates>
            
            <xsl:copy-of select="$ancestors[. != $theCode]"/>
            
            <xsl:apply-templates select="$ancestors" mode="doAncSlf"/>
            
            <xsl:apply-templates select="$children" mode="doChild">
                <xsl:with-param name="checkParentChild" select="$checkParentChild"/>
                <xsl:with-param name="theLevel" select="$theLevel"/>
            </xsl:apply-templates>
            
            <xsl:copy-of select="property"/>
            <xsl:choose>
                <xsl:when test="property[@code = 'notSelectable']"/>
                <xsl:when test="$isAbstract">
                    <property code="notSelectable">
                        <valueBoolean value="true"/>
                    </property>
                </xsl:when>
            </xsl:choose>
        </concept>
    </xsl:template>
    
    <xsl:template match="codedConcept" mode="designationToLabel">
        <xsl:for-each-group select="designation" group-by="@language">
            <xsl:sort select="@language"/>
            <xsl:variable name="theLanguage" select="current-grouping-key()"/>
            <xsl:variable name="theLabel" as="xs:string*">
                <xsl:choose>
                    <xsl:when test=".[@type = 'preferred']">
                        <xsl:value-of select=".[@type = 'preferred']/@displayName"/>
                    </xsl:when>
                    <xsl:when test=".[@type = 'fsn']">
                        <xsl:value-of select=".[@type = 'fsn']/@displayName"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="@displayName"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <label lang="{$theLanguage}">
                <xsl:value-of select="($theLabel)[1]"/>
            </label>
        </xsl:for-each-group>
    </xsl:template>
    
    <xsl:template match="codedConcept" mode="getAncestors">
        <xsl:param name="checkParentChild" as="xs:boolean"/>
        <xsl:param name="distance" select="0" as="xs:integer"/>

        <ancestor distance="{$distance}"><xsl:value-of select="@code"/></ancestor>
        <xsl:choose>
            <xsl:when test="$checkParentChild">
                <xsl:apply-templates select="key('code', parent/@code)" mode="getAncestors">
                    <xsl:with-param name="checkParentChild" select="$checkParentChild"/>
                    <xsl:with-param name="distance" select="$distance + 1"/>
                </xsl:apply-templates>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="theLevel" select="if (@level castable as xs:integer) then xs:integer(@level) else ()"/>
                <xsl:variable name="subParent" select="preceding-sibling::codedConcept[@level castable as xs:integer][xs:integer(@level) lt $theLevel][1]"/>
                
                <xsl:apply-templates select="$subParent" mode="getAncestors">
                    <xsl:with-param name="checkParentChild" select="$checkParentChild"/>
                    <xsl:with-param name="distance" select="$distance + 1"/>
                </xsl:apply-templates>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="designation">
        <xsl:variable name="theUse" as="xs:string">
            <xsl:choose>
                <xsl:when test="@type = 'preferred'">pref</xsl:when>
                <xsl:when test="@type = 'fsn'">fsn</xsl:when>
                <xsl:when test="@type = 'synonym'">syn</xsl:when>
                <xsl:when test="@type = 'abbreviation'">abbr</xsl:when>
                <xsl:otherwise>pref</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="theDisplay" select="@displayName" as="xs:string?"/>
        <xsl:variable name="theCount" select="count(tokenize(normalize-space($theDisplay), '\s'))"/>
        <xsl:variable name="theLength" select="string-length($theDisplay)"/>
        <designation>
            <xsl:attribute name="lang" select="@language"/>
            <xsl:attribute name="use" select="$theUse"/>
            <xsl:attribute name="statusCode">active</xsl:attribute>
            <!--<xsl:attribute name="effectiveDate"/>-->
            <!--<xsl:attribute name="expirationDate"/>-->
            <xsl:if test="@lastTranslated">
                <xsl:attribute name="lastEditDate" select="@lastTranslated"/>
            </xsl:if>
            <xsl:attribute name="count" select="$theCount"/>
            <xsl:attribute name="length" select="$theLength"/>
            <xsl:value-of select="$theDisplay"/>
        </designation>
    </xsl:template>
    
    <xsl:template match="desc" mode="doDefinition">
        <xsl:variable name="theCount" select="count(tokenize(@displayName, '\s'))"/>
        <xsl:variable name="theLength" select="string-length(@displayName)"/>
        <definition>
            <xsl:attribute name="lang" select="@language"/>
            <xsl:attribute name="statusCode">active</xsl:attribute>
            <!--<xsl:attribute name="effectiveDate"/>-->
            <!--<xsl:attribute name="expirationDate"/>-->
            <xsl:if test="@lastTranslated">
                <xsl:attribute name="lastEditDate" select="@lastTranslated"/>
            </xsl:if>
            <!--
            <xsl:attribute name="count" select="$theCount"/>
            <xsl:attribute name="length" select="$theLength"/>
            -->
            <xsl:copy-of select="node()"/></definition>
    </xsl:template>
    
    <xsl:template match="ancestor" mode="doAncSlf">
        <ancSlf>
            <xsl:value-of select="."/>
        </ancSlf>
    </xsl:template>
    
    <xsl:template match="*" mode="doParent">
        <xsl:param name="checkParentChild" as="xs:boolean"/>
        <xsl:param name="theLevel" as="xs:integer"/>
        
        <xsl:variable name="theSubCode" select="@code"/>
        <xsl:variable name="theSubLevel" select="if (@level castable as xs:integer) then xs:integer(@level) else ()"/>
        <xsl:variable name="isAbstract" select="@type = 'A' or property[@code = 'notSelectable']/valueBoolean/@value = 'true'" as="xs:boolean"/>
        
        <xsl:variable name="subNextSubFirstOtherHierarchy" select="if ($checkParentChild) then () else (following-sibling::codedConcept[@level castable as xs:integer][xs:integer(@level) le $theSubLevel])[1]"/>
        <xsl:variable name="subChildren" select="if ($checkParentChild) then key('parentcode', $theSubCode)/parent::codedConcept else following-sibling::codedConcept[@level castable as xs:integer][xs:integer(@level) = ($theSubLevel + 1)][not(preceding-sibling::codedConcept[@code = $subNextSubFirstOtherHierarchy/@code])]" as="element(codedConcept)*"/>
        
        <xsl:variable name="subParents" select="if ($checkParentChild) then key('code', parent/@code) else preceding-sibling::codedConcept[@level castable as xs:integer][xs:integer(@level) = ($theSubLevel - 1)][1]" as="element(codedConcept)*"/>
        
        <xsl:variable name="theSubType" as="xs:string">
            <xsl:choose>
                <xsl:when test="@statusCode = ('cancelled', 'rejected', 'deprecated', 'retired')">D</xsl:when>
                <xsl:when test="$isAbstract">A</xsl:when>
                <xsl:when test="$subChildren">S</xsl:when>
                <xsl:otherwise>L</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <parent pCode="{$theSubCode}" subCount="{count($subChildren)}" level="{$theLevel - 1}" type="{$theSubType}">
            <xsl:apply-templates select="." mode="designationToLabel"/>
        </parent>
    </xsl:template>
    
    <xsl:template match="*" mode="doChild">
        <xsl:param name="checkParentChild" as="xs:boolean"/>
        <xsl:param name="theLevel" as="xs:integer"/>
        
        <xsl:variable name="theSubCode" select="@code"/>
        <xsl:variable name="theSubLevel" select="if (@level castable as xs:integer) then xs:integer(@level) else ()"/>
        <xsl:variable name="isAbstract" select="@type = 'A' or property[@code = 'notSelectable']/valueBoolean/@value = 'true'" as="xs:boolean"/>
        
        <xsl:variable name="subNextSubFirstOtherHierarchy" select="if ($checkParentChild) then () else (following-sibling::codedConcept[@level castable as xs:integer][xs:integer(@level) le $theSubLevel])[1]"/>
        <xsl:variable name="subChildren" select="if ($checkParentChild) then key('parentcode', $theSubCode)/parent::codedConcept else following-sibling::codedConcept[@level castable as xs:integer][xs:integer(@level) = ($theSubLevel + 1)][not(preceding-sibling::codedConcept[@code = $subNextSubFirstOtherHierarchy/@code])]" as="element(codedConcept)*"/>
        
        <xsl:variable name="subParents" select="if ($checkParentChild) then key('code', parent/@code) else preceding-sibling::codedConcept[@level castable as xs:integer][xs:integer(@level) = ($theSubLevel - 1)][1]" as="element(codedConcept)*"/>
        
        <xsl:variable name="theSubType" as="xs:string">
            <xsl:choose>
                <xsl:when test="@statusCode = ('cancelled', 'rejected', 'deprecated', 'retired')">D</xsl:when>
                <xsl:when test="$isAbstract">A</xsl:when>
                <xsl:when test="$subChildren">S</xsl:when>
                <xsl:otherwise>L</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <child cCode="{$theSubCode}" subCount="{count($subChildren)}" level="{$theLevel + 1}" type="{$theSubType}">
            <xsl:apply-templates select="." mode="designationToLabel"/>
        </child>
    </xsl:template>
</xsl:stylesheet>