<!--
	Copyright (C) 2011-2017 ART-DECOR Expert Group (art-decor.org)
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html

--><xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" version="2.0">
    <xsl:output method="xml" indent="no" exclude-result-prefixes="#all" encoding="UTF-8"/>
    <xsl:key name="code" match="concept" use="@code"/>
    <xsl:template match="browsableCodeSystem">
        <browsableCodeSystem>
            <xsl:copy-of select="@*"/>
            <xsl:copy-of select="*[not(name() = 'concept')]"/>
            <xsl:for-each select="concept">
                <concept>
                    <xsl:copy-of select="@*"/>
                    <xsl:copy-of select="*[not(name() = ('child', 'parent'))]"/>
                    <xsl:for-each select="child">
                        <child>
                            <xsl:copy-of select="@*"/>
                            <xsl:attribute name="subCount" select="count(key('code', @cCode)/child)"/>
                            <xsl:copy-of select="*"/>
                        </child>
                    </xsl:for-each>
                    <xsl:for-each select="parent">
                        <parent>
                            <xsl:copy-of select="@*"/>
                            <xsl:attribute name="subCount" select="count(key('code', @pCode)/child)"/>
                            <xsl:copy-of select="*"/>
                        </parent>
                    </xsl:for-each>
                    <xsl:if test="parent">
                        <xsl:variable name="ancestors">
                            <xsl:for-each select="parent">
                                <ancestor aCode="{@pCode}" distance="1"/>
                                <xsl:call-template name="resolveAncestors">
                                    <xsl:with-param name="parentCode" select="@pCode"/>
                                    <xsl:with-param name="distance" select="2"/>
                                </xsl:call-template>
                            </xsl:for-each>
                        </xsl:variable>
                        <xsl:for-each select="distinct-values($ancestors/ancestor/@aCode)">
                            <xsl:variable name="currentCode" select="."/>
                            <xsl:variable name="minimumDistance" select="min($ancestors/ancestor[@aCode = $currentCode]/@distance)"/>
                            <ancestor distance="{$minimumDistance}">
                                <xsl:value-of select="$currentCode"/>
                            </ancestor>
                        </xsl:for-each>
                        <xsl:for-each select="distinct-values($ancestors/ancestor/@aCode)">
                            <ancSlf>
                                <xsl:value-of select="."/>
                            </ancSlf>
                        </xsl:for-each>
                    </xsl:if>
                    <ancSlf>
                        <xsl:value-of select="@code"/>
                    </ancSlf>
                </concept>
            </xsl:for-each>
        </browsableCodeSystem>
    </xsl:template>
    <xsl:template name="resolveAncestors">
        <xsl:param name="parentCode"/>
        <xsl:param name="distance"/>
        <xsl:if test="$distance lt 11">
            <xsl:for-each select="key('code', $parentCode)/parent">
                <ancestor aCode="{@pCode}" distance="{$distance}"/>
                <xsl:call-template name="resolveAncestors">
                    <xsl:with-param name="parentCode" select="@pCode"/>
                    <xsl:with-param name="distance" select="$distance + 1"/>
                </xsl:call-template>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>