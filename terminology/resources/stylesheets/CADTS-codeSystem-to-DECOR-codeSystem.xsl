<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="#all"
    version="2.0">
    <!-- 
        Purpose: create a DECOR project codeSystem from a Centralized ART-DECOR Terminology Services codeSystem 
        
        Skips cancelled and rejected codedConcepts
        
        Input: CADTS codeSystem for view/expand
        Output: DECOR codeSystem from a project 
    -->
    
    <xsl:output indent="yes" method="xml" exclude-result-prefixes="#all"/>
    <xsl:key name="code" match="codedConcept[not(@statusCode = ('cancelled', 'rejected'))]" use="@code"/>
    <xsl:key name="parentcode" match="codedConcept[not(@statusCode = ('cancelled', 'rejected'))]/parent" use="@code"/>
    
    <xsl:param name="language" as="xs:string?"/>
    
    <!-- Target Format
        
        <codeSystem id="2.16.840.1.113883.3.1937.99.60.11.5.2" name="codesystem2" displayName="Codesystem 2: Simple list multi designations" effectiveDate="2022-02-18T00:00:00" statusCode="draft">
            <desc language="en-US" lastTranslated="2022-06-09T18:05:47">This is a codesystem with mutliple designations per code</desc>
            <conceptList>
                <codedConcept code="A" statusCode="draft" level="0" type="S">
                    <designation language="en-US" type="preferred" displayName="Code A"/>
                    <designation language="de-DE" type="preferred" displayName="Kode A"/>
                    <designation language="en-US" type="synonym" displayName="Acode"/>
                </codedConcept>
                <codedConcept code="B" statusCode="draft" level="1" type="L">
                    <designation language="en-US" type="preferred" displayName="Code B"/>
                    <designation language="de-DE" type="preferred" displayName="Kode B"/>
                    <designation language="en-US" type="synonym" displayName="Bcode"/>
                    <parent code="A"/>
                    <parent code="C"/>
                </codedConcept>
                <codedConcept code="C" statusCode="draft" level="0" type="S">
                    <designation language="en-US" type="preferred" displayName="Code C"/>
                    <designation language="de-DE" type="preferred" displayName="Kode C"/>
                    <designation language="en-US" type="synonym" displayName="Ccode"/>
                </codedConcept>
            </conceptList>
        </codeSystem>
    
    -->
    <xsl:template match="/">
        <xsl:apply-templates select="//browsableCodeSystem"/>
    </xsl:template>
    
    <xsl:template match="browsableCodeSystem">
        <xsl:variable name="language" select="($language, @defaultLanguage, .//@language)[1]" as="xs:string"/>
        <xsl:variable name="checkParentChild" select="exists(//codedConcept[parent | child])"/>
        <xsl:variable name="codedConcepts" select=".//codedConcept" as="element()*"/>
        <xsl:variable name="codedConceptCount" select="count($codedConcepts)"/>
        <codeSystem>
            <!-- <codeSystem id="2.....2" name="codesystem2" displayName="..." effectiveDate="2022-02-18T00:00:00" statusCode="draft"> -->
            <xsl:attribute name="id" select="@oid"/>
            <xsl:attribute name="name" select="@id"/>
            <xsl:attribute name="displayName" select="name/text()"/>
            <xsl:attribute name="effectiveDate" select="concat(@effectiveDate, 'T00:00:00')"/>
            <xsl:choose>
                <xsl:when test="@statusCode='active'">
                    <xsl:attribute name="statusCode" select="'final'"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="statusCode" select="@statusCode"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:attribute name="type" select="@type"/>
            <xsl:attribute name="canonicalUri" select="(@canonicalUri, 'urn:oid:' || identifier/@id)[1]"/>
            <xsl:attribute name="experimental" select="@experimental = 'true'"/>
            
            <xsl:apply-templates select="description"/>
            
            <xsl:for-each select="license">
                <publishingAuthority>
                    <xsl:copy-of select="@*|node()"/>
                </publishingAuthority>
            </xsl:for-each>

            <xsl:for-each select="license">
                <copyright>
                    <xsl:copy-of select="@*|node()"/>
                </copyright>
            </xsl:for-each>
            
            <xsl:for-each select="property">
                <!-- <property code="preferred" uri="https://www.dvmd.de/properties-KDL#preferred" type="string"/> -->
                <property code="{@code}" uri="{@uri}" type="{@type}"/>
            </xsl:for-each>
            
            <conceptList>
                <xsl:apply-templates select="concept[not(@statusCode = ('cancelled', 'rejected'))]">
                    <xsl:with-param name="language" select="$language"/>
                </xsl:apply-templates>
            </conceptList>
            
        </codeSystem>
    </xsl:template>
    
    <xsl:template match="description">
        <xsl:if test="node()">
            <desc language="{@language}"><xsl:copy-of select="node()"/></desc>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="concept">
        <!-- 
        <concept code="AD" statusCode="active" level="0">
            <designation language="de-DE" use="preferred" count="1" length="17">Arztdokumentation</designation>
            <child cCode="AD0101" subCount="14">
                <label language="de-DE">Arztberichte</label>
            </child>
            <child cCode="AD0201" subCount="9">
                <label language="de-DE">Bescheinigungen</label>
            </child>
            <child cCode="AD0202" subCount="9">
                <label language="de-DE">Befunderhebungen</label>
            </child>
            <child cCode="AD0601" subCount="10">
                <label language="de-DE">Fallbesprechungen</label>
            </child>
            <ancSlf>AD</ancSlf>
        </concept>
        <concept code="AD010102" statusCode="active" level="2">
            <designation language="de-DE" use="preferred" count="1" length="21">Durchgangsarztbericht</designation>
            <definition language="de-DE">Die Dokumentation beinhaltet die ärztliche Beurteilung des Arbeits- bzw. Wegeunfalls auf standardisiertem Formular. Exkl.: NachschauberichtDie Dokumentation beinhaltet die ärztliche Beurteilung des Arbeits- bzw. Wegeunfalls auf standardisiertem Formular. Exkl.: Nachschaubericht</definition>
            <parent pCode="AD0101" subCount="14">
                <label language="de-DE">Arztberichte</label>
            </parent>
            <ancestor distance="1">AD0101</ancestor>
            <ancestor distance="2">AD</ancestor>
            <ancSlf>AD0101</ancSlf>
            <ancSlf>AD</ancSlf>
            <ancSlf>AD010102</ancSlf>
        </concept>
        -->
        <xsl:param name="language" as="xs:string"/>
        <xsl:variable name="theCode" select="@code"/>
        <xsl:variable name="theLevel" select="if (@level castable as xs:integer) then xs:integer(@level) else ()"/>
        <xsl:variable name="isAbstract" select="@type = 'A' or property[@code = 'notSelectable']/valueBoolean/@value = 'true'" as="xs:boolean"/>
        <!--<xsl:variable name="effectiveDate"/>-->
        <!--<xsl:variable name="expirationDate" select="(property[@code = 'deprecationDate']/valueDateTime/@value, property[@code = 'retirementDate']/valueDateTime/@value)[1]"/>-->
        <xsl:variable name="lastModifiedDate" select="@lastModifiedDate"/>
        
        <!-- Based on level a codedConcept is only a direct child of the current concept if it has current level+1, and is following the current concept, and does not also follow a concept with the same or lower level than the current concept -->
        <!--<xsl:variable name="nextFirstOtherHierarchy" select="if ($checkParentChild) then () else (following-sibling::codedConcept[@level castable as xs:integer][xs:integer(@level) le $theLevel])[1]"/>
        <xsl:variable name="children" select="if ($checkParentChild) then key('parentcode', $theCode)/parent::codedConcept else following-sibling::codedConcept[@level castable as xs:integer][xs:integer(@level) = ($theLevel + 1)][not(preceding-sibling::codedConcept[@code = $nextFirstOtherHierarchy/@code])]" as="element(codedConcept)*"/>
        -->
        <!-- make the codedConcept
             <codedConcept code="AB" statusCode="draft" level="1" type="L">
                    <designation language="en-US" type="preferred" displayName="Code AB"/>
                    <parent code="A"/>
             </codedConcept>
        -->
        
        <xsl:variable name="theDisplayName" as="xs:string">
            <xsl:choose>
                <xsl:when test="designation[@language = $language]">
                    <xsl:value-of select="(designation[@language = $language][@type = 'preferred'], designation[@language = $language][@type = 'fsn'], designation[@language = $language])[1]/@displayName"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="(designation[@type = 'preferred'], designation[@type = 'fsn'], designation)[1]/@displayName"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="theType" as="xs:string">
            <xsl:choose>
                <xsl:when test="@statusCode = ('cancelled', 'rejected', 'deprecated', 'retired')">D</xsl:when>
                <xsl:when test="$isAbstract">A</xsl:when>
                <xsl:when test="child">S</xsl:when>
                <xsl:otherwise>L</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <codedConcept>
            <xsl:attribute name="code" select="$theCode"/> 
            <xsl:choose>
                <xsl:when test="@statusCode='active'">
                    <xsl:attribute name="statusCode">active</xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="statusCode">deprecated</xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>             
            <!--<xsl:attribute name="abstract" select="$isAbstract"/>-->
            <xsl:attribute name="level" select="$theLevel"/> 
            <xsl:attribute name="type" select="$theType"/>
            
            <xsl:for-each select="designation">
                <xsl:sort select="@language"/>
                <xsl:apply-templates select="."/>
            </xsl:for-each>
            
            <xsl:for-each select="definition">
                <xsl:sort select="@language"/>
                <xsl:apply-templates select="." mode="doDefinition"/>
            </xsl:for-each>
            
            <xsl:copy-of select="property"/>
            
            <!-- 
                <parent pCode="AD0101" subCount="14">
                    <label language="de-DE">Arztberichte</label>
                </parent>
            -->
            <xsl:for-each select="parent">
                <parent code="{@pCode}"/>
            </xsl:for-each>
        </codedConcept>
    </xsl:template>
    
    <xsl:template match="designation">
        <designation>
            <xsl:attribute name="language" select="@language"/>
            <xsl:attribute name="type">
                <xsl:choose>
                    <xsl:when test="pref">preferred</xsl:when>
                    <xsl:when test="syn">synonym</xsl:when>
                    <xsl:when test="abbr">abbreviation</xsl:when>
                    <xsl:when test="fsn">fsn</xsl:when>
                    <xsl:when test="lfsn">fsn</xsl:when>
                    <xsl:when test="rel">preferred</xsl:when>
                    <xsl:when test="prefShrt">preferred</xsl:when>
                    <xsl:when test="prefPat">patient-friendly</xsl:when>
                    <xsl:when test="prefPatB1">patient-friendly</xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="@use"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <!--
            <xsl:choose>
                <xsl:when test="@statusCode='inactive'">
                    <xsl:attribute name="statusCode" select="'deprecated'"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="statusCode" select="'active'"/>
                </xsl:otherwise>
            </xsl:choose>
            -->
            <xsl:attribute name="displayName" select="node()"/>
            <xsl:copy-of select="@lastModifiedDate"/>
            <xsl:copy-of select="node()"/>
        </designation>
    </xsl:template>
    
    <xsl:template match="desc|definition" mode="doDefinition">
        <xsl:variable name="theCount" select="count(tokenize(@displayName, '\s'))"/>
        <xsl:variable name="theLength" select="string-length(@displayName)"/>
        <desc>
            <xsl:attribute name="language" select="@language"/>
            <!--<xsl:attribute name="statusCode">active</xsl:attribute>-->
            <!--<xsl:attribute name="effectiveDate"/>-->
            <!--<xsl:attribute name="expirationDate"/>-->
            <xsl:copy-of select="@lastModifiedDate"/>
            <xsl:copy-of select="node()"/>
        </desc>
    </xsl:template>

</xsl:stylesheet>