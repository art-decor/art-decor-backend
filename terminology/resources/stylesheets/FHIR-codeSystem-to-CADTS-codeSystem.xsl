<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fhir="http://hl7.org/fhir" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs fhir" version="2.0" xml:space="default" xpath-default-namespace="">
    <xsl:output indent="yes" method="xml"/>
    <xsl:key name="code" match="fhir:concept" use="fhir:code/@value"/>
    <xsl:key name="child" match="fhir:concept" use="fhir:property[fhir:code/@value = 'subsumedBy']/fhir:valueCode/@value"/>
    <xsl:variable name="defaultLanguage" select="'en-US'"/>
    <xsl:template match="fhir:CodeSystem">
        <browsableCodeSystem>
            <xsl:attribute name="id">
                <xsl:value-of select="fhir:id/@value"/>
            </xsl:attribute>
            <xsl:attribute name="oid">
                <xsl:value-of select="fhir:identifier/fhir:value/substring-after(@value, 'urn:oid:')"/>
            </xsl:attribute>
            <xsl:attribute name="url">
                <xsl:value-of select="fhir:url/@value"/>
            </xsl:attribute>
            <xsl:attribute name="version">
                <xsl:value-of select="fhir:version/@value"/>
            </xsl:attribute>
            <xsl:attribute name="statusCode">
                <xsl:value-of select="fhir:status/@value"/>
            </xsl:attribute>
            <xsl:attribute name="experimental">
                <xsl:choose>
                    <xsl:when test="fhir:experimental/@value = 'true'">
                        <xsl:value-of select="'true'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'true'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="effectiveDate">
                <xsl:choose>
                    <xsl:when test="fhir:date/@value castable as xs:dateTime">
                        <xsl:value-of select="format-dateTime(fhir:date/@value, '[Y0001]-[M01]-[D01]')"/>
                    </xsl:when>
                    <xsl:when test="fhir:date/@value castable as xs:date">
                        <xsl:value-of select="format-date(fhir:date/@value, '[Y0001]-[M01]-[D01]')"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="defaultLanguage">
                <xsl:value-of select="$defaultLanguage"/>
            </xsl:attribute>
            <xsl:attribute name="content">
                <xsl:value-of select="fhir:content/@value"/>
            </xsl:attribute>
<!--            <xsl:attribute name="count">
                <xsl:value-of select="fhir:count/@value"/>
            </xsl:attribute>-->
            <identifier>
                <xsl:attribute name="system">
                    <xsl:value-of select="fhir:identifier/fhir:system/@value"/>
                </xsl:attribute>
                <xsl:attribute name="id">
                    <xsl:value-of select="fhir:identifier/fhir:value/@value"/>
                </xsl:attribute>
            </identifier>
            <name language="{$defaultLanguage}">
                <xsl:value-of select="fhir:name/@value"/>
            </name>
            <license>
                <text>License text</text>
            </license>
            <publishingAuthority name="{fhir:publisher/@value}">
                <xsl:for-each select="//fhir:telecom">
                    <addrLine>
                        <xsl:attribute name="type">
                            <xsl:choose>
                                <xsl:when test="fhir:system/@value = 'url'">
                                    <xsl:value-of select="'uri'"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="fhir:system/@value"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:attribute>
                        <xsl:value-of select="fhir:value/@value"/>
                    </addrLine>
                </xsl:for-each>
            </publishingAuthority>
            <description language="{$defaultLanguage}">
                <xsl:value-of select="fhir:description/@value"/>
            </description>
            <xsl:for-each select="//fhir:concept">
                <xsl:call-template name="concept"/>
            </xsl:for-each>
        </browsableCodeSystem>
    </xsl:template>
    <xsl:template name="concept">
        <concept>
            <!-- test if abstract -->
            <xsl:if test="fhir:property/fhir:code[@value = 'notSelectable']">
                <xsl:attribute name="abstract">
                    <xsl:value-of select="fhir:property[fhir:code/@value = 'notSelectable']/fhir:valueBoolean/@value"/>
                </xsl:attribute>
            </xsl:if>
            <!-- code -->
            <xsl:attribute name="code">
                <xsl:value-of select="fhir:code/@value"/>
            </xsl:attribute>
            <!-- statusCode -->
            <xsl:attribute name="statusCode">
                <xsl:choose>
                    <xsl:when test="fhir:property[fhir:code/@value = 'status']">
                        <xsl:value-of select="fhir:property[fhir:code/@value = 'status']/fhir:valueCode/@value"/>
                    </xsl:when>
                <xsl:otherwise>
                        <xsl:value-of select="'active'"/>
                    </xsl:otherwise>
                </xsl:choose>
                </xsl:attribute>
            <xsl:attribute name="lastModifiedDate"/>
            <!-- designation from display name -->
            <designation lang="{$defaultLanguage}" use="pref" statusCode="active" effectiveDate="" expirationDate="" lastModifiedDate="">
                <xsl:attribute name="count">
                    <xsl:value-of select="count(tokenize(fhir:display/@value, '\s'))"/>
                </xsl:attribute>
                <xsl:attribute name="length">
                    <xsl:value-of select="string-length(fhir:display/@value)"/>
                </xsl:attribute>
                <xsl:value-of select="fhir:display/@value"/>
            </designation>
            <!-- designations  -->
            <xsl:for-each select="fhir:designation">
                <xsl:variable name="designationLanguage">
                    <xsl:choose>
                        <xsl:when test="fhir:language/@value = 'de'">
                            <xsl:value-of select="'de-DE'"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$defaultLanguage"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="designationUse">
                    <xsl:choose>
                        <xsl:when test="fhir:use/fhir:code/@value = 'preferredForLanguage'">
                            <xsl:value-of select="'pref'"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="'syn'"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <designation statusCode="active" effectiveDate="" expirationDate="" lastModifiedDate="">
                    <xsl:attribute name="lang">
                        <xsl:value-of select="$designationLanguage"/>
                    </xsl:attribute>
                    <xsl:attribute name="use">
                        <xsl:value-of select="$designationUse"/>
                    </xsl:attribute>
                    <xsl:attribute name="count">
                        <xsl:value-of select="count(tokenize(fhir:value/@value, '\s'))"/>
                    </xsl:attribute>
                    <xsl:attribute name="length">
                        <xsl:value-of select="string-length(fhir:value/@value)"/>
                    </xsl:attribute>
                    <xsl:value-of select="fhir:value/@value"/>
                </designation>
            </xsl:for-each>
            <xsl:if test="string-length(normalize-space(fhir:definition/@value)) &gt; 0">
                <definition lang="{$defaultLanguage}" statusCode="active" effectiveDate="" expirationDate="" lastModifiedDate="">
                    <xsl:value-of select="normalize-space(fhir:definition/@value)"/>
                </definition>
            </xsl:if>
            <!-- children definied by extension -->
            <xsl:for-each select="fhir:modifierExtension[@url = 'http://hl7.org/fhir/StructureDefinition/codesystem-subsumes']">
                <child>
                    <xsl:variable name="child" select="key('code', fhir:valueCode/@value)"/>
                    <xsl:attribute name="cCode">
                        <xsl:value-of select="fhir:valueCode/@value"/>
                    </xsl:attribute>
                    <label lang="{$defaultLanguage}">
                        <xsl:value-of select="$child/fhir:display/@value"/>
                    </label>
                </child>
            </xsl:for-each>
            <!-- children defined by hierarchy -->
            <xsl:for-each select="fhir:concept">
                <child>
                    <xsl:attribute name="cCode">
                        <xsl:value-of select="fhir:code/@value"/>
                    </xsl:attribute>
                    <label lang="{$defaultLanguage}">
                        <xsl:value-of select="fhir:display/@value"/>
                    </label>
                </child>
            </xsl:for-each>
            <!-- children defined by subsumption -->
            <xsl:for-each select="key('child', fhir:code/@value)">
                <child>
                    <xsl:attribute name="cCode">
                        <xsl:value-of select="fhir:code/@value"/>
                    </xsl:attribute>
                    <label lang="{$defaultLanguage}">
                        <xsl:value-of select="fhir:display/@value"/>
                    </label>
                </child>
            </xsl:for-each>
            <!-- parents definied by subsumedBy -->
            <xsl:for-each select="fhir:property[fhir:code/@value = 'subsumedBy']">
                <parent>
                    <xsl:variable name="parent" select="key('code', fhir:valueCode/@value)"/>
                    <xsl:attribute name="pCode">
                        <xsl:value-of select="fhir:valueCode/@value"/>
                    </xsl:attribute>
                    <label lang="{$defaultLanguage}">
                        <xsl:value-of select="$parent/fhir:display/@value"/>
                    </label>
                </parent>
            </xsl:for-each>
        </concept>
    </xsl:template>
</xsl:stylesheet>