xquery version "1.0";
(:
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace sm      = "http://exist-db.org/xquery/securitymanager";
import module namespace xmldb   = "http://exist-db.org/xquery/xmldb";
import module namespace repo    = "http://exist-db.org/xquery/repo";
declare namespace cfg           = "http://exist-db.org/collection-config/1.0";

(: The following external variables are set by the repo:deploy function :)

(: file path pointing to the exist installation directory :)
declare variable $home external;
(: path to the directory containing the unpacked .xar package :)
declare variable $dir external;
(: the target collection into which the app is deployed :)
declare variable $target external;
declare variable $root := repo:get-root();



declare %private function local:createICACollections() {
    let $b1             := 'terminology-data'
    let $b2             := 'ica-data'
    let $baseDir        := concat($b1,'/',$b2)
    let $icaConf        :=
        <collection xmlns="http://exist-db.org/collection-config/1.0">
            <index xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:mods="http://www.loc.gov/mods/v3">
                <fulltext default="none" attributes="false"/>
                <!-- eXist-db 2.2 -->
                <range>
                    <create qname="@effectiveDate" type="xs:string"/>
                    <create qname="@expirationDate" type="xs:string"/>
                    <create qname="@id" type="xs:string"/>
                    <create qname="@code" type="xs:string"/>
                    <create qname="@statusCode" type="xs:string"/>
                </range>
                <!-- eXist-db 2.1 -->
                <create qname="@effectiveDate" type="xs:string"/>
                <create qname="@expirationDate" type="xs:string"/>
                <create qname="@id" type="xs:string"/>
                <create qname="@code" type="xs:string"/>
                <create qname="@statusCode" type="xs:string"/>
            </index>
        </collection>
    
    return (
        (:/db/apps/terminology-data/ica-data:)
        if (not(xmldb:collection-available(concat($root,$baseDir)))) then
            xmldb:create-collection(concat($root,$b1),$b2)
        else()
        ,
        for $coll in ('concepts','history','log','meta', 'releases')
        return (
            if (not(xmldb:collection-available(concat($root,$baseDir,'/',$coll)))) then (
                xmldb:create-collection(concat($root,$baseDir),$coll)
            ) else ()
        )
        ,
        (:/db/system/config/terminology-data/ica-data:)
        if (not(xmldb:collection-available(concat('/db/system/config',$root,$baseDir)))) then
            xmldb:create-collection(concat('/db/system/config',$root),$baseDir)
        else()
        ,
        let $index-file := concat('/db/system/config',$root,$baseDir,'/collection.xconf')
        return
        if (doc-available($index-file) and deep-equal(doc($index-file)/cfg:collection,$icaConf)) then () else (
            xmldb:store(concat('/db/system/config',$root,$baseDir),'collection.xconf',$icaConf)
            ,
            xmldb:reindex(concat($root,$baseDir))
        )
    )
};

declare %private function local:createTerminologyDemoCollections() {
    let $b1             := 'terminology-data'
    let $b2             := 'nictiz-demo-data'
    let $baseDir        := concat($b1,'/',$b2)
    let $demoConf        :=
                           <collection xmlns="http://exist-db.org/collection-config/1.0">
                              <index xmlns:xs="http://www.w3.org/2001/XMLSchema">
                                 <lucene>
                                    <analyzer class="org.apache.lucene.analysis.standard.StandardAnalyzer">
                                       <param name="stopwords" type="org.apache.lucene.analysis.util.CharArraySet"/>
                                    </analyzer>
                                    <text qname="desc"/>
                                 </lucene>
                                 <range>
                                    <create qname="@conceptId" type="xs:string"/>
                                    <create qname="@code" type="xs:string"/>
                                 </range>
                              </index>
                           </collection>
    
    return (
        (:/db/apps/terminology-data/nictiz-demo-data:)
        if (not(xmldb:collection-available(concat($root,$baseDir)))) then
            xmldb:create-collection(concat($root,$b1),$b2)
        else()
        ,
        (:/db/system/config/terminology-data/ica-data:)
        if (not(xmldb:collection-available(concat('/db/system/config',$root,$baseDir)))) then
            xmldb:create-collection(concat('/db/system/config',$root),$baseDir)
        else()
        ,
        let $index-file := concat('/db/system/config',$root,$baseDir,'/collection.xconf')
        return
        if (doc-available($index-file) and deep-equal(doc($index-file)/cfg:collection,$demoConf)) then () else (
            xmldb:store(concat('/db/system/config',$root,$baseDir),'collection.xconf',$demoConf)
            ,
            xmldb:reindex(concat($root,$baseDir))
        )
    )
};


declare %private function local:createTerminologyCollections(){
   let $terminologyBaseCollection      := 'terminology-data'
   let $terminologyCollections         := ('codesystem-stable-data','codesystem-authoring-data','valueset-stable-data','valueset-authoring-data','conceptmap-stable-data','conceptmap-authoring-data','valueset-expansion-data')
   let $terminologyExternalCollection  := 'external'
   let $terminologyExternalGroups      := ('hl7','ihe','iso','orphanet','regenstrief','rsna','snomed','who')
   let $codesystemConf                 :=
                                       <collection xmlns="http://exist-db.org/collection-config/1.0">
                                        <index xmlns:mods="http://www.loc.gov/mods/v3" xmlns:xlink="http://www.w3.org/1999/xlink">
                                            <fulltext default="none" attributes="false"/>
                                            <lucene>
                                                <analyzer class="org.apache.lucene.analysis.standard.StandardAnalyzer">
                                                    <param name="stopwords" type="org.apache.lucene.analysis.util.CharArraySet"/>
                                                </analyzer>
                                                <text qname="@id"/>
                                                <text qname="designation"/>
                                                <text qname="definition"/>
                                                <text qname="name"/>
                                            </lucene>
                                            <range>
                                                <create qname="@id" type="xs:string"/>
                                                <create qname="@oid" type="xs:string"/>
                                                <create qname="@code" type="xs:string"/>
                                                <create qname="@cCode" type="xs:string"/>
                                                <create qname="@pCode" type="xs:string"/>
                                                <create qname="@name" type="xs:string"/>
                                                <create qname="@type" type="xs:string"/>
                                                <create qname="@typeCode" type="xs:string"/>
                                                <create qname="@destCode" type="xs:string"/>
                                                <create qname="@use" type="xs:string"/>
                                                <create qname="@level" type="xs:string"/>
                                                <create qname="@value" type="xs:string"/>
                                                <create qname="@statusCode" type="xs:string"/>
                                                <create qname="@effectiveDate" type="xs:string"/>
                                                <create qname="ancestor" type="xs:string"/>
                                                <create qname="ancest" type="xs:string"/>
                                                <create qname="ancSlf" type="xs:string"/>
                                            </range>
                                        </index>
                                       </collection>    
   let $valuesetConf                   :=
                                       <collection xmlns="http://exist-db.org/collection-config/1.0">
                                        <index xmlns:mods="http://www.loc.gov/mods/v3" xmlns:xlink="http://www.w3.org/1999/xlink">
                                            <fulltext default="none" attributes="false"/>
                                            <lucene>
                                                <analyzer class="org.apache.lucene.analysis.standard.StandardAnalyzer">
                                                    <param name="stopwords" type="org.apache.lucene.analysis.util.CharArraySet"/>
                                                </analyzer>
                                                <text qname="designation"/>
                                                <text qname="definition"/>
                                                <text qname="name"/>
                                            </lucene>
                                            <range>
                                                <create qname="@id" type="xs:string"/>
                                                <create qname="@code" type="xs:string"/>
                                                <create qname="@name" type="xs:string"/>
                                                <create qname="@type" type="xs:string"/>
                                                <create qname="@use" type="xs:string"/>
                                                <create qname="@level" type="xs:string"/>
                                                <create qname="@value" type="xs:string"/>
                                                <create qname="@statusCode" type="xs:string"/>
                                                <create qname="@effectiveDate" type="xs:string"/>
                                            </range>
                                        </index>
                                       </collection>
   let $conceptmapConf                 :=
                                       <collection xmlns="http://exist-db.org/collection-config/1.0">
                                        <index xmlns:mods="http://www.loc.gov/mods/v3" xmlns:xlink="http://www.w3.org/1999/xlink">
                                            <fulltext default="none" attributes="false"/>
                                            <lucene>
                                                <analyzer class="org.apache.lucene.analysis.standard.StandardAnalyzer">
                                                    <param name="stopwords" type="org.apache.lucene.analysis.util.CharArraySet"/>
                                                </analyzer>
                                                <text qname="designation"/>
                                                <text qname="definition"/>
                                            </lucene>
                                            <range>
                                                <create qname="@id" type="xs:string"/>
                                                <create qname="@code" type="xs:string"/>
                                                <create qname="@cCode" type="xs:string"/>
                                                <create qname="@pCode" type="xs:string"/>
                                                <create qname="@name" type="xs:string"/>
                                                <create qname="@type" type="xs:string"/>
                                                <create qname="@typeCode" type="xs:string"/>
                                                <create qname="@destCode" type="xs:string"/>
                                                <create qname="@use" type="xs:string"/>
                                                <create qname="@level" type="xs:string"/>
                                                <create qname="@value" type="xs:string"/>
                                                <create qname="@statusCode" type="xs:string"/>
                                                <create qname="@effectiveDate" type="xs:string"/>
                                                <create qname="ancestor" type="xs:string"/>
                                                <create qname="ancest" type="xs:string"/>
                                                <create qname="ancSlf" type="xs:string"/>
                                            </range>
                                        </index>
                                       </collection>     
    return (
        (: toplevel terminology collections:)
        for $collection in $terminologyCollections
        return
            xmldb:create-collection(concat($root,$terminologyBaseCollection,'/'),$collection)
        ,
        (: external terminology collections:)
        for $toplevel in $terminologyCollections
        return (
             if (contains($toplevel,'stable') or contains($toplevel,'expansion')) then (
                 xmldb:create-collection(concat($root,$terminologyBaseCollection,'/',$toplevel), $terminologyExternalCollection)
             ) else ()
        )
        ,
        (:   governamce group collections:)
        for $toplevel in $terminologyCollections
        return (
           if (contains($toplevel,'stable')) then
              for $externalCollection in $terminologyExternalGroups
              return
                  xmldb:create-collection(concat($root,$terminologyBaseCollection,'/',$toplevel,'/',$terminologyExternalCollection), $externalCollection)
           else ()
        )
        ,
        for $toplevel in $terminologyCollections
        let $indexDefinition:= 
             if (starts-with($toplevel,'valueset')) then
                $valuesetConf 
             else if (starts-with($toplevel,'concept')) then
                $conceptmapConf
             else (
                $codesystemConf
             )
        return (
            xmldb:create-collection(concat('/db/system/config', $root, $terminologyBaseCollection),$toplevel)
            ,
            let $index-file := concat('/db/system/config', $root, $terminologyBaseCollection, '/', $toplevel, '/collection.xconf')
            return
            if (doc-available($index-file) and deep-equal(doc($index-file)/cfg:collection, $indexDefinition)) then () else (
                xmldb:store(concat('/db/system/config', $root, $terminologyBaseCollection, '/', $toplevel), 'collection.xconf', $indexDefinition)
                ,
                xmldb:reindex(concat($root,$terminologyBaseCollection,'/',$toplevel))
            )
        )
    )
};


(: store the collection configuration :)
xmldb:create-collection('/db/system/config',$target),
xmldb:store-files-from-pattern(concat("/system/config", $target), $dir, "*.xconf"),
local:createICACollections(),
local:createTerminologyCollections(),
local:createTerminologyDemoCollections()