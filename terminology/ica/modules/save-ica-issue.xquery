xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get    = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace aduser = "http://art-decor.org/ns/art-decor-users" at "../../../art/api/api-user-settings.xqm";
declare namespace request      = "http://exist-db.org/xquery/request";
declare namespace xhtml        = "http://www.w3.org/1999/xhtml";

let $request         := request:get-data()/issue

let $issues             := collection(concat($get:strTerminologyData,'/ica-data/meta'))//issues
let $userName        := $request/tracking/author
let $userDisplayName :=
    try {
        aduser:getUserDisplayName($userName)
    }
    catch * {
        $userName
    }

let $newIssue := 	
    <issue id="{util:uuid()}" priority="{$request/@priority}" displayName="{$request/@displayName}" type="{$request/@type}">
    {
        if ($request/object) then
            <object id="{$request/object/@id}" type="{$request/object/@type}" effectiveDate="{$request/object/@effectiveDate}"/>
        else()
    }
        <tracking effectiveDate="{format-dateTime(current-dateTime(), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')}" statusCode="open">
           <author id="{$request/tracking/author/@id}">{$userDisplayName}</author>
           <desc language="{string($request/tracking/desc/@language)}">{util:parse-html($request/tracking/desc)//xhtml:body/text()|util:parse-html($request/tracking/desc)//BODY/node()}</desc>
        </tracking>
    </issue>

return
<response>
{
 update insert $newIssue into $issues
}
</response>