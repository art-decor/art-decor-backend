xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace art ="http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
(:let $language := request:get-parameter('lang','nl-NL'):)
(:let $language := 'nl-NL':)

let $icaMapping       := request:get-data()/version

let $storedMapping    := collection(concat($get:strTerminologyData,'/ica-data/concepts'))//cics/version[@effectiveDate=$icaMapping/@effectiveDate]

let $ciUpdate         :=
    for $ci in $icaMapping/ci[edit]
    let $statusCode   := if ($ci/@statusCode='new') then 'draft' else ($ci/@statusCode/string())
    let $preparedCi   :=
        <ci id="{$ci/@id}" code="{$ci/@code}" statusCode="{$statusCode}" effectiveDate="{$ci/@effectiveDate}">
        {
            for $desc in $ci/description
            return
                art:parseNode($desc)
            ,
            for $rationale in $ci/rationale
            return
                art:parseNode($rationale)
            ,
            $ci/text,
            $ci/cic,
            $ci/icpc,
            $ci/icd-9,
            $ci/icd-10,
            $ci/snomed,
            $ci/other
        }
        </ci>
    return
    if ($ci[edit/@mode='edit']) then
        update replace $storedMapping//ci[@id=$ci/@id] with $preparedCi
    else if ($ci[edit/@mode='new']) then
        update insert $preparedCi into $storedMapping
    else ()

return
    <list-safe>true</list-safe>