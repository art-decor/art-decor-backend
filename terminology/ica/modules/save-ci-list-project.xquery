xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art ="http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";

let $editedProject := request:get-data()/project
(:let $editedProject :=
<project>
    <author id="1" username="admin" admin="true" authorize="false" edit="false" issues="false">Admin</author>
    <author id="2" username="gerrit" admin="true" authorize="true" edit="true" issues="true">Gerrit Boers</author>
    <author id="3" username="guest" admin="false" authorize="false" edit="false" issues="false">Guest</author>
    <release effectiveDate="2013-12-26" label="december 2013">
        <comment>Optioneel <i>commentaar</i> bij deze release</comment>
    </release>
</project>:)

let $storedProject  := collection(concat($get:strTerminologyData,'/ica-data/meta'))/project
let $user           := get:strCurrentUserName()

let $preparedProject :=
   <project>
   {
   $editedProject/author,
      for $release in $editedProject/release
   return
   <release>
   {
   $release/@*,
   for $comment in $release/comment
   return
   art:parseNode($comment)
   }
   </release>
   }
   </project>

let $projectUpdate :=
   if ($storedProject/author[@username=$user][@admin='true']) then
       let $update := update value $storedProject with $preparedProject/*
       return
       'OK'
   else('NO PERMISSION')


return
<data-safe>{if ($projectUpdate='OK') then 'true' else 'false'}</data-safe>