xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace art ="http://art-decor.org/ns/art" at "xmldb:exist:///db/apps/art/modules/art-decor.xqm";
(:let $language := request:get-parameter('lang','nl-NL'):)
(:let $language := 'nl-NL':)



let $icaMapping := request:get-data()/cics

let $storedMapping := collection(concat($get:strTerminologyData,'/ica-data/concepts'))//cics

let $preparedUpdate :=
   <cics>
      {
      for $ci in $icaMapping/ci
      order by $ci/text
      return
      <ci>
         {
         $ci/@*,
         for $desc in $ci/description
         return
         art:parseNode($desc)
         ,
         $ci/text,
         $ci/icd-9,
         $ci/icd-10,
         $ci/snomed
         }
      </ci>
      }
   </cics>

return
update value $storedMapping with $preparedUpdate/*