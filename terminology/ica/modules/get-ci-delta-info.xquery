xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";


let $deltaConcepts := collection(concat($get:strTerminologyData,'/snomed-data/Delta/Terminology'))//concept
let $refset := collection(concat($get:strTerminologyData,'/ica-data/concepts'))/cics

let $retiredConcepts :=
   for $concept in $deltaConcepts[@active='0']
   
   let $associatedConcepts := collection(concat($get:strTerminologyData,'/snomed-data/Delta/Refset/Content'))//assocationReference[@referencedComponentId=$concept/@id]
   let $cis :=$refset//snomed[@code=$concept/@id]/parent::ci
   return
   for $ci in $cis[@statusCode!='retired']
   
   return
      <concept id="{$ci/@id}" text="{$ci/text/text()}" conceptId="{$concept/@id}" fsn="">
         {
         for $association in $associatedConcepts
         let $type:=collection(concat($get:strTerminologyData,'/snomed-data/en-GB'))//concept[@conceptId=$association/@refsetId]
         let $target:=collection(concat($get:strTerminologyData,'/snomed-data/en-GB'))//concept[@conceptId=$association/@targetComponentId]
         return
         <associationReference refset="{$type/desc[@type='pref']}" targetComponent="{$target/desc[@type='fsn']}" targetComponentId="{$association/@targetComponentId}">
         </associationReference>
         }
         
      </concept>
return
<retired>
{
for $concept in $retiredConcepts
order by lower-case($concept/@text)
return
$concept
}
</retired>