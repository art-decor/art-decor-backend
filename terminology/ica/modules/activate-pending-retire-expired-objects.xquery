xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";

declare variable $logFileName := concat('transactions-', format-date(current-date(), '[Y0001]'),'.xml');


let $contraIndications := collection(concat($get:strTerminologyData,'/ica-data/concepts'))//cics

let $checkLog :=
   if (not(doc-available(concat($get:strTerminologyData,'/ica-data/log/',$logFileName)))) then
      (
      xmldb:store(concat($get:strTerminologyData,'/ica-data/log/'), $logFileName, <log/>),
      sm:chown(xs:anyURI(concat($get:strTerminologyData,'/ica-data/log/',$logFileName)),'admin'),
      sm:chgrp(xs:anyURI(concat($get:strTerminologyData,'/ica-data/log/',$logFileName)),'terminology'),
      sm:chmod(xs:anyURI(concat($get:strTerminologyData,'/ica-data/log/',$logFileName)),sm:octal-to-mode('0664')),
      sm:clear-acl(xs:anyURI(concat($get:strTerminologyData,'/ica-data/log/',$logFileName)))
      )
   else()
   
let $logFile := doc(concat($get:strTerminologyData,'/ica-data/log/',$logFileName))

let $activations :=
   (
      for $object in $contraIndications/ci[@statusCode='pending' and xs:date(@effectiveDate) le current-date()]
      return
      (update value $object/@statusCode with 'active',<ci>{$object/@*}</ci>)
      ,
      for $object in $contraIndications//cic[@statusCode='pending' and xs:date(@effectiveDate) le current-date()]
      return
      (update value $object/@statusCode with 'active',<cic>{$object/@*}</cic>)
      ,
      for $object in $contraIndications//shb-ci[@statusCode='pending' and xs:date(@effectiveDate) le current-date()]
      return
      (update value $object/@statusCode with 'active',<shb-ci>{$object/@*}</shb-ci>)
      ,
      for $object in $contraIndications//icpc[@statusCode='pending' and xs:date(@effectiveDate) le current-date()]
      return
      (update value $object/@statusCode with 'active',<icpc>{$object/@*}</icpc>)
      ,
      for $object in $contraIndications//icd-9[@statusCode='pending' and xs:date(@effectiveDate) le current-date()]
      return
      (update value $object/@statusCode with 'active',<icd-9>{$object/@*}</icd-9>)
      ,
      for $object in $contraIndications//icd-10[@statusCode='pending' and xs:date(@effectiveDate) le current-date()]
      return
      (update value $object/@statusCode with 'active',<icd-10>{$object/@*}</icd-10>)
      ,
      for $object in $contraIndications//snomed[@statusCode='pending' and xs:date(@effectiveDate) le current-date()]
      return
      (update value $object/@statusCode with 'active',<snomed>{$object/@*}</snomed>)
   )

let $retirements :=
      (
      for $object in $contraIndications/ci[@statusCode='active' and @expirationDate castable as xs:date and xs:date(@expirationDate) le current-date()]
      return
      (update value $object/@statusCode with 'retired',<ci>{$object/@*}</ci>)
	      ,
      for $object in $contraIndications//cic[@statusCode='active' and @expirationDate castable as xs:date and xs:date(@expirationDate) le current-date()]
      return
      (update value $object/@statusCode with 'retired',<cic>{$object/@*}</cic>)
      ,
      for $object in $contraIndications//shb-ci[@statusCode='active' and @expirationDate castable as xs:date and xs:date(@expirationDate) le current-date()]
      return
      (update value $object/@statusCode with 'retired',<shb-ci>{$object/@*}</shb-ci>)
      ,
      for $object in $contraIndications//icpc[@statusCode='active' and @expirationDate castable as xs:date and xs:date(@expirationDate) le current-date()]
      return
      (update value $object/@statusCode with 'retired',<icpc>{$object/@*}</icpc>)
      ,
      for $object in $contraIndications//icd-9[@statusCode='active' and @expirationDate castable as xs:date and xs:date(@expirationDate) le current-date()]
      return
      (update value $object/@statusCode with 'retired',<icd-9>{$object/@*}</icd-9>)
      ,
      for $object in $contraIndications//icd-10[@statusCode='active' and @expirationDate castable as xs:date and xs:date(@expirationDate) le current-date()]
      return
      (update value $object/@statusCode with 'retired',<icd-10>{$object/@*}</icd-10>)
      ,
      for $object in $contraIndications//snomed[@statusCode='active' and @expirationDate castable as xs:date and xs:date(@expirationDate) le current-date()]
      return
      (update value $object/@statusCode with 'retired',<snomed>{$object/@*}</snomed>)
      )
      
let $jobs :=
   <systemJob date="{current-date()}">
   {$activations,$retirements}
   </systemJob>

return

<systemJob date="{current-date()}">
   {
   if (count($jobs/*) gt 0) then
      update insert $jobs into $logFile/log
   else()   
      }
</systemJob>
