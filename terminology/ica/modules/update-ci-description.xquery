xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";

let $editedDesc := request:get-data()/description

let $storedDesc:= collection(concat($get:strTerminologyData,'/ica-data/concepts'))//desc[@interfaceId=$editedDesc/@interfaceId]

(: get user for permission check:)
let $user := get:strCurrentUserName()
let $project :=collection(concat($get:strTerminologyData,'/dhd-data/meta'))/project
let $edit := xs:boolean($project/author[@username=$user]/@edit)

let $preparedDesc :=
         <desc count="{count(tokenize($editedDesc/text(),'\s'))}" length="{string-length($editedDesc/text())}">
         {
         $editedDesc/@*[not(name()=('count','length'))],
         $editedDesc/text()
         }
         </desc>

let $response :=
   (:check if user is authorized:)
   if ($edit) then
       (
       update replace $storedDesc[@statusCode=('draft','update')] with $preparedDesc,
       update value $storedDesc/parent::concept/@editDate with  format-date(current-date(), '[Y0001]-[M01]-[D01]'),
       <response>OK</response>
       )
   else(<member>NO PERMISSION</member>)
   
return
$response