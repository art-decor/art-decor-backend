xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";

let $ciId :=util:unescape-uri(request:get-parameter('ciId',('')),'UTF-8')
let $type :=util:unescape-uri(request:get-parameter('type',('')),'UTF-8')
let $code :=util:unescape-uri(request:get-parameter('code',('')),'UTF-8')
let $desc :=util:unescape-uri(request:get-parameter('desc',('')),'UTF-8')
let $codeSystemName :=util:unescape-uri(request:get-parameter('codeSystemName',('')),'UTF-8')

(: get user for permission check:)
let $user := get:strCurrentUserName()
let $project :=collection(concat($get:strTerminologyData,'/ica-data/meta'))/project
let $edit := xs:boolean($project/author[@username=$user]/@edit)
let $currentDate :=format-date(current-date(), '[Y0001]-[M01]-[D01]')

let $ci := collection(concat($get:strTerminologyData,'/ica-data/concepts'))//ci[@id=$ciId]
let $response :=
   (:check if user is authorized:)
   if ($edit) then
      let $newMapping :=
         if ($type='icd9') then
            <icd-9 id="{util:uuid()}" code="{$code}" effectiveDate="" expirationDate="" editDate="{$currentDate}" statusCode="draft"><desc>{$desc}</desc></icd-9>
         else if ($type='icd10') then
            <icd-10 id="{util:uuid()}" code="{$code}" effectiveDate="" expirationDate="" editDate="{$currentDate}" statusCode="draft"><desc>{$desc}</desc></icd-10>
         else if ($type='snomed') then
            <snomed id="{util:uuid()}" code="{$code}" effectiveDate="" expirationDate="" editDate="{$currentDate}" statusCode="draft"><desc>{$desc}</desc></snomed>
         else if ($type='shb-ci') then
            <shb-ci id="{util:uuid()}" code="{$code}" effectiveDate="" expirationDate="" editDate="{$currentDate}" statusCode="draft"><desc>{$desc}</desc></shb-ci>
         else()
       return
       (
       update insert $newMapping into $ci,
       update value $ci/@editDate with  format-date(current-date(), '[Y0001]-[M01]-[D01]'),
       $newMapping
       )
   else(<concept>NO PERMISSION</concept>)
return
$response