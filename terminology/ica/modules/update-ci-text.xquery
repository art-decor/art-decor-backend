xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";

let $editedText := request:get-data()/text

let $storedCi:= collection(concat($get:strTerminologyData,'/ica-data/concepts'))//ci[@id=$editedText/@ciId]

(: get user for permission check:)
let $user := get:strCurrentUserName()
let $project :=collection(concat($get:strTerminologyData,'/ica-data/meta'))/project
let $edit := xs:boolean($project/author[@username=$user]/@edit)

let $preparedText := <text language="{$editedText/@language}">{$editedText/text()}</text>

let $response :=
   (:check if user is authorized:)
   if ($edit) then
       (
       if ($storedCi/text/text() != $preparedText/text() or empty($storedCi/text/text())) then
          (
          update replace $storedCi[@statusCode=('draft','update')]/text with $preparedText,
          update value $storedCi/@editDate with  format-date(current-date(), '[Y0001]-[M01]-[D01]')
          )
       else()
       ,
       <response>OK</response>
       )
   else(<member>NO PERMISSION</member>)
return
$response