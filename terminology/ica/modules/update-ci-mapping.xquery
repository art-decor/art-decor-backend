xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";

let $editedMapping := request:get-data()/*

let $storedMapping:= collection(concat($get:strTerminologyData,'/ica-data/concepts'))//ci/*[@id=$editedMapping/@id]

(: get user for permission check:)
let $user := get:strCurrentUserName()
let $project :=collection(concat($get:strTerminologyData,'/ica-data/meta'))/project
let $edit := xs:boolean($project/author[@username=$user]/@edit)
let $okForEdit := $storedMapping[@statusCode=('draft','update') or name()='cic']
let $response :=
   (:check if user is authorized:)
   if ($edit and $okForEdit) then
       (
       if ($storedMapping/@code != $editedMapping/@code or $storedMapping/text() != $editedMapping/text() or empty($storedMapping/text())) then
          (
          update replace $storedMapping with $editedMapping,
          update value $storedMapping/@editDate with  format-date(current-date(), '[Y0001]-[M01]-[D01]'),
          update value $storedMapping/parent::ci/@editDate with  format-date(current-date(), '[Y0001]-[M01]-[D01]')
          )
       else(),
       <response>OK</response>
       )
   else(<member>NO PERMISSION</member>)
return
$response