xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
declare variable $root := repo:get-root();

 declare %private function local:setCIlistQueryPermissions() {
   for $query in xmldb:get-child-resources(xs:anyURI(concat('xmldb:exist:///',$root,'terminology/ica/modules')))
   return
   (
   sm:chown(xs:anyURI(concat('xmldb:exist:///',$root,'terminology/ica/modules/',$query)),'admin'),
   sm:chgrp(xs:anyURI(concat('xmldb:exist:///',$root,'terminology/ica/modules/',$query)),'terminology'),
   if (starts-with($query,('check','get','retrieve','search'))) then
      sm:chmod(xs:anyURI(concat('xmldb:exist:///',$root,'terminology/ica/modules/',$query)),sm:octal-to-mode('0755'))
   else(sm:chmod(xs:anyURI(concat('xmldb:exist:///',$root,'terminology/ica/modules/',$query)),sm:octal-to-mode('0754')))
   ,
   sm:clear-acl(xs:anyURI(concat('xmldb:exist:///',$root,'terminology/ica/modules/',$query)))
   )
};

let $missingCIC  := collection(concat($get:strTerminologyData,'/ica-data/concepts'))//ci[not(cic)]
let $currentDate :=format-date(current-date(), '[Y0001]-[M01]-[D01]')
return
for $ci in $missingCIC
let $cic :=  
   <cic id="{util:uuid()}" code="" statusCode="draft" effectiveDate="" expirationDate="" editDate="{$currentDate}">
      <desc></desc>
   </cic>
return
update insert $cic into $ci
