xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:declare option exist:serialize "method=xml media-type=text/xml";:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art ="http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";
(:let $language := request:get-parameter('lang','nl-NL'):)
(:let $language := 'nl-NL':)
let $icaMapping := collection(concat($get:strTerminologyData,'/ica-data/concepts'))//cics

return
<cics>
   <project>
   {
   $icaMapping/project/@*,
   $icaMapping/project/name,
   for $desc in $icaMapping/project/description
      return
      art:serializeNode($desc)
      ,
   $icaMapping/project/author
   }
   
   </project>
   {
   
   for $ci in $icaMapping/ci
   order by $ci/text
   return
   <ci>
      {
      $ci/@*,
      for $desc in $ci/description
      return
      art:serializeNode($desc)
      ,
      for $rationale in $ci/rationale
      return
      art:serializeNode($rationale)
      ,
      $ci/text,
      $ci/cic,
      $ci/icd-9,
      $ci/icd-10,
      $ci/snomed,
      $ci/other
      }
   </ci>
   }
</cics>