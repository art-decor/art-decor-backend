xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art                 = "http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";
declare namespace xs="http://www.w3.org/2001/XMLSchema";
declare namespace xforms="http://www.w3.org/2002/xforms";

let $types              := if (request:exists()) then (tokenize(request:get-parameter('type',())[string-length()>0],'\s')) else ()
let $priorities         := if (request:exists()) then (tokenize(request:get-parameter('priority',())[string-length()>0],'\s')) else ()
let $statuscodes        := if (request:exists()) then (tokenize(request:get-parameter('statusCode',())[string-length()>0],'\s')) else ()
let $lastassignedids    := if (request:exists()) then (tokenize(request:get-parameter('assignedTo',())[string-length()>0],'\s')) else ()
let $labels             := if (request:exists()) then (tokenize(request:get-parameter('labels',())[string-length()>0],'\s')) else ()
let $sort               := if (request:exists()) then (request:get-parameter('sort',())[string-length()>0]) else ()

let $language := 'nl-NL'
let $ica := collection(concat($get:strTerminologyData,'/ica-data/concepts'))//cics
let $enumerations := $get:docDecorSchema/xs:schema/xs:simpleType[*/xs:enumeration]
let $issues := collection(concat($get:strTerminologyData,'/ica-data/meta'))//issue

let $issueList              :=
   <issues>
   {
   for $issue in $issues
       let $lastTracking := max($issue//tracking/xs:dateTime(@effectiveDate))
       let $lastAssignment := max($issue//assignment/xs:dateTime(@effectiveDate))
       let $type := $issue/@type
       let $priority := if (string-length($issue/@priority)>0) then ($issue/@priority/string()) else ('N')
       return
           <issue id="{$issue/@id}" priority="{$priority}" displayName="{$issue/@displayName}" 
               typeName="{$enumerations[@name='IssueType']//xs:enumeration[@value=$type]/xs:annotation/xs:appinfo/xforms:label[@xml:lang=$language]}"
               type="{$issue/@type}" lastDate="{$lastTracking}" currentStatusCode="{$issue/tracking[@effectiveDate=$lastTracking][1]/@statusCode}"
               lastAuthor="{$issue/tracking[@effectiveDate=$lastTracking]/author/text()}"
               lastAssignment="{$issue/assignment[@effectiveDate=$lastAssignment]/@name}">
               {
               for $object in $issue/object
               let $type := $object/@type
               order by $type
               return
               <object id="{$object/@id}"   type="{$type}">
   
               </object>    
                   
               ,
               for $event in $issue/tracking|$issue/assignment
               order by xs:dateTime($event/@effectiveDate) descending
               return
               if (name($event)='tracking') then
               <tracking effectiveDate="{$event/@effectiveDate}" statusCode="{$event/@statusCode}">
               {$event/author}
               {
               for $desc in $event/desc
               return
               art:serializeNode($desc)
               }
               </tracking>
               else if (name($event)='assignment') then
               <assignment to="{$event/@to}" name="{$event/@name}" effectiveDate="{$event/@effectiveDate}">
               {$event/author}
               {
               for $desc in $event/desc
               return
               art:serializeNode($desc)
               }
               </assignment>
               else()
               }
           </issue>
   }
   </issues>
   
let $results    :=
    switch ($sort) 
    case 'issue'        return for $issue in $issueList/issue order by $issue/@displayName return $issue
    case 'priority'     return for $issue in $issueList/issue order by $issue/@priority return $issue
    case 'type'         return for $issue in $issueList/issue order by $issue/@type return $issue
    case 'status'       return for $issue in $issueList/issue order by $issue/@currentStatusCode return $issue
    case 'date'         return for $issue in $issueList/issue order by $issue/@lastDate return $issue
    case 'assigned-to'  return for $issue in $issueList/issue order by $issue/@lastAssignment return $issue
    case 'label'        return for $issue in $issueList/issue order by $issue/@currentLabels return $issue
    default             return $issues
return
<issues>
{$results}
</issues>