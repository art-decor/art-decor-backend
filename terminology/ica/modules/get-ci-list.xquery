xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:declare option exist:serialize "method=xml media-type=text/xml";:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art ="http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";

(:let $version := '':)
let $language := 'nl-NL'
let $list := collection(concat($get:strTerminologyData,'/ica-data/concepts'))//cics

return
<list>
   {
   $list/@*,
   for $ci in $list/ci
   order by lower-case($ci/text)
   return
   <ci>
      {
      $ci/@*,
      $ci/text
      }
   </ci>
   }
</list>