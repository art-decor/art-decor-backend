xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";

let $ciId :=util:unescape-uri(request:get-parameter('id',('')),'UTF-8')

(: get user for permission check:)
let $user := get:strCurrentUserName()
let $project :=collection(concat($get:strTerminologyData,'/ica-data/meta'))/project
let $edit := xs:boolean($project/author[@username=$user]/@edit)

let $ci := collection(concat($get:strTerminologyData,'/ica-data/concepts'))//ci[@id=$ciId]
let $response :=
   (:check if user is authorized:)
   if ($edit and $ci[@statusCode='draft']) then
       (
       update delete $ci,
       <ci>OK</ci>
       )
   else(<ci>NO PERMISSION</ci>)
return
$response