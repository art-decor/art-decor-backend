xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:declare option exist:serialize "method=xml media-type=text/xml";:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art ="http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";

let $release            := util:unescape-uri(request:get-parameter('release',('')),'UTF-8')
(:let $release := '20140331_135546':)
let $releases           := collection(concat($get:strTerminologyData,'/ica-data/meta'))//release

let $dtmpicture         := '[Y0001][M01][D01]_[H01][m01][s01]'
let $xml                :=
    if (string-length($release) gt 0 and doc-available(concat($get:strTerminologyData,'/ica-data/releases/',$release,'.xml'))) then
        doc(concat($get:strTerminologyData,'/ica-data/releases/',$release,'.xml'))
    else if (string-length($release) = 0 and $releases) then
        let $latestRelease := max($releases/xs:dateTime(@effectiveTime))
        return
        doc(concat($get:strTerminologyData,'/ica-data/releases/', format-dateTime($latestRelease, $dtmpicture),'.xml'))
    else(<cics/>)

return
<cics>
{
    for $ci in $xml/cics/ci
    order by lower-case($ci/text)
    return
    $ci
}
</cics>