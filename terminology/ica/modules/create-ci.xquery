xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:declare option exist:serialize "method=xml media-type=text/xml";:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art ="http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";


let $language := 'nl-NL'

(: get user for permission check:)
let $user := get:strCurrentUserName()
let $project :=collection(concat($get:strTerminologyData,'/ica-data/meta'))/project
let $edit := xs:boolean($project/author[@username=$user]/@edit)
let $currentDate :=format-date(current-date(), '[Y0001]-[M01]-[D01]')

let $newId := max(collection(concat($get:strTerminologyData,'/ica-data/concepts'))//ci/xs:integer(@id)) + 1
let $newCi :=
   <ci id="{$newId}" statusCode="draft" effectiveDate="" expirationDate="" editDate="{$currentDate}">
      <text language="{$language}"></text>
      <description language="{$language}"></description>
      <rationale language="{$language}"></rationale>
      <cic id="{util:uuid()}" code="" statusCode="draft" effectiveDate="" expirationDate="" editDate="{$currentDate}">
         <desc></desc>
      </cic>
   </ci>
let $insert := 
   if ($edit) then
      update insert $newCi into collection(concat($get:strTerminologyData,'/ica-data/concepts'))/cics
   else()
return
<id>{$newId}</id>
