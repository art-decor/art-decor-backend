xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
declare namespace xhtml     = "http://www.w3.org/1999/xhtml";
(: parse serialized content to html for storage :)
declare %private function local:parseNode($textWithMarkup as element()?) as element() {
let $parsed-html := 
    if (exists($textWithMarkup)) then ( 
        element {name($textWithMarkup)} {
            $textWithMarkup/@*[not(name()='ciId')],
            util:parse-html($textWithMarkup)//xhtml:body/text()|util:parse-html($textWithMarkup)//BODY/node()
        }
    )
    else ()
return $parsed-html
};

let $editedText := request:get-data()/*

let $storedCi:= collection(concat($get:strTerminologyData,'/ica-data/concepts'))//ci[@id=$editedText/@ciId][@statusCode=('draft','update')]

(: get user for permission check:)
let $user := get:strCurrentUserName()
let $project :=collection(concat($get:strTerminologyData,'/ica-data/meta'))/project
let $edit := xs:boolean($project/author[@username=$user]/@edit)

let $preparedText := local:parseNode($editedText)

let $response :=
   (:check if user is authorized:)
   if ($edit) then
       (
       if (name($editedText)='description') then
         if ($storedCi/description//text() != $preparedText//text() or empty($storedCi/description/text())) then
          (
          update replace $storedCi/description with $preparedText,
          update value $storedCi/@editDate with  format-date(current-date(), '[Y0001]-[M01]-[D01]')
          )
         else()
       else if (name($editedText)='rationale') then
         if ($storedCi/rationale//text()!=$preparedText//text() or empty($storedCi/rationale/text())) then
          (
          update replace $storedCi/rationale with $preparedText,
          update value $storedCi/@editDate with  format-date(current-date(), '[Y0001]-[M01]-[D01]')
          )
         else()
       else()
       ,
       <response>OK</response>
       )
   else(<member>NO PERMISSION</member>)
return
$response