xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:declare option exist:serialize "method=xml media-type=text/xml";:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art ="http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";
(:let $language := request:get-parameter('lang','nl-NL'):)
(:let $language := 'nl-NL':)
let $project := collection(concat($get:strTerminologyData,'/ica-data/meta'))/project
let $ciList := collection(concat($get:strTerminologyData,'/ica-data/concepts'))/cics
return
<project>
   <concepts count="{count($ciList/ci)}" draft="{count($ciList/ci[@statusCode='draft'])}" update="{count($ciList/ci[@statusCode='update'])}" review="{count($ciList/ci[@statusCode='review'])}"/>
   {
   $project/author,
   for $release in $project/release
   order by xs:dateTime($release/@effectiveTime) descending
   return
   <release>
   {
   $release/@*,
   for $comment in $release/comment
   return
   art:serializeNode($comment)
   }
   </release>
   }
</project>
