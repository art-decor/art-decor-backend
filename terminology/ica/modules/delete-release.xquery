xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art ="http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";

let $release := request:get-data()/release


let $storedProject      := collection(concat($get:strTerminologyData,'/ica-data/meta'))/project
let $user               := get:strCurrentUserName()
(:check if user is admin:)
let $admin              := $storedProject/author[@username=$user][@admin='true']

let $dtmpicture         := '[Y0001][M01][D01]_[H01][m01][s01]'
let $collectionRemove   :=
   (
   if (xmldb:collection-available(concat($get:strTerminologyData,'/ica-data/releases/', format-dateTime($release/@effectiveTime, $dtmpicture))) and $admin) then
        xmldb:remove(concat($get:strTerminologyData,'/ica-data/releases/', format-dateTime($release/@effectiveTime, $dtmpicture)))
   else(),
   if (doc-available(concat($get:strTerminologyData,'/ica-data/releases/', format-dateTime($release/@effectiveTime, $dtmpicture),'.xml')) and $admin) then
        xmldb:remove(concat($get:strTerminologyData,'/ica-data/releases/'),concat(format-dateTime($release/@effectiveTime, $dtmpicture),'.xml'))
   else()
   )
let $projectUpdate :=
   if ($admin) then
       let $update := update delete $storedProject/release[@effectiveTime=$release/@effectiveTime]
       return
       'OK'
   else('NO PERMISSION')


return
<data-safe>{if ($projectUpdate='OK') then 'true' else 'false'}</data-safe>