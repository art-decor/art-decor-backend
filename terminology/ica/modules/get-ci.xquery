xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:declare option exist:serialize "method=xml media-type=text/xml";:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace art ="http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";

let $id := request:get-parameter('id','')
let $language := 'nl-NL'
let $ci := collection(concat($get:strTerminologyData,'/ica-data/concepts'))//ci[@id=$id]
let $response :=
   if ($ci) then
      let $history := count(collection(concat($get:strTerminologyData,'/ica-data/history'))//ci[@id=$id])
      return
      <ci history="{$history}">
      {
      $ci/@*,
      for $desc in $ci/description
      return
      art:serializeNode($desc)
      ,
      for $rationale in $ci/rationale
      return
      art:serializeNode($rationale)
      ,
      $ci/text,
      $ci/cic,
      for $icd9 in $ci/icd-9
      order by $icd9/@code
      return
      $icd9
      ,
      for $icd10 in $ci/icd-10
      order by $icd10/@code
      return
      $icd10
      ,
      for $snomed in $ci/snomed
      order by $snomed/@code
      return
      $snomed,
      for $shb-ci in $ci/shb-ci
      order by $shb-ci/@code
      return
      $shb-ci
      }
   </ci>
   else (<ci/>)

return
$response