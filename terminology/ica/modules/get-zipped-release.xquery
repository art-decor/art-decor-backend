xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
declare namespace compression="http://exist-db.org/xquery/compression";
declare option exist:serialize "method=text media-type=application/zip,application/octet-stream charset=utf-8";


let $collection := request:get-parameter('release','')



return
   if (string-length($collection) gt 0 and xmldb:collection-available(concat($get:strTerminologyData,'/ica-data/releases/',$collection))) then
   (
   response:set-header("Content-Disposition", concat('attachment; filename=',$collection,'.zip'))
   ,
   response:stream-binary(
       compression:zip( xs:anyURI(concat($get:strTerminologyData,'/ica-data/releases/',$collection,'/')), false() ),
       'application/zip',
       concat($collection,'.zip')
       )
   )
   else()
 