xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
module namespace icapfix     = "http://art-decor.org/ns/ica-permissions";
declare namespace sm            = "http://exist-db.org/xquery/securitymanager";

(:  Mode            Octal
 :  rw-r--r--   ==  0644
 :  rw-rw-r--   ==  0664
 :  rwxr-xr--   ==  0754
 :  rwxr-xr-x   ==  0755
 :  rwxrwxr-x   ==  0775
 :)

(:install path for art (normally /db/apps/), includes trailing slash :)
declare variable $icapfix:root                   := repo:get-root();
declare variable $icapfix:strIcaModules       := concat($icapfix:root,'terminology/ica/modules');
declare variable $icapfix:strIcaData := concat($icapfix:root,'terminology-data/ica-data');
declare variable $icapfix:strIcaLog := concat($icapfix:root,'terminology-data/ica-data/log');

declare function icapfix:setICAQueryPermissions() {
    icapfix:checkIfUserDba(),
    
    for $query in xmldb:get-child-resources(xs:anyURI(concat('xmldb:exist:///',$icapfix:strIcaModules)))
    return (
        sm:chown(xs:anyURI(concat('xmldb:exist:///',$icapfix:strIcaModules,'/',$query)),'admin'),
        sm:chgrp(xs:anyURI(concat('xmldb:exist:///',$icapfix:strIcaModules,'/',$query)),'terminology'),
        if (starts-with($query,('check','get','is-','retrieve','search','view','activate-pending'))) then
            sm:chmod(xs:anyURI(concat('xmldb:exist:///',$icapfix:strIcaModules,'/',$query)),sm:octal-to-mode('0755'))
        else (
            sm:chmod(xs:anyURI(concat('xmldb:exist:///',$icapfix:strIcaModules,'/',$query)),sm:octal-to-mode('0754'))
        )
        ,
        sm:clear-acl(xs:anyURI(concat('xmldb:exist:///',$icapfix:strIcaModules,'/',$query)))
    )
};

declare function icapfix:setICACollectionPermissions() {
    icapfix:checkIfUserDba(),

    icapfix:setPermissions($icapfix:strIcaData,'admin:terminology',sm:octal-to-mode('0775'),'admin:terminology',sm:octal-to-mode('0775')),
    icapfix:setPermissions($icapfix:strIcaLog,'admin:terminology',sm:octal-to-mode('0775'),'admin:terminology',sm:octal-to-mode('0666'))
    
  
};

(:
:   Helper function with recursion for icapfix:setSCTExtensionCollectionPermissions()
:)
declare %private function icapfix:setPermissions($path as xs:string, $collown as xs:string, $collmode as xs:string, $resown as xs:string, $resmode as xs:string) {
    sm:chown(xs:anyURI($path),$collown),
    sm:chmod(xs:anyURI($path),$collmode),
    sm:clear-acl(xs:anyURI($path)),
    for $res in xmldb:get-child-resources(xs:anyURI($path))
    return (
        sm:chown(xs:anyURI(concat($path,'/',$res)),$resown),
        sm:chmod(xs:anyURI(concat($path,'/',$res)),$resmode),
        sm:clear-acl(xs:anyURI(concat($path,'/',$res)))
    )
    ,
    for $collection in xmldb:get-child-collections($path)
    return
        icapfix:setPermissions(concat($path,'/',$collection), $collown, $collmode, $resown, $resmode)
};

declare %private function icapfix:checkIfUserDba() {
    if (sm:is-dba((sm:id()//sm:real/sm:username/text())[1])) then () else (
        error(QName('http://art-decor.org/ns/art-decor-permissions', 'NotAllowed'), concat('Only dba user can use this module. ',(sm:id()//sm:real/sm:username/text())[1]))
    )
};