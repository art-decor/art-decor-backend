<!--
  Copyright (C) 2010 Orbeon, Inc.
  
   Adaptations by Gerrit Boers

  This program is free software; you can redistribute it and/or modify it under the terms of the
  GNU Lesser General Public License as published by the Free Software Foundation; either version
  2.1 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU Lesser General Public License for more details.

  The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
  -->
<xsl:stylesheet version="2.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:xs="http://www.w3.org/2001/XMLSchema" 
                xmlns:f="http://orbeon.org/oxf/xml/formatting"
                xmlns:xh="http://www.w3.org/1999/xhtml" 
                xmlns:xf="http://www.w3.org/2002/xforms" 
                xmlns:xxf="http://orbeon.org/oxf/xml/xforms" 
                xmlns:version="java:org.orbeon.oxf.common.Version"
                xmlns:xi="http://www.w3.org/2001/XInclude"
                xmlns:xxi="http://orbeon.org/oxf/xml/xinclude"
                xmlns:p="http://www.orbeon.com/oxf/pipeline">

    <!-- XML formatting -->
    <xsl:import href="oxf:/ops/utils/formatting/formatting.xsl"/>

    <!-- Try to obtain a meaningful title for the example -->
    <xsl:variable name="title" as="xs:string">
        <xsl:choose>
            <xsl:when test="/xh:html/xh:head/xh:title[not(. = '')]">
                <xsl:value-of select="/xh:html/xh:head/xh:title"/>
            </xsl:when>
            <xsl:when test="/xh:html/xh:body/xh:h1">
                <xsl:value-of select="(/xh:html/xh:body/xh:h1)[1]"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>[Untitled]</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
   
    <!-- Orbeon Forms version -->
    <xsl:variable name="version" as="xs:string?" select="version:versionStringIfAllowedOrEmpty()"/>

    <!-- - - - - - - Minimalistic page template - - - - - - -->
    <xsl:template match="xh:html">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:apply-templates select="xh:head | xh:body"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="xh:head">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xh:title>
                <xsl:value-of select="$title"/>
            </xh:title>
            <!-- Orbeon Forms version -->
            <xsl:if test="$version">
                <xh:meta name="generator" content="{$version}"/>
            </xsl:if>
            <!-- Note the order of CSS. We load form CSS last so we know that takes prevalence! -->
            <xh:link rel="stylesheet" href="/apps/fr/style/bootstrap/css/bootstrap.css" type="text/css"/>
            <xh:link rel="stylesheet" href="/apps/fr/style/form-runner-bootstrap-override.css" type="text/css"/>
            <xh:link rel="stylesheet" href="/config/theme/orbeon.css" type="text/css" media="all"/>
            <xh:link rel="shortcut icon" href="/img/favicon.ico"/>
            
            <xsl:comment> Form Content from html/head </xsl:comment>
            <xsl:apply-templates select="node() except (self::xh:meta[not(@name = 'generator')] | self::xh:link[not(@rel = 'shortcut icon')])"/>
            
            <!-- Handle post-body scripts if present. They can be placed here by oxf:resources-aggregator -->
            <xsl:if test="/xh:html/xh:script">
                <xsl:comment> Form Scripts from html </xsl:comment>
                <xsl:apply-templates select="/xh:html/xh:script"/>
            </xsl:if>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="xh:body">
        <xsl:copy>
            <xsl:apply-templates select="@* except @class"/>
            <xsl:attribute name="class" select="string-join(('orbeon', @class), ' ')"/>
            <!--<xh:div class="container">
                <xh:div class="navbar navbar-inverse">
                    <xh:div class="navbar-inner">
                        <xh:div class="container">
                            <xh:a href="http://www.orbeon.com/">
                                <xh:img src="/apps/fr/style/orbeon-navbar-logo.png" alt="Orbeon Forms"/>
                            </xh:a>
                            <xh:h1><xsl:value-of select="$title"/></xh:h1>
                        </xh:div>
                    </xh:div>
                </xh:div>
                <xsl:apply-templates select="node()"/>
            </xh:div>-->
            <xh:table id="main">
                <xh:tr>
                    <xh:td>
                        <!--<xsl:apply-templates select="node()"/>-->
                        <xsl:copy-of select="node()"/>
                    </xh:td>
                </xh:tr>
            </xh:table>
            <xsl:if test="p:property('oxf.epilogue.show-feedback')">
                <xsl:copy-of select="doc('oxf:/config/feedback.xhtml')"/>
            </xsl:if>
        </xsl:copy>
    </xsl:template>

    <!-- Simply copy everything that's not matched -->
    <xsl:template match="@*|node()" priority="-2">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>
