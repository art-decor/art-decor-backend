This zip file packages CDA Schema with SDTC extensions approved by HL7 SDWG.


This package supplements the published CDA R2 bases standard with an updated Schema definitions.

This package was prepared by HL7 SDWG.
 

========================
Contents of the update package:

readme.txt:	
  This file
  
CDA_SDTC.xsd:
   XML schema for message type POCD_MT000040.**No Change in this release.**

POCD_MT000040_SDTC.xsd:
   Schema defining the elements and attributes including SDTC extensions for message type POCD_MT000040

** UPDATE **
2016-05-12 Added extension approved by SDWG on 2016-01-28 Component4/priorityNumber

SDTC.xsd:
   Schema defining all extensions in the SDTC namespace. It contains elements, attributes and datatypes

** UPDATE **
2016-05-12 Added extension approved by SDWG on 2016-01-28 priorityNumber

datatypes.xsd:
   Schema defining the CDA data types.**No Change in this release.**

datatypes-base_SDTC.xsd:
   Schema defining base data types with SDTC extensions.**No Change in this release.**

infrastructureRoot.xsd:
   Schema defining the infrastructureRoot. **No Change in this release.**  

NarrativeBlock.xsd:
   Schema defining constructs allowed in the CDA narrative block. **No Change in this release.**

voc.xsd:
   Schema defining allowed vocabulary values. **No Change in this release.**

June 1, 2016