#!/usr/bin/tclsh

global argc argv
global ctr
set ctr 0

proc ds {op dir} {
    global ctr
    
    set doXSL [string match "examples*" $dir ]
    
    set skipdirs [list css coreschemas schemas xsl]
    foreach l [lsort -dictionary [glob -nocomplain $dir/*]] {
        if { [regexp -- {^\./} $l] } {
            set l [string range $l 2 end]
        }
        if { [file isdirectory $l] } {
            if { [lsearch $skipdirs $l] >= 0 } { continue }
            
            lappend op "            <li><b>$l</b>"
            lappend op "            <ul>"
            set op [ds $op $l]
            lappend op "            </ul></li>"
        } elseif { [file extension $l] == ".xml" } {
            regsub -all " " $l "%20" ll
            set depth [regsub -all {/} $ll "" ttt]
            
            set lang [rpl $l $depth $doXSL]
            incr ctr
            
            if { [regexp "CDAr3" $l] } { set l "<b>[lindex [split $l "/"] end]</b>" } else {set l [lindex [split $l "/"] end]}
            if { [string length $lang] > 0 } { set lang "- language is $lang" }
            
            lappend op "            <li><a href=\"$ll\">$l</a> $lang</li>"
        }
    }
    return $op
}

proc rpl { f depth doXSL } {
    #set kdList1 [list _CDA_Kerndossier_Patient1_JorisHalkema.xml _CDA_Kerndossier_Patient2_MartinBrouwer.xml]
    set kdList1 {}
    
    set fi [open $f r]
    fconfigure $fi -translation {binary binary}
    set c  [read $fi]
    close $fi
    
    set full "" ; set lang "" ; set pfx ""
    for {set i 0} {$i < $depth} {incr i} { append pfx "../" }
    
    if {$doXSL == 1} {
        regsub -all {<\?xml-stylesheet[^\?]+\?>\r?\n} $c "" c
        if { [lsearch $kdList1 [file tail $f]] >= 0 } {
            regsub -- {(<([^:]+:)?ClinicalDocument)} $c "<?xml-stylesheet type=\"text/xsl\" href=\"${pfx}xsl/cda-kerndossier.xsl\"?>\n\\1" c
        } else {
            regsub -- {(<([^:]+:)?ClinicalDocument)} $c "<?xml-stylesheet type=\"text/xsl\" href=\"${pfx}xsl/CDA.xsl\"?>\n\\1" c
        }
    }
    
    regexp -- {<languageCode[^>]+code="([^"]+)"} $c full lang
    
    #puts stdout "$full $lang"
    
    set fo [open $f w+]
    fconfigure $fo -translation {binary binary}
    puts -nonewline $fo $c
    close $fo
    
    return $lang
}

set xslzip [lindex $argv 0]

if {[string length $xslzip] == 0} {
    puts stderr "Required value for xslzip missing"
    exit 1
}

set op {}; set opF1 {}; set opF2 {}
set op [ds $op examples]

set oop {}
if {[file isdirectory other-examples]} { 
    set oop [ds $oop other-examples]
}

set opH {}
lappend opH "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
lappend opH "<!DOCTYPE html SYSTEM \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"
lappend opH "<html xmlns=\"http://www.w3.org/1999/xhtml\">"
lappend opH "    <head>"
lappend opH "        <title>CDA XSL Test</title>"
lappend opH "        <link type=\"text/css\" rel=\"stylesheet\" href=\"css/nictiz.css\"/>"
lappend opH "    </head>"
lappend opH "    <body style=\"font-family: Verdana, Arial, sans-serif;font-size:10pt;\">"
lappend opH "        <a href=\"http://www.nictiz.nl\"><img src=\"css/nictiz.png\" alt=\"\" style=\"float: right;\" border=\"0\"/></a>"
lappend opH "        <h1>CDA XSL Test</h1>"
lappend opH "        <p><b>Last updated</b>: [clock format [clock seconds] -format %c]</p>"
lappend opH "        <p><b>Purpose</b>: Nictiz maintains a localizable, general purpose version of the official <a href=\"http://gforge.hl7.org/gf/project/strucdoc/frs/\">CDA Stylesheet</a>. It's built to work with multiple languages and maintained for feature parity with the official master.</p>"
lappend opH "        <p>Click any of these $ctr files to view. A file that has its languageCode set will render in that language if available, or revert to the default language en-US</p>"
lappend opH "        <p>"
lappend opH "        <b>Download</b> the set: <a href=\"CDA.zip\">CDA.zip</a> or just the <a href=\"${xslzip}\">Stylesheet</a>. View the <a href=\"xsl/cda_l10n.xml\">list of translations</a> or the <a href=\"xsl/documentation/CDA.html\">stylesheet documentation</a>. Note: this file is big so loading takes a while.<br/>"
lappend opH "        <b>Info</b>: henket(a)nictiz.nl.<br/>"
lappend opH "        <b>Credits</b>: For the better part, this list of examples was compiled by <b>Ren&eacute; Spronk of <a href=\"http://www.ringholm.com\">Ringholm</a></b>. The XSL is derived of the original CDAr2 XSL that has been updated by many people and has lastly graciously been updated and hosted by <b>Lantana Group</b>. See <a href=\"xsl/documentation/CDA.html\">here</a> for full documentation and credits for the XSL."
lappend opH "        </p>"
lappend opH "        <hr/>"

if {[file isdirectory other-examples]} { 
    lappend opH "        <table width=\"100%\">"
    lappend opH "           <tr>"
    lappend opH "               <td width=\"50%\">"
}
lappend opH "        <ul>"

lappend opF1 "        </ul>"

if {[file isdirectory other-examples]} { 
    lappend opF1 "        </td>"

    lappend opF1 "        <td style=\"vertical-align: top;\">"
    lappend opF1 "        <ul>"

    set opF2 {}

    lappend opF2 "        </ul>"
    lappend opF2 "              </td>"
    lappend opF2 "          </tr>"
    lappend opF2 "        </table>"
}
lappend opF2 "    </body>"
lappend opF2 "</html>"

set fo    [open index.html w+]
puts $fo  [join $opH \n]
puts $fo  [join $op \n]
puts $fo  [join $opF1 \n]
puts $fo  [join $oop \n]
puts $fo  [join $opF2 \n]
close $fo

puts "Processed $ctr files"
