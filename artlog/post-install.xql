xquery version "1.0";
(:
	Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
	see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace xmldb   = "http://exist-db.org/xquery/xmldb";
import module namespace sm      = "http://exist-db.org/xquery/securitymanager";
import module namespace repo    = "http://exist-db.org/xquery/repo";

declare variable $root := repo:get-root();

let $strArtlog         := if (xmldb:collection-available(concat($root, 'artlog'))) then concat($root, 'artlog') else xmldb:create-collection($root,'artlog')
let $strArtlogData     := if (xmldb:collection-available(concat($root, 'artlog-data'))) then concat($root, 'artlog-data') else xmldb:create-collection($root,'artlog-data')

let $collperm := sm:chmod(xs:anyURI($strArtlogData),'rwxrwxr-x')
let $collperm := sm:chmod(xs:anyURI(concat($strArtlog, '/modules')),'rwxr-xr-x')
let $collperm := sm:chmod(xs:anyURI(concat($strArtlog, '/modules/artlog.xqm')),'rwxr-xr-x')
let $collperm := sm:chmod(xs:anyURI(concat($strArtlog, '/settings.xml')),'rwxrwxr-x')

return <ok/>