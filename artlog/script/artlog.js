/*
*    Author: Marc de Graauw
*
*    This program is free software; you can redistribute it and/or modify it under the terms 
*    of the GNU General Public License as published by the Free Software Foundation; 
*    either version 3 of the License, or (at your option) any later version.
*    
*    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
*    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
*    See the GNU General Public License for more details.
*    
*    See http://www.gnu.org/licenses/gpl.html
*/
function sendXmlData(method, data, strUrl) {
    var xmlhttp=new XMLHttpRequest();
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4) {
            document.getElementById('url').value = strUrl;
            document.getElementById('returnedStatus').value = xmlhttp.status;
            document.getElementById('returnedData').value = xmlhttp.responseText;
        }
    };
    data = document.getElementById('showXml').value;
    xmlhttp.open(method, strUrl, true);
    xmlhttp.setRequestHeader("Content-type","application/xml");
    if (document.getElementById('dataType').value == 'POSTdata') {
        parser = new DOMParser();
        xmlDoc = parser.parseFromString(document.getElementById('showXml').value,"text/xml");
        xmlhttp.send(xmlDoc);
    }
    else {
        xmlhttp.send();
    }; 
    
}

function showXmlData(strUrl, dataType) {
    var xmlhttp=new XMLHttpRequest();
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4) {
            document.getElementById('showXml').value = xmlhttp.responseText;
            document.getElementById('dataType').value = dataType;
        }
    };
    xmlhttp.open("GET", strUrl, true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send();
}