xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace artlog ="http://art-decor.org/ns/artlog" at "artlog.xqm";

let $logOn :=
    if ($artlog:logOn) 
    then 
        let $collperm := sm:chmod(xs:anyURI($artlog:strArtLog),'rwxrwxr-x')
        return update delete doc('../settings.xml')/logging/@on
    else 
        let $collperm := sm:chmod(xs:anyURI($artlog:strArtLog),'rwxrwxrwx')
        return update insert attribute on {"true"} into doc('../settings.xml')/logging
return response:redirect-to(xs:anyURI('logging.xquery'))