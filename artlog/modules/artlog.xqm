xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
module namespace artlog = "http://art-decor.org/ns/artlog";
declare namespace repo="http://exist-db.org/xquery/repo";

declare variable $artlog:strArtLog := concat(repo:get-root(), 'artlog-data');
declare variable $artlog:logOn := if (doc('../settings.xml')/logging/@on) then true() else false();

declare %private function artlog:storeLog($log as node(), $app as xs:string, $name as xs:string) {
    (: This check should be included when calling artlog, this is extra security that no calls are handled when logging is off :)
    if (not($artlog:logOn)) then () else 
    let $cleanup := 
        for $resource in xmldb:get-child-resources($artlog:strArtLog)
        return if (xmldb:created($artlog:strArtLog, $resource) < (fn:current-dateTime()-  xs:dayTimeDuration('PT30M') )) then xmldb:remove($artlog:strArtLog, $resource) else ()
    return 
        xmldb:store($artlog:strArtLog, concat($name, '-', $app, '-', fn:substring(translate(xs:string(fn:current-dateTime()), ':T-.+', ''),1,17), '.xml'), $log)
};

declare function artlog:log($data as node()) {
    artlog:log($data, '-', 'LOG')
};

declare function artlog:log($data as node(), $app as xs:string?) {
    artlog:log($data, $app, 'LOG')
};

declare function artlog:logPostData($data as node()) {
    artlog:storeLog($data, '-', 'POSTdata')
};

declare function artlog:logPostData($data as node(), $app as xs:string?) {
    artlog:storeLog($data, $app, 'POSTdata')
};

declare function artlog:log($data as node(), $app as xs:string?, $name as xs:string?) {
    let $object :=
        <log> 
            <user>{(sm:id()//sm:real/sm:username/text())[1]}</user>
            <app>{$app}</app>
            <time>{fn:current-dateTime()}</time>
            <resource>{$name}</resource>
            <data>{$data}</data>
        </log>
    return artlog:storeLog($object, $app, $name)
};

declare function artlog:logRequest($app as xs:string) {
    let $parts := tokenize(request:get-uri(), '/')
    let $object :=
        <request>
            <user>{(sm:id()//sm:real/sm:username/text())[1]}</user>
            <app>{$app}</app>
            <time>{fn:current-dateTime()}</time>
            <uri>{request:get-uri()}  </uri>
            <resource>{$parts[last()]}</resource>
            <method>{request:get-method()}</method>
            <accept>{request:get-header('Accept')}</accept>
            <params>
            {for $param in request:get-parameter-names()
            return <param name="{$param}" value="{request:get-parameter($param, '')}"/>
            }
            </params>
        </request>
    return artlog:storeLog($object, $app, request:get-method())
};
