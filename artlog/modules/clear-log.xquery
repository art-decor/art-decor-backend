(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
xquery version "3.0";

import module namespace ada ="http://art-decor.org/ns/artlog" at "artlog.xqm";

let $clear :=
    for $resource in xmldb:get-child-resources($ada:strArtLog)
    return xmldb:remove($ada:strArtLog, $resource)
return response:redirect-to(xs:anyURI('logging.xquery'))