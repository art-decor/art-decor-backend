xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

declare namespace sm           = "http://exist-db.org/xquery/securitymanager";
declare namespace request      = "http://exist-db.org/xquery/request";
declare namespace response     = "http://exist-db.org/xquery/response";

let $test   := request:get-data()
let $user   := 
    if ($test//username/text()) then
        $test//username/text()
    else ('guest')
    
let $pwd    := 
    if ($test//password/text()) then
        $test//password/text()
    else('guest')
let $create := session:create()

return
if (xmldb:authenticate('/db',$user,$pwd)) then
    let $login            := xmldb:login('/db',$user,$pwd,xs:boolean('true'))
    let $username         := (sm:id()//sm:real/sm:username/text())[1]
    let $groups           := sm:get-user-groups($username)
    return
        <user>
            <username>{$username}</username>
            <groups>{$groups}</groups>
            <password>{$pwd}</password>
            <logged-in>true</logged-in>
        </user>
else (
    let $logout   := session:invalidate()
    (:let $username := (sm:id()//sm:real/sm:username/text())[1]:)
    (:let $groups   := sm:get-user-groups($user):)
    return
        <user>
            <username>{$user}</username>
            <groups></groups>
            <password></password>
            <logged-in>false</logged-in>
        </user>
)