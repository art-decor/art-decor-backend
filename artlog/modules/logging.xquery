xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
declare option exist:serialize "method=xhtml media-type=text/html indent=yes";
import module namespace artlog ="http://art-decor.org/ns/artlog" at "artlog.xqm";

<html>
   <head>
      <title>ART Logging</title>
      <link rel="stylesheet" type="text/css" href="https://assets.art-decor.org/ADAR/rv/assets/css/default.css"/>
      <style>
      table   {{ 
        border-collapse:collapse;
        }}
      td, th  {{ 
        border:1px solid;
        }}
      input {{
        width: 60em;
        }}
      </style>
      <script src="../script/artlog.js"></script>
    </head>
    <body class="orbeon">
       <h1>ART Logging</h1>
       <p>Logged in as: {(sm:id()//sm:real/sm:username/text())[1]}</p>
       <p>Logging is turned {if ($artlog:logOn) then 'ON' else 'OFF'} (Press <a href="logging.xquery">F5</a> to refresh)</p>
       <p><a href="toggle-logging.xquery">Turn logging {if ($artlog:logOn) then 'OFF' else 'ON'}</a></p>
       <p><a href="clear-log.xquery">Clear log</a></p>
       <p><a href="https://www.art-decor.org/mediawiki/index.php?title=ART_Logging" target="_blank">Help</a></p>
       <table>
            <tr>
                <th>Time</th>
                <th>App</th>
                <th>User</th>
                <th>Resource</th>
                <th>Logfile</th>
                <th>Execute</th>
                <th>eXide</th>
            </tr>
           {
            for $resource in xmldb:get-child-resources($artlog:strArtLog)
            let $created := xmldb:created($artlog:strArtLog, $resource)
            let $doc := doc(concat($artlog:strArtLog, '/', $resource))
            let $querystring := 
                if($doc//params/param) 
                then
                    let $params := 
                        for $param in $doc//params/param
                        return concat($param/@name, '=', $param/@value)
                    return concat('?', string-join($params, '&amp;'))
                else ()
            order by $created descending
            return 
                <tr>
                    <td>{fn:substring($doc/*/time, 12, 8)}</td>
                    <td>{$doc/*/app}</td>
                    <td>{$doc/*/user}</td>
                    <td>{if ($doc/*/resource) then $doc/*/resource else 'POSTdata'}</td>
                    <td><button type="button" onclick="showXmlData('{substring-after($artlog:strArtLog, '/db')}/{$resource}', '{if ($doc/*/resource) then $doc/*/resource else 'POSTdata'}')">SHOW</button><a href="../../artlog-data/{$resource}" target="_blank">{$resource}</a></td>
                    <td>{if ($doc/*/method) then <button type="button" onclick="sendXmlData('{$doc/*/method}', '{$resource}', '{$doc//uri}{$querystring}')">{$doc/*/method}</button> else()}</td>
                    <td><a href="/apps/eXide/index.html?open={$artlog:strArtLog}/{$resource}" target="eXide">Open</a></td>
                </tr>
            }
      </table>
      <br/>
      <table style="width:100%">
            <tr><td></td><td></td><td>URL:</td><td><input id="url"/></td></tr>
            <tr><td>Data:</td><td><input id="dataType"/></td><td>Status:</td><td><input id="returnedStatus"/></td></tr>
            <tr><td colspan="2" style="width:50%;"><textarea id="showXml" style="width:100%;height:24em"></textarea></td><td colspan="2" ><textarea id="returnedData" style="width:100%;height:24em"></textarea></td></tr>
      </table>
    </body>
</html>