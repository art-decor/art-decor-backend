This directory contains the list files and changes needed to an eXist-db 5.0.0 installation for ART-DECOR

- etc/jetty/jetty-http.xml
- etc/jetty/jetty-ssl.xml
Contains jetty port definition

- etc/webapp/WEB-INF/betterform-config.xml
Contains property to switch off betterform

- etc/webapp/WEB-INF/controller-config.xml
Contains ART-DECOR paths
Contains services, WebDAV is switched off