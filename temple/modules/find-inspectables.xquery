xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace temple      = "http://art-decor.org/ns/temple" at "temple.xqm";
import module namespace adsearch    = "http://art-decor.org/ns/decor/search" at "../../art/api/api-decor-search.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at "../../art/modules/art-decor.xqm";
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";

(: For testing :)
declare %private function local:getDodo($find as xs:string) as node()* {
    try {
        (
        util:import-module(xs:anyURI("http://art-decor.org/ns/terminology/dodo"), "dodo", xs:anyURI("../../terminology/dodo/api/api-dodo.xqm"))
        ,
        let $function := function-lookup(xs:QName("dodo:searchConcept"), 1)
        return $function($find)
        )
    } catch * {<error>This server does not support dodos</error>}
};

declare %private function local:getLoinc($find as xs:string, $language as xs:string) as node()* {
    try {
        (
        util:import-module(xs:anyURI("http://art-decor.org/ns/terminology/loinc"), "adloinc", xs:anyURI("../../terminology/loinc/api/api-loinc.xqm"))
        ,
        let $function := function-lookup(xs:QName("adloinc:searchConcept"), 6)
        return $function($find, 1, (), (), 'ACTIVE', $language)
        )
    } catch * {<error>This server does not support LOINC</error>}
};

declare %private function local:getSnomedCt($find as xs:string) as node()* {
    try {
        (
        util:import-module(xs:anyURI("http://art-decor.org/ns/terminology/snomed"), "snomed", xs:anyURI("../../terminology/snomed/api/api-snomed.xqm"))
        ,
        (: See: https://stackoverflow.com/questions/3854345/xpath-test-if-node-value-is-number
        Tests like string(number(myNode)) != 'NaN' fail for large numbers since they string() returns 1.23E8 (exponents) :)
        if (number($find) = number($find))
        then 
            let $function := function-lookup(xs:QName("snomed:getRawConcept"), 1)
            return $function($find)
        else 
            let $function := function-lookup(xs:QName("snomed:searchDescription"), 4)
            return $function($find, 50, (), ())
        )
    } catch * {<error>This server does not support SNOMED CT</error>}
};

let $find                   := if (request:exists()) then request:get-parameter('find', '') else 'myocard infar' 
let $prefix                 := if (request:exists()) then request:get-parameter('prefix', '') else 'dodo-' (:'peri20-':)
let $scope                  := if (request:exists()) then request:get-parameter('scope', '') else '*' 
let $language               := if (request:exists()) then request:get-parameter('language', $get:strArtLanguage) else 'nl-NL'
let $datasetId              := art:getDecorByPrefix($prefix)//dataset[last()]/@id/string()
let $searchTerms            := tokenize($find,'\s')
let $prefix                 := if (ends-with($prefix, '-')) then $prefix else concat($prefix, '-')

let $inspectables          :=
    (: For testing :)
    if ($scope = 'dodo') then 
        local:getDodo($find)
    else if ($scope = 'loinc') then 
        local:getLoinc($find, $language)
    else if ($scope = 'sct') then 
        local:getSnomedCt($find)
    else if (starts-with($scope, 'con')) then
    (
        adsearch:searchConcept($prefix, $searchTerms, (), $datasetId, (), (), false(), if ($prefix='all-' or $prefix='*-') then false() else true())/*
    )
    else if (starts-with($scope, 'tra')) then
    (
        adsearch:searchTransaction($prefix,$searchTerms, (), (), $language)/*
    )
    else if (starts-with($scope, 'cod')) then
    (
        adsearch:searchCodesystem($prefix,$searchTerms, (), (), $language)/*
    )
    else if (starts-with($scope, 'val')) then
    (
        adsearch:searchValueset($prefix, $searchTerms, (), (), $language)/*
    )
    else if (starts-with($scope, 'tem')) then
    (
        adsearch:searchTemplate($prefix, $searchTerms, (), (), $language)/*
    )
    else (
        adsearch:searchConcept($prefix, $searchTerms, (), $datasetId, (), (), false(), if ($prefix='all-' or $prefix='*-') then false() else true())/* |
        adsearch:searchTransaction($prefix,$searchTerms, (), (), $language)/* | 
        adsearch:searchCodesystem($prefix,$searchTerms, (), (), $language)/* |
        adsearch:searchValueset($prefix, $searchTerms, (), (), $language)/* | 
        adsearch:searchTemplate($prefix, $searchTerms, (), (), $language)/*
    )

return 
<found string="{$find}" prefix="{$prefix}" scope="{$scope}">
{
    for $inspectable in $inspectables
    return 
        if ($inspectable/local-name() = 'error') then 
            $inspectable
        else 
        if ($scope = 'loinc') then
            (: search is for LOINC id :)
            if (replace($find, '-', '') = xs:string(number(replace($find, '-', '')))) then (
                <!-- LOINC + project language concept -->,
                let $concept := $inspectables/concept[1]
                return <concept>{$concept/@*, $concept/(* except concept), $concept/concept[@language=$language]}</concept>
            )
            else (
                <!-- ACTIVE codes only -->,
                for $concept in $inspectables/*
                return <concept code="{$concept/@loinc_num}" codeSystem="2.16.840.1.113883.6.1" displayName="{$concept/longName}"/>
            )
        else 
        if ($scope = 'sct')
        then 
            (: search is for SNOMED CT id :)
            if (matches($find, '^\d+$')) then (
                <!-- raw snomed concept -->,
                $inspectables/*
            )
            else (
                <!-- max 50 results -->,
                for $concept in $inspectables/*
                return <concept code="{$concept/@conceptId}" codeSystem="2.16.840.1.113883.6.96" displayName="{$concept/@fullName}"/>
            )
        else
        element {local-name($inspectable)} {
            $inspectable/@name, 
            $inspectable/@displayName, 
            $inspectable/@id, 
            $inspectable/@effectiveDate, 
            $inspectable/@statusCode, 
            $inspectable/@type, 
            $inspectable/name[@language=$language], 
            $inspectable/path[@language=$language]
        }
}
</found>