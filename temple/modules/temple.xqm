xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
module namespace temple = "http://art-decor.org/ns/temple";
import module namespace art     = "http://art-decor.org/ns/art" at "../../art/modules/art-decor.xqm";
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";

declare variable $temple:userData       := doc(concat($get:root, 'temple-data/userData.xml'));
declare variable $temple:myBookmarks    := $temple:userData//user[@name=get:strCurrentUserName()]/bookmarks;
declare variable $temple:strUserTemp    := concat($get:root, 'temple-data/temp');

declare function temple:isOid($oid as xs:string?) as xs:boolean {
    matches($oid, "^[0-2](\.(0|[1-9][0-9]*))*$")
};

declare function temple:conceptShortName($id as xs:string, $effectiveDate as xs:string?, $decor as node(), $language as xs:string) as xs:string {
    let $concept := art:getOriginalForConcept(<concept><inherit ref="{$id}" effectiveDate="{$effectiveDate}"/></concept>)
    return
        if ($concept) then 
            let $name := 
                if ($concept/name[@language=$language][string-length()>0]) 
                then $concept/name[@language=$language] 
                else $concept/name[string-length()>0][1]
            
            return if ($name[string-length()>0]) then art:shortName($name) else 'no_name_found'
        else
            'no_concept_in_project'
};
declare function temple:codeSystemShortName($id as xs:string, $effectiveDate as xs:string?, $decor as node(), $language as xs:string) as xs:string {
    let $object     := replace(art:getNameForOID($id, $language, $decor/project/@prefix),'''','')
    return
        if (string-length($object)>0) then $object else 'no_name_found'
};
declare function temple:valueSetShortName($id as xs:string, $effectiveDate as xs:string?, $decor as node(), $language as xs:string) as xs:string {
    let $effectiveDate  :=
        if ($effectiveDate castable as xs:dateTime) then $effectiveDate else (
            let $valuesets := $decor//valueSet[@id = $id]
            return max($valuesets/xs:dateTime(@effectiveDate))
        )
    
    let $object         := 
        $decor//valueSet[@id = $id][@effectiveDate = $effectiveDate] | $decor//valueSet[@ref = $id]
    return
        if ($object[@name]) then ($object/@name)[1] else if ($object) then 'no_name_found' else 'no_valueSet_in_project'
};

declare function temple:oidToName($oid as xs:string?, $effectiveDate as xs:string?) as xs:string {
    if (temple:isOid($oid)) then (
        let $things         := $get:colDecorData//template[@id=$oid] | 
                               $get:colDecorData//valueSet[@id=$oid] |
                               $get:colDecorCache//template[@id=$oid] | 
                               $get:colDecorCache//valueSet[@id=$oid]
        let $effectiveDate  := 
            if ($effectiveDate castable as xs:dateTime)
            then $effectiveDate
            else max($things/xs:dateTime(@effectiveDate))
        let $thing          := $things[@effectiveDate=$effectiveDate]
        return concat($thing[1]/@name, '[', $oid , ']' ) 
    )
    else (
        (: if it's not an oid, just return it :)
        $oid
    )
};

declare function temple:addNamesToRulesOids($xml as node(), $decor as node(), $language as xs:string) as node() {
    let $result :=
        if (local-name($xml)='example') then 
            $xml
        else
            element {local-name($xml)} 
            {
                for $att in $xml/@*
                return 
                if ( local-name($att)='ref' and local-name($xml)='concept' ) then
                    attribute {name($att)} {concat(temple:conceptShortName($att, $att/../@effectiveDate, $decor, $language), '[', $att, ']')}
                else if ( local-name($att)='ref' and local-name($xml)='include' ) then 
                    attribute {name($att)} {temple:oidToName($xml/@ref/string(), $xml/@flexibility)}
                else if ( local-name($att)='contains' and local-name($xml)='element' ) then 
                    attribute {name($att)} {temple:oidToName($xml/@contains/string(), $xml/@flexibility)}
                else if ( local-name($att)='codeSystem' and local-name($xml)='vocabulary' ) then 
                    attribute {name($att)} {concat(temple:codeSystemShortName($att, (), $decor, $language), '[', $att, ']')}
                else if ( local-name($att)='valueSet' and local-name($xml)='vocabulary' ) then 
                    attribute {name($att)} {concat(temple:valueSetShortName($att, $xml/@flexibility, $decor, $language), '[', $att, ']')}
                else $att, 
                for $child in $xml/node() 
                return 
                    if ($child instance of element()) then temple:addNamesToRulesOids($child, $decor, $language) else $child
           }
    return $result
};

(: Will return OID for things like Datummeting[2.16.840.1.113883.3.1937.99.62.3.10.4], else just return what it got :)
declare function temple:toOid($oid as xs:string) as xs:string {
    let $innerOid := substring-before(substring-after($oid,'['),']')
    return if (temple:isOid($innerOid)) then $innerOid else $oid
};

declare function temple:removeNamesFromRulesOids($xml as node()) as node() {
    let $result :=
        if (local-name($xml)='example') then 
            $xml
        else
            element {local-name($xml)}
            {
                for $att in $xml/@*
                return 
                    attribute {local-name($att)} {
                        if (local-name($att)='ref' or local-name($att)='contains' or local-name($att)='codeSystem' or local-name($att)='valueSet') 
                        then temple:toOid($att/string()) else $att/string()
                    }
                , 
                for $child in $xml/node() 
                return 
                    if ($child instance of element()) then temple:removeNamesFromRulesOids($child) else $child
            }
    return $result
};

(: If there's a new @id, replace it in templateId/@root, attribute/@value of hl7:templateId and element/@id as well :) 
declare function temple:replaceTemplateNewId($xml as node(), $sentId, $newId) as node() {
    let $result :=
        element {local-name($xml)}
        {
            for $att in $xml/@*
            return 
                attribute {local-name($att)} {
                    if ((local-name($xml) = 'element' and local-name($att)='id') 
                        or (local-name($xml) = 'concept' and local-name($att)='elementId')
                        or (local-name($xml) = 'templateId' and local-name($att)='root')
                        or (local-name($xml) = 'attribute' and $xml[@name= 'root'] and local-name($att)='value')
                        ) 
                    then replace($att/string(), $sentId, $newId) else $att/string()
                }
            , 
            for $child in $xml/node() 
            return 
                if ($child instance of element()) then temple:replaceTemplateNewId($child, $sentId, $newId) else $child
        }
    return $result
};

declare function temple:addNamesToTerminologyOids($xml as node(), $decor as node(), $language as xs:string) as node() {
    let $result :=
        element {local-name($xml)} 
        {
            for $att in $xml/@*
            return 
            if ( local-name($att)='conceptId' ) then
                attribute {name($att)} {concat(temple:conceptShortName($att, $att/../@conceptFlexibility, $decor, $language), '[', $att, ']')}
            else if ( local-name($att)='codeSystem' ) then 
                attribute {name($att)} {concat(temple:codeSystemShortName($att, (), $decor, $language), '[', $att, ']')}
            else if ( local-name($att)='valueSet' ) then 
                attribute {name($att)} {concat(temple:valueSetShortName($att, $att/../@flexibility, $decor, $language), '[', $att, ']')}
            else if ( local-name($att)='ref' and local-name($xml)='include' ) then 
                attribute {name($att)} {concat(temple:valueSetShortName($att, $att/../@flexibility, $decor, $language), '[', $att, ']')}
            else $att, 
            for $child in $xml/node() 
            return 
                if ($child instance of element()) then temple:addNamesToTerminologyOids($child, $decor, $language) else $child
       }
    return $result
};

declare function temple:removeNamesFromTerminologyOids($xml as node()) as node() {
    let $result :=
        if (local-name($xml)='example') then 
            $xml
        else
            element {local-name($xml)}
            {
                for $att in $xml/@*
                return 
                    attribute {local-name($att)} {
                        if (local-name($att)='conceptId' or local-name($att)='codeSystem' or local-name($att)='valueSet' or local-name($att)='ref') 
                        then temple:toOid($att/string()) else $att/string()
                    }
                , 
                for $child in $xml/node() 
                return 
                    if ($child instance of element()) then temple:removeNamesFromTerminologyOids($child) else $child
            }
    return $result
};
