xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace temple      = "http://art-decor.org/ns/temple" at "temple.xqm";
import module namespace setlib      = "http://art-decor.org/ns/api/settings" at "../../api/modules/library/settings-lib.xqm";
import module namespace utillib     = "http://art-decor.org/ns/api/util" at "../../api/modules/library/util-lib.xqm";
import module namespace decor       = "http://art-decor.org/ns/decor" at "../../art/api/api-decor.xqm";
import module namespace histlib     = "http://art-decor.org/ns/api/history" at "../../api/modules/library/history-lib.xqm";
declare namespace request       = "http://exist-db.org/xquery/request";
declare namespace response      = "http://exist-db.org/xquery/response";
declare namespace util          = "http://exist-db.org/xquery/util";
declare namespace validation    = "http://exist-db.org/xquery/validation";

(: You would say that saving this pseudo project to the db and then validate it is a lot of redudant work
   Why not in memory you ask? Well put simply: as soon the conceptMap contains new content in a 'different'
   namespace like hl7nl: or pharm: we get validation errors like this:
   
   Input: <element name="hl7:numerator" datatype="hl7nl:PQ" ...
   
   Temple validation error, code: NotSchemaValid, description: invalid 82 UndeclaredPrefix: Cannot resolve 
   'hl7nl:PQ' as a QName: the prefix 'hl7nl' is not declared.cvc-attribute.3: The value 'hl7nl:PQ' of 
   attribute 'datatype' on element 'element' is not valid with respect to its type, 'QName'.
   
   When we save to the db, and validate from there, this does not occur. So: this is a pragmatic workaround
   for an unexplained phenomenon.
:)
declare %private function local:validateSchema($decor as element(), $newTerminology as element(terminology)) as node() {
    let $res            := concat(util:uuid(),'.xml')
    let $tempProject    :=
        <decor>
        {
            for $ns at $i in utillib:getDecorNamespaces($decor)
            let $ns-uri     := $ns/@uri/string()
            let $ns-prefix  := $ns/@prefix/string()
            return
                attribute {QName($ns-uri,concat($ns-prefix,':dummy-',$i))} {$ns-uri}
        }
            <project>
            {
                $decor/project/@*
            }
                <name/>
                <copyright years=""/>
            </project>
            <datasets/>
            <scenarios>
                <actors/>
            </scenarios>
            <ids/>
            <terminology>
            {
                $newTerminology/node()
            }
            </terminology>
            <rules/>
            <issues/>
        </decor>
    let $store          := xmldb:store($temple:strUserTemp, $res, $tempProject)
    
    (: Using the schema at 'https://assets.art-decor.org/ADAR/rv/DECOR.xsd' throws a SAX error, using the local schema does not. :)
    let $result         := validation:jaxv-report(doc($store), $setlib:docDecorSchema)
    
    let $delete         := xmldb:remove($temple:strUserTemp, $res)
    
    return
        $result
};

declare %private function local:saveConceptMap($prefix as xs:string, $newTerminology as element(terminology), $reportOnly as xs:string) as element(){
    let $decor              := utillib:getDecorByPrefix($prefix)
    let $user               := setlib:strCurrentUserName()
    let $isAuthor           := decor:authorCanEditP($decor, $decor:SECTION-TERMINOLOGY)
    let $assert             := 
        if ($isAuthor) then () else (
            error(QName('http://art-decor.org/ns/error', 'YouAreNoAuthor'), concat('User ', $user, ' is not an author in this project'))
        )
    let $logfile            := doc('/db/apps/temple/xml/log.xml')
    let $logon              := false()
    let $assert             := 
        if (count($newTerminology/conceptMap[@id][@effectiveDate]) = 1 ) then () else (
            error(QName('http://art-decor.org/ns/error', 'MissingConceptMapWithIdAndEffectiveDate'), 'Terminology must contain exactly one conceptMap with @id and @effectiveDate')
        )
    let $assert             := 
        if ($newTerminology/conceptMap[@ref]) then (
            error(QName('http://art-decor.org/ns/error', 'NoConceptMapWithRef'), 'Terminology must not contain exactly conceptMap(s) with @ref')
        ) else ()
    (:let $assert         := 
        for $vs in $newTerminology/terminologyAssociation[@conceptMap]
        return 
            if ($vs/@conceptMap = $newTerminology/conceptMap/@id) then () else (
                error(QName('http://art-decor.org/ns/error', 'TerminologyAssociationShallMatchConceptMapId'), concat('TerminologyAssociation for concept ', $vs/@conceptId, ' with conceptMap ',$vs/@conceptMap,' SHALL conceptMap/@id in this set'))
            ):)
    (:let $oldTermAssociation     := $decor//terminologyAssociation[@conceptMap=$newId][@flexibility=$newEffectiveDate]:)
    
    (: although it looks as if we could support multiple conceptMaps like this, we cannot because of the redirect at the very end to exactly 1 conceptMap :)
    let $newTerminology     :=
        <terminology>
        {
            $newTerminology/@*
        }
        {
            for $conceptMap in $newTerminology/conceptMap
            let $newId                  := $conceptMap/@id
            let $newEffectiveDate       := $conceptMap/@effectiveDate
            let $newStatus              := $conceptMap/@statusCode
            let $oldConceptMap          := $decor//conceptMap[@id=$newId][@effectiveDate=$newEffectiveDate]
            
            (: Check status :)
            let $statusAllowed          := if ($oldConceptMap) then utillib:isStatusChangeAllowable($oldConceptMap, $newStatus) else (true())
            let $assert                 :=  
                if ($statusAllowed) then () else (
                    error(QName('http://art-decor.org/ns/error', 'StatusChangeNotAllowed'), concat('Setting status from ', $oldConceptMap/@statusCode, ' to ', $newStatus, ' is not allowed'))
                )
            
            (: Make a new conceptMap when submitted one has id 'new', or submitted id exists but submitted effectiveDate doesn't :)
            let $makeNewConceptMap        :=
                (: conceptMap with id and effectiveDate exists :)
                if ($oldConceptMap) then false()
                (: A new conceptMap, new id must be issued :)
                else if ($newId[ends-with(.,'new')]) then true()
                (: conceptMap with this id exists, but not this effectiveDate :)
                else if ($decor//conceptMap[@id=$newId]) then not($oldConceptMap)
                (: Id does not exist and != 'new :)
                else error(QName('http://art-decor.org/ns/error', 'IdNotValid'), concat('conceptMap/@id must either exist or be ''{{some baseId}}.new''. Found ', $newId))
            let $makeNewVersion         :=
                if ($oldConceptMap) then false() else if ($decor//conceptMap[@id=$newId]) then true() else false()
            
            let $assert         :=  
                if (not($makeNewConceptMap and $decor//conceptMap[@name=$conceptMap/@name][@effectiveDate=$newEffectiveDate])) then () else (
                    error(QName('http://art-decor.org/ns/error', 'ConceptMapByNameAndDateExists'), concat('A conceptMap with this name=''',$conceptMap/@name,''' and effectiveDate=''',$newEffectiveDate,''' already exists.'))
                )
            
            return
                if ($makeNewConceptMap) then (
                    (: Issue new id :)
                    (: MdG Note to self: Code copied from create-decor-conceptMap.xquery. I cannot use the entire xquery, code like 
                    this - and more - should be refactored into conceptMaps api :)
                    let $conceptMapRoot   := 
                        if ($makeNewVersion)
                        then ($newId) else
                        if (temple:isOid(replace($newId,'.?new',''))) 
                        then (replace($newId,'.?new',''))
                        else (decor:getDefaultBaseIdsP($decor, $decor:OBJECTTYPE-MAPPING)/@id)
                    
                    (:generate new id if mode is 'new' or 'adapt', else use existing id:)
                    let $newConceptMapId  := 
                        if ($makeNewVersion) then
                            $newId
                        else (
                            let $precedingNewlings      :=
                                count(
                                    for $vs in $conceptMap/preceding-sibling::conceptMap
                                    let $i  := $vs/@id
                                    let $e  := $vs/@effectiveDate
                                    let $d  := $decor//conceptMap[@id=$i]
                                    return $i[ends-with(.,'new')] or ($d and not($d[@effectiveDate = $e]))
                                )
                            let $conceptMapIdsAll         := 
                                $decor//@id[matches(.,concat('^',$conceptMapRoot,'\.\d+$'))]
                            return
                            if ($conceptMapIdsAll) then
                                concat($conceptMapRoot,'.',max($conceptMapIdsAll/xs:integer(tokenize(.,'\.')[last()])) + 1 + $precedingNewlings)
                            else (
                                concat($conceptMapRoot,'.', 1 + $precedingNewlings)
                            )
                        )
                        
                    let $assert             := 
                        if (matches($newConceptMapId, '^[0-2](\.(0|[1-9][0-9]*)){3,}$')) then () else (
                            error(QName('http://art-decor.org/ns/error', 'ConceptMapIdInvalid'), 'ConceptMap ' || $newConceptMapId || ' pattern SHALL be [0-2](\.(0|[1-9][0-9]*))*. An OID identifies exactly one object and SHALL NOT be reused. OIDs of 3 nodes or less are always reserved for a purpose. See https://oid-info.com or this servers'' OID index when in doubt..')
                        )
                    
                    let $baseConceptMap   := 
                        <conceptMap>
                        {
                            attribute id {$newConceptMapId},
                            $conceptMap/@name[string-length()>0],
                            $conceptMap/@displayName[string-length()>0],
                            attribute effectiveDate {$newEffectiveDate},
                            $conceptMap/@statusCode[string-length()>0],
                            $conceptMap/@versionLabel[string-length()>0],
                            $conceptMap/@expirationDate[string-length()>0],
                            $conceptMap/@officialReleaseDate[string-length()>0],
                            $conceptMap/@experimental[string-length()>0],
                            $conceptMap/@caseSensitive[string-length()>0],
                            $conceptMap/@canonicalUri[string-length()>0]
                        }
                        </conceptMap>
                    return
                        utillib:prepareConceptMapForUpdate($conceptMap, $baseConceptMap)
                )
                else (
                    let $baseConceptMap   := 
                        <conceptMap>
                        {
                            $conceptMap/@id[string-length()>0],
                            $conceptMap/@name[string-length()>0],
                            $conceptMap/@displayName[string-length()>0],
                            $conceptMap/@effectiveDate[string-length()>0],
                            $conceptMap/@statusCode[string-length()>0],
                            $conceptMap/@versionLabel[string-length()>0],
                            $conceptMap/@expirationDate[string-length()>0],
                            $conceptMap/@officialReleaseDate[string-length()>0],
                            $conceptMap/@experimental[string-length()>0],
                            $conceptMap/@caseSensitive[string-length()>0],
                            $conceptMap/@canonicalUri[string-length()>0]
                        }
                        </conceptMap>
                    return
                        utillib:prepareConceptMapForUpdate($conceptMap, $baseConceptMap)
                )
        }
        </terminology>
    
    let $newTerminology         := temple:removeNamesFromTerminologyOids($newTerminology)
    
    (: Validate against decor.xsd :)
    let $schemaResults          := local:validateSchema($decor, $newTerminology)
    let $assert                 :=  
        if (not($schemaResults//status="invalid")) then () else (
            error(QName('http://art-decor.org/ns/error', 'NotSchemaValid'), $schemaResults)
        )
    
    (: Block updates if an error occurred :)
    let $update                 := 
        if ($reportOnly = 'true') then (<ok/>) else (
            (:if ($logon) then 
                update insert 
                    <update user="{$user}" time="{fn:current-dateTime()}">
                        <old><terminology>{$oldConceptMap}</terminology></old>
                        <new>{$newTerminology}</new>
                    </update>
                into $logfile/logroot 
            else <nothing/>
            ,:)
            let $update             :=
                for $conceptMap in $newTerminology/conceptMap
                let $newId              := $conceptMap/@id
                let $newEffectiveDate   := $conceptMap/@effectiveDate
                let $existingConceptMap := $decor//conceptMap[@id=$newId][@effectiveDate=$newEffectiveDate]
                (: now update the value set :)
                return
                    if ($existingConceptMap) then (
                        (: ======= do history first ======= :)
                        let $intention          :=
                            if ($existingConceptMap[@statusCode='final'][@effectiveDate = $conceptMap/@effectiveDate]) then 'patch' else 'version'
                        let $history            :=
                            if ($existingConceptMap) then histlib:AddHistory(setlib:strCurrentUserName(), $decor:OBJECTTYPE-MAPPING, $prefix, $intention, $existingConceptMap) else ()
                        return
                            update replace $existingConceptMap with $conceptMap
                    )
                    else
                    if ($decor/terminology/valueSet) then 
                        update insert $newTerminology/conceptMap preceding $decor/terminology/valueSet[1]
                    else
                    if ($decor/terminology/terminologyAssociation) then 
                        update insert $newTerminology/conceptMap following $decor/terminology/terminologyAssociation[last()]
                    else (
                        update insert $newTerminology/conceptMap into $decor/terminology
                    )
                
            return <ok/>
        )
    return $newTerminology
};

let $prefix         := if (request:exists()) then request:get-parameter('prefix', '') else 'demo1-'
let $language       := if (request:exists()) then request:get-parameter('language', '') else 'nl-NL'
let $code           := if (request:exists()) then request:get-parameter('code', '') else ''
let $reportOnly     := if (request:exists()) then request:get-parameter('reportOnly', 'false') else 'true'
let $newTerminology :=  
    try {fn:parse-xml($code)/*}
    catch * {
        <templeError at="{current-dateTime()}">{concat('Temple parsing error, code: ', $err:code, ', description: ', $err:description, ' value: ',$err:value,' module: ',$err:module,' (',$err:line-number,' ',$err:column-number,')')}</templeError>
    }

(: When there is a parsing error, save and validate. If there is a schema validation error, validate again to catch output, else return the error as is. :)
let $result         := 
    if ($newTerminology[self::templeError]) then $newTerminology else (
        try {local:saveConceptMap($prefix, $newTerminology, $reportOnly)}
        catch * {
            <templeError>{concat('Temple validation error, code: ', $err:code, ', description: ', $err:description)}</templeError>
        }
    )
    
let $decor  := utillib:getDecorByPrefix($prefix)
let $language := if (request:exists()) then request:get-parameter('language', $setlib:strArtLanguage) else ''
let $result := temple:addNamesToTerminologyOids($result, $decor, $language)

let $dummy := 
    if (response:exists()) then ( 
        if ($result//status[.='invalid'] | $result[self::templeError]) 
        then (response:set-status-code(400)) 
        else (response:set-status-code(200))
    ) else ()
    
return $result