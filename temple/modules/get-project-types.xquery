xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace art     = "http://art-decor.org/ns/art" at "/db/apps/templ/modules/../../art/modules/art-decor.xqm";
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace vs      = "http://art-decor.org/ns/decor/valueset" at "../..art/api/api-decor-valueset.xqm";
import module namespace templ   = "http://art-decor.org/ns/decor/template" at "/db/apps/templ/modules/../../art/api/api-decor-template.xqm";
declare namespace request       = "http://exist-db.org/xquery/request";
declare namespace output        = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace json          = "http://www.json.org";
declare option output:method "text";
declare option output:media-type "text/javascript";

(: return json :)
declare %private function local:toJson($el as node()) as xs:string {
    concat("{'text' : '", $el/text/string(), "', 'displayText' : '", local:attText($el/displayText/string()), "'}")
};
(: get data and eliminate undesired tokens from displayNames :)
declare %private function local:attText($str as xs:string) as xs:string {
    translate($str, "'", "")
};

(: return json :)
declare %private function local:toJs($el as node()) as xs:string {
    concat('{"text" : "', local:attJs($el/text/string()), '", "displayText" : "', local:attJs($el/displayText/string()), '"}')
};
declare %private function local:attJs($str as xs:string) as xs:string {
    let $str := replace($str, "\\", "\\\\") (: backslash :)
    let $str := replace($str, '"', '\\"') (: double quote :)
    let $str := replace($str, "&#x09;", "\\t") (: tab :)
    let $str := replace($str, "&#x0a;", "\\n") (: newline :)
    let $str := replace($str, "&#x0d;", "\\r") (: carriage return :)
    return $str
};

let $nl                     := "&#10;"
let $format                 := if (request:exists()) then request:get-parameter('format', 'js')[string-length()>0] else 'json'
let $language               := if (request:exists()) then request:get-parameter('language', ())[string-length()>0] else ()
let $prefix                 := if (request:exists()) then request:get-parameter('prefix',())[string-length()>0] else 'demo1-'
let $id                     := if (request:exists()) then request:get-parameter('id', '') else '2.16.840.1.113883.2.4.6.10.90.70'
let $datasetId              := if (request:exists()) then request:get-parameter('datasetId', ())[string-length()>0] else ()
let $datasetEffectiveDate   := if (request:exists()) then request:get-parameter('datasetEffectiveDate', ())[string-length()>0] else ()

let $decor                  := if (empty($prefix)) then () else art:getDecorByPrefix($prefix)
let $language               := if (empty($language)) then data($decor/project/@defaultLanguage) else $language

let $dataset                := 
    if (empty($datasetId)) then () else (
        art:getDataset($datasetId, $datasetEffectiveDate, (), $language) | art:getTransaction($datasetId, $datasetEffectiveDate, (), $language)
    )

let $dataset                :=
    if ($dataset[self::dataset]) then
        art:getDatasetTree($datasetId, $datasetEffectiveDate, (), (), false())
    else (
        art:getDatasetTree((), (), $datasetId, $datasetEffectiveDate, false())
    )

let $datasets := art:getDatasetList($prefix, (), (), false(), false())
let $projectDataSets        :=
for $dataset in $datasets/dataset
return
    <dataset json:array="true">
        <text>{concat($dataset/@id,'--',$dataset/@effectiveDate)}</text>
        <displayText>
            {
        if ($dataset/name[@language = $language]) then
            data($dataset/name[@language = $language]/@selectorName)
        else data($dataset/name[1])
        }
        </displayText>
    </dataset>

let $conceptList            :=
    for $concept in $dataset//concept[not(@absent | ancestor::conceptList)]
    return
        <concept json:array="true">
        {
            $concept/@*,
            if ($concept[name]) then (
                let $name   := if ($concept/name[@language=$language]) then $concept/name[@language=$language][1] else ($concept/name[1])
                return
                    <name>{$name/@language, string-join(for $c in $concept/ancestor::concept return '&#160;&#160;&#160;&#160;',''), $name/node()}</name>
            )
            else (
                let $orig   := art:getOriginalForConcept($concept)
                let $name   := if ($orig/name[@language=$language]) then $orig/name[@language=$language][1] else ($orig/name[1])
                return
                    <name>{$name/@language, string-join(for $c in $concept/ancestor::concept return '&#160;&#160;&#160;&#160;',''), $name/node()}</name>
            ),
            <parent>{
                if ($concept/../name) then (
                    if ($concept/../name[@language=$language]) then $concept/../name[@language=$language][1] else ($concept/../name[1])
                )
                else (
                    let $orig := art:getOriginalConceptName($concept/../inherit)
                    return
                        if ($orig/name[@language=$language]) then $orig/name[@language=$language][1] else ($orig/name[1])
                )
            }</parent>
        }
        </concept>

let $projectValueSets       :=
    for $valuesetName in distinct-values($decor/terminology//valueSet/@name)
    let $valuesetsByRef :=
        for $t in $decor//valueSet[@name=$valuesetName][@ref]
        let $ref := $t/@ref
        group by $ref
        return
            vs:getValueSetById($ref[1], (), $decor/project/@prefix, false())//valueSet[@effectiveDate]

    order by $valuesetName ascending
    return
        for $valueset in ($decor//valueSet[@name=$valuesetName][@id] | $valuesetsByRef)
        order by $valueset/@effectiveDate descending
        return
        <valueSet json:array="true">
            <text>{concat('"', $valuesetName, '[', $valueset/@id, ']" flexibility="', $valueset/@effectiveDate, '"')}</text>
            <displayText>{concat($valuesetName, " (", $valueset/@effectiveDate, ")")}</displayText>
        </valueSet>

let $projectTemplates       :=
    for $templateName in distinct-values($decor//template/@name)
    let $templatesByRef :=
        for $t in $decor//template[@name=$templateName][@ref]
        let $ref := $t/@ref
        group by $ref
        return
            templ:getTemplateById($ref[1], (), $decor/project/@prefix)//template[@effectiveDate]

    order by $templateName ascending
    return
        for $template in ($decor//template[@name=$templateName][@id] | $templatesByRef)
        order by $template/@effectiveDate descending
        return
            <template json:array="true">
                <text>{concat('"', $templateName, '[', $template/@id, ']" flexibility="', $template/@effectiveDate, '"')}</text>
                <displayText>{concat($templateName, " (", $template/@effectiveDate, ")")}</displayText>
            </template>

let $projectConcepts        :=
    for $concept in $conceptList
    (:order by $concept/name ascending, $concept/@effectiveDate descending:)
    return
        <concept json:array="true">
            <text>{concat('"', art:shortName($concept/name[1]), '[', $concept/@id/string(), ']" effectiveDate="', $concept/@effectiveDate/string(), '"')}</text>
            <displayText>{concat(replace($concept/name[1], '"', '\\"'), " (", $concept/@effectiveDate/string(), ") -- ", art:shortName($concept/parent/name[1]))}</displayText>
        </concept>

let $projectIds             :=
    for $id in $decor//ids/id[designation[@language=$language]]
    order by $id/designation[1]/@displayName ascending
    return
        <projectId json:array="true">
            <text>"{$id/@root/string()}"</text>
            <displayText>{$id/designation[@language=$language][1]/@displayName/string()}</displayText>
        </projectId>

let $projectCodeSystems     :=
    for $id in distinct-values(($decor//normalize-space(@codeSystem),'2.16.840.1.113883.6.1','2.16.840.1.113883.6.96'))
    let $displayName    := replace(art:getNameForOID($id, $language, $prefix),'''','')
    order by $displayName ascending
    return
        if (string-length($id) = 0) then () else (
            <projectCodeSystems json:array="true">
                <text>{concat('"', $displayName, '[', $id, ']"')}</text>
                <displayText>{concat($displayName, " -- ", $id)}</displayText>
            </projectCodeSystems>
        )

let $baseIds                :=
    for $baseId in $decor/ids/baseId[@type='TM']
    let $defaultBaseId := $baseId/../defaultBaseId[@id=$baseId/@id] | $baseId[@default='true']
    let $defaultText   := if ($defaultBaseId) then ' (default)' else ()
    return
        <baseId json:array="true">
            <text>"{concat($baseId/@id, '.new')}"</text>
            <displayText>{$baseId/@id/string()}</displayText>
        </baseId>

let $effectiveDates         :=
    (
    <effectiveDate>
        <text>"{substring(xs:string(current-dateTime()), 1, 19)}"</text>
        <displayText>now</displayText>
    </effectiveDate>
    ,
    <effectiveDate>
        <text>"{concat(substring(xs:string(current-dateTime()), 1, 11), '00:00:00')}"</text>
        <displayText>today</displayText>
    </effectiveDate>
    )

let $elementIds             :=
    if ($id='') then '' else for $num in (1 to 9)
    return concat('"', $id, '.', $num, '"')

return (
    if ($format = 'json') then (
        if (response:exists()) then response:set-header('Content-Type', 'application/json') else (),
        '{ ', $nl,
        '"projectDatasets" : [', 
            for $el at $pos in $projectDataSets 
            return (if ($pos > 1) then ', ' else (), local:toJs($el))
        , '], '
        , $nl, 
        '"projectConcepts" : [',  
            for $el at $pos in $projectConcepts 
            return (if ($pos > 1) then ', ' else (), local:toJs($el))
        , '], '
        , $nl, 
        '"projectTemplates" : [',
            for $el at $pos in $projectTemplates
            return (if ($pos > 1) then ', ' else (), local:toJs($el))
        , '], '
        , $nl,
        '"projectValuesets" : [',
            for $el at $pos in $projectValueSets
            return (if ($pos > 1) then ', ' else (), local:toJs($el))
        , '], '
        , $nl,
        '"baseIds" : [',
            for $el at $pos in $baseIds
            return (if ($pos > 1) then ', ' else (), local:toJs($el))
        , '], '
        , $nl,
        '"projectIds" : [',
            for $el at $pos in $projectIds
            return (if ($pos > 1) then ', ' else (), local:toJs($el))
        , '], '
        , $nl,
        '"elementIds" : [',
            string-join($elementIds, ",")
        , '], '
        , $nl,
        '"effectiveDates" : [',
            for $el at $pos in $effectiveDates
            return (if ($pos > 1) then ', ' else (), local:toJs($el))
        , '], '
        , $nl,
         '"projectCodeSystems" : [',
            for $el at $pos in $projectCodeSystems
            return (if ($pos > 1) then ', ' else (), local:toJs($el))
        , '] '
        , $nl,
        '}'
    )
    else (
        'var projectConcepts = [',
            for $el at $pos in $projectConcepts
            return (if ($pos > 1) then ', ' else (), local:toJson($el))
         , ']; '
         ,
        'var projectTemplates = [',
            for $el at $pos in $projectTemplates
            return (if ($pos > 1) then ', ' else (), local:toJson($el))
         , ']; '
         ,
        'var projectValuesets = [',
            for $el at $pos in $projectValueSets
            return (if ($pos > 1) then ', ' else (), local:toJson($el))
         , ']; '
         ,
        'var baseIds = [',
            for $el at $pos in $baseIds
            return (if ($pos > 1) then ', ' else (), local:toJson($el))
         , ']; '
         ,
        'var projectIds = [',
            for $el at $pos in $projectIds
            return (if ($pos > 1) then ', ' else (), local:toJson($el))
         , ']; '
         ,
        'var elementIds = [',
            string-join($elementIds, ",")
         , ']; '
         ,
        'var effectiveDates = [',
            for $el at $pos in $effectiveDates
            return (if ($pos > 1) then ', ' else (), local:toJson($el))
         , ']; '
         ,
         'var projectCodeSystems = [',
            for $el at $pos in $projectCodeSystems
            return (if ($pos > 1) then ', ' else (), local:toJson($el))
         , ']; '
     )
 )
