xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace temple  = "http://art-decor.org/ns/temple" at "temple.xqm";
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace art     = "http://art-decor.org/ns/art" at "../../art/modules/art-decor.xqm";
import module namespace decor   = "http://art-decor.org/ns/decor" at "../../art/api/api-decor.xqm";
import module namespace templ   = "http://art-decor.org/ns/decor/valueset" at "../../art/api/api-decor-valueset.xqm";
import module namespace hist    = "http://art-decor.org/ns/decor/history" at "../../art/api/api-decor-history.xqm";
declare namespace request       = "http://exist-db.org/xquery/request";
declare namespace response      = "http://exist-db.org/xquery/response";
declare namespace util          = "http://exist-db.org/xquery/util";
declare namespace validation    = "http://exist-db.org/xquery/validation";

(: You would say that saving this pseudo project to the db and then validate it is a lot of redudant work
   Why not in memory you ask? Well put simply: as soon the valueSet contains new content in a 'different'
   namespace like hl7nl: or pharm: we get validation errors like this:
   
   Input: <element name="hl7:numerator" datatype="hl7nl:PQ" ...
   
   Temple validation error, code: NotSchemaValid, description: invalid 82 UndeclaredPrefix: Cannot resolve 
   'hl7nl:PQ' as a QName: the prefix 'hl7nl' is not declared.cvc-attribute.3: The value 'hl7nl:PQ' of 
   attribute 'datatype' on element 'element' is not valid with respect to its type, 'QName'.
   
   When we save to the db, and validate from there, this does not occur. So: this is a pragmatic workaround
   for an unexplained phenomenon.
:)
declare %private function local:validateSchema($decor as element(), $newTerminology as element(terminology)) as node() {
    let $res            := concat(util:uuid(),'.xml')
    let $tempProject    :=
        <decor>
        {
            for $ns at $i in art:getDecorNamespaces($decor)
            let $ns-uri     := $ns/@uri/string()
            let $ns-prefix  := $ns/@prefix/string()
            return
                attribute {QName($ns-uri,concat($ns-prefix,':dummy-',$i))} {$ns-uri}
        }
            <project>
            {
                $decor/project/@*
            }
                <name/>
                <copyright years=""/>
            </project>
            <datasets/>
            <scenarios>
                <actors/>
            </scenarios>
            <ids/>
            <terminology>
            {
                $newTerminology/node()
            }
            </terminology>
            <rules/>
            <issues/>
        </decor>
    let $store          := xmldb:store($temple:strUserTemp, $res, $tempProject)
    
    (: Using the schema at 'https://assets.art-decor.org/ADAR/rv/DECOR.xsd' throws a SAX error, using the local schema does not. :)
    let $result         := validation:jaxv-report(doc($store), $get:docDecorSchema)
    
    let $delete         := xmldb:remove($temple:strUserTemp, $res)
    
    return
        $result
};

declare %private function local:saveValueset($prefix as xs:string, $newTerminology as element(terminology), $reportOnly as xs:string) as element(){
    let $decor              := art:getDecorByPrefix($prefix)
    let $user               := get:strCurrentUserName()
    let $isAuthor           := decor:authorCanEditP($decor, $decor:SECTION-TERMINOLOGY)
    let $assert             := 
        if ($isAuthor) then () else (
            error(QName('http://art-decor.org/ns/error', 'YouAreNoAuthor'), concat('User ', $user, ' is not an author in this project'))
        )
    let $logfile            := doc('/db/apps/temple/xml/log.xml')
    let $logon              := false()
    let $assert             := 
        if (count($newTerminology/valueSet[@id][@effectiveDate]) = 1 ) then () else (
            error(QName('http://art-decor.org/ns/error', 'MissingValueSetWithIdAndEffectiveDate'), 'Terminology must contain exactly one valueSet with @id and @effectiveDate')
        )
    
    let $assert             := 
        if ($newTerminology/valueSet[@ref]) then (
            error(QName('http://art-decor.org/ns/error', 'NoValueSetWithRef'), 'Terminology must not contain valueSet(s) with @ref')
        ) else ()
    let $assert             := 
        if ($newTerminology/valueSet//exclude[@codeSystem][empty(@code)]) then (
            error(QName('http://art-decor.org/ns/error', 'NoValueSetWithCompleteCodeSystemExclude'), 'Value set with exclude on complete code system not supported. Exclude SHALL have both @code and @codeSystem')
        ) else ()
    let $assert             := 
        if ($newTerminology/valueSet//exclude[@ref]) then (
            error(QName('http://art-decor.org/ns/error', 'NoValueSetWithCompleteCodeSystemExclude'), 'Value set with exclude on value set not supported. Exclude SHALL have both @code and @codeSystem')
        ) else ()
    let $css                := $newTerminology/valueSet/conceptList/include[@codeSystem][empty(@code)]/replace(@codeSystem, '[^\d\.]', '')
    let $assert             := 
        if (count($css) = count(distinct-values($css))) then () else (
            error(QName('http://art-decor.org/ns/error', 'CompleteCodeSystemIncludeUnique'), 'Value set can only contain 1 include per complete code system.')
        )
    let $css                := $newTerminology/valueSet/conceptList/*[@ref]/replace(@ref, '[^\d\.]', '')
    let $assert             := 
        if (count($css) = count(distinct-values($css))) then () else (
            error(QName('http://art-decor.org/ns/error', 'ValueSetIncludeUnique'), 'Value set can only contain 1 include/exclude per value set regardless of its version.')
        )
    (:let $assert         := 
        for $vs in $newTerminology/terminologyAssociation[@valueSet]
        return 
            if ($vs/@valueSet = $newTerminology/valueSet/@id) then () else (
                error(QName('http://art-decor.org/ns/error', 'TerminologyAssociationShallMatchValueSetId'), concat('TerminologyAssociation for concept ', $vs/@conceptId, ' with valueSet ',$vs/@valueSet,' SHALL valueSet/@id in this set'))
            ):)
    (:let $oldTermAssociation     := $decor//terminologyAssociation[@valueSet=$newId][@flexibility=$newEffectiveDate]:)
    
    (: although it looks as if we could support multiple valueSets like this, we cannot because of the redirect at the very end to exactly 1 valueSet :)
    let $newTerminology     :=
        <terminology>
        {
            $newTerminology/@*
        }
        {
            for $valueSet in $newTerminology/valueSet
            let $newId                  := $valueSet/@id
            let $newEffectiveDate       := $valueSet/@effectiveDate
            let $newStatus              := $valueSet/@statusCode
            let $oldValueset            := $decor//valueSet[@id=$newId][@effectiveDate=$newEffectiveDate]
            
            (: Check status :)
            let $statusAllowed          := if ($oldValueset) then art:isStatusChangeAllowable($oldValueset, $newStatus) else (true())
            let $assert                 :=  
                if ($statusAllowed) then () else (
                    error(QName('http://art-decor.org/ns/error', 'StatusChangeNotAllowed'), concat('Setting status from ', $oldValueset/@statusCode, ' to ', $newStatus, ' is not allowed'))
                )
            
            (: Make a new valueSet when submitted one has id 'new', or submitted id exists but submitted effectiveDate doesn't :)
            let $makeNewValueset        :=
                (: valueSet with id and effectiveDate exists :)
                if ($oldValueset) then false()
                (: A new valueSet, new id must be issued :)
                else if ($newId[ends-with(.,'new')]) then true()
                (: valueSet with this id exists, but not this effectiveDate :)
                else if ($decor//valueSet[@id=$newId]) then not($oldValueset)
                (: Id does not exist and != 'new :)
                else error(QName('http://art-decor.org/ns/error', 'IdNotValid'), concat('valueSet/@id must either exist or be ''{{some baseId}}.new''. Found ', $newId))
            let $makeNewVersion         :=
                if ($oldValueset) then false() else if ($decor//valueSet[@id=$newId]) then true() else false()
            
            let $assert         :=  
                if (not($makeNewValueset and $decor//valueSet[@name=$valueSet/@name][@effectiveDate=$newEffectiveDate])) then () else (
                    error(QName('http://art-decor.org/ns/error', 'ValueSetByNameAndDateExists'), concat('A valueSet with this name=''',$valueSet/@name,''' and effectiveDate=''',$newEffectiveDate,''' already exists.'))
                )
            
            return
                if ($makeNewValueset) then (
                    (: Issue new id :)
                    (: MdG Note to self: Code copied from create-decor-valueSet.xquery. I cannot use the entire xquery, code like 
                    this - and more - should be refactored into valueSets api :)
                    let $valueSetRoot   := 
                        if ($makeNewVersion)
                        then ($newId) else
                        if (temple:isOid(replace($newId,'.?new',''))) 
                        then (replace($newId,'.?new',''))
                        else (decor:getDefaultBaseIdsP($decor, $decor:OBJECTTYPE-VALUESET)/@id)
                    
                    (:generate new id if mode is 'new' or 'adapt', else use existing id:)
                    let $newValueSetId  := 
                        if ($makeNewVersion) then
                            $newId
                        else (
                            let $precedingNewlings      :=
                                count(
                                    for $vs in $valueSet/preceding-sibling::valueSet
                                    let $i  := $vs/@id
                                    let $e  := $vs/@effectiveDate
                                    let $d  := $decor//valueSet[@id=$i]
                                    return $i[ends-with(.,'new')] or ($d and not($d[@effectiveDate = $e]))
                                )
                            let $valueSetIdsAll         := 
                                $decor//@id[matches(.,concat('^',$valueSetRoot,'\.\d+$'))]
                            return
                            if ($valueSetIdsAll) then
                                concat($valueSetRoot,'.',max($valueSetIdsAll/xs:integer(tokenize(.,'\.')[last()])) + 1 + $precedingNewlings)
                            else (
                                concat($valueSetRoot,'.', 1 + $precedingNewlings)
                            )
                        )
                    let $baseValueset   := 
                        <valueSet>
                        {
                            attribute id {$newValueSetId},
                            $valueSet/@name[string-length()>0],
                            $valueSet/@displayName[string-length()>0],
                            attribute effectiveDate {$newEffectiveDate},
                            $valueSet/@statusCode[string-length()>0],
                            $valueSet/@versionLabel[string-length()>0],
                            $valueSet/@expirationDate[string-length()>0],
                            $valueSet/@officialReleaseDate[string-length()>0],
                            $valueSet/@experimental[string-length()>0],
                            $valueSet/@canonicalUri[string-length()>0]
                        }
                        </valueSet>
                    let $preparedValueSet   := art:prepareValueSetForUpdate($valueSet, $baseValueset)
                    
                    let $assert             := 
                        if (matches($preparedValueSet/@id, '^[0-2](\.(0|[1-9][0-9]*)){3,}$')) then () else (
                            error(QName('http://art-decor.org/ns/error', 'ValueSetIdInvalid'), 'Value set ' || $preparedValueSet/@id || ' pattern SHALL be [0-2](\.(0|[1-9][0-9]*))*. An OID identifies exactly one object and SHALL NOT be reused. OIDs of 3 nodes or less are always reserved for a purpose. See https://oid-info.com or this servers'' OID index when in doubt..')
                        )
                    let $assert             := 
                        if ($preparedValueSet[@id = .//include/@ref]) then 
                            error(QName('http://art-decor.org/ns/error', 'ValueSetIncludeCircular'), 'Value set ' || $preparedValueSet/@id || ' SHALL NOT include itself.')
                        else ()
                    
                    return
                        $preparedValueSet
                )
                else (
                    let $baseValueset       := 
                        <valueSet>
                        {
                            $valueSet/@id[string-length()>0],
                            $valueSet/@name[string-length()>0],
                            $valueSet/@displayName[string-length()>0],
                            $valueSet/@effectiveDate[string-length()>0],
                            $valueSet/@statusCode[string-length()>0],
                            $valueSet/@versionLabel[string-length()>0],
                            $valueSet/@expirationDate[string-length()>0],
                            $valueSet/@officialReleaseDate[string-length()>0],
                            $valueSet/@experimental[string-length()>0],
                            $valueSet/@canonicalUri[string-length()>0]
                        }
                        </valueSet>
                    let $preparedValueSet   := art:prepareValueSetForUpdate($valueSet, $baseValueset)
                    
                    return
                        $preparedValueSet
                )
        }
        </terminology>
    
    let $newTerminology         := temple:removeNamesFromTerminologyOids($newTerminology)
    
    (: Validate against decor.xsd :)
    let $schemaResults          := local:validateSchema($decor, $newTerminology)
    let $assert                 :=  
        if (not($schemaResults//status="invalid")) then () else (
            error(QName('http://art-decor.org/ns/error', 'NotSchemaValid'), $schemaResults)
        )
    let $assert                 := 
        if ($newTerminology/valueSet[@id = .//include/@ref]) then 
            error(QName('http://art-decor.org/ns/error', 'ValueSetIncludeCircular'), 'Value set ' || $newTerminology/valueSet/@id || ' SHALL NOT include itself.')
        else ()
    
    (: Block updates if an error occurred :)
    let $update                 := 
        if ($reportOnly = 'true') then (<ok/>) else (
            (:if ($logon) then 
                update insert 
                    <update user="{$user}" time="{fn:current-dateTime()}">
                        <old><terminology>{$oldValueset}</terminology></old>
                        <new>{$newTerminology}</new>
                    </update>
                into $logfile/logroot 
            else <nothing/>
            ,:)
            let $update             :=
                for $valueSet in $newTerminology/valueSet
                let $newId              := $valueSet/@id
                let $newEffectiveDate   := $valueSet/@effectiveDate
                let $existingValueset   := $decor//valueSet[@id=$newId][@effectiveDate=$newEffectiveDate]
                (: now update the value set :)
                return
                    if ($existingValueset) then (
                        (: ======= do history first ======= :)
                        let $intention          :=
                            if ($existingValueset[@statusCode='final'][@effectiveDate = $valueSet/@effectiveDate]) then 'patch' else 'version'
                        let $history            :=
                            if ($existingValueset) then hist:AddHistory ($decor:OBJECTTYPE-VALUESET, $prefix, $intention, $existingValueset) else ()
                        return
                            update replace $existingValueset with $valueSet
                    )
                    else (
                        update insert $newTerminology/valueSet into $decor/terminology
                    )
                
            return <ok/>
        )
    return $newTerminology
};

let $prefix         := if (request:exists()) then request:get-parameter('prefix', '') else 'demo1-'
let $language       := if (request:exists()) then request:get-parameter('language', '') else 'nl-NL'
let $code           := if (request:exists()) then request:get-parameter('code', '') else '&lt;terminology&gt;&lt;!-- Create an @id derived from a baseId to create a new valueSet --&gt;
    &lt;valueSet name="meting_door2" displayName="Meting door2" id="2.16.840.1.113883.3.1937.99.62.3.11.7" effectiveDate="2017-03-21T16:43:26" statusCode="draft"&gt;
        &lt;conceptList&gt;
            &lt;concept code="P" codeSystem="no_concept_in_project[1.2.3]" displayName="Patient" level="0" type="L"/&gt;
            &lt;concept code="H" codeSystem="no_concept_in_project[1.2.3]" displayName="Home care provider" level="0" type="L"/&gt;
            &lt;concept code="GP" codeSystem="no_concept_in_project[1.2.3]" displayName="GP" level="0" type="L"/&gt;
            &lt;concept code="PN" codeSystem="no_concept_in_project[1.2.3]" displayName="Personal network" level="0" type="L"/&gt;
        &lt;/conceptList&gt;
    &lt;/valueSet&gt;
&lt;/terminology&gt;'
let $reportOnly     := if (request:exists()) then request:get-parameter('reportOnly', 'false') else 'true'
let $newTerminology :=  
    try {fn:parse-xml($code)/*}
    catch * {
        <templeError at="{current-dateTime()}">{concat('Temple parsing error, code: ', $err:code, ', description: ', $err:description, ' value: ',$err:value,' module: ',$err:module,' (',$err:line-number,' ',$err:column-number,')')}</templeError>
    }

(: When there is a parsing error, save and validate. If there is a schema validation error, validate again to catch output, else return the error as is. :)
let $result         := 
    if ($newTerminology[self::templeError]) then $newTerminology else (
        try {local:saveValueset($prefix, $newTerminology, $reportOnly)}
        catch * {
            <templeError>{concat('Temple validation error, code: ', $err:code, ', description: ', $err:description)}</templeError>
        }
    )
    
let $decor  := art:getDecorByPrefix($prefix)
let $language := if (request:exists()) then request:get-parameter('language', $get:strArtLanguage) else ''
let $result := temple:addNamesToTerminologyOids($result, $decor, $language)

let $dummy := 
    if (response:exists()) then ( 
        if ($result//status[.='invalid'] | $result[self::templeError]) 
        then (response:set-status-code(400))
        else (response:set-status-code(200))
    ) else ()
    
return $result