xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace temple  = "http://art-decor.org/ns/temple" at "temple.xqm";
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace art     = "http://art-decor.org/ns/art" at "../../art/modules/art-decor.xqm";
import module namespace decor   = "http://art-decor.org/ns/decor" at "../../art/api/api-decor.xqm";
import module namespace templ   = "http://art-decor.org/ns/decor/codesystem" at "../../art/api/api-decor-codesystem.xqm";
import module namespace hist    = "http://art-decor.org/ns/decor/history" at "../../art/api/api-decor-history.xqm";
declare namespace request       = "http://exist-db.org/xquery/request";
declare namespace response      = "http://exist-db.org/xquery/response";
declare namespace util          = "http://exist-db.org/xquery/util";
declare namespace validation    = "http://exist-db.org/xquery/validation";

(: You would say that saving this pseudo project to the db and then validate it is a lot of redudant work
   Why not in memory you ask? Well put simply: as soon the codeSystem contains new content in a 'different'
   namespace like hl7nl: or pharm: we get validation errors like this:
   
   Input: <element name="hl7:numerator" datatype="hl7nl:PQ" ...
   
   Temple validation error, code: NotSchemaValid, description: invalid 82 UndeclaredPrefix: Cannot resolve 
   'hl7nl:PQ' as a QName: the prefix 'hl7nl' is not declared.cvc-attribute.3: The value 'hl7nl:PQ' of 
   attribute 'datatype' on element 'element' is not valid with respect to its type, 'QName'.
   
   When we save to the db, and validate from there, this does not occur. So: this is a pragmatic workaround
   for an unexplained phenomenon.
:)
declare %private function local:validateSchema($decor as element(), $newTerminology as element(terminology)) as node() {
    let $res            := concat(util:uuid(),'.xml')
    let $tempProject    :=
        <decor>
        {
            for $ns at $i in art:getDecorNamespaces($decor)
            let $ns-uri     := $ns/@uri/string()
            let $ns-prefix  := $ns/@prefix/string()
            return
                attribute {QName($ns-uri,concat($ns-prefix,':dummy-',$i))} {$ns-uri}
        }
            <project>
            {
                $decor/project/@*
            }
                <name/>
                <copyright years=""/>
            </project>
            <datasets/>
            <scenarios>
                <actors/>
            </scenarios>
            <ids/>
            <terminology>
            {
                $newTerminology/node()
            }
            </terminology>
            <rules/>
            <issues/>
        </decor>
    let $store          := xmldb:store($temple:strUserTemp, $res, $tempProject)
    
    (: Using the schema at 'https://assets.art-decor.org/ADAR/rv/DECOR.xsd' throws a SAX error, using the local schema does not. :)
    let $result         := validation:jaxv-report(doc($store), $get:docDecorSchema)
    
    let $delete         := xmldb:remove($temple:strUserTemp, $res)
    
    return
        $result
};

declare %private function local:saveCodesystem($prefix as xs:string, $newTerminology as element(terminology), $reportOnly as xs:string) as element(){
    let $decor              := art:getDecorByPrefix($prefix)
    let $user               := get:strCurrentUserName()
    let $isAuthor           := decor:authorCanEditP($decor, $decor:SECTION-TERMINOLOGY)
    let $assert             := 
        if ($isAuthor) then () else (
            error(QName('http://art-decor.org/ns/error', 'YouAreNoAuthor'), concat('User ', $user, ' is not an author in this project'))
        )
    let $logfile            := doc('/db/apps/temple/xml/log.xml')
    let $logon              := false()
    let $assert             := 
        if (count($newTerminology/codeSystem[@id][@effectiveDate]) = 1 ) then () else (
            error(QName('http://art-decor.org/ns/error', 'MissingCodeSystemWithIdAndEffectiveDate'), 'Terminology must contain exactly one codeSystem with @id and @effectiveDate')
        )
    let $assert             := 
        if ($newTerminology/codeSystem[@ref]) then (
            error(QName('http://art-decor.org/ns/error', 'NoCodeSystemWithRef'), 'Terminology must not contain exactly codeSystem(s) with @ref')
        ) else ()
    (:let $assert         := 
        for $vs in $newTerminology/terminologyAssociation[@codeSystem]
        return 
            if ($vs/@codeSystem = $newTerminology/codeSystem/@id) then () else (
                error(QName('http://art-decor.org/ns/error', 'TerminologyAssociationShallMatchCodeSystemId'), concat('TerminologyAssociation for concept ', $vs/@conceptId, ' with codeSystem ',$vs/@codeSystem,' SHALL codeSystem/@id in this set'))
            ):)
    (:let $oldTermAssociation     := $decor//terminologyAssociation[@codeSystem=$newId][@flexibility=$newEffectiveDate]:)
    
    (: although it looks as if we could support multiple codeSystems like this, we cannot because of the redirect at the very end to exactly 1 codeSystem :)
    let $newTerminology     :=
        <terminology>
        {
            $newTerminology/@*
        }
        {
            for $codeSystem in $newTerminology/codeSystem
            let $newId                  := $codeSystem/@id
            let $newEffectiveDate       := $codeSystem/@effectiveDate
            let $newStatus              := $codeSystem/@statusCode
            let $oldCodesystem            := $decor//codeSystem[@id=$newId][@effectiveDate=$newEffectiveDate]
            
            (: Check status :)
            let $statusAllowed          := if ($oldCodesystem) then art:isStatusChangeAllowable($oldCodesystem, $newStatus) else (true())
            let $assert                 :=  
                if ($statusAllowed) then () else (
                    error(QName('http://art-decor.org/ns/error', 'StatusChangeNotAllowed'), concat('Setting status from ', $oldCodesystem/@statusCode, ' to ', $newStatus, ' is not allowed'))
                )
            
            (: Make a new codeSystem when submitted one has id 'new', or submitted id exists but submitted effectiveDate doesn't :)
            let $makeNewCodesystem        :=
                (: codeSystem with id and effectiveDate exists :)
                if ($oldCodesystem) then false()
                (: A new codeSystem, new id must be issued :)
                else if ($newId[ends-with(.,'new')]) then true()
                (: codeSystem with this id exists, but not this effectiveDate :)
                else if ($decor//codeSystem[@id=$newId]) then not($oldCodesystem)
                (: Id does not exist and != 'new :)
                else error(QName('http://art-decor.org/ns/error', 'IdNotValid'), concat('codeSystem/@id must either exist or be ''{{some baseId}}.new''. Found ', $newId))
            let $makeNewVersion         :=
                if ($oldCodesystem) then false() else if ($decor//codeSystem[@id=$newId]) then true() else false()
            
            let $assert         :=  
                if (not($makeNewCodesystem and $decor//codeSystem[@name=$codeSystem/@name][@effectiveDate=$newEffectiveDate])) then () else (
                    error(QName('http://art-decor.org/ns/error', 'CodeSystemByNameAndDateExists'), concat('A codeSystem with this name=''',$codeSystem/@name,''' and effectiveDate=''',$newEffectiveDate,''' already exists.'))
                )
            
            return
                if ($makeNewCodesystem) then (
                    (: Issue new id :)
                    (: MdG Note to self: Code copied from create-decor-codeSystem.xquery. I cannot use the entire xquery, code like 
                    this - and more - should be refactored into codeSystems api :)
                    let $codeSystemRoot   := 
                        if ($makeNewVersion)
                        then ($newId) else
                        if (temple:isOid(replace($newId,'.?new',''))) 
                        then (replace($newId,'.?new',''))
                        else (decor:getDefaultBaseIdsP($decor, $decor:OBJECTTYPE-CODESYSTEM)/@id)
                    
                    (:generate new id if mode is 'new' or 'adapt', else use existing id:)
                    let $newCodeSystemId  := 
                        if ($makeNewVersion) then
                            $newId
                        else (
                            let $precedingNewlings      :=
                                count(
                                    for $vs in $codeSystem/preceding-sibling::codeSystem
                                    let $i  := $vs/@id
                                    let $e  := $vs/@effectiveDate
                                    let $d  := $decor//codeSystem[@id=$i]
                                    return $i[ends-with(.,'new')] or ($d and not($d[@effectiveDate = $e]))
                                )
                            let $codeSystemIdsAll         := 
                                $decor//@id[matches(.,concat('^',$codeSystemRoot,'\.\d+$'))]
                            return
                            if ($codeSystemIdsAll) then
                                concat($codeSystemRoot,'.',max($codeSystemIdsAll/xs:integer(tokenize(.,'\.')[last()])) + 1 + $precedingNewlings)
                            else (
                                concat($codeSystemRoot,'.', 1 + $precedingNewlings)
                            )
                        )
                        
                    let $assert             := 
                        if (matches($newCodeSystemId, '^[0-2](\.(0|[1-9][0-9]*)){3,}$')) then () else (
                            error(QName('http://art-decor.org/ns/error', 'CodeSystemIdInvalid'), 'ConceptMap ' || $newCodeSystemId || ' pattern SHALL be [0-2](\.(0|[1-9][0-9]*))*. An OID identifies exactly one object and SHALL NOT be reused. OIDs of 3 nodes or less are always reserved for a purpose. See https://oid-info.com or this servers'' OID index when in doubt..')
                        )
                        
                    let $baseCodesystem   := 
                        <codeSystem>
                        {
                            attribute id {$newCodeSystemId},
                            $codeSystem/@name[string-length()>0],
                            $codeSystem/@displayName[string-length()>0],
                            attribute effectiveDate {$newEffectiveDate},
                            $codeSystem/@statusCode[string-length()>0],
                            $codeSystem/@versionLabel[string-length()>0],
                            $codeSystem/@expirationDate[string-length()>0],
                            $codeSystem/@officialReleaseDate[string-length()>0],
                            $codeSystem/@experimental[string-length()>0],
                            $codeSystem/@caseSensitive[string-length()>0],
                            $codeSystem/@canonicalUri[string-length()>0]
                        }
                        </codeSystem>
                    return
                        art:prepareCodeSystemForUpdate($codeSystem, $baseCodesystem)
                )
                else (
                    let $baseCodesystem   := 
                        <codeSystem>
                        {
                            $codeSystem/@id[string-length()>0],
                            $codeSystem/@name[string-length()>0],
                            $codeSystem/@displayName[string-length()>0],
                            $codeSystem/@effectiveDate[string-length()>0],
                            $codeSystem/@statusCode[string-length()>0],
                            $codeSystem/@versionLabel[string-length()>0],
                            $codeSystem/@expirationDate[string-length()>0],
                            $codeSystem/@officialReleaseDate[string-length()>0],
                            $codeSystem/@experimental[string-length()>0],
                            $codeSystem/@caseSensitive[string-length()>0],
                            $codeSystem/@canonicalUri[string-length()>0]
                        }
                        </codeSystem>
                    return
                        art:prepareCodeSystemForUpdate($codeSystem, $baseCodesystem)
                )
        }
        </terminology>
    
    let $newTerminology         := temple:removeNamesFromTerminologyOids($newTerminology)
    
    (: Validate against decor.xsd :)
    let $schemaResults          := local:validateSchema($decor, $newTerminology)
    let $assert                 :=  
        if (not($schemaResults//status="invalid")) then () else (
            error(QName('http://art-decor.org/ns/error', 'NotSchemaValid'), $schemaResults)
        )
    
    (: Block updates if an error occurred :)
    let $update                 := 
        if ($reportOnly = 'true') then (<ok/>) else (
            (:if ($logon) then 
                update insert 
                    <update user="{$user}" time="{fn:current-dateTime()}">
                        <old><terminology>{$oldCodesystem}</terminology></old>
                        <new>{$newTerminology}</new>
                    </update>
                into $logfile/logroot 
            else <nothing/>
            ,:)
            let $update             :=
                for $codeSystem in $newTerminology/codeSystem
                let $newId              := $codeSystem/@id
                let $newEffectiveDate   := $codeSystem/@effectiveDate
                let $existingCodesystem := $decor//codeSystem[@id=$newId][@effectiveDate=$newEffectiveDate]
                (: now update the value set :)
                return
                    if ($existingCodesystem) then (
                        (: ======= do history first ======= :)
                        let $intention          :=
                            if ($existingCodesystem[@statusCode='final'][@effectiveDate = $codeSystem/@effectiveDate]) then 'patch' else 'version'
                        let $history            :=
                            if ($existingCodesystem) then hist:AddHistory ($decor:OBJECTTYPE-CODESYSTEM, $prefix, $intention, $existingCodesystem) else ()
                        return
                            update replace $existingCodesystem with $codeSystem
                    )
                    else
                    if ($decor/terminology/valueSet) then 
                        update insert $newTerminology/codeSystem preceding $decor/terminology/valueSet[1]
                    else
                    if ($decor/terminology/terminologyAssociation) then 
                        update insert $newTerminology/codeSystem following $decor/terminology/terminologyAssociation[last()]
                    else (
                        update insert $newTerminology/codeSystem into $decor/terminology
                    )
                
            return <ok/>
        )
    return $newTerminology
};

let $prefix         := if (request:exists()) then request:get-parameter('prefix', '') else 'demo1-'
let $language       := if (request:exists()) then request:get-parameter('language', '') else 'nl-NL'
let $code           := if (request:exists()) then request:get-parameter('code', '') else ''
let $reportOnly     := if (request:exists()) then request:get-parameter('reportOnly', 'false') else 'true'
let $newTerminology :=  
    try {fn:parse-xml($code)/*}
    catch * {
        <templeError at="{current-dateTime()}">{concat('Temple parsing error, code: ', $err:code, ', description: ', $err:description, ' value: ',$err:value,' module: ',$err:module,' (',$err:line-number,' ',$err:column-number,')')}</templeError>
    }

(: When there is a parsing error, save and validate. If there is a schema validation error, validate again to catch output, else return the error as is. :)
let $result         := 
    if ($newTerminology[self::templeError]) then $newTerminology else (
        try {local:saveCodesystem($prefix, $newTerminology, $reportOnly)}
        catch * {
            <templeError>{concat('Temple validation error, code: ', $err:code, ', description: ', $err:description)}</templeError>
        }
    )
    
let $decor  := art:getDecorByPrefix($prefix)
let $language := if (request:exists()) then request:get-parameter('language', $get:strArtLanguage) else ''
let $result := temple:addNamesToTerminologyOids($result, $decor, $language)

let $dummy := 
    if (response:exists()) then ( 
        if ($result//status[.='invalid'] | $result[self::templeError]) 
        then (response:set-status-code(400)) 
        else (response:set-status-code(200))
    ) else ()
    
return $result