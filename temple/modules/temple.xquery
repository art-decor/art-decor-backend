xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace temple      = "http://art-decor.org/ns/temple" at "temple.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at "../../art/modules/art-decor.xqm";
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace aduser      = "http://art-decor.org/ns/art-decor-users" at "../../art/api/api-user-settings.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../art/api/api-server-settings.xqm";
import module namespace decor       = "http://art-decor.org/ns/decor" at "../../art/api/api-decor.xqm";

declare function local:patchTemplate($nodes as node()*) as node()* {
    for $node in $nodes
    return
        if ($node instance of element()) then
            element {name($node)} {
                for $att in $node/@*
                return
                    if ($att[ancestor::template][local-name() = 'strength']) then
                        if ($att = 'CNE') then
                            attribute strength {'required'}
                        else
                        if ($att = 'CWE') then
                            attribute strength {'extensible'}
                        else (
                            $att
                        )
                    else (
                        $att
                    )
                ,
                local:patchTemplate($node/node())
            }
        else (
            $node
        )
};

(: Don't copy element/@id for clones, those would be duplicates :)
declare function local:getClonedContent($nodes as element()*) as element()* {
    for $node in $nodes
    return if (local-name($node) = 'element' or local-name($node) = 'attribute') then element {local-name($node)} {$node/(@* except @id), local:getClonedContent($node/*)} else $node
};

let $nl                     := "&#10;"
let $templeType             := if (request:exists()) then request:get-parameter('type', ())[string-length()>0] else 'template'
let $prefix                 := if (request:exists()) then request:get-parameter('prefix', ())[string-length()>0] else 'demo1-' (:'peri20-':)
let $id                     := if (request:exists()) then request:get-parameter('id', ())[string-length()>0] else () (:'2.16.840.1.113883.3.1937.99.62.3.10.6':)
let $effectiveDate          := if (request:exists()) then request:get-parameter('effectiveDate', ())[string-length()>0] else () (:'2014-10-24T00:00:00':)
let $datasetId              := if (request:exists()) then request:get-parameter('datasetId', ())[string-length()>0] else ()
let $datasetEffectiveDate   := if (request:exists()) then request:get-parameter('datasetEffectiveDate', ())[string-length()>0] else ()
let $clone                  := if (request:exists()) then request:get-parameter('clone', 'false') else 'false'
let $language               := if (request:exists()) then request:get-parameter('language', $get:strArtLanguage) else ''
let $mode                   := if (request:exists()) then request:get-parameter('mode', 'new') else 'new'
let $format                 := if (request:exists()) then request:get-parameter('format', 'html') else 'xml'

let $decor                  := if (empty($prefix)) then ($get:colDecorData) else art:getDecorByPrefix($prefix)
let $isOid                  := temple:isOid($id)
let $objects                := if (not($id)) then () else
    switch ($templeType)
    case 'template' return (
        let $object         := if ($isOid) then $decor//template[@id = $id] else $decor//template[@name = $id]
        let $object         := if ($object) then $object else (
                $get:colDecorData//template[@id = $id] | $get:colDecorCache//template[@id = $id]
            )
        return
            $object
    )
    case 'valueSet' return (
        let $object         := if ($isOid) then $decor//valueSet[@id = $id] else $decor//valueSet[@name = $id]
        let $object         := if ($object) then $object else (
                $get:colDecorData//valueSet[@id = $id] | $get:colDecorCache//valueSet[@id = $id]
            )
        return
            $object
    )
    case 'codeSystem' return (
        let $object         := if ($isOid) then $decor//codeSystem[@id = $id] else $decor//codeSystem[@name = $id]
        let $object         := if ($object) then $object else (
                $get:colDecorData//codeSystem[@id = $id] | $get:colDecorCache//codeSystem[@id = $id]
            )
        return
            $object
    )
    case 'conceptMap' return (
        let $object         := if ($isOid) then $decor//conceptMap[@id = $id] else $decor//conceptMap[@name = $id]
        let $object         := if ($object) then $object else (
                $get:colDecorData//conceptMap[@id = $id] | $get:colDecorCache//conceptMap[@id = $id]
            )
        return
            $object
    )
    default return (
        let $object         :=
            if ($isOid) then (
                $decor//template[@id = $id] | $decor//valueSet[@id = $id] | $decor//codeSystem[@id = $id] | $decor//conceptMap[@id = $id]
            )
            else (
                $decor//template[@name = $id] | $decor//valueSet[@name = $id] | $decor//codeSystem[@name = $id] | $decor//conceptMap[@displayName = $id]
            )
        let $object         := if ($object) then $object else (
                $get:colDecorData//template[@id = $id] | $get:colDecorCache//template[@id = $id] |
                $get:colDecorData//valueSet[@id = $id] | $get:colDecorCache//valueSet[@id = $id] |
                $get:colDecorData//codeSystem[@id = $id] | $get:colDecorCache//codeSystem[@id = $id] |
                $get:colDecorData//conceptMap[@id = $id] | $get:colDecorCache//conceptMap[@id = $id]
            )
        return
            $object
    )

let $object := 
    if ($objects) then
        let $objectEffectiveDate          :=
            if ($effectiveDate castable as xs:dateTime) then $effectiveDate else (
                max($objects/xs:dateTime(@effectiveDate))
            )
        return $objects[@effectiveDate = $objectEffectiveDate]
    else ()

(: Get the prefix when temple was started with just an id :)
let $decor                  := if (empty($prefix)) then $object/ancestor-or-self::decor else art:getDecorByPrefix($prefix)
let $prefix                 := if (empty($prefix)) then $decor/project/@prefix else $prefix
let $templeType             := if (empty($templeType)) then ($object/name())[1] else ($templeType)

let $check                  :=
    if (count($object) = 1 or string-length($prefix) > 0) then () else (
        error(QName('http://art-decor.org/ns/error', 'IllegalParameter'), concat('Parameter prefix is missing and id=''', $id, ''' and effectiveDate=''',$effectiveDate,''' do not lead to a single object in a single project. Found ',count($object),' objects.'))
    )

let $namespaces-attrs       :=
    for $ns at $i in art:getDecorNamespaces($decor)
    return attribute {QName($ns/@uri,concat($ns/@prefix,':dummy-',$i))} {$ns/@uri}

let $dataset                := if (empty($datasetId)) then () else (art:getDataset($datasetId, $datasetEffectiveDate))
let $datasetId              :=
    switch ($templeType)
    case 'template'   return $dataset/@id/string()
    case 'valueSet'   return ((: TODO? :))
    case 'codeSystem' return ((: TODO? :))
    default return ()
let $datasetEd              :=
    switch ($templeType)
    case 'template'   return $dataset/@effectiveDate/string()
    case 'valueSet'   return ((: TODO? :))
    case 'codeSystem' return ((: TODO? :))
    default return ()
let $resources              := art:getFormResources('temple',$language)/resources[1]

let $id                     := $object/@id
let $effectiveDate          := $object/@effectiveDate
let $associations           :=
    switch ($templeType)
    case 'template'   return if (not($id)) then <templateAssociation/> else $decor//templateAssociation[@templateId = $id][@effectiveDate = $effectiveDate]
    case 'valueSet'   return ((: TODO? :))
    case 'codeSystem' return ((: TODO? :))
    case 'conceptMap' return ((: TODO? :))
    default return ()

let $error          := 
    if ($id and empty($object)) 
    then error(QName('http://art-decor.org/ns/error', 'NoObjectFound'), concat('No object found for id=''', $id))
    else ()
    
let $xml            :=
    if (empty($id) or empty($object)) then (
        switch ($templeType)
        case 'template' return
            <rules xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                {$namespaces-attrs}
                {$nl}
                <!-- Temple will copy @id and @effectiveDate from template to templateAssociation -->
                <templateAssociation/>
                {$nl}
                <!-- Create an @id derived from a baseId to create a new template -->
                <template id="{concat(decor:getDefaultBaseIdsP($decor, $decor:OBJECTTYPE-TEMPLATE)[1]/@id/string(), '.new')}" name="" displayName="" effectiveDate="{concat(substring(xs:string(current-dateTime()), 1, 11), '00:00:00')}" statusCode="new">
                {
                    for $lang in distinct-values(($decor/project/@defaultLanguage,$decor/project/name/@language))
                    return
                        <desc language="{$lang}"></desc>
                }
                </template>
            </rules>
        case 'valueSet' return
            <terminology xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <!-- Create an @id derived from a baseId to create a new valueSet -->
                <valueSet id="{concat(decor:getDefaultBaseIdsP($decor, $decor:OBJECTTYPE-VALUESET)[1]/@id/string(), '.new')}" name="" displayName="" effectiveDate="{concat(substring(xs:string(current-dateTime()), 1, 11), '00:00:00')}" statusCode="new">
                {
                    for $lang in distinct-values(($decor/project/@defaultLanguage,$decor/project/name/@language))
                    return
                        <desc language="{$lang}"></desc>
                }
                </valueSet>
            </terminology>
        case 'codeSystem' return
            <terminology xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <!-- Create an @id derived from a baseId to create a new codeSystem -->
                <codeSystem id="{concat(decor:getDefaultBaseIdsP($decor, $decor:OBJECTTYPE-CODESYSTEM)[1]/@id/string(), '.new')}" name="" displayName="" effectiveDate="{concat(substring(xs:string(current-dateTime()), 1, 11), '00:00:00')}" statusCode="new">
                {
                    for $lang in distinct-values(($decor/project/@defaultLanguage,$decor/project/name/@language))
                    return
                        <desc language="{$lang}"></desc>
                }
                </codeSystem>
            </terminology>
         case 'conceptMap' return
            <terminology xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <!-- Create an @id derived from a baseId to create a new codeSystem -->
                <conceptMap id="{concat(decor:getDefaultBaseIdsP($decor, $decor:OBJECTTYPE-MAPPING)[1]/@id/string(), '.new')}" name="" displayName="" effectiveDate="{concat(substring(xs:string(current-dateTime()), 1, 11), '00:00:00')}" statusCode="new">
                {
                    for $lang in distinct-values(($decor/project/@defaultLanguage,$decor/project/name/@language))
                    return
                        <desc language="{$lang}"></desc>
                }
                </conceptMap>
            </terminology>
        default return ()
    )
    else if ($mode=('adapt','version')) then (
        switch ($templeType)
        case 'template' return
            <rules xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            {
                $namespaces-attrs,
                <!-- Temple will copy @id and @effectiveDate from template to templateAssociation -->,
                element templateAssociation {
                    for $associatedConcept in $associations/*
                    let $concept    := $decor//concept[@id=$associatedConcept/@ref][@effectiveDate=$associatedConcept/@effectiveDate][not(ancestor::history)]
                    let $concept    := if ($concept/inherit) then art:getOriginalConceptName($concept/inherit) else $concept
                    return (
                        $nl,
                        comment {concat('Concept: ', $concept/name[@language=$language][1])},
                        $associatedConcept
                    )
                },
                <!-- Create an @id derived from a baseId to create a new template -->,
                element template {
                    attribute id {if ($mode='version') then $object/@id else (concat(decor:getDefaultBaseIdsP($decor, $decor:OBJECTTYPE-TEMPLATE)[1]/@id/string(), '.new'))},
                    $object/@name,
                    $object/@displayName,
                    attribute effectiveDate {substring(string(current-dateTime()),1,19)},
                    attribute statusCode {'new'},
                    $object/(@* except (@id | @name | @displayName | @effectiveDate | @statusCode)),
                    $object/*
                }
            }
            </rules>
        case 'valueSet' return
            <terminology xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <!-- Create an @id derived from a baseId to create a new valueSet -->
                <valueSet>
                {
                    attribute id {if ($mode='version') then $object/@id else (concat(decor:getDefaultBaseIdsP($decor, $decor:OBJECTTYPE-VALUESET)[1]/@id/string(), '.new'))},
                    $object/@name,
                    $object/@displayName,
                    attribute effectiveDate {substring(string(current-dateTime()),1,19)},
                    attribute statusCode {'new'},
                    $object/(@* except (@id | @name | @displayName | @effectiveDate | @statusCode)),
                    $object/*
                }
                </valueSet>
            </terminology>
        case 'codeSystem' return
            <terminology xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <!-- Create an @id derived from a baseId to create a new codeSystem -->
                <codeSystem>
                {
                    attribute id {if ($mode='version') then $object/@id else (concat(decor:getDefaultBaseIdsP($decor, $decor:OBJECTTYPE-CODESYSTEM)[1]/@id/string(), '.new'))},
                    $object/@name,
                    $object/@displayName,
                    attribute effectiveDate {substring(string(current-dateTime()),1,19)},
                    attribute statusCode {'new'},
                    $object/(@* except (@id | @name | @displayName | @effectiveDate | @statusCode)),
                    $object/*
                }
                </codeSystem>
            </terminology>
         case 'conceptMap' return
            <terminology xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <!-- Create an @id derived from a baseId to create a new conceptMap -->
                <conceptMap>
                {
                    attribute id {if ($mode='version') then $object/@id else (concat(decor:getDefaultBaseIdsP($decor, $decor:OBJECTTYPE-MAPPING)[1]/@id/string(), '.new'))},
                    $object/@displayName,
                    attribute effectiveDate {substring(string(current-dateTime()),1,19)},
                    attribute statusCode {'new'},
                    $object/(@* except (@id | @name | @displayName | @effectiveDate | @statusCode)),
                    $object/*
                }
                </conceptMap>
            </terminology>
        default return ()
    )
    else (
        switch ($templeType)
        case 'template' return
            <rules xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            {
                $namespaces-attrs,
                <!-- Temple will copy @id and @effectiveDate from template to templateAssociation -->,
                element templateAssociation {
                    for $associatedConcept in $associations/*
                    let $concept := $decor//concept[@id=$associatedConcept/@ref][@effectiveDate=$associatedConcept/@effectiveDate][not(ancestor::history)]
                    let $concept := if ($concept/inherit) then art:getOriginalConceptName($concept/inherit) else $concept
                    return (
                        $nl,
                        comment {concat('Concept: ', $concept/name[@language=$language][1])},
                        $associatedConcept
                    )
                },
                $object
            }
            </rules>
        case 'valueSet' return
            <terminology xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <!-- Create an @id derived from a baseId to create a new valueSet -->
                {$object}
            </terminology>
        case 'codeSystem' return
            <terminology xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <!-- Create an @id derived from a baseId to create a new codeSystem -->
                {$object}
            </terminology>
        case 'conceptMap' return
            <terminology xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <!-- Create an @id derived from a baseId to create a new conceptMap -->
                {$object}
            </terminology>
        default return ()
    )

(: Create a clone, if desired :)
let $xml          :=
    if ($clone='true') then
        switch ($templeType)
        case 'template' return
            <rules xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            {
                $namespaces-attrs,
                <!-- Temple will copy @id and @effectiveDate from template to templateAssociation -->,
                $xml/templateAssociation,
                <!-- Create an @id derived from a baseId to create a new template -->,
                element template {
                    attribute id {concat(decor:getDefaultBaseIdsP($decor, $decor:OBJECTTYPE-TEMPLATE)/@id,'.new')},
                    $xml/template/@name,
                    $xml/template/@displayName,
                    attribute effectiveDate {concat(format-date(current-date(),'[Y0001]-[M01]-[D01]'),'T00:00:00')},
                    attribute statusCode {'new'},
                    $xml/template/(@* except (@id | @name | @displayName | @effectiveDate | @statusCode)),
                    (:local:getClonedContent($xml/template/*):)
                    $xml/template/*
                }
            }
            </rules>
        case 'valueSet' return
            <terminology xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            {
                <!-- Create an @id derived from a baseId to create a new valueset -->,
                <valueSet>
                {
                    attribute id {concat(decor:getDefaultBaseIdsP($decor, $decor:OBJECTTYPE-VALUESET)/@id,'.new')},
                    $xml/valueSet/@name,
                    $xml/valueSet/@displayName,
                    attribute effectiveDate {concat(format-date(current-date(),'[Y0001]-[M01]-[D01]'),'T00:00:00')},
                    attribute statusCode {'new'},
                    $xml/valueSet/(@* except (@id | @name | @displayName | @effectiveDate | @statusCode)),
                    $xml/valueSet/*
                }
                </valueSet>
            }
            </terminology>
        case 'codeSystem' return
            <terminology xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            {
                <!-- Create an @id derived from a baseId to create a new codeSystem -->,
                <codeSystem>
                {
                    attribute id {concat(decor:getDefaultBaseIdsP($decor, $decor:OBJECTTYPE-CODESYSTEM)/@id,'.new')},
                    $xml/codeSystem/@name,
                    $xml/codeSystem/@displayName,
                    attribute effectiveDate {concat(format-date(current-date(),'[Y0001]-[M01]-[D01]'),'T00:00:00')},
                    attribute statusCode {'new'},
                    $xml/codeSystem/(@* except (@id | @name | @displayName | @effectiveDate | @statusCode)),
                    $xml/codeSystem/*
                }
                </codeSystem>
            }
            </terminology>
        case 'conceptMap' return
            <terminology xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            {
                <!-- Create an @id derived from a baseId to create a new conceptMap -->,
                <conceptMap>
                {
                    attribute id {concat(decor:getDefaultBaseIdsP($decor, $decor:OBJECTTYPE-MAPPING)/@id,'.new')},
                    $xml/conceptMap/@displayName,
                    attribute effectiveDate {concat(format-date(current-date(),'[Y0001]-[M01]-[D01]'),'T00:00:00')},
                    attribute statusCode {'new'},
                    $xml/conceptMap/(@* except (@id | @name | @displayName | @effectiveDate | @statusCode)),
                    $xml/conceptMap/*
                }
                </conceptMap>
            }
            </terminology>
        default return ()
    else $xml

let $xml            :=
    if ($id) then (
        switch ($templeType)
        case 'template'   return temple:addNamesToRulesOids($xml, $decor, $language)
        case 'valueSet'   return temple:addNamesToTerminologyOids($xml, $decor, $language)
        case 'codeSystem' return temple:addNamesToTerminologyOids($xml, $decor, $language)
        case 'conceptMap' return temple:addNamesToTerminologyOids($xml, $decor, $language)
        default return ()
    )
    else $xml

let $xml            :=
    switch ($templeType)
    case 'template'   return local:patchTemplate($xml)
    case 'valueSet'   return $xml
    case 'codeSystem' return $xml
    case 'conceptMap' return $xml
    default return ($xml)

let $edit-blocked   := (
    $mode[. = 'readonly'] or (
    $xml/template[@statusCode=('active', 'final', 'cancelled', 'rejected', 'retired', 'review', 'pending', 'deprecated')] |
    $xml/valueSet[@statusCode=('active', 'final', 'cancelled', 'rejected', 'retired', 'review', 'pending', 'deprecated')] |
    $xml/codeSystem[@statusCode=('active', 'final', 'cancelled', 'rejected', 'retired', 'review', 'pending', 'deprecated')] |
    $xml/conceptMap[@statusCode=('active', 'final', 'cancelled', 'rejected', 'retired', 'review', 'pending', 'deprecated')] |
    $object[not(ancestor::decor/project/@prefix = $prefix)]
    )
)

let $TempleName         := $xml/(template|valueSet|codeSystem|conceptMap)[1]/@name
let $TempleDisplayName  := $xml/(template|valueSet|codeSystem|conceptMap)[1]/@displayName

return
if ($format = 'xml') then
(
    if (request:exists()) then 
    (
        response:set-header("Content-Type", "application/xml"),
        response:set-header("TempleReadOnly", $edit-blocked),
        if (empty($templeType)) then () else response:set-header("TempleType", $templeType),
        if (empty($TempleName)) then () else response:set-header("TempleName", $TempleName), 
        if (empty($TempleDisplayName)) then () else response:set-header("TempleDisplayName", $TempleDisplayName),
        response:set-header("TempleUser", sm:id()//sm:real/sm:username/string()), 
        if ($id) then response:set-header("TempleId", $id) else (), 
        if ($effectiveDate) then response:set-header("TempleEffectiveDate", $effectiveDate) else(),
        if ($datasetId) then response:set-header("TempleDatasetId", $datasetId) else (), 
        if ($datasetEffectiveDate) then response:set-header("TempleDatasetEffectiveDate", $datasetEffectiveDate) else ()
    ) else (),
    $xml
)
else
()
