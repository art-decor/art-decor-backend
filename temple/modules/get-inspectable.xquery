xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace temple  = "http://art-decor.org/ns/temple" at "temple.xqm";
import module namespace art     = "http://art-decor.org/ns/art" at "../../art/modules/art-decor.xqm";
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
declare namespace output        = "http://www.w3.org/2010/xslt-xquery-serialization";

declare %private function local:sanitizeConcept($concept as element(), $language as xs:string) as node()* {
    let $parent :=  <parent>{$concept/../@*, $concept/../name[@language=$language]}</parent>
    let $concept := art:getFullConcept($concept/ancestor::decor, $concept, $language, (), (), (), ())
    return (
        $parent,
        if ($concept[@type='item']) then 
            <concept>{$concept/@*, $concept/*}</concept>
        else 
        if ($concept[@type='group']) then 
            <concept>
            {
                $concept/@*, $concept/name[@language=$language],
                for $child in $concept/* 
                return 
                    if (local-name($child) = 'concept') then <concept>{$child/@*, $child/name}</concept> else ()
            }
            </concept>
        else ()
    )
};

declare %private function local:isOid($oid as xs:string) as xs:boolean {
    matches($oid, "^[1-9][0-9]*(\.[0-9]+)*$")
};

let $nl                     := "&#10;"
let $id                     := if (request:exists()) then request:get-parameter('id', '') else '2.16.840.1.113883.3.1937.99.62.3.10.6' (:'2.16.840.1.113883.2.4.6.10.90.900950':) 
let $effectiveDate          := if (request:exists()) then request:get-parameter('effectiveDate', '') else '' (:'2014-10-24T00:00:00':)
let $prefix                 := if (request:exists()) then request:get-parameter('prefix', '') else 'demo1-' (:'peri20-':)
let $language               := if (request:exists()) then request:get-parameter('language', $get:strArtLanguage) else ''

(: Allow global search only on id :)
let $decor                  := 
    if (not($prefix)) 
    then $get:colDecorData//decor
    else art:getDecorByPrefix($prefix)

let $inspectable := 
    if (local:isOid($id)) then 
        $get:colDecorData//(concept | transaction | valueSet | template)[@id = $id][not(ancestor::history)][not(self::object)] 
    else 
        $decor//(valueSet | template)[@name = $id][not(self::object)]

(: Filter for effectiveDate, except for element id's :)
let $inspectable := 
    if (local-name($inspectable[1]) = 'element') then $inspectable else (
        let $effectiveDate := 
            if (not($effectiveDate) or $effectiveDate="dynamic") 
            then max($inspectable/xs:dateTime(@effectiveDate))
            else $effectiveDate
        return $inspectable[@effectiveDate = $effectiveDate]
    )

let $response := 
    for $item in $inspectable
    return
        if ($item[local-name() = 'concept']) then 
            local:sanitizeConcept($item, $language)
        else
        if ($item[local-name() = 'element'][@id]) then 
            for $assoc in $decor//templateAssociation/concept[@elementId = $item/@id]
            return 
                local:sanitizeConcept(art:getConcept($assoc/@ref, $assoc/@flexibility), $language)
        else if (local-name($item) = 'template')
        then temple:addNamesToRulesOids($item, $item/ancestor::decor, $language)
        else if (local-name($item) = 'transaction')
        then $item
        else if (local-name($item) = 'valueSet')
        then temple:addNamesToTerminologyOids($item, $item/ancestor::decor, $language)
        else if (local-name($item) = 'codeSystem')
        then $item
        else ()
return 
    if ($response) then 
        <result>{$response}</result> 
    else 
        <error>Id {$id} with flexibility {$effectiveDate} is not inspectable, found {count($inspectable)} instances in project {$prefix}.</error>