xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace temple  = "http://art-decor.org/ns/temple" at "temple.xqm";
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace art     = "http://art-decor.org/ns/art" at "../../art/modules/art-decor.xqm";
import module namespace decor   = "http://art-decor.org/ns/decor" at "../../art/api/api-decor.xqm";
import module namespace templ   = "http://art-decor.org/ns/decor/template" at "../../art/api/api-decor-template.xqm";
import module namespace hist    = "http://art-decor.org/ns/decor/history" at "../../art/api/api-decor-history.xqm";
declare namespace request       = "http://exist-db.org/xquery/request";
declare namespace response      = "http://exist-db.org/xquery/response";
declare namespace util          = "http://exist-db.org/xquery/util";
declare namespace validation    = "http://exist-db.org/xquery/validation";
(: You would say that saving this pseudo project to the db and then validate it is a lot of redudant work
   Why not in memory you ask? Well put simply: as soon the template contains new content in a 'different'
   namespace like hl7nl: or pharm: we get validation errors like this:

   Input: <element name="hl7:numerator" datatype="hl7nl:PQ" ...

   Temple validation error, code: NotSchemaValid, description: invalid 82 UndeclaredPrefix: Cannot resolve
   'hl7nl:PQ' as a QName: the prefix 'hl7nl' is not declared.cvc-attribute.3: The value 'hl7nl:PQ' of
   attribute 'datatype' on element 'element' is not valid with respect to its type, 'QName'.

   When we save to the db, and validate from there, this does not occur. So: this is a pragmatic workaround
   for an unexplained phenomenon.
:)
declare %private function local:validateSchema($decor as element(), $newRules as element(rules)) as node() {
    let $res            := concat(util:uuid(),'.xml')
    let $tempProject    :=
        <decor>
        {
            for $ns at $i in art:getDecorNamespaces($decor)
            let $ns-uri     := $ns/@uri/string()
            let $ns-prefix  := $ns/@prefix/string()
            return
                attribute {QName($ns-uri,concat($ns-prefix,':dummy-',$i))} {$ns-uri}
        }
            <project>
            {
                $decor/project/@*
            }
                <name/>
                <copyright years=""/>
            </project>
            <datasets/>
            <scenarios>
                <actors/>
            </scenarios>
            <ids/>
            <terminology/>
            <rules>
            {
                $newRules/node()
            }
            </rules>
            <issues/>
        </decor>
    let $store          := xmldb:store($temple:strUserTemp, $res, $tempProject)

    (: Using the schema at 'https://assets.art-decor.org/ADAR/rv/DECOR.xsd' throws a SAX error, using the local schema does not. :)
    let $result         := validation:jaxv-report(doc($store), $get:docDecorSchema)

    let $delete         := xmldb:remove($temple:strUserTemp, $res)

    return
        $result
};

declare %private function local:saveTemplate($prefix as xs:string, $newRules as node(), $reportOnly as xs:string) as element(){
    let $decor          := art:getDecorByPrefix($prefix)
    let $user           := get:strCurrentUserName()
    let $isAuthor       := decor:authorCanEditP($decor, $decor:SECTION-RULES)
    let $assert         :=
        if ($isAuthor) then () else (
            error(QName('http://art-decor.org/ns/error', 'YouAreNoAuthor'), concat('User ', $user, ' is not an author in this project'))
        )
    let $logfile        := doc('/db/apps/temple/xml/log.xml')
    let $logon          := false()
    let $assert         :=
        if (count($newRules//template) = 1) then () else (
            error(QName('http://art-decor.org/ns/error', 'MoreOrLessThanOneTemplate'), 'Rules must contain exactly one template')
        )
    let $assert         :=
        if (count($newRules//templateAssociation) <= 1) then () else (
            error(QName('http://art-decor.org/ns/error', 'MoreThanOneTemplateAssociation'), 'Rules may not contain more than one templateAssociation')
        )
    let $assert         :=
        for $id in $newRules//templateAssociation/concept/@elementId
        return
            if ($newRules//element[@id=$id]) then () else
            if ($newRules//attribute[@id=$id]) then () else (
                error(QName('http://art-decor.org/ns/error', 'TemplateAssociationButNoElementOrAttribute'), concat('Id ', $id, ' has a templateAssociation but no associated element or attribute id'))
            )
    let $assert         :=
        for $id in $newRules//element/@id
        return
            if (count($newRules//element[@id=$id]) < 2) then () else (
                error(QName('http://art-decor.org/ns/error', 'DuplicateElementId'), concat('Element id ', $id, ' occurs more than once in template'))
            )
    let $assert         :=
        for $qname in $newRules//element/@datatype | $newRules//attribute/@datatype | $newRules//element/@name | $newRules//attribute/@name
        let $ns-prefix      := substring-before($qname,':')
        let $ns-status      :=
            (:@datatype does not need a prefix, nor does attribute/@name:)
            if (string-length($ns-prefix) = 0) then (
                if (name($qname) = 'datatype') then ('valid') else
                if (name($qname/..) = 'attribute') then ('valid') else ('missing')
            )
            (: hl7, cda, and xsi are always considered declared:)
            else (
                if ($ns-prefix=('hl7','cda','xsi')) then ('valid') else
                if (empty(namespace-uri-for-prefix($ns-prefix,$decor))) then ('invalid') else ('valid')
            )
        return
            if ($ns-status = 'valid') then () else if ($ns-status = 'missing') then (
                error(QName('http://art-decor.org/ns/error', 'MissingNamespacePrefix'), concat(name($qname/..),'/@',name($qname),'=&quot;',$qname,'&quot; is missing a namespace prefix'))
            )
            else (
                error(QName('http://art-decor.org/ns/error', 'InvalidNamespacePrefix'), concat(name($qname/..),'/@',name($qname),'=&quot;',$qname,'&quot; contains an undeclared prefix &quot;',$ns-prefix,'&quot;'))
            )
    let $newId                  := $newRules//template/@id
    let $newEffectiveDate       := $newRules//template/@effectiveDate

    let $oldTemplateAssociation := $decor//templateAssociation[@templateId=$newId][@effectiveDate=$newEffectiveDate]
    let $oldTemplate            := $decor//template[@id=$newId][@effectiveDate=$newEffectiveDate]
    (: Make a new template when submitted one has id 'new', or submitted id exists but submitted effectiveDate not :)
    let $makeNewTemplate        :=
        (: Template with id and effectiveDate exists :)
        if ($oldTemplate) then false()
        (: A new template, new id must be issued :)
        else if ($newId[ends-with(.,'new')]) then true()
        (: Template with this id exists, but not this effectiveDate :)
        else if ($decor//template[@id=$newId]) then not($oldTemplate)
        (: Id does not exist and != 'new :)
        else error(QName('http://art-decor.org/ns/error', 'IdNotValid'), "Template/@id must either exist or be '{some baseId}.new'.")
    let $makeNewTemplateVersion :=
        if ($oldTemplate) then false() else if ($decor//template[@id=$newId]) then true() else false()

    let $assert         :=
        if (not($makeNewTemplate and $decor//template[@name=$newRules//template/@name][@effectiveDate=$newEffectiveDate])) then () else (
            error(QName('http://art-decor.org/ns/error', 'TemplateByNameAndDateExists'), "A template with this name and effectiveDate already exists.")
        )

    let $newRules       :=
        if ($makeNewTemplate) then (
            (: Issue new id :)
            (: MdG Note to self: Code copied from create-decor-template.xquery. I cannot use the entire xquery, code like
            this - and more - should be refactored into templates api :)
            let $templateRoot   :=
                if ($makeNewTemplateVersion)
                then ($newId) else
                if (temple:isOid(replace($newId,'.?new','')))
                then (replace($newId,'.?new',''))
                else (decor:getDefaultBaseIdsP($decor, $decor:OBJECTTYPE-TEMPLATE)/@id)

            (:generate new id if mode is 'new' or 'adapt', else use existing id:)
            let $newTemplateId             :=
                if ($makeNewTemplateVersion) then
                    $newId
                else (
                    let $templateIdsAll         :=
                        $decor//@id[matches(.,concat('^',$templateRoot,'\.\d+$'))]
                    return
                    if ($templateIdsAll) then
                        concat($templateRoot,'.',max($templateIdsAll/xs:integer(tokenize(.,'\.')[last()]))+1)
                    else (
                        concat($templateRoot,'.',1)
                    )
                )
            
            let $assert             := 
                if (matches($newTemplateId, '^[0-2](\.(0|[1-9][0-9]*)){3,}$')) then () else (
                    error(QName('http://art-decor.org/ns/error', 'TemplateIdInvalid'), 'Template ' || $newTemplateId || ' pattern SHALL be [0-2](\.(0|[1-9][0-9]*))*. An OID identifies exactly one object and SHALL NOT be reused. OIDs of 3 nodes or less are always reserved for a purpose. See https://oid-info.com or this servers'' OID index when in doubt..')
                )
            
            let $result :=
            <rules>
            {
                $newRules/@*,
                element templateAssociation {
                    attribute templateId {$newTemplateId},
                    attribute effectiveDate {$newEffectiveDate},
                    $newRules//templateAssociation/(@* except @templateId),
                    $newRules//templateAssociation/*
                },
                element template {
                    attribute id {$newTemplateId},
                    $newRules//template/(@* except (@id, @statusCode)),
                    attribute statusCode {'draft'},
                    $newRules//template/*
                }
            }
            </rules>
            (: Replace 'new' in templateId/@root and element/@id :)
            return temple:replaceTemplateNewId($result, $newId, $newTemplateId)
        )
        else (
            (: Make a templateAssociation if one doesn't exist. We also set templateId and
            effectiveDate to the same values as on template, so user may omit them. :)
            <rules>
            {
                $newRules/@*,
                element templateAssociation {
                    attribute templateId {$newId},
                    attribute effectiveDate {$newEffectiveDate},
                    $newRules//templateAssociation/concept
                },
                $newRules//template
            }
            </rules>
        )
    let $newRules       := temple:removeNamesFromRulesOids($newRules)

    (: Validate against decor.xsd :)
    let $schemaResults  := local:validateSchema($decor, $newRules)
    let $assert         :=
        if (not($schemaResults//status="invalid"))
        then ()
        else error(QName('http://art-decor.org/ns/error', 'NotSchemaValid'), $schemaResults)
    let $assert         :=
        for $concept in $newRules//templateAssociation/concept
        return
            if ($decor//dataset//concept[@id = $concept/@ref][@effectiveDate = $concept/@effectiveDate]) then () else (
                error(QName('http://art-decor.org/ns/error', 'ConceptDoesNotExist'), concat('Concept id=', $concept/@ref, ' effectiveDate=', $concept/@effectiveDate, ' in templateAssociation/concept does not exist.'))
            )
    (: Check status :)
    let $statusAllowed  := if ($oldTemplate) then art:isStatusChangeAllowable($oldTemplate, $newRules//template/@statusCode[1]) else (true())
    let $assert         :=
        if ($statusAllowed) then () else (
            error(QName('http://art-decor.org/ns/error', 'StatusChangeNotAllowed'), concat('Setting status from ', $oldTemplate/@statusCode, ' to ', $newRules//template/@statusCode, ' is not allowed'))
        )
    (: Block updates if an error occurred :)
    let $update         :=
        if ($reportOnly = 'true') then (<ok/>) else (
            if ($logon) then
                update insert
                    <update user="{$user}" time="{fn:current-dateTime()}">
                        <old><rules>{$oldTemplateAssociation, $oldTemplate}</rules></old>
                        <new>{$newRules}</new>
                    </update>
                into $logfile/logroot
            else <nothing/>
            ,
            if ($makeNewTemplate) then (
                update insert $newRules//templateAssociation into $decor/rules,
                update insert $newRules//template into $decor/rules
            )
            else (
                let $existingTemplate               := $decor//template[@id=$newId][@effectiveDate=$newEffectiveDate]
                let $existingTemplateAssociation    := $decor//templateAssociation[@templateId=$newId][@effectiveDate=$newEffectiveDate]
                (: ======= do history first ======= :)
                let $intention          :=
                    if ($existingTemplate[@statusCode='active'][@effectiveDate = $newRules//template/@effectiveDate]) then 'patch' else 'version'
                let $history            :=
                    if ($existingTemplate) then (hist:AddHistory ($decor:OBJECTTYPE-TEMPLATE, $prefix, $intention, $existingTemplate)) else ()
                return (
                    if ($existingTemplateAssociation) then (
                        update replace $existingTemplateAssociation[1] with $newRules//templateAssociation
                    )
                    else (
                        update insert comment {string-join((' ',if ($newRules//template[@displayName]) then $newRules//template/@displayName else $newRules//template/@name,' '),'')} preceding $existingTemplate,
                        update insert $newRules//templateAssociation preceding $existingTemplate
                    )
                    ,
                    update replace $existingTemplate with $newRules//template
                )
            ),
            templ:addTemplateElementAndAttributeIds($decor, ($newRules//template)[1]/@id, ($newRules//template)[1]/@effectiveDate)
            ,
            <ok/>
        )
    return $newRules
};

declare %private function local:addSelect($items as item()*) as item()* {
    for $item in $items
    return
        element {name($item)} {
            attribute selected {()},
            $item/(@* except @selected),
            local:addSelect($item/*)
        }
};

let $prefix         := if (request:exists()) then request:get-parameter('prefix', '') else 'demo1-'
let $code           := if (request:exists()) then request:get-parameter('code', '') else ''
let $reportOnly     := if (request:exists()) then request:get-parameter('reportOnly', 'false') else 'false'
let $datasetId      := if (request:exists()) then request:get-parameter('datasetId', ()) else ()
let $datasetEd      := if (request:exists()) then request:get-parameter('datasetEffectiveDate', ()) else ()
let $newRules       :=
    try {fn:parse-xml($code)}
    catch * {
        <templeError at="{current-dateTime()}">{concat('Temple parsing error, code: ', $err:code, ', description: ', $err:description, ' value: ',$err:value,' module: ',$err:module,' (',$err:line-number,' ',$err:column-number,')')}</templeError>
    }

(: When there is a parsing error, save and validate. If there is a schema validation error, validate again to catch output, else return the error as is. :)
let $result         :=
    if ($newRules[self::templeError]) then $newRules else (
        try {local:saveTemplate($prefix, $newRules, $reportOnly)}
        catch * {
            <templeError>{concat('Temple validation error, code: ', $err:code, ', description: ', $err:description)}</templeError>
        }
    )

let $decor  := art:getDecorByPrefix($prefix)
let $language := if (request:exists()) then request:get-parameter('language', $get:strArtLanguage) else ''
let $result := temple:addNamesToRulesOids($result, $decor, $language)

let $dummy :=
    if (response:exists()) then (
        if ($result//status[.='invalid'] | $result[self::templeError])
        then (response:set-status-code(400))
        else (response:set-status-code(200))
    ) else ()

return $result
