import module namespace art         = "http://art-decor.org/ns/art" at "../../art/modules/art-decor.xqm";
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";

declare namespace json      = "http://www.json.org";
declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";

declare option exist:serialize "method=json media-type=text/javascript";

let $prefix                 := if (request:exists()) then request:get-parameter('prefix', ())[string-length()>0] else 'demo1-' (:'peri20-':)
let $language := if (request:exists()) then request:get-parameter('language', $get:strArtLanguage) else ''
let $datasets := art:getDatasetList($prefix, (), (), false(), false())
return 
<datasets>
{
for $dataset in $datasets/dataset
let $datasetName    := if ($dataset/name[@language = $language][text()]) then $dataset/name[@language = $language][1] else $dataset/name[1]
return
    <dataset json:array='true' value="{concat($dataset/@id,'--',$dataset/@effectiveDate)}" >
    {
        if ($dataset/name[@language = $language]) then
            data($dataset/name[@language = $language]/@selectorName)
        else data($dataset/name[1])
    }
    </dataset>
}
</datasets>
