xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace temple  = "http://art-decor.org/ns/temple" at "temple.xqm";
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";

let $nl                     := "&#10;"
let $id                     := if (request:exists()) then request:get-parameter('id', '') else "2.16.840.1.113883.3.1937.99.62.3.10.11"
let $effectiveDate          := if (request:exists()) then request:get-parameter('effectiveDate', '') else "2012-03-11T00:00:00"
let $language               := if (request:exists()) then request:get-parameter('language', '') else 'nl-NL' (:'2014-10-24T00:00:00':)

let $update     := 
    if ($temple:userData/users/user[@name=get:strCurrentUserName()])
    then ()
    else update insert <user name="{get:strCurrentUserName()}"><preferences/><bookmarks/></user> into $temple:userData/users
let $thing      := $get:colDecorData//*[@id=$id][@effectiveDate=$effectiveDate][not(ancestor::history)][not(self::object)]
let $action     := if ($temple:myBookmarks//*[@id=$id][@effectiveDate=$effectiveDate]) then 'removed' else 'added'
let $update     := 
    if ($temple:myBookmarks//*[@id=$id][@effectiveDate=$effectiveDate]) 
    then update delete $temple:myBookmarks//*[@id=$id][@effectiveDate=$effectiveDate]
    else update insert
        element {local-name($thing)} {$thing/@*, $thing/name[@language=$language]}
        into $temple:myBookmarks
return $action