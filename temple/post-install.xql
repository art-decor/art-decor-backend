xquery version "3.0";
(:
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
    
    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    
    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace sm          = "http://exist-db.org/xquery/securitymanager";
import module namespace xmldb       = "http://exist-db.org/xquery/xmldb";
import module namespace repo        = "http://exist-db.org/xquery/repo";
import module namespace art         = "http://art-decor.org/ns/art" at "../art/modules/art-decor.xqm";
declare namespace xis               = "http://art-decor.org/ns/xis";

(: The following external variables are set by the repo:deploy function :)

(: file path pointing to the exist installation directory :)
declare variable $home external;
(: path to the directory containing the unpacked .xar package :)
declare variable $dir external;
(: the target collection into which the app is deployed :)
declare variable $target external;
declare variable $root := repo:get-root();

(: helper function for setting write permissions :)
declare %private function local:setPermissions() {
    (: 754 == -rwxr-xr-- :)
    
    for $x in xmldb:get-child-resources(concat($root,'/temple/modules'))
    return (
        sm:chown(xs:anyURI(concat('xmldb:exist:///',$root,'/temple/modules/',$x)),'admin:decor'),
        if (starts-with($x, 'find-') or starts-with($x, 'get-') or starts-with($x, 'flip-'))
        then
            sm:chmod(xs:anyURI(concat('xmldb:exist:///',$root,'/temple/modules/',$x)), 'rwxr-xr-x')
        else
            sm:chmod(xs:anyURI(concat('xmldb:exist:///',$root,'/temple/modules/',$x)), 'rwxr-xr--')
        ,
        sm:clear-acl(xs:anyURI(concat('xmldb:exist:///',$root,'/temple/modules/',$x)))
    )
};

let $strTempleData      := 
    if (xmldb:collection-available(concat($root, 'temple-data')))
    then concat($root, 'temple-data')
    else xmldb:create-collection($root,'temple-data')
let $strTempleDataTemp  := 
    if (xmldb:collection-available(concat($root, 'temple-data/temp')))
    then concat($root, 'temple-data/temp')
    else xmldb:create-collection($root,'temple-data/temp')
let $strUserData        := 
    if (doc-available(concat($strTempleData, '/userData.xml')))
    then (concat($strTempleData,'/userData.xml'))
    else xmldb:store($strTempleData, 'userData.xml', <users/>)
let $perms          := sm:chown($strTempleData,'admin:decor')
let $perms          := sm:chmod($strTempleData, 'rwxrwxr--')
let $perms          := sm:chown($strTempleDataTemp,'admin:decor')
let $perms          := sm:chmod($strTempleDataTemp, 'rwxrwxr--')
let $perms          := sm:chown($strUserData,'admin:decor')
let $perms          := sm:chmod($strUserData, 'rwxrwxr--')

let $update         := local:setPermissions()

return ()