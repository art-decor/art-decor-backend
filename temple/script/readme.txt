Temple search box:
?                       : This text
[scope]::[prefix]:xxx   : General syntax: optional scope, optional prefix, string or *
weight                  : Search for all artefacts containing 'weight' in current project.
*:weight                : Search 'weight' in current project and referenced BBR's.
tra::weight             : Search for 'weight' in scope 'tra[nsactions]'.
val::weight             : Same for valuesets. Or concepts (con::xxx), codeSystems (cod::xxx),
                          templates (tem::xxx)
tem::demo1:weight       : Templates containing 'weight' in 'demo1-' project.
                          This allows searching in other projects than current project.
val::*                  : All valuesets in current project.
loinc::amoxi serum      : Search for LOINC concepts containing 'amoxi' AND 'serum'.
loinc::18-2             : Search for LOINC concept on id. Returns concept details.
sct::myocard infarct    : Search for Snomed CT concepts containing 'myocard' AND 'infarct'.
sct::22298006           : Search for Snomed CT concept on id. Returns concept details.
(sct and loinc search will display an error on servers where LOINC or Snomed are not installed.)

Temple shortcuts:
Ctrl+I or Cmd-I         : Inspect what's on current line and display in inspector.
                          Temple will look for an id on the line.
Alt-I                   : Inspect what's on current line and open in editor.

Select text (double-click: word, triple-click: line, shift-click: rectangle) and drag-and-drop to code window.

Temple II (c) 2014-2019 Marc de Graauw and others, MIT License
CodeMirror (c) 2017 by Marijn Haverbeke and others, MIT License
