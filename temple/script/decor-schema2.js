/*
*    Author: Marc de Graauw, derived from decor.xsd
*
*    This program is free software; you can redistribute it and/or modify it under the terms
*    of the GNU General Public License as published by the Free Software Foundation;
*    either version 3 of the License, or (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
*    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*    See the GNU General Public License for more details.
*
*    See http://www.gnu.org/licenses/gpl.html
*/
var tags_rules = {
    "!top": ["rules"],
    rules: {
        attrs: {},
        children: ["templateAssociation", "template"]
    },
    templateAssociation: {
        attrs: {
            templateId: temple.getBaseIds,
            effectiveDate: temple.getEffectiveDates
        },
        children: ["concept"]
    },
    concept: {
        attrs: {
            ref: temple.getProjectConcepts,
            effectiveDate: temple.getEffectiveDates,
            elementId: temple.getElementIds
        }
    },
    template: {
        attrs: {
            id: temple.getBaseIds,
            name: null,
            displayName: null,
            effectiveDate: temple.getEffectiveDates,
            statusCode: TemplateStatusCodeLifeCycle,
            isClosed: ["true", "false"]
        },
        children: ["desc", "classification", "relationship", "context", "item", "example", "publishingAuthority", "endorsingAuthority", "revisionHistory", "attribute", "choice", "element", "include"]
    },
    desc: {
        attrs: {
            language: languages
        }
    },
    classification: {
        attrs: {
            type: ["cdadocumentlevel", "messagelevel", "cdaheaderlevel", "cdasectionlevel", "cdaentrylevel", "segmentlevel", "clinicalstatementlevel", "controlactlevel", "payloadlevel", "datatypelevel"]
        },
        children: ["tag"]
    },
    relationship: {
        attrs: {
            type: ["REPL", "SPEC", "GEN", "COPY", "ADAPT", "EQUIV", "VERSION", "BACKWD", "DRIV"],
            template: null,
            model: null,
            flexibility: ["dynamic"]
        }
    },
    context: {
        attrs: {
            id: ["*", "**"],
            path: null
        }
    },
    publishingAuthority: {
        attrs: {
            id: null,
            name: null
        },
        children: ["addrLine"]
    },
    endorsingAuthority: {
        attrs: {
            id: null,
            name: null
        },
        children: ["addrLine"]
    },
    revisionHistory: {
        attrs: {
            date: temple.getEffectiveDates,
            by: null
        },
        children: ["desc"]
    },
    addrLine: {
        attrs: {
            type: AddressLineType
        }
    },
    let: {
        attrs: {
            name: null,
            value: null
        }
    },
    assert: {
        attrs: {
            role: ["fatal", "error", "warning", "info"],
            test: null,
            see: null,
            flag: null
        },
        children: ["name", "value-of", "emph", "span", "dir"]
    },
    report: {
        attrs: {
            role: ["fatal", "error", "warning", "hint"],
            test: null,
            see: null,
            flag: null
        },
        children: ["name", "value-of", "emph", "span", "dir"]
    },
    name: {
        attrs: {
            path: null
        }
    },
    span: {
        attrs: {
            "class": null
        }
    },
    "value-of": {
        attrs: {
            select: null
        }
    },
    dir: {
        attrs: {
            value: ["ltr","rtr"]
        }
    },
    defineVariable: {
        attrs: {
            name: null,
            path: null
        },
        children: ["code", "use"]
    },
    code: {
        attrs: {
            code: null,
            codeSystem: temple.getProjectIds
        }
    },
    use: {
        attrs: {
            path: null,
            as: null
        }
    },
    element: {
        attrs: {
            name: temple.getCdaName,
            cc: conformance_cardinality,
            id: temple.getElementIds,
            minimumMultiplicity: ["0", "1"],
            maximumMultiplicity: ["1", "*"],
            isMandatory: bool,
            conformance: ["R", "NP", "C"],
            datatype: ["AD", "ADXP", "ANY", "BIN", "BL", "BN", "BXIT_CD", "BXIT_IVL_PQ", "CD", "CE", "CO", "CR", "CS", "CV", "ED", "EIVL_PPD_TS", "EIVL_TS", "EN", "ENXP", "GLIST_PQ", "GLIST_TS", "HXIT_CE", "HXIT_PQ", "II", "INT", "hl7nl:INT", "IVL_INT", "IVL_MO", "IVL_PPD_PQ", "IVL_PPD_TS", "IVL_PQ", "IVL_REAL", "IVL_TS", "IVXB_INT", "IVXB_MO", "IVXB_PPD_PQ", "IVXB_PPD_TS", "IVXB_PQ", "IVXB_REAL", "IVXB_TS", "MO", "ON", "PIVL_PPD_TS", "PIVL_TS", "hl7nl:PIVL_TS", "PN", "PPD_PQ", "PPD_TS", "PQ", "PQR", "QTY", "REAL", "RTO", "RTO_MO_PQ", "RTO_PQ_PQ", "RTO_QTY_QTY", "SC", "SLIST_PQ", "SLIST_TS", "ST", "SXCM_CD", "SXCM_INT", "SXCM_MO", "SXCM_PPD_PQ", "SXCM_PPD_TS", "SXCM_PQ", "SXCM_REAL", "SXCM_TS", "SXPR_TS", "TEL", "TN", "TS", "URL", "UVP_TS"],
            strength: ["required","extensible","preferred","example"],
            contains: temple.getProjectTemplates,
            isClosed: ["true", "false"]
        },
        children: ["desc", "item", "example", "inherit", "vocabulary", "property", "text", "attribute", "let", "assert", "report", "defineVariable" ,"element", "choice", "include", "constraint"]
    },
    attribute: {
        attrs: {
            as: attribute_shortcut,
            name: null,
            value: null,
            id: temple.getElementIds,
            isOptional: bool,
            prohibited: bool,
            datatype: ["bin", "bl", "bn", "int", "oid", "ruid", "uuid", "uid", "real", "set_cs", "cs", "st", "ts"]
        },
        children: ["desc", "item", "vocabulary"]
    },
    property: {
        attrs: {
            unit: null,
            currency: null,
            minInclude: null,
            maxInclude: null,
            fractionDigits: null,
            minLength: null,
            maxLength: null,
            value: null
        }
    },
    vocabulary: {
        attrs: {
            code: null,
            codeSystem: temple.getProjectCodeSystems,
            displayName: null,
            valueSet: temple.getProjectValuesets,
            flexibility: ["dynamic"],
            domain: null
        }
    },
    include: {
        attrs: {
            ref: temple.getProjectTemplates,
            cc: conformance_cardinality,
            minimumMultiplicity: ["0", "1"],
            maximumMultiplicity: ["1", "*"],
            isMandatory: bool,
            conformance: ["R", "NP", "C"]
        },
        children: ["desc", "item", "example", "constraint"]
    },
    choice: {
        attrs: {
            cc: cardinality,
            minimumMultiplicity: ["0", "1"],
            maximumMultiplicity: ["1", "*"]
        },
        children: ["desc", "item", "element", "include", "choice"]
    },
    example: {
        attrs: {
            caption: null,
            type: ["error", "valid", "neutral"]
        }
    },
    constraint: {
        attrs: {
            language: languages
        }
    }
};
var tags_terminology_valueset = {
    "!top": ["terminology"],
    terminology: {
        attrs: {},
        children: ["terminologyAssociation", "valueSet"]
    },
    valueSet: {
        attrs: {
            id: temple.getBaseIds,
            name: null,
            displayName: null,
            effectiveDate: temple.getEffectiveDates,
            expirationDate: temple.getEffectiveDates,
            statusCode: ItemStatusCodeLifeCycle,
            experimental: ["true", "false"],
            canonicalUri: null
        },
        children: ["desc", "publishingAuthority", "purpose", "copyright", "revisionHistory", "conceptList"]
    },
    desc: {
        attrs: {
            language: languages
        }
    },
    publishingAuthority: {
        attrs: {
            id: null,
            name: null
        },
        children: ["addrLine"]
    },
    revisionHistory: {
        attrs: {
            date: temple.getEffectiveDates,
            by: null
        },
        children: ["desc"]
    },
    addrLine: {
        attrs: {
            type: AddressLineType
        }
    },
    filter: {
        attrs: {
            property: null,
            op: ["descendent-of", "is-a", "is-not-a", "regex", "in", "equal", "exist"],
            value: null
        }
    },
    purpose: {
        attrs: {
            language: languages
        }
    },
    copyright: {
        attrs: {
            language: languages
        }
    },
    conceptList: {
        children: ["concept", "include", "exclude", "exception"]
    },
    concept: {
        attrs: {
            code: null,
            codeSystem: temple.getProjectCodeSystems,
            codeSystemName: null,
            codeSystemVersion: null,
            displayName: null,
            ordinal: null,
            level: ["0","1","2","3","4","5","6","7","8","9"],
            type: VocabType
        },
        children: ["designation","desc"]
    },
    include: {
        attrs: {
            ref: temple.getProjectValuesets,
            flexibility: ["dynamic"],
            exception: ["true", "false"],
            op: ["descendent-of", "is-a", "is-not-a", "regex", "in", "equal", "exist"],
            code: null,
            codeSystem: temple.getProjectCodeSystems,
            displayName: null,
            type: "D"
        },
        children: ["desc", "filter"]
    },
    exclude: {
        attrs: {
            ref: temple.getProjectValuesets,
            flexibility: ["dynamic"],
            exception: ["true", "false"],
            op: ["descendent-of", "is-a", "is-not-a", "regex"],
            code: null,
            codeSystem: temple.getProjectCodeSystems,
            displayName: null
        },
        children: ["desc"]
    },
    exception: {
        attrs: {
            code: null,
            codeSystem: temple.getProjectCodeSystems,
            codeSystemName: null,
            codeSystemVersion: null,
            displayName: null,
            ordinal: null,
            level: ["0","1","2","3","4","5","6","7","8","9"],
            type: VocabType
        },
        children: ["designation","desc"]
    },
    designation: {
        attrs: {
            type: DesignationType,
            language: languages
        }
    }
};
var tags_terminology_codesystem = {
    "!top": ["terminology"],
    terminology: {
        attrs: {},
        children: ["codeSystem"]
    },
    codeSystem: {
        attrs: {
            id: temple.getBaseIds,
            name: null,
            displayName: null,
            effectiveDate: temple.getEffectiveDates,
            expirationDate: temple.getEffectiveDates,
            statusCode: ItemStatusCodeLifeCycle,
            experimental: ["true", "false"],
            caseSensitive: ["true", "false"],
            canonicalUri: null
        },
        children: ["desc", "publishingAuthority", "endorsingAuthority", "purpose", "copyright", "revisionHistory", "property", "conceptList"]
    },
    desc: {
        attrs: {
            language: languages
        }
    },
    publishingAuthority: {
        attrs: {
            id: null,
            name: null
        },
        children: ["addrLine"]
    },
    endorsingAuthority: {
        attrs: {
            id: null,
            name: null
        },
        children: ["addrLine"]
    },
    revisionHistory: {
        attrs: {
            date: temple.getEffectiveDates,
            by: null
        },
        children: ["desc"]
    },
    addrLine: {
        attrs: {
            type: AddressLineType
        }
    },
    purpose: {
        attrs: {
            language: languages
        }
    },
    copyright: {
        attrs: {
            language: languages
        }
    },
    property: {
        attrs: {
            code: null,
            uri: null,
            description: null,
            type: ["code","Coding","string","integer","boolean","dateTime","decimal"]
        },
        children: ["valueCode","valueCoding","valueString","valueInteger","valueBoolean","valueDateTime","valueDecimal"]
    },
    conceptList: {
        children: ["codedConcept"]
    },
    codedConcept: {
        attrs: {
            code: null,
            ordinal: null,
            level: ["0","1","2","3","4","5","6","7","8","9"],
            type: VocabType,
            statusCode: TemplateStatusCodeLifeCycle,
            effectiveDate: temple.getEffectiveDates,
            expirationDate: temple.getEffectiveDates
        },
        children: ["designation", "desc","property","parent","child"]
    },
    designation: {
        attrs: {
            type: DesignationType,
            language: languages,
            displayName: null
        }
    },
    parent: {
        attrs: {
            code: null
        }
    },
    child: {
        attrs: {
            code: null
        }
    },
    valueCode: {
        attrs: {
            code: null
        }
    },
    valueCoding: {
        attrs: {
            code: null,
            codeSystem: null,
            canonicalUri: null
        }
    },
    valueString: {
        attrs: {
            value: null
        }
    },
    valueInteger: {
        attrs: {
            value: null
        }
    },
    valueBoolean: {
        attrs: {
            value: null
        }
    },
    valueDateTime: {
        attrs: {
            value: null
        }
    },
    valueDecimal: {
        attrs: {
            value: null
        }
    }
}
var tags_terminology_conceptmap = {
    "!top": ["terminology"],
    terminology: {
        attrs: {},
        children: ["conceptMap"]
    },
    codeSystem: {
        attrs: {
            id: temple.getBaseIds,
            displayName: null,
            effectiveDate: temple.getEffectiveDates,
            expirationDate: temple.getEffectiveDates,
            statusCode: ItemStatusCodeLifeCycle,
            canonicalUri: null
        },
        children: ["desc", "publishingAuthority", "endorsingAuthority", "purpose", "copyright", "jurisdiction", "sourceScope", "targetScope", "group"]
    },
    desc: {
        attrs: {
            language: languages
        }
    },
    publishingAuthority: {
        attrs: {
            id: null,
            name: null
        },
        children: ["addrLine"]
    },
    endorsingAuthority: {
        attrs: {
            id: null,
            name: null
        },
        children: ["addrLine"]
    },
    addrLine: {
        attrs: {
            type: AddressLineType
        }
    },
    purpose: {
        attrs: {
            language: languages
        }
    },
    copyright: {
        attrs: {
            language: languages
        }
    },
    jurisdiction: {
        attrs: {
            code: null,
            codeSystem: null,
            displayName: null,
            canonicalUri: null
        }
    },
    sourceScope: {
        attrs: {
            ref: null,
            flexibility: null,
            displayName: null,
            canonicalUri: null
        }
    },
    targetScope: {
        attrs: {
            ref: null,
            flexibility: null,
            displayName: null,
            canonicalUri: null
        }
    },
    group: {
        children: ["source", "target", "element"]
    },
    source: {
        attrs: {
            codeSystem: null,
            codeSystemVersion: null
        }
    },
    target: {
        attrs: {
            codeSystem: null,
            codeSystemVersion: null
        }
    },
    element: {
        attrs: {
            code: null,
            displayName: null
        },
        children: ["target"]
    },
    target: {
        attrs: {
            code: null,
            displayName: null,
            relationship: ["equal", "equivalent", "relatedto", "wider", "narrower", "unmatched"]
        },
        children: ["comment"]
    },
    comment: {
        attrs: {
            language: languages
        }
    }
}
