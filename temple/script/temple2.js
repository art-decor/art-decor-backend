/*
 *    Author: this file written by Marc de Graauw
 *
 *    This program is free software; you can redistribute it and/or modify it under the terms
 *    of the GNU General Public License as published by the Free Software Foundation;
 *    either version 3 of the License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *    See the GNU General Public License for more details.
 *
 *    See http://www.gnu.org/licenses/gpl.html
 */
var temple = (function() {
    var strRoot = '../modules/'
    var templedocs = {}
    var schemas = {}
    var lastDocId = 0
    var currentDocId = 0
    var history = []
    var future = []
    var loading = 0

    // GENERIC FUNCTIONS
    function isOid(oid) {
        var patt = new RegExp("^[1-9][0-9]*(\\.[0-9]+)*$");
        var res = patt.test(oid);
        return res;
    }

    // function getOid(oid) {
    //     var patt = new RegExp("\\[[\\d\\.]*\\]");
    //     var res = patt.test(oid);
    //     return res;
    // }

    // SERVER FUNCTIONS
    function validateOrSave(reportOnly) {
        busy('saving');
        var request = new XMLHttpRequest();
        request.onload = function() {
            if (request.status == 200) {
                if (reportOnly == false) {
                    temple.cmCode.setValue(request.responseText);
                    addDecorClass(temple.cmCode);
                    document.getElementById('changed' + currentDocId).style.display = 'none'
                    getProjectTypes(temple.strPrefix, temple.strLanguage)
                } 
                temple.cmInspector.setValue('Document is valid');
                } else if (request.readyState == 4 && request.status != 200) {
                temple.cmInspector.setValue(request.responseText);
                if (reportOnly == false) {
                    window.alert("ERROR: Document is not saved, see validation errors.")
                }
            }
            busy('savingdone');
        };
        var strQuery = '?&prefix=' + temple.strPrefix + '&id=' + temple.artefactId;
        var strUrl = strRoot + "save-" + templedocs[currentDocId]["metadata"]["Type"].toLowerCase() +  ".xquery" + strQuery;
        var param = reportOnly ? "&reportOnly=true" : "&reportOnly=false"
        strUrl = strUrl + param
        request.open("POST", strUrl, true);
        request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        request.send('code=' + encodeURIComponent(temple.cmCode.getValue()));
        // showPanes(temple.show === 'hidden' ? 'right' : null);
    }

    function createExample(recursive='false') {
        busy("loading");
        var strQuery = '?&prefix=' + temple.strPrefix + '&id=' + templedocs[currentDocId]["metadata"]["Id"] + '&serialized=false&selectedOnly=false&recursive=' + recursive;
        var strUrl = "/decor/services/Template2Example" + strQuery;
        var request = new XMLHttpRequest();
        request.onload = function() {
            if (request.status == 200) {
                temple.cmInspector.setValue(request.responseText);
            } else if (request.readyState == 4 && request.status != 200) {
                temple.cmInspector.setValue(request.responseText);
            }
            busy("done");
        };
        request.open("POST", strUrl, true);
        request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        request.send('code=' + encodeURIComponent(temple.cmCode.getValue()));
        showPanes(temple.show === 'hidden' ? 'right' : null);
    }

    // INSPECTOR FUNCTIONS
    function fillInspector(strUrl) {
        try {
            temple.cmInspector.setValue('Retrieving...')
        } catch(e) {window.alert('Unexpected error: ' + e)}
        getArtefact(strUrl).then(
            function(response) {
                temple.cmInspector.setValue(response["xml"])
                addDecorClass(temple.cmInspector)
            }, function(error) {
                temple.cmInspector.setValue(error)
            })

        temple.history.push(strUrl);
        setInspectedUrl(strUrl);
        if (temple.history.length === 1) {
            document.getElementById("btnPrevious").disabled = true;
        } else {
            document.getElementById("btnPrevious").disabled = false;
        }
        if (temple.future.length === 0) {
            document.getElementById("btnNext").disabled = true;
        } else {
            document.getElementById("btnNext").disabled = false;
        }
    }

    function inspectPrevious() {
        if (temple.history.length >= 1) {
            // last history url to future
            temple.future.push(temple.history.pop());
            var strUrl = temple.history[temple.history.length - 1];
            setInspectedUrl(strUrl);
            fillInspector(strUrl);
            // remove the one which is just added again
            temple.history.pop();
        }
        showPanes(temple.show === 'hidden' ? 'right' : null);
    }

    function inspectNext() {
        if (temple.future.length >= 1) {
            // last future url to future
            temple.history.push(temple.future.pop());
            var strUrl = temple.history[temple.history.length - 1];
            setInspectedUrl(strUrl);
            fillInspector(strUrl);
            // remove the one which is just added again
            temple.history.pop();
        }
        showPanes(temple.show === 'hidden' ? 'right' : null);
    }

    function setInspectedUrl(strUrl) {
        // inspectedUrl is '' when previous was not an inspectable, otherwise the corresponding temple query
        if (strUrl.indexOf(strRoot + 'get-inspectable.xquery') === -1) {
            window.inspectedUrl = '';
            document.getElementById("btnEdit").disabled = true;
        } else {
            window.inspectedUrl = strUrl.replace('get-inspectable.xquery', 'temple.xquery') + "&format=xml";
            document.getElementById("btnEdit").disabled = false;
        }
    }

    // inspect will get what's at the cursor, see if it is or contains an OID, and try to retrieve it and load it in the inspector
    function editAtCursor(cm) {
        inspectAtCursor(cm, 'editor')
        return false
    }

    // inspect will get what's at the cursor, see if it is or contains an OID, and try to retrieve it and load it in the inspector
    function inspectAtCursor(cm, editor='inspector') {
        // Get token at cursor
        var tok = cm.getTokenAt(cm.getCursor());
        if (tok.type == "string") {
            var ln = cm.getCursor().line;
            var tokens = cm.getLineTokens(ln);
            // Dynamic if we cannot find another value
            var strFlexibility = "dynamic";
            // Find attribute effectiveDate or flexibility (on same line only) and get value
            for (var i = 1; i < tokens.length; i++) {
                if (tokens[i].type == "attribute" && (tokens[i].string == "effectiveDate" || tokens[i].string == "flexibility")) {
                    if (tokens.length >= i + 2) {
                        strFlexibility = tokens[i + 2].string.substring(1, tokens[i + 2].string.length - 1);
                    }
                }
            }
            cm.setSelection({
                line: ln,
                ch: tok.start
            }, {
                line: ln,
                ch: tok.end
            });
            // Get attribute string without the extra quotes
            var idOrName = tok.string.substring(1, tok.string.length - 1);
            // See if we're looking up a code. Assume 2 (=") chars before start of this token (i.e. code="123456")
            var pos = CodeMirror.Pos(ln, tok.start - 2);
            var prev = cm.getTokenAt(pos);
            if (prev.type == "attribute" && prev.string == "code") {
                if (cm.getLine(ln).includes("2.16.840.1.113883.6.96")) {
                    findInspectables("sct::" + idOrName)
                }
                if (cm.getLine(ln).includes("2.16.840.1.113883.6.1")) {
                    findInspectables("loinc::" + idOrName)
                }
            } else {
                // If it's not an OID, see if it is "name[OID]"
                if (!isOid(idOrName)) {
                    var innerId = tok.string.substring(tok.string.search("\\[") + 1, tok.string.search("\\]"));
                    if (isOid(innerId)) {
                        idOrName = innerId
                    }
                }
                // Build the url for Ajax
                var strQuery = "?id=" + idOrName + '&language=' + temple.strLanguage + '&effectiveDate=' + strFlexibility;
                // Only search with prefix when searching on name
                strQuery = (isOid(idOrName)) ? strQuery : strQuery + '&prefix=' + temple.strPrefix;
                if (editor === 'inspector') {
                    var strUrl = strRoot + "get-inspectable.xquery" + strQuery;
                    window.inspectedUrl = strRoot + 'temple.xquery' + strQuery;
                    fillInspector(strUrl)
                } else {
                    newDocument(strRoot + 'temple.xquery' + strQuery + '&format=xml')
                }
                temple.future = [];
            }
        } else {
            try {
                temple.cmInspector.setValue('Cursor is not on an id')
            } catch(e) {window.alert('Unexpected error: ' + e)}
        }
        showPanes(temple.show === 'hidden' ? 'right' : null);
        return true;
    }

    // find inspectables starting with string
    function findInspectables(strFind) {
        if (strFind === '?') {
            busy("loading");
            var request = new XMLHttpRequest();
            request.onload = function() {
                if (request.status == 200) {
                    temple.cmInspector.setValue(request.responseText)
                }
                showPanes(temple.show === 'hidden' ? 'right' : null);
                busy("done");
            }
            request.open("GET", '../script/readme.txt', true);
            request.send();
            return;
        }
        // Use scope from search for "scope::xxx" or "scope::pre:xxx" strings, else window prefix
        var scope = (strFind.indexOf("::") > -1) ? strFind.substring(0, strFind.indexOf("::")) : "*";
        var rest = (strFind.indexOf("::") > -1) ? strFind.substring(strFind.indexOf("::") + 2, strFind.length) : strFind;
        // Use prefix from search for "pre:xxx" strings, else window prefix
        var realPrefix = (rest.indexOf(":") > -1) ? rest.substring(0, rest.indexOf(":")) + "-" : temple.strPrefix;
        // Get search string for "pre:xxx" strings
        var realString = (rest.indexOf(":") > -1) ? rest.substring(rest.indexOf(":") + 1, rest.length) : rest;
        var strQuery = "?find=" + realString + '&language=' + temple.strLanguage + '&prefix=' + realPrefix + '&scope=' + scope;
        var strUrl = strRoot + "find-inspectables.xquery" + strQuery;
        window.inspectedUrl = '';
        fillInspector(strUrl);
        temple.future = [];
        showPanes(temple.show === 'hidden' ? 'right' : null);
        return false;
    }

    // PANES and UI
    // show = right, bottom, hide
    function showPanes(show = null) {
        var height = window.innerHeight - document.getElementById("header").offsetHeight - document.getElementById("cmMenu").offsetHeight - 40
        var width = window.innerWidth
        if (!show) {
            show = (!temple.show) ? "hidden" : temple.show
        } else {
            temple.show = show
        }
        if (show === "hidden") {
            temple.cmInspector.setSize(width, height * 0.05)
            temple.cmCode.setSize(width, height * 0.95)
            document.getElementById("btnHide").style.display = "none";
            document.getElementById("btnBottom").style.display = "none";
            document.getElementById("btnUp").style.display = "inline-block";
            document.getElementById("btnRight").style.display = "inline-block";
        } else if (show === 'bottom') {
            temple.cmInspector.setSize(width, 0.5 * height)
            temple.cmCode.setSize(width, 0.5 * height)
            document.getElementById("btnHide").style.display = "inline-block";
            document.getElementById("btnBottom").style.display = "none";
            document.getElementById("btnUp").style.display = "none";
            document.getElementById("btnRight").style.display = "inline-block";
        } else if (show === 'right') {
            temple.cmInspector.setSize(0.48 * width, height)
            temple.cmCode.setSize(0.48 * width, height)
            document.getElementById("btnHide").style.display = "inline-block";
            document.getElementById("btnBottom").style.display = "inline-block";
            document.getElementById("btnRight").style.display = "none";
            document.getElementById("btnUp").style.display = "none";
        }
    }

    function busy(status) {
        switch (status) {
            case "loading":
                loading += 1;
                document.getElementById('loadingBusy').style.display = 'inline';
                break;
            case "saving":
                document.getElementById('savingBusy').style.display = 'inline';
                document.getElementById('loadingDone').style.display = 'none';
                break;
            case "done":
                loading -= 1;
                if (loading === 0) {document.getElementById('loadingBusy').style.display = 'none'}
                break;
            case "savingdone":
                document.getElementById('savingBusy').style.display = 'none';
                document.getElementById('loadingDone').style.display = 'inline';
                break;
        }

    }

    // COMPLETIONS
    function getBaseIds(cm, option) {
        return getCompletion(cm, option, temple.completions.baseIds)
    }

    function getEffectiveDates(cm, option) {
        return getCompletion(cm, option, temple.completions.effectiveDates)
    }

    // This only works if id param is provided, and element id's are provided anyway, so may not be useful
    // function getElementIds(cm, option) {
    //     return getCompletion(cm, option, temple.completions.elementIds)
    // }

    function getProjectIds(cm, option) {
        return getCompletion(cm, option, temple.completions.projectIds)
    }

    function getProjectTemplates(cm, option) {
        return getCompletion(cm, option, temple.completions.projectTemplates)
    }

    function getProjectCodeSystems(cm, option) {
        return getCompletion(cm, option, temple.completions.projectCodeSystems)
    }

    function getProjectValuesets(cm, option) {
        return getCompletion(cm, option, temple.completions.projectValuesets)
    }
    
    function getProjectConcepts(cm, option) {
        return getCompletion(cm, option, temple.completions.projectConcepts)
    }

    function getCompletion(cm, option, completionType) {
        return new Promise(function(accept) {
            setTimeout(function() {
                var cursor = cm.getCursor()
                var comp = completionType
                return accept({
                    list: comp,
                    from: cursor,
                    to: cursor
                })
            }, 100)
        })
    }

    function getCdaName(cm) {
        return new Promise(function(accept) {
            setTimeout(function() {
                var cursor = cm.getCursor()
                var parentElement = 'top'
                var closedElements = 0;
                var comp = [];
                for (var i = cursor.line - 1; i >= 0; i--) {
                    var ln = cm.getLine(i);
                    // Look for a
                    if (ln.includes('<element') && (ln.includes('/>') || ln.includes('</element>'))) {
                        // single line element, skip
                    } else if (ln.includes('</element>')) {
                        // element close tag
                        closedElements += 1;
                    } else if (ln.includes('<element') && (closedElements > 0)) {
                        // element start tag without close
                        closedElements -= 1;
                    } else if (ln.includes('<element') && (closedElements == 0)) {
                        // The parent
                        var tokens = cm.getLineTokens(i);
                        // Find attribute effectiveDate or flexibility (on same line only) and get value
                        for (var j = 1; j < tokens.length; j++) {
                            if (tokens[j].type == "attribute" && (tokens[j].string == "effectiveDate" || tokens[j].string == "name")) {
                                if (tokens[j + 2]) {
                                    parentElement = tokens[j + 2].string
                                }
                                parentElement = parentElement.replace('hl7:', '')
                                parentElement = parentElement.substring(1, parentElement.length - 1)
                                if (cda_tags[parentElement]) {
                                    for (tag in cda_tags[parentElement].children) {
                                        comp.push('"hl7:' + cda_tags[parentElement].children[tag] + '"')

                                    }
                                }
                                break;
                            }
                        }
                    }
                }
                return accept({
                    list: comp,
                    from: cursor,
                    to: cursor
                })
            }, 100)
        })
    }

    // CODEMIRROR STUFF
    function indentXml(cm) {
        for (var i = 0, e = cm.lineCount(); i < e; ++i) {
            cm.indentLine(i);
            addDecorClassForLine(cm, i);
        }
    }

    function completeAfter(cm, pred) {
        var tags;
        if (temple.strTempleType == 'valueSet') {
            tags = tags_terminology_valueset
        } else
        if (temple.strTempleType == 'codeSystem') {
            tags = tags_terminology_codesystem
        } else
        if (temple.strTempleType == 'conceptMap') {
            tags = tags_terminology_conceptmap
        } else {
            tags = tags_rules
        }
        if (!pred || pred()) setTimeout(function() {
            if (!cm.state.completionActive)
                CodeMirror.showHint(cm, CodeMirror.hint.xml, {
                    schemaInfo: tags,
                    completeSingle: false
                });
        }, 100);
        addDecorClassForLine(cm, cm.getCursor().line);
        return CodeMirror.Pass;
    }

    function completeIfAfterLt(cm) {
        return completeAfter(cm, function() {
            var cur = cm.getCursor();
            return cm.getRange(CodeMirror.Pos(cur.line, cur.ch - 1), cur) == "<";
        });
    }

    function completeIfInTag(cm) {
        return completeAfter(cm, function() {
            var tok = cm.getTokenAt(cm.getCursor());
            if (tok.type == "string" && (!/['"]/.test(tok.string.charAt(tok.string.length - 1)) || tok.string.length == 1)) return false;
            var inner = CodeMirror.innerMode(cm.getMode(), tok.state).state;
            return inner.tagName;
        });
    }

    function showMetadata() {
        var metatxt = '';
        for (var md in templedocs[currentDocId]["metadata"]) {
            metatxt += md + ": " + templedocs[currentDocId]["metadata"][md] + "\n";
        }
        temple.cmInspector.setValue(metatxt)
    showPanes(temple.show === 'hidden' ? 'right' : null);
    }

    // DECOR STYLING
    function addDecorClass(cm) {
        for (var i = 0, e = cm.lineCount(); i < e; ++i) {
            addDecorClassForLine(cm, i);
        }
    }

    function addDecorClassForLine(cm, i) {
        var strClassName = '';
        var tokens = cm.getLineTokens(i);
        var marks = cm.findMarks({
            line: i,
            ch: 0
        }, {
            line: i,
            ch: cm.getLine(i).length
        });
        for (var k = 0; k < marks.length; k++) {
            marks[k].clear();
        }
        for (var j = 0; j < tokens.length; j++) {
            if (tokens[j].type == "attribute") {
                if ((tokens[j].string == "ref") || (tokens[j].string == "valueSet") || (tokens[j].string == "id") || (tokens[j].string == "contains") || (tokens[j].string == "code")) {
                    strClassName = 'decor-ref';
                }
                // codes from Snomed and LOINC are inspectable
                else if ((tokens[j].string == "code") && (cm.getLine(i).includes("2.16.840.1.113883.6.96") || cm.getLine(i).includes("2.16.840.1.113883.6.1"))) {
                    strClassName = 'decor-ref';
                } else {
                    strClassName = '';
                }
                if (tokens[j].string == "name") {
                    switch (tokens[j].state.tagName) {
                        case "element":
                            strClassName = "decor-element-name";
                            break;
                        case "template":
                            strClassName = "decor-template-name";
                            break;
                        case "attribute":
                            strClassName = "decor-attribute-name";
                            break;
                        default:
                            strClassName = '';
                    }
                }
                if (tokens[j].string == "value") {
                    switch (tokens[j].state.tagName) {
                        case "attribute":
                            strClassName = "decor-attribute-value";
                            break;
                        default:
                            strClassName = '';
                    }
                }
                if (tokens[j].string == "test") {
                    switch (tokens[j].state.tagName) {
                        case "assert":
                            strClassName = "decor-assert-test";
                            break;
                        default:
                            strClassName = '';
                    }
                }
                if (tokens.length > j + 2 && tokens[j + 2].type == "string" && strClassName !== '') {
                    cm.markText({
                        line: i,
                        ch: tokens[j + 2].start
                    }, {
                        line: i,
                        ch: tokens[j + 2].end
                    }, {
                        className: strClassName
                    });
                }
            }
        }
    }

    // templedocs and GET
    // getArtefact gets the document content
    function getArtefact(strUrl) {
        return new Promise(function(resolve, reject) {
            busy('loading');
            var request = new XMLHttpRequest();
            var metadata = {};
            request.onload = function() {
                if (request.status == 200) {
                    metadata["ReadOnly"] = request.getResponseHeader("TempleReadOnly")
                    metadata["User"] = request.getResponseHeader("TempleUser")
                    metadata["Type"] = request.getResponseHeader("TempleType")
                    metadata["Name"] = request.getResponseHeader("TempleName")
                    metadata["DisplayName"] = request.getResponseHeader("TempleDisplayName")
                    metadata["Id"] = request.getResponseHeader("TempleId")
                    metadata["EffectiveDate"] = request.getResponseHeader("TempleEffectiveDate")
                    metadata["DatasetId"] = request.getResponseHeader("TempleDatasetId")
                    metadata["DatasetEffectiveDate"] = request.getResponseHeader("TempleDatasetEffectiveDate")
                    metadata["Url"] = strUrl
                    resolve({"metadata": metadata, "xml": request.responseText});
                 }
                 else {
                     reject('Error, got status: ' + request.status + '\nbody: ' + request.responseText + '\nfor query: ' + strUrl);
                 }
                 busy('done');
            }
            request.open("GET", strUrl, true);
            request.send();
        });
    }

    function newArtefact(type='template') {
        var strUrlArtefact = "../modules/temple.xquery?prefix=" + temple.strPrefix + "&type=" + type + '&format=xml'
        newDocument(strUrlArtefact)
    }

    function cloneArtefact(type='template') {
        var strUrlArtefact = "../modules/temple.xquery?prefix=" + temple.strPrefix + '&id=' + temple.artefactId + "&type=" + type + '&clone=true&format=xml'
        newDocument(strUrlArtefact)
    }

    function newDocument(strUrl=null) {
        var newDocId = ++lastDocId;
        var txtNode = '<span id="btnDoc' + newDocId + '" class="tabButton" onclick="temple.switchDocument(' + newDocId + ')">Code' + newDocId + '<span id="changed' + newDocId + '">*</span></span>';
        var tabs = document.getElementById("tabList");
        tabs.insertAdjacentHTML("afterbegin", txtNode);
        document.getElementById('changed' + newDocId).style.display= 'none'
        if (strUrl) {
            getArtefact(strUrl).then(
                function(response) {
                    templedocs[newDocId] = CodeMirror.Doc(response["xml"], "xml");
                    templedocs[newDocId]["metadata"] = response["metadata"];
                    switchDocument(newDocId)
                    var newName = response["metadata"]["DisplayName"] === "" ? "New" + newDocId : response["metadata"]["DisplayName"]
                    document.getElementById('btnDoc' + newDocId).innerHTML = newName +  '<span id="changed' + newDocId + '">*</span>'
                    document.getElementById('changed' + newDocId).style.display= 'none'
               }, function(error) {
                    templedocs[newDocId] = CodeMirror.Doc(error, "xml");
                    switchDocument(newDocId)
                })
            } else {
                templedocs[newDocId] = CodeMirror.Doc("<doc/>", "xml");
                switchDocument(newDocId)
            }
    }

    function activateTab(docId) {
        var tablinks = document.getElementsByClassName("tabButton");
        for (var i = 0; i < tablinks.length; i++) {
          tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        var activeTab = document.getElementById('btnDoc' + docId)
        activeTab.style.display = "block";
        activeTab.className += " active";
    }

    function switchDocument(newDocId) {
        var old;
        old = temple.cmCode.swapDoc(templedocs[newDocId]);
        addDecorClass(temple.cmCode);
        // Save the old doc, unless doc is removed
        if (currentDocId != 0) {
            templedocs[currentDocId] = old;
        }
        currentDocId = newDocId;
        temple.strTempleType = templedocs[newDocId].metadata.Type;
        activateTab(newDocId)
        if (templedocs[newDocId]["metadata"]["ReadOnly"] === "true") {
            temple.cmCode.setOption("readOnly", true)
            document.getElementById('locked').style.display = 'inline';
        } else {
            temple.cmCode.setOption("readOnly", false)
            document.getElementById('locked').style.display = 'none';
        }
    }

    function closeDocument(rmDocId=null) {
        if (rmDocId === null) {
            rmDocId = currentDocId;
            currentDocId = 0;
        }
        delete templedocs[rmDocId];
        var element = document.getElementById('btnDoc' + rmDocId);
        element.parentNode.removeChild(element);
        // if last doc closed, empty Temple
        if (Object.keys(templedocs).length === 0) {temple.cmCode.setValue('')}
        for (var docId in templedocs) {
            // Move to first doc
            temple.cmCode.swapDoc(templedocs[docId]);
            switchDocument(docId);
            break;
        }
    }

    // EDITORS
    // Makes a new CodeMirror instance. Does not populate the XML content.
    function createEditor(id, editable, schema) {
        var tags;
        if (temple.strTempleType == 'valueSet') {
            tags = tags_terminology_valueset
        } else
        if (temple.strTempleType == 'codeSystem') {
            tags = tags_terminology_codesystem
        } else
        if (temple.strTempleType == 'conceptMap') {
            tags = tags_terminology_conceptmap
        } else {
            tags = tags_rules
        }
        if (schema) {
            tags = temple.schemas['decor']
        }
        var editor = CodeMirror.fromTextArea(document.getElementById(id), {
            mode: "xml",
            lineNumbers: true,
            autoCloseTags: true,
            foldGutter: true,
            readOnly: !editable,
            gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
        });
        editor.setOption("extraKeys", {
            "Ctrl-O": function(cm) { editAtCursor(cm); },
                "'<'": completeAfter,
                "'/'": completeIfAfterLt,
                "' '": completeIfInTag,
                "'='": completeIfInTag,
                "Ctrl-Space": function(cm) {
                    CodeMirror.showHint(cm, CodeMirror.hint.xml, {
                        schemaInfo: tags
                    });
                },
                "Ctrl-I": inspectAtCursor,
                "Alt-I": editAtCursor,
                "Ctrl-K": editAtCursor,
                "Cmd-I": inspectAtCursor,
                "F11": function(cm) {
                    cm.setOption("fullScreen", !cm.getOption("fullScreen"));
                },
                "Esc": function(cm) {
                    if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
                }
            })
        CodeMirror.commands.save = function () {
            validateOrSave(reportOnly=false)
        };
        indentXml(editor);
        addDecorClass(editor);
        return editor;
    }

    // This is for (re)loading projectTypes.
    function getProjectTypes(prefix, language, datasetId, datasetEffectiveDate) {
        busy('loading');
        var request = new XMLHttpRequest();
        var strUrlTypes = '../modules/get-project-types.xquery?format=json&prefix=' + prefix + '&language=' + language
        if (datasetId != undefined) {strUrlTypes = strUrlTypes + '&datasetId=' + datasetId + '&datasetEffectiveDate=' + datasetEffectiveDate} 
        request.onload = function() {
            if (request.status == 200) {
                var json = JSON.parse(request.responseText);
                if (json != null) {
                    temple.completions = json
                    temple.schemas['decor'] = json
                    var dropdown = document.getElementById("datasetSelector");
                    for (var i=0; i < json['projectDatasets'].length; i++) {
                        var optn = document.createElement("OPTION");
                        optn.text = json['projectDatasets'][i]['displayText'];
                        optn.value = json['projectDatasets'][i]['text'];
                        dropdown.options.add(optn);
                    }
                }
            } else {
                temple.cmCode.setValue('Error, got status: ' + request.status + '\nbody: ' + request.responseText + '\nfor query: ' + strUrlTypes);
            }
            busy("done");
        }
        request.open("GET", strUrlTypes, true);
        request.send();
    }

    // creates a new Temple instance, retrieves projectTypes, then calls createEditor
    function createTemple() {
        busy("loading");
        // findInspectables('?');
        window.inspectedUrl = '';
        // var request = new XMLHttpRequest();
        temple.cmInspector = createEditor("inspector", "false");
        temple.cmCode = createEditor("code", "true");
        temple.cmCode.on("changes", function(cm, changes) {
            !currentDocId ? {} :document.getElementById('changed' + currentDocId).style.display= ''
        });
        temple.cmCode.addPanel(document.getElementById('tabList'), {
            position: 'top'
        })
        temple.cmInspector.addPanel(document.getElementById('inspectorMenu'), {
            position: 'top'
        })
        if (temple.artefactId) {
            var strUrlArtefact = "../modules/temple.xquery?prefix=" + temple.strPrefix + "&id=" + temple.artefactId + '&effectiveDate=' + temple.artefactEffectiveDate + '&format=xml&mode=' + temple.strMode
            newDocument(strUrlArtefact)
        } else {temple.cmCode.setValue('Search for artefacts in Inspector')}
        getProjectTypes(temple.strPrefix, temple.strLanguage)
        temple.showPanes('hidden')
        busy('done')
        return temple.cmCode
    }

    return {
        createEditor,
        createTemple,
        newDocument,
        newArtefact,
        cloneArtefact,
        closeDocument,
        showPanes,
        showMetadata,
        switchDocument,
        findInspectables,
        inspectPrevious,
        inspectNext,
        indentXml,
        createExample,
        validateOrSave,
        history,
        future,
        templedocs,
        schemas,
        getCdaName,
        getProjectIds,
        getProjectConcepts,
        getProjectTemplates,
        getProjectValuesets,
        getProjectTypes,
        getProjectCodeSystems,
        getEffectiveDates,
        getBaseIds
    };
})();
