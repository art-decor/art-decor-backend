xquery version "3.0";

import module namespace sm      = "http://exist-db.org/xquery/securitymanager";
import module namespace xmldb   = "http://exist-db.org/xquery/xmldb";
import module namespace repo    = "http://exist-db.org/xquery/repo";
declare namespace cfg           = "http://exist-db.org/collection-config/1.0";
(: The following external variables are set by the repo:deploy function :)

(: file path pointing to the exist installation directory :)
declare variable $home external;
(: path to the directory containing the unpacked .xar package :)
declare variable $dir external;
(: the target collection into which the app is deployed :)
declare variable $target external;
(:install path for art (/db, /db/apps), no trailing slash :)
declare variable $root := repo:get-root();

(: helper function for creating top level database collection and index definitions required for Art webapplication :)
declare %private function local:createTopCollections() {
    let $dataIndex :=
        <collection xmlns="http://exist-db.org/collection-config/1.0">
            <index xmlns:hl7="urn:hl7-org:v3" xmlns:xs="http://www.w3.org/2001/XMLSchema">
                <fulltext default="none" attributes="false"/>
                <lucene>
                    <analyzer class="org.apache.lucene.analysis.standard.StandardAnalyzer">
                        <param name="stopwords" type="org.apache.lucene.analysis.util.CharArraySet"/>
                    </analyzer>
                    <text qname="concept"/>
                </lucene>
                <create qname="@id" type="xs:string"/>
                <create qname="@code" type="xs:string"/>
            <!-- Voor zoeken in LOINC-Snomed map -->
                <create qname="@system" type="xs:string"/>
            </index>
        </collection>    
    let $labIndex :=
        <collection xmlns="http://exist-db.org/collection-config/1.0">
            <index xmlns:hl7="urn:hl7-org:v3" xmlns:xs="http://www.w3.org/2001/XMLSchema">
                <fulltext default="none" attributes="false"/>
                <lucene>
                    <analyzer class="org.apache.lucene.analysis.standard.StandardAnalyzer">
                        <param name="stopwords" type="org.apache.lucene.analysis.util.CharArraySet"/>
                    </analyzer>
                    <text qname="lab_concept"/>
                    <ignore qname="elem"/>
                </lucene>
                <create qname="@loinc_num" type="xs:string"/>
                <create qname="@status" type="xs:string"/>
                <create qname="@user" type="xs:string"/>
                <create qname="@id" type="xs:string"/>
                <create qname="component" type="xs:string"/>
                <create qname="property" type="xs:string"/>
                <create qname="timing" type="xs:string"/>
                <create qname="system" type="xs:string"/>
                <create qname="scale" type="xs:string"/>
                <create qname="method" type="xs:string"/>
                <create qname="class" type="xs:string"/>
                <!-- Voor zoeken op name='PanelType' -->
                <create qname="@name" type="xs:string"/>
                <!-- Voor error/@code -->
                <create qname="@code" type="xs:string"/>
            <!-- Voor zoeken op nl-NL concepten -->
                <create qname="@language" type="xs:string"/>
            </index>
        </collection>
    let $pubIndex :=
        <collection xmlns="http://exist-db.org/collection-config/1.0">
            <index xmlns:hl7="urn:hl7-org:v3" xmlns:xs="http://www.w3.org/2001/XMLSchema">
                <fulltext default="none" attributes="false"/>
                <create qname="@effectiveDate" type="xs:string"/>
                <create qname="@loinc_num" type="xs:string"/>
                <create qname="@id" type="xs:string"/>
                <create qname="@status" type="xs:string"/>
                <create qname="Loinc" type="xs:string"/>
            </index>
        </collection>
    let $panelIndex :=
        <collection xmlns="http://exist-db.org/collection-config/1.0">
            <index xmlns:xs="http://www.w3.org/2001/XMLSchema">
                <create qname="Id" type="xs:string"/>
                <create qname="ParentId" type="xs:string"/>
                <create qname="Loinc" type="xs:string"/>
            </index>
        </collection>
    return (
        (:/db/apps collections:)
        for $coll in ('lab-data/data')
        return (
            if (xmldb:collection-available(concat($root,$coll))) then () else (
                xmldb:create-collection($root,$coll)
            )
        )
        ,
        for $coll in ('lab-data/data/lab_concepts')
        return (
            if (xmldb:collection-available(concat($root,$coll))) then () else (
                xmldb:create-collection($root,$coll)
            )
        )
        ,
        (:/db/apps collections:)
        for $coll in ('lab-data/data/local_panels')
        return (
            if (xmldb:collection-available(concat($root,$coll))) then () else (
                xmldb:create-collection($root,$coll)
            )
        )
        ,
        (:/db/system collections:)
        for $coll in ('lab-data')
        return (
            if (xmldb:collection-available(concat('/db/system/config',$root,$coll))) then () else (
                xmldb:create-collection(concat('/db/system/config',$root),$coll)
            )
        )
        ,
        (:/db/system collections:)
        for $coll in ('lab-data/data')
        return (
            if (xmldb:collection-available(concat('/db/system/config',$root,$coll))) then () else (
                xmldb:create-collection(concat('/db/system/config',$root),$coll)
            )
        )
        ,
        (:/db/system collections:)
        for $coll in ('lab-data/data/lab_concepts')
        return (
            if (xmldb:collection-available(concat('/db/system/config',$root,$coll))) then () else (
                xmldb:create-collection(concat('/db/system/config',$root),$coll)
            )
        )
        ,
        (:/db/system collections:)
        for $coll in ('lab-data/data/local_panels')
        return (
            if (xmldb:collection-available(concat('/db/system/config',$root,$coll))) then () else (
                xmldb:create-collection(concat('/db/system/config',$root),$coll)
            )
        )
        ,
        (:/db/system collections:)
        for $coll in ('lab-data/publications')
        return (
            if (xmldb:collection-available(concat('/db/system/config',$root,$coll))) then () else (
                xmldb:create-collection(concat('/db/system/config',$root),$coll)
            )
        )
        ,
        (:== indexes ==:)
        xmldb:store(concat('/db/system/config',$root,'/lab-data/data/lab_concepts'),'collection.xconf',$labIndex)
        ,
        xmldb:store(concat('/db/system/config',$root,'/lab-data/data/local_panels'),'collection.xconf',$panelIndex)
        ,
        xmldb:store(concat('/db/system/config',$root,'/lab-data/publications'),'collection.xconf',$pubIndex)
        ,
        xmldb:store(concat('/db/system/config',$root,'/lab-data/data'),'collection.xconf',$dataIndex)
        ,
        xmldb:reindex(concat($root,'lab-data'))
    )
};

(: helper function for creating database groups required for Art webapplication :)
declare %private function local:setupUsersAndGroups() {

   if (sm:group-exists('lab')) then () else (
      sm:create-group('lab','admin','Group for Lab Terminology')
   ),
   if (sm:group-exists('lab-admin')) then () else (
      sm:create-group('lab-admin','admin','Group for Lab Terminology')
   )
};

let $update := local:createTopCollections()
let $update := local:setupUsersAndGroups()

return ()
