xquery version "3.0";

module namespace labterm            = "http://art-decor.org/ns/labterm";
import module namespace console     = "http://exist-db.org/xquery/console";

declare variable $labterm:root              := repo:get-root();
declare variable $labterm:strTerminologyData := concat($labterm:root,'terminology-data');
declare variable $labterm:strLoincData      := concat($labterm:strTerminologyData,'/loinc-data/data/');
(: Do not use strLoincPanels directly but use getFlatPanel instead :)
declare variable $labterm:strLoincPanels    := concat($labterm:strTerminologyData,'/loinc-data/panels/');
declare variable $labterm:strLab            := concat($labterm:root,'lab');
declare variable $labterm:strLabData        := concat($labterm:root,'lab-data/data');
declare variable $labterm:labConcepts       := collection(concat($labterm:strLabData, '/lab_concepts'));
declare variable $labterm:strLocalPanels    := concat($labterm:strLabData, '/local_panels');
declare variable $labterm:localPanels       := collection($labterm:strLocalPanels);
declare variable $labterm:strLabDataLog     := concat($labterm:root,'lab-data/log');
declare variable $labterm:LabDataLog        := collection($labterm:strLabDataLog);
declare variable $labterm:strLabPublications    := concat($labterm:root,'lab-data/publications');
declare variable $labterm:labLoincSnomedMapping := doc(concat($labterm:strLabData, '/loincsystem-to-snomed.xml'))/map;
declare variable $labterm:labUnits          := doc(concat($labterm:strLabData, '/units.xml'))/units;
declare variable $labterm:labOrdinals       := doc(concat($labterm:strLabData, '/ordinals.xml'))/ordinals;
declare variable $labterm:labNominals       := doc(concat($labterm:strLabData, '/nominals.xml'))/nominals;
declare variable $labterm:loincConcepts     := doc(concat($labterm:root, 'terminology-data/loinc-data/data/universal/', 'LOINC_DB_with_LinguisticVariants.xml'))/loinc_db;
declare variable $labterm:loincPrerelease   := doc(concat($labterm:strLabData, '/prerelease.xml'));
declare variable $labterm:strLabConcept     := concat($labterm:strLabData,'/lab_concepts');

(:~ Get current user name. effective user name if available or real user name otherwise. The function labterm:get-current-user() was removed 
:   in eXist-db 5.0 and replaced with sm:id(). By centralizing the new slightly more complicated way of doing this, you can avoid making 
:   mistakes throughout the code base
:   @return user name string.
:   @since 2019-11-11
:)
declare function labterm:get-current-user() as xs:string? {
    sm:id()//sm:real/sm:username/string()
};
(:~
This function gets the errors for a concept.

Update the errors table when code changes at:
https://informatiestandaarden.nictiz.nl/wiki/Landingspagina_Labcodeset#Errors
:)
declare function labterm:getErrors($labConcept as node(), $language as xs:string) as node()* {
    let $loinc_num      := $labConcept/concept/@loinc_num/string()
    let $loincConcept   := $labterm:loincConcepts//concept[@loinc_num = $loinc_num]
    let $errors := ()

    let $errors :=
        if($loincConcept or $labConcept/concept/@status="PRERELEASE")
        then $errors
        else ($errors, <error src="checkConcept" code="MISSING" dateTime="{fn:current-dateTime()}">LOINC concept {$loinc_num} is not present in LOINC</error>)
    let $errors :=
        if($loincConcept and $loincConcept[@status = 'ACTIVE']) 
        then $errors
        (: Warning, status TRIAL is not an error in itself :)
        else if($loincConcept and $loincConcept[@status = 'TRIAL']) 
        then ($errors, <warning src="checkConcept" code="TRIAL" dateTime="{fn:current-dateTime()}">LOINC concept {$loinc_num} has LOINC status 'TRIAL'</warning>)
        else if($labConcept/concept/@status="PRERELEASE")
        then $errors
        else if($labConcept/@status = 'retired') 
        then $errors
        else ($errors, <error src="checkConcept" code="STATUS" dateTime="{fn:current-dateTime()}">LOINC concept {$loinc_num} has status {$loincConcept/@status/string()} and lab concept has status {$labConcept/@status/string()}</error>)
    let $errors :=
        (: See BITS LCB-117, check on OrdQn is disabled for the time being :)
        (: Scale SemiQn has no checks either, see LCB-451 :)
        (: Both may have to be stricter in the future :)
        (: Scale Qn must have a unit, except panel types. :)
        if($labConcept[concept/scale!='Qn' or units/unit or concept/elem/@name='PanelType'])
        then $errors
        else ($errors, <error src="checkConcept" code="NOUNIT" dateTime="{fn:current-dateTime()}">LOINC concept {$loinc_num} with scale {$labConcept/concept/scale/text()} has no unit</error>)
    let $errors :=
        ($errors,
        for $unitRef in $labConcept//unit/@ref
        return 
            if ($labterm:labUnits/unit[@id=$unitRef])
            then ()
            else ($errors, <error src="checkConcept" code="UNITREF" dateTime="{fn:current-dateTime()}">LOINC concept {$loinc_num} has unit ref {$unitRef/string()} which is missing in units.</error>)
        )
    let $errors :=
        ($errors,
        for $valueSetRef in $labConcept//valueSet/@ref
        return 
            if ($labterm:labOrdinals//valueSet[@id=$valueSetRef])
            then ()
            else ($errors, <error src="checkConcept" code="VALUESETREF" dateTime="{fn:current-dateTime()}">LOINC concept {$loinc_num} has valueSet ref {$valueSetRef/string()} which is missing in valueSets.</error>)
        )
    let $errors :=
        if($labConcept//concept[@language=$language]/component) 
        then $errors
        else ($errors, <warning src="checkConcept" code="NOCOMP" dateTime="{fn:current-dateTime()}">LOINC concept {$loinc_num} has no component in {$language}</warning>)
    let $errors :=
        if($labConcept/concept/@status != "PRERELEASE")
        then $errors
        else ($errors, <warning src="checkConcept" code="PRERELEASE" dateTime="{fn:current-dateTime()}">LOINC concept {$loinc_num} has status PRERELEASE</warning>)
    let $errors :=
        if ($labterm:labLoincSnomedMapping//material[@system=$labConcept/concept/system])
        then $errors
        else ($errors, <error src="checkConcept" code="NOMAPPING" dateTime="{fn:current-dateTime()}">No LOINC-Snomed mapping for {$loinc_num} for system {$labConcept/concept/system/string()}</error>)
    let $panel_check :=
        if ($labConcept/concept/elem/@name='PanelType')
        then
            let $panelTree  := labterm:getLabPanelById($loinc_num, 'nl-NL')
            let $check :=  
                if (not($panelTree))
                then <error src="checkConcept" code="NOPANEL" dateTime="{fn:current-dateTime()}">LOINC concept {$loinc_num} is of type panel, but panel cannot be found</error>
                else if ($panelTree//lab_concept[not(@panelMember='removed') and @status = 'potential'])
                then <error src="checkConcept" code="PANELMEMBER" dateTime="{fn:current-dateTime()}">LOINC concept {$loinc_num} is a panel and some panel members are not part of the Labcodeset</error>
                (: panel members should be active, except for the panel itself, and removed members :)
                else if ($panelTree//lab_concept[not(concept[@loinc_num=$loinc_num])][not(@panelMember='removed')][@status != 'active'])
                then <error src="checkConcept" code="PANELSTATUS" dateTime="{fn:current-dateTime()}">LOINC concept {$loinc_num} is a panel with status 'active' and some panel members are not 'active'</error>
                else 'OK'
            return $check
        else
            'OK'
    let $errors :=
        if($panel_check = 'OK') 
        then $errors
        else ($errors, $panel_check)

    return $errors
};

(:~
This function will check a concept for errors.
- will insert <errors> element with errors when there are errors
- will delete <errors> if no errors are found
An errors element may contain multiple errors.

If lab_concept/@status='active' status will be set to 'update' if there are errors.
:)
declare function labterm:checkConcept($loinc_num as xs:string?, $language as xs:string) as node() {
    let $labConcept     := $labterm:labConcepts/lab_concept[concept[@loinc_num = $loinc_num]]
    let $errors         := labterm:getErrors($labConcept, $language)
    let $do := update delete $labConcept/errors
    let $do := 
        if ($errors) 
        then (
            update insert <errors loinc_num="{$loinc_num}">{$errors}</errors> into $labConcept
            ,
            (: no status change for warnings :)
            if ($labConcept/@status = 'active' and count($errors[local-name() = 'error']) >= 1) 
            then labterm:updateStatus('update', $loinc_num,            'system')
            else ()
        ) else ()
    return if ($errors) then <result loinc_num="{$loinc_num}" status="ERRORS">{$errors}</result> else <result loinc_num="{$loinc_num}" status="OK"/>
};

declare function labterm:checkAllConcept($statusList as xs:string*) as node()* {
    let $concepts := if ($statusList) then $labterm:labConcepts/lab_concept[@status=$statusList] else $labterm:labConcepts/lab_concept
    let $panels := $concepts[concept/elem[@name='PanelType']]
    let $nonpanels := $concepts[not(concept/elem[@name='PanelType'])]
    return
    <lab_concepts> {
        for $labConcept in $nonpanels
        return labterm:checkConcept($labConcept/concept/@loinc_num, 'nl-NL')
        ,
        for $labConcept in $panels
        return labterm:checkConcept($labConcept/concept/@loinc_num, 'nl-NL')
        }
    </lab_concepts>
};

(: This function will append the log in /lab-data/log/{$logFileName}.xml if the file is already present. Otherwise will create the file and then write the log provided as parameter :)
declare function labterm:appendLog($logData as node()) as xs:boolean {
    let $logFileDir := concat($labterm:strLabDataLog,  '/')
    let $logFileName := concat('transactions-',substring(xs:string(current-dateTime()), 1, 10),'.xml')
    let $createLogFile :=
    if (not(doc-available(concat($logFileDir,$logFileName)))) then (
        xmldb:store($logFileDir, $logFileName, <logs/>),
        sm:chmod(xs:anyURI(concat($logFileDir,$logFileName)),'rwxrwxr--'),
        sm:clear-acl(xs:anyURI(concat($logFileDir,$logFileName)))
    )
    else()
    let $log := doc(concat($logFileDir, $logFileName))/logs   
    let $appendLog :=  
            if($log and sm:has-access(xs:anyURI(concat($logFileDir, $logFileName)),'w')) then
                update insert $logData into $log
            else()    
    return true()    
};
(: This function adds active concepts in publication file and updates the reference of given node with actual node from the database. :)
declare function labterm:updatePublication($publicationFilePath as xs:string, $currentDate as xs:string) as element()? {
    let $publication := doc($publicationFilePath)/publication
    let $update := update replace $publication/@effectiveDate with $currentDate
    let $update := 
        (: getOnlyLabCoonceptById will get the last active concept, if any, for update concepts :)
        for $lab_concept in $labterm:labConcepts/lab_concept[@status = ('update', 'active', 'retired')]
        let $content := labterm:getOnlyLabConceptById($lab_concept/concept/@loinc_num, true())/lab_concept 
        return if ($content) then update insert $content into $publication/lab_concepts else () 
    let    $update := update insert $labterm:labLoincSnomedMapping/.. into $publication
    let $update := update insert $labterm:labUnits/.. into $publication
    let $update := update insert $labterm:labOrdinals/.. into $publication
    let $update := update insert $labterm:labNominals/.. into $publication
    let $panels := 
    <panels>{
        for $concept in $publication//lab_concept/concept[elem[@name="PanelType"]='Panel']
        return labterm:getFinalLabPanelTreeById($concept/@loinc_num, 'nl=NL')
    }</panels>
    let $update := update insert $panels into $publication
    return $update
};

(: This function changes the status of the concept having concept id passed in second parameter with status passed in first paramter.  :)
declare function labterm:changeStatus($status as xs:string, $loincId as xs:string) as node() {
    (: First get old status, as it may change to 'update' when there are errors :)
    let $concept := labterm:getOnlyLabConceptById($loincId)/lab_concept
    let $oldStatus := $concept/@status/string()
    (: Check concept to make sure all errors are up to datae or no errors present :) 
    let $check := labterm:checkConcept($loincId, 'nl-NL')
    (: Get the concept with updated errors again :)
    let $concept := labterm:getOnlyLabConceptById($loincId)/lab_concept
    
    let $allowed := 
        if (not($oldStatus)) then concat('No concept ', $loincId)
        else if ($oldStatus = 'initial' and ($status = 'draft' or $status = 'rejected')) then 'OK'
        else if ($oldStatus = 'draft' and ($status = 'active' or $status = 'rejected')) then 'OK'
        else if ($oldStatus = 'update' and ($status = 'active' or $status = 'retired')) then 'OK'
        else if ($oldStatus = 'active' and ($status = 'update' or $status = 'retired')) then 'OK'
        else if ($oldStatus = 'retired' and $status = 'draft' and labterm:isLabAdmin()) then 'OK'
        else if ($oldStatus = 'retired' and $status = 'update' and labterm:isLabAdmin()) then 'OK'
        else concat('You cannot change ', $oldStatus, ' to ', $status, ' for concept ', $loincId)
    let $allowed := 
        if ($allowed != 'OK') then $allowed
        else 
            (: Admin may make concepts active if only error is no translation :)
            if ($status != 'active')
            then $allowed
            else if (not($concept//error))
            then $allowed
            else concat('You cannot make concept ', $loincId, ' active when it has errors.')
    (: For panels, only allow active when all in panel are active too :)
    let $panelAllowed :=
        if ($status != 'active' or not($concept/concept/elem[@name='PanelType']))
        then true()
        else
            let $panelTree  := labterm:getLabPanelById($loincId, 'nl-NL')
            return 
                (: Check if other concepts than the current are not active :)
                if ($panelTree//lab_concept[concept/@loinc_num!=$loincId][(@status != 'active' and @panelMember != 'removed')])
                then false()
                else true()
    (: If there are active Panel parents, do not allow making this concept non-active :)
    let $panelParentsOk :=
        if ($status != 'active' and count(labterm:getActiveLabPanelParents($loincId)) > 0)
        then false()
        else true()
    let $update := 
        if ($allowed = 'OK' and $panelAllowed and $panelParentsOk) then 
            if (labterm:updateStatus($status, $loincId)) then 'OK' else 'Unexpected error'
        else 
            if (not($panelAllowed))
            then 'Status change for panel is not allowed, not all members are active.'
            else if (not($panelParentsOk))
            then concat('Changing a concept is not allowed if it has an active parent (see parent: ', string-join(labterm:getActiveLabPanelParents($loincId), ' '), ') ')
            else if ($allowed != 'OK') 
            then $allowed 
            else 'Status change is not allowed.'
    let $concept := labterm:getOnlyLabConceptById($loincId)/lab_concept
    let $result := <result status="{if ($update = 'OK') then 'OK' else 'NOK'}" message="{$update}">{$concept}</result>
    return $result
};

(: This function checks if the current user is of lab group or not. :)
declare function labterm:isLabUser() as xs:boolean {
    let $user := labterm:get-current-user()
    let $groups := sm:get-user-groups($user)
    return if ($groups= 'lab') then true() else false()
};

(: This function checks if the current user is of lab group or not. :)
declare function labterm:isLabAdmin() as xs:boolean {
    let $user := labterm:get-current-user()
    let $groups := sm:get-user-groups($user)
    return if ($groups= 'lab-admin') then true() else false()
};

(: This function checks if the current user is of lab group or not. :)
declare function labterm:isDba() as xs:boolean {
    let $user := labterm:get-current-user()
    let $groups := sm:get-user-groups($user)
    return if ($groups= 'dba') then true() else false()
};

(: This function checks if the current user is of lab group or not. :)
declare function labterm:isLocked() as xs:boolean {
    try
        {if (doc(concat($labterm:strLab, '/lock.xml'))/locked) then true() else false()}
    catch *
        {false()}
};

(: This function changes the status of the concept and deletes the concept from the database if the new status is rejected :)
declare function labterm:updateStatus($status as xs:string, $loincId as xs:string) as xs:boolean { 
    labterm:updateStatus($status, $loincId, labterm:get-current-user())
};

(: This function changes the status of the concept and deletes the concept from the database if the new status is rejected.
$user parm should only be used for updates by 'system', i.e. automated checks etc.
:)
declare function labterm:updateStatus($status as xs:string, $loincId as xs:string, $user as xs:string) as xs:boolean { 
        if($status = 'rejected') then (
            let $deleteResource :=  
                if(doc-available(concat($labterm:strLabConcept,'/lab_concept_', $loincId, '.xml'))) 
                then (xmldb:remove($labterm:strLabConcept, concat('lab_concept_', $loincId, '.xml'))) 
                else (error(QName('http://art-decor.org/ns/error', 'ConceptDoesNotExist'), concat('Concept ', $loincId, ' is not present.'))
            )
            return true()
        ) else 
            let $labConcept := $labterm:labConcepts//@loinc_num[.=$loincId]/ancestor::lab_concept
            let $logdata := <statusChange object="concept" statusCode="{$status}" effectiveTime="{substring(xs:string(current-dateTime()), 1, 19)}" user="{labterm:get-current-user()}" loincId="{$loincId}">{$labConcept}</statusChange>
            let $logdata := labterm:appendLog($logdata)
            let $update := 
                if ($labConcept/@previousStatus) then
                    update replace $labConcept/@previousStatus with $labConcept/@status
            else update insert attribute {'previousStatus'} {$labConcept/@status} into $labConcept
            let $update := 
                if ($labConcept/@previousUser) then
                    update replace $labConcept/@previousUser with $labConcept/@user
            else update insert attribute {'previousUser'} {$labConcept/@user} into $labConcept
            let $update := 
                if ($labConcept/@last_update) then
                    update replace $labConcept/@last_update with current-dateTime()
                    else update insert attribute {'last_update'} {current-dateTime()} into $labConcept
            let $update := update replace $labConcept/@status with $status
            let $update := update replace $labConcept/@user with $user
            return true()
};

declare function labterm:addLabConcept($loincId as xs:string, $language as xs:string) as node() {
    let $concept            := labterm:getLabConceptById($loincId, $language, true(), true(), '')/lab_concept
    let $concept            := if ($concept/concept) then $concept else labterm:getPrereleaseConcept($loincId)
    let $conceptFilePath    := concat($labterm:strLabConcept, '/lab_concept_', $loincId, '.xml')
    (: If document for that loinc is already present then it's an error. :)
    let $checkFileForLabConcept := 
        if (doc-available($conceptFilePath)) then (
            error(QName('http://art-decor.org/ns/error', 'ConceptAlreadyExists'), concat('Concept ', $loincId, ' already exists.'))
        ) else ()
    
    (: For panels, create all concepts which do not exist in lab concepts :)
    let $doPanel :=     
        if (not($concept/concept/elem[@name='PanelType']))
        then ()
        else
            let $panelTree  := labterm:getFlatPanel($loincId)
            for $concept in $panelTree/concept/concept
            let $panelConcept := labterm:getOnlyLabConceptById($concept/Loinc/string())
            return 
                if ($panelConcept/lab_concept) then ()
                else                labterm:addLabConcept($concept/Loinc/string(), $language)
    let $labConcept := 
        (: Don't add materials, those are added on the fly :)
        <lab_concept status="draft" user="{labterm:get-current-user()}" last_update="{current-dateTime()}">
            {$concept/(@* except @status), $concept/(* except materials)}
        </lab_concept>
    let $errors := labterm:getErrors($labConcept, $language)
    let $labConcept := 
        <lab_concept>
            {$labConcept/@*, $labConcept/*, if ($errors) then <errors>{$errors}</errors> else ()}
        </lab_concept>
    let $createFileForLabConcept    := xmldb:store($labterm:strLabConcept, concat('/lab_concept_', $loincId, '.xml'), $labConcept)
    let $perms                      := sm:chmod(xs:anyURI($conceptFilePath), 'rwxrwxr--')
    let $perms                      := sm:chgrp(xs:anyURI($conceptFilePath), 'lab')
    let $perms                      := sm:clear-acl(xs:anyURI($conceptFilePath))
    let $result := <result status="OK">{$labConcept}</result>
    let $logdata := <statusChange object="concept" statusCode="draft" effectiveTime="{substring(xs:string(current-dateTime()), 1, 19)}" current-user="{labterm:get-current-user()}" loincId="{$loincId}"/>
    let $logdata := labterm:appendLog($logdata)
    return $result

};

declare function labterm:saveUnit($unit as node()) as xs:boolean {
    if ($unit/rm = '') 
    then error(QName('http://art-decor.org/ns/error', 'UnitEmpty'), 'Unit cannot be empty.')
    else if ($unit/name = '') 
    then error(QName('http://art-decor.org/ns/error', 'NameEmpty'), 'Name cannot be empty.')
    else if ($unit/nlname = '') 
    then error(QName('http://art-decor.org/ns/error', 'NlNameEmpty'), 'NL name cannot be empty.')
    else if (not(labterm:isLabAdmin()))
        then error(QName('http://art-decor.org/ns/error', 'UserIsNotAdmin'), 'Only admin users can add units.')
    else
        if ($labterm:labUnits//unit[@id=$unit/@id])
        then
            let $do := update replace $labterm:labUnits//unit[@id=$unit/@id] with <unit id="{$unit/@id}" status="active">{$unit/*}</unit>
            return true()
        else
            if ($labterm:labUnits//unit[rm=$unit/rm])
            then error(QName('http://art-decor.org/ns/error', 'UnitExists'), 'Unit already exists.')
            else 
            let $newId := max($labterm:labUnits//@id) + 1
            let $do := update insert <unit id="{$newId}" status="active">{$unit/*}</unit> into $labterm:labUnits
            return true()
};

declare function labterm:saveMap($map as node()) as xs:boolean {
    if ($map/@code = '') 
    then error(QName('http://art-decor.org/ns/error', 'CodeEmpty'), 'Code cannot be empty.')
    else if ($map/@displayName = '') 
    then error(QName('http://art-decor.org/ns/error', 'NameEmpty'), 'Name cannot be empty.')
    else if ($map/@system = '') 
    then error(QName('http://art-decor.org/ns/error', 'SystemEmpty'), 'System cannot be empty.')
    else if (not(labterm:isLabAdmin()))
    then error(QName('http://art-decor.org/ns/error', 'UserIsNotAdmin'), 'Only admin users can add maps.')
    else
        if ($labterm:labLoincSnomedMapping//material[@code=$map/@code][@system=$map/@system])
        then
            let $do :=update replace $labterm:labLoincSnomedMapping//material[@code=$map/@code][@system=$map/@system] with $map
            return true()
        else 
            let $do :=update insert $map into $labterm:labLoincSnomedMapping
            return true()
};

declare function labterm:removeUnit($unit as node()) as xs:boolean {
    if ($labterm:labConcepts//unit[@ref=$unit/@id])
    then error(QName('http://art-decor.org/ns/error', 'UnitInUse'), 'Unit is in use by concepts.')
    else if (not(labterm:isLabAdmin()))
    then error(QName('http://art-decor.org/ns/error', 'UserIsNotAdmin'), 'Only admin users can remove units.')
    else
        let $do := update value $labterm:labUnits//unit[@id=$unit/@id]/@status with "retired"
        return true()
};

declare function labterm:removeMap($map as node()) as xs:boolean {
    if (not($labterm:labLoincSnomedMapping//material[@code=$map/@code][@system=$map/@system]))
    then error(QName('http://art-decor.org/ns/error', 'NoMap'), 'Map does not exist.')
    else if ($labterm:labConcepts/lab_concept/system[./string()=$map/@system/string()])
    then error(QName('http://art-decor.org/ns/error', 'SystemInUse'), 'System is in use by concepts.')
    else if (not(labterm:isLabAdmin()))
    then error(QName('http://art-decor.org/ns/error', 'UserIsNotAdmin'), 'Only admin users can remove maps.')
    else
        let $do := update delete $labterm:labLoincSnomedMapping//material[@code=$map/@code][@system=$map/@system]
        return true()
};

declare function labterm:updateComment($loincId as xs:string, $comment as xs:string) as xs:boolean {
    let $oldConcept := $labterm:labConcepts/lab_concept[concept[@loinc_num=$loincId]]
    let $commentElement := <comment>{$comment}</comment>
    let $update := 
        if($oldConcept) then
            if ($oldConcept/comment)
            then
 if (string-length($comment) = 0)
                then update delete $oldConcept/comment
                else update replace $oldConcept/comment with $commentElement
            else
                update insert $commentElement into $oldConcept
        else (
            error(QName('http://art-decor.org/ns/error', 'NoConceptFound'), 'No concept found.')
        )       
    return true()
};

declare function labterm:updateReleaseNote($loincId as xs:string, $note as xs:string) as xs:boolean {
    let $oldConcept := $labterm:labConcepts/lab_concept[concept[@loinc_num=$loincId]]
    let $commentElement := <releasenote>{$note}</releasenote>
    let $update := 
        if($oldConcept) then
            if ($oldConcept/releasenote)
            then 
                if (string-length($note) = 0)
                then update delete $oldConcept/releasenote
                else update replace $oldConcept/releasenote with $commentElement
            else
                update insert $commentElement into $oldConcept
        else (
            error(QName('http://art-decor.org/ns/error', 'NoConceptFound'), 'No concept found.')
        )       
    return true()
};

declare function labterm:updateRetiredReason($loincId as xs:string, $note as xs:string) as xs:boolean {
    let $oldConcept := $labterm:labConcepts/lab_concept[concept[@loinc_num=$loincId]]
    let $commentElement := <retired-reason>{$note}</retired-reason>
    let $update := 
        if($oldConcept) then
            if ($oldConcept/retired-reason)
            then 
                if (string-length($note) = 0)
                then update delete $oldConcept/retired-reason
                else update replace $oldConcept/retired-reason with $commentElement
            else
                update insert $commentElement into $oldConcept
        else (
            error(QName('http://art-decor.org/ns/error', 'NoConceptFound'), 'No concept found.')
        )       
    return true()
};

declare function labterm:updateAssignee($loincId as xs:string, $assignee as xs:string) as xs:boolean {
    let $concept := $labterm:labConcepts/lab_concept[concept[@loinc_num=$loincId]]
    let $assignee := if ($assignee = "--empty--" or $assignee = "--all--" ) then '' else $assignee
    let $update := 
        if ($concept/@assignee)
        then update replace $concept/@assignee with $assignee
        else update insert attribute {'assignee'} {$assignee} into $concept
    return true()
};

declare function labterm:updateRetiredReplacement($loincId as xs:string, $replacements as xs:string) as xs:boolean {
    let $oldConcept := $labterm:labConcepts/lab_concept[concept[@loinc_num=$loincId]]
    let $commentElement := <retired-replacement>{$replacements}</retired-replacement>
    let $check :=
        for $replacement in tokenize($replacements, ',')
        let $concept := $labterm:labConcepts//lab_concept[concept/@loinc_num=normalize-space($replacement)]
        return 
            if (not($concept))
            then error(QName('http://art-decor.org/ns/error', 'ReplacementDoesNotExist'), concat('Replacement ', $replacement, ' is not part of the Labcodeset'))        
            else if (not($concept/@status = 'active'))
            then error(QName('http://art-decor.org/ns/error', 'ReplacementNotActive'), concat('Replacement ', $replacement, ' is not active'))
            else ()
    let $update := 
        if($oldConcept) then
            if ($oldConcept/retired-replacement)
            then 
                if (string-length($replacements) = 0)
                then update delete $oldConcept/retired-replacement
                else update replace $oldConcept/retired-replacement with $commentElement
            else
                update insert $commentElement into $oldConcept
        else (
            error(QName('http://art-decor.org/ns/error', 'NoConceptFound'), 'No concept found.')
        )       
    return true()
};

(: Add materials for non-XXX concepts :)
declare function labterm:addMaterialsToConcept($labconcept as element()?) as element()? {
    element lab_concept {
        $labconcept/@*,
        $labconcept/*,
        (: Add materials from mapping :)
        <materials>{
            for $material in $labterm:labLoincSnomedMapping//material[@system=$labconcept//concept[@loinc_num]/system]
            return $material
        }</materials>
    }
};

(: Add parent panels :)
declare function labterm:addParentsToConcept($labconcept as element()?) as element()? {
    if (not($labconcept/concept/@loinc_num)) then () else
        let $parents := labterm:getLabPanelParents($labconcept/concept/@loinc_num)
    return 
        if (count($parents) > 0) then 
            element lab_concept {
                $labconcept/@*,
                $labconcept/*,
                (: Add panel parents :)
                <parents>{
                    for $parent in $parents
                    return <parent>{$parent}</parent>
                }</parents>
            }
        else $labconcept
};

(: Function which returns just LCS concepts, no LOINC concepts :) 
declare function labterm:getOnlyLabConceptById($loincId as xs:string) as element()? {
    labterm:getOnlyLabConceptById($loincId, false())
};

(: 
Function which returns just LCS concepts, no LOINC concepts.
If lastActive, then get the most recent active concept for update, if not lastActive, then the concept as is. 
Wrap, add materials.
:)
declare function labterm:getOnlyLabConceptById($loincId as xs:string, $lastActive as xs:boolean) as element()? {
    let $labConcept := $labterm:labConcepts//concept[@loinc_num=$loincId]/ancestor::lab_concept
    let $labConcept := 
            if (not($lastActive)) then $labConcept
            else if ($labConcept[@status = ('active', 'retired')]) then $labConcept
            else if ($labConcept[@status = ('update')]) then labterm:getLastActiveConcept($loincId)
            else ()
    return  <result count="{count($labConcept)}" search="{$loincId}" current-user="{labterm:get-current-user()}">{if ($labConcept) then labterm:addAllToConcept($labConcept) else ()}</result>
};



declare function labterm:getLabConceptById($loincId as xs:string) as element()? {
    labterm:getLabConceptById($loincId, 'nl-NL', false(), false(), '')
};

(: Function returns LCS concepts when there is one, otherwise LOINC concept :)
declare function labterm:getLabConceptById($loincId as xs:string, $language as xs:string, $searchloinc as xs:boolean, $searchprerelease as xs:boolean, $statusSearch as xs:string) as element()? {
    let $labConcept :=  $labterm:labConcepts//concept[@loinc_num=$loincId]/ancestor::lab_concept
    let $labConcept :=  
        if (string-length($statusSearch) > 1) 
                then $labConcept[@status = tokenize($statusSearch,',')]
                else $labConcept
    let $loincConcept := if (not($searchloinc)) then () else collection($labterm:strLoincData)//concept[@loinc_num=$loincId][1]
    let $loincConcept := if (not($loincConcept)) then() else 
        <lab_concept status="potential">
            <concept>
                {$loincConcept/@*, $loincConcept/(* except concept), $loincConcept/concept[@language=$language]}
            </concept>
        </lab_concept>
    let $preleaseConcept := if (not($searchprerelease)) then () else labterm:getPrereleaseConcept($loincId)        
    let $result := if ($labConcept) then $labConcept else if ($loincConcept) then $loincConcept else if ($preleaseConcept) then $preleaseConcept else ()
    let $result := labterm:addAllToConcept($result)
    return  <result count="{count($result)}" search="{$loincId}" current-user="{labterm:get-current-user()}">{$result}</result>
};

(: Access function for labConcepts, sends query to the appropriate function.
This is the function called by modules and XForms for concept search.
Helpers sometimes call the other :)
declare function labterm:getLabConcepts($search as xs:string, $language as xs:string, $searchloinc as xs:boolean, $searchprerelease as xs:boolean, $statusSearch as xs:string, $user as xs:string, $assignee as xs:string, $show as xs:integer?, $onlyErrors as xs:boolean?, $onlyComments as xs:boolean?, $property as xs:string?, $timing as xs:string?, $system as xs:string?, $scale as xs:string?, $class as xs:string?) as element()* {
    if (fn:normalize-space($search)) then
        (: Allow whitespace before and after loinc_num :)
        if (fn:matches($search, '^\s*[0-9]+\-[0-9]\s*$')) then 
            let $result := labterm:getLabConceptById(fn:normalize-space($search), $language, $searchloinc, $searchprerelease, $statusSearch)
            (: If the concept is a panel, always retrieve whole panel except for prereleases which do not support it :)
            return if ($result//concept[1]/elem[@name='PanelType']='Panel' and $searchprerelease = false()) 
                then labterm:getLabPanelById(fn:normalize-space($search), $language)
                else $result
        else 
            labterm:getLabConceptBySearchTerm($search, $searchloinc, $searchprerelease, $statusSearch, $user, $assignee, $show, $onlyErrors, $onlyComments, $property, $timing, $system, $scale, $class) 
    else 
        (: If empty search, return nothing :)
        <result count="0" search=""/>
};

declare function labterm:getPrereleaseConcept($id) as element()* {
    let $concept := $labterm:loincPrerelease//concept[LOINC=$id]
    let $labconcept := $labterm:labConcepts//lab_concept[concept/@loinc_num=$id]
    return if ($labconcept or $concept) then 
        <lab_concept status="{if ($labconcept) then $labconcept/@status else 'potential'}">
            <concept loinc_num="{$concept/LOINC}" status="PRERELEASE">
                <elem name="LOINC_NUM">{$concept/LOINC/string()}</elem>
                <component name="COMPONENT">{$concept/component/string()}</component>
                <property name="PROPERTY">{$concept/property/string()}</property>
                <timing name="TIME_ASPCT">{$concept/timing/string()}</timing>
                <system name="SYSTEM">{$concept/system/string()}</system>
                <scale name="SCALE_TYP">{$concept/scale/string()}</scale>
                <method name="METHOD_TYP">{$concept/method/string()}</method>
                <elem name="STATUS">PRERELEASE</elem>
                <shortName name="SHORTNAME">{$concept/shortName/string()}</shortName>
                <longName name="LONG_COMMON_NAME">{$concept/longName/string()}</longName>
            </concept>
        </lab_concept>
    else ()
};

declare function labterm:getLabConceptBySearchTerm($search as xs:string) as element()* {
    labterm:getLabConceptBySearchTerm(
        $search, false(), false(), '', '', '', (), (), (), (), (), (), (), ())
};

declare function labterm:getLabConceptBySearchTerm(
    $search as xs:string, 
 $searchloinc as xs:boolean, 
    $searchprerelease as xs:boolean, 
    $statusSearch as xs:string, 
    $user as xs:string, 
    $assignee as xs:string, 
    $show as xs:integer?, 
    $onlyErrors as xs:boolean?, 
    $onlyComments as xs:boolean?, 
    $property as xs:string?, 
    $timing as xs:string?, 
    $system as xs:string?, 
    $scale as xs:string?, 
    $class as xs:string?) as element()* {
    let $labresults :=  
	    if ($searchloinc or $searchprerelease)
	    then labterm:textOnlyQuery($search)//lab_concept
	    else labterm:query($search)//lab_concept
    let $labresults := if($onlyErrors) then $labresults[errors/(error | warning)] else $labresults
    let $labresults := if($onlyComments) then $labresults[comment] else $labresults
    let $labresults := if(string-length($user) > 1) then 
        if ($user = '--all--') then $labresults
        else if ($user = '--empty--') then $labresults[@user = '' or not(@user)] 
        else $labresults[@user = $user]
        else $labresults
    let $labresults := if(string-length($assignee) > 1) then 
        if ($assignee = '--all--') then $labresults
        else if ($assignee = '--empty--') then $labresults[@assignee = '' or not(@assignee)] 
        else $labresults[@assignee = $assignee]
        else $labresults
    let $labresults := 
        if($property) 
        then $labresults[.//property=$property] 
        else $labresults
    let $labresults := 
        if($timing) 
        then $labresults[.//timing=$timing] 
        else $labresults
    let $labresults := 
        if($system) 
        then $labresults[.//system=$system] 
        else $labresults
    let $labresults := 
        if($scale) 
        then $labresults[.//scale=$scale] 
        else $labresults
    let $labresults := 
        if($class) 
        then $labresults[.//class=$class] 
        else $labresults
    let $labcount := count($labresults)
    let $loincresults := 
        if (not($searchloinc)) then () 
        else if ($search = '*') then () else
        for $loincConcept in labterm:searchLoincConcept($search)/concept
        let $labConcept := $labresults//@loinc_num[.=$loincConcept/@loinc_num]/ancestor::lab_concept
        return 
            if ($labConcept) then $labConcept
            else 
                let $loincConcept :=
                    <lab_concept status="potential">
                        <concept>
                            {$loincConcept/@*, $loincConcept/(* except concept), $loincConcept/concept[@language='nl-NL']}
                        </concept>
                    </lab_concept>
                return $loincConcept
    let $prereleaseresults := 
        if (not($searchprerelease)) then () 
        else 
        for $preConcept in labterm:prereleaseQuery($search)
        return labterm:getPrereleaseConcept($preConcept/LOINC/string())
    let $results := 
        if ($searchloinc) then $loincresults 
        else if ($searchprerelease) then $prereleaseresults 
        else $labresults
    let $results := 
    for $result in $results
            order by $result/concept/longName ascending
        return 
            if (labterm:isLabUser()) then $result
            else if ($result[lower-case(@status) = ('active', 'retired')]) then $result
            else if ($result[lower-case(@status) = ('update')]) then labterm:getLastActiveConcept($result/concept/@loinc_num)
    else ()
    let $results := if((string-length($statusSearch) > 1) and ((not($searchloinc)))) then $results[@status = tokenize($statusSearch,',')] else $results
    let $total := count($results)
    let $showResults := if ($show) then $results[position() <= $show] else $results
    let $showResults := for $result in $showResults return labterm:addAllToConcept($result)
    return 
        <result count="{count($showResults)}" total="{$total}" search="{$search}" current-user="{labterm:get-current-user()}" labuser="{labterm:isLabUser()}">
            {$showResults}
        </result>
};

(:~
:   Performs search. The search string is tokenized based on whitespace into terms. If there is 1 term and this matches the pattern of a 
:   LOINC code then only this attribute is considered in search. Otherwise other properties are searched.
:
:   @param $statusCodes limits scope to concepts with requested status (see LOINC documentation for valid values, but 'ACTIVE' and "DEPRECATED' are normally supported)
:   @param $language limits scope to requested language with pattern ll-CC, e.g. de-DE (see LOINC documentation for which linguistic variants the current version supports).
:)
declare function labterm:searchLoincConcept($searchString as xs:string) as element(concept)* {
    if ($searchString[matches(.,'^[0-9]+-[0-9]$')]) then 
        $labterm:loincConcepts//concept[@loinc_num = $searchString][1]
    else (
         (: Replace Lucene special characters - I couldn't make escaping work - the former tokenize didn't include all Lucene special characters :)
        let $searchString   := translate($searchString, '+-&amp;|!(){}[]^"~*?:\/', '                    ')
        let $searchTerms    := tokenize(lower-case($searchString),'\s|\[|\]|\(|\)|&amp;|\\|\+|-|\$|\*')
        let $options        := 
        <options>
            <filter-rewrite>yes</filter-rewrite>
        </options>
        let $text := 
            for $part in $searchTerms
            return concat($part, '*')
            let $query  := string-join($text, ' AND ')
        let $result         :=
            $labterm:loincConcepts//concept[ft:query(., $query, $options)][@loinc_num]
        let $totalcount         := count($result)
        return
        <result total="{$totalcount}">{$result}</result>
    )
};

declare function labterm:addAllToConcept($labconcept as element()?) as element()? {
    labterm:addAssigneeToConcept(labterm:addParentsToConcept(labterm:addMaterialsToConcept($labconcept)))
};

declare function labterm:addAssigneeToConcept($labconcept as element()?) as element()? {
    if ($labconcept/@assignee) then
        $labconcept
    else
        element lab_concept {
            $labconcept/@*,
            attribute {'assignee'} {},
            $labconcept/*}
};
(: Get the last active version of a concept from the logs. 

Use for 'update' or concepts. 
For 'active' or 'retired' concepts, you get the concept itself.
For other status (i.e. 'draft'), empty sequence. 
:) 
declare function labterm:getLastActiveConcept($loincId as xs:string) as node()? {
    let $labConcept := $labterm:labConcepts/lab_concept[concept/@loinc_num=$loincId]
    return 
        if ($labConcept[@status=('update' )])
        then 
            let $logItems := 
                for $logLine in $labterm:LabDataLog//lab_concept[concept/@loinc_num=$loincId][@status='active']/ancestor::statusChange
                order by $logLine/@effectiveTime ascending
                return $logLine
            return ($logItems[last()])/lab_concept
        else if ($labConcept[@status=('active' , 'retired')])
        then $labConcept
        else ()
};

(:============ Panels ===========:)
declare function labterm:makeLocalPanelConcept($concept as node()) as node() {
    <concept panelMember="original">
        {
            $concept/@*, 
            $concept/(* except concept),
            for $child in $concept/concept return labterm:makeLocalPanelConcept($child)
        }
    </concept>
};

declare function labterm:addLocalPanel($panelLoincId as xs:string) as xs:string {
    let $concept := labterm:getLabConceptById($panelLoincId)//lab_concept
    let $id := $concept/concept/@loinc_num
    (: Look for top-level panels to avoid catching child panels :)
    let $panel := labterm:getFlatPanel($id)/concept
    let $check :=
        if (not($concept)) then error(QName('http://art-decor.org/ns/error', 'NotAConcept'), concat('LOINC ', $panelLoincId, ' is not in Labcodeset'))
        else if (not($concept/@status=('draft', 'update'))) then error(QName('http://art-decor.org/ns/error', 'StatusError'), concat('LOINC ', $panelLoincId, " has status '", $concept/@status/string(), "' and can't be changed"))
        else if (not($panel)) then error(QName('http://art-decor.org/ns/error', 'NoLoincPanelConcept'), concat('No LOINC Panel ', $panelLoincId, ' found'))
        else if (not($concept/@status = 'draft' or $concept/@status = 'update')) then error(QName('http://art-decor.org/ns/error', 'PanelNotEditable'), concat('LOINC ', $panelLoincId, ' must be draft or update to edit it'))
        else if (not($concept/concept[elem[@name='PanelType']='Panel'])) then error(QName('http://art-decor.org/ns/error', 'NotAPanel'), concat('LOINC ', $panelLoincId, ' is not a panel'))
        else if (collection($labterm:strLocalPanels)/concept[Loinc=$id]) then error(QName('http://art-decor.org/ns/error', 'LocalPanelExist'), concat('A local panel ', $panelLoincId, ' already exists'))
        else ()
    let $localPanel := labterm:makeLocalPanelConcept($panel)
    let $resource := xmldb:store($labterm:strLocalPanels, concat('local_panel_', $id, '.xml'), $localPanel)
    let $do := sm:chgrp($resource, 'lab') 
    return <result status="OK"/>
};

declare function labterm:removeLocalPanel($panelLoincId as xs:string) as xs:string? {
    let $panel := $labterm:localPanels//concept[Loinc=$panelLoincId]
    let $concept := labterm:getOnlyLabConceptById($panelLoincId)
    let $check :=
        if (not($panel)) then error(QName('http://art-decor.org/ns/error', 'NotALocalPanel'), concat('LOINC ', $panelLoincId, ' is not a local panel'))
        else if (not($concept)) then error(QName('http://art-decor.org/ns/error', 'NotAConcept'), concat('LOINC ', $panelLoincId, ' is not in the Labcodeset'))
        else if (not($concept/lab_concept/@status=('draft', 'update'))) then error(QName('http://art-decor.org/ns/error', 'StatusError'), concat('LOINC ', $panelLoincId, " has status '", $concept/lab_concept/@status/string(), "' and can't be changed"))
        else ()
    let $do := xmldb:remove($labterm:strLocalPanels, concat('local_panel_', $panelLoincId, '.xml'))
    return <result status="OK"/>
};

declare function labterm:replaceInLocalPanel($panelLoincId as xs:string, $oldLoincId as xs:string, $newLoincId as xs:string) as node() {
    let $newConcept := labterm:getLabConceptById($newLoincId)//lab_concept
    let $panelConcept := labterm:getLabConceptById($panelLoincId)//lab_concept
    let $localPanel := collection($labterm:strLocalPanels)//concept[Loinc=$panelLoincId][1]
    let $check :=
        if (not($newConcept)) then error(QName('http://art-decor.org/ns/error', 'NotAConcept'), concat('LOINC ', $newLoincId, ' is not in Labcodeset'))
        else if (not($localPanel/concept[Loinc=$oldLoincId])) then error(QName('http://art-decor.org/ns/error', 'NotAMember'), concat('LOINC ', $oldLoincId, ' is not a member of panel', $panelLoincId))
        else if (not($panelConcept/@status = 'draft' or $panelConcept/@status = 'update')) then error(QName('http://art-decor.org/ns/error', 'PanelNotEditable'), concat('LOINC ', $panelLoincId, ' must be draft or update to edit it'))
        else if (not($panelConcept/concept[elem[@name='PanelType']='Panel'])) then error(QName('http://art-decor.org/ns/error', 'NotAPanel'), concat('LOINC ', $panelLoincId, ' is not a panel'))
        else ()
    let $oldPanelData := $localPanel/concept[Loinc=$oldLoincId]
    let $xml :=
        <concept panelMember="added">
            {$oldPanelData/ParentId, $oldPanelData/ParentLoinc, $oldPanelData/ParentName, $oldPanelData/ParentId, $oldPanelData/ParentId, $oldPanelData/ID, $oldPanelData/SEQUENCE}
            <Loinc>{$newConcept/concept/@loinc_num/string()}</Loinc>
            <LoincName>{$newConcept/concept/shortName/string()}</LoincName>
            <ObservationRequiredInPanel>O</ObservationRequiredInPanel>
        </concept>
    let $update := update insert $xml following $oldPanelData
    let $update := update replace $oldPanelData/@panelMember with 'removed'
    return $xml
};

declare function labterm:changeLocalPanelMembers($panelLoincId as xs:string, $loincId as xs:string, $membership as xs:string) as node() {
    let $panel := $labterm:localPanels//concept[Loinc=$panelLoincId]
    let $panelConcept := labterm:getLabConceptById($panelLoincId)
    let $concept := labterm:getLocalPanelMemberById($panelLoincId, $loincId)
    let $check :=
        if (not($panel)) then error(QName('http://art-decor.org/ns/error', 'NotALocalPanel'), concat('LOINC ', $panelLoincId, ' is not a local panel'))
        else if (not($concept)) then error(QName('http://art-decor.org/ns/error', 'NotAConcept'), concat('LOINC ', $panelLoincId, ' is not in the Labcodeset'))
        else if (not($panelConcept/lab_concept/@status=('draft','update'))) then error(QName('http://art-decor.org/ns/error', 'StatusError'), concat('LOINC ', $panelLoincId, " has status '", $panelConcept/lab_concept/@status, "' and can't be changed"))
        else ()
    let $update := 
        if ($concept/lab_concept/@panelMember="added")
        then update delete $labterm:localPanels/concept[Loinc=$panelLoincId]//concept[Loinc=$loincId]
        else update replace $labterm:localPanels/concept[Loinc=$panelLoincId]//concept[Loinc=$loincId]/@panelMember with $membership
    return <result status="OK"/>
};

(:~ Get panel node with LOINC concepts:)
declare function labterm:getLabPanelWithConcepts($panelChild as node(), $language as xs:string, $level as xs:string) as node()* {
    let $loincId        := $panelChild/Loinc/string()
    let $loincConcept   := collection($labterm:strLoincData)//concept[@loinc_num=$loincId]
    let $labConcept     := labterm:getOnlyLabConceptById($loincId)/lab_concept
    let $result :=
        <lab_concept level="{$level}">
            {
            $panelChild/@*,
            if ($labConcept) then $labConcept/@* else attribute status {'potential'}
            ,
            $panelChild/(* except concept)
            ,
            if ($labConcept) 
            then $labConcept/* 
            else
                <concept>
                    {$loincConcept/@*, $loincConcept/(* except concept), $loincConcept/concept[@language=$language]}
                </concept>
            }
        </lab_concept>
    let $level          := concat($level, '--') 
    return 
        (
        $result,
        for $child in $panelChild/concept
        order by xs:integer($child/SEQUENCE) ascending
        return labterm:getLabPanelWithConcepts($child, $language, $level)
        )

};

(: Get the panel as a tree.
For panels, we will always search loinc, since each panel member MUST be present in Labcodeset if the panel is present.
:)
declare function labterm:getLabPanelTreeById($loincId as xs:string, $language as xs:string) as element()? {
    let $loincConcept := collection($labterm:strLoincData)//concept[@loinc_num=$loincId][1]
    (: There may be multiple panels with the same Loinc, i.e. organizers which appear in multiple panels. They should all be the same, so just pick the first :)
    let $localPanel := collection($labterm:strLocalPanels)/concept[Loinc=$loincId][1]
    let $panelTree  := 
        if ($localPanel) then <panel type="localpanel">{$localPanel}</panel> 
        else if($loincConcept/elem[@name='PanelType']) 
 then labterm:getFlatPanel($loincId)
            else ()
    return $panelTree
};

(:
In the labcodeset all panels are flattened to one level deep. This is to facilitate local panels: a subpanel may be a local panel
which is different than the original LOINC panel which is included 'as is' in a master panel, and thus not shown as a localized variant.
Storing all local panels as one-level-deep panels, and displaying panels as such removes those differences. 
:)
declare function labterm:getFlatPanel($loincId as xs:string) as element()? {
    let $loincPanel := collection($labterm:strLoincPanels)//concept[Loinc=$loincId][1]
            let $flatPanel := 
                <concept>
                {
                    $loincPanel/(* except concept), 
                    for $concept in $loincPanel/concept return <concept>{$concept/(* except concept)}</concept>
                }
                </concept>
            return <panel type="panel">{$flatPanel}</panel> 
};

(: Get the panel as a tree for publication (no "removed" concepts :)
declare function labterm:getFinalLabPanelConcept($concept as element()) as element()? {
    if ($concept/@panelMember = 'removed')
    then ()
    else
        <concept>{$concept/@*, $concept/(* except concept), for $child in $concept/concept return labterm:getFinalLabPanelConcept($child)}</concept>
};

(: Get the panel as a tree for publication (no "removed" concepts :)
declare function labterm:getFinalLabPanelTreeById($loincId as xs:string, $language as xs:string) as element()? {
    let $panel := labterm:getLabPanelTreeById($loincId, $language)
    return <panel>{$panel/@*, $panel/(* except concept), for $child in $panel/concept return labterm:getFinalLabPanelConcept($child)}</panel>
};

(: Get panel by id.
This function returns a flat list with @level, not a tree. Easier for display in front-end. :)
declare function labterm:getLabPanelById($loincId as xs:string, $language as xs:string) as element()? {
    let $panelTree  := 
        if (labterm:isLabUser()) 
        then labterm:getLabPanelTreeById($loincId, $language)
    (: Guest users only see original + added concepts :)
        else labterm:getFinalLabPanelTreeById($loincId, $language)
    let $results    :=
        if (not($panelTree)) then error(QName('http://art-decor.org/ns/error', 'NoLoincPanelConcept'), concat('No LOINC Panel ', $loincId, ' found')) 
        else labterm:getLabPanelWithConcepts($panelTree/concept, $language, '')
    (: We return a list with panel items. The first one is the panel itself, the next ones are ordered by Sequence (item in panel), and have a 'level' attribute :)
    return <result count="{count($results)}" search="{$loincId}" mode="{$panelTree/@type}" current-user="{labterm:get-current-user()}">{$results}</result>
};

declare function labterm:getLocalPanelMemberById($panelLoincId as xs:string, $loincId as xs:string) as element()? {
    let $localPanel := $labterm:localPanels/concept[Loinc=$panelLoincId]
    let $check := if (not($localPanel)) then error(QName('http://art-decor.org/ns/error', 'NoLocalPanel'), concat('LOINC  ', $panelLoincId, '  is not a local panel ')) else ()
    let $panelMember := $localPanel//concept[Loinc=$loincId]
    let $level := replace(string-join($panelMember/ancestor::concept/local-name()), 'concept', '--')
    let $result :=
        if (not($panelMember)) then error(QName('http://art-decor.org/ns/error', 'NoLocalPanelMember'), concat('LOINC  ', $loincId, '  is not a member of local panel ', $panelLoincId)) 
        else labterm:getLabPanelWithConcepts($panelMember, 'nl-NL', $level)
    return <result status="OK">{$result}</result>
};

(: Get Panel parents IF this concept is child of such an active panel :)
declare function labterm:getActiveLabPanelParents($loincId as xs:string) as xs:string* {
    (: Get all top-level concepts in LOINC Panels who have a descendant with $loincId :)
    let $panelParents   := collection($labterm:strLoincPanels)//Panels/concept[descendant::concept[Loinc=$loincId]]/Loinc/string()
    let $activeLabParents     := $labterm:labConcepts//concept[@loinc_num=$panelParents][../@status='active']/@loinc_num
    return $activeLabParents
};

(: Get Panel parents IF this concept is child of such a panel :)
declare function labterm:getLabPanelParents($loincId as xs:string) as xs:string* {
    (: Get all top-level concepts in LOINC Panels who have a descendant with $loincId :)
    let $panelParents   := collection($labterm:strLoincPanels)//Panels//concept[descendant::concept[Loinc=$loincId]]/Loinc/string()
    let $parents     := $labterm:labConcepts//concept[@loinc_num=$panelParents]/@loinc_num
    return $parents
};


(:==================== Search =======================:)
declare %private function local:exclude($parts as xs:string*, $results as node()*) as node()*{
    let $part := $parts[last()]
    let $part := if (starts-with($part, '+')) then substring($part, 2) else $part
    let $axis := substring-before($part, ':')
    let $text := substring-after($part, ':')
    (:'pro', 'tim', 'sys', 'sca', 'cla', 'err':)
    let $results:=
        if ($axis = '-pro')
        then $results[not(starts-with(.//property/lower-case(.), $text))]
        else if ($axis = '-tim')
        then $results[not(starts-with(.//timing/lower-case(.), $text))]
        else if ($axis = '-sys')
        then $results[not(starts-with(.//system/lower-case(.), $text))]
        else if ($axis = '-sca')
        then $results[not(starts-with(.//scale/lower-case(.), $text))]
        else if ($axis = '-cla')
        then $results[not(starts-with(.//class/lower-case(.), $text))]
        else if ($axis = '-err')
        then $results[not(starts-with(.//errors/*/lower-case(@code), $text))]
        else $results
    return
        if (count($parts) > 1) 
        then local:exclude($parts[not(position()=last())], $results) 
        else $results
};

declare %private function local:include($parts as xs:string*, $results as node()*) as node()*{
    (:'pro', 'tim', 'sys', 'sca', 'cla', 'err':)
    let $proin := $parts[starts-with(., 'pro')]
    let $timin := $parts[starts-with(., 'tim')]
    let $sysin := $parts[starts-with(., 'sys')]
    let $scain := $parts[starts-with(., 'sca')]
    let $clain := $parts[starts-with(., 'cla')]
    let $errin := $parts[starts-with(., 'err')]
    let $results :=
        if (count($proin) > 0) then
            for $include in $proin
            return $results[starts-with(.//property/lower-case(.), substring-after($include, ':'))]
        else $results
    let $results :=
        if (count($timin) > 0) then
            for $include in $timin
            return $results[starts-with(.//timing/lower-case(.), substring-after($include, ':'))]
        else $results
    let $results :=
        if (count($sysin) > 0) then
            for $include in $sysin
            return $results[starts-with(.//system/lower-case(.), substring-after($include, ':'))]
        else $results
    let $results :=
        if (count($scain) > 0) then
            for $include in $scain
            return $results[starts-with(.//scale/lower-case(.), substring-after($include, ':'))]
        else $results
    let $results :=
        if (count($clain) > 0) then
            for $include in $clain
            return $results[starts-with(.//class/lower-case(.), substring-after($include, ':'))]
        else $results
    let $results :=
        if (count($errin) > 0) then
            for $include in $errin
            return $results[starts-with(.//errors/*/lower-case(@code), substring-after($include, ':'))]
        else $results
    return
        $results
};

declare %private function local:unitQuery($query as xs:string*, $results as node()*) as node()*{
    let $unitq := tokenize($query, ' ')[contains(., 'unit:')]
    let $unitId := $labterm:labUnits//unit[rm=substring-after($unitq, ':')]/@id
    return $results[.//unit/@ref=$unitId]
};

(:~ Normalize to single space, split on space, remove chars which could end statement and cause xquery injection :)
(: Also replace Lucene special characters - I couldn't make escaping work :)
declare %private function local:parse($query as xs:string) as xs:string*{
    tokenize(lower-case(normalize-space(translate($query,  '&amp;|!(){}[]^"~*?\/', '                '))), ' ')
};

(: textQuery discards all axis parts (containing ':') and searches :)
declare %private function local:textQuery($parts as xs:string*) as node()*{
    let $options := 
        <options>
            <filter-rewrite>yes</filter-rewrite>
        </options>
    let $col := $labterm:labConcepts
    let $text := for $part in $parts 
                 where not(contains($part, ':')) 
                 return concat($part, '*')
    let $textquery  := string-join($text, ' AND ')
    let $results := 
        if (not($textquery) or $textquery = '**') then $col else $col//lab_concept[ft:query(., $textquery, $options)]
    return $results
};

(: Text search in prerelease, text only :)
declare function labterm:prereleaseQuery($query as xs:string*) as node()*{
    let $options := 
        <options>
            <filter-rewrite>yes</filter-rewrite>
        </options>
    let $col := $labterm:loincPrerelease
    let $parts := local:parse($query)
    let $textquery := 
    if ($query = '*') then '**'
        else
        let $text := for $part in $parts 
                 where not(contains($part, ':')) 
                 return concat($part, '*')
    return string-join($text, ' AND ')
    let $results := 
        if (not($textquery) or $textquery = '**') then $col//concept else $col//concept[ft:query(., $textquery, $options)]
    return $results
};

declare %private function local:axisQuery($parts as xs:string*, $results as node()*) as node()*{
    let $qualifiers :=  
        for $part in $parts[contains(., ':')]
        let $axis := substring-before($part, ':')
        let $axis := if (starts-with($axis, '+')) then substring($axis, 2, 5) else substring($axis, 1, 4)
        let $condition := substring-after($part, ':')
        return concat($axis, ':', $condition)
    return if (count($qualifiers) > 0) 
    then 
        let $includes := $qualifiers[not(starts-with(., '-'))] 
        let $excludes := $qualifiers[starts-with(., '-')] 
        let $results := if (count($includes) > 0) then local:include($includes, $results) else $results
        let $results := if (count($excludes) > 0) then local:exclude($excludes, $results) else $results
        return $results
    else $results
};

(: query first searches for text parts (without ':' and then filters for axes :)
declare function labterm:query($query as xs:string) as node()*{
    let $query := if (contains($query, "uni:")) then fn:replace($query, 'uni:', 'unit:') else $query
    let $subquery := if (contains($query, "unit:")) then string-join(tokenize($query, ' ')[not(contains(., 'unit:'))], ' ') else $query
    let $parts := local:parse($subquery)
    let $results := local:textQuery($parts)
    let $results := local:axisQuery($parts, $results)
    let $results := if (contains($query, "unit:")) then local:unitQuery($query, $results) else $results
    return 
        <results count="{count($results)}" query="{string-join($parts, ' ')}">
            {$results}
        </results>
};

(: textOnlyQuery discards all axis parts (containing ':') and searches :)
declare function labterm:textOnlyQuery($query as xs:string) as node()*{
    let $parts := local:parse($query)
    let $results := local:textQuery($parts)
    return 
        <results count="{count($results)}" query="{string-join($parts, ' ')}">
            {$results}
        </results>
};