xquery version "3.0";

module namespace labpfix        = "http://art-decor.org/ns/lab/permissions";
declare namespace sm            = "http://exist-db.org/xquery/securitymanager";

(:  Mode            Octal
 :  rw-r--r--   ==  0644
 :  rw-rw-r--   ==  0664
 :  rwxr-xr--   ==  0754
 :  rwxr-xr-x   ==  0755
 :  rwxrwxr-x   ==  0775
 :)

(:install path for art (normally /db/apps/), includes trailing slash :)
declare variable $labpfix:root   := repo:get-root();
declare variable $labpfix:strLabModules       := concat($labpfix:root,'lab/modules');
declare variable $labpfix:strLabApi       := concat($labpfix:root,'lab/api');
declare variable $labpfix:strLabData          := concat($labpfix:root,'lab-data/data');
declare variable $labpfix:strLabPublications  := concat($labpfix:root,'lab-data/publications');
declare variable $labpfix:strLabConcepts          := concat($labpfix:root,'lab-data/data/lab_concepts');
declare variable $labpfix:strLocalPanels          := concat($labpfix:root,'lab-data/data/local_panels');

declare %private function local:exec4group($uri as xs:string, $usergroup as xs:string, $access-string as xs:string) {
    sm:chown($uri,$usergroup),
    sm:chmod($uri,$access-string),
    sm:clear-acl($uri)
};

declare function labpfix:setLabPermissions() {
    let $check          := local:checkIfUserDba()
    let $do := sm:chgrp(xs:anyURI(concat('xmldb:exist:///',$labpfix:root, 'lab')),'lab')
    let $do := sm:chgrp(xs:anyURI(concat('xmldb:exist:///',$labpfix:root, 'lab/api')),'lab')
    let $do := sm:chgrp(xs:anyURI(concat('xmldb:exist:///',$labpfix:root, 'lab/modules')),'lab')
    let $do := sm:chgrp(xs:anyURI(concat('xmldb:exist:///',$labpfix:root, 'lab/resources')),'lab')
    let $do := sm:chgrp(xs:anyURI(concat('xmldb:exist:///',$labpfix:root, 'lab/xforms')),'lab')
    let $do := sm:chgrp(xs:anyURI(concat('xmldb:exist:///',$labpfix:root, 'lab-data')),'lab')
    let $do := sm:chgrp(xs:anyURI(concat('xmldb:exist:///',$labpfix:root, 'lab-data/data')),'lab')
    let $do := sm:chgrp(xs:anyURI(concat('xmldb:exist:///',$labpfix:root, 'lab-data/data/lab_concepts')),'lab')
    let $do := sm:chgrp(xs:anyURI(concat('xmldb:exist:///',$labpfix:root, 'lab-data/data/local_panels')),'lab')
    let $do := sm:chgrp(xs:anyURI(concat('xmldb:exist:///',$labpfix:root, 'lab-data/log')),'lab')
    let $do := sm:chgrp(xs:anyURI(concat('xmldb:exist:///',$labpfix:root, 'lab-data/publications')),'lab')
    let $do := sm:chmod(xs:anyURI(concat('xmldb:exist:///',$labpfix:root, 'lab/controller.xql')),'rwxr-xr-x')
    let $do := sm:chmod(xs:anyURI(concat('xmldb:exist:///',$labpfix:root, 'lab-data')),'rwxrwxr-x')
    let $do := sm:chmod(xs:anyURI(concat('xmldb:exist:///',$labpfix:root, 'lab-data/data')),'rwxrwxr-x')
    let $do := sm:chmod(xs:anyURI(concat('xmldb:exist:///',$labpfix:root, 'lab-data/data/lab_concepts')),'rwxrwxr-x')
    let $do := sm:chmod(xs:anyURI(concat('xmldb:exist:///',$labpfix:root, 'lab-data/data/local_panels')),'rwxrwxr-x')
    let $do := sm:chmod(xs:anyURI(concat('xmldb:exist:///',$labpfix:root, 'lab-data/log')),'rwxrwxr-x')
    let $do := sm:chmod(xs:anyURI(concat('xmldb:exist:///',$labpfix:root, 'lab-data/publications')),'rwxrwxr-x')
    let $do := sm:chgrp(xs:anyURI(concat('xmldb:exist:///',$labpfix:root, 'lab-data/publications')),'lab-admin')

    return (
        for $query in xmldb:get-child-resources(xs:anyURI(concat('xmldb:exist:///', $labpfix:strLabModules)))
        return (
            sm:chown(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLabModules,'/',$query)),'admin'),
            sm:chgrp(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLabModules,'/',$query)),'lab'),
            if (starts-with($query,('check','get','is-','retrieve','search','view','download'))) then
                sm:chmod(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLabModules,'/',$query)), 'rwxr-xr-x')
            else (
                sm:chmod(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLabModules,'/',$query)), 'rwxr-xr--')
            )
            ,
            sm:clear-acl(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLabModules,'/',$query)))
        )
    ,
        for $query in xmldb:get-child-resources(xs:anyURI(concat('xmldb:exist:///', $labpfix:strLabApi)))
        return (
            sm:chown(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLabApi,'/',$query)),'admin'),
            sm:chgrp(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLabApi,'/',$query)),'lab'),
            if (starts-with($query,('check','get','is-','retrieve','search','view','download'))) then
                sm:chmod(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLabApi,'/',$query)), 'rwxr-xr-x')
            else (
                sm:chmod(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLabApi,'/',$query)), 'rwxr-xr--')
            )
            ,
            sm:clear-acl(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLabApi,'/',$query)))
        )
    ,
        for $data in xmldb:get-child-resources(xs:anyURI(concat('xmldb:exist:///', $labpfix:strLabData)))
        return (
            sm:chown(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLabData,'/',$data)),'admin'),
            sm:chgrp(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLabData,'/',$data)),'lab-admin'),
            sm:chmod(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLabData,'/',$data)), 'rwxrwxr--'),
            sm:clear-acl(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLabData,'/',$data)))
        )
    ,
        for $data in xmldb:get-child-resources(xs:anyURI(concat('xmldb:exist:///', $labpfix:strLabPublications)))
        return (
            sm:chown(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLabPublications,'/',$data)),'admin'),
            sm:chgrp(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLabPublications,'/',$data)),'lab-admin'),
            sm:chmod(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLabPublications,'/',$data)), 'rwxrwxr--'),
            sm:clear-acl(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLabPublications,'/',$data)))
        )
    ,
        for $data in xmldb:get-child-resources(xs:anyURI(concat('xmldb:exist:///', $labpfix:strLabConcepts)))
        return (
            sm:chown(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLabConcepts,'/',$data)),'admin'),
            sm:chgrp(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLabConcepts,'/',$data)),'lab'),
            sm:chmod(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLabConcepts,'/',$data)), 'rwxrwxr--'),
            sm:clear-acl(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLabConcepts,'/',$data)))
        )
    ,
        for $data in xmldb:get-child-resources(xs:anyURI(concat('xmldb:exist:///', $labpfix:strLocalPanels)))
        return (
            sm:chown(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLocalPanels,'/',$data)),'admin'),
            sm:chgrp(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLocalPanels,'/',$data)),'lab'),
            sm:chmod(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLocalPanels,'/',$data)), 'rwxrwxr--'),
            sm:clear-acl(xs:anyURI(concat('xmldb:exist:///',$labpfix:strLocalPanels,'/',$data)))
        )
    )
};

declare %private function local:checkIfUserDba() {
    if (sm:is-dba(sm:id()//sm:real/sm:username/string())) then () else (
        error(QName('http://art-decor.org/ns/lab/permissions', 'NotAllowed'), concat('Only dba user can use this module. ', sm:id()//sm:real/sm:username/string()))
    )
};