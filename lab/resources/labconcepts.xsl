<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
  <h2>Labconcepts</h2>
  <table border="1">
    <tr bgcolor="#9acd32">
      <th>Loinc</th>
      <th>component</th>
      <th>system</th>
      <th>orderObs</th>
      <th>longname</th>
      <th>property</th>
      <th>method</th>
      <th>scale</th>
      <th>system</th>
      <th>Class</th>
    </tr>
    <xsl:for-each select="publication/lab_concepts/lab_concept/concept">
    <xsl:if test = "class = 'MICRO' or class = 'ABXBACT'">
    <tr>
      <td><xsl:value-of select="elem[@name='LOINC_NUM']"/></td>
      <td><xsl:value-of select="component"/></td>
      <td><xsl:value-of select="system"/></td>
      <td><xsl:value-of select="orderObs"/></td>
      <td><xsl:value-of select="longName"/></td>
      <td><xsl:value-of select="property"/></td>
      <td><xsl:value-of select="method"/></td>
      <td><xsl:value-of select="scale"/></td>
      <td><xsl:value-of select="system"/></td>
      <td><xsl:value-of select="class"/></td>
    </tr>
    </xsl:if>
    </xsl:for-each>
  </table>
  </body>
  </html>
</xsl:template>

</xsl:stylesheet> 