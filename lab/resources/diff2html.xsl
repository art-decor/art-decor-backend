<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    <xsl:output method="html"/>
    <xsl:template match="diff">
        <xsl:variable name="title" select="concat('Diff publicatie ', publication1/@effectiveDate, ' en ', publication2/@effectiveDate)"/>
        <html style="font-size:14px;">
            <head>
                <title>
                    <xsl:value-of select="$title"/>
                </title>
                <meta charset="utf-8"/>
                <style>
                    body {
                    background-color: #f2f2f2;
                    }
                    
                    table, th, td {
                    border: 1px solid black;
                    border-collapse: collapse;
                    }                    }
                </style>
            </head>
            <body>
                <h1>
                    <xsl:value-of select="$title"/>
                </h1>
                <div>
                    Publication 1: <xsl:value-of select="publication1/desc"/>
                </div>
                <div>
                    Publication 2 :<xsl:value-of select="publication2/desc"/>
                </div>
                <h2>Concepts</h2>
                <xsl:if test="concepts/removed">
                    <h3>Removed from Labcodeset - please fix</h3>
                    <table>
                        <thead>
                            <tr>
                                <th>LOINC</th>
                                <th>LOINC status</th>
                                <th>Labcodeset status</th>
                                <th>Volledige naam</th>
                            </tr>
                        </thead>
                        <tbody>
                            <xsl:for-each select="concepts/removed">
                                <tr>
                                    <td>
                                        <xsl:value-of select="@loinc_num"/>
                                    </td>
                                    <td>
                                        <xsl:value-of select="@loincStatus"/>
                                    </td>
                                    <td>
                                        <xsl:value-of select="@lcsStatus"/>
                                    </td>
                                    <td>
                                        <xsl:value-of select="longName"/>
                                    </td>
                                </tr>
                            </xsl:for-each>
                        </tbody>
                    </table>
                </xsl:if>
                <h3>Retired from Labcodeset</h3>
                <table>
                    <thead>
                        <tr>
                            <th>LOINC</th>
                            <th>LOINC status</th>
                            <th>Labcodeset status</th>
                            <th>Previous Labcodeset status</th>
                            <th>Volledige naam</th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="concepts/retired">
                            <tr>
                                <td>
                                    <xsl:value-of select="@loinc_num"/>
                                </td>
                                <td>
                                    <xsl:value-of select="@loincStatus"/>
                                </td>
                                <td>
                                    <xsl:value-of select="@lcsStatus"/>
                                </td>
                                <td>
                                    <xsl:value-of select="@oldStatus"/>
                                </td>
                                <td>
                                    <xsl:value-of select="longName"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
                <h3>Added to Labcodeset</h3>
                <table>
                    <thead>
                        <tr>
                            <th>LOINC</th>
                            <th>LOINC status</th>
                            <th>Labcodeset status</th>
                            <th>Volledige naam</th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="concepts/added">
                            <tr>
                                <td>
                                    <xsl:value-of select="@loinc_num"/>
                                </td>
                                <td>
                                    <xsl:value-of select="@loincStatus"/>
                                </td>
                                <td>
                                    <xsl:value-of select="@lcsStatus"/>
                                </td>
                                <td>
                                    <xsl:value-of select="longName"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
                <h3>Change in translation, outcome or axis</h3>
                <table>
                    <thead>
                        <tr>
                            <th>LOINC</th>
                            <th>Before/After</th>
                            <th>Status</th>
                            <th>LOINC Status</th>
                            <th>EN component</th>
                            <th>EN property</th>
                            <th>EN system</th>
                            <th>EN scale</th>
                            <th>EN method</th>
                            <th>EN shortName</th>
                            <th>EN longName</th>
                            <th>NL component</th>
                            <th>NL property</th>
                            <th>NL system</th>
                            <th>NL scale</th>
                            <th>NL method</th>
                            <th>NL shortName</th>
                            <th>NL longName</th>
                            <th>Materials</th>
                            <th>Units</th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="concepts/changed/concept">
                            <tr>
                                <td>
                                    <xsl:value-of select="@loinc_num"/>
                                </td>
                                <td>BEFORE</td>
                                <td>
                                    <xsl:value-of select="status[1]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="loincstatus[1]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="english/component[1]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="english/property[1]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="english/system[1]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="english/scale[1]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="english/method[1]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="english/shortName[1]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="english/longName[1]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="nederlands/component[1]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="nederlands/property[1]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="nederlands/system[1]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="nederlands/scale[1]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="nederlands/method[1]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="nederlands/shortName[1]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="nederlands/longName[1]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="string-join(materials[1]/material/@displayName/string(), ' ')"/>
                                </td>
                                <td>
                                    <xsl:value-of select="string-join(units[1]/unit/rm/string(), ' ')"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <xsl:value-of select="@loinc_num"/>
                                </td>
                                <td>AFTER</td>
                                <td>
                                    <xsl:value-of select="status[2]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="loincstatus[2]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="english/component[2]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="english/property[2]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="english/system[2]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="english/scale[2]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="english/method[2]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="english/shortName[2]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="english/longName[2]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="nederlands/component[2]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="nederlands/property[2]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="nederlands/system[2]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="nederlands/scale[2]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="nederlands/method[2]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="nederlands/shortName[2]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="nederlands/longName[2]"/>
                                </td>
                                <td>
                                    <xsl:value-of select="string-join(materials[2]/material/@displayName/string(), ' ')"/>
                                </td>
                                <td>
                                    <xsl:value-of select="string-join(units[2]/unit/rm/string(), ' ')"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
                <h2>Units</h2>
                <h3>Removed</h3>
                <table>
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>UCUM</th>
                            <th>EN</th>
                            <th>NL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="units/removed/unit">
                            <tr>
                                <td>
                                    <xsl:value-of select="@id"/>
                                </td>
                                <td>
                                    <xsl:value-of select="rm"/>
                                </td>
                                <td>
                                    <xsl:value-of select="name"/>
                                </td>
                                <td>
                                    <xsl:value-of select="nlname"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
                <h3>Added</h3>
                <table>
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>UCUM</th>
                            <th>EN</th>
                            <th>NL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="units/added/unit">
                            <tr>
                                <td>
                                    <xsl:value-of select="@id"/>
                                </td>
                                <td>
                                    <xsl:value-of select="rm"/>
                                </td>
                                <td>
                                    <xsl:value-of select="name"/>
                                </td>
                                <td>
                                    <xsl:value-of select="nlname"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
                <h3>Changed</h3>
                <table>
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Before/after</th>
                            <th>Status</th>
                            <th>Id</th>
                            <th>EN</th>
                            <th>NL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="units/changed">
                            <tr>
                                <td>
                                    <xsl:value-of select="unit[1]/@id"/>
                                </td>
                                <td>BEFORE</td>
                                <td>
                                    <xsl:value-of select="unit[1]/@status"/>
                                </td>
                                <td>
                                    <xsl:value-of select="unit[1]/rm"/>
                                </td>
                                <td>
                                    <xsl:value-of select="unit[1]/name"/>
                                </td>
                                <td>
                                    <xsl:value-of select="unit[1]/nlname"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <xsl:value-of select="unit[2]/@id"/>
                                </td>
                                <td>AFTER</td>
                                <td>
                                    <xsl:value-of select="unit[2]/@status"/>
                                </td>
                                <td>
                                    <xsl:value-of select="unit[2]/rm"/>
                                </td>
                                <td>
                                    <xsl:value-of select="unit[2]/name"/>
                                </td>
                                <td>
                                    <xsl:value-of select="unit[2]/nlname"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
                <h2>Ordinals</h2>
                <h3>Removed</h3>
                <table>
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>displaName</th>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="ordinals/removed/valueSet">
                            <tr>
                                <td>
                                    <xsl:value-of select="@id"/>
                                </td>
                                <td>
                                    <xsl:value-of select="@displayName"/>
                                </td>
                                <td>
                                    <xsl:value-of select="@name"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
                <h3>Added</h3>
                <table>
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>displaName</th>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="ordinals/added/valueSet">
                            <tr>
                                <td>
                                    <xsl:value-of select="@id"/>
                                </td>
                                <td>
                                    <xsl:value-of select="@displayName"/>
                                </td>
                                <td>
                                    <xsl:value-of select="@name"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
                <h3>Changed</h3>
                <div>See publication for details</div>
                <table>
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>displayName</th>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="ordinals/changed/valueSet">
                            <tr>
                                <td>
                                    <xsl:value-of select="@id"/>
                                </td>
                                <td>
                                    <xsl:value-of select="@displayName"/>
                                </td>
                                <td>
                                    <xsl:value-of select="@name"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
                <h2>Panels</h2>
                <h3>Removed</h3>
                <table>
                    <thead>
                        <tr>
                            <th>LOINC</th>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="panels/removed">
                            <tr>
                                <td>
                                    <xsl:value-of select="Loinc"/>
                                </td>
                                <td>
                                    <xsl:value-of select="LoincName"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
                <h3>Added</h3>
                <table>
                    <thead>
                        <tr>
                            <th>LOINC</th>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="panels/added">
                            <tr>
                                <td>
                                    <xsl:value-of select="Loinc"/>
                                </td>
                                <td>
                                    <xsl:value-of select="LoincName"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
                <h3>Changed</h3>
                <table>
                    <thead>
                        <tr>
                            <th>Changed</th>
                            </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="panels/changed">
                            <tr>
                                <td>
                                    <xsl:value-of select="text()"/>
                                </td>
                                </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>