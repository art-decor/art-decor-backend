<!-- 
    Author: Marc de Graauw 2013
    Copyright: to be decided, until then all rights reserved 
    
    Output: 
--><xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
    <xsl:output method="xml" indent="yes" exclude-result-prefixes="#all"/>
    <xsl:strip-space elements="*"/>
    <xsl:template match="publication" exclude-result-prefixes="#all">
        <publication type="simple">
            <xsl:copy-of select="(@* except @type)"/>
            <xsl:comment>This dataset uses LOINC, UCUM and SNOMED. 
The use of SNOMED requires a license, which can be requested at https://mlds.ihtsdotools.org/. 
LOINC and UCUM can be used free of charge, provided you adhere to their respective license agreements at https://loinc.org/license/ and http://unitsofmeasure.org/ucum.html#license.</xsl:comment>
            <xsl:apply-templates exclude-result-prefixes="#all"/>
        </publication>
    </xsl:template>
    <xsl:template match="lab_concept">
        <lab_concept>
            <xsl:copy-of select="(@status, @lastUpdate)"/>
            <xsl:apply-templates select="concept[@loinc_num]"/>
            <xsl:apply-templates select="materials"/>
            <xsl:apply-templates select="outcomes"/>
            <xsl:apply-templates select="units"/>
            <xsl:copy-of select="retired-reason"/>
            <xsl:copy-of select="retired-replacement"/>
            <xsl:copy-of select="releasenote"/>
        </lab_concept>
    </xsl:template>
    <xsl:template match="concept[@loinc_num]">
        <loincConcept>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates exclude-result-prefixes="#all"/>
            <references>
                <xsl:variable name="url" select="concat('https://terminologie.nictiz.nl/art-decor/loinc?conceptId=', @loinc_num)"/>
                <a href="{$url}">
                    <xsl:value-of select="$url"/>
                </a>
                <xsl:variable name="url" select="concat('https://search.loinc.org/searchLOINC/search.zul?query=', @loinc_num)"/>
                <a href="{$url}">
                    <xsl:value-of select="$url"/>
                </a>
            </references>
        </loincConcept>
    </xsl:template>
    <!-- als er geen component is, is er ook geen goede vertaling -->
    <xsl:template match="concept[@language][not(component)]"/>
    <xsl:template match="concept[@language][component]">
        <translation>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates exclude-result-prefixes="#all"/>
        </translation>
    </xsl:template>
    <xsl:template match="elem[@name = 'PanelType']">
        <panelType>
            <xsl:value-of select="."/>
        </panelType>
    </xsl:template>
    <xsl:template match="elem | relatedNames2 | shortName | exUnits | exUCUMunits | comment "/>
    <xsl:template match="@name | @count | @length"/>
    <xsl:template match="material[ancestor::lab_concept]">
        <material>
            <xsl:copy-of select="@code | @displayName"/>
        </material>
    </xsl:template>
    <xsl:template match="unit[@ref]">
        <unit>
            <xsl:copy-of select="@ref"/>
        </unit>
    </xsl:template>
    <xsl:template match="valueSet[@ref]">
        <valueSet>
            <xsl:copy-of select="@ref"/>
        </valueSet>
    </xsl:template>
    <xsl:template match="valueSet[@id]">
        <valueSet>
            <xsl:copy-of select="(@* except @versionLabel)"/>
            <xsl:copy-of select="conceptList"></xsl:copy-of>
        </valueSet>
    </xsl:template>
    <xsl:template match="panel[(@type = 'panel') or (@type = 'localpanel')]">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="concept[ancestor::panel]">
        <loincConcept loinc_num="{Loinc}">
            <xsl:copy-of select="@*"/>
            <xsl:if test="parent::panel">
                <xsl:copy-of select="parent::panel/@type"/>
            </xsl:if>
            <xsl:copy-of select="(SEQUENCE, LoincName, ObservationRequiredInPanel)"/>
            <xsl:if test="concept">
                <members>
                    <xsl:for-each select="concept">
                        <xsl:sort select="SEQUENCE"/>
                        <xsl:apply-templates select="."/>
                    </xsl:for-each>
                </members>
            </xsl:if>
            </loincConcept>
    </xsl:template>
    <xsl:template match="comment()"/>
    <xsl:template match="@* | element()">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>