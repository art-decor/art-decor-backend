<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    <xsl:output method="html"/>
    <xsl:template match="publication">
        <html style="font-size:14px;">
            <head>
                <title>Publicatie(<xsl:value-of select="publication/@effectiveDate"/>)</title>
                <meta charset="utf-8"/>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"/>
                <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css"/>
                <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"/>
            </head>
            <body>
                <h1>Publicatie <xsl:value-of select="@effectiveDate"/>
                </h1>
                <div>
                    <xsl:value-of select="desc"/>
                </div>
                <table id="table_id" class="display">
                    <thead>
                        <tr>
                            <th>Volledige naam</th>
                            <th>Status</th>
                            <th>LOINC</th>
                            <th>LOINC status</th>
                            <th>Component</th>
                            <th>Kenmerk</th>
                            <th>Timing</th>
                            <th>Systeem</th>
                            <th>Schaal</th>
                            <th>Methode</th>
                            <th>Panel</th>
                            <th>Klasse</th>
                            </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="//lab_concept/concept">
                            <xsl:variable name="concept" select="if (concept/component) then concept else ."/>
                            <tr>
                                <td>
                                    <xsl:value-of select="$concept/longName"/>
                                </td>
                                <td>
                                    <xsl:value-of select="../@status/string()"/>
                                </td>
                                <td>
                                    <xsl:value-of select="$concept/elem[@name='LOINC_NUM']"/>
                                </td>
                                <td>
                                    <xsl:value-of select="@status/string()"/>
                                </td>
                                <td>
                                    <xsl:value-of select="$concept/component"/>
                                </td>
                                <td>
                                    <xsl:value-of select="$concept/property"/>
                                </td>
                                <td>
                                    <xsl:value-of select="$concept/timing"/>
                                </td>
                                <td>
                                    <xsl:value-of select="$concept/system"/>
                                </td>
                                <td>
                                    <xsl:value-of select="$concept/scale"/>
                                </td>
                                <td>
                                    <xsl:value-of select="$concept/methode"/>
                                </td>
                                <td>
                                    <xsl:value-of select="$concept/elem[@name='PanelType']"/>
                                </td>
                                <td>
                                    <xsl:value-of select="$concept/class"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
                <script>
                    $(document).ready( function () {
                    $('#table_id').DataTable();
                    } );
                </script>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>