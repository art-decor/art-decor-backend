server {
        listen       80;
        # listen       [::]:80;
        server_name  terminologie.test-nictiz.nl;
        root         /var/www/letsencrypt;

    location /.well-known/acme-challenge {
        root /var/www/letsencrypt;
    }
    location / {
        return 301 https://terminologie.test-nictiz.nl;
    }
}
