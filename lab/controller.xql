xquery version "3.0";
(:import module namespace console     = "http://exist-db.org/xquery/console";:)
import module namespace labterm            = "http://art-decor.org/ns/labterm" at "api/api-labterm.xqm";
declare variable $exist:path external;
declare variable $exist:resource external;
declare variable $exist:controller external;

(:let $log := console:log(concat($exist:path, '?', request:get-query-string())):)
let $parts := tokenize($exist:path, '/')
let $accept := request:get-header('Accept')
let $method := request:get-method()

let $format :=  
    if (contains($accept, 'json')) then 'json' 
    else if (contains($accept, 'xml')) then 'xml' 
    else 'html'
let $rest       := if (tokenize($exist:path, '/')[2] = 'api') then true() else false()
let $system     := tokenize($exist:path, '/')[3]
let $id         := tokenize($exist:path, '/')[4]
let $searchloinc := if ($system = 'loinc') then 'true' else 'false'
let $language   := 'nl-NL'
let $statuses := ''

return if (labterm:isLocked() and not(labterm:isDba())) then <locked/> else
if ($exist:path = "/prerelease") then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/../lab-data/data/prerelease.xml">
            <set-header name="Cache-Control" value="max-age=3600, must-revalidate"/>
        </forward>
    </dispatch>
else if ($exist:path = "/labtable") then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/modules/get-lab-table-json.xquery" method="GET">
        </forward>
    </dispatch>
else if ($parts[2] = 'publications' and contains($accept, 'text/html')) then
    let $url := concat($exist:controller, '/../lab-data/', $exist:path)
    return
 <dispatch xmlns="http://exist.sourceforge.net/NS/exist"> <forward url="{$url}">
            <set-header name="Cache-Control" value="max-age=3600, must-revalidate"/>
        </forward> </dispatch>
else if (request:get-method()='GET' and $rest) then
    if ($format='xml') then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/modules/get-concept.xquery" method="GET">
            <add-parameter name="id" value="{$id}"/>
        <add-parameter name="loinc" value="{$searchloinc}"/>
                <add-parameter name="status" value="{$statuses}"/>
                <add-parameter name="language" value="{$language}"/>
            </forward>
    </dispatch>
else if ($format='json') then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/modules/get-concept-json.xquery" method="GET">
                <add-parameter name="id" value="{$id}"/>
                <add-parameter name="loinc" value="{$searchloinc}"/>
                <add-parameter name="status" value="{$statuses}"/>
                <add-parameter name="language" value="{$language}"/>
            </forward>
        </dispatch>
    else 
        <ignore xmlns="http://exist.sourceforge.net/NS/exist">
            <cache-control cache="no"/>
        </ignore>
else  
        <ignore xmlns="http://exist.sourceforge.net/NS/exist">
            <cache-control cache="no"/>
        </ignore>
