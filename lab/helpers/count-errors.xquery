xquery version "3.0";
import module namespace labterm = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

for $error in distinct-values($labterm:labConcepts//lab_concept[@status=('update', 'active')]//(error | warning)/@code)
return ($error, count($labterm:labConcepts//lab_concept[@status=('update', 'active')]//(error | warning)[@code=$error]))