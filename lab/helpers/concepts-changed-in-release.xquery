xquery version "3.0";

import module namespace labterm = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

$labterm:labConcepts//lab_concept[@status='active']/concept[elem[@name='VersionLastChanged']='2.64']/@loinc_num/string()