import module namespace labterm     = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";
 
for $local in $labterm:localPanels/concept
let $id := $local/Loinc/string()
let $loinc := collection($labterm:strLoincPanels)//concept[Loinc=$id]
return  (
 for $localmember in $local/concept[not(@panelMember="added")]
 let $loincmember := ($loinc/concept[Loinc=$localmember/Loinc])[1]
 return 
    if (not($loincmember)) then <changed>Local panel {$local/Loinc/text()} is missing {$localmember/Loinc/text()} in loinc version</changed>
    else if ($localmember/ObservationRequiredInPanel/text() != $loincmember/ObservationRequiredInPanel/text()) then <changed>Panel {$local/Loinc/text()} has a different ObservationRequiredInPanel value for {$localmember/Loinc/text()}, was {$localmember/ObservationRequiredInPanel/text()}, is {$loincmember/ObservationRequiredInPanel/text()} in newest version</changed>
    else ()
 ,
 for $loincmember in $loinc/concept
 let $localmember := $local/concept[Loinc=$loincmember/Loinc]
 return 
     if (not($localmember)) then <changed>Local panel {$loinc/Loinc/text()} is missing {$loincmember/Loinc/text()} in local version</changed>
     else ()
)