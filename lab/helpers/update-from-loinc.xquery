(:
Deze module gebruiken na opname van een nieuwe LOINC release.

Deze zal:
- LOINC concepten vervangen door de nieuwere LOINC concepten
- Controleren of status LOINC <> ACTIVE is voor concepten die wel active zijn in LCS
- Foutmelding toevoegen voor beide bovenstaande issues.

LET OP!
Na draaien: controleren en verwijderen van old-data als alles goed gegaan is.
:)
xquery version "3.0";
import module namespace labterm = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

let $old := concat($labterm:strLabData, '/old_data')
let $do := if(xmldb:collection-available($old)) then xmldb:remove($old) else ()
let $new := concat($labterm:strLabData, '/lab_concepts')
let $do := xmldb:rename($new, 'old_data')
let $do := xmldb:create-collection($labterm:strLabData, 'lab_concepts')
let $do := sm:chmod($old, 'rwxrwxr-x')
let $do := sm:chmod($new, 'rwxrwxr-x')
let $do := sm:chgrp($old, 'lab')
let $do := sm:chown($old, 'admin')
let $do := sm:chgrp($new, 'lab')
let $do := sm:chown($new, 'admin')

for $labConcept in collection($old)//lab_concept
return 
    let $loincConcept := $labterm:loincConcepts//concept[@loinc_num = $labConcept/concept/@loinc_num]
    return 
        let $statuserror := 
            if ($labConcept/concept/@status="PRERELEASE")
            then
                <error src="LOINCrelease" code="STATUSCHANGE" dateTime="{fn:current-dateTime()}">Lab concept was from LOINC prerelease, needs to be checked</error> 
            else if ($loincConcept/@status != 'ACTIVE' and $labConcept/@status='active')
            then 
                <error src="LOINCrelease" code="STATUSCHANGE" dateTime="{fn:current-dateTime()}">LOINC status is {$loincConcept/@status/string()}, Labcodeset status was: {$labConcept/@status/string()}</error> 
            else ()
        (: Check  'component', 'property', 'timing', 'system', 'scale', 'method' :)
        let $axiserrors := ()
        (: LCB-75: Wijziging Engelse component leidt niet tot status change (teveel spellingsdingetjes :)
        (:let $axiserrors := 
            if ($loincConcept/concept/component != $labConcept/concept/component)
            then 
                ($axiserrors, <error src="LOINCrelease" code="AXISCHANGE" dateTime="{fn:current-dateTime()}">LOINC component changed to {$loincConcept/component/string()}, Labcodeset was: {$labConcept/component/string()}</error>) 
            else $axiserrors:)
        let $axiserrors := 
            if ($loincConcept/property != $labConcept/concept/property)
            then 
                ($axiserrors, <error src="LOINCrelease" code="AXISCHANGE" dateTime="{fn:current-dateTime()}">LOINC property changed to {$loincConcept/property/string()}, Labcodeset was: {$labConcept/concept/property/string()}</error>) 
            else $axiserrors
        let $axiserrors := 
            if ($loincConcept/timing != $labConcept/concept/timing)
            then 
                ($axiserrors, <error src="LOINCrelease" code="AXISCHANGE" dateTime="{fn:current-dateTime()}">LOINC timing changed to {$loincConcept/timing/string()}, Labcodeset was: {$labConcept/concept/timing/string()}</error>) 
            else $axiserrors
        let $axiserrors := 
            if ($loincConcept/system != $labConcept/concept/system)
            then 
                ($axiserrors, <error src="LOINCrelease" code="AXISCHANGE" dateTime="{fn:current-dateTime()}">LOINC system changed to {$loincConcept/system/string()}, Labcodeset was: {$labConcept/concept/system/string()}</error>) 
            else $axiserrors
        let $axiserrors := 
            if ($loincConcept/scale != $labConcept/concept/scale)
            then 
                ($axiserrors, <error src="LOINCrelease" code="AXISCHANGE" dateTime="{fn:current-dateTime()}">LOINC scale changed to {$loincConcept/scale/string()}, Labcodeset was: {$labConcept/concept/scale/string()}</error>) 
            else $axiserrors
        let $axiserrors := 
            if ($loincConcept/method != $labConcept/concept/method)
            then 
                ($axiserrors, <error src="LOINCrelease" code="AXISCHANGE" dateTime="{fn:current-dateTime()}">LOINC method changed to {$loincConcept/method/string()}, Labcodeset was: {$labConcept/concept/method/string()}</error>) 
            else $axiserrors
        let $newConcept := 
            (: This shouldn't occur, just to make sure nothing is lost if it does. Does occur for older T-concepts. :)
            if (not($loincConcept)) 
            then 
                <lab_concept>
                {
                $labConcept/@*, $labConcept/*
                }
                </lab_concept>
            else
            <lab_concept>
                {if (($statuserror or $axiserrors) and ($labConcept/@status = 'active')) 
 then (attribute status {'update'}, attribute user {'system'}, attribute previousStatus {$labConcept/@status}, attribute previousUser {$labConcept/@user})
 else ($labConcept/(@* except @last_update))
                    ,
                    attribute {'last_update'} {current-dateTime()}
                    ,
                    <concept>
                    {
                        $loincConcept/@*,
                        $loincConcept/(* except concept),
                        $loincConcept/concept[@language='nl-NL']
                    }
                    </concept>
                    ,
                    $labConcept/(* except (concept, errors)),
                    <errors>{$labConcept/errors/*, $statuserror, $axiserrors}</errors>
                }
            </lab_concept>
            let $file := concat('lab_concept_', $labConcept/concept/@loinc_num, '.xml')
            let $do := xmldb:store($new, $file, $newConcept)
            let $do := sm:chmod(xs:anyURI(concat($new, '/', $file)), 'rwxrwxr-x')
            let $do := sm:chgrp(xs:anyURI(concat($new, '/', $file)), 'lab')
            let $do := sm:chown(xs:anyURI(concat($new, '/', $file)), 'admin')
            let $do := 
                if (($statuserror or $axiserrors) and ($labConcept/@status = 'active')) 
                then 
                    let $logdata := <statusChange object="concept" statusCode="update" effectiveTime="{substring(xs:string(current-dateTime()), 1, 19)}" user="system" loincId="{$labConcept/concept/@loinc_num}"/>
                    return labterm:appendLog($logdata)
                 else ()
return ()