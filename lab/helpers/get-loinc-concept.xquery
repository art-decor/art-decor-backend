xquery version "3.0";

import module namespace labterm  = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";
import module namespace labpanels = "http://art-decor.org/ns/labpanels" at "../api/api-labpanels.xqm";

(:labterm:getLabConceptBySearchTerm('coag surf'):)

(: $search as xs:string, $language as xs:string, $searchloinc as xs:boolean, $searchprerelease as xs:boolean, $statusSearch as xs:string, $user as xs:string, $show as xs:integer?, $onlyErrors as xs:boolean?, $onlyComments as xs:boolean? :) 
(:labterm:getLabConceptBySearchTerm('47700', 'nl-NL', false(), true(), '', '', (), (), ()):)

(:labterm:getLabConceptById('24332-9', 'nl-NL', false(), ''):)

(:$labterm:colLoincDb//concept[@loinc_num='614-8']:)

(:labterm:getOnlyLabConceptById('50210-4'):)

(: Stored concept :)
(:let $id := '58093-6'
return doc(concat($labterm:strLabData, '/lab_concepts/lab_concept_', $id, '.xml')):)

labterm:getOnlyLabConceptById('74625-5')

(:labpanels:getLabPanelById('48798-3', 'nl-NL'):)

(:labpanels:getLabPanelTreeById('48798-3', 'nl-NL'):)

(:collection($labterm:strLoincPanels)//concept[Loinc='78960-2']:)