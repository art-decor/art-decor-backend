xquery version "3.0";
import module namespace labterm = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

let $old := concat($labterm:strLabData, '/old_data')
return if(xmldb:collection-available($old)) then xmldb:remove($old) else ()
