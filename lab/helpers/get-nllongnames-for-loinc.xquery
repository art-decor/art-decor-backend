import module namespace labterm            = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";
import module namespace nlLongName  = "http://art-decor.org/ns/nlLongName" at "../api-nl-longname.xqm";

(: Get all NL longNames for LOINC, runs in about 1.5 hours :)

<doc>{
let $loincnl := ($labterm:loincConcepts//concept[@loinc_num][concept[@language='nl-NL']/component])
for $concept in $loincnl
let $lab_concept :=
<lab_concept>
    <concept>{$concept/@*, $concept/(* except concept), $concept/concept[@language="nl-NL"]}</concept>
</lab_concept>
return 
<concept id="{$lab_concept/concept/@loinc_num}" component="{nlLongName:getLongName($lab_concept)}"/>
}</doc>