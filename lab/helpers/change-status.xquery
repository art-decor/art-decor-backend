xquery version "3.0";

import module namespace labterm    = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

let $concept := $labterm:labConcepts//@loinc_num[.='73580-3']/ancestor::lab_concept
let $do := update replace $concept/@status with 'draft'
let $do := update replace $concept/@user with 'jan'
return $concept