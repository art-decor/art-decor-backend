xquery version "3.0";
import module namespace labterm = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

let $loincId := '33966-3'
(:
Take care - remove all concepts !!
for $loincId in $labterm:labConcepts/lab_concept/concept/@loinc_num/string()
:)
return xmldb:remove($labterm:strLabConcept, concat('lab_concept_', $loincId, '.xml'))