xquery version "3.0";
import module namespace labterm = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

for $status in distinct-values($labterm:labConcepts//lab_concept/@status)
return ($status, count($labterm:labConcepts//lab_concept[@status=$status]))