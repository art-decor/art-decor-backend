xquery version "3.0";
import module namespace labterm = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";
import module namespace nlLongName  = "http://art-decor.org/ns/nlLongName" at "api-nl-longname.xqm";

let $all := 
<alllongnames>{
for $concept in ($labterm:labConcepts//lab_concept)
return <concept loinc_num="{$concept/concept/@loinc_num}">{nlLongName:getLongName($concept)}</concept>
}</alllongnames>
return 
<duplicates>{
for $double in $all/concept
return ($all/concept[text() = $double/text()])[2]
}</duplicates>