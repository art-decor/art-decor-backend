xquery version "3.0";

import module namespace snomed     = "http://art-decor.org/ns/terminology/snomed" at "../../terminology/snomed/api/api-snomed.xqm";

let $id := '44145005'
let $concept := snomed:getRawConcept($id)

return $concept