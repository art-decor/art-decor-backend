xquery version "3.0";
import module namespace labterm = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";
import module namespace nlLongName  = "http://art-decor.org/ns/nlLongName" at "api-nl-longname.xqm";

<classes>{
    for $class in distinct-values($labterm:labConcepts//class)
    let $concepten := $labterm:labConcepts//concept[@loinc_num][class=$class]/ancestor::lab_concept
    return  
    <class>
        <class>{$class}</class>
        <total>{count($concepten)}</total>
        <initial>{count($concepten[@status='initial'])}</initial>
        <draft>{count($concepten[@status='draft'])}</draft>
        <active>{count($concepten[@status='active'])}</active>
        <update>{count($concepten[@status='update'])}</update>
        <retired>{count($concepten[@status='retired'])}</retired>
    </class>
}</classes>