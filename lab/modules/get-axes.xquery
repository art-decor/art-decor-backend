xquery version "3.0";

import module namespace labterm            = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

<axis language="nl-NL">
    <property>
        <value/>
        {
        for $value in distinct-values($labterm:labConcepts//concept[@language='nl-NL']/property/string())
        order by $value
        return <value>{$value}</value>
        }        
    </property>
    <timing>
        <value/>
        {
        for $value in distinct-values($labterm:labConcepts//concept[@language='nl-NL']/timing/string())
        order by $value
        return <value>{$value}</value>
        }        
    </timing>
    <system>
        <value/>
        {
        for $value in distinct-values($labterm:labConcepts//concept[@language='nl-NL']/system/string())
        order by $value
        return <value>{$value}</value>
        }        
    </system>
    <scale>
        <value/>
        {
        for $value in distinct-values($labterm:labConcepts//concept[@language='nl-NL']/scale/string())
        order by $value
        return <value>{$value}</value>
        }        
    </scale>
    <class>
        <value/>
        {
        for $value in distinct-values($labterm:labConcepts/lab_concept/concept/class/string())
        order by $value
        return <value>{$value}</value>
        }        
    </class>
</axis>