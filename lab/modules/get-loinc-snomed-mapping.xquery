xquery version "3.0";

import module namespace labterm            = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

<map>{
    for $material in $labterm:labLoincSnomedMapping/material
    order by $material/@system
    return $material
}</map>