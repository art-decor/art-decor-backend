xquery version "3.0";

declare namespace request       = "http://exist-db.org/xquery/request";
declare namespace response      = "http://exist-db.org/xquery/response";

let $fileName           := if (request:exists()) then request:get-parameter('fileName','') else ''
let $publicationsDir    := '/db/apps/lab-data/publications/'
let $htmlName           := concat($fileName, '.html')
let $xmlName            := concat($fileName, '.xml')
let $fullName           := if (contains($fileName, 'labconcepts-')) then concat(replace($fileName, 'labconcepts-', 'labconcepts-full-'), '.xml') else ()

(: Don't fail for full xml or html, may not be there for older pubs :)
let $do                 :=
 try {
        if ($fullName) then xmldb:remove($publicationsDir, $fullName) else ()
    } catch * {'No full XML'}
let $do                 :=
 try {
        if ($htmlName) then xmldb:remove($publicationsDir, $htmlName) else ()
    } catch * {'No full HTML'}
return xmldb:remove($publicationsDir, $xmlName) 