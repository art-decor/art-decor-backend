xquery version "3.0";

import module namespace labterm            = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

let $ids := if (request:exists()) then request:get-parameter('ids','') else 'coli' 

let $results :=
    for $id in tokenize($ids, ';')
    return labterm:changeStatus('draft', $id)
    (: This will return 'OK' for success, otherwise a specified error :)
return <result>OK</result>
