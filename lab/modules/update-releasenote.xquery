xquery version "3.0";

declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";

import module namespace labterm            = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

let $loincId := if (request:exists()) then request:get-parameter('loincId','') else ''
let $data := if (request:exists()) then request:get-data() else <test>test 4</test>
let $releasenote := $data/string()

let $result        := 
 try {<result status="OK">{labterm:updateReleaseNote($loincId, $releasenote)}</result>}
 catch * {<error status="NOK" message="{$err:description}"/>}
return $result