xquery version "3.0";

declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";

import module namespace labterm            = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

declare %private function local:addUnitNames($node as node()) as node() {
    <units>{
        for $child in $node/unit
        return <unit>{$child/@*, $labterm:labUnits//unit[@id=$child/@ref]/rm}</unit>
    }</units>
};

let $pub1Date:= if (request:exists()) then request:get-parameter('pub1', '') else 'labvoor'
let $pub2Date := if (request:exists()) then request:get-parameter('pub2', '') else 'labna'
(:let $pub1Date := if (request:exists()) then request:get-parameter('pub1', '') else '20190515-test-first'
let $pub2Date := if (request:exists()) then request:get-parameter('pub2', '') else '20190515-test-second':)

let $pub1 := collection($labterm:strLabPublications)//publication[@effectiveDate=$pub1Date][not(@type) or @type='full']
let $pub2 := collection($labterm:strLabPublications)//publication[@effectiveDate=$pub2Date][not(@type) or @type='full']

let $diff := 
    <diff generated="{fn:current-dateTime()}">
        <publication1>{$pub1/@*, $pub1/desc}</publication1>
        <publication2>{$pub2/@*, $pub2/desc}</publication2>
        <concepts>
            {
            (: in pub1, niet in pub2 :)
            for $id in $pub1//concept/@loinc_num
            let $pub2concept := $pub2//lab_concept[concept/@loinc_num = $id]
            return if ($pub2concept) then () else <removed>{$id, attribute lcsStatus {$id/../../@status}, attribute loincStatus {$id/../@status}, $pub1//concept[@loinc_num=$id]/concept/longName}</removed>
            }
            {
            (: in pub2 retired :)
            for $pub2concept in $pub2//lab_concept[@status='retired']
            let            $pub1concept := $pub1//lab_concept[concept/@loinc_num = $pub2concept/concept/@loinc_num]
                return <retired>{$pub2concept/concept/@loinc_num, attribute oldStatus {$pub1concept/@status/string()}, attribute lcsStatus {$pub2concept/@status/string()}, attribute loincStatus {$pub2concept/concept/@status/string()}, $pub2concept/concept/concept/longName}</retired>
                }
            {
                (: in pub2, niet retired, niet in pub1 :)
            for $id in $pub2//lab_concept[not(@status='retired')]/concept[not(@loinc_num=$pub1//@loinc_num)]/@loinc_num
            return <added>{$id, attribute lcsStatus {$id/../../@status}, attribute loincStatus {$id/../@status}, $pub2//concept[@loinc_num=$id]/concept/longName}</added>
            }
            {
            (: in pub2, niet retired en ook in pub1 :)
            for $id in $pub2//lab_concept[not(@status='retired')]/concept[@loinc_num=$pub1//@loinc_num]/@loinc_num
            for $pub1concept in $pub1//lab_concept[concept/@loinc_num=$id]
            let $pub2concept := $id/ancestor::lab_concept
            (: Generic approach does not perform
            let $names := ('component', 'property', 'timing', 'system', 'scale', 'method', 'shortName', 'longName')
            :)
            let $diffen := 
                (
 if ($pub2concept/concept/component != $pub1concept/concept/component) then ($pub1concept/concept/component, $pub2concept/concept/component) else () 
                ,if ($pub2concept/concept/property != $pub1concept/concept/property) then ($pub1concept/concept/property, $pub2concept/concept/property) else () 
                ,if ($pub2concept/concept/timing != $pub1concept/concept/timing) then ($pub1concept/concept/timing, $pub2concept/concept/timing) else () 
            ,if ($pub2concept/concept/system != $pub1concept/concept/system)                then ($pub1concept/concept/system, $pub2concept/concept/system) else                () 
                ,if ($pub2concept/concept/scale != $pub1concept/concept/scale) then ($pub1concept/concept/scale, $pub2concept/concept/scale) else () 
                ,if ($pub2concept/concept/method != $pub1concept/concept/method) then ($pub1concept/concept/method, $pub2concept/concept/method) else () 
                ,if ($pub2concept/concept/shortName != $pub1concept/concept/shortName) then ($pub1concept/concept/shortName, $pub2concept/concept/shortName) else () 
                ,if ($pub2concept/concept/longName != $pub1concept/concept/longName) then ($pub1concept/concept/longName, $pub2concept/concept/longName) else () 
            )
            let $diffnl := 
                (
 if ($pub2concept/concept/concept/component != $pub1concept/concept/concept/component) then ($pub1concept/concept/concept/component, $pub2concept/concept/concept/component) else () 
                ,if ($pub2concept/concept/concept/property != $pub1concept/concept/concept/property) then ($pub1concept/concept/concept/property, $pub2concept/concept/concept/property) else () 
                ,if ($pub2concept/concept/concept/timing != $pub1concept/concept/concept/timing) then ($pub1concept/concept/concept/timing, $pub2concept/concept/concept/timing) else () 
                ,if ($pub2concept/concept/concept/system != $pub1concept/concept/concept/system) then ($pub1concept/concept/concept/system, $pub2concept/concept/concept/system) else () 
                ,if ($pub2concept/concept/concept/scale != $pub1concept/concept/concept/scale) then ($pub1concept/concept/concept/scale, $pub2concept/concept/concept/scale) else () 
                ,if ($pub2concept/concept/concept/method != $pub1concept/concept/concept/method) then ($pub1concept/concept/concept/method, $pub2concept/concept/concept/method) else () 
                ,if ($pub2concept/concept/concept/shortName != $pub1concept/concept/concept/shortName) then ($pub1concept/concept/concept/shortName, $pub2concept/concept/concept/shortName) else () 
            ,if ($pub2concept/concept/concept/longName != $pub1concept/concept/concept/longName) then ($pub1concept/concept/concept/longName, $pub2concept/concept/concept/longName) else () 
                )
            let $diffstatus := 
                if ($pub2concept/@status != $pub1concept/@status) then (<status>{$pub1concept/@status/string()}</status>, <status>{$pub2concept/@status/string()}</status>) else ()
            let $diffloincstatus := 
                if ($pub2concept/concept/@status != $pub1concept/concept/@status) then (<loincstatus>{$pub1concept/concept/@status/string()}</loincstatus>, <loincstatus>{$pub2concept/concept/@status/string()}</loincstatus>) else ()
            let $matdiff :=
                if (string-join($pub2concept/materials//@code, ' ') != string-join($pub1concept/materials//@code, ' ')) then ($pub1concept/materials, $pub2concept/materials) else ()
            let $unitdiff :=
                if ($pub2concept/units//@ref != $pub1concept/units//@ref) then (local:addUnitNames($pub1concept/units), local:addUnitNames($pub2concept/units)) else ()
            return 
                if ($diffen or $diffnl or $diffstatus or $diffloincstatus or $matdiff or $unitdiff)
                then
                    <changed>
                        {<concept>{$pub2concept/concept/@loinc_num, $diffstatus, $diffloincstatus, <english>{$diffen}</english>, <nederlands>{$diffnl}</nederlands>, $matdiff, $unitdiff}</concept>}
                    </changed>
                else ()
            }
        </concepts>
        <map>
            {
            for $material in $pub1/map/material[not(@code=$pub2/map/material/@code)]
            return <removed>{$material}</removed>
            }
            {
            for $material in $pub2/map/material[not(@code=$pub1/map/material/@code)]
            return <added>{$material}</added>
            }
            {
            for $material1 in $pub1/map/material[@code=$pub2/map/material/@code]
            let $material2 := $pub2/map/material[@code=$material1/@code]
            return if (fn:deep-equal($material1, $material2)) then () else <changed>{$material1, $material2}</changed>
            }
        </map>
        <units>
            {
            for $unit in $pub1/units/unit[not(@id=$pub2/units/unit/@id)]
            return <removed>{$unit}</removed>
            }
            {
            for $unit in $pub2/units/unit[not(@id=$pub1/units/unit/@id)]
            return <added>{$unit}</added>
            }
            {
            for $unit1 in $pub1/units/unit[@id=$pub2/units/unit/@id]
            let $unit2 := $pub2/units/unit[@id=$unit1/@id]
            return if (fn:deep-equal($unit1, $unit2)) then () else <changed>{$unit1, $unit2}</changed>
            }
        </units>
        <ordinals>
            {
            for $ordinal in $pub1/ordinals/valueSet[not(@id=$pub2/ordinals/valueSet/@id)]
            return <removed>{$ordinal}</removed>
            }
            {
            for $ordinal in $pub2/ordinals/valueSet[not(@id=$pub1/ordinals/valueSet/@id)]
            return <added>{$ordinal}</added>
            }
            {
            for $ordinal1 in $pub1/ordinals/valueSet[@id=$pub2/ordinals/valueSet/@id]
            let $ordinal2 := $pub2/ordinals/valueSet[@id=$ordinal1/@id]
            return if (fn:deep-equal($ordinal1, $ordinal2)) then () else <changed>{$ordinal1, $ordinal2}</changed>
            }
        </ordinals>
        <nominals>
            {
            for $nominal in $pub1/nominals/refset[not(@conceptId=$pub2/nominals/refset/@conceptId)]
            return <removed>{$nominal}</removed>
            }
            {
            for $nominal in $pub2/nominals/refset[not(@conceptId=$pub1/nominals/refset/@conceptId)]
            return <added>{$nominal}</added>
            }
            {
            for $nominal1 in $pub1/nominals/refset[@conceptId=$pub2/nominals/refset/@conceptId]
            let $nominal2 := $pub2/nominals/refset[@conceptId=$nominal1/@conceptId]
            return if (fn:deep-equal($nominal1, $nominal2)) then () else <changed>{$nominal1, $nominal2}</changed>
            }
        </nominals>
        <panels>
            {
            for $panel in $pub1/panels/panel/concept[not(Loinc=$pub2/panels/panel/concept/Loinc)]
            return <removed>{$panel/Loinc, $panel/LoincName}</removed>
            }
            {
            for $panel in $pub2/panels/panel/concept[not(Loinc=$pub1/panels/panel/concept/Loinc)]
            return <added>{$panel/Loinc, $panel/LoincName}</added>
            }
            {
            for $panel1 in $pub1/panels/panel[concept/Loinc=$pub2/panels/panel/concept/Loinc]
            let $panel2 := $pub2/panels/panel[concept/Loinc=$panel1/concept/Loinc]
            return 
 (
                for $member1 in $panel1/concept/concept
 let $member2 := $panel2/concept/concept[Loinc=$member1/Loinc]
                return 
                    if (not($member2)) then <changed>Panel {$panel1/concept/Loinc/text()} is missing {$member1/Loinc/text()} in newest version</changed>
 else if ($member1/ObservationRequiredInPanel != $member2/ObservationRequiredInPanel) then <changed>Panel {$panel1/concept/Loinc/text()} has a different ObservationRequiredInPanel value for {$member1/Loinc/text()}, was {$member1/ObservationRequiredInPanel/text()}, is {$member2/ObservationRequiredInPanel/text()} in newest version</changed>
                    else ()
                ,
 for $member2 in $panel2/concept/concept
                let $member1 := $panel1/concept/concept[Loinc=$member2/Loinc]
                return 
                    if (not($member1)) then <changed>Panel {$panel2/concept/Loinc/text()} is missing {$member2/Loinc/text()} in older version</changed>
            else ()
                )
            (:return if (fn:deep-equal($panel1/concept/concept, $panel2/concept/concept)) then () else <changed>{$panel2/concept/Loinc, $panel2/concept/LoincName}</changed>:)
            }
        </panels>
    </diff>

let $diffName := concat('diff-', $pub1Date, '-', $pub2Date)
let $diffXml := concat($diffName,'.xml')
let $diffHtml := concat($diffName,'.html')
let $file := xmldb:store($labterm:strLabPublications, $diffXml, $diff)
let $doc := doc(concat($labterm:strLabPublications, '/', $diffXml))
let $xslt := doc('../resources/diff2html.xsl')
let $html := transform:transform($doc, $xslt, ())
let $file := xmldb:store($labterm:strLabPublications, $diffHtml, $html)

return $diff
