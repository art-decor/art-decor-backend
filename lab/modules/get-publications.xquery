xquery version "3.0";

import module namespace labterm            = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

let $publicationsDir := '/db/apps/lab-data/publications/'
let $publicationCol := collection($publicationsDir)
let $publications :=  
    if (labterm:get-current-user() = 'guest') 
 then $publicationCol/publication[not(@type='full')][@publication='true']
 else $publicationCol/publication[not(@type='full')]
return    
    <publications> {
        for $publication in $publications
        order by $publication/@effectiveDate descending
        return  <publication selected="false">
                    {$publication/@*, $publication/desc}
                </publication>
    }
    </publications>