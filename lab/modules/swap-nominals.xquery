xquery version "3.0";

declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";
import module namespace labterm = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

(: Aanname is dat alle nominals refsets zijn en alle ordinals valueSets. :)

let $conceptId := if (request:exists()) then request:get-parameter('conceptId','') else '56022-7'
let $nominalId := if (request:exists()) then request:get-parameter('nominalId','') else '2581000146104'

let $labnominal := doc(concat($labterm:strLabData, '/nominals.xml'))/nominals/refset[@conceptId = $nominalId]
let $labconcept := $labterm:labConcepts//lab_concept[concept/@loinc_num=$conceptId]

let $do :=  update delete $labconcept/outcomes
let $do :=
    if ($nominalId = '---') then
        ()
    else if ($labnominal) then
        update insert <outcomes>{$labnominal}</outcomes> into $labconcept
    else 
		error(QName('http://art-decor.org/ns/error', 'NoSuchNominal'), concat('No nominal with id: ', $nominalId))
let $check := labterm:checkConcept($conceptId, 'nl-NL')
let $do := update replace $labconcept/@user with labterm:get-current-user()
let $labConcept     := labterm:getOnlyLabConceptById($conceptId)
return <result status="OK">{$labConcept/lab_concept}</result>