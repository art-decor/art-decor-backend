xquery version "3.0";

declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";

import module namespace labterm            = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

let $postData := request:get-data()
let $data := $postData/*
let $result := 
    try {<result>{labterm:saveUnit($data/unit)}</result>}
    catch * {<error>{$err:description}</error>}
let $result :=
    <root>
        {
        if (local-name($result) = 'error') 
        (: If error, return data plus error message :)
        then ($data/unit, $result) 
        (: If success, return empty unit :)
        else (<unit id=""><rm/><name/><nlname/></unit>)
        }
    </root>
return $result