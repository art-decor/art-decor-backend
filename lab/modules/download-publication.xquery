xquery version "3.0";

declare namespace request       = "http://exist-db.org/xquery/request";
declare namespace response      = "http://exist-db.org/xquery/response";

let $download           := if (request:exists()) then request:get-parameter('download','false')='true' else (false())
let $open               := if (request:exists()) then request:get-parameter('open','false')='true' else (false())
let $fileName           := if (request:exists()) then request:get-parameter('fileName','') else ''
let $publicationsDir    := '/db/apps/lab-data/publications/'
let $fileNameforDownload := doc(concat($publicationsDir, $fileName))
let $data := if($fileName and doc-available(concat($publicationsDir, $fileName))) then
                doc(concat($publicationsDir, $fileName))
             else ()    
return
(
    if (request:exists() and $data) then (
        if (ends-with($fileName, '.xml'))
        then response:set-header('Content-Type','application/xml')
        else response:set-header('Content-Type','text/html'),
        if ($download) then (
            response:set-header('Content-Disposition', concat('attachment; filename=', $fileName)),
            $fileNameforDownload
        ) else if($open) then (
            response:set-header('Content-Disposition', concat('filename=', $fileName)),
            $fileNameforDownload
        ) else()
    )
    else (
        <error>Resource {$fileName} not found.</error>    
    )
)
