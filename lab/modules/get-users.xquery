xquery version "3.0";

import module namespace labterm = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

(: We bypass the user API, to avoid dependency on unexpected changes in functionality there. :)
let $userInfoFile := doc('/db/apps/art-data/user-info.xml')
let $users :=
<users>
    <user/>
    <user>--all--</user>
    <user>--empty--</user>
    {
    if (labterm:get-current-user() = 'guest') then () else
    for $user in distinct-values($labterm:labConcepts//lab_concept/@user/string())
    let $userInfo := $userInfoFile//user[@name=$user]
    let $userDisplayName      := $userInfo/displayName
    let $userEmail            := $userInfo/email
    return if (string-length($user) >= 1) then <user email="{$userEmail}" displayName="{$userDisplayName}">{$user}</user> else ()
    }
</users>
return $users