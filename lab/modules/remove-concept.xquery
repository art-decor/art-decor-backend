xquery version "3.0";

import module namespace labterm            = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

let $loinc_num    := if (request:exists()) then request:get-parameter('loincId','21003-9') else '56146-4'
return update delete $labterm:labConcepts//lab_concept[concept/@loinc_num=$loinc_num]