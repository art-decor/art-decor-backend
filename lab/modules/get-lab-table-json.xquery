xquery version "3.0";

declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "json";
declare option output:media-type "application/json";
declare option output:json-ignore-whitespace-text-nodes "yes";
import module namespace labterm            = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

if (not(labterm:isLabUser())) 
then (response:set-status-code(401), 'Geen permissie')
else 
    <lab_concepts>{
        for $id in ($labterm:labConcepts//@loinc_num)
        let $concept := labterm:getOnlyLabConceptById($id)/lab_concept 
        return 
        <lab_concept>
            <status>{$concept/@status/string()}</status>
            <user>{$concept/@user/string()}</user>
            <unit>{$labterm:labUnits//unit[@id=$concept//unit[1]/@ref]/rm/string()}</unit>
            <outcome>{$concept//outcomes/valueSet/@displayName/string()}</outcome>
            <materials>{string-join($labterm:labLoincSnomedMapping//material[@system=$concept/concept/system/string()]/@displayName/string(), ', ')}</materials>
            {
                for $item in $concept/concept/*[@name]
                return element {$item/@name/string()} {$item/text()}
                ,
                for $item in $concept/concept/concept/*[@name]
                return element {concat('NL_', $item/@name/string())} {$item/text()}
                }            </lab_concept>
    }</lab_concepts>