xquery version "3.0";

declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";

import module namespace labterm            = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";
(:import module namespace console     = "http://exist-db.org/xquery/console";
let $log := console:log(request:get-query-string()):)

let $loincId := if (request:exists()) then request:get-parameter('loincId','') else '13533-5'
let $assignee := if (request:exists()) then request:get-parameter('assignee','') else 'marc'

return labterm:updateAssignee($loincId, $assignee)