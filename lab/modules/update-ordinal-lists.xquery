xquery version "3.0";

declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";
import module namespace labterm     = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

declare function local:update-ordinals() as xs:string {
    let $uri := "https://decor.nictiz.nl/decor/services/ValueSetIndex?prefix=lc-&amp;format=xml"
    let $requestHeaders := 
        <http:request method="GET" href="{$uri}">
            <http:header name="Content-Type" value="text/xml"/>
            <http:header name="Cache-Control" value="no-cache"/>
            <http:header name="Max-Forwards" value="1"/>
        </http:request>
    let $server-response := http:send-request($requestHeaders)
    let $server-check    :=
        if ($server-response[1]/@status='200') then () else (
            error(QName('http://art-decor.org/ns/error', 'RetrieveError'), concat('Server returned HTTP status: ',$server-response[1]/@status, ' for URI ''',$uri,''' with body ''',string-join($server-response[2], ' '),''''))
        )
    let $ordinals := 
    <ordinals>{
        for $vs in $server-response[2]/return/valueSet
            let $uri := concat("https://decor.nictiz.nl/decor/services/RetrieveValueSet?prefix=lc-&amp;format=xml&amp;id=", $vs/@id/string(), "&amp;effectiveDate=", $vs/@effectiveDate)
            let $requestHeaders := 
                <http:request method="GET" href="{$uri}">
                    <http:header name="Content-Type" value="text/xml"/>
                    <http:header name="Cache-Control" value="no-cache"/>
                    <http:header name="Max-Forwards" value="1"/>
                </http:request>
            let $server-response := http:send-request($requestHeaders)
            let $server-check    :=
                if ($server-response[1]/@status='200') then () else (
                    error(QName('http://art-decor.org/ns/error', 'RetrieveError'), concat('Server returned HTTP status: ',$server-response[1]/@status, ' for URI ''',$uri,''' with body ''',string-join($server-response[2], ' '),''''))
                )
        return $server-response//valueSet
    }</ordinals>
    return xmldb:store($labterm:strLabData, 'ordinals.xml', $ordinals)
};

let $result := 
    try {<result status="OK">{local:update-ordinals()}</result>}
    catch * {<error status="NOK">{$err:description}</error>}
return $result