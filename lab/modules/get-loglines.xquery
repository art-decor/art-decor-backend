import module namespace labterm     = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

let $loincId        := if (request:exists()) then request:get-parameter('loincId','') else "55235-6"
return 
<changes id="{$loincId}">{
    for $change in collection($labterm:strLabDataLog)//statusChange[@loincId=$loincId]
    order by $change/@effectiveTime
    return $change
}</changes>