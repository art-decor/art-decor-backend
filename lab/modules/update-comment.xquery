xquery version "3.0";

declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";

import module namespace labterm            = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

let $loincId := if (request:exists()) then request:get-parameter('loincId','') else '13533-5'
let $data := if (request:exists()) then request:get-data() else <test>test 4</test>
let $comment := $data/string()

let $result        := 
 try {<result status="OK">{labterm:updateComment($loincId, $comment)}</result>}
 catch * {<error status="NOK" message="{$err:description}"/>}
return $result