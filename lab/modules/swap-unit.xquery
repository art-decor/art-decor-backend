xquery version "3.0";

declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";
import module namespace labterm            = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

let $conceptId := if (request:exists()) then request:get-parameter('conceptId','') else '11-7'
let $unitId := if (request:exists()) then request:get-parameter('unitId','') else '31'

let $labunit := $labterm:labUnits//unit[@id = $unitId]
let $labconcept := $labterm:labConcepts//lab_concept[concept/@loinc_num=$conceptId]

let $do := 
    (
    update delete $labconcept/units
    ,
    if ($unitId = '---') then ()
    else if ($labunit) then
        let $newunit := <units><unit ref="{$unitId}"/></units>
        return update insert $newunit into $labconcept
    else 
        error(QName('http://art-decor.org/ns/error', 'NoSuchUnit'), concat('No unit with id: ', $unitId))
    )
let $do := update replace $labconcept/@user with labterm:get-current-user()
let $check      := labterm:checkConcept($conceptId, 'nl-NL')
let $labConcept := labterm:getOnlyLabConceptById($conceptId)
return <result status="OK">{$labConcept/lab_concept}</result>