xquery version "3.0";

declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";

import module namespace labterm            = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

let $newStatus      := if (request:exists()) then request:get-parameter('newStatus','') else 'active'
let $loincId        := if (request:exists()) then request:get-parameter('loincId','') else '2829-0'
let $language       := if (request:exists()) then request:get-parameter('language','') else 'nl-NL'
let $labConcept     := labterm:getOnlyLabConceptById($loincId)

let $change :=
    if ($labConcept/@count = 0)
    then labterm:addLabConcept($loincId, $language)
    else labterm:changeStatus($newStatus, $loincId)
(: This will return 'OK' for success, otherwise a specified error :)
return $change