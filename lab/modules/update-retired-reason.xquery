xquery version "3.0";

declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";

import module namespace labterm            = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

let $loincId := if (request:exists()) then request:get-parameter('loincId','') else ''
let $retired-reason := if (request:exists()) then request:get-parameter('retired-reason','') else ''

let $result := 
    try {<result status="OK">{labterm:updateRetiredReason($loincId, $retired-reason)}</result>}
    catch * {<error status="NOK" message="{$err:description}"/>}
return $result