xquery version "3.0";

import module namespace labterm            = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";
(:import module namespace console     = "http://exist-db.org/xquery/console";
let $log := console:log(request:get-uri()):)

let $search         := if (request:exists()) then request:get-parameter('search','') else '47700' 
let $statusSearch   := if (request:exists()) then request:get-parameter('status','') else ''
let $user           := if (request:exists()) then request:get-parameter('user','') else ''
let $assignee       := if (request:exists()) then request:get-parameter('assignee','') else ''
let $panel          := if (request:exists()) then request:get-parameter('panel','false') else 'false'
let $language       := if (request:exists()) then request:get-parameter('language','') else 'nl-NL' 
let $property       := if (request:exists()) then request:get-parameter('property','') else '' 
let $timing         := if (request:exists()) then request:get-parameter('timing','') else '' 
let $system         := if (request:exists()) then request:get-parameter('system','') else '' 
let $scale          := if (request:exists()) then request:get-parameter('scale','') else '' 
let $class          := if (request:exists()) then request:get-parameter('class','') else '' 
let $searchloinc    := if (request:exists()) then request:get-parameter('loinc','') else 'false'
let $searchloinc    := if ($searchloinc = 'true') then true() else false()
let $showAll        := if (request:exists()) then request:get-parameter('showAll','') else ''
let $onlyErrors     := if (request:exists()) then request:get-parameter('onlyErrors','') else ''
let $onlyErrors     := if ($onlyErrors = 'true') then true() else false()
let $onlyComments   := if (request:exists()) then request:get-parameter('onlyComments','') else ''
let $onlyComments   := if ($onlyComments = 'true') then true() else false()
let $prerelease     := if (request:exists()) then request:get-parameter('prerelease', '') else 'true'
let $prerelease     := if ($prerelease = 'true') then true() else false()

let $show :=  if ($showAll = 'true') then () else 30
let $concepts := 
    labterm:getLabConcepts($search, $language, $searchloinc, $prerelease, $statusSearch, $user, $assignee, $show, $onlyErrors, $onlyComments, $property, $timing, $system, $scale, $class)
return $concepts
