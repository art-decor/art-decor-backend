xquery version "3.0";

import module namespace labterm            = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

let $id             := if (request:exists()) then request:get-parameter('id', '') else '55235-6'
let $statusSearch   := if (request:exists()) then request:get-parameter('status', '') else ''
let $language       := if (request:exists()) then request:get-parameter('language', 'nl-NL') else 'nl-NL' 
let $searchloinc    := if (request:exists()) then request:get-parameter('loinc', 'false') else 'true'
let $searchloinc    := if ($searchloinc = 'true') then true() else false()
let $prerelease     := if (request:exists()) then request:get-parameter('prerelease', '') else 'true'
let $prerelease     := if ($prerelease = 'true') then true() else false()

return labterm:getLabConceptById($id, $language, $searchloinc, $prerelease, $statusSearch)