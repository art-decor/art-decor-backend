xquery version "3.0";

declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";

import module namespace labterm            = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

let $panelMember    := if (request:exists()) then request:get-parameter('panelMember','') else 'original'
let $loincId        := if (request:exists()) then request:get-parameter('loincId','') else '33254-4'
let $newLoincId        := if (request:exists()) then request:get-parameter('newLoincId','') else '33254-4'
let $panelId        := if (request:exists()) then request:get-parameter('panelId','') else '24336-0'

let $do := 
    try {
        if ($panelMember = 'replace') 
        then labterm:replaceInLocalPanel($panelId, $loincId, $newLoincId) 
        else labterm:changeLocalPanelMembers($panelId, $loincId, $panelMember)
        }
        catch * {<error>{$err:description}</error>}

let $do := update replace $labterm:labConcepts//lab_concept[concept/@loinc_num=$panelId]/@user with labterm:get-current-user()
let $result := 
    if (not(local-name($do) = 'error')) 
    then <result status="OK"/>
    else <result status="NOK">{$do}</result>
return $result