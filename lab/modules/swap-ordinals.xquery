xquery version "3.0";

declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";
import module namespace labterm = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

(: Aanname is dat alle nominals refsets zijn en alle ordinals valueSets. :)

let $conceptId := if (request:exists()) then request:get-parameter('conceptId','') else ''
let $ordinalId := if (request:exists()) then request:get-parameter('ordinalId','') else ''

let $labordinal := doc(concat($labterm:strLabData, '/ordinals.xml'))/ordinals/valueSet[@id = $ordinalId]
let $labconcept := $labterm:labConcepts//lab_concept[concept/@loinc_num=$conceptId]

let $do := 
    if ($ordinalId = '---') then
        update delete $labconcept/outcomes/valueSet
    else if ($labordinal) then
        let $do := 
    		if ($labconcept/outcomes) then () 
			else 
				update insert <outcomes/> into $labconcept
		return	for $outcomes in $labconcept/outcomes
                return (if($outcomes/valueSet) then
	    	        update replace $outcomes/valueSet with <valueSet ref="{$labordinal/@id}" displayName="{$labordinal/@displayName}"/>
	                    else
		                    update insert <valueSet ref="{$labordinal/@id}" effectiveDate="{$labordinal/@effectiveDate}" displayName="{$labordinal/@displayName}"/> into $outcomes)
    else 
		error(QName('http://art-decor.org/ns/error', 'NoSuchOrdinal'), concat('No Ordinal with id: ', $ordinalId))
let $do := update replace $labconcept/@user with labterm:get-current-user()
let $check := labterm:checkConcept($conceptId, 'nl-NL')
let $labConcept     := labterm:getOnlyLabConceptById($conceptId)
return <result status="OK">{$labConcept/lab_concept}</result>