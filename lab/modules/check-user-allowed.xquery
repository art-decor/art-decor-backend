xquery version "3.0";

import module namespace labterm            = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

<allow admin="{if (labterm:isLabAdmin()) then 'true' else 'false'}">{if (labterm:isLabUser()) then 'lab' else ''}</allow>