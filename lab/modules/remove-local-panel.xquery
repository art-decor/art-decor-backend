xquery version "3.0";

declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";

import module namespace labterm            = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

let $panelId        := if (request:exists()) then request:get-parameter('panelId','') else '24336-0'

let $do := 
    try {<result>{labterm:removeLocalPanel($panelId)}</result>}
    catch * {<error>{$err:description}</error>}

let $result := 
    if (not(local-name($do) = 'error')) 
    then <result status="OK"/>
    else <result status="NOK">{$do}</result>
return $result
