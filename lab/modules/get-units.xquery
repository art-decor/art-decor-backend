xquery version "3.0";

declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";
import module namespace labterm            = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

<units>{for $unit in $labterm:labUnits/*[@status='active'] order by lower-case($unit/rm) return $unit}</units>