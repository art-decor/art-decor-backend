xquery version "3.0";

declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "json";
declare option output:media-type "application/json";
declare option output:json-ignore-whitespace-text-nodes "yes";
import module namespace labterm            = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

declare function local:jsonize($els as node()*) as node()* {
    for $el in $els
    return 
    if (local-name($el) = 'concept')
    then element {'concept'} {$el/@*,
        for $child in $el/*
        return 
            if (local-name($child) = 'concept') 
            then local:jsonize($child)
            else element {if (local-name($child) = 'elem') then lower-case($child/@name) else local-name($child)} {$child/string()}
            }
    else element {local-name($el)} {$el/@*, local:jsonize($el/*)}
    };

let $id             := if (request:exists()) then request:get-parameter('id', '') else '48333-9'
let $statusSearch   := if (request:exists()) then request:get-parameter('status', '') else ''
let $language       := if (request:exists()) then request:get-parameter('language', 'nl-NL') else 'nl-NL' 
let $searchloinc    := if (request:exists()) then request:get-parameter('loinc', 'false') else 'true'
let $searchloinc    := if ($searchloinc = 'true') then true() else false()
let $prerelease     := if (request:exists()) then request:get-parameter('prerelease', '') else 'true'
let $prerelease     := if ($prerelease = 'true') then true() else false()

return local:jsonize(labterm:getLabConceptById($id, $language, $searchloinc, $prerelease, $statusSearch))
