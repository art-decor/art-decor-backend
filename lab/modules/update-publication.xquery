xquery version "3.0";

declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";

import module namespace labterm            = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

declare function local:setPermission($files as xs:string*) {
    for $file in $files
    let $loc := xs:anyURI(concat($labterm:strLabPublications, '/', $file))
    return 
        (
        sm:chmod($loc,'rwxr-xr--'),
        sm:chown($loc,'admin'),
        sm:chgrp($loc,'lab-admin')
        )
};

let $data := if (request:exists()) then request:get-data()/publication/desc/string() else 'Test permissions'
let $currentDate         := format-dateTime(current-dateTime(),"[Y0001][M01][D01]-[H01][m01][s01][f001]")
let $pub := 
    <publication effectiveDate="{$currentDate}" user="{labterm:get-current-user()}" type="full">
        <!-- This material contains content from LOINC 
(http://loinc.org). LOINC is copyright © 1995-2020, Regenstrief
Institute, Inc. and the Logical Observation Identifiers Names
and Codes (LOINC) Committee and is available at no cost under
the license at http://loinc.org/license. LOINC® is a registered
United States trademark of Regenstrief Institute, Inc. -->
        <desc>{$data}</desc>
        <lab_concepts/>
    </publication>

let $publicationFull := concat('labconcepts-full-',$currentDate,'.xml')
let $publicationSimple := concat('labconcepts-',$currentDate,'.xml')
let $publicationHtml     := concat('labconcepts-',$currentDate,'.html')
let $publicationFilePath := concat($labterm:strLabPublications, '/', $publicationFull)
let $file := xmldb:store($labterm:strLabPublications, $publicationFull, $pub)
let $do := labterm:updatePublication($publicationFilePath, $currentDate)
let $pub := doc($publicationFilePath)
let $xslt := doc('../resources/simplify-publication.xsl')
let $simple := transform:transform($pub, $xslt, ())
let $file := xmldb:store($labterm:strLabPublications, $publicationSimple, $simple)
let $xslt := doc('../resources/publicatie2html.xsl')
let $html := transform:transform($pub, $xslt, ())
let $file := xmldb:store($labterm:strLabPublications, $publicationHtml, $html)
let $do := local:setPermission(($publicationFull, $publicationSimple, $publicationHtml))
return $file
