xquery version "3.0";

import module namespace labterm            = "http://art-decor.org/ns/labterm" at "../api/api-labterm.xqm";

let $publicationsDir := '/db/apps/lab-data/publications/'
let $publicationCol := collection($publicationsDir)
let $diffs :=  
    if (labterm:get-current-user() = 'guest') 
 then $publicationCol/diff[@publication='true']
    else $publicationCol/diff
return 
    <diffs> {
        for $diff in $diffs
        order by $diff/publication2/@effectiveDate descending
        return  <diff>
                    {$diff/@*, $diff/publication1, $diff/publication2}
                </diff>
    }
    </diffs>
