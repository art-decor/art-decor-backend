xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get      = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace art      = "http://art-decor.org/ns/art" at "../../art/modules/art-decor.xqm";
import module namespace aduser   = "http://art-decor.org/ns/art-decor-users" at "../../art/api/api-user-settings.xqm";
import module namespace adserver = "http://art-decor.org/ns/art-decor-server" at "../../art/api/api-server-settings.xqm";

(:
    Email through Sendmail from eXist about recently changed issues per user per trigger
:)

declare namespace request       = "http://exist-db.org/xquery/request";
declare namespace response      = "http://exist-db.org/xquery/response";
declare namespace mail          = "http://exist-db.org/xquery/mail";
declare namespace datetime      = "http://exist-db.org/xquery/datetime";
declare namespace xmldb         = "http://exist-db.org/xquery/xmldb";

declare variable $MISSING      := "*MISSING*";

let $deeplinkprefix             := if (request:exists()) then request:get-parameter('deeplinkprefix', adserver:getServerURLArt()) else (adserver:getServerURLArt())
let $mysender                   := if (request:exists()) then request:get-parameter('mysender','ART-DECOR Notifier <reply.not.possible@art-decor.org>') else ('ART-DECOR Notifier <reply.not.possible@art-decor.org>')

(: a secret parameter :)
let $secret                     := if (request:exists()) then request:get-parameter('secret', '') else ('')

(: get login credentials :)
let $theactingnotifierusername  := if (request:exists()) then request:get-parameter('user', '') else ('')
let $theactingnotifierpassword  := if (request:exists()) then request:get-parameter('password', '') else ('')

(: debug ************************* :)
(: debug only, send email when true (normal), or don't send email and don't update last notified dates of users :)
let $sendmail                   := if (request:exists()) then request:get-parameter('sendmail', 'true') else ('false')

(:debug only, send everything to this address:)
let $thedebugaddress            := if (request:exists()) then request:get-parameter('email', ()) else ()
(:debug only, update lastIssueNotify when true (normal), or don't update (debug) :)
let $updatenotify               := if (request:exists()) then request:get-parameter('updatenotify', 'true') else ('false')
(: debug ************************* :)

let $now                        := format-dateTime(current-dateTime(), '[Y0001]-[M01]-[D01] [H01]:[m01]:[s01]')
(:~ use localhost :)
let $smtp                       := ()

(:css in html head goes poof somewhere in mail relay, so use style attribute instead:)
let $style-labelouterbox        := 'background-color: white; float: right; display: inline; color: black; font-weight: bold; font-size: smaller; border: 1px solid grey; margin: 0px; padding: 0px;'
let $style-labelinnerbox        := 'background-color: white; float: left; display: inline;'

(: multi language form resource :)
let $userlang-map               :=
    map:merge(
        for $author in $get:colDecorData//project[@id][not(ancestor::decor/issues/@notifier='off')]/author[@email][@username[not(. = 'guest')]]
        let $username   := $author/@username
        group by $username
        return
            map:entry($username, aduser:getUserLanguage($username))
    )

(:
   PROCESS
     LOOP: hush through every project (that does not have the issue notifier switch set to 'off')
     then
       LOOP: hush through every user (author, contrinutor) of this project with an email address and notifier on
       then 
         SECTION I: process all issues 
            determine per user whether he has already gotten a message about the latest changes
            of the respective issue or he needs to get such a message
         SECTION II: process all publications, check whether there is a new release publication for this project
            determine per user whether he has already gotten a message about the latest changes
            of the respective publications or he needs to get such a message
:)

let $notifyresult               :=
    if ($secret='61fgs756.s9' and (xmldb:login('/db', $theactingnotifierusername, $theactingnotifierpassword))) then
        <notify>
        {
            (:
                go thru every project, that does not have the notifier switch set to 'off'
            :)
            for $pa in $get:colDecorData//project[@id][not(ancestor::decor/issues/@notifier='off')]
            let $issues             := $pa/ancestor::decor/issues
            let $projectprefix      := $pa/@prefix
            let $projectlanguage    := $pa/@defaultLanguage
            let $projectname        := $pa/name[@language=$projectlanguage]/text()
            (: 
                do the notifications per project
            :)
            return
            <project projectid="{$pa/@id}" projectname="{$projectname}" projectprefix="{$projectprefix}" projectlanguage="{$projectlanguage}" issuecount="{count($issues/issue)}" notifier="on">
            {
                (: 
                        go thru every user of this project with an email address and notifier on 
                   :)
                for $pu in $pa/author[@email][@username][@notifier='on'][not(@username='guest')]
                (: store user id :)
                let $userid             := $pu/@id
                (: store user name :)
                let $user               := $pu/@username
                (: store user preferred language :)
                let $userlang           := map:get($userlang-map, $user)
                (: get correct language strings if possible or get first set if not :)
                let $artXformResources  := art:getFormResources('art', $userlang)/resources
                let $artXformResources  := 
                    if ($artXformResources[@xml:lang = $userlang]) then 
                        $artXformResources[@xml:lang = $userlang][1]
                    else (
                        art:getFormResources('art', $projectlanguage)/resources[1]
                    )
                (: store email of this user :)
                let $email              := 
                    if ($thedebugaddress) 
                    then $thedebugaddress 
                    else if (starts-with($pu/@email, 'mailto:')) 
                    then substring-after($pu/@email, 'mailto:') 
                    else string($pu/@email)
                (: when was the last time issues were notified, if never (or error) then assume "very long ago" :)
                let $lastissuenotify    := 
                    if (aduser:getUserLastIssueNotify($user) castable as xs:dateTime)
                    then (aduser:getUserLastIssueNotify($user))
                    else (xs:dateTime('2099-01-01T00:00:00'))
                (: store notifications :)
                let $nfspu              :=
                    <notifies>
                    {
                        (: 
                               SECTION I: process all issues 
                                    determine per user whether he has already gotten a message about the latest changes
                                    of the respective issue or he needs to get such a message
                        :)
                        for $i at $issuecount in $issues/issue
                        let $issueid                    := $i/@id/string()
                        let $userissubscribed           := aduser:userHasIssueSubscription($user, $issueid)
                        (: only the issue # :)
                        let $issuenumber                := tokenize($issueid, '\.')[last()]
                        (: title of the issue :)
                        let $issuetitle                 := $i/@displayName
                        
                        let $maxEffEvent                := max(($i/tracking/xs:dateTime(@effectiveDate), $i/assignment/xs:dateTime(@effectiveDate)))
                        let $lastTouchedObject          := $i/tracking[@effectiveDate = $maxEffEvent] | $i/assignment[@effectiveDate = $maxEffEvent]
                        
                        (: only tracking has statusCode :)
                        let $lastTouchedObjectStatus    := ($lastTouchedObject/@statusCode)[last()]
                        let $lastTouchedAuthorIsUserId  := $lastTouchedObject[last()]/author[@id = $userid]
                        return
                            (:only if user is subscribed and is not the last author in the issue:)
                            if ($userissubscribed and empty($lastTouchedAuthorIsUserId)) then (
                                <issue issuenumber="{$issuenumber}" issuetitle="{$issuetitle}" laststatus="{$lastTouchedObjectStatus}">
                                {
                                    (: return all issue's trackings or assignments touched after $lastissuenotify for this user :)
                                    for $ii in ($i/tracking | $i/assignment)
                                    (: when tracking|assignment was last touched :)
                                    let $thisTouch          := $ii/@effectiveDate
                                    let $thisTouchFormatted := 
                                        if ($ii/@effectiveDate castable as xs:dateTime) 
                                        then format-dateTime(xs:dateTime($ii/@effectiveDate),'[Y0001]-[M01]-[D01] [H01]:[m01]', (), (), ()) 
                                        else $ii/@effectiveDate
                                    (: touched by whom :)
                                    let $modifiedby      :=
                                        if ($ii/author[string-length(.)>0])
                                        then $ii/author[string-length(.)>0][1]/text()
                                        else if ($ii/author[string-length(@id)>0])
                                        then $ii/author[string-length(@id)>0][1]/@id
                                        else 'unknown'
                                    return
                                        if ($lastissuenotify < $thisTouch) then
                                            <notify issueid="{$issueid}" issuenumber="{$issuenumber}" issuetitle="{$issuetitle}"
                                                modifier="{$modifiedby}" what="{$ii/name()}" assignmentto="{$ii/@name}"
                                                touched="{$thisTouchFormatted}">
                                            {
                                                 $ii/@statusCode,
                                                 $ii/@labels,
                                                 $ii
                                            }
                                            </notify>
                                        else ()
                                  }
                                  </issue>
                              ) else ()
                    }
                    {
                        (: 
                               SECTION II: process all publications, check whether there is a new release publication for this project
                                    determine per user whether he has already gotten a message about the latest changes
                                    of the respective publications or he needs to get such a message
                                    
                        :)
                        for $rel in $pa/release
                        let $thisTouch   := 
                            if ($rel/@date castable as xs:dateTime)
                                then ($rel/@date)
                                else (xs:dateTime('1981-01-01T00:00:00'))
                        let $thisTouchFormatted := 
                        if ($rel/@date castable as xs:dateTime) 
                            then format-dateTime(xs:dateTime($rel/@date),'[Y0001]-[M01]-[D01] [H01]:[m01]', (), (), ()) 
                            else $rel/@date
                        return
                            if ($lastissuenotify < $thisTouch) then (
                            <notify release="{$thisTouchFormatted}" by="{$rel/@by}" statusCode="{$rel/@statusCode}" versionLabel="{$rel/@versionLabel}">
                            {
                                $rel/note
                            }
                            </notify>
                        ) else ()
                    }
                    </notifies>
                
                   (: 
                       After SECTION I and II all notofication requests are collected in "notifies"
                    go thru all notifications for this user, prepare message and send email, one per project 
                :)
                let $message1 :=
                    if (count($nfspu//notify[@issueid])=0) then () else (
                        <mail>
                            <from>{$mysender}</from>
                            <to>{$email}</to>
                            <subject>{concat($artXformResources/changeonissues/text(), ' ', $projectname)}</subject>
                            <message>
                                <xhtml>
                                    <html>
                                        <head>
                                            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                            <title>{concat($artXformResources/changeonissues/text(), ' ', $projectname)}</title>
                                            <style>
                                            * {{ font-family: Verdana, Arial, sans-serif; font-size: 9.0pt; }}
                                            h3 {{ font-size: 10.0pt; }}
                                            div.labelouterbox {{ {$style-labelouterbox} }}
                                            div.labelinnerbox {{ {$style-labelinnerbox} }}
                                            </style>
                                        </head>
                                        <body>
                                        {
                                            <p>
                                                <h3>{concat($artXformResources/changeonissues/text(), ' ', $projectname, ' ', $artXformResources/as-of, ' ', $now)}</h3>
                                                <br/>{concat($artXformResources/compiledforuser, ' ', $user, ' (', $email, '). ', $artXformResources/dontreply)}
                                            </p>
                                        }
                                        {
                                            for $issue in $nfspu/issue[notify[@issueid]]
                                            let $iheading := concat($artXformResources/issue, ' #', $issue/@issuenumber, ': ', $issue/@issuetitle)
                                            let $istatus := if (string-length($issue/@laststatus)>0) then concat(' [', $artXformResources/issue-status ,': ', $issue/@laststatus, '].') else '.'
                                            return (
                                                <p>
                                                {
                                                    <hr/>,
                                                    <strong>{$iheading}{$istatus}</strong>,
                                                    <br/>,
                                                    $artXformResources/directlink1/text(), ' ',
                                                    <a href='{$deeplinkprefix}/decor-issues--{$projectprefix}?issueId={$issue/@issuenumber}&amp;serclosed=true&amp;language={$projectlanguage}'>{concat($artXformResources/directlink2/text(), '...')}</a>,
                                                    for $nf at $icnt in $issue/notify
                                                    (: New information has been added by X at T. Current status: "S". Current labels aa bb :)
                                                    (: A new assignment has been added by X at T, now assigned to Y. Current status: "S". Current labels aa bb :)
                                                    (: A new "@what" has been added by X at T. Current status: "Y". current labels aa bb :)
                                                    let $txt := concat(
                                                        if ($nf/@what='tracking') then 
                                                            $artXformResources/anewtracking 
                                                        else if ($nf/@what='assignment') then 
                                                            $artXformResources/anewassignment 
                                                        else 
                                                            concat($artXformResources/anew, ' ''',$nf/@what,''' ', $artXformResources/hasbeenadded)
                                                        , ' ', $nf/@modifier
                                                        , ' ', $artXformResources/at, ' ', $nf/@touched
                                                        , 
                                                        if ($nf/@what='assignment') then 
                                                            concat(', ', $artXformResources/nowassignedto,' ', $nf/@assignmentto) 
                                                        else ''
                                                        , '.'
                                                    )
                                                    let $backgroundcolor    := if ($nf/@statusCode='closed') then '#F4FFF4' else if ($nf/@statusCode=('rejected','deferred','cancelled')) then '#D1DDFF' else '#FFEAEA'
                                                    return
                                                        <p>
                                                            {
                                                                <div style="background-color:{$backgroundcolor}; margin: 10px 0 0 10px; padding: 3px;">
                                                                {
                                                                    $txt
                                                                    ,
                                                                    if ($nf/@statusCode) then 
                                                                        (' ', $artXformResources/status/text(),': ''',<b>{$nf/@statusCode/string()}</b>,'''.')
                                                                    else (),
                                                                    (: <labels>
                                                                     :   <label code="cc" name="name" color="white"/>
                                                                       </labels>:)
                                                                    for $label in tokenize($nf/@labels,'\s')
                                                                    let $selectedLabelColor := $issues/labels/label[@code=$label]/@color
                                                                    let $selectedLabelName  := $issues/labels/label[@code=$label]/@name
                                                                    return
                                                                        <div style="{$style-labelouterbox}" title="{$selectedLabelName}" class="labelouterbox">
                                                                            <div style="background-color:{$selectedLabelColor}; display: inline; padding-left: 10px; margin: -3px;">&#160;</div>
                                                                            <div style="{$style-labelinnerbox}" class="labelinnerbox">
                                                                            {
                                                                                concat('&#160;',$label,'&#160;')
                                                                            }
                                                                            </div>
                                                                        </div>
                                                                }
                                                                </div>
                                                            }
                                                            {
                                                                for $nd in $nf/(tracking|assignment)/desc[.//text()[string-length()>0]]
                                                                return
                                                                    <div style="background-color:#EEEEEE; margin: 0 0 0 10px; padding: 3px;">{$nd}</div>
                                                            }
                                                        </p>
                                                    }
                                                </p>
                                            )
                                        }
                                            <p style="text-align: center; font-size: -1">
                                            {
                                                $artXformResources/you-are-receiving-this-because/text(), 
                                                ' ', 
                                                <a href="{$deeplinkprefix}/user-settings" alt="" title="{$artXformResources/unsubscribe-hint}">{$artXformResources/unsubscribe-here/text()}</a>, 
                                                '. '
                                            }
                                            </p>
                                        </body>
                                    </html>
                                </xhtml>
                            </message>
                        </mail>
                    )
                let $message2 :=
                    if (count($nfspu//notify[@release])=0) then () else (
                        <mail>
                            <from>{$mysender}</from>
                            <to>{$email}</to>
                            <subject>{concat($artXformResources/changeonreleases/text(), ' ', $projectname)}</subject>
                            <message>
                                <xhtml>
                                    <html>
                                        <head>
                                            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                            <title>{concat($artXformResources/changeonreleases/text(), ' ', $projectname)}</title>
                                            <style>
                                            * {{ font-family: Verdana, Arial, sans-serif; font-size: 9.0pt; }}
                                            h3 {{ font-size: 10.0pt; }}
                                            div.labelouterbox {{ {$style-labelouterbox} }}
                                            div.labelinnerbox {{ {$style-labelinnerbox} }}
                                            </style>
                                        </head>
                                        <body>
                                        {
                                            <p>
                                                <h3>{concat($artXformResources/changeonreleases/text(), ' ', $projectname, ' ', $artXformResources/as-of, ' ', $now)}</h3>
                                                <br/>{concat($artXformResources/compiledforuser, ' ', $user, ' (', $email, '). ')}
                                            </p>
                                        }
                                        {
                                            
                                                for $rel in $nfspu//notify[@release]
                                                return (
                                                    <strong>
                                                    {
                                                        concat($artXformResources/release/text(), ' ', $rel/@release/string(), ' - ', 
                                                            $artXformResources/action-by/text(), ': ', $rel/@by/string(), ' - ',
                                                            $artXformResources/status/text(), ': ', $rel/@statusCode/string(), ' - ',
                                                            $artXformResources/versionLabel/text(), ': ', $rel/@versionLabel/string())
                                                    }
                                                    </strong>,
                                                    <br/>,
                                                    $artXformResources/directlink1/text(), ' ',
                                                    <a href='{$deeplinkprefix}/decor-project--{$projectprefix}?language={$projectlanguage}'>{concat($artXformResources/directlink3/text(), '...')}</a>,
                                                    
                                                    let $backgroundcolor    := if ($rel/@statusCode='active') then '#F4FFF4' else if ($rel/@statusCode=('draft','pending')) then '#FFFAF4' else '#EAEAEA'
                                                    return
                                                        <p>
                                                        {
                                                                <div style="background-color:{$backgroundcolor}; margin: 10px 0 0 10px; padding: 3px;">
                                                                {
                                                                    $rel/*
                                                                }
                                                                </div>
                                                        }
                                                        </p>
                                                )
                                        }
                                            <p style="text-align: center; font-size: -1">
                                            {
                                                $artXformResources/you-are-receiving-this-because/text(), 
                                                ' ', 
                                                <a href="{$deeplinkprefix}/user-settings" alt="" title="{$artXformResources/unsubscribe-hint}">{$artXformResources/unsubscribe-here/text()}</a>, 
                                                '. '
                                            }
                                            </p>
                                        </body>
                                    </html>
                                </xhtml>
                            </message>
                        </mail>
                    )
                 
                let $message := $message1 | $message2
                (: send an email :)
                let $sendmailstatus     := 
                    if ($sendmail='true') then (
                        if (empty($message1)) then () else if (mail:send-email($message1, $smtp, "UTF-8")) then <success>issue notification</success> else <failure>issue notification</failure>,
                        if (empty($message2)) then () else if (mail:send-email($message2, $smtp, "UTF-8")) then <success>release notification</success> else <failure>release notification</failure>
                    ) else (
                        <disabled/>
                    )
                
                let $issues2notify      := count($nfspu//notify[@issueid])
                let $releases4notify    := count($nfspu//notify[@release])
                let $ninfo              :=
                    if (($issues2notify + $releases4notify) = 0) then () else
                    <ninfo user="{$user}" lastissuenotify="{$lastissuenotify}" issues2notify="{$issues2notify}" releases4notify="{$releases4notify}"/>       
   
                let $test               := 
                    if (empty($message1) and empty($message2)) then () else (
                        <mail user="{$user}" to="{$email}" lastissuenotify="{$lastissuenotify}" subject="{$message1/subject/text()} {$message2/subject/text()}">
                        {
                            for $nf in $nfspu//notify
                            return (
                                <notify>{$nf/@*}</notify>
                            ),
                            $sendmailstatus
                            (:,
                            if ($sendmail='true') then () else $message
                            :)
                        }
                        </mail>
                    )
                
                return (
                    $test,
                    $ninfo
                )
            }
            </project>
        }
        {
        
            (: 
               SECTION III: process all users in the system, check whether they need to be notified 
               that they got a new account. If notified set their flag to to-date               
            :)
            let $theUserList :=
                for $auser in aduser:getUserList()
                let $display   := aduser:getUserDisplayName($auser)
                let $email     := aduser:getUserEmail($auser)
                let $since     := aduser:getUserCreationDate($auser)
                let $lastLogin := aduser:getUserLastLoginTime($auser)
                let $active    := aduser:isUserActive($auser)
                let $accnotify := aduser:getUserLastAccountNotify($auser)
                return
                    <user name="{$auser}" display="{$display}" active="{$active}" since="{$since}">
                    {
                        if (string-length($email) > 0) then attribute { 'email' } { $email } else (),
                        if (string-length($lastLogin) > 0) then attribute { 'lastActive' } { $lastLogin } else (),
                        if (string-length($accnotify) > 0) then attribute { 'lastAccountNotify' } { $accnotify } else ()
                    }
                    </user>
            return
                for $auser in $theUserList[@active="true"][@email]
                let $name      := string($auser/@name)
                let $password  := 'xxxxxxxxNOT-SETxxxxxxxx'
                let $email     := string($auser/@email)
                let $display   := string($auser/@display)
                let $plist     := ()
                let $projects  :=
                    if (count($plist) > 0)
                        then 'At this point in time, you have been added to the following projects:{$plist}.'
                        else 'At this point in time, you have not been added to any project.'
                let $mailmsg   :=
                    <mail>
                        <from>{$mysender}</from>
                        <to>{$email}</to>
                        <subject>[art-decor] Welcome to ART-DECOR®</subject>
                        <message>
                            <xhtml>
                                <html>
                                    <head>
                                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                        <title>[art-decor] Welcome to ART-DECOR®</title>
                                        <style>
                                            * {{ font-family: Verdana, Arial, sans-serif; font-size: 9.0pt; }}
                                            h3 {{ font-size: 10.0pt; }}
                                            .uat {{ font-family: monospace; }}
                                        </style>
                                    </head>
                                    <body>
                                    {
                                        <p>
                                        <h3>Subject: Your ART-DECOR account</h3>
                                        <h3>Attn: {$display}</h3>
                                        </p>,
                                        <p>
                                        Thank you for contacting us. This is a notification that your account has been established for ART-DECOR®. 
                                        We also provide a few more details.
                                        </p>,
                                        <p>
                                        An account has been created for you at: https://art-decor.org/art-decor<p/>
                                        <table border="0">
                                            <tr>
                                                <td>Username:</td>
                                                <td><span class="uat">{$name}</span></td>
                                            </tr>
                                            <tr>
                                                <td>Password:</td>
                                                <td><span class="uat">{$password}</span></td>
                                            </tr>
                                        </table>
                                        </p>,
                                        <p>
                                        You can browse any projects without logging in. In addition, you now can log in 
                                        with the credentials and create and modify artefacts of ART-DECOR® projects 
                                        where you are recognized as an author or contributor.
                                        </p>,
                                        <p>
                                        {$projects}<br/>Please note, that project authors can also add you to a project as author or contributor.
                                        </p>,
                                        <p>Please contact us via email (support at art-decor.org) if you have questions regarding this email.</p>,
                                        <p>On behalf of the entire team</p>,
                                        <p>
                                        Maarten Ligtvoet, Productmanager ART-DECOR at Nictiz<br/>Dr Kai U. Heitmann, CEO, ART-DECOR Open Tools GmbH
                                        </p>
                                    }
                                    </body>
                                </html>
                            </xhtml>
                        </message>
                    </mail>
                return
                    <user>
                    {
                        $auser/@*,
                        $mailmsg
                    }
                    </user>
        }
        </notify>
    else
        <unauthorized/>
        
return 
    <notifyresult>
    {
        $notifyresult
    }
    {
        (: only update last notifed date of users when actually sent emails :)
        if ($sendmail='true') then
            for $up in $notifyresult//project/mail
            let $upuser := $up/@user
            group by $upuser
            return
                (: set last notified if user exist :)
                let $updateUser         := 
                    if (empty(aduser:getUserInfo($upuser))) 
                    then $MISSING
                    else if ($updatenotify='true') 
                    then aduser:setUserLastIssueNotify($upuser, current-dateTime()) 
                    else ()
                let $newlastissuenotify := <lastissuenotify at="{aduser:getUserLastIssueNotify($upuser)}"/>
                return
                    <userupdateon user="{$upuser}" mailsuccess="{count($notifyresult//project/mail[@to=$up/@to][success])}" mailfailure="{count($notifyresult//project/mail[@to=$up/@to][failure])}">
                    {
                        if ($updateUser=$MISSING) then attribute error { 'user-is-missing-on-this-system' } else ()
                    }
                    {
                        if ($sendmail='true' and $thedebugaddress) then (<debug>email sent to fixed address {$thedebugaddress}</debug>) else ()
                    }
                    {
                        if ($updateUser=$MISSING) then () else if ($updatenotify='false') then (<debug>lastIssueNotify not updated</debug>) else ($newlastissuenotify)
                    }
                    </userupdateon>
         else ()
    }
    </notifyresult>