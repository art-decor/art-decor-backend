xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace art     = "http://art-decor.org/ns/art" at "../../art/modules/art-decor.xqm";

declare namespace request       = "http://exist-db.org/xquery/request";
declare namespace response      = "http://exist-db.org/xquery/response";
declare namespace xmldb         = "http://exist-db.org/xquery/xmldb";
declare namespace util          = "http://exist-db.org/xquery/util";
declare option exist:serialize "indent=yes";
declare option exist:serialize "omit-xml-declaration=no";


(: a secret parameter :)
let $secret := if (request:exists()) then request:get-parameter('secret', '') else ('')

(: get login credentials :)
let $theactingnotifierusername  := if (request:exists()) then request:get-parameter('user', '') else ('')
let $theactingnotifierpassword  := if (request:exists()) then request:get-parameter('password', '') else ('')

(:
    get possible extra action parameter
    action = setpublicationstatus with parameters project (prefix) and date (release date), set to parameter status
:)
let $specialaction              := if (request:exists()) then request:get-parameter('action', '') else ('')
let $projectPrefix              := if (request:exists()) then request:get-parameter('project', '') else ('')
let $date                       := if (request:exists()) then request:get-parameter('date', '') else ('')
let $signature                  := if (request:exists()) then request:get-parameter('signature', '') else ('')
let $status                     := if (request:exists()) then request:get-parameter('status', '') else ('')
let $message                    := if (request:exists()) then request:get-parameter('message', '') else ('')
let $percentage                 := if (request:exists()) then request:get-parameter('percentage', '') else ('')
let $busy                       := if (request:exists()) then request:get-parameter('busy', '') else ('')

let $shortproject               := substring($projectPrefix, 1, string-length($projectPrefix) - 1)
let $coll                       := concat($get:strDecorVersion, '/', $shortproject, '/version-', $signature)
let $project                    := $get:colDecorData//decor[project/@prefix=$projectPrefix]
let $projectversionreleaseitem  := $project/project/(version|release)[@date=$date]

let $timeStamp                  := substring-before(xs:string(current-dateTime()), '.')

return
    if ($secret='61fgs756.s9' and (xmldb:login('/db', $theactingnotifierusername, $theactingnotifierpassword))) then
        if (string-length($signature) != 15 and $specialaction = ('setpublicationstatus', 'setprogressmessage')) then
            <invalid-signature/>
        else if ($specialaction = 'setpublicationstatus') then
            <result collection="{$coll}">
            {
                (: 
                    create an xml file named (projectprefix)-publication-completed.xml 
                    with the process status the following content
                :)
                let $s := <publication projectPrefix="{$projectPrefix}" versionDate="{$projectversionreleaseitem/@date}" statusCode="{$status}"/>
                let $x := xmldb:store($coll, concat($projectPrefix, $signature, '-publication-completed.xml'), $s)
                (:
                    ART-DECOR Release 2 Compatibility!
                    set the status of the release version of the project just processed
                    - to 'draft' while the process status (in $status) is NOT 'completed'
                    - to 'pending' in all other cases
                    as long as status of the release version was not set to any other value in the mean time
                :)
                let $itemstatusCode := if ($status='completed') then 'pending' else 'draft'
                let $statusupdate :=
                    if ((string-length($date)>0) and (string-length($projectPrefix)>0) and (count($projectversionreleaseitem)>0) and ($status='completed')) then
                        if (string-length($projectversionreleaseitem/@statusCode)=0) then
                            update insert attribute statusCode {$itemstatusCode} into $projectversionreleaseitem
                        else
                            () (: do nothing :)
                    else ()
                (: return :)
                return ()
            }
            </result>
        else if ($specialaction = 'setprogressmessage') then
            <result collection="{$coll}">
            {
                let $procprogress :=                    doc(concat($coll, '/', $projectPrefix, $signature, '-publication-completed.xml'))/publication
                let $messageupdate :=
                        if (string-length($message)=0) then
                            (: if empty message then delete attribute :)
                            update delete $procprogress/@message
                        else
                            update insert attribute message { $message } into $procprogress
                (:
                    ART-DECOR Release 3 Feature
                    here the scheduled task item in $get:strDecorScheduledTasks 
                    contains valuable information for the frontend
                :)
                let $schtaskcoll := collection($get:strDecorScheduledTasks)
                let $schtaskhere := $schtaskcoll//publication-request[@for=$projectPrefix][@on=$projectversionreleaseitem/@date]
                let $schtaskupdt := 
                    if (not(empty($schtaskhere))) then (
                        update insert attribute progress { $message } into $schtaskhere,
                        if ($percentage >= '0') then update insert attribute progress-percentage { $percentage } into $schtaskhere else (),
                        if (string-length($busy) > 0) then update insert attribute busy { $busy } into $schtaskhere else ()
                     ) else ()
                (: if percentage is 100, all is done, delete scheduled task request :)
                let $schtaskupdt := 
                    if ($percentage = '100') then (
                        xmldb:remove(util:collection-name($schtaskhere), util:document-name($schtaskhere))
                    ) else ()
                (: return :)
                return $procprogress
            }
            </result>
        else (
            <publication-request-list count="{count($get:colDecorVersion//publication[count(processed)=0][count(request)>0])}">
            {
                (: look thru all publication requests in version directory that carry a request but are not yet processed :)
                for $pubrequest in $get:colDecorVersion//publication[count(processed)=0][count(request)>0]
                (: get parent collection name :)
                let $parent := util:collection-name($pubrequest)
                (: hush thru all child resources and if they are a compilation create a publication reuqest :)
                let $compiledFiles  :=
                    for $xcollection in xmldb:get-child-resources($parent)
                    let $docname            := concat($parent, '/', $xcollection)
                    let $ddoc               := doc($docname)
                    (: compilationDate indicates that this resource is a compilation :)
                    let $compilationDate    := if ($ddoc/decor/@compilationDate) then $ddoc/decor/@compilationDate else ''
                    return (
                        if ($ddoc/decor[not(@language = '*')]) then
                            (: return the resource = DECOR XML (original or compiled file as a base64 encoded blob :)
                            <compiled-file-b64 name="{$xcollection}" prefix="{$ddoc/decor/project/@prefix}" language="{$ddoc/decor/@language}">
                            {
                                if (string-length($compilationDate)>0)
                                then attribute {"compilationDate"} {$compilationDate}
                                else attribute {"original"} {"true"}
                            }
                            {
                                util:base64-encode(fn:serialize($ddoc,
                                    <output:serialization-parameters xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization">
                                        <output:method>xml</output:method>
                                    </output:serialization-parameters>
                                ))
                            }
                            </compiled-file-b64>
                        else ()
                    )
                (: get the logo directory if exitent and include it as a zip :)
                let $theprefix := ($compiledFiles/@prefix)[1]
                let $projectlogos := concat($parent, '/', $theprefix, 'logos')
                let $projectlogoszip :=
                    if (xmldb:collection-available($projectlogos))
                    then <logos-zip>{compression:zip(xs:anyURI($projectlogos), true(), $parent)}</logos-zip> 
                    else <logos-zip/>
                (: # of compiled files for later :)
                let $noofcfiles := count($compiledFiles)
                (: if we found requests, mark the publication reuqest a processed = OK else as NOQ no request :)
                let $processed := <processed on="{$timeStamp}" status="{if ($noofcfiles > 0) then 'OK' else 'NOQ'}"/>
                (: do the real update of the process request file :)
                let $result := update insert $processed into $pubrequest
                return (
                    if ($noofcfiles gt 0) then
                        <publication-request count="{$noofcfiles}">
                        {$pubrequest}
                        {$compiledFiles}
                        {$projectlogoszip}
                        </publication-request>
                    else
                        <publication-request count="0"/>
                )
            }
            </publication-request-list>
        )
    else <no-access/>
