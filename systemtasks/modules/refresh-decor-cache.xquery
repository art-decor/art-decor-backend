xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../../art/api/api-server-settings.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at "../../art/modules/art-decor.xqm";
(:import module namespace getf        = "http://art-decor.org/ns/fhir-settings" at "../../fhir/3.0/api/fhir-settings.xqm";:)

declare namespace http              = "http://expath.org/ns/http-client";
declare namespace f             = "http://hl7.org/fhir";
declare namespace util          = "http://exist-db.org/xquery/util";

declare option exist:output-size-limit "9000000";

declare variable $strDecorServices   := adserver:getServerURLServices();

(:recursive function to get all templates hanging off from the current template. has circular reference protection. Does not return the start templates :)
declare %private function local:getRepositories($bbrs as element(buildingBlockRepository)*, $foundResults as item(), $trustedservermap as item(), $mkdir as xs:string?) as element(cacheme)* {
    let $results        := 
        for $bbr in $bbrs
        let $url    := $bbr/@url
        let $ident  := $bbr/@ident
        group by $url, $ident
        return
            if (map:contains($foundResults, concat($ident, $url))) then () else (
                local:getRepository($url, $ident, $trustedservermap, $mkdir)
            )
    
    (: map:merge is not supported by eXist 2.2 :)
    let $foundResults   := 
        map:merge((
            for $key in map:keys($foundResults) return map:entry($key, ''),
            for $key in distinct-values($bbrs/concat(@ident, @url)) return if (map:contains($foundResults, $key)) then () else map:entry($key, '')
        ))
    
    return
        if ($results) then (
            $results | local:getRepositories(
                $results/buildingBlockRepository[empty(@format) or @format = 'decor'],
                $foundResults,
                $trustedservermap,
                $mkdir
            )
        ) else ()
};

(: get stuff that doesn't live on our own server :)
declare %private function local:getRepository($url as xs:string?, $ident as xs:string?, $trustedservermap as item(), $mkdir as xs:string?) as element()? {
    if ($url = $strDecorServices) then () else (
        let $service-uri    := concat($url,'RetrieveProject?format=xml&amp;mode=cache&amp;prefix=',$ident)
        let $requestHeaders := 
            <http:request method="GET" href="{$service-uri}">
                <http:header name="Content-Type" value="application/xml"/>
                <http:header name="Cache-Control" value="no-cache"/>
                <http:header name="Max-Forwards" value="1"/>
            </http:request>
        let $x := 
            try {
                let $server-response := http:send-request($requestHeaders)
                let $server-check    :=
                    if ($server-response[1]/@status='200') then () else (
                        error(QName('http://art-decor.org/ns/error', 'RetrieveError'), concat('Server returned HTTP status: ',$server-response[1]/@status, ' for URI ''',$service-uri,''' with body ''',string-join($server-response[2], ' '),''''))
                    )
                return $server-response[position() gt 1]/descendant-or-self::decor
            } 
            catch * { 
                <error>Caught error {$err:code}: {$err:description}. Data: {$err:value}</error>
            } 
        let $projectId      := $x//project/@id
        let $rootisdecor    := name(($x)[1]) = 'decor'
        
        let $fileId         := util:uuid()
        let $result         := 
            if ($rootisdecor) then (
                let $previous   := collection($mkdir)//cacheme[@bbrurl = $url][@bbrident = $ident]
                let $delete     := 
                    for $instance in $previous
                    return xmldb:remove($mkdir, util:document-name($instance))
                return ()
            ) else ()
        
        let $xml            := 
            <cacheme bbrurl="{$url}" bbrident="{$ident}" format="decor">
            {
                if ($projectId)   then attribute projectId {$projectId} else (),
                if ($rootisdecor) then () else attribute error {true()},
                if (map:contains($trustedservermap, concat($ident, $url))) then
                    attribute isTrusted {'true'}
                else (),
                if ($rootisdecor) then attribute fileId {$fileId} else (),
                $x
            }
            </cacheme>
        let $result         := if ($rootisdecor) then xmldb:store($mkdir, concat($ident, $fileId, '.xml'), $xml) else ()
            
        return
            <cacheme>{$xml/@*, $xml//project/buildingBlockRepository}</cacheme>
    )
};

let $externalservers            := adserver:getServerExternalRepositories()
let $secret                     := if (request:exists()) then request:get-parameter('secret', '') else ''

(: get login credentials :)
let $theactingnotifierusername := if (request:exists()) then request:get-parameter('user', '') else ''
let $theactingnotifierpassword := if (request:exists()) then request:get-parameter('password', '') else ''

let $statusonly                 := if (request:exists()) then request:get-parameter('statusonly', 'false') else 'false'

let $cacheformat                := if (request:exists()) then request:get-parameter('cacheformat', 'decor') else 'decor'

return
    if ($secret='61fgs756.s9' and (xmldb:login('/db', $theactingnotifierusername, $theactingnotifierpassword)) ) then (
        switch ($cacheformat)
            case "decor"
            return
                <cachedBuildingBlockRepositories statusonly="{$statusonly}">
                {
                    if ($statusonly castable as xs:boolean and xs:boolean($statusonly)) then (
                        if ($get:colDecorCache//cachedBuildingBlockRepositories) then (
                            let $bbrcount       := $get:colDecorCache//cachedBuildingBlockRepositories/@count
                            let $bbrerrors      := $get:colDecorCache//cachedBuildingBlockRepositories/@errors
                            let $timeStamp      := $get:colDecorCache//cachedBuildingBlockRepositories/@time
                            return
                            <result cached="{$bbrcount}" unreachable="{$bbrerrors}" time="{$timeStamp}">
                            {
                                for $bbr in $get:colDecorCache//cachedBuildingBlockRepositories/ok
                                order by $bbr/@url, $bbr/@ident
                                return $bbr
                                ,
                                for $bbr in $get:colDecorCache//cachedBuildingBlockRepositories/unreachable
                                order by $bbr/@url, $bbr/@ident
                                return $bbr
                            }
                            </result>
                        ) else ()
                    ) else (
        
                        (: make directory :)
                        let $mkdir          := xmldb:create-collection($get:strDecorCache,'/bbr')
        
                        (: overall bbr references <buildingBlockRepository url="http://localhost:8877/decor/services/" ident="ad1bbr-"/> :)
                        let $trustedservermap   := 
                            map:merge(
                                for $bbr in $externalservers
                                let $urlident   := concat($bbr/@ident, $bbr/@url)
                                group by $urlident
                                return map:entry($urlident, $bbr[1])
                            )
                        
                        let $bbrsfound      := local:getRepositories($get:colDecorData//project/buildingBlockRepository[empty(@format) or @format = 'decor'], map:merge(()), $trustedservermap, $mkdir)
                        
                        let $foundok        :=
                            for $x in $bbrsfound[@projectId][empty(@error)]
                            let $id         := $x/@projectId
                            group by $id
                            return
                                if ($x[@isTrusted]) then ($x[@isTrusted][1]) else $x[1]
                        
                        let $founderror     :=
                            for $x in $bbrsfound[@error] | $bbrsfound[empty(@projectId)]
                            let $url    := $x/@bbrurl
                            let $ident  := $x/@bbrident
                            group by $url, $ident
                            return
                                $x[1]
                        
                        let $bbrcount       := count($foundok/@projectId)
                        let $bbrerrors      := count($founderror)
                        let $timeStamp      := current-dateTime()
                        
                        let $allbbrs        :=
                            <cachedBuildingBlockRepositories status="OK" count="{$bbrcount}" errors="{$bbrerrors}" time="{$timeStamp}">
                            {
                                (: store only BBRs with unique project ids :)
                                (:$foundok
                                ,:)
                                for $y in $founderror
                                let $url    := $y/@bbrurl
                                let $ident  := $y/@bbrident
                                group by $url, $ident
                                return
                                    <unreachable url="{$url}" ident="{$ident}" format="decor">{($y/@isTrusted)[1], $y[1]}</unreachable>
                                ,
                                for $y in $foundok
                                let $url    := $y/@bbrurl
                                let $ident  := $y/@bbrident
                                group by $url, $ident
                                return
                                    <ok url="{$url}" ident="{$ident}" format="decor">{($y/@isTrusted)[1]}</ok>
                            }
                            </cachedBuildingBlockRepositories>
                        
                        (: renew current cache :)
                        let $result         := xmldb:store($mkdir, 'cache.xml', $allbbrs)
                        
                        (: update stubs with real cache fragments :)
                        (:let $tmp1 :=
                            for $bbb in $allbbrs//cacheme[@fileId]
                            let $thisFileId := $bbb/@fileId
                            let $fn := concat($mkdir, '/', $thisFileId, '.xml')
                            let $fc := doc($fn)
                            let $existingCacheStub := $get:colDecorCache//cacheme[@fileId=$thisFileId]
                            return
                                update insert $fc into $existingCacheStub:)
                        
                        (: delete cache fragments :)
                        (:let $tmp2 :=
                            for $bbb in $allbbrs//cacheme[@fileId]
                            let $thisFileId := $bbb/@fileId
                            let $fn := concat($thisFileId, '.xml')
                            return
                                xmldb:remove($mkdir, $fn):)
                            
                        return 
                            <result cached="{$bbrcount}" unreachable="{count($allbbrs//unreachable)}" time="{$timeStamp}">
                            {
                                for $bbr in $allbbrs/ok
                                order by $bbr/@url, $bbr/@ident
                                return $bbr
                                ,
                                for $bbr in $allbbrs/unreachable
                                order by $bbr/@url, $bbr/@ident
                                return $bbr
                            }
                            </result>
                    )
                }
                </cachedBuildingBlockRepositories>
            case "fhir"
            return
                <cachedFHIRRepositories statusonly="{$statusonly}">
                {
                    if ($statusonly castable as xs:boolean and xs:boolean($statusonly)) then (
                        
                        if ($get:colDecorCache//cachedFHIRRepositories) then (
                            let $bbrcount       := $get:colDecorCache//cachedFHIRRepositories/@count
                            let $bbrerrors      := $get:colDecorCache//cachedFHIRRepositories/@errors
                            let $timeStamp      := $get:colDecorCache//cachedFHIRRepositories/@time
                            return
                            <result cached="{$bbrcount}" unreachable="{$bbrerrors}" time="{$timeStamp}">
                            {
                                for $bbr in $get:colDecorCache//cachedFHIRRepositories/ok
                                order by $bbr/@url, $bbr/@ident
                                return $bbr
                                ,
                                for $bbr in $get:colDecorCache//cachedFHIRRepositories/unreachable
                                order by $bbr/@url, $bbr/@ident
                                return $bbr
                            }
                            </result>
                        ) else ()
                        
                    ) else (
                        
                        (: make directory :)
                        let $mkdir          := xmldb:create-collection($get:strDecorCache,'/fhir')
        
                        (: overall bbr references <buildingBlockRepository url="http://localhost:8877/decor/services/" ident="ad1bbr-"/> :)
                        let $thisserverbbrs := $get:colDecorData//project/buildingBlockRepository[@format = 'fhir']

                        let $requestHeaders         := 
                            <http:request method="GET">
                                <http:header name="Content-Type" value="application/fhir+xml"/>
                                <http:header name="Cache-Control" value="no-cache"/>
                                <http:header name="Max-Forwards" value="1"/>
                            </http:request>
                            
                        let $tmp :=
                            for $repository in $thisserverbbrs
                            return
                                 try {
                                     let $url             := concat($repository/@url, 'StructureDefinition')
                                     let $server-response := http:send-request($requestHeaders, $url)
                                     let $server-check    :=
                                     if ($server-response[1]/@status='200') then (
                                            <ok url="{$repository/@url}" ident="{$repository/@ident}" format="fhir"/>
                                        ) else (
                                            <unreachable url="{$repository/@url}" ident="{$repository/@ident}" format="fhir"/>,
                                            error(QName('http://art-decor.org/ns/error', 'RetrieveError'), concat('Server returned HTTP status: ',$server-response[1]/@status/string()))
                                      )
                                     return (
                                        <cacheme bbrurl="{$repository/@url}" bbrident="{$repository/@ident}" fileId="{util:uuid()}" format="fhir">
                                        {
                                            $server-response[position() gt 1]/descendant-or-self::f:Bundle
                                        }
                                        </cacheme>
                                     )
                                 } 
                                 catch * { 
                                     <error>Caught error {$err:code}: {$err:description}. Data: {$err:value}</error>
                                 }
                            
                        let $bbrcount       := count($tmp/f:Bundle)
                        let $bbrerrors      := count($tmp/error)
                        let $timeStamp      := current-dateTime()
                        
                        let $allbbrs        :=
                            <cachedFHIRRepositories status="OK" count="{$bbrcount}" errors="{$bbrerrors}" time="{$timeStamp}">
                            {
                                $tmp
                            }
                            </cachedFHIRRepositories>
                            
                        (: renew current cache :)
                        let $result         := xmldb:store($mkdir, 'cache.xml', $allbbrs)
                        
                        return 
                            <result cached="{$bbrcount}" unreachable="{count($allbbrs//unreachable)}" time="{$timeStamp}">
                            {
                                for $bbr in $allbbrs/ok
                                order by $bbr/@url, $bbr/@ident
                                return $bbr
                                ,
                                for $bbr in $allbbrs/unreachable
                                order by $bbr/@url, $bbr/@ident
                                return $bbr
                            }
                            </result>
                     )
                }
                </cachedFHIRRepositories>
            default
            return <ERRORUnknownCacheFormat format="$cacheformat"/>
     )
     else
        switch ($cacheformat)
            case "decor"
            return
                <cachedBuildingBlockRepositories>NOTAUTHORIZED</cachedBuildingBlockRepositories>
            case "fhir"
            return
                <cachedFHIRRepositories>NOTAUTHORIZED</cachedFHIRRepositories>
            default
            return <ERRORUnknownCacheFormat format="$cacheformat"/>