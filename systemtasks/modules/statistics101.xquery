xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:
    Module: display some statistics
:)

import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "../../art/modules/art-decor-settings.xqm";
import module namespace art     = "http://art-decor.org/ns/art" at "../../art/modules/art-decor.xqm";


let $decor := $get:colDecorData//rules/template[@id]

return
    <result total="{count($decor)}">
    {
        for $d in $decor/classification
        let $t := $d/@type
        group by $t
        return <class type="{$t}" count="{count($decor/classification[@type=$t])}"/>
        
    }
    </result>
