xquery version "1.0";

import module namespace claml = "http://art-decor.org/ns/terminology/claml" at "../../terminology/claml/api/api-claml.xqm";

(: The following external variables are set by the repo:deploy function :)

(: file path pointing to the exist installation directory :)
declare variable $home external;
(: path to the directory containing the unpacked .xar package :)
declare variable $dir external;
(: the target collection into which the app is deployed :)
declare variable $target external;

let $package    := replace(doc('repo.xml')//target[1],'terminology-data/','')

return (
    claml:createDenormalizedFile($package),
    claml:createDescriptionsFile($package)
)