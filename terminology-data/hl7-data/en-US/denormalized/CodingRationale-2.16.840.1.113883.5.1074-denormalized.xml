<?xml version="1.0" encoding="UTF-8"?>
<ClaML-denormalized>
   <Meta name="statusCode" value="active"/>
   <Meta name="hl7CoreMifPackage" value="DEFN=UV=VO=1360-20160323"/>
   <Meta name="releaseDate" value="2016-03-23"/>
   <Meta name="publisherVersionId" value="128"/>
   <Meta name="hl7MaintainedIndicator" value="true"/>
   <Meta name="completeCodesIndicator" value="true"/>
   <Meta name="hl7ApprovedIndicator" value="true"/>
   <Meta name="custodianOrganisation" value="HL7"/>
   <Meta name="custodianOrganisationLogo" value="HL7NL-logo40.png"/>
   <Meta name="custodianOrganisationUrl" value="http://www.hl7.nl"/>
   <Identifier authority="hl7.org" uid="2.16.840.1.113883.5.1074"/>
   <Title name="CodingRationale" date="2016-03-23" version="1.000">Identifies how to interpret the instance of the code, codeSystem value in a set of translations. Since HL7 (or a government body) may mandate that codes from certain code systems be sent in conformant messages, other synonyms that are sent in the translation set need to be distinguished among the originally captured source, the HL7 specified code, or some future role. When this code is NULL, it indicates that the translation is an undefined type. When valued, this property must contain one of the following values: SRC - Source (or original) code HL7 - HL7 Specified or Mandated SH - both HL7 mandated and the original code (precoordination) There may be additional values added to this value set as we work through the use of codes in messages and determine other Use Cases requiring special interpretation of the translations.</Title>
   <Class code="rootClass">
      <SubClass subCount="0" code="O" kind="concept">
         <Rubric id="d1e99088-2" kind="preferred">
            <Label xml:lang="en-US">originally produced code</Label>
         </Rubric>
      </SubClass>
      <SubClass subCount="0" code="OR" kind="concept">
         <Rubric id="d1e99112-2" kind="preferred">
            <Label xml:lang="en-US">original and required</Label>
         </Rubric>
      </SubClass>
      <SubClass subCount="0" code="P" kind="concept">
         <Rubric id="d1e99139-2" kind="preferred">
            <Label xml:lang="en-US">post-coded</Label>
         </Rubric>
      </SubClass>
      <SubClass subCount="0" code="PR" kind="concept">
         <Rubric id="d1e99163-2" kind="preferred">
            <Label xml:lang="en-US">post-coded and required</Label>
         </Rubric>
      </SubClass>
      <SubClass subCount="0" code="R" kind="concept">
         <Rubric id="d1e99190-2" kind="preferred">
            <Label xml:lang="en-US">required</Label>
         </Rubric>
      </SubClass>
      <Rubric kind="preferred">
         <Label xml:lang="en-US">CodingRationale</Label>
      </Rubric>
      <Rubric kind="description">
         <Label xml:lang="en-US">Identifies how to interpret the instance of the code, codeSystem value in a set of translations. Since HL7 (or a government body) may mandate that codes from certain code systems be sent in conformant messages, other synonyms that are sent in the translation set need to be distinguished among the originally captured source, the HL7 specified code, or some future role. When this code is NULL, it indicates that the translation is an undefined type. When valued, this property must contain one of the following values: SRC - Source (or original) code HL7 - HL7 Specified or Mandated SH - both HL7 mandated and the original code (precoordination) There may be additional values added to this value set as we work through the use of codes in messages and determine other Use Cases requiring special interpretation of the translations.</Label>
      </Rubric>
   </Class>
   <Class code="O" kind="concept">
      <Meta name="internalId" value="22615"/>
      <Rubric id="d1e99072" kind="definition">
         <Label xml:lang="en-US">
            <p>
               <b>Description:</b>Originally produced code.</p>
         </Label>
      </Rubric>
      <Rubric id="d1e99088-1" kind="short">
         <Label xml:lang="en-US">originally produced code</Label>
      </Rubric>
      <Rubric id="d1e99088-2" kind="preferred">
         <Label xml:lang="en-US">originally produced code</Label>
      </Rubric>
   </Class>
   <Class code="OR" kind="concept">
      <Meta name="internalId" value="23533"/>
      <Rubric id="d1e99099" kind="definition">
         <Label xml:lang="en-US">
            <p>Originally produced code, required by the specification describing the use of the coded concept.</p>
         </Label>
      </Rubric>
      <Rubric id="d1e99112-1" kind="short">
         <Label xml:lang="en-US">original and required</Label>
      </Rubric>
      <Rubric id="d1e99112-2" kind="preferred">
         <Label xml:lang="en-US">original and required</Label>
      </Rubric>
   </Class>
   <Class code="P" kind="concept">
      <Meta name="internalId" value="22617"/>
      <Rubric id="d1e99123" kind="definition">
         <Label xml:lang="en-US">
            <p>
               <b>Description:</b>Post-coded from free text source&lt;/description&gt;</p>
         </Label>
      </Rubric>
      <Rubric id="d1e99139-1" kind="short">
         <Label xml:lang="en-US">post-coded</Label>
      </Rubric>
      <Rubric id="d1e99139-2" kind="preferred">
         <Label xml:lang="en-US">post-coded</Label>
      </Rubric>
   </Class>
   <Class code="PR" kind="concept">
      <Meta name="internalId" value="23534"/>
      <Rubric id="d1e99150" kind="definition">
         <Label xml:lang="en-US">
            <p>Post-coded from free text source, required by the specification describing the use of the coded concept.</p>
         </Label>
      </Rubric>
      <Rubric id="d1e99163-1" kind="short">
         <Label xml:lang="en-US">post-coded and required</Label>
      </Rubric>
      <Rubric id="d1e99163-2" kind="preferred">
         <Label xml:lang="en-US">post-coded and required</Label>
      </Rubric>
   </Class>
   <Class code="R" kind="concept">
      <Meta name="internalId" value="22616"/>
      <Rubric id="d1e99174" kind="definition">
         <Label xml:lang="en-US">
            <p>
               <b>Description:</b>Required standard code for HL7.</p>
         </Label>
      </Rubric>
      <Rubric id="d1e99190-1" kind="short">
         <Label xml:lang="en-US">required</Label>
      </Rubric>
      <Rubric id="d1e99190-2" kind="preferred">
         <Label xml:lang="en-US">required</Label>
      </Rubric>
   </Class>
</ClaML-denormalized>
