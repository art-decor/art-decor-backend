xquery version "3.0";

import module namespace ada ="http://art-decor.org/ns/ada-common" at "modules/ada-common.xqm";

declare variable $exist:path external;
declare variable $exist:resource external;
declare variable $exist:controller external;

(: 
Path will be for existing data:
  /projects/[app]/[id]
or for new resource:
  /projects/[app]/new
or for a list:
  /projects/[app] 
:)
let $app    := tokenize($exist:path, '/')[3]
let $id     := if ($exist:resource=$app) then '' else $exist:resource
let $from   := if (request:exists()) then request:get-parameter('from','') else '' 
let $format := if (request:exists()) then request:get-parameter('format','') else 'xml' 
let $format :=  
    if (contains($format, 'json')) then 'json' 
    else if (contains($format, 'xml')) then 'xml' 
    else if (contains($format, 'html')) then 'html' 
    else if (contains(request:get-header('Accept'), 'application/json')) then 'json'
    else if (contains(request:get-header('Accept'), 'application/xml')) then 'xml'
    else if (contains(request:get-header('Accept'), 'text/html')) then 'html'
    else 'xml'
let $logOn  := if ($ada:logOn and not(contains($exist:path, 'logging.xquery')))
    then ada:log(request:get-method(), $app, 
        <request>
            <path>{$exist:path}</path>
            <resource>{$exist:resource}</resource>
            <method>{request:get-method()}</method>
            <accept>{request:get-header('Accept')}</accept>
            {for $param in request:get-parameter-names()
            return <param name="{$param}" value="{request:get-parameter($param, '')}"/>
            }
        </request>
         )
     else ()
return

(: Everything which is not in ada/projects is processed as-is, both for backward compatibility and easier testing and debugging :)
if (not(starts-with($exist:path, '/projects/'))) then
    <ignore xmlns="http://exist.sourceforge.net/NS/exist">
        <cache-control cache="no"/>
    </ignore>
(: Serve resources from ada (not: ada/projects) :)
else if (contains($exist:path, "/resources/")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/resources/{substring-after($exist:path, '/resources/')}">
            <set-header name="Cache-Control" value="max-age=3600, must-revalidate"/>
        </forward>
    </dispatch>
else if (request:get-method()='DELETE') then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/modules/remove-data.xquery">
            <add-parameter name="id" value="{$id}"/>
            <add-parameter name="app" value="{$app}"/>
        </forward>        
    </dispatch>
else if (request:get-method()='POST') then
    if (contains(request:get-header('Content-Type'), 'application/json')) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/modules/save-json.xquery"/>
        </dispatch>
    else if (contains(request:get-header('Content-Type'), 'application/xml')) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/modules/save-data.xquery"/>
        </dispatch>
    else 
        <ignore xmlns="http://exist.sourceforge.net/NS/exist">
            <cache-control cache="no"/>
        </ignore>
else if (request:get-method()='GET') then
    if ($format='json') then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/modules/get-json.xquery" method="GET">
                <add-parameter name="id" value="{$id}"/>
                <add-parameter name="app" value="{$app}"/>
            </forward>
        </dispatch>
    else if ($format='xml') then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/modules/get-data.xquery" method="GET">
                <add-parameter name="id" value="{$id}"/>
                <add-parameter name="app" value="{$app}"/>
            </forward>
        </dispatch>
    else if ($format='html') then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/modules/get-html.xquery" method="GET">
                <add-parameter name="id" value="{$id}"/>
                <add-parameter name="app" value="{$app}"/>
            </forward>
        </dispatch>
    else 
        <ignore xmlns="http://exist.sourceforge.net/NS/exist">
            <cache-control cache="no"/>
        </ignore>
else 
    <ignore xmlns="http://exist.sourceforge.net/NS/exist">
        <cache-control cache="no"/>
    </ignore>
