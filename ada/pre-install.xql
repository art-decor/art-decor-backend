xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace xmldb   = "http://exist-db.org/xquery/xmldb";
import module namespace sm      = "http://exist-db.org/xquery/securitymanager";
import module namespace repo    = "http://exist-db.org/xquery/repo";
declare namespace cfg           = "http://exist-db.org/collection-config/1.0";

(: file path pointing to the exist installation directory :)
declare variable $home external;
(: path to the directory containing the unpacked .xar package :)
declare variable $dir external;
(: the target collection into which the app is deployed :)
declare variable $target external;
(:install path for art (/db, /db/apps), no trailing slash :)
declare variable $root := repo:get-root();

let $strAdaData         := 
    if (xmldb:collection-available(concat($root, 'ada-data')))
    then concat($root, 'ada-data')
    else xmldb:create-collection($root,'ada-data')

let $strAdaDataIdxDir   := 
    if (xmldb:collection-available(concat('/db/system/config',$root,'ada-data'))) 
    then concat('/db/system/config',$root,'ada-data') 
    else xmldb:create-collection(concat('/db/system/config',$root),'ada-data')

let $adaIndex :=
    <collection xmlns="http://exist-db.org/collection-config/1.0">
        <index  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:hl7="urn:hl7-org:v3" xmlns:xs="http://www.w3.org/2001/XMLSchema">
            <fulltext default="none" attributes="false"/>
            <!-- eXist-db 2.2 -->
            <range>
                <create qname="@conceptId" type="xs:string"/>
                <create qname="@id" type="xs:string"/>
                <create qname="@value" type="xs:string"/>
                <create qname="@type" type="xs:string"/>
                <create qname="@transactionRef" type="xs:string"/>
                <create qname="@transactionEffectiveDate" type="xs:string"/>
                <create qname="@app" type="xs:string"/>
            </range>
        </index>
    </collection>

let $index-file := concat($strAdaDataIdxDir, '/collection.xconf')
let $reindex    :=
    if (doc-available($index-file) and deep-equal(doc($index-file)/cfg:collection,$adaIndex)) then () else (
        xmldb:store($strAdaDataIdxDir,'collection.xconf',$adaIndex),
        xmldb:reindex($strAdaData)
    )

return <ok/>