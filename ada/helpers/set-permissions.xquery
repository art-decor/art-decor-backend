xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace xmldb   = "http://exist-db.org/xquery/xmldb";
import module namespace ada     = "http://art-decor.org/ns/ada-common" at "../modules/ada-common.xqm";
import module namespace adpfix  = "http://art-decor.org/ns/ada/permissions" at "../api/api-permissions.xqm";

let $project    := if (request:exists()) then request:get-parameter('project',()) else 'mp-mp9'

let $update     := adpfix:setAdaPermissions()

return 
    if ($project) then (
        ada:setPermissions($project)
    )
    else (
        for $project in xmldb:get-child-collections($ada:strAdaProjects)
        return
        ada:setPermissions($project)
    )