xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:
    Instruction: under EDIT ZONE around line 135, change the variables $diskRoot and $projects to 
    match your situation and run against the exist-db OR place a dba authenticated call to
    https://decor.nictiz.nl/ada/helpers/get-project-from-disk.xquery?diskroot=[path-to-ada-parent-folder]{&project=[ada-folder]}
    
    where param project may repeat if you want to upload multiple projects.
:)

import module namespace ada     = "http://art-decor.org/ns/ada-common" at "../modules/ada-common.xqm";
declare namespace xmldb         = "http://exist-db.org/xquery/xmldb";
declare namespace sm            = "http://exist-db.org/xquery/securitymanager";
declare namespace output        = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "xml";

(:~ Local function that removes the existing project from the database, creates the new project folder structure from local disk and creates the data folder
    DO NOT EDIT THIS FUNCTION UNLESS YOU AIM TO COMMIT IT TO THE ART-DECOR SOURCE
:)
declare %private function local:get-project($diskRoot as xs:string, $project as xs:string) {
    let $pathSeparator      := if (empty(index-of($diskRoot, '/'))) then '/' else '\' 
    let $diskRoot           := if (ends-with($diskRoot, $pathSeparator)) then $diskRoot else concat($diskRoot, $pathSeparator)
    
    let $localAdaRoot       := concat($diskRoot, $project, $pathSeparator) 
    let $serverAdaRoot      := concat($ada:strAdaProjects, $project, $pathSeparator)
    
    let $empty := 
        if (xmldb:collection-available($serverAdaRoot)) then
            for $child in xmldb:get-child-resources($serverAdaRoot)
            return xmldb:remove($serverAdaRoot, $child)
        else ()
    
    let $empty :=
        if (xmldb:collection-available($serverAdaRoot)) then
            for $childcoll in xmldb:get-child-collections($serverAdaRoot)
            return xmldb:remove(concat($serverAdaRoot, $childcoll))
        else ()
    
    let $collections        := 
        <result>
        {
            let $datadir    := xmldb:create-collection($ada:strAdaData, $project)
            let $cc         := xmldb:create-collection($datadir, 'data')
            
            let $projectdir := xmldb:create-collection($ada:strAdaProjects, $project)
            let $cc         := xmldb:create-collection($projectdir, 'definitions')
            (:let $cc         := xmldb:create-collection($projectdir, 'modules'):)
            let $cc         := xmldb:create-collection($projectdir, 'new')
            let $cc         := xmldb:create-collection($projectdir, 'schemas')
            let $cc         := xmldb:create-collection($projectdir, 'views')
            
            return <dir>{$cc}</dir>
        }
        </result>
    
    let $result             :=
        <result>
            <definitions>
            {
                for $f in xmldb:store-files-from-pattern(concat($serverAdaRoot, 'definitions'), concat($localAdaRoot, 'definitions'), '*.xml') 
                return <file>{$f}</file>
            }
            </definitions>
            <schemas>
            {
                for $f in xmldb:store-files-from-pattern(concat($serverAdaRoot, 'schemas'), concat($localAdaRoot, 'schemas'), '*.xsd') 
                return <file>{$f}</file>
            }
            </schemas>
            <!--js>
            {
                for $f in xmldb:store-files-from-pattern(concat($serverAdaRoot, 'js'), concat($localAdaRoot, 'js'), ('*.js')) 
                return <file>{$f}</file>
            }
            </js-->
            <!--modules>
            {
                for $f in xmldb:store-files-from-pattern(concat($serverAdaRoot, 'modules'), concat($localAdaRoot, 'modules'), ('*.xquery')) 
                return <file>{$f}</file>
            }
            </modules-->
            <views>
            {
                for $f in xmldb:store-files-from-pattern(concat($serverAdaRoot, 'views'), concat($localAdaRoot, 'views'), '*.html') 
                return <file>{$f}</file>
                ,
                for $f in xmldb:store-files-from-pattern(concat($serverAdaRoot, 'views'), concat($localAdaRoot, 'views'), '*.xhtml') 
                return <file>{$f}</file>
            }
            </views>
            <new>
            {
                for $f in xmldb:store-files-from-pattern(concat($serverAdaRoot, 'new'), concat($localAdaRoot, 'new'), '*.xml') 
                return <file>{$f}</file>
            }
            </new>
            <root>
            {
                for $f in xmldb:store-files-from-pattern($serverAdaRoot, $localAdaRoot, '*.xml') 
                return <file>{$f}</file>
                ,
                for $f in xmldb:store-files-from-pattern($serverAdaRoot, $localAdaRoot, '*.xql') 
                return <file>{$f}</file>
            }
            </root>
        </result>
    
    let $permissions        := ada:setPermissions($project)
    return $result
};

(: ======================================================================================= :)
(: ==== EDIT ZONE      Change $diskRoot and $projects to match what you aim to upload ==== :)

(:                    example Windows path, final \ is not required                        :)
(:let $diskRoot       := 'C:\Git\art_decor\projects\'                                      :)
(:                    example macOS path, final / is not required                          :)
(:let $diskRoot       := '/Users/[username]/Development/GitHub/Nictiz/art_decor/projects/' :)

(:                    example for a single project                                         :)
(:let $projects       := ('project1')                                                      :)
(:                    example for multiple projects                                        :)
(:let $projects       := ('project1', 'project2')                                          :)

(: ====                                                                               ==== :)
(: ======================================================================================= :)

(: /path/to/the/folder/containing/the/projects :)
let $diskRoot       := if (request:exists()) then request:get-parameter('diskroot', '') else ''

(: list of projects to upload :)
let $projects       := if (request:exists()) then request:get-parameter('project', '') else  ('')

(: ======================================================================================= :)
(: ==== END EDIT ZONE                                                                 ==== :)
(: ======================================================================================= :)

let $check          :=
    if ($diskRoot = '') then
        error(xs:QName('ada:IncorrectAdaParentFolder'), 'Empty parent folder name. Please use parameter diskroot (once) to give this a value.') 
    else
    if (count($diskRoot) gt 1) then
        error(xs:QName('ada:IncorrectAdaParentFolder'), 'Multiple parent folder names. Please use parameter diskroot only once.')
    else
    if (empty($projects[string-length(.) gt 0])) then
        error(xs:QName('ada:IncorrectAdaFolder'), 'No projects to work with. Please use parameter project at least once.') 
    else
    if ($projects[contains(., '/') or contains(., '\')]) then
        error(xs:QName('ada:IncorrectAdaFolder'), 'Project name cannot contain ''/'' or ''\'': ' || string-join($projects[contains(., '/') or contains(., '\')], ' '))
    else ()

let $result         := ()
(: de-duplicate projects just in case :)
let $result         := 
    for $project in $projects[string-length(.) gt 0]
    group by $project
    return (
        $result, local:get-project($diskRoot, $project[1])
    )

return $result