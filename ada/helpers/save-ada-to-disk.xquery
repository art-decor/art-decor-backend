xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";
declare namespace file       = "http://exist-db.org/xquery/file";
declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "xml";

let $adaDir := 'C:\Dropbox\development\ART-DECOR\ada\'
(:TODO: $xform[2] since xform contains processing instruction:)
(:return xdb:store($xformPath, $xformName, $xform[2]):)
let $result := file:sync('/db/apps/ada/helpers/', concat($adaDir, 'helpers'), ())
let $result := file:sync('/db/apps/ada/modules/', concat($adaDir, 'modules'), ())
(:let $result := file:sync('/db/apps/ada-data/projects/', concat($adaDir, 'projects'), ()):)
return $result