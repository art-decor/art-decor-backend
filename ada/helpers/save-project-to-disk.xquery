xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";
declare namespace file       = "http://exist-db.org/xquery/file";
declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "xml";

declare variable $adaprojectroot := concat(repo:get-root(),'/ada/projects');

let $project    := 
    if (request:exists()) 
    then (request:get-parameter('project',()))
    else ('demoapp')
let $adaDir     := 
    if (request:exists()) 
    then (request:get-parameter('localdir',())) 
    else (concat('/mnt/hgfs/source/ART DECOR trunk/ada-data/projects/', $project, '/')) 
let $adaRoot    := concat($adaprojectroot,'/', $project, '/')

let $result := file:sync(concat($adaRoot, 'data'), concat($adaDir, 'data'), ())
let $result := file:sync(concat($adaRoot, 'hl7'), concat($adaDir, 'hl7'), ())
let $result := file:sync(concat($adaRoot, 'xslt'), concat($adaDir, 'xslt'), ())
return $result