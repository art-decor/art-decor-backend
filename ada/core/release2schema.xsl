<?xml version="1.0" encoding="UTF-8"?>
<!-- 
Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

This program is free software; you can redistribute it and/or modify it under the terms 
of the GNU Affero General Public Licenseas published by the Free Software Foundation; 
either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the GNU Affero General Public Licensefor more details.

See http://www.gnu.org/licenses/
-->
<xsl:stylesheet xmlns:ada="http://www.art-decor.org/ada"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" exclude-result-prefixes="#all" version="2.0">
    <xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>
    <xsl:include href="ada-basics.xsl"/>

    <xsl:template match="/">
        <xsl:apply-templates select="ada//view[@type = 'crud']/dataset"/>
        <xsl:call-template name="meta"/>
    </xsl:template>
    
    <xsl:template name="meta">
        <!-- ADA meta part to be included in both -->
        <xsl:result-document method="xml" href="{concat($projectDiskRoot, 'schemas/ada_meta.xsd')}">
            <xs:schema elementFormDefault="qualified">
                <xs:complexType name="meta_type">
                    <xs:sequence>
                        <xs:any namespace="##any" processContents="skip" minOccurs="0" maxOccurs="unbounded"/>
                    </xs:sequence>
                    <xs:attribute name="status" type="xs:string"/>
                    <xs:attribute name="created-by" type="xs:string"/>
                    <xs:attribute name="last-update-by" type="xs:string"/>
                    <xs:attribute name="creation-date" type="xs:dateTime"/>
                    <xs:attribute name="last-update-date" type="xs:dateTime"/>
                </xs:complexType>
            </xs:schema>
        </xsl:result-document>
    </xsl:template>

    <xsl:template name="dataset" match="dataset[@transactionId]">
        <xsl:variable name="href" select="concat($projectDiskRoot, 'schemas/', @shortName, '.xsd')"/>
        <xsl:variable name="schema">
            <xs:schema>
                <xs:annotation>
                    <xs:documentation>Schema for transaction: <xsl:value-of select="@transactionId"/></xs:documentation>
                </xs:annotation>
                <!--<xs:simpleType name="uid">
                    <xs:annotation>
                        <xs:documentation>
                            A unique identifier string is a character string which
                            identifies an object in a globally unique and timeless
                            manner. The allowable formats and values and procedures
                            of this data type are strictly controlled by HL7. At this
                            time, user-assigned identifiers may be certain character
                            representations of ISO Object Identifiers () and DCE
                            Universally Unique Identifiers ().
                            HL7 also reserves the right to assign other forms of UIDs (,
                            such as mnemonic identifiers for code systems.
                        </xs:documentation>
                    </xs:annotation>
                    <xs:union memberTypes="oid uuid ruid"/>
                </xs:simpleType>
                <xs:simpleType name="oid">
                    <xs:annotation>
                        <xs:documentation/>
                    </xs:annotation>
                    <xs:restriction base="xs:string">
                        <xs:pattern value="[0-2](\.(0|[1-9][0-9]*))*"/>
                    </xs:restriction>
                </xs:simpleType>
                <xs:simpleType name="uuid">
                    <xs:annotation>
                        <xs:documentation/>
                    </xs:annotation>
                    <xs:restriction base="xs:string">
                        <xs:pattern value="[0-9a-zA-Z]{8}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{12}"/>
                    </xs:restriction>
                </xs:simpleType>
                <xs:simpleType name="ruid">
                    <xs:annotation>
                        <xs:documentation/>
                    </xs:annotation>
                    <xs:restriction base="xs:string">
                        <xs:pattern value="[A-Za-z][A-Za-z0-9\-]*"/>
                    </xs:restriction>
                </xs:simpleType>
                <xs:simpleType name="nullFlavor_unknown">
                    <xs:restriction base="xs:string">
                        <xs:enumeration value="UNK">
                            <xs:annotation><xs:documentation>nullFlavor UNK = Unknown</xs:documentation></xs:annotation>
                        </xs:enumeration>
                    </xs:restriction>
                </xs:simpleType>
                <xs:simpleType name="nullFlavor_other">
                    <xs:restriction base="xs:string">
                        <xs:enumeration value="OTH">
                            <xs:annotation><xs:documentation>nullFlavor OTH = Other</xs:documentation></xs:annotation>
                        </xs:enumeration>
                    </xs:restriction>
                </xs:simpleType>-->
                <xs:simpleType name="empty_string">
                    <xs:annotation>
                        <xs:documentation>Type for empty value-strings on non-mandatory concepts</xs:documentation>
                    </xs:annotation>
                    <xs:restriction base="xs:string">
                        <xs:maxLength value="0"/>
                    </xs:restriction>
                </xs:simpleType>
                <xs:element name="{@shortName}" type="{@shortName}_type"/>
                <xs:complexType name="{@shortName}_type">
                    <xs:sequence>
                        <xsl:call-template name="doConceptGroupContent"/>
                    </xs:sequence>
                    <xs:attribute name="id" type="xs:string"/>
                    <xs:attribute name="app" type="xs:string"/>
                    <!-- shortName and formName are newer attributes and may not exist on 
                        data created before their introduction hence they cannot be required -->
                    <xs:attribute name="shortName" type="xs:string" use="optional"/>
                    <xs:attribute name="formName" type="xs:string" use="optional"/>
                    <xs:attribute name="transactionRef" type="xs:string"/>
                    <xs:attribute name="transactionEffectiveDate" type="xs:dateTime"/>
                    <xs:attribute name="versionDate" type="xs:string"/>
                    <xs:attribute name="language" type="xs:string"/>
                    <xs:attribute name="prefix" type="xs:string"/>
                    <xs:attribute name="adaVersion" type="xs:decimal"/>
                    <xs:attribute name="title" type="xs:string"/>
                    <xs:attribute name="desc" type="xs:string"/>
                    <xs:attribute name="rights" type="xs:string"/>
                    <xs:attribute name="owner" type="xs:string"/>
                </xs:complexType>
                <!-- This for generating types for all concepts-->
                <xsl:apply-templates select=".//concept[@type = 'group']" mode="doTypes"/>
                <xsl:apply-templates select=".//concept[@type = 'item']" mode="doTypes"/>
                <xs:complexType name="{$extensionElementName}_type">
                    <xs:sequence>
                        <xs:any processContents="skip" minOccurs="0" maxOccurs="unbounded"/>
                    </xs:sequence>
                    <xs:anyAttribute processContents="skip"/>
                </xs:complexType>
                <xs:simpleType name="NullFlavorNoInformation">
                    <xs:restriction base="xs:string">
                        <!-- not a relevant value in instances, except at runtime in XForms. Empty value is presented 
                            there to be able to offer UI, but empty attributes are not persisted -->
                        <xs:enumeration value=""/>
                        <xs:enumeration value="NI"/>
                        <xs:enumeration value="UNK"/>
                        <xs:enumeration value="OTH"/>
                        <xs:enumeration value="MSK"/>
                        <xs:enumeration value="UNC"/>
                        <xs:enumeration value="NA"/>
                        <xs:enumeration value="NAV"/>
                        <xs:enumeration value="ASKU"/>
                        <xs:enumeration value="NASK"/>
                        <xs:enumeration value="INV"/>
                        <xs:enumeration value="DER"/>
                        <xs:enumeration value="NINF"/>
                        <xs:enumeration value="PINF"/>
                        <xs:enumeration value="QS"/>
                        <xs:enumeration value="TRC"/>
                    </xs:restriction>
                </xs:simpleType>
                <xs:simpleType name="ComplexDatatypes">
                    <xs:restriction base="xs:string">
                        <xsl:for-each select="distinct-values(($theDatatypes/DataSetValueType/enumeration/@value, 'reference'))">
                            <xs:enumeration value="{.}"/>
                        </xsl:for-each>
                    </xs:restriction>
                </xs:simpleType>
                <xs:simpleType name="VariableDate">
                    <xs:union memberTypes="xs:date RelativeDate"/>
                </xs:simpleType>
                <xs:simpleType name="VariableDateTime">
                    <xs:union memberTypes="xs:dateTime xs:date RelativeDateTime RelativeDate"/>
                </xs:simpleType>
                <xs:simpleType name="VariableVagueDate">
                    <xs:union memberTypes="xs:date RelativeDate DateYearOptionalMonthNoDays"/>
                </xs:simpleType>
                <xs:simpleType name="VariableVagueDateTime">
                    <xs:union memberTypes="xs:dateTime xs:date RelativeDateTime DateYearOptionalMonthNoDays DateTimeOptionalMinutesSeconds"/>
                </xs:simpleType>
                <xs:simpleType name="VagueDate">
                    <xs:union memberTypes="xs:date DateYearOptionalMonthNoDays"/>
                </xs:simpleType>
                <xs:simpleType name="VagueDateTime">
                    <xs:union memberTypes="xs:dateTime xs:date DateYearOptionalMonthNoDays DateTimeOptionalMinutesSeconds"/>
                </xs:simpleType>
                <xs:simpleType name="RelativeDate">
                    <!-- T-50D = T minus 50 days -->
                    <xs:restriction base="xs:string">
                        <xs:pattern value="(T|DOB)([+\-](\d+(\.\d+)?[YMD]){{1,3}})?"/>
                    </xs:restriction>
                </xs:simpleType>
                <xs:simpleType name="RelativeDateTime">
                    <!-- T-50D = T minus 50 days, or T-10M{12:34:56} = T minus 10 months at 12:34:56, or T{12:34:56} = T at 12:34:56 -->
                    <xs:restriction base="xs:string">
                        <xs:pattern value="(T|DOB)([+\-](\d+(\.\d+)?[YMD]){{1,3}})?(\{{([01]\d|2[0-3]):(0\d|[1-5]\d)(:(0\d|[1-5]\d))?\}})?"/>
                    </xs:restriction>
                </xs:simpleType>
                <xs:simpleType name="DateYear">
                    <!-- YYYY matches ART-DECOR Y! -->
                    <xs:restriction base="xs:string">
                        <xs:pattern value="\d{{4}}"/>
                    </xs:restriction>
                </xs:simpleType>
                <xs:simpleType name="DateYearMonth">
                    <!-- YYYY matches ART-DECOR YM! -->
                    <xs:restriction base="xs:string">
                        <xs:pattern value="\d{{4}}-(0[1-9]|1[012])"/>
                    </xs:restriction>
                </xs:simpleType>
                <xs:simpleType name="DateYearOptionalMonthNoDays">
                    <!-- YYYY(-MM)? -->
                    <xs:restriction base="xs:string">
                        <xs:pattern value="\d{{4}}(-(0[1-9]|1[012]))?"/>
                    </xs:restriction>
                </xs:simpleType>
                <xs:simpleType name="DateTimeOptionalMinutesSeconds">
                    <!-- YYYY-MM-DDThh(:mm)? -->
                    <xs:restriction base="xs:string">
                        <xs:pattern value="\d{{4}}-(0[1-9]|1[012])-(0[1-9]|[12]\d|3[01])T([01]\d|2[0-3])(:(0\d|[1-5]\d)(:(0\d|[1-5]\d))?)?"/>
                    </xs:restriction>
                </xs:simpleType>
                <xs:simpleType name="VariableVagueTime">
                    <xs:union memberTypes="xs:time RelativeTime TimeHourOptionalMinutesSeconds"/>
                </xs:simpleType>
                <xs:simpleType name="VariableTime">
                    <xs:union memberTypes="xs:time RelativeTime"/>
                </xs:simpleType>
                <xs:simpleType name="VagueTime">
                    <xs:union memberTypes="xs:time TimeHourOptionalMinutesSeconds"/>
                </xs:simpleType>
                <xs:simpleType name="RelativeTime">
                    <!-- T-50H = T minus 50 hour, or T-10M{:00} = use the current hour and minutes T minus 10 minutes and assume 00 seconds, or T{:00:00} = use the hour of T and assume 00:00 minutes/seconds -->
                    <xs:restriction base="xs:string">
                        <xs:pattern value="(T|DOB)(([+\-]\d+(\.\d+)?[HMS])?(\{{:(0\d|[1-5]\d)(:(0\d|[1-5]\d))?\}})?)?"/>
                    </xs:restriction>
                </xs:simpleType>
                <xs:simpleType name="TimeHourOptionalMinutesSeconds">
                    <!-- hh(:mm(:ss)?)? -->
                    <xs:restriction base="xs:string">
                        <xs:pattern value="([01]\d|2[0-3])(:(0\d|[1-5]\d)(:(0\d|[1-5]\d))?)?"/>
                    </xs:restriction>
                </xs:simpleType>
                <xs:simpleType name="TimeHour">
                    <!-- HH matches ART-DECOR H! -->
                    <xs:restriction base="xs:string">
                        <xs:pattern value="[01]\d|2[0-3]"/>
                    </xs:restriction>
                </xs:simpleType>
                <xs:simpleType name="TimeHourMinute">
                    <!-- HH:MM matches ART-DECOR HM! -->
                    <xs:restriction base="xs:string">
                        <xs:pattern value="([01]\d|2[0-3]):(0\d|[1-5]\d)"/>
                    </xs:restriction>
                </xs:simpleType>
                <xs:simpleType name="BooleanOrEmpty">
                    <xs:restriction base="xs:string">
                        <xs:enumeration value=""/>
                        <xs:enumeration value="true"/>
                        <xs:enumeration value="true"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:schema>
        </xsl:variable>
        <xsl:result-document method="xml" href="{$href}">
            <xsl:copy-of select="$schema"/>
        </xsl:result-document>
        <!-- Draft schema, will be copy of original schema, but 
        <attribute name="value" use="required"/>
        is replaced by
        <attribute name="value"/>
        which makes elements without @value valid, even for 1..1 elements.
        Allows 'draft' xml which still is incomplete to be valid against {schema-name}-draft.xsd
        -->
        <xsl:variable name="href-draft" select="concat($projectDiskRoot, 'schemas/', @shortName, '_draft.xsd')"/>
        <xsl:result-document method="xml" href="{$href-draft}">
            <xsl:apply-templates mode="doDraftSchema" select="$schema/*"/>
        </xsl:result-document>
        <!-- Wrapper schema which validates ADA XML Storage format -->
        <xsl:result-document method="xml" href="{concat($projectDiskRoot, 'schemas/ada_', @shortName, '.xsd')}">
            <xs:schema elementFormDefault="qualified">
                <xs:include schemaLocation="{@shortName}.xsd"/>
                <xs:include schemaLocation="ada_meta.xsd"/>
                <xs:element name="adaxml">
                    <xs:complexType>
                        <xs:sequence>
                            <xs:element name="meta" type="meta_type"/>
                            <xs:element name="data">
                                <xs:complexType>
                                    <xs:sequence>
                                        <xs:element name="{@shortName}" type="{@shortName}_type"/>
                                    </xs:sequence>
                                </xs:complexType>
                            </xs:element>
                        </xs:sequence>
                    </xs:complexType>
                </xs:element>
            </xs:schema>
        </xsl:result-document>
        <!-- Wrapper schema which validates ADA XML Storage format for drafts -->
        <xsl:result-document method="xml" href="{concat($projectDiskRoot, 'schemas/ada_', @shortName, '_draft.xsd')}">
            <xs:schema elementFormDefault="qualified">
                <xs:include schemaLocation="{@shortName}_draft.xsd"/>
                <xs:include schemaLocation="ada_meta.xsd"/>
                <xs:element name="adaxml">
                    <xs:complexType>
                        <xs:sequence>
                            <xs:element name="meta" type="meta_type"/>
                            <xs:element name="data">
                                <xs:complexType>
                                    <xs:sequence>
                                        <xs:element name="{@shortName}" type="{@shortName}_type"/>
                                    </xs:sequence>
                                </xs:complexType>
                            </xs:element>
                        </xs:sequence>
                    </xs:complexType>
                </xs:element>
            </xs:schema>
        </xsl:result-document>
    </xsl:template>

    <xsl:template match="xs:attribute" mode="doDraftSchema">
        <xsl:copy>
            <xsl:apply-templates select="(@* except @use) | node()" mode="doDraftSchema"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="@* | node()" mode="doDraftSchema">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()" mode="doDraftSchema"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="concept[@type = 'group']" mode="doTypes">
        <xs:complexType name="{implementation/@shortName}_type_{translate((@id, @ref)[1], '.', '_')}">
            <xs:annotation>
                <xs:documentation>Type for concept group: <xsl:value-of select="name"/></xs:documentation>
            </xs:annotation>
            <xs:sequence>
                <xsl:call-template name="doConceptGroupContent"/>
            </xs:sequence>
            <xs:attribute name="conceptId" fixed="{(@id, @ref)[1]}"/>
            <!-- Add @id if @adaId='true' -->
            <xsl:if test="@adaId = 'true'">
                <xs:attribute name="id" type="xs:ID"/>
            </xsl:if>
            <xs:attribute name="comment" type="xs:string" use="optional"/>
        </xs:complexType>
    </xsl:template>

    <xsl:template match="concept[@type = 'group']">
        <xs:element name="{./implementation/@shortName}" type="{./implementation/@shortName}_type_{translate((@id, @ref)[1], '.', '_')}">
            <xsl:apply-templates select="@minimumMultiplicity | @maximumMultiplicity"/>
        </xs:element>
    </xsl:template>

    <xsl:template name="doConceptGroupContent">
        <xsl:for-each select="concept">
            <!-- For all groups. Not really needed for 1..1 groups, but if it was 0..1 in older release, we still need start hook. -->
            <xs:element name="{./implementation/@shortName}-start" minOccurs="0" maxOccurs="1">
                <xs:complexType>
                    <xs:attribute name="conceptId" fixed="{(@id, @ref)[1]}"/>
                    <xs:attribute name="hidden" fixed="true"/>
                </xs:complexType>
            </xs:element>
            <xsl:choose>
                <xsl:when test="@type = 'item'">
                    <xs:element name="{./implementation/@shortName}" type="{./implementation/@shortName}_type_{translate((@id, @ref)[1], '.', '_')}">
                        <xsl:apply-templates select="@minimumMultiplicity | @maximumMultiplicity"/>
                    </xs:element>
                </xsl:when>
                <xsl:when test="@type = 'group'">
                    <xsl:apply-templates select="."/>
                </xsl:when>
            </xsl:choose>
        </xsl:for-each>
        <xs:element name="{$extensionElementName}-start" minOccurs="0" maxOccurs="1">
            <xs:complexType>
                <xs:attribute name="hidden" fixed="true"/>
            </xs:complexType>
        </xs:element>
        <xs:element name="{$extensionElementName}" type="adaextension_type" minOccurs="0" maxOccurs="1"/>
    </xsl:template>

    <xsl:template name="doConceptItems" match="concept[@type = 'item']" mode="doTypes">
        <xsl:variable name="isMandatory" select="ada:isMandatory(.)" as="xs:boolean"/>
        <xsl:variable name="isRequired" select="ada:isRequired(.)" as="xs:boolean"/>
        
        <xsl:variable name="datatypeAttribute" as="item()?">
            <xs:attribute name="datatype" type="ComplexDatatypes">
                <xsl:choose>
                    <!-- Nothing more to report if there is no valueDomain or if there are multiple -->
                    <xsl:when test="count(valueDomain) != 1"/>
                    <!-- Add fixed=reference if contains and valueDomain = 'string' -->
                    <xsl:when test="valueDomain/@type = 'string' and contains">
                        <xsl:attribute name="fixed" select="'reference'"/>
                    </xsl:when>
                    <!-- Add fixed=@originaltype if different from @type -->
                    <xsl:when test="valueDomain[@type][@originaltype][not(@type = @originaltype)]">
                        <xsl:attribute name="fixed" select="(valueDomain/@originaltype)[1]"/>
                    </xsl:when>
                    <!-- Add default=@type if complex -->
                    <xsl:when test="valueDomain[@type = 'complex']">
                        <xsl:attribute name="default" select="(valueDomain/@type)[1]"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:attribute name="fixed" select="(valueDomain/@type)[1]"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xs:attribute>
        </xsl:variable>
        <xsl:variable name="valueAttribute" as="item()?">
            <xs:attribute name="value">
                <xsl:if test="$isMandatory and not(valueDomain/@type = ('code', 'ordinal'))">
                    <xsl:attribute name="use">required</xsl:attribute>
                </xsl:if>
                <xsl:choose>
                    <!-- Use yes/no/unknown for non-mandatory booleans -->
                    <xsl:when test="valueDomain[@type = 'boolean']">
                        <!--<xsl:choose>
                            <xsl:when test="valueDomain/property/@fixed[lower-case(.) = ('yes','si','ja','oui','true','1')]">
                                <xsl:attribute name="fixed">true</xsl:attribute>
                            </xsl:when>
                            <xsl:when test="valueDomain/property/@fixed[lower-case(.) = ('no','non','nein','false','0')]">
                                <xsl:attribute name="fixed">false</xsl:attribute>
                            </xsl:when>
                            <xsl:when test="valueDomain/property/@default[lower-case(.) = ('yes','si','ja','oui','true','1')]">
                                <xsl:attribute name="fixed">true</xsl:attribute>
                            </xsl:when>
                            <xsl:when test="valueDomain/property/@default[lower-case(.) = ('no','non','nein','false','0')]">
                                <xsl:attribute name="fixed">false</xsl:attribute>
                            </xsl:when>
                        </xsl:choose>-->
                        <xs:simpleType>
                            <xs:restriction base="xs:string">
                                <xs:annotation>
                                    <xsl:choose>
                                        <xsl:when test="@conformance = 'M' or @isMandatory = 'true'">
                                            <xs:documentation>mandatory boolean</xs:documentation>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xs:documentation>non-mandatory boolean</xs:documentation>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xs:annotation>
                                <xsl:if test="@minimumMultiplicity = '0' or not(@minimumMultiplicity) or not(@conformance = 'M')">
                                    <xs:enumeration value="">
                                        <xs:annotation>
                                            <xs:documentation>Empty</xs:documentation>
                                        </xs:annotation>
                                    </xs:enumeration>
                                </xsl:if>
                                <xs:enumeration value="true">
                                    <xs:annotation>
                                        <xs:documentation>Yes</xs:documentation>
                                    </xs:annotation>
                                </xs:enumeration>
                                <xs:enumeration value="false">
                                    <xs:annotation>
                                        <xs:documentation>No</xs:documentation>
                                    </xs:annotation>
                                </xs:enumeration>
                                <!-- For non-mandatory elements, allow empty value strings -->
                                <xsl:if test="not(@conformance = 'M' or @isMandatory = 'true')">
                                    <xs:enumeration value="UNK">
                                        <xs:annotation>
                                            <xs:documentation>Unknown</xs:documentation>
                                        </xs:annotation>
                                    </xs:enumeration>
                                </xsl:if>
                            </xs:restriction>
                        </xs:simpleType>
                    </xsl:when>
                    <xsl:when test="valueDomain[@type = 'identifier'][identifierAssociation]">
                        <xs:simpleType>
                            <xs:restriction base="xs:string">
                                <xs:annotation>
                                    <xs:documentation>identifierAssociation</xs:documentation>
                                </xs:annotation>
                                <xsl:if test="@minimumMultiplicity = '0' or not(@minimumMultiplicity) or not(@conformance = 'M')">
                                    <xs:enumeration value="">
                                        <xs:annotation>
                                            <xs:documentation>Empty</xs:documentation>
                                        </xs:annotation>
                                    </xs:enumeration>
                                </xsl:if>
                                <!-- If there are valueSet items, pick those -->
                                <xsl:for-each select="identifierAssociation">
                                    <xs:enumeration value="{@ref}">
                                        <xs:annotation>
                                            <xs:documentation>
                                                <xsl:value-of select="@ref"/> - <xsl:value-of select="@refdisplay"/>
                                            </xs:documentation>
                                        </xs:annotation>
                                    </xs:enumeration>
                                </xsl:for-each>
                            </xs:restriction>
                        </xs:simpleType>
                    </xsl:when>
                    <!-- For code, always use a simpleType with enumeration -->
                    <xsl:when test="valueDomain[@type = ('code', 'ordinal')][../valueSet/conceptList/(concept | exception)]">
                        <xsl:variable name="simpleType" as="element()">
                            <xs:simpleType>
                                <xs:restriction base="xs:string">
                                    <xs:annotation>
                                        <xs:documentation>valueSet/conceptList/concept</xs:documentation>
                                    </xs:annotation>
                                    <xsl:if test="../valueSet/completeCodeSystem or ../valueSet/conceptList/include or @minimumMultiplicity = '0' or not(@minimumMultiplicity) or not(@conformance = 'M')">
                                        <xs:enumeration value="">
                                            <xs:annotation>
                                                <xs:documentation>Empty</xs:documentation>
                                            </xs:annotation>
                                        </xs:enumeration>
                                    </xsl:if>
                                    <!-- If there are valueSet items, pick those -->
                                    <xsl:if test="count(valueSet/conceptList/(concept | exception)) > 0">
                                        <xsl:for-each select="valueSet/conceptList/(concept | exception)">
                                            <xs:enumeration value="{@localId}">
                                                <xs:annotation>
                                                    <xs:documentation>
                                                        <xsl:value-of select="@displayName"/>
                                                    </xs:documentation>
                                                </xs:annotation>
                                            </xs:enumeration>
                                        </xsl:for-each>
                                    </xsl:if>
                                    <!-- If there are no valueSet items, but valueDomain items, pick those -->
                                    <xsl:if test="count(valueSet/conceptList/(concept | exception)) = 0 and count(valueDomain/conceptList/(concept | exception)) > 0">
                                        <xsl:for-each select="valueDomain/conceptList/(concept | exception)">
                                            <xs:enumeration value="{name}"/>
                                        </xsl:for-each>
                                    </xsl:if>
                                </xs:restriction>
                            </xs:simpleType>
                        </xsl:variable>
                        
                        <xsl:choose>
                            <xsl:when test="$simpleType/xs:restriction[@base][*]">
                                <xsl:copy-of select="$simpleType" copy-namespaces="no"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:attribute name="type" select="$simpleType/xs:restriction/@base"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <!-- For string where originaltype was date or datetime with YMD!, always use VariableDate -->
                    <xsl:when test="valueDomain[@type = 'string'][@originaltype = 'date'][property/@timeStampPrecision = ('Y', 'YM', 'YMD')]">
                        <xsl:attribute name="type">VariableVagueDate</xsl:attribute>
                    </xsl:when>
                    <xsl:when test="valueDomain[@type = 'string'][@originaltype = ('date', 'datetime')][property/@timeStampPrecision = ('Y', 'YM', 'YMD', 'YMDHM')]">
                        <xsl:attribute name="type">VariableVagueDateTime</xsl:attribute>
                    </xsl:when>
                    <xsl:when test="valueDomain[@type = 'string'][@originaltype = ('date', 'datetime')][property/@timeStampPrecision = 'Y!']">
                        <xsl:attribute name="type">DateYear</xsl:attribute>
                    </xsl:when>
                    <xsl:when test="valueDomain[@type = 'string'][@originaltype = ('date', 'datetime')][property/@timeStampPrecision = 'YM!']">
                        <xsl:attribute name="type">DateYearMonth</xsl:attribute>
                    </xsl:when>
                    <xsl:when test="valueDomain[@type = 'string'][@originaltype = 'date'] |
                        valueDomain[@type = 'string'][@originaltype = ('date', 'datetime')][property/@timeStampPrecision = 'YMD!']">
                        <xsl:attribute name="type">VariableDate</xsl:attribute>
                    </xsl:when>
                    <xsl:when test="valueDomain[@type = 'string'][@originaltype = 'datetime']">
                        <xsl:attribute name="type">VariableDateTime</xsl:attribute>
                    </xsl:when>
                    <xsl:when test="valueDomain[@type = 'string'][@originaltype = 'time'][property/@timeStampPrecision = ('H', 'HM', 'HMS')]">
                        <xsl:attribute name="type">VariableVagueTime</xsl:attribute>
                    </xsl:when>
                    <xsl:when test="valueDomain[@type = 'string'][@originaltype = 'time'][property/@timeStampPrecision = 'H!']">
                        <xsl:attribute name="type">TimeHour</xsl:attribute>
                    </xsl:when>
                    <xsl:when test="valueDomain[@type = 'string'][@originaltype = 'time'][property/@timeStampPrecision = 'HM!']">
                        <xsl:attribute name="type">TimeHourMinute</xsl:attribute>
                    </xsl:when>
                    <xsl:when test="valueDomain[@type = 'string'][@originaltype = 'time']">
                        <xsl:attribute name="type">VariableTime</xsl:attribute>
                    </xsl:when>
                    <!-- For valueDomain with non-empty properties, use a simpleType memberTypes for each property -->
                    <!-- 
                        Note: for
                        <property minInclude="25" maxInclude="240" unit="kg"/>
                        <property minInclude="2500" maxInclude="24000" unit="g"/>
                        this will still allow 2500 kg... To fix, we need XML Schema 1.1 or Schematron 
                    -->
                    <xsl:when test="valueDomain[not(@type = ('code','ordinal'))][property/@*]">
                        <xs:simpleType>
                            <xs:union>
                                <xsl:attribute name="memberTypes">
                                    <xsl:for-each select="valueDomain/property[@*]">
                                        <xsl:value-of select="../../implementation/@shortName"/>
                                        <xsl:text>_</xsl:text>
                                        <xsl:value-of select="position()"/>
                                        <xsl:text>_datatype_</xsl:text>
                                        <xsl:value-of select="translate(../../(@id, @ref)[1], '.', '_')"/>
                                        <xsl:text> </xsl:text>
                                    </xsl:for-each>
                                    <!-- For non-mandatory elements, allow empty value strings -->
                                    <xsl:if test="@minimumMultiplicity != '1'">
                                        <xsl:text>empty_string</xsl:text>
                                    </xsl:if>
                                </xsl:attribute>
                            </xs:union>
                        </xs:simpleType>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:variable name="simpleType" as="element()">
                            <xs:simpleType>
                                <xs:restriction>
                                    <xsl:attribute name="base">
                                        <!-- try to deal with multiple valueDomains. use xs:string if available, otherwise first from list -->
                                        <xsl:variable name="baseTypes" as="xs:string+">
                                            <xsl:apply-templates select="valueDomain" mode="getSchemaType"/>
                                        </xsl:variable>
                                        <xsl:choose>
                                            <xsl:when test="$baseTypes = 'xs:string'">xs:string</xsl:when>
                                            <xsl:otherwise>
                                                <xsl:value-of select="$baseTypes[1]"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:attribute>
                                    <xsl:if test="valueDomain[@type = ('string', 'text', 'identifier', 'complex')][../@isMandatory = 'true']">
                                        <xs:annotation>
                                            <xs:documentation>@conformance='M'</xs:documentation>
                                        </xs:annotation>
                                        <xs:minLength value="1"/>
                                        <xs:pattern value=".*[^\s].*"/>
                                    </xsl:if>
                                </xs:restriction>
                            </xs:simpleType>
                        </xsl:variable>
                        
                        <xsl:choose>
                            <xsl:when test="$simpleType/xs:restriction[@base][*]">
                                <xsl:copy-of select="$simpleType" copy-namespaces="no"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:attribute name="type" select="$simpleType/xs:restriction/@base"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
            </xs:attribute>
        </xsl:variable>
        <!-- xsl:if test="valueDomain/@type = ('code', 'ordinal')" -->
        <xsl:variable name="codedAttributeGroup" as="item()*">
            <xsl:if test="valueDomain/@type = ('code', 'ordinal')">
                <xs:attribute name="code" type="xs:string">
                    <xsl:if test="$isMandatory">
                        <xsl:attribute name="use">required</xsl:attribute>
                    </xsl:if>
                </xs:attribute>
                <xs:attribute name="codeSystem" type="xs:string">
                    <xsl:if test="$isMandatory">
                        <xsl:attribute name="use">required</xsl:attribute>
                    </xsl:if>
                    <!-- if conformance is mandatory then there cannot be another codeSystem, else there could be NullFlavor -->
                    <xsl:variable name="codeSystem" select="distinct-values(valueSet//@codeSystem)"/>
                    <xsl:if test="count($codeSystem) = 1 and $isMandatory">
                        <xsl:attribute name="fixed" select="$codeSystem"/>
                    </xsl:if>
                </xs:attribute>
                <xs:attribute name="codeSystemName" type="xs:string"/>
                <xs:attribute name="codeSystemVersion" type="xs:string"/>
                <xs:attribute name="displayName" type="xs:string">
                    <xsl:if test="$isMandatory">
                        <xsl:attribute name="use">required</xsl:attribute>
                    </xsl:if>
                </xs:attribute>
                <xsl:if test="valueDomain[@type = ('ordinal')]">
                    <xs:attribute name="ordinal" type="xs:decimal"/>
                </xsl:if>
                <xs:attribute name="preferred" type="BooleanOrEmpty"/>
                <xs:attribute name="originalText" type="xs:string"/>
            </xsl:if>
        </xsl:variable>
        <!-- xsl:if test="valueDomain/@type = ('quantity', 'duration')" -->
        <xsl:variable name="quantityAttributeGroup" as="item()*">
            <xsl:if test="valueDomain/@type = ('quantity', 'duration')">
                <xs:attribute name="unit">
                    <xsl:choose>
                        <xsl:when test="valueDomain/property/@unit">
                            <xs:simpleType>
                                <xs:restriction base="xs:string">
                                    <xsl:for-each select="valueDomain/property/@unit">
                                        <xs:enumeration value="{.}"/>
                                    </xsl:for-each>
                                </xs:restriction>
                            </xs:simpleType>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="type">xs:string</xsl:attribute>
                        </xsl:otherwise>
                    </xsl:choose>
                </xs:attribute>
            </xsl:if>
        </xsl:variable>
        <xsl:variable name="complexCodedAttributeGroup" as="item()*">
            <xsl:if test="valueDomain/@type = 'complex'">
                <xs:attribute name="code" type="xs:string"/>
                <xs:attribute name="codeSystem" type="xs:string"/>
                <xs:attribute name="codeSystemName" type="xs:string"/>
                <xs:attribute name="codeSystemVersion" type="xs:string"/>
                <xs:attribute name="displayName" type="xs:string"/>
                <xs:attribute name="ordinal" type="xs:decimal"/>
                <xs:attribute name="preferred" type="BooleanOrEmpty"/>
                <xs:attribute name="originalText" type="xs:string"/>
            </xsl:if>
        </xsl:variable>
        <xsl:variable name="nullFlavorAttribute" as="item()?">
            <xsl:if test="not($isMandatory or valueDomain/@type = ('code', 'ordinal'))">
                <xs:attribute name="nullFlavor" type="NullFlavorNoInformation"/>
            </xsl:if>
        </xsl:variable>
        
        <xsl:if test="count(valueDomain) > 1">
            <xsl:message>
                <xsl:text>Concept has more than 1 valueDomain. This is not supported. Please check concept "</xsl:text>
                <xsl:value-of select="name[1]"/>
                <xsl:text>" id=</xsl:text>
                <xsl:value-of select="@id | @ref"/>
                <xsl:text> effectiveDate=</xsl:text>
                <xsl:value-of select="@effectiveDate | @flexibility"/>
            </xsl:message>
        </xsl:if>
        <xs:annotation>
            <xs:documentation>
                <xsl:text>Type for concept item: </xsl:text>
                <xsl:value-of select="name"/>
                <xsl:text> - </xsl:text>
                <xsl:value-of select="@minimumMultiplicity"/>
                <xsl:text>..</xsl:text>
                <xsl:value-of select="@maximumMultiplicity"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="@conformance"/>
            </xs:documentation>
        </xs:annotation>
        <!-- complexType for concept item -->
        <xs:complexType name="{implementation/@shortName}_type_{translate((@id, @ref)[1], '.', '_')}">
            <xs:sequence>
                <!--<xsl:if test="valueDomain/@type = ('code','ordinal','complex','quantity', 'duration')">
                    <xs:element name="translation-start"  minOccurs="0" maxOccurs="1">
                        <xs:complexType>
                            <xs:attribute name="hidden" fixed="true"/>
                        </xs:complexType>
                    </xs:element>
                    <xs:element name="translation" minOccurs="0" maxOccurs="unbounded">
                        <xs:complexType>
                            <xsl:copy-of select="$datatypeAttribute"/>
                            <xsl:copy-of select="$valueAttribute"/>
                            <xsl:copy-of select="$codedAttributeGroup"/>
                            <xsl:copy-of select="$quantityAttributeGroup"/>
                            <xsl:copy-of select="$complexCodedAttributeGroup"/>
                            <xsl:copy-of select="$nullFlavorAttribute"/>
                        </xs:complexType>
                    </xs:element>
                </xsl:if>-->
                <xs:element name="{$extensionElementName}-start" minOccurs="0" maxOccurs="1">
                    <xs:complexType>
                        <xs:attribute name="hidden" fixed="true"/>
                    </xs:complexType>
                </xs:element>
                <xs:element name="{$extensionElementName}" type="adaextension_type" minOccurs="0" maxOccurs="1"/>
            </xs:sequence>
            <!-- @conceptId -->
            <xs:attribute name="conceptId" fixed="{(@id, @ref)[1]}"/>
            <!-- @value -->
            <xsl:copy-of select="$valueAttribute"/>
            <!-- @datatype -->
            <xsl:copy-of select="$datatypeAttribute"/>
            <!-- @unit, use an anonymous simpleType with enumeration -->
            <xsl:copy-of select="$quantityAttributeGroup"/>
            <!-- @root, optional attribute for identifier -->
            <xsl:if test="valueDomain[@type = 'identifier']">
                <xsl:variable name="root" select="distinct-values(identifierAssociation/@ref)"/>
                <xs:attribute name="root" type="xs:string">
                    <xsl:if test="count($root) = 1">
                        <xsl:attribute name="default" select="$root"/>
                    </xsl:if>
                </xs:attribute>
            </xsl:if>
            <!-- @displayName, @code, @codeSystem, @codeSystemName, @codeSystemVersion optional attributes for code. -->
            <xsl:copy-of select="$codedAttributeGroup"/>
            <!-- Add every special attribute in case of complex. we do not know what complex is. 
                it could be an observation/value destined for xsi:type -->
            <xsl:if test="valueDomain/@type = ('complex')">
                <!--<xs:attribute name="datatype" type="ComplexDatatypes"/>-->
                <xs:attribute name="root" type="xs:string"/>
                <xs:attribute name="unit" type="xs:string"/>
                <xsl:copy-of select="$complexCodedAttributeGroup"/>
            </xsl:if>
            <!-- Add nullFlavor if not mandatory, code or ordinal (because then it would be in code/codeSystem) -->
            <xsl:copy-of select="$nullFlavorAttribute"/>
            <!-- Add @id if @adaId='true' -->
            <xsl:choose>
                <xsl:when test="@dobId = 'true'">
                    <xs:attribute name="id" fixed="DOB"/>
                </xsl:when>
                <xsl:when test="@adaId = 'true'">
                    <xs:attribute name="id" type="xs:ID"/>
                </xsl:when>
            </xsl:choose>
        </xs:complexType>
        <xsl:apply-templates/>
    </xsl:template>

    <!-- Do the multiplicities -->
    <xsl:template name="minimumMultiplicity" match="@minimumMultiplicity">
        <xsl:choose>
            <xsl:when test="(. = '') or (. = '0')">
                <xsl:attribute name="minOccurs">0</xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
                <xsl:attribute name="minOccurs">1</xsl:attribute>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template name="maximumMultiplicity" match="@maximumMultiplicity">
        <xsl:choose>
            <xsl:when test="(. = '') or (. = '*')">
                <xsl:attribute name="maxOccurs">unbounded</xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
                <xsl:attribute name="maxOccurs">
                    <xsl:value-of select="."/>
                </xsl:attribute>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Make a simpleType for each property. Name will be {@shortName)_{counter for each property}_datatype_{underscored_id}, 
        i.e. weight_1_datatype_2_16..., weight_2_datatype_2_16... -->
    <xsl:template name="simpleTypeForProperty" match="valueDomain[property/@*]">
        <!-- For all non-empty properties -->
        <xsl:for-each select="property[@*]">
            <xsl:variable name="baseType" as="xs:string">
                <xsl:apply-templates select="parent::valueDomain" mode="getSchemaType"/>
            </xsl:variable>
            <xs:annotation>
                <xs:documentation>simpleType for valueDomain: <xsl:value-of select="../../name"/>: <xsl:value-of select="../@type"/>
                </xs:documentation>
            </xs:annotation>
            <xs:simpleType name="{../../implementation/@shortName}_{position()}_datatype_{translate(../../(@id, @ref)[1], '.', '_')}">
                <xsl:choose>
                    <xsl:when test="parent::valueDomain/@type = 'datetime' and @timeStampPrecision = 'YMD'">
                        <xs:union memberTypes="xs:date xs:dateTime"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xs:restriction>
                            <xsl:attribute name="base" select="$baseType"/>
                            <xsl:if test="@minInclude and not($baseType = 'xs:string')">
                                <xs:minInclusive value="{@minInclude}"/>
                            </xsl:if>
                            <xsl:if test="@maxInclude and not($baseType = 'xs:string')">
                                <xs:maxInclusive value="{@maxInclude}"/>
                            </xsl:if>
                            <xsl:if test="@minLength and $baseType = 'xs:string'">
                                <xs:minLength value="{@minLength}"/>
                            </xsl:if>
                            <xsl:if test="not(@minLength) and (../../@conformance = 'M') and $baseType = 'xs:string'">
                                <xs:annotation>
                                    <xs:documentation>@conformance='M'</xs:documentation>
                                </xs:annotation>
                                <xs:minLength value="1"/>
                                <xs:pattern value=".*[^\s].*"/>
                            </xsl:if>
                            <xsl:if test="@maxLength and $baseType = 'xs:string'">
                                <xs:maxLength value="{@maxLength}"/>
                            </xsl:if>
                            <xsl:if test="@timeStampPrecision = 'Y!' and $baseType = 'xs:string'">
                                <xs:pattern value="\d{{4}}"/>
                            </xsl:if>
                            <xsl:if test="@timeStampPrecision = 'YM!' and $baseType = 'xs:string'">
                                <!-- 2018-04 -->
                                <xs:pattern value="\d{{4}}-(0[1-9])|(1[0-2])"/>
                            </xsl:if>
                        </xs:restriction>
                    </xsl:otherwise>
                </xsl:choose>
            </xs:simpleType>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="valueDomain" mode="getSchemaType">
        <xsl:choose>
            <xsl:when test="@type = 'count'">xs:nonNegativeInteger</xsl:when>
            <xsl:when test="@type = 'boolean'">xs:boolean</xsl:when>
            <xsl:when test="@type = ('date','datetime') and property/@timeStampPrecision = ('Y!','YM!')">xs:string</xsl:when>
            <xsl:when test="@type = 'date'">xs:date</xsl:when>
            <xsl:when test="@type = 'datetime' and property/@timeStampPrecision = 'YMD!'">xs:date</xsl:when>
            <xsl:when test="@type = 'datetime'">xs:dateTime</xsl:when>
            <!-- Add better support for duration when it is better defined in DECOR -->
            <xsl:when test="@type = 'duration'">xs:string</xsl:when>
            <xsl:when test="@type = 'blob'">xs:base64Binary</xsl:when>
            <!-- this would be strange, a quantity without property, just in case... -->
            <xsl:when test="@type = 'quantity'">xs:decimal</xsl:when>
            <!-- complex, currency, ratio not supported yet -->
            <!-- For others (string, text, identifier, catchall we do a string -->
            <xsl:otherwise>xs:string</xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Skip the rest -->
    <xsl:template match="text() | @*"/>
</xsl:stylesheet>
