<?xml version="1.0" encoding="UTF-8"?>
<!-- 
Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

This program is free software; you can redistribute it and/or modify it under the terms 
of the GNU Affero General Public Licenseas published by the Free Software Foundation; 
either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the GNU Affero General Public Licensefor more details.

See http://www.gnu.org/licenses/
-->
<xsl:stylesheet xmlns:ada="http://www.art-decor.org/ada" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ev="http://www.w3.org/2001/xml-events" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fr="http://orbeon.org/oxf/xml/form-runner" xmlns:xxf="http://orbeon.org/oxf/xml/xforms" xmlns:xf="http://www.w3.org/2002/xforms" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/2002/08/xhtml/xhtml1-transitional.xsd http://www.w3.org/2002/xforms http://www.w3.org/MarkUp/Forms/2002/XForms-Schema.xsd" version="2.0">
    <xsl:output method="html" omit-xml-declaration="yes"/>
    <xsl:include href="ada-basics.xsl"/>
    <xsl:variable name="debug" select="true()"/>
    <xsl:variable name="theNullFlavor" select="'2.16.840.1.113883.5.1008'"/>
    <xsl:variable name="theNullFlavors" as="element()">
        <xs:simpleType name="NoInformation">
            <xs:restriction base="cs">
                <xs:enumeration value="NI"/>
                <xs:enumeration value="INV"/>
                <xs:enumeration value="DER"/>
                <xs:enumeration value="OTH"/>
                <xs:enumeration value="NINF"/>
                <xs:enumeration value="PINF"/>
                <xs:enumeration value="UNC"/>
                <xs:enumeration value="MSK"/>
                <xs:enumeration value="NA"/>
                <xs:enumeration value="UNK"/>
                <xs:enumeration value="ASKU"/>
                <xs:enumeration value="NAV"/>
                <xs:enumeration value="NASK"/>
                <xs:enumeration value="QS"/>
                <xs:enumeration value="TRC"/>
            </xs:restriction>
        </xs:simpleType>
    </xsl:variable>
    
    <xsl:template match="/">
        <xsl:for-each select="//view[@target = 'xforms']">
            <xsl:variable name="href" select="concat($projectDiskRoot, 'views/', implementation/@shortName, '.xhtml')"/>
            <xsl:result-document href="{$href}" method="xhtml">
                <xsl:comment>ADA XForms generator, <xsl:value-of select="current-dateTime()"/></xsl:comment>
                <xsl:text>
</xsl:text>
                <xsl:apply-templates select=".">
                    <xsl:with-param name="href" select="$href"/>
                </xsl:apply-templates>
            </xsl:result-document>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="view">
        <xsl:param name="href"/>
        <xsl:variable name="viewType" select="@type"/>
        <xsl:processing-instruction name="xml-model">
            <xsl:text>href="http://www.oxygenxml.com/1999/xhtml/xhtml-xforms.nvdl" schematypens="http://purl.oclc.org/dsdl/nvdl/ns/structure/1.0"</xsl:text>
        </xsl:processing-instruction>
        <xsl:text>
</xsl:text>
        <xhtml:html>
            <xhtml:head>
                <xhtml:title>
                    <xsl:value-of select="name"/>
                    <xsl:value-of select="ada:getMessage('save')"/>
                </xhtml:title>
                <xhtml:link rel="stylesheet" type="text/css" href="/forms/assets/ada.css"/>
                <xhtml:style type="text/css">
                    .CodeMirror { max-width: 1204px !important; height: 100% !important; }
                    span.plain textarea { padding: 4px 2px; height: 2.1em; resize: both; }
                    div.adaSingle:not(.adaGroup):hover,
                    div.adaMany:not(.adaGroup) div:hover {
                        background-color: lightblue;
                    }
                </xhtml:style>
                <!-- TODO: actual schema -->
                <xf:model id="adaxml">
                    <xsl:if test="not($viewType = 'index')">
                        <xsl:attribute name="schema">
                            <xsl:text>../schemas/</xsl:text>
                            <xsl:value-of select="dataset/@shortName"/>
                            <xsl:text>.xsd</xsl:text>
                        </xsl:attribute>
                    </xsl:if>
                    <xf:instance id="data">
                        <data/>
                    </xf:instance>
                    <xf:action ev:observer="data" ev:event="xforms-insert xforms-delete xxforms-value-changed">
                        <xf:setvalue ref="instance('data-safe')">false</xf:setvalue>
                    </xf:action>
                    <xf:instance id="data-safe">
                        <data>true</data>
                    </xf:instance>
                    <xf:bind nodeset="instance('data-safe')" readonly=".='true'"/>
                    <xxf:variable name="ada-exist" select="xxf:property('ada.exist.url')"/>
                    <xxf:variable name="ada-exist-external" select="xxf:property('ada.external.exist.url')"/>
                    <xxf:variable name="decor-external-exist" select="xxf:property('decor.external.exist.url')"/>
                    <!-- Variables to display buttons, real permissions checks are also server-side on submit -->
                    <xxf:variable name="update" select="contains(instance('data')/@rights, 'update')"/>
                    <xxf:variable name="delete" select="contains(instance('data')/@rights, 'delete')"/>
                    <xsl:choose>
                        <xsl:when test="$viewType = 'crud'">
                            <xsl:call-template name="crudSubmissions"/>
                            <!-- crud doesn't really need this, but it simplifies body logic -->
                            <xf:instance id="selected-data">
                                <data><xsl:value-of select="implementation/@shortName"/>.xhtml</data>
                            </xf:instance>
                            <xf:instance id="data-choices">
                                <data>
                                    <form name="{implementation/@shortName}.xhtml" desc="{name}" transactionId="{@transactionId}" transactionEffectiveDate="{@transactionEffectiveDate}"/>
                                </data>
                            </xf:instance>
                            <xf:instance id="data-changed">
                                <updated>false</updated>
                            </xf:instance>
                            <xf:action ev:observer="data-changed" ev:event="xxforms-value-changed" if="instance('data-changed') = 'true'">
                                <xsl:comment> delete concepts that are not hidden, that have a preceding hidden concept with the same id (means optional) and has no data </xsl:comment>
                                <xf:delete nodeset="instance('data')/descendant::*[not(@hidden | *)][@conceptId = preceding-sibling::*[@hidden]/@conceptId][string-length(string-join(@value|@code|@originalText|@nullFlavor,'')) = 0]"/>
                                <xsl:comment> delete concept groups that are not hidden, that have a preceding hidden concept with the same id (means optional), and have no descendant concepts with data </xsl:comment>
                                <xf:delete context="instance('data')/descendant::*[not(@hidden)][*][@conceptId = preceding-sibling::*[@hidden]/@conceptId][not(descendant::*[not(@hidden)][string-length(string-join(@value|@code|@originalText|@nullFlavor,'')) gt 0])]"/>
                                
                                <xf:setvalue ref="instance('data-changed')">false</xf:setvalue>
                            </xf:action>
                            <xf:instance id="nullFlavorSelection">
                                <nullFlavors>
                                    <xsl:for-each select="$theNullFlavors//xs:enumeration/@value">
                                        <enumeration value="{.}">
                                            <xsl:value-of select="ada:getMessage(concat('nullFlavor_', .))"/>
                                        </enumeration>
                                    </xsl:for-each>
                                </nullFlavors>
                            </xf:instance>
                            <xf:instance id="datatypeSelection">
                                <xsl:for-each select="$theDatatypes/DataSetValueType">
                                    <xsl:copy copy-namespaces="no">
                                        <xsl:copy-of select="*"/>
                                        <xsl:if test="not(enumeration[@value = 'reference'])">
                                            <enumeration value="reference">
                                                <label language="nl-NL">Verwijzing</label>
                                                <label language="en-US">Reference</label>
                                                <label language="de-DE">Referenz</label>
                                                <label language="pl-PL">Reference</label>
                                            </enumeration>
                                        </xsl:if>
                                    </xsl:copy>
                                </xsl:for-each>
                            </xf:instance>
                        </xsl:when>
                        <xsl:when test="$viewType = 'index'">
                            <xsl:call-template name="indexSubmissions"/>
                            <!-- index only really needs this for apps with multiple cruds for different transactions -->
                            <xf:instance id="selected-data">
                                <data><xsl:value-of select="indexOf[1]/@shortName"/>.xhtml</data>
                            </xf:instance>
                            <xf:instance id="data-choices">
                                <data>
                                    <xsl:for-each select="indexOf">
                                        <xsl:variable name="ref" select="@ref"/>
                                        <form name="{@shortName}.xhtml" desc="{//view[@id=$ref]/name}" transactionId="{//view[@id=$ref]/@transactionId}" transactionEffectiveDate="{//view[@id=$ref]/@transactionEffectiveDate}"/>
                                    </xsl:for-each>
                                </data>
                            </xf:instance>
                            <xf:instance id="data-id">
                                <id/>
                            </xf:instance>
                            <xf:instance id="rename-instance">
                                <rename app="{$projectName}" id="" newid="" active="false"/>
                            </xf:instance>
                            <xf:submission id="rename-data" method="post" ref="instance('rename-instance')" replace="none" xxf:username="{{xxf:get-session-attribute('username')}}" xxf:password="{{xxf:get-session-attribute('password')}}" resource="{{$ada-exist-external}}/modules/rename-data.xquery?app={ancestor::ada/@app}&amp;id={{encode-for-uri(instance('rename-instance')/@id)}}&amp;newid={{encode-for-uri(instance('rename-instance')/@newid)}}">
                                <xf:message ev:event="xforms-submit-error" level="modal">A submission error occurred: 
                                    <xf:output value="event('error-type')"/>; Status: 
                                    <xf:output value="event('response-status-code')"/>; URI: 
                                    <xf:output value="event('resource-uri')"/>; Headers: 
                                    <xf:output value="event('response-headers')"/>; Body: 
                                    <xf:output value="event('response-body')"/>
                                </xf:message>
                                <xf:setvalue ev:event="xforms-submit-done" ref="instance('rename-instance')/@active" value="false()"/>
                                <xf:send ev:event="xforms-submit-done" submission="get-data"/>
                            </xf:submission>
                            <xf:submission id="remove-data" method="delete" replace="none" serialization="none" xxf:username="{{xxf:get-session-attribute('username')}}" xxf:password="{{xxf:get-session-attribute('password')}}">
                                <xsl:attribute name="resource">
                                    <xsl:text>{$ada-exist}/projects/</xsl:text>
                                    <xsl:value-of select="$projectName"/>
                                    <xsl:text>/{instance('data-id')/string()}</xsl:text>
                                </xsl:attribute>
                                <xsl:call-template name="doSubmitErrorMessage"/>
                                <xf:send ev:event="xforms-submit-done" submission="get-data"/>
                            </xf:submission>
                            <xf:instance id="show-id">
                                <id>false</id>
                            </xf:instance>
                            <xf:instance id="show-desc">
                                <id>false</id>
                            </xf:instance>
                        </xsl:when>
                    </xsl:choose>
                    <xf:action ev:event="xforms-model-construct-done">
                        <!-- Always get data, permissions are controlled server-side -->
                        <xf:send submission="get-data"/>
                        <xsl:if test="$viewType = 'crud'">
                            <xf:send submission="get-summary"/>
                        </xsl:if>
                    </xf:action>
                    <xf:action ev:event="xforms-ready">
                        <xf:setvalue ref="instance('data-changed')">true</xf:setvalue>
                        <xf:setvalue ref="instance('data-safe')">true</xf:setvalue>
                    </xf:action>
                    <xsl:apply-templates mode="doTheBindings"/>
                    <xsl:comment> Generic bindings</xsl:comment>
                    <xf:bind nodeset="//*/@root">
                        <xsl:attribute name="constraint">string-length()=0 or matches(.,'^[0-2](\.(0|[1-9][0-9]*))*$')</xsl:attribute>
                    </xf:bind>
                    <xf:bind nodeset="//*/@codeSystem">
                        <xsl:attribute name="constraint">string-length()=0 or matches(.,'^[0-2](\.(0|[1-9][0-9]*))*$')</xsl:attribute>
                    </xf:bind>
                    <!--<xf:bind nodeset="//@preferred" type="xf:boolean"/>-->
                    <xsl:apply-templates mode="doTheConditions"/>
                    <xxf:variable name="dba" select="tokenize(xxf:get-session-attribute('groups'), '\s') = 'dba'"/>
                    <xxf:variable name="editor" select="($dba or tokenize(xxf:get-session-attribute('groups'), '\s') = 'ada-user') and string-length(xxf:get-session-attribute('username')) gt 0"/>
                    <xxf:variable name="debug" select="$dba or tokenize(xxf:get-session-attribute('groups'), '\s') = 'debug'"/>
                </xf:model>
            </xhtml:head>
            <xhtml:body style="background: none" class="ada">
                <xf:group ref=".[not($editor)]">
                    <xhtml:div>
                        <xsl:value-of select="ada:getMessage('warning')"/>
                    </xhtml:div>
                </xf:group>
                <xhtml:h1>
                    <xsl:value-of select="name"/>
                </xhtml:h1>
                <xhtml:div style="font-size: smaller;">
                    <xsl:value-of select="ada:getMessage('disclaimer')"/>
                </xhtml:div>
                <xhtml:table id="toprow" width="100%" style="background: transparent; margin-bottom: 20px; border: 0px; border-bottom: 2px solid black; border-top: 2px solid black;">
                    <!-- row with menu, login -->
                    <xhtml:tr>
                        <xhtml:td width="50%" style="border-left: 0px; border-right: 0px;">
                            <xsl:choose>
                                <xsl:when test="$viewType = 'crud'">
                                    <xhtml:div>
                                        <xsl:if test="dataset//concept[@widget = 'collapse'] | dataset//concept[@type = 'group'][not(@widget = 'tab')]">
                                            <xf:trigger>
                                                <xf:label>
                                                    <xsl:value-of select="ada:getMessage('expand-all')"/>
                                                </xf:label>
                                                <xf:dispatch ev:event="DOMActivate" target="main-accordion" name="fr-show-all"/>
                                            </xf:trigger>
                                            <xf:trigger>
                                                <xf:label>
                                                    <xsl:value-of select="ada:getMessage('collapse-all')"/>
                                                </xf:label>
                                                <xf:dispatch ev:event="DOMActivate" target="main-accordion" name="fr-hide-all"/>
                                            </xf:trigger>
                                        </xsl:if>
                                        <xf:trigger ref=".[instance('data')/descendant::*[not(@hidden | *)][@conceptId = preceding-sibling::*[@hidden]/@conceptId][string-length(string-join(@value|@code|@originalText|@nullFlavor,'')) = 0]]" id="removeOptionalEmptyConcepts">
                                            <xf:label>
                                                <xsl:value-of select="ada:getMessage('remove-empty-concepts-and-groups')"/>
                                            </xf:label>
                                            <xf:setvalue ev:event="DOMActivate" ref="instance('data-changed')" value="'true'"/>
                                        </xf:trigger>
                                    </xhtml:div>
                                </xsl:when>
                                <xsl:when test="$viewType = 'index'">
                                    <xf:group ref=".[$editor]">
                                        <xf:trigger appearance="compact">
                                            <xf:label>
                                                <xsl:value-of select="ada:getMessage('new')"/>
                                            </xf:label>
                                            <xf:action ev:event="DOMActivate">
                                                <!-- transactionId/transactionEffectiveDate are relevant when multiple 'new' files could exist -->
                                                <xf:load resource="{{instance('selected-data')}}?id=new&amp;transactionId={{instance('data-choices')/form[@name=instance('selected-data')]/@transactionId}}&amp;transactionEffectiveDate={{instance('data-choices')/form[@name=instance('selected-data')]/@transactionEffectiveDate}}" show="new"/>
                                            </xf:action>
                                        </xf:trigger>
                                    </xf:group>
                                    <xsl:if test="count(indexOf) > 1">
                                        <xhtml:span style="padding-left: 20px;"/>
                                        <xf:group ref=".[$editor][count(instance('data-choices')/*)>1]">
                                            <xf:select1 ref="instance('selected-data')">
                                                <xf:itemset nodeset="xxf:sort(instance('data-choices')/form, @desc)">
                                                    <xf:label ref="@desc"/>
                                                    <xf:value ref="@name"/>
                                                </xf:itemset>
                                            </xf:select1>
                                        </xf:group>
                                    </xsl:if>
                                    <xf:group ref=".[count(distinct-values(instance('data')//resource/*/@shortName)) gt 1]">
                                        <xf:trigger>
                                            <xf:label>
                                                <xsl:value-of select="ada:getMessage('expand-all')"/>
                                            </xf:label>
                                            <xf:dispatch ev:event="DOMActivate" target="main-accordion" name="fr-show-all"/>
                                        </xf:trigger>
                                        <xf:trigger>
                                            <xf:label>
                                                <xsl:value-of select="ada:getMessage('collapse-all')"/>
                                            </xf:label>
                                            <xf:dispatch ev:event="DOMActivate" target="main-accordion" name="fr-hide-all"/>
                                        </xf:trigger>
                                    </xf:group>
                                </xsl:when>
                            </xsl:choose>
                        </xhtml:td>
                        <xhtml:td width="25%" style="border-left: 0px; border-right: 0px;">
                            <xf:trigger appearance="minimal">
                                <xf:label>
                                    <xsl:value-of select="ada:getMessage('documentation')"/>
                                </xf:label>
                                <xf:action ev:event="DOMActivate">
                                    <xf:load resource="{concat($releaseBaseUri, '?id=', '{instance(''data-choices'')/form[@name=instance(''selected-data'')]/@transactionId}', '&amp;language=', $language,'&amp;version=', $versionDate, '&amp;format=html')}" show="new"/>
                                </xf:action>
                            </xf:trigger>
                            <!-- for index the selector will be next to the "new" button already hence we don't duplicate here -->
                            <xsl:if test="not($viewType = 'index') and count(indexOf) > 1">
                                <xhtml:span style="padding-left: 20px;"/>
                                <xf:group ref=".[count(instance('data-choices')/*)>1]">
                                    <xf:select1 ref="instance('selected-data')">
                                        <xf:itemset nodeset="instance('data-choices')/form">
                                            <xf:label ref="@desc"/>
                                            <xf:value ref="@name"/>
                                        </xf:itemset>
                                    </xf:select1>
                                </xf:group>
                            </xsl:if>
                        </xhtml:td>
                        <!-- login -->
                        <xhtml:td style="border-left: 0px; border-right: 0px;">
                            <xsl:if test="$viewType = 'index'">
                                <xf:trigger ref=".[not(instance('show-id') = 'true')]" appearance="minimal">
                                    <xf:label>
                                        <xsl:value-of select="ada:getMessage('Id')"/>
                                        <xsl:text>&#160;[+]</xsl:text>
                                    </xf:label>
                                    <xf:setvalue ev:event="DOMActivate" ref="instance('show-id')">true</xf:setvalue>
                                </xf:trigger>
                                <xf:trigger ref=".[not(instance('show-desc') = 'true')]" appearance="minimal">
                                    <xf:label>
                                        <xsl:value-of select="ada:getMessage('Description')"/>
                                        <xsl:text>&#160;[+]</xsl:text>
                                    </xf:label>
                                    <xf:setvalue ev:event="DOMActivate" ref="instance('show-desc')">true</xf:setvalue>
                                </xf:trigger>
                            </xsl:if>
                            <xhtml:div style="float:right;">
                                <xf:output ref="concat('{ada:getMessage('user')}: ',xxf:get-session-attribute('username'),' ')"/>
                                <xf:group ref=".[$editor]">
                                    <xf:trigger appearance="minimal">
                                        <xf:label>
                                            <xsl:value-of select="ada:getMessage('logout')"/>
                                        </xf:label>
                                        <xf:action ev:event="DOMActivate">
                                            <xf:load resource="../../../../session/logout"/>
                                        </xf:action>
                                    </xf:trigger>
                                </xf:group>
                                <xf:group ref=".[not($editor)]">
                                    <xf:trigger appearance="minimal">
                                        <xf:label>
                                            <xsl:value-of select="ada:getMessage('login')"/>
                                        </xf:label>
                                        <xf:action ev:event="DOMActivate">
                                            <xf:load resource="../../../../login?returnToUrl={{encode-for-uri('{replace($href,'^.*/art_decor','ada-data')}')}}"/>
                                        </xf:action>
                                    </xf:trigger>
                                </xf:group>
                            </xhtml:div>
                        </xhtml:td>
                    </xhtml:tr>
                </xhtml:table>
                <xf:group id="main-ui" appearance="full">
                    <xsl:choose>
                        <xsl:when test="$viewType = 'crud'">
                            <xsl:call-template name="crudForm"/>
                        </xsl:when>
                        <xsl:when test="$viewType = 'index'">
                            <xsl:call-template name="indexForm"/>
                        </xsl:when>
                    </xsl:choose>
                </xf:group>
                <xsl:choose>
                    <xsl:when test="$debug">
                        <fr:xforms-inspector/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xf:group ref=".[$debug]">
                            <fr:xforms-inspector/>
                        </xf:group>
                    </xsl:otherwise>
                </xsl:choose>
            </xhtml:body>
        </xhtml:html>
    </xsl:template>
    <xsl:template name="indexSubmissions">
        <xf:submission id="get-data" serialization="none" method="get" replace="instance" instance="data" xxf:username="{{xxf:get-session-attribute('username')}}" xxf:password="{{xxf:get-session-attribute('password')}}">
            <xsl:attribute name="resource">
                <xsl:text>{$ada-exist}/projects/</xsl:text>
                <xsl:value-of select="$projectName"/>
                <xsl:text>?summary=true</xsl:text>
            </xsl:attribute>
            <xf:header>
                <xf:name>Accept</xf:name>
                <xf:value>application/xml</xf:value>
            </xf:header>
            <xsl:call-template name="doSubmitErrorMessage"/>
            <xf:setvalue ev:event="xforms-submit-done" ref="instance('data-safe')">true</xf:setvalue>
        </xf:submission>
    </xsl:template>
    <xsl:template name="crudSubmissions">
        <xf:instance id="new">
            <xsl:attribute name="src">
                <xsl:text>../new/</xsl:text>
                <xsl:value-of select="implementation/@shortName"/>
                <xsl:text>.xml</xsl:text>
            </xsl:attribute>
        </xf:instance>
        <xf:instance id="summary-instance">
            <data/>
        </xf:instance>
        <xf:submission id="get-summary" mode="asynchronous" serialization="none" method="get" replace="instance" resource="{{$ada-exist}}/projects/{$projectName}?summary=ids" instance="summary-instance" xxf:username="{{xxf:get-session-attribute('username')}}" xxf:password="{{xxf:get-session-attribute('password')}}">
            <xf:header>
                <xf:name>Accept</xf:name>
                <xf:value>application/xml</xf:value>
            </xf:header>
            <xf:message ev:event="xforms-submit-error" level="modal">A submission error occurred: 
                <xf:output value="event('error-type')"/>; Status: 
                <xf:output value="event('response-status-code')"/>; URI: 
                <xf:output value="event('resource-uri')"/>; Headers: 
                <xf:output value="event('response-headers')"/>; Body: 
                <xf:output value="event('response-body')"/>
            </xf:message>
            <xf:setvalue ev:event="xforms-submit-done" ref="instance('data-safe')">true</xf:setvalue>
        </xf:submission>
        <xf:submission id="get-data" serialization="none" method="get" replace="instance" instance="data" xxf:username="{{xxf:get-session-attribute('username')}}" xxf:password="{{xxf:get-session-attribute('password')}}">
            <!-- transactionId/transactionEffectiveDate are relevant when multiple 'new' files could exist -->
            <xsl:attribute name="resource">
                <xsl:text>{$ada-exist}/projects/</xsl:text>
                <xsl:value-of select="$projectName"/>
                <xsl:text>/{xxf:get-request-parameter('id')}</xsl:text>
                <xsl:text>?</xsl:text>
                <xsl:text>{for $from in xxf:get-request-parameter('from')[string-length()>0] return concat('from=',$from,'&amp;')}</xsl:text>
                <xsl:text>&amp;transactionId={xxf:get-request-parameter('transactionId')}</xsl:text>
                <xsl:text>&amp;transactionEffectiveDate={xxf:get-request-parameter('transactionEffectiveDate')}</xsl:text>
            </xsl:attribute>
            <xf:header>
                <xf:name>Accept</xf:name>
                <xf:value>application/xml</xf:value>
            </xf:header>
            <xsl:call-template name="doSubmitErrorMessage"/>
            <xf:setvalue ev:event="xforms-submit-done" ref="instance('data-safe')">true</xf:setvalue>
        </xf:submission>
        <xf:submission id="save-data" ref="instance('data')" method="post" replace="instance" xxf:username="{{xxf:get-session-attribute('username')}}" xxf:password="{{xxf:get-session-attribute('password')}}">
            <xsl:attribute name="resource">
                <xsl:text>{$ada-exist}/projects/</xsl:text>
                <xsl:value-of select="$projectName"/>
            </xsl:attribute>
            <xsl:call-template name="doSubmitErrorMessage"/>
            <xf:action ev:event="xforms-submit-done">
                <!-- Patch... somehow this is already returned in save-data, but it does not reach the instance in the form and so we cannot save anymore. -->
                <xf:setvalue if="xxf:get-request-parameter('id') = 'new'" ref="instance('data')[@rights = '']">update</xf:setvalue>
                <xf:setvalue ref="instance('data-changed')">true</xf:setvalue>
                <xf:setvalue ref="instance('data-safe')">true</xf:setvalue>
            </xf:action>
        </xf:submission>
        <xf:submission id="save-data-and-close" ref="instance('data')" method="post" replace="none" validate="true" xxf:username="{{xxf:get-session-attribute('username')}}" xxf:password="{{xxf:get-session-attribute('password')}}">
            <xsl:attribute name="resource">
                <xsl:text>{$ada-exist}/projects/</xsl:text>
                <xsl:value-of select="$projectName"/>
            </xsl:attribute>
            <xsl:call-template name="doSubmitErrorMessage"/>
            <xsl:call-template name="returnToIndex"/>
        </xf:submission>
        <xf:submission id="save-draft-data" ref="instance('data')" method="post" replace="instance" validate="false" relevant="false" xxf:username="{{xxf:get-session-attribute('username')}}" xxf:password="{{xxf:get-session-attribute('password')}}">
            <xsl:attribute name="resource">
                <xsl:text>{$ada-exist}/projects/</xsl:text>
                <xsl:value-of select="$projectName"/>
            </xsl:attribute>
            <xsl:call-template name="doSubmitErrorMessage"/>
            <xf:action ev:event="xforms-submit-done">
                <!-- Patch... somehow this is already returned in save-data, but it does not reach the instance in the form and so we cannot save anymore. -->
                <xf:setvalue if="xxf:get-request-parameter('id') = 'new'" ref="instance('data')[@rights = '']">update</xf:setvalue>
                <xf:setvalue ref="instance('data-changed')">true</xf:setvalue>
                <xf:setvalue ref="instance('data-safe')">true</xf:setvalue>
            </xf:action>
        </xf:submission>
        <xf:submission id="save-draft-data-and-close" ref="instance('data')" method="post" replace="none" validate="false" relevant="false" xxf:username="{{xxf:get-session-attribute('username')}}" xxf:password="{{xxf:get-session-attribute('password')}}">
            <xsl:attribute name="resource">
                <xsl:text>{$ada-exist}/projects/</xsl:text>
                <xsl:value-of select="$projectName"/>
            </xsl:attribute>
            <xsl:call-template name="doSubmitErrorMessage"/>
            <xsl:call-template name="returnToIndex"/>
        </xf:submission>
        <xf:submission id="remove-data" ref="instance('data')" method="delete" replace="none" serialization="none" xxf:username="{{xxf:get-session-attribute('username')}}" xxf:password="{{xxf:get-session-attribute('password')}}">
            <xsl:attribute name="resource">
                <xsl:text>{$ada-exist}/projects/</xsl:text>
                <xsl:value-of select="$projectName"/>
                <xsl:text>/{instance('data')/@id/string()}</xsl:text>
            </xsl:attribute>
            <xsl:call-template name="doSubmitErrorMessage"/>
            <xsl:call-template name="returnToIndex"/>
        </xf:submission>
    </xsl:template>
    <xsl:template name="doSubmitErrorMessage">
        <xf:message ev:event="xforms-submit-error" level="modal">
            <xsl:text>A submission error occurred: </xsl:text>
            <xf:output value="event('error-type')"/>
            <xsl:text>; Status: </xsl:text>
            <xf:output value="event('response-status-code')"/>
            <xsl:text>; URI: </xsl:text>
            <xf:output value="event('resource-uri')"/>
            <xsl:text>; Headers: </xsl:text>
            <xf:output value="event('response-headers')"/>
            <xsl:text>; Body: </xsl:text>
            <xf:output value="event('response-body')"/>
        </xf:message>
    </xsl:template>
    <xsl:template name="returnToIndex">
        <xf:action ev:event="xforms-submit-done">
            <!-- Now that the index starts new tabs, we should just close the window, rather than 
                go back to the index, otherwise we end up with a gazillion tabs -->
            <xxf:script>window.close();</xxf:script>
            <!--<xsl:choose>
                <xsl:when test="//view[@type = 'index'][1]/@target = 'xforms'">
                    <xsl:variable name="transactionId" select="@transactionId"/>
                    <xsl:variable name="shortNameIndex" select="//view[@type='index'][@transactionId=$transactionId][1]/implementation/@shortName"/>
                    <xsl:choose>
                        <xsl:when test="$shortNameIndex">
                            <xf:load resource="{concat($shortNameIndex[1]/string(), '.xhtml')}"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xf:load resource="{concat(//view[@type='index'][1]/implementation/@shortName/string(), '.xhtml')}"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="//view[@type = 'index'][1]/@target = 'xquery'">
                    <xf:load resource="../modules/index.xquery"/>
                </xsl:when>
                <xsl:otherwise/>
            </xsl:choose>-->
        </xf:action>
    </xsl:template>
    <xsl:template name="crudForm">
        <xhtml:div class="adaForm">
            <xhtml:div class="adaSingle adaGroup">
                <xf:textarea ref="@title" mediatype="text/plain" incremental="true" class="plain">
                    <xf:label>
                        <xsl:value-of select="ada:getMessage('title')"/>
                    </xf:label>
                    <xf:help>
                        <xsl:value-of select="ada:getMessage('title-help')"/>
                    </xf:help>
                </xf:textarea>
            </xhtml:div>
            <fr:accordion class="fr-accordion-lnf">
                <fr:case selected="false">
                    <fr:label>
                        <xsl:attribute name="ref">
                            <xsl:text>concat('</xsl:text>
                            <xsl:value-of select="replace(ada:getMessage('description'), '''', '''''')"/>
                            <xsl:text>: ', if (string-length(@desc) = 0) then '-' else substring(@desc, 1, 50))</xsl:text>
                        </xsl:attribute>
                    </fr:label>
                    <xf:trigger ref=".[not(@desc)]">
                        <xf:label>[+] Description</xf:label>
                        <xf:insert ev:event="DOMActivate" nodeset="@*" origin="xxf:attribute('desc', '')"/>
                    </xf:trigger>
                    <xf:textarea ref="@desc" mediatype="text/html" incremental="true">
                        <xf:label>
                            <xsl:value-of select="ada:getMessage('description')"/>
                        </xf:label>
                        <xf:help>
                            <xsl:value-of select="ada:getMessage('description-help')"/>
                        </xf:help>
                    </xf:textarea>
                </fr:case>
            </fr:accordion>
            <xhtml:hr style="margin: 1em 0;"/>
            
            <xsl:choose>
                <xsl:when test="dataset/concept[@widget = 'tab']">
                    <fr:tabview>
                        <xsl:apply-templates mode="doTheForm" select="dataset/concept">
                            <xsl:sort select="@order"/>
                        </xsl:apply-templates>
                    </fr:tabview>
                </xsl:when>
                <xsl:when test="dataset/concept[@widget = 'collapse'] | dataset/concept[@type = 'group']">
                    <fr:accordion id="main-accordion" class="fr-accordion-lnf">
                        <xsl:apply-templates mode="doTheForm" select="dataset/concept">
                            <xsl:sort select="@order"/>
                        </xsl:apply-templates>
                    </fr:accordion>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates mode="doTheForm" select="dataset/concept">
                        <xsl:sort select="@order"/>
                    </xsl:apply-templates>
                </xsl:otherwise>
            </xsl:choose>
        </xhtml:div>
        <xsl:if test="not(@errorSummary = 'false')">
            <fr:error-summary observer="main-ui" id="error-summary">
                <fr:label>
                    <xsl:value-of select="ada:getMessage('form-errors')"/>
                </fr:label>
                <!--<fr:errors nodeset="instance('errors')/error">
                    <fr:label ref="label"/>
                    <fr:alert ref="alert"/>
                </fr:errors>-->
            </fr:error-summary>
        </xsl:if>
        <xf:group ref=".[$update]">
            <xf:trigger ref="instance('data-safe')">
                <xf:label>
                    <xsl:value-of select="ada:getMessage('save')"/>
                </xf:label>
                <xf:action ev:event="DOMActivate">
                    <xsl:if test="not(@errorSummary='false')">
                        <xf:dispatch name="fr-visit-all" targetid="error-summary"/>
                        <xf:refresh/>
                        <xf:dispatch name="fr-update" targetid="error-summary"/>
                    </xsl:if>
                    <xf:send submission="save-data"/>
                </xf:action>
            </xf:trigger>
            <xf:trigger ref="instance('data-safe')">
                <xf:label>
                    <xsl:value-of select="concat(ada:getMessage('save'), '  &amp; ', ada:getMessage('close'))"/>
                </xf:label>
                <xf:action ev:event="DOMActivate">
                    <xsl:if test="not(@errorSummary='false')">
                        <xf:dispatch name="fr-visit-all" targetid="error-summary"/>
                        <xf:refresh/>
                        <xf:dispatch name="fr-update" targetid="error-summary"/>
                    </xsl:if>
                    <xf:send submission="save-data-and-close"/>
                </xf:action>
            </xf:trigger>
            <xf:trigger ref="instance('data-safe')">
                <xf:label>
                    <xsl:value-of select="ada:getMessage('save-draft')"/>
                </xf:label>
                <xf:action ev:event="DOMActivate">
                    <xsl:if test="not(@errorSummary='false')">
                        <xf:dispatch name="fr-visit-all" targetid="error-summary"/>
                        <xf:refresh/>
                        <xf:dispatch name="fr-update" targetid="error-summary"/>
                    </xsl:if>
                    <xf:send submission="save-draft-data"/>
                </xf:action>
            </xf:trigger>
            <xf:trigger ref="instance('data-safe')">
                <xf:label>
                    <xsl:value-of select="concat(ada:getMessage('save-draft'), '  &amp; ', ada:getMessage('close'))"/>
                </xf:label>
                <xf:action ev:event="DOMActivate">
                    <xsl:if test="not(@errorSummary='false')">
                        <xf:dispatch name="fr-visit-all" targetid="error-summary"/>
                        <xf:refresh/>
                        <xf:dispatch name="fr-update" targetid="error-summary"/>
                    </xsl:if>
                    <xf:send submission="save-draft-data-and-close"/>
                </xf:action>
            </xf:trigger>
        </xf:group>
        <xf:group ref=".[$delete]">
            <xf:trigger>
                <xf:label>
                    <xsl:value-of select="ada:getMessage('remove')"/>
                </xf:label>
                <xf:action ev:event="DOMActivate">
                    <xf:send submission="remove-data"/>
                </xf:action>
            </xf:trigger>
        </xf:group>
        <xf:trigger>
            <xf:label>
                <xsl:value-of select="ada:getMessage('close')"/>
            </xf:label>
            <xf:action ev:event="DOMActivate">
                <xsl:call-template name="returnToIndex"/>
            </xf:action>
        </xf:trigger>
    </xsl:template>
    <xsl:template name="indexForm">
        <xhtml:div class="adaForm">
            <xxf:variable name="resources" select="//resource"/>
            <xxf:variable name="shortNames" select="distinct-values($resources/*/@shortName)"/>
            <xf:group ref=".[count($shortNames) gt 1]">
                <fr:accordion id="main-accordion" class="fr-accordion-lnf">
                    <xf:repeat nodeset="$shortNames" appearance="full" id="main_index_shortNames">
                        <xxf:variable name="shortName" select="."/>
                        <xxf:variable name="trid" select="$resources/*[@shortName = $shortName]/@transactionRef"/>
                        <xxf:variable name="tred" select="$resources/*[@shortName = $shortName]/@transactionEffectiveDate"/>
                        <xxf:variable name="longName" select="(instance('data-choices')/form[@transactionId = $trid][@transactionEffectiveDate = $tred]/@desc, $shortName)[1]"/>
                        <fr:case selected="false">
                            <fr:label>
                                <xf:output ref="concat($longName, ' (',count($resources/*[@shortName = $shortName]),')')"/>>
                            </fr:label>
                            <xsl:call-template name="doTheIndexTable">
                                <xsl:with-param name="repeatsetid">main_index_multi</xsl:with-param>
                                <xsl:with-param name="repeatset">$resources/*[@shortName = $shortName]</xsl:with-param>
                            </xsl:call-template>
                        </fr:case>
                    </xf:repeat>
                </fr:accordion>
            </xf:group>
            <xf:group ref=".[count(distinct-values($resources/*/@shortName)) le 1]">
                <xxf:variable name="shortName" select="."/>
                <xsl:call-template name="doTheIndexTable">
                    <xsl:with-param name="repeatsetid">main_index</xsl:with-param>
                    <xsl:with-param name="repeatset">$resources/*</xsl:with-param>
                </xsl:call-template>
            </xf:group>
        </xhtml:div>
    </xsl:template>
    <xsl:template name="doTheIndexTable">
        <xsl:param name="repeatsetid" as="xs:string"/>
        <xsl:param name="repeatset" as="xs:string"/>
        <xhtml:table class="spaced" width="90%">
            <xhtml:tr>
                <xsl:for-each select="dataset">
                    <xf:group ref=".[$shortName = '{@shortName}']">
                        <xsl:for-each select="concept">
                            <xhtml:td class="item-label">
                                <xsl:value-of select="name"/>
                            </xhtml:td>
                        </xsl:for-each>
                    </xf:group>
                </xsl:for-each>
                <xhtml:td class="item-label">
                    <xsl:value-of select="ada:getMessage('title')"/>
                </xhtml:td>
                <xf:group ref=".[instance('show-desc') = 'true']">
                    <xhtml:td class="item-label">
                        <xsl:value-of select="ada:getMessage('Description')"/>
                        <xsl:text>&#160;</xsl:text>
                        <xf:trigger appearance="minimal">
                            <xf:label>[-]</xf:label>
                            <xf:setvalue ev:event="DOMActivate" ref="instance('show-desc')">false</xf:setvalue>
                        </xf:trigger>
                    </xhtml:td>
                </xf:group>
                <xhtml:td class="item-label">
                    <xsl:value-of select="ada:getMessage('owner')"/>
                </xhtml:td>
                <xhtml:td class="item-label">
                    <xsl:value-of select="ada:getMessage('Modified date')"/>
                </xhtml:td>
                <xf:group ref=".[instance('show-id') = 'true']">
                    <xhtml:td class="item-label">
                        <xsl:value-of select="ada:getMessage('Id')"/>
                        <xsl:text>&#160;</xsl:text>
                        <xf:trigger appearance="minimal">
                            <xf:label>[-]</xf:label>
                            <xf:setvalue ev:event="DOMActivate" ref="instance('show-id')">false</xf:setvalue>
                        </xf:trigger>
                    </xhtml:td>
                </xf:group>
                <xhtml:td class="item-label"/>
            </xhtml:tr>
            <xf:repeat nodeset="xxf:sort({$repeatset}, lower-case(@id), 'text', 'ascending')" appearance="full" id="{$repeatsetid}">
                <xhtml:tr>
                    <xsl:for-each select="dataset">
                        <xf:group ref=".[$shortName = '{@shortName}']">
                            <xsl:for-each select="concept">
                                <xhtml:td>
                                    <xf:output>
                                        <xsl:attribute name="ref">
                                            <xsl:text>.//*[@conceptId='</xsl:text>
                                            <xsl:value-of select="@id"/>
                                            <xsl:text>']/@value/string()</xsl:text>
                                        </xsl:attribute>
                                    </xf:output>
                                </xhtml:td>
                            </xsl:for-each>
                        </xf:group>
                    </xsl:for-each>
                    <xhtml:td>
                        <xf:output ref="@title">
                            <xf:help ref="context()/../@desc[not(. = '')]" mediatype="text/html"/>
                        </xf:output>
                    </xhtml:td>
                    <xf:group ref=".[instance('show-desc') = 'true']">
                        <xhtml:td>
                            <xf:output mediatype="text/html" ref="@desc"/>
                        </xhtml:td>
                    </xf:group>
                    <xhtml:td>
                        <xf:output ref="@owner/string()"/>
                    </xhtml:td>
                    <xhtml:td>
                        <xf:output ref="@last-update-date/string()" xxf:format="if (. castable as xs:dateTime) then replace(format-dateTime(xs:dateTime(.),'[Y0001]-[M01]-[D01] [H01]:[m01]:[s01]', (), (), ()),' 00:00:00','') else (.)"/>
                    </xhtml:td>
                    <xf:group ref=".[instance('show-id') = 'true']">
                        <xhtml:td>
                            <xf:output ref="@id/string()"/>
                            <xf:group ref=".[$editor][xxf:get-session-attribute('username') = @owner]">
                                <xf:trigger ref="@id[instance('rename-instance')/@active = 'false']">
                                    <xf:label>#</xf:label>
                                    <xf:hint>
                                        <xsl:value-of select="ada:getMessage('edit')"/>
                                        <xsl:text>…</xsl:text>
                                    </xf:hint>
                                    <xf:action ev:event="DOMActivate">
                                        <xf:setvalue ref="instance('rename-instance')/@id" value="context()"/>
                                        <xf:setvalue ref="instance('rename-instance')/@newid" value="''"/>
                                        <xf:setvalue ref="instance('rename-instance')/@active" value="true()"/>
                                    </xf:action>
                                </xf:trigger>
                                <xxf:variable name="oldid" select="@id"/>
                                <xf:group ref="instance('rename-instance')[@active = 'true'][@id = $oldid]">
                                    <xhtml:div>
                                        <xf:textarea ref="@newid" mediatype="text/plain" incremental="true" class="plain">
                                            <xf:label appearance="minimal">
                                                <xsl:value-of select="ada:getMessage('new')"/>
                                            </xf:label>
                                        </xf:textarea>
                                    </xhtml:div>
                                    <xhtml:div>
                                        <xf:trigger>
                                            <xf:label>
                                                <xsl:value-of select="ada:getMessage('cancel')"/>
                                            </xf:label>
                                            <xf:action ev:event="DOMActivate">
                                                <xf:setvalue ref="context()/@active" value="false()"/>
                                            </xf:action>
                                        </xf:trigger>
                                        <xf:trigger>
                                            <xf:label>
                                                <xsl:value-of select="ada:getMessage('save')"/>
                                            </xf:label>
                                            <xf:send ev:event="DOMActivate" submission="rename-data" if=".[not(@id = @newid)][string-length(@newid) gt 0]"/>
                                        </xf:trigger>
                                    </xhtml:div>
                                </xf:group>
                            </xf:group>
                        </xhtml:td>
                    </xf:group>
                    <xhtml:td style="min-width: 400px; text-align: center;">
                        <xf:group ref=".[$editor][xxf:get-session-attribute('username') = @owner]">
                            <xhtml:span style="margin-right: 10px;">
                                <xf:trigger appearance="compact">
                                    <xf:label>
                                        <xsl:value-of select="ada:getMessage('edit')"/>
                                    </xf:label>
                                    <xf:action ev:event="DOMActivate">
                                        <!-- in newer projects there is a formname in the data, otherwise assume there is only 1 type of crud -->
                                        <xf:load resource="{{if (context()/@formName) then context()/@formName else ('{indexOf[1]/@shortName}')}}.xhtml?id={{context()/@id}}&amp;transactionId={{context()/@transactionRef}}&amp;transactionEffectiveDate={{context()/@transactionEffectiveDate}}" show="new"/>
                                    </xf:action>
                                </xf:trigger>
                            </xhtml:span>
                        </xf:group>
                        <xhtml:span style="{{if (.[$editor][xxf:get-session-attribute('username') = @owner]) then 'margin-left: 10px;' else ()}} margin-right: 10px;">
                            <xf:trigger appearance="compact">
                                <xf:label>
                                    <xsl:value-of select="ada:getMessage('overview')"/>
                                </xf:label>
                                <xf:action ev:event="DOMActivate">
                                    <xf:load resource="{{$ada-exist-external}}/modules/get-html.xquery?id={{context()/@id}}&amp;app={{context()/@app}}" show="new"/>
                                </xf:action>
                            </xf:trigger>
                        </xhtml:span>
                        <xf:group ref=".[$editor]">
                            <xhtml:span style="margin-right: 10px;">
                                <xf:trigger appearance="compact">
                                    <xf:label>
                                        <xsl:value-of select="ada:getMessage('duplicate')"/>
                                    </xf:label>
                                    <xf:action ev:event="DOMActivate">
                                        <!-- in newer projects there is a shortname in the data, otherwise assume there is only 1 type of crud -->
                                        <xf:load resource="{{if (context()/@formName) then context()/@formName else ('{indexOf[1]/@shortName}')}}.xhtml?id=new&amp;from={{context()/@id}}&amp;transactionId={{context()/@transactionRef}}&amp;transactionEffectiveDate={{context()/@transactionEffectiveDate}}" show="new"/>
                                    </xf:action>
                                </xf:trigger>
                            </xhtml:span>
                        </xf:group>
                        <xf:group ref=".[$editor][xxf:get-session-attribute('username') = @owner]">
                            <xhtml:span style="margin-right: 10px;">
                                <xf:trigger appearance="compact">
                                    <xf:label>
                                        <xsl:value-of select="ada:getMessage('Delete')"/>
                                    </xf:label>
                                    <xf:action ev:event="DOMActivate">
                                        <xf:setvalue ref="instance('data-id')" value="context()/@id"/>
                                        <xf:send submission="remove-data"/>
                                    </xf:action>
                                </xf:trigger>
                            </xhtml:span>
                        </xf:group>
                    </xhtml:td>
                </xhtml:tr>
            </xf:repeat>
        </xhtml:table>
    </xsl:template>
    <!-- Process concepts for bindings -->
    <xsl:template mode="doTheBindings" match="concept[@adaId | @dobId] | concept[valueDomain]">
        <xsl:param name="isMandatory" select="ada:isMandatory(.)" as="xs:boolean"/>
        <xsl:variable name="emptyTest">
            <xsl:if test="$isMandatory">. = '' or </xsl:if>
        </xsl:variable>
        <xsl:variable name="theBindings" as="element(xf:bind)*">
            <!--<xsl:if test="valueDomain/@type = 'identifier'">
                <!-\- @root is not required in the Schemas. It'll depend on the use case whether or not it is required. -\->
                <xf:bind nodeset="//*[@conceptId='{@id}']/@root">
                    <xsl:attribute name="constraint">string-length()=0 or matches(.,'^[0-2](\.(0|[1-9][0-9]*))*$')</xsl:attribute>
                </xf:bind>
            </xsl:if>-->
            <xsl:if test="@adaId | @dobId">
                <xf:bind nodeset="//*[@conceptId='{@id}']/@id">
                    <xsl:attribute name="constraint">
                        <xsl:value-of select="$emptyTest"/>
                        <xsl:text>. castable as xs:ID</xsl:text>
                    </xsl:attribute>
                </xf:bind>
            </xsl:if>
            <xf:bind nodeset="//*[@conceptId='{@id}']/@value">
                <!-- The XForms data types for allow empty string, the XSD ones don't -->
                <xsl:variable name="dataTypePrefix">
                    <xsl:choose>
                        <xsl:when test="$isMandatory">xs:</xsl:when>
                        <xsl:otherwise>xf:</xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:choose>
                    <!-- code is constrained by the valueSet -->
                    <xsl:when test="valueDomain/@type = 'complex'">
                        <xsl:attribute name="constraint">
                            <xsl:text>if (../@datatype = 'boolean') then (</xsl:text>
                            <xsl:value-of select="$emptyTest"/>
                            <xsl:text>. castable as xs:boolean) else </xsl:text>
                            <xsl:text>if (../@datatype = ('quantity', 'decimal', 'duration')) then (</xsl:text>
                            <xsl:value-of select="$emptyTest"/>
                            <xsl:text>. castable as xs:decimal) else </xsl:text>
                            <xsl:text>if (../@datatype = 'count') then (</xsl:text>
                            <xsl:value-of select="$emptyTest"/>
                            <xsl:text>. castable as xs:integer) else </xsl:text>
                            <xsl:text>if (../@datatype = 'reference') then (</xsl:text>
                            <xsl:value-of select="$emptyTest"/>
                            <xsl:text>.= /*//*[not(@hidden)][string-length(@id) gt 0]/@id or .=instance('summary-instance')//@compositeId) else </xsl:text>
                            <!--<xsl:text>if (../@datatype = 'date') then (. castable as xs:date) else </xsl:text>-->
                            <!--<xsl:text>if (../@datatype = 'datetime') then (. castable as xs:dateTime) else </xsl:text>-->
                            <xsl:text>(true())</xsl:text>
                        </xsl:attribute>
                    </xsl:when>
                    <xsl:when test="valueDomain/@type = 'count'">
                        <xsl:variable name="properties" select="valueDomain[@type = ('count')]/property[@minInclude | @maxInclude | @fixed]"/>
                        
                        <xsl:attribute name="type" select="concat($dataTypePrefix, 'nonNegativeInteger')"/>
                        
                        <xsl:if test="$properties">
                            <xsl:attribute name="constraint">
                                <xsl:text>. = ''</xsl:text>
                                <xsl:choose>
                                    <xsl:when test="$properties[@fixed]">
                                        <xsl:for-each select="$properties/@fixed">
                                            <xsl:text> or . = </xsl:text>
                                            <xsl:value-of select="."/>
                                        </xsl:for-each>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:text> or (</xsl:text>
                                        
                                        <xsl:for-each select="$properties/@minInclude | $properties/@maxInclude">
                                            <xsl:choose>
                                                <xsl:when test="name() = 'minInclude'">
                                                    <xsl:text>xs:nonNegativeInteger(.) ge </xsl:text>
                                                    <xsl:value-of select="."/>
                                                </xsl:when>
                                                <xsl:when test="name() = 'maxInclude'">
                                                    <xsl:text>xs:nonNegativeInteger(.) le </xsl:text>
                                                    <xsl:value-of select="."/>
                                                </xsl:when>
                                            </xsl:choose>
                                            
                                            <xsl:if test="position() != last()">
                                                <xsl:text> and </xsl:text>
                                            </xsl:if>
                                        </xsl:for-each>
                                        
                                        <xsl:text>)</xsl:text>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:attribute>
                        </xsl:if>
                    </xsl:when>
                    <!-- Test for IDREF -->
                    <xsl:when test="valueDomain/@type = 'string' and contains">
                        <xsl:attribute name="constraint">
                            <xsl:value-of select="$emptyTest"/>
                            <xsl:text>.= /*//*[not(@hidden)][@conceptId = '</xsl:text>
                            <xsl:value-of select="contains[1]/@ref"/>
                            <xsl:text>'][string-length(@id) gt 0]/@id or .=instance('summary-instance')//*[@conceptId = '</xsl:text>
                            <xsl:value-of select="contains[1]/@ref"/>
                            <xsl:text>']/@compositeId</xsl:text>
                        </xsl:attribute>
                    </xsl:when>
                    
                    <!-- VariableVagueDate = date, YYYY(-MM)?, T[+-]\d[YMD] -->
                    <xsl:when test="valueDomain[@type = 'string'][@originaltype = 'date'][property/@timeStampPrecision = ('Y', 'YM', 'YMD')]">
                        <xsl:attribute name="constraint">
                            <xsl:value-of select="$emptyTest"/>
                            <xsl:text>. castable as xs:date or matches(., '^\d{4}(-(0[1-9]|1[012]))?$') or (matches(., '^(T|DOB)([+\-](\d+(\.\d+)?[YMD]){1,3})?$') and (not(starts-with(., 'DOB')) or //*[@id = 'DOB']))</xsl:text>
                        </xsl:attribute>
                    </xsl:when>
                    <!-- VariableVagueDateTime = date, dateTime, YYYY(-MM)?, YYYY-MM-DDThh:mm, T([+-]\d[YMD]){0,3}({time})? -->
                    <xsl:when test="valueDomain[@type = 'string'][@originaltype = 'datetime'][property/@timeStampPrecision = ('Y', 'YM', 'YMD')]">
                        <xsl:attribute name="constraint">
                            <xsl:value-of select="$emptyTest"/>
                            <xsl:text>. castable as xs:dateTime or . castable as xs:date or matches(., '^\d{4}(-(0[1-9]|1[012]))?$') or matches(., '^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12]\d|3[01])T([01]\d|2[0-3])(:(0\d|[1-5]\d)(:(0\d|[1-5]\d))?)?$') or (matches(., '^(T|DOB)([+\-](\d+(\.\d+)?[YMD]){1,3})?(\{([01]\d|2[0-3]):(0\d|[1-5]\d)(:(0\d|[1-5]\d))?\})?$') and (not(starts-with(., 'DOB')) or //*[@id = 'DOB']))</xsl:text>
                        </xsl:attribute>
                    </xsl:when>
                    <xsl:when test="valueDomain[@type = 'string'][@originaltype = 'datetime'][property/@timeStampPrecision = ('YMDHM')]">
                        <xsl:attribute name="constraint">
                            <xsl:value-of select="$emptyTest"/>
                            <xsl:text>. castable as xs:dateTime or matches(., '^\d{4}(-(0[1-9]|1[012]))?$') or matches(., '^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12]\d|3[01])T([01]\d|2[0-3])(:(0\d|[1-5]\d)(:(0\d|[1-5]\d))?)?$') or (matches(., '^(T|DOB)([+\-](\d+(\.\d+)?[YMD]){1,3})?(\{([01]\d|2[0-3]):(0\d|[1-5]\d)(:(0\d|[1-5]\d))?\})?$') and (not(starts-with(., 'DOB')) or //*[@id = 'DOB']))</xsl:text>
                        </xsl:attribute>
                    </xsl:when>
                    <!-- DateYear = Just a year -->
                    <xsl:when test="valueDomain[@type = 'string'][@originaltype = ('date', 'datetime')][property/@timeStampPrecision = 'Y!']">
                        <xsl:attribute name="constraint">
                            <xsl:value-of select="$emptyTest"/>
                            <xsl:text>matches(., '^\d{4}$')</xsl:text>
                        </xsl:attribute>
                    </xsl:when>
                    <!-- DateYear = Just a year and month -->
                    <xsl:when test="valueDomain[@type = 'string'][@originaltype = ('date', 'datetime')][property/@timeStampPrecision = 'YM!']">
                        <xsl:attribute name="constraint">
                            <xsl:value-of select="$emptyTest"/>
                            <xsl:text>matches(., '^\d{4}-(0[1-9]|1[012])$')</xsl:text>
                        </xsl:attribute>
                    </xsl:when>
                    <!-- VariableDate = date, T[+-]\d[YMD] -->
                    <xsl:when test="valueDomain[@type = 'string'][@originaltype = 'date'] |
                                    valueDomain[@type = 'string'][@originaltype = ('date', 'datetime')][property/@timeStampPrecision = 'YMD!']">
                        <xsl:attribute name="constraint">
                            <xsl:value-of select="$emptyTest"/>
                            <xsl:text>. castable as xs:date or (matches(., '^(T|DOB)([+\-]\d+(\.\d+)?[YMD]){0,3}$') and (not(starts-with(., 'DOB')) or //*[@id = 'DOB']))</xsl:text>
                        </xsl:attribute>
                    </xsl:when>
                    <!-- VariableDateTime = dateTime, T([+-]\d[YMD]){0,3}({time})? -->
                    <xsl:when test="valueDomain[@type = 'string'][@originaltype = 'datetime']">
                        <xsl:attribute name="constraint">
                            <xsl:value-of select="$emptyTest"/>
                            <xsl:text>. castable as xs:dateTime or . castable as xs:date or (matches(., '^(T|DOB)([+\-](\d+(\.\d+)?[YMD]){1,3})?(\{([01]\d|2[0-3]):(0\d|[1-5]\d)(:(0\d|[1-5]\d))?\})?$') and (not(starts-with(., 'DOB')) or //*[@id = 'DOB']))</xsl:text>
                        </xsl:attribute>
                    </xsl:when>
                    <!--VariableVagueTime-->
                    <xsl:when test="valueDomain[@type = 'string'][@originaltype = 'time'][property/@timeStampPrecision = ('H', 'HM', 'HMS')]">
                        <xsl:attribute name="constraint">
                            <xsl:value-of select="$emptyTest"/>
                            <xsl:text>. castable as xs:time or matches(., '^([01]\d|2[0-3])(:(0\d|[1-5]\d)(:(0\d|[1-5]\d))?)?$') or (matches(., '^(T|DOB)([+\-](\d+(\.\d+)?[HMS]){1,3})?(\{:(0\d|[1-5]\d)(:(0\d|[1-5]\d))?\})?$') and (not(starts-with(., 'DOB')) or //*[@id = 'DOB']))</xsl:text>
                        </xsl:attribute>
                    </xsl:when>
                    <!--TimeHour-->
                    <xsl:when test="valueDomain[@type = 'string'][@originaltype = 'time'][property/@timeStampPrecision = 'H!']">
                        <xsl:attribute name="constraint">
                            <xsl:value-of select="$emptyTest"/>
                            <xsl:text>matches(., '^[01]\d|2[0-3]$')</xsl:text>
                        </xsl:attribute>
                    </xsl:when>
                    <!--TimeHourMinute-->
                    <xsl:when test="valueDomain[@type = 'string'][@originaltype = 'time'][property/@timeStampPrecision = 'HM!']">
                        <xsl:attribute name="constraint">
                            <xsl:value-of select="$emptyTest"/>
                            <xsl:text>matches(., '^([01]\d|2[0-3]):(0\d|[1-5]\d)$')</xsl:text>
                        </xsl:attribute>
                    </xsl:when>
                    <!--VariableTime-->
                    <xsl:when test="valueDomain[@type = 'string'][@originaltype = 'time']">
                        <xsl:attribute name="constraint">
                            <xsl:value-of select="$emptyTest"/>
                            <xsl:text>. castable as xs:time or (matches(., '^(T|DOB)([+\-](\d+(\.\d+)?[HMS]){1,3})?(\{:(0\d|[1-5]\d)(:(0\d|[1-5]\d))?\})?$') and (not(starts-with(., 'DOB')) or //*[@id = 'DOB']))</xsl:text>
                        </xsl:attribute>
                    </xsl:when>
                    <!-- ordinal -->
                    <xsl:when test="valueDomain/@type = 'ordinal'">
                        <xsl:attribute name="nodeset" select="concat('//*[@conceptId=''', @id, ''']/@ordinal')"/>
                        <xsl:attribute name="type" select="concat($dataTypePrefix, 'decimal')"/>
                    </xsl:when>
                    <!-- date or datetime restricted to date -->
                    <xsl:when test="valueDomain[@type = 'date'] | 
                                    valueDomain[property/@timeStampPrecision = 'YMD!'][@type = 'datetime']">
                        <xsl:attribute name="type" select="concat($dataTypePrefix, 'date')"/>
                    </xsl:when>
                    <!-- datetime -->
                    <xsl:when test="valueDomain/@type = 'datetime'">
                        <xsl:attribute name="type" select="concat($dataTypePrefix, 'dateTime')"/>
                    </xsl:when>
                    <!-- time -->
                    <xsl:when test="valueDomain/@type = 'time'">
                        <xsl:attribute name="type" select="concat($dataTypePrefix, 'time')"/>
                    </xsl:when>
                    <!-- quantity, decimal, duration -->
                    <xsl:when test="valueDomain/@type = ('quantity', 'decimal', 'duration')">
                        <!-- Empty decimals give errors, possibly Orbeon bug, so use constraint. -->
                        <xsl:variable name="properties" select="valueDomain[@type = ('quantity', 'decimal', 'duration')]/property[@minInclude | @maxInclude | @fractionDigits | @fixed]"/>
                        
                        <!--<xsl:attribute name="type" select="concat($dataTypePrefix, 'decimal')"/>-->
                        <xsl:attribute name="constraint">
                            <xsl:text>. = ''</xsl:text>
                            <xsl:choose>
                                <xsl:when test="$properties[@fixed]">
                                    <xsl:for-each select="$properties[@fixed]">
                                        <xsl:text> or (</xsl:text>
                                        <xsl:if test="@unit">
                                            <xsl:text>../@unit = '</xsl:text>
                                            <xsl:value-of select="replace(@unit, '''', '''''')"/>
                                            <xsl:text>' and </xsl:text>
                                        </xsl:if>
                                        <xsl:if test="@currency">
                                            <xsl:text>../@currency = '</xsl:text>
                                            <xsl:value-of select="replace(@currency, '''', '''''')"/>
                                            <xsl:text>' and </xsl:text>
                                        </xsl:if>
                                        <xsl:text>. = </xsl:text>
                                        <xsl:value-of select="@fixed"/>
                                        <xsl:text>)</xsl:text>
                                    </xsl:for-each>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:text> or (. castable as xs:decimal</xsl:text>
                                    <xsl:if test="$properties">
                                        <xsl:text> and (</xsl:text>
                                    </xsl:if>
                                    
                                    <xsl:for-each select="$properties">
                                        
                                        <xsl:text>(</xsl:text>
                                        <xsl:if test="@unit">
                                            <xsl:text>../@unit = '</xsl:text>
                                            <xsl:value-of select="replace(@unit, '''', '''''')"/>
                                            <xsl:text>' and </xsl:text>
                                        </xsl:if>
                                        <xsl:if test="@currency">
                                            <xsl:text>../@currency = '</xsl:text>
                                            <xsl:value-of select="replace(@currency, '''', '''''')"/>
                                            <xsl:text>' and </xsl:text>
                                        </xsl:if>
                                        <xsl:if test="@minInclude">
                                            <xsl:text>xs:decimal(.) ge </xsl:text>
                                            <xsl:value-of select="@minInclude"/>
                                            <xsl:if test="@maxInclude | @fractionDigits">
                                                <xsl:text> and </xsl:text>
                                            </xsl:if>
                                        </xsl:if>
                                        <xsl:if test="@maxInclude">
                                            <xsl:text>xs:decimal(.) le </xsl:text>
                                            <xsl:value-of select="@maxInclude"/>
                                            <xsl:if test="@fractionDigits">
                                                <xsl:text> and </xsl:text>
                                            </xsl:if>
                                        </xsl:if>
                                        <xsl:if test="@fractionDigits">
                                            <xsl:variable name="theFractionDigits" select="xs:integer(replace(@fractionDigits, '[^\d]', ''))"/>
                                            <xsl:variable name="exact" select="ends-with(@fractionDigits, '!')" as="xs:boolean"/>
                                            <xsl:variable name="max" select="ends-with(@fractionDigits, '.')" as="xs:boolean"/>
                                            
                                            <xsl:text>matches(.,'^-?\d+</xsl:text>
                                            <xsl:if test="$theFractionDigits gt 0">
                                                <xsl:text>(\.\d{</xsl:text>
                                                <!-- minimum 1 digit after the decimal point, so when max is 1 
                                                 digit, then this effectively means exactly 1 digit -->
                                                <xsl:if test="$max and $theFractionDigits gt 1">
                                                    <xsl:text>1,</xsl:text>
                                                </xsl:if>
                                                <xsl:value-of select="$theFractionDigits"/>
                                                <!-- some xpath eval engines don't like {n,} (upper undetermined) so 
                                                always make this fraction digit thing to {n,999} -->
                                                <xsl:if test="not($max or $exact)">
                                                    <xsl:text>,999</xsl:text>
                                                </xsl:if>
                                                <xsl:text>})</xsl:text>
                                                <xsl:if test="$max">
                                                    <xsl:text>?</xsl:text>
                                                </xsl:if>
                                            </xsl:if>
                                            <xsl:text>$')</xsl:text>
                                        </xsl:if>
                                        <xsl:text>)</xsl:text>
                                        
                                        <xsl:if test="position() != last()">
                                            <xsl:text> or </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                    
                                    <xsl:if test="$properties">
                                        <xsl:text>)</xsl:text>
                                    </xsl:if>
                                    <xsl:text>)</xsl:text>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:attribute>
                    </xsl:when>
                    <!-- non-Mandatory booleans are yes/no/unknown -->
                    <xsl:when test="valueDomain/@type = 'boolean'">
                        <xsl:choose>
                            <xsl:when test="$isMandatory">
                                <xsl:attribute name="type" select="concat($dataTypePrefix, 'boolean')"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:attribute name="constraint">
                                    <xsl:value-of select="$emptyTest"/>
                                    <xsl:text>. castable as xs:boolean</xsl:text>
                                </xsl:attribute>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <!-- blob -->
                    <xsl:when test="valueDomain/@type = 'blob'">
                        <xsl:attribute name="type">xs:base64Binary</xsl:attribute>
                    </xsl:when>
                    <!-- string, text, identifier -->
                    <xsl:when test="valueDomain/@type = ('string', 'text', 'identifier')">
                        <xsl:variable name="properties" select="valueDomain[@type = ('string', 'text', 'identifier')]/property[@minLength | @maxLength | @fixed]"/>
                        
                        <xsl:attribute name="type" select="concat($dataTypePrefix, 'string')"/>
                        
                        <xsl:if test="$properties">
                            <xsl:attribute name="constraint">
                                <xsl:text>. = ''</xsl:text>
                                <xsl:choose>
                                    <xsl:when test="$properties[@fixed]">
                                        <xsl:for-each select="$properties/@fixed">
                                            <xsl:text> or . = '</xsl:text>
                                            <xsl:value-of select="replace(., '''', '''''')"/>
                                            <xsl:text>'</xsl:text>
                                        </xsl:for-each>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:text> or (</xsl:text>
                                        
                                        <xsl:for-each select="$properties/@minLength | $properties/@maxLength">
                                            <xsl:choose>
                                                <xsl:when test="name() = 'minLength'">
                                                    <xsl:text>string-length() ge </xsl:text>
                                                    <xsl:value-of select="."/>
                                                </xsl:when>
                                                <xsl:when test="name() = 'maxLength'">
                                                    <xsl:text>string-length() le </xsl:text>
                                                    <xsl:value-of select="."/>
                                                </xsl:when>
                                            </xsl:choose>
                                            
                                            <xsl:if test="position() != last()">
                                                <xsl:text> and </xsl:text>
                                            </xsl:if>
                                        </xsl:for-each>
                                        
                                        <xsl:text>)</xsl:text>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:attribute>
                        </xsl:if>
                    </xsl:when>
                </xsl:choose>
            </xf:bind>
        </xsl:variable>
        <xsl:if test="$theBindings[@* except @nodeset]">
            <xsl:comment>
                <xsl:text>doTheBindings for: </xsl:text>
                <xsl:value-of select="implementation/@shortName"/>
            </xsl:comment>
            <xsl:copy-of select="$theBindings[@* except @nodeset]"/>
        </xsl:if>
        <xsl:apply-templates mode="doTheBindings"/>
    </xsl:template>
    <!-- Process concepts for bindings -->
    <xsl:template mode="doTheConditions" match="concept[@notPresentWhen]">
        <xsl:comment>
            <xsl:text>doTheConditions for: </xsl:text>
            <xsl:value-of select="implementation/@shortName"/>
        </xsl:comment>
        <xf:bind nodeset="//*[@conceptId='{@id}']">
            <xsl:attribute name="relevant">
                <xsl:text>not(</xsl:text>
                <xsl:value-of select="@notPresentWhen"/>
                <xsl:text>)</xsl:text>
            </xsl:attribute>
        </xf:bind>
    </xsl:template>
    <!-- Process concept groups -->
    <xsl:template mode="doTheForm" name="concept_group" match="concept[@type = 'group']">
        <xsl:comment>
            <xsl:text>doTheForm for concept_group: </xsl:text>
            <xsl:value-of select="implementation/@shortName"/>
        </xsl:comment>
        <xsl:choose>
            <xsl:when test="@skip">
                <xsl:call-template name="concept_content"/>
            </xsl:when>
            <xsl:when test="@widget = 'tab'">
                <fr:tab>
                    <fr:label>
                        <xsl:value-of select="name"/>
                    </fr:label>
                    <xsl:call-template name="concept_content"/>
                </fr:tab>
            </xsl:when>
            <xsl:when test="parent::dataset">
                <fr:case>
                    <xsl:if test="@initial = 'open'">
                        <xsl:attribute name="selected">true</xsl:attribute>
                    </xsl:if>
                    <fr:label>
                        <xsl:value-of select="name"/>
                    </fr:label>
                    <xsl:call-template name="concept_content"/>
                </fr:case>
            </xsl:when>
            <xsl:otherwise>
                <fr:accordion class="fr-accordion-lnf">
                    <fr:case>
                        <xsl:if test="@initial = 'open'">
                            <xsl:attribute name="selected">true</xsl:attribute>
                        </xsl:if>
                        <fr:label>
                            <xsl:value-of select="name"/>
                        </fr:label>
                        <xsl:call-template name="concept_content"/>
                    </fr:case>
                </fr:accordion>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <!-- Process concept content -->
    <xsl:template name="concept_content">
        <xsl:param name="isRequired" select="ada:isRequired(.)" as="xs:boolean"/>
        <xsl:param name="isMandatory" select="ada:isMandatory(.)" as="xs:boolean"/>
        <xsl:comment>
            <xsl:text>doTheForm for concept: </xsl:text>
            <xsl:value-of select="implementation/@shortName"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="@minimumMultiplicity"/>
            <xsl:text>..</xsl:text>
            <xsl:value-of select="@maximumMultiplicity"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="@conformance"/>
        </xsl:comment>
        <xsl:choose>
            <xsl:when test="@maximumMultiplicity = '0' or @skip">
                <xsl:comment>Skipping <xsl:value-of select="implementation/@shortName"/> <xsl:if test="@skip = 'cascade'"> with cascade</xsl:if></xsl:comment>
                <xsl:if test="@type = 'group' and @skip != 'cascade'">
                    <xf:group ref="*[@conceptId='{@id}']" appearance="full">
                        <xsl:apply-templates mode="doTheForm"/>
                    </xf:group>
                </xsl:if>
            </xsl:when>
            <!-- Process x..* concepts -->
            <xsl:when test="@maximumMultiplicity = '1'">
                <!-- This group contains all concepts with current id, including {concept-name}-start and @hidden. 
                    This allows us to hide everything when there is a notPresentWhen attribute. -->
                <xf:group ref="*[@conceptId='{@id}']" appearance="full">
                    <xhtml:div>
                        <xsl:attribute name="class">
                            <xsl:text>adaSingle</xsl:text>
                            <xsl:if test="@type = 'group'"> adaGroup</xsl:if>
                        </xsl:attribute>
                        <xsl:if test="@type = 'group'">
                            <xsl:call-template name="addGroupHeader">
                                <xsl:with-param name="context" select="concat('../', implementation/@shortName)"/>
                            </xsl:call-template>
                        </xsl:if>
                        <!-- This group will only display if the last item is @hidden (i.e., no 'real' concepts -->
                        <xf:group ref="../*[@conceptId='{@id}'][last()][@hidden]" appearance="full">
                            <xf:trigger>
                                <xf:label>[+] <xsl:value-of select="(name[@language = $language], name)[1]"/>
                                </xf:label>
                                <xf:insert ev:event="DOMActivate" nodeset="../*[@conceptId='{@id}']" at="last()" position="after" origin="instance('new')//*[@conceptId='{@id}'][not(@hidden)][1]"/>
                            </xf:trigger>
                        </xf:group>
                        <!-- This group will contain only 'real' concepts, not the start hook -->
                        <xf:group ref="../{implementation/@shortName}">
                            <xsl:if test="@type = 'item'">
                                <!-- When we are at R but not M, there will be a @value or a @nullFlavor, both 
                                    should have a label, but it cannot be directly on either. complex, code and ordinal 
                                    valueDoamin solve their nullFlavor in the valueDomain itself, so will have 
                                    their own label -->
                                <!--<xsl:if test="not($isMandatory) and $isRequired and not(.[contains]/valueDomain[@type = 'string'] | valueDomain[@type = ('complex', 'code', 'ordinal')])">-->
                                    <xhtml:span class="orbeon ada">
                                        <xhtml:label class="xforms-label">
                                            <xsl:value-of select="(name[@language = $language], name)[1]"/>
                                        </xhtml:label>
                                    </xhtml:span>
                                <!--</xsl:if>-->
                                <xsl:apply-templates mode="doTheForm"/>
                            </xsl:if>
                            <!-- For items, button after item, for groups, before -->
                            <!-- Trigger to delete or add items for 0..1 concepts -->
                            <xsl:if test="@minimumMultiplicity = '0'">
                                <xf:trigger>
                                    <xf:label>
                                        <xsl:text>[-] </xsl:text>
                                        <xsl:value-of select="(name[@language = $language], name)[1]"/>
                                    </xf:label>
                                    <xf:delete ev:event="DOMActivate" nodeset="../*[@conceptId='{@id}'][not(@hidden)]"/>
                                </xf:trigger>
                            </xsl:if>
                            <xsl:apply-templates mode="doTheForm" select="concept">
                                <xsl:sort select="@order"/>
                            </xsl:apply-templates>
                        </xf:group>
                    </xhtml:div>
                    <!--<xsl:if test="@adaId[. = 'true'] | @dobId[. = 'true']">
                        <xhtml:span>
                            <xsl:call-template name="addIdAttribute"/>
                        </xhtml:span>
                    </xsl:if>-->
                </xf:group>
            </xsl:when>
            <xsl:otherwise>
                <!-- @maximumMultiplicity gt '1' -->
                <!-- This group contains all concepts with current id, including {concept-name}-start and @hidden. 
                    This allows us to hide everything when there is a notPresentWhen attribute. -->
                <xf:group ref="*[@conceptId='{@id}']" appearance="full">
                    <xhtml:div>
                        <xsl:attribute name="class">
                            <xsl:text>adaMany</xsl:text>
                            <xsl:if test="@type = 'group'"> adaGroup</xsl:if>
                        </xsl:attribute>
                        <!-- The repeater will show all concepts but not the {concept-name}-start hook, which is there to insert new rows when all are deleted -->
                        <xf:repeat nodeset="../{implementation/@shortName}" appearance="full" id="repeat-{implementation/@shortName}-{(@id, @ref)[1]}">
                            <xhtml:div class="adaRow">
                                <xsl:if test="@type = 'item'">
                                    <xsl:call-template name="addMoveButtons"/>
                                    <!-- When we are at R but not M, there will be a @value or a @nullFlavor, both 
                                    should have a label, but it cannot be directly on either. complex, code and ordinal
                                    valueDoamin solve their nullFlavor in the valueDomain itself, so will have 
                                    their own label -->
                                    <!--<xsl:if test="not($isMandatory) and $isRequired and not(.[contains]/valueDomain[@type = 'string'] | valueDomain[@type = ('complex', 'code', 'ordinal')])">-->
                                        <xhtml:span class="orbeon ada">
                                            <xhtml:label class="xforms-label">
                                                <xsl:value-of select="(name[@language = $language], name)[1]"/>
                                            </xhtml:label>
                                        </xhtml:span>
                                    <!--</xsl:if>-->
                                    <xsl:apply-templates mode="doTheForm"/>
                                </xsl:if>
                                <!-- Add header for groups -->
                                <xsl:if test="@type = 'group'">
                                    <xsl:call-template name="addGroupHeader">
                                        <xsl:with-param name="context" select="'.'"/>
                                    </xsl:call-template>
                                </xsl:if>
                                <!-- For items, button after item, for groups, before -->
                                <xf:trigger>
                                    <xf:label>
                                        <xsl:text>[-] </xsl:text>
                                        <xsl:value-of select="(name[@language = $language], name)[1]"/>
                                    </xf:label>
                                    <!-- Clear the value, for 1..* last item -->
                                    <xf:action ev:event="DOMActivate">
                                        <xf:setvalue ref="@value"/>
                                    </xf:action>
                                    <!-- Delete current row. -->
                                    <xf:delete ev:event="DOMActivate" nodeset=".">
                                        <!-- For 1..*, don't delete the last one. -->
                                        <xsl:if test="@minimumMultiplicity = '1'">
                                            <xsl:attribute name="if">
                                                <xsl:text>count(../*[@conceptId='</xsl:text>
                                                <xsl:value-of select="@id"/>
                                                <xsl:text>'][not(@hidden)])&gt;1</xsl:text>
                                            </xsl:attribute>
                                        </xsl:if>
                                    </xf:delete>
                                </xf:trigger>
                                <xsl:if test="@type = 'group'">
                                    <xsl:apply-templates mode="doTheForm" select="concept">
                                        <xsl:sort select="@order"/>
                                    </xsl:apply-templates>
                                </xsl:if>
                            </xhtml:div>
                        </xf:repeat>
                        <xf:trigger>
                            <xf:label>[+] <xsl:value-of select="(name[@language = $language], name)[1]"/>
                            </xf:label>
                            <xf:insert ev:event="DOMActivate" nodeset="../*[@conceptId='{@id}']" at="last()" position="after" origin="instance('new')//*[@conceptId='{@id}'][not(@hidden)][1]"/>
                        </xf:trigger>
                    </xhtml:div>
                </xf:group>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template name="addGroupHeader">
        <xsl:param name="context" select="'.'" as="xs:string?"/>
        <xhtml:h3>
            <xsl:call-template name="addMoveButtons"/>
            <xf:output ref="'{replace((name[@language = $language], name)[1], '''', '''''')}'">
                <!-- not compatible with orbeon 2019 -->
                <!--<xf:label class="hidden">
                    <xsl:value-of select="(name[@language = $language], name)[1]"/>
                </xf:label>-->
                <xsl:if test="desc[.//text()[string-length() gt 0]][not(. = '-')]">
                    <xf:help>
                        <xsl:value-of select="(desc[@language = $language], desc)[1]"/>
                    </xf:help>
                </xsl:if>
            </xf:output>
            <xsl:if test="@adaId = 'true' or @dobId = 'true' or ancestor::view/@addComments = 'true'">
                <xhtml:span style="float:right;">
                    <xsl:call-template name="addIdAttribute">
                        <xsl:with-param name="context" select="$context"/>
                    </xsl:call-template>
                    <xsl:if test="ancestor::view/@addComments = 'true'">
                        <xf:textarea ref="@comment" mediatype="text/plain" incremental="true" class="plain">
                            <xf:label appearance="minimal">
                                <xsl:value-of select="ada:getMessage('comment')"/>
                            </xf:label>
                        </xf:textarea>
                    </xsl:if>
                </xhtml:span>
            </xsl:if>
        </xhtml:h3>
    </xsl:template>
    <xsl:template name="addMoveButtons">
        <xsl:if test="@maximumMultiplicity != '0'">
            <xsl:variable name="theElement" select="implementation/@shortName"/>
            <xf:group ref=".[not(@hidden)][following-sibling::{$theElement}]">
                <xf:trigger appearance="compact">
                    <xf:label>
                        <xhtml:img src="/img/move_task_down.png" alt=""/>
                    </xf:label>
                    <xf:hint>
                        <xsl:value-of select="ada:getMessage('move-group-one-down')"/>
                    </xf:hint>
                    <xf:action ev:event="DOMActivate">
                        <xxf:variable name="this" select="context()"/>
                        <xxf:variable name="other" select="context()/following-sibling::{$theElement}[1]"/>
                        <xf:insert nodeset="$other" position="after" origin="$this"/>
                        <xf:delete nodeset="$this"/>
                    </xf:action>
                </xf:trigger>
            </xf:group>
            <xf:group ref=".[not(@hidden)][preceding-sibling::{$theElement}]">
                <xf:trigger>
                    <xf:label>
                        <xhtml:img src="/img/move_task_up.png" alt=""/>
                    </xf:label>
                    <xf:hint>
                        <xsl:value-of select="ada:getMessage('move-group-one-up')"/>
                    </xf:hint>
                    <xf:action ev:event="DOMActivate">
                        <xxf:variable name="this" select="context()"/>
                        <xxf:variable name="other" select="context()/preceding-sibling::{$theElement}[1]"/>
                        <xf:insert nodeset="$other" position="before" origin="$this"/>
                        <xf:delete nodeset="$this"/>
                    </xf:action>
                </xf:trigger>
            </xf:group>
        </xsl:if>
    </xsl:template>
    <xsl:template name="addIdAttribute">
        <xsl:param name="context" select="'.'" as="xs:string?"/>
        <xsl:if test="ancestor-or-self::concept[1][@adaId[. = 'true'] | @dobId[. = 'true']]">
            <xf:textarea ref="{$context}/@id" mediatype="text/plain" incremental="true" class="plain">
                <xf:label appearance="minimal">
                    <xsl:value-of select="ancestor-or-self::concept[1]/(name[@language = $language], name)[1]"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="ada:getMessage('Id')"/>
                </xf:label>
                <xf:hint>
                    <xsl:value-of select="ancestor-or-self::concept[1]/(name[@language = $language], name)[1]"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="ada:getMessage('Id')"/>
                </xf:hint>
                <xf:alert>
                    <xsl:value-of select="ada:getMessage('value-must-be')"/>
                    <xsl:text>xs:ID - </xsl:text>
                    <xsl:value-of select="ada:getMessage('xsid-help')"/>
                </xf:alert>
            </xf:textarea>
            <xf:trigger ref="{$context}/@id">
                <xf:label>[-] <xsl:value-of select="ada:getMessage('Id')"/></xf:label>
                <xf:action ev:event="DOMActivate">
                    <xf:delete nodeset="context()"/>
                </xf:action>
            </xf:trigger>
            <xf:trigger ref="{$context}[not(@id)]">
                <xf:label>[+] <xsl:value-of select="ada:getMessage('Id')"/></xf:label>
                <xf:action ev:event="DOMActivate">
                    <xf:insert nodeset="context()/@*" origin="xxf:attribute('id', '')"/>
                </xf:action>
            </xf:trigger>
        </xsl:if>
    </xsl:template>
    <!-- Process concept items for xform inputs -->
    <xsl:template mode="doTheForm" name="concept_item" match="concept[@type = 'item']">
        <xsl:call-template name="concept_content"/>
    </xsl:template>
    <xsl:template name="typeIsQuantity" mode="doTheForm" match="valueDomain[@type = ('quantity', 'duration')]">
        <xsl:param name="isRequired" select="ada:isRequired(..)" as="xs:boolean"/>
        <xsl:param name="isMandatory" select="ada:isMandatory(..)" as="xs:boolean"/>
        
        <xsl:comment>Template typeIsQuantity</xsl:comment>
        <xf:textarea mediatype="text/plain" incremental="true" class="plain">
            <xsl:call-template name="addInputDetails">
                <xsl:with-param name="hideLabel" select="$isRequired and not($isMandatory)"/>
            </xsl:call-template>
            <!-- empty nullFlavor if we have a value -->
            <xf:delete ev:event="xforms-value-changed" if="context() != ''" ref="context()/../@nullFlavor"/>
            <xsl:if test="not(ada:isMandatory(..))">
                <xf:insert ev:event="xforms-value-changed" if="context() = ''" nodeset="context()/..[not(@nullFlavor)]/@*" origin="xxf:attribute('nullFlavor', '')"/>
            </xsl:if>
        </xf:textarea>
        <xsl:choose>
            <xsl:when test="count(property/@unit) > 1">
                <xf:select1 ref="@unit" class="adaUnit">
                    <xsl:if test="../@widget = 'radio'">
                        <xsl:attribute name="appearance" select="'full'"/>
                    </xsl:if>
                    <!-- Choice between available units -->
                    <!--<xf:label/>-->
                    <xsl:for-each select="property/@unit">
                        <xf:item>
                            <xf:label>
                                <xsl:value-of select="."/>
                            </xf:label>
                            <xf:value>
                                <xsl:value-of select="."/>
                            </xf:value>
                        </xf:item>
                    </xsl:for-each>
                </xf:select1>
            </xsl:when>
            <xsl:when test="count(property/@unit) = 1">
                <xf:output>
                    <xf:hint>
                        <xsl:value-of select="property/@unit"/>
                    </xf:hint>
                </xf:output>
            </xsl:when>
            <xsl:otherwise>
                <!-- no unit specified -->
                <xf:input ref="@unit">
                    <xf:hint>
                        <xsl:value-of select="ada:getMessage('unit')"/>
                    </xf:hint>
                </xf:input>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:call-template name="addNullFlavorSelection">
            <xsl:with-param name="isRequired" select="$isRequired" as="xs:boolean"/>
            <xsl:with-param name="isMandatory" select="$isMandatory" as="xs:boolean"/>
        </xsl:call-template>
        <xsl:call-template name="addIdAttribute"/>
    </xsl:template>
    <xsl:template name="typeIsIdentifier" mode="doTheForm" match="valueDomain[@type = 'identifier']">
        <xsl:param name="isRequired" select="ada:isRequired(..)" as="xs:boolean"/>
        <xsl:param name="isMandatory" select="ada:isMandatory(..)" as="xs:boolean"/>
        
        <xsl:comment>Template typeIsIdentifier</xsl:comment>
        <xf:textarea mediatype="text/plain" incremental="true" class="plain">
            <xsl:call-template name="addInputDetails">
                <xsl:with-param name="hideLabel" select="$isRequired and not($isMandatory)"/>
            </xsl:call-template>
            <!-- empty nullFlavor if we have a value -->
            <xf:delete ev:event="xforms-value-changed" if="context() != ''" ref="context()/../@nullFlavor"/>
            <xsl:if test="not(ada:isMandatory(..))">
                <xf:insert ev:event="xforms-value-changed" if="context() = ''" nodeset="context()/..[not(@nullFlavor)]/@*" origin="xxf:attribute('nullFlavor', '')"/>
            </xsl:if>
        </xf:textarea>
        <xsl:if test="count(../identifierAssociation) > 0">
            <!-- We create a drop-down with values -->
            <xf:select1 ref="@root">
                <!--<xsl:if test="../@widget = 'radio'">
                    <xsl:attribute name="appearance" select="'full'"/>
                </xsl:if>-->
                <xf:hint>
                    <xsl:value-of select="ada:getMessage('root')"/>
                </xf:hint>
                <xf:item>
                    <!--  For overrides in @root and/or conf=R, add empty choice -->
                    <xf:label>-----</xf:label>
                    <xf:value/>
                </xf:item>
                <xsl:for-each select="../identifierAssociation">
                    <xf:item>
                        <xf:label>
                            <xsl:value-of select="if (string-length(@refdisplay)>0) then @refdisplay else @ref"/>
                        </xf:label>
                        <xf:value>
                            <xsl:value-of select="@ref"/>
                        </xf:value>
                    </xf:item>
                </xsl:for-each>
            </xf:select1>
        </xsl:if>
        <xf:textarea ref="@root" mediatype="text/plain" incremental="true" class="plain">
            <xf:label appearance="minimal">
                <xsl:value-of select="ada:getMessage('root')"/>
            </xf:label>
            <xf:hint>
                <xsl:value-of select="ada:getMessage('root')"/>
            </xf:hint>
            <xf:help>
                <xsl:value-of select="ada:getMessage('root-help')"/>
            </xf:help>
            <xf:alert>
                <xsl:value-of select="ada:getMessage('value-must-be')"/>
                <xsl:value-of select="@type"/>
            </xf:alert>
        </xf:textarea>
        <xsl:call-template name="addNullFlavorSelection">
            <xsl:with-param name="isRequired" select="$isRequired" as="xs:boolean"/>
            <xsl:with-param name="isMandatory" select="$isMandatory" as="xs:boolean"/>
        </xsl:call-template>
        <xsl:call-template name="addIdAttribute"/>
    </xsl:template>
    <xsl:template name="typeIsCodeOrOrdinal" mode="doTheForm" match="valueDomain[@type = ('code', 'ordinal')]">
        <xsl:param name="isMandatory" select="ada:isMandatory(..)" as="xs:boolean"/>
        <xsl:param name="isRequired" select="ada:isRequired(..)" as="xs:boolean"/>
        
        <xsl:comment>Template typeIsCodeOrOrdinal</xsl:comment>
        <xsl:choose>
            <xsl:when test="count(../valueSet) = 0">
                <xsl:variable name="valueDropDown" select="../valueDomain/conceptList/(concept | exception)" as="element()*"/>
                <!-- No valueSet items available -->
                <xsl:choose>
                    <xsl:when test="$valueDropDown">
                        <!-- There is a valueDomain/conceptList/concept, pick those -->
                        <!-- We create a drop-down with values -->
                        <xf:select1>
                            <xsl:if test="../@widget = 'radio'">
                                <xsl:attribute name="appearance" select="'full'"/>
                            </xsl:if>
                            <!-- TODO: translation -->
                            <xsl:call-template name="addInputDetails">
                                <xsl:with-param name="hideLabel" select="false()"/>
                            </xsl:call-template>
                            <xsl:if test="not($isMandatory) or count($valueDropDown) = 1">
                                <xf:item>
                                    <!--  For codes conf=R or if there is just 1 choice, add empty choice. If there is just 1 choice then you cannot 'select' that choice so the @value cannot be populated -->
                                    <xf:label>-----</xf:label>
                                    <xf:value/>
                                </xf:item>
                            </xsl:if>
                            <xsl:for-each select="$valueDropDown">
                                <xf:item>
                                    <xf:label>
                                        <xsl:if test="@level castable as xs:integer">
                                            <!-- Orbeon will kill leading spaces so we insert this character to avoid that -->
                                            <xsl:if test="xs:integer(@level) gt 0">
                                                <xsl:text>-</xsl:text>
                                            </xsl:if>
                                            <xsl:value-of select="for $i in (1 to xs:integer(@level)) return '&#160;&#160;'"/>
                                        </xsl:if>
                                        <xsl:if test="@type = 'D'">
                                            <xsl:text>[</xsl:text>
                                            <xsl:value-of select="ada:getMessage('deprecated')"/>
                                            <xsl:text>] </xsl:text>
                                        </xsl:if>
                                        <xsl:if test="@type = 'A'">
                                            <xsl:text>[</xsl:text>
                                            <xsl:value-of select="ada:getMessage('abstract')"/>
                                            <xsl:text>] </xsl:text>
                                        </xsl:if>
                                        <xsl:value-of select="(name[@language = $language], name)[1]"/>
                                    </xf:label>
                                    <xf:value>
                                        <xsl:value-of select="(name[@language = $language], name)[1]"/>
                                    </xf:value>
                                </xf:item>
                            </xsl:for-each>
                        </xf:select1>
                        <xsl:if test="$valueDropDown[@type = 'D']">
                            <xsl:variable name="obsoletes" select="concat('(', string-join($valueDropDown[@type = 'D']/concat('''', replace((name[@language = $language], name)[1], '''', ''''''), ''''), ','), ')')"/>
                            <xf:group ref="@value[. = {$obsoletes}]">
                                <xhtml:span class="xforms-alert xforms-active"><xsl:value-of select="ada:getMessage('deprecated-warning')"/></xhtml:span>
                            </xf:group>
                        </xsl:if>
                        <xsl:if test="$valueDropDown[@type = 'A']">
                            <xsl:variable name="abstracts" select="concat('(', string-join($valueDropDown[@type = 'A']/concat('''', replace((name[@language = $language], name)[1], '''', ''''''), ''''), ','), ')')"/>
                            <xf:group ref="@value[. = {$abstracts}]">
                                <xhtml:span class="xforms-alert xforms-active"><xsl:value-of select="ada:getMessage('abstract-warning')"/></xhtml:span>
                            </xf:group>
                        </xsl:if>
                    </xsl:when>
                    <xsl:when test="count(../valueDomain/conceptList/(concept | exception)) = 0">
                        <!-- If no items are available, no conceptList of valueSet or valueDomain has been provided. This can be 
                            legal (i.e. with large external lists such as medication code, therefore we provide an 
                            open selection (user can manually enter codes) in those cases. 
                        -->
                        <xsl:comment> No code list available </xsl:comment>
                        <xf:textarea ref=".[not(@codeSystem = '{$theNullFlavor}')]/@code" appearance="minimal" mediatype="text/plain" incremental="true" class="plain">
                            <xf:label appearance="minimal">
                                <xsl:value-of select="../(name[@language = $language], name)[1]"/>
                            </xf:label>
                            <xf:help>
                                <xsl:value-of select="../(desc[@language = $language], desc)[1]"/>
                            </xf:help>
                            <xf:hint>
                                <xsl:value-of select="ada:getMessage('code')"/>
                            </xf:hint>
                            <!-- empty @value when @code gets a value -->
                            <xf:setvalue ev:event="xforms-value-changed" if="context() != ''" ref="context()/../@value" value="''"/>
                        </xf:textarea>
                        
                        <xsl:call-template name="addNullFlavorSelection">
                            <xsl:with-param name="show-label" select="true()" as="xs:boolean"/>
                            <xsl:with-param name="select-label" select="../(name[@language = $language], name)[1]"/>
                            <xsl:with-param name="select-ref">.[@codeSystem = '<xsl:value-of select="$theNullFlavor"/>']/@code</xsl:with-param>
                            <xsl:with-param name="isRequired" select="true()" as="xs:boolean"/>
                            <xsl:with-param name="isMandatory" select="false()" as="xs:boolean"/>
                        </xsl:call-template>
                        
                        <xf:textarea ref="@codeSystem" appearance="minimal" mediatype="text/plain" incremental="true" class="plain">
                            <xf:label appearance="minimal">
                                <xsl:value-of select="ada:getMessage('codesystem')"/>
                            </xf:label>
                            <xf:hint>
                                <xsl:value-of select="ada:getMessage('codesystem')"/>
                            </xf:hint>
                            <xf:help>
                                <xsl:value-of select="ada:getMessage('no-code-list-available')"/>
                            </xf:help>
                        </xf:textarea>
                        <!--<xf:group ref=".[not(@codeSystem = '{$theNullFlavor}')]">-->
                        <xf:textarea ref="@displayName" appearance="minimal" mediatype="text/plain" incremental="true" class="plain">
                            <xf:label appearance="minimal">
                                <xsl:value-of select="ada:getMessage('displayname')"/>
                            </xf:label>
                            <xf:hint>
                                <xsl:value-of select="ada:getMessage('displayname')"/>
                            </xf:hint>
                            <xf:help>
                                <xsl:value-of select="ada:getMessage('no-code-list-available')"/>
                            </xf:help>
                        </xf:textarea>
                        <!--</xf:group>-->
                        <xsl:if test="@type = ('ordinal')">
                            <xf:textarea ref="@ordinal" appearance="minimal" mediatype="text/plain" incremental="true" class="plain short-number">
                                <xf:label appearance="minimal">
                                    <xsl:value-of select="ada:getMessage('ordinal')"/>
                                </xf:label>
                                <xf:alert>
                                    <xsl:value-of select="ada:getMessage('integer-only')"/>
                                </xf:alert>
                                <xf:hint>
                                    <xsl:value-of select="ada:getMessage('ordinal')"/>
                                </xf:hint>
                            </xf:textarea>
                        </xsl:if>
                        <xf:group ref=".[@code = 'OTH'][@codeSystem = '{$theNullFlavor}']">
                            <xf:textarea ref="@originalText" appearance="minimal" mediatype="text/plain" incremental="true" class="plain">
                                <xf:label appearance="minimal">
                                    <xsl:value-of select="ada:getMessage('originaltext')"/>
                                </xf:label>
                                <xf:hint>
                                    <xsl:value-of select="ada:getMessage('originaltext')"/>
                                </xf:hint>
                                <xf:help>
                                    <xsl:value-of select="ada:getMessage('no-code-available')"/>
                                </xf:help>
                            </xf:textarea>
                        </xf:group>
                        <xf:textarea ref="@codeSystemName" appearance="minimal" mediatype="text/plain" incremental="true" class="plain">
                            <xf:label appearance="minimal">
                                <xsl:value-of select="ada:getMessage('codesystemname')"/>
                            </xf:label>
                            <xf:hint>
                                <xsl:value-of select="ada:getMessage('codesystemname')"/>
                            </xf:hint>
                        </xf:textarea>
                        <xf:textarea ref="@codeSystemVersion" appearance="minimal" mediatype="text/plain" incremental="true" class="plain">
                            <xf:label appearance="minimal">
                                <xsl:value-of select="ada:getMessage('codesystemversion')"/>
                            </xf:label>
                            <xf:hint>
                                <xsl:value-of select="ada:getMessage('codesystemversion')"/>
                            </xf:hint>
                        </xf:textarea>
                        <xf:select1 ref="@preferred" appearance="minimal" class="auto-width">
                            <xf:label appearance="minimal" class="hidden">
                                <xsl:value-of select="ada:getMessage('preferred')"/>
                            </xf:label>
                            <xf:hint>
                                <xsl:value-of select="ada:getMessage('preferred')"/>
                            </xf:hint>
                            <xf:help>
                                <xsl:value-of select="ada:getMessage('preferred')"/>
                            </xf:help>
                            <xf:item>
                                <xf:label>----</xf:label>
                                <xf:value ref="''"/>
                            </xf:item>
                            <xf:item>
                                <xf:label>
                                    <xsl:value-of select="ada:getMessage('preferredvalue')"/>
                                </xf:label>
                                <xf:value ref="'true'"/>
                            </xf:item>
                            <xf:item>
                                <xf:label>
                                    <xsl:value-of select="ada:getMessage('alternativevalue')"/>
                                </xf:label>
                                <xf:value ref="'false'"/>
                            </xf:item>
                        </xf:select1>
                    </xsl:when>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="count(../valueSet) gt 0">
                <!-- We create a drop-down with values -->
                <xsl:variable name="nullFlavorOther" select="../valueSet/conceptList/(concept | exception)[@code = 'OTH'][@codeSystem = $theNullFlavor]/@localId"/>
                <xsl:variable name="valueDropDown" as="element()*">
                    <xsl:choose>
                        <xsl:when test="count(../valueSet/conceptList/(concept | exception)) > 0">
                            <xsl:copy-of select="../valueSet/conceptList/(concept | exception)"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:copy-of select="../valueDomain/conceptList/(concept | exception)"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:if test="$valueDropDown">
                    <xf:select1>
                        <xsl:if test="../@widget = 'radio'">
                            <xsl:attribute name="appearance" select="'full'"/>
                        </xsl:if>
                        <xsl:call-template name="addInputDetails">
                            <xsl:with-param name="hideLabel" select="false()"/>
                        </xsl:call-template>
                        <xsl:if test="not($isMandatory) or ../valueSet/completeCodeSystem or ../valueSet/conceptList/include or count($valueDropDown) = 1">
                            <xf:item>
                                <!--  For codes conf=R or if there is just 1 choice, add empty choice. If there is just 1 choice then you cannot 'select' that choice so the @value cannot be populated -->
                                <xf:label>-----</xf:label>
                                <xf:value/>
                            </xf:item>
                        </xsl:if>
                        <xsl:for-each select="$valueDropDown">
                            <xf:item>
                                <xf:label>
                                    <xsl:if test="@level castable as xs:integer">
                                        <!-- Orbeon will kill leading spaces so we insert this character to avoid that -->
                                        <xsl:if test="xs:integer(@level) gt 0">
                                            <xsl:text>-</xsl:text>
                                        </xsl:if>
                                        <xsl:value-of select="for $i in (1 to xs:integer(@level)) return '&#160;&#160;'"/>
                                    </xsl:if>
                                    <xsl:if test="@type = 'D'">
                                        <xsl:text>[</xsl:text>
                                        <xsl:value-of select="ada:getMessage('deprecated')"/>
                                        <xsl:text>] </xsl:text>
                                    </xsl:if>
                                    <xsl:if test="@type = 'A'">
                                        <xsl:text>[</xsl:text>
                                        <xsl:value-of select="ada:getMessage('abstract')"/>
                                        <xsl:text>] </xsl:text>
                                    </xsl:if>
                                    <xsl:value-of select="(name[@language = $language], name)[1]"/>
                                </xf:label>
                                <xf:value>
                                    <xsl:value-of select="(@localId, name)[1]"/>
                                </xf:value>
                            </xf:item>
                        </xsl:for-each>
                        <xf:action ev:event="xforms-value-changed" if="not(context() = '')">
                            <xf:setvalue ref="context()/../@code" value="''"/>
                            <xf:setvalue ref="context()/../@codeSystem" value="''"/>
                            <xf:setvalue ref="context()/../@codeSystemName" value="''"/>
                            <xf:setvalue ref="context()/../@codeSystemVersion" value="''"/>
                            <xf:setvalue ref="context()/../@displayName" value="''"/>
                            <xf:setvalue ref="context()/../@ordinal" value="''"/>
                            <xf:setvalue ref="context()/../@originalText" value="''"/>
                        </xf:action>
                        <xf:action ev:event="xforms-value-changed" if="context() = ''">
                            <xf:setvalue ref="context()/../@preferred" value="''"/>
                        </xf:action>
                    </xf:select1>
                    
                    <xsl:if test="$valueDropDown[@type = 'D']">
                        <xsl:variable name="obsoletes" select="concat('(', string-join($valueDropDown[@type = 'D']/concat('''', replace((@localId, name)[1], '''', ''''''), ''''), ','), ')')"/>
                        <xf:group ref="@value[. = {$obsoletes}]">
                            <xhtml:span class="xforms-alert xforms-active"><xsl:value-of select="ada:getMessage('deprecated-warning')"/></xhtml:span>
                        </xf:group>
                    </xsl:if>
                    <xsl:if test="$valueDropDown[@type = 'A']">
                        <xsl:variable name="abstracts" select="concat('(', string-join($valueDropDown[@type = 'A']/concat('''', replace((@localId, name)[1], '''', ''''''), ''''), ','), ')')"/>
                        <xf:group ref="@value[. = {$abstracts}]">
                            <xhtml:span class="xforms-alert xforms-active"><xsl:value-of select="ada:getMessage('abstract-warning')"/></xhtml:span>
                        </xf:group>
                    </xsl:if>
                </xsl:if>
                <xsl:if test="count(../valueSet/conceptList/(concept | exception)) = 0 or ../valueSet/completeCodeSystem or ../valueSet/conceptList/include">
                    <!-- 2020-03-13 AH Deactivated this group because in some circumstances it prohibits seeing the code/codeSystem etc. -->
                    <xf:group ref=".[string-length(@value) = 0]">
                        <xsl:choose>
                            <xsl:when test="../valueSet/conceptList/(concept | exception)[@codeSystem = $theNullFlavor]">
                                <xf:textarea ref="@code" appearance="minimal" mediatype="text/plain" incremental="true" class="plain">
                                    <!--<xsl:choose>
                                        <xsl:when test="count($valueDropDown) = 0">
                                            <!-\- label will have been done on first part -\->
                                            <xf:label>
                                                <xsl:value-of select="../(name[@language = $language], name)[1]"/>
                                            </xf:label>
                                        </xsl:when>
                                        <xsl:otherwise>-->
                                    <xf:label appearance="minimal">
                                        <xsl:value-of select="ada:getMessage('code')"/>
                                    </xf:label>
                                    <!--</xsl:otherwise>
                                    </xsl:choose>-->
                                    <xf:help>
                                        <xsl:value-of select="../(desc[@language = $language], desc)[1]"/>
                                    </xf:help>
                                    <xf:hint>
                                        <xsl:value-of select="ada:getMessage('code')"/>
                                    </xf:hint>
                                    <!-- empty @value when @code gets a value -->
                                    <xf:setvalue ev:event="xforms-value-changed" if="context() != ''" ref="context()/../@value" value="''"/>
                                </xf:textarea>
                            </xsl:when>
                            <xsl:otherwise>
                                <xf:textarea ref=".[not(@codeSystem = '{$theNullFlavor}')]/@code" appearance="minimal" mediatype="text/plain" incremental="true" class="plain">
                                    <!--<xsl:choose>
                                        <xsl:when test="count($valueDropDown) = 0">
                                            <!-\- label will have been done on first part -\->
                                            <xf:label>
                                                <xsl:value-of select="../(name[@language = $language], name)[1]"/>
                                            </xf:label>
                                        </xsl:when>
                                        <xsl:otherwise>-->
                                    <xf:label appearance="minimal">
                                        <xsl:value-of select="ada:getMessage('code')"/>
                                    </xf:label>
                                    <!--</xsl:otherwise>
                                    </xsl:choose>-->
                                    <xf:help>
                                        <xsl:value-of select="../(desc[@language = $language], desc)[1]"/>
                                    </xf:help>
                                    <xf:hint>
                                        <xsl:value-of select="ada:getMessage('code')"/>
                                    </xf:hint>
                                    <!-- empty @value when @code gets a value -->
                                    <xf:setvalue ev:event="xforms-value-changed" if="context() != ''" ref="context()/../@value" value="''"/>
                                </xf:textarea>
                                
                                <xsl:call-template name="addNullFlavorSelection">
                                    <xsl:with-param name="show-label" select="true()" as="xs:boolean"/>
                                    <xsl:with-param name="select-label" select="../(name[@language = $language], name)[1]"/>
                                    <xsl:with-param name="select-ref">.[@codeSystem = '<xsl:value-of select="$theNullFlavor"/>']/@code</xsl:with-param>
                                    <xsl:with-param name="isRequired" select="true()" as="xs:boolean"/>
                                    <xsl:with-param name="isMandatory" select="false()" as="xs:boolean"/>
                                </xsl:call-template>
                            </xsl:otherwise>
                        </xsl:choose>
                        
                        <xsl:choose>
                            <xsl:when test="../valueSet/sourceCodeSystem">
                                <xf:select1 ref="@codeSystem">
                                    <xf:help>
                                        <xsl:value-of select="ada:getMessage('codesystem')"/>
                                    </xf:help>
                                    <xsl:if test="not($isMandatory)">
                                        <xf:item>
                                            <!--  For codes conf=R, add empty choice -->
                                            <xf:label>-----</xf:label>
                                            <xf:value/>
                                        </xf:item>
                                    </xsl:if>
                                    <xsl:for-each-group select="../valueSet/sourceCodeSystem" group-by="@id">
                                        <xsl:sort select="@identifierName"/>
                                        <xf:item>
                                            <xf:label>
                                                <xsl:choose>
                                                    <xsl:when test="current-group()/@identifierName[not(normalize-space(.) = '')]">
                                                        <xsl:value-of select="(current-group()/@identifierName[not(normalize-space(.) = '')])[1]"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="current-group()[1]/@id"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xf:label>
                                            <xf:value>
                                                <xsl:value-of select="current-group()[1]/@id"/>
                                            </xf:value>
                                        </xf:item>
                                    </xsl:for-each-group>
                                    <xsl:if test="$isRequired and not(../valueSet/sourceCodeSystem[@id = $theNullFlavor])">
                                        <xf:item>
                                            <!--  For codes conf=R, add NullFlavor choice -->
                                            <xf:label>NullFlavor</xf:label>
                                            <xf:value>
                                                <xsl:value-of select="$theNullFlavor"/>
                                            </xf:value>
                                        </xf:item>
                                    </xsl:if>
                                    <xf:setvalue ev:event="xforms-value-changed" ref="context()/../@codeSystemName" value="''"/>
                                    <xf:setvalue ev:event="xforms-value-changed" ref="context()/../@codeSystemVersion" value="''"/>
                                </xf:select1>
                            </xsl:when>
                            <xsl:otherwise>
                                <xf:textarea ref="@codeSystem" appearance="minimal" mediatype="text/plain" incremental="true" class="plain">
                                    <xf:label appearance="minimal">
                                        <xsl:value-of select="ada:getMessage('codesystem')"/>
                                    </xf:label>
                                    <xf:hint>
                                        <xsl:value-of select="ada:getMessage('codesystem')"/>
                                    </xf:hint>
                                    <xf:help>
                                        <xsl:value-of select="ada:getMessage('no-code-list-available')"/>
                                    </xf:help>
                                </xf:textarea>
                            </xsl:otherwise>
                        </xsl:choose>
                        <!--<xf:group ref=".[not(@codeSystem = '{$theNullFlavor}')]">-->
                        <xf:textarea ref="@displayName" appearance="minimal" mediatype="text/plain" incremental="true" class="plain">
                            <xf:label appearance="minimal">
                                <xsl:value-of select="ada:getMessage('displayname')"/>
                            </xf:label>
                            <xf:hint>
                                <xsl:value-of select="ada:getMessage('displayname')"/>
                            </xf:hint>
                            <xf:help>
                                <xsl:value-of select="ada:getMessage('no-code-list-available')"/>
                            </xf:help>
                        </xf:textarea>
                        <!--</xf:group>-->
                        <xsl:if test="@type = ('ordinal')">
                            <xf:textarea ref="@ordinal" appearance="minimal" mediatype="text/plain" incremental="true" class="plain short-number">
                                <xf:label appearance="minimal">
                                    <xsl:value-of select="ada:getMessage('ordinal')"/>
                                </xf:label>
                                <xf:alert>
                                    <xsl:value-of select="ada:getMessage('integer-only')"/>
                                </xf:alert>
                                <xf:hint>
                                    <xsl:value-of select="ada:getMessage('ordinal')"/>
                                </xf:hint>
                            </xf:textarea>
                        </xsl:if>
                        <xf:textarea ref="@codeSystemName" appearance="minimal" mediatype="text/plain" incremental="true" class="plain">
                            <xf:label appearance="minimal">
                                <xsl:value-of select="ada:getMessage('codesystemname')"/>
                            </xf:label>
                            <xf:hint>
                                <xsl:value-of select="ada:getMessage('codesystemname')"/>
                            </xf:hint>
                        </xf:textarea>
                        <xf:textarea ref="@codeSystemVersion" appearance="minimal" mediatype="text/plain" incremental="true" class="plain">
                            <xf:label appearance="minimal">
                                <xsl:value-of select="ada:getMessage('codesystemversion')"/>
                            </xf:label>
                            <xf:hint>
                                <xsl:value-of select="ada:getMessage('codesystemversion')"/>
                            </xf:hint>
                        </xf:textarea>
                    </xf:group>
                    <xf:select1 ref="@preferred" appearance="minimal" class="auto-width">
                        <xf:label appearance="minimal" class="hidden">
                            <xsl:value-of select="ada:getMessage('preferred')"/>
                        </xf:label>
                        <xf:hint>
                            <xsl:value-of select="ada:getMessage('preferred-hint')"/>
                        </xf:hint>
                        <xf:help>
                            <xsl:value-of select="ada:getMessage('preferred-hint')"/>
                        </xf:help>
                        <xf:item>
                            <xf:label>----</xf:label>
                            <xf:value ref="''"/>
                        </xf:item>
                        <xf:item>
                            <xf:label>
                                <xsl:value-of select="ada:getMessage('preferredvalue')"/>
                            </xf:label>
                            <xf:value ref="'true'"/>
                        </xf:item>
                        <xf:item>
                            <xf:label>
                                <xsl:value-of select="ada:getMessage('alternativevalue')"/>
                            </xf:label>
                            <xf:value ref="'false'"/>
                        </xf:item>
                    </xf:select1>
                </xsl:if>
                <xsl:if test="$nullFlavorOther or count(../valueSet/conceptList/(concept | exception)) = 0 or ../valueSet/completeCodeSystem or ../valueSet/conceptList/include">
                    <xf:group ref=".[@code = 'OTH'][@codeSystem = '{$theNullFlavor}']{if ($nullFlavorOther) then (concat(' | .[@value = (''', string-join($nullFlavorOther, ''','''), ''')]')) else ()}">
                        <xf:textarea ref="@originalText" appearance="minimal" mediatype="text/plain" incremental="true" class="plain">
                            <xf:label appearance="minimal">
                                <xsl:value-of select="ada:getMessage('originaltext')"/>
                            </xf:label>
                            <xf:hint>
                                <xsl:value-of select="ada:getMessage('originaltext')"/>
                            </xf:hint>
                            <xf:help>
                                <xsl:value-of select="ada:getMessage('no-code-available')"/>
                            </xf:help>
                        </xf:textarea>
                    </xf:group>
                </xsl:if>
            </xsl:when>
        </xsl:choose>
        <xsl:call-template name="addIdAttribute"/>
    </xsl:template>
    <xsl:template name="typeIsText" mode="doTheForm" match="valueDomain[@type = 'text']">
        <xsl:param name="isRequired" select="ada:isRequired(..)" as="xs:boolean"/>
        <xsl:param name="isMandatory" select="ada:isMandatory(..)" as="xs:boolean"/>
        <xsl:comment>
            <xsl:text>Template typeIsText</xsl:text>
        </xsl:comment>
        <xf:textarea mediatype="text/html" incremental="true">
            <xsl:call-template name="addInputDetails">
                <xsl:with-param name="hideLabel" select="$isRequired and not($isMandatory)"/>
            </xsl:call-template>
            <!-- empty nullFlavor if we have a value -->
            <xf:delete ev:event="xforms-value-changed" if="context() != ''" ref="context()/../@nullFlavor"/>
            <xsl:if test="not(ada:isMandatory(..))">
                <xf:insert ev:event="xforms-value-changed" if="context() = ''" nodeset="context()/..[not(@nullFlavor)]/@*" origin="xxf:attribute('nullFlavor', '')"/>
            </xsl:if>
        </xf:textarea>
        <xsl:call-template name="addNullFlavorSelection">
            <xsl:with-param name="isRequired" select="$isRequired" as="xs:boolean"/>
            <xsl:with-param name="isMandatory" select="$isMandatory" as="xs:boolean"/>
        </xsl:call-template>
        <xsl:call-template name="addIdAttribute"/>
    </xsl:template>
    <xsl:template name="typeIsBoolean" mode="doTheForm" match="valueDomain[@type = 'boolean']">
        <xsl:param name="isRequired" select="ada:isRequired(..)" as="xs:boolean"/>
        <xsl:param name="isMandatory" select="ada:isMandatory(..)" as="xs:boolean"/>
        <xf:select1 appearance="full">
            <xsl:call-template name="addInputDetails">
                <xsl:with-param name="hideLabel" select="$isRequired and not($isMandatory)"/>
            </xsl:call-template>
            <xf:item>
                <xf:label>
                    <xsl:value-of select="ada:getMessage('yes')"/>
                </xf:label>
                <xf:value>true</xf:value>
            </xf:item>
            <xf:item>
                <xf:label>
                    <xsl:value-of select="ada:getMessage('no')"/>
                </xf:label>
                <xf:value>false</xf:value>
            </xf:item>
            <xsl:if test="not($isMandatory)">
                <xf:item>
                    <!--  For boolean conf=R, add empty choice to support going to nullFlavor -->
                    <xf:label><xf:output ref="if (. = ('true', 'false')) then '---' else concat('-',.,'-')"/></xf:label>
                    <xf:value/>
                </xf:item>
            </xsl:if>
            <!-- Now that we support nullFlavor, we don't need this anymore -->
            <!--<xsl:if test="not(../@conformance = 'M' or ../@isMandatory = 'true')">
                <xf:item>
                    <xf:label>
                        <xsl:value-of select="ada:getMessage('unknown')"/>
                    </xf:label>
                    <xf:value>UNK</xf:value>
                </xf:item>
            </xsl:if>-->
            <!-- empty nullFlavor if we have a value -->
            <xf:delete ev:event="xforms-value-changed" if="context() != ''" ref="context()/../@nullFlavor"/>
            <xsl:if test="not(ada:isMandatory(..))">
                <xf:insert ev:event="xforms-value-changed" if="context() = ''" nodeset="context()/..[not(@nullFlavor)]/@*" origin="xxf:attribute('nullFlavor', '')"/>
            </xsl:if>
        </xf:select1>
        <xsl:call-template name="addNullFlavorSelection">
            <xsl:with-param name="isRequired" select="$isRequired" as="xs:boolean"/>
            <xsl:with-param name="isMandatory" select="$isMandatory" as="xs:boolean"/>
        </xsl:call-template>
        <xsl:call-template name="addIdAttribute"/>
    </xsl:template>
    <xsl:template name="typeIsComplex" mode="doTheForm" match="valueDomain[@type = 'complex']">
        <xsl:param name="isMandatory" select="ada:isMandatory(..)" as="xs:boolean"/>
        <xsl:param name="isRequired" select="ada:isRequired(..)" as="xs:boolean"/>
        
        <xsl:comment>Template typeIsComplex</xsl:comment>
        <xf:select1 ref="@datatype" appearance="minimal" class="auto-width">
            <xf:label class="hidden">
                <xsl:value-of select="../(name[@language = $language], name)[1]"/>
            </xf:label>
            <xf:help>
                <xsl:value-of select="../(desc[@language = $language], desc)[1]"/>
            </xf:help>
            <xf:hint>
                <xsl:value-of select="ada:getMessage('datatype')"/>
            </xf:hint>
            <xf:item>
                <xf:label>-<xsl:value-of select="ada:getMessage('unknown')"/>-</xf:label>
                <xf:value/>
            </xf:item>
            <xf:itemset nodeset="instance('datatypeSelection')/enumeration">
                <xf:label ref="label[@language = '{$language}']"/>
                <xf:value ref="@value"/>
            </xf:itemset>
            <xf:action ev:event="xforms-value-changed" if="context() = 'identifier'">
                <xf:insert nodeset="context()/../@*" origin="xxf:attribute('value', '')" if="not(context()/../@value)"/>
                <xf:insert nodeset="context()/../@*" origin="xxf:attribute('root', '')" if="not(context()/../@root)"/>
                <!--<xf:insert nodeset="context()/../@*" origin="xxf:attribute('nullFlavor', '')" if="not(@nullFlavor) and string-length(@value) = 0"/>-->
                <xf:delete nodeset="context()/../(@unit | @ordinal | @code | @codeSystem | @codeSystemName | @codeSystemVersion | @displayName | @preferred | @originalText)"/>
                <!--<xf:action xxf:iterate="context()/../(@unit | @ordinal | @code | @codeSystem | @codeSystemName | @codeSystemVersion | @displayName | @preferred | @originalText)">
                    <xf:setvalue ref="context()" value="''"/>
                </xf:action>-->
            </xf:action>
            <xf:action ev:event="xforms-value-changed" if="context() = 'quantity'">
                <xf:insert nodeset="context()/../@*" origin="xxf:attribute('value', '')" if="not(context()/../@value)"/>
                <xf:insert nodeset="context()/../@*" origin="xxf:attribute('unit', '')" if="not(context()/../@unit)"/>
                <!--<xf:insert nodeset="context()/../@*" origin="xxf:attribute('nullFlavor', '')" if="not(@nullFlavor) and string-length(@value) = 0"/>-->
                <xf:delete nodeset="context()/../(@root | @ordinal | @code | @codeSystem | @codeSystemName | @codeSystemVersion | @displayName | @preferred | @originalText)"/>
                <!--<xf:action xxf:iterate="context()/../(@root | @ordinal | @code | @codeSystem | @codeSystemName | @codeSystemVersion | @displayName | @preferred | @originalText)">
                    <xf:setvalue ref="context()" value="''"/>
                </xf:action>-->
            </xf:action>
            <xf:action ev:event="xforms-value-changed" if="context() = 'code'">
                <xf:setvalue ref="context()/../@value" value="''"/>
                <xf:insert nodeset="context()/../@*" origin="xxf:attribute('code', '')" if="not(context()/../@code)"/>
                <xf:insert nodeset="context()/../@*" origin="xxf:attribute('codeSystem', '')" if="not(context()/../@codeSystem)"/>
                <xf:insert nodeset="context()/../@*" origin="xxf:attribute('codeSystemName', '')" if="not(context()/../@codeSystemName)"/>
                <xf:insert nodeset="context()/../@*" origin="xxf:attribute('codeSystemVersion', '')" if="not(context()/../@codeSystemVersion)"/>
                <xf:insert nodeset="context()/../@*" origin="xxf:attribute('displayName', '')" if="not(context()/../@displayName)"/>
                <xf:insert nodeset="context()/../@*" origin="xxf:attribute('preferred', '')" if="not(context()/../@preferred)"/>
                <xf:delete nodeset="context()/../(@root | @unit | @nullFlavor)"/>
                <!--<xf:action xxf:iterate="context()/../(@root | @unit | @nullFlavor)">
                    <xf:setvalue ref="context()" value="''"/>
                </xf:action>-->
            </xf:action>
            <xf:action ev:event="xforms-value-changed" if="not(context() = ('identifier', 'quantity', 'code'))">
                <xf:insert nodeset="context()/../@*" origin="xxf:attribute('value', '')" if="not(context()/../@value)"/>
                <!--<xf:insert nodeset="context()/../@*" origin="xxf:attribute('nullFlavor', '')" if="not(@nullFlavor) and string-length(@value) = 0"/>-->
                <xf:delete nodeset="context()/../(@root | @unit | @ordinal | @code | @codeSystem | @codeSystemName | @codeSystemVersion | @displayName | @preferred | @originalText)"/>
                <!--<xf:action xxf:iterate="context()/../(@root | @unit | @ordinal | @code | @codeSystem | @codeSystemName | @codeSystemVersion | @displayName | @preferred | @originalText)">
                    <xf:setvalue ref="context()" value="''"/>
                </xf:action>-->
            </xf:action>
        </xf:select1>
        
        <!-- Activate II attributes -->
        <!--<xf:trigger ref=".[not(@root and @nullFlavor) or (@unit | @ordinal | @code | @codeSystem | @codeSystemName | @codeSystemVersion | @displayName | @preferred | @originalText)]">
            <xf:label>II</xf:label>
            <xf:hint>
                <xsl:text>Activate identifier attributes only (root)</xsl:text>
            </xf:hint>
            <xf:action ev:event="DOMActivate">
                <xf:insert nodeset="@*" origin="xxf:attribute('root', '')" if="not(@root)"/>
                <xf:insert nodeset="@*" origin="xxf:attribute('nullFlavor', '')" if="not(@nullFlavor)"/>
                <xf:delete nodeset="@unit | @ordinal | @code | @codeSystem | @codeSystemName | @codeSystemVersion | @displayName | @preferred | @originalText"/>
            </xf:action>
        </xf:trigger>-->
        <!-- Activate PQ attributes -->
        <!--<xf:trigger ref=".[not(@unit and @nullFlavor) or (@root | @ordinal | @code | @codeSystem | @codeSystemName | @codeSystemVersion | @displayName | @preferred | @originalText)]">
            <xf:label>PQ</xf:label>
            <xf:hint>
                <xsl:text>Activate quantity attributes only (unit)</xsl:text>
            </xf:hint>
            <xf:action ev:event="DOMActivate">
                <xf:insert nodeset="@*" origin="xxf:attribute('unit', '')" if="not(@unit)"/>
                <xf:insert nodeset="@*" origin="xxf:attribute('nullFlavor', '')" if="not(@nullFlavor)"/>
                <xf:delete nodeset="@root | @ordinal | @code | @codeSystem | @codeSystemName | @codeSystemVersion | @displayName | @preferred | @originalText"/>
            </xf:action>
        </xf:trigger>-->
        <!-- Activate CV attributes -->
        <!--<xf:trigger ref=".[not(@code | @codeSystem | @codeSystemName | @codeSystemVersion | @displayName | @preferred | @originalText) or (@root | @unit | @nullFlavor)]">
            <xf:label>CV</xf:label>
            <xf:hint>
                <xsl:text>Activate code attributes only, leaving ordinal attribute as-is (code, codeSystem, codeSystemName, codeSystemVersion)</xsl:text>
            </xf:hint>
            <xf:action ev:event="DOMActivate">
                <xf:setvalue ref="@value" value="''"/>
                <xf:insert nodeset="@*" origin="xxf:attribute('code', '')" if="not(@code)"/>
                <xf:insert nodeset="@*" origin="xxf:attribute('codeSystem', '')" if="not(@codeSystem)"/>
                <xf:insert nodeset="@*" origin="xxf:attribute('codeSystemName', '')" if="not(@codeSystemName)"/>
                <xf:insert nodeset="@*" origin="xxf:attribute('codeSystemVersion', '')" if="not(@codeSystemVersion)"/>
                <xf:insert nodeset="@*" origin="xxf:attribute('displayName', '')" if="not(@displayName)"/>
                <xf:insert nodeset="@*" origin="xxf:attribute('preferred', '')" if="not(@preferred)"/>
                <xf:delete nodeset="@root | @unit | @nullFlavor"/>
            </xf:action>
        </xf:trigger>-->
        <!-- Other simple datatype. Remove attributes other than @value -->
        <!--<xf:trigger ref=".[(@root | @unit | @code | @codeSystem | @codeSystemName | @codeSystemVersion | @displayName | @preferred | @originalText)]">
            <xf:label>OTH</xf:label>
            <xf:hint>
                <xsl:text>This is a simple datatype like boolean, string, text, numeric, etc. Delete other attributes except ordinal (root, unit, code, codeSystem etc.)</xsl:text>
            </xf:hint>
            <xf:action ev:event="DOMActivate">
                <xf:insert nodeset="@*" origin="xxf:attribute('value', '')" if="not(@value)"/>
                <xf:insert nodeset="@*" origin="xxf:attribute('nullFlavor', '')" if="not(@nullFlavor)"/>
                <xf:delete nodeset="@root | @unit | @ordinal | @code | @codeSystem | @codeSystemName | @codeSystemVersion | @displayName | @preferred | @originalText"/>
            </xf:action>
        </xf:trigger>-->
        <!-- Activate ordinal attribute -->
        <xf:trigger ref=".[not(@ordinal)]">
            <xf:label>[+] ord</xf:label>
            <xf:hint>
                <xsl:text>Activate ordinal attribute, leaving other attributes as-is</xsl:text>
            </xf:hint>
            <xf:action ev:event="DOMActivate">
                <xf:insert nodeset="@*" origin="xxf:attribute('ordinal', '')" if="not(@ordinal)"/>
            </xf:action>
        </xf:trigger>
        <!-- Delete ordinal attribute -->
        <xf:trigger ref=".[@ordinal]">
            <xf:label>[-] ord</xf:label>
            <xf:hint>
                <xsl:text>Remove ordinal attribute, leaving other attributes as-is</xsl:text>
            </xf:hint>
            <xf:action ev:event="DOMActivate">
                <xf:delete nodeset="@ordinal"/>
            </xf:action>
        </xf:trigger>
        
        <xsl:if test="not($isMandatory)">
            <!-- Activate nullFlavor attribute -->
            <xf:trigger ref=".[not(@nullFlavor | @codeSystem[. = '{$theNullFlavor}'])]">
                <xf:label>[+] null</xf:label>
                <xf:hint>
                    <xsl:text>Activate nullFlavor attribute, leaving other attributes as-is</xsl:text>
                </xf:hint>
                <xf:action ev:event="DOMActivate">
                    <xf:insert nodeset="@*" origin="xxf:attribute('nullFlavor', '')" if="not(@nullFlavor | @code)"/>
                    <xf:setvalue ref="@value" value="''"/>
                    <xf:setvalue ref="@codeSystem" value="'{$theNullFlavor}'"/>
                    <xf:setvalue ref="@codeSystemVersion" value="''"/>
                </xf:action>
            </xf:trigger>
        </xsl:if>
        <!-- Delete nullFlavor attribute -->
        <xf:trigger ref=".[@nullFlavor | @codeSystem[. = '{$theNullFlavor}']]">
            <xf:label>[-] null</xf:label>
            <xf:hint>
                <xsl:text>Remove nullFlavor attribute, leaving other attributes as-is</xsl:text>
            </xf:hint>
            <xf:action ev:event="DOMActivate">
                <xf:delete nodeset="@nullFlavor"/>
                <!--<xf:setvalue ref="@code" value="''"/>
                <xf:setvalue ref="@codeSystem" value="''"/>
                <xf:setvalue ref="@codeSystemName" value="''"/>
                <xf:setvalue ref="@codeSystemVersion" value="''"/>
                <xf:setvalue ref="@displayName" value="''"/>-->
            </xf:action>
        </xf:trigger>
        
        <!-- Activate @value if there is no @code or if there is and @root|@unit is also still active (initial state) -->
        <xf:group ref=".[not(@datatype = 'code')]">
            <xf:group ref=".[@datatype = 'reference']">
                <xsl:call-template name="typeIsReference">
                    <xsl:with-param name="hideLabel" select="true()"/>
                    <!-- Prevent duplicate NullFlavor -->
                    <xsl:with-param name="allowNullFlavor" select="false()"/>
                </xsl:call-template>
            </xf:group>
            <xf:group ref=".[@datatype = 'text']">
                <xf:textarea mediatype="text/html" incremental="true">
                    <xsl:call-template name="addInputDetails">
                        <!-- Label will be on datatype attribute -->
                        <xsl:with-param name="hideLabel" select="true()"/>
                    </xsl:call-template>
                    <!-- empty nullFlavor if we have a value -->
                    <xf:delete nodeset="context()/../@nullFlavor" ev:event="xforms-value-changed" if="context() != ''"/>
                </xf:textarea>
            </xf:group>
            <xf:group ref=".[not(@datatype = ('reference', 'text'))]">
                <xf:textarea mediatype="text/plain" incremental="true" class="plain">
                    <xsl:call-template name="addInputDetails">
                        <xsl:with-param name="hideLabel" select="true()"/>
                    </xsl:call-template>
                    <!-- empty nullFlavor if we have a value -->
                    <xf:delete nodeset="context()/../@nullFlavor" ev:event="xforms-value-changed" if="context() != ''"/>
                </xf:textarea>
            </xf:group>
        </xf:group>
        
        <!-- root if not code or unit -->
        <xf:textarea ref=".[string-length(@root) gt 0 or @datatype = 'identifier']/@root" mediatype="text/plain" incremental="true" class="plain">
            <xf:label appearance="minimal">
                <xsl:value-of select="ada:getMessage('root')"/>
            </xf:label>
            <xf:hint>
                <xsl:value-of select="ada:getMessage('root')"/>
            </xf:hint>
            <xf:help>
                <xsl:value-of select="ada:getMessage('root-help')"/>
            </xf:help>
            <xf:alert>
                <xsl:value-of select="ada:getMessage('value-must-be')"/>
                <xsl:value-of select="@type"/>
            </xf:alert>
        </xf:textarea>
        
        <!-- unit if not code or root -->
        <xf:textarea ref=".[string-length(@unit) gt 0 or @datatype = ('quantity', 'duration')]/@unit" mediatype="text/plain" incremental="true" class="plain">
            <xf:label appearance="minimal">
                <xsl:value-of select="ada:getMessage('unit')"/>
            </xf:label>
            <xf:hint>
                <xsl:value-of select="ada:getMessage('unit')"/>
            </xf:hint>
            <xf:help>
                <xsl:value-of select="ada:getMessage('unit-help')"/>
            </xf:help>
        </xf:textarea>
        
        <!-- code if not root or unit -->
        <xsl:comment> No code list available </xsl:comment>
        <xf:textarea ref=".[string-length(@code) gt 0 or @datatype = ('code', 'ordinal')]/@code" appearance="minimal" mediatype="text/plain" incremental="true" class="plain">
            <xf:label appearance="minimal">
                <xsl:value-of select="ada:getMessage('code')"/>
            </xf:label>
            <xf:hint>
                <xsl:value-of select="ada:getMessage('code')"/>
            </xf:hint>
            <!-- empty @value when @code gets a value -->
            <xf:setvalue ev:event="xforms-value-changed" if="context() != ''" ref="context()/../@value" value="''"/>
        </xf:textarea>
        
        <xsl:call-template name="addNullFlavorSelection">
            <xsl:with-param name="show-label" select="false()" as="xs:boolean"/>
            <xsl:with-param name="select-label" select="../(name[@language = $language], name)[1]"/>
            <xsl:with-param name="select-ref">.[string-length(@root) = 0][string-length(@unit) = 0][@codeSystem = '<xsl:value-of select="$theNullFlavor"/>']/@code</xsl:with-param>
            <xsl:with-param name="isRequired" select="true()" as="xs:boolean"/>
            <xsl:with-param name="isMandatory" select="false()" as="xs:boolean"/>
        </xsl:call-template>
        
        <!-- codeSystem if code or not root or unit -->
        <xf:textarea ref=".[string-length(@codeSystem) gt 0 or @datatype = ('code', 'ordinal')]/@codeSystem" appearance="minimal" mediatype="text/plain" incremental="true" class="plain">
            <xf:label appearance="minimal">
                <xsl:value-of select="ada:getMessage('codesystem')"/>
            </xf:label>
            <xf:hint>
                <xsl:value-of select="ada:getMessage('codesystem')"/>
            </xf:hint>
            <xf:help>
                <xsl:value-of select="ada:getMessage('no-code-list-available')"/>
            </xf:help>
        </xf:textarea>
        
        <!-- displayName if code or not root or unit -->
        <xf:textarea ref=".[string-length(@displayName) gt 0 or @datatype = ('code', 'ordinal')]/@displayName" appearance="minimal" mediatype="text/plain" incremental="true" class="plain">
            <xf:label appearance="minimal">
                <xsl:value-of select="ada:getMessage('displayname')"/>
            </xf:label>
            <xf:hint>
                <xsl:value-of select="ada:getMessage('displayname')"/>
            </xf:hint>
            <xf:help>
                <xsl:value-of select="ada:getMessage('no-code-list-available')"/>
            </xf:help>
        </xf:textarea>
        
        <!-- ordinal if not root or unit -->
        <xf:textarea ref="@ordinal" appearance="minimal" mediatype="text/plain" incremental="true" class="plain short-number">
            <xf:label appearance="minimal">
                <xsl:value-of select="ada:getMessage('ordinal')"/>
            </xf:label>
            <xf:alert>
                <xsl:value-of select="ada:getMessage('integer-only')"/>
            </xf:alert>
            <xf:hint>
                <xsl:value-of select="ada:getMessage('ordinal')"/>
            </xf:hint>
        </xf:textarea>
        
        <!-- originalText -->
        <xf:group ref=".[string-length(@originalText) gt 0 or .[@code = 'OTH'][@codeSystem = '{$theNullFlavor}']]">
            <xf:textarea ref="@originalText" appearance="minimal" mediatype="text/plain" incremental="true" class="plain">
                <xf:label appearance="minimal">
                    <xsl:value-of select="ada:getMessage('originaltext')"/>
                </xf:label>
                <xf:hint>
                    <xsl:value-of select="ada:getMessage('originaltext')"/>
                </xf:hint>
                <xf:help>
                    <xsl:value-of select="ada:getMessage('no-code-available')"/>
                </xf:help>
            </xf:textarea>
        </xf:group>
        
        <!-- codeSystemName if codeSystem or not root or unit -->
        <xf:textarea ref=".[string-length(@codeSystemName) gt 0 or @datatype = ('code', 'ordinal')]/@codeSystemName" appearance="minimal" mediatype="text/plain" incremental="true" class="plain">
            <xf:label appearance="minimal">
                <xsl:value-of select="ada:getMessage('codesystemname')"/>
            </xf:label>
            <xf:hint>
                <xsl:value-of select="ada:getMessage('codesystemname')"/>
            </xf:hint>
        </xf:textarea>
        
        <!-- codeSystemVersion if codeSystem or not root or unit -->
        <xf:textarea ref=".[string-length(@codeSystemVersion) gt 0 or @datatype = ('code', 'ordinal')]/@codeSystemVersion" appearance="minimal" mediatype="text/plain" incremental="true" class="plain">
            <xf:label appearance="minimal">
                <xsl:value-of select="ada:getMessage('codesystemversion')"/>
            </xf:label>
            <xf:hint>
                <xsl:value-of select="ada:getMessage('codesystemversion')"/>
            </xf:hint>
        </xf:textarea>
        
        <!-- preferred if codeSystem or not root or unit -->
        <xf:select1 ref=".[string-length(@preferred) gt 0 or @datatype = ('code', 'ordinal')]/@preferred" appearance="minimal" class="auto-width">
            <xf:label appearance="minimal" class="hidden">
                <xsl:value-of select="ada:getMessage('preferred')"/>
            </xf:label>
            <xf:hint>
                <xsl:value-of select="ada:getMessage('preferred-hint')"/>
            </xf:hint>
            <xf:help>
                <xsl:value-of select="ada:getMessage('preferred-hint')"/>
            </xf:help>
            <xf:item>
                <xf:label>----</xf:label>
                <xf:value ref="''"/>
            </xf:item>
            <xf:item>
                <xf:label>
                    <xsl:value-of select="ada:getMessage('preferredvalue')"/>
                </xf:label>
                <xf:value ref="'true'"/>
            </xf:item>
            <xf:item>
                <xf:label>
                    <xsl:value-of select="ada:getMessage('alternativevalue')"/>
                </xf:label>
                <xf:value ref="'false'"/>
            </xf:item>
        </xf:select1>
        
        <!-- nullFlavor -->
        <xsl:call-template name="addNullFlavorSelection">
            <xsl:with-param name="isRequired" select="$isRequired" as="xs:boolean"/>
            <xsl:with-param name="isMandatory" select="$isMandatory" as="xs:boolean"/>
        </xsl:call-template>
        <xsl:call-template name="addIdAttribute"/>
    </xsl:template>
    <xsl:template name="typeIsReference" mode="doTheForm" match="*[contains]/valueDomain[@type = 'string']" priority="+10">
        <xsl:param name="isRequired" select="ada:isRequired(..)" as="xs:boolean"/>
        <xsl:param name="isMandatory" select="ada:isMandatory(..)" as="xs:boolean"/>
        <xsl:param name="hideLabel" select="false()" as="xs:boolean"/>
        <xsl:param name="allowNullFlavor" select="true()" as="xs:boolean"/>
        <xsl:comment>
            <xsl:text>Template typeIsReference</xsl:text>
        </xsl:comment>
        <xxf:variable name="internal" select="/*//*[not(@hidden)][@conceptId = '{../contains[1]/@ref}'][string-length(@id) gt 0]"/>
        <xxf:variable name="external" select="instance('summary-instance')//*[@conceptId = '{../contains[1]/@ref}'][string-length(@compositeId) gt 0]"/>
        <xf:select1>
            <!-- TODO: translation -->
            <xsl:call-template name="addInputDetails">
                <xsl:with-param name="hideLabel" select="$hideLabel"/>
            </xsl:call-template>
            <xf:item>
                <!--  add empty choice, for conf=R, but also to avoid making it look like a value is present when there actually isn't -->
                <xf:label>-----</xf:label>
                <xf:value/>
            </xf:item>
            <!-- add current value unless it is in the set of available @id -->
            <!--<xf:itemset nodeset="context()[not(. = /*//*[not(@hidden)][string-length(@id) gt 0]/@id)] | /*//*[not(@hidden)][string-length(@id) gt 0]/@id">
                <xf:label ref="concat(., ' - ', if (local-name() = 'value') then ('-\-{ada:getMessage('unknown')}-\-') else string-join(ancestor-or-self::*/local-name(), '/'))"/>
                <xf:value ref="."/>
            </xf:itemset>-->
            <xf:item>
                <xf:label><xsl:text>--> </xsl:text><xsl:value-of select="ada:getMessage('internal-references')"/><xsl:text> (</xsl:text><xf:output ref="count($internal)"/><xsl:text>)</xsl:text></xf:label>
                <xf:value/>
            </xf:item>
            <!-- <ademhaling_registratie rights="" owner="annevanderkant" last-update-date="2021-06-01T12:20:27.65+02:00" app="zib2020-nl" shortName="ademhaling_registratie" formName="ademhaling" 
                                                               transactionRef="2.16.840.1.113883.2.4.3.11.60.121.4.2.12.5" transactionEffectiveDate="2020-09-01T00:00:00" versionDate="" prefix="zib2017-" language="nl-NL" 
                                                               title="Ademhaling-complete" desc="" id="4d706fd0-902d-48c5-9326-4d7ecd3eec3d"/> -->
            <xf:itemset nodeset="$internal">
                <xf:label ref="string-join((local-name(), @id, @title), ' - ')"/>
                <xf:value ref="@id"/>
            </xf:itemset>
            <xf:item>
                <xf:label><xsl:text>--> </xsl:text><xsl:value-of select="ada:getMessage('external-references')"/><xsl:text> (</xsl:text><xf:output ref="count($external)"/><xsl:text>)</xsl:text></xf:label>
                <xf:value/>
            </xf:item>
            <xf:itemset nodeset="xxf:sort($external, local-name(), 'text', 'ascending')">
                <xf:label ref="concat(string-join((local-name(), @id, @title), ' - '), ' (', string-join((../local-name(), ../@id), ' '), ')')"/>
                <xf:value ref="@compositeId"/>
            </xf:itemset>
            <!-- empty nullFlavor if we have a value -->
            <xf:delete ev:event="xforms-value-changed" if="context() != ''" ref="context()/../@nullFlavor"/>
            <xsl:if test="not(ada:isMandatory(..))">
                <xf:insert ev:event="xforms-value-changed" if="context() = ''" nodeset="context()/..[not(@nullFlavor)]/@*" origin="xxf:attribute('nullFlavor', '')"/>
            </xsl:if>
        </xf:select1>
        <xsl:if test="$allowNullFlavor">
            <xsl:call-template name="addNullFlavorSelection">
                <xsl:with-param name="isRequired" select="$isRequired" as="xs:boolean"/>
                <xsl:with-param name="isMandatory" select="$isMandatory" as="xs:boolean"/>
            </xsl:call-template>
        </xsl:if>
        <xsl:call-template name="addIdAttribute"/>
    </xsl:template>
    <xsl:template name="typeIsOther" mode="doTheForm" match="
            valueDomain[@type = ('count',
            'string',
            'date',
            'datetime',
            'blob',
            'currency',
            'ratio',
            'decimal')]">
        <xsl:param name="isRequired" select="ada:isRequired(..)" as="xs:boolean"/>
        <xsl:param name="isMandatory" select="ada:isMandatory(..)" as="xs:boolean"/>
        <xsl:comment>
            <xsl:text>Template typeIsOther</xsl:text>
        </xsl:comment>
        <xf:textarea mediatype="text/plain" incremental="true" class="plain">
            <xsl:call-template name="addInputDetails">
                <xsl:with-param name="hideLabel" select="$isRequired and not($isMandatory)"/>
            </xsl:call-template>
            <!-- empty nullFlavor if we have a value -->
            <xf:delete ev:event="xforms-value-changed" if="context() != ''" ref="context()/../@nullFlavor"/>
            <xsl:if test="not(ada:isMandatory(..))">
                <xf:insert ev:event="xforms-value-changed" if="context() = ''" nodeset="context()/..[not(@nullFlavor)]/@*" origin="xxf:attribute('nullFlavor', '')"/>
            </xsl:if>
        </xf:textarea>
        <xsl:call-template name="addNullFlavorSelection">
            <xsl:with-param name="isRequired" select="$isRequired" as="xs:boolean"/>
            <xsl:with-param name="isMandatory" select="$isMandatory" as="xs:boolean"/>
        </xsl:call-template>
        <xsl:call-template name="addIdAttribute"/>
    </xsl:template>
    <!-- Skip this -->
    <xsl:template name="community" mode="doTheForm" match="community"/>
    
    <xsl:template name="addNullFlavorSelection">
        <xsl:param name="show-label" select="false()" as="xs:boolean"/>
        <xsl:param name="select-label"><xsl:value-of select="ada:getMessage('nullFlavor')"/></xsl:param>
        <xsl:param name="select-ref">@nullFlavor</xsl:param>
        <xsl:param name="isRequired" as="xs:boolean" required="yes"/>
        <xsl:param name="isMandatory" as="xs:boolean" required="yes"/>
        <xsl:if test="not($isMandatory) and $isRequired">
            <xf:select1 ref="{$select-ref}">
                <xf:label>
                    <!--<xsl:if test="$show-label = false()">-->
                        <xsl:attribute name="class">hidden</xsl:attribute>
                    <!--</xsl:if>-->
                    <xsl:value-of select="$select-label"/>
                </xf:label>
                <xf:item>
                    <xf:label>-----</xf:label>
                    <xf:value/>
                </xf:item>
                <xf:itemset nodeset="instance('nullFlavorSelection')/*">
                    <xf:label ref="."/>
                    <xf:value ref="@value"/>
                </xf:itemset>
                <xf:hint>
                    <xsl:value-of select="ada:getMessage('nullFlavor')"/>
                </xf:hint>
                <xf:help>
                    <xsl:value-of select="ada:getMessage('nullFlavor-help')"/>
                </xf:help>
                <xf:action ev:event="xforms-value-changed">
                    <xf:delete if="context() != ''" nodeset="context()/../@value"/>
                    <xf:insert if="context() = '' and not(context()/../@value)" nodeset="context()/../@*" origin="xxf:attribute('value', '')"/>
                    <!-- populate displayName with nullFlavor name if this a code -->
                    <xsl:if test="ends-with($select-ref, '@code')">
                        <xf:setvalue ref="context()/../@displayName" value="instance('nullFlavorSelection')/*[@value = context()]"/>
                        <xf:setvalue ref="context()/../@codeSystemName" value="'NullFlavor'"/>
                        <xf:setvalue ref="context()/../@codeSystemVersion" value="''"/>
                    </xsl:if>
                </xf:action>
            </xf:select1>
        </xsl:if>
    </xsl:template>
    <!-- Adds label, hint, alert to input -->
    <xsl:template name="addInputDetails">
        <xsl:param name="hideLabel" as="xs:boolean" required="yes"/>
        <xsl:attribute name="ref">@value</xsl:attribute>
        <xsl:comment>
            <xsl:text>Template addInputDetails</xsl:text>
        </xsl:comment>
        <xf:label>
            <!--<xsl:if test="$hideLabel">-->
                <xsl:attribute name="class">hidden</xsl:attribute>
            <!--</xsl:if>-->
            <xsl:value-of select="../(name[@language = $language], name)[1]"/>
        </xf:label>
        <xf:help>
            <xsl:value-of select="../(desc[@language = $language], desc)[1]"/>
        </xf:help>
        <xf:alert>
            <!-- the &amplt; and putting it in a @ref instead of direct text() prevents a display bug in Orbeon... sigh -->
            <xsl:variable name="refstring">
                <!-- TODO: other properties -->
                <xsl:value-of select="ada:getMessage('value-must-be')"/>
                <xsl:value-of select="@type"/>
                <xsl:choose>
                    <!-- VariableVagueDate = date, YYYY(-MM)?, T[+-]\d[YMD] -->
                    <xsl:when test=".[@type = 'string'][@originaltype = 'date'][property/@timeStampPrecision = ('Y', 'YM', 'YMD')]">
                        <xsl:text> (</xsl:text>
                        <xsl:value-of select="ada:getMessage('variable-vague-date-help')"/>
                        <xsl:text>)</xsl:text>
                    </xsl:when>
                    <!-- VariableVagueDateTime = date, dateTime, YYYY(-MM)?, YYYY-MM-DDThh:mm, T[+-]\d[YMD]({time})? -->
                    <xsl:when test=".[@type = 'string'][@originaltype = 'datetime'][property/@timeStampPrecision = ('Y', 'YM', 'YMD', 'YMDHM')]">
                        <xsl:text> (</xsl:text>
                        <xsl:value-of select="ada:getMessage('variable-vague-datetime-help')"/>
                        <xsl:text>)</xsl:text>
                    </xsl:when>
                    <!-- VariableDate = date, T[+-]\d[YMD] -->
                    <xsl:when test=".[@type = 'string'][@originaltype = 'date'] |
                        valueDomain[@type = 'string'][@originaltype = ('date', 'datetime')][property/@timeStampPrecision = 'YMD!']">
                        <xsl:text> (</xsl:text>
                        <xsl:value-of select="ada:getMessage('variable-date-help')"/>
                        <xsl:text>)</xsl:text>
                    </xsl:when>
                    <!-- VariableDateTime = dateTime, T[+-]\d[YMD]({time})? -->
                    <xsl:when test=".[@type = 'string'][@originaltype = 'datetime']">
                        <xsl:text> (</xsl:text>
                        <xsl:value-of select="ada:getMessage('variable-datetime-help')"/>
                        <xsl:text>)</xsl:text>
                    </xsl:when>
                    <!--VariableVagueTime-->
                    <xsl:when test=".[@type = 'string'][@originaltype = 'time'][property/@timeStampPrecision = ('H', 'HM', 'HMS')]">
                        <xsl:text> (</xsl:text>
                        <xsl:value-of select="ada:getMessage('variable-vague-time-help')"/>
                        <xsl:text>)</xsl:text>
                    </xsl:when>
                    <!--TimeHour-->
                    <xsl:when test=".[@type = 'string'][@originaltype = 'time'][property/@timeStampPrecision = 'H!']">
                        <xsl:text> (</xsl:text>
                        <xsl:value-of select="ada:getMessage('time-hour-help')"/>
                        <xsl:text>)</xsl:text>
                    </xsl:when>
                    <!--TimeHourMinute-->
                    <xsl:when test=".[@type = 'string'][@originaltype = 'time'][property/@timeStampPrecision = 'HM!']">
                        <xsl:text> (</xsl:text>
                        <xsl:value-of select="ada:getMessage('time-hour-minute-help')"/>
                        <xsl:text>)</xsl:text>
                    </xsl:when>
                    <!--VariableTime-->
                    <xsl:when test=".[@type = 'string'][@originaltype = 'time']">
                        <xsl:text> (</xsl:text>
                        <xsl:value-of select="ada:getMessage('variable-time-help')"/>
                        <xsl:text>)</xsl:text>
                    </xsl:when>
                    <xsl:when test=".[@type = 'string'][../contains]">
                        <xsl:text> (ref to concept with id = </xsl:text>
                        <xsl:value-of select="../contains/@ref"/>
                        <xsl:text>)</xsl:text>
                    </xsl:when>
                    <xsl:when test="@originaltype and not(@type = @originaltype)">
                        <xsl:text> (</xsl:text>
                        <xsl:value-of select="@originaltype"/>
                        <xsl:text>)</xsl:text>
                    </xsl:when>
                </xsl:choose>
                <xsl:for-each select="property[@unit | @minInclude | @maxInclude | @minLength | @maxLength | @fractionDigits| @timeStampPrecision | @fixed]">
                    <xsl:if test="@unit">
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="@unit"/>
                    </xsl:if>
                    <xsl:if test="@minInclude">
                        <xsl:text> (&gt;= </xsl:text>
                        <xsl:value-of select="@minInclude"/>
                        <xsl:text>)</xsl:text>
                    </xsl:if>
                    <xsl:if test="@maxInclude">
                        <xsl:text> (&amp;lt;= </xsl:text>
                        <xsl:value-of select="@maxInclude"/>
                        <xsl:text>)</xsl:text>
                    </xsl:if>
                    <xsl:if test="@minLength">
                        <xsl:text> (&gt;= </xsl:text>
                        <xsl:value-of select="@minLength"/>
                        <xsl:text>)</xsl:text>
                    </xsl:if>
                    <xsl:if test="@maxLength">
                        <xsl:text> (&amp;lt;= </xsl:text>
                        <xsl:value-of select="@maxLength"/>
                        <xsl:text>)</xsl:text>
                    </xsl:if>
                    <xsl:if test="@fractionDigits">
                        <xsl:text> (</xsl:text>
                        <xsl:value-of select="ada:getMessage('fractionDigits')"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="@fractionDigits"/>
                        <xsl:text>)</xsl:text>
                    </xsl:if>
                    <xsl:if test="@timeStampPrecision">
                        <xsl:text> (</xsl:text>
                        <xsl:value-of select="@timeStampPrecision"/>
                        <xsl:text>)</xsl:text>
                    </xsl:if>
                    <xsl:if test="@fixed">
                        <xsl:text> ('</xsl:text>
                        <xsl:value-of select="@fixed"/>
                        <xsl:text>')</xsl:text>
                    </xsl:if>
                    <xsl:if test="following-sibling::property[@unit | @minInclude | @maxInclude | @minLength | @maxLength | @fractionDigits | @timeStampPrecision | @fixed]">
                        <xsl:text>,</xsl:text>
                    </xsl:if>
                </xsl:for-each>
            </xsl:variable>
            <xsl:attribute name="ref">
                <xsl:text>concat('</xsl:text>
                <xsl:value-of select="replace(string-join($refstring,''),'''','''''')"/>
                <xsl:text>', if (../@datatype) then concat(' (', ../@datatype, ')') else ())</xsl:text>
            </xsl:attribute>
        </xf:alert>
    </xsl:template>
    <!-- Override all default templates for text, attributes -->
    <xsl:template mode="doTheBindings" match="text() | @*"/>
    <xsl:template mode="doTheConditions" match="text() | @*"/>
    <xsl:template mode="doTheForm" match="text() | @*"/>
    <xsl:template match="text() | @*"/>
</xsl:stylesheet>
