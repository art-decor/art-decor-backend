<?xml version="1.0" encoding="UTF-8"?>
<!-- 
Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

This program is free software; you can redistribute it and/or modify it under the terms 
of the GNU Affero General Public Licenseas published by the Free Software Foundation; 
either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the GNU Affero General Public Licensefor more details.

See http://www.gnu.org/licenses/
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="2.0">
    <xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>
    <xsl:include href="ada-basics.xsl"/>
    
    <xsl:param name="versionString" select="$appVersion"/>
    <xsl:param name="authorString">Nictiz</xsl:param>
    <xsl:param name="authorWebsiteString">
        <xsl:choose>
            <xsl:when test="lower-case($authorString) = 'nictiz'">https://www.nictiz.nl</xsl:when>
            <xsl:when test="lower-case($authorString) = 'vzvz'">https://www.vzvz.nl</xsl:when>
        </xsl:choose>
    </xsl:param>
    <xsl:param name="projectStatus">final</xsl:param>
    
    <xsl:variable name="generatedWarning"> ** NOTE: THIS IS A GENERATED FILE. DO NOT EDIT HERE UNLESS YOU KNOW WHAT YOU ARE DOING ** </xsl:variable>
    <xsl:template match="ada">
        <xsl:if test="not(matches($versionString, '^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$'))">
            <xsl:message terminate="yes">VersionString "<xsl:value-of select="$versionString"/>" is not semver compliant.</xsl:message>
        </xsl:if>
        
        <xsl:variable name="href" select="concat($projectDiskRoot, 'build.xml')"/>
        <xsl:variable name="project.description" select="(//view[@type = 'index']/name)[1] ||  ' (from: ' || ($versionDate, 'live')[not(. = '')][1] || ')'"/>
        <xsl:variable name="project.abbrev" select="concat('ada-', replace($projectName, '^ada-', ''))"/>
        <!-- build.xml contains ALL configuration and merges that into expath-pkg and repo.xml -->
        <xsl:result-document href="{$href}" method="xml">
            <xsl:comment><xsl:value-of select="$generatedWarning"/></xsl:comment>
            <project default="xar" name="ada-{$projectName}">
                <property name="project.title" value="ADA {$projectName} (from: {($versionDate, 'live')[not(. = '')][1]})"/>
                <property name="project.version" value="{$versionString}"/>
                <property name="project.abbrev" value="{$project.abbrev}"/>
                <property name="project.app" value="ada-{$projectName}"/>
                <property name="project.uri" value="http://art-decor.org/ns/ada/{$projectName}"/>
                
                <property name="project.description" value="ADA {$project.description}"/>
                <property name="project.author" value="{$authorString}"/>
                <property name="project.website" value="{$authorWebsiteString}"/>
                <property name="project.status" value="{$projectStatus}"/>
                <property name="project.license" value="GNU-LGPL"/>
                <property name="project.copyright" value="true"/>
                <property name="repo.type" value="application"/>
                <property name="repo.target" value="ada-data/projects/{$projectName}"/>
                <property name="repo.prepare.script" value=""/>
                <property name="repo.finish.script" value="post-install.xql"/>
                <property name="project.uri.releasenotes" value="{$authorWebsiteString}"/>
                <property name="build.dir" value="build"/>
                <target name="xar">
                    <mkdir dir="${{build.dir}}"/>
                    <copy file="expath-pkg.xml.tmpl" tofile="expath-pkg.xml" filtering="true" overwrite="true">
                        <filterset>
                            <filter token="project.title" value="${{project.title}}"/>
                            <filter token="project.description" value="${{project.description}}"/>
                            <filter token="project.version" value="${{project.version}}"/>
                            <filter token="project.abbrev" value="${{project.abbrev}}"/>
                            <filter token="project.uri" value="${{project.uri}}"/>
                        </filterset>
                    </copy>
                    <copy file="repo.xml.tmpl" tofile="repo.xml" filtering="true" overwrite="true">
                        <filterset>
                            <filter token="project.version" value="${{project.version}}"/>
                            
                            <filter token="project.description" value="${{project.description}}"/>
                            <filter token="project.author" value="${{project.author}}"/>
                            <filter token="project.website" value="${{project.website}}"/>
                            <filter token="project.status" value="${{project.status}}"/>
                            <filter token="project.license" value="${{project.license}}"/>
                            <filter token="project.copyright" value="${{project.copyright}}"/>
                            <filter token="repo.type" value="${{repo.type}}"/>
                            <filter token="repo.target" value="${{repo.target}}"/>
                            <filter token="repo.prepare.script" value="${{repo.prepare.script}}"/>
                            <filter token="repo.finish.script" value="${{repo.finish.script}}"/>
                            <filter token="project.uri.releasenotes" value="${{project.uri.releasenotes}}"/>
                        </filterset>
                    </copy>
                    <zip basedir="." destfile="${{build.dir}}/${{project.app}}-${{project.version}}.xar" excludes="${{build.dir}}/* expath-pkg.xml.tmpl repo.xml.tmpl"/>
                </target>
            </project>
        </xsl:result-document>
        <xsl:variable name="href" select="concat($projectDiskRoot, 'expath-pkg.xml')"/>
        <xsl:result-document href="{$href}" method="xml">
            <xsl:comment select="$generatedWarning"/>
            <package xmlns="http://expath.org/ns/pkg" name="http://art-decor.org/ns/ada/{@app}" abbrev="{$project.abbrev}" version="{$versionString}" spec="1.0">
                <title>ADA <xsl:value-of select="$project.description"/></title>
                <!--<dependency package="http://exist-db.org/apps/shared"/>-->
            </package>
        </xsl:result-document>
        <xsl:variable name="href" select="concat($projectDiskRoot, 'expath-pkg.xml.tmpl')"/>
        <xsl:result-document href="{$href}" method="xml">
            <xsl:comment><xsl:value-of select="$generatedWarning"/></xsl:comment>
            <package xmlns="http://expath.org/ns/pkg" name="@project.uri@" abbrev="@project.abbrev@" version="@project.version@" spec="1.0">
                <title>@project.description@</title>
                <!--<dependency package="http://exist-db.org/apps/shared"/>-->
            </package>
        </xsl:result-document>
        <xsl:variable name="href" select="concat($projectDiskRoot, 'repo.xml.tmpl')"/>
        <xsl:result-document href="{$href}" method="xml">
            <xsl:comment select="$generatedWarning"/>
            <meta xmlns="http://exist-db.org/xquery/repo">
                <description>@project.description@</description>
                <author>@project.author@</author>
                <website>@project.website@</website>
                <status>@project.status@</status>
                <license>@project.license@</license>
                <copyright>@project.copyright@</copyright>
                <type>@repo.type@</type>
                <target>@repo.target@</target>
                <prepare>@repo.prepare.script@</prepare>
                <finish>@repo.finish.script@</finish>
                <deployed>
                    <xsl:value-of select="@release"/>
                </deployed>
            </meta>
        </xsl:result-document>
        <xsl:variable name="href" select="concat($projectDiskRoot, 'repo.xml')"/>
        <xsl:result-document href="{$href}" method="xml">
            <xsl:comment select="$generatedWarning"/>
            <meta xmlns="http://exist-db.org/xquery/repo">
                <description>ADA <xsl:value-of select="$project.description"/>
                </description>
                <author><xsl:value-of select="$authorString"/></author>
                <website><xsl:value-of select="$authorWebsiteString"/></website>
                <status><xsl:value-of select="$projectStatus"/></status>
                <license>GNU-LGPL</license>
                <copyright>true</copyright>
                <type>application</type>
                <target>ada-data/projects/<xsl:value-of select="$projectName"/></target>
                <prepare></prepare>
                <finish>post-install.xql</finish>
                <deployed>
                    <xsl:value-of select="@release"/>
                </deployed>
            </meta>
        </xsl:result-document>
        <xsl:variable name="href" select="concat($projectDiskRoot, 'post-install.xql')"/>
        <xsl:result-document href="{$href}" method="text">
            <xsl:text>(:</xsl:text>
            <xsl:value-of select="$generatedWarning"/>
            <xsl:text>:)
xquery version "3.1";
import module namespace ada     = "http://art-decor.org/ns/ada-common" at "xmldb:exist:///db/apps/ada/modules/ada-common.xqm";

(:  app name SHALL match final part of install location of the application and as such SHALL match
    the final part of the expath-pkg &lt;target&gt; element
:)
let $projectName := '</xsl:text>
            <xsl:value-of select="$projectName"/>
            <xsl:text>'

(:  create the app data directory. if one already exists, this 
    does nothing except that the full path is returned:)
let $datapath   := xmldb:create-collection($ada:strAdaData, $projectName)
let $x          := xmldb:create-collection($datapath, 'data')

(:  create the app data backup directory. if one already exists, this 
    does nothing except that the full path is returned:)
let $bkuppath   := xmldb:create-collection($ada:strAdaBackup, $projectName)

(:  backup current data in case this new package mangles something during use :)
let $x      := 
    if (xmldb:collection-available($bkuppath)) then 
        xmldb:remove($bkuppath) 
    else ()
let $x      := 
    if (xmldb:collection-available($datapath)) then 
        xmldb:copy-collection($datapath, $ada:strAdaBackup)
    else ()
let $x      := ada:setPermissions($projectName)

return ()</xsl:text>
        </xsl:result-document>
    </xsl:template>
    <xsl:template match="text()|@*"/>
</xsl:stylesheet>