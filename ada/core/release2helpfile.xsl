<?xml version="1.0" encoding="UTF-8"?>
<!-- 
Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

This program is free software; you can redistribute it and/or modify it under the terms 
of the GNU Affero General Public Licenseas published by the Free Software Foundation; 
either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the GNU Affero General Public Licensefor more details.

See http://www.gnu.org/licenses/
-->
<xsl:stylesheet xmlns:ada="http://www.art-decor.org/ada" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="#all" version="2.0">
    <xsl:output method="xhtml" indent="yes" omit-xml-declaration="yes"/>
    <xsl:include href="ada-basics.xsl"/>
    <xsl:template match="/">
        <xsl:variable name="href" select="concat($projectDiskRoot, 'index.html')"/>
        <xsl:result-document href="{$href}" method="xhtml" media-type="text/html" indent="yes" exclude-result-prefixes="#all" doctype-public="html">
            <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <title><xsl:value-of select="(//views/view[@type='index']/name)[1]"/></title>
                    <style>
                        table {
                            border-collapse: collapse;
                        }
                        table,
                        th,
                        td {
                            border: 1px solid black;
                        }
                        h1 {
                            font-size: x-large;
                        }
                        h2 {
                            font-size: large;
                        }</style>
                </head>
                <body>
                    <p>
                        <xsl:value-of select="ada:getMessage('version')"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="(*/@versionDate, '-')[1]"/>
                    </p>
                    <xsl:for-each select="//dataset">
                        <h1>
                            <xsl:value-of select="ada:getMessage('tables')"/>
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="name"/>
                        </h1>
                        <xsl:apply-templates select=".//concept[valueSet]">
                            <xsl:sort select="(name[@language = $language], name)[1]"/>
                        </xsl:apply-templates>
                    </xsl:for-each>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>
    <xsl:template match="concept[valueSet]">
        <xsl:variable name="doCodeSystemName" select="exists(valueSet/conceptList/(concept|exception)[@codeSystemName])" as="xs:boolean"/>
        <xsl:variable name="doCodeSystemVersion" select="exists(valueSet/conceptList/(concept|exception)[@codeSystemVersion])" as="xs:boolean"/>
        <h2 xmlns="http://www.w3.org/1999/xhtml">
            <xsl:value-of select="name"/>
        </h2>
        <table xmlns="http://www.w3.org/1999/xhtml">
            <tr>
                <th><xsl:value-of select="ada:getMessage('localid')"/></th>
                <th><xsl:value-of select="ada:getMessage('name')"/></th>
                <th><xsl:value-of select="ada:getMessage('code')"/></th>
                <th><xsl:value-of select="ada:getMessage('codesystem')"/></th>
                <xsl:if test="$doCodeSystemName">
                    <th><xsl:value-of select="ada:getMessage('codesystemname')"/></th>
                </xsl:if>
                <xsl:if test="$doCodeSystemVersion">
                    <th><xsl:value-of select="ada:getMessage('codesystemversion')"/></th>
                </xsl:if>
            </tr>
            <xsl:apply-templates select="valueSet/conceptList/(concept|exception)">
                <xsl:with-param name="doCodeSystemName" select="$doCodeSystemName"/>
                <xsl:with-param name="doCodeSystemVersion" select="$doCodeSystemVersion"/>
            </xsl:apply-templates>
        </table>
    </xsl:template>
    <xsl:template match="concept|exception">
        <xsl:param name="doCodeSystemName" as="xs:boolean"/>
        <xsl:param name="doCodeSystemVersion" as="xs:boolean"/>
        <tr xmlns="http://www.w3.org/1999/xhtml">
            <td>
                <xsl:value-of select="@localId"/>
            </td>
            <td>
                <xsl:value-of select="name"/>
            </td>
            <td>
                <xsl:value-of select="@code"/>
            </td>
            <td>
                <xsl:value-of select="@codeSystem"/>
            </td>
            <xsl:if test="$doCodeSystemName">
                <td>
                    <xsl:value-of select="@codeSystemName"/>
                </td>
            </xsl:if>
            <xsl:if test="$doCodeSystemVersion">
                <td>
                    <xsl:value-of select="@codeSystemVersion"/>
                </td>
            </xsl:if>
        </tr>
    </xsl:template>
    <xsl:template match="@*|node()"/>
</xsl:stylesheet>