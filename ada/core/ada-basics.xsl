<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
    
    This program is free software; you can redistribute it and/or modify it under the terms 
    of the GNU Affero General Public Licenseas published by the Free Software Foundation; 
    either version 3 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Affero General Public Licensefor more details.
    
    See http://www.gnu.org/licenses/
-->
<xsl:stylesheet xmlns:ada="http://www.art-decor.org/ada"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:xxf="http://orbeon.org/oxf/xml/xforms" 
    xmlns:xf="http://www.w3.org/2002/xforms" version="2.0">
    <!-- The project prefix, as defined in the ADA XML definition file. -->
    <xsl:variable name="prefix" select="/ada/project/@prefix/string()"/>
    <!-- The URI from where to retrieve transactions, as defined in the ADA XML definition file. -->
    <xsl:variable name="releaseBaseUri" select="/ada/project/release/@baseUri/string()"/>
    <!-- The location in eXist of this project -->
    <xsl:variable name="existInternalBaseUri" select="'/db/apps/'"/>
    <!-- The local folder where the project is stored (one up from definition folder) -->
    <xsl:variable name="projectDiskRoot" select="resolve-uri('..', base-uri())"/>
    <!-- The name of this app -->
    <xsl:variable name="projectName" select="tokenize($projectDiskRoot, '/')[last()-1]"/>
    <!-- The version of this app -->
    <xsl:variable name="appVersion" select="(/ada/@version, '1.0.0')[not(. = '')][1]"/>
    <!-- The URI for this project, starting from ada-data -->
    <xsl:variable name="projectUri" select="concat('ada-data/projects/', $projectName, '/')"/>
    <!-- The project language, as defined in the ADA XML definition file. -->
    <xsl:variable name="language" select="/ada/project/@language/string()"/>
    <!-- The project versionDate, as defined in the ADA XML definition file. -->
    <xsl:variable name="versionDate" select="/ada/project/@versionDate/string()"/>
    <!-- Special element in schema to support extensability -->
    <xsl:variable name="extensionElementName">adaextension</xsl:variable>
    
    <xsl:variable name="theMESSAGES" select="document('ada-i18n.xml')"/>
    <xsl:variable name="theLanguages" select="distinct-values($theMESSAGES//entry[1]/*/@language)" as="xs:string*"/>
    
    <!-- DECOR dataset datatypes -->
    <xsl:variable name="theDatatypes" as="element(decorTypes)">
        <xsl:variable name="strDecorXSD" select="'https://assets.art-decor.org/ADAR/rv/DECOR.xsd'"/>
        <xsl:variable name="strDecorDatatypesXSD" select="'https://assets.art-decor.org/ADAR/rv/DECOR-datatypes.xsd'"/>
        <xsl:variable name="docDecorMainXSD" select="if (doc-available($strDecorXSD)) then (doc($strDecorXSD)/xs:schema) else ()" as="element(xs:schema)?"/>
        <xsl:variable name="docDecorDatatypesXSD" select="if (doc-available($strDecorDatatypesXSD)) then (doc($strDecorDatatypesXSD)/xs:schema) else ()" as="element(xs:schema)?"/>
        <xsl:variable name="docDecorXSD" select="$docDecorMainXSD | $docDecorDatatypesXSD" as="element(xs:schema)*"/>
        <xsl:choose>
            <xsl:when test="$docDecorXSD">
                <xsl:variable name="types" select="$docDecorXSD/xs:element | $docDecorXSD/xs:complexType | $docDecorXSD/xs:simpleType[.//xs:enumeration]" as="element()*"/>
                
                <decorTypes>
                    <xsl:for-each select="$types">
                        <xsl:element name="{@name}">
                            <xsl:for-each select=".//xs:element | .//xs:attribute">
                                <xsl:element name="{local-name()}">
                                    <xsl:copy-of select="@name"/>
                                    <xsl:copy-of select="@ref"/>
                                    <xsl:copy-of select="ada:getLabelsAndHints(.)"/>
                                </xsl:element>
                            </xsl:for-each>
                            <xsl:for-each select=".//xs:enumeration">
                                <enumeration>
                                    <xsl:copy-of select="@value"/>
                                    <xsl:copy-of select="ada:getLabelsAndHints(.)"/>
                                </enumeration>
                            </xsl:for-each>
                        </xsl:element>
                    </xsl:for-each>
                </decorTypes>
            </xsl:when>
            <xsl:otherwise>
                <!-- These are the ones we truly need. The rest is optional-->
                <decorTypes>
                    <DataSetValueType>
                        <enumeration value="string">
                            <label language="nl-NL">String</label>
                            <label language="en-US">String</label>
                            <label language="de-DE">String</label>
                            <label language="pl-PL">String</label>
                        </enumeration>
                        <enumeration value="code">
                            <label language="nl-NL">Code</label>
                            <label language="en-US">Code</label>
                            <label language="de-DE">Kode</label>
                            <label language="pl-PL">Code</label>
                        </enumeration>
                        <enumeration value="identifier">
                            <label language="nl-NL">Identificatie</label>
                            <label language="en-US">Identifier</label>
                            <label language="de-DE">Identifier</label>
                            <label language="pl-PL">Identifier</label>
                        </enumeration>
                        <enumeration value="date">
                            <label language="nl-NL">Datum</label>
                            <label language="en-US">Date</label>
                            <label language="de-DE">Datum</label>
                            <label language="pl-PL">Date</label>
                        </enumeration>
                        <enumeration value="datetime">
                            <label language="nl-NL">Datum/tijd</label>
                            <label language="en-US">Date/time</label>
                            <label language="de-DE">Datum/Zeit</label>
                            <label language="pl-PL">Date/time</label>
                        </enumeration>
                        <enumeration value="time">
                            <label language="nl-NL">Tijd</label>
                            <label language="en-US">Time</label>
                            <label language="de-DE">Zeit</label>
                            <label language="pl-PL">Time</label>
                        </enumeration>
                        <enumeration value="quantity">
                            <label language="nl-NL">Hoeveelheid</label>
                            <label language="en-US">Quantity</label>
                            <label language="de-DE">Quantität</label>
                            <label language="pl-PL">Quantity</label>
                        </enumeration>
                        <enumeration value="duration">
                            <label language="nl-NL">Tijdsduur</label>
                            <label language="en-US">Duration</label>
                            <label language="de-DE">Dauer</label>
                            <label language="pl-PL">Duration</label>
                        </enumeration>
                        <enumeration value="boolean">
                            <label language="nl-NL">Boolean</label>
                            <label language="en-US">Boolean</label>
                            <label language="de-DE">Boolean</label>
                            <label language="pl-PL">Boolean</label>
                        </enumeration>
                        <enumeration value="count">
                            <label language="nl-NL">Aantal</label>
                            <label language="en-US">Count</label>
                            <label language="de-DE">Count</label>
                            <label language="pl-PL">Count</label>
                        </enumeration>
                        <enumeration value="ordinal">
                            <label language="nl-NL">Ordinaal</label>
                            <label language="en-US">Ordinal</label>
                            <label language="de-DE">Ordinal</label>
                            <label language="pl-PL">Ordinal</label>
                        </enumeration>
                        <enumeration value="text">
                            <label language="nl-NL">Tekst</label>
                            <label language="en-US">Text</label>
                            <label language="de-DE">Text</label>
                            <label language="pl-PL">Text</label>
                        </enumeration>
                        <enumeration value="decimal">
                            <label language="nl-NL">Decimaal getal</label>
                            <label language="en-US">Decimal number</label>
                            <label language="de-DE">Dezimalzahl</label>
                            <label language="pl-PL">Decimal number</label>
                        </enumeration>
                        <enumeration value="blob">
                            <label language="nl-NL">Binair</label>
                            <label language="en-US">Binary</label>
                            <label language="de-DE">Binary</label>
                            <label language="pl-PL">Binary</label>
                        </enumeration>
                        <enumeration value="complex">
                            <label language="nl-NL">Samengestelde gegevens</label>
                            <label language="en-US">Collection of data</label>
                            <label language="de-DE">Zusammenstellung von Daten</label>
                            <label language="pl-PL">Collection of data</label>
                        </enumeration>
                    </DataSetValueType>
                </decorTypes>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:function name="ada:getMessage">
        <xsl:param name="key"/>
        <xsl:sequence select="$theMESSAGES//entry[@key = $key]/text[@language = $language]/text()"/>
    </xsl:function>
    
    <xsl:function name="ada:isRequired" as="xs:boolean">
        <xsl:param name="concept" as="element(concept)"/>
        <xsl:variable name="isMandatory" select="ada:isMandatory($concept)" as="xs:boolean"/>
        <xsl:sequence select="($concept[@minimumMultiplicity castable as xs:integer]/@minimumMultiplicity > 0 or $concept/@conformance = ('R', 'C')) and not($isMandatory)"/>
    </xsl:function>
    <xsl:function name="ada:isMandatory" as="xs:boolean">
        <xsl:param name="concept" as="element(concept)"/>
        <xsl:sequence select="$concept/@conformance = 'M' or $concept/@isMandatory = 'true'"/>
    </xsl:function>
    
    <xsl:function name="ada:getLabelsAndHints" as="element()*" exclude-result-prefixes="#all">
        <xsl:param name="type" as="element()"/>
        
        <xsl:for-each select="$type/xs:annotation/xs:appinfo/xf:label">
            <label language="{@xml:lang}"><xsl:value-of select="."/></label>
        </xsl:for-each>
        
        <!-- add any missing language as copy from en-US -->
        <xsl:if test="$type/xs:annotation/xs:appinfo/xf:label">
            <xsl:for-each select="$language[not(. = $type/xs:annotation/xs:appinfo/xf:label/@xml:lang)]">
                <label language="{.}"><xsl:value-of select="$type/xs:annotation/xs:appinfo/xf:label[@xml:lang='en-US']"/></label>
            </xsl:for-each>
        </xsl:if>
        
        <xsl:for-each select="$type/xs:annotation/xs:appinfo/xf:hint">
            <hint language="{@xml:lang}"><xsl:value-of select="."/></hint>
        </xsl:for-each>
        
        <!-- add any missing language as copy from en-US -->
        <xsl:if test="$type/xs:annotation/xs:appinfo/xf:hint">
            <xsl:for-each select="$language[not(. = $type/xs:annotation/xs:appinfo/xf:hint/@xml:lang)]">
                <hint language="{.}"><xsl:value-of select="$type/xs:annotation/xs:appinfo/xf:hint[@xml:lang='en-US']"/></hint>
            </xsl:for-each>
        </xsl:if>
    </xsl:function>
</xsl:stylesheet>