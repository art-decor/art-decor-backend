<?xml version="1.0" encoding="UTF-8"?>
<!-- 
Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

This program is free software; you can redistribute it and/or modify it under the terms 
of the GNU Affero General Public Licenseas published by the Free Software Foundation; 
either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the GNU Affero General Public Licensefor more details.

See http://www.gnu.org/licenses/
-->
<xsl:stylesheet xmlns:ada="http://www.art-decor.org/ada" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:ev="http://www.w3.org/2001/xml-events" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:fr="http://orbeon.org/oxf/xml/form-runner" 
    xmlns:xxforms="http://orbeon.org/oxf/xml/xforms" 
    xmlns:xf="http://www.w3.org/2002/xforms" 
    xmlns:xhtml="http://www.w3.org/1999/xhtml" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/2002/08/xhtml/xhtml1-transitional.xsd     http://www.w3.org/2002/xforms http://www.w3.org/MarkUp/Forms/2002/XForms-Schema.xsd" version="2.0">
    <xsl:output method="text"/>
    <xsl:include href="ada-basics.xsl"/>
    <xsl:template match="/">
        <xsl:for-each select="//view[@type='index'][@target='xquery']">
            <xsl:variable name="href" select="concat($projectDiskRoot, 'modules/index.xquery')"/>
            <xsl:result-document href="{$href}" method="xml" omit-xml-declaration="yes" indent="yes">
xquery version "1.0";
declare option exist:serialize "method=xhtml media-type=text/html indent=yes";
let $orbeonBaseUri := doc('/db/apps/ada-data/conf.xml')//orbeon/@uri
let $cssUri := doc('/db/apps/ada-data/conf.xml')//css/@uri
let $dataUri := doc('/db/apps/ada-data/conf.xml')//data/@uri
return
        <html>
                    <head>
                        <title>
                            <xsl:value-of select="name"/>
                        </title>
                        <link rel="stylesheet" type="text/css" href="{{$cssUri}}"/>
                    </head>
                    <body class="orbeon ada">
                        <h1>
                            <xsl:value-of select="name"/>
                        </h1>
                <!--<p>User: {xmldb:get-current-user()}</p>-->
                        <a href="{{$orbeonBaseUri}}{$projectUri}views/{indexOf/@shortName/string()}.xhtml?id=new">New</a>
                        <table>
                            <tr>
                                <xsl:for-each select="dataset/concept">
                                    <th>
                                        <xsl:value-of select="name"/>
                                    </th>
                                </xsl:for-each>
                                <th><xsl:value-of select="ada:getMessage('title')"/></th>
                                <th><xsl:value-of select="ada:getMessage('user')"/></th>
                                <th><xsl:value-of select="ada:getMessage('last-updated')"/></th>
                                <th/>
                            </tr>
                    { for $instance in collection($dataUri)//adaxml[data[*[@app='<xsl:value-of select="$projectName"/>']]]
                    order by $instance/meta/@creation-date
                    return
                    <tr>
                                <xsl:for-each select="dataset/concept">
                                    <td>{$instance/data//*[@conceptId='<xsl:value-of select="@id"/>']/@value/string()}</td>
                                </xsl:for-each>
                                <td>{data($instance/data/*/@title)}</td>
                                <td>{data($instance/meta/@created-by)}</td>
                                <td>{data($instance/meta/@last-update-date)}</td>
                                <td>
                                    <a href="{{$orbeonBaseUri}}{$projectUri}views/{indexOf/@shortName/string()}.xhtml?id={{data($instance/data/*/@id)}}">Edit</a>
                                </td>
                                <xsl:if test="controls/button[@type='xml']">
                                    <td>
                                        <a href="/ada/projects/{$projectName}/{{data($instance/data/*/@id)}}">XML</a>
                                    </td>
                                </xsl:if>
                            </tr>
                    }
                </table>
                    </body>
                </html>
            </xsl:result-document>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>