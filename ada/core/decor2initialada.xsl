<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  exclude-result-prefixes="xs"
  version="2.0">
  
  <!-- Takes a DECOR project as input and generates an initial ADA file based on all active transactions that define concepts in their representingTemplate -->
  <xsl:output indent="yes" omit-xml-declaration="yes"/>
  
  
  <!-- The server to retrieve data from when you run the initial file -->
  <xsl:param name="baseUri" select="(/decor/@deeplinkprefixservices, 'https://decor.nictiz.nl/decor/services/')[1] || 'RetrieveTransaction'"/>
  <!-- The language of the project to work with. Normally the default language of the project -->
  <xsl:param name="language" select="/decor/project/@defaultLanguage"/>
  <!-- The version of the project to work with. You might want to override this param -->
  <xsl:param name="version" select="(/decor/@versionDate, 'development')[1]"/>
  <!--Using this parameter you can override valueDomain/@type='datetime', valueDomain/@type='date', valueDomain/@type='time' as string. 
        This supports forms that need parametrized dates like format-dateTime(current-dateTime(), '...') which are not valid dates in itself 
        The only other way to do this is by listing and overriding each concept separately using valueDomainType='string'. Default = true -->
  <xsl:param name="overrideDatatypeDateTimeAsString">true</xsl:param>
  <!--Using this parameter you can override valueDomain/@type='text' as string. 
        This supports forms that do not want rich text as value for the concept values. 
        The only other way to do this is by listing and overriding each concept separately using valueDomainType='string'. Default = true -->
  <xsl:param name="overrideDatatypeTextAsString">true</xsl:param>
  <!--Using this parameter you can override concepts with a contains element, that do not have a valueDomain and are not marked for expansion 
        somewhere to be an item with valueDomain/@type='identifier'. This supports referencing identified 'objects' that live somewhere else although undefined where , like 
        health care provider or patient. The only other way to do this is by listing and overriding each concept separately. Default = true -->
  <xsl:param name="overrideContainsAsIdentifier">true</xsl:param>
  
  <!-- Invokes the generation of comment option for all transaction input in the XForms. Default = false -->
  <xsl:param name="addComments">false</xsl:param>
  <!-- Invokes the generation of an error summary for transaction input in the XForms. Default = true -->
  <xsl:param name="errorSummary">true</xsl:param>
  
  <xsl:template match="/">
    <xsl:variable name="transactions" select="//transaction[representingTemplate/concept]" as="element()*"/>
    
    <ada xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="https://decor.nictiz.nl/ada/core/ada.xsd">
      <project prefix="{/decor/project/@prefix}" language="{$language}">
        <xsl:if test="string-length($version)">
          <xsl:attribute name="versionDate" select="$version"></xsl:attribute>
        </xsl:if>
        <release baseUri="{$baseUri}"/>
      </project>
      <applications>
        <application>
          <params>
            <param name="overrideDatatypeDateTimeAsString" value="{$overrideDatatypeDateTimeAsString}"/>
            <param name="overrideDatatypeTextAsString" value="{$overrideDatatypeTextAsString}"/>
            <param name="overrideContainsAsIdentifier" value="{$overrideContainsAsIdentifier}"/>
          </params>
          <views>
            <view id="1" type="index" target="xforms" addComments="true">
              <name><xsl:value-of select="//decor/project/name[@language = $language]"/> Index</name>
              <xsl:for-each select="$transactions">
                  <indexOf ref="{position() + 1}"/>
              </xsl:for-each>
              <controls>
                <button type="xml"/>
              </controls>
            </view>
            <xsl:apply-templates select="$transactions"/>
          </views>
        </application>
      </applications>
    </ada>
  </xsl:template>
  
  <xsl:template match="transaction">
    <xsl:comment><xsl:text> </xsl:text><!--<xsl:value-of select="@id"/><xsl:text> - </xsl:text>--><xsl:value-of select="replace((name[@language = $language], name)[1], '--', '::')"/><xsl:text> </xsl:text></xsl:comment>
    <view id="{position() + 1}" type="crud" target="xforms" addComments="{$addComments}" errorSummary="{$errorSummary}" transactionId="{@id}" transactionEffectiveDate="{@effectiveDate}">
      <name>
        <xsl:value-of select="(name[@language = $language], name)[1]"/>
      </name>
      <concepts include="all"/>
    </view>
  </xsl:template>
</xsl:stylesheet>