# ADA Setup

## Prerequisites

Familiarize yourself with [ART-DECOR ADA documentation](https://docs.art-decor.org/release2/specials/#art-decor-applications-aka-ada)

Know how to run transforms using XSLT. oXygen XML Developer, Saxon command line or otherwise.

## Steps
- Create folder matching projectprefix + version, e.g. `demo1-1.0.0`, create one subfolder `definitions`
- Get (compiled) project from server: /decor/services/RetrieveProject?prefix=[prefix]
  - Note that if you get the correct publication version, then this helps in setting up
- Create initial ADA file by transforming the project file using `decor2initialada.xsl` into the definitions folder, e.g. as `demo1-1.0.0-ada.xml`
- Create release file by transforming the initial ADA file from the previous step using ada2release.xsl, this produces `[input name]-release.xml` next to the initial file
- Using `[input name]-release.xml` as input run the following transformations in any order:
  - release2newxml.xsl
  - release2xform.xsl
  - release2schema.xsl
  - release2package.xsl
  
You are now ready to get the `demo1-1.0.0` with its contents onto the ART-DECOR server in the database. Ways to do this:

1. Run `ant` (Apache project) in the folder which produces a build folder containing `demo1-1.0.0.xar`, and install this xar file using the [eXist-db Package Manager](http://localhost:8877/exist/apps/dashboard/index.html)
1. Import the folder in the eXist-db database under path /db/apps/ada-data/projects/ using the file manager in [eXist-db eXide](http://localhost:8877/exist/apps/eXide/index.html)
  - If a previous version exists: remove that before import
  - After import: open /db/apps/ada-data/db/projects/demo1-1.0.0/post-install.xq and run eval on that. This preps the /db/apps/ada-data/db/demo1-1.0.0/data folder where instances for this project will live
