<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
    
    This program is free software; you can redistribute it and/or modify it under the terms 
    of the GNU Affero General Public Licenseas published by the Free Software Foundation; 
    either version 3 of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Affero General Public Licensefor more details.
    
    See http://www.gnu.org/licenses/
-->
<!--
    Input: a {project}-ada.xml doc
    Output: a {project}-{versionDate}-{language}-ada.xml doc
    
    Will retrieve transaction for each view, output contains all attributes and constructs from {project}-ada.xml doc 
-->
<xsl:stylesheet xmlns:ada="http://www.art-decor.org/ada" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="#all" version="2.0">
    <xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>
    <xsl:include href="ada-basics.xsl"/>
    <xsl:include href="shortName.xsl"/>
    
    <xsl:variable name="transactions" as="element()">
        <transactionDatasets>
            <xsl:for-each-group select="//*[@transactionId]" group-by="concat(@transactionId, @transactionEffectiveDate[. castable as xs:dateTime])">
                <xsl:variable name="trid" select="current-group()[1]/@transactionId"/>
                <xsl:variable name="tred" select="current-group()[1]/@transactionEffectiveDate[. castable as xs:dateTime]"/>
                <xsl:variable name="lang" select="(@language, $language)[1]"/>
                <xsl:variable name="uri" select="concat($releaseBaseUri, '?id=', $trid, '&amp;effectiveDate=', $tred, '&amp;language=', $lang, '&amp;version=', $versionDate, '&amp;format=xml')"/>
                
                <xsl:copy-of copy-namespaces="no" select="document($uri)"/>
            </xsl:for-each-group>
        </transactionDatasets>
    </xsl:variable>
    <!-- This is a helper variable for finding concepts that might do with an adaId even though the community or other doesn't tell us so -->
    <xsl:variable name="containElements" select="//concept[@type = 'group']/contains | $transactions//concept[@type = 'group']/contains" as="element()*"/>
    <xsl:template match="/">
        <xsl:result-document href="{replace(base-uri(), '.xml', '-release.xml')}" method="xml">
            <xsl:comment>ADA release generator version 1, <xsl:value-of select="current-dateTime()"/>
            </xsl:comment>
            <xsl:text/>
            <xsl:apply-templates/>
        </xsl:result-document>
    </xsl:template>
    <xsl:template match="ada">
        <xsl:copy copy-namespaces="no">
            <xsl:attribute name="adaVersion" select="'1'"/>
            <xsl:attribute name="release" select="current-dateTime()"/>
            <xsl:attribute name="app" select="$projectName"/>
            <xsl:apply-templates select="(@* except @xsi:noNamespaceSchemaLocation)|node()"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="view">
        <xsl:variable name="lang" select="(@language, $language)[1]"/>
        <!-- Using this parameter you can override valueDomain/@type='datetime' or 'date' or 'time' as string. This supports forms that need 
            parametrized dates like format-dateTime(current-dateTime(), '...') which are not valid dates in itself 
            The only current way to do this is by listing and overriding each concept separately. -->
        <!-- This gets the first matching parameter in scope for this view, all views, this applications, all applications -->
        <xsl:variable name="overrideDatatypeDateTimeAsString" select="(ancestor-or-self::*/params/param[@name = 'overrideDatatypeDateTimeAsString']/@value)[1]" as="xs:string?"/>
        <!-- Using this parameter you can override valueDomain/@type='text' as string. This supports forms that do not want rich text as value for the concept values. 
            The only other way to do this is by listing and overriding each concept separately using valueDomainType='string'. -->
        <!-- This gets the first matching parameter in scope for this view, all views, this applications, all applications -->
        <xsl:variable name="overrideDatatypeTextAsString" select="(ancestor-or-self::*/params/param[@name = 'overrideDatatypeTextAsString']/@value)[1]" as="xs:string?"/>
        <!-- Using this parameter you can override concepts with a contains element, that do not have a valueDomain and are not marked for expansion 
            somewhere to be an item with valueDomain/@type='identifier'. This supports referencing identified 'objects' that live somewhere else although undefined where , like 
            health care provider or patient. The only other way to do this is by listing and overriding each concept separately. -->
        <xsl:variable name="overrideContainsAsIdentifier" select="(ancestor-or-self::*/params/param[@name = 'overrideContainsAsIdentifier']/@value)[1]" as="xs:string?"/>
        
        <view>
            <xsl:apply-templates select="@*"/>
            <!-- relevant for apps with multiple cruds for different transactions -->
            <xsl:for-each select="indexOf">
                <xsl:copy copy-namespaces="no">
                    <xsl:copy-of copy-namespaces="no" select="@*"/>
                    <xsl:variable name="num" select="@ref"/>
                    <xsl:attribute name="shortName">
                        <xsl:call-template name="shortName">
                            <xsl:with-param name="name">
                                <xsl:value-of select="(//view[@id = $num]/name[@language = $lang], //view[@id = $num]/name)[1]"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </xsl:attribute>
                </xsl:copy>
            </xsl:for-each>
            <xsl:copy-of copy-namespaces="no" select="name"/>
            <implementation>
                <xsl:attribute name="shortName">
                    <xsl:call-template name="shortName">
                        <xsl:with-param name="name">
                            <xsl:value-of select="(name[@language = $lang], name)[1]"/>
                        </xsl:with-param>
                    </xsl:call-template>
                </xsl:attribute>
            </implementation>
            <xsl:variable name="theConcepts" select="concepts" as="element()*"/>
            <!-- Supported for indexes:
                Old:
                <view type="index" transactionId=".." ..> <concepts/> </view>
                    Meaning: handle everything under concepts for view/@transactionId
                
                New:
                <view type="index" ..> <concepts/> </view>
                    Meaning: handle everything under concepts for all transactions that have a crud view
                or
                <view type="index" ..> <concepts transactionId=".." transactionEffectiveDate=".."/> <concepts transactionId=".." transactionEffectiveDate=".."/> </view>
                    Meaning: handle everything under concepts for respective concepts/@transactionId. Each concepts elements should point to a different transaction using both transactionId and transactionEffectiveDate
            -->
            <xsl:choose>
                <xsl:when test="(@transactionId and indexOf/concepts) or (indexOf/concepts and concepts)">
                    <xsl:message terminate="yes">Ambiguous definition. Found view[@id = <xsl:value-of select="@id"/>][@type = <xsl:value-of select="@type"/>] with (@transactionId and indexOf/concepts) or (indexOf/concepts and concepts). These combinations are not allowed</xsl:message>
                </xsl:when>
                <xsl:when test="@transactionId and (count($theConcepts) gt 1 or $theConcepts[@transactionId])">
                    <xsl:message terminate="yes">Ambiguous definition. Found view[@id = <xsl:value-of select="@id"/>][@type = <xsl:value-of select="@type"/>] with @transactionId and multiple concepts elements or concepts element with @transactionId. If there is a view/@transactionId, there can be only one concepts element</xsl:message>
                </xsl:when>
                <xsl:when test="$theConcepts[not(@transactionId)] and $theConcepts[@transactionId]">
                    <xsl:message terminate="yes">Ambiguous definition. Found view[@id = <xsl:value-of select="@id"/>][@type = <xsl:value-of select="@type"/>] with concepts elements both with and without @transactionId. If there are multiple concepts elements then all shall have a transactionId attribute, otherwise there can only be exactly one concepts element without @transactionId to indicate concepts for all transactions</xsl:message>
                </xsl:when>
                <xsl:when test="not(@type = 'index') and count($theConcepts) gt 1">
                    <xsl:message terminate="yes">Ambiguous definition. Found view[@id = <xsl:value-of select="@id"/>][@type = <xsl:value-of select="@type"/>] with multiple concepts elements. Unless this is an index view there can be only one concepts element</xsl:message>
                </xsl:when>
                <xsl:when test="count(distinct-values($theConcepts/concat(@transactionId,@transactionEffectiveDate))) != count($theConcepts/concat(@transactionId,@transactionEffectiveDate))">
                    <xsl:message terminate="yes">Ambiguous definition. Found view[@id = <xsl:value-of select="@id"/>][@type = <xsl:value-of select="@type"/>] with duplicate concepts definitions. @transactionId / @transactionEffectiveDate should be unique per concepts elements</xsl:message>
                </xsl:when>
                <xsl:when test="@type = 'index'">
                    <!-- No need for datasets in the index -->
                </xsl:when>
                <!-- Old style -->
                <xsl:when test="@transactionId">
                    <xsl:variable name="transactionId" select="@transactionId"/>
                    <xsl:variable name="transaction" select="$transactions//dataset[@transactionId = $transactionId]" as="element()?"/>
                    <dataset>
                        <xsl:copy-of copy-namespaces="no" select="$transaction/@*"/>
                        <xsl:if test="count($transactions//dataset[@shortName = $transaction/@shortName]) gt 1">
                            <xsl:attribute name="shortName">
                                <xsl:call-template name="shortName">
                                    <xsl:with-param name="name" select="(name[@language = $lang], name)[1]"/>
                                </xsl:call-template>
                                <xsl:text>_</xsl:text>
                                <xsl:value-of select="$transaction/@shortName"/>
                            </xsl:attribute>
                        </xsl:if>
                        <xsl:call-template name="copyConcept">
                            <xsl:with-param name="overrideDatatypeDateTimeAsString" select="$overrideDatatypeDateTimeAsString"/>
                            <xsl:with-param name="overrideDatatypeTextAsString" select="$overrideDatatypeTextAsString"/>
                            <xsl:with-param name="overrideContainsAsIdentifier" select="$overrideContainsAsIdentifier"/>
                            <xsl:with-param name="datasetConcepts" select="$transaction"/>
                            <xsl:with-param name="viewConcepts" select="$theConcepts"/>
                            <xsl:with-param name="lang" select="$lang"/>
                        </xsl:call-template>
                    </dataset>
                </xsl:when>
                <!-- New - apply concepts to all transactions with a crud view -->
                <xsl:when test="$theConcepts[not(@transactionId)]">
                    <xsl:for-each select="../view[@type = 'crud'][@transactionId]">
                        <xsl:variable name="transactionId" select="@transactionId"/>
                        <xsl:variable name="transactionEffectiveDate" select="@transactionEffectiveDate"/>
                        <xsl:variable name="transaction" select="$transactions//dataset[@transactionId = $transactionId][@transactionEffectiveDate = $transactionEffectiveDate]" as="element()?"/>
                        <dataset>
                            <xsl:copy-of copy-namespaces="no" select="$transaction/@*"/>
                            <xsl:if test="count($transactions//dataset[@shortName = $transaction/@shortName]) gt 1">
                                <xsl:attribute name="shortName">
                                    <xsl:call-template name="shortName">
                                        <xsl:with-param name="name" select="(name[@language = $lang], name)[1]"/>
                                    </xsl:call-template>
                                    <xsl:text>_</xsl:text>
                                    <xsl:value-of select="$transaction/@shortName"/>
                                </xsl:attribute>
                            </xsl:if>
                            <xsl:call-template name="copyConcept">
                                <xsl:with-param name="overrideDatatypeDateTimeAsString" select="$overrideDatatypeDateTimeAsString"/>
                                <xsl:with-param name="overrideDatatypeTextAsString" select="$overrideDatatypeTextAsString"/>
                                <xsl:with-param name="overrideContainsAsIdentifier" select="$overrideContainsAsIdentifier"/>
                                <xsl:with-param name="datasetConcepts" select="$transaction"/>
                                <xsl:with-param name="viewConcepts" select="$theConcepts"/>
                                <xsl:with-param name="lang" select="$lang"/>
                            </xsl:call-template>
                        </dataset>
                    </xsl:for-each>
                </xsl:when>
                <!-- New - apply indexOf/concepts to all transactions with a crud view -->
                <xsl:when test="indexOf[concepts]">
                    <xsl:for-each select="indexOf[concepts]">
                        <xsl:variable name="viewRef" select="@ref"/>
                        <xsl:variable name="view" select="../../view[@id = $viewRef][@transactionId]" as="element(view)"/>
                        <xsl:variable name="transactionId" select="$view/@transactionId"/>
                        <xsl:variable name="transactionEffectiveDate" select="$view/@transactionEffectiveDate"/>
                        <xsl:variable name="transaction" select="$transactions//dataset[@transactionId = $transactionId][@transactionEffectiveDate = $transactionEffectiveDate]" as="element()?"/>
                        <dataset>
                            <xsl:copy-of copy-namespaces="no" select="$transaction/@*"/>
                            <xsl:if test="count($transactions//dataset[@shortName = $transaction/@shortName]) gt 1">
                                <xsl:attribute name="shortName">
                                    <xsl:call-template name="shortName">
                                        <xsl:with-param name="name" select="(name[@language = $lang], name)[1]"/>
                                    </xsl:call-template>
                                    <xsl:text>_</xsl:text>
                                    <xsl:value-of select="$transaction/@shortName"/>
                                </xsl:attribute>
                            </xsl:if>
                            <xsl:call-template name="copyConcept">
                                <xsl:with-param name="overrideDatatypeDateTimeAsString" select="$overrideDatatypeDateTimeAsString"/>
                                <xsl:with-param name="overrideDatatypeTextAsString" select="$overrideDatatypeTextAsString"/>
                                <xsl:with-param name="overrideContainsAsIdentifier" select="$overrideContainsAsIdentifier"/>
                                <xsl:with-param name="datasetConcepts" select="$transaction"/>
                                <xsl:with-param name="viewConcepts" select="concepts"/>
                                <xsl:with-param name="lang" select="$lang"/>
                            </xsl:call-template>
                        </dataset>
                    </xsl:for-each>
                </xsl:when>
                <!-- New - apply concepts to respective transactions -->
                <xsl:when test="$theConcepts[@transactionId]">
                    <xsl:for-each select="$theConcepts[@transactionId]">
                        <xsl:variable name="transactionId" select="@transactionId"/>
                        <xsl:variable name="transactionEffectiveDate" select="@transactionEffectiveDate"/>
                        <xsl:variable name="transaction" select="$transactions//dataset[@transactionId = $transactionId][@transactionEffectiveDate = $transactionEffectiveDate]" as="element()?"/>
                        <dataset>
                            <xsl:copy-of copy-namespaces="no" select="$transaction/@*"/>
                            <xsl:if test="count($transactions//dataset[@shortName = $transaction/@shortName]) gt 1">
                                <xsl:attribute name="shortName">
                                    <xsl:call-template name="shortName">
                                        <xsl:with-param name="name" select="(name[@language = $lang], name)[1]"/>
                                    </xsl:call-template>
                                    <xsl:text>_</xsl:text>
                                    <xsl:value-of select="$transaction/@shortName"/>
                                </xsl:attribute>
                            </xsl:if>
                            <xsl:call-template name="copyConcept">
                                <xsl:with-param name="overrideDatatypeDateTimeAsString" select="$overrideDatatypeDateTimeAsString"/>
                                <xsl:with-param name="overrideDatatypeTextAsString" select="$overrideDatatypeTextAsString"/>
                                <xsl:with-param name="overrideContainsAsIdentifier" select="$overrideContainsAsIdentifier"/>
                                <xsl:with-param name="datasetConcepts" select="$transaction"/>
                                <xsl:with-param name="viewConcepts" select="."/>
                                <xsl:with-param name="lang" select="$lang"/>
                            </xsl:call-template>
                        </dataset>
                    </xsl:for-each>
                </xsl:when>
            </xsl:choose>
            <xsl:copy-of copy-namespaces="no" select="controls"/>
        </view>
    </xsl:template>

    <xsl:template name="copyConcept">
        <xsl:param name="overrideDatatypeDateTimeAsString" as="xs:string?"/>
        <xsl:param name="overrideDatatypeTextAsString" as="xs:string?"/>
        <xsl:param name="overrideContainsAsIdentifier" as="xs:string?"/>
        <xsl:param name="datasetConcepts" as="element()?"/>
        <xsl:param name="viewConcepts" as="element()?"/>
        <xsl:param name="lang" as="xs:string" required="yes"/>
        <xsl:for-each select="$datasetConcepts/concept">
            <xsl:variable name="id" select="@id"/>
            <xsl:variable name="ed" select="@effectiveDate"/>
            <xsl:variable name="containedid" select="contains/@ref" as="item()?"/>
            <xsl:variable name="containeded" select="contains/@flexibility" as="item()?"/>
            <xsl:variable name="viewConcept" select="$viewConcepts/concept[@ref = $id]"/>
            <xsl:variable name="precedingViewConcept" select="$viewConcepts/concept[@preceding-ref = $id]"/>
            <xsl:variable name="followingViewConcept" select="$viewConcepts/concept[@following-ref = $id]"/>
            <!-- if this concept is a group and 'contains', and content is not merged into the fullDatasetTree, nor in the viewConcept, 
                then assume we need to be able to reference some other object in the xforms. For that we need the group to act as an 
                item of type identifier -->
            <xsl:variable name="treatAsReference" select="exists(.[@type = 'group'][contains][not(concept | valueDomain | valueSet | $viewConcept[concept | valueDomain | valueSet])])" as="xs:boolean"/>
            
            <!-- This helps us to determine if concept X is being referred to from within our context -->
            <xsl:variable name="shouldHaveAdaId" select="exists($containElements[@ref = $id][@flexibility = $ed])" as="xs:boolean"/>
            
            <!-- Do any concepts marked with preceding-ref -->
            <xsl:if test="$precedingViewConcept[concept]">
                <xsl:call-template name="copyConcept">
                    <xsl:with-param name="overrideDatatypeDateTimeAsString" select="$overrideDatatypeDateTimeAsString"/>
                    <xsl:with-param name="overrideDatatypeTextAsString" select="$overrideDatatypeTextAsString"/>
                    <xsl:with-param name="overrideContainsAsIdentifier" select="$overrideContainsAsIdentifier"/>
                    <xsl:with-param name="datasetConcepts" select="$precedingViewConcept"/>
                    <xsl:with-param name="viewConcepts" as="element()">
                        <concepts include="all"/>
                    </xsl:with-param>
                    <xsl:with-param name="lang" select="$lang"/>
                </xsl:call-template>
            </xsl:if>
            
            <xsl:choose>
                <xsl:when test="$viewConcepts/@include = 'all' or @id = $viewConcepts/concept/@ref">
                    <xsl:variable name="adaCommunityConcept" as="element()">
                        <xsl:variable name="communityInfo" select="community[@name = 'ada']/data" as="element()*"/>
                        <!-- Merge properties from ADA community (potentially multiple) -->
                        <concept>
                            <!-- First: process stuff from a foreign community (only possible when the transaction is from project A and dataset is from project B) -->
                            <xsl:for-each select="$communityInfo[@projectRef[not(. = $prefix)]]">
                                <xsl:attribute name="{@type}" select="./text()"/>
                            </xsl:for-each>
                            <!-- Next, overwriting any previous ones: process stuff from our current community (only relevant when the transaction is from project A and dataset is from project B) -->
                            <xsl:for-each select="$communityInfo[not(@projectRef[. = $prefix])]">
                                <xsl:attribute name="{@type}" select="./text()"/>
                            </xsl:for-each>
                        </concept>
                    </xsl:variable>
                    
                    <xsl:choose>
                        <!-- if viewConcept has max = 0 it overrides anything: don't include this concept
                             if viewConcept does not override anything, then if adaCommunityConcept has max = 0: don't include this concept
                             if viewConcept and adaCommunityConcept do not override anything, then if concept max = 0: don't include this concept
                        -->
                        <xsl:when test="$viewConcept[@maximumMultiplicity = '0'] or                                          ($adaCommunityConcept[@maximumMultiplicity = '0'] and not($viewConcept/@maximumMultiplicity)) or                                          (@maximumMultiplicity = '0'                       and not($viewConcept/@maximumMultiplicity | $adaCommunityConcept/@maximumMultiplicity))">
                            <xsl:message>WARNING: Skipping concept id="<xsl:value-of select="@id | @ref"/>" <xsl:value-of select="(name | $adaCommunityConcept/name)[1]"/> because max = 0</xsl:message>
                        </xsl:when>
                        
                        <!-- we find a concept/@ref in $datasetConcepts/concepts, typically because this has been explicitly added at a 'random' location in ada definitions -->
                        <xsl:when test="@ref and not(@id)">
                            <!-- let's find the real transaction concept first -->
                            <xsl:variable name="transactionConcept" as="element()?">
                                <concepts>
                                    <xsl:sequence select="$transactions/*[@transactionId = current()/ancestor::view/@transactionId][@transactionEffectiveDate = current()/ancestor::view/@transactionEffectiveDate]//concept[@id = current()/@ref]"/>
                                </concepts>
                            </xsl:variable>
                            
                            <!-- store the current actually defined transactionConcept als ViewConcepts -->
                            <!-- this may be a group that has further overrides, but the copyConcept template expects a flat list, so we add the concept children in this 'flat list' here -->
                            <xsl:variable name="currentViewConcepts" as="element()?">
                                <concepts include="all">
                                    <xsl:copy-of select="."/>
                                    <xsl:copy-of select="concept"/>
                                </concepts>
                            </xsl:variable>
                            
                            <!-- and call the default copy template to allow for all normal functionalities -->
                            <xsl:call-template name="copyConcept">
                                <xsl:with-param name="overrideDatatypeDateTimeAsString" select="$overrideDatatypeDateTimeAsString"/>
                                <xsl:with-param name="overrideDatatypeTextAsString" select="$overrideDatatypeTextAsString"/>
                                <xsl:with-param name="overrideContainsAsIdentifier" select="$overrideContainsAsIdentifier"/>
                                <xsl:with-param name="datasetConcepts" select="$transactionConcept"/>
                                <xsl:with-param name="viewConcepts" select="$currentViewConcepts"/>
                                <xsl:with-param name="lang" select="$lang"/>
                            </xsl:call-template>
                        </xsl:when>
                        
                        <xsl:otherwise>
                            <xsl:variable name="theConcept" as="element(concept)">
                                <xsl:copy copy-namespaces="no">
                                    <!-- Standard behavior of copy-of is to override existing atts with the same name.
                                     So we copy for this concept: 1) decor atts 2) myCommunity ada atts 3) {project}-ada.xml atts
                                     Thus 3) will take precedence over the 2), and 2) over 1)
                                -->
                                    <xsl:copy-of copy-namespaces="no" select="@*"/>
                                    <!-- Override potential minimumMultiplicity when the concept is optional. It might be what we 
                                    want in the transaction views, but not in the ADA UI. We want conditional concepts to be optional.
                                    We do this before any potential secondary overrides from ADA Community so that those are respected.
                                -->
                                    <xsl:if test="@maximumMultiplicity = '0' or (@minimumMultiplicity and @minimumMultiplicity != '0' and @conformance = 'C')">
                                        <xsl:attribute name="minimumMultiplicity">0</xsl:attribute>
                                    </xsl:if>
                                    <xsl:if test="$shouldHaveAdaId">
                                        <!-- This can be overruled through community or ada config -->
                                        <xsl:attribute name="adaId">true</xsl:attribute>
                                    </xsl:if>
                                    <xsl:copy-of copy-namespaces="no" select="$adaCommunityConcept/(@* except @valueDomainType)"/>
                                    <xsl:copy-of copy-namespaces="no" select="$viewConcept/(@* except @ref)"/>
                                    
                                    <!-- If we want to be able to use the referring mechanism properly in the concept we created at the beginning of the xsl:otherwise, this group cannot be required -->
                                    <xsl:variable name="theAdaId" as="element()">
                                        <ada>
                                            <xsl:copy-of copy-namespaces="no" select="@adaIdAndConceptRef"/>
                                            <xsl:copy-of copy-namespaces="no" select="$adaCommunityConcept/@adaIdAndConceptRef"/>
                                            <xsl:copy-of copy-namespaces="no" select="$viewConcept/@adaIdAndConceptRef"/>
                                        </ada>
                                    </xsl:variable>
                                    <xsl:if test="$theAdaId/@adaIdAndConceptRef = 'true'">
                                        <xsl:attribute name="adaId">true</xsl:attribute>
                                        <xsl:attribute name="minimumMultiplicity" select="0"/>
                                        <xsl:attribute name="conformance" select="'R'"/>
                                        <xsl:attribute name="isMandatory" select="false()"/>
                                        <xsl:if test="@maximumMultiplicity = '1'">
                                            <xsl:attribute name="notPresentWhen">
                                                <xsl:text>../*[@conceptId = '1.999.</xsl:text>
                                                <xsl:value-of select="@id"/>
                                                <xsl:text>'][not(@hidden)]</xsl:text>
                                            </xsl:attribute>
                                        </xsl:if>
                                    </xsl:if>

                                    <xsl:if test="$treatAsReference">
                                        <xsl:attribute name="type">item</xsl:attribute>
                                    </xsl:if>

                                    <!-- These might be missing from input if they are view concepts and the author forgot them in the definition file -->
                                    <xsl:if test="not(@shortName)">
                                        <xsl:attribute name="shortName">
                                            <xsl:call-template name="shortName">
                                                <xsl:with-param name="name" select="(name[@language = $lang], name)[1]"/>
                                            </xsl:call-template>
                                        </xsl:attribute>
                                    </xsl:if>
                                    <xsl:if test="not(implementation)">
                                        <implementation>
                                            <xsl:attribute name="shortName">
                                                <xsl:call-template name="shortName">
                                                    <xsl:with-param name="name" select="(name[@language = $lang], name)[1]"/>
                                                </xsl:call-template>
                                            </xsl:attribute>
                                        </implementation>
                                    </xsl:if>

                                    <xsl:if test="$viewConcept/@notPresentWhen and (@minimumMultiplicity != '0' and not(@conformance = 'C'))">
                                        <xsl:message terminate="no">WARNING: notPresentWhen on <xsl:value-of select="(@shortName | implementation/@shortName)[1]"/> and minimumMultiplicity != 0</xsl:message>
                                    </xsl:if>
                                    <!-- Copy everything except concept children and valueSet-->
                                    <xsl:apply-templates select="* except (concept | valueSet | valueDomain)"/>
                                    <!-- Copy children which are introduced in ada definitions -->
                                    <xsl:apply-templates select="$viewConcept/(* except concept)">
                                        <xsl:with-param name="overrideDatatypeDateTimeAsString" select="$overrideDatatypeDateTimeAsString"/>
                                        <xsl:with-param name="overrideDatatypeTextAsString" select="$overrideDatatypeTextAsString"/>
                                        <xsl:with-param name="overrideContainsAsIdentifier" select="$overrideContainsAsIdentifier"/>
                                    </xsl:apply-templates>
                                    <!-- Copy first valueDomain.
                                    If a valueDomain is defined in ADA definition, this will take precendence -->
                                    <xsl:choose>
                                        <!-- Handle valueDomain override -->
                                        <xsl:when test="$viewConcept/valueDomain">
                                            <!-- Already handled above -->
                                        </xsl:when>
                                        <!-- Handle dataset valueDomain -->
                                        <xsl:when test="valueDomain">
                                            <xsl:apply-templates select="valueDomain">
                                                <xsl:with-param name="overrideDatatypeDateTimeAsString" select="$overrideDatatypeDateTimeAsString"/>
                                                <xsl:with-param name="overrideDatatypeTextAsString" select="$overrideDatatypeTextAsString"/>
                                                <xsl:with-param name="overrideContainsAsIdentifier" select="$overrideContainsAsIdentifier"/>
                                                <xsl:with-param name="adaCommunityConcept" select="$adaCommunityConcept" as="element()"/>
                                            </xsl:apply-templates>
                                        </xsl:when>
                                        <!-- Handle concept with contains, replace that with a valueDomain of type identifier so we can reference that object/concept -->
                                        <xsl:when test="$treatAsReference and $overrideContainsAsIdentifier = 'true'">
                                            <valueDomain type="identifier"/>
                                        </xsl:when>
                                        <!-- Final fallback for concept items that do not have a valueDomain -->
                                        <xsl:when test="@type = 'item' or not(concept)">
                                            <valueDomain type="string"/>
                                        </xsl:when>
                                    </xsl:choose>

                                    <!-- Copy first valueSet, no support for multiple valueSets with values yet.
                                    If a valueSet is defined in ADA definition, this will take precedence -->
                                    <xsl:if test="not($viewConcept/valueSet)">
                                        <xsl:choose>
                                            <xsl:when test="valueSet/conceptList[concept | exception]">
                                                <xsl:call-template name="mergeValueSets">
                                                    <xsl:with-param name="valueSets" select="valueSet" as="element()*"/>
                                                </xsl:call-template>
                                                <!--<xsl:copy-of copy-namespaces="no" select="valueSet[conceptList[concept | exception]][1]"/>-->
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:copy-of copy-namespaces="no" select="valueSet"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:if>
                                    
                                    <!-- if there is an additional concept[@id] in the definition file compared to the original transaction, handle that now -->
                                    <xsl:if test="$viewConcept[concept[@id]]">
                                        <xsl:call-template name="copyConcept">
                                            <xsl:with-param name="overrideDatatypeDateTimeAsString" select="$overrideDatatypeDateTimeAsString"/>
                                            <xsl:with-param name="overrideDatatypeTextAsString" select="$overrideDatatypeTextAsString"/>
                                            <xsl:with-param name="overrideContainsAsIdentifier" select="$overrideContainsAsIdentifier"/>
                                            <xsl:with-param name="datasetConcepts" select="$viewConcept"/>
                                            <xsl:with-param name="viewConcepts" as="element()">
                                                <concepts include="all"/>
                                            </xsl:with-param>
                                            <xsl:with-param name="lang" select="$lang"/>
                                        </xsl:call-template>
                                    </xsl:if>

                                    <xsl:call-template name="copyConcept">
                                        <xsl:with-param name="overrideDatatypeDateTimeAsString" select="$overrideDatatypeDateTimeAsString"/>
                                        <xsl:with-param name="overrideDatatypeTextAsString" select="$overrideDatatypeTextAsString"/>
                                        <xsl:with-param name="overrideContainsAsIdentifier" select="$overrideContainsAsIdentifier"/>
                                        <xsl:with-param name="datasetConcepts" select="."/>
                                        <xsl:with-param name="viewConcepts" select="$viewConcepts"/>
                                        <xsl:with-param name="lang" select="$lang"/>
                                    </xsl:call-template>
                                </xsl:copy>
                            </xsl:variable>
                            
                            <!-- Whenever you add adaId to a group, we may assume that you also want to refer to it. The usual dataset is not particularly suitable for ADA this way, 
                                so we add an extra concept before such a group concept that would allow us to refer to exactly this group from the same context in the dataset tree
                                When we do this, the group itself has to be minmumMuliplicity 0, as does the referringConcept otherwise we still to populate, instead of reference
                            -->
                            <xsl:if test="$theConcept[@adaIdAndConceptRef = 'true'][concept]">
                                <xsl:variable name="refererringConcept" as="element(concept)">
                                    <concept preceding-ref="{@id}">
                                        <xsl:for-each select="$theConcept">
                                            <concept>
                                                <xsl:copy-of select="@* except (@adaId | @adaIdAndConceptRef | @widget | @initial)"/>
                                                <xsl:attribute name="id" select="concat('1.999.', @id)"/>
                                                <xsl:attribute name="type" select="'item'"/>
                                                <xsl:attribute name="minimumMultiplicity" select="0"/>
                                                <xsl:if test="@conformance = 'M'">
                                                    <xsl:attribute name="conformance" select="'R'"/>
                                                </xsl:if>
                                                <xsl:attribute name="isMandatory" select="false()"/>
                                                <xsl:if test="@maximumMultiplicity = '1'">
                                                    <xsl:attribute name="notPresentWhen">
                                                        <xsl:text>../*[@conceptId = '</xsl:text>
                                                        <xsl:value-of select="@id"/>
                                                        <xsl:text>'][not(@hidden)]</xsl:text>
                                                    </xsl:attribute>
                                                </xsl:if>
                                                <xsl:for-each select="name">
                                                    <xsl:copy>
                                                        <xsl:copy-of select="@*"/>
                                                        <xsl:value-of select="normalize-space(.)"/>
                                                        <xsl:text>Ref</xsl:text>
                                                    </xsl:copy>
                                                </xsl:for-each>
                                                <xsl:for-each select="name">
                                                    <desc>
                                                        <xsl:copy-of select="@*"/>
                                                        <xsl:value-of select="."/>
                                                        <xsl:text>: </xsl:text>
                                                        <xsl:choose>
                                                            <xsl:when test="$theConcept/@maximumMultiplicity = '1'">
                                                                <xsl:value-of select="ada:getMessage('Contains a reference description single')"/>
                                                            </xsl:when>
                                                            <xsl:otherwise>
                                                                <xsl:value-of select="ada:getMessage('Contains a reference description multiple')"/>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                        <xsl:text> </xsl:text>
                                                        <xsl:copy-of select="(../desc[@language = current/@language], ../desc)[1]/node()"/>
                                                    </desc>
                                                </xsl:for-each>
                                                <contains ref="{@id}" flexibility="{@effectiveDate}"/>
                                                <valueDomain type="string"/>
                                            </concept>
                                        </xsl:for-each>
                                    </concept>
                                </xsl:variable>
                                
                                <xsl:for-each select="$refererringConcept">
                                    <xsl:call-template name="copyConcept">
                                        <xsl:with-param name="overrideDatatypeDateTimeAsString" select="$overrideDatatypeDateTimeAsString"/>
                                        <xsl:with-param name="overrideDatatypeTextAsString" select="$overrideDatatypeTextAsString"/>
                                        <xsl:with-param name="overrideContainsAsIdentifier" select="$overrideContainsAsIdentifier"/>
                                        <xsl:with-param name="datasetConcepts" select="$refererringConcept"/>
                                        <xsl:with-param name="viewConcepts" as="element()">
                                            <concepts include="all"/>
                                        </xsl:with-param>
                                        <xsl:with-param name="lang" select="$lang"/>
                                    </xsl:call-template>
                                </xsl:for-each>
                            </xsl:if>
                            
                            <xsl:copy-of select="$theConcept"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="copyConcept">
                        <xsl:with-param name="overrideDatatypeDateTimeAsString" select="$overrideDatatypeDateTimeAsString"/>
                        <xsl:with-param name="overrideDatatypeTextAsString" select="$overrideDatatypeTextAsString"/>
                        <xsl:with-param name="overrideContainsAsIdentifier" select="$overrideContainsAsIdentifier"/>
                        <xsl:with-param name="datasetConcepts" select="."/>
                        <xsl:with-param name="viewConcepts" select="$viewConcepts"/>
                        <xsl:with-param name="lang" select="$lang"/>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
            
            <!-- Do any concepts marked with following-ref -->
            <xsl:if test="$followingViewConcept[concept]">
                <xsl:call-template name="copyConcept">
                    <xsl:with-param name="overrideDatatypeDateTimeAsString" select="$overrideDatatypeDateTimeAsString"/>
                    <xsl:with-param name="overrideDatatypeTextAsString" select="$overrideDatatypeTextAsString"/>
                    <xsl:with-param name="overrideContainsAsIdentifier" select="$overrideContainsAsIdentifier"/>
                    <xsl:with-param name="datasetConcepts" select="$followingViewConcept"/>
                    <xsl:with-param name="viewConcepts" as="element()">
                        <concepts include="all"/>
                    </xsl:with-param>
                    <xsl:with-param name="lang" select="$lang"/>
                </xsl:call-template>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="valueDomain">
        <xsl:param name="overrideDatatypeDateTimeAsString" as="xs:string?"/>
        <xsl:param name="overrideDatatypeTextAsString" as="xs:string?"/>
        <xsl:param name="overrideContainsAsIdentifier" as="xs:string?"/>
        <xsl:param name="adaCommunityConcept" as="element()?"/>
        
        <xsl:copy copy-namespaces="no">
            <xsl:copy-of copy-namespaces="no" select="@*"/>
            <xsl:choose>
                <xsl:when test="$adaCommunityConcept/@valueDomainType">
                    <xsl:attribute name="type" select="$adaCommunityConcept/@valueDomainType"/>
                    <xsl:attribute name="originaltype" select="@type"/>
                </xsl:when>
                <xsl:when test="$overrideDatatypeDateTimeAsString = 'true' and @type = ('time', 'date', 'datetime')">
                    <xsl:attribute name="type">string</xsl:attribute>
                    <xsl:attribute name="originaltype" select="@type"/>
                    <xsl:comment>converted to string to support things like T-500 or other syntaxes</xsl:comment>
                </xsl:when>
                <xsl:when test="$overrideDatatypeTextAsString = 'true' and @type = ('text')">
                    <xsl:attribute name="type">string</xsl:attribute>
                    <xsl:attribute name="originaltype" select="@type"/>
                    <xsl:comment>converted to string based on global overrideDatatypeTextAsString</xsl:comment>
                </xsl:when>
            </xsl:choose>
            <xsl:copy-of copy-namespaces="no" select="node()"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template name="mergeValueSets">
        <xsl:param name="valueSets" as="element(valueSet)*"/>
        
        <xsl:choose>
            <!-- if there's only one none at all, just copy $valueSets -->
            <xsl:when test="count($valueSets) le 1">
                <xsl:copy-of select="$valueSets"/>
            </xsl:when>
            <!-- if there's more than one: merge them -->
            <xsl:otherwise>
                <xsl:variable name="conceptListContent" as="element()*">
                    <xsl:for-each select="$valueSets[completeCodeSystem | conceptList/*]">
                        <xsl:variable name="p" select="position()"/>
                        <concept localId="_{$p}" code="_{@name}" codeSystem="1.2.3.999.5.999" displayName="{(@displayName, @name)[1]}" level="0" type="A">
                            <name>
                                <xsl:value-of select="(@displayName, @name)[1]"/>
                            </name>
                        </concept>
                        <xsl:copy-of select="conceptList/*"/>
                    </xsl:for-each>
                </xsl:variable>
                
                <xsl:comment> Merged Value Set. The attributes are from the first value set, the contents are compiled from all value sets </xsl:comment>
                <valueSet>
                    <xsl:copy-of select="$valueSets[1]/@*"/>
                    <!-- Copies all completeCodeSystem elements as the most important part, but also desc/copyright etc. -->
                    <xsl:copy-of select="$valueSets/(node() except conceptList)"/>
                    <xsl:if test="$conceptListContent">
                        <conceptList>
                            <xsl:for-each select="$conceptListContent">
                                <xsl:copy>
                                    <xsl:copy-of select="@*"/>
                                    <!-- overwrite duplicates that would otherwise occur. -->
                                    <xsl:if test="@localId">
                                        <xsl:attribute name="localId" select="position()"/>
                                    </xsl:if>
                                    <xsl:copy-of select="node()"/>
                                </xsl:copy>
                            </xsl:for-each>
                        </conceptList>
                    </xsl:if>
                </valueSet>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="@*|node()">
        <xsl:copy copy-namespaces="no">
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>