<?xml version="1.0" encoding="UTF-8"?>
<!-- 
Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

This program is free software; you can redistribute it and/or modify it under the terms 
of the GNU Affero General Public Licenseas published by the Free Software Foundation; 
either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the GNU Affero General Public Licensefor more details.

See http://www.gnu.org/licenses/
-->
<xsl:stylesheet xmlns:ada="http://www.art-decor.org/ada"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="2.0">
    <xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>
    <xsl:include href="ada-basics.xsl"/>
    <xsl:template match="/">
        <xsl:comment>Empty instance generator version 1, <xsl:value-of select="current-dateTime()"/>
        </xsl:comment>
        <xsl:apply-templates select="ada//view[@type = 'crud']/dataset"/>
    </xsl:template>
    <xsl:template match="dataset[@shortName]">
        <xsl:variable name="href" select="concat($projectDiskRoot, 'new/', ../implementation/@shortName, '.xml')"/>
        <xsl:result-document href="{$href}" method="xml">
            <xsl:comment>New XML generator, <xsl:value-of select="current-dateTime()"/>
            </xsl:comment>
            <xsl:element name="{@shortName}">
                <xsl:attribute name="id">new</xsl:attribute>
                <xsl:attribute name="app">
                    <xsl:value-of select="$projectName"/>
                </xsl:attribute>
                <!-- relevant for apps with multiple cruds for different transactions -->
                <xsl:copy-of select="@shortName"/>
                <xsl:attribute name="formName">
                    <xsl:copy-of select="parent::view/implementation/@shortName"/>
                </xsl:attribute>
                <xsl:attribute name="transactionRef">
                    <xsl:value-of select="@transactionId"/>
                </xsl:attribute>
                <xsl:copy-of select="@transactionEffectiveDate"/>
                <xsl:copy-of select="/ada/project/@versionDate"/>
                <xsl:copy-of select="/ada/project/@prefix"/>
                <xsl:copy-of select="/ada/project/@language"/>
                <xsl:attribute name="title"/>
                <xsl:attribute name="desc"/>
                <xsl:apply-templates select="concept[@type]"/>
            </xsl:element>
        </xsl:result-document>
    </xsl:template>
    <xsl:template match="concept">
        <!--  All 0..x concepts start with a row with @hidden (what value it contains is irrelevant, if @hidden is present, the row is hidden).
              This serves a as point to attach new conceptss when all are removed.
        -->
        <xsl:if test="@minimumMultiplicity = '0'">
            <xsl:element name="{implementation/@shortName}-start">
                <xsl:attribute name="hidden">
                    <xsl:value-of select="'true'"/>
                </xsl:attribute>
                <xsl:attribute name="conceptId">
                    <xsl:value-of select="@id"/>
                </xsl:attribute>
            </xsl:element>
        </xsl:if>
        <xsl:apply-templates select="." mode="doConceptContent"/>
    </xsl:template>
    <xsl:template match="concept[@type = 'group']" mode="doConceptContent">
        <xsl:element name="{implementation/@shortName}">
            <xsl:attribute name="conceptId">
                <xsl:value-of select="@id"/>
            </xsl:attribute>
            <!-- Add @id if @adaId='true' -->
            <!-- 2020-02-19 Deactivated. Adding it here causes mix-in upon every re-open of the instance and causes the headache of first having to remove 
                them again before normal save is possible. The XForm already allow adding an id where appicable so mix-in through new xml is not required.
            -->
            <!--<xsl:if test="@adaId = 'true'">
                <xsl:attribute name="id"/>
            </xsl:if>-->
            <xsl:if test="ancestor::view/@addComments='true'">
                <xsl:attribute name="comment"/>
            </xsl:if>
            <xsl:apply-templates select="concept[@type]"/>
            <xsl:element name="{$extensionElementName}-start">
                <xsl:attribute name="hidden" select="'true'"/>
            </xsl:element>
            <xsl:element name="{$extensionElementName}"/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="concept[@type = 'item']" mode="doConceptContent">
        <xsl:variable name="isMandatory" select="ada:isMandatory(.)"/>
        <xsl:variable name="isRequired" select="ada:isRequired(.)"/>
        
        <xsl:variable name="fixedCode" as="element()?">
            <xsl:if test="valueDomain[@type = ('code', 'ordinal')] and valueSet[empty(completeCodeSystem | conceptList/include | conceptList/exclude)] and count(valueSet/conceptList/concept/@localId | valueSet/conceptList/exception/@localId) = 1">
                <xsl:copy-of select="valueSet/conceptList/concept| valueSet/conceptList/exception"/>
            </xsl:if>
        </xsl:variable>
        <xsl:element name="{implementation/@shortName}">
            <!-- @value -->
            <xsl:attribute name="value">
                <xsl:choose>
                    <xsl:when test="not(empty(@default))">
                        <xsl:value-of select="@default"/>
                    </xsl:when>
                    <!-- if there is only 1 choice -->
                    <xsl:when test="$fixedCode">
                        <xsl:value-of select="$fixedCode/@localId"/>
                    </xsl:when>
                    <!-- 20191105 Deactivated this part based on user feedback that it should be a conscious decision 
                        of a user to activate a field. The mandatory field could be 7 optional groups deep, which is 
                        hard to track in larger datasets 
                        Hence: if a user forgets to activate/populate mandatory fields it is better to have him solve 
                        those rather than the other way around where he may save inadvertent defaults
                    -->
                    <!-- Provide defaults for data which may not be empty. Not doing this leads to errors. -->
                    <!--<xsl:when test="$isMandatory">
                        <xsl:choose>
                            <xsl:when test="valueSet/conceptList/(concept | exception)">
                                <xsl:value-of select="(valueSet/conceptList/(concept | exception))[1]/@localId"/>
                            </xsl:when>
                            <xsl:when test="valueDomain/@type = 'boolean'">false</xsl:when>
                            <xsl:when test="valueDomain/@type = 'count'">0</xsl:when>
                            <xsl:when test="valueDomain/@type = 'decimal'">0</xsl:when>
                            <xsl:when test="valueDomain/@type = 'quantity'">0</xsl:when>
                            <xsl:when test="valueDomain/@type = 'date'">
                                <xsl:value-of select="substring(xs:string(current-dateTime()), 1, 10)"/>
                            </xsl:when>
                            <xsl:when test="valueDomain/@type = 'dateTime'">
                                <xsl:value-of select="current-dateTime()"/>
                            </xsl:when>
                        </xsl:choose>
                    </xsl:when>-->
                </xsl:choose>
            </xsl:attribute>
            <!-- @datatype -->
            <xsl:choose>
                <!-- Nothing more to report if there is no valueDomain or if there are multiple -->
                <xsl:when test="count(valueDomain) != 1"/>
                <!-- Add fixed=reference if contains and valueDomain = 'string' -->
                <xsl:when test="valueDomain/@type = 'string' and contains">
                    <xsl:attribute name="datatype" select="'reference'"/>
                </xsl:when>
                <!-- Add fixed=@originaltype if different from @type -->
                <xsl:when test="valueDomain[@type][@originaltype][not(@type = @originaltype)]">
                    <xsl:attribute name="datatype" select="(valueDomain/@originaltype)[1]"/>
                </xsl:when>
                <!-- Add default @datatype=complex -->
                <xsl:when test="valueDomain[@type = 'complex']">
                    <xsl:attribute name="datatype" select="(valueDomain/@type)[1]"/>
                </xsl:when>
                <!-- Don't populate by default? -->
                <xsl:otherwise>
                    <!--<xsl:attribute name="datatype" select="(valueDomain/@type)[1]"/>-->
                </xsl:otherwise>
            </xsl:choose>
            
            <!-- @unit -->
            <xsl:if test="valueDomain[@type = 'quantity'] | valueDomain[@type = 'duration'] | valueDomain[property/@unit]">
                <xsl:attribute name="unit">
                    <xsl:value-of select="(valueDomain/property/@unit)[1]"/>
                </xsl:attribute>
            </xsl:if>
            <!-- @root -->
            <xsl:if test="valueDomain[@type = 'identifier']">
                <xsl:attribute name="root">
                    <xsl:value-of select="(identifierAssociation/@ref)[1]"/>
                </xsl:attribute>
            </xsl:if>
            
            <!-- We don't have any relevant bindings or inline concept to use for @value, so we need explicit code/codeSystem/displayName options to give to the XForm -->
            <!-- @code | @codeSystem | @codeSystemName | @codeSystemVersion | @displayName | @ordinal -->
            <!--<xsl:if test="valueDomain[@type = ('code', 'ordinal')] and not(valueDomain/conceptList/(concept | exception) | valueSet/conceptList/(concept | exception))">-->
            <xsl:if test="valueDomain[@type = ('code', 'ordinal')] and (valueSet[completeCodeSystem | conceptList/include | conceptList/exclude] or not(valueDomain/conceptList/(concept | exception) | valueSet/conceptList/(concept | exception)))">
                <xsl:attribute name="code"/>
                <xsl:attribute name="codeSystem">
                    <xsl:if test="count(valueSet/sourceCodeSystem) = 1">
                        <xsl:value-of select="valueSet/sourceCodeSystem/@id"/>
                    </xsl:if>
                </xsl:attribute>
                <xsl:attribute name="codeSystemName">
                    <xsl:if test="count(valueSet/sourceCodeSystem) = 1">
                        <xsl:value-of select="valueSet/sourceCodeSystem/@identifierName"/>
                    </xsl:if>
                </xsl:attribute>
                <xsl:attribute name="codeSystemVersion"/>
                <xsl:attribute name="displayName">
                    <!-- Do not add valueDomain as displayName if there are valueSet entries for the dropdown based on @value
                        If there are no valueDomain concepts, then this value-of yields nothing -->
                    <xsl:if test="not(valueSet/conceptList/(concept | exception))">
                        <xsl:value-of select="(valueDomain/conceptList/concept/name)[1]"/>
                    </xsl:if>
                </xsl:attribute>
                <xsl:if test="valueDomain[@type = ('ordinal')]">
                    <xsl:attribute name="ordinal"/>
                </xsl:if>
                <xsl:attribute name="preferred"/>
            </xsl:if>
            <!-- in case of a fixed code -->
            <xsl:if test="$fixedCode">
                <xsl:copy-of select="$fixedCode/@code"/>
                <xsl:copy-of select="$fixedCode/@codeSystem"/>
                <xsl:copy-of select="$fixedCode/@codeSystemName"/>
                <xsl:copy-of select="$fixedCode/@codeSystemVersion"/>
                <xsl:copy-of select="$fixedCode/@displayName"/>
            </xsl:if>
            <!-- @originalText -->
            <xsl:if test="valueDomain[@type = ('code', 'ordinal')] and not($isMandatory)">
                <xsl:attribute name="originalText"/>
            </xsl:if>
            <!-- Add every special attribute in case of complex. we do not know what complex is. 
                it could be an observation/value destined for xsi:type -->
            <!-- @unit | @root | @code | @codeSystem | @codeSystemName | @codeSystemVersion | @displayName | @ordinal | @originalText -->
            <xsl:if test="valueDomain/@type = ('complex')">
                <!--<xsl:attribute name="datatype">complex</xsl:attribute>-->
                <xsl:attribute name="unit"/>
                <xsl:attribute name="root"/>
                <xsl:attribute name="code"/>
                <xsl:attribute name="codeSystem"/>
                <xsl:attribute name="codeSystemName"/>
                <xsl:attribute name="codeSystemVersion"/>
                <xsl:attribute name="displayName"/>
                <xsl:attribute name="ordinal"/>
                <xsl:attribute name="preferred"/>
                <xsl:attribute name="originalText"/>
            </xsl:if>
            <!-- Add nullFlavor if not mandatory, code or ordinal (because then it would be in code/codeSystem) -->
            <!-- @nullFlavor -->
            <xsl:if test="not($isMandatory) and $isRequired and not(valueDomain/@type = ('code', 'ordinal'))">
                <xsl:attribute name="nullFlavor"/>
            </xsl:if>
            <!-- Add @id if @adaId='true' -->
            <xsl:choose>
                <xsl:when test="@dobId = 'true'">
                    <xsl:attribute name="id" select="'DOB'"/>
                </xsl:when>
                <xsl:when test="@adaId = 'true'">
                    <xsl:attribute name="id"/>
                </xsl:when>
            </xsl:choose>
            <xsl:attribute name="conceptId" select="@id"/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="text() | @*"/>
</xsl:stylesheet>
