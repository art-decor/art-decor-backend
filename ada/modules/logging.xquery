xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
declare option exist:serialize "method=xhtml media-type=text/html indent=yes";
import module namespace ada ="http://art-decor.org/ns/ada-common" at "ada-common.xqm";

<html>
    <head>
        <title>ADA Logging</title>
        <link rel="stylesheet" type="text/css" href="../resources/css/ada.css"/>
    </head>
    <body class="orbeon ada">
        <h1>ADA Logging</h1>
        <p>Logged in as: {ada:strCurrentUserName()}</p>
        <p>Logging is turned {if ($ada:logOn) then 'ON' else 'OFF'} (Press F5 to refresh)</p>
        <p><a href="toggle-logging.xquery">Turn logging {if ($ada:logOn) then 'OFF' else 'ON'}</a></p>
        <p><a href="clear-log.xquery">Clear log</a></p>
        <table>
        {
            for $resource in xmldb:get-child-resources($ada:strAdaLog)
            let $created := xmldb:created($ada:strAdaLog, $resource)
            order by $created descending
            return 
                <tr>
                    <td>{$created}</td>
                    <td><a href="../../ada-data/log/{$resource}">{$resource}</a></td>
                </tr>
        }
        </table>
    </body>
</html>