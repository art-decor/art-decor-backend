xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace ada     = "http://art-decor.org/ns/ada-common" at "ada-common.xqm";
import module namespace adaxml  = "http://art-decor.org/ns/ada-xml" at "ada-xml.xqm";

declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";

let $id         := if (request:exists()) then request:get-parameter('id','') else 'c1bb078c-21e4-4330-b761-c069afb3b33a'
let $app        := if (request:exists()) then request:get-parameter('app','') else ''
let $data       := ada:getCollection($app, 'data')//*[@app = $app][@id = $id]

return 
    if (string-length($app) = 0 or string-length($id) = 0) then (
        if (request:exists()) then response:set-status-code(400) else (),
        <error>Missing parameter app='{$app}' or id='{$id}'</error>
    )
    else
    if (ada:hasPermission($data, 'delete')) then (
        if (request:exists()) then response:set-status-code(200) else (),
        xmldb:remove(util:collection-name($data), util:document-name($data))
    )
    else (
        if (request:exists()) then response:set-status-code(401) else (),
        <error>Only the creator may delete a record</error>
    )
