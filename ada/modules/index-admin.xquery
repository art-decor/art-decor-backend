xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace ada     = "http://art-decor.org/ns/ada-common" at "ada-common.xqm";
import module namespace adaxml  = "http://art-decor.org/ns/ada-xml" at "ada-xml.xqm";
declare option exist:serialize "method=xhtml media-type=text/html indent=yes";

let $apps       := if (request:exists()) then request:get-parameter('app',())[string-length() gt 0] else 'demoapp'
let $dovalidate := if (request:exists()) then request:get-parameter('validate','false')[string-length() gt 0] = ('true', 'on') else true()
let $dofilter   := if (request:exists()) then request:get-parameter('filter','true')[string-length() gt 0] = ('true', 'on') else true()
let $projects   := xmldb:get-child-collections($ada:strAdaProjects)

let $language   := if (request:exists()) then request:get-parameter('language',())[1] else ()
let $language   := if (string-length($language)=0) then (ada:getLanguage()) else ($language)

return
<html>
    <head>
        <title>{ada:getString('Admin', $language),' ',if (count($apps) = 1) then $apps else ()}</title>
        <!--<script type="text/javascript">
        function showHide(obj) {{
            var h=document.getElementById(obj); if (h.style.display=='block') {{h.style.display='none'}} else {{h.style.display='block'}}
        }}
        </script>-->
        <link rel="stylesheet" type="text/css" href="../resources/css/ada.css"/>
        <script type="text/javascript" src="../resources/scripts/ada.js"/>
    </head>
    <body class="orbeon ada">
        <h1>{ada:getString('Admin', $language)}</h1>
        <table width="100%">
            <tr>
                <th style="width: 20%; vertical-align: top;">{ada:getString('Upload new ADA project version',$language)}</th>
                <td>
                    <img src="/art-decor/img/help.gif" onclick="showHide('upload-help')"/>
                    <form action="upload-ada-project-zip.xquery" method="post" enctype="multipart/form-data" style="display: inline;">
                        <input type="file" id="file" name="file"/>
                        <input type="submit"/>
                    </form>
                    <div id="upload-help" style="display: none;">
                        <ul>
                            <li>Generic documentation: <a href="https://docs.art-decor.org/release2/specials/#art-decor-applications-aka-ada">ART-DECOR ADA</a></li>
                            <li>Expects top level <em>repo.xml</em> and <em>expath-pkg.xml</em> files - generated using release2package.xsl</li>
                            <li>-----</li>
                            <li>Zip the <em>contents</em> of the ADA project folder as <em>[folder].zip</em>, so the zip does not contain the top level folder itself</li>
                            <li><b>Windows 10 or up</b>: In Powershell, <em>go into project directory</em> and use <pre>$output=(Split-Path -Path (Get-Location) -Leaf); Compress-Archive -Path '*' -DestinationPath ../$output.zip</pre></li>
                            <li><b>macOS</b>: make sure the zip does not include macOS specific (hidden) files. In Terminal.app, <em>go into project directory</em> and use <pre>zip -r `basename $PWD`.zip *</pre></li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <th style="width: 20%; vertical-align: top;">{ada:getString('Select',$language)}</th>
                <td>
                    <select id="prefixSelector" 
                        onchange="javascript:location.href=window.location.pathname+'?app='+this.options[this.selectedIndex].value+'&amp;language={$language}&amp;validate='+document.getElementById('validationSelector').checked">
                        <option value="">----</option>
                    {
                        for $project in $projects
                        order by lower-case($project)
                        return
                            <option value="{$project}">
                            {
                                if (count($apps) = 1 and $project = $apps) then attribute selected {'true'} else (), 
                                data($project)
                            }
                            </option>
                    }
                    </select>
                    <input id="validationSelector" type="checkbox" name="validation" style="margin-left: 1em;"
                        onchange="javascript:location.href=window.location.pathname+'?app='+document.getElementById('prefixSelector').options[document.getElementById('prefixSelector').selectedIndex].value+'&amp;language={$language}&amp;validate='+document.getElementById('validationSelector').checked">
                    {
                        if ($dovalidate) then attribute checked {'checked'} else ()
                        ,
                        ada:getString('Do validation', $language)
                    }
                    </input>
                {
                    if ($dovalidate) then (
                        <input id="validationFilter2" type="checkbox" name="validationFilter2" style="margin-left: 1em;"
                            onchange="showHideRows()">
                        {
                            ada:getString('Hide valid', $language)
                        }
                        </input>
                    ) else ()
                }
                </td>
            </tr>
        </table>
        {
            for $project in $apps
            let $xform          := collection(concat($ada:strAdaProjects, $project))//ada[@adaVersion]//view[@type='crud'][@target='xforms']/implementation/@shortName
            let $index          := collection(concat($ada:strAdaProjects, $project))//ada[@adaVersion]//view[@type='index'][1]
            let $indexForm      := $index/implementation/@shortName/string()
            let $indexExists    := exists(xmldb:get-child-resources(concat($ada:strAdaProjects,'/',$project,'/views'))[.=concat($indexForm, '.xhtml')])
            order by lower-case($project)
            return (
                <h2>{$project}</h2>
                ,
                <table style="width: 100%;">
                    <tr>
                        <th style="width: 25%; padding-right: 2px;"><b>{$project}</b></th>
                        <th style="width: 25%; padding-right: 2px;"><a href="../projects/{$project}/">{ada:getString('Search', $language)}</a></th>
                        <th><a href="../projects/{$project}?summary=true">{ada:getString('Summary', $language)}</a></th>
                        <th>
                        {
                            if ($indexExists) then 
                                <a href="{concat($ada:orbeonBaseUri, 'ada-data/projects/', $project, '/views/', $indexForm, '.xhtml')}">{ada:getString('Index', $language)}</a>
                            else (
                                <span style="opacity: 0.5;">{ada:getString('Index', $language)}</span>
                            )
                        }
                        </th>
                        <th colspan="2">{if (count(distinct-values($xform))>1) then () else <a href="../projects/{$project}/new">{ada:getString('New', $language)}</a>}</th>
                        <th colspan="2">{if (count(distinct-values($xform))>1) then () else <a href="{$ada:orbeonBaseUri}ada-data/projects/{$project}/views/{$xform[1]}.xhtml?id=new">{ada:getString('New XForm', $language)}</a>}</th>
                        <th><a href="{$ada:orbeonBaseUri}ada-data/projects/{$project}/definitions/{$project}-ada.xml">{ada:getString('ADA Definitions', $language)}</a></th>
                        <th><a href="{$ada:orbeonBaseUri}ada-data/projects/{$project}/definitions/{$project}-ada-release.xml">{ada:getString('ADA Release', $language)}</a></th>
                        
                        <th><a href="check-coverage.xquery?app={$project}&amp;language={$language}">{ada:getString('Coverage', $language)}</a></th>
                    </tr>
                    <tr>
                        <th>{ada:getString('Id', $language)}</th>
                        <th>{ada:getString('Title', $language)}</th>
                        <th>{ada:getString('Created by', $language)}</th>
                        <th>{ada:getString('Modified date', $language)}</th>
                        <th colspan="6" style="border-left: 1px solid black; border-right: 1px solid black;">{ada:getString('Retrieve', $language)}</th>
                        <th>{ada:getString('Raw XML status', $language)}</th>
                    </tr>
                    {
                        for $shortNames in $ada:colAdaData//*[@app = $project]
                        let $shortName      := $shortNames/@shortName
                        group by $shortName
                        order by $shortName
                        return (
                            <tr style="border: 1px solid black;">
                                <th colspan="11">{data($shortName[1])}</th>
                            </tr>,
                            for $data in $shortNames
                            let $id             := $data/@id/string()
                            let $owner          := $data/ancestor::adaxml/meta/@created-by
                            let $lastmodified   := $data/ancestor::adaxml/meta/@last-update-date
                            let $trid           := $data/@transactionRef
                            let $tred           := $data/@transactionEffectiveDate
                            let $form           := if ($data/@formName) then $data/@formName else ($xform)
                            let $validation     := if ($dovalidate) then adaxml:validateSchema($data) else ()
                            let $rowclass       := if ($dovalidate and $dofilter) then 'row-' || data($validation/status) else ()
                            order by $data/@shortName, $owner[1], $lastmodified descending
                            return
                                <tr class="{$rowclass}">
                                    <td>{$id}</td>
                                    <td>{$data/@title/string()}</td>
                                    <td>{$data/ancestor::adaxml/meta/@created-by/string()}</td>
                                    <td>{if ($lastmodified castable as xs:dateTime) then replace(format-dateTime(xs:dateTime($lastmodified),'[Y0001]-[M01]-[D01] [H01]:[m01]:[s01]', (), (), ()),' 00:00:00','') else ($lastmodified)}</td>
                                    <td><a href="{$ada:orbeonBaseUri}ada-data/projects/{$project}/views/{$form}.xhtml?id={$id}&amp;transactionId={$trid}&amp;transactionEffectiveDate={$tred}">XForm</a></td>
                                    <td><a href="../projects/{$project}/{$id}">REST</a></td>
                                    <td><a href="get-html.xquery?id={$id}&amp;app={$project}">HTML</a></td>
                                    <td><a href="get-data.xquery?id={$id}&amp;app={$project}&amp;raw=true">XML</a></td>
                                    <td><a href="get-json.xquery?id={$id}&amp;app={$project}">JSON</a></td>
                                    <td><a href="validate-data.xquery?id={$id}&amp;app={$project}">{ada:getString('Validate', $language)}</a></td>
                                    <td><span class="validate-{$validation/status}">{data($validation/status)}</span></td>
                                </tr>
                        )
                    }
                </table>
            ,
                <h2>{ada:getString('New XForm', $language)}</h2>
        ,
                <table>
                    {
                        for $xf in $xform 
                        let $formName := $xf/../../name
                        let $trid           := $xf/../../dataset/@transactionId
                        let $tred           := $xf/../../dataset/@transactionEffectiveDate
                        order by lower-case($formName)
                        return
                        <tr>
                            <td><a href="{$ada:orbeonBaseUri}ada-data/projects/{$project}/views/{$xf}.xhtml?id=new&amp;transactionId={$trid}&amp;transactionEffectiveDate={$tred}">{$formName}</a></td>
                        </tr>
                    }
                </table>
            )
        }
    </body>
</html>
