xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace ada = "http://art-decor.org/ns/ada-common" at "ada-common.xqm";
declare option exist:serialize "method=xhtml media-type=text/html indent=yes";

let $language   := if (request:exists()) then request:get-parameter('language',())[1] else ()
let $language   := if (string-length($language)=0) then (ada:getLanguage()) else ($language)
let $format     := if (request:exists()) then request:get-parameter('format','html')[1] else ()

let $ada-apps   := ada:getApplications((), $language)

return
    if ($format = 'xml') then (
        if (response:exists()) then response:set-header('Content-Type', 'application/xml') else (),
        <return>
        {
            $ada-apps
        }
        </return>
    )
    else (
        <html>
            <head>
                <title>ADA {ada:getString('Projects', $language)}</title>
                <link rel="stylesheet" type="text/css" href="../resources/css/ada.css"/>
            </head>
            <body class="orbeon ada">
                <h1>ADA {ada:getString('Projects', $language)}</h1>
                <table>
                {
                    for $ada-app in $ada-apps
                    order by $ada-app
                    return 
                    <tr>
                        <td>
                        {
                            if ($ada-app/@xforms) then 
                                <a href="{concat($ada:orbeonBaseUri, $ada-app/@xforms)}">{data($ada-app/@xformsName)}</a>
                            else 
                            if ($ada-app/@xquery) then
                                <a href="{concat($ada:orbeonBaseUri, $ada-app/@xquery)}">{data($ada-app/@xqueryName)}</a>
                            else 
                                ada:getString('No index defined', $language)
                        }
                        </td>
                        <td><a href="{$ada-app/@admin}">{ada:getString('Admin', $language)}</a></td>
                    </tr>
                }
                </table>
            </body>
        </html>
    )