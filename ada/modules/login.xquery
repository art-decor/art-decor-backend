xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
declare namespace sm           = "http://exist-db.org/xquery/securitymanager";
declare namespace request      = "http://exist-db.org/xquery/request";
declare namespace response     = "http://exist-db.org/xquery/response";

let $test       := request:get-data()/user
let $user       := ($test/username, 'guest')[1]
let $pwd        := $test/password
let $create     := session:create()

return
if (xmldb:authenticate('/db',$user,$pwd)) then
    let $login              := xmldb:login('/db', $user, $pwd, true())
    let $username           := ada:strCurrentUserName()
    let $groups             := sm:get-user-groups($username)
    return
        <user>
            <username>{data($username)}</username>
            <groups>{$groups}</groups>
            <password>{data($pwd)}</password>
            <logged-in>true</logged-in>
        </user>
else (
    let $logout             := session:invalidate()
    return
        <user>
            <username>{data($user)}</username>
            <groups></groups>
            <password></password>
            <logged-in>false</logged-in>
        </user>
)

