xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace ada     = "http://art-decor.org/ns/ada-common" at "ada-common.xqm";
declare option exist:serialize "method=xhtml media-type=text/html indent=yes";

(:~
 : Remove the package identified by its unique name.
 :
 : @return true if the package could be removed, false otherwise
 :)
declare function local:remove($package-url as xs:string) as xs:boolean {
    if ($package-url = repo:list()) then
        let $undeploy := repo:undeploy($package-url)
        let $remove := repo:remove($package-url)
        return
            $remove
    else
        false()
};

let $acceptMediaType    := 'application/zip'
let $referrer           := if (request:exists()) then request:get-header('Referer') else ()
let $filename           := if (request:exists()) then request:get-uploaded-file-name('file') else ()
let $size               := if (request:exists()) then request:get-uploaded-file-size('file') else ()
let $zip                := if (request:exists()) then request:get-uploaded-file-data('file') else ()
let $mediatype          := if (ends-with($filename, '.zip') or ends-with($filename, '.xar')) then 'application/zip' else ()

let $extension          := tokenize(tokenize($filename, '/')[last()], '\.')[last()]
let $projectname        := substring-before(tokenize($filename, '/')[last()], '.' || $extension)

let $check              :=
    if (empty($filename)) then 
        error(QName('http://art-decor.org/ns/error', 'MissingParameter'), 'Missing filename')
    else
    if (empty($zip)) then
        error(QName('http://art-decor.org/ns/error', 'MissingParameter'), 'Missing file contents. Expected multipart/form-data with ''file'' part')
    else
    if ($mediatype = $acceptMediaType) then () else (
        error(QName('http://art-decor.org/ns/error', 'UnsupportedFileType'), concat('Filename must end in .zip or .xar. Found ', tokenize($filename, '\.')[last()]))
    )

let $storedzip          := xmldb:store('/db/apps/decor/tmp', $filename, xs:base64Binary($zip))

(: extract just these from the zip/xar for inspection :)
let $entryCb            := function($path as xs:anyURI, $type as xs:string, $params as item()*) { $path='repo.xml' or $path='expath-pkg.xml' }
(:let $dataCb             := function($path as xs:anyURI, $type as xs:string, $data as item()?, $params as item()*) { $data }:)
let $dataCb             :=
    function ($path as xs:string, $data-type as xs:string, $data as item()?, $param as item()*) as element() {
        if ($data-type = 'folder') then
            ()
        else (
        (: if ($data-type = 'resource') :)
            (: === expath-pkg.xml ===
            if the zip is created from jgz-8.0 directory as jgz-8.0.zip then projectname is jgz-8.0          , the expath-pkg.xml will be what release2package.xsl wrote into it.
            if the xar is created as ada-jgz-8.0-1.0.0.xar              then projectname is ada-jgz-8.0-1.0.0, the expath-pkg.xml will be what ant build.xml wrote into it.
            
            if directory is jgz-8.0, and version is 1.0.0, then expath-pkg.xml looks like:
            <package xmlns="http://expath.org/ns/pkg" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" name="http://art-decor.org/ns/ada/jgz-8.0" abbrev="ada-jgz-8.0" version="1.0.0" spec="1.0">
              <title>ADA Jeugdgezondheidszorg Index 8.0.0-dev.6</title>
            </package>
            
               === repo.xml ===
            <meta xmlns="http://exist-db.org/xquery/repo"
                  xmlns:xs="http://www.w3.org/2001/XMLSchema"
                  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
               ...
               <target>ada-data/projects/jgz-8.0</target>
               ...
            </meta>
            :)
            
            (: repo:    - :)
            let $target   := $data/*/*:target
            (: expath:  name="http://art-decor.org/ns/ada/ketenzorg-3.0" abbrev="ada-ketenzorg-3.0" version="1.0.0" :)
            let $abbrev   := $data/*/@abbrev
            let $uri      := $data/*/@name
            let $version  := $data/*/@version
            
            let $check    :=
                switch ($path)
                case 'expath-pkg.xml' return
                    if (not($abbrev = 'ada-' || $projectname or 
                            $abbrev = substring-before($projectname, '-' || $version))
                       ) then
                        error(QName('http://art-decor.org/ns/error', 'UnsupportedAbbrev'), 'Illegal @abbrev value in ' || $path || '. Expected ada-' || $projectname || ', found: ' || $abbrev)
                    else
                    if (not($uri = 'http://art-decor.org/ns/ada/' || replace($projectname, '^ada-', '') or 
                            $uri = 'http://art-decor.org/ns/ada/' || substring-before(replace($projectname, '^ada-', ''), '-' || $version))
                       ) then
                        error(QName('http://art-decor.org/ns/error', 'UnsupportedUri'), 'Illegal @name value in ' || $path || '. Expected http://art-decor.org/ns/ada/' || replace($projectname, '^ada-', '') || ', found: ' || $uri)
                    else ()
                case 'repo.xml' return
                    if ($target = 'ada-data/projects/' || $projectname or 
                        starts-with('ada-data/projects/' || replace($projectname, '^ada-', ''), $target)) then $data/descendant-or-self::*[1] else (
                        error(QName('http://art-decor.org/ns/error', 'UnsupportedTarget'), 'Illegal target collection in ' || $path || '. Expected ada-data/projects/' || replace($projectname, '^ada-', '') || ', found: ' || $target)
                    )
                default return ()
            return
                <entry>
                    <path>{$path}</path>
                    <type>{$type}</type>
                    <data>{$data}</data>
                </entry>
        )
    }

let $install            := 
    try {
        (: check zip/xar consistency with functions defined earlier :)
        let $f := compression:unzip(util:binary-doc($storedzip), $entryCb, (), $dataCb, ())
        
        return
            if (count($f) = 2) then () else (
                error(QName('http://art-decor.org/ns/error', 'IncorrectPackage'), 'Missing repo.xml and/or expath-pkg.xml in the package')
            )
        ,
        util:log('INFO', 'Installing and deploying from db: ' || $storedzip)
        ,
        let $meta       := compression:unzip(util:binary-doc($storedzip), local:entry-filter#3,(),  local:entry-data#4, ())
        let $package    := $meta/descendant-or-self::expath:package/string(@name)
        let $remove     := local:remove($package)
        return
        repo:install-and-deploy-from-db($storedzip)
    }
    catch java.lang.NumberFormatException {
        <fail line="{$err:line-number}:{$err:column-number}" module="{$err:module}">{$err:code, $err:description}. This error usually means that the previous version of {$filename} could not be uninstalled. Please have an ART-DECOR administrator uninstall this before retrying</fail>
    }
    catch * {
        <fail line="{$err:line-number}:{$err:column-number}" module="{$err:module}">{$err:code, $err:description}</fail>
    }
let $delete             := xmldb:remove('/db/apps/decor/tmp', $filename)

(: return to Referrer header url if present and result is ok, otherwise just show the error and leave it at that :)
return 
    <html>
        <head>
            <title>{local-name($install[1])}</title>
            <style type="text/css">* {{font-family: verdana; font-size: 1em;}}</style>
            <meta name="robots" content="noindex, nofollow" />
        {
            if ($install/@result = 'ok' and not(empty($referrer))) then 
                <meta http-equiv="refresh" content="5; URL={$referrer}"/> |
                <meta http-equiv="expires" content="0" />
            else ()
        }
        </head>
        <body>
        {
            if ($install/@result = 'ok') then (
                <h3>Install successful</h3>,
                <p>{serialize($install)}</p>,
                if (empty($referrer)) then () else (
                    <p>Returning in five seconds to: <a href="{$referrer}">{$referrer}</a></p>
                )
            )
            else (
                <h3>Install failed</h3>,
                <p>{serialize($install)}</p>,
                <ul>
                    <li>Generic documentation: <a href="https://docs.art-decor.org/release2/specials/#art-decor-applications-aka-ada">ART-DECOR ADA</a></li>
                    <li>Expects top level <em>repo.xml</em> and <em>expath-pkg.xml</em> files - generated using release2package.xsl</li>
                    <li>-----</li>
                    <li>Zip the contents of the ADA project folder, so the zip does not contain the top level folder itself</li>
                    <li><b>macOS</b>: make sure the zip does not include macOS specific (hidden) files. In Terminal.app, <em>go into the directory</em> and use <pre>zip -r `basename $PWD`.zip *</pre></li>
                </ul>
            )
        }
        </body>
    </html>