xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:~ Common ADA functions and utilities for locations etc.

@version 0.1
@param $els Sequence of elements to which conceptId will be added (and to child nodes as well)
@param $spec A single enhanced dataset for a particular transaction, usually from a specific {project}-{version}-ada-release.xml
@return The sequence of elements from input, with conceptId
:)

module namespace ada = "http://art-decor.org/ns/ada-common";
declare namespace repo  = "http://exist-db.org/xquery/repo";
declare namespace sm    = "http://exist-db.org/xquery/securitymanager";

declare variable $ada:root := repo:get-root();
(:~ String variable for ADA projects :)
declare variable $ada:strAdaProjects    := concat($ada:root,'ada-data/projects/');
(:~ String variable for ADA log :)
declare variable $ada:strAdaLog         := concat($ada:root,'ada-data/log/');
(:~ Collection variable  for ADA projects  :)
declare variable $ada:colAdaProjects    := collection($ada:strAdaProjects);
(:~ String variable for ADA data :)
declare variable $ada:strAdaData        := doc(concat($ada:strAdaProjects, '../conf.xml'))//data/@uri;
(:~ String variable for ADA data :)
declare variable $ada:strAdaBackup        := doc(concat($ada:strAdaProjects, '../conf.xml'))//backup/@uri;
(:~ Collection variable for ADA data:)
declare variable $ada:colAdaData        := collection($ada:strAdaData);
(:~ Collection variable for ADA log:)
declare variable $ada:colAdaLog         := collection($ada:strAdaLog);
(: Orbeon base URI :)
declare variable $ada:orbeonBaseUri     := doc(concat($ada:strAdaProjects, '../conf.xml'))//orbeon/@uri;
(: Log on/off :)
declare variable $ada:logOn             := if (doc(concat($ada:strAdaProjects, '../conf.xml'))//log) then true() else false();
(: i18n :)
declare variable $ada:adaStrings        := doc('../core/ada-i18n.xml')//entry;

(:~ Get a schema for a document. Will determine if we need schema for adaxml, or just data content

@param  ADA document, or its data content
@return Schema URI
:)
declare function ada:getSchemaUri($doc as element()) as xs:string {
    ada:getSchemaUri($doc, false())
};

(:~ Get a schema for a document. Will determine if we need schema for adaxml, or just data content

@param  ADA document, or its data content
@draft  If true, return draft schema
@return Schema URI
:)
declare function ada:getSchemaUri($doc as element(), $draft as xs:boolean) as xs:string {
    let $transaction := if (local-name($doc)='adaxml') then $doc//data/* else $doc
    let $app := data($transaction/@app)
    let $uri := ada:getUri($app, 'schemas')
    return if (local-name($doc)='adaxml') then concat($uri, 'ada_', local-name($transaction), if($draft) then '_draft' else '', '.xsd') else concat($uri, local-name($transaction), '.xsd')
};

(:~ Get the transactionDataset for an ADA document.

@param  ADA document, or its data content
@return ADA transactionDataset
:)
declare function ada:getTransactionDataset($doc as node()) as element()? {
    (: Default is crud :)
    ada:getTransactionDataset($doc, 'crud')
};

(:~ Get the transactionDataset (crud or index) for an ADA document.

@param  ADA document, or its data content
@param  Type = 'index' or 'crud'
@return ADA transactionDataset
:)
declare function ada:getTransactionDataset($doc as node(), $type as xs:string) as element()? {
    let $transaction    := 
        if ($doc/@app) 
        then $doc 
        else if (local-name($doc)='Bundle') 
        then ($doc//resource/*)[1] 
        else $doc//*[@app][1]
    let $app            := data($transaction/@app)
    let $uri            := ada:getUri($app, 'definitions')
    let $datasets       := collection($uri)//ada[@app]//view[@type = $type]/dataset
    return 
        $datasets[@transactionId=$transaction/@transactionRef][@transactionEffectiveDate=$transaction/@transactionEffectiveDate][1]
};

(:~ Get a collection for a a subcollection in ADA

@param $app See ada:getUri
@param $type   See ada:getUri
@return A collection 
:)
declare function ada:getCollection($app as xs:string, $type as xs:string?) as node()* {
    let $uri := ada:getUri($app, $type)
    return collection($uri)
};

(:~ Get the URI for a a subcollection in ADA. Function does not verfiy whether the collection exists, which would be just overhead if the caller is tested.

@param $app Project app with or without trailing hyphen, i.e. 'demo1' or 'demo1-'
@param $type Desired subcollection, i.e. 'data' or 'definitions', i.e.: 'definitions' or 'data'
@return URI string, i.e. '/db/apps/ada/projects/demo1/data/'
:)
declare function ada:getUri($app as xs:string, $type as xs:string?) as xs:string {
    let $app := if (ends-with($app, '-')) then substring($app, 1, string-length($app) - 1) else $app
    let $uri := 
        if ($type = ()) then $ada:strAdaProjects
        else 
        if ($type = 'data') then concat($ada:strAdaData, $app, '/data/')
        else concat($ada:strAdaProjects, $app, '/', $type, '/')
    return $uri
};

(:~ Set group to ada-user, permissions rx for all :)
declare function ada:rx4all($uri as xs:anyURI) {
    sm:chgrp($uri,'ada-user'),
    sm:chmod($uri,'rwxr-xr-x'),
    sm:clear-acl($uri)
};

(:~ Set group to ada-user, permissions rwx for group :)
declare function ada:rwx4grp($uri as xs:anyURI) {
    sm:chgrp($uri,'ada-user'),
    sm:chmod($uri,'rwxrwxr-x'),
    sm:clear-acl($uri)
};

(:~ Set group to ada-user, permissions r for all :)
declare function ada:setRights($uri as xs:anyURI, $rights as xs:string) {
    if (matches($rights, '([r\-][w\-][x\-]){3}')) then (
        sm:chgrp($uri, 'ada-user'),
        sm:chmod($uri, $rights)
        (:,sm:clear-acl($uri):)
    ) else ()
};

(:~ Recursively set permissions for collection :)
declare function ada:doCollection($uri as xs:anyURI) {
    let $perms := ada:setRights($uri, 'rwxr-xr-x')
    let $result :=
        for $resource in xmldb:get-child-resources($uri)
        return ada:setRights(xs:anyURI(concat($uri, '/', $resource)),'rwxr-xr-x')
    let $result :=
        for $collection in xmldb:get-child-collections($uri)
        return ada:doCollection(xs:anyURI(concat($uri, '/', $collection)))
    return $result
};

(:~ Set permissions for ada project :)
declare function ada:setPermissions($project as xs:string) {
    let $project    := if (ends-with($project,'-')) then substring($project,1,string-length($project)-1) else ($project)
    let $uri        := xs:anyURI(concat($ada:strAdaProjects, $project))
    let $result     := ada:doCollection($uri)
    let $uri        := xs:anyURI(concat($ada:strAdaData, $project))
    let $result     := xmldb:create-collection($ada:strAdaData, concat($project, '/data'))
    
    (: Only set permissions on data collection, not on individual files :)
    let $result     := ada:setRights(xs:anyURI($uri), 'rwxrwxr-x')
    let $result     := ada:setRights(xs:anyURI(concat($uri, '/data')), 'rwxrwxr-x')
    let $result.    :=
    for $res in xmldb:get-child-resources(xs:anyURI(concat($uri, '/data')))
    return
        ada:setRights(xs:anyURI(concat($uri, '/data/', $res)), 'rw-rw-r--')
    return $result
};

(:~ Check who is owner of record.

@param     $data    ADA XML
@return    string
:)
declare function ada:getOwner($data as element()?) as xs:string?{
    ($data/ancestor-or-self::adaxml/meta/@created-by)[1]
};

(:~ Check who is owner of record.

@param     $app    ADA app
@param     $id     ADA XML id
@return    string
:)
declare function ada:getOwner($app as xs:string, $id as xs:string) as xs:string?{
    let $data       := if ($id = 'new') then () else ada:getCollection($app, 'data')//*[@app = $app][@id = $id]
    return ada:getOwner($data)
};

(:~ Check whether user has permission for action. For now, user has permission for update/delete only if user has created record.

@param     $data    ADA XML
@param     $action 'update' or 'delete'
@return    true or false
:)
declare function ada:hasPermission($data as element()?, $action as xs:string) as xs:boolean{
    let $creator    := if ($data) then ada:getOwner($data) else ()
    return  
        $data and ($action='update' or $action='delete') and $creator = ada:strCurrentUserName()
};

(:~ Check whether user has permission for action. For now, user has permission for update/delete only if user has created record.

@param     $app    ADA app
@param     $id     ADA XML id
@param     $action 'update' or 'delete'
@return    true or false
:)
declare function ada:hasPermission($app as xs:string, $id as xs:string, $action as xs:string) as xs:boolean{
    ada:hasPermission(ada:getCollection($app, 'data')//*[@app = $app][@id = $id], $action)
};

(:~ Check the last modification date.

@param     $data    ADA XML
@return    xs:dateTime
:)
declare function ada:dataLastModified($data as element()?) as xs:dateTime? {
    let $date       := max($data/ancestor::adaxml/meta/xs:dateTime(@last-update-date))
    
    return
        try { if (empty($date)) then xmldb:last-modified(util:collection-name($data), util:document-name($data)) else ($date) } catch * {()}
};

(:~ Check the last modification date.

@param     $app    ADA app
@param     $id     ADA XML id
@return    xs:dateTime
:)
declare function ada:dataLastModified($app as xs:string, $id as xs:string) as xs:dateTime? {
    ada:dataLastModified(ada:getCollection($app, 'data')//*[@app = $app][@id = $id])
};

(:~ Check whether user has permission for action. For now, user has permission for update/delete only if user has created record.

@param     $name   Name for the log item
@param     $app    
@param     $node   Optional content to be logged
:)
declare function ada:log($name as xs:string, $app as xs:string, $log as node()?) {
    try {xmldb:store($ada:strAdaLog, concat($name, '-', $app, '-', fn:substring(translate(xs:string(fn:current-dateTime()), ':T-.+', ''),1,17), '.xml'), $log)}
    catch * {()}
};

declare function ada:getString($key as xs:string?, $language as xs:string) as xs:string? {
    let $s  := $ada:adaStrings[@key=$key]/*[@language=$language]
    
    return
        if ($s) then $s/text() else $key
};

(:~ Get the language from the art package if installed or use en-US as fallback :)
declare function ada:getLanguage() as xs:string {
    ada:getLanguage(())
};
declare function ada:getLanguage($defaultLanguage as xs:string?) as xs:string {
    let $defaultLanguage    := if (string-length($defaultLanguage)=0) then 'en-US' else $defaultLanguage
    
    return
    try {
        let $load   := util:import-module(xs:anyURI('http://art-decor.org/ns/art-decor-settings'),'get',xs:anyURI(concat(repo:get-root(),'art/modules/art-decor-settings.xqm')))
        return
            util:eval('$get:strArtLanguage')
    }
    catch * {$defaultLanguage}
};

declare function ada:getApplications($apps as xs:string*, $language as xs:string?) as element(app)* {
    let $projects   := 
        if (empty($apps)) then xmldb:get-child-collections($ada:strAdaProjects) else 
            for $project in xmldb:get-child-collections($ada:strAdaProjects)
            return $project[. = $apps]
    
    for $project in $projects
    for $index in collection(concat($ada:strAdaProjects, $project))//ada[@adaVersion]//view[@type='index']
    order by $project
    return
        <app app="{$project}">
        {
            if ($index/@target='xforms') then (
                attribute xforms {concat('ada-data/projects/', $project, '/views/', $index/implementation/@shortName/string(), '.xhtml')},
                attribute xformsName {concat($project, ' [', $index/name, ']')}
            )
            else 
            if ($index/@target='xquery') then (
                attribute xquery {concat('ada-data/projects/', $project, '/modules/index.xquery')},
                attribute xqueryName {$project}
            )
            else ()
            ,
            attribute admin {concat('index-admin.xquery?app=',$project,'&amp;language=',$language)}
            ,
            for $view in collection(concat($ada:strAdaProjects, $project))//ada[@adaVersion]//view[@transactionId]
            let $transactionId  := $view/@transactionId
            let $transactionEd  := $view/@transactionEffectiveDate
            group by $transactionId, $transactionEd
            return
                <view>{$transactionId, $transactionEd}</view>
        }
        </app>
};

(:~ Get current user name. effective user name if available or real user name otherwise. The function xmldb:get-current-user() was removed 
in eXist-db 5.0 and replaced with sm:id(). By centralizing the new slightly more complicated way of doing this, you can avoid making 
mistakes throughout the code base

@return user name string.
@since 2019-11-11
:)
declare function ada:strCurrentUserName() as xs:string? {
    let $current-user-function  := function-lookup(xs:QName('xmldb:get-current-user'), 0)
    let $sm-id-function         := function-lookup(xs:QName('sm:id'), 0)
    
    return
    if (exists($current-user-function)) then 
        $current-user-function()
    else 
    if (exists($sm-id-function)) then (
        let $smid   := $sm-id-function()
        
        return
            ($smid//sm:effective/sm:username, $smid//sm:real/sm:username)[1]
    ) 
    else 
        'guest'
};
