xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace ada ="http://art-decor.org/ns/ada-common" at "ada-common.xqm";
import module namespace adaxml ="http://art-decor.org/ns/ada-xml" at "ada-xml.xqm";

declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";

let $data       := if (request:exists()) then request:get-data()/* else ()

(: Don't log data in controller, since get-data removes them from request :)
let $logOn      := 
    if ($ada:logOn) then (
        let $log := <data>{$data}</data>
        (: don't store if not logged in or log not available :)
        return  
            try {xmldb:store($ada:strAdaLog, concat('DATA', '-', $data/@app, '-', substring(translate(xs:string(current-dateTime()), ':T-.+', ''),1,17), '.xml'), $log)}
            catch * {()}
     )
     else ()
let $result     := 
    try {adaxml:saveAdaXml($data)}
    catch * {
        <error>Caught error {$err:code}: {$err:description}</error>
    }
let $logOn      := if ($ada:logOn) then ada:log('save-data-result', $data/@app, $result) else ()
return 
    if (local-name($result) = 'error') then (
        if (request:exists()) then response:set-status-code(400) else (),
        $result
    )
    else (
        if (request:exists()) then (if ($data/@id = 'new') then response:set-status-code(201) else response:set-status-code(200)) else (),
        adaxml:getXmlData($data/@app, $result, (), 'false', $data/@transactionRef, $data/@transactionEffectiveDate)
    )