xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace ada = "http://art-decor.org/ns/ada-common" at "ada-common.xqm";
declare option exist:serialize "method=xhtml media-type=text/html indent=yes";

let $language   := if (request:exists()) then request:get-parameter('language',())[1] else ()
let $language   := if (string-length($language)=0) then (ada:getLanguage()) else ($language)

return
<html>
    <head>
        <title>ADA Dashboard</title>
        <link rel="stylesheet" type="text/css" href="../resources/css/ada.css"/>
    </head>
    <body class="orbeon ada">
        <h1>ADA Dashboard</h1>
        <h2>ADA {ada:getString('Projects', $language)}</h2>
        <p><a href="index.xquery?language={$language}">ADA {ada:getString('Index', $language)}</a></p>
        <h2>ADA {ada:getString('Logging', $language)}</h2>
        <p><a href="logging.xquery?language={$language}">ADA 
        {
            if ($ada:logOn) then ada:getString('Logging is: ON', $language) else ada:getString('Logging is: OFF', $language)
        }</a></p>
        <h2>ADA {ada:getString('Admin all projects', $language)}</h2>
        <p><a href="index-admin.xquery?language={$language}">{ada:getString('Admin', $language)}</a></p>
        <h2>ADA {ada:getString('Admin', $language)}</h2>
        <table>{
            for $project in xmldb:get-child-collections($ada:strAdaProjects)
            order by $project
            return 
            <tr>
                <td><a href="index-admin.xquery?app={$project}&amp;language={$language}">{ada:getString('Projects', $language),$project}</a></td>
            </tr>
        }
        </table>
    </body>
</html>