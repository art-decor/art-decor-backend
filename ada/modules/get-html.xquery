xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace ada     = "http://art-decor.org/ns/ada-common" at "ada-common.xqm";
import module namespace adaxml  = "http://art-decor.org/ns/ada-xml" at "ada-xml.xqm";

declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";
declare option exist:serialize "method=xhtml media-type=text/html indent=yes";

declare %private function local:getConceptRows($concept as element(), $definition as element()?, $i18n-preferred as element()?, $i18n-alternative as element()?) as element()* {
    
    (: Get the definition corresponding to this concept :)
    let $conceptDef := if ($concept/@app) then $definition else $definition/descendant-or-self::concept[@id = $concept/@conceptId][1]
    
    let $c          :=
        if (count($conceptDef)=1) then () else
        if (count($conceptDef)=0) then (
            (:error(QName('http://art-decor.org/ns/ada/get-html', 'MissingDefinition'), concat('Found ',count($conceptDef),' definitions, while exactly 1 was expected for concept id=&quot;',$concept/@conceptId,'&quot;')):)
        )
        else (
            error(QName('http://art-decor.org/ns/ada/get-html', 'TooManyDefinitions'), concat('Found ',count($conceptDef),' definitions, while exactly 1 was expected for concept id=&quot;',$concept/@conceptId,'&quot;'))
        )
    
    (: For each child of definition, find non-empty concepts in concept :)
    for $childDef in $conceptDef/concept
    let $concepts := $concept/*[@conceptId = $childDef/@id][.//@value | .//@code | .//@codeSystem | .//@codeSystemName | .//@codeSystemVersion | .//@displayName | .//@unit | .//@root | .//@nullFlavor]
    return (
        if ($concepts) then (
            (: Header based on definition :)
            if ($childDef[@type = 'group'] | $childDef[local-name() = 'dataset']) then 
                let $level := count($childDef/ancestor::concept) + 1
                let $level := if ($level > 6) then 6 else $level
                return element {concat('h', $level)} {$childDef/name/string()}
            else ()
            ,
            (: for children with a value or valued child :)
            for $child at $pos in $concepts
            return (
                (: for groups :)
                if ($childDef[@type = 'group'] | $concept[@app]) then (
                    (: for 1..* groups, make a header row with counter :)
                    if ($childDef[@maximumMultiplicity = '1']) then (
                        <div>{$child/@id}</div>
                    ) else (
                        <div>
                            <div class="concept-group-header">
                                {$child/@id}
                                <div class="concept-item-name">{$childDef/name/string()}</div>
                                <div class="concept-item-cell">{concat(' #', xs:string($pos))}</div>
                            </div>
                        </div>
                    )
                    ,
                    (: show group content :)
                    let $groupContent   := local:getConceptRows($child, $childDef, $i18n-preferred, $i18n-alternative)
                    return
                        if ($groupContent) then <div class="concept-group-content">{$groupContent}</div> else ()
                )
                else 
                (: for items :)
                if ($child[@value | @code | @codeSystem | @codeSystemName | @codeSystemVersion | @displayName | @ordinal | @unit | @root | @nullFlavor]) then (
                    <div class="concept-item">
                        {$child/@id}
                        <div class="concept-item-name">{$childDef/name/string()}</div>
                        <div class="concept-item-cell">
                        {
                            if ($childDef/valueDomain[@type= ('code', 'ordinal')]) then (
                                let $pref   := if ($child[@preferred = 'true']) then $i18n-preferred else if ($child[@preferred = 'false']) then $i18n-alternative else ()
                                
                                let $value  := 
                                    if ($child[@displayName][@originalText]) then string-join(($child/@displayName, $child/@originalText), ': ') else
                                    if ($child[@code][@originalText])        then string-join(($child/@code, $child/@originalText), ': ') else (
                                        $childDef/valueSet/conceptList/concept[@localId = $child/@value]/name[1]
                                    )
                                let $value  := if ($value) then $value else $child/@displayName
                                let $value  := if ($value) then $value else string-join(($child/@code, $child/@codeSystem, $child/@codeSystemName, $child/@codeSystemVersion), ' - ')
                                let $value  := if ($value) then $value else string-join($child/(@* except @conceptId), ' - ')
                                let $value  := if ($child[@ordinal]) then concat('( ', $child[1]/@ordinal, ' ): ', $value) else ($value)
                                let $value  := if (empty($pref)) then $value else concat($value, ' - ( ', $pref, ' )')
                                return
                                    data($value)
                            )
                            else 
                            if ($child/@nullFlavor) then
                                string-join(($child/@nullFlavor, $child/@root, $child/@unit, $child/@originalText), ' ')
                            else
                            if ($childDef/valueDomain[@type = 'boolean']) then 
                                if ($child/@value='UNK') then '?' else (
                                    <input type="checkbox" disabled="disabled">{if ($child/@value='true') then attribute checked {'checked'} else ()}</input>
                                )
                            else
                            if ($child[@datatype = 'reference']) then 
                                <a href="{if (starts-with($child/@value, '#')) then () else ('#')}{$child/@value}">{data($child/@value)}</a>
                            else (
                                let $value  := if ($child/@value castable as xs:dateTime) then replace(format-dateTime(xs:dateTime($child/@value),'[Y0001]-[M01]-[D01] [H01]:[m01]:[s01]', (), (), ()),' 00:00:00','') else ($child/@value)
                                return
                                string-join(($value, $child/@root, $child/@unit), ' ')
                            )
                        }
                        </div>
                    </div>
                ) else ()
            )
        ) else ()
    )
};

let $format         := if (request:exists()) then request:get-parameter('format','html') else 'xml'

let $app            := if (request:exists()) then request:get-parameter('app', '') else 'demoapp'
let $id             := if (request:exists()) then request:get-parameter('id','') else 'demo1-ttt-01'

(:let $data           := $ada:colAdaData//adaxml/data/*[@app = $app][@id = $id]:)

let $data           := 
    try {
        if (string-length($app) gt 0) then
            adaxml:getXmlData($app, $id, (), 'false', (), ())
        else (
            let $xml    := $ada:colAdaData//adaxml/data/*[@id = $id]
            let $app    := if ($xml[@app]) then $xml/@app else $app
            return
                adaxml:getXmlData($app, $id, (), 'false', (), ())
        )
    }
    catch * {<error query="{if (request:exists()) then request:get-query-string() else ()}" id="{$id}" app="{$app}">Caught error {$err:code}: {$err:description}</error>}

let $data           := adaxml:removeEmptyValues($data)

let $result         := 
    if ($data and $format ='xml') then
        <results>
            <data>{$data}</data>
            <transactiondata>{ada:getTransactionDataset($data)}</transactiondata>
        </results>
    else 
    if ($data) then
        <html>
            <head>
                <title>{$data/@title/string()}</title>
                <link rel="stylesheet" type="text/css" href="../resources/css/ada.css"/>
            </head>
            <body class="orbeon ada">
                <h1>{$data/@title/string()}</h1>
                <input type="button" onclick="window.print()" value="Print"/>
                {
                    let $definition         := ada:getTransactionDataset($data)
                    let $i18n-preferred     := $ada:adaStrings[@key = 'preferredvalue']/text
                    let $i18n-preferred     := ($i18n-preferred[@language = $definition/concept/name/@language], $i18n-preferred/text)[1]
                    let $i18n-alternative   := $ada:adaStrings[@key = 'alternativevalue']/text 
                    let $i18n-alternative   := ($i18n-alternative[@language = $definition/concept/name/@language], $i18n-alternative)[1]
                    
                    return local:getConceptRows($data, $definition, $i18n-preferred, $i18n-alternative)
                }
                <input type="button" onclick="window.print()" value="Print"/>
            </body>
        </html>
    else (concat('No data found for id ', $id))

return 
    if ($data and $format = 'xml') then (
        response:set-header('Content-Type', 'application/xml'),
        $result
    )
    else 
    if ($data) then $result else (response:set-status-code(404), $result)