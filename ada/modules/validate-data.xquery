xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace ada     = "http://art-decor.org/ns/ada-common" at "ada-common.xqm";
import module namespace adaxml  = "http://art-decor.org/ns/ada-xml" at "ada-xml.xqm";
declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";

declare option exist:serialize "method=xhtml media-type=text/html indent=yes";

(: adaxml:getXmlData will add empty attributes for the xform editor like codeSystemVersion and nullFlavor. We will strip those
    We will also strip empty attributes from the raw data this way, but those will be caught in the validation of the raw data
:)
declare %private function local:removeEmptyAttributes($nodes as node()*) as node()* {
    for $node in $nodes
    return
        if ($node instance of element()) then (
            element {name($node)} {
                $node/@*[not(. = '')],
                for $child in $node/node()
                return local:removeEmptyAttributes($child)
            }
        )
        else ($node)
};

(:<beschikbaarstellen_medicatiegegevens app="mp-mp9" 
    shortName="beschikbaarstellen_medicatiegegevens" 
    formName="uitwisselen_medicatiegegevens" 
    transactionRef="2.16.840.1.113883.2.4.3.11.60.20.77.4.102" 
    transactionEffectiveDate="2016-03-23T16:32:43" 
    versionDate="" 
    prefix="mp-" 
    language="nl-NL" 
    title="Raadplegen_MA_1.1" 
    id="906cc78b-7c6a-47b7-a4bf-3a40d3ac22f9">:)

let $id                 := if (request:exists()) then request:get-parameter('id','') else '906cc78b-7c6a-47b7-a4bf-3a40d3ac22f9' 
let $app                := if (request:exists()) then request:get-parameter('app','') else 'mp-mp9' 

let $trid               := if (request:exists()) then request:get-parameter('transactionId',())[string-length()>0] else ()
let $treff              := if (request:exists()) then request:get-parameter('transactionEffectiveDate',())[string-length()>0] else ()

let $from               := if (request:exists()) then request:get-parameter('from','') else '' 

let $result             := local:removeEmptyAttributes(adaxml:getXmlData($app, $id, $from, 'false', $trid, $treff))
let $validation         := for $r in $result return adaxml:validateSchema($r)
let $rawXml             := ada:getCollection($app, 'data')//*[@app = $app][@id = $id]/ancestor::adaxml
let $rawValidation      := adaxml:validateSchema($rawXml, false())
let $draftValidation    := adaxml:validateSchema($rawXml, true())

return
<html>
    <head>
        <title>ADA Projects</title>
        <link rel="stylesheet" type="text/css" href="../resources/css/ada.css"/>
    </head>
    <body class="orbeon ada">
    <h1>{$app} - {$id}</h1>
    <p>Advice:</p>
    {
        for $val in $validation
        return
        if ($val/status='valid' and $rawValidation/status='valid')
        then <div>Both preprocessed and raw XML are valid, everything is fine.</div>
        else if ($val/status='valid' and $rawValidation/status='invalid')
        then <div>Preprocessed XML is valid, and raw XML is not. Editing and saving with XForm will make raw XML valid, but this may lose data. Alternative is to edit the raw XML to retain data which would otherwise be lost.</div>
        else if ($val/status='invalid' and $rawValidation/status='valid')
        then <div>Preprocessed XML is invalid, and raw XML is valid. Potential ADA software bug in preprocessing step.</div>
        else if ($val/status='invalid' and $rawValidation/status='invalid' and $draftValidation/status='valid')
        then <div>Neither preprocessed nor raw XML is not valid, but raw XML is a valid draft, which can be opened and completed.</div>
        else if ($val/status='invalid' and $rawValidation/status='invalid')
        then <div>Neither preprocessed nor raw XML is valid. Probably raw XML contains invalid values (i.e. "string" where new schema expects "integer" etc.) and raw XML must be edited or discarded. Editing with XForm may or may not work in this case.</div>
        else <div>unexpected situation</div>
    }
    <h2>Preprocessed XML</h2>
    <p>(after get-data, this may lose older data that does not exist in newer XML format)</p>
    {
        for $val at $i in $validation
        return
        <table>
            <tr>
                <th><b>{$app}</b></th>
                <th>{data($val/status)}</th>
                <th>{data($val/duration), $val/duration/@unit/string()}</th>
                <td><a href="get-data.xquery?id={$id}&amp;app={$app}&amp;transactionId={$result[$id]/@transactionRef}&amp;transactionEffectiveDate={$result[$id]/@transactionEffectiveDate}">XML</a></td>
            </tr>
            <tr>
                <th>Level</th>
                <th>Line</th>
                <th>Column</th>
                <td>Message</td>
            </tr>
            {
                for $message in $val/message
                return 
                <tr>
                    <td>{$message/@level/string()}</td>
                    <td>{$message/@line/string()}</td>
                    <td>{$message/@column/string()}</td>
                    <td>{data($message)}</td>
                </tr>
            }
        </table>
    }
    <h2>Raw XML</h2>
    <p>(as it is in the database)</p>
    <table>
        <tr>
            <th><b>{$app}</b></th>
            <th>{data($rawValidation/status)}</th>
            <th>{data($rawValidation/duration), $rawValidation/duration/@unit/string()}</th>
            <td><a href="../../ada-data/db/{$app}/data/{$id}.xml">Raw XML</a></td>
        </tr>
        <tr>
            <th>Level</th>
            <th>Line</th>
            <th>Column</th>
            <td>Message</td>
        </tr>
        {for $message in $rawValidation/message
        return 
        <tr>
            <td>{$message/@level/string()}</td>
            <td>{$message/@line/string()}</td>
            <td>{$message/@column/string()}</td>
            <td>{data($message)}</td>
        </tr>
        }
    </table>
    <h2>Raw XML against draft schema</h2>
    <p>(as it is in the database, against draft schema)</p>
    <table>
        <tr>
            <th><b>{$app}</b></th>
            <th>{data($draftValidation/status)}</th>
            <th>{data($draftValidation/duration), $draftValidation/duration/@unit/string()}</th>
            <td><a href="../../ada-data/db/{$app}/data/{$id}.xml">Raw XML</a></td>
        </tr>
        <tr>
            <th>Level</th>
            <th>Line</th>
            <th>Column</th>
            <td>Message</td>
        </tr>
        {for $message in $draftValidation/message
        return 
        <tr>
            <td>{$message/@level/string()}</td>
            <td>{$message/@line/string()}</td>
            <td>{$message/@column/string()}</td>
            <td>{data($message)}</td>
        </tr>
        }
    </table>
    </body>
</html>
