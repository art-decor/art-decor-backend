xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace ada     = "http://art-decor.org/ns/ada-common" at "ada-common.xqm";
import module namespace adaxml  = "http://art-decor.org/ns/ada-xml" at "ada-xml.xqm";

declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";

let $params         := if (request:exists()) then try { request:get-data()/* } catch * {()} else <rename app="demo1" id="demo1-ttt-01" newid="demo1-ttt-02"/>

let $app        := if ($params) then $params/@app else if (request:exists()) then request:get-parameter('app', ())[1] else 'demoapp'
let $id         := if ($params) then $params/@id else if (request:exists()) then request:get-parameter('id', ())[1] else 'demo1-ttt-01'
let $newid      := if ($params) then $params/@newid else if (request:exists()) then request:get-parameter('newid', ())[1] else 'demo1-ttt-03'
let $newid2     := replace($newid, '[^A-Za-z\d()\._+-]', '')

let $check      :=
    if (string-length($app) = 0 or string-length($id) = 0 or string-length($newid2) = 0) then
        error(QName('http://art-decor.org/ns/ada-common', 'MissingParameter'), 'Missing required parameter app=''' || $app[1] || ''', id=''' || $id[1] || ''' or newid=''' || $newid2 || '''. Please update your ADA index form.')
    else ()

(: if multiple exist, move the first. This may be used as a means for deduplication :)
let $olddata    := (ada:getCollection($app, 'data')//*[@app = $app][@id = $id])[1]
let $newdata    := ada:getCollection($app, 'data')//*[@app = $app][@id = $newid]

let $colName    := util:collection-name($olddata)
let $newName    := concat($newid, '.xml')
return 
    if (string-length($app) = 0 or string-length($id) = 0 or string-length($newid) = 0) then (
        if (request:exists()) then response:set-status-code(400) else (),
        <error>Missing parameter app='{data($app)}' or id='{data($id)}' or newid='{data($newid)}'</error>
    )
    else
    if ($id = $newid) then (
        if (request:exists()) then response:set-status-code(400) else (),
        <error>Cannot rename to newid: id='{data($id)}' is equal to newid='{data($newid)}'</error>
    )
    else
    if (not($newid = $newid2)) then (
        if (request:exists()) then response:set-status-code(400) else (),
        <error>Cannot rename to newid '{data($newid)}': can only contain the characters A-Za-z0-9()._+-</error>
    )
    else
    if (empty($olddata)) then (
        if (request:exists()) then response:set-status-code(400) else (),
        <error>Cannot rename to newid '{data($newid)}': source does not exist</error>
    )
    else
    if ($newdata) then (
        if (request:exists()) then response:set-status-code(400) else (),
        <error>Cannot use newid '{data($newid)}': already exists</error>
    )
    else
    if (ada:hasPermission($olddata, 'update')) then (
        if (request:exists()) then response:set-status-code(200) else (),
        xmldb:rename($colName, util:document-name($olddata), $newName),
        (: in case we're moving a duplicate id, we cannot rely on collection() selection :)
        update value doc(concat($colName, '/', $newName))//*[@app = $app][@id = $id]/@id with $newid,
        (: update references to the old id, to the newid :)
        for $reference in collection($colName)//*[@datatype = 'reference'][starts-with(@value, $id || '#')]
        let $newref   := $newid || '#' || substring-after($reference/@value, '#')
        return
            update value $reference/@value with $newref
    )
    else (
        if (request:exists()) then response:set-status-code(401) else (),
        <error>Only the creator may update a record</error>
    )
