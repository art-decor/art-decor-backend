xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace ada     = "http://art-decor.org/ns/ada-common" at "ada-common.xqm";
import module namespace adaxml  = "http://art-decor.org/ns/ada-xml" at "ada-xml.xqm";

declare namespace json      = "http://www.json.org";
declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "json";
declare option output:media-type "application/json";

(: 
    Return attributes for JSON serialization 
    eXist will serialize JSON, but we need to set literal values for boolean, count, quantity
:)
declare %private function local:adaAttributes2Json($el as element(), $spec as element()) as element()* {
    let $specConcept := $spec//concept[@id=$el/@conceptId]
    let $datatype := $specConcept/valueDomain/@type/string()
    return 
        (
        for $at in $el/@*
        return 
            element {$at/local-name()}
                {
                if ($at/local-name() = 'value' and ($datatype = 'count' or $datatype = 'boolean' or $datatype = 'quantity' or $datatype = 'decimal')) then attribute json:literal {'true'} else ()
                , $at/string()
                }
        ,if ($datatype != "") then element datatype {$datatype} else ()
        )
};

(: 
    Return elements for JSON serialization 
    eXist will serialize JSON, but we want all (not only repeating) elements in JSON array
:)
declare %private function local:adaElement2Json($els as element()*, $spec as element()) as element()* {
    for $el in $els
    return element {$el/local-name()} {attribute json:array {'true'}, local:adaAttributes2Json($el, $spec), local:adaElement2Json($el/*, $spec)} 
};

(: 
    Remove hidden elements, use [] empty array instead
:)
declare %private function local:removeHiddenElements($els as element()*, $newXml as node()) as element()* {
    for $el in $els
    return 
        (: For @hidden start hooks :)
        if ($el[@hidden]) 
        then 
            (: If there are siblings with 'real' data :)
            if ($els[empty(@hidden)][@conceptId=$el/@conceptId])
            (: just skip the @hidden element :)
            then ()
            else
            if ($el[local-name() = 'adaextension-start'] and $els[empty(@hidden)][local-name() = 'adaextension'])
            (: just skip the @hidden element :)
            then ()
            (: otherwise make an empty element with the right name (from new XML :)
            else element {local-name($newXml//*[empty(@hidden)][@conceptId=$el/@conceptId][1])} {}
        (: Keep data elements as they are :)
        else element {$el/local-name()} {$el/@*, local:removeHiddenElements($el/*, $newXml)} 
};

let $id         := if (request:exists()) then request:get-parameter('id','') else 'new' 
let $app        := if (request:exists()) then request:get-parameter('app','') else 'demoapp' 
let $from       := if (request:exists()) then request:get-parameter('from','') else '7976584c-c63f-4846-abaf-7334bbaf758d' 

let $trid       := if (request:exists()) then request:get-parameter('transactionId','') else ''
let $treff      := if (request:exists()) then request:get-parameter('transactionEffectiveDate','') else ''

(: NOTE: Summary does not work yet since get-json adds elements again. This needs some rework anyway, since get-json does not properly return empty arrays for empty data anyway. :)
let $summary    := if (request:exists()) then request:get-parameter('summary', 'false') else 'false'
let $summary    := $summary[. = ('true', 'false', 'ids')]

let $newXml     := adaxml:getNewXml($app, $trid, $treff)
let $data       := adaxml:getXmlData($app, $id, $from, $summary, $trid, $treff)
let $result     := local:removeHiddenElements($data, $newXml[@transactionRef=$data/@transactionRef][@transactionEffectiveDate=$data/@transactionEffectiveDate])
let $result     := if (not($data)) 
    then <error>No data found for id: {$id}, app: {$app}</error>
    else
        (: Return top element with attributes, return data children and newXml children :)
        let $spec := ada:getTransactionDataset($result[1])
        let $results := local:adaElement2Json($result, $spec)
        let $bundle := if ($id='') then adaxml:bundle($results) else $results
        return element root {$bundle}
        
let $logOn      := if ($ada:logOn) then ada:log('get-json-result', $app, <result>{$result}</result>) else ()
return 
    if ($data) then $result
    else 
        if (request:exists())
        then (response:set-status-code(404), $result)
        else $result
