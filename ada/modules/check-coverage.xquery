xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace ada     = "http://art-decor.org/ns/ada-common" at "ada-common.xqm";
import module namespace adaxml  = "http://art-decor.org/ns/ada-xml" at "ada-xml.xqm";

declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";
declare option exist:serialize "method=xhtml media-type=text/html indent=yes";

let $app            := if (request:exists()) then request:get-parameter('app','') else 'demoapp' 

let $data           := $ada:colAdaData//adaxml/data/*[@app=$app]
let $result         := 
    if (not($data)) 
    then concat('No data found for app ', $app) 
    else 
    let $definition     := ada:getTransactionDataset($data[1])
    return
        <html>
           <head>
              <title>Coverage for {$definition/name}</title>
              <link rel="stylesheet" type="text/css" href="../resources/css/ada.css"/>
            </head>
            <body class="orbeon ada">
                <h1>Coverage for {$definition/name}</h1>
                <table>
                    <tr>
                        <th>Concept</th>
                        <th>Id</th>
                        <th>Type</th>
                        <th>Touched</th>
                    </tr>
                    {
                    for $concept in $definition//concept[@type=('item', 'group')]
                    let $touched :=
                        if ($concept[@type='item'])
                        then count($data//*[@value][@conceptId=$concept/@id])
                        else count($data//*[empty(@hidden)][@conceptId=$concept/@id])
                    return 
                    <tr style="{if ($touched=0) then 'background-color:lightpink;' else ''}">
                        <td>{$concept/name}</td>
                        <td>{$concept/@id/string()}</td>
                        <td>{$concept/@type/string()}</td>
                        <td>{$touched}</td>
                    </tr>
                    }
                </table>
            </body>
        </html>
return if ($data) then $result else (if (request:exists()) then response:set-status-code(404) else (), $result)