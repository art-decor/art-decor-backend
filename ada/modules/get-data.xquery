xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace ada     = "http://art-decor.org/ns/ada-common" at "ada-common.xqm";
import module namespace adaxml  = "http://art-decor.org/ns/ada-xml" at "ada-xml.xqm";

declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";

let $trline     := 
    <versturen_vaccinatiesoproeplijst 
        app="perinatologie"
        transactionRef="2.16.840.1.113883.2.4.3.11.60.90.77.4.2446" 
        transactionEffectiveDate="2018-06-01T15:07:24" 
        id="new"
        from="4c086a0a-b104-46ed-8595-26f61cd2f43b"
        summary="false"
        raw="false"/>

(:https://decor.nictiz.nl/art-decor/ada-data/projects/cio/views/beschikbaarstellen_icavertaling.xhtml?id=new&from=3af29210-12d2-4e3d-8cd7-05a6adde938f&transactionId=2.16.840.1.113883.2.4.3.11.60.26.4.3&transactionEffectiveDate=2019-08-28T13:33:41:)

let $id         := if (request:exists()) then request:get-parameter('id','') else ($trline/@id)
let $app        := if (request:exists()) then request:get-parameter('app','') else ($trline/@app)

let $trid       := if (request:exists()) then request:get-parameter('transactionId',())[string-length() gt 0] else ($trline/@transactionRef)
let $treff      := if (request:exists()) then request:get-parameter('transactionEffectiveDate',())[string-length() gt 0] else ($trline/@transactionEffectiveDate)

let $from       := if (request:exists()) then request:get-parameter('from','') else ($trline/@from)
let $summary    := if (request:exists()) then request:get-parameter('summary', 'false') else $trline/@summary
let $summary    := $summary[. = ('true', 'false', 'ids')]

(: useful for index-admin link to pure xml instead of preprocessed :)
let $ada        := if (request:exists()) then request:get-parameter('ada', 'false') = 'true' else $trline/@ada = 'true'
let $raw        := if ($ada) then true() else if (request:exists()) then request:get-parameter('raw', 'false') = 'true' else $trline/@raw = 'true'

let $result     := 
    try {
        if ($raw) then (
            let $rawdata :=
                if ($from[string-length() gt 0]) then 
                    ada:getCollection($app, 'data')//*[@app = $app][@id = $from]
                else
                if ($id[string-length() gt 0]) then 
                    ada:getCollection($app, 'data')//*[@app = $app][@id = $id]
                else (
                    ada:getCollection($app, 'data')//*[@app = $app]
                )
            return
                if ($ada) then $rawdata/ancestor::*[last()] else $rawdata
        )
        else (
            adaxml:getXmlData($app, $id, $from, $summary, $trid, $treff)
        )
    }
    catch * {<error query="{if (request:exists()) then request:get-query-string() else ()}" id="{$id}" app="{$app}" transactionId="{$trid}" transactionEffectiveDate="{$treff}" from="{$from}" summary="{$summary}">Caught error {$err:code}: {$err:description}</error>}
    
let $logOn      := if ($ada:logOn) then ada:log('get-data-result', $app, <result>{$result}</result>) else ()

return
    if ($result or $id='') then 
        if ($id='') then adaxml:bundle($result) else $result
    else (
        if (request:exists()) then response:set-status-code(404) else (), 
        <error>No data found for id: {data($id)}, app: {data($app)}</error>
    )