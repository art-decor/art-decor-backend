xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:~
:Common ADA functions for processing ADA XML format. The ADA XML format is (XML not rendered well by xqdoc):
:<adaxml>
:  <meta status="{status}">
:  </meta>
:  <data>
:    The actual data, conforming to a schema in projects/{project}/schemas
:  </data>
:  Zero or more translations data, i.e. CDA, SOAP etc.
:</adaxml>
:
:@version 0.1
:)
module namespace adaxml         = "http://art-decor.org/ns/ada-xml";
import module namespace ada     = "http://art-decor.org/ns/ada-common" at "ada-common.xqm";
(: John Snelson's JSON parser, see: http://john.snelson.org.uk/post/48547628468/parsing-json-into-xquery :)
(:import module namespace xqjson = "http://xqilla.sourceforge.net/lib/xqjson" at "xqjson.xql";:)
import module namespace jx      = "http://joewiz.org/ns/xquery/json-xml" at "json-xml.xqm";
declare namespace validation    = "http://exist-db.org/xquery/validation";

(:~
: Removes empty values from adaxml/data. Elements are considered empty when they don't have or 
: contain an attribute (other than @hidden and @conceptId) or text() node with data. text() with 
: whitespace only is considered empty.
: Cleaning happens on elements adaextension and adaextension-type too.
:
:@param $elements Sequence of elements from which empty values will be removed.
:@return The sequence of elements from input, without empty attributes, and hidden elements removed
:)
declare function adaxml:removeEmptyValues($elements as element()*) as element()* {
    (:for $element in $elements[empty(@hidden)][.//(@* except (@hidden | @conceptId))[not(. = '')] | .//text()[not(normalize-space(.) = '')]]:)
    for $element in $elements[empty(@hidden)][.//@*[not(name() = ('hidden', 'conceptId', 'datatype'))][not(. = '')] | .//text()[not(normalize-space(.) = '')]]
    let $newNode := 
        element {name($element)} {
            if ($element/*) then (
                (: For concept groups (with children) :)
                (: all attributes, process element children :)
                $element/@*,
                for $child in $element/*
                return adaxml:removeEmptyValues($child)
            )
            else (
                (: For concept items :)
                (:only conceptId for empty concepts, all for non-empty concepts:)
                (:if ($element/@value='') then ($element/@conceptId) else ($element/@*):)
                $element/@*[not(. = '')]
            )
        }
     return $newNode
};

(:~
: Adds missing elements to adaxml/data
: This will restructure ADA XML according to the newXml format
: If an element exists in data, pick that, otherwise pick the newXml one.
: When ADA XML is present in an older format, this will retain all data which are the same between old and new format, 
: discard what has become obsolete, and add what's new.
: Will also add all elements which were empty (not present) in data.
:
:@param $elements Sequence of elements to which attributes will be added (and to child nodes as well)
:@param $nexXml Elements which contains an empty XML instance with all attributes
:@param $forXml 
:   For XML serializations, we return [element-name]-start as start hook for insertions
:   For JSON we return an empty element, which makes an (empty) JSON array for insertions 
:@return The sequence of elements from input, with missing elements
:)
declare function adaxml:addElements($data as node()*, $newXml as node()*, $spec as element()*, $forXml as xs:boolean) as node()* {
    (: For each concept in newXml :)
    for $newElement in $newXml
    let $definitionElement      := $spec//concept[@id = $newElement/@conceptId]
    (: HACK. this line seems completely superfluous, but without it, sometimes yields 2 hits, where one of them is not the right one :)
    let $definitionElement      := $definitionElement[@id = $newElement/@conceptId]
    (: select all corresponding data elements :)
    let $dataElements := 
        if ($newElement[@hidden = 'true']) then 
            $newElement 
        else (
            let $d := $data[not(@hidden = 'true')][@conceptId = $newElement/@conceptId]
            return
            if ($d) then $d else (
                $data[not(@hidden = 'true')][local-name() = local-name($newElement)]
            )
        )
    return 
        (: If there are corresponding data elements :)
        if ($dataElements) then (
            (: return the data elements with attributes, also add attributes from newXml which were missing, and process children :)
            for $dataElement in $dataElements
            let $codedConcept           := $definitionElement[valueDomain[@type = 'code'] | valueDomain[@type = 'ordinal']]
            (: for codes we want to check if the @value based on @localId is still correct according 
                to the spec or expand the localId in @value to the right code/codeSystem etc.:)
            let $dataElement            := 
                if ($codedConcept and $dataElement[@value | @code]) then adaxml:addCode($dataElement, $spec) else ($dataElement)
            return 
                if (name($newElement) = 'adaextension' or name($newElement) = 'adaextension-type') then
                    (: special case for extensibility. don't touch, just copy :)
                    $dataElements
                else (
                    element {name($newElement)} 
                    {
                        $dataElement/@*
                        , 
                        for $att in $newElement/@*
                        return
                            if ($dataElement/@*[name() = name($att)])               then () else
                            if (name($att) = 'conceptId')                           then ($att) else 
                            if (name($att) = 'datatype')                            then ($att) else
                            
                            (: never reinstate value if nullFlavor exists :)
                            if ($dataElement[@nullFlavor] and name($att) = 'value') then () else
                            
                            (: never reinstate nullFlavor if value exists :)
                            if ($dataElement[@value] and name($att) = 'nullFlavor') then () else
                            
                            (: never reinstate ordinal. the user can do that in the form if needed 
                                and it can cause trouble if added empty + xs:decimal datatype.
                                if ordinal was relevant the user will already have populated 
                                it in the first time :)
                            if (name($att) = 'ordinal')                         then () else
                            
                            (: everything except nullFlavor, root and unit :)
                            if ($dataElement[@datatype = 'code']) then (
                                if (name($att) = 'nullFlavor')  then () else
                                if (name($att) = 'root') then () else
                                if (name($att) = 'unit') then () else 
                                ($att)
                            ) else
                            (: only value and root :)
                            if ($dataElement[@datatype = 'identifier'])         then (
                                if (name($att) = 'value') then ($att) else
                                if (name($att) = 'root') then ($att) else
                                ()
                            ) else
                            (: only value and unit :)
                            if ($dataElement[@datatype = 'quantity'])           then (
                                if (name($att) = 'value') then ($att) else
                                if (name($att) = 'unit') then ($att) else
                                ()
                            )
                            else
                            if ($dataElement[@nullFlavor])                          then (attribute {name($att)} {''}) else
                            if (name($att) = 'value' and $dataElement[@code])       then (attribute value {adaxml:addCode($dataElement, $spec)/@value}) else
                            if (name($att) = 'root' and $dataElement[@value])       then (attribute root {''}) else 
                            $att
                        ,
                        adaxml:addElements($dataElement/*, $newElement/*, $spec, $forXml)
                    }
                )
        )
        else (
            (: if we want start hooks, return only the start hook, since there was no data element :)
            if ($forXml) then 
                if ($newElement/@hidden) then $newElement else 
                (: but if there is no @hidden element corresponding to newElement, then output newElement anyway 
                    (this is a mandatory element, which probably was newly introduced after data were stored
                    2020-03-15 AH You'd think that just the conceptId would be enough but it turned out not to be so local-name() added
                :)
                if ($newXml/../*[local-name() = local-name($newElement)][@conceptId = $newElement/@conceptId][@hidden]) then () 
                else
                if ($definitionElement[@minimumMultiplicity][@minimumMultiplicity != '0'] | $definitionElement[@isMandatory = 'true']) then
                    $newElement
                else ()
            else 
            (: if we don't want start hooks, don't return the hidden elements but an empty element :)
            if ($newElement/@hidden) then () else $newElement
        )
};

(:~
:Removes conceptId from all nodes recursively
:
:@param $elements Sequence of elements to which conceptId will be added (and to child nodes as well)
:@return The sequence of elements from input, with conceptId removed
:)
declare function adaxml:removeConceptId($elements as element()*) as element()* {
    for $element in $elements
    let $newel := 
        element {name($element)} 
        {$element/(@* except @conceptId), for $child in $element/* return adaxml:removeConceptId($child)}
    return $newel
};

(:~
:Adds conceptId to all concept nodes recursively
:
:@param $elements Sequence of elements to which conceptId will be added (and to child nodes as well)
:@param $spec A single enhanced dataset for a particular transaction, usually from a specific {project}-{version}-ada-release.xml
:@return The sequence of elements from input, with conceptId
:)
declare function adaxml:addConceptId($elements as element()*, $spec as element()*) as element()* {
    for $element in $elements
    let $specConcept    := $spec/descendant-or-self::*[@shortName=local-name($element)][1]
    let $conceptId      := $specConcept/@id
    let $newel          := 
        element {name($element)} {
            $element/(@* except @conceptId), 
            if ($conceptId) then attribute conceptId {data($conceptId)} else (),
            for $child in $element/* return adaxml:addConceptId($child, $specConcept)
        }
    return $newel
};

(:~
: Adds code, codeSystem, codeSystemName, codeSystemVersion, displayname, ordinal and preferred (if relevant) to coded concepts
:
:@param $elements Sequence of elements to which code etc. will be added (and to child nodes as well)
:@param $spec A single enhanced dataset for a particular transaction, usually from a specific {project}-{version}-ada-release.xml
:@return The sequence of elements from input, with code etc.
:)
declare function adaxml:addCode($elements as element()*, $spec as element()) as element()* {
    for $element in $elements
    (: No support for multiple valueSets, if there's more than one, first will be used. See: ada2release.xsl too. :)
    let $language       := $spec/ancestor::*/project/@language
    let $concept        := $spec//concept[@id = $element/@conceptId]
    (: HACK. this line seems completely superfluous, but without it, sometimes 2 hits are yielded, where one of them is not the right one :)
    let $concept        := $concept[@id = $element/@conceptId]
    let $codedConcept   := $concept[valueDomain[@type = 'code'] | valueDomain[@type = 'ordinal']]
    let $valueSet       := $codedConcept/valueSet[conceptList/(concept | exception)]
    let $theCode        := 
        if ($element[@code][@codeSystem]) then
            $valueSet[1]/conceptList/concept[@code = $element/@code][@codeSystem = $element/@codeSystem] | $valueSet[1]/conceptList/exception[@code = $element/@code][@codeSystem = $element/@codeSystem]
        else (
            $valueSet[1]/conceptList/concept[@localId = $element/@value] | $valueSet[1]/conceptList/exception[@localId = $element/@value]
        )
    
    let $complexConcept := $concept[valueDomain[@type = 'complex']]
    let $unitConcept    := $concept[valueDomain[@type = 'quantity'] | valueDomain[@type = 'duration'] | valueDomain[property/@unit]]
    let $idConcept      := $concept[valueDomain[@type = 'identifier']]
    return
        element {name($element)} {
            if ($theCode) then (
                $element/(@* except (@value|@code|@codeSystem|@codeSystemName|@codeSystemVersion|@displayName|@ordinal|@root|@unit)),
                attribute value {$theCode[1]/@localId},
                $theCode[1]/@code,
                $theCode[1]/@codeSystem,
                $theCode[1]/@codeSystemName,
                $theCode[1]/@codeSystemVersion,
                (: source: valueDomain/conceptList/concept/name :)
                if ($theCode[@code][1]/name[@language = $language]) then
                    attribute displayName {($theCode[@code][1]/name[@language = $language])[1]}
                else
                (: source: SNOMED CT :)
                if ($theCode[1]/designation[@language = $language][@type = 'fsn'][@displayName]) then
                    ($theCode[1]/designation[@language = $language][@type = 'fsn']/@displayName)[1]
                else
                (: source any CodeSystem :)
                if ($theCode[1]/designation[@language = $language][@type = 'preferred'][@displayName]) then
                    ($theCode[1]/designation[@language = $language][@type = 'preferred']/@displayName)[1]
                else
                if ($theCode/@displayName) then (
                    ($theCode/@displayName)[1]
                )
                else (
                    $element/@displayName[string-length() gt 0]
                ),
                $theCode[1]/@ordinal,
                if ($unitConcept) then ($element/@unit) else ()
            )
            else
            if ($unitConcept or ($complexConcept and $element[@datatype = 'quantity'])) then (
                $element/(@* except @root)
            )
            else
            if ($idConcept or ($complexConcept and $element[@datatype = 'identifier'])) then (
                $element/(@* except @unit)
            )
            else
            if ($complexConcept and $element[@datatype = 'code']) then (
                $element/(@* except (@unit | @root))
            )
            else (
                $element/@*
            )
            ,
            for $child in $element/* return adaxml:addCode($child, $spec)
        }
};

(:~
: Adds localId when @code is provided but @value is omitted
:
:@param $elements Sequence of elements to which conceptId will be added (and to child nodes as well)
:@param $spec A single enhanced dataset for a particular transaction, usually from a specific {project}-{version}-ada-release.xml
:@return The sequence of elements from input, with localId
:)
declare function adaxml:addLocalId($elements as element()*, $spec as element()*) as element()* {
    for $element in $elements
    let $concept        := $spec[@id = $element/@conceptId]
    let $codedConcept   := $concept[valueDomain[@type = 'code'] | valueDomain[@type = 'ordinal']]
    let $valueSet       := $codedConcept/valueSet[conceptList/(concept | exception)]
    let $theCode        := 
        if ($element[@code][@codeSystem]) then
            $valueSet[1]/conceptList/concept[@code = $element/@code][@codeSystem = $element/@codeSystem] | $valueSet[1]/conceptList/exception[@code = $element/@code][@codeSystem = $element/@codeSystem]
        else (
            $valueSet[1]/conceptList/concept[@localId = $element/@value] | $valueSet[1]/conceptList/exception[@localId = $element/@value]
        )
    
    let $localId        := $theCode/@localId
    let $newel          := 
        element {name($element)} 
        {
            if ($localId) then (
                $element/(@* except @value),
                if (count($localId) le 1) then 
                    attribute value {$localId}
                else (
                    $element/@value |
                    attribute error {concat('More than one localId found for code ', $element/@code, ', concept ', $element/@conceptId)} 
                )
            )
            else (
                $element/@*
            ),
            for $child in $element/* return adaxml:addLocalId($child, $spec/*)
        }
    return $newel
};

(:~
:Validate a data in an ADA XML document with schema. Tests for existence of document and schema. 
:
:@param $doc 
:@return step element
:)
declare function adaxml:validateSchema($doc as node()) as node() {
    adaxml:validateSchema($doc, false())
};

(:~
:Validate a data in an ADA XML document with draft schema. Tests for existence of document and schema. 
:
:@param $doc 
:@return step element
:)
declare function adaxml:validateSchema($doc as node(), $draft as xs:boolean) as node() {
    let $schemaUri := if ($draft) then ada:getSchemaUri($doc, true()) else ada:getSchemaUri($doc, false())
    let $schema := adaxml:getDocument($schemaUri)
    let $result := validation:jaxv-report($doc, $schema)
    return $result
};

(:~
:Convert data using stylesheet
:
:@param $doc            Node
:@param $stylesheetUri  URI string
:@return fhir
:)
declare function adaxml:convertNode($doc as node(), $stylesheetUri as xs:string) as node()*{
    let $result := 
        try {
            let $stylesheet:= adaxml:getDocument($stylesheetUri)
            return transform:transform($doc, $stylesheet, ()) }
        catch * {
            <exception type="stylesheet" role="error" xslt="{$stylesheetUri}">
                <description>ERROR {$err:code} in transform: {$err:description, "', module: ", $err:module}</description>
                <location line="{$err:line-number}" column="{$err:column-number}"/>
            </exception>
        }
    return $result
};

(:~
: Safe version of doc($uri), raises a 'DocNotAvailable' error when doc not available instead of 
: returning empty sequence. To be used when document must exist.
:
:@param $uri URI string
:@return document node
:)
declare function adaxml:getDocument($uri as xs:string) as node(){
    let $doc := 
        if (doc-available($uri)) then 
            doc($uri) 
        else (error(QName('http://art-decor.org/ns/error', 'DocNotAvailable'), concat('Document not available: ', $uri))
    )
    return $doc
};

(:~
: Get the new XML for an app
:
:@app    app name
:@return new xml
:)
declare function adaxml:getNewXml($app as xs:string) as node()+{
    adaxml:getNewXml($app, (), ())
};
declare function adaxml:getNewXml($app as xs:string, $transactionId as xs:string?, $transactionEffectiveDate as xs:string?) as node()+{
    if (string-length($transactionId)=0) then 
        ada:getCollection($app, 'new')/*[@id = 'new']
    else
    if (string-length($transactionEffectiveDate)=0) then 
        ada:getCollection($app, 'new')/*[@id = 'new'][@transactionRef = $transactionId]
    else
        ada:getCollection($app, 'new')/*[@id = 'new'][@transactionRef = $transactionId][@transactionEffectiveDate = $transactionEffectiveDate]
};

(:~
: Get XML data. Get empty XML for id='new'. Get all XML for id=''. Add elements which are empty.
:
:@app    app name
:@id     id (uuid for data, or '' for a list, or 'new')
:@from   id of a prototype. This must be a unique id in the ada data collection. IF $from is provided, this document is used as starting point for a new document.
:@summary if true, returns only summary data
:@return new xml
:)
declare function adaxml:getXmlData($app as xs:string, $id as xs:string?, $from as xs:string?) as node()*{
    adaxml:getXmlData($app, $id, $from, 'false', (), ())
};
declare function adaxml:getXmlData($app as xs:string, $id as xs:string?, $from as xs:string?, $summary as xs:string?) as node()*{
    adaxml:getXmlData($app, $id, $from, $summary, (), ())
};
declare function adaxml:getXmlData($app as xs:string, $id as xs:string?, $from as xs:string?, $summary as xs:string?, $transactionId as xs:string?, $transactionEffectiveDate as xs:string?) as node()*{
    (:let $newXml             := adaxml:getNewXml($app, $transactionId, $transactionEffectiveDate):)

    let $originalData := 
        (: Make a new ADA XML doc :)
        if ($id = 'new')  then (
            (: From new XML :)
            if (string-length($from) = 0 or $from = 'new') then 
                adaxml:getNewXml($app, $transactionId, $transactionEffectiveDate)
            (: Except when a 'from' prototype was given, then use that :)
            else (
                (: Look for prototype in data collection :)
                let $prototype := ada:getCollection($app, 'data')//*[@app = $app][@id = $from]
                (: Else look in /new/ collection :)
                let $prototype := if ($prototype) then $prototype else ada:getCollection($app, 'new')/*[@id = $from]
                return 
                    if ($prototype) then 
                        element {local-name($prototype)} {attribute id {'new'}, $prototype/(@* except @id), $prototype/*}
                    else 
                        error(QName('http://art-decor.org/ns/error', 'PrototypeNotFound'), concat('Prototype "', $from, '" not found.'))
            )
        )
        (: If not id = 'new' :)
        else if (string-length($id) = 0)
        (: If no id is given, this is a searchset, return all data :)
        then ada:getCollection($app, 'data')//adaxml/data/*
        (: Else return the existing data for id :)
        else ada:getCollection($app, 'data')//*[@app = $app][@id = $id]
    
    (: Return top element with attributes, return data children and newXml children :)
    (: If empty data is an error it should be converted to error in calling module :)
    for $item in $originalData
    let $rights         :=
        if ($id = 'new') then 'update'
        else 
        if (ada:hasPermission($item, 'update') and ada:hasPermission($item, 'delete')) then 'update delete'
        else 
        if (ada:hasPermission($item, 'update')) then 'update'
        else 
        if (ada:hasPermission($item, 'delete')) then 'delete'
        else ''
    let $owner          := ada:getOwner($item)
    let $lastmodified   := ada:dataLastModified($item)
    let $newStuff       := adaxml:getNewXml($app, $item/@transactionRef, $item/@transactionEffectiveDate)
    order by $item/@shortName, $owner[1], $lastmodified descending
    return 
        element {if ($newStuff) then local-name($newStuff[1]) else local-name($item)} 
        {
            attribute rights {$rights}, 
            attribute owner {$owner},
            $item/@app,
            ($newStuff/@shortName, $item/@shortName)[1],
            $item/(@* except (@rights|@owner|@last-update-date|@app|@shortName)),
            if ($summary = ('true', 'ids')) then (
                let $indexSpec := ada:getTransactionDataset($item, 'index') 
                return (
                    attribute last-update-date {$lastmodified},
                    $item//*[@conceptId = $indexSpec//concept/@id],
                    if ($summary = 'ids') then (
                        for $object in $item/descendant::*[@id]
                        let $title := normalize-space(
                            string-join(
                                for $n in ( $object/*[contains(local-name(), 'naam') or contains(local-name(), 'name')] |
                                            $object/*[contains(local-name(), 'type')])[1]
                                return
                                    for $o in $n/descendant-or-self::*[@value | @displayName]
                                    return 
                                        if ($n[contains(local-name(), 'naam') or contains(local-name(), 'name')] and $o[@displayName]) then () else (
                                            $o/@displayName, $o/@value
                                        )[1]
                            , ' ')
                        )
                        return
                            element {local-name($object)} { 
                                $object/(@* except (@compositeId)),
                                attribute compositeId {string-join(($item/@id, $object/@id), '#')},
                                if ($title = '') then () else (
                                    attribute title {$title}
                                )
                            }
                    ) else ()
                )
            )
            else (
                let $spec       := ada:getTransactionDataset($item)
                return (
                    adaxml:addElements($item/*, $newStuff/*, $spec, true())
            )
        )
        }
};

(:~
: Bundle XML data
:
:@nodes  Nodes to be bundled 
:@return Bundled xml
:)
declare function adaxml:bundle($nodes as element()*) as node(){
    <Bundle>
        <type value="searchset"/>
        {
            for $item in $nodes
            return <entry><resource>{$item}</resource></entry>
        }
    </Bundle> 
};

(:~
: Save ADA XML. Clean up, add codes, issue uuid if not present, and save or update.
:
:@param     $data   ADA XML which conforms to [app-schema].xsd (not ada_[app-schema].xsd)
:@return    The data as saved in the db.
:)
declare function adaxml:saveAdaXml($data as node()) as node()?{
    let $app            := $data/@app 
    let $id             := $data/@id
    let $datadir        := ada:getUri($app, 'data')
    let $spec           := ada:getTransactionDataset($data)
    let $cleanedData    := adaxml:removeEmptyValues($data)
    let $cleanedData    := adaxml:addCode($cleanedData, $spec)
    (: don't store @rights which may be different for each user :)
    let $cleanedData    := element {$cleanedData/local-name()} {$cleanedData/(@* except (@rights, @owner)), $cleanedData/*}
    let $storedData     := ada:getCollection($app, 'data')//*[@app = $app][@id = $id]
    let $result         := 
        (: id must exist, and extra check if value not 'new' (should not occur, but breaks everything if it happens accidentally somewhere) :)
        if ($storedData and $id != 'new') 
        then (
            if (ada:hasPermission($storedData, 'update')) then (
                let $adaxml := $storedData/ancestor::adaxml
                let $dummy  := update replace $adaxml/meta/@last-update-by with ada:strCurrentUserName()
                let $dummy  := update replace $adaxml/meta/@last-update-date with fn:current-dateTime()
                let $dummy  := update delete $adaxml/data/*
                let $update := update insert $cleanedData into $adaxml/data
                return data($id)
            )
            else (
                error(QName('http://art-decor.org/ns/error', 'DocNotAvailable'), 'No permission to update data')
            )
        )
        else (
            let $uuid       := util:uuid()
            let $newdata    := 
                element adaxml {
                    element meta {
                        attribute status {'new'},
                        attribute created-by {ada:strCurrentUserName()},
                        attribute last-update-by {ada:strCurrentUserName()},
                        attribute creation-date {current-dateTime()},
                        attribute last-update-date {current-dateTime()}
                    },
                    element data {
                        element {name($cleanedData)} {$cleanedData/(@* except (@rights, @owner, @id)), attribute id {$uuid}, $cleanedData/*}
                    }
                }
            let $store      := xmldb:store($datadir, concat($uuid, '.xml'), $newdata)
            let $store      := ada:setRights(xs:anyURI($store), 'rw-rw-r--')
            return $uuid
        )
    return <success>{$result}</success>
};

(:~ Type switch on $request-body and return valid xml or nothing

@param $request-body           - required. Input from $request?body
@return valid xml
:)
declare function adaxml:getBodyAsXml($request-body as item()?) as element()? {
    adaxml:getBodyAsXml($request-body, (), ())
};
(:~ Type switch on $request-body and return valid xml or nothing. 

Example input
<map xmlns="http://www.w3.org/2005/xpath-functions">
   <number key="number">557</number>
   <string key="name">Fleece Pullover</string>
   <array key="colorChoices">
      <string>navy</string>
      <string>black</string>
   </array>
   <boolean key="is-current">true</boolean>
   <null key="other"/>
   <map key="priceInfo">
      <number key="price">19.99</number>
      <number key="discount">10.00</number>
   </map>
</map>

Example output
<unknown_object number="557" name="Fleece Pullover" is-current="true" other="">
   <colorChoices>navy</colorChoices>
   <colorChoices>black</colorChoices>
   <priceInfo price="19.99" discount="10.00"/>
</unknown_object>

@param $request-body           - required. Input from $request?body
@param $root                   - optional. Name of the root element if not in json input. Defaults to unknown_object
@param $namespace              - optional. Namespace of the root element. Defaults to empty string.
@return valid xml
:)
declare function adaxml:getBodyAsXml($request-body as item()?, $root as xs:string?, $namespace as xs:string?) as element()? {
    let $xslt           :=
        <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" exclude-result-prefixes="#all" version="3.0">
            <xsl:param name="root" select="'unknown_object'"/>
            <xsl:param name="namespace" select="''"/>
        
            <xsl:template match="/">
                <xsl:apply-templates/>
            </xsl:template>
            
            <xsl:template match="/fn:map">
                <xsl:choose>
                  <xsl:when test=".[empty(fn:map[@key]) or count(*) gt 1]">
                    <xsl:element name="{{$root}}" namespace="{{$namespace}}">
                      <xsl:apply-templates select="(fn:string | fn:boolean | fn:number | fn:null) except fn:*[@key = '#text']"/>
                      <xsl:apply-templates select="(fn:array | fn:map) except fn:*[@key = '#text']"/>
                      <xsl:apply-templates select="fn:*[@key = '#text']"/>
                    </xsl:element>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:apply-templates select="(fn:string | fn:boolean | fn:number | fn:null) except fn:*[@key = '#text']"/>
                    <xsl:apply-templates select="(fn:array | fn:map) except fn:*[@key = '#text']"/>
                    <xsl:apply-templates select="fn:*[@key = '#text']"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:template>
            <xsl:template match="fn:map">
              <xsl:element name="{{(parent::fn:array/@key, @key)}}">
                <xsl:apply-templates select="(fn:string | fn:boolean | fn:number | fn:null) except fn:*[@key = '#text']"/>
                <xsl:apply-templates select="(fn:array | fn:map) except fn:*[@key = '#text']"/>
                <xsl:apply-templates select="fn:*[@key = '#text']"/>
              </xsl:element>
            </xsl:template>
            <xsl:template match="fn:array">
              <xsl:apply-templates select="(fn:string | fn:boolean | fn:number | fn:null) except fn:*[@key = '#text']"/>
              <xsl:apply-templates select="(fn:array | fn:map) except fn:*[@key = '#text']"/>
              <xsl:apply-templates select="fn:*[@key = '#text']"/>
            </xsl:template>
            <xsl:template match="fn:string | fn:boolean | fn:number | fn:null">
              <xsl:choose>
                <xsl:when test="empty(@key) and parent::fn:array/@key">
                  <xsl:element name="{{parent::fn:array/@key}}">
                    <xsl:try select="fn:parse-xml('&lt;div&gt;' || . || '&lt;/div&gt;')/div/node()">
                      <xsl:catch select="text()"/>
                    </xsl:try>
                  </xsl:element>
                </xsl:when>
                <xsl:when test="@key = '#text'">
                  <xsl:try select="fn:parse-xml('&lt;div&gt;' || . || '&lt;/div&gt;')/div/node()">
                    <xsl:catch select="text()"/>
                  </xsl:try>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="{{@key}}" select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:template>
        </xsl:stylesheet>
    let $xsltParameters :=
        <parameters>
        {   if (empty($root)) then () else (
                <param name="root" value="{$root}"/>,
                if (empty($namespace)) then () else (
                    <param name="namespace" value="{$namespace}"/>
                )
            )
        }
        </parameters>
    return
    if (empty($request-body)) then () else (
        typeswitch ($request-body)
        case element() return $request-body
        case document-node() return $request-body/*
        default return (
            (:fn:serialize($request-body, map{'method':'json', 'indent': true(), 'json-ignore-whitespace-text-nodes':'yes'}) => 
            json-to-xml() => transform:transform($xslt, $xsltParameters):)
            fn:serialize($request-body, map{'method':'json', 'indent': true(), 'json-ignore-whitespace-text-nodes':'yes'}) =>
            jx:json-to-xml() => transform:transform($xslt, $xsltParameters)
        )
    )
};

