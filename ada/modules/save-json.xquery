xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace ada ="http://art-decor.org/ns/ada-common" at "ada-common.xqm";
import module namespace adaxml ="http://art-decor.org/ns/ada-xml" at "ada-xml.xqm";

declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";

let $json := if (request:exists()) then util:base64-decode(request:get-data()) else '{
    "measurement_message": [
        {
            "rights": "update",
            "owner": null,
            "last-update-date": "2021-08-20T13:03:57.275+02:00",
            "id": "new",
            "app": "demoapp",
            "shortName": "measurement_message",
            "formName": "measurement_form",
            "transactionRef": "2.16.840.1.113883.3.1937.99.62.3.4.2",
            "transactionEffectiveDate": "2012-09-05T16:59:35",
            "versionDate": null,
            "prefix": "demoapp-",
            "language": "en-US",
            "title": null,
            "name": [
                {
                    "value": "Marc",
                    "conceptId": "2.16.840.1.113883.3.1937.99.62.3.2.4",
                    "datatype": "complex"
                }
            ],
            "bsn": [
                {
                    "value": null,
                    "conceptId": "2.16.840.1.113883.3.1937.99.62.3.2.7",
                    "datatype": "identifier"
                }
            ]
        }
    ]
}'
let $str    := parse-json($json)
let $adaxml := adaxml:getBodyAsXml($str, 'adaxml', ())/*
let $result := adaxml:saveAdaXml($adaxml)

return (
    'OK'
)