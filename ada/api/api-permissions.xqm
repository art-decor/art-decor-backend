xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
module namespace adpfix         = "http://art-decor.org/ns/ada/permissions";
import module namespace ada     = "http://art-decor.org/ns/ada-common" at "../modules/ada-common.xqm";
declare namespace sm            = "http://exist-db.org/xquery/securitymanager";

(:  Mode            Octal
 :  rw-r--r--   ==  0644
 :  rw-rw-r--   ==  0664
 :  rwxr-xr--   ==  0754
 :  rwxr-xr-x   ==  0755
 :  rwxrwxr-x   ==  0775
 :)

(:install path for art (normally /db/apps/), includes trailing slash :)
declare variable $adpfix:root   := repo:get-root();

declare %private function adpfix:exec4group($uri as xs:string, $usergroup as xs:string, $access-string as xs:string) {
    sm:chown($uri,$usergroup),
    sm:chmod($uri,$access-string),
    sm:clear-acl($uri)
    (: in eXist-db 2.2 the sticky bit thing does not work, hence we need an ACL to make that work, i.e. have write permissions for the group 
        https://github.com/eXist-db/exist/issues/3157
    :)
    ,
    sm:add-group-ace($uri, replace($usergroup, '^.*:', ''), true(), 'rwx')
};

declare function adpfix:setAdaPermissions() {
    let $check          := adpfix:checkIfUserDba()
    
    let $ada            := if (sm:group-exists('ada-user')) then () else sm:create-group('ada-user')
    let $ada            := if (sm:get-group-members('ada-user')[. = 'admin']) then () else sm:add-group-member('ada-user', 'admin')
    
    let $strAda         := if (xmldb:collection-available(concat($adpfix:root, 'ada'))) then concat($adpfix:root, 'ada') else xmldb:create-collection($adpfix:root,'ada')
    let $strAdaData     := if (xmldb:collection-available(concat($adpfix:root, 'ada-data'))) then concat($adpfix:root, 'ada-data') else xmldb:create-collection($adpfix:root,'ada-data')
    let $strAdaProjects := if (xmldb:collection-available(concat($strAdaData, '/projects'))) then concat($strAdaData, '/projects') else xmldb:create-collection($strAdaData, 'projects')
    let $strAdaDb       := if (xmldb:collection-available(concat($strAdaData, '/db'))) then concat($strAdaData, '/db') else xmldb:create-collection($strAdaData, 'db')
    let $strAdaBackup   := if (xmldb:collection-available(concat($strAdaData, '/backup'))) then concat($strAdaData, '/backup') else xmldb:create-collection($strAdaData, 'backup')
    let $strAdaLog      := if (xmldb:collection-available(concat($strAdaData, '/log'))) then concat($strAdaData, '/log') else xmldb:create-collection($strAdaData, 'log')
    
    let $conf           := concat($strAdaData, '/conf.xml')
    let $conf           := if (doc-available($conf)) then $conf else
        xmldb:store($strAdaData, 'conf.xml', 
            <uris>
                <!-- This is used by eXist to find Orbeon forms -->
                <orbeon uri="http://localhost:8080/art-decor/"/>
                <!-- Used by eXist to locate css -->
                <css uri="http://localhost:8877/apps/ada/resources/css/ada.css"/>
                <!-- ADA data, as an eXist-db path -->
                <data uri="{$strAdaDb}/"/>
            </uris>
        )
    
    let $dummy          := if (doc($conf)//data[@uri]) then () else update insert <data uri="/db/apps/ada-data/db/"/> into doc($conf)//uris[1]
    let $dummy          := if (doc($conf)//backup[@uri]) then () else update insert <backup uri="/db/apps/ada-data/backup/"/> into doc($conf)//uris[1]
    let $dummy          :=
        for $project in xmldb:get-child-collections($strAdaProjects)
        let $olddatadir := concat($strAdaProjects, '/', $project, '/data')
        let $newdatadir := concat($strAdaDb, '/', $project)
        return 
            if (xmldb:collection-available($newdatadir)) then ()
            else (
                (: create a data dir :)
                let $newdatadir := xmldb:create-collection($strAdaDb, $project)
                (: move the data to db :)
                let $move       := if (xmldb:collection-available($olddatadir)) then (xmldb:move($olddatadir, $newdatadir)) else ()
                return ()
            )
    let $dummy          :=
        for $project in xmldb:get-child-collections($strAdaProjects)
        return 
            if (xmldb:collection-available(concat($strAdaBackup, '/', $project)))
            then ()
            else xmldb:create-collection($strAdaBackup, $project)
    
    let $collPerm       := adpfix:exec4group($strAdaLog, 'admin:ada-user', 'rwxrwxr-x')
    let $collPerm       := adpfix:exec4group($strAdaData, 'admin:ada-user', 'rwxrwxr-x')
    let $collPerm       := adpfix:exec4group($strAdaDb, 'admin:ada-user', 'rwxrwsr-x')
    let $collPerm       := adpfix:exec4group($strAdaBackup, 'admin:ada-user', 'rwxrwxr-x')
    let $collPerm       := adpfix:exec4group($strAdaProjects, 'admin:ada-user', 'rwxr-xr-x')
    let $collPerm       := adpfix:exec4group($strAda, 'admin:ada-user', 'rwxr-xr-x')
    
    let $collPerm       := adpfix:exec4group(concat($strAda, '/helpers'), 'admin:dba', 'rwxr-xr-x')
    let $collPerm       :=
        for $resource in xmldb:get-child-resources(concat($strAda, '/helpers'))
        return 
            adpfix:exec4group(concat($strAda, '/helpers/', $resource), 'admin:dba', 'rwxr-x---')
    let $collPerm       := adpfix:exec4group(concat($strAda, '/modules'), 'admin:ada-user', 'rwxr-xr-x')
    let $collPerm       := adpfix:exec4group(concat($strAda, '/modules'), 'admin:ada-user', 'rwxr-xr-x')
    let $collPerm       :=
        for $resource in xmldb:get-child-resources(concat($strAda, '/modules'))
        return 
            adpfix:exec4group(concat($strAda, '/modules/', $resource), 'admin:ada-user', 'rwxr-x---')
    
    let $collperm       := sm:chmod(xs:anyURI(concat($strAda, '/modules/get-html.xquery')),'rwxr-xr-x')
    let $collperm       := sm:chmod(xs:anyURI(concat($strAda, '/modules/get-data.xquery')),'rwxr-xr-x')
    let $collperm       := sm:chmod(xs:anyURI(concat($strAda, '/modules/get-json.xquery')),'rwxr-xr-x')
    let $collperm       := sm:chmod(xs:anyURI(concat($strAda, '/modules/index.xquery')),'rwxr-xr-x')
    let $collperm       := sm:chmod(xs:anyURI(concat($strAda, '/modules/index-admin.xquery')),'rwxr-xr--')
    let $collperm       := sm:chmod(xs:anyURI(concat($strAda, '/modules/upload-ada-project-zip.xquery')),'rwsr-sr--')
    let $collperm       := sm:chmod(xs:anyURI(concat($strAda, '/modules/login.xquery')),'rwxr-xr-x')
    let $ch             := sm:chgrp(xs:anyURI(concat($strAda, '/modules/toggle-logging.xquery')),'dba')
    let $ch             := sm:chgrp(xs:anyURI(concat($strAda, '/modules/clear-log.xquery')),'dba')
    let $collperm       := sm:chmod(xs:anyURI(concat($strAda, '/modules/ada-common.xqm')),'rwxr-xr-x')
    let $collperm       := sm:chmod(xs:anyURI(concat($strAda, '/modules/ada-xml.xqm')),'rwxr-xr--')
    let $collperm       := sm:chmod(xs:anyURI(concat($strAda, '/modules/json-xml.xqm')),'rwxr-xr--')
    let $collperm       := sm:chmod(xs:anyURI(concat($strAda, '/resources')),'rwxr-xr-x')
    let $collperm       := sm:chmod(xs:anyURI(concat($strAda, '/resources/css')),'rwxr-xr-x')
    let $collperm       := sm:chmod(xs:anyURI(concat($strAda, '/resources/css/ada.css')),'rwxr-xr--')
    let $collperm       := sm:chmod(xs:anyURI(concat($strAda, '/controller.xql')),'rwxr-xr-x')
    let $collperm       := sm:chmod(xs:anyURI(concat($strAdaData, '/conf.xml')),'rwxr-xr-x')
    
    let $collperm       := sm:chgrp(xs:anyURI(concat($strAda, '/modules/toggle-logging.xquery')),'dba')
    let $collperm       := sm:chgrp(xs:anyURI(concat($strAda, '/modules/clear-log.xquery')),'dba')
    (:let $collperm       := sm:chgrp(xs:anyURI(concat($strAda, '/modules/fix-permissions.xquery')),'dba'):)
    
    let $updatedb       :=
        for $f in collection($strAdaDb)//adaxml
        let $owner  := ($f/meta/@created-by)[1]
        let $isuser := sm:user-exists($owner)
        let $col    := util:collection-name($f)
        let $res    := util:document-name($f)
        return
            if ($isuser) then sm:chown(xs:anyURI(concat($col,'/',$res)), concat($owner,':ada-user')) else ()
        
return ()
};

(:~ Check if user is dba :)
declare %private function adpfix:checkIfUserDba() {
    if (sm:is-dba(ada:strCurrentUserName())) then () else (
        error(QName('http://art-decor.org/ns/ada/permissions', 'NotAllowed'), concat('Only dba user can use this module. ',ada:strCurrentUserName()))
    )
};