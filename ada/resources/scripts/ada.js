function showHide(obj) {
  var h = document.getElementById(obj);
  
  if (h.style.display == 'block') {
    h.style.display = 'none'
  } else {
    h.style.display = 'block'
  };
}

function showHideRows() {
  var elements = document.getElementsByClassName('row-valid');
  
  for (var i = 0; i < elements.length;++ i) {
    if (elements[i].style.display == 'none') {
      elements[i].style.display = 'table-row';
    } else {
      elements[i].style.display = 'none';
    };
  }
}