xquery version "3.1";
(:
	Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
	see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace oids    = "http://art-decor.org/ns/tools/oids" at "/db/apps/tools/oids/api/api-oids.xqm";

(: the target collection into which the app is deployed :)
declare variable $target external;

let $doLookup           := oids:createOidRegistriesLookup('hl7org', true())
let $removePackageFiles :=
    for $r in xmldb:get-child-resources($target)
    return
        xmldb:remove($target, $r)

return ()