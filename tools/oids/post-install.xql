xquery version "3.1";
(:
	Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
	see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
	
	This program is free software; you can redistribute it and/or modify it under the terms of the
	GNU Lesser General Public License as published by the Free Software Foundation; either version
	2.1 of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.
	
	The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)
import module namespace art         = "http://art-decor.org/ns/art" at "../../art/modules/art-decor.xqm";
import module namespace adofix      = "http://art-decor.org/ns/oids/permissions" at "api/api-permissions.xqm";
import module namespace oids        = "http://art-decor.org/ns/tools/oids" at "api/api-oids.xqm";

import module namespace sm          = "http://exist-db.org/xquery/securitymanager";
import module namespace xmldb       = "http://exist-db.org/xquery/xmldb";
import module namespace repo        = "http://exist-db.org/xquery/repo";

(: The following external variables are set by the repo:deploy function :)

(: file path pointing to the exist installation directory :)
declare variable $home external;
(: path to the directory containing the unpacked .xar package :)
declare variable $dir external;
(: the target collection into which the app is deployed :)
declare variable $target external;
declare variable $root := repo:get-root();

let $updatepermissions      := adofix:setOidsPermissions()

let $updatelookups          := if (collection(concat($root,'tools/oids-data'))/oidList/oid[@symbolicName]) then () else oids:createOidRegistriesLookup((), true())

let $updatexforms           := try { art:saveFormResources('tools/oids',art:mergeLocalLanguageUpdates('tools/oids')) } catch * {()}

return ()