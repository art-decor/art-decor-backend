xquery version "3.0";
import module namespace oids           = "http://art-decor.org/ns/tools/oids" at "api/api-oids.xqm";

declare variable $exist:path external;
declare variable $exist:resource external;
declare variable $exist:controller external;

let $_debug                     := request:get-header('X-Request-Echo')='true' or request:get-parameter('_debug','false')='true'
let $_request-method            := request:get-method()

return
if ($_debug) then (
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/modules/get-request-echo.xquery"/>
    </dispatch>
)
else 
if ($exist:path eq "/") then
    (: forward root path to index.xql :)
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <redirect url="index.xhtml"/>
    </dispatch>
else
if (oids:isOid($exist:resource)) then
    oids:getOid($exist:resource)
else
    (: everything else is passed through :)
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <cache-control cache="yes"/>
    </dispatch>
    
(:
URI synax:
[N]                                             Some positive integer
[AN]                                            Some string
[OID]                                           Some OID ("^[0-2](\.(0|[1-9][0-9]*))*$")
[ll-CC]                                         ISO 639 language code (lower case)-ISO 3166 country code (upper case). Case sensitive

/tools/oids/                                    [base]
[base]                                          /db/apps/art/modules/get-form.xq?form=oid-registry 
            TODO: not sure yet...

===  QUERYING  ===
[base]?oid=[OID]                                GET oid based on OID value
                                                    -- multiple comma separated values supported as logical OR
                                                    -- parameter may not repeat 
                                                    -- works with all other parameters as logical AND
                                                    -- other methods not supported
[base]?registry=[AN]                            GET from specific registry, parameter may repeat 
                                                    -- works with all other parameters as logical AND
                                                    -- other methods not supported
[base]?description=[AN]                         GET OIDs matching string. Search is lucene based on symbolicName and/or description
                                                    -- parameter may not repeat
                                                    -- supports modifier [ll-CC] to limit search to a particular language, e.g. description:en-US=snomed searches for snomed in en-US description only.
                                                    -- works with all other parameters as logical AND
                                                    -- other methods not supported
[base]?responsibleId=[OID]                      GET OIDs matching OID. Search is exact based on responsibleAuthority/scopingOrganization/id
                                                    -- multiple comma separated values supported as logical OR
                                                    -- parameter may not repeat 
                                                    -- works with all other parameters as logical AND
                                                    -- other methods not supported
[base]?responsibleName=[AN]                     GET OIDs matching string. Search is lucene based on responsibleAuthority/scopingOrganization/name
                                                    -- parameter may not repeat 
                                                    -- works with all other parameters as logical AND
                                                    -- other methods not supported
[base]?status=[AN]                              GET OIDs matching code. Search is exact based on status (pending | completed | retired | deprecated | unknown), 
                                                    -- multiple comma separated values supported as logical OR
                                                    -- parameter may not repeat 
                                                    -- works with all other parameters as logical AND
                                                    -- other methods not supported
[base]?category=[AN]                            GET OIDs matching code. Search is exact based on category (N | NRA | NMN | L | LIO | LNS), 
                                                    -- multiple comma separated values supported as logical OR
                                                    -- parameter may not repeat 
                                                    -- works with all other parameters as logical AND
                                                    -- other methods not supported

===  QUERY RELATED  ===
[base]?_registries                              GET list of registries on this server. 
                                                    -- other parameters will be ignored
                                                    -- other methods not supported
[base]?_sort=[oid | description | responsibleName | status | category]
                                                Sort results based on code, no sorting by default. Description sorts based on oid/description[1]/@value for oid based queries
                                                    -- multiple comma separated values supported. Sorting will be applied on subsequent values
                                                    -- parameter may not repeat
[base]?_count=[N]                               Maximum number of results to return. Defaults to 50.
                                                    -- parameter may not repeat

=== MANAGEMENT ===
# Guest access available for GET [base]/[registry]/[OID]
# Need to be an author for all other access

[base]/[registry]/[api-call]                    Run API method for registry. Supported API-calls:
    myoidregistry                                   POST myoidregistry as new/updated registry. [registry] MUST match myoidregistry/@name
                                                    PUT myoidregistry as new/updated registry. [registry] MUST match myoidregistry/@name
                                                    DELETE myoidregistry
                                                        -- other methods not supported
    
    [OID]                                           See oid/[OID] for GET and DELETE
    oid(/[OID])                                     GET [OID] details from registry, requires /[OID] - Equivalent to [base]?oid=[OID]&registry=[AN]
                                                    POST oid details to registry as add/update, MUST NOT use /[OID] as that could only lead to conflicts with body of the request
                                                    PUT oid details to registry as add/update, MUST NOT use /[OID] as that could only lead to conflicts with body of the request
                                                    DELETE [OID] details from registry, requires /[OID]
                                                        -- other methods not supported
            TODO: to create new OIDs we need access to probably nodes of different categories than L, LIO or LNS, or
                    to OIDs of a given additionalProperty such as Oid_Type = 6 (external Code System). Need a selection method for that
    
    author(/[AN])                                   GET all authors or specific one by username from registry
                                                    POST [author] to registry as add/update
                                                    PUT [author] to registry as add/update
                                                    DELETE [author] from registry
                                                        -- other methods not supported
    
    property(/[AN])                                 GET all scopedAdditionalProperties or specific one by case sensitive code from registry
                                                    POST [property] to registry as add/update
                                                    PUT [property] to registry as add/update
                                                    DELETE [property] from registry
                                                        -- other methods not supported
    
    validTime                                       GET registry validTime from registry
                                                    POST registry validTime to registry as add/update
                                                    PUT registry validTime to registry as add/update
                                                        -- other methods not supported
    
    scopedOID(/[OID])                               GET all scopedOIDs or specific one by OID from registry
                                                    POST scopedOIDs to registry as add/update
                                                    PUT scopedOIDs to registry as add/update
                                                    DELETE scopedOIDs from registry
                                                        -- other methods not supported
    
    name                                            GET registry name from registry
                                                    POST registry name to registry as add/update
                                                    PUT registry name to registry as add/update
                                                        -- other methods not supported
    
    description(/[ll-CC])                           GET all registry descriptions or specific one by case-sensitive language code from registry
                                                    POST registry descriptions to registry as add/update
                                                    PUT registry descriptions to registry as add/update
                                                    DELETE registry descriptions from registry
                                                        -- other methods not supported
    
    lastModifiedDate                                GET registry lastModifiedDate from registry
                                                    POST registry lastModifiedDate to registry as add/update. Updating any OID will trigger this update too.
                                                    PUT registry lastModifiedDate to registry as add/update. Updating any OID will trigger this update too.
                                                    DELETE registry lastModifiedDate from registry
                                                        -- other methods not supported
    
    person                                          GET all registry contact persons from registry
                                                    POST registry contact persons to registry as replacement of existing set
                                                    PUT registry contact persons to registry as replacement of existing set
                                                        -- other methods not supported
    
    hostingOrganization(/[OID])                     GET all registry hostingOrganizations or specific one by [OID] from registry
                                                    POST registry hostingOrganizations to registry as replacement of existing set or specific one by [OID]
                                                    PUT registry hostingOrganizations to registry as add/update
                                                    DELETE registry hostingOrganizations from registry
                                                        -- other methods not supported
:)