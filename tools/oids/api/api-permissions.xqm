xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
module namespace adofix         = "http://art-decor.org/ns/oids/permissions";
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
declare namespace sm            = "http://exist-db.org/xquery/securitymanager";

(:  Mode            Octal
 :  rw-r--r--   ==  0644
 :  rw-rw-r--   ==  0664
 :  rwxr-xr--   ==  0754
 :  rwxr-xr-x   ==  0755
 :  rwxrwxr-x   ==  0775
 :)

(:install path for art (normally /db/apps/), includes trailing slash :)
declare variable $adofix:root   := repo:get-root();

(:
:   Call to fix any potential permissions problems in the paths
:       /db/apps/art/modules
:       /db/apps/art/resources
:       /db/apps/art-data
:   Dependency: $get:strArt, $get:strArtResources, $get:strArtData
:)
declare function adofix:setOidsPermissions() {
    adofix:checkIfUserDba(),
    adofix:setOidsQueryPermissions(),
    adofix:setOidsDataPermissions()
};

(:
:   Call to fix any potential permissions problems in the path /db/apps/art/modules
:   Dependency: $get:strArt
:)
declare %private function adofix:setOidsQueryPermissions() {
    let $strOidsModules := concat($adofix:root,'tools/oids/modules/')
    for $query in xmldb:get-child-resources(xs:anyURI($strOidsModules))
    return (
        sm:chown(xs:anyURI(concat($strOidsModules,$query)),'admin:decor'),
        if (starts-with($query,('art-decor','check','collect','get','login','retrieve','search'))) then
            sm:chmod(xs:anyURI(concat($strOidsModules,$query)),sm:octal-to-mode('0755'))
        else(
            sm:chmod(xs:anyURI(concat($strOidsModules,$query)),sm:octal-to-mode('0754'))
        )
        ,
        sm:clear-acl(xs:anyURI(concat($strOidsModules,$query)))
    )
};

(:
:   Call to fix any potential permissions problems in the path /db/apps/tools/oids-data
:   Dependency: $get:strOidsData
:)
declare %private function adofix:setOidsDataPermissions() {
    for $file in xmldb:get-child-resources(xs:anyURI($get:strOidsData))
    return (
        sm:chown(xs:anyURI(concat($get:strOidsData,'/',$file)),'admin:decor'),
        sm:chmod(xs:anyURI(concat($get:strOidsData,'/',$file)),sm:octal-to-mode('0644')),
        sm:clear-acl(xs:anyURI(concat($get:strOidsData,'/',$file)))
    )
};

(:
:   Helper function with recursion for adofix:setDecorPermissions()
:)
declare %private function adofix:setPermissions($path as xs:string, $collown as xs:string, $collmode as xs:string, $resown as xs:string, $resmode as xs:string) {
    sm:chown(xs:anyURI($path),$collown),
    sm:chmod(xs:anyURI($path),$collmode),
    sm:clear-acl(xs:anyURI($path)),
    for $res in xmldb:get-child-resources(xs:anyURI($path))
    return (
        sm:chown(xs:anyURI(concat($path,'/',$res)),$resown),
        sm:chmod(xs:anyURI(concat($path,'/',$res)),$resmode),
        sm:clear-acl(xs:anyURI(concat($path,'/',$res)))
    )
    ,
    for $collection in xmldb:get-child-collections($path)
    return
        adofix:setPermissions(concat($path,'/',$collection), $collown, $collmode, $resown, $resmode)
};

declare %private function adofix:checkIfUserDba() {
    if (sm:is-dba(get:strCurrentUserName())) then () else (
        error(QName('http://art-decor.org/ns/oids/permissions', 'NotAllowed'), concat('Only dba user can use this module. ',get:strCurrentUserName()))
    )
};