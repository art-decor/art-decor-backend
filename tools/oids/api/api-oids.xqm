xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:
:   Library of OID Registry functions. Most if not all functions are available in two flavors.
:   One based on registry name and the other based on some element inside the registry usually the decor
:   root element. It is the expectation that these function are sometimes called somewhere in the middle of 
:   other logic. Therefor you may be at any point in the OID Registry at the stage where to you need to call
:   a function in this library.
:   The $myoidregistry parameter is always used as $myoidregistry/ancestor-or-self::myoidregistry to make sure we have the 
:   correct starting point.
:   Note: unlike Java, XQuery doesn't distinguish functions by the same based on signature of the parameters
:         so all functions that take $myoidregistry are postfixed with a P in the function name.
:)
module namespace oids           = "http://art-decor.org/ns/tools/oids";
import module namespace art     = "http://art-decor.org/ns/art" at "../../../art/modules/art-decor.xqm";
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";

declare namespace error         = "http://art-decor.org/ns/decor/error";
declare namespace sm            = "http://exist-db.org/xquery/securitymanager";
declare namespace xs            = "http://www.w3.org/2001/XMLSchema";

(:~
:   Legend:
:   Any guest/user always has read access
:
:   @email/         Not supported: handled by api-user-settings for now.
:   @notifier
:
:   @id             User id is an integer that increments with 1 for every user. The id is registry unique and does not (necessarily) match across registries
:                           Used e.g. to bind issue authors/assignments to registry authors
:
:   @username       Username that corresponds with his exist-db username
:
:   @active         User is active if there is a registry/author[@username=$username][@active='true']. Note that they might be deactivated at exist-db level
:                           Maps to: (author[@username=$username][empty(@active)] and $username=sm:get-group-members('decor'))
:)
declare variable $oids:AUTHOR-TEMPLATE as element(author)   := <author id="" username="" active="true"/>;

declare variable $oids:iso-13582-schema                     := concat($get:strOidsCore, '/iso-13582-2015.xsd');
declare variable $oids:myoidregistry-schema                 := concat($get:strOidsCore, '/DECORmyoidregistry.xsd');
declare variable $oids:additionalProperties                 := ($get:strKeyCanonicalUriPrefd, $get:strKeyCanonicalUri, $get:strKeyCanonicalUriPrefdDSTU2, $get:strKeyCanonicalUriPrefdSTU3, $get:strKeyCanonicalUriPrefdR4, $get:strKeyHL7v2Table0396CodePrefd);

declare function oids:getOid($oid as xs:string) as element(oid)* {
    oids:getOidP($get:colOidsData/myoidregistry, $oid)
};
declare function oids:getOid($registryName as xs:string?, $oid as xs:string) as element(oid)* {
    if (empty($registryName)) then
        oids:getOidP($get:colOidsData/myoidregistry, $oid)
    else (
        $get:colOidsData//oid[dotNotation[@value = $oid]][ancestor::myoidregistry[@name = $registryName]]
    )
};
declare function oids:getOidP($myoidregistry as element(myoidregistry)*, $oid as xs:string) as element(oid)* {
    if (empty($myoidregistry)) then ()
    else (
        $myoidregistry//oid[dotNotation[@value = $oid]]
    )
};

declare function oids:getOidLookup($oid as xs:string) as element(oid)* {
    oids:getOidLookup((), $oid)
};
declare function oids:getOidLookup($registryName as xs:string?, $oid as xs:string) as element(oid)* {
    if (empty($registryName)) then
        $get:colOidsData//oid[@oid = $oid]
    else (
        $get:colOidsData//oid[@oid = $oid][ancestor::oidList[@name = $registryName]]
    )
};
declare function oids:getOidLookupP($myoidregistrylookup as element(oidList)+, $oid as xs:string) as element(oid)* {
    $myoidregistrylookup/oid[@oid = $oid]
};

declare function oids:getMyOidRegistry($registryName as xs:string) as element(myoidregistry)? {
    $get:colOidsData/myoidregistry[@name = $registryName]
};
declare function oids:getOidLookupFile($registryName as xs:string) as element(oidList)? {
    $get:colOidsData/oidList[@name = $registryName]
};
declare function oids:getOidLookupFileP($myoidregistry as element(myoidregistry)) as element(oidList)? {
    oids:getOidLookupFile($myoidregistry/@name)
};

(: ========== UPDATING OID (LOOKUPS) ========== :)

declare function oids:updateOid($registryName as xs:string, $oids as element(oid)+) {
    let $myoidregistry  := oids:getMyOidRegistry($registryName)
    
    let $error          :=
        if ($myoidregistry) then () else (
            error(xs:QName('error:NoRegistry'), concat('Registry ''', $registryName, ''' does not exist'))
        )
    
    return
        oids:updateOidP($myoidregistry, $oids)
};
declare function oids:updateOidP($myoidregistry as element(myoidregistry), $oids as element(oid)+) {
    let $check          := oids:checkEditPermissionsP($myoidregistry)
    
    let $preparedOids   := for $oid in $oids return oids:prepareOid($oid, true())
    
    let $updateOids      := 
        for $oid in $preparedOids
        let $storedOid      := oids:getOidP($myoidregistry, $oid/dotNotation/@value)
        return
            if ($storedOid) then
                update replace $storedOid with $oid
            else
                update insert $oid into $myoidregistry/registry
    
    let $updateModifyDate   := if ($preparedOids) then oids:updateRegistryLastModifiedDate($myoidregistry) else ()
    
    return ()
};

declare function oids:updateOidLookup($registryName as xs:string, $oids as element(oid)+) {
    let $myoidregistrylookup    := oids:getOidLookupFile($registryName)
    
    return
        if ($myoidregistrylookup) then 
            oids:updateOidLookupP($myoidregistrylookup, $oids)
        else (
            error(xs:QName('error:NoRegistryLookup'),concat('Registry ',$registryName,' does not have a lookup file yet. Please create that first.'))
        )
};
declare function oids:updateOidLookupP($myoidregistrylookup as element(oidList), $oids as element(oid)+) {
    let $preparedOids   := for $oid in $oids return oids:prepareOidLookup($oid)
    
    for $oid in $preparedOids
    let $storedOid      := oids:getOidLookupP($myoidregistrylookup, $oid/@oid)
    return
        if ($storedOid) then 
            update replace $storedOid with $oid 
        else (
            update insert $oid into $myoidregistrylookup
        )
};

declare function oids:createOidRegistry($registryName as xs:string, $oids as element(oid)*, $force as xs:boolean) as xs:string {
    let $storedOidRegistry  := oids:getMyOidRegistry($registryName)
    
    return
        if ($storedOidRegistry and not($force)) then
            error(xs:QName('error:RegistryExists'), concat('Cannot create registry ''', $registryName, '''. It already exists'))
        else (
            oids:createOidRegistryP(oids:prepareMyOidRegistry($registryName, $oids), $force, false())
        )
};
declare function oids:createOidRegistryP($myoidregistry as element(myoidregistry), $force as xs:boolean, $fullvalidation as xs:boolean) as xs:string {
    let $storedOidRegistry  := oids:getMyOidRegistry($myoidregistry/@name)
    
    return
        if ($storedOidRegistry and not($force)) then
            error(xs:QName('error:RegistryExists'), concat('Cannot create registry ''', $myoidregistry/@name, '''. It already exists'))
        else (
            let $myoidregistry      := if ($fullvalidation) then oids:prepareMyOidRegistryP($myoidregistry) else ($myoidregistry)
            let $check              := validation:jaxv-report($myoidregistry, doc($oids:myoidregistry-schema))
            
            return
                if ($check//status = 'invalid') then error(xs:QName('error:NotSchemaValid'), $check) else (
                    xmldb:store($get:strOidsData, concat($myoidregistry/@name, '-oids.xml'), $myoidregistry)
                )
        )
};

declare function oids:createOidRegistryLookup($registryName as xs:string, $force as xs:boolean) as element(lookup) {
    oids:createOidRegistryLookupP(oids:getMyOidRegistry($registryName), $force)
};
declare function oids:createOidRegistryLookupP($myoidregistry as element(myoidregistry), $force as xs:boolean) as element(lookup) {
    let $registryCollection := util:collection-name($myoidregistry)
    let $registryName       := $myoidregistry/@name
    let $lookupFileName     := concat($registryName, 'oids-lookup.xml')
    let $lookupPath         := concat($registryCollection, '/', $lookupFileName)
    let $needsRefresh       := 
        if ($force) then true() else 
        if (doc-available($lookupPath)) then (
            (: true if lookup file older than registry :)
            xmldb:last-modified($registryCollection, $lookupFileName) lt xmldb:last-modified($registryCollection, util:document-name($myoidregistry))
        ) 
        else (
            true()
        )
    let $update             :=
        if ($needsRefresh) then 
            xmldb:store($registryCollection, $lookupFileName, oids:prepareOidLookupFileP($myoidregistry))
        else (
            $lookupPath
        )
    
    return
        <lookup registryName="{$registryName}" oids="{count($myoidregistry//oid)}" lastModified="{xmldb:last-modified($registryCollection, $lookupFileName)}" path="{$update}">
        {
            if ($needsRefresh) then () else attribute norefreshneeded {''}
        }
        </lookup>
        
};

declare function oids:createOidRegistriesLookup($registryNames as xs:string*, $force as xs:boolean) as element(result) {
    oids:createOidRegistriesLookupP(for $registryName in $registryNames return oids:getMyOidRegistry($registryName), $force)
};
declare function oids:createOidRegistriesLookupP($registries as element(myoidregistry)*, $force as xs:boolean) as element(result) {
    let $registries         := if (empty($registries)) then collection($get:strOidsData)//myoidregistry else $registries
    
    let $lookupFiles        := 
        for $registry in $registries
        return 
            oids:createOidRegistryLookupP($registry, $force)

    return
        <result time="{current-dateTime()}">{$lookupFiles}</result>
};

(: ========== SCOPED ADDITIONAL PROPERTY ========== :)

(:~ Returns all scopedAdditionalProperty from a registry. Note that this does not take the exist-db level into account :)
declare function oids:getScopedAdditionalProperties($registryName as xs:string) as element(scopedAdditionalProperty)* {
    oids:getScopedAdditionalPropertiesP(oids:getMyOidRegistry($registryName))
};
(:~ Returns all scopedAdditionalProperty from a registry. Note that this does not take the exist-db level into account :)
declare function oids:getScopedAdditionalPropertiesP($myoidregistry as element()) as element(scopedAdditionalProperty)* {
    $myoidregistry/ancestor-or-self::myoidregistry/scopedAdditionalProperties/scopedAdditionalProperty
};

(:~ Returns scopedAdditionalProperty from a registry. Note that this does not take the exist-db level into account :)
declare function oids:getScopedAdditionalProperty($registryName as xs:string, $propertyCode as xs:string) as element(scopedAdditionalProperty)* {
    oids:getScopedAdditionalPropertyP(oids:getMyOidRegistry($registryName), $propertyCode)
};
(:~ Returns scopedAdditionalProperty from a registry. Note that this does not take the exist-db level into account :)
declare function oids:getScopedAdditionalPropertyP($myoidregistry as element(), $propertyCode as xs:string) as element(scopedAdditionalProperty)* {
    $myoidregistry/ancestor-or-self::myoidregistry/scopedAdditionalProperties/scopedAdditionalProperty[@code = $propertyCode]
};

(:~ Add/Update scopedAdditionalProperty from a registry. Note that this does not take the exist-db level into account :)
declare function oids:updateScopedAdditionalProperty($registryName as xs:string, $scopedAdditionalProperty as element(scopedAdditionalProperty)*) {
    oids:updateScopedAdditionalPropertyP(oids:getMyOidRegistry($registryName), $scopedAdditionalProperty)
};
(:~ Add/Update scopedAdditionalProperty from a registry. Note that this does not take the exist-db level into account :)
declare function oids:updateScopedAdditionalPropertyP($myoidregistry as element(), $scopedAdditionalProperty as element(scopedAdditionalProperty)*) {
    let $check                              := oids:checkEditPermissionsP($myoidregistry)
    
    let $preparedAdditionalProperties       := oids:prepareScopedAdditionalProperty($scopedAdditionalProperty)
    
    for $preparedAdditionalProperty in $preparedAdditionalProperties
    let $storedProperty     := oids:getScopedAdditionalPropertyP($myoidregistry, $preparedAdditionalProperty/@code)
    return
        if ($storedProperty) then
            update replace $storedProperty with $preparedAdditionalProperty
        else
            update insert $preparedAdditionalProperty into $myoidregistry/scopedAdditionalProperties
};

(:~ Update scopedAdditionalProperties from a registry. Note that this does not take the exist-db level into account :)
declare function oids:updateScopedAdditionalProperties($registryName as xs:string, $scopedAdditionalProperties as element(scopedAdditionalProperties)*) {
    oids:updateScopedAdditionalPropertiesP(oids:getMyOidRegistry($registryName), $scopedAdditionalProperties)
};
(:~ Update scopedAdditionalProperties from a registry. Note that this does not take the exist-db level into account :)
declare function oids:updateScopedAdditionalPropertiesP($myoidregistry as element(), $scopedAdditionalProperties as element(scopedAdditionalProperties)*) {
    let $check                              := oids:checkEditPermissionsP($myoidregistry)
    
    let $preparedAdditionalProperties       := <scopedAdditionalProperties>{oids:prepareScopedAdditionalProperty($scopedAdditionalProperties/scopedAdditionalProperty)}</scopedAdditionalProperties>
    let $storedProperties                   := $myoidregistry/scopedAdditionalProperties
    return
        if ($storedProperties) then
            update replace $storedProperties with $preparedAdditionalProperties
        else
            update insert $preparedAdditionalProperties following $myoidregistry/access[last()]
};

(: ========== REGISTRY SCOPEDOID ========== :)

(:~ Returns name from a registry. Note that this does not take the exist-db level into account :)
declare function oids:getRegistryScopedOIDs($registryName as xs:string) as element(scopedOID)* {
    oids:getRegistryScopedOIDsP(oids:getMyOidRegistry($registryName))
};
(:~ Returns name from a registry. Note that this does not take the exist-db level into account :)
declare function oids:getRegistryScopedOIDsP($myoidregistry as element()) as element(scopedOID)* {
    $myoidregistry/ancestor-or-self::myoidregistry/registry/scopedOID
};

(:~ Add/Update name from a registry. Note that this does not take the exist-db level into account :)
declare function oids:updateRegistryScopedOIDs($registryName as xs:string, $scopedOIDs as element(scopedOID)*) {
    oids:updateRegistryScopedOIDsP(oids:getMyOidRegistry($registryName), $scopedOIDs)
};
(:~ Add/Update name from a registry. Note that this does not take the exist-db level into account :)
declare function oids:updateRegistryScopedOIDsP($myoidregistry as element(), $scopedOIDs as element(scopedOID)*) {
    let $check                              := oids:checkEditPermissionsP($myoidregistry)
    
    let $preparedOIDs                       := oids:prepareOidST($scopedOIDs)
    let $storedOIDs                         := oids:getRegistryScopedOIDsP($myoidregistry)
    return
        if ($storedOIDs) then
            update replace $storedOIDs with $preparedOIDs
        else
            update insert $preparedOIDs following $myoidregistry/registry/validTime[last()]
};

(: ========== REGISTRY NAME ========== :)

(:~ Returns name from a registry. Note that this does not take the exist-db level into account :)
declare function oids:getRegistryName($registryName as xs:string) as element(name)* {
    oids:getRegistryNameP(oids:getMyOidRegistry($registryName))
};
(:~ Returns name from a registry. Note that this does not take the exist-db level into account :)
declare function oids:getRegistryNameP($myoidregistry as element()) as element(name)* {
    $myoidregistry/ancestor-or-self::myoidregistry/registry/name
};

(:~ Add/Update name from a registry. Note that this does not take the exist-db level into account :)
declare function oids:updateRegistryName($registryName as xs:string, $name as element(name)?) {
    oids:updateRegistryNameP(oids:getMyOidRegistry($registryName), $name)
};
(:~ Add/Update name from a registry. Note that this does not take the exist-db level into account :)
declare function oids:updateRegistryNameP($myoidregistry as element(), $name as element(name)?) {
    let $check                              := oids:checkEditPermissionsP($myoidregistry)
    
    let $preparedName                       := oids:prepareOidST($name)
    let $storedName                         := oids:getRegistryNameP($myoidregistry)
    return
        if ($storedName) then
            update replace $storedName with $preparedName
        else
            update insert $preparedName following ($myoidregistry/registry/validTime | $myoidregistry/registry/scopedOID)[last()]
};

(: ========== REGISTRY DESCRIPTION ========== :)

(:~ Returns descriptions from a registry. Note that this does not take the exist-db level into account :)
declare function oids:getRegistryDescription($registryName as xs:string) as element(description)* {
    oids:getRegistryDescriptionP(oids:getMyOidRegistry($registryName))
};
(:~ Returns descriptions from a registry. Note that this does not take the exist-db level into account :)
declare function oids:getRegistryDescriptionP($myoidregistry as element()) as element(description)* {
    $myoidregistry/ancestor-or-self::myoidregistry/registry/description
};

(:~ Add/Update descriptions from a registry. Note that this does not take the exist-db level into account :)
declare function oids:updateRegistryDescription($registryName as xs:string, $description as element(description)*) {
    oids:updateRegistryDescriptionP(oids:getMyOidRegistry($registryName), $description)
};
(:~ Add/Update descriptions from a registry. Note that this does not take the exist-db level into account :)
declare function oids:updateRegistryDescriptionP($myoidregistry as element(), $description as element(description)*) {
    let $check                              := oids:checkEditPermissionsP($myoidregistry)
    
    let $preparedDescriptions               := oids:prepareOidST($description)
    let $storedDescriptions                 := oids:getRegistryDescriptionP($myoidregistry)
    
    return
        if ($preparedDescriptions) then 
            if ($storedDescriptions) then (
                update insert $preparedDescriptions following $storedDescriptions[last()],
                update delete $storedDescriptions
            )
            else
                update insert $preparedDescriptions following ($myoidregistry/registry/validTime | $myoidregistry/registry/scopedOID | 
                                                               $myoidregistry/registry/name)[last()]
        else ()
};

(: ========== REGISTRY LASTMODIFIEDDATE ========== :)

(:~ Returns lastModifiedDate from a registry. Note that this does not take the exist-db level into account :)
declare function oids:getRegistryLastModifiedDate($registryName as xs:string) as xs:dateTime? {
    oids:getRegistryLastModifiedDateP(oids:getMyOidRegistry($registryName))
};
(:~ Returns lastModifiedDate from a registry. Note that this does not take the exist-db level into account :)
declare function oids:getRegistryLastModifiedDateP($myoidregistry as element()) as xs:dateTime? {
    let $lastModifiedDate   := $myoidregistry/ancestor-or-self::myoidregistry/registry/lastModifiedDate/@value
    let $lastModifiedDate   := 
        if ($lastModifiedDate) then (
            oids:hl7TStoDateTime($lastModifiedDate)
        ) else (
            xmldb:last-modified(util:collection-name($myoidregistry), util:document-name($myoidregistry))
        )
    
    return
        $lastModifiedDate[. castable as xs:dateTime]
};

(:~ Add/Update lastModifiedDate from a registry. Note that this does not take the exist-db level into account :)
declare function oids:updateRegistryLastModifiedDate($registryName as xs:string) {
    oids:updateRegistryLastModifiedDateP(oids:getMyOidRegistry($registryName))
};
(:~ Add/Update lastModifiedDate from a registry. Note that this does not take the exist-db level into account :)
declare function oids:updateRegistryLastModifiedDateP($myoidregistry as element()) {
    let $check                              := oids:checkEditPermissionsP($myoidregistry)
    
    let $preparedModificationDate           := 
        oids:prepareOidTS(<lastModifiedDate value="{xmldb:last-modified(util:collection-name($myoidregistry), util:document-name($myoidregistry))}"/>)
    let $storedModificationDate             := $myoidregistry/ancestor-or-self::myoidregistry/registry/lastModifiedDate
    
    return
        if ($storedModificationDate) then (
            update insert $preparedModificationDate following $storedModificationDate,
            update delete $storedModificationDate
        )
        else
            update insert $preparedModificationDate following ($myoidregistry/registry/validTime | $myoidregistry/registry/scopedOID | 
                                                               $myoidregistry/registry/name | $myoidregistry/registry/description)[last()]
};

(: ========== REGISTRY PERSON ========== :)

(:~ Returns name from a registry. Note that this does not take the exist-db level into account :)
declare function oids:getRegistryPersons($registryName as xs:string) as element(person)* {
    oids:getRegistryPersonsP(oids:getMyOidRegistry($registryName))
};
(:~ Returns name from a registry. Note that this does not take the exist-db level into account :)
declare function oids:getRegistryPersonsP($myoidregistry as element()) as element(person)* {
    $myoidregistry/ancestor-or-self::myoidregistry/registry/description
};

(:~ Add/Update name from a registry. Note that this does not take the exist-db level into account :)
declare function oids:updateRegistryPersons($registryName as xs:string, $person as element(person)+) {
    oids:updateRegistryPersonsP(oids:getMyOidRegistry($registryName), $person)
};
(:~ Add/Update name from a registry. Note that this does not take the exist-db level into account :)
declare function oids:updateRegistryPersonsP($myoidregistry as element(), $person as element(person)+) {
    let $check                              := oids:checkEditPermissionsP($myoidregistry)
    
    let $preparedPersons                    := oids:prepareOidPerson($person)
    let $storedPersons                      := oids:getRegistryPersonsP($myoidregistry)
    
    return
        if ($preparedPersons) then 
            if ($storedPersons) then (
                update insert $preparedPersons following $storedPersons[last()],
                update delete $storedPersons
            )
            else
                update insert $preparedPersons following ($myoidregistry/registry/validTime | $myoidregistry/registry/scopedOID | 
                                                          $myoidregistry/registry/name | $myoidregistry/registry/description | 
                                                          $myoidregistry/registry/lastModifiedDate)[last()]
        else ()
};

(: ========== REGISTRY HOSTING ORGANIZATION ========== :)

(:~ Returns name from a registry. Note that this does not take the exist-db level into account :)
declare function oids:getRegistryHostingOrganizations($registryName as xs:string) as element(hostingOrganization)* {
    oids:getRegistryHostingOrganizationsP(oids:getMyOidRegistry($registryName))
};
(:~ Returns name from a registry. Note that this does not take the exist-db level into account :)
declare function oids:getRegistryHostingOrganizationsP($myoidregistry as element()) as element(hostingOrganization)* {
    $myoidregistry/ancestor-or-self::myoidregistry/registry/hostingOrganization
};

(:~ Add/Update name from a registry. Note that this does not take the exist-db level into account :)
declare function oids:updateRegistryHostingOrganizations($registryName as xs:string, $hostingOrganizations as element(hostingOrganization)+) {
    oids:updateRegistryHostingOrganizationsP(oids:getMyOidRegistry($registryName), $hostingOrganizations)
};
(:~ Add/Update name from a registry. Note that this does not take the exist-db level into account :)
declare function oids:updateRegistryHostingOrganizationsP($myoidregistry as element(), $hostingOrganizations as element(hostingOrganization)+) {
    let $check                              := oids:checkEditPermissionsP($myoidregistry)
    
    let $preparedOrganizations              := oids:prepareOidOrganization($hostingOrganizations)
    let $storedOrganizations                := oids:getRegistryHostingOrganizationsP($myoidregistry)
    
    return
        if ($preparedOrganizations) then 
            if ($storedOrganizations) then (
                update insert $preparedOrganizations following $storedOrganizations[last()],
                update delete $storedOrganizations
            )
            else
                update insert $preparedPersons following (  $myoidregistry/registry/validTime | $myoidregistry/registry/scopedOID | 
                                                            $myoidregistry/registry/name | $myoidregistry/registry/description | 
                                                            $myoidregistry/registry/lastModifiedDate | $myoidregistry/registry/person)[last()]
        else ()
};

(: ==========    USER MANAGEMENT    ========== :)

(:~ Returns all authors from a registry. Note that this does not take the exist-db level into account :)
declare function oids:getAuthors($registryName as xs:string) as xs:string* {
    oids:getAuthorsP(oids:getMyOidRegistry($registryName))
};

(:~ Returns all authors from a registry. Note that this does not take the exist-db level into account :)
declare function oids:getAuthorsP($myoidregistry as element()) as xs:string* {
    $myoidregistry/ancestor-or-self::myoidregistry/access/author/@username
};

(:~ Returns all active authors from a registry. Note that this does not take the exist-db level into account :)
declare function oids:getActiveAuthors($registryName as xs:string) as xs:string* {
    oids:getActiveAuthorsP(oids:getMyOidRegistry($registryName))
};

(:~ Returns all active authors from a registry. Note that this does not take the exist-db level into account :)
declare function oids:getActiveAuthorsP($myoidregistry as element()) as xs:string* {
    $myoidregistry/ancestor-or-self::myoidregistry/access/author[@active = 'true']/@username | 
    $myoidregistry/ancestor-or-self::myoidregistry/access/author[empty(@active)]/@username
};

(:~ Returns all inactive authors from a registry. Note that this does not take the exist-db level into account :)
declare function oids:getInactiveAuthors($registryName as xs:string) as xs:string* {
    oids:getInactiveAuthorsP(oids:getMyOidRegistry($registryName))
};

(:~ Returns all inactive authors from a registry. Note that this does not take the exist-db level into account :)
declare function oids:getInactiveAuthorsP($myoidregistry as element()) as xs:string* {
    $myoidregistry/ancestor-or-self::myoidregistry/access/author[@active = 'false']/@username
};

(:~ Returns boolean true|false whether or not the current user is author in the given registry. Note that they might be inactive and/or deactivated at exist-db level :)
declare function oids:isAuthor($registryName as xs:string) as xs:boolean {
    oids:isAuthorP(oids:getMyOidRegistry($registryName), get:strCurrentUserName())
};

(:~ Returns boolean true|false whether or not the given user is author in the given registry. Note that they might be inactive and/or deactivated at exist-db level :)
declare function oids:isAuthor($registryName as xs:string, $username as xs:string) as xs:boolean {
    oids:isAuthorP(oids:getMyOidRegistry($registryName), $username)
};

(:~ Returns boolean true|false whether or not the current user is author in the given registry. Note that they might be inactive and/or deactivated at exist-db level :)
declare function oids:isAuthorP($myoidregistry as element()) as xs:boolean {
    oids:isAuthorP($myoidregistry, get:strCurrentUserName())
};

(:~ Returns boolean true|false whether or not the given author is active in the given registry. Note that they might be inactive and/or deactivated at exist-db level :)
declare function oids:isAuthorP($myoidregistry as element(), $username as xs:string) as xs:boolean {
    oids:getAuthorsP($myoidregistry) = $username
};

(:~ Returns boolean true|false whether or not the current user is active in the given registry. Note that they might be deactivated at exist-db level :)
declare function oids:isActiveAuthor($registryName as xs:string) as xs:boolean {
    oids:isActiveAuthorP(oids:getMyOidRegistry($registryName), get:strCurrentUserName())
};

(:~ Returns boolean true|false whether or not the given user is active in the given registry. Note that they might be deactivated at exist-db level :)
declare function oids:isActiveAuthor($registryName as xs:string, $username as xs:string) as xs:boolean {
    oids:isActiveAuthorP(oids:getMyOidRegistry($registryName), $username)
};

(:~ Returns boolean true|false whether or not the current user is active in the given registry. Note that they might be deactivated at exist-db level :)
declare function oids:isActiveAuthorP($myoidregistry as element()) as xs:boolean {
    oids:isActiveAuthorP($myoidregistry, get:strCurrentUserName())
};

(:~ Returns boolean true|false whether or not the given user is active in the given registry. Note that they might be deactivated at exist-db level :)
declare function oids:isActiveAuthorP($myoidregistry as element(), $username as xs:string) as xs:boolean {
    oids:getActiveAuthorsP($myoidregistry) = $username
};

(:~ Returns boolean true|false whether or not the current user is active in the given registry and can edit.
:   Note that they might be deactivated at exist-db level
:)
declare function oids:authorCanEdit($registryName as xs:string) as xs:boolean {
    oids:authorCanEditP(oids:getMyOidRegistry($registryName), get:strCurrentUserName())
};

(:~ Returns boolean true|false whether or not the current user is active in the given registry and can edit.
:   Note that they might be deactivated at exist-db level
:)
declare function oids:authorCanEdit($registryName as xs:string, $username as xs:string) as xs:boolean {
    oids:authorCanEditP(oids:getMyOidRegistry($registryName), $username)
};

(:~ Returns boolean true|false whether or not the current user is active in the given registry and can edit.
:   Note that they might be deactivated at exist-db level
:)
declare function oids:authorCanEditP($myoidregistry as element()) as xs:boolean {
    oids:authorCanEditP($myoidregistry, get:strCurrentUserName())
};

(:~ Returns boolean true|false whether or not the user is active in the given registry and can edit.
:   Note that they might be deactivated at exist-db level
:)
declare function oids:authorCanEditP($myoidregistry as element(), $username as xs:string) as xs:boolean {
    let $author         := oids:getAuthor($myoidregistry, get:strCurrentUserName())
    
    return
        exists($author[@rights = 'rw'])
};

(:~ Updates a user in a registry with given username and the first available increment as id. Adds the user if necessary when $addIfNecessary=true
:   Upon adding the user, all permissions are defaulted from $oids:AUTHOR-TEMPLATE. In updating the user settings only those $setting attributes 
:   are considered that match an attribute in $oids:AUTHOR-TEMPLATE.
:   Returns the resulting <author.../> element or error
:)
declare function oids:setAuthor($registryName as xs:string, $username as xs:string, $settings as element(author)) as element(author) {
    oids:setAuthorP(oids:getMyOidRegistry($registryName), $username, $settings)
};

(:~ Updates a user in a registry with given username and the first available increment as id. Adds the user if necessary when $addIfNecessary=true
:   Upon adding the user, all permissions are defaulted from $oids:AUTHOR-TEMPLATE. In updating the user settings only those $setting attributes 
:   are considered that match an attribute in $oids:AUTHOR-TEMPLATE.
:   Returns the resulting <author.../> element or error
:)
declare function oids:setAuthorP($myoidregistry as element(), $username as xs:string, $settings as element(author)) as element(author) {
    let $check                              := oids:checkEditPermissionsP($myoidregistry)
    
    (: get update set. skip anything empty. skip anything we don't know (includes email and notifier) :)
    let $update-atts                        :=
        for $att in $settings/(@active|@rights)[not(.='')]
        return
        if ($att castable as xs:boolean) then ($att) else (
            error(xs:QName('error:PermissionInvalid'),concat('Permission ',$att/name(),' must be a boolean value. Found ''',$att/string(),''''))
        )
    
    let $myoidregistry                      := $decor/ancestor-or-self::myoidregistry
    let $registryName                       := $myoidregistry/@name
    
    (: add the user with default template, this takes care of @id/@username/full name :)
    let $author                             := oids:setAuthor($myoidregistry, $username)
    
    (: update properties according to what we determined :)
    let $update                             :=
        if ($author) then (
            for $att in $update-atts
            return
                if ($author/@*[name()=$att/name()]) then (
                    update value $author/@*[name()=$att/name()] with $att
                )
                else (
                    update insert $att into $author
                )
        ) else (
            error(xs:QName('error:AuthorNotFound'),'Author with this username does not exist in this registry. Cannot update properties.')
        )
    
    return
        $author
};

(:~ Adds a user to a registry with given username and the first available increment as id. 
:   All permissions are taken from $decor:AUTHOR-TEMPLATE
:   Returns author element or error
:
:   Dependency aduser:getUserDisplayName($username)
:)
declare %private function oids:setAuthor($myoidregistry as element(myoidregistry)?, $username as xs:string) as element(author) {
    (:permissions are checked by caller, don't need new check:)
    
    let $maxid      := if ($myoidregistry/access/author[@id]) then max($myoidregistry/access/author/xs:integer(@id)) else (1)
    
    (: we did not use to have :)
    let $update     :=
        for $author at $i in $myoidregistry/access/author[empty(@id)]
        return
            update insert attribute id {$maxid + $i} into $author
    
    let $maxid      := if ($myoidregistry/access/author[@id]) then max($myoidregistry/access/author/xs:integer(@id)) else (1)
    
    let $author     := oids:getAuthor($myoidregistry, $username)
    let $newauthor  :=
        <author>
        {
            attribute id {$maxid + 1},
            attribute username {$username},
            $oids:AUTHOR-TEMPLATE/(@* except (@id|@username))
        }
        </author>
    
    return
        if ($author) then
            (:unexpected but possible... do not need to add anything, just return author as-is:)
            $author
        else (
            (:add user and return what we added:)
            let $add    := update insert $newauthor into $access
            return oids:getAuthor($myoidregistry, $username)
        )
};

(:~ Shortcut call to oids:setAuthor to activate a registry user. Note: does not manipulate the exist-db setting :)
declare function oids:activateAuthor($registryName as xs:string, $username as xs:string) as element(author) {
    oids:setAuthor($registryName, $username, <author active="true"/>)
};

(:~ Shortcut call to oids:setAuthor to activate a registry user. Note: does not manipulate the exist-db setting :)
declare function oids:activateAuthorP($myoidregistry as element(myoidregistry), $username as xs:string) as element(author) {
    oids:setAuthorP($myoidregistry, $username, <author active="true"/>)
};

(:~ Shortcut call to oids:setAuthor to deactivate a registry user. Note: does not manipulate the exist-db setting :)
declare function oids:deactivateAuthor($registryName as xs:string, $username as xs:string) as element(author) {
    oids:setAuthor($registryName, $username, <author active="false"/>)
};

(:~ Shortcut call to oids:setAuthor to deactivate a registry user. Note: does not manipulate the exist-db setting :)
declare function oids:deactivateAuthorP($myoidregistry as element(myoidregistry), $username as xs:string) as element(author) {
    oids:setAuthorP($myoidregistry, $username, <author active="false"/>)
};

(:~ Return full author element to caller. Caller needs to do write access checking if necessary.
:   Should multiple authors by that username exist, then this is a problem of the registry, not this function
:)
declare %private function oids:getAuthor($myoidregistry as element(myoidregistry)?, $username as xs:string) as element(author)? {
    $myoidregistry/access/author[@username = $username]
};

(:~ Return full author element to caller. Caller needs to do write access checking if necessary.
:   Should multiple authors by that username exist, then this is a problem of the registry, not this function
:)
declare %private function oids:getActiveAuthor($myoidregistry as element(myoidregistry)?, $username as xs:string) as element(author)? {
    $myoidregistry/access/author[@active='true'][@username = $username] | 
    $myoidregistry/access/author[empty(@active)][@username = $username]
};

(: =============== UTILITY ================ :)

(:~ Return true() if input matches OID pattern or false() if not :)
declare function oids:isOid($oid as xs:string?) as xs:boolean {
    matches($oid, "^[0-2](\.(0|[1-9][0-9]*))*$")
};

(:~ Return parent node from OID. Example: 1.2.3 will return 1.2 :)
declare function oids:getOidParent($oid as xs:string?) as xs:string? {
    if (oids:isOid($oid)) then 
        string-join(tokenize($oid, '\.')[position() != last()], '.')
    else ()
};

(:~ Return parent node from OID. Example: 1.2.3 will return 3 :)
declare function oids:getOidLeaf($oid as xs:string?) as xs:string? {
    if (oids:isOid($oid)) then 
        tokenize($oid, '\.')[last()]
    else ()
};

(:~ Return xs:dateTime for HL7 TS value if possible or empty() if not :)
declare function oids:hl7TStoDateTime($ts as xs:string?) as xs:dateTime? {
    let $year       := substring($ts, 1, 4)
    let $month      := substring($ts, 5, 2)
    let $day        := substring($ts, 7, 2)
    let $hour       := substring($ts, 9, 2)
    let $minute     := substring($ts, 11, 2)
    let $second     := substring($ts, 13, 2)
    let $subsec     := replace($ts, '\d+(\.\d*).*', '$1')
    let $timezone   := replace($ts, '\d+(\.\d*)?(.*)', '$2')
    let $timezone   := 
        if (matches($timezone, '^[+\-]\d{4}$')) then concat(substring($timezone, 1, 3), ':', substring($timezone, 4, 2)) else ()
    
    let $date       := string-join(($year, $month, $day), '-')
    let $time       := concat(string-join(($hour, $minute, $second), ':'), $subsec, $timezone)
    let $dateTime   := string-join(($date, $time), 'T')
    
    return
        if ($dateTime castable as xs:dateTime) then xs:dateTime($dateTime) else
        if ($ts castable as xs:dateTime) then xs:dateTime($ts) else ()
};

(:~ Return xs:dateTime for HL7 TS value if possible or empty() if not :)
declare function oids:hl7TStoDate($ts as xs:string?) as xs:date? {
    let $year       := substring($ts, 1, 4)
    let $month      := substring($ts, 5, 2)
    let $day        := substring($ts, 7, 2)
    
    let $date       := string-join(($year, $month, $day), '-')
    
    return
        if ($date castable as xs:date) then xs:date($date) else
        if ($ts castable as xs:date) then xs:date($ts) else ()
};

(:~ Return xs:dateTime(@date) of last release or version in the registry or null if none exists :)
declare function oids:getLastRevisionDate($registryName as xs:string) as xs:dateTime? {
    let $myoidregistry      := oids:getMyOidRegistry($registryName)
    return
        if ($myoidregistry) then oids:getLastRevisionDateP($myoidregistry) else ()
};

(:~ Return xs:dateTime(@date) from exist-db modification date :)
declare function oids:getLastRevisionDateP($myoidregistry as element()) as xs:dateTime? {
    xmldb:last-modified(util:collection-name($myoidregistry), util:document-name($myoidregistry))
};

declare function oids:prepareOid($oid as element(oid), $doSchemaCheck as xs:boolean) as element(oid) {
let $preparedOid        :=
    <oid>
    {
        oids:prepareOidST($oid/dotNotation)
        ,
        oids:prepareOidST($oid/asn1Notation)
        ,
        oids:prepareOidST($oid/iriNotation)
        ,
        oids:prepareOidST($oid/symbolicName)
        ,
        oids:prepareOidCS($oid/category)
        ,
        let $status     := oids:prepareOidCS($oid/status)
        return
            if ($status) then $status else <status code="unknown"/>
        ,
        let $creation   := oids:prepareOidTS($oid/creationDate)
        return
            if ($creation) then $creation else <creationDate value="{replace(string(current-dateTime()), '[T:\-]', '')}"/>
        ,
        let $lastmod    := oids:prepareOidTS($oid/lastModifiedDate)
        return
            if ($lastmod) then $lastmod else <lastModifiedDate value="{replace(string(current-dateTime()), '[T:\-]', '')}"/>
        ,
        oids:prepareOidCS($oid/realm)
        ,
        let $desc       := oids:prepareOidED($oid/description, 'en-US')
        return
            if ($desc) then $desc else (
                <description language="en-US" mediaType="text/plain" value="Missing Description">
                    <thumbnail value="Missing Description"/>
                </description>
            )
    }
    
    {
        oids:prepareOidRegistrationAuthority($oid/registrationAuthority)
        ,
        oids:prepareOidResponsibleAuthority($oid/responsibleAuthority)
        ,
        oids:prepareOidSubmittingAuthority($oid/submittingAuthority)
        ,
        oids:prepareOidAdditionalProperty($oid/additionalProperty)
        ,
        oids:prepareOidHistoryAnnotation($oid/historyAnnotation)
        ,
        oids:prepareOidReference($oid/reference)
    }
    </oid>

let $check              := if ($doSchemaCheck) then validation:jaxv-report($preparedOid, doc($oids:iso-13582-schema)) else ()

return
    if ($check//status = 'invalid') then 
        error(xs:QName('error:NotSchemaValid'), $check, $check)
    else (
        $preparedOid
    )
};
declare function oids:prepareOidLookup($oid as element(oid)) as element(oid)? {
    if ($oid/dotNotation/@value[not(string(.) = '')]) then 
        <oid oid="{$oid/dotNotation/@value}" creationDate="{$oid/creationDate/@value}" status="{$oid/status/@code}" symbolicName="{$oid/symbolicName/@value}">
        {
            let $category                   := $oid/additionalProperty[attribute[@value='Oid_Type']]/value/@value
            return
                if ($category) then attribute category {$category} else ()
        }
        {
            let $registrationAuthorityCode  := $oid/registrationAuthority/code/@code
            return
                if ($registrationAuthorityCode) then attribute registrationAuthorityCode {$registrationAuthorityCode[1]} else ()
        }
        {
            for $desc in $oid/description
            return
                <name>
                {
                    $desc/@language,
                    if ($desc/thumbnail[@value]) then 
                        $desc/thumbnail/@value/string() 
                    else (
                        substring($desc/@value/string(),1,200),
                        if (string-length($desc/@value) gt 200) then '...' else ()
                    )
                }
                </name>
        }
        {
            for $desc in $oid/description
            return
                <desc>{$desc/@language, $desc/@value/string()}</desc>
        }
        {
            for $property in $oid/additionalProperty[attribute/@value][value/@value]
            return
                <property type="{$property/attribute/@value}">{$property/value[1]/@value}</property>
        }
        </oid>
    else if ($oid[@oid]) then 
        $oid
    else ()
};

declare function oids:prepareMyOidRegistry($registryName as xs:string, $oids as element(oid)*) as element(myoidregistry) {
    <myoidregistry name="{$registryName}" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../../tools/oids/core/DECORmyoidregistry.xsd">
        <access>
            <author id="1" username="{get:strCurrentUserName()}" rights="rw"/>
        </access>
        <scopedAdditionalProperties>
        {
            for $additionalProperty in $oids:additionalProperties
            return
                <scopedAdditionalProperty code="{$additionalProperty}">
                    <name language="en-US">Default utility property for FHIR System URIs</name>
                    <valueDomain type="string"/>
                </scopedAdditionalProperty>
            ,
            for $additionalProperty in distinct-values($oids//additionalProperty/attribute/@code[not(. = $oids:additionalProperties)])
            return
                <scopedAdditionalProperty code="{$additionalProperty}">
                    <name language="en-US">{$additionalProperty}</name>
                    <valueDomain type="string"/>
                </scopedAdditionalProperty>
        }
        </scopedAdditionalProperties>
        <registry>
            <validTime>
                <low value="{replace(current-dateTime(), '[T:\-]', '')}"/>
            </validTime>
            <name value="{$registryName}"/>
            <description language="en-US" mediaType="text/plain" value="This is the {$registryName} registry"/>
            <person>
                <name>
                    <part value="{get:strCurrentUserName()}"/>
                </name>
            </person>
            <hostingOrganization>
                <name>
                    <part value="ART-DECOR"/>
                </name>
            </hostingOrganization>
        {
            (: no need to schema check the separate oids if we're going to do the whole :)
            for $oid in $oids
            return
                oids:prepareOid($oid, false())
        }
        </registry>
    </myoidregistry>
};
declare function oids:prepareMyOidRegistryP($myoidregistry as element(myoidregistry)) as element(myoidregistry) {
    <myoidregistry name="{$myoidregistry/@name}" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../../tools/oids/core/DECORmyoidregistry.xsd">
        <access>
        {
            if ($myoidregistry/access/author) then $myoidregistry/access/author else (
                <author id="1" username="{get:strCurrentUserName()}" rights="rw"/>
            )
        }
        </access>
        <scopedAdditionalProperties>
        {
            $myoidregistry/scopedAdditionalProperties/scopedAdditionalProperty
            ,
            for $additionalProperty in $oids:additionalProperties[not(. = $myoidregistry/scopedAdditionalProperties/scopedAdditionalProperty/@code)]
            return
                <scopedAdditionalProperty code="{$additionalProperty}">
                    <name language="en-US">Default utility property for FHIR System URIs</name>
                    <valueDomain type="string"/>
                </scopedAdditionalProperty>
            ,
            for $additionalProperty in distinct-values($myoidregistry//additionalProperty/attribute/@code[not(. = $oids:additionalProperties)][not(. = $myoidregistry/scopedAdditionalProperties/scopedAdditionalProperty/@code)])
            return
                <scopedAdditionalProperty code="{$additionalProperty}">
                    <name language="en-US">{$additionalProperty}</name>
                    <valueDomain type="string"/>
                </scopedAdditionalProperty>
        }
        </scopedAdditionalProperties>
        <registry>
        {
            let $validtime      := oids:prepareOidIVL_TS($myoidregistry/registry/validTime)
            return
                if ($validtime) then $validtime else (
                    <validTime><low value="{replace(current-dateTime(), '[T:\-]', '')}"/></validTime>
                )
            ,
            oids:prepareOidST($myoidregistry/registry/scopedOID)
            ,
            let $name           := oids:prepareOidST($myoidregistry/registry/name)
            return
                if ($name) then $name else (
                    <name value="{$myoidregistry/@name}"/>
                )
            ,
            oids:prepareOidED($myoidregistry/registry/description, 'en-US')
            ,
            let $person         := oids:prepareOidPerson($myoidregistry/registry/person)
            return
                if ($person) then $person else if ($myoidregistry/access/author) then
                    for $author in $myoidregistry/access/author
                    return
                        <person><name><part value="{$author/@username}"/></name></person>
                else (
                    <person><name><part value="{get:strCurrentUserName()}"/></name></person>
                )
            ,
            let $organization   := oids:prepareOidOrganization($myoidregistry/registry/hostingOrganization)
            return
                if ($organization) then $organization else (
                    <hostingOrganization><name><part value="ART-DECOR"/></name></hostingOrganization>
                )
            ,
            (: no need to schema check the separate oids if we're going to do the whole :)
            for $oid in $myoidregistry/registry/oid
            return
                oids:prepareOid($oid, false())
        }
        </registry>
    </myoidregistry>
};

declare function oids:prepareOidLookupFile($registryName as xs:string) as element(oidList) {
    let $myoidregistry      := oids:getMyOidRegistry($registryName)
    
    return
        if ($myoidregistry) then oids:prepareOidLookupFileP($myoidregistry) else (
            error(xs:QName('error:NoRegistry'), concat('Registry ''', $registryName, ''' does not exist'))
        )
};
declare function oids:prepareOidLookupFileP($myoidregistry as element(myoidregistry)) as element(oidList)? {
    <oidList name="{$myoidregistry/@name}">
    {
        let $hostingOrganization  := $myoidregistry/registry/hostingOrganization/name/part/@value
        return
            if ($hostingOrganization) then attribute publisher {string-join($hostingOrganization, ' ')} else ()
    }
    {
        for $oid in $myoidregistry/registry/oid
        return
            oids:prepareOidLookup($oid)
    }
    </oidList>
};

declare function oids:prepareOidAD($node as element()*) as element()* {
    for $n in $node[@nullFlavor[not(string(.) = '')] | part/@value[not(string(.) = '')]]
    return
        element {name($n)} {
            $n/@*[not(string(.) = '')],
            $n/part[@value[not(string(.) = '')]],
            $n/useablePeriod[@value[not(string(.) = '')]]
        }
};
declare function oids:prepareOidCS($node as element()*) as element()* {
    for $n in $node[@code[not(string(.) = '')] | @nullFlavor[not(string(.) = '')]]
    return
        element {name($n)} {
            $n/@code[not(string(.) = '')],
            $n/(@* except @code)[not(string(.) = '')]
        }
};
declare function oids:prepareOidED($node as element()*, $language as xs:string?) as element()* {
    for $n in $node[@value[not(string(.) = '')]]
    return
        element {name($n)} {
            if ($n[@language]) then $n/@language else 
            if (empty($language)) then () else attribute language {$language}
            ,
            $n/(@* except @language)[not(string(.) = '')]
            ,
            $n/data[not(empty(.))]
            ,
            $n/xml[@*[not(string(.) = '')] | * | text()[not(string(.) = '')]]
            ,
            oids:prepareOidTEL($n/reference)
            ,
            $n/integrityCheck[not(empty(.))]
            ,
            let $thumbnail  := oids:prepareOidED($n/thumbnail, ())
            return
                if ($thumbnail) then $thumbnail else 
                if (empty($language)) then () else <thumbnail>{$n/@value}</thumbnail>
            ,
            oids:prepareOidST($n/description)
            ,
            oids:prepareOidED($n/translation, ())
        }
};
declare function oids:prepareOidEN($node as element()*) as element()* {
    for $n in $node[@nullFlavor[not(string(.) = '')] | part/@value[not(string(.) = '')]]
    return
        element {name($n)} {
            $n/@*[not(string(.) = '')],
            $n/part[@value[not(string(.) = '')]]
        }
};
declare function oids:prepareOidIVL_TS($node as element()*) as element()* {
    for $n in $node[@nullFlavor[not(string(.) = '')] | */@value[not(string(.) = '')]]
    return
        element {name($n)} {
            $n/@*[not(string(.) = '')]
            ,
            oids:prepareOidTS($n/low)
            ,
            oids:prepareOidTS($n/high)
            ,
            oids:prepareOidQTY($n/width)
            ,
            oids:prepareOidTS($n/any)
        }
};
declare function oids:prepareOidQTY($node as element()*) as element()* {
    for $n in $node[@value[not(string(.) = '')]]
    return
        element {name($n)} {
            $n/@*[not(string(.) = '')]
        }
};
declare function oids:prepareOidST($node as element()*) as element()* {
    for $n in $node[@value[not(string(.) = '')] | @nullFlavor[not(string(.) = '')]]
    return
        element {name($n)} {
            $n/@value[not(string(.) = '')],
            $n/(@* except @value)[not(string(.) = '')]
        }
};
declare function oids:prepareOidTEL($node as element()*) as element()* {
    for $n in $node[@nullFlavor[not(string(.) = '')] | @value[not(string(.) = '')]]
    return
        element {name($n)} {
            $n/@*[not(string(.) = '')],
            $n/useablePeriod[@*[not(string(.) = '')]]
        }
};
declare function oids:prepareOidTS($node as element()*) as element()* {
    for $n in $node[@value[not(string(.) = '')] | @nullFlavor[not(string(.) = '')]]
    return
        element {name($n)} {
            if ($n/@value[not(string(.) = '')]) then attribute value {replace(string($n/@value), '[T:\-]', '')} else (),
            $n/(@* except @value)[not(string(.) = '')]
        }
};
declare function oids:prepareOidPerson($node as element()*) as element()* {
    for $n in $node[name[not(string(@nullFlavor) = '')] | addr[not(string(@nullFlavor) = '')] | telecom[not(string(@nullFlavor) = '')] |
                    name/part[not(string(@value) = '')] | addr/part[not(string(@value) = '')] | telecom[not(string(@value) = '')]]
    return
        element {name($n)} {
            oids:prepareOidEN($n/name)
            ,
            oids:prepareOidAD($n/addr)
            ,
            oids:prepareOidTEL($n/telecom)
        }
};
declare function oids:prepareOidOrganization($node as element()*) as element()* {
    for $n in $node[id[not(string(@nullFlavor) = '')] | name[not(string(@nullFlavor) = '')] | addr[not(string(@nullFlavor) = '')] | telecom[not(string(@nullFlavor) = '')] | 
                    id[not(string(@value) = '')]      | name/part[not(string(@value) = '')] | addr/part[not(string(@value) = '')] | telecom[not(string(@value) = '')]]
    return
        element {name($n)} {
            oids:prepareOidST($n/id)
            ,
            oids:prepareOidEN($n/name)
            ,
            oids:prepareOidAD($n/addr)
            ,
            oids:prepareOidTEL($n/telecom)
        }
};
declare function oids:prepareOidRegistrationAuthority($node as element(registrationAuthority)*) as element(registrationAuthority)* {
    for $n in $node
    return
        element {name($n)} {
            oids:prepareOidCS($n/code)
            ,
            oids:prepareOidPerson($n/person)
            ,
            oids:prepareOidOrganization($n/scopingOrganization)
        }
};
declare function oids:prepareOidResponsibleAuthority($node as element(responsibleAuthority)*) as element(responsibleAuthority)* {
    for $n in $node
    return
        element {name($n)} {
            oids:prepareOidCS($n/code)
            ,
            oids:prepareOidCS($n/statusCode)
            ,
            let $validtime  := oids:prepareOidIVL_TS($n/validTime)
            return
                if ($validtime) then $validtime else <validTime nullFlavor="NI"/>
            ,
            oids:prepareOidPerson($n/person)
            ,
            oids:prepareOidOrganization($n/scopingOrganization)
        }
};
declare function oids:prepareOidSubmittingAuthority($node as element(submittingAuthority)*) as element(submittingAuthority)* {
    for $n in $node
    return
        element {name($n)} {
            oids:prepareOidCS($n/code)
            ,
            oids:prepareOidTS($n/applicationDate)
            ,
            oids:prepareOidPerson($n/person)
            ,
            oids:prepareOidOrganization($n/scopingOrganization)
        }
};
declare function oids:prepareOidAdditionalProperty($additionalProperty as element(additionalProperty)*) as element(additionalProperty)* {
    for $node in $additionalProperty[attribute/@value[not(string(.) = '')]][value/@value[not(string(.) = '')]]
    return
        element {name($node)} {
            $node/attribute,
            $node/value
        }
};
declare function oids:prepareOidHistoryAnnotation($historyAnnotation as element(historyAnnotation)*) as element(historyAnnotation)* {
    for $node in $historyAnnotation[text[not(empty(@value) and empty(@data) and empty(@xml))]]
    return
        element {name($node)} {
            for $childnode in $node/annotationDate[@value[not(string(.) = '')]]
            return
                element {name($childnode)} {
                    replace(string($childnode/@value), '[T:\-]', ''),
                    $childnode/(@* except @value)[not(string(.) = '')]
                }
            ,
            <text>{$node/@*[not(string(.) = '')]}</text>
        }
};
declare function oids:prepareOidReference($reference as element(reference)*) as element(reference)* {
    for $n in $reference[ref/@value[not(string(.) = '')]]
    return
        element {name($n)} {
            for $childnode in $n/ref[@*[not(string(.) = '')]]
            return
                element {name($childnode)} {
                    $childnode/@*[not(string(.) = '')],
                    $childnode/useablePeriod[@*[not(string(.) = '')]]
                }
            ,
            for $childnode in $n/type[@code[not(string(.) = '')]]
            return
                element {name($childnode)} {
                    $childnode/@code[not(string(.) = '')]
                }
            ,
            for $childnode in $n/lastVisitedDate[@value[not(string(.) = '')]]
            return
                element {name($childnode)} {
                    replace(string($childnode/@value), '[T:\-]', ''),
                    $childnode/(@* except @value)[not(string(.) = '')]
                }
        }
};

declare function oids:prepareScopedAdditionalProperty($node as element(scopedAdditionalProperty)*) as element(scopedAdditionalProperty)* {
    for $n in $node[@code[not(string(.) = '')]]
    return
        element {name($node)} {
            $n/@code
            ,
            for $childnode in $node/name return art:parseNode($childnode)
            ,
            for $childnode in $node/desc return art:parseNode($childnode)
            ,
            for $valueDomain in $node/valueDomain
            return
                <valueDomain>
                {
                    $valueDomain/@*,
                    for $conceptList in $valueDomain[@type = ('code', 'ordinal')]/conceptList
                    return
                        <conceptList>
                        {
                            $conceptList/@*,
                            for $conceptListItem in $conceptList/concept[@code]
                            return (
                                <concept>
                                {
                                    $conceptListItem/@code,
                                    $conceptListItem/name,
                                    $conceptListItem/desc
                                }
                                </concept>
                            )
                        }
                        </conceptList>
                    ,
                    for $property in $valueDomain/conceptList/property
                    return
                        <property>{$property/@*[not(string(.) = '')]}</property>
                }
                </valueDomain>
        }
};

(: ========= CHECKS ========== :)

declare function oids:checkEditPermissions($registryName as xs:string) {
    let $myoidregistry  := oids:getMyOidRegistry($registryName)
    
    let $error          :=
        if ($myoidregistry) then () else (
            error(xs:QName('error:NoRegistry'), concat('Registry ''', $registryName, ''' does not exist'))
        )
    
    return
        oids:checkEditPermissionsP($myoidregistry)
};
declare function oids:checkEditPermissionsP($myoidregistry as element()) {
    let $userCanEdit    := oids:authorCanEditP($myoidregistry)
    
    return
        if ($userCanEdit) then () else (
            error(xs:QName('error:NoPermission'), concat('User cannot edit registry ''', $myoidregistry/@name,''''))
        )
};
