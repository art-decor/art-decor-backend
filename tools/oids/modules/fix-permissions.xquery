xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adofix   = "http://art-decor.org/ns/oids/permissions" at "../api/api-permissions.xqm";

let $fix    := adofix:setOidsPermissions()

return
    <return>true</return>
