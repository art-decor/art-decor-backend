xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace oids    = "http://art-decor.org/ns/tools/oids" at "/db/apps/tools/oids/api/api-oids.xqm";

let $registryName           := if (request:exists()) then request:get-parameter('registry', ())[string-length() gt 0] else ('hl7org')
let $force                  := if (request:exists()) then request:get-parameter('force', 'false')='true' else (true())

return
    oids:createOidRegistriesLookup($registryName, $force)