xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get      = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
import module namespace adofix   = "http://art-decor.org/ns/oids/permissions" at "../api/api-permissions.xqm";
import module namespace oids     = "http://art-decor.org/ns/tools/oids" at "/db/apps/tools/oids/api/api-oids.xqm";

declare namespace util          = "http://exist-db.org/xquery/util";
declare namespace xmldb         = "http://exist-db.org/xquery/xmldb";

declare option exist:serialize "method=xml indent=no omit-xml-declaration=no";

let $request            := if (request:exists()) then request:get-data()/content else ()
let $force              := if (request:exists()) then request:get-parameter('force', 'false')='true' else (true())
let $validate           := if (request:exists()) then request:get-parameter('validate', 'false')='true' else (false())

let $filecontent        := if (empty($request)) then () else fn:parse-xml(util:base64-decode($request//data))
let $filecontent        := if (empty($filecontent)) then () else doc(xmldb:store($get:strDecorTemp, concat(util:uuid(), '.xml'), $filecontent))/myoidregistry

let $registryName       := data($filecontent/@name)

let $storeRegistry      := ()

let $storeRegistry      := 
    if ($filecontent instance of element(myoidregistry)) then (
        try {
            let $store              := oids:createOidRegistryP($filecontent, $force, $validate)
            let $doLookup           := oids:createOidRegistriesLookup($filecontent/@name, $force)
            let $fixPermissions     := adofix:setOidsPermissions()
            return 
                $store
        }
        catch * {<error>+++ Creation of OID Registry failed: {$err:code}: {$err:description} {$err:module} [at line {$err:line-number}]</error>}
    )
    else ()

let $dp                 := if (empty($filecontent)) then () else xmldb:remove($get:strDecorTemp, util:document-name($filecontent))

return (
    if (response:exists()) then (response:set-header('Content-Type','text/xml; charset=utf-8')) else (),
    <result dateTime="{current-dateTime()}" name="{$registryName}">{$storeRegistry}</result>
)