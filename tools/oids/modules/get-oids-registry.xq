xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "../../../art/modules/art-decor-settings.xqm";
declare namespace xs        = "http://www.w3.org/2001/XMLSchema";
declare namespace xforms    = "http://www.w3.org/2002/xforms";
declare namespace request   = "http://exist-db.org/xquery/request";
declare option exist:serialize "method=xml media-type=text/xml";

let $registry           := if (request:exists()) then request:get-parameter('registry', ()) else ()
let $registry           := if (count($registry)>1) then () else $registry
let $registrycollection := collection($get:strOidsData)//myoidregistry[@name=$registry]

let $all                := if (request:exists()) then request:get-parameter('all', 'false') else 'false'
let $theoidreg          := if (exists($registrycollection)) then
                                if ($all='true')
                                then $registrycollection/registry/oid
                                else subsequence($registrycollection/registry/oid,1,200)
                           else ()

let $theregistry        := 
    if (exists($registrycollection)) then (
        <registry total="{count($registrycollection/registry/oid)}">
        {
            $registrycollection/registry/@*,
            $registrycollection/registry/*[not(name()='oid')]
        }
        {
            for $oiddata in $theoidreg
            return
                <oid status="{$oiddata/status[1]/@code/string()}" dotNotation="{$oiddata/dotNotation[1]/@value/string()}">
                {
                    $oiddata/description
                }
                </oid>
        }
        </registry>
    )
    else (
        <registry/>
    )

return
    $theregistry