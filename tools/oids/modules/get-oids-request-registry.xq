xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "xmldb:exist:///db/apps/art/modules/art-decor-settings.xqm";
declare namespace xs="http://www.w3.org/2001/XMLSchema";
declare namespace xforms="http://www.w3.org/2002/xforms";

declare namespace request = "http://exist-db.org/xquery/request";

declare option exist:serialize "method=xml media-type=text/xml";

let $registry := request:get-parameter('registry', 'nictiz')

let $theregistry := doc(concat($get:strOidsData,'/',$registry, 'oids-request.xml'))/*

return
    $theregistry