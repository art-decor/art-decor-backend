xquery version "1.0";

import module namespace xmldb   = "http://exist-db.org/xquery/xmldb";
declare namespace cfg           = "http://exist-db.org/collection-config/1.0";

(: The following external variables are set by the repo:deploy function :)

(: file path pointing to the exist installation directory :)
declare variable $home external;
(: path to the directory containing the unpacked .xar package :)
declare variable $dir external;
(: the target collection into which the app is deployed :)
declare variable $target external;
declare variable $root := repo:get-root();

declare %private function local:storeSettings() {
    (: 2.2-LTS or 4.0.0 :)
    let $system-version     := tokenize(system:get-version(), '\.')[1]
    (: new range appears stable from version 4 and up:)
    let $use-new-range      := if ($system-version castable as xs:integer) then xs:integer($system-version) ge 4 else false()
    
    let $oidsConf :=
        <collection xmlns="http://exist-db.org/collection-config/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
            <index xmlns:hl7="urn:hl7-org:v3" xmlns:xs="http://www.w3.org/2001/XMLSchema">
                <fulltext default="none" attributes="false"/>
            {
                let $idx    :=
                    <!--<create qname="@packageRoot" type="xs:string"/>--> |
                    <create qname="@key" type="xs:string"/>
                return
                    if ($use-new-range) then 
                        <range>{ $idx }</range> 
                    else (
                        $idx
                    )
            }
            </index>
        </collection>
    
    let $oidsDataConf :=
        <collection xmlns="http://exist-db.org/collection-config/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
            <index xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:mods="http://www.loc.gov/mods/v3">
                <!-- Disable the old full text index -->
                <fulltext default="none" attributes="false"/>
                <lucene>
                    <analyzer class="org.apache.lucene.analysis.standard.StandardAnalyzer">
                        <param name="stopwords" type="org.apache.lucene.analysis.util.CharArraySet"/>
                    </analyzer>
                    <text qname="name"/>
                    <text qname="desc"/>
                    <text qname="@value"/>
                </lucene>
            {
                let $idx    :=
                    <create qname="@code" type="xs:string"/> |
                    <create qname="@name" type="xs:string"/> |
                    <create qname="@oid" type="xs:string"/> |
                    <create qname="@registrationAuthorityCode" type="xs:string"/> |
                    <create qname="@type" type="xs:string"/> |
                    <create qname="@value" type="xs:string"/>
                return
                    if ($use-new-range) then 
                        (: oddly... the new-range is working against us, not for us :)
                        <range>{ $idx }</range>
                        (:$idx:)
                    else (
                        $idx
                    )
            }
            </index>
        </collection>
    
    return (
        (:create apps dir:)
        for $coll in ('tools/oids','tools/oids-data')
        return (
            if (not(xmldb:collection-available(concat($root,$coll)))) then (
                xmldb:create-collection($root,$coll),
                sm:chown(xs:anyURI(concat('xmldb:exist:///',$root,$coll)),'admin:tools'),
                sm:chmod(xs:anyURI(concat('xmldb:exist:///',$root,$coll)),'rwxrwxr-x'),
                sm:clear-acl(xs:anyURI(concat('xmldb:exist:///',$root,$coll)))
            ) else ()
        )
        ,
        (:create index dir:)
        for $coll in ('tools/oids','tools/oids-data')
        return (
            if (not(xmldb:collection-available(concat('/db/system/config',$root,$coll)))) then (
                xmldb:create-collection(concat('/db/system/config',$root),$coll)
            ) else ()
        )
        ,
        (:move any old-style oids package data into oids-data:)
        if (xmldb:collection-available(concat($root,'tools/oids/data'))) then
            for $resource in xmldb:get-child-resources(concat($root,'tools/oids/data'))
            return (
                xmldb:move(concat($root,'tools/oids/data'),concat($root,'tools/oids-data'),$resource),
                sm:chown(concat($root,'tools/oids-data',$resource),'admin:tools'),
                sm:chmod(concat($root,'tools/oids-data',$resource),'rw-rw-r--')
            )
        else()
        ,
        (:add/overwrite index file and reindex:)
        let $index-file := concat('/db/system/config',$root,'tools/oids/collection.xconf')
        return
        if (doc-available($index-file)) then () else (
            xmldb:store(concat('/db/system/config',$root,'tools/oids'),'collection.xconf',$oidsConf),
            xmldb:reindex(concat($root,'tools/oids'))
        )
        ,
        (:add/overwrite index file and reindex:)
        let $index-file := concat('/db/system/config',$root,'tools/oids-data/collection.xconf')
        return
        if (not(doc-available($index-file)) or doc($index-file)/*[not(*:index/*:range)]) then (
            xmldb:store(concat('/db/system/config',$root,'tools/oids-data'),'collection.xconf', $oidsDataConf),
            xmldb:reindex(concat($root,'tools/oids-data'))
        ) else ()
    )
};

local:storeSettings()