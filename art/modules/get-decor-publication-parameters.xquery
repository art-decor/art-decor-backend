xquery version "3.0";
(:
:   Copyright (C) 2016 ART-DECOR expert group art-decor.org
:   
:   This program is free software; you can redistribute it and/or modify it under the terms of the
:   GNU Lesser General Public License as published by the Free Software Foundation; either version
:   2.1 of the License, or (at your option) any later version.
:   
:   This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
:   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
:   See the GNU Lesser General Public License for more details.
:   
:   The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)

(:
   Query for retrieving decor-parameters.xml (publication parameters) for specific project
:)
import module namespace get             = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art             = "http://art-decor.org/ns/art" at "art-decor.xqm";
import module namespace comp            = "http://art-decor.org/ns/art-decor-compile" at "../api/api-decor-compile.xqm";
import module namespace aduser          = "http://art-decor.org/ns/art-decor-users" at "../api/api-user-settings.xqm";
import module namespace adserver        = "http://art-decor.org/ns/art-decor-server" at "../api/api-server-settings.xqm";
import module namespace iss             = "http://art-decor.org/ns/decor/issue" at "../api/api-decor-issue.xqm";
import module namespace i18n            = "http://art-decor.org/ns/decor/i18n" at "../api/api-decor-i18n.xqm";


let $projectPrefix              := if (request:exists()) then request:get-parameter('project', ())[not(. = '')] else ('demo1-')
let $language                   := if (request:exists()) then request:get-parameter('language', ())[not(. = '')] else ()
let $filterid                   := if (request:exists()) then request:get-parameter('filterid', ())[not(. = '')] else ()

let $decor                      := if (string-length($projectPrefix) gt 0) then art:getDecorByPrefix($projectPrefix) else ()
let $language                   := if (empty($language)) then $decor/project/@defaultLanguage else $language

let $parent                     := if ($decor) then util:collection-name($decor) else ()
let $parameters                 := if ($decor) then collection(util:collection-name($decor))/decor-parameters[util:document-name(.) = 'decor-parameters.xml'] else ()
let $filters                    := if ($decor) then comp:getCompilationFilterSet($decor) else ()

return
    if ($decor) then (
        <decor-parameters xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="https://assets.art-decor.org/ADAR/rv/decor-parameters.xsd">
        {
            (: special attributes to indicate return of default on non-existent decor-parameters.xml and project prefix :)
            attribute {'initialdefault'} {count($parameters/*)=0},
            attribute {'project'} {$projectPrefix},
            (:
                hush baby thru the parameters
            :)
            if ( $parameters[switchCreateSchematron0] )
            then <switchCreateSchematron0/>
            else <switchCreateSchematron1/>,
            
            if ( $parameters[switchCreateSchematronWithWrapperIncludes1] )
            then <switchCreateSchematronWithWrapperIncludes1/>
            else <switchCreateSchematronWithWrapperIncludes0/>,
            
            if ( $parameters[switchCreateSchematronWithWarningsOnOpen1] )
            then <switchCreateSchematronWithWarningsOnOpen1/>
            else <switchCreateSchematronWithWarningsOnOpen0/>,
            
            if ( $parameters[switchCreateSchematronClosed1] )
            then <switchCreateSchematronClosed1/>
            else <switchCreateSchematronClosed0/>,
            
            if ( $parameters[switchCreateSchematronWithExplicitIncludes1] )
            then <switchCreateSchematronWithExplicitIncludes1/>
            else <switchCreateSchematronWithExplicitIncludes0/>,
            
            if ( $parameters[switchCreateDocHTML0] )
            then <switchCreateDocHTML0/>
            else <switchCreateDocHTML1/>,
            
            if ( $parameters[switchCreateDocSVG0] )
            then <switchCreateDocSVG0/>
            else <switchCreateDocSVG1/>,
            
            if ( $parameters[switchCreateDocDocbook1] )
            then <switchCreateDocDocbook1/>
            else <switchCreateDocDocbook0/>,
            
            (: special handling for switchCreateDocPDF1: turn all @include switches (chars) into element content :)
            if ( $parameters[switchCreateDocPDF1] )
            then <switchCreateDocPDF1>
                 {
                     $parameters/switchCreateDocPDF1/normalize-space(replace(@include,'(.)','$1 '))
                 }
                 </switchCreateDocPDF1>
            else <switchCreateDocPDF0/>,
            
            if ( $parameters[useLocalAssets0] )
            then <useLocalAssets0/>
            else <useLocalAssets1/>,
            
            if ( $parameters[useLocalLogos0] )
            then <useLocalLogos0/>
            else <useLocalLogos1/>,
            
            if ( $parameters[useCustomLogo1] )
            then <useCustomLogo1/>
            else <useCustomLogo0/>,
            
            if ( $parameters[useLatestDecorVersion0] )
            then <useLatestDecorVersion0/>
            else <useLatestDecorVersion1/>,

            if ( $parameters[inDevelopment1] )
            then <inDevelopment1/>
            else <inDevelopment0/>,
            
            if ( $parameters[defaultLanguage = $decor/project/name/@language] )
            then $parameters/defaultLanguage
            else <defaultLanguage>{data($decor/project/@defaultLanguage)}</defaultLanguage>,
            
            if ( $parameters[switchCreateDatatypeChecks0] )
            then <switchCreateDatatypeChecks0/>
            else <switchCreateDatatypeChecks1/>,
            
            if ( $parameters[createDefaultInstancesForRepresentingTemplates0] )
            then <createDefaultInstancesForRepresentingTemplates0/>
            else <createDefaultInstancesForRepresentingTemplates1/>,
            
            if ( $parameters[artdecordeeplinkprefix castable as xs:anyURI] )
            then $parameters/artdecordeeplinkprefix
            else <artdecordeeplinkprefix>{adserver:getServerURLArt()}</artdecordeeplinkprefix>,
            
            if ( $parameters[bindingBehavior] )
            then $parameters/bindingBehavior
            else <bindingBehavior valueSets="freeze"/>,
    
            if ( $parameters[switchCreateTreeTableHtml0] )
            then <switchCreateTreeTableHtml0/>
            else <switchCreateTreeTableHtml1/>
        }
        {
            $filters
        }
            <documentation>
            {
                for $e in doc(concat($get:strDecorCore, '/decor-parameters.xsd'))//xs:element
                let $d      := 
                    if ($e/xs:annotation/xs:documentation[@xml:lang=$language]) 
                    then $e/xs:annotation/xs:documentation[@xml:lang=$language]
                    else $e/xs:annotation/xs:documentation[@xml:lang='en-US']
                let $name   := $e/@name | $d/@ref
                group by $name
                return
                    <description name="{$name}">
                    {
                        $d/node()
                    }
                    </description>
            }
            </documentation>
            <transactions>
            {
                for $t in $decor//transaction[@type="group"]
                let $trid       := $t/@id
                let $tred       := $t/@effectiveDate
                return
                    <transaction>
                    {
                        $t/@*,
                        attribute filtered {exists($filters//transaction[@ref = $trid][@flexibility = $tred])},
                        $t/representingTemplate/@ref,
                        $t/name,
                        $t/desc
                    }
                    </transaction>
            }
            </transactions>
            
        </decor-parameters>
    )
    else (
        (: if no project is given, return an error :)
        error(QName('http://art-decor.org/ns/art-decor', 'MissingParameter'), concat('You are missing parameter ''project'' or its value ''',$projectPrefix,''' did not match any known project/@prefix'))
    )