xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace gg          = "http://art-decor.org/ns/decor/governancegroups" at "../api/api-decor-governancegroups.xqm";
declare namespace request           = "http://exist-db.org/xquery/request";

let $groupid            := if (request:exists()) then request:get-parameter('id',())[string-length()>0] else ()
let $language           := if (request:exists()) then request:get-parameter('language',())[string-length()>0] else ()
let $addArtefactList    := if (request:exists()) then request:get-parameter('getdetails','false')[string-length()>0] else ('false')

return
    gg:getGovernanceGroups($groupid, $addArtefactList='true', $language, true())
