xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art     = "http://art-decor.org/ns/art" at "art-decor.xqm";
import module namespace templ   = "http://art-decor.org/ns/decor/template" at "../api/api-decor-template.xqm";

let $id               := if (request:exists()) then request:get-parameter('id',())[string-length()>0][1] else ()
let $name             := if (request:exists()) then request:get-parameter('name',())[string-length()>0][1] else ()
let $ref              := if (request:exists()) then request:get-parameter('ref',())[string-length()>0][1] else ()
let $useRegexMatching := if (request:exists()) then request:get-parameter('regex',false())[string-length()>0][1] else (false())

let $effectiveDate    := if (request:exists()) then request:get-parameter('effectiveDate',())[string-length()>0][1] else ()
let $projectPrefix    := if (request:exists()) then request:get-parameter('project',())[string-length()>0][1] else ()
let $projectVersion   := if (request:exists()) then request:get-parameter('version',())[string-length()>0][1] else ()
let $projectLanguage  := if (request:exists()) then request:get-parameter('language',())[string-length()>0][1] else ()
let $serialize        := if (request:exists()) then not(request:get-parameter('serialize', 'true')[string-length()>0][1] = 'false') else true()

let $templates := 
    if (not(empty($id))) then
        if (empty($projectPrefix)) then
            templ:getExpandedTemplateById($id, $effectiveDate, $serialize)
        else (
            templ:getExpandedTemplateById($id, $effectiveDate, $projectPrefix, $projectVersion, $projectLanguage, $serialize)
        )
    
    else if (not(empty($name))) then
        if (empty($projectPrefix)) then
            templ:getExpandedTemplateByName($name, $effectiveDate, $useRegexMatching, $serialize)
        else (
            templ:getExpandedTemplateByName($name, $effectiveDate, $useRegexMatching, $projectPrefix, $projectVersion, $projectLanguage, $serialize)
        )
    
    else if (not(empty($ref)) and not(empty($projectPrefix))) then
        templ:getExpandedTemplateByRef($ref, $effectiveDate, $projectPrefix, $projectVersion, $projectLanguage, $serialize)
    
    else ()

return
    $templates