xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get             = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace decor           = "http://art-decor.org/ns/decor" at "../api/api-decor.xqm";

let $templateName   := request:get-parameter('template','')
let $effectiveDate  := request:get-parameter('effectiveDate','')
(:let $templateName :='bloedgroep'
let $effectiveDate :='2013-01-30T16:27:09' :)

let $template               := $get:colDecorData//template[@name=$templateName][@effectiveDate=$effectiveDate][@statusCode='new']
let $templateAssociations   := if ($template) then $get:colDecorData//templateAssociation[@templateId=$template/@id][@effectiveDate=$effectiveDate] else ()
let $decor                  := $template/ancestor::decor

let $response           :=
    if (decor:authorCanEditP($decor, $decor:SECTION-RULES)) then (
        update delete $templateAssociations,
        update delete $template,
        <response>OK</response>
    ) else (
        <response>NO PERMISSION</response>
    )
   
return
    $response