xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get    = "http://art-decor.org/ns/art-decor-settings" at "../modules/art-decor-settings.xqm";
import module namespace aduser = "http://art-decor.org/ns/art-decor-users" at "../api/api-user-settings.xqm";

let $projectPrefix     := if (request:exists()) then request:get-parameter('prefix',()) else ()
let $notifierSetting   := if (request:exists()) then request:get-parameter('notifier','') else ('')

let $decorIssues       := $get:colDecorData/decor[project/@prefix=$projectPrefix]/issues

let $projectUpdate     := 
    if (exists($decorIssues) and $notifierSetting=('on','off','')) then
        if ($decorIssues[@notifier]) then 
            update value $decorIssues/@notifier with $notifierSetting
        else (
            update insert attribute notifier {$notifierSetting} into $decorIssues
        )
    else ()

return
    <data-safe>true</data-safe>