xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace iss     = "http://art-decor.org/ns/decor/issue" at "../api/api-decor-issue.xqm";
declare namespace request       = "http://exist-db.org/xquery/request";

let $projectPrefix      := if (request:exists()) then (request:get-parameter('project',())) else ()
let $projectPrefix      := if (request:exists() and empty($projectPrefix)) then (request:get-parameter('prefix',())) else ($projectPrefix)

let $searchTerms        := if (request:exists()) then (tokenize(lower-case(request:get-parameter('searchString',())[string-length()>0]),'\s')) else ()
let $types              := if (request:exists()) then (tokenize(request:get-parameter('type',())[string-length()>0],'\s')) else ()
let $priorities         := if (request:exists()) then (tokenize(request:get-parameter('priority',())[string-length()>0],'\s')) else ()
let $statuscodes        := if (request:exists()) then (tokenize(request:get-parameter('statusCode',())[string-length()>0],'\s')) else ()
let $lastassignedids    := if (request:exists()) then (tokenize(request:get-parameter('assignedTo',())[string-length()>0],'\s')) else ()
let $labels             := if (request:exists()) then (tokenize(request:get-parameter('labels',())[string-length()>0],'\s')) else ()
let $changedafter       := if (request:exists()) then (request:get-parameter('changedafter',())[string-length()>0]) else ()
let $sort               := if (request:exists()) then (request:get-parameter('sort',())[string-length()>0]) else ()
let $max                := if (request:exists()) then (request:get-parameter('max',$iss:maxResults)[string-length()>0]) else ($iss:maxResults)

let $max                := if ($max castable as xs:integer) then xs:integer($max) else (75)
return
    iss:getIssueList($projectPrefix, $searchTerms, $types, $priorities, $statuscodes, $lastassignedids, $labels, $changedafter, $sort, $max)