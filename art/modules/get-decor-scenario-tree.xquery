xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art = "http://art-decor.org/ns/art" at "art-decor.xqm";
declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";

let $projectPrefix          := if (request:exists()) then request:get-parameter('project','')[string-length()>0] else ()
let $projectVersion         := if (request:exists()) then request:get-parameter('version','')[string-length()>0] else ()
let $language               := if (request:exists()) then request:get-parameter('language','')[string-length()>0] else ()

(: if valued, will filter the scenario tree based on whether it references the requested dataset:)
let $datasetId              := if (request:exists()) then request:get-parameter('datasetId', ())[string-length()>0][not(.='*')] else ()
let $datasetEffectiveDate   := if (request:exists()) then request:get-parameter('datasetEffectiveDate', ())[string-length()>0][not(.='*')] else ()

(:let $projectPrefix  := 'demo1-':)

let $filteredScenarios      := 
    if (empty($datasetId))
    then art:getDecorByPrefix($projectPrefix, $projectVersion)//scenarios/scenario
    else art:getScenariosByDataset($datasetId, $datasetEffectiveDate)

return
<scenarios projectPrefix="{$projectPrefix}">
{
    for $scenario in $filteredScenarios
    order by $scenario/name[1]/lower-case(text()[1])
    return
        art:scenarioTree($scenario, $language, $datasetId, $datasetEffectiveDate)
}
</scenarios>
