xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art     = "http://art-decor.org/ns/art" at "art-decor.xqm";
import module namespace decor   = "http://art-decor.org/ns/decor" at "../api/api-decor.xqm";
import module namespace aduser  = "http://art-decor.org/ns/art-decor-users" at "../api/api-user-settings.xqm";

let $request                := if (request:exists()) then request:get-data()/issue else ()

let $decor                  := art:getDecorByPrefix($request/@project)
let $prefix                 := $decor/project/@prefix
let $newId                  := decor:getNextAvailableIdP($decor, $decor:OBJECTTYPE-ISSUE, decor:getDefaultBaseIdsP($decor, $decor:OBJECTTYPE-ISSUE))/@id
let $nowTracking            := format-dateTime(current-dateTime(), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')
let $nowAssignment          := format-dateTime(current-dateTime()+xs:dayTimeDuration('PT1S'), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')
let $issueAuthorId          := $request/tracking/author/@id
let $issueAuthorUserName    := $decor/project/author[@id = $issueAuthorId]/@username
let $issueAssignedId        := $request/tracking/@to[string-length()>0]
let $issueAssignedUserName  := $decor/project/author[@id = $issueAssignedId]/@username
let $issueAssignedName      := $decor/project/author[@id = $issueAssignedId]/string()

let $newIssue               :=
    <issue id="{$newId}" priority="{$request/@priority}" displayName="{$request/@displayName}" type="{$request/@type}">
    {
        if ($request/object) then
            <object id="{$request/object/@id}" type="{$request/object/@type}">
            {
                $request/object/@effectiveDate[not(.='')],
                $request/object/@name[not(.='')]
            }
            </object>
        else()
    }
        <tracking effectiveDate="{$nowTracking}" statusCode="open">
        {
            if (string-length($request/tracking/@labels)>0) then ($request/tracking/@labels) else ()
        }
            <author id="{$issueAuthorId}">{$request/tracking/author/text()}</author>
        {
            for $desc in $request/tracking/desc
            return
                art:parseNode($desc)
        }
        </tracking>
    {
        if ($issueAssignedId) then (
            <assignment to="{$issueAssignedId}" name="{$issueAssignedName}" effectiveDate="{$nowAssignment}">
            {
                $request/tracking/@labels[not(.='')]
            }
                <author id="{$issueAuthorId}">{$request/tracking/author/text()}</author>
            {
                <desc language="{($request/tracking/desc[1])/@language}"/>
            }
            </assignment>
        ) else ()
    }
    </issue>

let $insert                 :=
    if ($decor[issues]) then
        update insert $newIssue into $decor/issues
    else (
        update insert <issues>{$newIssue}</issues> into $decor
    )

let $subsribe               :=
    for $authorUserName in $decor/project/author/@username
    let $userAutoSubscribes := aduser:userHasIssueAutoSubscription($authorUserName, $prefix, $newId, $newIssue/object/@type, $issueAuthorUserName[1], $issueAssignedUserName[1])
    return
        if ($userAutoSubscribes) then
            aduser:setUserIssueSubscription($authorUserName, $newId)
        else ()

return
    <response>{$newId}</response>