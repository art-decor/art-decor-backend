xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

declare namespace sm      = "http://exist-db.org/xquery/securitymanager";
declare namespace request = "http://exist-db.org/xquery/request";

let $updatedInfo   := request:get-data()/pass
(:let $updatedInfo   :=
    <pass name="testuser" currpwd="1112" newpwd="ttt" newpwd-confirm="ttt"/>:)

let $userName      := 
    if ($updatedInfo[string-length(@name)>0]) 
    then ($updatedInfo/@name/string())
    else ()
let $userCurrPass  :=
    if ($updatedInfo[string-length(@currpwd)>0]) 
    then ($updatedInfo/@currpwd/string())
    else ()
let $userNewPass   := 
    if ($updatedInfo[@newpwd=@newpwd-confirm and string-length(@newpwd)>0]) 
    then ($updatedInfo/@newpwd/string()) 
    else ()

let $newpwd        := 
    if (not(empty($userName) or empty($userCurrPass) or empty($userNewPass))) 
    then (
        (:if (xmldb:login('/db',$userName,$userCurrPass)) 
        then 
            let $d := sm:passwd($userName,$userNewPass)
            return true()
        else (false()):)
        let $d := sm:passwd($userName,$userNewPass)
        return true()
     )
     else (false())

return
<data-safe>{$newpwd}</data-safe>
