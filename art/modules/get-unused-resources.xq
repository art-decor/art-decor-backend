xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art     = "http://art-decor.org/ns/art" at "art-decor.xqm";
declare namespace xforms        = "http://www.w3.org/2002/xforms";
declare namespace xhtml         = "http://www.w3.org/1999/xhtml";
declare namespace xsl           = "http://www.w3.org/1999/XSL/Transform";

let $packageRoot        := if (request:exists()) then request:get-parameter('packageRoot','art') else 'art'
let $artXformResources  := art:getFormResources($packageRoot)
let $resources          := $artXformResources/resources[1]
let $forms              :=  collection(concat($get:root,$packageRoot,'/xforms'))/xhtml:html | 
                            collection(concat($get:root,$packageRoot,'/resources/stylesheets'))/xsl:stylesheet |
                            collection(concat($get:strArtData,'/resources'))//menu
                            
let $results        := 
    for $resource in $resources/*/name()
    let $fullPath       := concat('$resources/',$resource)
    let $matches        :=
            $forms//*[contains(@ref,$fullPath)] |
            $forms//*[contains(@value,$fullPath)] |
            $forms//xsl:attribute[contains(.,$fullPath)] |
            $forms//*[@label=$resource]
    return
        if (empty($matches)) then (
            <resource>{$resource}</resource>
        ) else ()
return
<unused unused="{count($results)}" total="{count($resources/*)}">
{
    $results
}
</unused>