xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
module namespace get                    = "http://art-decor.org/ns/art-decor-settings";

declare namespace repo                  = "http://exist-db.org/xquery/repo";
declare variable $get:root                  := repo:get-root();

(:~ String variable with everything under art :)
declare variable $get:strArt                := concat($get:root,'art');
(:~ String variable with location of art/resources :)
declare variable $get:strArtResources       := concat($get:root,'art/resources');
(:~ Collection variable with everything under art/resources :)
declare variable $get:colArtResources       := collection($get:strArtResources);

(:~ String variable with everything under art-data :)
declare variable $get:strArtData            := concat($get:root,'art-data');
(: Should not expose these user-info variables. Use the api-user-settings.xqm instead :)
(:~ String variable to the art user-info.xml contents :)
declare variable $get:strUserInfo           := $get:strArtData || '/user-info';

(:~ String variable with the ART-DECOR server config:)
declare variable $get:strServerInfo         := concat($get:strArtData,'/server-info.xml');
(:~ String variable with the default ART-DECOR server language. May be user overridden :)
declare variable $get:strArtLanguage        := 
    if (doc-available($get:strServerInfo) and doc($get:strServerInfo)/server-info/defaultLanguage) then
        doc($get:strServerInfo)/server-info/defaultLanguage/text()
    else ('en-US');

(:~ String variable to the art decor-locks.xml contents :)
declare variable $get:strDecorLocks         := concat($get:strArtData,'/decor-locks.xml');
(:~ Document variable with the art decor-locks.xml contents :)
declare variable $get:docDecorLocks         := doc($get:strDecorLocks);

(:~ String variable with the DECOR types :)
declare variable $get:strDecorTypes         := concat($get:strArtData,'/decor-xsd-types.xml');
(:~ String variable with the database location of decor collection :)
declare variable $get:strDecor              := concat($get:root,'decor');
(:~ String variable with the database location of decor/core collection :)
declare variable $get:strDecorCore          := concat($get:strDecor,'/core');
(:~ String variable with the database location of decor/cache collection :)
declare variable $get:strDecorCache         := concat($get:strDecor,'/cache');
(:~ String variable with the database location of decor/data collection :)
declare variable $get:strDecorData          := concat($get:strDecor,'/data');
(:~ String variable with the database location of decor/implementationguides collection :)
declare variable $get:strDecorDataIG        := concat($get:strDecor,'/implementationguides');
(:~ String variable with the database location of version collection :)
declare variable $get:strDecorVersion       := concat($get:strDecor,'/releases');
(:~ String variable with the database location of decor/scheduled-tasks collection. This is where 
    requests for long running tasks should be picked up by a scheduled process :)
declare variable $get:strDecorScheduledTasks  := concat($get:strDecor,'/scheduled-tasks');
(:~ String variable with the database location of decor/services collection :)
declare variable $get:strDecorServices      := concat($get:strDecor,'/services');
(:~ String variable with the database location of decor/develop collection :)
(:declare variable $get:strDecorDevelop       := concat($get:strDecor,'/develop');:)
(:~ String variable with the database location of decor/history collection :)
declare variable $get:strDecorHistory       := concat($get:strDecor,'/history');
(:~ String variable with the database location of decor/tmp collection :)
declare variable $get:strDecorTemp          := concat($get:strDecor,'/tmp');

(:~ Collection variable with the database location of version collection :)
declare variable $get:colDecorVersion       := collection($get:strDecorVersion);
(:~ Collection variable with everything under decor/cache :)
declare variable $get:colDecorCache         := collection($get:strDecorCache);
(:~ Collection variable with everything under decor/data :)
declare variable $get:colDecorData          := collection($get:strDecorData);
(:~ Collection variable with everything under decor/implementationguides :)
declare variable $get:colDecorDataIG        := collection($get:strDecorDataIG);
(:~ String variable with path to ISO Schematron transformations to XSL :)
declare variable $get:strUtilISOSCH2SVRL    := concat($get:strArtResources,'/iso-schematron');
(:~ Collection variable with everything under decor/core :)
declare variable $get:colDecorCore          := collection($get:strDecorCore);
(:~ Doc variable with DECOR.xsd contents:)
declare variable $get:docDecorSchema        := doc(concat($get:strDecorCore,'/DECOR.xsd'));
(:~ Doc variable with DECOR.xsd contents:)
declare variable $get:docDecorBasicSchema   := doc(concat($get:strDecorCore,'/DECOR-datatypes.xsd'));

(:~ String variable with everything under fhir :)
declare variable $get:strFhir               := concat($get:root,'fhir');

(:~ Collection variable with everything under hl7 :)
declare variable $get:strHl7                := concat($get:root,'hl7');
(:~ String variable to CDA stylesheet :)
declare variable $get:strCdaXsl             := concat($get:strHl7,'/CDAr2/xsl/CDA.xsl');

(:~ Collection variable with everything under lab :)
declare variable $get:strLab                := concat($get:root,'lab');
(:~ Collection variable with everything under lab-data :)
declare variable $get:strLabData            := concat($get:root,'lab-data/data');

(:~ Collection variable with everything under terminology :)
declare variable $get:strTerminology        := concat($get:root,'terminology');
(:~ Collection variable with everything under terminology-data :)
declare variable $get:strTerminologyData    := concat($get:root,'terminology-data');
(:~ Collection variable with everything under terminology-data/codesystem-stable-data :)
declare variable $get:strCodesystemStableData       := concat($get:root,'terminology-data/codesystem-stable-data');
(:~ Collection variable with everything under terminology-data/codesystem-authoring-data :)
declare variable $get:strCodesystemAuthoringData    := concat($get:root,'terminology-data/codesystem-authoring-data');
(:~ Collection variable with everything under terminology-data/codesystem-stable-data :)
declare variable $get:strValuesetStableData := concat($get:root,'terminology-data/valueset-stable-data');
(:~ Collection variable with everything under terminology-data/codesystem-authoring-data :)
declare variable $get:strValuesetAuthoringData      := concat($get:root,'terminology-data/valueset-authoring-data');
(:~ Collection variable with everything under terminology-data/codesystem-stable-data :)
declare variable $get:strConcepmapStableData        := concat($get:root,'terminology-data/conceptmap-stable-data');
(:~ Collection variable with everything under terminology-data/codesystem-authoring-data :)
declare variable $get:strConceptmapAuthoringData    := concat($get:root,'terminology-data/conceptmap-authoring-data');
(:~ Collection variable with everything under terminology-data/valueset-expansion-data :)
declare variable $get:strValuesetExpansionData      := concat($get:root,'terminology-data/valueset-expansion-data');

(:~ Collection variable with everything under dhd :)
declare variable $get:strDhd     := concat($get:root,'dhd');
(:~ Collection variable with everything under dhd-data :)
declare variable $get:strDhdData := concat($get:root,'dhd-data');

(:~ String variable with everything under xis :)
declare variable $get:strXis             := concat($get:root,'xis');
(:~ String variable with everything under xis resources :)
declare variable $get:strXisResources    := concat($get:root,'xis/resources');
(:~ String variable with the database location of xis-data collection :)
declare variable $get:strXisData         := concat($get:root,'xis-data');
(:~ String variable with everything under xis accounts :)
declare variable $get:strXisAccounts     := concat($get:strXisData,'/accounts');
(:~ String variable with the database location of xis-data/data collection :)
declare variable $get:strXisHelperConfig := concat($get:strXisData,'/data');
(:~ String variable with the database location of test accounts :)
declare variable $get:strTestAccounts    := concat($get:strXisData,'/test-accounts.xml');
(:~ String variable with the database location of test suites :)
declare variable $get:strTestSuites      := concat($get:strXisData,'/test-suites.xml');
(:~ String variable with the database location of test suites :)
declare variable $get:strSoapServiceList := concat($get:strXisData,'/soap-service-list.xml');

(:~ String variable with the database location of OIDS data collection :)
declare variable $get:strOidsData        := concat($get:root,'tools/oids-data');
(:~ Collection variable with everything under oids/data :)
declare variable $get:colOidsData        := collection(concat($get:root,'tools/oids-data'));
(:~ String variable with the database location of OIDS core collection :)
declare variable $get:strOidsCore        := concat($get:root,'tools/oids/core');
(:~ String variable with the database location of OIDS resources collection :)
declare variable $get:strOidsResources   := concat($get:root,'tools/oids/resources');
(:~ String variable for the name of the ISO schema :)
declare variable $get:strISO13582schema := 'iso-13582-2015.xsd';

(:~ String variable for the key to HL7 V2 Table 0396 mnemonics (https://terminology.hl7.org/CodeSystem-v2-0396.html) :)
declare variable $get:strKeyHL7v2Table0396CodePrefd := 'HL7-V2-Table-0396-Code';
(:~ String variable for the key to preferred FHIR URIs :)
declare variable $get:strKeyCanonicalUriPrefd       := 'HL7-FHIR-System-URI-Preferred';
(:~ String variable for the key to FHIR DSTU2 :)
declare variable $get:strKeyFHIRDSTU2               := 'DSTU2';
(:~ String variable for the key to preferred FHIR URIs in DSTU2 :)
declare variable $get:strKeyCanonicalUriPrefdDSTU2  := concat($get:strKeyCanonicalUriPrefd, '-', $get:strKeyFHIRDSTU2);
(:~ String variable for the key to FHIR STU3 :)
declare variable $get:strKeyFHIRSTU3                := 'STU3';
(:~ String variable for the key to preferred FHIR URIs in STU3 :)
declare variable $get:strKeyCanonicalUriPrefdSTU3   := concat($get:strKeyCanonicalUriPrefd, '-', $get:strKeyFHIRSTU3);
(:~ String variable for the key to FHIR R4 :)
declare variable $get:strKeyFHIRR4                  := 'R4';
(:~ String variable for the key to preferred FHIR URIs in R4 :)
declare variable $get:strKeyCanonicalUriPrefdR4     := concat($get:strKeyCanonicalUriPrefd, '-', $get:strKeyFHIRR4);
(:~ String variable for the key to FHIR R4B :)
declare variable $get:strKeyFHIRR4B                 := 'R4B';
(:~ String variable for the key to preferred FHIR URIs in R4B :)
declare variable $get:strKeyCanonicalUriPrefdR4B    := concat($get:strKeyCanonicalUriPrefd, '-', $get:strKeyFHIRR4B);
(:~ String variable for the key to FHIR R5 :)
declare variable $get:strKeyFHIRR5                  := 'R5';
(:~ String variable for the key to preferred FHIR URIs in R5 :)
declare variable $get:strKeyCanonicalUriPrefdR5     := concat($get:strKeyCanonicalUriPrefd, '-', $get:strKeyFHIRR5);
(:~ String variable for the key to FHIR URIs that exist, but are not preferred over other identifiers like OIDs or URIs :)
declare variable $get:strKeyCanonicalUri            := 'HL7-FHIR-System-URI';
(:~ Array variable containing all FHIR version strings :)
declare variable $get:arrKeyFHIRVersions            := ($get:strKeyFHIRDSTU2, $get:strKeyFHIRSTU3, $get:strKeyFHIRR4, $get:strKeyFHIRR4B, $get:strKeyFHIRR5);

(:~ Returns the property key for the preferred canonical uri for a system for a given FHIR version :)
declare function get:strKeyCanonicalUriFhirPrefd($fhirVersion as xs:string?) as xs:string? {
    if ($fhirVersion = $get:arrKeyFHIRVersions) then
        string-join(($get:strKeyCanonicalUriPrefd, $fhirVersion), '-')
    else
    if ($fhirVersion = '1.0') then $get:strKeyCanonicalUriPrefdDSTU2 else
    if ($fhirVersion = '3.0') then $get:strKeyCanonicalUriPrefdSTU3 else
    if ($fhirVersion = '4.0') then $get:strKeyCanonicalUriPrefdR4 else
    if ($fhirVersion = '4.3') then $get:strKeyCanonicalUriPrefdR4B else
    if ($fhirVersion = '5.0') then $get:strKeyCanonicalUriPrefdR5
    else
        $get:strKeyCanonicalUriPrefd
};

(: Collection for storing check-decor results, e.g.
    /db/apps/decor/releases/abr/development/
:)
declare function get:strProjectDevelopment($projectPrefix as xs:string) as xs:string {
    concat(
        $get:strDecorVersion, '/', 
        replace($projectPrefix, '-$', ''), 
        '/development/'
    )
};
(: Full path for storing check-decor results, e.g.
    /db/apps/decor/releases/abr/development/abr-decorcheck-report.xml
   or if $doconly is false()
    abr-decorcheck-report.xml
:)
declare function get:strProjectDevelopmentDoc($projectPrefix as xs:string, $doconly as xs:boolean) as xs:string {
    if ($doconly) then
        concat($projectPrefix, 'decorcheck-report.xml')
    else (
        concat(get:strProjectDevelopment($projectPrefix), $projectPrefix, 'decorcheck-report.xml')
    )
};

(:~ Get current user name. effective user name if available or real user name otherwise. The function xmldb:get-current-user() was removed 
:   in eXist-db 5.0 and replaced with sm:id(). By centralizing the new slightly more complicated way of doing this, you can avoid making 
:   mistakes throughout the code base
:   @return user name string.
:   @since 2019-11-11
:)
declare function get:strCurrentUserName() as xs:string? {
    let $current-user-function  := function-lookup(xs:QName('xmldb:get-current-user'), 0)
    let $sm-id-function         := function-lookup(xs:QName('sm:id'), 0)
    
    return
    if (exists($current-user-function)) then 
        $current-user-function()
    else 
    if (exists($sm-id-function)) then (
        let $smid   := $sm-id-function()
        
        return
            ($smid//sm:effective/sm:username, $smid//sm:real/sm:username)[1]
    ) 
    else 
        'guest'
};
