xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace vs  = "http://art-decor.org/ns/decor/valueset"     at "../api/api-decor-valueset.xqm";
declare namespace xs        = "http://www.w3.org/2001/XMLSchema";
declare namespace xforms    = "http://www.w3.org/2002/xforms";

let $projectPrefix          := if (request:exists()) then request:get-parameter('project',()) else ()
let $version                := if (request:exists()) then request:get-parameter('version',()) else ()
let $id                     := if (request:exists()) then request:get-parameter('id',()) else ()
let $name                   := if (request:exists()) then request:get-parameter('name',()) else ()
let $flexibility            := if (request:exists()) then request:get-parameter('flexibility',()) else ()
(:let $project              := 'peri20-':)
let $treetype               := if (request:exists()) then request:get-parameter('treetype',$vs:TREETYPELIMITEDMARKED) else ($vs:TREETYPEMARKED)
let $withversions           := if (request:exists()) then request:get-parameter('withversions','true')='true' else (true())
let $doV2                   := if (request:exists()) then request:get-parameter('v2','false')='true' else ()

let $valueSets              := 
    if ($doV2) then
        vs:getValueSetList-v2($id, $name, $flexibility, $projectPrefix, $version, $withversions, $treetype)
    else
        vs:getValueSetList($id, $name, $flexibility, $projectPrefix, $version)
let $decorProjectValuesets  := $valueSets/project

return
    if ($doV2) then
        $valueSets
    else
    if ($withversions) then
        <valueSetList>
        {
            for $valueSet in $decorProjectValuesets/valueSet
            let $id := $valueSet/@id | $valueSet/@ref
            group by $id
            return
            <valueSet>
            {
                $valueSet[1]/@id | $valueSet[1]/@ref
            }
            {
                for $v in $decorProjectValuesets/valueSet[(@id|@ref)=$id]
                order by $v/@effectiveDate descending
                return
                    $v
            }
            </valueSet>
        }
        </valueSetList>
    else
        <valueSetList>
        {
            let $valueSetList       :=
                for $valueSetsById in $decorProjectValuesets/valueSet
                let $id             := $valueSetsById/@id | $valueSetsById/@ref
                group by $id
                return
                    <valueSet uuid="{util:uuid()}">
                    {
                        let $latestVersion  := string(max($valueSetsById/xs:dateTime(@effectiveDate)))
                        let $latestValueSet := if ($latestVersion) then ($valueSetsById[@effectiveDate=$latestVersion][1]) else $valueSetsById[1]
                        
                        return
                        $latestValueSet/@*
                    }
                    </valueSet>
            
            for $valueSet in $valueSetList
            order by $valueSet/lower-case(@displayName)
            return
                $valueSet
        }
        </valueSetList>
