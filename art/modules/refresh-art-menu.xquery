xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get      = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace adserver = "http://art-decor.org/ns/art-decor-server" at "../api/api-server-settings.xqm";
declare namespace xsl            = "http://www.w3.org/1999/XSL/Transform";

let $xformStylesheet := adserver:getServerXSLArt()
let $xformStylesheet := if (string-length($xformStylesheet)=0) then 'apply-rules.xsl' else ($xformStylesheet)
   
let $stylesheet      := doc(concat($adserver:strServerXSLPath,'/',$xformStylesheet))

let $lastRefresh     := $stylesheet//xsl:param[@name='lastRefresh']

let $update          := update value $lastRefresh/@select with concat('''',current-dateTime(),'''')
return
<refresh dateTime="{$stylesheet//xsl:param[@name='lastRefresh']/@select}"/>
