xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

(:
   Save decor-parameters.xml (publication parameters) for specific project
:)
import module namespace get             = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art             = "http://art-decor.org/ns/art" at "art-decor.xqm";
import module namespace comp            = "http://art-decor.org/ns/art-decor-compile" at "../api/api-decor-compile.xqm";
import module namespace aduser          = "http://art-decor.org/ns/art-decor-users" at "../api/api-user-settings.xqm";
import module namespace adserver        = "http://art-decor.org/ns/art-decor-server" at "../api/api-server-settings.xqm";
import module namespace iss             = "http://art-decor.org/ns/decor/issue" at "../api/api-decor-issue.xqm";
import module namespace i18n            = "http://art-decor.org/ns/decor/i18n" at "../api/api-decor-i18n.xqm";


let $parameters         := if (request:exists()) then request:get-data()/decor-parameters else (
<decor-parameters xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="https://assets.art-decor.org/ADAR/rv/decor-parameters.xsd" initialdefault="false" project="demo1-">
    <switchCreateSchematron1/>
    <switchCreateSchematronWithWrapperIncludes0/>
    <switchCreateSchematronWithWarningsOnOpen0/>
    <switchCreateSchematronClosed0/>
    <switchCreateSchematronWithExplicitIncludes0/>
    <switchCreateDocHTML1/>
    <switchCreateDocSVG1/>
    <switchCreateDocDocbook0/>
    <switchCreateDocPDF0/>
    <useLocalAssets1/>
    <useLocalLogos1/>
    <useCustomLogo0/>
    <useLatestDecorVersion1/>
    <inDevelopment0/>
    <defaultLanguage>en-US</defaultLanguage>
    <switchCreateDatatypeChecks1/>
    <createDefaultInstancesForRepresentingTemplates1/>
    <artdecordeeplinkprefix>https://decor.nictiz.nl/art-decor/</artdecordeeplinkprefix>
    <bindingBehavior valueSets="freeze"/>
    <switchCreateTreeTableHtml1/>
    <filters filter="on" filterId="1">
        <filters id="1" label="TestLabel" created="2020-05-06T08:28:30" modified="2020-05-06T08:28:30">
            <transaction ref="2.16.840.1.113883.3.1937.99.62.3.4.1" flexibility="2012-09-05T16:59:35" statusCode="draft" name="Measurement"/>
        </filters>
    </filters>
    <documentation>
        <!-- ... -->
    </documentation>
    <transactions>
        <transaction id="2.16.840.1.113883.3.1937.99.62.3.4.1" effectiveDate="2012-09-05T16:59:35" statusCode="draft" type="group" filtered="true">
            <name language="en-US">Measurement</name>
            <name language="nl-NL">Meting</name>
            <desc language="en-US">A patient measures his or her own weight and length periodically and submits the results to a registry.</desc>
            <desc language="nl-NL">Een patiënt voert metingen uit en verzend deze periodiek naar een register.</desc>
        </transaction>
        <transaction id="2.16.840.1.113883.3.1937.99.62.3.4.4" effectiveDate="2012-09-05T16:59:35" statusCode="draft" type="group" filtered="false">
            <name language="en-US">Measurement with BSN</name>
            <name language="nl-NL">Meting op BSN</name>
            <desc language="en-US">A patient measures his or her own weight and length periodically and submits the results to a registry.</desc>
            <desc language="nl-NL">Een patiënt voert metingen uit en verzend deze periodiek naar een register.</desc>
        </transaction>
    </transactions>
</decor-parameters>
)
let $projectPrefix      := $parameters/@project[string-length() gt 0]
let $decor              := if (empty($projectPrefix)) then () else art:getDecorByPrefix($projectPrefix)
let $parent             := if ($decor) then util:collection-name($decor) else ()
let $parameterfile      := if ($parent) then doc(concat($parent, '/', 'decor-parameters.xml'))/decor-parameters else ()

(: translate to parameter file from $parameters :)
let $skeletonParameters   := 
    <decor-parameters xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="https://assets.art-decor.org/ADAR/rv/decor-parameters.xsd">
    {
        for $p in $parameters/(* except (documentation | filters | transactions))
        return
            switch ($p/name())
            case 'switchCreateDocPDF1'
                (: special handling for switchCreateDocPDF1: turn element content into @include attribute, omit spaces :)
                return <switchCreateDocPDF1 include="{if (string-length($p) = 0) then 'dsntri' else replace($p, ' ', '')}"/>
            case 'documentation' 
                (: ignore documentation borrowed from xsd :)
                return ()
            case 'filters' 
                (: ignore filters. they go into the filters.xml :)
                return ()
            default
                return $p
    }
    </decor-parameters>

let $doit4parameter        :=
    if (empty($parent)) then (
        (: if no parameter post is given, return an error :)
        error(QName('http://art-decor.org/ns/art-decor', 'MissingParameter'), concat('You are missing post parameter ''parameters'''))
    )
    else
    if (empty($parameterfile)) then (
        (: create new parameter file :)
        let $create         := xmldb:store($parent, 'decor-parameters.xml', $skeletonParameters)
        return ()
    ) 
    else (
        let $update         := update value $parameterfile with $skeletonParameters/*
        return ()
    )
let $doit4filters          := 
    if (empty($decor) or empty($parameters/filters)) then () else (
        comp:updateCompilationFilterSet($decor, $parameters/filters)
    )

return
    <data-safe>true</data-safe>