xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace art         = "http://art-decor.org/ns/art" at "art-decor.xqm";
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";

let $getdefault     := if (request:exists()) then request:get-parameter('getdefault','false') else ('false')
let $packageRoot    := if (request:exists()) then request:get-parameter('packageRoot','art') else 'art'

return if ($getdefault='true') then art:getLanguageResources($packageRoot,false()) else art:getFormResources($packageRoot)