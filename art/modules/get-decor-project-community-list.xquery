xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace art ="http://art-decor.org/ns/art" at "art-decor.xqm";
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";

let $prefix         := if (request:exists()) then request:get-parameter('prefix', ())[not(. = '')] else ('kz-')
let $projectId      := if (request:exists()) then request:get-parameter('id', ())[not(. = '')] else ()

let $decor          :=
    if (not(empty($prefix))) then 
        art:getDecorByPrefix($prefix) 
    else
    if (not(empty($projectId))) then 
        art:getDecorById($projectId) 
    else ()
let $decorProject   := $decor/project

return
<communities>
{
    for $community in $get:colDecorData//community[@projectId = $decorProject/@id]
    order by lower-case(($community/@displayName, $community/@name)[1])
    return
    <community>
    {
        $community/@*,
        for $desc in $community/desc
        return
        art:serializeNode($desc)
    }
    </community>
}
</communities>
