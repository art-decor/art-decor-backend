xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace adserver = "http://art-decor.org/ns/art-decor-server" at "../api/api-server-settings.xqm";

let $uri  := if (request:exists()) then (request:get-parameter('uri',())) else ('http://art-decor.org/decor/services/')
let $type := if (request:exists()) then (request:get-parameter('type',())) else ('')

return
    if ($uri) then (
        adserver:getRepositoriesFromServer($uri)
    )
    else if ($type='all') then (
        adserver:getServerAllRepositories()
    )
    else if ($type='internal') then (
        adserver:getServerInternalRepositories()
    )
    else if ($type='external') then (
        adserver:getServerExternalRepositories()
    )
    else ()