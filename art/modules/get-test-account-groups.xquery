xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

declare namespace xis       = "http://art-decor.org/ns/xis";
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";

let $testAccountGroups :=
    if (xmldb:collection-available($get:strXisResources)) then
        doc($get:strTestAccounts)//xis:testAccount/@name/string()
    else()
   
return
    <groups>
    {
        for $group at $i in $testAccountGroups
        order by lower-case($group)
        return
            <group navkey="{$i}">{$group}</group>
    }
    </groups>