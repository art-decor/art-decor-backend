xquery version "3.1";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art     = "http://art-decor.org/ns/art" at "art-decor.xqm";

declare option exist:serialize "indent=yes";
declare option exist:serialize "omit-xml-declaration=no";

let $projectPrefix          := if (request:exists()) then request:get-parameter('prefix','') else ('onco-generic-')

let $decor                  := art:getDecorByPrefix($projectPrefix)
let $checkReportColl        := get:strProjectDevelopment($projectPrefix)
let $checkReportName        := get:strProjectDevelopmentDoc($projectPrefix, true())
let $lastChecked            := 
    if (doc-available(get:strProjectDevelopmentDoc($projectPrefix, false()))) then 
        xmldb:last-modified(get:strProjectDevelopment($projectPrefix), $checkReportName) 
    else ()
let $lastModified           := if ($decor) then xmldb:last-modified(util:collection-name($decor), util:document-name($decor)) else current-dateTime()
let $checked                := $lastChecked > $lastModified

return
<dates>
{
    if ($decor) then 
        <decor lastChecked="{$lastChecked}" lastModified="{$lastModified}" checked="{$checked}"/>
    else ()
}
</dates>