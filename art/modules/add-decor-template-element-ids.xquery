xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get             = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art             = "http://art-decor.org/ns/art" at "art-decor.xqm";
import module namespace templ           = "http://art-decor.org/ns/decor/template" at "../api/api-decor-template.xqm";

let $projectPrefix          := if (request:exists()) then request:get-parameter('prefix',())[not(. = '')] else ()
let $templateId             := if (request:exists()) then request:get-parameter('id',())[not(. = '')] else ()
let $templateEffectiveDate  := if (request:exists()) then request:get-parameter('effectiveDate',())[not(. = '')] else ()

(::)
let $decorProjects          := art:getDecorByPrefix($projectPrefix)
let $update                 := 
    for $decor in $decorProjects
    return templ:addTemplateElementAndAttributeIds($decor, $templateId, $templateEffectiveDate)

return
    <data-safe>true</data-safe>