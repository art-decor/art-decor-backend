xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace art ="http://art-decor.org/ns/art" at "art-decor.xqm";
declare namespace request = "http://exist-db.org/xquery/request";

let $datasetId                  := if (request:exists()) then request:get-parameter('id',())[not(.='')]                       else ()
let $datasetEffectiveDate       := if (request:exists()) then request:get-parameter('effectiveDate',())[not(.='')]            else ()
let $transactionId              := if (request:exists()) then request:get-parameter('transactionId',())[not(.='')]            else ()
let $transactionEffectiveDate   := if (request:exists()) then request:get-parameter('transactionEffectiveDate',())[not(.='')] else ()
(:relevant only for transactions. If all you care about is the final tree without any absent concepts: set this parameter to false:)
let $fullTree                   := if (request:exists()) then request:get-parameter('fullTree','true')[not(.='')] else ()

return art:getDatasetTree($datasetId, $datasetEffectiveDate, $transactionId, $transactionEffectiveDate, $fullTree='true')