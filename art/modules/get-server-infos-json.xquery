xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace adserver = "http://art-decor.org/ns/art-decor-server" at "../api/api-server-settings.xqm";
import module namespace get      = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace json   = "http://www.json.org";
declare namespace expath = "http://expath.org/ns/pkg";

declare option output:method "json";
declare option output:media-type "application/json";
declare option exist:serialize "json-ignore-whitespace-text-nodes=yes";

let $projects := collection($get:strDecorData)//decor
let $cvs := count($projects/terminology/valueSet)
let $ccs := count($projects/terminology/codeSystem)
let $ccda := count($projects/rules/template)
let $cfhir := count($projects/rules/profile)
let $allPackages := collection($get:root)/expath:package

return
  <serverinfo>
      {
          <defaultLanguage>{adserver:getServerLanguage()}</defaultLanguage>,
          <statistics>
              <projects
                count="{count($projects/project)}"/>
              <datasets
                  count="{count($projects/datasets/dataset)}"
                  concepts="{count($projects/datasets/dataset//concept)}"/>
              <scenarios
                  count="{count($projects/scenarios/scenario)}"
                  transactions="{count($projects/scenarios//transaction)}"/>
              <terminologies
                  count="{$cvs + $ccs}"
                  valueSets="{$cvs}"
                  codeSystems="{$ccs}"/>
              <rules
                  count="{$ccda + $cfhir}"
                  template="{$ccda}"
                  profile="{$cfhir}"/>
              <issues
                  count="{count($projects/issues/issue)}"
                  open="{count($projects/issues/issue[@statusCode = ('open')])}"/>
              <releases
                  count="{count($projects/project/release)}"/>
          </statistics>,
          <packages>
          {
                for $package in $allPackages
                let $parentCollection := substring-after(util:collection-name($package),$get:root)
                order by lower-case($parentCollection)
                return
                <package title="{$package//expath:title}" collection="{$parentCollection}">
                {
                    $package/@version | $package/@abbrev
                }
                </package>
          }
          </packages>,
          <database version="{system:get-version()}"/>
      }
  </serverinfo>
