xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at "art-decor.xqm";
import module namespace templ       = "http://art-decor.org/ns/decor/template" at "../api/api-decor-template.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../api/api-server-settings.xqm";

declare namespace http              = "http://expath.org/ns/http-client";

declare %private function local:getProjectName($prefix as xs:string, $url as xs:string?, $language as xs:string) as xs:string? {
    if (string-length($url)=0 or $url = adserver:getServerURLServices()) then (
        let $projectNames   := $get:colDecorData/decor/project[@prefix=$prefix]/name
        
        return if ($projectNames[@language=$language]) then $projectNames[@language=$language] else ($projectNames/name[1])
    ) else (
        let $service-uri    := xs:anyURI(concat($url, 'ProjectIndex?format=xml&amp;language=',$language))
        let $requestHeaders := 
            <http:request method="GET" href="{$service-uri}">
                <http:header name="Content-Type" value="text/xml"/>
                <http:header name="Cache-Control" value="no-cache"/>
                <http:header name="Max-Forwards" value="1"/>
            </http:request>
        
        let $projectNames   := $get:colDecorCache/decor/project[@prefix=$prefix]/@name
        
        return
            if ($projectNames[@language=$language]) 
            then $projectNames[@language=$language] 
            else if ($projectNames)
            then ($projectNames/name[1])
            else (
                let $server-response := http:send-request($requestHeaders)
                return
                    $server-response[2]//project[@prefix=$prefix]/@name
            )
    )
};

let $id                         := if (request:exists()) then request:get-parameter('id',()) else ('2.16.840.1.113883.2.4.3.11.60.40.1.12.5.9')
let $effectiveDate              := if (request:exists()) then request:get-parameter('effectiveDate',()) else ('2015-04-01T00:00:00')

let $dataset                    := art:getDataset($id, $effectiveDate)
let $effectiveDate              := if (string-length($effectiveDate)=0) then $dataset/@effectiveDate else $effectiveDate

let $language                   := if (request:exists()) then request:get-parameter('language',()) else ('nl-NL')
let $language                   := if (string-length($language)>0) then $language else ($dataset/ancestor::decor/project/@defaultLanguage)[1]

let $transactions               :=
    for $transaction in art:getTransactionsByDataset($id, $effectiveDate)
    return
        <transaction>
        {
            $transaction/@id,
            $transaction/@effectiveDate,
            $transaction/@statusCode,
            $transaction/@versionLabel,
            $transaction/@canonicalUri,
            $transaction/@lastModifiedDate,
            attribute prefix {$transaction/ancestor::decor/project/@prefix},
            attribute projectName {local:getProjectName($transaction/ancestor::decor/project/@prefix, (), $language)},
            attribute datasetId {$id},
            attribute datasetEffectiveDate {$effectiveDate},
            $transaction/name, (:transaction name:)
            for $datasetName in $dataset/name
            return
                <datasetName>
                {
                    $datasetName/@*,
                    $datasetName/../@versionLabel,
                    $datasetName/text() 
                }
                </datasetName>
        }
        </transaction>

return
<usage id="{$id}" effectiveDate="{$effectiveDate}">
{
    $transactions
}
</usage>


