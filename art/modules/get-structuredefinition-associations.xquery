xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art     = "http://art-decor.org/ns/art" at "art-decor.xqm";

let $projectPrefix          := if (request:exists()) then request:get-parameter('project',()) else ('demo1-')
let $projectVersion         := if (request:exists()) then request:get-parameter('version',()) else ()
let $publicationUrl         := if (request:exists()) then request:get-parameter('publicationurl',()) else ()

let $decor                  := art:getDecorByPrefix($projectPrefix)

return
    <return>
    {
        (:<structuredefinitionAssociation baseref="1" ref="Nictiz-bgz-AllergyIntolerance" displayName="AllergyIntolerance"/>:)
        (:for $reference in $decor/rules/structuredefinitionAssociation
        let $url    := concat($restURIs[@id=$reference/@baseref][1],'StructureDefinition/',$reference/@ref)
        return
            element {$reference/name()} {
                $reference/@*,
                attribute url {$url}
            }:)
        for $sd in $decor/rules/structuredefinition[@publicationurl=$publicationUrl]
        return
            <structuredefinition>
            {
                $sd/@*,
                (:<concept ref="2.16.840.1.113883.3.1937.99.62.3.2.1" effectiveDate="2012-05-30T11:32:36" elementId="2.16.840.1.113883.3.1937.99.62.3.11.12.1"/>:)
                for $concept in $sd/concept
                let $conceptId      := $concept/@ref
                let $conceptEd      := $concept/@effectiveDate
                let $elementId      := $concept/@elementId
                let $elementPath    := $concept/@elementPath
                let $theConcept     := art:getConcept($conceptId, $conceptEd)
                let $theOriginalConcept := art:getOriginalForConcept($theConcept)
                let $conceptEd      := $theConcept/@effectiveDate
                let $theDataset     := $theConcept/ancestor::*:dataset
                let $datasetId      := $theDataset/@id
                let $datasetEd      := $theDataset/@effectiveDate
                return
                    <concept>
                    {
                        $conceptId, $conceptEd, $elementId, $elementPath,
                        (:attribute refdisplay {art:getNameForOID($elementId, (), ())},:)
                        attribute datasetId {$datasetId},
                        attribute datasetEffectiveDate {$datasetEd},
                        attribute prefix {$theConcept/ancestor::decor/project/@prefix},
                        <path>
                        {
                            for $ancestor in $theConcept/ancestor::concept
                            return
                                concat(art:getOriginalForConcept($ancestor)/name[1]/text(), ' / ')
                        }
                        </path>,
                        $theOriginalConcept/name
                    }
                    </concept>
            }
            </structuredefinition>
    }
    </return>