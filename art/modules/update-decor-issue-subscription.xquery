xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace aduser  = "http://art-decor.org/ns/art-decor-users" at "../api/api-user-settings.xqm";
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";

let $issueId    := if (request:exists()) then request:get-parameter('id',()) else ()
let $action     := if (request:exists()) then request:get-parameter('action',()) else ()

let $update     :=
    if ($action='add') then
        if (exists($get:colDecorData//issue[@id=$issueId])) then
            aduser:setUserIssueSubscription($issueId)
        else (
            error(QName('http://art-decor.org/ns/error', 'UnsupportedParameters'), concat('Could not find issue with id ''',$issueId,'''.'))
        )
    else if ($action='delete') then
        aduser:unsetUserIssueSubscription($issueId)
    else (
        error(QName('http://art-decor.org/ns/error', 'UnsupportedParameters'), concat('Unsupported value for parameter action ''',$action,'''. Supported actions are ''add'' and ''delete'''))
    )

return <result q="{request:get-query-string()}">{not($update=false())}</result>