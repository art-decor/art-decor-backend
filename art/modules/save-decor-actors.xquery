xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art ="http://art-decor.org/ns/art" at "art-decor.xqm";

let $newActors          := request:get-data()/actors
let $projectPrefix      := request:get-parameter('project','')

(:let $newActors :=
   <actors projectPrefix="tfw-">
      <actor id="2.16.840.1.113883.2.4.3.46.99.3.5.1" type="person">
         <name language="nl-NL">Patiënt</name>
         <name language="en-US">Patient</name>
      </actor>
      <actor id="2.16.840.1.113883.2.4.3.46.99.3.5.2" type="organization">
         <name language="en-US">Weight Registry</name>
         <name language="nl-NL">Gewicht register</name>
      </actor>
   </actors>:)


let $decor              := $get:colDecorData//decor[project/@prefix=$projectPrefix]

let $actorsAvailable    :=
    if (not($decor/scenarios)) then 
        update insert <scenarios><actors/></scenarios> preceding $decor/ids 
    else if (not($decor/scenarios/actors)) then
        update insert <actors/> preceding $decor/scenarios/node()
    else()
   
let $actorsUpdate       :=
    <actors>
    {
        for $actor in $newActors/actor
        return
        <actor>
        {
            $actor/@*
            ,
            for $name in $actor/name
            return
            art:parseNode($name)
            ,
            for $desc in $actor/desc
            return
            art:parseNode($desc)
        }
        </actor>
    }
    </actors>

let $update             := update replace $decor/scenarios/actors with $actorsUpdate

return ()
