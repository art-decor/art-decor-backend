xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace vs  = "http://art-decor.org/ns/decor/valueset" at "../api/api-decor-valueset.xqm";
import module namespace art = "http://art-decor.org/ns/art" at "art-decor.xqm";
declare namespace xs        = "http://www.w3.org/2001/XMLSchema";
declare namespace xforms    = "http://www.w3.org/2002/xforms";

let $id               := if (request:exists()) then request:get-parameter('id',())[string-length()>0][1] else ()
let $name             := if (request:exists()) then request:get-parameter('name',())[string-length()>0][1] else ()
let $ref              := if (request:exists()) then request:get-parameter('ref',())[string-length()>0][1] else ()
let $useRegexMatching := if (request:exists()) then request:get-parameter('regex',false())[string-length()>0][1] else (false())

let $effectiveDate    := if (request:exists()) then request:get-parameter('effectiveDate',())[string-length()>0][1] else ()
let $projectPrefix    := if (request:exists()) then request:get-parameter('project',())[string-length()>0][1] else ('demo1-')
let $projectVersion   := if (request:exists()) then request:get-parameter('version',())[string-length()>0][1] else ()
let $projectLanguage  := if (request:exists()) then request:get-parameter('language',())[string-length()>0][1] else ()
let $serialize        := if (request:exists()) then not(request:get-parameter('serialize', 'true')[string-length()>0][1] = 'false') else true()

let $valueSets := 
    if (not(empty($id))) then
        if (empty($projectPrefix)) then
            vs:getExpandedValueSetById($id, $effectiveDate, $serialize)
        else (
            vs:getExpandedValueSetById($id, $effectiveDate, $projectPrefix, $projectVersion, $projectLanguage, $serialize)
        )
    
    else if (not(empty($name))) then
        if (empty($projectPrefix)) then
            vs:getExpandedValueSetByName($name, $effectiveDate,$useRegexMatching, $serialize)
        else (
            vs:getExpandedValueSetByName($name, $effectiveDate, $useRegexMatching, $projectPrefix, $projectVersion, $projectLanguage, $serialize)
        )
    
    else if (not(empty($ref)) and not(empty($projectPrefix))) then
        vs:getExpandedValueSetByRef($ref, $effectiveDate, $projectPrefix, $projectVersion, $projectLanguage, $serialize)
    
    else ()

return
<valueSetVersions>
{
    for $valueSet in $valueSets/descendant-or-self::valueSet
    order by xs:dateTime($valueSet/@effectiveDate) descending
    return
        <valueSet>
        {
            $valueSet/@*,
            if (not($valueSet/@url)) then $valueSet/parent::*/@url else (),
            if (not($valueSet/@ident)) then $valueSet/parent::*/@ident else (),
            $valueSet/node()
        }
        </valueSet>
}
</valueSetVersions>