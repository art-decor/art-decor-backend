xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace cs  = "http://art-decor.org/ns/decor/codesystem"     at "../api/api-decor-codesystem.xqm";
declare namespace xs        = "http://www.w3.org/2001/XMLSchema";
declare namespace xforms    = "http://www.w3.org/2002/xforms";

let $projectPrefix          := if (request:exists()) then request:get-parameter('project',()) else ()
let $version                := if (request:exists()) then request:get-parameter('version',()) else ()
let $id                     := if (request:exists()) then request:get-parameter('id',()) else ()
let $name                   := if (request:exists()) then request:get-parameter('name',()) else ()
let $flexibility            := if (request:exists()) then request:get-parameter('flexibility',()) else ()
(:let $project              := 'peri20-':)
let $withversions           := if (request:exists()) then request:get-parameter('withversions','true')='true' else (true())
let $doV2                   := if (request:exists()) then request:get-parameter('v2','false')='true' else ()

let $codeSystems              := 
    if ($doV2) then
        cs:getCodeSystemList-v2($id, $name, $flexibility, $projectPrefix, $version, $withversions)
    else
        cs:getCodeSystemList($id, $name, $flexibility, $projectPrefix, $version)
let $decorProjectCodesystems  := $codeSystems/project

return
    if ($doV2) then
        $codeSystems
    else
    if ($withversions) then
        <codeSystemList>
        {
            for $codeSystem in $decorProjectCodesystems/codeSystem
            let $id := $codeSystem/@id | $codeSystem/@ref
            group by $id
            return
            <codeSystem>
            {
                $codeSystem[1]/@id | $codeSystem[1]/@ref
            }
            {
                for $v in $decorProjectCodesystems/codeSystem[(@id|@ref)=$id]
                order by $v/@effectiveDate descending
                return
                    $v
            }
            </codeSystem>
        }
        </codeSystemList>
    else
        <codeSystemList>
        {
            let $codeSystemList       :=
                for $codeSystemsById in $decorProjectCodesystems/codeSystem
                let $id             := $codeSystemsById/@id | $codeSystemsById/@ref
                group by $id
                return
                    <codeSystem uuid="{util:uuid()}">
                    {
                        let $latestVersion  := string(max($codeSystemsById/xs:dateTime(@effectiveDate)))
                        let $latestCodeSystem := if ($latestVersion) then ($codeSystemsById[@effectiveDate=$latestVersion][1]) else $codeSystemsById[1]
                        
                        return
                        $latestCodeSystem/@*
                    }
                    </codeSystem>
            
            for $codeSystem in $codeSystemList
            order by $codeSystem/lower-case(@displayName)
            return
                $codeSystem
        }
        </codeSystemList>
