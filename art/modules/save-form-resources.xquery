xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace art             = "http://art-decor.org/ns/art" at "art-decor.xqm";
declare namespace request               = "http://exist-db.org/xquery/request";

(: Add resources to form-resources, package root is in requestData/@packageRoot:)
let $requestData            := if (request:exists()) then request:get-data()/artXformResources else ()
let $packageRoot            := if (request:exists()) then request:get-parameter('packageRoot',()) else ()
(:  Include updating the ART menu if applicable :)
let $doArtMenuUpdate        := true()

let $update                 := 
    if (empty($packageRoot)) then ()
    else
        art:saveFormResources($packageRoot, $requestData, $doArtMenuUpdate)

return
    <response status="saved"/>
