xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art         = "http://art-decor.org/ns/art" at "art-decor.xqm";
import module namespace decor       = "http://art-decor.org/ns/decor" at "../api/api-decor.xqm";

(:let $transaction    := request:get-data()/transaction:)
let $editedRepTemp              := if (request:exists()) then request:get-data()/representingTemplate else ()
let $transactionId              := 
    if ($editedRepTemp) then ($editedRepTemp/@id) else if (request:exists()) then request:get-parameter('id',())[string-length()>0] else ()
let $transactionEffectiveDate   := 
    if ($editedRepTemp) then ($editedRepTemp/@effectiveDate) else if (request:exists()) then request:get-parameter('effectiveDate',())[string-length()>0] else ()

(:assume deletion if empty:)
let $templateId                 := 
    if ($editedRepTemp) then ($editedRepTemp/@templateId) else if (request:exists()) then request:get-parameter('templateId',())[string-length()>0] else ()
let $templateEffectiveDate      := 
    if ($editedRepTemp) then ($editedRepTemp/@templateEffectiveDate) else if (request:exists()) then request:get-parameter('templateEffectiveDate',())[string-length()>0] else ()

let $savedTransaction           := art:getTransaction($transactionId,$transactionEffectiveDate)

let $userCanEdit        :=
    if ($savedTransaction) then
        decor:authorCanEditP($savedTransaction/ancestor::decor, $decor:SECTION-SCENARIOS)
    else (false())

let $response :=
    (:cannot update a transaction that does not exist:)
    if ($savedTransaction) then (
        (:check if user id author in project:)
        if ($userCanEdit) then (
            let $update         := update delete $savedTransaction/representingTemplate/@ref
            let $update         := update delete $savedTransaction/representingTemplate/@flexibility
            let $update         := 
                if ($savedTransaction[representingTemplate]) then () else (
                    update insert <representingTemplate/> into $savedTransaction
                )
            
            let $update         := 
                if (empty($templateId)) then () else (
                    update insert attribute ref {$templateId} into $savedTransaction/representingTemplate,
                    if (empty($templateEffectiveDate)) then () else (
                        update insert attribute flexibility {$templateEffectiveDate} into $savedTransaction/representingTemplate
                    )
                )
            
            return <data-safe>true</data-safe>
        )
        else (
            <data-safe error="{concat('You don''t have permission to save this transaction/representingTemplate. user="',get:strCurrentUserName(),'"')}">false</data-safe>
        )
    )
    else (
        <data-safe error="{concat('It''s not possible to save this data as the transaction with id="',$transactionId,'" and effectiveDate="',$transactionEffectiveDate,'" cannot be found.')}">false</data-safe>
    )

return
    $response
