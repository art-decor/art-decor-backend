xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

(: scans the database for artXformResources and returns a list of package roots with package title and abbreviation:)
import module namespace art         = "http://art-decor.org/ns/art" at "art-decor.xqm";

art:getPackageList()