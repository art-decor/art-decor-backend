xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get             = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art             = "http://art-decor.org/ns/art" at "art-decor.xqm";
import module namespace decor           = "http://art-decor.org/ns/decor" at "../api/api-decor.xqm";

let $projectId          := if (request:exists()) then request:get-parameter('projectId', ())[string-length() gt 0] else ('2.16.840.1.113883.2.4.3.11.60.90')
let $communityName      := if (request:exists()) then request:get-parameter('name', ())[string-length() gt 0] else ('prn')
let $format             := if (request:exists()) then request:get-parameter('format','xforms') else ('xml')
let $user               := get:strCurrentUserName()
(:let $community          := $get:colDecorData//community[@projectId=$projectId][@name=$communityName][access/author/@username=$user]:)

let $community          := 
    if ($projectId[string-length() gt 0] and $communityName[string-length() gt 0]) then 
        decor:getDecorCommunity($communityName, $projectId, true()) 
    else
        error(QName('http://art-decor.org/ns/error', 'InsufficientParameters'), 'Parameters projectId and name (community name) are required')

let $community          :=
    if (count($community) = 1) then 
        <community>
        {
            $community/@*,
            if ($format = 'xforms') then 
                for $desc in $community/desc
                return
                    art:serializeNode($desc)
            else (
                $community/desc
            )
            ,
            $community/access
        }
        {
        if ($community/prototype/@ref) then 
            <prototype>
            {
                let $external := doc(xs:anyURI($community/prototype/@ref))/prototype
                return (
                    $community/prototype/@ref,
                    $external/node()
                )
            }
            </prototype>
        else
            <prototype>
            {
                $community/prototype/@ref,
                for $data in $community/prototype/data
                return
                    <data>
                    {
                        $data/@*,
                        if ($format = 'xforms') then 
                            for $desc in $data/desc
                            return
                                art:serializeNode($desc)
                        else (
                            $data/desc
                        )
                        ,
                        $data/enumValue
                    }
                    </data>
            }
            </prototype>
        }
        {
            <associations>
            {
                for $association in $community/associations/association
                return
                    <association>
                    {
                        $association/object,
                        if ($format = 'xforms') then 
                            for $desc in $association/data
                            return
                                art:serializeNode($desc)
                        else (
                            $association/data
                        )
                    }
                    </association>
            }
            </associations>
        }
        </community>
    else ()
    
return (
    if (response:exists()) then
        if ($community) then () else response:set-status-code(404)
    else (),
    $community
)