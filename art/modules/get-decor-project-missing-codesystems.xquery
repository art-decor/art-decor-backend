xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art     = "http://art-decor.org/ns/art" at "art-decor.xqm";

declare namespace xs    = "http://www.w3.org/2001/XMLSchema";

let $project            := if (request:exists()) then (request:get-parameter('project',())) else ()
let $decor              := $get:colDecorData//decor[project/@prefix=$project]
let $codeSystems        := $decor/terminology/codeSystem
let $language           := if (request:exists()) then (request:get-parameter('language',$decor/project/@defaultLanguage)) else ($decor/project/@defaultLanguage)

let $codeSystems        := $decor/terminology/codeSystem
let $usedCodeSystems    := distinct-values($decor//terminologyAssociation/@codeSystem | $decor//valueSet//@codeSystem | $decor//vocabulary/@codeSystem)

return
    <missingCodeSystems projectPrefix="{$decor/project/@prefix}">
    {
        for $usedId in $usedCodeSystems[not(.=($codeSystems/@ref|$codeSystems/@id))]
        let $oidName := art:getNameForOID($usedId, $language, $decor)
        order by $usedId
        return
            <codeSystem ref="{$usedId}" name="{$oidName}" displayName="{$oidName}"/>
    }
    </missingCodeSystems>
