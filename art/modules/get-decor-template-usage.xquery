xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art     = "http://art-decor.org/ns/art" at "art-decor.xqm";
import module namespace templ   = "http://art-decor.org/ns/decor/template" at "../api/api-decor-template.xqm";

let $prefix         := request:get-parameter('project',())[string-length()>0]
let $ref            := request:get-parameter('ref',())[string-length()>0]
let $effectiveDate  := request:get-parameter('effectiveDate',())[string-length()>0]
let $result         :=
    <result>
    {
        if ($prefix and $ref) then
            templ:getDependenciesAndUsage($ref, $effectiveDate, $prefix)
        else ()
    }
    </result>

return
<usage ref="{$ref}" effectiveDate="{$effectiveDate}" prefix="{$prefix}" refcnt="{count($result/ref)}" usescnt="{count($result/uses)}">
{
    $result/*
}
</usage>


