xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art     = "http://art-decor.org/ns/art" at "art-decor.xqm";
import module namespace decor   = "http://art-decor.org/ns/decor" at "../api/api-decor.xqm";
import module namespace aduser  = "http://art-decor.org/ns/art-decor-users" at "../api/api-user-settings.xqm";

declare variable $art-languages  := art:getArtLanguages();
(: issue mailing :)
(:
import module namespace im ="http://art-decor.org/ns/artxtra" at "/mailsignal-issues.xqm";
:)

declare %private function local:projectDetails($project as element(project), $user as xs:string) as element(project) {
    let $projectId      := $project/@id
    let $projectPrefix  := $project/@prefix
    let $projectLang    := $project/@defaultLanguage
    let $authorEmail    := $project/author[@username = $user]/@email
    let $lastVersion    := max($project/(release|version)/xs:dateTime(@date))

    return
    <project prefix="{$projectPrefix}" defaultLanguage="{$projectLang}" authoremail="{$authorEmail}" lastVersion="{$lastVersion}">
    {
        attribute experimental {$project/@experimental = 'true'},
        attribute private {$project/ancestor::decor/@private = 'true'},
        attribute repository {$project/ancestor::decor/@repository = 'true'}
    }
    {
        art:getLanguageNodes($project/name, $project/name/@language),
  
        (: --------- community collections --------- :)
        for $c in decor:getDecorCommunity((), $project/@id, true())
        return
            <community>
            {
                $c/@*,
                for $data in $c/desc
                return
                    art:serializeNode($data),
                $c/access
            }
            </community>
    }
    </project>
};

let $user            := if (request:exists()) then (request:get-parameter('user',())[1]) else ()
let $user            := 
    if (string-length($user)>0) then 
        $user 
    else if (string-length(get:strCurrentUserName())>0) then 
        get:strCurrentUserName() 
    else (
        'guest'
    )
let $comefrom        := if (request:exists()) then (request:get-parameter('comefrom',())) else ()

let $now             := current-dateTime()
let $userLastlogin   := if ($user = 'guest') then () else aduser:getUserLastLoginTime($user)
let $userDisplayName := if ($user = 'guest') then () else aduser:getUserDisplayName($user)

return
<report>
{
(: --------- user id test --------- :)
    <userid>{$user}</userid>,
    <displayName>{if (string-length($userDisplayName)=0) then $user else $userDisplayName}</displayName>
}
{
    <userupdate comefrom="{$comefrom}">
    {
        (: we come from login, add last login record :)
        if ($comefrom='login' and $user != 'guest') then (
            aduser:setUserLastLoginTime($now)
        ) else ()
    }
    </userupdate>
}
{
    (: --------- last login --------- :)
    <lastlogin>{$userLastlogin}</lastlogin>
}
{
    if ($user='guest') then () else (
        (:list of decor projects where user is author:)
        let $decors     := $get:colDecorData/decor[project/author/@username = $user]
        let $projects   :=
            for $m in $decors/project
            return local:projectDetails($m, $user)
        
        return (
            (:normal projects:)
            for $m in $projects[not(@experimental='true')][not(@repository='true')]
            let $lang   := $m/@defaultLanguage
            let $n      := if ($m/name[@language=$lang]) then $m/name[@language=$lang] else $m/name[1] 
            order by lower-case($n)
            return $m
            ,
            (:experimental projects:)
            for $m in $projects[@experimental='true'][not(@repository='true')]
            let $lang   := $m/@defaultLanguage
            let $n      := if ($m/name[@language=$lang]) then $m/name[@language=$lang] else $m/name[1] 
            order by lower-case($n)
            return $m
            ,
            (:repositories:)
            for $m in $projects[not(@experimental='true')][@repository='true']
            let $lang   := $m/@defaultLanguage
            let $n      := if ($m/name[@language=$lang]) then $m/name[@language=$lang] else $m/name[1] 
            order by lower-case($n)
            return $m
        )
    )
}
</report>