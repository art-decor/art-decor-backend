xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get    = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace aduser = "http://art-decor.org/ns/art-decor-users" at "../api/api-user-settings.xqm";

let $propDescription    := xs:anyURI('http://exist-db.org/security/description')
let $userName           := if (request:exists()) then request:get-parameter('user',())[string-length() gt 0] else ()
let $userName           := if (empty($userName)) then get:strCurrentUserName() else ($userName)

return
try {
    if (sm:is-authenticated() and sm:user-exists($userName) and not($userName='SYSTEM')) then (
        (: Return user details for all users except SYSTEM :)
        let $userIsActive         := sm:is-account-enabled($userName)
        
        let $userDisplayName      := aduser:getUserDisplayName($userName)
        let $userEmail            := aduser:getUserEmail($userName)
        let $userOrganization     := aduser:getUserOrganization($userName)
        let $userLanguage         := aduser:getUserLanguage($userName, false())
        let $userCreationDate     := aduser:getUserCreationDate($userName)
        let $userLastLogin        := aduser:getUserLastLoginTime($userName)
        let $userLastIssueNotify  := aduser:getUserLastIssueNotify($userName)
        
        return
        <user name="{$userName}" active="{$userIsActive}" pass="" newpwd="" newpwd-confirm="">
            {
                if (sm:is-dba(get:strCurrentUserName()) or $userName = get:strCurrentUserName()) then (
                    <!-- account-info -->,
                    <groups>{for $group in sm:get-user-groups($userName) return <group>{$group}</group>}</groups>,
                    <primarygroup>{sm:get-user-primary-group($userName)}</primarygroup>,
                    <description>{sm:get-account-metadata($userName,$propDescription)}</description>
                ) else ()
            }
            <!-- user-info -->
            <defaultLanguage>{$userLanguage}</defaultLanguage>
            <displayName>{$userDisplayName}</displayName>
            <email>{$userEmail}</email>
            <organization>{$userOrganization}</organization>
            <creationdate>{$userCreationDate}</creationdate>
            <lastlogin>{$userLastLogin}</lastlogin>
            <lastissuenotify>{$userLastIssueNotify}</lastissuenotify>
            <projects>
            {
                for $project in $get:colDecorData//decor/project[author/@username=$userName]
                let $projectName     := 
                    if (exists($project/name[@language=$userLanguage])) then (
                        $project/name[@language=$userLanguage][1]
                    ) else (
                        $project/name[@language=$project/@defaultLanguage][1]
                    )
                let $authorEmail     := $project/author[@username=$userName]/@email/string()
                let $authorNotifier  := $project/author[@username=$userName]/@notifier/string()
                order by lower-case($projectName)
                return
                    <project prefix="{$project/@prefix/string()}" name="{$projectName}" email="{$authorEmail}" notifier="{$authorNotifier}"/>
            }
            </projects>
        </user>
    ) else (
        <user name="{$userName}" active="false" pass=""/>
    )
}
catch * {()}