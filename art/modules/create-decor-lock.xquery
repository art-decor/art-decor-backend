xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

(:
   Query for retrieving decor locks
:)
import module namespace get             = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace decor           = "http://art-decor.org/ns/decor" at "../api/api-decor.xqm";

let $id                     := if (request:exists()) then request:get-parameter('id',(),true())[string-length()>0] else ()
let $effectiveDate          := if (request:exists()) then request:get-parameter('effectiveDate',())[string-length()>0] else ()
let $breakLock              := if (request:exists()) then request:get-parameter('breakLock',())[string-length()>0] else ()

let $username               := get:strCurrentUserName()

return
    decor:setLock($id, $effectiveDate, $breakLock)
