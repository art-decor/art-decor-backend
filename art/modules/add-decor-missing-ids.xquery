xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";

declare namespace xs        = "http://www.w3.org/2001/XMLSchema";
declare namespace xforms    = "http://www.w3.org/2002/xforms";

let $missingIds     := request:get-data()/missingIds
let $decor          := $get:colDecorData//decor[project/@prefix=$missingIds/@projectPrefix]

let $insert :=
    for $id in $missingIds/id
    return
    update insert $id following $decor/ids/defaultBaseId[last()]

return
<missingIds projectPrefix="{$decor/project/@prefix}"/>
