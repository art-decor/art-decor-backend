xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

(:
   Query for retrieving decor locks for specific project
:)
import module namespace get             = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";

let $projectPrefix  := if (request:exists()) then request:get-parameter('prefix', ()) else ()

let $allLocks       := $get:docDecorLocks//lock[@prefix=$projectPrefix]

let $locks :=  
    for $locks in $get:docDecorLocks//lock[@prefix=$projectPrefix]
    let $user := $locks/@user
    group by $user
    return
    <userLocks user="{$user}" userName="{$locks[1]/@userName}">
    {
        for $lock in $locks
        order by $lock/@type
        return $lock
    }
    </userLocks>

return
    <locks>
    {$locks}
    </locks>
