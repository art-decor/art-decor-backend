xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
let $decorProjects := $get:colDecorData/decor

return
<projects>
{
    for $decor in $decorProjects
    order by lower-case($decor/project/name[1])
    return
    <project>
    {
        $decor/project/(@prefix|@id),
        $decor/project/name
    }
    </project>
}
</projects>