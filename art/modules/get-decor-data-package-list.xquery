xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

(: scans the database for artXformResources and returns a list of package roots with package title and abbreviation:)
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
declare namespace expath    = "http://expath.org/ns/pkg";

<packageRoots>
{
   for $package in xmldb:get-child-collections($get:strDecorData)
   let $abbrev      := $package
   let $expath-file := concat($get:strDecorData,'/',$package,'/expath-pkg.xml')
   let $title       := if (doc-available($expath-file)) then doc($expath-file)//expath:title else ($abbrev)
   order by lower-case($title)
   return
   <root abbrev="{$abbrev}" title="{$title}" ispackage="{doc-available($expath-file)}"/>
}
</packageRoots>