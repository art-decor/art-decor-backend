xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";

declare namespace hl7="urn:hl7-org:v3";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace datetime="http://exist-db.org/xquery/datetime";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

let $id :=request:get-parameter('id',())
(:let $id :='2.16.840.1.113883.2.4.6.99.1.77.2.20101':)
let $concept := $get:colDecorData//concept[@id=$id][not(ancestor::history)]
return
<conceptHistory>
{
	for $history in $concept/history
	return
	$history
}
</conceptHistory>


