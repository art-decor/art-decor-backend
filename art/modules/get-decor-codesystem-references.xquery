xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";

declare namespace xs    = "http://www.w3.org/2001/XMLSchema";

let $project            := if (request:exists()) then (request:get-parameter('project',())) else ()
let $decor              := $get:colDecorData//decor[project/@prefix=$project]
let $codeSystems        := $decor/terminology/codeSystem
let $language           := if (request:exists()) then (request:get-parameter('language',$decor/project/@defaultLanguage)) else ($decor/project/@defaultLanguage)

return
    <codeSystems projectPrefix="{$project}">
    {
        for $codeSystem in $codeSystems[@ref]
        order by $codeSystem/@ref
        return
        <codeSystem>
        {
            $codeSystem/@*
        }
        </codeSystem>
    }
    </codeSystems>