xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace ada     = "http://art-decor.org/ns/ada-common" at "../../ada/modules/ada-common.xqm";
import module namespace adaxml  = "http://art-decor.org/ns/ada-xml" at "../../ada/modules/ada-xml.xqm";
import module namespace art     = "http://art-decor.org/ns/art" at "art-decor.xqm";

let $projectPrefix  := if (request:exists()) then request:get-parameter('prefix',())[1] else ('demo1-')
let $decor          := if (empty($projectPrefix)) then () else art:getDecorByPrefix($projectPrefix)
let $language       := if (request:exists()) then request:get-parameter('language', $decor/project/@defaultLanguage)[1] else ($decor/project/@defaultLanguage)[1]

let $ada-apps       := if ($decor) then ada:getApplications((), $language) else ()

let $ada-apps       :=
    for $ada-app in $ada-apps
    return
        if ($decor//transaction[@id = $ada-app/view/@transactionId]) then ( $ada-app ) else ()

return
    <return>
    {
        $ada-apps
    }
    </return>
