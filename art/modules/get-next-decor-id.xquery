xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace decor   = "http://art-decor.org/ns/decor" at "../api/api-decor.xqm";

(: SHALL have either base or $projectPrefix + $type (may be multiple), where type is any object type as defined in api-decor.xqm :)
let $projectPrefix          := if (request:exists()) then request:get-parameter('project',()) else ('bccdae2e-')
let $types                  := if (request:exists()) then request:get-parameter('types',())[string-length()>0] else ($decor:OBJECTTYPE-SCENARIO)
let $baseId                 := if (request:exists()) then request:get-parameter('baseId',())[string-length()>0] else ()

let $types                  := tokenize(normalize-space(string-join($types, ' ')), '\s')

return
    <result>
    {
        decor:getNextAvailableId($projectPrefix, $types, $baseId)
    }
    </result>