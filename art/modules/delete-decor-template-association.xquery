xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
let $templateAssociation    := request:get-data()/templateAssociation
(:let $templateAssociation :=
<templateAssociation projectPrefix="demo1-" templateId="2.16.840.1.113883.3.1937.99.62.3.10.1" effectiveDate="2013-09-24T18:20:25">
   <concept ref="2.16.840.1.113883.3.1937.99.62.3.2.17" effectiveDate="2013-09-24T14:30:19" elementId="2.16.840.1.113883.3.1937.99.62.3.9.1"/>
</templateAssociation>
:)
let $decor                  := $get:colDecorData//decor[project/@prefix=$templateAssociation/@projectPrefix]
let $user                   := get:strCurrentUserName()
let $templateAssocInDb      := $decor//templateAssociation[@templateId=$templateAssociation/@templateId][@effectiveDate=$templateAssociation/@effectiveDate]

return
    if ($user=$decor/project/author/@username) then (
        let $delete :=
            for $assoc in $templateAssocInDb/concept[@ref=$templateAssociation/concept/@ref][@effectiveDate=$templateAssociation/concept/@effectiveDate][@elementId=$templateAssociation/concept/@elementId]
            return
                update delete $assoc
         
        return
            <response>OK</response>
    )
    else (
        <response>NO PERMISSION</response>
    )