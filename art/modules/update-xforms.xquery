xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adpfix      = "http://art-decor.org/ns/art-decor-permissions" at "../api/api-permissions.xqm";

(: Mode: are we going to upgrade to the new tree views, or downgrade to the old :)
let $mode                       := if (request:exists()) then request:get-parameter('mode', '') else 'status'
let $statusonly                 := if (request:exists()) then request:get-parameter('statusonly', 'false')='true' else true()

let $mode                       := if ($statusonly) then 'status' else $mode

return
    adpfix:updateXForms($mode)