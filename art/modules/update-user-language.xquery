xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace aduser      = "http://art-decor.org/ns/art-decor-users" at "../api/api-user-settings.xqm";

let $newUserLanguage := if (request:exists()) then request:get-parameter('language',()) else ()

let $userUpdate      :=  aduser:setUserLanguage(get:strCurrentUserName(),$newUserLanguage)

return
    <defaultLanguage>{aduser:getUserLanguage(get:strCurrentUserName())}</defaultLanguage>