xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
    
    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
declare namespace xs            = "http://www.w3.org/2001/XMLSchema";
declare namespace xforms        = "http://www.w3.org/2002/xforms";
declare option exist:serialize "method=xml media-type=text/xml";

let $simpleTypes := doc(concat($get:strOidsCore, '/', $get:strISO13582schema))/xs:schema/xs:simpleType[*/xs:enumeration]
return
    doc(concat($get:strOidsCore, '/', $get:strISO13582schema))
(:<enumeratedTypes>
{
    for $simpleType in $simpleTypes
    return
        element {$simpleType/@name} {
            for $enumeration in $simpleType//xs:enumeration
            return
                <enumeration value="{$enumeration/@value}">
                {
                    for $label in $enumeration//xforms:label
                    return
                        <label language="{$label/@xml:lang}">{$label/text()}</label>
                }
                </enumeration>
        }
}
</enumeratedTypes>:)