xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";

declare namespace xs="http://www.w3.org/2001/XMLSchema";
declare namespace xforms="http://www.w3.org/2002/xforms";



let $project := request:get-parameter('project','')
let $projectId := request:get-parameter('id','')
(:let $projectId :=''
let $project := 'sandbox-':)
let $decor :=
		if (string-length($project)>0) then
				$get:colDecorData//decor[project/@prefix=$project]
		else if (string-length($projectId)>0) then
				$get:colDecorData//decor[project/@id=$projectId]
		else()

let $root :=util:collection-name($decor)
let $runtimeColllection :=concat($root,'/',$decor/project/@prefix,'runtime-develop')

let $status :=
   if (xmldb:collection-available($runtimeColllection)) then
      let $mainTemplates :=xmldb:created($runtimeColllection,concat($decor/project/@prefix,'main-templates.xsl'))
      let $generatedTemplates :=xmldb:created($runtimeColllection,concat($decor/project/@prefix,'generated-templates.xsl'))
      let $generatedValuesets :=xmldb:created($runtimeColllection,concat($decor/project/@prefix,'generated-valuesets.xsl'))
      let $generatedXpaths :=xmldb:created($runtimeColllection,concat($decor/project/@prefix,'xpaths.xml'))
      let $allAvailable :=
         if (string-length($mainTemplates)>0 and string-length($generatedTemplates)>0 and string-length($generatedValuesets)>0 and string-length($generatedXpaths)>0) then
            true()
         else(false())
      return
      <status allAvailable="{$allAvailable}" projectPrefix="{$decor/project/@prefix}">
         <main created="{$mainTemplates}"/>
         <templates created="{$generatedTemplates}"/>
         <valuesets created="{$generatedValuesets}"/>
         <xpaths created="{$generatedXpaths}"/>
      </status>
   else(
      <status allAvailable="{false()}" projectPrefix="{$decor/project/@prefix}">
         <main created=""/>
         <templates created=""/>
         <valuesets created=""/>
         <xpaths created=""/>
      </status>
   )
return
$status