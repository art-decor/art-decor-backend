xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

(:
   Query for retrieving decor locks for specific project
:)
import module namespace get             = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art             = "http://art-decor.org/ns/art" at "art-decor.xqm";
import module namespace aduser          = "http://art-decor.org/ns/art-decor-users" at "../api/api-user-settings.xqm";
import module namespace adserver        = "http://art-decor.org/ns/art-decor-server" at "../api/api-server-settings.xqm";
import module namespace iss             = "http://art-decor.org/ns/decor/issue" at "../api/api-decor-issue.xqm";
import module namespace i18n            = "http://art-decor.org/ns/decor/i18n" at "../api/api-decor-i18n.xqm";

declare variable $docMessages           := i18n:getMessagesDoc('decor/services');
let $decorSchemaTypes                   := art:getDecorTypes()

let $projectPrefix              := if (request:exists()) then request:get-parameter('project', ())[not(. = '')]          else ()
let $decor                      := art:getDecorByPrefix($projectPrefix)

let $language                   := if (request:exists()) then request:get-parameter('language', ())[not(. = '')]        else ()
let $language                   := if (empty($language)) then $decor/project/@defaultLanguage else $language

let $currentUser                := get:strCurrentUserName()
let $currentUserDisplayName     := try {aduser:getUserDisplayName($currentUser)} catch * {$currentUser}
let $currentUserName            := if (string-length($currentUserDisplayName)=0) then $currentUser else $currentUserDisplayName

let $compileLanguages           := string-join($decor/project/name/@language,' ')

let $baseDate                   := if (request:exists()) then request:get-parameter('baseDate', ())[. castable as xs:dateTime] else ()
let $lastProjectDate            := if (empty($baseDate)) then max(($decor/project/version/xs:dateTime(@date),$decor/project/release/xs:dateTime(@date))) else xs:dateTime($baseDate)

return
    if ($projectPrefix and $decor) then (
        <version by="{$currentUserName}" versionLabel="" release="false" development="false" prefix="{$projectPrefix}" compile-language="{$compileLanguages}" publication-request="false" date="">
        {
            art:serializeNode(
            <desc language="{$language}">
            {
                let $issues :=
                    for $issue in $decor/issues/issue
                    let $meta           := iss:getIssueMeta($issue, false(), $decor/project/author, $projectPrefix)
                    let $issueId        := $issue/@id
                    let $issueName      := $issue/@displayName
                    let $issueStatus    := $decorSchemaTypes//IssueStatusCodeLifeCycle/*[@value=$meta/@currentStatusCode]/label[@language=$language]
                    return
                        if (empty($lastProjectDate) or $meta/xs:dateTime(@lastDate) > $lastProjectDate) then (
                            <li>
                                <div class="node-issue-{$meta/@currentStatusCode}">
                                {
                                    i18n:getMessage($docMessages, 'IssueProcessedMoreInfo', $language, concat(adserver:getServerURLArt(),'decor-issues--',$projectPrefix,'?id=',$issueId), tokenize($issueId,'\.')[last()], $issueName, $issueStatus),
                                    ' ',
                                    i18n:getMessage($docMessages, 'LastIssueEvent', $language),
                                    concat(' (',$meta/string(@lastDate),'): ')
                                }
                                </div>
                                <div style="padding: 10px;">{$issue/tracking[@effectiveDate = max($issue/tracking/xs:dateTime(@effectiveDate))]/desc[1]/node()}</div>
                            </li>
                        ) else ()
                
                return
                    if ($issues) then (
                        if (count($issues) > 1) then 
                            <div>{i18n:getMessage($docMessages, 'IssuesWithChangesSinceLastVersionOrRelease', $language, string(count($issues)))}</div>
                        else
                            <div>{i18n:getMessage($docMessages, 'IssueWithChangesSinceLastVersionOrRelease', $language)}</div>
                        ,
                        <ul>{$issues}</ul>
                    ) else (
                        <div>{i18n:getMessage($docMessages, 'IssuesNotChangedSinceLastVersionOrRelease', $language)}</div>
                    )
            }
            </desc>
            )
        }
        </version> 
    )
    else (
        error(QName('http://art-decor.org/ns/art-decor', 'MissingParameter'), concat('You are missing parameter ''project'' or its value ''',$projectPrefix,''' did not match any known project/@prefix'))
    )