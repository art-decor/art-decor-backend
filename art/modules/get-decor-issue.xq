xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace iss     = "http://art-decor.org/ns/decor/issue" at "../api/api-decor-issue.xqm";

let $issueId        := if (request:exists()) then (request:get-parameter('id',())) else ()
let $skipObjects    := false()

return
    iss:getExpandedIssuesById($issueId,$skipObjects)