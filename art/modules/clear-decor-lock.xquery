xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

(:
   Query for deleting lock
:)
import module namespace decor          = "http://art-decor.org/ns/decor" at "../api/api-decor.xqm";

let $ref            := if (request:exists()) then request:get-parameter('ref',())[not(.='')]            else ()
let $effectiveDate  := if (request:exists()) then request:get-parameter('effectiveDate',())[not(.='')]  else ()
let $projectId      := if (request:exists()) then request:get-parameter('projectId',())[not(.='')]      else ()

let $clear          := decor:deleteLock($ref, $effectiveDate, $projectId)

return
    <data-safe>true</data-safe>
