xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";

declare namespace expath    = "http://expath.org/ns/pkg";

let $allPackages := collection($get:root)/expath:package
return
<packages>
{
    for $package in $allPackages
    let $parentCollection := substring-after(util:collection-name($package),$get:root)
    order by lower-case($parentCollection)
    return
    <package title="{$package//expath:title}" collection="{$parentCollection}">
    {
        $package/@version | $package/@abbrev
    }
    </package>
}
</packages>