xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

(:
   Query for deleting lock entries AND deleting newly created concepts
   Input:
   edited dataset 
   Return:
   data-safe=true
:)
import module namespace decor           = "http://art-decor.org/ns/decor" at "../api/api-decor.xqm";
import module namespace get             = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";

let $dataset        := if (request:exists()) then request:get-data()/dataset else ()

let $clear          := 
    for $lock in $dataset//lock[@type=($decor:OBJECTTYPE-DATASET,$decor:OBJECTTYPE-DATASETCONCEPT)]
    let $ref            := $lock/@ref
    let $effectiveDate  := $lock/@effectiveDate
    let $projectId      := ()
    return
        decor:deleteLock($ref, $effectiveDate, $projectId)

let $deletes    :=
    for $concept in $dataset//concept[@statusCode='new'][not(ancestor::history)]
    return
        update delete $get:colDecorData//concept[@id=$concept/@id][@effectiveDate=$concept/@effectiveDate][not(ancestor::history)]

return
    <data-safe>true</data-safe>

