xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art     = "http://art-decor.org/ns/art" at "art-decor.xqm";

declare namespace xs            = "http://www.w3.org/2001/XMLSchema";
declare namespace xforms        = "http://www.w3.org/2002/xforms";

let $projectPrefix  := if (request:exists()) then request:get-parameter('project',()) else ()
let $projectId      := if (request:exists()) then request:get-parameter('id',()) else ()

let $decorProject   :=
    if (string-length($projectPrefix) gt 0) then
        art:getDecorByPrefix($projectPrefix)
    else 
    if (string-length($projectId) gt 0) then
        art:getDecorById($projectId)
    else ()

let $prefix         := $decorProject/project/@prefix
let $pprefix        := replace($prefix,'-$','')
let $strVersions    := concat($get:strDecorVersion, '/', $pprefix)
return
<versions projectPrefix="{$prefix}" projectId="{$decorProject/project/@id}" asOf="{format-dateTime(current-dateTime(), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')}">
{
    for $version in $decorProject/project/version|$decorProject/project/release
    let $psuffix                := replace($version/@date, '[:\-]', '')
    let $strVersion             := concat($strVersions, '/version-', $psuffix)
    let $strRequest             := concat($pprefix, '-', $psuffix, '-publication-request.xml')
    let $strComplete            := concat($pprefix, '-', $psuffix, '-publication-completed.xml')
    (: publicationstatus 
        version     = version stored but no publication request
        pending     = publication request issued, but publication not yet completed nor started
        completed   = publication request issued, publication completed without errors
        failed      = publication request issued, publication completed with errors
        inprogress  = publication request is being processed
    :)
    let $pubexists              := xmldb:collection-available($strVersion)
    let $archiveexists          := count(doc(concat($get:strDecorHistory, '/', $pprefix, '/DECOR.xml'))/histories[@type='DECOR'][@project=$prefix]/history[format-dateTime(@date, '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')=$version/@date])=1
    let $docComplete            := 
        if ($pubexists) then 
            if (xmldb:get-child-resources($strVersion) = $strComplete) then (doc(concat($strVersion,'/',$strComplete))/publication) else ()
        else ()
    let $pubstatus      := 
        if ($docComplete) then (
            (:if a publication completed notice exists use it:)
            if ($docComplete[string-length(@statusCode) gt 0]) then $docComplete/@statusCode else 'completed'
        )
        else 
        if ($version[@statusCode = 'cancelled']) then (
            (:if a publication completed notice does not exist and current project version is cancelled, never mind publishing:)
        )
        else 
        if ($version[@statusCode = 'retired']) then (
            (:if a publication completed notice does not exist and current project version is retired, never mind publishing:)
        )
        else
        if ($pubexists) then (
            (:if a publication completed notice does not exist but publication request notice exists, assume pending:)
            if (xmldb:get-child-resources($strVersion)[.=$strRequest]) then ('pending') else ()
        )
        else ()
    let $pubstatus      := if ($pubexists) then string-join(('version',$pubstatus),' ') else ()
    order by $version/@date descending
    return
        element {$version/name()} {
            $version/(@* except (@publicationdate|@publicationstatus)),
            attribute {'publicationdate'} {
                $psuffix
            },
            attribute {'publicationstatus'} {
                $pubstatus
            },
            attribute {'archivestatus'} {
                $archiveexists
            },
            for $desc in $version/desc|$version/note
            return
                art:serializeNode($desc)
        }

}
</versions>