xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace art     = "http://art-decor.org/ns/art" at "art-decor.xqm";
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "xmldb:exist:///db/apps/art/modules/art-decor-settings.xqm";

let $project        := if (request:exists()) then request:get-parameter('project',()) else ('peri20-')
(: 
    all     = all transactions
    ds      = only transactions that have a representingTemplate/@sourceDataset
    tm      = only transactions that have a representingTemplate/@ref
    group   = only transactions of type group (transactions that group other transactions)
    item    = only transactions of type item (transactions that are stationary, initial or back)
:)
let $type           := if (request:exists()) then request:get-parameter('type','all') else ('ds')

return
    art:getScenarioList($project, (), (), $type)