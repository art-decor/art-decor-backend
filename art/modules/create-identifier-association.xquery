xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace art             = "http://art-decor.org/ns/art" at "art-decor.xqm";
import module namespace get             = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace adserver        = "http://art-decor.org/ns/art-decor-server" at "../api/api-server-settings.xqm";

let $identifierAssociation  := if (request:exists()) then request:get-data()/association else (
    (:<association bind="code" conceptId="2.16.840.1.113883.3.1937.99.62.3.2.5.1" conceptFlexibility=""
        code="DM" codeSystem="2.16.840.1.113883.3.1937.99.62.3.5.2" displayName="Datum meting" codeSystemName="" codeSystemVersion="" equivalence="" 
        valueSet="" flexibility="" strength="" 
        effectiveDate="" expirationDate="" versionLabel="" 
        prefix="demo-1" inheritFromPrefix="demo1-" parentConceptId="2.16.840.1.113883.3.1937.99.62.3.2.5" parentConceptFlexibility="2011-01-28T00:00:00" transactionId="2.16.840.1.113883.3.1937.99.62.3.4.2" transactionEffectiveDate="2012-09-05T16:59:35"/>:)
)
let $projectPrefix              := $identifierAssociation/@prefix[string-length() gt 0]
let $transactionId              := $identifierAssociation/@transactionId[string-length() gt 0]
let $transactionEffectiveDate   := $identifierAssociation/@transactionEffectiveDate[string-length() gt 0]

(: relevant for transaction bindings to conceptList/concept to know which concept to add them to :)
let $conceptId                  := if ($identifierAssociation/@parentConceptId[string-length() gt 0]) then $identifierAssociation/@parentConceptId else $identifierAssociation/@conceptId
let $conceptEffectiveDate       := if ($identifierAssociation/@parentConceptFlexibility[string-length() gt 0]) then $identifierAssociation/@parentConceptFlexibility else $identifierAssociation/@conceptFlexibility

let $newAssociation             := art:prepareIdentifierAssociationForUpdate($identifierAssociation)

let $concept                    :=
    if ($transactionId) then
        art:getTransactionConcept($conceptId, $conceptEffectiveDate, $transactionId, $transactionEffectiveDate, (), ())
    else (
        art:getConcept($conceptId, $conceptEffectiveDate)
    )
    
let $decor                      := 
    if (empty($projectPrefix)) then
        $concept/ancestor::decor
    else (
        art:getDecorByPrefix($projectPrefix)
    )

let $updateAssociation          :=
    if ($transactionId) then (
        update insert $newAssociation into $concept
    )
    else
    if ($decor/ids/*) then (
        update insert $newAssociation following $decor/ids/*[last()]
    )
    else (
        update insert $newAssociation into $decor/ids
    )

return
    <data-safe>{true()}</data-safe>