xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

(:
Searches for template by @name and @displayName
If project prefix is present search is limited to that project
:)
import module namespace adsearch       = "http://art-decor.org/ns/decor/search" at "../api/api-decor-search.xqm";

let $searchTerms    := if (request:exists()) then tokenize(lower-case(request:get-parameter('searchString',())),'[\s\-]') else ('demo')
let $language       := if (request:exists()) then request:get-parameter('language',())[string-length() gt 0] else ('demo')

let $maxResults     := xs:integer('50')

(:
<result current="24" total="24">
    <template project="epsos-" projectName="epSOS" sortname="epSOS CDA recordTarget" id="2.16.840.1.113883.2.4.3.11.60.22.10.100" 
            name="epSOSCDArecordTarget" displayName="epSOS CDA recordTarget" 
            effectiveDate="2013-12-20T00:00:00" statusCode="draft" isClosed="false">
        <classification type="cdaheaderlevel"/>
    </template>
</result>
:)
return
    adsearch:searchProject($searchTerms[not(. = '')], $maxResults, (), $language[1])