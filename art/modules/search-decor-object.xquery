xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adsearch        = "http://art-decor.org/ns/decor/search" at "../api/api-decor-search.xqm";
import module namespace get             = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";

let $searchTerms            := if (request:exists()) then (
                                    for $q in request:get-parameter('searchString',())[string-length() gt 0]
                                    return tokenize(normalize-space(lower-case($q)),'\s')
 ) else ()
let $registryName           := if (request:exists()) then (request:get-parameter('registry',())[string-length()>0]) else ()
let $projectPrefix          := if (request:exists()) then (request:get-parameter('project',())[string-length()>0]) else ()
let $projectVersion         := if (request:exists()) then (request:get-parameter('version',())[string-length()>0]) else ()
let $projectLanguage        := if (request:exists()) then (request:get-parameter('language',())[string-length()>0]) else ()
let $type                   := if (request:exists()) then (request:get-parameter('type',())[string-length()>0]) else ()
let $maxResults             := if (request:exists()) then (request:get-parameter('maxResults',())[string-length()>0]) else ()
let $maxResults             := if ($maxResults castable as xs:integer) then xs:integer($maxResults) else xs:integer('10')

(:specific to searching concepts:)
let $datasetId              := if (request:exists()) then (request:get-parameter('datasetId',())[string-length()>0]) else ()
let $datasetEffectiveDate   := if (request:exists()) then (request:get-parameter('datasetEffectiveDate',())[string-length()>0]) else ()
let $conceptType            := if (request:exists()) then (request:get-parameter('conceptType',())[string-length()>0]) else ()
let $originalConceptsOnly   := if (request:exists()) then (request:get-parameter('originalConceptsOnly',('false'))) else ('false')
let $localConceptsOnly      := if (request:exists()) then (request:get-parameter('localConceptsOnly',('true'))) else ('true')

return
    if ($type='VS') then (
        adsearch:searchValueset($projectPrefix, $searchTerms, $maxResults, $projectVersion, $projectLanguage)
    )
    else if ($type='DE') then (
        adsearch:searchConcept($projectPrefix, $searchTerms, $maxResults, $datasetId, $datasetEffectiveDate, $conceptType, $originalConceptsOnly='true', $localConceptsOnly='true')
    )
    else if ($type='TM') then (
        adsearch:searchTemplate($projectPrefix, $searchTerms, $maxResults, $projectVersion, $projectLanguage)
    )
    else if ($type='IS') then (
        adsearch:searchIssue($projectPrefix, $searchTerms, $maxResults, $projectVersion, $projectLanguage)
    )
    else if ($type='SC') then (
        adsearch:searchScenario($projectPrefix, $searchTerms, $maxResults, $projectVersion, $projectLanguage)
    )
    else if ($type='TR') then (
        adsearch:searchTransaction($projectPrefix, $searchTerms, $maxResults, $projectVersion, $projectLanguage)
    )
    else if ($type='QQ' or $type='QE') then (
        adsearch:searchQuestionnaire($projectPrefix, $searchTerms, $maxResults, $projectVersion, $projectLanguage)
    )
    else if ($type='QR') then (
        adsearch:searchQuestionnaireResponse($projectPrefix, $searchTerms, $maxResults, $projectVersion, $projectLanguage)
    )
    else if ($type='DS') then (
        adsearch:searchDataset($projectPrefix, $searchTerms, $maxResults, $projectVersion, $projectLanguage)
    )
    else if ($type='CS') then (
        adsearch:searchCodesystem($projectPrefix, $searchTerms, $maxResults, $projectVersion, $projectLanguage)
    )
    else if ($type='id') then (
        adsearch:searchOID($registryName, $searchTerms, $maxResults, $projectPrefix, $projectVersion, $projectLanguage)
    )
    else (
    )