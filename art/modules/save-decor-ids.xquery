xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art     = "http://art-decor.org/ns/art" at "art-decor.xqm";
import module namespace decor   = "http://art-decor.org/ns/decor" at "../api/api-decor.xqm";
declare namespace request       = "http://exist-db.org/xquery/request";

let $updatedIds     := if (request:exists()) then request:get-data()/ids else ()
let $projectPrefix  := if (request:exists()) then request:get-parameter('project',())[string-length()>0] else ()
let $decor          := if (empty($projectPrefix)) then () else art:getDecorByPrefix($projectPrefix)

let $storedIds      := $decor/ids

let $userCanEdit    :=
    if ($decor and $updatedIds) then decor:authorCanEditP($decor, $decor:SECTION-IDS) else (false())

let $cleanIds       := 
    <ids>
    {
        (:
            Old style:
                <baseId id="1.2.3" type="DS" prefix="xyz"/>
                <defaultBaseId id="1.2.3" type="DS"/>
            New style:
                <baseId id="1.2.3" type="DS" prefix="xyz" default="true"/>
                
            Rewrite old style to new style.
        :)
        for $baseId in $updatedIds/baseId[@id[string-length()>0]][@type][@prefix]
        return
            <baseId>
            {
                $baseId/(@*[string-length()>0] except @status)
                ,
                if ($baseId[empty(@default)]) then (
                    attribute {'default'} {$updatedIds/defaultBaseId[@id=$baseId/@id]}
                )
                else()
            }
            </baseId>
        ,
        (: For now: keep old style so we can fix all dependent code later :)
        if ($updatedIds/baseId[@default]) then 
            for $baseId in $updatedIds/baseId[@id[string-length()>0]][@type][@default = 'true']
            return
                <defaultBaseId>{$baseId/@id, $baseId/@type}</defaultBaseId>
        else (
            for $baseId in $updatedIds/defaultBaseId[@id[string-length()>0]][@type]
            return
                <defaultBaseId>{$baseId/@id, $baseId/@type}</defaultBaseId>
        )
        ,
        for $n in $updatedIds/id[@root[string-length()>0]]
        return
            <id>
            {
                $n/@*,
                for $designation in $n/designation[@displayName[string-length()>0]][string-length(string-join(.,''))>0]
                return
                    <designation>
                    {
                        $designation/@*[string-length()>0],
                        $designation/node()
                    }
                    </designation>
                ,
                for $property in $n/property[@name[string-length()>0]][string-length(string-join(.,''))>0]
                return
                    <property>
                    {
                        $property/@name,
                        $property/node()
                    }
                    </property>
            }
            </id>
        ,
        $storedIds/(* except (baseId | defaultBaseId | id))
    }
    </ids>
    
let $update         := 
    if ($userCanEdit) then (update replace $storedIds with $cleanIds) else (
        error(QName('http://art-decor.org/ns/art-decor-permissions', 'NoPermission'), concat('You don''t have permission to save these ids. ',get:strCurrentUserName()))
    )

return
<data-safe>{$userCanEdit}</data-safe>