xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art     = "http://art-decor.org/ns/art" at "art-decor.xqm";
import module namespace templ   = "http://art-decor.org/ns/decor/template" at "../api/api-decor-template.xqm";

let $project                := if (request:exists()) then request:get-parameter('project',()) else ('demo1-')
let $version                := if (request:exists()) then request:get-parameter('version',()) else ()
let $classified             := if (request:exists()) then request:get-parameter('classified',()) else ()
let $templateId             := if (request:exists()) then request:get-parameter('id',()) else ('2.16.840.1.113883.3.1937.99.62.3.10.4')
let $templateEffectiveDate  := if (request:exists()) then request:get-parameter('effectiveDate',()) else ('2009-10-01T00:00:00')
let $treetype               := if (request:exists()) then request:get-parameter('treetype',()) else ($templ:TREETYPEMARKED)

return
    templ:getTemplateList($templateId,(),$templateEffectiveDate,$project,$version,$classified='true',$treetype)