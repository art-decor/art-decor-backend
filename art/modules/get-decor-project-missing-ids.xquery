xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";

declare namespace xs="http://www.w3.org/2001/XMLSchema";
declare namespace xforms="http://www.w3.org/2002/xforms";



let $project := request:get-parameter('project','')
let $projectId := request:get-parameter('id','')
(:let $projectId :=''
let $project := 'sandbox-':)
let $decor :=
		if (string-length($project)>0) then
				$get:colDecorData//decor[project/@prefix=$project]
		else if (string-length($projectId)>0) then
				$get:colDecorData//decor[project/@id=$projectId]
		else()

let $decorIds:= $decor/ids
let $usedIds := (distinct-values($decor//valueSet//concept/@codeSystem),distinct-values($decor//valueSet//sourceCodeSystem/@id))

let $artLanguages := $get:colArtResources//resources/@xml:lang/string()

return
<missingIds projectPrefix="{$decor/project/@prefix}">
{
   for $usedId in $usedIds[not(.=$decorIds//id/@root)]
   return
   <id root="{$usedId}">
      {
         for $lang in $artLanguages
         return
         <designation language="{$lang}" type="preferred" displayName=""/>
       }
   </id>
}
</missingIds>
