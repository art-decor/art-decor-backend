xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace adserver = "http://art-decor.org/ns/art-decor-server" at "../api/api-server-settings.xqm";

let $getdefault := if (request:exists()) then request:get-parameter('getdefault','false') else ('false')
let $settings   := if ($getdefault='true') then adserver:getServerSettingsDefault() else adserver:getServerSettings()

return
    element {$settings/name()} {
        attribute action {''},
        $settings/(@* except @action),
        $settings/node()
    }