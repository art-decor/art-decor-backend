xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get    = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace aduser = "http://art-decor.org/ns/art-decor-users" at "../api/api-user-settings.xqm";
declare namespace sm           = "http://exist-db.org/xquery/securitymanager";
declare namespace request      = "http://exist-db.org/xquery/request";
declare namespace response     = "http://exist-db.org/xquery/response";

(:<user>
    <username>username</username>
    <password>password</password>
</user>:)
(:Before login:

<sm:id xmlns:sm="http://exist-db.org/xquery/securitymanager">
    <sm:real>
        <sm:username>admin</sm:username>
        <sm:groups>
            <sm:group>dba</sm:group>
        </sm:groups>
    </sm:real>
</sm:id>:)
(:After login for different user:

<sm:id xmlns:sm="http://exist-db.org/xquery/securitymanager">
    <sm:real>
        <sm:username>admin</sm:username>
        <sm:groups>
            <sm:group>dba</sm:group>
        </sm:groups>
    </sm:real>
    <sm:effective>
        <sm:username>alexander</sm:username>
        <sm:groups>
            <sm:group>dba</sm:group>
            <sm:group>debug</sm:group>
            <sm:group>decor</sm:group>
            <sm:group>decor-admin</sm:group>
            <sm:group>editor</sm:group>
            <sm:group>issues</sm:group>
            <sm:group>terminology</sm:group>
            <sm:group>tools</sm:group>
        </sm:groups>
    </sm:effective>
</sm:id>:)
let $login-data         := if (request:exists()) then request:get-data()/user else ()
let $user               := $login-data/@name
let $pwd                := $login-data/@pass
let $return-url         := ($login-data/@return-url, 'home')[not(. = '')][not(matches(normalize-space(lower-case(.)), '^[a-z_\-]+:'))][1]
let $session            := session:create()
let $login              := if (xmldb:authenticate('/db', $user, $pwd)) then xmldb:login('/db', $user, $pwd, true()) else false()

let $effective-username := get:strCurrentUserName()

return
if ($login) then (
    let $userLanguage       := aduser:getUserLanguage($user)
    let $userDisplayName    := aduser:getUserDisplayName($user)
    let $groups             := sm:get-user-groups($user)
    return
        <user name="{$effective-username}" pass="{$pwd}" logged-in="{$login}" return-url="{$return-url}">
            <defaultLanguage>{$userLanguage}</defaultLanguage>
            <displayName>{$userDisplayName}</displayName>
            <groups>{for $group in $groups return <group>{$group}</group>}</groups>
        </user>
)
else (
    let $logout             := session:invalidate()
    return
        <user name="{$user}" pass="{$pwd}" logged-in="{$login}" return-url="{$return-url}">
            <defaultLanguage>{$get:strArtLanguage}</defaultLanguage>
            <displayName/>
            <groups/>
        </user>
)

