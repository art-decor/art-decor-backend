xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace adserver = "http://art-decor.org/ns/art-decor-server" at "../api/api-server-settings.xqm";

let $newServerLanguage  := if (request:exists()) then request:get-parameter('language',()) else ()
    
let $update             := adserver:setServerLanguage($newServerLanguage)

return
    <defaultLanguage>{adserver:getServerLanguage()}</defaultLanguage>