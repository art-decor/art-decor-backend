xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get             = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art             = "http://art-decor.org/ns/art" at  "art-decor.xqm";
import module namespace decor           = "http://art-decor.org/ns/decor" at "../api/api-decor.xqm";

declare %private function local:preparePrototype($prototype as element()) as element() {
    <prototype>
    {
        for $data in $prototype/data
        return
            <data>
            {
                $data/@*,
                for $desc in $data/desc
                return
                    art:parseNode($desc),
                $data/enumValue
            }
            </data>
    }
    </prototype>
};

declare %private function local:prepareAssociations($associations as element()) as element() {
    <associations>
    {
        for $association in $associations/association
        return
            <association>
            {
                $association/object,
                for $data in $association/data
                return
                    (: Discard data if empty :)
                    if ($data/text()) then art:parseNode($data) else ()
            }
            </association>
    }
    </associations>
};

(: get community from request :)
let $editedCommunity    := if (request:exists()) then request:get-data()/community else ()
(:let $editedCommunity    := 
    <community name="demo1" projectId="2.16.840.1.113883.3.1937.99.62.3" displayName="demo1">
        <edit/>
        <lock type="CM" ref="demo1" user="alexander" userName="Alexander Henket" since="2015-07-08T01:50:27.712+02:00" prefix="demo1-" projectId="2.16.840.1.113883.3.1937.99.62.3"/>
        <desc language="nl-NL">demo1</desc>
        <access>
            <author username="alexander" rights="rw"/>
        </access>
        <prototype>
            <data type="label1" label="label1" datatype="text">
                <desc language="nl-NL">label1</desc>
            </data>
        </prototype>
        <associations/>
    </community>:)

(: community stored in DB :)
let $storedCommunity    := decor:getDecorCommunity($editedCommunity/@name, $editedCommunity/@projectId, true())
(: check if lock in edited community is matched in db :)
let $userHasLock        := decor:isLockedByUser($editedCommunity/lock/@ref, (), $editedCommunity/lock/@projectId)

let $updates            :=
    if ($userHasLock) then (
        update value $storedCommunity/@displayName with $editedCommunity/@displayName,
        update replace $storedCommunity/desc with for $desc in $editedCommunity/desc return art:parseNode($desc),
        update replace $storedCommunity/access with $editedCommunity/access,
        if ($storedCommunity/prototype/@ref) then () else (
            update replace $storedCommunity/prototype with local:preparePrototype($editedCommunity/prototype)
        ),
        if ($storedCommunity/associations) then () else (
            update insert <associations/> following $storedCommunity/prototype
        ),
        update replace $storedCommunity/associations with local:prepareAssociations($editedCommunity/associations),
        decor:deleteLock($editedCommunity/lock/@ref, (), $editedCommunity/lock/@projectId)
    ) else 
        error(QName('http://art-decor.org/ns/error', 'UserHasNoLock'), concat('User ', get:strCurrentUserName(), ' has no lock.'))

return
    <data-safe>{$userHasLock}</data-safe>