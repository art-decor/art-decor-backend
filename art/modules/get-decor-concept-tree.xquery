xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace art ="http://art-decor.org/ns/art" at "art-decor.xqm";
declare namespace request = "http://exist-db.org/xquery/request";

let $conceptId                  := if (request:exists()) then request:get-parameter('id',())[not(.='')]                       else ('2.16.840.1.113883.3.1937.99.62.3.2.18')
let $conceptEffectiveDate       := if (request:exists()) then request:get-parameter('effectiveDate',())[not(.='')]            else ('2011-01-28T00:00:00')
let $transactionId              := if (request:exists()) then request:get-parameter('transactionId',())[not(.='')]            else ('2.16.840.1.113883.3.1937.99.62.3.4.2')
let $transactionEffectiveDate   := if (request:exists()) then request:get-parameter('transactionEffectiveDate',())[not(.='')] else ('2012-09-05T16:59:35')
(:relevant only for transactions. If all you care about is the final tree without any absent concepts: set this parameter to false:)
let $fullTree                   := if (request:exists()) then request:get-parameter('fullTree','true')[not(.='')] else ('false')

return art:getConceptTree($conceptId, $conceptEffectiveDate, $transactionId, $transactionEffectiveDate, $fullTree='true')