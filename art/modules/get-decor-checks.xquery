xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";

declare option exist:serialize "indent=yes";
declare option exist:serialize "omit-xml-declaration=no";

let $projectPrefix          := if (request:exists()) then request:get-parameter('prefix','') else ('onco-generic-')

return
if ($projectPrefix= '') then (
    if (response:exists()) then (response:set-status-code(404), response:set-header('Content-Type','text/xml; charset=utf-8')) else (), 
    <error>Missing parameter 'prefix'</error>
)
else (
    if (response:exists()) then (response:set-header('Content-Type','text/xml; charset=utf-8')) else (),
    doc(get:strProjectDevelopmentDoc($projectPrefix, false()))
)