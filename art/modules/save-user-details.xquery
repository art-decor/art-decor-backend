xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get    = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace aduser = "http://art-decor.org/ns/art-decor-users" at "../api/api-user-settings.xqm";

declare namespace sm           = "http://exist-db.org/xquery/securitymanager";

let $userInfo             := if (request:exists()) then (request:get-data()/user) else ()
(:let $userInfo :=
    <user name="test" active="true">
        <!-- user-info -->
        <defaultLanguage>{$get:strArtLanguage}</defaultLanguage>
        <displayName>Test user</displayName>
        <email></email>
        <organization>Test organization</organization>
        <projects>
            <project prefix="sandbox-" name="Sandbox" email="xxx@yyy.nl"/>
        </projects>
    </user>:)
    
let $userName            := $userInfo[string-length(@name)>0]/@name/string()

(: user-info Initially set by the admin that creates the account, but also user editable from there on :)
let $userLanguage        := $userInfo/defaultLanguage[string-length()>0]/text()
let $userDisplayName     := $userInfo/displayName[string-length()>0]/text()
let $userEmail           := $userInfo/email[string-length()>0]/text()
let $userOrganization    := $userInfo/organization[string-length()>0]/text()

(: Save user details for all users except SYSTEM :)
let $userSaved           :=
    if (sm:user-exists($userName)) then (
        if (not($userName='SYSTEM')) then (
            (: Update user-info :)
            let $updatedExtraInfo    := aduser:setUserInfo($userName, $userLanguage, $userDisplayName, $userEmail, $userOrganization)
            return 'true'
        ) else (
            'user-SYSTEM-not-supported'
        )
    ) else (
        'user-does-not-exist'
    )
let $updatedProjects     :=
    if ($userSaved='true') then (
        if (sm:get-user-groups($userName)='decor') then (
            try {
                for $project in $userInfo/projects/project
                let $matchingAuthor  := $get:colDecorData//project[@prefix = $project/@prefix]
                let $matchingAuthor  := $matchingAuthor/author[@username = $userName]
                let $emailUpdate     :=
                    if ($matchingAuthor) then (
                        if ($project/@email[string-length()>0]) then (
                            if ($matchingAuthor[@email]) then ( 
                                update value $matchingAuthor/@email with $project/@email
                            ) else (
                                update insert attribute email {$project/@email} into $matchingAuthor
                            )
                        ) else if (exists($matchingAuthor/@email)) then (
                            update delete $matchingAuthor/@email
                        ) else ( (:nothing to do:) )
                    ) else (     (: author does not exist? :) )
                let $notifierUpdate  :=
                    if ($matchingAuthor) then (
                        if ($project/@notifier[string-length()>0]) then (
                            if ($matchingAuthor[@notifier]) then ( 
                                update value $matchingAuthor/@notifier with $project/@notifier
                            ) else (
                                update insert attribute notifier {$project/@notifier} into $matchingAuthor
                            )
                        ) else if ($matchingAuthor[@notifier]) then (
                            update delete $matchingAuthor/@notifier
                        ) else ( (:nothing to do:) )
                    ) else (     (: author does not exist? :) )
                return concat('project-',$project/@prefix,'done')
            }
            catch * {
                'error-updating-projects'
            }
        ) else (
            'error-user-not-in-group-decor'
        )
    ) else (
        'error-user-projects-not-saved'
    )

let $projects-safe      := not(exists($updatedProjects[starts-with(.,'error-')]))
let $users-safe         := $userSaved='true'

return
    <data-safe projects-safe="{$projects-safe}">
    {
        if ($projects-safe) then () else (attribute projects-safe-text {$updatedProjects}),
        if ($users-safe) then () else (attribute user-safe-text {$userSaved}),
        $users-safe
    }
    </data-safe>