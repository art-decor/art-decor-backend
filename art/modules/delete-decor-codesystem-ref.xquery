xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

(:
   Xquery for inserting valueSet ref into terminology
   Input: post of valueSet element:
   <valueSet projectPrefix="demo1-" ref="2.16.840.1.113883.1.11.1" name="AdministrativeGender" displayName="AdministrativeGender"/>
:)
import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art ="http://art-decor.org/ns/art" at "art-decor.xqm";

let $projectPrefix  := request:get-parameter('prefix','')
let $objref         := request:get-parameter('ref','')

(:get decor file:)
let $decor          := $get:colDecorData//decor[project/@prefix=$projectPrefix]
let $ref            := $decor//codeSystem[@ref=$objref]
(: get user for permission check:)
let $user           := get:strCurrentUserName()
let $response       :=
    if ($user=$decor/project/author/@username) then (
        update delete $ref,
        <response>OK</response>
    )
    else(<response>NO PERMISSION</response>)

return
    $response