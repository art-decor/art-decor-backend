xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art     = "http://art-decor.org/ns/art" at "art-decor.xqm";

declare option exist:serialize "indent=yes";
declare option exist:serialize "omit-xml-declaration=no";

let $projectPrefix  := if (request:exists()) then request:get-parameter('project','') else ('demo1-')
let $checks         := if (request:exists()) then request:get-parameter('checks','') else ('aa bb unibid')
let $checkFile      := concat($projectPrefix, 'check-config.xml')
(: 
If there is $get:strDecorVersion and no [project]/development, create it 
This code can be removed if release/[project]/development once is created on project creation
:)
let $make           :=  if (not(xmldb:collection-available(get:strProjectDevelopment($projectPrefix))) and xmldb:collection-available($get:strDecorVersion)) 
                        then art:mkcol($get:strDecorVersion, replace(get:strProjectDevelopment($projectPrefix), $get:strDecorVersion, ''))
                        else ()
let $save           := try { xmldb:store(get:strProjectDevelopment($projectPrefix), $checkFile, <config optionalChecks="{$checks}"/>) } catch * {"NOTSAVED"}
let $save           := if ($save="NOTSAVED") then $save else try { sm:chmod(xs:anyURI($save), 'rw-rw-r--') } catch * {"NOTCHMODED"}

return ()