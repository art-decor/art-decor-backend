xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace adsearch       = "http://art-decor.org/ns/decor/search" at "../api/api-decor-search.xqm";

let $searchTerms            := if (request:exists()) then tokenize(lower-case(request:get-parameter('searchString',())),'\s') else ()
let $projectPrefix          := if (request:exists()) then (request:get-parameter('project',())) else ()
let $datasetId              := if (request:exists()) then (request:get-parameter('datasetId',())) else ()
let $datasetEffectiveDate   := if (request:exists()) then (request:get-parameter('datasetEffectiveDate',())) else ()
let $conceptType            := if (request:exists()) then (request:get-parameter('type',())) else ()
let $statusCodes            := if (request:exists()) then (request:get-parameter('status',())) else ()
let $searchPrefix           := if (request:exists()) then (request:get-parameter('searchPrefix',())[string-length() gt 0]) else ()

(:mostly interesting when we're called from the similar concepts context in the dataset form when we're creating a new concept:)
(:we inherit only from original concepts:)
let $originalOnly           := if (request:exists()) then request:get-parameter('originalonly',()) else ()

(:we want to distinguish between local searches and searches that include the current project:)
let $localConceptsOnly      := if (request:exists()) then request:get-parameter('localConceptsOnly','true') else ('true')

let $maxResults             := if (request:exists()) then (request:get-parameter('max',())) else ()
let $maxResults             := 
    if ($maxResults castable as xs:integer and xs:integer($maxResults)>0) then (xs:integer($maxResults)) else 50

return
adsearch:searchConcept($projectPrefix, $searchTerms, $maxResults, (), (), $datasetId, $datasetEffectiveDate, $conceptType, $statusCodes, $originalOnly='true', $localConceptsOnly='true', $searchPrefix)