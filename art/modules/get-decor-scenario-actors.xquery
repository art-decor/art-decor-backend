xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art = "http://art-decor.org/ns/art" at "art-decor.xqm";
import module namespace decor = "http://art-decor.org/ns/decor" at "../api-decor.xqm";
declare namespace request = "http://exist-db.org/xquery/request";

let $project        := request:get-parameter('project','')
(:let $project      := 'demo1-':)
let $decorScenarios := $get:colDecorData//decor[project/@prefix=$project]//scenarios
let $isauthor       := try { decor:authorCanEditP($decorScenarios/ancestor::decor, $decor:SECTION-SCENARIOS) } catch * {false()}

return
    <actors>
    {
        for $actor in $decorScenarios/actors/actor
        return
        <actor id="{$actor/@id}" type="{$actor/@type}">
        {
            for $name in $actor/name
            return
            art:serializeNode($name)
            ,
            if ($isauthor) then
                for $lang in $actor/ancestor::decor/project/name/@language
                return
                    if ($actor/name[@language = $lang]) then () else (
                        <name language="{$lang}"/>
                    )
            else ()
            ,
            for $desc in $actor/desc
            return
            art:serializeNode($desc)
            ,
            if ($isauthor) then
                for $lang in $actor/ancestor::decor/project/name/@language
                return
                    if ($actor/desc[@language = $lang]) then () else (
                        <desc language="{$lang}"/>
                    )
            else ()
        }
        </actor>
    }
    </actors>
