xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

(:
   Xquery for deleting template ref
   Input: post of template element:
   <template projectPrefix="demo1-" ref="2.16.840.1.113883.1.10.1234" name="x" displayName="y"/>
:)
import module namespace get             = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art             = "http://art-decor.org/ns/art" at "art-decor.xqm";
import module namespace decor           = "http://art-decor.org/ns/decor" at "../api/api-decor.xqm";

let $projectPrefix      := request:get-parameter('prefix','')
let $ref                := request:get-parameter('ref','')

(:get decor file:)
let $decor                  := if (string-length($projectPrefix)=0) then () else (art:getDecorByPrefix($projectPrefix))
let $templateRef            := $decor//template[@ref = $ref]
(: get templateAssociations without concepts for this ref :)
let $templateAssociations   := art:getTemplateAssociationsP($decor, $ref, ())[not(concept)]

let $response           :=
    if ($decor and decor:authorCanEditP($decor, $decor:SECTION-RULES)) then (
        update delete $templateRef,
        update delete $templateAssociations,
        <response>OK</response>
    ) else (
        <response>NO PERMISSION</response>
    )
   
return
    $response