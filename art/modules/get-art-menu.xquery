xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(: PATH MUST BE ABSOLUTE BECAUSE THE CONTEXT IS THE APPLY-RULES STYLESHEET:)
import module namespace art         = "http://art-decor.org/ns/art" at "xmldb:exist:///db/apps/art/modules/art-decor.xqm";

let $menufile           := if (request:exists()) then request:get-parameter('menufile',()) else ()

return art:getArtMenu($menufile)