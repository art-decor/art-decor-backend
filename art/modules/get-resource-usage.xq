xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get     = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
declare namespace xhtml         = "http://www.w3.org/1999/xhtml";
declare namespace xsl           = "http://www.w3.org/1999/XSL/Transform";

let $packageRoot    := if (request:exists()) then request:get-parameter('packageRoot','art') else 'art'
let $resource       := if (request:exists()) then request:get-parameter('resource',()) else ()
let $forms          := 
    collection(concat($get:root,$packageRoot,'/xforms'))/xhtml:html | 
    collection(concat($get:root,$packageRoot,'/resources/stylesheets'))/xsl:stylesheet

let $fullPath       := concat('$resources/',$resource)

return
<usage>
{
    if (empty($resource)) then () else (
        for $form in $forms
        let $count  := count( 
            $form//@ref[contains(., $fullPath)] | 
            $form//@title[contains(., $fullPath)] |  
            $form//@value[contains(., $fullPath)] | 
            $form//xsl:attribute[contains(., $fullPath)] 
        )
        return
            if ($count gt 0) then 
                <xform name="{util:document-name($form)}" count="{$count}"/>
            else ()
        ,
        if ($packageRoot = 'art') then (
            for $form in collection(concat($get:root,$packageRoot,'/resources'))/menu
            let $count  := count( $form//@label[. = $resource] )
            return
                if ($count gt 0) then 
                    <xform name="{util:document-name($form)}" count="{$count}"/>
                else ()
        ) else ()
    )
}
</usage>


