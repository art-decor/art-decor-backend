xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get             = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art             = "http://art-decor.org/ns/art" at "art-decor.xqm";
import module namespace decor           = "http://art-decor.org/ns/decor" at "../api/api-decor.xqm";

let $projectId                  := if (request:exists()) then request:get-parameter('projectId',())[not(.='')]          else ()
(:let $projectId                := '2.16.840.1.113883.2.4.3.11.60.90':)
let $communityName              := if (request:exists()) then request:get-parameter('name',())[not(.='')]               else ()
let $breakLock                  := if (request:exists()) then xs:boolean(request:get-parameter('breakLock','false'))    else (false())
(:let $communityName    := 'prn':)

let $lock                       := decor:setLock($projectId, $communityName, (), $breakLock)
let $response                   :=
    if ($lock/self::true) then (
        let $community          := decor:getDecorCommunity($communityName, $projectId, false())
        let $prototypes         := 
            if ($community/prototype/@ref) then
                doc(xs:anyURI($community/prototype/@ref))/prototype
            else (
                $community/prototype
            )
        let $newLock            := $lock/node()
        
        return
        <community>
        {
            $community/@*,
            if (not($community/@displayName)) then
                attribute displayName {''}
            else()
        }
            <edit/>
        {
            $newLock,
            for $data in $community/desc
            return
                art:serializeNode($data)
            ,
            $community/access
        }
            <prototype>
            {
                $community/prototype/@ref,
                for $data in $prototypes/data
                return
                    <data>
                    {
                        $data/@*,
                        for $desc in $data/desc
                        return
                            art:serializeNode($desc)
                        ,
                        $data/enumValue
                    }
                    </data>
            }
            </prototype>
            <associations>
            {
                for $association in $community/associations/association
                return
                    <association>
                    {
                        $association/object,
                        for $data in $prototypes/data
                        let $existingData := $association/data[@type = $data/@type]
                        (:let $log := update insert <event>{$existingData}</event> into doc('/db/log.xml')/events:)
                        return
                        (: Make empty rows for data which isn't filled in yet; discard empty data on save :)
                        if ($existingData) then
                            for $e in $existingData return art:serializeNode($e)
                        else (
                            <data type="{$data/@type}"/>
                        )
                    }
                    </association>
            }
            </associations>
        </community>
    )
    else (
        <community>{$lock/node()}</community>
    )

return
$response