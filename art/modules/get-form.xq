xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get         = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace aduser      = "http://art-decor.org/ns/art-decor-users" at "../api/api-user-settings.xqm";
import module namespace adserver    = "http://art-decor.org/ns/art-decor-server" at "../api/api-server-settings.xqm";

declare namespace request       = "http://exist-db.org/xquery/request";
declare namespace session       = "http://exist-db.org/xquery/session";
declare namespace xhtml         = "http://www.w3.org/1999/xhtml";

(: 
   Get form name and decor document prefix
   ! This needs to be changed to use a request parameter for the decor project prefix
:)
let $fullForm := if (request:exists()) then request:get-parameter('form','home') else ('')

let $formName := 
    if (contains($fullForm,'--')) then
        substring-before($fullForm,'--')
    else (
        $fullForm
    )

let $document := 
    if (contains($fullForm,'--')) then
        substring-after($fullForm,'--')
    else (
        ''
    )


(: Get user info for access control, user preferences and display :)
let $user               := get:strCurrentUserName()
let $userDisplayName    := try { aduser:getUserDisplayName($user) } catch * {()}
let $userDisplayName    := if ($userDisplayName[not(.='')]) then $userDisplayName else $user

let $groups             := sm:get-user-groups($user)

(: get package list and create lookup list for xforms :)
(:let $resourcesList    := collection($get:root)//artXformResources:)
(:let $fullFormPath     :=   
    for $resources in $resourcesList
    let $root := substring-before(util:collection-name($resources),'/resources')
    let $list := 
        for $form in collection(concat($root,'/xforms'))//xhtml:html
        return
            <form name="{substring-before(util:document-name($form),'.xhtml')}" path="{concat(util:collection-name($form),'/',util:document-name($form))}"/>
    return
        $list:)
let $fullFormPath := 
    for $form in collection($get:root)//xhtml:html[ends-with(util:collection-name(.),'/xforms')]
    return
        <form name="{substring-before(util:document-name($form),'.xhtml')}" path="{concat(util:collection-name($form),'/',util:document-name($form))}"/>


let $form               := doc($fullFormPath[@name=$formName]/@path)
let $xsltParameters     :=
    <parameters>
        <param name="current-application" value="{$formName}"/>
        <param name="user" value="{$userDisplayName}"/>
        <param name="group" value="{string-join($groups,'|')}"/>
        <param name="document" value="{$document}"/>
        <param name="cameFromUri" value="{$fullForm}"/>
        <param name="strMenuFilePath" value="{adserver:getServerMenuTemplate()}"/>
        <param name="defaultLogo" value="{adserver:getServerLogo()}"/>
        <param name="defaultHref" value="{adserver:getServerLogoUrl()}"/>
        <param name="orbeonVersion" value="{adserver:getServerOrbeonVersion()}"/>
    </parameters>

let $xformStylesheet := adserver:getServerXSLArt()
let $xformStylesheet := if (string-length($xformStylesheet)=0) then 'apply-rules.xsl' else ($xformStylesheet)

return
(:$form:)
transform:transform($form, xs:anyURI(concat('xmldb:exist://',$get:strArtResources,'/stylesheets/',$xformStylesheet)), $xsltParameters)