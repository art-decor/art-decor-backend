xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
(:~ Return the list of available server projects, wrapped in an element <projects/>. Optionally BBRs may be skipped, and private/experimental projects may be included.
:   Each project has all its normal attributes according to the DECOR schema, and additionally contains @experimental|@private|@repository from it <decor/> parent and 
:   @lastmodified (file based) and lastVersion (latest project/(release|version) date)
:   Each project includes elements <name/>, <desc/>, <author/> (current user only), <copyright/> (full) and <community name="" displayName="" projectId=""/> (for communities that the current user is an author for, no community contents)
:   Example:
:
:   <project id="2.16.840.1.113883.2.4.3.11.60.41" prefix="bgz2015-" defaultLanguage="nl-NL" experimental="false" repository="false" private="false" lastVersion="2016-06-12T00:00:00" lastmodified="2017-10-19T14:13:28.013+02:00">
:       <name language="nl-NL">Basisgegevensset Zorg 2015 (BgZ)</name>
:       <name language="en-US">Basic Health Data Set 2015 (BgZ)</name>
:       <desc language="nl-NL">De Basisgegevensset Zorg is ...</desc>
:       <desc language="en-US">The Basic Health Data Set is ...</desc>
:       <author id="2" username="alexander" email="henket@nictiz.nl" notifier="off">Alexander Henket</author>
:       <copyright by="Nictiz" logo="nictiz-logo.png" years="2016" type="author">
:           <addrLine type="uri">http://www.nictiz.nl</addrLine>
:       </copyright>
:       <copyright by="Registratie aan de bron" logo="logo_registratieaandebron.png" years="2016-" type="author">
:           <addrLine type="uri">https://www.registratieaandebron.nl</addrLine>
:           <addrLine type="uri">https://www.zibs.nl</addrLine>
:       </copyright>
:       <community name="ada" projectId="2.16.840.1.113883.2.4.3.11.60.41" displayName="ART-DECOR Applications"/>
:   </project>
:)
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art = "http://art-decor.org/ns/art" at "art-decor.xqm";
import module namespace decor = "http://art-decor.org/ns/decor" at "../api/api-decor.xqm";

(: 0..1 language determines the minimum project name/description that should be present on each. If not available then the first name/description will be copied into this language :)
let $language               := if (request:exists()) then request:get-parameter('language', ())[string-length() > 0] else ()
(: 0..1 true (default) or false. If true will include BBR projects in the return :)
let $includeRepository      := if (request:exists()) then request:get-parameter('includeRepository', 'true') = 'true' else ()
(: 0..1 true or false (default). If true will include private projects in the return :)
let $includePrivate         := if (request:exists()) then request:get-parameter('includePrivate', 'false') = 'true' else ()
(: 0..1 true or false (default). If true will include experimental projects in the return :)
let $includeExperimental    := if (request:exists()) then request:get-parameter('includeExperimental', 'false') = 'true' else ()

let $user                   := if (string-length(get:strCurrentUserName()) = 0) then 'guest' else get:strCurrentUserName()

let $decors                 :=
    if ($includeRepository) then 
        $get:colDecorData/decor 
    else 
        $get:colDecorData/decor[not(@repository = 'true')]

let $decors                 :=
    if ($includePrivate) then 
        $decors
    else 
        $decors[not(@private = 'true')]

let $projects               := 
    if ($includeExperimental) then 
        $decors/project 
    else 
        $decors/project[not(@experimental = 'true')]

return
    <projects>
    {
        for $project in $projects
        return
            <project>
            {
                $project/(@id | @prefix | @defaultLanguage),
                attribute experimental {$project/@experimental = 'true'},
                attribute repository {$project/parent::decor/@repository = 'true'},
                attribute private {$project/parent::decor/@private = 'true'},
                attribute lastVersion {max($project/(release | version)/xs:dateTime(@date))},
                attribute lastmodified {xmldb:last-modified(util:collection-name($project), util:document-name($project))}
            }
            {
                if (empty($language)) then () else if ($project/name[@language = $language]) then () else (
                    <name language="{$language}">{$project/name[1]/node()}</name>
                )
                ,
                $project/name
                ,
                if (empty($language)) then () else if ($project/desc[@language = $language]) then () else (
                    <desc language="{$language}">{art:serializeNode($project/desc[1])/node()}</desc>
                )
                ,
                for $node in $project/desc
                return
                    art:serializeNode($node)
                ,
                $project/copyright,
                $project/license,
                $project/author[@username = $user],
                
                (: --------- community collections --------- :)
                for $c in decor:getDecorCommunitiesForCurrentUser($project/@id)
                return
                    <community>{$c/@*}</community>
            }
            </project>
    }
    </projects>