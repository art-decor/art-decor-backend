xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace artx ="http://art-decor.org/ns/art/xpath" at  "art-decor-xpath.xqm";

declare namespace request   = "http://exist-db.org/xquery/request";
declare namespace response  = "http://exist-db.org/xquery/response";
declare namespace hl7       = "urn:hl7-org:v3";
declare namespace util      = 'http://exist-db.org/xquery/util';
declare namespace xis       = "http://art-decor.org/ns/xis";
declare namespace xdb       = "http://exist-db.org/xquery/xmldb";
declare option exist:serialize "indent=no";
declare option exist:serialize "omit-xml-declaration=no";

let $nl         := "&#10;"
let $tab        := "&#9;"
let $collection := $get:colDecorData

let $projectPrefix  := if (request:exists()) then request:get-parameter('prefix','') else 'peri20-' 
let $dataset        := if (request:exists()) then request:get-parameter('dataset','') else '2.16.840.1.113883.2.4.3.11.60.90.77.1.5'
let $lang           := if (request:exists()) then request:get-parameter('lang','nl-NL') else 'nl-NL' 
let $version        := if (request:exists()) then request:get-parameter('version','') else '2014-02-17T14:55:13'

let $decor := 
    if ($version)
    then $get:colDecorVersion//decor[project[@prefix=$projectPrefix]][@versionDate=$version][empty($lang) or @language=$lang][1]
    else $get:colDecorData//decor[project[@prefix=$projectPrefix]]

let $representingTemplates := 
    if ($dataset = '') then $decor//representingTemplate else $decor//representingTemplate[@sourceDataset=$dataset]

let $allXpaths :=
    <xpaths status="draft" version="{max($decor/project/(version|release)/xs:dateTime(@date))}" generated="{current-dateTime()}">{
        for $representingTemplate in $representingTemplates
        return artx:getXpaths($decor, $representingTemplate)
    }</xpaths>
    
let $path := concat('xmldb:exist://', util:collection-name($decor), '/')
let $runtimedir := 'resources/'
let $dummy := if (not(xmldb:collection-available(concat($path, $runtimedir))))
    then xmldb:create-collection($path, $runtimedir) 
    else ()
    
let $xpathFile := 
    if (empty(doc(concat($path, $runtimedir, $projectPrefix, 'xpaths.xml')))) 
    then xdb:store(concat($path, $runtimedir), concat($projectPrefix, 'xpaths.xml'), <projectXpaths/>)
    else concat($path, $runtimedir, $projectPrefix, 'xpaths.xml')
let $update := update insert $allXpaths into doc($xpathFile)/projectXpaths
(:let $xpath-file := xdb:store(concat($path, $runtimedir), concat($projectPrefix, 'xpaths.xml'), $allXpaths, '.xml'):) 
return $allXpaths