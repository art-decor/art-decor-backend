xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace get ="http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art ="http://art-decor.org/ns/art" at "art-decor.xqm";

declare namespace xmldb="http://exist-db.org/xquery/xmldb";
declare namespace util="http://exist-db.org/xquery/util";

let $timeStamp := current-dateTime()
let $urgentnewslocation := concat($get:strArtData, '/', 'urgentnews.xml')
let $urgentnews := doc($urgentnewslocation)
let $result := 
    <r>
        {
            for $un in $urgentnews//news
            let $d := if ($un/@showuntil castable as xs:dateTime) then xs:dateTime($un/@showuntil) else xs:dateTime(0)
            return if ($d > $timeStamp) then $un else ()
        }
    </r>
let $cnt := count ($result/*)

return
    if ($cnt = 0) then
        <urgentnews status="NONE" count="0" time="{$timeStamp}"/>
    else
        <urgentnews status="OK" count="{$cnt}" time="{$timeStamp}">
        {   
            for $x in $result/news
            return
                <news>
                {
                    $x/@*,
                    for $t in $x/text
                    return art:serializeNode($t)
                }
                </news>
        }
        </urgentnews>