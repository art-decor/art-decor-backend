xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace get = "http://art-decor.org/ns/art-decor-settings" at "art-decor-settings.xqm";
import module namespace art = "http://art-decor.org/ns/art" at "art-decor.xqm";
declare namespace request       = "http://exist-db.org/xquery/request";

let $projectPrefix      := if (request:exists()) then (request:get-parameter('project',())) else ()
let $language           := if (request:exists()) then (request:get-parameter('language',())) else ()
(:
    <labels>
        <label code="" name="" color="">
            <desc language="en-US">....</desc>
        </label>
    </labels>
:)
let $labels             := $get:colDecorData//decor[project/@prefix=$projectPrefix]/issues/labels

return
    <labels>
    {
        for $label in $labels/label
        return
            <label code="{$label/@code}" name="{$label/@name}" color="{$label/@color}">
            {
                for $node in $label/desc
                return
                    art:serializeNode($node)
            }
            </label>
    }
    </labels>