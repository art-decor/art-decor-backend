xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)

import module namespace adserver = "http://art-decor.org/ns/art-decor-server" at "../api/api-server-settings.xqm";

let $baseurl    := adserver:getServerURLFhirServices()

return
    <endpoints base="{$baseurl}">
    {
        for $endpoint in adserver:getInstalledFhirServices()
        order by $endpoint
        return 
            <endpoint>
            {
                switch ($endpoint)
                case '1.0' return attribute name {'DSTU2'}
                case 'dstu2' return attribute name {'DSTU2'}
                case '3.0' return attribute name {'STU3'}
                case 'stu3' return attribute name {'STU3'}
                default return (
                    if (matches($endpoint, '^\d+\.\d+(\.\d+)?$')) then
                        attribute name {concat('R', substring-before($endpoint, '.'))}
                    else (
                        attribute name {$endpoint}
                    )
                )
                ,
                $endpoint
            }
            </endpoint>
    }
    </endpoints>