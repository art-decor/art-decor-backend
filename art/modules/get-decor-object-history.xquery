xquery version "1.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace hist     = "http://art-decor.org/ns/decor/history" at "../api/api-decor-history.xqm";

declare namespace request        = "http://exist-db.org/xquery/request";

let $artefact       := if (request:exists()) then (request:get-parameter('artefact','')) else ()
let $prefix         := if (request:exists()) then (request:get-parameter('prefix',())) else ()
let $id             := if (request:exists()) then (request:get-parameter('id','')) else ()
let $effectiveDate  := if (request:exists()) then (request:get-parameter('effectiveDate','')) else ()

return
    if (string-length($artefact) gt 0 and string-length($prefix) gt 0)
    then hist:ListHistory($artefact, $prefix, $id, $effectiveDate)
    else (
        <histories>
        {
            if (empty($artefact)) then () else attribute artefactType {$artefact},
            if (empty($prefix)) then () else attribute projectPrefix {$prefix},
            if (empty($id)) then () else attribute id {$id},
            if (empty($effectiveDate)) then () else attribute effectiveDate {$effectiveDate}
        }
        </histories>
    )