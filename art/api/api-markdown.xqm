xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
module namespace md                 = "http://art-decor.org/ns/art/markdown";

declare namespace output    = "http://www.w3.org/2010/xslt-xquery-serialization";

declare variable $md:CR   := '&#xD;';
declare variable $md:LF   := '&#xA;';
declare variable $md:CRLF := '&#xD;&#xA;';

declare
    %output:media-type("text/plain")
    %output:method("text")
function md:html2markdown($html as item()*) {
    let $t := md:typeswitcher($html, $md:LF)
    (: some string based corrections and return :)
    let $u := replace($t, '^\s+|\s+$', '')
    return replace($u, '\*\s\*\*|\*\*\s\*', '***')
};

declare %private function md:typeswitcher($node as item()*, $newline as xs:string) {
string-join(
for $child in $node
return
    typeswitch ($child)
    case element() return
        switch (local-name($child))
        case 'div' return concat($newline, $newline, md:typeswitcher($child/node(),$newline), $newline, $newline)
        case 'p' return if ($child/parent::li) then md:typeswitcher($child/node(),$newline) else concat($newline, md:typeswitcher($child/node(),$newline), $newline)
        case 'span' return md:typeswitcher($child/node(),$newline)
        case 'ul' return concat($newline, md:typeswitcher($child/(li | caption),$newline), $newline)
        case 'ol' return concat($newline, md:typeswitcher($child/(li | caption),$newline), $newline)
        case 'li' return concat(string-join(for $i in (1 to count($child/ancestor::li)) return '    ', ''), if ($child/parent::ol) then '1. ' else '* ', md:typeswitcher($child/node(),$newline), $newline)
        case 'em'
        case 'i' return concat('*', md:typeswitcher($child/node(),$newline), '*')
        case 'b' 
        case 'strong' return concat('**', md:typeswitcher($child/node(),$newline), '**')
        case 'a' return concat('[', md:typeswitcher($child/node(),$newline), '](', $child/@href,  if ($child[@title]) then concat(' "', replace($child/@title, '"', '&#39;'), '"') else (), ')')
        case 'br' return $newline
        case 'h1' return concat($newline, '# ', md:typeswitcher($child/node(),$newline), $newline)
        case 'h2' return concat($newline, '## ', md:typeswitcher($child/node(),$newline), $newline)
        case 'h3' return concat($newline, '### ', md:typeswitcher($child/node(),$newline), $newline)
        case 'h4' return concat($newline, '#### ', md:typeswitcher($child/node(),$newline), $newline)
        case 'h5' return concat($newline, '##### ', md:typeswitcher($child/node(),$newline), $newline)
        case 'h6' return concat($newline, '###### ', md:typeswitcher($child/node(),$newline), $newline)
        case 'h7' return concat($newline, '###### ', md:typeswitcher($child/node(),$newline), $newline)
        case 'h8' return concat($newline, '###### ', md:typeswitcher($child/node(),$newline), $newline)
        case 'h9' return concat($newline, '###### ', md:typeswitcher($child/node(),$newline), $newline)
        case 'pre' return concat($newline, '```', $newline, md:typeswitcher($child/node(),$newline), $newline, '```', $newline)
        case 'blockquote' return concat($newline, '> ', md:typeswitcher($child/node(),$newline), $newline)
        case 'hr' return concat($newline, '----------', $newline)
        case 'table' return concat($newline, $newline, md:typeswitcher($child/(caption, thead, tbody, tfoot, tr),$newline), $newline, $newline)
        case 'caption' return concat($newline, '**', md:typeswitcher($child/node(),$newline), '**', $newline)
        case 'thead' return md:typeswitcher($child/tr,$newline)
        case 'tbody' return md:typeswitcher($child/tr,$newline)
        case 'tfoot' return md:typeswitcher($child/tr,$newline)
        case 'tr' return concat('| ', string-join(for $c in $child/(th | td) return md:typeswitcher($c,$newline), ' | '), ' |', $newline,
                                      if ($child[th][empty(following-sibling::tr[th])]) then concat('|', string-join(for $i in (1 to count($child/(th | td))) return '---|', ''), $newline) else ())
        case 'th' return md:typeswitcher($child/node(),$newline)
        case 'td' return md:typeswitcher($child/node(),$newline)
        default return (
            let $startmarker     := if ($child[matches(parent::*/@style, 'font-weight:\s*bold')]) then '**' else ()
            let $startmarker     := if ($child[matches(parent::*/@style, 'font-style:\s*italic')]) then concat('*', $startmarker) else $startmarker
            let $startmarker     := if ($child[matches(parent::*/@style, 'text-decoration:\s*line-through')]) then concat('~~', $startmarker) else $startmarker
            
            return concat($startmarker, $child, string-join(codepoints-to-string(reverse(string-to-codepoints($startmarker))), ''))
        )
    case comment() return ()
    case text() return (
        let $startmarker     := if ($child[matches(parent::*/@style, 'font-weight:\s*bold')]) then '**' else ()
        let $startmarker     := if ($child[matches(parent::*/@style, 'font-style:\s*italic')]) then concat('*', $startmarker) else $startmarker
        let $startmarker     := if ($child[matches(parent::*/@style, 'text-decoration:\s*line-through')]) then concat('~~', $startmarker) else $startmarker
        
        let $child           := replace($child, $md:LF, ' ')

        return concat($startmarker, $child, string-join(codepoints-to-string(reverse(string-to-codepoints($startmarker))), ''))
    )
    default return md:typeswitcher($child/node(),$newline)
, '')
};

declare function md:minifyHTML($html as item()*) {
    (:minify xhtml: remove redundant (white-space) characters :)
    let $replace :=
        <replaces>
            <replace type="shorten multiple whitespace sequences; keep new-line characters because they matter in JS" a="/([\t ])+/s" b=" "/>
            <replace type="remove tabs after HTML tags" a="/\>[^\S ]+/s" b=">"/>
            <replace type="remove tabs before HTML tags" a="/[^\S ]+\&lt;/s" b="&lt;"/>
        </replaces>
        (:
        //remove tabs before and after HTML tags
        '/\>[^\S ]+/s'   => '>',
        '/[^\S ]+\</s'   => '<',
        //shorten multiple whitespace sequences; keep new-line characters because they matter in JS!!!
        '/([\t ])+/s'  => ' ',
        //remove leading and trailing spaces
        '/^([\t ])+/m' => '',
        '/([\t ])+$/m' => '',
        // remove JS line comments (simple only); do NOT remove lines containing URL (e.g. 'src="http://server.com/"')!!!
        '~//[a-zA-Z0-9 ]+$~m' => '',
        //remove empty lines (sequence of line-end and white-space characters)
        '/[\r\n]+([\t ]?[\r\n]+)+/s'  => "\n",
        //remove empty lines (between HTML tags); cannot remove just any line-end characters because in inline JS they can matter!
        '/\>[\r\n\t ]+\</s'    => '><',
        //remove "empty" lines containing only JS's block end character; join with next line (e.g. "}\n}\n</script>" --> "}}</script>"
        '/}[\r\n\t ]+/s'  => '}',
        '/}[\r\n\t ]+,[\r\n\t ]+/s'  => '},',
        //remove new-line after JS's function or condition start; join with next line
        '/\)[\r\n\t ]?{[\r\n\t ]+/s'  => '){',
        '/,[\r\n\t ]?{[\r\n\t ]+/s'  => ',{',
        //remove new-line after JS's line end (only most obvious and safe cases)
        '/\),[\r\n\t ]+/s'  => '),',
        //remove quotes from HTML attributes that does not contain spaces; keep quotes around URLs!
        '~([\r\n\t ])?([a-zA-Z0-9]+)="([a-zA-Z0-9_/\\-]+)"([\r\n\t ])?~s' => '$1$2=$3$4', //$1 and $4 insert first white-space character found before/after attribute
    );
    $body = preg_replace(array_keys($replace), array_values($replace), $body);
    :)
    for $c in $html/node()
    return 
        typeswitch ($c)
            case text() return (
                let $c := replace($c, '&#xA;', ' ')
                let $c := replace($c, '^([\t ])+', ' ')
                let $c := replace($c, '^\s+|\s+$', '')
                return <span>{$c}</span>
             )
            default return (
                element { local-name($c) } {
                    $c/@*,
                    md:minifyHTML($c)
                }
            )
};