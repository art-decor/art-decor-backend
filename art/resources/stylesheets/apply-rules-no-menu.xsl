<!--
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.

    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
-->
<xsl:stylesheet xmlns:f="http://orbeon.org/oxf/xml/formatting" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:ev="http://www.w3.org/2001/xml-events" xmlns:xforms="http://www.w3.org/2002/xforms" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:atp="urn:nictiz.atp" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fr="http://orbeon.org/oxf/xml/form-runner" xmlns:xxforms="http://orbeon.org/oxf/xml/xforms" xmlns:widget="http://orbeon.org/oxf/xml/widget" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="2.0">
    <xsl:output method="xml"/>
    <xsl:strip-space elements="*"/>
   <!-- dummy parameter for 'touching' file to cause xinclude refresh -->
    <xsl:param name="lastRefresh" select="'2015-07-02T14:59:58.086+02:00'"/>
   <!-- parameter for user, default value is 'guest' -->
    <xsl:param name="user" select="'guest'"/>
   <!-- parameter for group, default value is 'guest' -->
    <xsl:param name="group" select="'guest'"/>
   <!-- parameter for document, default value is empty' -->
    <xsl:param name="document" select="''"/>
   <!-- parameter for current application, used to highlight current app in menu -->
    <xsl:param name="current-application" select="'actors'" as="xs:string"/>
   <!-- parameter for the URL the user came from when he clicked Login -->
    <xsl:param name="cameFromUri"/>
   <!-- parameter for art/resources path -->
    <xsl:param name="strArt" select="'/db/apps/art'"/>
   <!-- parameter for art-data path -->
    <xsl:param name="strArt-data" select="'/db/apps/art-data'"/>
   <!-- parameter for default logo -->
    <xsl:param name="defaultLogo" select="'art-decor-logo40.png'"/>
   <!-- parameter for default url on logo -->
    <xsl:param name="defaultHref" select="'https://art-decor.org'"/>
   <!-- parameter for the right orbeon version -->
    <xsl:param name="orbeonVersion" select="'3.9'"/>
   <!-- application menu  -->
    <xsl:variable name="isDecor" select="$current-application=('decor-project','decor-development','decor-datasets','decor-scenarios','decor-transaction-editor','decor-codesystems','decor-codesystem-editor','decor-codesystem-ids','decor-terminology','decor-valuesets','decor-valueset-editor','decor-valueset-ids','decor-templates','decor-template-editor','decor-template-mapping','decor-template-ids','decor-issues','decor-mycommunity')"/>
    <!-- in some cases the $document variable should have been a prefix with hyphen, but the hyphen was 'forgotten' -->
    <xsl:variable name="documentChecked">
        <xsl:choose>
            <xsl:when test="$isDecor and string-length($document)&gt;0 and not(ends-with($document,'-'))">
                <xsl:value-of select="concat($document,'-')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$document"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="artdecorCss">
        <xi:include href="/db/apps/art/resources/stylesheets/common-decor-style.xml">
            <xi:fallback>
                <xhtml:meta name="missing-file" content="Included document /db/apps/art/resources/stylesheets/common-decor-style.xml not found!"/>
            </xi:fallback>
        </xi:include>
        <xsl:choose>
            <xsl:when test="$orbeonVersion = '3.9'">
                <xi:include href="/db/apps/art/resources/stylesheets/orbeon-3.9-tree-style.xml">
                    <xi:fallback>
                        <xhtml:meta name="missing-file" content="Included document /db/apps/art/resources/stylesheets/orbeon-3.9-tree-style.xml not found!"/>
                    </xi:fallback>
                </xi:include>
            </xsl:when>
            <xsl:when test="$orbeonVersion = '2017'">
                <xi:include href="/db/apps/art/resources/stylesheets/orbeon-2017-tree-style.xml">
                    <xi:fallback>
                        <xhtml:meta name="missing-file" content="Included document /db/apps/art/resources/stylesheets/orbeon-2017-tree-style.xml not found!"/>
                    </xi:fallback>
                </xi:include>
                <xhtml:style type="text/css" media="all">
                    .thebigreasonSkin table.mceLayout { width: 100% !important; }
                </xhtml:style>
            </xsl:when>
            <xsl:otherwise>
                <xi:include href="/db/apps/art/resources/stylesheets/orbeon-2018-tree-style.xml">
                    <xi:fallback>
                        <xhtml:meta name="missing-file" content="Included document /db/apps/art/resources/stylesheets/orbeon-2018-tree-style.xml not found!"/>
                    </xi:fallback>
                </xi:include>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="artDecorJs">
        <xsl:choose>
            <!-- Deprecated Orbeon 3.9 YUI editor -->
            <xsl:when test="$orbeonVersion = '3.9'">
                <xhtml:script type="text/javascript">
                    var YUI_RTE_CUSTOM_CONFIG = {
                        height: '150px',
                        width: '100%',
                        toolbar: {
                            titlebar: false,
                            buttons:[ 
                                { group: 'textstyle', label: '', buttons:[ 
                                    { type: 'push', label: 'Bold', value: 'bold' }, 
                                    { type: 'push', label: 'Italic', value: 'italic' }, 
                                    { type: 'push', label: 'Underline', value: 'underline' }, 
                                    { type: 'separator' }, 
                                    { type: 'push', label: 'Subscript', value: 'subscript', disabled: true }, 
                                    { type: 'push', label: 'Superscript', value: 'superscript', disabled: true }, 
                                    { type: 'separator' }, 
                                    { type: 'push', label: 'Indent', value: 'indent', disabled: true }, 
                                    { type: 'push', label: 'Outdent', value: 'outdent', disabled: true }, 
                                    { type: 'push', label: 'Create an Unordered List', value: 'insertunorderedlist' }, 
                                    { type: 'push', label: 'Create an Ordered List', value: 'insertorderedlist' }
                                ] }, 
                                { type: 'separator' }, 
                                { 
                                    group: 'indentlist2', label: '', buttons:[ 
                                        { type: 'push', label: 'Remove Formatting', value: 'removeformat', disabled: true }, 
                                        { type: 'push', label: 'Undo', value: 'undo', disabled: true }, 
                                        { type: 'push', label: 'Redo', value: 'redo', disabled: true }
                                    ]
                                }, 
                                { type: 'separator' }, 
                                { 
                                    group: 'insertitem', label: '', buttons:[ 
                                        { type: 'push', label: 'Insert Image', value: 'insertimage' }, 
                                        { type: 'push', label: 'HTML Link CTRL + SHIFT + L', value: 'createlink', disabled: true }
                                    ]
                                }
                            ]
                        }
                    }
                </xhtml:script>
            </xsl:when>
            <!-- TinyMCE 3 editor until Orbeon 2018.1 -->
            <xsl:when test="$orbeonVersion = '2017'">
                <xhtml:script type="text/javascript"><![CDATA[
                    var TINYMCE_CUSTOM_CONFIG = {
                        language:                   "en",
                        selector:                   "textarea",  // change this value according to your HTML
                        skin:                       "thebigreason",
                        mode:                       "exact",
                        plugins:                    "spellchecker,style,table,save,iespell,paste,visualchars,nonbreaking,xhtmlxtras,template,fullscreen",
                        theme_advanced_buttons1:    "bold,italic,underline,|,sub,sup,|,outdent,indent,|,bullist,numlist,|,removeformat,undo,redo,|,link,unlink,|,fullscreen",
                        gecko_spellcheck:           true,
                        encoding:                   "xml",
                        entity_encoding:            "raw",
                        forced_root_block:          'div',
                        remove_redundant_brs:       true,
                        verify_html:                true,
                        editor_css:                 ""      // don't let the editor load UI CSS because that fails in portlets
                    };
                ]]></xhtml:script>
            </xsl:when>
            <!-- TinyMCE 4 editor from Orbeon 2018.1 -->
            <xsl:otherwise>
                <xhtml:script type="text/javascript"><![CDATA[
                    var TINYMCE_CUSTOM_CONFIG = {
                        language:                   "en",
                        selector:                   "textarea",  // change this value according to your HTML
                        skin:                       "lightgray",
                        menu:                       {},
                        plugins:                    "spellchecker table save paste lists link visualchars nonbreaking template fullscreen",
                        toolbar:                    "undo redo | styleselect | bold italic underline | outdent indent | alignleft aligncenter alignright | bullist numlist | removeformat | link unlink | fullscreen",
                        encoding:                   "xml",
                        entity_encoding:            "raw",
                        forced_root_block:          'div',
                        remove_redundant_brs:       true,
                        verify_html:                true,
                        branding:                   false,
                        editor_css:                 "",      // don't let the editor load UI CSS because that fails in portlets
                        content_style:              "* {font-family: Arial, Helvetica, Geneva, sans-serif; font-size: 12px;}",
                        height:                     300,
                        max_height:                 500,
                        resize:                     true
                    };
                ]]></xhtml:script>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <!-- **** READ LOCAL EXTRAS LIKE GOOGLE ANALYTICS **** -->
    <xsl:variable name="headerExtras">
        <xi:include href="/db/apps/art-data/resources/local-header-resources.xml">
            <xi:fallback>
                <xhtml:meta name="missing-file" content="Included document /db/apps/art-data/resources/local-header-resources.xml not found!"/>
            </xi:fallback>
        </xi:include>
    </xsl:variable>
    <xsl:template match="/">
        <xhtml:html>
            <xsl:apply-templates select="/xhtml:html/@*"/>
            <xhtml:head>
                <xsl:apply-templates select="/xhtml:html/xhtml:head/@*"/>
                <xhtml:meta name="robots" content="noindex, nofollow"/>
                <xsl:apply-templates select="/xhtml:html/xhtml:head/xhtml:meta[not(@name = 'robots')]"/>
                <!--  -->
                <xsl:comment> Scripts... </xsl:comment>
                <xsl:copy-of select="$artDecorJs"/>
                <xsl:if test="$headerExtras//xhtml:script">
                    <xsl:comment> ***** LOCAL EXTRAS ***** </xsl:comment>
                    <xsl:copy-of select="$headerExtras//xhtml:script"/>
                </xsl:if>
                <!--  -->
                <xsl:comment> Style... </xsl:comment>
                <xsl:copy-of select="$artdecorCss"/>
                <xsl:if test="$headerExtras//xhtml:style">
                    <xsl:comment> ***** LOCAL EXTRAS ***** </xsl:comment>
                    <xsl:copy-of select="$headerExtras//xhtml:style"/>
                </xsl:if>
                <!--  -->
                <xsl:comment> Original form head content </xsl:comment>
                <xsl:apply-templates select="/xhtml:html/xhtml:head/(node() except xhtml:meta)"/>
            </xhtml:head>
            <xhtml:body>
                <!-- Copy body attributes -->
                <xsl:apply-templates select="/xhtml:html/xhtml:body/@*"/>
                <xhtml:table id="maincontent" width="100%" style="background: transparent;">
                    <!-- row with page heading and clickable logo -->
                    <xhtml:tr>
                        <!-- login and language -->
                        <xhtml:td align="right" style="margin:0;padding:0;vertical-align:text-bottom;">
                            <xsl:if test="lower-case($user)!='guest'">
                                <xforms:output ref="$resources/logged-in-as"/>
                                <xsl:text>: </xsl:text>
                                <xhtml:a href="/user-settings" title="{{$resources/user-settings}}">
                                    <xsl:value-of select="$user"/>
                                    <xhtml:img alt="" style="display: inline; margin-left:4px;">
                                        <xsl:attribute name="src">/img/flags/{instance('language')/lower-case(substring(.,4,2))}.png</xsl:attribute>
                                        <xsl:attribute name="title">{$resources/@displayName}</xsl:attribute>
                                    </xhtml:img>
                                </xhtml:a>
                                <xsl:text> </xsl:text>
                                <xhtml:a href="/session/logout">
                                    <xforms:output ref="$resources/logout"/>
                                </xhtml:a>
                            </xsl:if>
                            <xsl:if test="lower-case($user)='guest'">
                                <xhtml:a href="/login?returnToUrl={encode-for-uri($cameFromUri)}">
                                    <xforms:output ref="$resources/login"/>
                                </xhtml:a>
                            </xsl:if>
                            <!-- Only in multi language 'applications' are flags needed. -->
                            <xsl:if test="$isDecor">
                                <!-- Lists supported languages in the project based on project/name, with displayName if possible (taken from $resources) -->
                                <xforms:select1 ref="instance('language')" class="auto-width" id="content-language-select">
                                    <xforms:itemset nodeset="instance('project-instance')/name[@language][not(@language=instance('resources-instance')/resources/@xml:lang)]">
                                        <xforms:label ref="@language"/>
                                        <xforms:value ref="@language"/>
                                    </xforms:itemset>
                                    <xforms:itemset nodeset="instance('resources-instance')/resources[@xml:lang=instance('project-instance')/name/@language]">
                                        <xforms:label ref="@displayName"/>
                                        <xforms:value ref="@xml:lang"/>
                                    </xforms:itemset>
                                    <!-- If we came from a project in a language that the current project does not have, we need that language in this list too, otherwise we may not be able to switch -->
                                    <xforms:itemset nodeset="instance('language')[not(.=instance('project-instance')/name/@language)]">
                                        <xforms:label ref="if ($resources/@displayName) then ($resources/@displayName) else (.)"/>
                                        <xforms:value ref="."/>
                                    </xforms:itemset>
                                </xforms:select1>
                                <xforms:action ev:observer="language" ev:event="xforms-insert xxforms-value-changed">
                                    <xforms:insert context="." origin="xxforms:set-session-attribute('language', instance('language'))"/>
                                </xforms:action>
                                <xhtml:img alt="" style="margin-right:0.5em;margin-left:0.5em;">
                                    <xsl:attribute name="src">/img/flags/{instance('language')/lower-case(substring(.,4,2))}.png</xsl:attribute>
                                    <xsl:attribute name="title">{$resources/@displayName}</xsl:attribute>
                                </xhtml:img>
                            </xsl:if>
                        </xhtml:td>
                    </xhtml:tr>
                    <xhtml:tr>
                        <xhtml:td class="form-content">
                            <xsl:apply-templates select="/xhtml:html/xhtml:body/*"/>
                            <!-- include xforms inspector if user is member of 'debug' group -->
                            <xsl:if test="tokenize($group,'\|')[. = 'debug']">
                                <fr:xforms-inspector/>
                            </xsl:if>
                        </xhtml:td>
                    </xhtml:tr>
                    <xhtml:tr>
                        <xhtml:td>
                            <xhtml:div style="float:right; padding-right:10px;">
                                <xforms:trigger appearance="minimal">
                                    <xforms:label>
                                        <xsl:attribute name="ref">$resources/about-legal-title</xsl:attribute>
                                    </xforms:label>
                                    <xforms:action ev:event="DOMActivate">
                                        <xforms:load resource="about-art-decor"/>
                                    </xforms:action>
                                </xforms:trigger>
                            </xhtml:div>
                        </xhtml:td>
                    </xhtml:tr>
                </xhtml:table>
            </xhtml:body>
        </xhtml:html>
    </xsl:template>

   <!-- match all elements and attributes, but not comment nodes -->
    <xsl:template match="@*|node()">
        <xsl:choose>
         <!-- insert requested document into document instance -->
            <xsl:when test="name(.)='xforms:instance' and @id='document' and string-length($documentChecked)&gt;1">
                <xforms:instance id="document">
                    <name>
                        <xsl:value-of select="$documentChecked"/>
                    </name>
                </xforms:instance>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:apply-templates select="@*|node()"/>
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>