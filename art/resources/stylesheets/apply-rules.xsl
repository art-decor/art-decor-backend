<!--
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.

    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
--><xsl:stylesheet xmlns:f="http://orbeon.org/oxf/xml/formatting" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:ev="http://www.w3.org/2001/xml-events" xmlns:xforms="http://www.w3.org/2002/xforms" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:atp="urn:nictiz.atp" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fr="http://orbeon.org/oxf/xml/form-runner" xmlns:xxforms="http://orbeon.org/oxf/xml/xforms" xmlns:widget="http://orbeon.org/oxf/xml/widget" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="2.0">
    <xsl:output method="xml"/>
    <xsl:strip-space elements="*"/>
   <!-- dummy parameter for 'touching' file to cause xinclude refresh -->
    <xsl:param name="lastRefresh" select="'2015-07-02T14:59:58.086+02:00'"/>
   <!-- parameter for user, default value is 'guest' -->
    <xsl:param name="user" select="'guest'"/>
   <!-- parameter for group, default value is 'guest' -->
    <xsl:param name="group" select="'guest'"/>
   <!-- parameter for document, default value is empty' -->
    <xsl:param name="document" select="''"/>
   <!-- parameter for current application, used to highlight current app in menu -->
    <xsl:param name="current-application" select="'actors'" as="xs:string"/>
   <!-- parameter for the URL the user came from when he clicked Login -->
    <xsl:param name="cameFromUri"/>
   <!-- parameter for art/resources path -->
    <xsl:param name="strArt" select="'/db/apps/art'"/>
   <!-- parameter for art-data path -->
    <xsl:param name="strArt-data" select="'/db/apps/art-data'"/>
    <!-- parameter for the right menu file expecting full path -->
    <xsl:param name="strMenuFilePath" select="'art-menu-template.xml'"/>
   <!-- parameter for default logo -->
    <xsl:param name="defaultLogo" select="'art-decor-logo40.png'"/>
   <!-- parameter for default url on logo -->
    <xsl:param name="defaultHref" select="'https://art-decor.org'"/>
   <!-- parameter for the right orbeon version -->
    <xsl:param name="orbeonVersion" select="'3.9'"/>
   <!-- application menu  -->
    <xsl:variable name="isDecor" select="$current-application=('decor-project','decor-development','decor-datasets','decor-scenarios','decor-transaction-editor','decor-codesystems','decor-codesystem-editor','decor-codesystem-ids','decor-terminology','decor-valuesets','decor-valueset-editor','decor-valueset-ids','decor-templates','decor-template-editor','decor-template-mapping','decor-template-ids','decor-issues','decor-mycommunity')"/>
    <!-- in some cases the $document variable should have been a prefix with hyphen, but the hyphen was 'forgotten' -->
    <xsl:variable name="documentChecked">
        <xsl:choose>
            <xsl:when test="$isDecor and string-length($document)&gt;0 and not(ends-with($document,'-'))">
                <xsl:value-of select="concat($document,'-')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$document"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="menuItems">
        <xsl:choose>
            <!-- show these when $isDecor -->
            <xsl:when test="$isDecor">('home','decor','project','project-overview','datasets','scenarios','decor-terminology','decor-templates','issues')</xsl:when>
            <!-- show these when not $isDecor -->
            <xsl:otherwise>('home','decor','demo','refsets','lab','terminology','testing','tools','application','thesauri')</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="artdecorCss">
        <xi:include href="/db/apps/art/resources/stylesheets/common-decor-style.xml">
            <xi:fallback>
                <xhtml:meta name="missing-file" content="Included document /db/apps/art/resources/stylesheets/common-decor-style.xml not found!"/>
            </xi:fallback>
        </xi:include>
        <xsl:choose>
            <xsl:when test="$orbeonVersion = '3.9'">
                <xi:include href="/db/apps/art/resources/stylesheets/orbeon-3.9-tree-style.xml">
                    <xi:fallback>
                        <xhtml:meta name="missing-file" content="Included document /db/apps/art/resources/stylesheets/orbeon-3.9-tree-style.xml not found!"/>
                    </xi:fallback>
                </xi:include>
            </xsl:when>
            <xsl:when test="$orbeonVersion = '2017'">
                <xi:include href="/db/apps/art/resources/stylesheets/orbeon-2017-tree-style.xml">
                    <xi:fallback>
                        <xhtml:meta name="missing-file" content="Included document /db/apps/art/resources/stylesheets/orbeon-2017-tree-style.xml not found!"/>
                    </xi:fallback>
                </xi:include>
                <xhtml:style type="text/css" media="all">
                    .thebigreasonSkin table.mceLayout { width: 100% !important; }
                </xhtml:style>
            </xsl:when>
            <xsl:when test="$orbeonVersion = '2018'">
                <xi:include href="/db/apps/art/resources/stylesheets/orbeon-2018-tree-style.xml">
                    <xi:fallback>
                        <xhtml:meta name="missing-file" content="Included document /db/apps/art/resources/stylesheets/orbeon-2018-tree-style.xml not found!"/>
                    </xi:fallback>
                </xi:include>
                <xhtml:style type="text/css" media="all">
                    .thebigreasonSkin table.mceLayout { width: 100% !important; }
                </xhtml:style>
            </xsl:when>
            <xsl:otherwise>
                <xi:include href="/db/apps/art/resources/stylesheets/orbeon-2019-tree-style.xml">
                    <xi:fallback>
                        <xhtml:meta name="missing-file" content="Included document /db/apps/art/resources/stylesheets/orbeon-2019-tree-style.xml not found!"/>
                    </xi:fallback>
                </xi:include>
                <xhtml:style type="text/css" media="all">
                    .thebigreasonSkin table.mceLayout { width: 100% !important; }
                </xhtml:style>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="artDecorJs">
        <xsl:choose>
            <!-- Deprecated Orbeon 3.9 YUI editor -->
            <xsl:when test="$orbeonVersion = '3.9'">
                <xhtml:script type="text/javascript">
                    var YUI_RTE_CUSTOM_CONFIG = {
                        height: '150px',
                        width: '100%',
                        toolbar: {
                            titlebar: false,
                            buttons:[ 
                                { group: 'textstyle', label: '', buttons:[ 
                                    { type: 'push', label: 'Bold', value: 'bold' }, 
                                    { type: 'push', label: 'Italic', value: 'italic' }, 
                                    { type: 'push', label: 'Underline', value: 'underline' }, 
                                    { type: 'separator' }, 
                                    { type: 'push', label: 'Subscript', value: 'subscript', disabled: true }, 
                                    { type: 'push', label: 'Superscript', value: 'superscript', disabled: true }, 
                                    { type: 'separator' }, 
                                    { type: 'push', label: 'Indent', value: 'indent', disabled: true }, 
                                    { type: 'push', label: 'Outdent', value: 'outdent', disabled: true }, 
                                    { type: 'push', label: 'Create an Unordered List', value: 'insertunorderedlist' }, 
                                    { type: 'push', label: 'Create an Ordered List', value: 'insertorderedlist' }
                                ] }, 
                                { type: 'separator' }, 
                                { 
                                    group: 'indentlist2', label: '', buttons:[ 
                                        { type: 'push', label: 'Remove Formatting', value: 'removeformat', disabled: true }, 
                                        { type: 'push', label: 'Undo', value: 'undo', disabled: true }, 
                                        { type: 'push', label: 'Redo', value: 'redo', disabled: true }
                                    ]
                                }, 
                                { type: 'separator' }, 
                                { 
                                    group: 'insertitem', label: '', buttons:[ 
                                        { type: 'push', label: 'Insert Image', value: 'insertimage' }, 
                                        { type: 'push', label: 'HTML Link CTRL + SHIFT + L', value: 'createlink', disabled: true }
                                    ]
                                }
                            ]
                        }
                    }
                </xhtml:script>
            </xsl:when>
            <!-- TinyMCE 3 editor until Orbeon 2018.1 -->
            <xsl:when test="$orbeonVersion = '2017'">
                <xhtml:script type="text/javascript"><![CDATA[
                    var TINYMCE_CUSTOM_CONFIG = {
                        language:                   "en",
                        selector:                   "textarea",  // change this value according to your HTML
                        skin:                       "thebigreason",
                        mode:                       "exact",
                        plugins:                    "spellchecker,style,table,save,iespell,paste,visualchars,nonbreaking,xhtmlxtras,template,fullscreen",
                        theme_advanced_buttons1:    "bold,italic,underline,|,sub,sup,|,outdent,indent,|,bullist,numlist,|,removeformat,undo,redo,|,link,unlink,|,fullscreen",
                        gecko_spellcheck:           true,
                        encoding:                   "xml",
                        entity_encoding:            "raw",
                        forced_root_block:          'div',
                        remove_redundant_brs:       true,
                        verify_html:                true,
                        editor_css:                 "",      // don't let the editor load UI CSS because that fails in portlets
                        relative_urls:              false,
                        remove_script_host:         false
                    };
                ]]></xhtml:script>
            </xsl:when>
            <!-- TinyMCE 4 editor from Orbeon 2018.1 -->
            <xsl:otherwise>
                <xhtml:script type="text/javascript"><![CDATA[
                    var TINYMCE_CUSTOM_CONFIG = {
                        language:                   "en",
                        selector:                   "textarea",  // change this value according to your HTML
                        skin:                       "lightgray",
                        menu:                       {},
                        plugins:                    "spellchecker table save paste lists link visualchars nonbreaking template fullscreen",
                        toolbar:                    "undo redo | styleselect | bold italic underline | outdent indent | alignleft aligncenter alignright | bullist numlist table | removeformat | link unlink | fullscreen",
                        menubar:                    "table",
                        table_toolbar:              "tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol",
                        encoding:                   "xml",
                        entity_encoding:            "raw",
                        forced_root_block:          'div',
                        remove_redundant_brs:       true,
                        verify_html:                true,
                        branding:                   false,
                        editor_css:                 "",      // don't let the editor load UI CSS because that fails in portlets
                        content_style:              "* {font-family: Arial, Helvetica, Geneva, sans-serif; font-size: 12px;}",
                        height:                     300,
                        max_height:                 500,
                        resize:                     true
                        relative_urls:              false,
                        remove_script_host:         false
                    };
                ]]></xhtml:script>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <!-- **** READ LOCAL EXTRAS LIKE GOOGLE ANALYTICS **** -->
    <xsl:variable name="headerExtras">
        <xi:include href="/db/apps/art-data/resources/local-header-resources.xml">
            <xi:fallback>
                <xhtml:meta name="missing-file" content="Included document /db/apps/art-data/resources/local-header-resources.xml not found!"/>
            </xi:fallback>
        </xi:include>
    </xsl:variable>
    <xsl:template match="/">
        <xhtml:html>
            <xsl:apply-templates select="/xhtml:html/@*"/>
            <xhtml:head>
                <xsl:apply-templates select="/xhtml:html/xhtml:head/@*"/>
                <xhtml:meta name="robots" content="noindex, nofollow"/>
                <xsl:apply-templates select="/xhtml:html/xhtml:head/xhtml:meta[not(@name = 'robots')]"/>
                <!--  -->
                <xsl:comment> Scripts... </xsl:comment>
                <xsl:copy-of select="$artDecorJs"/>
                <xsl:if test="$headerExtras//xhtml:script">
                    <xsl:comment> ***** LOCAL EXTRAS ***** </xsl:comment>
                    <xsl:copy-of select="$headerExtras//xhtml:script"/>
                </xsl:if>
                <!--  -->
                <xsl:comment> Style... </xsl:comment>
                <xsl:copy-of select="$artdecorCss"/>
                <xsl:if test="$headerExtras//xhtml:style">
                    <xsl:comment> ***** LOCAL EXTRAS ***** </xsl:comment>
                    <xsl:copy-of select="$headerExtras//xhtml:style"/>
                </xsl:if>
                <!--  -->
                <xsl:comment> Original form head content </xsl:comment>
                <xsl:apply-templates select="/xhtml:html/xhtml:head/(node() except xhtml:meta)"/>
                <!--  -->
                <xforms:model id="art-menu">
                    <!-- Variable with path to art-exist for use by form -->
                    <xxforms:variable name="art-exist" select="xxforms:property('art.exist.url')"/>
                    <!-- instance for art-menu-language -->
                    <xforms:instance id="art-menu-language">
                        <dummy/>
                    </xforms:instance>
                    <!-- instance for menu -->
                    <xforms:instance id="menu">
                        <dummy/>
                    </xforms:instance>
                    <xforms:submission id="get-art-menu-submission" serialization="none" method="get" resource="{{$art-exist}}/modules/get-art-menu.xquery?menufile={{encode-for-uri('{$strMenuFilePath}')}}" replace="instance" instance="menu">
                        <xforms:message ev:event="xforms-submit-error" level="modal">
                            A submission error occurred: <xforms:output value="event('error-type')"/>; Status: <xforms:output value="event('response-status-code')"/>; URI: <xforms:output value="event('resource-uri')"/>; Headers: <xforms:output value="event('response-headers')"/>; Body: <xforms:output value="event('response-body')"/>
                        </xforms:message>
                    </xforms:submission>
                    <!-- instance for decor project menu -->
                    <xforms:instance id="decor-menu">
                        <dummy/>
                    </xforms:instance>
                    <xforms:submission id="get-art-project-menu-submission" mode="asynchronous" serialization="none" method="get" resource="{{$art-exist}}/modules/get-art-project-menu.xquery" replace="instance" instance="decor-menu">
                        <xforms:message ev:event="xforms-submit-error" level="modal">
                            A submission error occurred: <xforms:output value="event('error-type')"/>; Status: <xforms:output value="event('response-status-code')"/>; URI: <xforms:output value="event('resource-uri')"/>; Headers: <xforms:output value="event('response-headers')"/>; Body: <xforms:output value="event('response-body')"/>
                        </xforms:message>
                    </xforms:submission>
                    <!-- instance for selected decor project or governance group menu item -->
                    <xforms:instance id="selected-decor-project">
                        <prefix/>
                    </xforms:instance>
                    <xforms:instance id="search-decor-project">
                        <dummy/>
                    </xforms:instance>
                    <!-- instance for search results based on select-decor-menu-item -->
                    <xforms:instance id="result-decor-project">
                        <dummy/>
                    </xforms:instance>
                    <xforms:submission id="search-decor-project-submission" serialization="none" method="get" resource="{{$art-exist}}/modules/search-decor-project.xquery?searchString={{encode-for-uri(instance('search-decor-project'))}}&amp;language={{instance('art-menu-language')}}" replace="instance" instance="result-decor-project">
                        <xforms:message ev:event="xforms-submit-error" level="modal">
                            A submission error occurred: <xforms:output value="event('error-type')"/>; Status: <xforms:output value="event('response-status-code')"/>; URI: <xforms:output value="event('resource-uri')"/>; Headers: <xforms:output value="event('response-headers')"/>; Body: <xforms:output value="event('response-body')"/>
                        </xforms:message>
                        <!--<xforms:message ev:event="xforms-submit-done" level="modal">
                            Status: <xforms:output value="event('response-status-code')"/>; URI: <xforms:output value="event('resource-uri')"/>; Headers: <xforms:output value="event('response-headers')"/>; Body: <xforms:output value="event('response-body')"/>
                        </xforms:message>-->
                    </xforms:submission>
                    <!-- instance for terminology menu -->
                    <xforms:instance id="terminology-menu">
                        <dummy/>
                    </xforms:instance>
                    <xforms:submission id="get-terminology-menu-submission" mode="asynchronous" serialization="none" method="get" resource="{{$art-exist}}/modules/get-terminology-menu.xquery" replace="instance" instance="terminology-menu">
                        <xforms:message ev:event="xforms-submit-error" level="modal">
                            A submission error occurred: <xforms:output value="event('error-type')"/>; Status: <xforms:output value="event('response-status-code')"/>; URI: <xforms:output value="event('resource-uri')"/>; Headers: <xforms:output value="event('response-headers')"/>; Body: <xforms:output value="event('response-body')"/>
                        </xforms:message>
                    </xforms:submission>
                    <xforms:instance id="user-instance">
                        <user name="" pass="" logged-in=""/>
                    </xforms:instance>
                    <xforms:bind nodeset="instance('user-instance')" readonly="instance('user-instance')/@logged-in = 'true'"/>
                    <xforms:submission id="login-submission" ref="instance('user-instance')" method="post" resource="{{$art-exist}}/modules/login.xq" replace="instance" instance="user-instance">
                        <xforms:message ev:event="xforms-submit-error" level="modal" close="true">
                            <xforms:output value="string-join(('A submission error occurred: ',string(event('error-type')),'; Status: ',string(event('response-status-code')),'; URI: ',event('resource-uri'),'; Headers: ',event('response-headers'),'; Body: ',event('response-body')),' ')"/>
                        </xforms:message>
                        <xforms:action ev:event="xforms-submit-done">
                            <xforms:action if="instance('user-instance')/@logged-in = 'true'">
                                <xforms:insert context="." origin="xxforms:set-session-attribute('username', string(instance('user-instance')/@name))"/>
                                <xforms:insert context="." origin="xxforms:set-session-attribute('password', string(instance('user-instance')/@pass))"/>
                                <xforms:insert context="." origin="xxforms:set-session-attribute('language', instance('user-instance')/defaultLanguage)"/>
                                <xforms:insert context="." origin="xxforms:set-session-attribute('displayName', instance('user-instance')/displayName)"/>
                                <xforms:insert context="." origin="xxforms:set-session-attribute('groups', string-join(instance('user-instance')/groups/group, ' '))"/>
                                <xforms:insert context="." origin="xxforms:set-session-attribute('comefrom', 'login')"/>
                                <xxforms:script>window.location.reload();</xxforms:script>
                                <xxforms:hide dialog="login-dialog"/>
                            </xforms:action>
                            <xforms:action if="instance('user-instance')/@logged-in = 'false'">
                                <xforms:setfocus control="username"/>
                                <xforms:insert context="." origin="xxforms:set-session-attribute('username', ())"/>
                                <xforms:insert context="." origin="xxforms:set-session-attribute('password', ())"/>
                                <xforms:insert context="." origin="xxforms:set-session-attribute('language', ())"/>
                                <xforms:insert context="." origin="xxforms:set-session-attribute('displayName', ())"/>
                                <xforms:insert context="." origin="xxforms:set-session-attribute('groups', ())"/>
                            </xforms:action>
                        </xforms:action>
                    </xforms:submission>
                    
                    <xforms:action ev:event="xforms-model-construct-done">
                        <xforms:send submission="get-art-menu-submission"/>
                        <!--<xforms:send if="{$menuItems}='decor'" submission="get-art-project-menu-submission"/>-->
                        <xforms:send if="{$menuItems}='terminology'" submission="get-terminology-menu-submission"/>
                    </xforms:action>
                    <xforms:action ev:event="xforms-ready">
                        <xforms:send submission="search-decor-project-submission"/>
                        <xforms:dispatch name="xforms-value-changed" target="art-ui-language"/>
                    </xforms:action>
                </xforms:model>
            </xhtml:head>
            <xhtml:body>
                <!-- Copy body attributes -->
                <xsl:apply-templates select="/xhtml:html/xhtml:body/@*"/>
                <!-- resources is scoped by the main model. Orbeon 3.9 will forgive the scoping problem
                    that leads to by using it for model art-menu too, but Orbeon 4.x doesn't and gives up.
                    Need to re-assign the variable in scope for both models, but we only use art-menu-resources 
                    for contents created here.
                -->
                <xxforms:variable name="art-menu-resources" select="$resources"/>
                <xxforms:variable name="art-menu-language" select="instance('language')"/>
                <!-- This is used to send to the TinyMCE config in each form that defines the instance('language') -->
                <xforms:output ref="instance('language')" id="art-ui-language" class="hidden">
                    <xforms:action ev:event="xforms-value-changed">
                        <xxforms:script>try { TINYMCE_CUSTOM_CONFIG.language = ORBEON.xforms.Document.getValue('art-ui-language').split('-')[0]; } catch (error) {}</xxforms:script>
                    </xforms:action>
                </xforms:output>
                <!-- dialog for logging into decor -->
                <xxforms:dialog id="login-dialog" model="art-menu" appearance="full" level="modal" close="true" draggable="true" visible="false">
                    <xforms:label ref="$art-menu-resources/login"/>
                    <xforms:action ev:event="xxforms-dialog-open">
                        <xforms:setfocus control="login-dialog-username"/>
                    </xforms:action>
                    <xforms:action ev:observer="login-group" ev:event="DOMActivate">
                        <xforms:send submission="login-submission"/>
                    </xforms:action>
                    <xforms:group ref="instance('user-instance')" id="login-group">
                        <xhtml:table width="350" class="spaced">
                            <xhtml:tr>
                                <xhtml:td class="item-label">
                                    <xforms:output ref="$art-menu-resources/username"/>
                                </xhtml:td>
                                <xhtml:td>
                                    <xforms:input id="login-dialog-username" ref="@name" class="short-text">
                                        <xforms:label ref="$art-menu-resources/username" class="hidden"/>
                                    </xforms:input>
                                </xhtml:td>
                            </xhtml:tr>
                            <xhtml:tr>
                                <xhtml:td class="item-label">
                                    <xforms:output ref="$art-menu-resources/password"/>
                                </xhtml:td>
                                <xhtml:td>
                                    <xforms:secret id="login-dialog-password" ref="@pass" class="short-text">
                                        <xforms:label ref="$art-menu-resources/password" class="hidden"/>
                                    </xforms:secret>
                                </xhtml:td>
                            </xhtml:tr>
                            <xhtml:tr>
                                <xhtml:td colspan="2">
                                    <xforms:group ref=".[@logged-in = 'false']">
                                        <xforms:output ref="$art-menu-resources/login-failure"/>
                                    </xforms:group>
                                    <xforms:output ref="' '"/>
                                </xhtml:td>
                            </xhtml:tr>
                            <xhtml:tr>
                                <xhtml:td colspan="2">
                                    <xhtml:div class="buttons">
                                        <xforms:group ref=".[@logged-in = 'true']">
                                            <xhtml:img src="/img/loader.gif" class="spinner" alt="Busy" style="margin-right: 5px;"/>
                                        </xforms:group>
                                        <xforms:trigger ref="." appearance="compact" id="login-dialog-button">
                                            <xforms:label ref="$art-menu-resources/login"/>
                                        </xforms:trigger>
                                    </xhtml:div>
                                </xhtml:td>
                            </xhtml:tr>
                        </xhtml:table>
                    </xforms:group>
                </xxforms:dialog>
                <!-- dialog for searching decor -->
                <xxforms:dialog id="search-decor-project-dialog" model="art-menu" appearance="full" level="modal" close="true" draggable="true" visible="false">
                    <xforms:action ev:event="xxforms-dialog-open">
                        <xforms:setvalue ref="instance('art-menu-language')" value="$art-menu-language"/>
                        <xforms:dispatch if="instance('result-decor-project')[not(project)]" target="searchbox-decor-project" name="xforms-value-changed"/>
                    </xforms:action>
                    <xforms:label ref="$art-menu-resources/search"/>
                    <xhtml:table class="detail spaced" width="500">
                        <xhtml:tr>
                            <xhtml:td class="item-label" style="padding-right: 10px;padding-left: 10px;">
                                <xhtml:img src="/img/search16.png" alt="" align="right" title="{{$art-menu-resources/search}}"/>
                            </xhtml:td>
                            <xhtml:td>
                                <xforms:input class="top" ref="instance('search-decor-project')" id="searchbox-decor-project" incremental="true">
                                    <xforms:label ref="$art-menu-resources/search" class="hidden"/>
                                    <xforms:action ev:event="xforms-value-changed">
                                        <xxforms:variable name="search-value" select="."/>
                                        <!--<xxforms:variable name="make-suggestion" select="string-length($search-value) >= 2"/>-->
                                        <xxforms:variable name="make-suggestion" select="true()"/>
                                        <xforms:action if="$make-suggestion">
                                            <xforms:send submission="search-decor-project-submission"/>
                                        </xforms:action>
                                    </xforms:action>
                                </xforms:input>
                                <xhtml:div style="margin-left: 10px;" class="buttons">
                                    <xforms:trigger appearance="compact">
                                        <xforms:label ref="$art-menu-resources/close"/>
                                        <xxforms:hide ev:event="DOMActivate" dialog="search-decor-project-dialog"/>
                                    </xforms:trigger>
                                </xhtml:div>
                            </xhtml:td>
                        </xhtml:tr>
                        <xhtml:tr>
                            <xhtml:td colspan="2">
                                <xhtml:div style="width: 100%; max-height: 450px; overflow-x: auto; overflow-y: auto;">
                                    <xhtml:table class="zebra-table spaced" width="100%">
                                        <xhtml:tr>
                                            <xhtml:td class="heading">
                                                <xforms:output ref="$art-menu-resources/project"/>
                                            </xhtml:td>
                                            <xhtml:td class="heading">
                                                <xforms:output ref="$art-menu-resources/governance-group" class="node-governance-group"/>
                                            </xhtml:td>
                                        </xhtml:tr>
                                        <xforms:repeat nodeset="xxforms:sort(instance('result-decor-project')/project[not(@repository='true')][not(@experimental='true')], name[@language=$art-menu-resources/@xml:lang][1]),
                                                                xxforms:sort(instance('result-decor-project')/project[@repository='true'], name[@language=$art-menu-resources/@xml:lang][1]),
                                                                xxforms:sort(instance('result-decor-project')/project[not(@repository='true')][@experimental='true'], name[@language=$art-menu-resources/@xml:lang][1]),
                                                                xxforms:sort(instance('result-decor-project')/group, name[@language=$art-menu-resources/@xml:lang][1])">
                                            <xhtml:tr class="not-selectable zebra-row-{{if (position() mod 2 = 0) then 'even' else 'odd'}}">
                                                <xhtml:td width="50%">
                                                    <xforms:group ref=".[self::project]">
                                                        <xhtml:a href="{{concat('/decor-project--',@prefix)}}">
                                                            <xforms:output ref="if (name[@language=$art-menu-language][text()]) then name[@language=$art-menu-language][text()][1] else if (name[@language='en-US'][text()]) then name[@language='en-US'][text()][1] else name[text()][1]" class="{{if (context()/@experimental='true') then 'node-project-experimental' else ''}} {{if (context()/@repository='true') then 'node-project-bbr' else ''}}"/>
                                                        </xhtml:a>
                                                    </xforms:group>
                                                </xhtml:td>
                                                <xhtml:td width="50%">
                                                    <xforms:repeat nodeset="governance | self::group">
                                                        <xhtml:div class="not-selectable" title="{{$art-menu-resources/governance-group}}">
                                                            <xhtml:a href="{{concat('/decor-governance-group--?id=',@id,'&amp;section=projects')}}">
                                                                <xforms:output ref="replace(if (name[@language=$art-menu-language][text()]) then name[@language=$art-menu-language][text()][1] else if (name[@language='en-US'][text()]) then name[@language='en-US'][text()][1] else name[text()][1],'\s','&#160;')"/>
                                                            </xhtml:a>
                                                        </xhtml:div>
                                                    </xforms:repeat>
                                                </xhtml:td>
                                            </xhtml:tr>
                                        </xforms:repeat>
                                    </xhtml:table>
                                </xhtml:div>
                            </xhtml:td>
                        </xhtml:tr>
                    </xhtml:table>
                </xxforms:dialog>
                <xhtml:table id="maincontent" width="100%" style="background: transparent;">
                    <!-- row with page heading and clickable logo -->
                    <xhtml:tr>
                        <xhtml:td class="page-heading">
                            <!-- decor-project','decor-development','decor-datasets','decor-scenarios','decor-transaction-editor',
                            'decor-codesystems','decor-codesystem-editor','decor-codesystem-ids',
                            'decor-terminology','decor-valuesets','decor-valueset-editor',
                            'decor-valueset-ids','decor-templates','decor-template-editor',
                            'decor-template-mapping','decor-template-ids','decor-issues',
                            'decor-mycommunity -->
                            <xsl:choose>
                                <xsl:when test="$current-application='decor-datasets'">
                                    <xhtml:img alt="" width="50px" src="img/pill-ds.png"/>
                                </xsl:when>
                                <xsl:when test="$current-application='decor-scenarios'">
                                    <xhtml:img alt="" width="50px" src="img/pill-sc.png"/>
                                </xsl:when>
                                <xsl:when test="$current-application=('decor-codesystems','decor-terminology','decor-valuesets','decor-valueset-editor')">
                                    <xhtml:img alt="" width="50px" src="img/pill-cd.png"/>
                                </xsl:when>
                                <xsl:when test="$current-application=('decor-codesystem-ids','decor-valueset-ids','decor-template-ids')">
                                    <xhtml:img alt="" width="50px" src="img/pill-id.png"/>
                                </xsl:when>
                                <xsl:when test="$current-application=('decor-templates','decor-template-mapping','decor-template-editor')">
                                    <xhtml:img alt="" width="50px" src="img/pill-tm.png"/>
                                </xsl:when>
                                <xsl:when test="$current-application='decor-issues'">
                                    <xhtml:img alt="" width="50px" src="img/pill-is.png"/>
                                </xsl:when>
                            </xsl:choose>
                            <xsl:copy-of select="/xhtml:html/xhtml:head/xhtml:title/node()"/>
                        </xhtml:td>
                        <xhtml:td colspan="2">
                            <xsl:variable name="logo-height" select="50"/>
                            <xsl:variable name="logo-padding" select="4"/>
                            <xsl:variable name="logo-div-height" select="58"/>
                            <!-- Logos can be max-height: 50px;, but may be smaller. We reserve that max space 
                                 + 4px padding top/bottom to make  sure that when the logo
                                 loads,it doesn't give a page shift in height, making the end result easier on the eye -->
                            <xhtml:div class="buttons" style="height: {$logo-div-height}px;">
                                <!-- Logos vary from form to form
                                    1.  DECOR project oriented forms (recognized by the instance('document') can call the 
                                        ProjectLogo service. This service will return the server default logo if no specific 
                                        logo was configured for this project
                                    2.  Any other form MAY have an instance('logo') that may be either just a logo name or 
                                        a full HTTP based URL. Logo names are assumed to be /img/logoname, and full URLs are 
                                        used as-is
                                -->
                                <xsl:if test="$isDecor and $documentChecked[string-length()&gt;0]">
                                    <xxforms:variable name="decor-external-exist" select="xxforms:property('decor.external.exist.url')"/>
                                </xsl:if>
                                <xforms:trigger appearance="minimal">
                                    <xforms:label>
                                        <xhtml:img alt="" style="max-height: {$logo-height}px; padding: {$logo-padding}px;">
                                            <xsl:attribute name="src">
                                                <xsl:choose>
                                                    <xsl:when test="$isDecor and $documentChecked[string-length()&gt;0]">
                                                        <xsl:text>{concat($decor-external-exist,'/services/ProjectLogo?prefix=',instance('document'))}</xsl:text>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:text>{if (string-length(instance('logo'))=0) then ('</xsl:text>
                                                        <xsl:choose>
                                                            <xsl:when test="starts-with($defaultLogo, 'http')">
                                                                <xsl:value-of select="$defaultLogo"/>
                                                            </xsl:when>
                                                            <xsl:otherwise>
                                                                <xsl:text>/img/</xsl:text>
                                                                <xsl:value-of select="$defaultLogo"/>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                        <xsl:text>') else</xsl:text>
                                                        <xsl:text> if (instance('logo')[starts-with(.,'http')]) then (</xsl:text>
                                                        <xsl:text>instance('logo')</xsl:text>
                                                        <xsl:text>) else (concat('/img/',encode-for-uri(instance('logo'))))}</xsl:text>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:attribute>
                                        </xhtml:img>
                                    </xforms:label>
                                    <xforms:action ev:event="DOMActivate">
                                        <xforms:load show="new">
                                            <xsl:attribute name="resource">
                                                <xsl:text>{if (string-length(instance('logo'))=0) then ('</xsl:text>
                                                <xsl:value-of select="$defaultHref"/>
                                                <xsl:text>') else (instance('logo')/@href)}</xsl:text>
                                            </xsl:attribute>
                                        </xforms:load>
                                    </xforms:action>
                                </xforms:trigger>
                            </xhtml:div>
                        </xhtml:td>
                    </xhtml:tr>
                    <!-- row with menu, login and language select flag buttons -->
                    <xhtml:tr>
                        <!-- menu -->
                        <xhtml:td style="margin:0;padding:0;vertical-align:text-bottom;" colspan="3">
                            <xxforms:variable name="art-menu-datasetId" select="if (instance('dataset-instance')[@id]) then (instance('dataset-instance')/string-join((@id,@effectiveDate),';;')) else if (instance('dataset-navigation-ids')) then (instance('dataset-navigation-ids')) else (instance('dataset-navigation'))"/>
                            <xxforms:variable name="art-menu-transactionId" select="if (instance('dataset-instance')[@transactionId]) then (instance('dataset-instance')/string-join((@transactionId,@transactionEffectiveDate),';;')) else ()"/>
                            <xxforms:variable name="art-menu-scenarioId" select="if (empty($art-menu-transactionId)) then instance('scenario-navigation') else $art-menu-transactionId"/>
                            <xxforms:variable name="art-menu-conceptId" select="if (instance('concept-navigation-ids')[not(.='')]) then instance('concept-navigation-ids') else (instance('concept-navigation'))"/>
                            <xxforms:variable name="art-menu-ds-or-tr" select="if (empty($art-menu-transactionId)) then 'ds' else 'tr'"/>
                            <xxforms:variable name="art-menu-ds-or-tr-id" select="if (empty($art-menu-transactionId)) then $art-menu-datasetId else $art-menu-transactionId"/>
                            <xxforms:variable name="username" select="xxforms:get-session-attribute('username')"/>
                            <xxforms:variable name="usergroups" select="tokenize(xxforms:get-session-attribute('groups'), '\s')"/>
                            <xhtml:ul id="navmenu">
                                <xforms:repeat model="art-menu" nodeset="instance('menu')/section[@id={$menuItems}][not(@groups) or count(tokenize(@groups,'\s')[. = $usergroups]) gt 0]">
                                    <xforms:group ref=".[@id='home' or @id='decor']">
                                        <!-- home alone -->
                                        <xhtml:li class="not-selectable half">
                                            <xhtml:a href="{{if (@link) then @link else ('#')}}">
                                                <xhtml:img src="/img/home.png" alt="" title="{{name[@language=$art-menu-resources/@xml:lang]}}" width="16" height="16"/>
                                            </xhtml:a>
                                        </xhtml:li>
                                    </xforms:group>
                                    <!-- Decor contains the list of projects. This list is taken from decor-menu -->
                                    <xforms:group ref=".[@id='decor']">
                                        <!-- explorer -->
                                        <xhtml:li class="not-selectable half">
                                            <xhtml:a href="/decor-explore">
                                                <xhtml:img src="/img/eye16.png" alt="Explore" title="{{$art-menu-resources/explore}}" width="16" height="16"/>
                                            </xhtml:a>
                                        </xhtml:li>
                                        <xhtml:li class="not-selectable half">
                                            <xforms:trigger appearance="minimal" id="decorProjectSearch">
                                                <xforms:label>
                                                    <xhtml:img src="/img/search16.png" alt="" title="{{$art-menu-resources/search}}" width="16" height="16"/>
                                                </xforms:label>
                                                <xxforms:show ev:event="DOMActivate" dialog="search-decor-project-dialog"/>
                                            </xforms:trigger>
                                        </xhtml:li>
                                        <xhtml:li class="not-selectable half">
                                            <xhtml:a href="#">
                                                <xhtml:img src="/img/greenclock16.png" alt="" title="{{$art-menu-resources/search}}" width="16" height="16"/>
                                            </xhtml:a>
                                            <xhtml:ul class="sub1">
                                                <xforms:repeat nodeset="xxforms:sort(instance('result-decor-project')/governancegroup, name[@language=$art-menu-resources/@xml:lang][1])">
                                                    <xhtml:li class="not-selectable">
                                                        <xsl:call-template name="decor-menuitem-governancegroup"/>
                                                    </xhtml:li>
                                                </xforms:repeat>
                                                <xforms:repeat nodeset="xxforms:sort(instance('result-decor-project')/project[not(@repository='true')][not(@experimental='true')], name[@language=$art-menu-resources/@xml:lang][1])">
                                                    <xhtml:li class="not-selectable">
                                                        <xsl:call-template name="decor-menuitem-project"/>
                                                    </xhtml:li>
                                                </xforms:repeat>
                                                <xforms:repeat nodeset="xxforms:sort(instance('result-decor-project')/project[@repository='true'], name[@language=$art-menu-resources/@xml:lang][1])">
                                                    <xhtml:li class="not-selectable">
                                                        <xsl:call-template name="decor-menuitem-project-bbr"/>
                                                    </xhtml:li>
                                                </xforms:repeat>
                                                <xforms:repeat nodeset="xxforms:sort(instance('result-decor-project')/project[not(@repository='true')][@experimental='true'], name[@language=$art-menu-resources/@xml:lang][1])">
                                                    <xhtml:li class="not-selectable">
                                                        <xsl:call-template name="decor-menuitem-project-experimental"/>
                                                    </xhtml:li>
                                                </xforms:repeat>
                                            </xhtml:ul>
                                        </xhtml:li>
                                    </xforms:group>
                                    <xforms:group ref=".[@id='project']">
                                        <xhtml:li class="not-selectable">
                                            <xhtml:ul class="sub1">
                                                <xforms:repeat nodeset="application">
                                                    <xhtml:li class="not-selectable">
                                                        <xhtml:a href="{{concat('/decor-development--',@prefix)}}">
                                                            <xforms:output ref="name[@language=$art-menu-resources/@xml:lang]"/>
                                                        </xhtml:a>
                                                    </xhtml:li>
                                                </xforms:repeat>
                                            </xhtml:ul>
                                        </xhtml:li>
                                    </xforms:group>
                                    <!-- Terminology has a number of specific applications (e.g. SNOMED CT / LOINC / ISO9999) and
                                        any number of ClaML based classifications (e.g. HL7, ATC, ICD10). The ClaML based classifications
                                        are taken from the terminology-menu instance -->
                                    <xforms:group ref=".[@id='terminology']">
                                        <xhtml:li class="not-selectable">
                                            <xhtml:a href="#">
                                                <xforms:output ref="name[@language=$art-menu-resources/@xml:lang]"/>
                                            </xhtml:a>
                                            <xhtml:ul class="sub1">
                                                <!-- only show menu item if package is available -->
                                                <xforms:repeat nodeset="application">
                                                    <xhtml:li class="not-selectable">
                                                        <xhtml:a href="{{@link}}">
                                                            <xforms:output ref="name[@language=$art-menu-resources/@xml:lang]"/>
                                                        </xhtml:a>
                                                    </xhtml:li>
                                                </xforms:repeat>
                                                <!-- show menu items for available ClaML packages -->
                                                <xforms:repeat model="art-menu" nodeset="xxforms:instance('terminology-menu')//classification">
                                                    <xhtml:li class="not-selectable">
                                                        <xhtml:a href="{{concat('claml?collection=',@collection)}}">
                                                            <xforms:output ref="@displayName"/>
                                                        </xhtml:a>
                                                    </xhtml:li>
                                                </xforms:repeat>
                                            </xhtml:ul>
                                        </xhtml:li>
                                    </xforms:group>
                                    <!-- Refsets has a number of specific applications (e.g. SNOMED CT / DHD) and 
                                        any number of existing RefSets. The existing RefSets are taken from the 
                                        terminology-menu instance -->
                                    <xforms:group ref=".[@id='refsets']">
                                        <xhtml:li class="not-selectable">
                                            <xhtml:a href="home">
                                                <xforms:output ref="name[@language=$art-menu-resources/@xml:lang]"/>
                                            </xhtml:a>
                                            <xhtml:ul class="sub1">
                                                <!-- show menu items for available SNOMED CT refsets -->
                                                <xforms:repeat model="art-menu" nodeset="xxforms:instance('terminology-menu')//refset">
                                                    <xhtml:li class="not-selectable">
                                                        <xhtml:a href="{{concat('refsets?id=',@id)}}">
                                                            <xforms:output ref="replace(name[@language=$art-menu-resources/@xml:lang][1],' ','&#160;')"/>
                                                        </xhtml:a>
                                                    </xhtml:li>
                                                </xforms:repeat>
                                                <!-- only show menu item if package is available - makeApplicationItem -->
                                                <xforms:group ref=".[application]">
                                                    <!--<xhtml:ul class="sub1">-->
                                                    <xforms:repeat nodeset="application[not(@groups) or count(tokenize(@groups,'\s')[. = $usergroups]) gt 0]">
                                                        <xhtml:li class="not-selectable">
                                                            <xhtml:a href="{{concat(@link,'{$documentChecked}')}}">
                                                                <xforms:output ref="name[@language=$art-menu-resources/@xml:lang]"/>
                                                            </xhtml:a>
                                                        </xhtml:li>
                                                    </xforms:repeat>
                                                    <!--</xhtml:ul>-->
                                                </xforms:group>
                                            </xhtml:ul>
                                        </xhtml:li>
                                    </xforms:group>
                                    <!-- This is the 'other' group of menu items with applications and max 2 levels of section + applications -->
                                    <xforms:group ref=".[not(@id=('home','decor','terminology','refsets'))]">
                                        <xhtml:li class="not-selectable">
                                            <xforms:group ref=".[@id='decor-explore']">
                                                <xhtml:a href="{{if (@link) then concat(@link,'{$documentChecked}') else ('#')}}">
                                                    <xhtml:img alt="" style="max-width: 16px; max-height: 16px; margin-right: 2px;" src="/img/home.png">
                                                        <xsl:attribute name="title">{name[@language=$art-menu-resources/@xml:lang]}</xsl:attribute>
                                                    </xhtml:img>
                                                </xhtml:a>
                                            </xforms:group>
                                            <!-- Datasets button should include context from where we came from if possible -->
                                            <xforms:group ref=".[@id=('datasets')]">
                                                <xhtml:a>
                                                    <xsl:attribute name="href">
                                                        <xsl:text>{if (@link) then concat(@link,'</xsl:text>
                                                        <xsl:value-of select="$documentChecked"/>
                                                        <xsl:text>','?id=',encode-for-uri(tokenize($art-menu-datasetId,';;')[1]),</xsl:text>
                                                        <xsl:text>'&amp;effectiveDate=',encode-for-uri(tokenize($art-menu-datasetId,';;')[2]),</xsl:text>
                                                        <xsl:text>'&amp;conceptId=',encode-for-uri(tokenize($art-menu-conceptId,';;')[1]),</xsl:text>
                                                        <xsl:text>'&amp;conceptEffectiveDate=',encode-for-uri(tokenize($art-menu-conceptId,';;')[2])</xsl:text>
                                                        <xsl:text>) else ('#')}</xsl:text>
                                                    </xsl:attribute>
                                                    <xforms:output ref="name[@language=$art-menu-resources/@xml:lang]"/>
                                                </xhtml:a>
                                            </xforms:group>
                                            <!-- Scenarios button should include context from where we came from if possible -->
                                            <xforms:group ref=".[@id=('scenarios')]">
                                                <xhtml:a>
                                                    <xsl:attribute name="href">
                                                        <xsl:text>{if (@link) then concat(@link,'</xsl:text>
                                                        <xsl:value-of select="$documentChecked"/>
                                                        <xsl:text>','?id=', encode-for-uri(tokenize($art-menu-scenarioId,';;')[1]),</xsl:text>
                                                        <xsl:text>'&amp;effectiveDate=', encode-for-uri(tokenize($art-menu-scenarioId,';;')[2]),</xsl:text>
                                                        <xsl:text>'&amp;datasetId=', encode-for-uri(tokenize($art-menu-datasetId,';;')[1]),</xsl:text>
                                                        <xsl:text>'&amp;datasetEffectiveDate=', encode-for-uri(tokenize($art-menu-datasetId,';;')[2]),</xsl:text>
                                                        <xsl:text>'&amp;conceptId=', encode-for-uri(tokenize($art-menu-conceptId,';;')[1]),</xsl:text>
                                                        <xsl:text>'&amp;conceptEffectiveDate=', encode-for-uri(tokenize($art-menu-conceptId,';;')[2])</xsl:text>
                                                        <xsl:text>) else ('#')}</xsl:text>
                                                    </xsl:attribute>
                                                    <xforms:output ref="name[@language=$art-menu-resources/@xml:lang]"/>
                                                </xhtml:a>
                                            </xforms:group>
                                            <xforms:group ref=".[not(@id=('decor-explore','datasets','scenarios'))]">
                                                <xhtml:a href="{{if (@link) then concat(@link,'{$documentChecked}') else ('#')}}">
                                                    <xforms:output ref="name[@language=$art-menu-resources/@xml:lang]"/>
                                                </xhtml:a>
                                            </xforms:group>
                                            <xforms:group ref=".[application|section]">
                                                <xhtml:ul class="sub1">
                                                    <xforms:repeat nodeset="(application|section)[not(ancestor-or-self::*/@groups) or count(tokenize((ancestor-or-self::*/@groups)[last()],'\s')[. = $usergroups]) gt 0]">
                                                        <xhtml:li class="not-selectable">
                                                            <xhtml:a href="{{if (@link) then concat(@link,'{$documentChecked}') else ('#')}}">
                                                                <xsl:attribute name="href">
                                                                    <!-- Terminology-association/template-mapping buttons should include context 
                                                                        from where we came from if possible -->
                                                                    <xsl:text>{if (@link) then concat(@link,'</xsl:text>
                                                                    <xsl:value-of select="$documentChecked"/>
                                                                    <xsl:text>',</xsl:text>
                                                                    <xsl:text>if (@id=('associations','template-mapping')) then concat('?',</xsl:text>
                                                                    <xsl:text>'&amp;selector=',encode-for-uri($art-menu-ds-or-tr),</xsl:text>
                                                                    <xsl:text>'&amp;datasetId=',encode-for-uri(tokenize($art-menu-ds-or-tr-id,';;')[1]),</xsl:text>
                                                                    <xsl:text>'&amp;datasetEffectiveDate=',encode-for-uri(tokenize($art-menu-ds-or-tr-id,';;')[2]),</xsl:text>
                                                                    <xsl:text>'&amp;conceptId=',encode-for-uri(tokenize($art-menu-conceptId,';;')[1]),</xsl:text>
                                                                    <xsl:text>'&amp;conceptEffectiveDate=',encode-for-uri(tokenize($art-menu-conceptId,';;')[2]))</xsl:text>
                                                                    <xsl:text> else if (@id=('templates')) then '?section=templates'</xsl:text>
                                                                    <xsl:text> else if (@id=('structuredefinitions')) then '?section=structuredefinitions'</xsl:text>
                                                                    <xsl:text> else ()</xsl:text>
                                                                    <xsl:text>) else ('#')}</xsl:text>
                                                                </xsl:attribute>
                                                                <xforms:output ref="name[@language=$art-menu-resources/@xml:lang]"/>
                                                            </xhtml:a>
                                                            <xforms:group ref=".[application|section]">
                                                                <xhtml:ul class="sub2">
                                                                    <xforms:repeat nodeset="(application|section)[not(ancestor-or-self::*/@groups) or count(tokenize((ancestor-or-self::*/@groups)[last()],'\s')[. = $usergroups]) gt 0]">
                                                                        <xhtml:li class="not-selectable">
                                                                            <xhtml:a href="{{if (@link) then concat(@link,'{$documentChecked}') else ('#')}}">
                                                                                <xforms:output ref="name[@language=$art-menu-resources/@xml:lang]"/>
                                                                            </xhtml:a>
                                                                            <xforms:group ref=".[application]">
                                                                                <xhtml:ul class="sub3">
                                                                                    <xforms:repeat nodeset="application[not(ancestor-or-self::*/@groups) or count(tokenize((ancestor-or-self::*/@groups)[last()],'\s')[. = $usergroups]) gt 0]">
                                                                                        <xhtml:li class="not-selectable">
                                                                                            <xhtml:a href="{{if (@link) then concat(@link,'{$documentChecked}') else ('#')}}">
                                                                                                <xforms:output ref="name[@language=$art-menu-resources/@xml:lang]"/>
                                                                                            </xhtml:a>
                                                                                        </xhtml:li>
                                                                                    </xforms:repeat>
                                                                                </xhtml:ul>
                                                                            </xforms:group>
                                                                        </xhtml:li>
                                                                    </xforms:repeat>
                                                                </xhtml:ul>
                                                            </xforms:group>
                                                        </xhtml:li>
                                                    </xforms:repeat>
                                                </xhtml:ul>
                                            </xforms:group>
                                        </xhtml:li>
                                    </xforms:group>
                                </xforms:repeat>
                                <!-- login -->
                                <xforms:group ref=".[empty($username) or $username = ('guest', '')]">
                                    <xhtml:li class="right" style="width: 100px;">
                                        <xsl:choose>
                                            <xsl:when test="$orbeonVersion = '3.9'">
                                                <xhtml:a href="/login?returnToUrl={encode-for-uri($cameFromUri)}">
                                                    <xforms:output ref="$resources/login"/>
                                                </xhtml:a>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xforms:trigger appearance="minimal" id="login-menu-item">
                                                    <xforms:label ref="$art-menu-resources/login"/>
                                                    <xxforms:show ev:event="DOMActivate" dialog="login-dialog"/>
                                                </xforms:trigger>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xhtml:li>
                                </xforms:group>
                                <xforms:group ref=".[not(empty($username) or $username = ('guest', ''))]">
                                    <xhtml:li class="right half">
                                        <xhtml:a href="#">
                                            <xhtml:img alt="" src="/img/User_16.png"/>
                                        </xhtml:a>
                                        <xhtml:ul class="sub1" style="left: -245px;">
                                            <xhtml:li>
                                                <xhtml:a href="/user-settings">
                                                    <xxforms:variable name="etc" select="if (string-length($username) gt 30) then '…' else ()"/>
                                                    <xforms:output ref="concat($art-menu-resources/user-settings,' - ', substring($username, 1 , 30), $etc)"/>
                                                </xhtml:a>
                                            </xhtml:li>
                                            <xhtml:li>
                                                <xforms:trigger appearance="minimal">
                                                    <xforms:label ref="$art-menu-resources/logout"/>
                                                    <xforms:action ev:event="DOMActivate">
                                                        <xforms:setvalue ref="instance('user-instance')/@name" value="''"/>
                                                        <xforms:setvalue ref="instance('user-instance')/@pass" value="''"/>
                                                        <xforms:setvalue ref="instance('user-instance')/defaultLanguage" value="''"/>
                                                        <xforms:setvalue ref="instance('user-instance')/displayName" value="''"/>
                                                        <xforms:setvalue ref="instance('user-instance')/groups" value="''"/>
                                                        <xforms:insert context="." origin="xxforms:set-session-attribute('username', ())"/>
                                                        <xforms:insert context="." origin="xxforms:set-session-attribute('password', ())"/>
                                                        <xforms:insert context="." origin="xxforms:set-session-attribute('language', ())"/>
                                                        <xforms:insert context="." origin="xxforms:set-session-attribute('displayName', ())"/>
                                                        <xforms:insert context="." origin="xxforms:set-session-attribute('groups', ())"/>
                                                        <xforms:insert context="." origin="xxforms:set-session-attribute('comefrom', ())"/>
                                                        <xxforms:script>window.location.reload();</xxforms:script>
                                                    </xforms:action>
                                                </xforms:trigger>
                                                <!--<xhtml:a href="/session/logout">
                                                    <xforms:output ref="$art-menu-resources/logout"/>
                                                </xhtml:a>-->
                                            </xhtml:li>
                                        </xhtml:ul>
                                    </xhtml:li>
                                </xforms:group>
                                <!-- language -->
                                <!-- Only in multi language 'applications' are flags needed. -->
                                <xsl:if test="$isDecor or $current-application = ('decor-governance-group')">
                                    <xhtml:li class="right half">
                                        <xhtml:a href="#">
                                            <xxforms:variable name="selected-langname" select="instance('resources-instance')/resources[@xml:lang=instance('language')]/@displayName"/>
                                            <!--<xforms:output ref="concat($resources/language,':')"/>-->
                                            <xhtml:img alt="" style="display: inline; margin-left: 4px;">
                                                <xsl:attribute name="src">/img/flags/{instance('language')/lower-case(substring(.,4,2))}.png</xsl:attribute>
                                                <xsl:attribute name="title">{if ($selected-langname) then $selected-langname else (instance('language'))}</xsl:attribute>
                                            </xhtml:img>
                                        </xhtml:a>
                                        <xhtml:ul class="sub1" style="left: -245px;">
                                            <!-- Show languages that exist in the project -->
                                            <!-- If we came from a project in a language that the current project does not have, we need that language 
                                                in this list too, otherwise we may not be able to switch -->
                                            <xxforms:variable name="languages" select="instance('project-instance')/name/@language | instance('language')[not(. = '')][not(. = instance('project-instance')/name/@language)]"/>
                                            <xforms:repeat nodeset="$languages">
                                                <xxforms:variable name="selected-language" select="."/>
                                                <xxforms:variable name="selected-langname" select="instance('resources-instance')/resources[@xml:lang=$selected-language]/@displayName"/>
                                                <xhtml:li class="not-selectable">
                                                    <xforms:trigger appearance="minimal">
                                                        <xforms:label>
                                                            <xhtml:img alt="" style="display: inline; margin-left:4px;">
                                                                <xsl:attribute name="src">/img/flags/{instance('language')/lower-case(substring($selected-language,4,2))}.png</xsl:attribute>
                                                                <xsl:attribute name="title">{if ($selected-langname) then $selected-langname else ($selected-language)}</xsl:attribute>
                                                            </xhtml:img>
                                                            <xforms:output ref="if ($selected-langname) then $selected-langname else ($selected-language)"/>
                                                            <xhtml:img alt="" style="display: inline; margin-right: 4px; float: right;" class="{{if (instance('language')[.=$selected-language]) then () else ('hidden')}}">
                                                                <xsl:attribute name="src">/img/tick.png</xsl:attribute>
                                                            </xhtml:img>
                                                        </xforms:label>
                                                        <xforms:action ev:event="DOMActivate">
                                                            <xforms:setvalue ref="instance('language')" value="$selected-language"/>
                                                            <xxforms:variable name="session-language" select="xxforms:set-session-attribute('language', instance('language'))"/>
                                                        </xforms:action>
                                                    </xforms:trigger>
                                                </xhtml:li>
                                            </xforms:repeat>
                                        </xhtml:ul>
                                    </xhtml:li>
                                </xsl:if>
                            </xhtml:ul>
                        </xhtml:td>
                    </xhtml:tr>
                    <!-- 
                        IN MIGRATION MESSAGES 
                        Introduced for smooth migrations,
                        see https://art-decor.atlassian.net/browse/AD30-819
                        KH 2022-07-27
                    -->
                    <xforms:group ref=".[instance('project-instance')/inmigration[@type]]">
                        <xhtml:tr>
                            <xhtml:td colspan="3">
                                <xhtml:p/>
                                <xhtml:table width="100%" style="background-color:#fff; padding: 7px;">
                                    <xhtml:tr class="not-selectable">
                                        <xhtml:td style="background-color: #ffc1d6; padding: 15px; width: 30px;">
                                            <xhtml:img src="/img/blueclock.png" alt="" width="48"/>
                                        </xhtml:td>
                                        <xhtml:td style="background-color: #ffc1d6; padding: 15px; font-size: 17px;">
                                            <xforms:group ref=".[instance('project-instance')/inmigration[@type='duplex']]">
                                                <xforms:output value="'This project is undergoing a migration to the new ART-DECOR Release 3 environment.                                  Also the ART-DECOR Release 2 (“classic”) environment has moved.                                  You are encouraged to follow one of the two links below, to continue your work.                                  Preferably use the ART-DECOR Release 3 Entrance link, use your known credentials                                  and directly work on the project with a better user experience and performance.'"/>
                                                <xhtml:p/>
                                                <xforms:output value="'ART-DECOR Release 2 Classic Entrance: go to '"/>
                                                <xxforms:variable name="label2" select="if (string-length(instance('project-instance')/inmigration/@label2) &gt; 0) then instance('project-instance')/inmigration/@label2 else instance('project-instance')/inmigration/@redirect2"/>
                                                <xforms:trigger appearance="compact">    
                                                    <xforms:label ref="$label2"/>
                                                    <xforms:action ev:event="DOMActivate">
                                                        <xforms:load show="new">
                                                            <xsl:attribute name="resource">
                                                                <xsl:text>{instance('project-instance')/inmigration/@redirect2}</xsl:text>
                                                            </xsl:attribute>
                                                        </xforms:load>
                                                    </xforms:action>
                                                </xforms:trigger>
                                                <xhtml:p/>
                                                <xforms:output value="'ART-DECOR Release 3 Entrance: go to '"/>
                                                <xxforms:variable name="label3" select="if (string-length(instance('project-instance')/inmigration/@label3) &gt; 0) then instance('project-instance')/inmigration/@label3 else instance('project-instance')/inmigration/@redirect3"/>
                                                <xforms:trigger appearance="compact">
                                                    <xforms:label ref="$label3"/>
                                                    <xforms:action ev:event="DOMActivate">
                                                        <xforms:load show="new">
                                                            <xsl:attribute name="resource">
                                                                <xsl:text>{instance('project-instance')/inmigration/@redirect3}</xsl:text>
                                                            </xsl:attribute>
                                                        </xforms:load>
                                                    </xforms:action>
                                                </xforms:trigger>
                                            </xforms:group>
                                            <xforms:group ref=".[instance('project-instance')/inmigration[@type='single3']]">
                                                <xforms:output value="'This project is undergoing a migration to the new ART-DECOR Release 3 environment.                                  The project has moved, at the location here the project is now read-only.                                  Please follow the link below, use your new credentials and directly work on the                                  project at the new location with a better user experience and performance.'"/>
                                                <xhtml:p/>
                                                <xforms:output value="'ART-DECOR Release 3 Entrance: go to '"/>
                                                <xxforms:variable name="label3" select="if (string-length(instance('project-instance')/inmigration/@label3) &gt; 0) then instance('project-instance')/inmigration/@label3 else instance('project-instance')/inmigration/@redirect3"/>
                                                <xforms:trigger appearance="compact">
                                                    <xforms:label ref="$label3"/>
                                                    <xforms:action ev:event="DOMActivate">
                                                        <xforms:load show="new">
                                                            <xsl:attribute name="resource">
                                                                <xsl:text>{instance('project-instance')/inmigration/@redirect3}</xsl:text>
                                                            </xsl:attribute>
                                                        </xforms:load>
                                                    </xforms:action>
                                                </xforms:trigger>
                                            </xforms:group>
                                        </xhtml:td>
                                    </xhtml:tr>
                                </xhtml:table>
                            </xhtml:td>
                        </xhtml:tr>
                    </xforms:group>
                    
                    <!-- spacer -->
                    <xhtml:tr>
                        <xhtml:td colspan="3">&#160;</xhtml:td>
                    </xhtml:tr>
                    <xhtml:tr>
                        <xhtml:td class="form-content" colspan="3">
                            <xsl:apply-templates select="/xhtml:html/xhtml:body/*"/>
                            <!-- include xforms inspector if user is member of 'debug' group -->
                            <xsl:if test="tokenize($group,'\|')[. = 'debug']">
                                <fr:xforms-inspector/>
                            </xsl:if>
                        </xhtml:td>
                    </xhtml:tr>
                    <xhtml:tr>
                        <xhtml:td colspan="3">
                            <xhtml:div style="float:right; padding-right:10px;">
                                <xforms:trigger appearance="minimal">
                                    <xforms:label>
                                        <xsl:attribute name="ref">$resources/about-legal-title</xsl:attribute>
                                    </xforms:label>
                                    <xforms:action ev:event="DOMActivate">
                                        <xforms:load resource="about-art-decor"/>
                                    </xforms:action>
                                </xforms:trigger>
                            </xhtml:div>
                        </xhtml:td>
                    </xhtml:tr>
                </xhtml:table>
            </xhtml:body>
        </xhtml:html>
    </xsl:template>
    <xsl:template name="decor-menuitem-project">
        <!-- project menu item, not a governance group, not experimental -->
        <xforms:group ref=".[self::project][not(@experimental='true')]">
            <xhtml:a href="{{concat('/decor-project--',@prefix)}}">
                <xforms:output ref="replace(name[@language=$art-menu-resources/@xml:lang][1],'\s','&#160;')"/>
            </xhtml:a>
        </xforms:group>
    </xsl:template>
    <xsl:template name="decor-menuitem-project-experimental">
        <!-- project menu item, not a governance group, but experimental -->
        <xforms:group ref=".[self::project][@experimental='true']">
            <xhtml:a href="{{concat('/decor-project--',@prefix)}}" style="color:#AAA;">
                <xforms:output ref="replace(name[@language=$art-menu-resources/@xml:lang][1],'\s','&#160;')" class="node-project-experimental"/>
            </xhtml:a>
        </xforms:group>
    </xsl:template>
    <xsl:template name="decor-menuitem-project-bbr">
        <!-- project menu item, not a governance group, but experimental -->
        <xforms:group ref=".[self::project][@repository='true']">
            <xhtml:a href="{{concat('/decor-project--',@prefix)}}">
                <xforms:output ref="replace(name[@language=$art-menu-resources/@xml:lang][1],'\s','&#160;')" class="node-project-bbr"/>
            </xhtml:a>
        </xforms:group>
    </xsl:template>
    <xsl:template name="decor-menuitem-governancegroup">
        <!-- a governance group menu item -->
        <xforms:group ref=".[self::governancegroup]">
            <xsl:call-template name="menucontent-governancegroup"/>
            <xhtml:ul class="sub2" style="width:300px;">
                <xforms:repeat nodeset="project[not(@repository='true')][not(@experimental='true')]">
                    <xhtml:li class="not-selectable">
                        <xsl:call-template name="decor-menuitem-project"/>
                    </xhtml:li>
                </xforms:repeat>
                <xforms:repeat nodeset="project[not(@repository='true')][@experimental='true']">
                    <xhtml:li class="not-selectable">
                        <xsl:call-template name="decor-menuitem-project-experimental"/>
                    </xhtml:li>
                </xforms:repeat>
                <xforms:repeat nodeset="project[@repository='true']">
                    <xhtml:li class="not-selectable">
                        <xsl:call-template name="decor-menuitem-project-bbr"/>
                    </xhtml:li>
                </xforms:repeat>
            </xhtml:ul>
        </xforms:group>
    </xsl:template>
    <xsl:template name="menucontent-governancegroup">
        <xhtml:a href="{{concat('/decor-governance-group--?id=',@id)}}">
            <xforms:output ref="replace(name[@language=$art-menu-resources/@xml:lang][1],'\s','&#160;')" class="node-governance-group"/>
            <xhtml:img alt="" style="width: 16px; height: 16px; margin-right: 2px; float:right;" src="/img/trClosed.gif" title="{{$art-menu-resources/governance-group}}"/>
        </xhtml:a>
    </xsl:template>
    
   <!-- match all elements and attributes, but not comment nodes -->
    <xsl:template match="@*|node()">
        <xsl:choose>
         <!-- insert requested document into document instance -->
            <xsl:when test="name(.)='xforms:instance' and @id='document' and string-length($documentChecked)&gt;1">
                <xforms:instance id="document">
                    <name>
                        <xsl:value-of select="$documentChecked"/>
                    </name>
                </xforms:instance>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:apply-templates select="@*|node()"/>
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>