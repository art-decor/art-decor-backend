xquery version "3.0";
(:
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.
:)
import module namespace sm      = "http://exist-db.org/xquery/securitymanager";
import module namespace xmldb   = "http://exist-db.org/xquery/xmldb";
import module namespace repo    = "http://exist-db.org/xquery/repo";
declare namespace cfg           = "http://exist-db.org/collection-config/1.0";
(: The following external variables are set by the repo:deploy function :)

(: file path pointing to the exist installation directory :)
declare variable $home external;
(: path to the directory containing the unpacked .xar package :)
declare variable $dir external;
(: the target collection into which the app is deployed :)
declare variable $target external;
(:install path for art (/db, /db/apps), no trailing slash :)
declare variable $root := repo:get-root();

declare %private function local:getArtIndexV2() as element() {
<collection xmlns="http://exist-db.org/collection-config/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema"  xmlns:xforms="http://www.w3.org/2002/xforms" xmlns:fr="http://orbeon.org/oxf/xml/form-runner" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!-- art index definition for eXist 2.2 -->
    <index>
        <fulltext default="none" attributes="false"/>
        <create qname="xsl:attribute" type="xs:string"/>
        <create qname="xforms:output" type="xs:string"/>
        <create qname="xforms:label" type="xs:string"/>
        <create qname="fr:label" type="xs:string"/>
        <create qname="section" type="xs:string"/>
        <create qname="application" type="xs:string"/>
        <create qname="@id" type="xs:string"/>
        <create qname="@ref" type="xs:string"/>
        <create qname="@label" type="xs:string"/>
        <create qname="@value" type="xs:string"/>
    </index>
</collection>
};
declare %private function local:getArtIndexV4() as element() {
<collection xmlns="http://exist-db.org/collection-config/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema"  xmlns:xforms="http://www.w3.org/2002/xforms" xmlns:fr="http://orbeon.org/oxf/xml/form-runner" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!-- art index definition for eXist 5.3. and up -->
    <index>
        <range>
            <create qname="xsl:attribute" type="xs:string"/>
            <create qname="xforms:output" type="xs:string"/>
            <create qname="xforms:label" type="xs:string"/>
            <create qname="fr:label" type="xs:string"/>
            <create qname="@id" type="xs:string"/>
            <create qname="@ref" type="xs:string"/>
            <create qname="@label" type="xs:string"/>
            <create qname="@title" type="xs:string"/>
            <create qname="@value" type="xs:string"/>
        </range>
    </index>
</collection>
};
declare %private function local:getArtDataIndexV2() as element() {
<collection xmlns="http://exist-db.org/collection-config/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <!-- art-data index definition for eXist 2.2 -->
    <index>
        <fulltext default="none" attributes="false"/>
        <lucene>
            <analyzer class="org.apache.lucene.analysis.standard.StandardAnalyzer">
                <param name="stopwords" type="org.apache.lucene.analysis.util.CharArraySet"/>
            </analyzer>
            <text qname="name"/>
            <text qname="synonym"/>
            <text qname="desc"/>
            <text qname="@name"/>
            <text qname="@displayName"/>
        </lucene>
        <create qname="@id" type="xs:string"/>
        <create qname="@label" type="xs:string"/>
        <create qname="@language" type="xs:string"/>
        <create qname="@name" type="xs:string"/>
        <create qname="@notifier" type="xs:string"/>
        <create qname="@notify" type="xs:string"/>
        <create qname="@prefix" type="xs:string"/>
        <create qname="@packageRoot" type="xs:string"/>
        <create qname="@updated" type="xs:string"/>
        <create qname="@user" type="xs:string"/>
        <create qname="@value" type="xs:string"/>
        <create qname="@xml:lang" type="xs:string"/>
    </index>
</collection>
};
declare %private function local:getArtDataIndexV4() as element() {
<collection xmlns="http://exist-db.org/collection-config/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <!-- art-data index definition for eXist 5.3 and up -->
    <index>
        <lucene>
            <analyzer class="org.apache.lucene.analysis.standard.StandardAnalyzer">
                <param name="stopwords" type="org.apache.lucene.analysis.util.CharArraySet"/>
            </analyzer>
            <text qname="name"/>
            <text qname="synonym"/>
            <text qname="desc"/>
            <text qname="@name"/>
            <text qname="@displayName"/>
        </lucene>
        <range>
            <create qname="@id" type="xs:string"/>
            <create qname="@label" type="xs:string"/>
            <create qname="@language" type="xs:string"/>
            <create qname="@name" type="xs:string"/>
            <create qname="@notifier" type="xs:string"/>
            <create qname="@notify" type="xs:string"/>
            <create qname="@prefix" type="xs:string"/>
            <create qname="@packageRoot" type="xs:string"/>
            <create qname="@ref" type="xs:string"/>
            <create qname="@title" type="xs:string"/>
            <create qname="@updated" type="xs:string"/>
            <create qname="@user" type="xs:string"/>
            <create qname="@value" type="xs:string"/>
            <create qname="@xml:lang" type="xs:string"/>
        </range>
    </index>
</collection>
};
declare %private function local:getDecorIndexV2() as element() {
<collection xmlns="http://exist-db.org/collection-config/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <!-- decor index definition for eXist 2.2 -->
    <index>
        <fulltext default="none" attributes="false"/>
        <lucene>
            <analyzer class="org.apache.lucene.analysis.standard.StandardAnalyzer">
                <param name="stopwords" type="org.apache.lucene.analysis.util.CharArraySet"/>
            </analyzer>
            <text qname="name"/>
            <text qname="synonym"/>
            <text qname="desc"/>
            <text qname="@name"/>
            <text qname="@displayName"/>
        </lucene>
        <create qname="@bbrident" type="xs:string"/>
        <create qname="@bbrurl" type="xs:string"/>
        <create qname="@canonicalUri" type="xs:string"/>
        <create qname="@code" type="xs:string"/>
        <create qname="@codeSystem" type="xs:string"/>
        <create qname="@concept" type="xs:string"/>
        <create qname="@conceptId" type="xs:string"/>
        <create qname="@conceptFlexibility" type="xs:string"/>
        <create qname="@contains" type="xs:string"/>
        <create qname="@date" type="xs:string"/>
        <create qname="@displayName" type="xs:string"/>
        <create qname="@deeplinkprefixservices" type="xs:string"/>
        <create qname="@effectiveDate" type="xs:string"/>
        <create qname="@elementId" type="xs:string"/>
        <create qname="@experimental" type="xs:string"/>
        <create qname="@flexibility" type="xs:string"/>
        <create qname="@id" type="xs:string"/>
        <create qname="@ident" type="xs:string"/>
        <create qname="@key" type="xs:string"/>
        <create qname="@name" type="xs:string"/>
        <create qname="@notifier" type="xs:string"/>
        <create qname="@private" type="xs:string"/>
        <create qname="@prefix" type="xs:string"/>
        <create qname="@ref" type="xs:string"/>
        <create qname="@repository" type="xs:string"/>
        <create qname="@root" type="xs:string"/>
        <create qname="@sourceDataset" type="xs:string"/>
        <create qname="@sourceDatasetFlexibility" type="xs:string"/>
        <create qname="@statusCode" type="xs:string"/>
        <create qname="@templateId" type="xs:string"/>
        <create qname="@transactionRef" type="xs:string"/>
        <create qname="@url" type="xs:string"/>
        <create qname="@username" type="xs:string"/>
        <create qname="@valueSet" type="xs:string"/>
        <create qname="@versionDate" type="xs:string"/>
        <create qname="@type" type="xs:string"/>
    </index>
</collection>
};
declare %private function local:getDecorIndexV4() as element() {
<collection xmlns="http://exist-db.org/collection-config/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <!-- decor index definition for eXist 5.3 -->
    <index>
        <lucene>
            <analyzer class="org.apache.lucene.analysis.standard.StandardAnalyzer">
                <param name="stopwords" type="org.apache.lucene.analysis.util.CharArraySet"/>
            </analyzer>
            <text qname="name"/>
            <text qname="synonym"/>
            <text qname="desc"/>
            <text qname="@name"/>
            <text qname="@displayName"/>
        </lucene>
        <range>
            <create qname="@bbrident" type="xs:string"/>
            <create qname="@bbrurl" type="xs:string"/>
            <create qname="@canonicalUri" type="xs:string"/>
            <create qname="@code" type="xs:string"/>
            <create qname="@codeSystem" type="xs:string"/>
            <create qname="@concept" type="xs:string"/>
            <create qname="@conceptId" type="xs:string"/>
            <create qname="@conceptFlexibility" type="xs:string"/>
            <create qname="@contains" type="xs:string"/>
            <create qname="@date" type="xs:string"/>
            <create qname="@displayName" type="xs:string"/>
            <create qname="@deeplinkprefixservices" type="xs:string"/>
            <create qname="@effectiveDate" type="xs:string"/>
            <create qname="@elementId" type="xs:string"/>
            <create qname="@experimental" type="xs:string"/>
            <create qname="@flexibility" type="xs:string"/>
            <create qname="@id" type="xs:string"/>
            <create qname="@ident" type="xs:string"/>
            <create qname="@key" type="xs:string"/>
            <create qname="@name" type="xs:string"/>
            <create qname="@notifier" type="xs:string"/>
            <create qname="@private" type="xs:string"/>
            <create qname="@prefix" type="xs:string"/>
            <create qname="@questionnaireId" type="xs:string"/>
            <create qname="@questionnaireEffectiveDate" type="xs:string"/>
            <create qname="@ref" type="xs:string"/>
            <create qname="@repository" type="xs:string"/>
            <create qname="@representingQuestionnaire" type="xs:string"/>
            <create qname="@representingQuestionnaireFlexibility" type="xs:string"/>
            <create qname="@root" type="xs:string"/>
            <create qname="@sourceDataset" type="xs:string"/>
            <create qname="@sourceDatasetFlexibility" type="xs:string"/>
            <create qname="@statusCode" type="xs:string"/>
            <create qname="@templateId" type="xs:string"/>
            <create qname="@transactionRef" type="xs:string"/>
            <create qname="@url" type="xs:string"/>
            <create qname="@username" type="xs:string"/>
            <create qname="@valueSet" type="xs:string"/>
            <create qname="@versionDate" type="xs:string"/>
            <create qname="@type" type="xs:string"/>
        </range>
    </index>
</collection>
(:
<collection xmlns="http://exist-db.org/collection-config/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <!-- decor index definition for eXist 5.3 and up -->
    <index>
        <lucene>
            <analyzer class="org.apache.lucene.analysis.standard.StandardAnalyzer">
                <param name="stopwords" type="org.apache.lucene.analysis.util.CharArraySet"/>
            </analyzer>
            <text qname="name"/>
            <text qname="synonym"/>
            <text qname="desc"/>
            <text qname="@name"/>
            <text qname="@displayName"/>
        </lucene>
        <range>
            <create qname="@canonicalUri" type="xs:string"/>
            <create qname="@code" type="xs:string"/>
            <create qname="@codeSystem" type="xs:string"/>
            <create qname="@concept" type="xs:string"/>
            <create qname="@conceptId" type="xs:string"/>
            <!--<create qname="@conceptFlexibility" type="xs:string"/>-->
            <create qname="@contains" type="xs:string"/>
            <create qname="@date" type="xs:string"/>
            <!--<create qname="@displayName" type="xs:string"/>-->
            <!--<create qname="@deeplinkprefixservices" type="xs:string"/>-->
            <!--<create qname="@effectiveDate" type="xs:string"/>-->
            <create qname="@elementId" type="xs:string"/>
            <create qname="@experimental" type="xs:string"/>
            <!--<create qname="@flexibility" type="xs:string"/>-->
            <create qname="@id" type="xs:string"/>
            <!--<create qname="@ident" type="xs:string"/>-->
            <create qname="@key" type="xs:string"/>
            <!--<create qname="@name" type="xs:string"/>-->
            <!--<create qname="@notifier" type="xs:string"/>-->
            <create qname="@private" type="xs:string"/>
            <create qname="@prefix" type="xs:string"/>
            <create qname="@ref" type="xs:string"/>
            <create qname="@repository" type="xs:string"/>
            <create qname="@root" type="xs:string"/>
            <create qname="@sourceDataset" type="xs:string"/>
            <!--<create qname="@sourceDatasetFlexibility" type="xs:string"/>-->
            <!--<create qname="@statusCode" type="xs:string"/>-->
            <create qname="@templateId" type="xs:string"/>
            <!--<create qname="@transactionRef" type="xs:string"/>-->
            <!--<create qname="@url" type="xs:string"/>-->
            <create qname="@username" type="xs:string"/>
            <create qname="@valueSet" type="xs:string"/>
            <create qname="@versionDate" type="xs:string"/>
            <create qname="@type" type="xs:string"/>
            
            <!-- cached decor project from other servers -->
            <create qname="cacheme">
                <field name="cacheme-bbrident" match="@bbrident" type="xs:string"/>
                <field name="cacheme-bbrurl" match="@bbrurl" type="xs:string"/>
            </create>
            <!-- compiled decor releases have these markers -->
            <create qname="decor">
                <field name="decor-versionDate" match="@versionDate" type="xs:string"/>
                <field name="decor-language" match="@language" type="xs:string"/>
            </create>
            <!--<create qname="project">
                <field name="project-prefix" match="@prefix" type="xs:string"/>
            </create>-->
            <!--<create qname="project">
                <field name="project-id" match="@id" type="xs:string"/>
            </create>-->
            <!-- baseId -->
            <!--create qname="baseId">
                <field name="baseId-id" match="@id" type="xs:string"/>
            </create-->
            <create qname="dataset">
                <field name="dataset-id" match="@id" type="xs:string"/>
                <field name="dataset-effectiveDate" match="@effectiveDate" type="xs:string"/>
            </create>
            <create qname="concept">
                <field name="concept-id" match="@id" type="xs:string"/>
                <field name="concept-effectiveDate" match="@effectiveDate" type="xs:string"/>
            </create>
            <create qname="valueSet">
                <field name="valueSet-id" match="@id" type="xs:string"/>
                <field name="valueSet-effectiveDate" match="@effectiveDate" type="xs:string"/>
            </create>
            <create qname="codeSystem">
                <field name="codeSystem-id" match="@id" type="xs:string"/>
                <field name="codeSystem-effectiveDate" match="@effectiveDate" type="xs:string"/>
            </create>
            <create qname="scenario">
                <field name="scenario-id" match="@id" type="xs:string"/>
                <field name="scenario-effectiveDate" match="@effectiveDate" type="xs:string"/>
            </create>
            <create qname="transaction">
                <field name="transaction-id" match="@id" type="xs:string"/>
                <field name="transaction-effectiveDate" match="@effectiveDate" type="xs:string"/>
            </create>
            <!-- transaction representingTemplate connected to a dataset -->
            <create qname="representingTemplate">
                <field name="representingTemplate-sourceDataset" match="@sourceDataset" type="xs:string"/>
                <field name="representingTemplate-sourceDatasetFlexibility" match="@sourceDatasetFlexibility" type="xs:string"/>
            </create>
            <!-- transaction representingTemplate connected to a template -->
            <create qname="representingTemplate">
                <field name="representingTemplate-ref" match="@ref" type="xs:string"/>
                <field name="representingTemplate-flexibility" match="@flexibility" type="xs:string"/>
            </create>
            <!-- transaction representingTemplate connected to a concept -->
            <create qname="concept">
                <field name="concept-ref" match="@ref" type="xs:string"/>
                <field name="concept-flexibility" match="@flexibility" type="xs:string"/>
            </create>
            <!-- template association for a concept -->
            <create qname="concept">
                <field name="concept-ref" match="@ref" type="xs:string"/>
                <field name="concept-effectiveDate" match="@effectiveDate" type="xs:string"/>
            </create>
            <create qname="template">
                <field name="template-id" match="@id" type="xs:string"/>
                <field name="template-effectiveDate" match="@effectiveDate" type="xs:string"/>
            </create>
            <create qname="templateAssociation">
                <field name="templateAssociation-id" match="@templateId" type="xs:string"/>
                <field name="templateAssociation-effectiveDate" match="@effectiveDate" type="xs:string"/>
            </create>
            <!-- connection to a concept -->
            <create qname="terminologyAssociation">
                <field name="terminologyAssociation-conceptId" match="@conceptId" type="xs:string"/>
                <field name="terminologyAssociation-conceptFlexibility" match="@conceptFlexibility" type="xs:string"/>
            </create>
            <!-- dataset concept connection to a value set -->
            <create qname="terminologyAssociation">
                <field name="terminologyAssociation-valueSet" match="@valueSet" type="xs:string"/>
                <field name="terminologyAssociation-flexibility" match="@flexibility" type="xs:string"/>
            </create>
            <!-- connection to a concept -->
            <create qname="identifierAssociation">
                <field name="identifierAssociation-conceptId" match="@conceptId" type="xs:string"/>
                <field name="identifierAssociation-conceptFlexibility" match="@conceptFlexibility" type="xs:string"/>
            </create>
            <!-- template element or attribute connection to a value set -->
            <create qname="vocabulary">
                <field name="vocabulary-valueSet" match="@valueSet" type="xs:string"/>
                <field name="vocabulary-flexibility" match="@flexibility" type="xs:string"/>
            </create>
            <!-- template element connection to a template -->
            <create qname="element">
                <field name="element-contains" match="@contains" type="xs:string"/>
                <field name="element-flexibility" match="@flexibility" type="xs:string"/>
            </create>
            <!-- concept inherit from other concept -->
            <create qname="inherit">
                <field name="inherit-ref" match="@ref" type="xs:string"/>
                <field name="inherit-effectiveDate" match="@effectiveDate" type="xs:string"/>
            </create>
            <!-- concept contains -->
            <create qname="contains">
                <field name="contains-ref" match="@ref" type="xs:string"/>
                <field name="contains-flexibility" match="@flexibility" type="xs:string"/>
            </create>
            <!-- dataset, concept, template etc. relationship -->
            <create qname="relationship">
                <field name="relationship-ref" match="@ref" type="xs:string"/>
                <field name="relationship-flexibility" match="@flexibility" type="xs:string"/>
            </create>
            <!-- template -->
            <create qname="include">
                <field name="include-ref" match="@ref" type="xs:string"/>
                <field name="include-flexibility" match="@flexibility" type="xs:string"/>
            </create>
            <!-- issue -->
            <create qname="object">
                <field name="object-id" match="@id" type="xs:string"/>
                <field name="object-effectiveDate" match="@effectiveDate" type="xs:string"/>
            </create>
            <!-- my community -->
            <create qname="object">
                <field name="object-ref" match="@ref" type="xs:string"/>
                <field name="object-flexibility" match="@flexibility" type="xs:string"/>
            </create>
        </range>
    </index>
</collection>
:)
};
declare %private function local:getIGIndexV2() as element() {
<collection xmlns="http://exist-db.org/collection-config/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <!-- decor index definition for eXist 2.2 -->
    <index>
        <lucene>
            <analyzer class="org.apache.lucene.analysis.standard.StandardAnalyzer">
                <param name="stopwords" type="org.apache.lucene.analysis.util.CharArraySet"/>
            </analyzer>
            <text qname="@name"/>
            <text qname="title"/>
            <text qname="desc"/>
        </lucene>
        <create qname="@canonicalUri" type="xs:string"/>
        <create qname="@effectiveDate" type="xs:string"/>
        <create qname="@id" type="xs:string"/>
        <create qname="@name" type="xs:string"/>
        <create qname="@projectId" type="xs:string"/>
    </index>
</collection>
};
declare %private function local:getIGIndexV4() as element() {
<collection xmlns="http://exist-db.org/collection-config/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <!-- decor index definition for eXist 5.3 -->
    <index>
        <lucene>
            <analyzer class="org.apache.lucene.analysis.standard.StandardAnalyzer">
                <param name="stopwords" type="org.apache.lucene.analysis.util.CharArraySet"/>
            </analyzer>
            <text qname="@name"/>
            <text qname="title"/>
            <text qname="desc"/>
        </lucene>
        <range>
            <create qname="@canonicalUri" type="xs:string"/>
            <create qname="@effectiveDate" type="xs:string"/>
            <create qname="@id" type="xs:string"/>
            <create qname="@name" type="xs:string"/>
            <create qname="@projectId" type="xs:string"/>
        </range>
    </index>
</collection>
};
declare %private function local:getDecorCoreIndexV2() as element() {
<collection xmlns="http://exist-db.org/collection-config/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <!-- decor/core index definition for eXist 2.2 -->
    <index>
        <fulltext default="none" attributes="false"/>
        <lucene>
            <analyzer class="org.apache.lucene.analysis.standard.StandardAnalyzer">
                <param name="stopwords" type="org.apache.lucene.analysis.util.CharArraySet"/>
            </analyzer>
            <text qname="name"/>
            <text qname="synonym"/>
            <text qname="desc"/>
            <text qname="@name"/>
            <text qname="@displayName"/>
        </lucene>
        <create qname="@code" type="xs:string"/>
        <create qname="@codeSystem" type="xs:string"/>
        <create qname="@concept" type="xs:string"/>
        <create qname="@conceptId" type="xs:string"/>
        <create qname="@conceptFlexibility" type="xs:string"/>
        <create qname="@contains" type="xs:string"/>
        <create qname="@displayName" type="xs:string"/>
        <create qname="@deeplinkprefixservices" type="xs:string"/>
        <create qname="@effectiveDate" type="xs:string"/>
        <create qname="@elementId" type="xs:string"/>
        <create qname="@flexibility" type="xs:string"/>
        <create qname="@id" type="xs:string"/>
        <create qname="@ident" type="xs:string"/>
        <create qname="@key" type="xs:string"/>
        <create qname="@name" type="xs:string"/>
        <create qname="@notifier" type="xs:string"/>
        <create qname="@prefix" type="xs:string"/>
        <create qname="@ref" type="xs:string"/>
        <create qname="@repository" type="xs:string"/>
        <create qname="@root" type="xs:string"/>
        <create qname="@sourceDataset" type="xs:string"/>
        <create qname="@sourceDatasetFlexibility" type="xs:string"/>
        <create qname="@statusCode" type="xs:string"/>
        <create qname="@templateId" type="xs:string"/>
        <create qname="@transactionRef" type="xs:string"/>
        <create qname="@url" type="xs:string"/>
        <create qname="@username" type="xs:string"/>
        <create qname="@valueSet" type="xs:string"/>
        <create qname="@versionDate" type="xs:string"/>
        <create qname="@type" type="xs:string"/>
    </index>
</collection>
};
declare %private function local:getDecorCoreIndexV4() as element() {
<collection xmlns="http://exist-db.org/collection-config/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <!-- decor/core index definition for eXist 5.3 and up -->
    <index>
        <range>
            <create qname="@key" type="xs:string"/>
            <create qname="@name" type="xs:string"/>
            <create qname="@packageRoot" type="xs:string"/>
        </range>
    </index>
</collection>
};
(: keep in sync with art/modules/fix-object-history.xquery :)
declare %private function local:getDecorHistoryIndexV2() as element() {
<collection xmlns="http://exist-db.org/collection-config/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <!-- decor/history index definition for eXist 2.2 -->
    <index>
        <fulltext default="none" attributes="false"/>
        <create qname="@artefactType" type="xs:string"/>
        <create qname="@projectPrefix" type="xs:string"/>
        <create qname="@artefactId" type="xs:string"/>
        <create qname="@artefactEffectiveDate" type="xs:string"/>
        <create qname="@date" type="xs:string"/>
    </index>
</collection>
};
(: keep in sync with art/modules/fix-object-history.xquery :)
declare %private function local:getDecorHistoryIndexV4() as element() {
<collection xmlns="http://exist-db.org/collection-config/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <!-- decor/history index definition for eXist 5.3 and up -->
    <index>
        <range>
            <create qname="@artefactType" type="xs:string"/>
            <create qname="@projectPrefix" type="xs:string"/>
            <create qname="@artefactId" type="xs:string"/>
            <create qname="@artefactEffectiveDate" type="xs:string"/>
            <create qname="@date" type="xs:string"/>
        </range>
    </index>
</collection>
};
declare %private function local:getDecorReleasesIndexV2() as element() {
<collection xmlns="http://exist-db.org/collection-config/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <!-- decor/releases index definition for eXist 2.2 -->
    <index>
        <fulltext default="none" attributes="false"/>
        <create qname="@bbrident" type="xs:string"/>
        <create qname="@bbrurl" type="xs:string"/>
        <create qname="@canonicalUri" type="xs:string"/>
        <create qname="@code" type="xs:string"/>
        <create qname="@codeSystem" type="xs:string"/>
        <create qname="@concept" type="xs:string"/>
        <create qname="@conceptId" type="xs:string"/>
        <create qname="@conceptFlexibility" type="xs:string"/>
        <create qname="@contains" type="xs:string"/>
        <create qname="@date" type="xs:string"/>
        <create qname="@displayName" type="xs:string"/>
        <create qname="@deeplinkprefixservices" type="xs:string"/>
        <create qname="@effectiveDate" type="xs:string"/>
        <create qname="@elementId" type="xs:string"/>
        <create qname="@experimental" type="xs:string"/>
        <create qname="@flexibility" type="xs:string"/>
        <create qname="@id" type="xs:string"/>
        <create qname="@ident" type="xs:string"/>
        <create qname="@key" type="xs:string"/>
        <create qname="@name" type="xs:string"/>
        <create qname="@notifier" type="xs:string"/>
        <create qname="@private" type="xs:string"/>
        <create qname="@prefix" type="xs:string"/>
        <create qname="@ref" type="xs:string"/>
        <create qname="@repository" type="xs:string"/>
        <create qname="@root" type="xs:string"/>
        <create qname="@sourceDataset" type="xs:string"/>
        <create qname="@sourceDatasetFlexibility" type="xs:string"/>
        <create qname="@statusCode" type="xs:string"/>
        <create qname="@templateId" type="xs:string"/>
        <create qname="@transactionRef" type="xs:string"/>
        <create qname="@url" type="xs:string"/>
        <create qname="@username" type="xs:string"/>
        <create qname="@valueSet" type="xs:string"/>
        <create qname="@versionDate" type="xs:string"/>
        <create qname="@type" type="xs:string"/>
    </index>
</collection>
};
declare %private function local:getDecorReleasesIndexV4() as element() {
<collection xmlns="http://exist-db.org/collection-config/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <!-- decor/releases index definition for eXist 5.3 and up -->
    <index>
        <range>
            <create qname="@contains" type="xs:string"/>
            <create qname="@defaultLanguage" type="xs:string"/>
            <create qname="@effectiveDate" type="xs:string"/>
            <create qname="@flexibility" type="xs:string"/>
            <create qname="@id" type="xs:string"/>
            <create qname="@prefix" type="xs:string"/>
            <create qname="@questionnaireId" type="xs:string"/>
            <create qname="@questionnaireEffectiveDate" type="xs:string"/>
            <create qname="@ref" type="xs:string"/>
            <create qname="@representingQuestionnaire" type="xs:string"/>
            <create qname="@representingQuestionnaireFlexibility" type="xs:string"/>
            <create qname="@sourceDataset" type="xs:string"/>
            <create qname="@sourceDatasetFlexibility" type="xs:string"/>
            <create qname="@templateId" type="xs:string"/>
            <create qname="@valueSet" type="xs:string"/>
            <create qname="@versionDate" type="xs:string"/>
            <!--<create qname="decor">
                <field name="decor-language" match="@language" type="xs:string"/>
                <field name="decor-versionDate" match="@versionDate" type="xs:string"/>
            </create>
            <create qname="project">
                <field name="project-prefix" match="@prefix" type="xs:string"/>
                <field name="project-defaultLanguage" match="@defaultLanguage" type="xs:string"/>
            </create>-->
        </range>
    </index>
</collection>
};
declare %private function local:getHL7IndexV2() as element() {
<collection xmlns="http://exist-db.org/collection-config/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <!-- hl7 index definition for eXist 2.2 -->
    <index xmlns:hl7="urn:hl7-org:v3">
        <fulltext default="none" attributes="false"/>
        <create qname="@code" type="xs:string"/>
        <create qname="@codeSystem" type="xs:string"/>
        <create qname="@extension" type="xs:string"/>
        <create qname="@message" type="xs:string"/>
        <create qname="@name" type="xs:string"/>
        <create qname="@ref" type="xs:string"/>
        <create qname="@root" type="xs:string"/>
        <create qname="@value" type="xs:string"/>
    </index>
</collection>
};
declare %private function local:getHL7IndexV4() as element() {
<collection xmlns="http://exist-db.org/collection-config/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <!-- hl7 index definition for eXist 5.3 and up -->
    <index xmlns:hl7="urn:hl7-org:v3">
        <range>
            <create qname="@code" type="xs:string"/>
            <create qname="@codeSystem" type="xs:string"/>
            <create qname="@extension" type="xs:string"/>
            <create qname="@message" type="xs:string"/>
            <create qname="@name" type="xs:string"/>
            <create qname="@ref" type="xs:string"/>
            <create qname="@root" type="xs:string"/>
            <create qname="@value" type="xs:string"/>
        </range>
    </index>
</collection>
};

(: helper function for creating top level database collection and index definitions required for Art webapplication :)
declare %private function local:createTopCollections() {
    (: 2.2-LTS or 4.0.0 :)
    let $system-version     := tokenize(system:get-version(), '\.')[1]
    (: new range appears stable from version 4 and up:)
    let $use-new-range      := if ($system-version castable as xs:integer) then xs:integer($system-version) ge 4 else false()
    let $artConf            := if ($use-new-range) then local:getArtIndexV4() else local:getArtIndexV2()
    
    let $artDataConf        := if ($use-new-range) then local:getArtDataIndexV4() else local:getArtDataIndexV2()
    
    (: cache + data :)
    let $decorConf          := if ($use-new-range) then local:getDecorIndexV4() else local:getDecorIndexV2()
    
    (: core - not sure why this index is needed :)
    (:let $decorCoreConf      := if ($use-new-range) then local:getDecorCoreIndexV4() else local:getDecorCoreIndexV2():)
    
    let $decorHistoryConf   := if ($use-new-range) then local:getDecorHistoryIndexV4() else local:getDecorHistoryIndexV2()
    
    (:$decorImplementationGuidesConf:)
    let $decorIGsConf       := if ($use-new-range) then local:getIGIndexV4() else local:getIGIndexV2()
    
    let $decorReleasesConf  := if ($use-new-range) then local:getDecorReleasesIndexV4() else local:getDecorReleasesIndexV2()
    
    let $hl7Conf            := if ($use-new-range) then local:getHL7IndexV4() else local:getHL7IndexV2()
    
    return (
        (:/db/apps collections:)
        for $coll in ('ada','art-data','art-data/resources/stylesheets','decor/cache','decor/data/projects','decor/history','decor/implementationguides','decor/releases','decor/scheduled-tasks','hl7','terminology','terminology-data')
        let $ttt  := xmldb:create-collection($root, $coll)
        let $tt   := sm:chmod(xs:anyURI($ttt), 'rwxrwsr-x')
        let $tt   := sm:chown(xs:anyURI($ttt), 'admin:decor')
        return 
            ()
        ,
        (:/db/system collections:)
        (:for $coll in ('art','art-data','decor/cache','decor/core','decor/data','decor/history','decor/implementationguides','decor/releases','hl7'):)
        for $coll in ('art','art-data','decor/cache','decor/data','decor/history','decor/implementationguides','decor/releases','hl7')
        return (
            xmldb:create-collection(concat('/db/system/config', $root), $coll)
        )
        ,
        (:we used to have 1 index on all of decor. splitting that up into cache, data and releases:)
        if (doc-available(concat('/db/system/config', $root, 'decor/collection.xconf'))) then (
            xmldb:remove(concat('/db/system/config', $root, 'decor'),'collection.xconf')
        ) else ()
        ,
        (:== indexes ==:)
        let $index-file := concat('/db/system/config', $root, 'art/collection.xconf')
        return
        if (doc-available($index-file)) then () else (
            xmldb:store(concat('/db/system/config', $root, 'art'),'collection.xconf', $artConf),
            xmldb:reindex(concat($root,'art'))
        )
        ,
        let $index-file := concat('/db/system/config',$root, 'art-data/collection.xconf')
        return
        if (doc-available($index-file)) then () else (
            xmldb:store(concat('/db/system/config', $root, 'art-data'),'collection.xconf', $artDataConf),
            xmldb:reindex(concat($root,'art-data'))
        )
        (:,
        let $index-file := concat('/db/system/config',$root, 'decor/core/collection.xconf')
        return
        if (doc-available($index-file)) then () else (
            xmldb:store(concat('/db/system/config', $root, 'decor/core'),'collection.xconf', $decorCoreConf),
            xmldb:reindex(concat($root,'decor/core'))
        ):)
        ,
        for $coll in ('decor/cache', 'decor/data')
        let $index-file := concat('/db/system/config', $root, $coll, '/collection.xconf')
        return
        if (doc-available($index-file)) then () else (
            xmldb:store(concat('/db/system/config',$root, $coll), 'collection.xconf', $decorConf),
            if (xmldb:collection-available(concat($root, $coll))) then (
                xmldb:reindex(concat($root,$coll))
            ) else ()
        )
        ,
        for $coll in ('decor/history')
        let $index-file := concat('/db/system/config', $root, $coll, '/collection.xconf')
        return
        if (doc-available($index-file)) then () else (
            xmldb:store(concat('/db/system/config',$root, $coll), 'collection.xconf', $decorHistoryConf),
            if (xmldb:collection-available(concat($root, $coll))) then (
                xmldb:reindex(concat($root,$coll))
            ) else ()
        )
        ,
        for $coll in ('decor/implementationguides')
        let $index-file := concat('/db/system/config', $root, $coll, '/collection.xconf')
        return
        if (doc-available($index-file)) then () else (
            xmldb:store(concat('/db/system/config',$root, $coll), 'collection.xconf', $decorIGsConf),
            if (xmldb:collection-available(concat($root, $coll))) then (
                xmldb:reindex(concat($root,$coll))
            ) else ()
        )
        ,
        for $coll in ('decor/releases')
        let $index-file := concat('/db/system/config', $root, $coll, '/collection.xconf')
        return
        if (doc-available($index-file)) then () else (
            xmldb:store(concat('/db/system/config',$root, $coll), 'collection.xconf', $decorReleasesConf),
            if (xmldb:collection-available(concat($root, $coll))) then (
                xmldb:reindex(concat($root,$coll))
            ) else ()
        )
        ,
        let $index-file := concat('/db/system/config',$root,'hl7/collection.xconf')
        return
        if (doc-available($index-file)) then () else (
            xmldb:store(concat('/db/system/config', $root, 'hl7'),'collection.xconf', $hl7Conf),
            xmldb:reindex(concat($root,'hl7'))
        )
    )
};

(: helper function for creating database groups required for Art webapplication :)
declare %private function local:createArtGroups() {
   if (sm:group-exists('decor')) then () else (
      sm:create-group('decor','admin','Group for general access to DECOR files')
   )
      ,
   if (sm:group-exists('decor-admin')) then () else (
      sm:create-group('decor-admin','admin','Group for DECOR project admins')
   )
   ,
   if (sm:group-exists('terminology')) then () else (
      sm:create-group('terminology','admin','General terminology group')
   )
   ,
   if (sm:group-exists('issues')) then () else (
      sm:create-group('issues','admin','Group to let users create issues')
   )
   ,
   if (sm:group-exists('editor')) then () else (
      sm:create-group('editor','admin','Group to let users edit in general')
   )
   ,
   if (sm:group-exists('tools')) then () else (
      sm:create-group('tools','admin','General tools group')
   )
      ,
   if (sm:group-exists('debug')) then () else (
      sm:create-group('debug','admin','Group to switch on Orbeon debug view for user')
   )
};

(:let $update := sm:set-user-primary-group('admin','dba'):)
let $update := local:createTopCollections()
let $update := local:createArtGroups()

return ()
